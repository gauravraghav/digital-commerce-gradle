// no copyright notice
package webservices;

import javax.naming.Context;

import com.digital.commerce.common.exception.RepricingServicesException;
import com.digital.commerce.common.services.repricing.IRepricingServices;

public class RequestRepricingSEIImpl extends atg.webservice.ManagedComponentProperties
		implements javax.xml.rpc.server.ServiceLifecycle, RequestRepricingSEI {
	private static final String SERVICE_NAME = "requestRepricing";

	private boolean sRegistered = false;

	private boolean sEnabled = true;

	public java.lang.String requestRepricing(java.lang.String requestString)
			throws java.rmi.RemoteException, atg.security.SecurityException {
		if (!sRegistered) {
			sEnabled = register(null);
			sRegistered = true;
		}

		if (!sEnabled) {
			if (isLoggingDebug())
				logDebug(SERVICE_NAME + " error: disabled");
			throw new java.rmi.RemoteException("disabled");
		}

		try {
			if (isLoggingDebug()) {
				logDebug(String.format("%s - %s: REQUEST - %s", SERVICE_NAME, SERVICE_NAME, requestString));
			}

			// look up the Nucleus service and try to call it
			String jndiName = "dynamo:/com/digital/commerce/services/pricing/reprice/RepricingServices";
			Context ctx = new javax.naming.InitialContext();
			IRepricingServices s = (IRepricingServices) ctx.lookup(jndiName);

			String response = s.repriceOrder(requestString);

			if (isLoggingDebug()) {
				logDebug(SERVICE_NAME
						+ " debug: executing com.digital.commerce.services.promotions.DigitalPromotionService.requestRepricing");

				logDebug(String.format("%s - %s: RESPONSE - %s", SERVICE_NAME, SERVICE_NAME, response));
			}

			return response;

		} catch (javax.naming.NamingException | RepricingServicesException ne) {
			if (isLoggingError())
				logError(SERVICE_NAME + " error: " + ne, ne);
			throw new java.rmi.RemoteException(ne.getMessage(), ne);
		}

	}

	public void destroy() {
		// Do nothing
	}

	public void init(Object pObject) throws javax.xml.rpc.ServiceException {
		if (!sRegistered) {
			sEnabled = register(pObject);
			sRegistered = true;
		}
	}
	@Override
	public String getComponentName() {
		return SERVICE_NAME;
	}
}
