package com.digital.commerce.integration.storedvalue.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StoredValueServiceRequest {
	public static final String USD = "USD";
	private String currencyName;
	private Double amount;
	private String cardNumber;
	private String pin;
	private String transactionId;

}
