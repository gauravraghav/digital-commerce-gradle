package com.digital.commerce.integration.storedvalue.domain;

import javax.xml.datatype.XMLGregorianCalendar;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StoredValueServiceResponse {
	private String transactionId;
	private Double amount;
	private boolean isSuccess;
	private String authorizationCode;
	private String returnCode;
	private String returnDescription;
	private XMLGregorianCalendar authorizationExpirationDate;

}
