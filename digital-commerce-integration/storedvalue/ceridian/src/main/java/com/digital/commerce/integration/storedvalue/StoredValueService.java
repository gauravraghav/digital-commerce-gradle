package com.digital.commerce.integration.storedvalue;

import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.service.Service;
import com.digital.commerce.integration.storedvalue.domain.StoredValueServiceRequest;
import com.digital.commerce.integration.storedvalue.domain.StoredValueServiceResponse;

import lombok.Getter;
import lombok.Setter;

/**
 * Interface to be used by the application to perform gift card authorization, 
 * balance inquiry, and authorization complete
 * 
 * @author JM406760
 *
 */
public interface StoredValueService extends Service{

	@Getter
	public enum ServiceMethod {
		PREAUTHORIZE("fp02"),
		BALANCE_INQUIRY("ps09"),
		AUTHORIZE_COMPLETE("fp01");
		
		String serviceMethodName;
		
		ServiceMethod(String serviceMethodName) {
			this.serviceMethodName = serviceMethodName;
		}
	}
	
	public StoredValueServiceResponse preAuthorize(StoredValueServiceRequest storedValueServiceData) throws DigitalIntegrationException;
	public StoredValueServiceResponse balanceInquiry(StoredValueServiceRequest storedValueServiceData) throws DigitalIntegrationException;
	public StoredValueServiceResponse authorizeComplete(StoredValueServiceRequest storedValueServiceData) throws DigitalIntegrationException;
}
