package com.digital.commerce.integration.storedvalue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.common.service.client.ServiceRequest;
import com.digital.commerce.integration.common.service.client.ServiceRequest.ServiceHttpMethod;
import com.digital.commerce.integration.storedvalue.domain.StoredValueServiceRequest;
import com.digital.commerce.integration.storedvalue.domain.StoredValueServiceResponse;
import com.digital.commerce.integration.util.PropertyUtil;
import com.digital.commerce.integration.external.storedvalue.ceridian.beans.Amount;
import com.digital.commerce.integration.external.storedvalue.ceridian.beans.BalanceInquiryRequest;
import com.digital.commerce.integration.external.storedvalue.ceridian.beans.Card;
import com.digital.commerce.integration.external.storedvalue.ceridian.beans.Merchant;
import com.digital.commerce.integration.external.storedvalue.ceridian.beans.PreAuthCompleteRequest;
import com.digital.commerce.integration.external.storedvalue.ceridian.beans.PreAuthRequest;
import com.digital.commerce.integration.external.storedvalue.ceridian.domain.BalanceInquiry;
import com.digital.commerce.integration.external.storedvalue.ceridian.domain.BalanceInquiryResponse;
import com.digital.commerce.integration.external.storedvalue.ceridian.domain.ObjectFactory;
import com.digital.commerce.integration.external.storedvalue.ceridian.domain.PreAuth;
import com.digital.commerce.integration.external.storedvalue.ceridian.domain.PreAuthComplete;
import com.digital.commerce.integration.external.storedvalue.ceridian.domain.PreAuthCompleteResponse;
import com.digital.commerce.integration.external.storedvalue.ceridian.domain.PreAuthResponse;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CeridianStoredValueService extends BaseIntegrationService implements StoredValueService {
	private static final DigitalLogger logger = DigitalLogger.getLogger(StoredValueService.class);

	private static final String PREAUTHORIZATION_SOAPACTION = "preAuthorization";
	private static final String PREAUTHORIZATIONCOMPLETE_SOAPACTION = "preAuthorizationComplete";
	private static final String BALANCINQUIRY_SOAPACTION = "balancInquiry";

	@Getter
	enum PreauthorizationReturnCode {
		APPROVAL("01");

		String codeValue;

		private PreauthorizationReturnCode(String codeValue) {
			this.codeValue = codeValue;
		}

	}

	/*
	private String userName;
	private String password;
	private String defaultUri = "";
	private Map<String, String> soapActions = new HashMap<String, String>();
	private Map<String, String> staticData = new HashMap<String, String>();
	*/
	
	private static final String SD_MERCHANTNAME = "merchantName";
	private static final String SD_MERCHANTNUMBER = "merchantNumber";
	private static final String SD_STORENUMBER = "storeNumber";
	private static final String SD_DIVISION = "division";
	private static final String SD_ROUTINGID = "routingId";

	private IntegrationServiceClient serviceClient;

	@Override
	public void postStartup() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void postShutdown() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getService() {
		return WebServicesConfig.ServicNames.CERIDIAN.getName();
	}
	
	@Override
	public StoredValueServiceResponse preAuthorize(StoredValueServiceRequest storedValueServiceData)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.PREAUTHORIZE.getServiceMethodName();
		boolean hasException = false;
		
		if (doRunService(serviceMethodName)) {
			
			startPerformanceMonitorOperation(serviceMethodName);
			
			try {

				PreAuth preAuth = generatePreAuthorizationRequest(storedValueServiceData);

				String bodyContent = serviceClient.getMarshalledStringFromObject(PreAuth.class, preAuth);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName), getRequestTimeout(serviceMethodName), getDefaultUri(),
						ServiceRequest.ServiceContentType.XML, ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setSoapAction(getPreAuthorizationSOAPAction());
				serviceRequest.setBody(bodyContent);


				HttpServiceResponse serviceResponse = serviceClient.processHttpRequest(serviceRequest, getUserName(), getPassword());

				validateHttpResponse(serviceResponse);

				PreAuthResponse preAuthResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(PreAuthResponse.class, serviceResponse);

				StoredValueServiceResponse storedValueServiceResponse = new StoredValueServiceResponse();

				if (preAuthResponse.getPreAuthReturn().getReturnCode().getReturnCode()
						.equals(PreauthorizationReturnCode.APPROVAL.getCodeValue())) {
					storedValueServiceResponse.setSuccess(true);
					
					GregorianCalendar c = new GregorianCalendar();
					c.setTime(new Date());
					c.add(Calendar.DAY_OF_MONTH, +8);//According to email, adding logic to set AuthorizationExpirationDate to be ApprovedDate+8days...
					XMLGregorianCalendar authorizationExpirationDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
					
					storedValueServiceResponse.setAuthorizationExpirationDate(authorizationExpirationDate);
				} else {
					storedValueServiceResponse.setSuccess(false);
				}

				storedValueServiceResponse.setTransactionId(preAuthResponse.getPreAuthReturn().getTransactionID());
				storedValueServiceResponse
						.setAmount(preAuthResponse.getPreAuthReturn().getApprovedAmount().getAmount());
				storedValueServiceResponse
						.setAuthorizationCode(preAuthResponse.getPreAuthReturn().getAuthorizationCode());
				storedValueServiceResponse
						.setReturnCode(preAuthResponse.getPreAuthReturn().getReturnCode().getReturnCode());
				storedValueServiceResponse
						.setReturnDescription(preAuthResponse.getPreAuthReturn().getReturnCode().getReturnDescription());
				
				return storedValueServiceResponse;

			} catch (Exception e) {
				hasException = true;
				
				throw new DigitalIntegrationBusinessException("Pre-authorization Error",ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}
	}

	protected PreAuth generatePreAuthorizationRequest(StoredValueServiceRequest storedValueServiceData) {
		ObjectFactory ob = new ObjectFactory();
		PreAuth preAuth = ob.createPreAuth();

		PreAuthRequest preAuthRequest = new PreAuthRequest();

		Card card = new Card();
		card.setCardCurrency("");
		card.setCardNumber(storedValueServiceData.getCardNumber());
		card.setPinNumber(storedValueServiceData.getPin());
		card.setCardExpiration("");
		card.setCardTrackOne("");
		card.setCardTrackTwo("");
		card.setEovDate("");

		Merchant merchant = generateMerchant(storedValueServiceData);

		preAuthRequest.setCard(card);
		preAuthRequest.setMerchant(merchant);
		preAuthRequest.setInvoiceNumber("");
		preAuthRequest.setRoutingID(getRoutingId());
		preAuthRequest.setStan("");
		preAuthRequest.setTransactionID(generateTransactionId(merchant.getMerchantNumber()));
		preAuthRequest.setCheckForDuplicate(true);
		preAuthRequest.setDate(generateDateString());

		Amount authAmount = new Amount();
		authAmount.setAmount(storedValueServiceData.getAmount());
		authAmount.setCurrency(storedValueServiceData.getCurrencyName());

		preAuthRequest.setRequestedAmount(authAmount);

		preAuth.setRequest(preAuthRequest);

		return preAuth;
	}

	protected BalanceInquiry generateBalanceInquiryRequest(StoredValueServiceRequest storedValueServiceData) {
		ObjectFactory ob = new ObjectFactory();
		BalanceInquiry balanceInquiry = ob.createBalanceInquiry();

		BalanceInquiryRequest balanceInquiryRequest = new BalanceInquiryRequest();

		Card card = new Card();
		card.setCardCurrency("");
		card.setCardNumber(storedValueServiceData.getCardNumber());
		card.setPinNumber(storedValueServiceData.getPin());
		card.setCardExpiration("");
		card.setCardTrackOne("");
		card.setCardTrackTwo("");
		card.setEovDate("");

		Merchant merchant = generateMerchant(storedValueServiceData);

		balanceInquiryRequest.setCard(card);
		balanceInquiryRequest.setMerchant(merchant);
		balanceInquiryRequest.setInvoiceNumber("");
		balanceInquiryRequest.setRoutingID(getRoutingId());
		balanceInquiryRequest.setStan("");
		balanceInquiryRequest.setTransactionID(generateTransactionId(merchant.getMerchantNumber()));
		balanceInquiryRequest.setCheckForDuplicate(true);
		balanceInquiryRequest.setDate(generateDateString());

		Amount authAmount = new Amount();
		authAmount.setAmount(0d);
		authAmount.setCurrency(StoredValueServiceRequest.USD);

		balanceInquiryRequest.setAmount(authAmount);

		balanceInquiry.setRequest(balanceInquiryRequest);

		return balanceInquiry;
	}

	protected PreAuthComplete generatePreAuthorizationCompleteRequest(
			StoredValueServiceRequest storedValueServiceData) {
		ObjectFactory ob = new ObjectFactory();

		PreAuthComplete preAuthComplete = ob.createPreAuthComplete();
		PreAuthCompleteRequest preAuthCompleteRequest = new PreAuthCompleteRequest();

		Card card = new Card();
		card.setCardCurrency("");
		card.setCardNumber(storedValueServiceData.getCardNumber());
		card.setPinNumber(storedValueServiceData.getPin());
		card.setCardExpiration("");
		card.setCardTrackOne("");
		card.setCardTrackTwo("");
		card.setEovDate("");

		Merchant merchant = generateMerchant(storedValueServiceData);

		preAuthCompleteRequest.setCard(card);
		preAuthCompleteRequest.setMerchant(merchant);
		preAuthCompleteRequest.setInvoiceNumber("");
		preAuthCompleteRequest.setRoutingID(getRoutingId());
		preAuthCompleteRequest.setStan("");
		preAuthCompleteRequest.setTransactionID(generateTransactionId(merchant.getMerchantNumber()));
		preAuthCompleteRequest.setCheckForDuplicate(true);
		preAuthCompleteRequest.setDate(generateDateString());

		Amount transactionAmount = new Amount();
		transactionAmount.setAmount(storedValueServiceData.getAmount());
		transactionAmount.setCurrency(storedValueServiceData.getCurrencyName());

		preAuthCompleteRequest.setTransactionAmount(transactionAmount);

		preAuthComplete.setRequest(preAuthCompleteRequest);

		return preAuthComplete;
	}

	protected String generateDateString() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return format.format(new Date());
	}

	protected Merchant generateMerchant(StoredValueServiceRequest storedValueServiceData) {
		Merchant merchant = new Merchant();
		merchant.setMerchantName(getMerchantName());
		merchant.setMerchantNumber(getMerchantNumber());
		merchant.setStoreNumber(getStoreNumber());
		merchant.setDivision(getDivision());

		return merchant;
	}

	@Override
	public StoredValueServiceResponse balanceInquiry(StoredValueServiceRequest storedValueServiceData)
			throws DigitalIntegrationException {
		
		String serviceMethodName = ServiceMethod.BALANCE_INQUIRY.getServiceMethodName();
		boolean hasException = false;
		
		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);
			
			try {

				BalanceInquiry balanceInquiry = generateBalanceInquiryRequest(storedValueServiceData);

				String bodyContent = serviceClient.getMarshalledStringFromObject(BalanceInquiry.class, balanceInquiry);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName), getRequestTimeout(serviceMethodName), getDefaultUri(),
						ServiceRequest.ServiceContentType.XML, ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setSoapAction(getBalancInquirySOAPAction());
				serviceRequest.setBody(bodyContent);


				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest, getUserName(), getPassword());

				validateHttpResponse(response);
				
				BalanceInquiryResponse balanceInquiryResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(BalanceInquiryResponse.class, response);

				StoredValueServiceResponse storedValueServiceResponse = new StoredValueServiceResponse();

				if (balanceInquiryResponse.getBalanceInquiryReturn().getReturnCode().getReturnCode()
						.equals(PreauthorizationReturnCode.APPROVAL.getCodeValue())) {
					storedValueServiceResponse.setSuccess(true);
				} else {
					storedValueServiceResponse.setSuccess(false);
				}



				storedValueServiceResponse
						.setTransactionId(balanceInquiryResponse.getBalanceInquiryReturn().getTransactionID());
				storedValueServiceResponse
						.setAmount(balanceInquiryResponse.getBalanceInquiryReturn().getBalanceAmount().getAmount());
				storedValueServiceResponse
						.setAuthorizationCode(balanceInquiryResponse.getBalanceInquiryReturn().getAuthorizationCode());
				storedValueServiceResponse
						.setReturnCode(balanceInquiryResponse.getBalanceInquiryReturn().getReturnCode().getReturnCode());
				storedValueServiceResponse
						.setReturnDescription(balanceInquiryResponse.getBalanceInquiryReturn().getReturnCode().getReturnDescription());
				
				return storedValueServiceResponse;

			} catch (Exception e) {
				hasException = true;
				
				throw new DigitalIntegrationBusinessException("Balance Inquiry Error",ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}
	}

	@Override
	public StoredValueServiceResponse authorizeComplete(StoredValueServiceRequest storedValueServiceData)
			throws DigitalIntegrationException {
		
		String serviceMethodName = ServiceMethod.AUTHORIZE_COMPLETE.getServiceMethodName();
		boolean hasException = false;
		
		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);
			
			try {

				PreAuthComplete preAuthComplete = generatePreAuthorizationCompleteRequest(storedValueServiceData);

				String bodyContent = serviceClient.getMarshalledStringFromObject(PreAuthComplete.class, preAuthComplete);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName), getRequestTimeout(serviceMethodName), getDefaultUri(),
						ServiceRequest.ServiceContentType.XML, ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setSoapAction(getPreAuthorizationCompleteSOAPAction());
				serviceRequest.setBody(bodyContent);


				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest, getUserName(), getPassword());

				validateHttpResponse(response);

				PreAuthCompleteResponse preAuthResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(PreAuthCompleteResponse.class, response);

				StoredValueServiceResponse storedValueServiceResponse = new StoredValueServiceResponse();

				if (preAuthResponse.getPreAuthCompleteReturn().getReturnCode().getReturnCode()
						.equals(PreauthorizationReturnCode.APPROVAL.getCodeValue())) {
					storedValueServiceResponse.setSuccess(true);
				} else {
					storedValueServiceResponse.setSuccess(false);
				}



				storedValueServiceResponse
						.setTransactionId(preAuthResponse.getPreAuthCompleteReturn().getTransactionID());
				storedValueServiceResponse
						.setAmount(preAuthResponse.getPreAuthCompleteReturn().getApprovedAmount().getAmount());
				storedValueServiceResponse
						.setAuthorizationCode(preAuthResponse.getPreAuthCompleteReturn().getAuthorizationCode());
				storedValueServiceResponse
						.setReturnCode(preAuthResponse.getPreAuthCompleteReturn().getReturnCode().getReturnCode());
				storedValueServiceResponse
						.setReturnDescription(preAuthResponse.getPreAuthCompleteReturn().getReturnCode().getReturnDescription());
				
				return storedValueServiceResponse;

			} catch (Exception e) {
				hasException = true;
				
				throw new DigitalIntegrationBusinessException("Authorization Error",ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}
	}

	private String generateTransactionId(String merchantNumber) {
		return reverseMerchantNumberInPairs(merchantNumber) + uniqueNumber();
	}

	private String reverseMerchantNumberInPairs(String merchantNumber) {
		
		String pairReversal = "";
		int length = merchantNumber.length();

		for (int i = 0; i <= length - 2; i += 2) {
			pairReversal += merchantNumber.charAt(i + 1) + "" + merchantNumber.charAt(i);
		}

		return pairReversal;
	}

	private String uniqueNumber() {
		long uniqueID = (long) (Math.random() * 100000 + 3333300000L);
		return String.valueOf(uniqueID);
	}

	public String getUserName() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, "userName");
	}

	public String getPassword() {
		return getWebServicesConfig().getVaultedData(getMapData(WebServicesConfig.DataTypes.STATIC, "password"));
	}

	public String getDefaultUri() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, "defaultUri");
	}
	
	public Map<String, String> getSoapActions() {
		return getMapData(WebServicesConfig.DataTypes.SOAP_ACTIONS);
	}
	
	public Map<String, String> getStaticData() {
		return getMapData(WebServicesConfig.DataTypes.STATIC);
	}

	public String getPreAuthorizationSOAPAction() {
		return PropertyUtil.getMappedProperty(getSoapActions(), PREAUTHORIZATION_SOAPACTION).toString();
	}

	public String getMerchantName() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_MERCHANTNAME).toString();
	}


	public String getMerchantNumber() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_MERCHANTNUMBER).toString();
	}

	public String getStoreNumber() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_STORENUMBER).toString();
	}

	public String getDivision() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_DIVISION).toString();
	}

	public String getRoutingId() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_ROUTINGID).toString();
	}

	public String getBalancInquirySOAPAction() {
		return PropertyUtil.getMappedProperty(getSoapActions(), BALANCINQUIRY_SOAPACTION).toString();
	}


	public String getPreAuthorizationCompleteSOAPAction() {
		return PropertyUtil.getMappedProperty(getStaticData(), PREAUTHORIZATIONCOMPLETE_SOAPACTION).toString();
	}

    public Map<String, Boolean> getServiceMethodsEnabled(){
		Map<String, Boolean> methods = new HashMap<>();
		for (ServiceMethod service: ServiceMethod.values()) {
			methods.put(service.getServiceMethodName(), isServiceMethodEnabled(service.getServiceMethodName()));
		}
		
		return methods;
    }
}
