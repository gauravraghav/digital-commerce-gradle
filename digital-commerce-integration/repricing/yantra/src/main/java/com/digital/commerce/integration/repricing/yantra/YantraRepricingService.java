/**
 * 
 */
package com.digital.commerce.integration.repricing.yantra;

import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.repricing.RepricingService;
import com.digital.commerce.integration.repricing.RepricingService.ServiceMethod;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@SuppressWarnings({ "unchecked" })
@Getter
@Setter
public class YantraRepricingService extends BaseIntegrationService implements RepricingService {

	

	@Override
	protected String getService() {
		return WebServicesConfig.ServicNames.YANTRA_REPRICING.getName();
	}

	private IntegrationServiceClient serviceClient;

	@Override
	public void postStartup() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void postShutdown() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	public String getRepricing(String repricingRequest) throws DigitalIntegrationException {
		
		String serviceMethodName = ServiceMethod.REPRICING.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			try {
				
				return repricingRequest;

			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("Repricing  Error", ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());

		}
	}

}
