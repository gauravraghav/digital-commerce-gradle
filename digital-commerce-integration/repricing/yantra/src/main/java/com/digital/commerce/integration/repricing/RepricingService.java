package com.digital.commerce.integration.repricing;

import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.service.Service;
import lombok.Getter;


/**
 * Interface to be used by the application to perform address validation
 * @author JM406760
 *
 */
public interface RepricingService extends Service{
	@Getter
	public enum ServiceMethod {
		REPRICING("repricing");
		
		String serviceMethodName;
		
		ServiceMethod(String serviceMethodName) {
			this.serviceMethodName = serviceMethodName;
		}
	}
	
	public String getRepricing(String repricingRequest)  throws DigitalIntegrationException;
}
