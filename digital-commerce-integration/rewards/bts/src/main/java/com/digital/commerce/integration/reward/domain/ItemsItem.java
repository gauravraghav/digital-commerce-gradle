package com.digital.commerce.integration.reward.domain;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ItemsItem implements Serializable{

	private static final long serialVersionUID = 1L;
	private Double shippingCost;
	private String fulfillmentStatusCode;
	private String longSku;
	private List<DiscountMarkDownCodes> discountMarkDownCodes;
	private String commerceLineNumber;
	private Boolean clearanceFlag;
	private Double saleTaxAmount;
	private Integer fulfillmentLocationNumber;
	private Double lineTotalWithoutTax;
	private Integer saleItemQuantity;
	private String shippingMethodCode;
	private String shippingTrackingNumber;
	private String fulfillmentTypeCode;
	private Integer inventorySourceLocationNumber;
	private Integer lineNumber;
	private Integer inventorySourceLocationBusinessUnit;
	private ShipToItem shipTo;
}