package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author mmallipu
 */
@Getter
@Setter
@ToString
public class RewardServiceRequest implements Serializable {


  private static final long serialVersionUID = 1L;

  /*
   * These attributes are the primary customerbean inputs. Be aware on a customer
   * update that these need populated or will be removed.
   */
  private Contact contact = new Contact();
  private Address address = new Address();
  private Person person = new Person();
  private Flags flags = new Flags();
  private String fraudCode;
  private String fraudCodeGroup;
  private String frequencyFashionEmail;
  private String preferredStore;
  private String signUpDate;
  private String signUpStore;
  private String pointsBanking;
  private String certDenomination;
  private String isPaperlessCert;
  /*
   * Additional non-customer bean attributes
   */
  private String barcode;

  private Date StartDate;
  private Date endDate;

  private String orderNumber;
  private String rewardsNumber;
  private Integer rewardPoints;

  private List<String> certificates;

  private List<String> offers;

  private BirthdayGift birthdayGift;
  private Shopfor shopfor;

  private String emailSource;

  private Date historyStartDate;
  private Date historyEndDate;
  private String historyProfileId;

  private String deviceName;

  private Charity charity;


  /**
   * @param isPaperlessCert the isPaperlessCert to set
   */
  public void setIsPaperlessCert(boolean isPaperlessCert) {
    if (isPaperlessCert) {
      this.isPaperlessCert = "Y";
    } else {
      this.isPaperlessCert = "N";
    }
  }
}
