package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DiscountMarkDownCodes{
	/**
	 * 
	 */
	private String discountMarkDownCode;
	private Double discountAmount;
	private String partyOfferId;

}
