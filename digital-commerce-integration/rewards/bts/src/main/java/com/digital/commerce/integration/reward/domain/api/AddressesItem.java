package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Setter
@Getter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressesItem{

	@JsonProperty("country")
	private String country;

	@JsonProperty("city")
	private String city;

	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("addressLine1")
	private String addressLine1;

	@JsonProperty("addressLine2")
	private String addressLine2;

	@JsonProperty("state")
	private String state;

	@JsonProperty("captureDate")
	private String captureDate;

	@JsonProperty("captureLocationNumber")
	private Integer captureLocationNumber;

	@JsonProperty("captureBusinessUnitNumber")
	private Integer captureBusinessUnitNumber;

	@JsonProperty("type")
	private String type;
}