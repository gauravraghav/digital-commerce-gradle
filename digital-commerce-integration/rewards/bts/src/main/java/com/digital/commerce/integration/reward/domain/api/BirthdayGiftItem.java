package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BirthdayGiftItem{

	@JsonProperty("giftDeliveryDate")
	private String giftDeliveryDate;

	@JsonProperty("recipientBirthday")
	private Integer recipientBirthday;

	@JsonProperty("giftCouponCode")
	private String giftCouponCode;

	@JsonProperty("giftRequestId")
	private Long giftRequestId;

	@JsonProperty("recipientGivenName")
	private String recipientGivenName;

	@JsonProperty("giftMessageText")
	private String giftMessageText;

	@JsonProperty("processedIndicator")
	private Boolean processedIndicator;

	@JsonProperty("giftRequestStatusCode")
	private String giftRequestStatusCode;

	@JsonProperty("recipientBirthMonth")
	private Integer recipientBirthMonth;

	@JsonProperty("recipientEmail")
	private String recipientEmail;

	@JsonProperty("giftRequestDate")
	private String giftRequestDate;

	public Boolean isProcessedIndicator() {
		return this.getProcessedIndicator();
	}

}