package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
public class Shopfor implements  Serializable{

    private static final long serialVersionUID = 1L;
    private String  shopforID;
    private String profileId;
    private boolean isActive;
    private String  rewardsShopforID;
    private String  shopforName;
    private String  relationship;
    private String  birthDay;
    private String  birthMonth;
    private String  webType;
    private String  gender;
    private String sizeGroup;
    private List<String> sizes;
    private List<String> widths;
    private Date  dateCreated;

}
