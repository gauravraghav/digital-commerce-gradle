/**
 *
 */
package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

/**
 * @author mmallipu
 */
@Getter
@Setter
@ToString
public class Points {

  private Integer calculatedBasePoints;
  private Integer calculatedBonusPoints;
  private String customerID;
  private String employeeID;
  private String loyaltyAccountTransactionDate;
  private String loyaltyAccountTransactionGroupCode;
  private String loyaltyAccountTransactionId;
  private String memberID;
  private Integer runningTotal;
  private long totalAdjustPntAmt;
  private Integer totalBonusPntAmt;
  private Integer totalCertIssueAmt;
  private Integer totalCertIssueCnt;
  private Integer totalCertIssuePntAmt;
  private Integer totalClearBonusPointAmt;
  private String totalClearDollarAmt;
  private Integer totalClearPointAmt;
  private Integer totalEarnPntAmt;
  private String totalItems;
  private long totalPoints;
  private Integer totalPremierBonusPointAmt;
  private Double totalQualifyingAmount;
  private Integer totalQualifyingPointsLY;
  private long totalRegBonusPntAmt;
  private Integer totalRegDollarAmt;
  private Integer totalRegPntAmt;
  private Date transDate;
  private String transType;
  private String transTypeCodeDesc;

  private List<String> markdowns;
  private String orderDate;
  private String orderNumber;
  private String pOSRegisterID;
  private String salesTransID;
  private String transID;
  private String transNumber;
  private String transLocationNumber;
  private String transBusinessUnitNumber;

  private long loyaltyPoints;
  private Date loyaltyTierStartDate;
  private Date loyaltyTierExpirationDate;
  private Date loyaltySignupDate;
  private String LoyaltySignupStoreNumber;
  private String tier;
  private long pointsUntilNextReward;
  private long pointsToNextStatus;
  private Date dateLoyaltyPoints;

  private long dollarsNextReward;
  private long dollarsFutureReward;
  private long dollarsNextTier;
  private long dollarsPendingCertIssue;

  private String storeID;
  private String storeMallPlazaName;
  private Double totalSalesAmount;

}
