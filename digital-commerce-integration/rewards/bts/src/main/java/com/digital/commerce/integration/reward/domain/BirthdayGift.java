package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
public class BirthdayGift implements  Serializable{

	private static final long serialVersionUID = 1L;
	private String birthdayGiftId;
	private String dayOfBirth;
	private String email;
	private String friendName;
	private String individualId;
	private String message;
	private String monthOfBirth;
	private String locked;
	private String typeCode;
	private Date addedDate;
	private String profileId;
	private String deliveryDate;

}
