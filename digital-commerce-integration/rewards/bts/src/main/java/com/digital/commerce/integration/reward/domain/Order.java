package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class Order {

	protected List<OrderLine> orderLine = new ArrayList<>();
	protected String currencyCode;
	protected String individualID;
	protected String localeCode;
	protected String onlineSiteName;
	protected String orderChannelCode;
	protected String orderDate;
	protected String orderLocationID;
	protected String orderLocationNumber;
	protected String orderNumber;
	protected double totalOrderAmount;
}