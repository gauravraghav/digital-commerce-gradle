package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Attributes{

	@JsonProperty("fraudCodeGroup")
	private String fraudCodeGroup;

	@JsonProperty("mobileAppLastLoginDate")
	private String mobileAppLastLoginDate;

	@JsonProperty("invalidLoyaltyMemberFlag")
	private Boolean invalidLoyaltyMemberFlag;

	@JsonProperty("mobileAppFirstLoginDate")
	private String mobileAppFirstLoginDate;

	@JsonProperty("fraudCode")
	private String fraudCode;

	public Boolean isInvalidLoyaltyMemberFlag() {
		return this.getInvalidLoyaltyMemberFlag();
	}

}