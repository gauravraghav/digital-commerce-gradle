package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer{

	@JsonProperty("birthday")
	private Birthday birthday;

	@JsonProperty("addresses")
	private List<AddressesItem> addresses;

	@JsonProperty("preferences")
	private Preferences preferences;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("loyalty")
	private Loyalty loyalty;

	@JsonProperty("phones")
	private List<PhonesItem> phones;

	@JsonProperty("loyaltyMemberFlag")
	private Boolean loyaltyMemberFlag;

	@JsonProperty("creditCardMember")
	private Boolean creditCardMember;

	@JsonProperty("primaryLoyaltyAccountNumber")
	private String primaryLoyaltyAccountNumber;

	@JsonProperty("spendUntilNextTier")
	private Integer spendUntilNextTier;

	@JsonProperty("emails")
	private List<EmailsItem> emails;

	@JsonProperty("dollarsFutureReward")
	private Integer dollarsFutureReward;

	@JsonProperty("dollarsNextReward")
	private Integer dollarsNextReward;

	@JsonProperty("memberCombinedFlag")
	private Boolean memberCombinedFlag;

	@JsonProperty("currentYearSpend")
	private Integer currentYearSpend;

	@JsonProperty("tier")
	private Tier tier;

	@JsonProperty("atgAccounts")
	private List<AtgAccountsItem> atgAccounts;

	@JsonProperty("name")
	private Name name;

	@JsonProperty("attributes")
	private Attributes attributes;

	@JsonProperty("partyId")
	private Long partyId;

	public Boolean isLoyaltyMemberFlag(){
		return this.getLoyaltyMemberFlag();
	}

	public Boolean isCreditCardMember() {
		return this.getCreditCardMember();
	}

}