package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Preferences{

	@JsonProperty("preferredStore")
	private String preferredStore;

	@JsonProperty("shopFors")
	private List<ShopForsItem> shopFors;

	@JsonProperty("certificateDenominationChoice")
	private Integer certificateDenominationChoice;

	@JsonProperty("emailOptIn")
	private Boolean emailOptIn;

	@JsonProperty("paperlessRewardsOptIn")
	private Boolean paperlessRewardsOptIn;

	@JsonProperty("textOptIn")
	private Boolean textOptIn;

	public Boolean isEmailOptIn() {
		return this.getEmailOptIn();
	}

	public Boolean isPaperlessRewardsOptIn() {
		return this.getPaperlessRewardsOptIn();
	}

	public Boolean isTextOptIn() {
		return this.getTextOptIn();
	}
}