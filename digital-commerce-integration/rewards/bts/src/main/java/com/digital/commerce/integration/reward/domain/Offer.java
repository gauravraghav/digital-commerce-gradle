/**
 * 
 */
package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
@ToString
public class Offer {

	private String campaignID;
	private String endDate;
	private String individualID;
	private String offerID;
	private String promoID;
	private String segmentID;
	private String startDate;
	
}
