package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BillToItem {
	
	/**
	 * 
	 */
	private String firstName;
	private String lastName;
	private String billToAddress1;
	private String billToPostCode;
	private String billToCountry;
	private String billToState;
	private String billToAddress2;
	private String billTocity;
}
