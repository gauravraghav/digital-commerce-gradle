package com.digital.commerce.integration.reward.domain.api;

import java.io.Serializable;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Tender implements Serializable{
	/**
	 * 
	 */
	@Generated("com.robohorse.robopojogenerator")
	@JsonInclude(Include.NON_DEFAULT)
	@JsonIgnoreProperties(ignoreUnknown = true)
	private static final long serialVersionUID = 1L;
	private String type;

}
