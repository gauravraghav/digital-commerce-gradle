package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@ToString
public class Contact implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public static final String MOBILE_PHONE_TYPE = "mobilePhone";
  public static final String HOME_PHONE_TYPE = "homePhone";
  public static final String OTHER_PHONE_TYPE = "otherPhone";
  public static final String LOGIN_EMAIL_TYPE = "loginEmail";
  public static final String REWARDS_EMAIL_TYPE = "rewardsEmail";

  private String email;
  private String mobilePhone;
  private String phone1;
  private String phone1Extension;
  private String phone2;
  private String phone2Extension;
  private String phoneCountryCode;
  private Map<String, Flags> flagsMap = new HashMap<>();
}
