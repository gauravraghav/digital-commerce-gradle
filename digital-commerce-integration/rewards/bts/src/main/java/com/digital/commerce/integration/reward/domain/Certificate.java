package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author mmallipu
 */
@Getter
@Setter
@ToString
public class Certificate {

  private String businessUnitID;
  private String canReissue;
  private String certEarnedValue;
  private double certificatesValue;
  private String certNbr;
  private String certRedeemed;
  private String certRedeemedValue;
  private String certStatus;
  private String certType;
  private String certValue;
  private String comments;
  private String discountCode;
  private String expCode;
  private String expDate;
  private String issueDate;
  private String markdownCode;
  private String memberID;
  private String orderID;
  private String redDate;
  private Integer redeemedCertificates;
  private double redeemedCertificatesValue;
  private String registerID;
  private String saleDate;
  private String storeName;
  private String storeNbr;
  private Integer totalCertificates;
  private String transactionID;
  private String voidFlag;
  private String offerDisplayName;
  private String offerDetailsPageName;

}
