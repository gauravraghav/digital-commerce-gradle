package com.digital.commerce.integration.reward.subscription;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import com.digital.commerce.integration.reward.RewardService;
import com.digital.commerce.integration.reward.bts.BtsRewardService;
import com.digital.commerce.integration.reward.domain.RewardServiceRequest;
import com.digital.commerce.common.logger.DigitalLogger;

import atg.dms.patchbay.MessageSink;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RewardsSubscriptionMessageSink implements MessageSink {
	private static transient DigitalLogger logger = DigitalLogger.getLogger(RewardsSubscriptionMessageSink.class);

	private BtsRewardService rewardsService;

	@Override
	public void receiveMessage(String s, Message message) throws JMSException {
		try {
            RewardServiceRequest rewardsRequest = (RewardServiceRequest) ((ObjectMessage) message).getObject();
			if (logger.isDebugEnabled()) {
				logger.debug("RewardsOptIn message -------> " + rewardsRequest);
			}

			// call rewards and proceed, do not worry even if it fails
			if (rewardsService.isServiceEnabled() && rewardsService.isServiceMethodEnabled(
					RewardService.ServiceMethod.UPDATE_EMAIL_SUBSCRIBER.getServiceMethodName())) {
				rewardsService.updateEmailSubscriber(rewardsRequest);
			} else {
				if (logger.isInfoEnabled()) {
					logger.info("Rewards/UpdateEmailSubscriber offline, not sending OptInOptOut info to rewards.");
				}
			}
		} catch (Exception e) {
			logger.info("Exception while posting rewards for opt-in/opt-out udpates post order placement: "
					+ e.getMessage());
		}
	}
}
