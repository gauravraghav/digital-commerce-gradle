package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class PointsTansaction {

  private int totalCurrentPoints;
  private List<Points> points;

}
