package com.digital.commerce.integration.reward.domain;

import java.io.Serializable;

import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Person  implements  Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	public enum Gender {
		MALE("M"), FEMALE("F"), UNKNOWN("U");
		
		String description;

		private Gender(String description) {
			this.description = description;
		}

		public static Gender getGenderFromDescription(String description) {
			for (Gender gender: values()) {
				if (DigitalStringUtil.equalsIgnoreCase(gender.getDescription(), description)) {
					return gender;
				}
			}
			
			return null;
		}
	}
	
	private String dayOfBirth;
	private String firstName;
	private Gender gender;
	private String lastName;
	private String monthOfBirth;
	private String customerID;
	private String profileID;
	private String memberID;
	private String householdID;
	private String individualID;

}
