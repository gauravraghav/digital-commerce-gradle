package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Address implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private String address1;
  private String address2;
  private String address3;
  private String city;
  private String country;
  private String state;
  private String postCode;
  private String addressType;
  private Flags flags = new Flags();

}
