package com.digital.commerce.integration.reward;

import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.service.Service;
import com.digital.commerce.integration.reward.domain.RewardServiceRequest;
import com.digital.commerce.integration.reward.domain.RewardServiceResponse;
import com.digital.commerce.integration.reward.domain.api.RewardsSendOrderRequest;

import lombok.Getter;


/**
 * @author mmallipu
 */
public interface RewardService extends Service {

  @Getter
  public enum ServiceMethod {
    ADD_CUSTOMER("addCustomer"),
    UPDATE_CUSTOMER("updateCustomer"),
    UPDATE_CUSTOMER_BY_MEMBERID("updateCustomerByMemberID"),
    SELECT_CUSTOMER("selectCustomer"),
    RETRIEVE_OFFERS("retrieveOffers"),
    RETRIEVE_MEMBERID("retrieveMemberID"),
    ADD_SHOP("addShopWithoutACardRequest"),
    VIEW_MEMBER_DATA("viewMemberData"),
    VIEW_CERTIFICATE_HISTORY("viewCertificatePointHistory"),
    CANCEL_LOYALTY_CERTIFICATE_RESERVATION("cancelLoyaltyCertificateReservation"),
    REQUEST_CERTIFICATE("requestCertificate"),
    RESERVE_LOYALTY_CERTIFICATE("reserveLoyaltyCertificate"),
    SELECT_CERTIFICATES_BY_MEMBERID("selectAvailableCertificatesByMemberID"),
    SELECT_CERTIFICATES_BY_PROFILEID("selectAvailableCertificatesByProfileID"),
    UPDATE_EMAIL_SUBSCRIBER("updateEmailSubscriber"),
    CAPTURE_EMAIL_FOOTER("captureEmailFooter"),
    ORDER_HISTORY("retrieveOrdersByProfildId"),
    ADD_BIRTHDAY_GIFT("addBirthdayGift"),
    UPDATE_BIRTHDAY_GIFT("updateBirthdayGift"),
    ADD_SHOPFOR("addShopFor"),
    UPDATE_SHOPFOR("updateShopFor"),
    DELETE_SHOPFOR("deleteShopFor"),
    RETRIEVE_SHOPFORS("retrieveShopFors"),
    RETRIEVE_BIRTHDAY_GIFTS("retrieveBirthdayGifts"),
    RETRIEVE_CHARITIES("retrieveCharities"),
    DONATE_CERTIFICATE("donateCertificate"),
    RETRIEVE_INCENTIVES("retrieveIncentives"),
    SEND_ORDER("sendOrder"),
    RETRIEVE_ENTICEMENTS("enticements");

    String serviceMethodName;

    ServiceMethod(String serviceMethodName) {
      this.serviceMethodName = serviceMethodName;
    }
    
  }

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse addCustomer(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse updateCustomer(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse updateCustomerByMemberId(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse selectCustomerByProfileId(
      RewardServiceRequest rewardServiceRequest) throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse retrieveOffersByProfileId(
      RewardServiceRequest rewardServiceRequest) throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse retrieveMemberID(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse addShopWithoutACardRequest(
      RewardServiceRequest rewardServiceRequest) throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse viewMemberData(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse viewCertificateHistory(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse cancelLoyaltyCertificateReservation(
      RewardServiceRequest rewardServiceRequest) throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse requestCertificate(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse reserveLoyaltyCertificate(
      RewardServiceRequest rewardServiceRequest) throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse selectAvailableCertificatesByMemberID(
      RewardServiceRequest rewardServiceRequest) throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse selectAvailableCertificatesByProfileID(
      RewardServiceRequest rewardServiceRequest) throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse updateEmailSubscriber(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse captureEmailFooter(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse orderHistory(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */

  public RewardServiceResponse addBirthdayGift(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse updateBirthdayGift(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse retrieveBirthdayGifts(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */

  public RewardServiceResponse addShopFor(RewardServiceRequest rewardServiceRequest)
          throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse updateShopFor(RewardServiceRequest rewardServiceRequest)
          throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse retrieveShopFors(RewardServiceRequest rewardServiceRequest)
          throws DigitalIntegrationException;


  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse deleteShopFor(RewardServiceRequest rewardServiceRequest)
          throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse retrieveCharities(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse donateCertificates(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse retrieveIncentives(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException;
  
  /**
   * 
   * @param rewardSendOrderRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse sendOrder(RewardsSendOrderRequest rewardSendOrderRequest)
	      throws DigitalIntegrationException;
  
  public RewardServiceResponse retrieveEnticements(RewardsSendOrderRequest rewardSendOrderRequest)
	      throws DigitalIntegrationException;
}
