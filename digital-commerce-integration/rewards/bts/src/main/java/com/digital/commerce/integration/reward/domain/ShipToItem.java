package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ShipToItem{
	/**
	 * 
	 */
	private String shipTocity;
	private String firstName;
	private String lastName;
	private String shipToCountry;
	private String shipToAddress1;
	private String shipToAddress2;
	private String shipToPostCode;
	private String shipToState;
}
