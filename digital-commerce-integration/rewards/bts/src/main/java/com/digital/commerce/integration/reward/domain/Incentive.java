package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Incentive {

  int denomination;
  int pointCost;
  boolean toBeRedeemed;
}
