package com.digital.commerce.integration.reward.domain;

import java.util.List;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RewardsSendOrderData {
	/**
	 * 
	 */
	@Generated("com.robohorse.robopojogenerator")
	@JsonInclude(Include.NON_DEFAULT)
	@JsonIgnoreProperties(ignoreUnknown = true)
	private List<TenderItem> tender;
	private String atgID;
	private Double totalSalesTax;
	private String transactionNumber;
	private Double baseShippingAmount;
	private List<DiscountMarkDownCodes> discountMarkDownCodes;
	private Long loyaltyAccountNumber;
	private String transactionDate;
	private Double totalSalesAmount;
	private Integer totalQuantity;
	private Integer businessUnitNumber;
	private Boolean isModified;
	private BillToItem billTo;
	private Integer partyID;
	private Integer locationNumber;
	private List<ItemsItem> items;
	private String channelCode;

}