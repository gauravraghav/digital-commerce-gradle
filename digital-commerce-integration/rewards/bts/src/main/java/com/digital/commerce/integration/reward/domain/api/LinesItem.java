package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinesItem{

	@JsonProperty("demandTransactionNumber")
	private String demandTransactionNumber;

	@JsonProperty("locationName")
	private String locationName;

	@JsonProperty("demandBusinessUnitNumber")
	private Integer demandBusinessUnitNumber;

	@JsonProperty("totalPoints")
	private Integer totalPoints;

	@JsonProperty("demandDate")
	private String demandDate;

	@JsonProperty("pointRuleDescription")
	private String pointRuleDescription;

	@JsonProperty("points")
	private Integer points;

	@JsonProperty("demandLocationNumber")
	private String demandLocationNumber;


}