package com.digital.commerce.integration.reward.domain.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product{

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("size")
	private List<String> size;

	@JsonProperty("genderSubType")
	private String genderSubType;

	@JsonProperty("width")
	private List<String> width;

	@JsonProperty("type")
	private String type;

}