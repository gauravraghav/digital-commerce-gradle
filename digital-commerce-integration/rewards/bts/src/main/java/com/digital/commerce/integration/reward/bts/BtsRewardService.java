package com.digital.commerce.integration.reward.bts;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.digital.commerce.integration.reward.RewardService;
import com.digital.commerce.integration.reward.domain.Address;
import com.digital.commerce.integration.reward.domain.BirthdayGift;
import com.digital.commerce.integration.reward.domain.Certificate;
import com.digital.commerce.integration.reward.domain.Charity;
import com.digital.commerce.integration.reward.domain.Contact;
import com.digital.commerce.integration.reward.domain.Flags;
import com.digital.commerce.integration.reward.domain.Incentive;
import com.digital.commerce.integration.reward.domain.Offer;
import com.digital.commerce.integration.reward.domain.Order;
import com.digital.commerce.integration.reward.domain.OrderLine;
import com.digital.commerce.integration.reward.domain.Person;
import com.digital.commerce.integration.reward.domain.Points;
import com.digital.commerce.integration.reward.domain.PointsTansaction;
import com.digital.commerce.integration.reward.domain.RewardServiceRequest;
import com.digital.commerce.integration.reward.domain.RewardServiceResponse;
import com.digital.commerce.integration.reward.domain.RewardsEnticementsResponse;
import com.digital.commerce.integration.reward.domain.Shopfor;
import com.digital.commerce.integration.reward.domain.api.AddressesItem;
import com.digital.commerce.integration.reward.domain.api.AtgAccountsItem;
import com.digital.commerce.integration.reward.domain.api.Attributes;
import com.digital.commerce.integration.reward.domain.api.AuthorizedOffersItem;
import com.digital.commerce.integration.reward.domain.api.Birthday;
import com.digital.commerce.integration.reward.domain.api.BirthdayGiftItem;
import com.digital.commerce.integration.reward.domain.api.CharityItem;
import com.digital.commerce.integration.reward.domain.api.Customer;
import com.digital.commerce.integration.reward.domain.api.EmailSubscriber;
import com.digital.commerce.integration.reward.domain.api.EmailsItem;
import com.digital.commerce.integration.reward.domain.api.ErrorItem;
import com.digital.commerce.integration.reward.domain.api.IncentiveItem;
import com.digital.commerce.integration.reward.domain.api.ItemsItem;
import com.digital.commerce.integration.reward.domain.api.LinesItem;
import com.digital.commerce.integration.reward.domain.api.Loyalty;
import com.digital.commerce.integration.reward.domain.api.Name;
import com.digital.commerce.integration.reward.domain.api.OffersItem;
import com.digital.commerce.integration.reward.domain.api.OrderHistory;
import com.digital.commerce.integration.reward.domain.api.OrdersItem;
import com.digital.commerce.integration.reward.domain.api.PhonesItem;
import com.digital.commerce.integration.reward.domain.api.PointsHistory;
import com.digital.commerce.integration.reward.domain.api.Preferences;
import com.digital.commerce.integration.reward.domain.api.Product;
import com.digital.commerce.integration.reward.domain.api.Rewards;
import com.digital.commerce.integration.reward.domain.api.RewardsItem;
import com.digital.commerce.integration.reward.domain.api.RewardsSendOrderRequest;
import com.digital.commerce.integration.reward.domain.api.SearchMember;
import com.digital.commerce.integration.reward.domain.api.ShopFor;
import com.digital.commerce.integration.reward.domain.api.ShopForsItem;
import com.digital.commerce.integration.reward.domain.api.Tier;
import com.digital.commerce.integration.reward.domain.api.TransactionsItem;
import com.digital.commerce.integration.reward.util.BtsRewardsCache;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.HttpServletUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.common.service.client.ServiceRequest;
import com.digital.commerce.integration.common.service.client.ServiceRequest.ServiceContentType;
import com.digital.commerce.integration.common.service.client.ServiceRequest.ServiceHttpMethod;
import com.digital.commerce.integration.util.PropertyUtil;
import com.fasterxml.jackson.core.type.TypeReference;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.xml.soap.SOAPBody;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import lombok.Getter;
import lombok.Setter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author mmallipu
 *
 */
@SuppressWarnings({ "unchecked" })
@Getter
@Setter
public class BtsRewardService extends BaseIntegrationService implements RewardService {

	private static final String LINKED_EXCEPTION_STACK_TRACE = "//linkedExceptionStackTrace";
	private static final String APIKEY = "apikey";
	private static final String MOCKING = "mocking";
	private static final String CACHE_REWARDS_CALLS = "cacheRewardsCalls";
	private static final String ADDRESS_TYPE_HOME = "Home";
	private static final String PHONE_TYPE_MOBILE = "Mobile";
	private static final String PHONE_TYPE_HOME = "Home";
	private static final String PHONE_TYPE_OTHER = "Other";
	private static final String EMAIL_TYPE_MARKETING = "Marketing";
	private static final String PROFILE_ID = "{id}";
	private static final String LOYALTY_ID = "{id}";
	private static final String BIRTHDAY_GIFT_ID = "{friendId}";
	private static final String SHOPFOR_ID = "{shopforId}";
	private static final String DENOMINATION = "{denomination}";
  private static final String ORDER_ID = "{orderId}";
  private static final String BARCODE = "{barcode}";

	private final XPathFactory factory = XPathFactory.newInstance();
	private final XPath faultXpath = factory.newXPath();

  private final SimpleDateFormat vipRewardsFormatter =
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);


	private IntegrationServiceClient serviceClient;

	private String getDefaultUri() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, "defaultUri");
	}

	public String getApplication() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, "application");
	}

	/**
	 * @return int
	 */
	private int getCaptureBusinessUnitNumber() {
		String captureBusinessUnitNumber = getMapData(WebServicesConfig.DataTypes.STATIC,
				"captureBusinessUnitNumber");
		if (DigitalStringUtil.isEmpty(captureBusinessUnitNumber)) {
			return 58;
		}
		return Integer.valueOf(captureBusinessUnitNumber);
	}

	/**
	 * @return int
	 */
	private int getCaptureLocationNumber() {
		String captureLocationNumber = getMapData(WebServicesConfig.DataTypes.STATIC,
				"captureLocationNumber");
		if (DigitalStringUtil.isEmpty(captureLocationNumber)) {
			return 99995;
		}
		return Integer.valueOf(captureLocationNumber);
	}

	/**
	 * @return int
	 */
	private int getOnlineStoreNumber() {
		String onlineStoreNumber = getMapData(WebServicesConfig.DataTypes.STATIC, "onlineStoreNumber");
		if (DigitalStringUtil.isEmpty(onlineStoreNumber)) {
			return 29108;
		}
		return Integer.valueOf(onlineStoreNumber);
	}

	/**
	 * Default days back of points history transactions
	 *
	 * @return int
	 */
	public int getDaysBackForPointsHistory() {
		String daysBackForPointsHistory = getMapData(WebServicesConfig.DataTypes.STATIC,
				"daysBackForPointsHistory");
		if (DigitalStringUtil.isEmpty(daysBackForPointsHistory)) {
			return 183;
		}
		return Integer.valueOf(daysBackForPointsHistory);
	}

	/**
	 * @return String
	 */
	public String getUsername() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, "username");
	}

	/**
	 * @return String
	 */
	public String getApiKey() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, APIKEY);
	}

	/**
	 * @return Map
	 */
	public Map<String, String> getRestResources() {
		return getMapData(WebServicesConfig.DataTypes.REST_RESOURCES);
	}

	/**
	 * @return Map
	 */
	public Map<String, String> getMockingResources() {
		return getMapData(WebServicesConfig.DataTypes.MOCKING_RESOURCES);
	}

	/**
	 * @return String
	 */
	private String getSelectCustomerResource() {
		return PropertyUtil
				.getMappedProperty(getRestResources(), ServiceMethod.SELECT_CUSTOMER.getServiceMethodName())
				.toString();
	}

	/**
	 * @return String
	 */
	private String getUpdateCustomerResource() {
		return PropertyUtil
				.getMappedProperty(getRestResources(), ServiceMethod.UPDATE_CUSTOMER.getServiceMethodName())
				.toString();
	}

	/**
	 * @return String
	 */
	private String getSendOrderResource() {
		return PropertyUtil
				.getMappedProperty(getRestResources(), ServiceMethod.SEND_ORDER.getServiceMethodName())
				.toString();
	}
	
	/**
	 * @return String
	 */
	private String getEnticementsResource() {
		return PropertyUtil
				.getMappedProperty(getRestResources(), ServiceMethod.RETRIEVE_ENTICEMENTS.getServiceMethodName())
				.toString();
	}

	/**
	 * @return String
	 */
	private String getUpdateCustomerByMemberIdResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.UPDATE_CUSTOMER_BY_MEMBERID.getServiceMethodName()).toString();
	}

	private String getViewMemberDataResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.VIEW_MEMBER_DATA.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getRetrieveOffersResource() {
		return PropertyUtil
				.getMappedProperty(getRestResources(), ServiceMethod.RETRIEVE_OFFERS.getServiceMethodName())
				.toString();
	}

	/**
	 * @return String
	 */
	private String getSelectCertificatesByProfileIdResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.SELECT_CERTIFICATES_BY_PROFILEID.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getSelectCertificatesByMemberIdResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.SELECT_CERTIFICATES_BY_MEMBERID.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getAddShopForResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.ADD_SHOPFOR.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getAddBirthdayGiftResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.ADD_BIRTHDAY_GIFT.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getUpdateShopForResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.UPDATE_SHOPFOR.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getDeleteShopForResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.DELETE_SHOPFOR.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getUpdateBirthdayGiftResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.UPDATE_BIRTHDAY_GIFT.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getRetrieveShopForsResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.RETRIEVE_SHOPFORS.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getRetrieveBirthdayGiftsResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.RETRIEVE_BIRTHDAY_GIFTS.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getEmailSubscriberResource() {
    return PropertyUtil
        .getMappedProperty(getRestResources(),
            ServiceMethod.UPDATE_EMAIL_SUBSCRIBER.getServiceMethodName())
        .toString();
  }

	/**
	 * @return String
	 */
	private String getCharitiesResource() {
		return PropertyUtil
				.getMappedProperty(getRestResources(),
						ServiceMethod.RETRIEVE_CHARITIES.getServiceMethodName())
				.toString();
	}

	/**
	 * @return String
	 */
	private String getDonateCertificatesResource() {
		return PropertyUtil
				.getMappedProperty(getRestResources(),
						ServiceMethod.DONATE_CERTIFICATE.getServiceMethodName())
				.toString();
	}

	/**
	 * @return String
	 */
	private String getShopWithoutACardResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.ADD_SHOP.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getReserveRewardsResource() {
    return PropertyUtil
        .getMappedProperty(getRestResources(),
            ServiceMethod.RESERVE_LOYALTY_CERTIFICATE.getServiceMethodName())
        .toString();
  }

	/**
	 * @return String
	 */
	private String getCancelRewardsResource() {
    return PropertyUtil
        .getMappedProperty(getRestResources(),
            ServiceMethod.CANCEL_LOYALTY_CERTIFICATE_RESERVATION.getServiceMethodName())
        .toString();
  }

	/**
	 * @return String
	 */
	private String getRetrieveMemberIdResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.RETRIEVE_MEMBERID.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getIncentivesResource() {
		return PropertyUtil
				.getMappedProperty(getRestResources(),
						ServiceMethod.RETRIEVE_INCENTIVES.getServiceMethodName())
				.toString();
	}

	/**
	 * @return String
	 */
	private String getRequestCertificateResource() {
		return PropertyUtil
				.getMappedProperty(getRestResources(),
						ServiceMethod.REQUEST_CERTIFICATE.getServiceMethodName())
				.toString();
	}

	/**
	 * @return String
	 */
	private String getRetrieveOrderHistoryResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.ORDER_HISTORY.getServiceMethodName()).toString();
	}

	/**
	 * @return String
	 */
	private String getSelectViewCertificatesHistoryResource() {
		return PropertyUtil.getMappedProperty(getRestResources(),
				ServiceMethod.VIEW_CERTIFICATE_HISTORY.getServiceMethodName()).toString();
	}

	/**
	 *
	 * @param methodName
	 * @return true or false
	 */
	private boolean isMockingEnabled(String methodName) {
		boolean mockingEnabled;
		String value = PropertyUtil.getMappedProperty(getMockingResources(), methodName).toString();
		if (value == null) {
			mockingEnabled = false;
		} else {
      mockingEnabled = value.equalsIgnoreCase("y") || value.equalsIgnoreCase("true")
          || value.equalsIgnoreCase("t");
		}

		return mockingEnabled;
	}

	@Override
	public void postStartup() throws DigitalIntegrationException {

	}

	@Override
	public void postShutdown() throws DigitalIntegrationException {

	}

	@Override
	protected String getService() {
		return WebServicesConfig.ServicNames.REWARDS.getName();
	}

  /**
   * Use only for anonymous users!
   *
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  @Override
  public RewardServiceResponse updateEmailSubscriber(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException {

    String serviceMethodName = ServiceMethod.UPDATE_EMAIL_SUBSCRIBER.getServiceMethodName();

    if (doRunService(serviceMethodName)) {

      startPerformanceMonitorOperation(serviceMethodName);

      if (rewardServiceRequest == null || rewardServiceRequest.getContact() == null
          || DigitalStringUtil.isBlank(rewardServiceRequest.getContact().getEmail())) {
        throw new DigitalIntegrationBusinessException("Email is missing for updateEmailSubscriber",
            ErrorCodes.MISSING_REQUIRED.getCode());
      }
      Boolean optIn = (DigitalStringUtil.isNotBlank(rewardServiceRequest.getFlags().getNoEMail()) &&
			  rewardServiceRequest.getFlags().getNoEMail().equalsIgnoreCase("Y"))? false : true;

      return processEmailSubscriber(rewardServiceRequest, serviceMethodName, optIn);

    } else {
      throw new DigitalIntegrationInactiveException(
          String
              .format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
          ErrorCodes.INACTIVE.getCode());
    }
  }

  /**
   * @param rewardServiceRequest RewardServiceRequest
   * @param serviceMethodName String
   * @param optIn Boolean
   *
   * @return RewardServiceResponse
   * @throws DigitalIntegrationBusinessException
   */
  private RewardServiceResponse processEmailSubscriber(RewardServiceRequest rewardServiceRequest,
      String serviceMethodName, Boolean optIn) throws DigitalIntegrationBusinessException {
    boolean hasException = false;
    try {
      RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
      rewardServiceResponse.setSuccess(false);

      EmailSubscriber emailSubscriberRequest = populateEmailSubscriberRequest(
          rewardServiceRequest, optIn);

      String bodyContent = serviceClient
          .getMarshalledStringFromObject(EmailSubscriber.class, emailSubscriberRequest);

      ServiceRequest serviceRequest = new ServiceRequest(
          getConnectionTimeout(serviceMethodName),
          getRequestTimeout(serviceMethodName),
          getDefaultUri() + getEmailSubscriberResource(),
          ServiceRequest.ServiceContentType.JSON, ServiceHttpMethod.PUT, serviceMethodName);
      serviceRequest.setBody(bodyContent);

      populateRequestHeaders(serviceRequest, serviceMethodName);

      HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

      if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
        rewardServiceResponse.setSuccess(true);
      } else {
        handleError(response, rewardServiceResponse);
      }

      return rewardServiceResponse;

    } catch (Exception e) {
      hasException = true;

      throw new DigitalIntegrationBusinessException("VIP Rewards processEmailSubscriber Error",
          ErrorCodes.COMM_ERROR.getCode(), e);
    } finally {
      endPerformanceMonitorOperation(serviceMethodName, hasException);
    }
  }

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse addCustomer(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.ADD_CUSTOMER.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			try {
				RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
				rewardServiceResponse.setSuccess(false);

				Customer addCustomerRequest = populateCustomerFromRewardsServiceRequest(
						rewardServiceRequest);

				populateCustomerWithLoyaltyInfo(rewardServiceRequest, addCustomerRequest);

				String bodyContent = serviceClient
						.getMarshalledStringFromObject(Customer.class, addCustomerRequest);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri(),
						ServiceRequest.ServiceContentType.JSON,
						ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setBody(bodyContent);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
					rewardServiceResponse.setSuccess(true);
				} else {
					handleError(response, rewardServiceResponse);
				}

				return rewardServiceResponse;

			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("VIP Rewards addCustomer Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * UpdateCustomer requires the customer object with changes.
	 *
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse updateCustomer(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.UPDATE_CUSTOMER.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException("ProfileId missing for updateCustomer",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			try {
				RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
				rewardServiceResponse.setSuccess(false);

				Customer updateCustomerRequest = populateCustomerFromRewardsServiceRequest(
						rewardServiceRequest);

				String bodyContent = serviceClient
						.getMarshalledStringFromObject(Customer.class, updateCustomerRequest);

				String profileId = "";
				if (rewardServiceRequest.getPerson() != null) {
					profileId = rewardServiceRequest.getPerson().getProfileID();
				}

				String endPoint = populateURL(getDefaultUri() + getUpdateCustomerResource(), PROFILE_ID,
						profileId);

				ServiceRequest serviceRequest = new ServiceRequest(
						getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), endPoint,
						ServiceRequest.ServiceContentType.JSON, ServiceHttpMethod.PUT, serviceMethodName);
				serviceRequest.setBody(bodyContent);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				handleAddOrUpdateCustomerResponse(rewardServiceResponse, response);

				return rewardServiceResponse;

			} catch (Exception e) {
				hasException = true;
				logger.error("VIP Rewards updateCustomer Error", e);
				throw new DigitalIntegrationBusinessException(e.getMessage(),
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}

	}


	/**
	 * UpdateCustomer requires the full customer object as well as the changes. Anything omitted will
	 * be removed from the rewards system
	 *
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse updateCustomerByMemberId(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.UPDATE_CUSTOMER_BY_MEMBERID.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null ||
					rewardServiceRequest.getPerson() == null ||
					DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getMemberID())) {
				throw new DigitalIntegrationBusinessException("MemberId missing for updateCustomerByMemberId",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			try {
				RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
				rewardServiceResponse.setSuccess(false);

				Customer updateCustomerRequest = populateCustomerFromRewardsServiceRequest(
						rewardServiceRequest);

				String bodyContent = serviceClient
						.getMarshalledStringFromObject(Customer.class, updateCustomerRequest);

				String loyaltyId = rewardServiceRequest.getPerson().getMemberID();

				String endPoint = populateURL(getDefaultUri() + getUpdateCustomerByMemberIdResource(),
						LOYALTY_ID, loyaltyId);

				ServiceRequest serviceRequest = new ServiceRequest(
						getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), endPoint,
						ServiceRequest.ServiceContentType.JSON, ServiceHttpMethod.PUT, serviceMethodName);
				serviceRequest.setBody(bodyContent);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				handleAddOrUpdateCustomerResponse(rewardServiceResponse, response);

				return rewardServiceResponse;

			} catch (Exception e) {
				hasException = true;
				logger.error("VIP Rewards updateCustomerByMemberId Error", e);
				throw new DigitalIntegrationBusinessException(e.getMessage(),
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}

	}

	/**
	 * @param rewardServiceResponse
	 * @param response
	 * @throws DigitalIntegrationException
	 */
	private void handleAddOrUpdateCustomerResponse(RewardServiceResponse rewardServiceResponse,
			HttpServiceResponse response)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			Customer customerResponse = serviceClient
					.getUnmarshalledObjectFromHttpServiceResponse(Customer.class, response);
			rewardServiceResponse.setSuccess(true);
			populateCustomerRewardsDataToRewardServiceResponse(rewardServiceResponse, customerResponse);
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param serviceRequest
	 * @param serviceMethodName
	 */
	private void populateRequestHeaders(ServiceRequest serviceRequest, String serviceMethodName) {
		Map<String, String> headerEntries = new HashMap<>();
		headerEntries.put(APIKEY, getApiKey());
		if (isMockingEnabled(serviceMethodName)) {
			headerEntries.put(MOCKING, "true");
		}
		String requestId = HttpServletUtil.getRequestId();
		if(DigitalStringUtil.isNotBlank(requestId)) {
			headerEntries.put("Request-Id", requestId);
		}
		serviceRequest.setHeaderEntries(headerEntries);
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse selectCustomerByProfileId(
			RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.SELECT_CUSTOMER.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException("ProfileId missing for selectCustomerByProfileId",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			// Create cacheKey for the method
			String cacheKey =
					serviceMethodName + ":" + rewardServiceRequest.getPerson().getProfileID();

			try {
				//  check whether we have a cached response within the request scope
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				boolean cacheEnabled = false;
				if (null != request) {
					String cacheRewardsCalls = request.getParameter(CACHE_REWARDS_CALLS);
					cacheEnabled = DigitalStringUtil.isNotBlank(cacheRewardsCalls) && "Y"
              .equalsIgnoreCase(cacheRewardsCalls);
				}
				RewardServiceResponse rewardServiceResponse = this
						.getCachedResponse(cacheKey, cacheEnabled);

				if (null == rewardServiceResponse) {
					rewardServiceResponse = new RewardServiceResponse();
					rewardServiceResponse.setSuccess(false);
					String profileId = "";
					if (rewardServiceRequest.getPerson() != null) {
						profileId = rewardServiceRequest.getPerson().getProfileID();
					}

					ServiceRequest serviceRequest = new ServiceRequest(
							getConnectionTimeout(serviceMethodName),
							getRequestTimeout(serviceMethodName),
							populateURL(getDefaultUri() + getSelectCustomerResource(), PROFILE_ID,
									profileId), ServiceRequest.ServiceContentType.JSON,
							ServiceHttpMethod.GET, serviceMethodName);

					populateRequestHeaders(serviceRequest, serviceMethodName);

					HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

					handleRetrieveCustomerResponse(cacheKey, cacheEnabled, rewardServiceResponse,
							response);
				}
				return rewardServiceResponse;

			} catch (Exception e) {
				hasException = true;
				logger.error("VIP Rewards selectCustomer Error", e);
				if (e instanceof DigitalIntegrationSystemException
						|| e instanceof DigitalIntegrationBusinessException) {
					throw e;
				}

				throw new DigitalIntegrationBusinessException("VIP Rewards selectCustomer Error",
						ErrorCodes.COMM_ERROR.getCode(), e.getCause());
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse retrieveOffersByProfileId(
			RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.RETRIEVE_OFFERS.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {
			startPerformanceMonitorOperation(serviceMethodName);

			// Do null check for profileId as we are sending blank if few flows, 
			// does not make sense to send blank and  rewards tell us profileId is blank
			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException("ProfileId missing for retrieveOffersByProfileId",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}
			// Create cacheKey for the method
			String cacheKey =
					serviceMethodName + ":" + rewardServiceRequest.getPerson().getProfileID();

			try {
				//  check whether we have a cached response within the request scope
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				boolean cacheEnabled = false;
				if (null != request) {
					String cacheRewardsCalls = request.getParameter(CACHE_REWARDS_CALLS);
					cacheEnabled = DigitalStringUtil.isNotBlank(cacheRewardsCalls) && "Y"
              .equalsIgnoreCase(cacheRewardsCalls);
				}
				RewardServiceResponse rewardServiceResponse = this
						.getCachedResponse(cacheKey, cacheEnabled);

				// if there is no data in the cache, then only attempt to make a rewards call
				if (null == rewardServiceResponse) {
					rewardServiceResponse = new RewardServiceResponse();
					rewardServiceResponse.setSuccess(false);
					String profileId = "";
					if (rewardServiceRequest.getPerson() != null) {
						profileId = rewardServiceRequest.getPerson().getProfileID();
					}

					ServiceRequest serviceRequest = new ServiceRequest(
							getConnectionTimeout(serviceMethodName),
							getRequestTimeout(serviceMethodName),
							populateURL(getDefaultUri() + getRetrieveOffersResource(), PROFILE_ID,
									profileId), ServiceRequest.ServiceContentType.JSON,
							ServiceHttpMethod.GET, serviceMethodName);

					populateRequestHeaders(serviceRequest, serviceMethodName);

					HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

					handleRetrieveOffersResponse(rewardServiceResponse, response);

				}
				return rewardServiceResponse;
			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("VIP Rewards retrieveOffers Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param offersItems
	 */
	private void populateOffersDataToBtsRewardServiceResponse(
			RewardServiceResponse rewardServiceResponse,
			List<OffersItem> offersItems) {
		if (offersItems != null) {
			List<Offer> offersList = new ArrayList<>();
			for (OffersItem offer : offersItems) {
				Offer vipOffer = new Offer();

				vipOffer.setCampaignID(offer.getCampaignId());
				vipOffer.setEndDate(offer.getEndDate());
				if(null != offer.getPartyId()) {
					vipOffer.setIndividualID(Long.toString(offer.getPartyId()));
				}
				vipOffer.setOfferID(offer.getOfferId());
				vipOffer.setPromoID(offer.getPromoId());
				vipOffer.setSegmentID(offer.getSegmentId());
				vipOffer.setStartDate(offer.getStartDate());
				offersList.add(vipOffer);
			}
			rewardServiceResponse.setOffers(offersList);
		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse retrieveMemberID(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.RETRIEVE_MEMBERID.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			try {
				RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
				rewardServiceResponse.setSuccess(false);

				SearchMember searchMemberRequest = populateMemberFromRewardsServiceRequest(
						rewardServiceRequest);

				String bodyContent = serviceClient
						.getMarshalledStringFromObject(Customer.class, searchMemberRequest);

				ServiceRequest serviceRequest = new ServiceRequest(
						getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri() +
						getRetrieveMemberIdResource(),
						ServiceContentType.JSON, ServiceHttpMethod.POST, serviceMethodName);

				serviceRequest.setBody(bodyContent);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
					Customer customer = serviceClient
							.getUnmarshalledObjectFromHttpServiceResponse(Customer.class, response);
					rewardServiceResponse.setSuccess(true);
					if(null != customer.getPrimaryLoyaltyAccountNumber()) {
						rewardServiceResponse.getPerson()
								.setMemberID(customer.getPrimaryLoyaltyAccountNumber());
					}
				} else {
					handleError(response, rewardServiceResponse);
				}

				return rewardServiceResponse;

			} catch (Exception e) {
				hasException = true;
				logger.error("Bts Rewards retrieveMemberId Error", e);
				throw new DigitalIntegrationBusinessException("VIP Rewards retrieveMemberId Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}

	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	public RewardServiceResponse addShopWithoutACardRequest(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.ADD_SHOP.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null ||
					rewardServiceRequest.getPerson() == null ||
					DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getMemberID())) {
				throw new DigitalIntegrationBusinessException("MemberId missing for addShopWithoutACardRequest",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			try {
				RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
				rewardServiceResponse.setSuccess(false);

				String loyaltyId = rewardServiceRequest.getPerson().getMemberID();

				String initEndPoint = populateURL(getDefaultUri() + getShopWithoutACardResource(),
						LOYALTY_ID, loyaltyId);

				String endPoint = populateURL(initEndPoint, BARCODE, rewardServiceRequest.getBarcode());

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), endPoint,
						ServiceRequest.ServiceContentType.JSON,
						ServiceHttpMethod.POST, serviceMethodName);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
					rewardServiceResponse.setSuccess(true);
				} else {
					handleError(response, rewardServiceResponse);
				}

				return rewardServiceResponse;

			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException(e.getMessage(),
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse viewMemberData(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.VIEW_MEMBER_DATA.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null ||
					rewardServiceRequest.getPerson() == null ||
					DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getMemberID())) {
				throw new DigitalIntegrationBusinessException("MemberId missing for viewMemberData",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			// Create cacheKey for the method
			String cacheKey =
					serviceMethodName + ":" + rewardServiceRequest.getPerson().getMemberID();

			try {
				//  check whether we have a cached response within the request scope
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				boolean cacheEnabled = false;
				if (null != request) {
					String cacheRewardsCalls = request.getParameter(CACHE_REWARDS_CALLS);
					cacheEnabled = DigitalStringUtil.isNotBlank(cacheRewardsCalls)
              && "Y".equalsIgnoreCase(cacheRewardsCalls);
				}
				RewardServiceResponse rewardServiceResponse = this
						.getCachedResponse(cacheKey, cacheEnabled);

				if (null == rewardServiceResponse) {
					rewardServiceResponse = new RewardServiceResponse();
					rewardServiceResponse.setSuccess(false);

					String loyaltyId = rewardServiceRequest.getPerson().getMemberID();

					ServiceRequest serviceRequest = new ServiceRequest(
							getConnectionTimeout(serviceMethodName),
							getRequestTimeout(serviceMethodName),
							populateURL(getDefaultUri() + getViewMemberDataResource(), LOYALTY_ID,
									loyaltyId), ServiceRequest.ServiceContentType.JSON,
							ServiceHttpMethod.GET, serviceMethodName);

					populateRequestHeaders(serviceRequest, serviceMethodName);

					HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

					handleRetrieveCustomerResponse(cacheKey, cacheEnabled, rewardServiceResponse,
							response);
				}
				return rewardServiceResponse;

			} catch (Exception e) {
				hasException = true;
				logger.error("VIP Rewards viewMemberData Error", e);
				throw new DigitalIntegrationBusinessException("VIP Rewards viewMemberData Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}

	}

	/**
	 * @param cacheKey
	 * @param cacheEnabled
	 * @param rewardServiceResponse
	 * @param response
	 * @throws DigitalIntegrationException
	 */
	private void handleRetrieveCustomerResponse(String cacheKey, boolean cacheEnabled,
			RewardServiceResponse rewardServiceResponse, HttpServiceResponse response)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			Customer customerResponse = serviceClient
					.getUnmarshalledObjectFromHttpServiceResponse(Customer.class, response);
			rewardServiceResponse.setSuccess(true);
			populateCustomerRewardsDataToRewardServiceResponse(rewardServiceResponse, customerResponse);
			if (cacheEnabled) {
				this.getRewardsCache().putRewardsData(cacheKey, rewardServiceResponse);
			}
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param cacheKey
	 * @param cacheEnabled
	 * @param rewardServiceResponse
	 * @param response
	 * @throws DigitalIntegrationException
	 */
	private void handleViewCertificateHistoryResponse(String cacheKey, boolean cacheEnabled,
			RewardServiceResponse rewardServiceResponse, HttpServiceResponse response)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			try {
				PointsHistory pointsHistoryResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(PointsHistory.class, response);

				rewardServiceResponse.setSuccess(true);

				populateCertificateHistoryDataToRewardServiceResponse(rewardServiceResponse,
						pointsHistoryResponse.getTransactions());
				if (cacheEnabled) {
					this.getRewardsCache().putRewardsData(cacheKey, rewardServiceResponse);
				}

			} catch (Exception e) {
				logger.error("Error marshalling response ", e);
				throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error",
						ErrorCodes.XFORM.getCode(), e);
			}
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param customer
	 */
	private void populateCustomerRewardsDataToRewardServiceResponse(
			RewardServiceResponse rewardServiceResponse,
			Customer customer) {
		List<AddressesItem> addressesItems = customer.getAddresses();
		if (null != addressesItems) {
			for (AddressesItem addressesItem : addressesItems) {
				if (ADDRESS_TYPE_HOME.equalsIgnoreCase(addressesItem.getType())) {
					Address address = new Address();
					address.setAddress1(addressesItem.getAddressLine1());
					address.setAddress2(addressesItem.getAddressLine2());
					address.setCity(addressesItem.getCity());
					address.setCountry(addressesItem.getCountry());
					address.setPostCode(addressesItem.getPostalCode());
					address.setState(addressesItem.getState());
					rewardServiceResponse.setAddress(address);
				}
			}
		}

		Contact contact = new Contact();
		List<PhonesItem> phonesItems = customer.getPhones();
		if (null != phonesItems) {
			for (PhonesItem phonesItem : phonesItems) {
				if (PHONE_TYPE_MOBILE.equalsIgnoreCase(phonesItem.getType())) {
					if(null != phonesItem.getPhoneNumber()) {
						contact.setMobilePhone(Long.toString(phonesItem.getPhoneNumber()));
					}
				} else if (PHONE_TYPE_HOME.equalsIgnoreCase(phonesItem.getType())) {
					if(null != phonesItem.getPhoneNumber()) {
						contact.setPhone1(Long.toString(phonesItem.getPhoneNumber()));
					}
				} else if (PHONE_TYPE_OTHER.equalsIgnoreCase(phonesItem.getType())) {
					if(null != phonesItem.getPhoneNumber()) {
						contact.setPhone2(Long.toString(phonesItem.getPhoneNumber()));
					}
				}
			}
		}

		List<EmailsItem> emailsItems = customer.getEmails();
		if (null != emailsItems) {
			for (EmailsItem emailsItem : emailsItems) {
				if (DigitalStringUtil.isNotBlank(emailsItem.getEmail())) {
					if (EMAIL_TYPE_MARKETING.equalsIgnoreCase(emailsItem.getType())) {
						contact.setEmail(emailsItem.getEmail());
					}
				}
			}
		}
		rewardServiceResponse.setContact(contact);

		Person person = new Person();
		if(null != customer.getPartyId()) {
			person.setCustomerID(Long.toString(customer.getPartyId()));
		}
		if (null != customer.getBirthday()) {
			if (null != customer.getBirthday().getDayOfBirth()) {
				person.setDayOfBirth(Long.toString(customer.getBirthday().getDayOfBirth()));
			}
			if (null != customer.getBirthday().getMonthOfBirth()) {
				person.setMonthOfBirth(Long.toString(customer.getBirthday().getMonthOfBirth()));
			}
		}

		if (null != customer.getName()) {
			person.setFirstName(customer.getName().getFirstName());
			person.setLastName(customer.getName().getLastName());
		}
		if (null != customer.getGender()) {
			person.setGender(Person.Gender.getGenderFromDescription(customer.getGender()));
		}

		if (null != customer.getPrimaryLoyaltyAccountNumber()) {
			person.setMemberID(customer.getPrimaryLoyaltyAccountNumber());
		}

		rewardServiceResponse.setPerson(person);
		Flags flags = new Flags();
		flags.setLoyaltyMemberFlag(flag(customer.isLoyaltyMemberFlag()));
		flags.setIsCardHolder(flag(customer.isCreditCardMember()));

		Attributes attributes = customer.getAttributes();
		if (null != attributes) {
			flags.setInvalidLoyaltyMemberFlag(flag(attributes.isInvalidLoyaltyMemberFlag()));
			rewardServiceResponse.setFraudCode(attributes.getFraudCode());
			rewardServiceResponse.setFraudCodeGroup(attributes.getFraudCodeGroup());
		}

		Preferences preferences = customer.getPreferences();

		if (null != preferences) {
			if (null != preferences.getCertificateDenominationChoice()) {
				rewardServiceResponse
						.setCertDenomination(Integer.toString(preferences.getCertificateDenominationChoice()));
			}
			rewardServiceResponse.setIsPaperlessCert(flag(preferences.isPaperlessRewardsOptIn()));
			if (null != preferences.getPreferredStore()) {
				rewardServiceResponse.setPreferredStore(preferences.getPreferredStore());
			}

			String emailOptInFlag = invertedFlag(preferences.isEmailOptIn());
			flags.setNoEMail(emailOptInFlag);
			flags.setOptOutFashionEmail(emailOptInFlag);

			String mobileTextOptInFlag = invertedFlag(preferences.isTextOptIn());
			flags.setNoPhone(mobileTextOptInFlag);
			flags.setOptOutRewardsText(mobileTextOptInFlag);
		}

		rewardServiceResponse.setFlags(flags);

		List<Points> points = new ArrayList<>();
		Points point = new Points();
		Tier tier = customer.getTier();
		if (null != tier) {
			if(DigitalStringUtil.isNotEmpty(tier.getTier()))
			point.setTier(tier.getTier().toUpperCase());
			if (DigitalStringUtil.isNotBlank(tier.getTierStartDate())) {
				try {
					point.setLoyaltyTierStartDate(vipRewardsFormatter.parse(tier.getTierStartDate()));
				} catch (ParseException e) {
					logger.error("Error parsing TierStartDate");
				}
			}
			if (DigitalStringUtil.isNotBlank(tier.getTierExpirationDate())) {
				try {
					point.setLoyaltyTierExpirationDate(
							vipRewardsFormatter.parse(tier.getTierExpirationDate()));
				} catch (ParseException e) {
					logger.error("Error parsing TierExpirationDate");
				}
			}
		}

		Loyalty loyalty = customer.getLoyalty();

		populateLoyaltyData(point, loyalty);

		if(null != customer.getDollarsNextReward()) {
			point.setDollarsNextReward(customer.getDollarsNextReward());
		}
		if(null != customer.getDollarsFutureReward()) {
			point.setDollarsFutureReward(customer.getDollarsFutureReward());
		}
		if(null != customer.getSpendUntilNextTier()) {
			point.setDollarsNextTier(customer.getSpendUntilNextTier());
		}

		// Default it to zero as the certificate is issued realtime
		point.setDollarsPendingCertIssue(0);

		if(null != customer.getCurrentYearSpend()) {
			point.setTotalQualifyingAmount(new Double(customer.getCurrentYearSpend()));
		}

		points.add(point);
		rewardServiceResponse.setPoints(points);

	}

	/**
	 * @param point
	 * @param loyalty
	 */
	private void populateLoyaltyData(Points point, Loyalty loyalty) {
		if (null != loyalty) {
			point.setLoyaltyPoints(loyalty.getCurrentPointsBalance());

			point.setPointsUntilNextReward(loyalty.getPointsUntilNextReward());
			point.setDateLoyaltyPoints(new Date());

			if (DigitalStringUtil.isNotBlank(loyalty.getSignUpDate())) {
				try {
					point.setLoyaltySignupDate(vipRewardsFormatter.parse(loyalty.getSignUpDate()));
				} catch (ParseException e) {
					logger.error("Failed to parse reward bDay gift added date", e);
				}
			}
			if(loyalty.getSignUpStore() != null) {
				point.setLoyaltySignupStoreNumber(Integer.toString(loyalty.getSignUpStore()));
			}

			point.setLoyaltyPoints(loyalty.getCurrentPointsBalance());
		}
	}

	/**
   * @param value
   * @return String
   */
  private String flag(Boolean value) {
    if (value != null && value) {
      return "Y";
    }
    return "N";
  }

  /**
   * @param value
   * @return String
   */
  private String invertedFlag(Boolean value) {
		if (value != null && value) {
      return "N";
    }
    return "Y";
  }

  /**
   * @param value
   * @return true or false
   */
  private boolean invertedStringFlag(String value) {
    return !"Y".equalsIgnoreCase(value);
  }

	/**
	 * @param value
	 * @return true or false
	 */
	private boolean convertStringToBooleanFlag(String value) {
		return !"N".equalsIgnoreCase(value);
	}


	/**
	 * @param rewardServiceRequest
	 * @return Rewards
	 */
	private Rewards populateRewardsFromRewardsServiceRequest(
			RewardServiceRequest rewardServiceRequest) {
		Rewards rewards = new Rewards();
		String captureDate = vipRewardsFormatter.format(new Date());

		if (null != rewardServiceRequest.getCertificates() && !rewardServiceRequest.getCertificates()
				.isEmpty()) {
			List<RewardsItem> rewardsItemList = new ArrayList<>();
			for (String certId : rewardServiceRequest.getCertificates()) {
				RewardsItem rewardsItem = new RewardsItem();
				rewardsItem.setId(Long.valueOf(certId));
				if (null == rewardServiceRequest.getCharity()) {
					rewardsItem.setStatus("RESERVE");
					rewardsItem.setDemandTransactionNumber(rewardServiceRequest.getOrderNumber());
					rewardsItem.setBusinessUnit(getCaptureBusinessUnitNumber());
					rewardsItem.setTransactionDate(captureDate);
					rewardsItem.setLocationNumber(getCaptureLocationNumber());
				} else {
					rewardsItem.setStatus("DONATE");
					rewardsItem.setCharityId(rewardServiceRequest.getCharity().getId());
				}
				rewardsItemList.add(rewardsItem);
			}
			rewards.setRewards(rewardsItemList);
		}

		if (null != rewardServiceRequest.getOffers() && !rewardServiceRequest.getOffers().isEmpty()) {
			List<AuthorizedOffersItem> authorizedOffersItemList = new ArrayList<>();
			for (String certId : rewardServiceRequest.getOffers()) {
				AuthorizedOffersItem authorizedOffersItem = new AuthorizedOffersItem();
				authorizedOffersItem.setId(Long.valueOf(certId));
				authorizedOffersItem.setStatus("RESERVE");
				authorizedOffersItem.setDemandTransactionNumber(rewardServiceRequest.getOrderNumber());
				authorizedOffersItem.setBusinessUnit(getCaptureBusinessUnitNumber());
				authorizedOffersItem.setTransactionDate(captureDate);
				authorizedOffersItem.setLocationNumber(getCaptureLocationNumber());
				authorizedOffersItemList.add(authorizedOffersItem);
			}
			rewards.setAuthorizedOffers(authorizedOffersItemList);
		}

		return rewards;
	}

	/**
	 * @param rewardServiceRequest
	 * @return ShopFor
	 */
	private ShopFor populateShopforFromRewardsServiceRequest(
			RewardServiceRequest rewardServiceRequest) {

		ShopFor shopForAPI = new ShopFor();

		if (rewardServiceRequest.getShopfor() != null) {
			Shopfor shopForDomain = rewardServiceRequest.getShopfor();

			//shopFor.name
			if (DigitalStringUtil.isNotBlank(shopForDomain.getShopforName())) {
				shopForAPI.setName(shopForDomain.getShopforName());
			}
			//shopFor.birthMonth
			if (shopForDomain.getBirthMonth() != null) {
				shopForAPI.setBirthMonth(Integer.parseInt(shopForDomain.getBirthMonth()));
			}
			//shopFor.birthday
			if (shopForDomain.getBirthDay() != null) {
				shopForAPI.setBirthday(Integer.parseInt(shopForDomain.getBirthDay()));
			}
			//shopFor.relationship
			if (DigitalStringUtil.isNotBlank(shopForDomain.getRelationship())) {
				shopForAPI.setRelationship(shopForDomain.getRelationship());
			}
			Product productFilterAPI = new Product();
			//type
			if (DigitalStringUtil.isNotBlank(shopForDomain.getWebType())) {
				productFilterAPI.setType(shopForDomain.getWebType());
			}
			//gender
			if (DigitalStringUtil.isNotBlank(shopForDomain.getGender())) {
				productFilterAPI.setGender(shopForDomain.getGender());
			}
			//genderSubType
			if (DigitalStringUtil.isNotBlank(shopForDomain.getSizeGroup())) {
				productFilterAPI.setGenderSubType(shopForDomain.getSizeGroup());
			}
			//sizes
			if (shopForDomain.getSizes() != null && shopForDomain.getSizes().size() > 0) {
				productFilterAPI.setSize(shopForDomain.getSizes());
			}
			//widths
			if (shopForDomain.getWidths() != null && shopForDomain.getWidths().size() > 0) {
				productFilterAPI.setWidth(shopForDomain.getWidths());
			}

			shopForAPI.setProduct(productFilterAPI);
		}
		return shopForAPI;
	}

	/**
	 * @param rewardServiceRequest
	 * @return BirthdayGiftItem
	 */
	private BirthdayGiftItem populateBirthdayGiftFromRewardsServiceRequest(
			RewardServiceRequest rewardServiceRequest) {

		BirthdayGiftItem birthdayGiftItem = new BirthdayGiftItem();

		if (rewardServiceRequest.getBirthdayGift() != null) {
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getBirthdayGift().getDeliveryDate())) {
				birthdayGiftItem.setGiftDeliveryDate(rewardServiceRequest.getBirthdayGift().getDeliveryDate());
			}
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getBirthdayGift().getBirthdayGiftId())) {
				birthdayGiftItem
						.setGiftRequestId(Long.valueOf(rewardServiceRequest.getBirthdayGift().getBirthdayGiftId()));
			}
			String giftRequestDate = vipRewardsFormatter.format(new Date());
			birthdayGiftItem.setGiftRequestDate(giftRequestDate);
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getBirthdayGift().getFriendName())) {
				birthdayGiftItem.setRecipientGivenName(rewardServiceRequest.getBirthdayGift().getFriendName());
			}
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getBirthdayGift().getEmail())) {
				birthdayGiftItem.setRecipientEmail(rewardServiceRequest.getBirthdayGift().getEmail());
			}
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getBirthdayGift().getMonthOfBirth())) {
				birthdayGiftItem.setRecipientBirthMonth(Integer.valueOf(rewardServiceRequest.getBirthdayGift().getMonthOfBirth()));
			}
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getBirthdayGift().getDayOfBirth())) {
				birthdayGiftItem.setRecipientBirthday(Integer.valueOf(rewardServiceRequest.getBirthdayGift().getDayOfBirth()));
			}
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getBirthdayGift().getMessage())) {
				birthdayGiftItem.setGiftMessageText(rewardServiceRequest.getBirthdayGift().getMessage());
			}
		}
		return birthdayGiftItem;
	}

  /**
   * @param btsRewardServiceRequest RewardServiceRequest
   * @param optIn Boolean
   * @return EmailSubscriber
   */
  private EmailSubscriber populateEmailSubscriberRequest(
      RewardServiceRequest btsRewardServiceRequest, Boolean optIn) {
    EmailSubscriber emailSubscriber = new EmailSubscriber();


    if (btsRewardServiceRequest != null && btsRewardServiceRequest.getContact() != null) {
      String email = btsRewardServiceRequest.getContact().getEmail();
      if (DigitalStringUtil.isNotBlank(email)) {
        emailSubscriber.setEmail(email);
        emailSubscriber.setOptIn(optIn);
      }
    }
    return emailSubscriber;
  }

	/**
	 * @param rewardServiceRequest
	 * @return SearchMember
	 */
	private SearchMember populateMemberFromRewardsServiceRequest(
			RewardServiceRequest rewardServiceRequest) {
		SearchMember customer = new SearchMember();

		if (rewardServiceRequest.getPerson() != null) {
			customer.setFirstName(rewardServiceRequest.getPerson().getFirstName());
			customer.setLastName(rewardServiceRequest.getPerson().getLastName());
		}

		if (rewardServiceRequest.getContact() != null &&
				DigitalStringUtil.isNotEmpty(rewardServiceRequest.getContact().getMobilePhone())) {
			customer.setPhoneNumber(Long.valueOf(rewardServiceRequest.getContact().getMobilePhone()));
		}

		if (rewardServiceRequest.getAddress() != null &&
				DigitalStringUtil.isNotEmpty(rewardServiceRequest.getAddress().getAddress1())) {
			customer.setAddressLine1(rewardServiceRequest.getAddress().getAddress1());
		}

		if (rewardServiceRequest.getContact() != null &&
				DigitalStringUtil.isNotEmpty(rewardServiceRequest.getContact().getEmail())) {
			customer.setEmail(rewardServiceRequest.getContact().getEmail());
		}

		return customer;
	}

	/**
	 * @param rewardServiceRequest
	 * @return Customer
	 */
	private Customer populateCustomerFromRewardsServiceRequest(
			RewardServiceRequest rewardServiceRequest) {
		Customer customer = new Customer();

        String captureDate;
        if (DigitalStringUtil.isBlank(rewardServiceRequest.getSignUpDate())) {
            captureDate = vipRewardsFormatter.format(new Date());
        } else {
            captureDate = rewardServiceRequest.getSignUpDate();
        }

		if (rewardServiceRequest.getAddress() != null) {
			if (DigitalStringUtil.isNotEmpty(rewardServiceRequest.getAddress().getAddress1())) {
				AddressesItem address = new AddressesItem();
				address.setAddressLine1(rewardServiceRequest.getAddress().getAddress1());
				if (DigitalStringUtil.isNotEmpty(rewardServiceRequest.getAddress().getAddress2())) {
					address.setAddressLine2(rewardServiceRequest.getAddress().getAddress2());
				}
				address.setCity(rewardServiceRequest.getAddress().getCity());
				address.setCountry(rewardServiceRequest.getAddress().getCountry());
				address.setPostalCode(rewardServiceRequest.getAddress().getPostCode());
				address.setState(rewardServiceRequest.getAddress().getState());
				address.setCountry(rewardServiceRequest.getAddress().getCountry());
				address.setType(ADDRESS_TYPE_HOME);
				address.setCaptureDate(captureDate);
				address.setCaptureLocationNumber(getCaptureLocationNumber());
				address.setCaptureBusinessUnitNumber(getCaptureBusinessUnitNumber());

				List<AddressesItem> addressesItemList = new ArrayList<>();
				addressesItemList.add(address);

				customer.setAddresses(addressesItemList);
			}
		}

		if (rewardServiceRequest.getPerson() != null) {
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getPerson().getCustomerID())) {
				customer.setPartyId(Long.parseLong(rewardServiceRequest.getPerson().getCustomerID()));
			}
			String dayOfBirth = rewardServiceRequest.getPerson().getDayOfBirth();
			boolean isBirthdayDataAvailable = false;
			Birthday birthday = new Birthday();
			if (DigitalStringUtil.isNotBlank(dayOfBirth) && !"0".equalsIgnoreCase(dayOfBirth)) {
				birthday.setDayOfBirth(Integer.parseInt(dayOfBirth));
				isBirthdayDataAvailable = true;
			}
			String monthOfBirth = rewardServiceRequest.getPerson().getMonthOfBirth();
			if (DigitalStringUtil.isNotBlank(monthOfBirth) && !"0".equalsIgnoreCase(monthOfBirth)) {
				birthday.setMonthOfBirth(Integer.parseInt(monthOfBirth));
				isBirthdayDataAvailable = true;
			}
			if (isBirthdayDataAvailable) {
				customer.setBirthday(birthday);
			}

			boolean nameAvailableFlag = false;

			Name name = new Name();
			if (DigitalStringUtil.isNotEmpty(rewardServiceRequest.getPerson().getFirstName())) {
				name.setFirstName(rewardServiceRequest.getPerson().getFirstName());
				nameAvailableFlag = true;
			}

			if (DigitalStringUtil.isNotEmpty(rewardServiceRequest.getPerson().getLastName())) {
				name.setLastName(rewardServiceRequest.getPerson().getLastName());
				nameAvailableFlag = true;
			}

			if (nameAvailableFlag) {
				customer.setName(name);
			}

			if (rewardServiceRequest.getPerson().getGender() != null) {
				customer.setGender(rewardServiceRequest.getPerson().getGender().getDescription());
			}
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getPerson().getMemberID())) {
				customer.setPrimaryLoyaltyAccountNumber(rewardServiceRequest.getPerson().getMemberID());
			}
		}

		if (rewardServiceRequest.getContact() != null) {

			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getContact().getEmail())) {
				EmailsItem emailsItem = new EmailsItem();
				emailsItem.setEmail(rewardServiceRequest.getContact().getEmail());

				emailsItem.setType(EMAIL_TYPE_MARKETING);
				emailsItem.setCaptureDate(captureDate);
				emailsItem.setCaptureLocationNumber(getCaptureLocationNumber());
				emailsItem.setCaptureBusinessUnitNumber(getCaptureBusinessUnitNumber());

				List<EmailsItem> emailsItemList = new ArrayList<>();
				emailsItemList.add(emailsItem);

				customer.setEmails(emailsItemList);
			}

			List<PhonesItem> phonesItemList = new ArrayList<>();

			String mobilePhone = rewardServiceRequest.getContact().getMobilePhone();
			if (DigitalStringUtil.isNotEmpty(mobilePhone)) {
				populatePhoneDetails(captureDate, phonesItemList, mobilePhone, PHONE_TYPE_MOBILE);
			}

			String homePhone = rewardServiceRequest.getContact().getPhone1();
			if (DigitalStringUtil.isNotEmpty(homePhone)) {
				populatePhoneDetails(captureDate, phonesItemList, homePhone, PHONE_TYPE_HOME);
			}

			String otherPhone = rewardServiceRequest.getContact().getPhone2();
			if (DigitalStringUtil.isNotEmpty(otherPhone)) {
				populatePhoneDetails(captureDate, phonesItemList, otherPhone, PHONE_TYPE_OTHER);
			}

			if (!phonesItemList.isEmpty()) {
				customer.setPhones(phonesItemList);
			}
		}

		Preferences preferences = new Preferences();

		boolean preferencesAvailable = false;

		if (rewardServiceRequest.getFlags() != null) {
			if (DigitalStringUtil.isNotEmpty(rewardServiceRequest.getFlags().getNoEMail())) {
				preferences.setEmailOptIn(invertedStringFlag(rewardServiceRequest.getFlags().getNoEMail()));
				preferencesAvailable = true;
			}
			if (DigitalStringUtil.isNotEmpty(rewardServiceRequest.getFlags().getNoPhone())) {
				preferences.setTextOptIn(invertedStringFlag(rewardServiceRequest.getFlags().getNoPhone()));
				preferencesAvailable = true;
			}
		}

		if (DigitalStringUtil.isNotEmpty(rewardServiceRequest.getCertDenomination())) {
			preferences.setCertificateDenominationChoice(
					Integer.valueOf(rewardServiceRequest.getCertDenomination()));
			preferencesAvailable = true;
		}

		if (DigitalStringUtil.isNotEmpty((rewardServiceRequest.getIsPaperlessCert()))) {
			preferences.setPaperlessRewardsOptIn(
					convertStringToBooleanFlag(rewardServiceRequest.getIsPaperlessCert()));
			preferencesAvailable = true;
		}

		if (DigitalStringUtil.isNotEmpty(rewardServiceRequest.getPreferredStore())) {
			preferences.setPreferredStore(rewardServiceRequest.getPreferredStore());
			preferencesAvailable = true;
		}

		if (preferencesAvailable) {
			customer.setPreferences(preferences);
		}

		if (rewardServiceRequest.getPerson() != null) {
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getPerson().getProfileID())) {
				AtgAccountsItem atgAccountsItem = new AtgAccountsItem();
				atgAccountsItem.setAtgId(rewardServiceRequest.getPerson().getProfileID());

				List<AtgAccountsItem> atgAccountsItemList = new ArrayList<>();
				atgAccountsItemList.add(atgAccountsItem);

				customer.setAtgAccounts(atgAccountsItemList);
			}
		}

		return customer;
	}

	/**
	 * @param rewardServiceRequest
	 * @param customer
	 */
	private void populateCustomerWithLoyaltyInfo(RewardServiceRequest rewardServiceRequest,
			Customer customer) {
	    String captureDate;
	    if (DigitalStringUtil.isBlank(rewardServiceRequest.getSignUpDate())) {
            captureDate = vipRewardsFormatter.format(new Date());
        } else {
	        captureDate = rewardServiceRequest.getSignUpDate();
        }
		Loyalty loyalty = new Loyalty();
		loyalty.setSignUpDate(captureDate);
		loyalty.setSignUpStore(getOnlineStoreNumber());
		loyalty.setEnrollMethod(rewardServiceRequest.getDeviceName());
		customer.setLoyalty(loyalty);
	}

	/**
	 * @param captureDate
	 * @param phonesItemList
	 * @param phoneNumber
	 * @param phoneType
	 */
	private void populatePhoneDetails(String captureDate, List<PhonesItem> phonesItemList,
			String phoneNumber, String phoneType) {
		if (DigitalStringUtil.isNotBlank(phoneNumber)) {
			PhonesItem phonesItem = new PhonesItem();
			phonesItem.setType(phoneType);
			phonesItem.setPhoneNumber(Long.parseLong(phoneNumber));
			phonesItem.setCaptureDate(captureDate);
			phonesItem.setCaptureLocationNumber(getCaptureLocationNumber());
			phonesItem.setCaptureBusinessUnitNumber(getCaptureBusinessUnitNumber());
			phonesItemList.add(phonesItem);
		}
	}

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  public RewardServiceResponse viewCertificateHistory(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException {

    String serviceMethodName = ServiceMethod.VIEW_CERTIFICATE_HISTORY.getServiceMethodName();
    boolean hasException = false;

    if (doRunService(serviceMethodName)) {

      startPerformanceMonitorOperation(serviceMethodName);

      if (rewardServiceRequest == null
          || DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
        throw new DigitalIntegrationBusinessException("ProfileID missing for viewCertificateHistory",
            ErrorCodes.MISSING_REQUIRED.getCode());
      }

      // Create cacheKey for the method
      String cacheKey =
          serviceMethodName + ":" + rewardServiceRequest.getPerson().getProfileID();

      try {
        //  check whether we have a cached response within the request scope
        DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
        boolean cacheEnabled = false;
        if (null != request) {
          String cacheRewardsCalls = request.getParameter(CACHE_REWARDS_CALLS);
          cacheEnabled = DigitalStringUtil.isNotBlank(cacheRewardsCalls) && "Y"
              .equalsIgnoreCase(cacheRewardsCalls);
        }
        RewardServiceResponse rewardServiceResponse = this
            .getCachedResponse(cacheKey, cacheEnabled);

        if (null == rewardServiceResponse) {
          rewardServiceResponse = new RewardServiceResponse();
          rewardServiceResponse.setSuccess(false);

          String profileId = "";
          if (rewardServiceRequest.getPerson() != null) {
            profileId = rewardServiceRequest.getPerson().getProfileID();
          }

          String endPoint = populateURL(
              getDefaultUri() + getSelectViewCertificatesHistoryResource(),
              PROFILE_ID, profileId);

          StringBuilder endPointBuilder = new StringBuilder();
          endPointBuilder.append(endPoint);

          long daysBack = getDaysBackForPointsHistory();

          Date startDate = rewardServiceRequest.getStartDate();
          Date endDate = rewardServiceRequest.getEndDate();

          if (null != startDate && null != endDate) {
            if (endDate.getTime() > startDate.getTime()) {
              long diffInMillies = Math.abs(endDate.getTime() - startDate.getTime());
              if (diffInMillies > 0) {
                daysBack = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
              }
            }
          }

          endPointBuilder.append("?");
          endPointBuilder.append("daysBack");
          endPointBuilder.append("=");
          endPointBuilder.append(daysBack);

          ServiceRequest serviceRequest = new ServiceRequest(
              getConnectionTimeout(serviceMethodName),
              getRequestTimeout(serviceMethodName), endPointBuilder.toString(),
              ServiceRequest.ServiceContentType.JSON,
              ServiceHttpMethod.GET, serviceMethodName);

          populateRequestHeaders(serviceRequest, serviceMethodName);

          HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

          handleViewCertificateHistoryResponse(cacheKey, cacheEnabled, rewardServiceResponse,
              response);
        }

        return rewardServiceResponse;

      } catch (Exception e) {
        hasException = true;
        throw new DigitalIntegrationBusinessException("VIP Rewards viewCertificateHistory Error",
            ErrorCodes.COMM_ERROR.getCode(), e);
      } finally {
        endPerformanceMonitorOperation(serviceMethodName, hasException);
      }

    } else {
      throw new DigitalIntegrationInactiveException(
          String
              .format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
          ErrorCodes.INACTIVE.getCode());
    }
  }

	/**
	 * @param rewardServiceResponse
	 * @param transactionsItems
	 */
	private void populateCertificateHistoryDataToRewardServiceResponse(
			RewardServiceResponse rewardServiceResponse,
			List<TransactionsItem> transactionsItems){

		List<PointsTansaction> pointsTansactionList = new ArrayList<>();
		int currentBalancePoint = 0, iteration = 0;
		for (TransactionsItem transactionsItem : transactionsItems) {
			PointsTansaction pointsTansaction = new PointsTansaction();
			List<Points> pointsLineList = new ArrayList<>();
			for (LinesItem linesItem : transactionsItem.getLines()) {
				Points point = new Points();
				point.setTotalPoints(linesItem.getPoints());
				if (DigitalStringUtil.isNotEmpty(linesItem.getDemandDate())) {
					try {
						point.setTransDate(vipRewardsFormatter.parse(linesItem.getDemandDate()));
					} catch (ParseException e) {
						logger.error("Failed to parse demand date on points history ", e);
					}
				}
				point.setStoreMallPlazaName(linesItem.getLocationName());
				point.setOrderNumber(linesItem.getDemandTransactionNumber());
				point.setTransID(linesItem.getDemandTransactionNumber());
				if(null != linesItem.getDemandBusinessUnitNumber()) {
				point.setTransBusinessUnitNumber(
						Integer.toString(linesItem.getDemandBusinessUnitNumber()));
				}
				point.setTransLocationNumber(linesItem.getDemandLocationNumber());
				point.setTransTypeCodeDesc(linesItem.getPointRuleDescription());
				point.setRunningTotal(linesItem.getTotalPoints());

				pointsLineList.add(point);
			}
			pointsTansaction.setTotalCurrentPoints(transactionsItem.getTotalCurrentPoints());
			if (!pointsLineList.isEmpty()) {
				pointsTansaction.setPoints(pointsLineList);
			}

			pointsTansactionList.add(pointsTansaction);

			if (currentBalancePoint == 0 && iteration == 0){
				currentBalancePoint = pointsTansaction.getTotalCurrentPoints();
			}
			iteration++;
		}

		if(!pointsTansactionList.isEmpty()) {
			rewardServiceResponse.setPointsTansaction(pointsTansactionList);
		}
		rewardServiceResponse.setCurrentBalancePoint(currentBalancePoint);
	}

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  @Override
  public RewardServiceResponse cancelLoyaltyCertificateReservation(
      RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException {

    String serviceMethodName = ServiceMethod.CANCEL_LOYALTY_CERTIFICATE_RESERVATION
        .getServiceMethodName();
    boolean hasException = false;

    if (doRunService(serviceMethodName)) {

      startPerformanceMonitorOperation(serviceMethodName);

      boolean isValid = true;

      if (DigitalStringUtil.isEmpty(rewardServiceRequest.getOrderNumber())) {
        isValid = false;
      }

      if (!isValid) {
        throw new DigitalIntegrationBusinessException(
            "OrderId missing for cancelLoyaltyCertificateReservation",
            ErrorCodes.MISSING_REQUIRED.getCode());
      }

      try {
        RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
        rewardServiceResponse.setSuccess(false);

        String endPoint = populateURL(getDefaultUri() + getCancelRewardsResource(),
            ORDER_ID, rewardServiceRequest.getOrderNumber());

        ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
            getRequestTimeout(serviceMethodName), endPoint,
            ServiceRequest.ServiceContentType.JSON,
            ServiceHttpMethod.POST, serviceMethodName);

        populateRequestHeaders(serviceRequest, serviceMethodName);

        HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

        handleRewardCertificatesResponse(rewardServiceResponse, response);

        return rewardServiceResponse;

      } catch (Exception e) {
        hasException = true;

        throw new DigitalIntegrationBusinessException(
            "VIP Rewards cancelLoyaltyCertificateReservation Error",
            ErrorCodes.COMM_ERROR.getCode(), e);
      } finally {
        endPerformanceMonitorOperation(serviceMethodName, hasException);
      }

    } else {
      throw new DigitalIntegrationInactiveException(
          String
              .format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
          ErrorCodes.INACTIVE.getCode());
    }

  }

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  @Override
  public RewardServiceResponse reserveLoyaltyCertificate(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException {

    String serviceMethodName = ServiceMethod.RESERVE_LOYALTY_CERTIFICATE.getServiceMethodName();
    boolean hasException = false;

    if (doRunService(serviceMethodName)) {

      startPerformanceMonitorOperation(serviceMethodName);

      if (rewardServiceRequest == null
          || DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
        throw new DigitalIntegrationBusinessException("ProfileId missing for reserveLoyaltyCertificate",
            ErrorCodes.MISSING_REQUIRED.getCode());
      }

      if ((rewardServiceRequest.getCertificates() == null ||
          rewardServiceRequest.getCertificates().isEmpty()) &&
          (rewardServiceRequest.getOffers() == null ||
              rewardServiceRequest.getOffers().isEmpty())) {
        throw new DigitalIntegrationBusinessException(
            "VIP Rewards Certificates/AuthOffers missing for reserveLoyaltyCertificate",
            ErrorCodes.MISSING_REQUIRED.getCode());
      }

      try {
        RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
        rewardServiceResponse.setSuccess(false);

        String profileId = "";
        if (rewardServiceRequest.getPerson() != null) {
          profileId = rewardServiceRequest.getPerson().getProfileID();
        }

        String endPoint = populateURL(getDefaultUri() + getReserveRewardsResource(), PROFILE_ID,
            profileId);

        Rewards rewardsItemRequest =
            populateRewardsFromRewardsServiceRequest(rewardServiceRequest);

        String bodyContent = serviceClient.getMarshalledStringFromObject(Rewards.class,
            rewardsItemRequest);

        ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
            getRequestTimeout(serviceMethodName), endPoint,
            ServiceRequest.ServiceContentType.JSON,
            ServiceHttpMethod.PUT, serviceMethodName);
        serviceRequest.setBody(bodyContent);

        populateRequestHeaders(serviceRequest, serviceMethodName);

        HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

        handleRewardCertificatesResponse(rewardServiceResponse, response);

        return rewardServiceResponse;
      } catch (Exception e) {
        hasException = true;

        throw new DigitalIntegrationBusinessException("VIP Rewards reserveLoyaltyCertificate Error",
            ErrorCodes.COMM_ERROR.getCode(), e);
      } finally {
        endPerformanceMonitorOperation(serviceMethodName, hasException);
      }

    } else {
      throw new DigitalIntegrationInactiveException(
          String
              .format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
          ErrorCodes.INACTIVE.getCode());
    }

  }

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse requestCertificate(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.REQUEST_CERTIFICATE.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			try {
				RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
				rewardServiceResponse.setSuccess(false);

				String profileId = "";
				if (rewardServiceRequest.getPerson() != null) {
					profileId = rewardServiceRequest.getPerson().getProfileID();
				}

				String denomValue = rewardServiceRequest.getCertDenomination();

				String endPoint = populateURL(getDefaultUri() + getRequestCertificateResource(),
						PROFILE_ID, profileId);

				ServiceRequest serviceRequest = new ServiceRequest(
						getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), populateURL(endPoint, DENOMINATION, denomValue),
						ServiceRequest.ServiceContentType.JSON,
						ServiceHttpMethod.POST, serviceMethodName);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
					rewardServiceResponse.setSuccess(true);
				} else {
					handleError(response, rewardServiceResponse);
				}
				return rewardServiceResponse;

			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("VIP Rewards requestCertificate Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}

	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse selectAvailableCertificatesByMemberID(
			RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.SELECT_CERTIFICATES_BY_MEMBERID.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null ||
					rewardServiceRequest.getPerson() == null ||
					DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getMemberID())) {
				throw new DigitalIntegrationBusinessException(
						"MemberId missing for selectAvailableCertificatesByMemberID",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			try {
				RewardServiceResponse btsRewardServiceResponse = new RewardServiceResponse();
				btsRewardServiceResponse.setSuccess(false);
				String loyaltyId = rewardServiceRequest.getPerson().getMemberID();

				ServiceRequest serviceRequest = new ServiceRequest(
						getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName),
						populateURL(getDefaultUri() + getSelectCertificatesByMemberIdResource(),
								LOYALTY_ID, loyaltyId), ServiceRequest.ServiceContentType.JSON,
						ServiceHttpMethod.GET, serviceMethodName);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
					Rewards rewardsResponse = serviceClient
							.getUnmarshalledObjectFromHttpServiceResponse(Rewards.class, response);
					btsRewardServiceResponse.setSuccess(true);
					populateRewardsDataToRewardServiceResponse(btsRewardServiceResponse, rewardsResponse);
				} else {
					handleError(response, btsRewardServiceResponse);
				}

				return btsRewardServiceResponse;

			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException(
						"VIP Rewards selectAvailableCertificatesByMemberID Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}

	}

	/**
	 * @param rewardServiceResponse
	 * @param shopForsItemList
	 * @param profileId
	 */
	private void populateShopForDataToRewardServiceResponse(
			RewardServiceResponse rewardServiceResponse,
			List<ShopForsItem> shopForsItemList, String profileId) {
		if (shopForsItemList != null) {
			List<Shopfor> shopforDomainList = new ArrayList<>();
			for (ShopForsItem shopforAPIDomain : shopForsItemList) {
				Shopfor shopfor = new Shopfor();
				if (null != shopforAPIDomain.getDataDate()) {
					try {
						shopfor.setDateCreated(vipRewardsFormatter.parse(shopforAPIDomain.getDataDate()));
					} catch (ParseException e) {
						logger.error("Failed to parse Shop For Date Created from Rewards Service", e);
					}
				}
				if (null != shopforAPIDomain.getShopForId()) {
					shopfor.setRewardsShopforID(Long.toString(shopforAPIDomain.getShopForId()));
				}
				if (null != shopforAPIDomain.getShopFor()) {
					ShopFor shopForAPI = shopforAPIDomain.getShopFor();

					if (DigitalStringUtil.isNotEmpty(shopForAPI.getName())) {
						shopfor.setShopforName(shopForAPI.getName());
					}
					if (DigitalStringUtil.isNotEmpty(shopForAPI.getRelationship())) {
						shopfor.setRelationship(shopForAPI.getRelationship());
					}
					if (shopForAPI.getBirthday() != null) {
						shopfor.setBirthDay(shopForAPI.getBirthday().toString());
					}
					if (shopForAPI.getBirthMonth() != null) {
						shopfor.setBirthMonth(shopForAPI.getBirthMonth().toString());
					}
					if (shopForAPI.getProduct() != null) {
						Product productFilterAPI = shopForAPI.getProduct();
						if (DigitalStringUtil.isNotEmpty(productFilterAPI.getType())) {
							shopfor.setWebType(productFilterAPI.getType());
						}
						if (DigitalStringUtil.isNotEmpty(productFilterAPI.getGender())) {
							shopfor.setGender(productFilterAPI.getGender());
						}
						if (DigitalStringUtil.isNotEmpty(productFilterAPI.getGenderSubType())) {
							shopfor.setSizeGroup(productFilterAPI.getGenderSubType());
						}
						if (productFilterAPI.getSize() != null) {
							shopfor.setSizes(productFilterAPI.getSize());
						}
						if (productFilterAPI.getWidth() != null) {
							shopfor.setWidths(productFilterAPI.getWidth());
						}
					}
				}

				shopfor.setProfileId(profileId);
				shopfor.setActive(true);

				shopforDomainList.add(shopfor);
			}
			rewardServiceResponse.setShopfors(shopforDomainList);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param birthdayGifts
	 * @param profileId
	 */
	private void populateBirthdayGiftsDataToRewardServiceResponse(
			RewardServiceResponse rewardServiceResponse,
			List<BirthdayGiftItem> birthdayGifts, String profileId) {
		if (birthdayGifts != null) {
			List<BirthdayGift> birthdayGiftList = new ArrayList<>();
			for (BirthdayGiftItem birthdayGiftItem : birthdayGifts) {
				BirthdayGift gift = new BirthdayGift();
				if (null != birthdayGiftItem.getGiftRequestId()) {
					gift.setBirthdayGiftId(Long.toString(birthdayGiftItem.getGiftRequestId()));
				}
				if (DigitalStringUtil.isNotEmpty(birthdayGiftItem.getGiftDeliveryDate())) {
					gift.setDeliveryDate(birthdayGiftItem.getGiftDeliveryDate());
				}
				if (DigitalStringUtil.isNotEmpty(birthdayGiftItem.getRecipientEmail())) {
					gift.setEmail(birthdayGiftItem.getRecipientEmail());
				}
				if (DigitalStringUtil.isNotEmpty(birthdayGiftItem.getRecipientGivenName())) {
					gift.setFriendName(birthdayGiftItem.getRecipientGivenName());
				}
				if (null != birthdayGiftItem.getRecipientBirthday()) {
					gift.setDayOfBirth(Integer.toString(birthdayGiftItem.getRecipientBirthday()));
				}
				if (null != birthdayGiftItem.getRecipientBirthMonth()) {
					gift.setMonthOfBirth(Integer.toString(birthdayGiftItem.getRecipientBirthMonth()));
				}
				if (DigitalStringUtil.isNotEmpty(birthdayGiftItem.getGiftMessageText())) {
					gift.setMessage(birthdayGiftItem.getGiftMessageText());
				}
				if (DigitalStringUtil.isNotEmpty(birthdayGiftItem.getGiftRequestDate())) {
					try {
						gift.setAddedDate(vipRewardsFormatter.parse(birthdayGiftItem.getGiftRequestDate()));
					} catch (ParseException e) {
						logger.error("Failed to parse reward bDay gift added date", e);
					}
				}
				String locked = (birthdayGiftItem.isProcessedIndicator()) ? "Y" : "N";
				gift.setLocked(locked);
				gift.setProfileId(profileId);

				birthdayGiftList.add(gift);
			}
			rewardServiceResponse.setBirthdayGifts(birthdayGiftList);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param charityItems
	 */
	private void populateCharitiesDataToRewardServiceResponse(
			RewardServiceResponse rewardServiceResponse,
			List<CharityItem> charityItems) {
		if (charityItems != null) {
			List<Charity> charityList = new ArrayList<>();
			for (CharityItem charityItem : charityItems) {
				Charity charity = new Charity();
				charity.setId(charityItem.getCharityId());
				charity.setName(charityItem.getCharityName());
				charity.setDescription(charityItem.getDescription());
				charityList.add(charity);
			}
			rewardServiceResponse.setCharities(charityList);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param rewards
	 */
	private void populateRewardsDataToRewardServiceResponse(
			RewardServiceResponse rewardServiceResponse,
			Rewards rewards) {

		if (rewards.getRewards() != null) {
			List<Certificate> certificates = new ArrayList<>();

			for (RewardsItem rewardsItem : rewards.getRewards()) {
				Certificate certificate = new Certificate();
				certificate.setCanReissue(flag(rewardsItem.isCanReissue()));
				if (null != rewardsItem.getId()) {
					certificate.setCertNbr(Long.toString(rewardsItem.getId()));
				}
				String rewardStatus = rewardsItem.getStatus();
				if (DigitalStringUtil.isNotBlank(rewardStatus) && "AVAIL".equalsIgnoreCase(rewardStatus)) {
					certificate.setCertStatus("INITIAL");
				} else {
					certificate.setCertStatus(rewardStatus);
				}
				certificate.setCertType("REW");
				if (null != rewardsItem.getValue()) {
					certificate.setCertValue(Double.toString(rewardsItem.getValue()));
				}
				if (DigitalStringUtil.isNotBlank(rewardsItem.getDiscountCode())) {
					certificate.setDiscountCode(rewardsItem.getDiscountCode());
				}
				if (DigitalStringUtil.isNotBlank(rewardsItem.getExpirationCode())) {
					certificate.setExpCode(rewardsItem.getExpirationCode());
				}
				if (DigitalStringUtil.isNotBlank(rewardsItem.getExpirationDate())) {
					certificate.setExpDate(rewardsItem.getExpirationDate());
				}
				if (DigitalStringUtil.isNotBlank(rewardsItem.getIssueDate())) {
					certificate.setIssueDate(rewardsItem.getIssueDate());
				}
				certificate.setVoidFlag(flag(rewardsItem.isVoidFlag()));
				if (DigitalStringUtil.isNotBlank(rewardsItem.getCustomerFacingName())) {
					certificate.setOfferDisplayName(rewardsItem.getCustomerFacingName());
				}
				if (null != rewardsItem.getBarcode()) {
					certificate.setMarkdownCode(Long.toString(rewardsItem.getBarcode()));
				}
				certificates.add(certificate);
			}


			for (AuthorizedOffersItem offersItem : rewards.getAuthorizedOffers()) {
				Certificate certificate = new Certificate();
				certificate.setCanReissue(flag(offersItem.isCanReissue()));
				if (null != offersItem.getId()) {
					certificate.setCertNbr(Long.toString(offersItem.getId()));
				}
				String rewardStatus = offersItem.getStatus();
				if (DigitalStringUtil.isNotBlank(rewardStatus) &&
						("AVAIL".equalsIgnoreCase(rewardStatus) ||
								"AVIL".equalsIgnoreCase(rewardStatus))) {
					certificate.setCertStatus("INITIAL");
				} else {
					certificate.setCertStatus(rewardStatus);
				}
				certificate.setCertType("OFFER");
				if (null != offersItem.getValue()) {
					certificate.setCertValue(Double.toString(offersItem.getValue()));
				}
				if (DigitalStringUtil.isNotBlank(offersItem.getDiscountCode())) {
					certificate.setDiscountCode(offersItem.getDiscountCode());
				}
				if (DigitalStringUtil.isNotBlank(offersItem.getExpirationCode())) {
					certificate.setExpCode(offersItem.getExpirationCode());
				}
				if (DigitalStringUtil.isNotBlank(offersItem.getOfferDetailsPageName())) {
					certificate.setOfferDetailsPageName(offersItem.getOfferDetailsPageName());
				}
				if (DigitalStringUtil.isNotBlank(offersItem.getExpirationDate())) {
					certificate.setExpDate(offersItem.getExpirationDate());
				}
				if (DigitalStringUtil.isNotBlank(offersItem.getIssueDate())) {
					certificate.setIssueDate(offersItem.getIssueDate());
				}
				certificate.setVoidFlag(flag(offersItem.isVoidFlag()));

				if (DigitalStringUtil.isNotBlank(offersItem.getCustomerFacingName())) {
					certificate.setOfferDisplayName(offersItem.getCustomerFacingName());
				}
				if (null != offersItem.getBarcode()) {
					certificate.setMarkdownCode(Long.toString(offersItem.getBarcode()));
				}

				certificates.add(certificate);
			}

			rewardServiceResponse.setCertificates(certificates);
			rewardServiceResponse.setTotalCertValue(rewards.getTotalRewardsValue());
		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse selectAvailableCertificatesByProfileID(
			RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.SELECT_CERTIFICATES_BY_PROFILEID
				.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			// Do null check for profileId as we are sending blank if few flows, 
			// does not make sense to send blank and  rewards tell us profileId is blank
			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException(
						"ProfileId missing for selectAvailableCertificatesByProfileID",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}
			// get cacheKey for the method
			String cacheKey = serviceMethodName + ":"
					+ rewardServiceRequest.getPerson().getProfileID();

			try {
				//  check whether we have a cached response within the request scope
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				boolean cacheEnabled = false;
				if (null != request) {
					String cacheRewardsCalls = request.getParameter(CACHE_REWARDS_CALLS);
					cacheEnabled = DigitalStringUtil.isNotBlank(cacheRewardsCalls) && "Y"
              .equalsIgnoreCase(cacheRewardsCalls);
				}
				RewardServiceResponse btsRewardServiceResponse = this
						.getCachedResponse(cacheKey, cacheEnabled);

				if (null == btsRewardServiceResponse) {

					btsRewardServiceResponse = new RewardServiceResponse();
					btsRewardServiceResponse.setSuccess(false);
					String profileId = "";
					if (rewardServiceRequest.getPerson() != null) {
						profileId = rewardServiceRequest.getPerson().getProfileID();
					}

					ServiceRequest serviceRequest = new ServiceRequest(
							getConnectionTimeout(serviceMethodName),
							getRequestTimeout(serviceMethodName),
							populateURL(getDefaultUri() + getSelectCertificatesByProfileIdResource(),
									PROFILE_ID, profileId), ServiceRequest.ServiceContentType.JSON,
							ServiceHttpMethod.GET, serviceMethodName);

					populateRequestHeaders(serviceRequest, serviceMethodName);

					HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

					if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
						Rewards rewardsResponse = serviceClient
								.getUnmarshalledObjectFromHttpServiceResponse(Rewards.class, response);
						btsRewardServiceResponse.setSuccess(true);
						populateRewardsDataToRewardServiceResponse(btsRewardServiceResponse, rewardsResponse);
						if (cacheEnabled) {
							this.getRewardsCache().putRewardsData(cacheKey, btsRewardServiceResponse);
						}
					} else {
						handleError(response, btsRewardServiceResponse);
					}
				}
				return btsRewardServiceResponse;
			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException(
						"VIP Rewards selectAvailableCertificatesByProfileID Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());

		}

	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse captureEmailFooter(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.CAPTURE_EMAIL_FOOTER.getServiceMethodName();

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null || rewardServiceRequest.getContact() == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getContact().getEmail())) {
				throw new DigitalIntegrationBusinessException("Email is missing for captureEmailFooter",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

      return processEmailSubscriber(rewardServiceRequest, serviceMethodName, true);

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());

		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse orderHistory(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {
		String serviceMethodName = ServiceMethod.ORDER_HISTORY.getServiceMethodName();
		boolean hasException = false;
		//final SimpleDateFormat orderHistoryFormatter = new SimpleDateFormat("yyyy/MM/dd");
		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			try {
				RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
				rewardServiceResponse.setSuccess(false);

				if (rewardServiceRequest == null
						|| DigitalStringUtil.isBlank(rewardServiceRequest.getHistoryProfileId())) {
					throw new DigitalIntegrationBusinessException("ProfileId missing for orderHistory",
							ErrorCodes.MISSING_REQUIRED.getCode());
				}

				if (null == rewardServiceRequest.getHistoryStartDate()) {
					throw new DigitalIntegrationBusinessException("StartDate missing for orderHistory",
							ErrorCodes.MISSING_REQUIRED.getCode());
				}

				if (null == rewardServiceRequest.getHistoryEndDate()) {
					throw new DigitalIntegrationBusinessException("EndDate missing for orderHistory",
							ErrorCodes.MISSING_REQUIRED.getCode());
				}

				String profileId = rewardServiceRequest.getHistoryProfileId();

				String startDate = vipRewardsFormatter.format(rewardServiceRequest.getHistoryStartDate());
				String endDate = vipRewardsFormatter.format(rewardServiceRequest.getHistoryEndDate());

				String initEndPoint = populateURL(getDefaultUri() +
								getRetrieveOrderHistoryResource(), PROFILE_ID, profileId);

				String endPoint = initEndPoint + "?startDate=" + startDate +"&endDate=" + endDate;

				ServiceRequest serviceRequest = new ServiceRequest(
						getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), endPoint,
						ServiceContentType.JSON, ServiceHttpMethod.GET, serviceMethodName);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				handleRetrieveOrderHistoryResponse(rewardServiceResponse, response);

				return rewardServiceResponse;

			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("VIP Rewards orderHistory Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * Add ShopFor item for this user
	 *
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse addShopFor(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.ADD_SHOPFOR.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException("ProfileId missing for AddShopFor",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			try {
				RewardServiceResponse btsRewardServiceResponse = new RewardServiceResponse();
				btsRewardServiceResponse.setSuccess(false);

				ShopFor addShopForRequest =
						populateShopforFromRewardsServiceRequest(rewardServiceRequest);

				String bodyContent = serviceClient.getMarshalledStringFromObject(ShopFor.class,
						addShopForRequest);

				String profileId = "";

				if (rewardServiceRequest.getPerson() != null) {
					profileId = rewardServiceRequest.getPerson().getProfileID();
				}

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), populateURL(getDefaultUri() +
						getAddShopForResource(), PROFILE_ID, profileId),
						ServiceRequest.ServiceContentType.JSON,
						ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setBody(bodyContent);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				handleAddOrUpdateShopforResponse(btsRewardServiceResponse, response, profileId);

				return btsRewardServiceResponse;

			} catch (DigitalIntegrationBusinessException e) {
				hasException = true;
				throw e;
			} catch (Exception e) {
				hasException = true;
				throw new DigitalIntegrationBusinessException("VIP Rewards shopFor Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());

		}
	}

	/**
	 * Add Birthday Offer for a friend
	 *
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse addBirthdayGift(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.ADD_BIRTHDAY_GIFT.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException("ProfileId missing for addBirthdayGift",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			try {
				RewardServiceResponse btsRewardServiceResponse = new RewardServiceResponse();
				btsRewardServiceResponse.setSuccess(false);

				BirthdayGiftItem addBirthdayGiftRequest =
						populateBirthdayGiftFromRewardsServiceRequest(rewardServiceRequest);

				String bodyContent = serviceClient.getMarshalledStringFromObject(BirthdayGiftItem.class,
						addBirthdayGiftRequest);

				String profileId = "";

				if (rewardServiceRequest.getPerson() != null) {
					profileId = rewardServiceRequest.getPerson().getProfileID();
				}

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), populateURL(getDefaultUri() +
						getAddBirthdayGiftResource(), PROFILE_ID, profileId),
						ServiceRequest.ServiceContentType.JSON,
						ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setBody(bodyContent);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				handleAddOrUpdateBirthdayGiftResponse(btsRewardServiceResponse, response, profileId);

				return btsRewardServiceResponse;

			} catch (DigitalIntegrationBusinessException e) {
				hasException = true;
				throw e;
			} catch (Exception e) {
				hasException = true;
				throw new DigitalIntegrationBusinessException("VIP Rewards addBirthdayGift Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());

		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param response
	 * @param profileId
	 * @throws DigitalIntegrationException
	 */
	private void handleAddOrUpdateShopforResponse(RewardServiceResponse rewardServiceResponse,
													   HttpServiceResponse response, String profileId)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			ShopForsItem shopForsItemResponse = serviceClient
					.getUnmarshalledObjectFromHttpServiceResponse(ShopForsItem.class, response);
			rewardServiceResponse.setSuccess(true);
			List<ShopForsItem> shopForsItems = new ArrayList<>();
			shopForsItems.add(shopForsItemResponse);
			populateShopForDataToRewardServiceResponse(rewardServiceResponse, shopForsItems, profileId);
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param response
	 * @param profileId
	 * @throws DigitalIntegrationException
	 */
	private void handleAddOrUpdateBirthdayGiftResponse(RewardServiceResponse rewardServiceResponse,
			HttpServiceResponse response, String profileId)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			BirthdayGiftItem birthdayGiftItemResponse = serviceClient
					.getUnmarshalledObjectFromHttpServiceResponse(BirthdayGiftItem.class, response);
			rewardServiceResponse.setSuccess(true);
			List<BirthdayGiftItem> birthdayGiftItems = new ArrayList<>();
			birthdayGiftItems.add(birthdayGiftItemResponse);
			populateBirthdayGiftsDataToRewardServiceResponse(rewardServiceResponse,
					birthdayGiftItems, profileId);
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param response
	 * @param profileId
	 * @throws DigitalIntegrationException
	 */
	private void handleRetrieveShopForResponse(RewardServiceResponse rewardServiceResponse,
													HttpServiceResponse response, String profileId)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			try {
				String responseString = response.getEntityString();

				serviceClient.logServiceResponse(responseString);

				TypeReference<List<ShopForsItem>> typeReference =
						new TypeReference<List<ShopForsItem>>() {
						};

				List<ShopForsItem> shopForsItems = serviceClient.getObjectMapper()
						.readValue(response.getEntityString(), typeReference);

				rewardServiceResponse.setSuccess(true);
				populateShopForDataToRewardServiceResponse(rewardServiceResponse,
						shopForsItems, profileId);

			} catch (Exception e) {
				logger.error("Error marshalling response ", e);
				throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error",
						ErrorCodes.XFORM.getCode(), e);
			}
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param response
	 * @param profileId
	 * @throws DigitalIntegrationException
	 */
	private void handleRetrieveBirthdayGiftResponse(RewardServiceResponse rewardServiceResponse,
			HttpServiceResponse response, String profileId)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			try {
				String responseString = response.getEntityString();

				serviceClient.logServiceResponse(responseString);

				TypeReference<List<BirthdayGiftItem>> typeReference =
						new TypeReference<List<BirthdayGiftItem>>() {
						};

				List<BirthdayGiftItem> birthdayGiftItems = serviceClient.getObjectMapper()
						.readValue(response.getEntityString(), typeReference);

				rewardServiceResponse.setSuccess(true);
				populateBirthdayGiftsDataToRewardServiceResponse(rewardServiceResponse,
						birthdayGiftItems, profileId);

			} catch (Exception e) {
				logger.error("Error marshalling response ", e);
				throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error",
						ErrorCodes.XFORM.getCode(), e);
			}
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param response
	 * @throws DigitalIntegrationException
	 */
	private void handleRetrieveOrderHistoryResponse(RewardServiceResponse rewardServiceResponse,
			HttpServiceResponse response)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			try {

				OrderHistory orderHistoryResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(OrderHistory.class, response);
				rewardServiceResponse.setSuccess(true);
				populateOrderHistoryDataToRewardServiceResponse(rewardServiceResponse,
						orderHistoryResponse.getOrders());

			} catch (Exception e) {
				logger.error("Error marshalling response ", e);
				throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error",
						ErrorCodes.XFORM.getCode(), e);
			}
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param orderItems
	 */
	private void populateOrderHistoryDataToRewardServiceResponse(
			RewardServiceResponse rewardServiceResponse,
			List<OrdersItem> orderItems) {
		if (orderItems != null) {

			rewardServiceResponse.setSuccess(true);

			for (OrdersItem orderItem : orderItems) {
				Order order = new Order();
				order.setCurrencyCode(orderItem.getCurrencyCode());
				order.setLocaleCode(orderItem.getLocaleCode());
				order.setOnlineSiteName(orderItem.getOnlineSiteId());
				//order.setOrderChannelCode(orderItem.getChannelCode());
				if(DigitalStringUtil.isNotEmpty(orderItem.getOrderDate())) {
					try {
						DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
						String orderDate = df.format(vipRewardsFormatter.parse(orderItem.getOrderDate()));
						order.setOrderDate(orderDate);
					} catch (ParseException e) {
						logger.error("Failed to parse order date from rewards order history service response");
				}
				}
				order.setOrderNumber(orderItem.getOrderNumber());
				if(null != orderItem.getOrderTotal()) {
				order.setTotalOrderAmount(orderItem.getOrderTotal());
				}

				List<ItemsItem> items = orderItem.getItems();

				for (ItemsItem item : items) {
					OrderLine orderLine = new OrderLine();
					if (null != item.getFulfillmentLocationNumber()) {
						orderLine
								.setFulfillmentLocationNumber(item.getFulfillmentLocationNumber());
					}
					orderLine.setFulfillmentTypeCode(item.getFulfillmentTypecode());
					orderLine.setItemFulfillmentStatusCode(item.getFulfillmentStatusCode());
					if(DigitalStringUtil.isNotEmpty(item.getFulfillmentStatusDate())) {
						try {
							DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
							String itemSatusDate = df.format(vipRewardsFormatter.parse(item.getFulfillmentStatusDate()));
							orderLine.setItemFulfillmentStatusDate(itemSatusDate);
						} catch (ParseException e) {
							logger.error("Failed to parse item status date from rewards order history service response");
						}
					}
					if (null != item.getQuantity()) {
						orderLine.setItemQuantity(Integer.toString(item.getQuantity()));
					}
					orderLine.setOrderDate(item.getOrderDate());
					if (null != item.getOrderLineItemNumber()) {
						orderLine.setOrderItemLineNumber(Long.toString(item.getOrderLineItemNumber()));
					}
					if (null != item.getOrderItemSequenceNumber()) {
						orderLine
								.setOrderItemLineSequenceNumber(
										Integer.toString(item.getOrderItemSequenceNumber()));
					}
					if (null != item.getOrderLocationNumber()) {
						orderLine.setOrderLocationID(item.getOrderLocationNumber());
					}
					orderLine.setOrderNumber(item.getOrderNumber());
					if (null != item.getPickupLocationNumber()) {
						orderLine.setPickupLocationNumber(Integer.toString(item.getPickupLocationNumber()));
					}
					if(null != item.getRetailPriceAmount()) {
					orderLine.setRetailPriceAmount(item.getRetailPriceAmount());
					}
					if (null != item.getReturnLocationNumber()) {
						orderLine
								.setReturnLocationNumber(Integer.toString(item.getReturnLocationNumber()));
					}
					orderLine.setShippingTrackingNumber(item.getTrackingNumber());
					orderLine.setShipToFirstName(item.getShipToFirstName());
					orderLine.setShipToLastName(item.getShipToLastName());
					orderLine.setSkuID(item.getSku());

					order.getOrderLine().add(orderLine);
				}

				rewardServiceResponse.getOrders().add(order);
			}
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param response
	 * @throws DigitalIntegrationException
	 */
	private void handleRewardCertificatesResponse(RewardServiceResponse rewardServiceResponse,
			HttpServiceResponse response)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			rewardServiceResponse.setSuccess(true);
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param response
	 * @throws DigitalIntegrationException
	 */
	private void handleRetrieveCharitiesResponse(RewardServiceResponse rewardServiceResponse,
			HttpServiceResponse response)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			try {
				String responseString = response.getEntityString();

				serviceClient.logServiceResponse(responseString);

				TypeReference<List<CharityItem>> typeReference =
						new TypeReference<List<CharityItem>>() {
						};

				List<CharityItem> charityItems = serviceClient.getObjectMapper()
						.readValue(response.getEntityString(), typeReference);

				rewardServiceResponse.setSuccess(true);

				populateCharitiesDataToRewardServiceResponse(rewardServiceResponse,
						charityItems);

			} catch (Exception e) {
				logger.error("Error marshalling response ", e);
				throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error",
						ErrorCodes.XFORM.getCode(), e);
			}
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param response
	 * @throws DigitalIntegrationException
	 */
	private void handleRetrieveOffersResponse(RewardServiceResponse rewardServiceResponse,
			HttpServiceResponse response)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			try {
				String responseString = response.getEntityString();

				serviceClient.logServiceResponse(responseString);

				TypeReference<List<OffersItem>> typeReference =
						new TypeReference<List<OffersItem>>() {
						};

				List<OffersItem> offersItems = serviceClient.getObjectMapper()
						.readValue(response.getEntityString(), typeReference);

				rewardServiceResponse.setSuccess(true);

				populateOffersDataToBtsRewardServiceResponse(rewardServiceResponse,
						offersItems);

			} catch (Exception e) {
				logger.error("Error marshalling response ", e);
				throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error",
						ErrorCodes.XFORM.getCode(), e);
			}
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse updateShopFor(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.UPDATE_SHOPFOR.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException("ProfileId missing for updateShopFor",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			try {
				RewardServiceResponse btsRewardServiceResponse = new RewardServiceResponse();
				btsRewardServiceResponse.setSuccess(false);

				ShopFor shopForsRequest =
						populateShopforFromRewardsServiceRequest(rewardServiceRequest);

				String profileId = "";

				if (rewardServiceRequest.getPerson() != null) {
					profileId = rewardServiceRequest.getPerson().getProfileID();
				}

				String rewardsShopforID = "";
				if (rewardServiceRequest.getShopfor() != null && rewardServiceRequest.getShopfor().getRewardsShopforID() != null) {
					rewardsShopforID = rewardServiceRequest.getShopfor().getRewardsShopforID();
				} else {
					logger.error("Rewards ShopforID is missing for the Shopfor Update");
				}


				String tmpEndPoint = populateURL(getDefaultUri() +
						getUpdateShopForResource(), PROFILE_ID, profileId);

				String endPoint = populateURL(tmpEndPoint, SHOPFOR_ID, rewardsShopforID);

				String bodyContent = serviceClient.getMarshalledStringFromObject(ShopForsItem.class,
						shopForsRequest);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), endPoint,
						ServiceRequest.ServiceContentType.JSON,
						ServiceHttpMethod.PUT, serviceMethodName);
				serviceRequest.setBody(bodyContent);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				handleAddOrUpdateShopforResponse(btsRewardServiceResponse, response, profileId);

				return btsRewardServiceResponse;

			} catch (DigitalIntegrationBusinessException e) {
				hasException = true;
				throw e;
			} catch (Exception e) {
				hasException = true;
				throw new DigitalIntegrationBusinessException("VIP Rewards updateShopFor Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse updateBirthdayGift(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.UPDATE_BIRTHDAY_GIFT.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException("ProfileId missing for updateBirthdayGift",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			try {
				RewardServiceResponse btsRewardServiceResponse = new RewardServiceResponse();
				btsRewardServiceResponse.setSuccess(false);

				BirthdayGiftItem updateBirthdayGiftRequest =
						populateBirthdayGiftFromRewardsServiceRequest(rewardServiceRequest);

				String profileId = "";

				if (rewardServiceRequest.getPerson() != null) {
					profileId = rewardServiceRequest.getPerson().getProfileID();
				}

				String giftId = Long.toString(updateBirthdayGiftRequest.getGiftRequestId());

				String tmpEndPoint = populateURL(getDefaultUri() +
						getUpdateBirthdayGiftResource(), PROFILE_ID, profileId);

				String endPoint = populateURL(tmpEndPoint, BIRTHDAY_GIFT_ID, giftId);

				String bodyContent = serviceClient.getMarshalledStringFromObject(BirthdayGiftItem.class,
						updateBirthdayGiftRequest);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), endPoint,
						ServiceRequest.ServiceContentType.JSON,
						ServiceHttpMethod.PUT, serviceMethodName);
				serviceRequest.setBody(bodyContent);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				handleAddOrUpdateBirthdayGiftResponse(btsRewardServiceResponse, response, profileId);

				return btsRewardServiceResponse;

			} catch (DigitalIntegrationBusinessException e) {
				hasException = true;
				throw e;
			} catch (Exception e) {
				hasException = true;
				throw new DigitalIntegrationBusinessException("VIP Rewards updateBirthdayGift Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse retrieveShopFors(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {
		String serviceMethodName = ServiceMethod.RETRIEVE_SHOPFORS.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			// Do null check for profileId as we are sending blank if few flows,
			// does not make sense to send blank and  rewards tell us profileId is blank
			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException(
						"ProfileId missing for retrieveShopFors",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}
			// get cacheKey for the method
			String cacheKey =
					serviceMethodName + ":" + rewardServiceRequest.getPerson().getProfileID();

			try {
				//  check whether we have a cached response within the request scope
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				boolean cacheEnabled = false;
				if (null != request) {
					String cacheRewardsCalls = request.getParameter(CACHE_REWARDS_CALLS);
					cacheEnabled = DigitalStringUtil.isNotBlank(cacheRewardsCalls)
              && "Y".equalsIgnoreCase(cacheRewardsCalls);
				}
				RewardServiceResponse btsRewardServiceResponse = this
						.getCachedResponse(cacheKey, cacheEnabled);

				if (null == btsRewardServiceResponse) {
					btsRewardServiceResponse = new RewardServiceResponse();
					btsRewardServiceResponse.setSuccess(false);

					String profileId = "";

					if (rewardServiceRequest.getPerson() != null) {
						profileId = rewardServiceRequest.getPerson().getProfileID();
					}

					ServiceRequest serviceRequest = new ServiceRequest(
							getConnectionTimeout(serviceMethodName),
							getRequestTimeout(serviceMethodName), populateURL(getDefaultUri() +
							getRetrieveShopForsResource(), PROFILE_ID, profileId),
							ServiceContentType.JSON, ServiceHttpMethod.GET, serviceMethodName);

					populateRequestHeaders(serviceRequest, serviceMethodName);

					HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

					handleRetrieveShopForResponse(btsRewardServiceResponse, response, profileId);

					if (cacheEnabled) {
						this.getRewardsCache().putRewardsData(cacheKey, btsRewardServiceResponse);
					}
				}
				return btsRewardServiceResponse;
			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("VIP Rewards retrieveShopFors Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse deleteShopFor(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {
		String serviceMethodName = ServiceMethod.DELETE_SHOPFOR.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException("ProfileId missing for deleteShopFor",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			try {
				RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
				rewardServiceResponse.setSuccess(false);

				String profileId = "";

				if (rewardServiceRequest.getPerson() != null) {
					profileId = rewardServiceRequest.getPerson().getProfileID();
				}

				String rewardsShopforID;
				if (rewardServiceRequest.getShopfor() != null
						&& rewardServiceRequest.getShopfor().getRewardsShopforID() != null) {
					rewardsShopforID = rewardServiceRequest.getShopfor().getRewardsShopforID();
				} else {
					throw new DigitalIntegrationBusinessException(
							"Rewards ShopforID is missing for the Shopfor Delete",
							ErrorCodes.MISSING_REQUIRED.getCode());
				}

				String tmpEndPoint = populateURL(getDefaultUri() +
						getDeleteShopForResource(), PROFILE_ID, profileId);

				String endPoint = populateURL(tmpEndPoint, SHOPFOR_ID, rewardsShopforID);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), endPoint,
						ServiceRequest.ServiceContentType.JSON,
						ServiceHttpMethod.DELETE, serviceMethodName);

				populateRequestHeaders(serviceRequest, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
					rewardServiceResponse.setSuccess(true);
				} else {
					handleError(response, rewardServiceResponse);
				}

				return rewardServiceResponse;

			} catch (DigitalIntegrationBusinessException e) {
				hasException = true;
				throw e;
			} catch (Exception e) {
				hasException = true;
				throw new DigitalIntegrationBusinessException("VIP Rewards deleteShopFor Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse retrieveBirthdayGifts(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {
		String serviceMethodName = ServiceMethod.RETRIEVE_BIRTHDAY_GIFTS.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			// Do null check for profileId as we are sending blank if few flows,
			// does not make sense to send blank and  rewards tell us profileId is blank
			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException(
						"ProfileId missing for retrieveBirthdayGifts",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}
			// get cacheKey for the method
			String cacheKey =
					serviceMethodName + ":" + rewardServiceRequest.getPerson().getProfileID();

			try {
				//  check whether we have a cached response within the request scope
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				boolean cacheEnabled = false;
				if (null != request) {
					String cacheRewardsCalls = request.getParameter(CACHE_REWARDS_CALLS);
					cacheEnabled = DigitalStringUtil.isNotBlank(cacheRewardsCalls)
              && "Y".equalsIgnoreCase(cacheRewardsCalls);
				}
				RewardServiceResponse btsRewardServiceResponse = this
						.getCachedResponse(cacheKey, cacheEnabled);

				if (null == btsRewardServiceResponse) {
					btsRewardServiceResponse = new RewardServiceResponse();
					btsRewardServiceResponse.setSuccess(false);

					String profileId = "";

					if (rewardServiceRequest.getPerson() != null) {
						profileId = rewardServiceRequest.getPerson().getProfileID();
					}

					ServiceRequest serviceRequest = new ServiceRequest(
							getConnectionTimeout(serviceMethodName),
							getRequestTimeout(serviceMethodName), populateURL(getDefaultUri() +
							getRetrieveBirthdayGiftsResource(), PROFILE_ID, profileId),
							ServiceContentType.JSON, ServiceHttpMethod.GET, serviceMethodName);

					populateRequestHeaders(serviceRequest, serviceMethodName);

					HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

					handleRetrieveBirthdayGiftResponse(btsRewardServiceResponse, response, profileId);

					if (cacheEnabled) {
						this.getRewardsCache().putRewardsData(cacheKey, btsRewardServiceResponse);
					}
				}
				return btsRewardServiceResponse;
			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("VIP Rewards retrieveBirthdayGifts Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse retrieveCharities(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {
		String serviceMethodName = ServiceMethod.RETRIEVE_CHARITIES.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException(
						"ProfileId missing for retrieveBirthdayGifts",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			// get cacheKey for the method
			String cacheKey =
					serviceMethodName + ":" + rewardServiceRequest.getPerson().getProfileID();

			try {
				//  check whether we have a cached response within the request scope
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				boolean cacheEnabled = false;
				if (null != request) {
					String cacheRewardsCalls = request.getParameter(CACHE_REWARDS_CALLS);
					cacheEnabled = DigitalStringUtil.isNotBlank(cacheRewardsCalls)
              && "Y".equalsIgnoreCase(cacheRewardsCalls);
				}
				RewardServiceResponse btsRewardServiceResponse = this
						.getCachedResponse(cacheKey, cacheEnabled);

				if (null == btsRewardServiceResponse) {
					btsRewardServiceResponse = new RewardServiceResponse();
					btsRewardServiceResponse.setSuccess(false);

					ServiceRequest serviceRequest = new ServiceRequest(
							getConnectionTimeout(serviceMethodName),
							getRequestTimeout(serviceMethodName), getDefaultUri() +
							getCharitiesResource(),
							ServiceContentType.JSON, ServiceHttpMethod.GET, serviceMethodName);

					populateRequestHeaders(serviceRequest, serviceMethodName);

					HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

					handleRetrieveCharitiesResponse(btsRewardServiceResponse, response);

					if (cacheEnabled) {
						this.getRewardsCache().putRewardsData(cacheKey, btsRewardServiceResponse);
					}
				}
				return btsRewardServiceResponse;
			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("VIP Rewards retrieveBirthdayGifts Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());

		}
	}

  /**
   * @param rewardServiceRequest
   * @return RewardServiceResponse
   * @throws DigitalIntegrationException
   */
  @Override
  public RewardServiceResponse donateCertificates(RewardServiceRequest rewardServiceRequest)
      throws DigitalIntegrationException {

    String serviceMethodName = ServiceMethod.DONATE_CERTIFICATE.getServiceMethodName();
    boolean hasException = false;

    if (doRunService(serviceMethodName)) {

      startPerformanceMonitorOperation(serviceMethodName);

      if (rewardServiceRequest == null
          || DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
        throw new DigitalIntegrationBusinessException(
            "ProfileId missing for donateCertificates",
            ErrorCodes.MISSING_REQUIRED.getCode());
      }

      if (rewardServiceRequest.getCertificates() == null
          || rewardServiceRequest.getCertificates().isEmpty()) {
        throw new DigitalIntegrationBusinessException(
            "Rewards Certificates missing for donateCertificates",
            ErrorCodes.MISSING_REQUIRED.getCode());
      }

      if (null == rewardServiceRequest.getCharity()
          || DigitalStringUtil.isEmpty(rewardServiceRequest.getCharity().getId())) {
        throw new DigitalIntegrationBusinessException(
            "CharityId missing for donateCertificates",
            ErrorCodes.MISSING_REQUIRED.getCode());
      }

      try {
        RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
        rewardServiceResponse.setSuccess(false);

				String profileId = "";
				if (rewardServiceRequest.getPerson() != null) {
					profileId = rewardServiceRequest.getPerson().getProfileID();
				}

				String endPoint = populateURL(getDefaultUri() + getDonateCertificatesResource(), PROFILE_ID,
						profileId);

				Rewards rewardsRequest =
						populateRewardsFromRewardsServiceRequest(rewardServiceRequest);

        String bodyContent = serviceClient.getMarshalledStringFromObject(Rewards.class,
						rewardsRequest);

        ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
            getRequestTimeout(serviceMethodName), endPoint,
            ServiceRequest.ServiceContentType.JSON,
            ServiceHttpMethod.PUT, serviceMethodName);
        serviceRequest.setBody(bodyContent);

        populateRequestHeaders(serviceRequest, serviceMethodName);

        HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				handleRewardCertificatesResponse(rewardServiceResponse, response);

        return rewardServiceResponse;
      } catch (Exception e) {
        hasException = true;

        throw new DigitalIntegrationBusinessException("VIP Rewards donateCertificates Error",
            ErrorCodes.COMM_ERROR.getCode(), e);
      } finally {
        endPerformanceMonitorOperation(serviceMethodName, hasException);
      }

    } else {
      throw new DigitalIntegrationInactiveException(
          String
              .format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
          ErrorCodes.INACTIVE.getCode());

    }
  }

	/**
	 * @param rewardServiceRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse retrieveIncentives(RewardServiceRequest rewardServiceRequest)
			throws DigitalIntegrationException {
		String serviceMethodName = ServiceMethod.RETRIEVE_INCENTIVES.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			if (rewardServiceRequest == null
					|| DigitalStringUtil.isBlank(rewardServiceRequest.getPerson().getProfileID())) {
				throw new DigitalIntegrationBusinessException(
						"ProfileId missing for retrieveIncentives",
						ErrorCodes.MISSING_REQUIRED.getCode());
			}

			// get cacheKey for the method
			String cacheKey =
					serviceMethodName + ":" + rewardServiceRequest.getPerson().getProfileID();

			try {
				//  check whether we have a cached response within the request scope
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				boolean cacheEnabled = false;
				if (null != request) {
					String cacheRewardsCalls = request.getParameter(CACHE_REWARDS_CALLS);
					cacheEnabled = DigitalStringUtil.isNotBlank(cacheRewardsCalls)
              && "Y".equalsIgnoreCase(cacheRewardsCalls);
				}
				RewardServiceResponse btsRewardServiceResponse = this
						.getCachedResponse(cacheKey, cacheEnabled);

				if (null == btsRewardServiceResponse) {
					btsRewardServiceResponse = new RewardServiceResponse();
					btsRewardServiceResponse.setSuccess(false);

					String profileId = "";

					if (rewardServiceRequest.getPerson() != null) {
						profileId = rewardServiceRequest.getPerson().getProfileID();
					}

					String endPoint = populateURL(getDefaultUri() +
							getIncentivesResource(), PROFILE_ID, profileId);

					ServiceRequest serviceRequest = new ServiceRequest(
							getConnectionTimeout(serviceMethodName),
							getRequestTimeout(serviceMethodName), endPoint,
							ServiceContentType.JSON, ServiceHttpMethod.GET, serviceMethodName);

					populateRequestHeaders(serviceRequest, serviceMethodName);

					HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

					handleRetrieveIncentivesResponse(btsRewardServiceResponse, response);

					if (cacheEnabled) {
						this.getRewardsCache().putRewardsData(cacheKey, btsRewardServiceResponse);
					}
				}
				return btsRewardServiceResponse;
			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("VIP Rewards retrieveIncentives Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String
							.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());

		}
	}
	
	
	public RewardServiceResponse retrieveEnticements(RewardsSendOrderRequest sendOrderRequest) throws DigitalIntegrationException {
	    String serviceMethodName = ServiceMethod.RETRIEVE_ENTICEMENTS.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			try {
			    RewardServiceResponse rewardsServiceResponse = new RewardServiceResponse();
			    RewardsEnticementsResponse rewardsEnticementsResponse=new RewardsEnticementsResponse();
			    rewardsServiceResponse.setSuccess(false);
				String bodyContent = serviceClient.getMarshalledStringFromObject(RewardsSendOrderRequest.class, sendOrderRequest);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri()+getEnticementsResource(),
						ServiceRequest.ServiceContentType.JSON,
						ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setBody(bodyContent);

				populateRequestHeaders(serviceRequest,serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				handleEnticementsResponse(rewardsServiceResponse, response);
				
				
				return rewardsServiceResponse;

			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("VIP Rewards  Error",ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param response
	 * @throws DigitalIntegrationException
	 */
	private void handleRetrieveIncentivesResponse(RewardServiceResponse rewardServiceResponse,
			HttpServiceResponse response)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			try {
				String responseString = response.getEntityString();

				serviceClient.logServiceResponse(responseString);

				TypeReference<List<IncentiveItem>> typeReference =
						new TypeReference<List<IncentiveItem>>() {
						};

				List<IncentiveItem> incentiveItems = serviceClient.getObjectMapper()
						.readValue(response.getEntityString(), typeReference);

				rewardServiceResponse.setSuccess(true);

				populateIncentivesDataToRewardServiceResponse(rewardServiceResponse,
						incentiveItems);

			} catch (Exception e) {
				logger.error("Error marshalling response ", e);
				throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error",
						ErrorCodes.XFORM.getCode(), e);
			}
		} else {
			handleError(response, rewardServiceResponse);
		}
	}

	/**
	 * @param rewardServiceResponse
	 * @param incentiveItems
	 */
	private void populateIncentivesDataToRewardServiceResponse(
			RewardServiceResponse rewardServiceResponse,
			List<IncentiveItem> incentiveItems) {
		if (incentiveItems != null) {
			List<Incentive> incentiveList = new ArrayList<>();
			for (IncentiveItem incentiveItem : incentiveItems) {
				Incentive incentive = new Incentive();
				incentive.setDenomination(incentiveItem.getDenomination());
				incentive.setPointCost(incentiveItem.getPointsCost());
				incentive.setToBeRedeemed(incentiveItem.isToBeRedeemed());
				incentiveList.add(incentive);
			}
			rewardServiceResponse.setIncentives(incentiveList);
		}
	}

	/**
	 * @param soapBody
	 * @return String
	 * @throws Exception
	 */
	public String getFaultDetails(SOAPBody soapBody) throws Exception {

		Document faultDocument = soapBody.getOwnerDocument();
		Element faultElement = (Element) faultXpath
				.evaluate(LINKED_EXCEPTION_STACK_TRACE, faultDocument,
				XPathConstants.NODE);

		if (faultElement != null) {
			String text = faultElement.getTextContent();
			return String.format("%s: %s", soapBody.getFault().getFaultString(),
					DigitalStringUtil.split(text, "\n\t")[0]);
		} else {
			return String
					.format("%s: %s", soapBody.getFault() != null ? soapBody.getFault().getFaultString() : "",
					"Unknown Error");
		}
	}

	/**
	 * Return request scoped rewards cache every time this is called
	 *
	 * @return BtsRewardsCache
	 */
	private BtsRewardsCache getRewardsCache() {
		return ComponentLookupUtil.lookupComponent(ComponentLookupUtil.REWARDS_CACHE, BtsRewardsCache.class);
	}

	/**
	 * @param key
	 */
	private RewardServiceResponse getRewardsDataFromCache(String key) {
		return this.getRewardsCache().getRewardsData(key);
	}

	/**
	 * @param key
	 */
	private void putRewardsDataFromCache(String key,  RewardServiceResponse response) {
		this.getRewardsCache().putRewardsData(key, response);
	}

	/**
	 * @return RewardServiceResponse
	 */
	private RewardServiceResponse getCachedResponse(String cacheKey, boolean cacheEnabled) {
		RewardServiceResponse response = null;
		if (logger.isDebugEnabled()) {
			logger.debug("cacheRewardsCalls: " + cacheEnabled + ",cacheKey:  " + cacheKey);
		}
		if(cacheEnabled) {
			response = this.getRewardsCache().getRewardsData(cacheKey);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("cacheKey : " + cacheKey + ",Rewardsreponse: " + response);
		}
		return response;
	}

	/**
	 *
	 * @param orginal
	 * @param replace
	 * @param with
	 * @return String
	 */
	private String populateURL(String orginal, String replace, String with){
		return orginal.replace(replace, with);
	}

	/**
	 * @param response
	 * @param btsRewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	private void handleError(HttpServiceResponse response,
			RewardServiceResponse btsRewardServiceResponse)
			throws DigitalIntegrationException {
        final String CONTENT_TYPE_JSON="application/json";
		String errorCode = "HTTP_" + response.getStatusCode();
		String errorDescription = "Genberic error. Please try again later";
		String contentType = response.getContentType();
		/*
		* If the error response is JSON unmarshal it with Apigee error JSON schema, else
		* treat as generic exception
		* */

		if (DigitalStringUtil.isNotBlank(response.getEntityString()) &&
                DigitalStringUtil.isNotBlank(contentType) &&
                contentType.contains(CONTENT_TYPE_JSON)) {
			ErrorItem error = serviceClient
					.getUnmarshalledObjectFromHttpServiceResponse(ErrorItem.class, response);
			if (error.getCode() != null) {
				errorCode = error.getCode();
			}
			errorDescription = error.getMessage();
		}
		List<ResponseError> errorList = new ArrayList<>();
		handleError(errorList, errorCode, errorDescription);
		btsRewardServiceResponse.setGenericExceptions(errorList);
	}

	/**
	 *
	 * @param errorList
	 * @param errorCode
	 * @param errorDescription
	 */
	private void handleError(List<ResponseError> errorList, String errorCode, String errorDescription){
		ResponseError responseError = new ResponseError(errorCode, errorDescription);
		errorList.add(responseError);
	}
	
	/**
	 * @param sendOrderRequest
	 * @return RewardServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@Override
	public RewardServiceResponse sendOrder(RewardsSendOrderRequest sendOrderRequest) throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.SEND_ORDER.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			try {
				RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
				rewardServiceResponse.setSuccess(false);
				String bodyContent = serviceClient.getMarshalledStringFromObject(RewardsSendOrderRequest.class, sendOrderRequest);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri()+getSendOrderResource(),
						ServiceRequest.ServiceContentType.JSON,
						ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setBody(bodyContent);

				populateRequestHeaders(serviceRequest,serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				handleSendOrderResponse(rewardServiceResponse, response);

				return rewardServiceResponse;

			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("VIP Rewards  Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}
	}
	
	
	/**
	 * @param rewardServiceResponse
	 * @param response
	 * @throws DigitalIntegrationException
	 */
	private void handleSendOrderResponse(RewardServiceResponse rewardServiceResponse,
			HttpServiceResponse response)
			throws DigitalIntegrationException {
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
			rewardServiceResponse.setSuccess(true);
			} else {
			handleError(response, rewardServiceResponse);
		}
	}
	
	private void handleEnticementsResponse(RewardServiceResponse rewardServiceResponse,
		HttpServiceResponse response)
			throws DigitalIntegrationException {
	    if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
		try {
		    String responseString = response.getEntityString();

		    serviceClient.logServiceResponse(responseString);

		    TypeReference<RewardsEnticementsResponse> typeReference =new TypeReference<RewardsEnticementsResponse>() {};

		    RewardsEnticementsResponse rewardsEnticementsResponse= serviceClient.getObjectMapper()
			    .readValue(response.getEntityString(), typeReference);

		    rewardServiceResponse.setSuccess(true);
		    rewardServiceResponse.setEnticementsPoints(rewardsEnticementsResponse.getPoints());

		} catch (Exception e) {
		    logger.error("Error marshalling response ", e);
		    throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error",
			    ErrorCodes.XFORM.getCode(), e);
		}
	    } else {
		handleError(response, rewardServiceResponse);
	    }
	}
	
    public Map<String, Boolean> getServiceMethodsEnabled(){
		Map<String, Boolean> methods = new HashMap<>();
		for (ServiceMethod service: ServiceMethod.values()) {
			methods.put(service.getServiceMethodName(), isServiceMethodEnabled(service.getServiceMethodName()));
		}
		
		return methods;
    }
}
