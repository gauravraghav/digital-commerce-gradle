package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IncentiveItem {

	@JsonProperty("pointsCost")
	private Integer pointsCost;

	@JsonProperty("toBeRedeemed")
	private Boolean toBeRedeemed;

	@JsonProperty("denomination")
	private Integer denomination;

	public Boolean isToBeRedeemed() {
		return this.getToBeRedeemed();
	}

}