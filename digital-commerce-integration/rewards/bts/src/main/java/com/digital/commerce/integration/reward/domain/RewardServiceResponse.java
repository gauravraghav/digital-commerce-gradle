package com.digital.commerce.integration.reward.domain;

import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.logger.DigitalLogger;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mmallipu
 */
@Getter
@Setter
@ToString
public class RewardServiceResponse implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected static final DigitalLogger logger = DigitalLogger.getLogger(RewardServiceResponse.class);
  private boolean isSuccess;

  private Contact contact = new Contact();
  private Address address = new Address();
  private Person person = new Person();
  private Flags flags = new Flags();
  private List<Order> orders = new ArrayList<>();

  private String fraudCode;
  private String fraudCodeGroup;
  private String frequencyFashionEmail;
  private String pointsBanking;
  private String preferredStore;
  private String signUpDate;
  private String signUpStore;

  private String partyPurchaseStatusCode;
  private String shoppingChannelCode;

  private List<Offer> offers;

  private List<Points> points;

  private List<PointsTansaction> pointsTansaction;

  private List<Certificate> certificates;

  private List<BirthdayGift> birthdayGifts;

  private List<Shopfor> shopfors;

  private List<Charity> charities;

  private List<Incentive> incentives;

  private double totalCertValue;

  private int currentBalancePoint;

  private int enticementsPoints;

  private List<ResponseError> genericExceptions = new ArrayList<>();

  /* For rewards Denomination - Start*/
  private String certDenomination;
  private String isPaperlessCert;
  /* For rewards Denomination - End*/


  /**
   * @return the isPaperlessCert
   */
  public boolean getIsPaperlessCert() {
    if (isPaperlessCert == null) {
      return false;
    }
    switch (isPaperlessCert) {
      case "N":
        return false;
      case "Y":
        return true;
      default:
        if (logger.isDebugEnabled()) {
          logger.debug("There was a issue with response. It was not 'Y' neither 'N' ");
        }
        return false;
    }
  }

}
