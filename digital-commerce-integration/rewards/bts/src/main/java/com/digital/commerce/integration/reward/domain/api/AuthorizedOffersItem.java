package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorizedOffersItem{

	@JsonProperty("isAReissue")
	private Boolean isAReissue;

	@JsonProperty("discountCode")
	private String discountCode;

	@JsonProperty("canReissue")
	private Boolean canReissue;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("issueDate")
	private String issueDate;

	@JsonProperty("barcode")
	private Long barcode;

	@JsonProperty("value")
	private Integer value;

	@JsonProperty("voidFlag")
	private Boolean voidFlag;

	@JsonProperty("expirationCode")
	private String expirationCode;

	@JsonProperty("customerFacingName")
	private String customerFacingName;

	@JsonProperty("offerDetailsPageName")
	private String offerDetailsPageName;

	@JsonProperty("status")
	private String status;

	@JsonProperty("expirationDate")
	private String expirationDate;

	@JsonProperty("demandTransactionNumber")
	private String demandTransactionNumber;

	@JsonProperty("businessUnit")
	private Integer businessUnit;

	@JsonProperty("transactionDate")
	private String transactionDate;

	@JsonProperty("locationNumber")
	private Integer locationNumber;

	public Boolean isCanReissue() {
		return this.getCanReissue();
	}

	public Boolean isVoidFlag(){
		return this.getVoidFlag();
	}

}