package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rewards{

	@JsonProperty("totalRewardsCount")
	private Integer totalRewardsCount;

	@JsonProperty("totalRewardsValue")
	private Integer totalRewardsValue;

	@JsonProperty("AuthorizedOffers")
	private List<AuthorizedOffersItem> authorizedOffers;

	@JsonProperty("totalAuthorizedOffersValue")
	private Integer totalAuthorizedOffersValue;

	@JsonProperty("Rewards")
	private List<RewardsItem> rewards;

	@JsonProperty("totalAuthorizedOffersCount")
	private Integer totalAuthorizedOffersCount;

}