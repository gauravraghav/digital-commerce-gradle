package com.digital.commerce.integration.reward.util;

import java.util.HashMap;
import java.util.Map;

import com.digital.commerce.integration.reward.domain.RewardServiceResponse;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;

public class BtsRewardsCache extends GenericService {
	private final Map<String, RewardServiceResponse> rewardsCache = new HashMap<>();
	private final Map<String, Integer> accessCount = new HashMap<>();

	private int cacheMisses = 0;
	private int cacheHits = 0;

	@Override
	public void doStartService() throws ServiceException {
		super.doStartService();
		if (isLoggingDebug()) {
			logDebug("Starting BtsRewardsCache Service");
		}
	}

	@Override
	public void doStopService() throws ServiceException {
		super.doStopService();
		if (isLoggingDebug()) {
			logDebug("Stopped BtsRewardsCache Service");
			logDebug("------ BtsRewardsCache Stats ------");
			logDebug("Total Requests: [ " + (cacheHits + cacheMisses) + " ] Hits: [ " + cacheHits + " ] Misses: [ "
					+ cacheMisses + " ]");
			for (String key : accessCount.keySet()) {
				logDebug('\t' + "Key: [ " + key + " ] Hit Count: [ " + accessCount.get(key) + " ]");
			}
		}
		rewardsCache.clear();
		accessCount.clear();
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public final RewardServiceResponse getRewardsData(final String key) {
		if (rewardsCache.containsKey(key)) {
			if (isLoggingDebug()) {
				if (accessCount.containsKey(key)) {
					int hitCount = accessCount.get(key) + 1;
					accessCount.put(key, hitCount);
				} else {
					accessCount.put(key, 1);
				}
				cacheHits++;
			}
			return rewardsCache.get(key);
		} else {
			if (isLoggingDebug()) {
				cacheMisses++;
			}
			return null;
		}
	}

	/**
	 * 
	 * @param key
	 * @param price
	 */
	public final void putRewardsData(final String key, final RewardServiceResponse price) {
		rewardsCache.put(key, price);
	}
}
