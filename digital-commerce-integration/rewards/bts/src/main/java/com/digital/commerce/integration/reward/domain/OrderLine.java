package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OrderLine {

	protected String fulfillmentLocationNumber;
	protected String fulfillmentTypeCode;
	protected String individualID;
	protected String itemFulfillmentStatusCode;
	protected String itemFulfillmentStatusDate;
	protected String itemQuantity;
	protected String orderDate;
	protected String orderItemLineNumber;
	protected String orderItemLineSequenceNumber;
	protected String orderLocationID;
	protected String orderNumber;
	protected String pickupLocationNumber;
	protected double retailPriceAmount;
	protected String returnLocationNumber;
	protected String shipToFirstName;
	protected String shipToLastName;
	protected String shippingTrackingNumber;
	protected String skuID;
	
}
