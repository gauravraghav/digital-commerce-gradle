package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrdersItem{

	@JsonProperty("storeNumber")
	private String storeNumber;

	@JsonProperty("orderNumber")
	private String orderNumber;

	@JsonProperty("localeCode")
	private String localeCode;

	@JsonProperty("postalCode")
	private Integer postalCode;

	@JsonProperty("onlineSiteId")
	private String onlineSiteId;

	@JsonProperty("accountNumber")
	private Long accountNumber;

	@JsonProperty("currencyCode")
	private String currencyCode;

	@JsonProperty("orderDate")
	private String orderDate;

	@JsonProperty("items")
	private List<ItemsItem> items;

	@JsonProperty("businessUnitId")
	private Integer businessUnitId;

	@JsonProperty("orderTotal")
	private Double orderTotal;

}