package com.digital.commerce.integration.reward.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class RewardsEnticementsResponse extends ResponseWrapper {

  int points;
}
