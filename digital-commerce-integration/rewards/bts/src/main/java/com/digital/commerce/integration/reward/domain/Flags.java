package com.digital.commerce.integration.reward.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Flags implements  Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String onlineAccountFlag;
	private String optOutFashionEmail;
	private String optOutOnlineEmail;
	private String optOutRewardsEmail;
	private String optOutRewardsText;
	private String optOutStoreEmail;
	private String optOutStoreText;
	private String optOutMobile;
	private String enrollAsMemberFlag;
	private String loyaltyMemberFlag;
	private String noEMail;
	private String noMail;
	private String noPhone;
	private String noRent;	
	private String badAddress;
	private String badEmail;
	private String badPhone;	
	private String isCardHolder;
	private String isLux;	
	private String topMemberCompanyIndicator;
	private String topMemberStoreIndicator;
	private String deletedMemberFlag;
	private String invalidLoyaltyMemberFlag;
	private String loyaltyProgramMemberStatus;
	private String memberCombinedFlag;
	private String menEmail;
	private String womenEmail;
	private String soft;
	private String hard;
}
