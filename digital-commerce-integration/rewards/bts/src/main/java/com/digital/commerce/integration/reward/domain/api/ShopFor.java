package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShopFor{

	@JsonProperty("birthday")
	private Integer birthday;

	@JsonProperty("birthMonth")
	private Integer birthMonth;

	@JsonProperty("product")
	private Product product;

	@JsonProperty("name")
	private String name;

	@JsonProperty("relationship")
	private String relationship;

}