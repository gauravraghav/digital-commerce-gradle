package com.digital.commerce.integration.reward.order;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import com.digital.commerce.integration.reward.RewardService;
import com.digital.commerce.integration.reward.bts.BtsRewardService;
import com.digital.commerce.integration.reward.domain.api.RewardsSendOrderRequest;
import com.digital.commerce.common.logger.DigitalLogger;

import atg.dms.patchbay.MessageSink;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RewardsSendOrderMessageSink implements MessageSink {
	private static transient DigitalLogger logger = DigitalLogger.getLogger(RewardsSendOrderMessageSink.class);

	private BtsRewardService rewardsService;

	@Override
	public void receiveMessage(String s, Message message) throws JMSException {
		try {
			RewardsSendOrderRequest sendOrderRequest = (RewardsSendOrderRequest) ((ObjectMessage) message).getObject();
			if (logger.isDebugEnabled()) {
				logger.debug("Rewards sendOrderRequest message -------> " + sendOrderRequest);
			}

			// call rewards and proceed, do not worry even if it fails
			if (rewardsService.isServiceEnabled() && rewardsService.isServiceMethodEnabled(
					RewardService.ServiceMethod.SEND_ORDER.getServiceMethodName())) {
				rewardsService.sendOrder(sendOrderRequest);
			} else {
				if (logger.isInfoEnabled()) {
					logger.info("Rewards/sendOrder offline, not sending order info to rewards.");
				}
			}
		} catch (Exception e) {
			logger.info("Exception while posting rewards for order udpates: "+ e.getMessage());
		}
	}

}
