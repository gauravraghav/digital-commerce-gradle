package com.digital.commerce.integration.reward.domain.api;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
public class OrderHistory{

	@JsonProperty("orders")
	private List<OrdersItem> orders;

}