package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShopForsItem{

	@JsonProperty("dataDate")
	private String dataDate;

	@JsonProperty("shopForId")
	private Long shopForId;

	@JsonProperty("shopFor")
	private ShopFor shopFor;

	@JsonProperty("customerId")
	private Long customerId;

}