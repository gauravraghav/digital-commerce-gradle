package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Loyalty{

	@JsonProperty("enrollMethod")
	private String enrollMethod;

	@JsonProperty("pointsExpireTwoMonths")
	private Integer pointsExpireTwoMonths;

	@JsonProperty("currentPointsBalance")
	private Integer currentPointsBalance;

	@JsonProperty("signUpStore")
	private Integer signUpStore;

	@JsonProperty("pointsUntilNextReward")
	private Integer pointsUntilNextReward;

	@JsonProperty("pointsExpireNextMonth")
	private Integer pointsExpireNextMonth;

	@JsonProperty("signUpDate")
	private String signUpDate;

	@JsonProperty("accountNumber")
	private String accountNumber;

}