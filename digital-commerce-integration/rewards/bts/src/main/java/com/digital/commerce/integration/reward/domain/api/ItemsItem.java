package com.digital.commerce.integration.reward.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@ToString
@Generated("com.robohorse.robopojogenerator")
@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemsItem{

	@JsonProperty("returnLocationNumber")
	private Integer returnLocationNumber;

	@JsonProperty("quantity")
	private Integer quantity;

	@JsonProperty("orderNumber")
	private String orderNumber;

	@JsonProperty("retailPriceAmount")
	private Double retailPriceAmount;

	@JsonProperty("fulfillmentStatusCode")
	private String fulfillmentStatusCode;

	@JsonProperty("shipToFirstName")
	private String shipToFirstName;

	@JsonProperty("fulfillmentStatusDate")
	private String fulfillmentStatusDate;

	@JsonProperty("fulfillmentTypecode")
	private String fulfillmentTypecode;

	@JsonProperty("fulfillmentLocationNumber")
	private String fulfillmentLocationNumber;

	@JsonProperty("orderItemSequenceNumber")
	private Integer orderItemSequenceNumber;

	@JsonProperty("orderLocationNumber")
	private String orderLocationNumber;

	@JsonProperty("pickupLocationNumber")
	private Integer pickupLocationNumber;

	@JsonProperty("orderLineItemNumber")
	private Long orderLineItemNumber;

	@JsonProperty("sku")
	private String sku;

	@JsonProperty("orderDate")
	private String orderDate;

	@JsonProperty("trackingNumber")
	private String trackingNumber;

	@JsonProperty("shipToLastName")
	private String shipToLastName;

}