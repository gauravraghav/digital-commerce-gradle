package com.digital.commerce.integration.common.jms;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;

import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;

public class DigitalJMSClient extends GenericService {
	private static final DigitalLogger logger = DigitalLogger
			.getLogger(DigitalJMSClient.class);

	private InitialContext context = null;
	private QueueConnectionFactory qcf = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see atg.nucleus.GenericService#doStartService()
	 */

	public void doStartService() throws ServiceException {
		try {
			if (context == null) {
				context = new InitialContext();
			}
			if (qcf == null) {
				qcf = (QueueConnectionFactory) context.lookup("/JmsXA");
			}

		} catch (Exception ex) {
			logger.error("Exception while creating a Initial context", ex);
			throw new ServiceException(ex);
		}
	}

	/**
	 * @param queueJNDI
	 * @return
	 * @throws DSWAppException
	 * @throws NamingException
	 */
	private Queue createJMSQueue(String queueJNDI)
			throws DigitalIntegrationException {
		Queue q = null;
		try {
			q = (Queue) context.lookup(queueJNDI);
		} catch (Exception ex) {
			logger.error("Unable to lookup queue " + queueJNDI);
			throw new DigitalIntegrationSystemException("Unable to lookup queue",
					ErrorCodes.CONFIG.getCode(), ex);
		}
		return q;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see atg.nucleus.GenericService#doStopService()
	 */
	public void doStopService() throws ServiceException {
		try {
			if (context != null) {
				context.close();
			}
		} catch (Exception ex) {
			logger.error("Exception while creating a Initial context");
			throw new ServiceException(ex);
		}
	}

	/**
	 * @param queueJNDI
	 * @param event
	 * @param jmsProperties
	 * @param transacted
	 * @throws DigitalIntegrationSystemException
	 */
	public void sendObjectMessage(String queueJNDI, Serializable event,
			HashMap<String, Object> jmsProperties, boolean transacted)
			throws DigitalIntegrationSystemException {
		QueueConnection connection = null;
		QueueSession session = null;
		try {
			connection = qcf.createQueueConnection();
			session = connection.createQueueSession(transacted,
					Session.AUTO_ACKNOWLEDGE);
			ObjectMessage message = session.createObjectMessage();
			message.setObject(event);
			message.setJMSTimestamp((new Date()).getTime());

			if (jmsProperties != null
					&& jmsProperties.containsKey("DELIVERY_DELAY")
					&& ((Long) jmsProperties.get("DELIVERY_DELAY")) > 0l) {

				long now = System.currentTimeMillis();
				Long deliveryDate = new Long(now
						+ (Long) jmsProperties.get("DELIVERY_DELAY"));
				message.setObjectProperty(
						atg.dms.patchbay.MessageLimbo.DELIVERY_DATE,
						deliveryDate);
			}
			QueueSender sender = session.createSender(this
					.createJMSQueue(queueJNDI));
			sender.send(message);
			if (transacted) {
				session.commit();
			}
		} catch (Exception ex) {
			logError("Exception while sending object message for queue "
					+ queueJNDI, ex);
			throw new DigitalIntegrationSystemException(
					"Exception while sending text message for queue",
					ErrorCodes.COMM_ERROR.getCode(), ex);
		} finally {
			if (null != connection) {
				try {
					connection.close();
				} catch (JMSException e) {
					logError("Exception while closing the queue connection ", e);
				}
			}
		}
	}

	/**
	 * @param queueJNDI
	 * @param event
	 * @param jmsProperties
	 * @param transacted
	 * @throws DigitalIntegrationSystemException
	 */
	public void sendTextMessage(String queueJNDI, String pXMLString,
			HashMap<String, Object> jmsProperties, boolean transacted)
			throws DigitalIntegrationSystemException {
		QueueConnection connection = null;
		QueueSession session = null;
		try {
			connection = qcf.createQueueConnection();
			session = connection.createQueueSession(transacted,
					Session.AUTO_ACKNOWLEDGE);

			TextMessage message = session.createTextMessage();
			message.setJMSType("javax.jms.TextMessage");
			message.setText(pXMLString);
			message.setJMSTimestamp((new Date()).getTime());

			if (jmsProperties != null
					&& jmsProperties.containsKey("DELIVERY_DELAY")
					&& ((Long) jmsProperties.get("DELIVERY_DELAY")) > 0l) {

				long now = System.currentTimeMillis();
				Long deliveryDate = new Long(now
						+ (Long) jmsProperties.get("DELIVERY_DELAY"));
				message.setObjectProperty(
						atg.dms.patchbay.MessageLimbo.DELIVERY_DATE,
						deliveryDate);
			}
			QueueSender sender = session.createSender(this
					.createJMSQueue(queueJNDI));
			sender.send(message);
			if (transacted) {
				session.commit();
			}
		} catch (Exception ex) {
			logError("Exception while sending text message for queue "
					+ queueJNDI, ex);
			throw new DigitalIntegrationSystemException(
					"Exception while sending text message for queue",
					ErrorCodes.COMM_ERROR.getCode(), ex);
		} finally {
			if (null != connection) {
				try {
					connection.close();
				} catch (JMSException e) {
					logError("Exception while closing the queue connection ", e);
				}
			}
		}
	}

}
