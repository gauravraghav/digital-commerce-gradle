package com.digital.commerce.integration.common.exception;

public class ExceptionHelper {
	public static String formatMessage(String message, String errorCode, String category) {
		return String.format("%s: %s - %s", errorCode, message, category );
	}
}
