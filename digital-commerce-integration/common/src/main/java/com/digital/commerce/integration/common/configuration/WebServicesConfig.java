package com.digital.commerce.integration.common.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.crypto.DigitalVaultUtil;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;

/**
 * Basic POJO that stores all integration configurable parameters used by the
 * integration services
 *
 * @author JM406760
 *
 */
public class WebServicesConfig {
	private DigitalLogger log = DigitalLogger.getLogger(WebServicesConfig.class);

	public enum ServicNames {
		USPS("usps"), INVENTORY_DECREMENT("invDec"), CERIDIAN("ceridian"), YANTRA_INVENTORY("yantraInv"), SMTP(
				"smtp"), CARDINAL("cardinal"), STORENET("storenet"), YANTRA_OMS(
						"order_submit"), VERTEX("vertex"), VANTIV("vantiv"), REWARDS("rewards"),
		YANTRA_REPRICING("yantraRepricing"), ORDER_STATUS("order_status"), AFTER_PAY("afterPay");

		String name;

		private ServicNames(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

	}

	public enum DataTypes {
		AVAILABILITY("availability"), TIMEOUTS("timeouts"), STATIC("static"), SOAP_ACTIONS("soapActions"),
		PHYSICAL_ADDRESS("physicalAddress"), ADMIN_ADDRESS("adminAddress"), GIFT_CARDS("gift_cards"),
		COUNTRY("countryMapping"), SOFT_DECLINE("softDecline"), PROCESS_OFFLINE("processAsOffline"),
		FRAUD_TEST("fraudTestMapping"),FRAUD_ONLY_TEST("fraudOnlyTestMapping"),REST_RESOURCES("rewardRestResources"),
		MOCKING_RESOURCES("rewardMockingResources");

		String name;

		private DataTypes(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

	}

	/*
	 * make sure that any getter that is vaulted uses the DigitalVaultUtil to
	 * decrypt. Make sure to use the getVaultedData method to return the value
	 */
	private boolean useVaultedPasswords = true;

	private Map<String, Map<String, Map<String, String>>> dataMap = new HashMap<>();

	// general client settings
	private Map<String, String> proxyInfo = new HashMap<>();
	private Map<String, String> defaultTimeout = new HashMap<>();
	private Map<String, String> retryCount = new HashMap<>();
	private Map<String, String> requireTLS12 = new HashMap<>();
	private Map<String, String> requireProxy = new HashMap<>();
	private Map<String, String> requiresConnectionPool = new HashMap<>();
	private Map<String, String> requiresKeepAliveStratergy = new HashMap<>();
	private Map<String, String> requiresGracefulConnectionShutdown = new HashMap<>();
	private Map<String, String> keepAliveTimeout = new HashMap<>();
	private Map<String, String> connectionManagerConfig = new HashMap<>();
	
	/*
	 * These attributes may need to be service specific
	 */
	private Integer insecurePort = 80;
	private Integer securePort = 443;

	private List<String> validResponseStatusCodes = new ArrayList<>();

	public Map<String, String> getConnectionManagerConfig() {
		return connectionManagerConfig;
	}

	public void setConnectionManagerConfig(Map<String, String> connectionManagerConfig) {
		this.connectionManagerConfig = connectionManagerConfig;
	}

	public Map<String, String> getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Map<String, String> retryCount) {
		this.retryCount = retryCount;
	}

	public Map<String, String> getRequireTLS12() {
		return requireTLS12;
	}

	public void setRequireTLS12(Map<String, String> requireTLS12) {
		this.requireTLS12 = requireTLS12;
	}

	public Map<String, String> getRequireProxy() {
		return requireProxy;
	}

	public void setRequireProxy(Map<String, String> requireProxy) {
		this.requireProxy = requireProxy;
	}

	public Map<String, String> getDefaultTimeout() {
		return defaultTimeout;
	}

	public void setDefaultTimeout(Map<String, String> defaultTimeout) {
		this.defaultTimeout = defaultTimeout;
	}

	/**
	 * This method is to be used in any accessor that uses the DigitalVaultUtil
	 * 
	 * @param value
	 * @return
	 */
	public String getVaultedData(String value) {
		if (useVaultedPasswords) {
			try {
				return DigitalVaultUtil.getDecryptedString(value);
			} catch (Exception e) {
				log.error("Exception when un-vaulting data", new DigitalIntegrationSystemException(
						"Exception when un-vaulting data", ErrorCodes.CONFIG.getCode(), e));

				return null;
			}
		} else {
			return value;
		}
	}

	public Map<String, String> getProxyInfo() {
		return proxyInfo;
	}

	public void setProxyInfo(Map<String, String> proxyInfo) {
		this.proxyInfo = proxyInfo;
	}

	public Map<String, String> getSvsSoapActions() {
		return getMapData(ServicNames.CERIDIAN.getName(), DataTypes.SOAP_ACTIONS.getName());
	}

	public void setSvsSoapActions(Map<String, String> svsSoapActions) {
		setMapData(ServicNames.CERIDIAN.getName(), DataTypes.SOAP_ACTIONS.getName(), svsSoapActions);
	}

	public Map<String, String> getSvsStaticData() {
		return getMapData(ServicNames.CERIDIAN.getName(), DataTypes.STATIC.getName());
	}

	public void setSvsStaticData(Map<String, String> svsStaticData) {
		setMapData(ServicNames.CERIDIAN.getName(), DataTypes.STATIC.getName(), svsStaticData);
	}

	public Map<String, String> getSvsServiceMethodAvailability() {
		return getMapData(ServicNames.CERIDIAN.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setSvsServiceMethodAvailability(Map<String, String> svsServiceMethodAvailability) {
		setMapData(ServicNames.CERIDIAN.getName(), DataTypes.AVAILABILITY.getName(), svsServiceMethodAvailability);
	}

	public Map<String, String> getSvsServiceMethodTimeout() {
		return getMapData(ServicNames.CERIDIAN.getName(), DataTypes.TIMEOUTS.getName());

	}

	public void setSvsServiceMethodTimeout(Map<String, String> svsServiceMethodTimeout) {
		setMapData(ServicNames.CERIDIAN.getName(), DataTypes.TIMEOUTS.getName(), svsServiceMethodTimeout);
	}

	public Map<String, String> getVantivStaticData() {
		return getMapData(ServicNames.VANTIV.getName(), DataTypes.STATIC.getName());
	}

	public void setVantivStaticData(Map<String, String> vantivStaticData) {
		setMapData(ServicNames.VANTIV.getName(), DataTypes.STATIC.getName(), vantivStaticData);
	}

	public Map<String, String> getVantivServiceMethodAvailability() {
		return getMapData(ServicNames.VANTIV.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setVantivServiceMethodAvailability(Map<String, String> vantivServiceMethodAvailability) {
		setMapData(ServicNames.VANTIV.getName(), DataTypes.AVAILABILITY.getName(), vantivServiceMethodAvailability);
	}

	public Map<String, String> getVantivServiceMethodTimeout() {
		return getMapData(ServicNames.VANTIV.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setVantivServiceMethodTimeout(Map<String, String> vantivServiceMethodTimeout) {
		setMapData(ServicNames.VANTIV.getName(), DataTypes.TIMEOUTS.getName(), vantivServiceMethodTimeout);
	}

	public Map<String, String> getUspsServiceMethodAvailability() {
		return getMapData(ServicNames.USPS.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setUspsServiceMethodAvailability(Map<String, String> uspsServiceMethodAvailability) {
		setMapData(ServicNames.USPS.getName(), DataTypes.AVAILABILITY.getName(), uspsServiceMethodAvailability);
	}

	public Map<String, String> getUspsServiceMethodTimeout() {
		return getMapData(ServicNames.USPS.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setUspsServiceMethodTimeout(Map<String, String> uspsServiceMethodTimeout) {
		setMapData(ServicNames.USPS.getName(), DataTypes.TIMEOUTS.getName(), uspsServiceMethodTimeout);
	}

	public Map<String, String> getVertexStaticData() {
		return getMapData(ServicNames.VERTEX.getName(), DataTypes.STATIC.getName());
	}

	public void setVertexStaticData(Map<String, String> vertexStaticData) {
		setMapData(ServicNames.VERTEX.getName(), DataTypes.STATIC.getName(), vertexStaticData);
	}

	public Map<String, String> getVertexServiceMethodAvailability() {
		return getMapData(ServicNames.VERTEX.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setVertexServiceMethodAvailability(Map<String, String> vertexServiceMethodAvailability) {
		setMapData(ServicNames.VERTEX.getName(), DataTypes.AVAILABILITY.getName(), vertexServiceMethodAvailability);
	}

	public Map<String, String> getVertexServiceMethodTimeout() {
		return getMapData(ServicNames.VERTEX.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setVertexServiceMethodTimeout(Map<String, String> vertexServiceMethodTimeout) {
		setMapData(ServicNames.VERTEX.getName(), DataTypes.TIMEOUTS.getName(), vertexServiceMethodTimeout);
	}

	
	public void setVantivCountryMapping(Map<String, String> vantivCountryMapping) {
		setMapData(ServicNames.VANTIV.getName(), DataTypes.COUNTRY.getName(), vantivCountryMapping);
	}
	
	public Map<String, String> getVantivCountryMapping() {
		return getMapData(ServicNames.VANTIV.getName(), DataTypes.COUNTRY.getName());
	}
	
	public void setVantivSoftDeclineMapping(Map<String, String> vantivSoftDecline) {
		setMapData(ServicNames.VANTIV.getName(), DataTypes.SOFT_DECLINE.getName(), vantivSoftDecline);
	}
	
	public Map<String, String> getVantivSoftDeclineMapping() {
		return getMapData(ServicNames.VANTIV.getName(), DataTypes.SOFT_DECLINE.getName());
	}
	
	public void setVantivProcessAsOfflineCombinationMapping(Map<String, String> processAsOffline) {
		setMapData(ServicNames.VANTIV.getName(), DataTypes.PROCESS_OFFLINE.getName(), processAsOffline);
	}
	
	public Map<String, String> getVantivProcessAsOfflineCombinationMapping() {
		return getMapData(ServicNames.VANTIV.getName(), DataTypes.PROCESS_OFFLINE.getName());
	}
	
	public Integer getInsecurePort() {
		return insecurePort;
	}

	public void setInsecurePort(Integer insecurePort) {
		this.insecurePort = insecurePort;
	}

	public Integer getSecurePort() {
		return securePort;
	}

	public void setSecurePort(Integer securePort) {
		this.securePort = securePort;
	}

	public List<String> getValidResponseStatusCodes() {
		return validResponseStatusCodes;
	}

	public void setValidResponseStatusCodes(List<String> validResponseStatusCodes) {
		this.validResponseStatusCodes = validResponseStatusCodes;
	}

	public Map<String, String> getVertexPhysicalOriginAddress() {
		return getMapData(ServicNames.VERTEX.getName(), DataTypes.PHYSICAL_ADDRESS.getName());
	}

	public void setVertexPhysicalOriginAddress(Map<String, String> vertexPhysicalOriginAddress) {
		setMapData(ServicNames.VERTEX.getName(), DataTypes.PHYSICAL_ADDRESS.getName(), vertexPhysicalOriginAddress);
	}

	public Map<String, String> getVertexAdministrativeOriginAddress() {
		return getMapData(ServicNames.VERTEX.getName(), DataTypes.ADMIN_ADDRESS.getName());
	}

	public void setVertexAdministrativeOriginAddress(Map<String, String> vertexAdministrativeOriginAddress) {
		setMapData(ServicNames.VERTEX.getName(), DataTypes.ADMIN_ADDRESS.getName(), vertexAdministrativeOriginAddress);
	}

	public Map<String, String> getCardinalServiceMethodTimeout() {
		return getMapData(ServicNames.CARDINAL.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setCardinalServiceMethodTimeout(Map<String, String> cardinalServiceMethodTimeout) {
		setMapData(ServicNames.CARDINAL.getName(), DataTypes.TIMEOUTS.getName(), cardinalServiceMethodTimeout);
	}

	public boolean isUseVaultedPasswords() {
		return useVaultedPasswords;
	}

	public void setUseVaultedPasswords(boolean useVaultedPasswords) {
		this.useVaultedPasswords = useVaultedPasswords;
	}

	public Map<String, String> getCardinalServiceMethodAvailability() {
		return getMapData(ServicNames.CARDINAL.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setCardinalServiceMethodAvailability(Map<String, String> cardinalServiceMethodAvailability) {
		setMapData(ServicNames.CARDINAL.getName(), DataTypes.AVAILABILITY.getName(), cardinalServiceMethodAvailability);
	}

	public Map<String, String> getCardinalStaticData() {
		return getMapData(ServicNames.CARDINAL.getName(), DataTypes.STATIC.getName());
	}

	public void setCardinalStaticData(Map<String, String> cardinalStaticData) {
		setMapData(ServicNames.CARDINAL.getName(), DataTypes.STATIC.getName(), cardinalStaticData);
	}


	public Map<String, String> getStorenetSoapActions() {
		return getMapData(ServicNames.STORENET.getName(), DataTypes.SOAP_ACTIONS.getName());
	}

	public void setStorenetSoapActions(Map<String, String> storenetSoapActions) {
		setMapData(ServicNames.STORENET.getName(), DataTypes.SOAP_ACTIONS.getName(), storenetSoapActions);
	}

	public Map<String, String> getStorenetServiceMethodAvailability() {
		return getMapData(ServicNames.STORENET.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setStorenetServiceMethodAvailability(Map<String, String> storenetServiceMethodAvailability) {
		setMapData(ServicNames.STORENET.getName(), DataTypes.AVAILABILITY.getName(), storenetServiceMethodAvailability);
	}

	public Map<String, String> getStorenetServiceMethodTimeout() {
		return getMapData(ServicNames.STORENET.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setStorenetServiceMethodTimeout(Map<String, String> storenetServiceMethodTimeout) {
		setMapData(ServicNames.STORENET.getName(), DataTypes.TIMEOUTS.getName(), storenetServiceMethodTimeout);
	}

	public Map<String, String> getStorenetStaticData() {
		return getMapData(ServicNames.STORENET.getName(), DataTypes.STATIC.getName());
	}

	public void setStorenetStaticData(Map<String, String> storenetStaticData) {
		setMapData(ServicNames.STORENET.getName(), DataTypes.STATIC.getName(), storenetStaticData);
	}
	
	public Map<String, String> getYantraServiceMethodAvailability() {
		return getMapData(ServicNames.YANTRA_INVENTORY.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setYantraServiceMethodAvailability(Map<String, String> yantraServiceMethodAvailability) {
		setMapData(ServicNames.YANTRA_INVENTORY.getName(), DataTypes.AVAILABILITY.getName(), yantraServiceMethodAvailability);
	}

	public Map<String, String> getYantraServiceMethodTimeout() {
		return getMapData(ServicNames.YANTRA_INVENTORY.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setYantraServiceMethodTimeout(Map<String, String> yantraServiceMethodTimeout) {
		setMapData(ServicNames.YANTRA_INVENTORY.getName(), DataTypes.TIMEOUTS.getName(), yantraServiceMethodTimeout);
	}

	public Map<String, String> getYantraStaticData() {
		return getMapData(ServicNames.YANTRA_INVENTORY.getName(), DataTypes.STATIC.getName());
	}

	public void setYantraStaticData(Map<String, String> yantraStaticData) {
		setMapData(ServicNames.YANTRA_INVENTORY.getName(), DataTypes.STATIC.getName(), yantraStaticData);
	}

	public Map<String, String> getRewardServiceMethodAvailability() {
		return getMapData(ServicNames.REWARDS.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setRewardServiceMethodAvailability(Map<String, String> rewardServiceMethodAvailability) {
		setMapData(ServicNames.REWARDS.getName(), DataTypes.AVAILABILITY.getName(), rewardServiceMethodAvailability);
	}

	public Map<String, String> getRewardServiceMethodTimeout() {
		return getMapData(ServicNames.REWARDS.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setRewardServiceMethodTimeout(Map<String, String> rewardServiceMethodTimeout) {
		setMapData(ServicNames.REWARDS.getName(), DataTypes.TIMEOUTS.getName(), rewardServiceMethodTimeout);
	}

	public Map<String, String> getRewardStaticData() {
		return getMapData(ServicNames.REWARDS.getName(), DataTypes.STATIC.getName());
	}

	public void setRewardStaticData(Map<String, String> rewardStaticData) {
		setMapData(ServicNames.REWARDS.getName(), DataTypes.STATIC.getName(), rewardStaticData);
	}

	public Map<String, String> getRewardRestResources() {
		return getMapData(ServicNames.REWARDS.getName(), DataTypes.REST_RESOURCES.getName());
	}

	public void setRewardRestResources(Map<String, String> rewardRestResources) {
		setMapData(ServicNames.REWARDS.getName(), DataTypes.REST_RESOURCES.getName(), rewardRestResources);
	}

	public Map<String, String> getAfterPayServiceMethodAvailability() {
		return getMapData(ServicNames.AFTER_PAY.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setAfterPayServiceMethodAvailability(Map<String, String> afterPayServiceMethodAvailability) {
		setMapData(ServicNames.AFTER_PAY.getName(), DataTypes.AVAILABILITY.getName(), afterPayServiceMethodAvailability);
	}

	public Map<String, String> getAfterPayServiceMethodTimeout() {
		return getMapData(ServicNames.AFTER_PAY.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setAfterPayServiceMethodTimeout(Map<String, String> afterPayServiceMethodTimeout) {
		setMapData(ServicNames.AFTER_PAY.getName(), DataTypes.TIMEOUTS.getName(), afterPayServiceMethodTimeout);
	}

	public Map<String, String> getAfterPayStaticData() {
		return getMapData(ServicNames.AFTER_PAY.getName(), DataTypes.STATIC.getName());
	}

	public void setAfterPayStaticData(Map<String, String> afterPayStaticData) {
		setMapData(ServicNames.AFTER_PAY.getName(), DataTypes.STATIC.getName(), afterPayStaticData);
	}

	public Map<String, String> getAfterPayRestResources() {
		return getMapData(ServicNames.AFTER_PAY.getName(), DataTypes.REST_RESOURCES.getName());
	}

	public void setAfterPayRestResources(Map<String, String> afterPayRestResources) {
		setMapData(ServicNames.AFTER_PAY.getName(), DataTypes.REST_RESOURCES.getName(), afterPayRestResources);
	}

	public Map<String, String> getRewardMockingResources() {
		return getMapData(ServicNames.REWARDS.getName(), DataTypes.MOCKING_RESOURCES.getName());
	}

	public void setRewardMockingResources(Map<String, String> rewardMockingResources) {
		setMapData(ServicNames.REWARDS.getName(), DataTypes.MOCKING_RESOURCES.getName(), rewardMockingResources);
	}

	public Map<String, String> getYantraOmsServiceMethodAvailability() {
		return getMapData(ServicNames.YANTRA_OMS.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setYantraOmsServiceMethodAvailability(Map<String, String> yantraOmsServiceMethodAvailability) {
		setMapData(ServicNames.YANTRA_OMS.getName(), DataTypes.AVAILABILITY.getName(), yantraOmsServiceMethodAvailability);
	}

	public Map<String, String> getYantraOmsServiceMethodTimeout() {
		return getMapData(ServicNames.YANTRA_OMS.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setYantraOmsServiceMethodTimeout(Map<String, String> yantraOmsServiceMethodTimeout) {
		setMapData(ServicNames.YANTRA_OMS.getName(), DataTypes.TIMEOUTS.getName(), yantraOmsServiceMethodTimeout);
	}

	public Map<String, String> getEmailServiceMethodAvailability() {
		return getMapData(ServicNames.SMTP.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setEmailServiceMethodAvailability(Map<String, String> emailServiceMethodAvailability) {
		setMapData(ServicNames.SMTP.getName(), DataTypes.AVAILABILITY.getName(), emailServiceMethodAvailability);
	}

	public Map<String, String> getEmailServiceMethodTimeout() {
		return getMapData(ServicNames.SMTP.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setEmailServiceMethodTimeout(Map<String, String> emailServiceMethodTimeout) {
		setMapData(ServicNames.SMTP.getName(), DataTypes.TIMEOUTS.getName(), emailServiceMethodTimeout);
	}

	public Map<String, String> getInventoryDecrementMethodAvailability() {
		return getMapData(ServicNames.INVENTORY_DECREMENT.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setInventoryDecrementMethodAvailability(Map<String, String> inventoryDecrementMethodAvailability) {
		setMapData(ServicNames.INVENTORY_DECREMENT.getName(), DataTypes.AVAILABILITY.getName(), inventoryDecrementMethodAvailability);
	}

	public Map<String, String> getInventoryDecrementServiceMethodTimeout() {
		return getMapData(ServicNames.INVENTORY_DECREMENT.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setInventoryDecrementServiceMethodTimeout(Map<String, String> inventoryDecrementServiceMethodTimeout) {
		setMapData(ServicNames.INVENTORY_DECREMENT.getName(), DataTypes.TIMEOUTS.getName(), inventoryDecrementServiceMethodTimeout);
	}

	public Map<String, String> getInventoryDecrementGiftCardTypes() {
		return getMapData(ServicNames.INVENTORY_DECREMENT.getName(), DataTypes.GIFT_CARDS.getName());
	}

	public void setInventoryDecrementGiftCardTypes(Map<String, String> inventoryDecrementStaticData) {
		setMapData(ServicNames.INVENTORY_DECREMENT.getName(), DataTypes.GIFT_CARDS.getName(), inventoryDecrementStaticData);
	}


	public Map<String, String> getOrderStatusMethodAvailability() {
		return getMapData(ServicNames.ORDER_STATUS.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setOrderStatusMethodAvailability(Map<String, String> orderStatusMethodAvailability) {
		setMapData(ServicNames.ORDER_STATUS.getName(), DataTypes.AVAILABILITY.getName(), orderStatusMethodAvailability);
	}

	public Map<String, String> getOrderStatusServiceMethodTimeout() {
		return getMapData(ServicNames.ORDER_STATUS.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setOrderStatusServiceMethodTimeout(Map<String, String> orderStatusServiceMethodTimeout) {
		setMapData(ServicNames.ORDER_STATUS.getName(), DataTypes.TIMEOUTS.getName(), orderStatusServiceMethodTimeout);
	}
	
	public Map<String, String> getUspsStaticData() {
		return getMapData(ServicNames.USPS.getName(), DataTypes.STATIC.getName());
	}


	public void setUspsStaticData(Map<String, String> uspsStaticData) {
		setMapData(ServicNames.USPS.getName(), DataTypes.STATIC.getName(), uspsStaticData);
	}

	public void setMapData(String serviceKey, String dataType, Map<String, String> data) {
		if (!dataMap.containsKey(serviceKey)) {
			dataMap.put(serviceKey, new HashMap<String, Map<String, String>>());
		}

		Map<String, Map<String, String>> serviceData = dataMap.get(serviceKey);

		if (!serviceData.containsKey(dataType)) {
			serviceData.put(dataType, new HashMap<String, String>());
		}

		serviceData.get(dataType).putAll(data);

	}

	public Map<String, String> getMapData(String serviceKey, String dataType) {
		if (dataMap.containsKey(serviceKey)) {
			if (dataMap.get(serviceKey).containsKey(dataType)) {
				return dataMap.get(serviceKey).get(dataType);
			}
		}

		return new HashMap<>();
	}
	
	public Map<String, String> getYantraRepricingServiceMethodAvailability() {
		return getMapData(ServicNames.YANTRA_REPRICING.getName(), DataTypes.AVAILABILITY.getName());
	}

	public void setYantraRepricingServiceMethodAvailability(Map<String, String> yantraRepricingServiceMethodAvailability) {
		setMapData(ServicNames.YANTRA_REPRICING.getName(), DataTypes.AVAILABILITY.getName(), yantraRepricingServiceMethodAvailability);
	}

	public Map<String, String> getYantraRepricingServiceMethodTimeout() {
		return getMapData(ServicNames.YANTRA_REPRICING.getName(), DataTypes.TIMEOUTS.getName());
	}

	public void setYantraRepricingServiceMethodTimeout(Map<String, String> yantraRepricingServiceMethodTimeout) {
		setMapData(ServicNames.YANTRA_REPRICING.getName(), DataTypes.TIMEOUTS.getName(), yantraRepricingServiceMethodTimeout);
	}
	public void setVantivFraudTestMapping(Map<String, String> vantivFraudTestMapping) {
		setMapData(ServicNames.VANTIV.getName(), DataTypes.FRAUD_TEST.getName(), vantivFraudTestMapping);
	}
	
	public Map<String, String> getVantivFraudTestMapping() {
		return getMapData(ServicNames.VANTIV.getName(), DataTypes.FRAUD_TEST.getName());
	}

	public void setVantivFraudOnlyTestMapping(Map<String, String> vantivFraudOnlyTestMapping) {
		setMapData(ServicNames.VANTIV.getName(), DataTypes.FRAUD_ONLY_TEST.getName(), vantivFraudOnlyTestMapping);
	}
	
	public Map<String, String> getVantivFraudOnlyTestMapping() {
		return getMapData(ServicNames.VANTIV.getName(), DataTypes.FRAUD_ONLY_TEST.getName());
	}

	public Map<String, String> getRequiresConnectionPool() {
		return requiresConnectionPool;
	}

	public void setRequiresConnectionPool(
			Map<String, String> requiresConnectionPool) {
		this.requiresConnectionPool = requiresConnectionPool;
	}

	public Map<String, String> getRequiresKeepAliveStratergy() {
		return requiresKeepAliveStratergy;
	}

	public void setRequiresKeepAliveStratergy(
			Map<String, String> requiresKeepAliveStratergy) {
		this.requiresKeepAliveStratergy = requiresKeepAliveStratergy;
	}

	public Map<String, String> getRequiresGracefulConnectionShutdown() {
		return requiresGracefulConnectionShutdown;
	}

	public void setRequiresGracefulConnectionShutdown(
			Map<String, String> requiresGracefulConnectionShutdown) {
		this.requiresGracefulConnectionShutdown = requiresGracefulConnectionShutdown;
	}

	public Map<String, String> getKeepAliveTimeout() {
		return keepAliveTimeout;
	}

	public void setKeepAliveTimeout(Map<String, String> keepAliveTimeout) {
		this.keepAliveTimeout = keepAliveTimeout;
	}
}
