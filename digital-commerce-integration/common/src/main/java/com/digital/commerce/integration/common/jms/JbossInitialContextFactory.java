package com.digital.commerce.integration.common.jms;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import atg.dms.patchbay.JMSInitialContextFactory;
import atg.nucleus.GenericService;

/*jmenon@atg.com */

public class JbossInitialContextFactory extends GenericService implements JMSInitialContextFactory {

	String	mINITIAL_CONTEXT_FACTORY	= "org.jnp.interfaces.NamingContextFactory";

	public void setINITIAL_CONTEXT_FACTORY( String s ) {
		mINITIAL_CONTEXT_FACTORY = s;
	}

	public String getINITIAL_CONTEXT_FACTORY() {
		return mINITIAL_CONTEXT_FACTORY;
	}

	String	mPROVIDER_URL	= "localhost";

	public void setPROVIDER_URL( String s ) {
		mPROVIDER_URL = s;
	}

	public String getPROVIDER_URL() {
		return mPROVIDER_URL;
	}

	public Context createInitialContext( String pProviderName, String pUsername, String pPassword, String pClientId ) throws NamingException {
		logDebug( "createInitialContext()" );
		logDebug( "pProviderName = " + pProviderName );

		Properties h = new Properties();
		h.put( Context.INITIAL_CONTEXT_FACTORY, mINITIAL_CONTEXT_FACTORY );
		h.put( Context.PROVIDER_URL, mPROVIDER_URL );

		return new InitialContext();
	}
}
