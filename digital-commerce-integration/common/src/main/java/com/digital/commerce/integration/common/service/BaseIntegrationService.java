package com.digital.commerce.integration.common.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import javax.xml.soap.SOAPBody;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.util.PropertyUtil;

import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;

/**
 * Base service class that allows its children to have several convenience
 * methods and hooks for use within a dynamo server.
 * 
 * Added Hooks for postStartup and postShutdown that allow components to init /
 * finalize themselves.
 * 
 * Contains methods to help determine if a service or service method should be
 * run, setting default timeouts, etc.
 * 
 * @author JM406760
 *
 */
public abstract class BaseIntegrationService extends BaseService implements FaultHandler {
	protected static final DigitalLogger logger = DigitalLogger.getLogger(BaseIntegrationService.class);

	protected static final String CONNECTION_TIMEOUT_SUFFIX = "_connect";
	protected static final String REQUEST_TIMEOUT_SUFFIX = "_request";
	private static final String TO_CONNECT = "connect";
	private static final String TO_REQUEST = "request";
	private static final String SERVICE_ENABLED = "service";

	private WebServicesConfig webServicesConfig;

	protected abstract String getService();

	public boolean isServiceEnabled() {
		String serviceEnabled = PropertyUtil.getMappedProperty(
				getWebServicesConfig().getMapData(getService(), WebServicesConfig.DataTypes.AVAILABILITY.getName()),
				SERVICE_ENABLED).toString();

		boolean isActive = false;
		
		if (serviceEnabled == null) {
			isActive = true;
		} else {
			if (serviceEnabled.equalsIgnoreCase("y") || serviceEnabled.equalsIgnoreCase("true")  || serviceEnabled.equalsIgnoreCase("t")) {
				isActive = true;
			} 
		}
		
		logServiceStatus(getService(), isActive);
		
		return isActive;
	}

	public boolean isServiceMethodEnabled(String serviceMethodName) {
		return doRunService(serviceMethodName);
	}

	public String getServiceEnabled() {
		return PropertyUtil.getMappedProperty(
				getWebServicesConfig().getMapData(getService(), WebServicesConfig.DataTypes.AVAILABILITY.getName()),
				SERVICE_ENABLED).toString();
	}

	public void setServiceEnabled(String serviceEnabled) {
		PropertyUtil.setMappedProperty(
				getWebServicesConfig().getMapData(getService(), WebServicesConfig.DataTypes.AVAILABILITY.getName()),
				SERVICE_ENABLED, serviceEnabled);
	}

	public Map<String, String> getDefaultTimeout() {
		return getWebServicesConfig().getDefaultTimeout();
	}

	public Integer getDefaultConnectionTimeout() {
		return PropertyUtil.getMappedProperty(getDefaultTimeout(), TO_CONNECT).toInteger();
	}

	public void setDefaultConnectionTimeout(Integer defaultConnectionTimeout) {
		PropertyUtil.setMappedProperty(getDefaultTimeout(), TO_CONNECT, defaultConnectionTimeout);
	}

	public Integer getDefaultRequestTimeout() {
		return PropertyUtil.getMappedProperty(getDefaultTimeout(), TO_REQUEST).toInteger();
	}

	public void setDefaultRequestTimeout(Integer defaultRequestTimeout) {
		PropertyUtil.setMappedProperty(getDefaultTimeout(), TO_REQUEST, defaultRequestTimeout);
	}

	public List<String> getValidResponseStatusCodes() {
		return getWebServicesConfig().getValidResponseStatusCodes();
	}

	protected boolean doRunService(String serviceMethodName) {
		boolean isActive = false;
		
		if (isServiceEnabled()) {
			if (getWebServicesConfig().getMapData(getService(), WebServicesConfig.DataTypes.AVAILABILITY.getName())
					.containsKey(serviceMethodName)) {
				String value = getWebServicesConfig()
						.getMapData(getService(), WebServicesConfig.DataTypes.AVAILABILITY.getName())
						.get(serviceMethodName);
								
				if (value == null) {
					isActive = false;
				} else {
					if (value.equalsIgnoreCase("y") || value.equalsIgnoreCase("true")  || value.equalsIgnoreCase("t")) {
						isActive = true;
					} else {
						isActive = false;
					}
				}
				
			} else {
				isActive = false;
			}
		}

		logServiceMethodStatus(getService(), serviceMethodName, isActive);
		
		return isActive;
	}

	protected int getConnectionTimeout(String serviceMethodName) {

		String serviceConnectionTimeoutName = serviceMethodName + CONNECTION_TIMEOUT_SUFFIX;

		try {
			if (getWebServicesConfig().getMapData(getService(), WebServicesConfig.DataTypes.TIMEOUTS.getName())
					.containsKey(serviceConnectionTimeoutName)) {
				return Integer.valueOf(
						getWebServicesConfig().getMapData(getService(), WebServicesConfig.DataTypes.TIMEOUTS.getName())
								.get(serviceConnectionTimeoutName))
						.intValue();
			} else {
				logger.warn("Specific serviceMethod connection timeout not supplied for " + serviceMethodName);

				return getDefaultConnectionTimeout().intValue();
			}
		} catch (Exception e) {
			logger.warn("Exception retrieving serviceMethod connection timeout");
		}

		return getDefaultConnectionTimeout().intValue();
	}

	protected int getRequestTimeout(String serviceMethodName) {

		String serviceRequestTimeoutName = serviceMethodName + REQUEST_TIMEOUT_SUFFIX;

		try {
			if (getWebServicesConfig().getMapData(getService(), WebServicesConfig.DataTypes.TIMEOUTS.getName())
					.containsKey(serviceRequestTimeoutName)) {
				return Integer.valueOf(
						getWebServicesConfig().getMapData(getService(), WebServicesConfig.DataTypes.TIMEOUTS.getName())
								.get(serviceRequestTimeoutName))
						.intValue();
			} else {
				logger.warn("Specific serviceMethod request timeout not supplied for " + serviceMethodName);

				return getDefaultRequestTimeout().intValue();
			}
		} catch (Exception e) {
			logger.warn("Exception retrieving serviceMethod request timeout");
		}

		return getDefaultRequestTimeout().intValue();
	}

	protected void startPerformanceMonitorOperation(String serviceMethod) {
		PerformanceMonitor.startOperation(getComponentName(), serviceMethod);
	}

	protected void endPerformanceMonitorOperation(String serviceMethod, Boolean isExceptionThrown) {
		try {
			PerformanceMonitor.endOperation(getComponentName(), serviceMethod);
		} catch (PerfStackMismatchException e) {
			logger.warn(String.format("%s-%s PerfStackMismatchException", getComponentName(), serviceMethod), e);
		}
	}

	protected void validateHttpResponse(HttpServiceResponse httpServiceResponse)
			throws DigitalIntegrationBusinessException {
		String statusCode = String.valueOf((httpServiceResponse.getStatusCode()));
		if (!getValidResponseStatusCodes().contains(statusCode)) {

			throw new DigitalIntegrationBusinessException(
					String.format("Invalid Client Service Response: %s for %s ", statusCode, getComponentName()),
					ErrorCodes.ERROR_RESPONSE.getCode());
		}
	}

	public static String urlEncodeString(String stringToEncode) throws UnsupportedEncodingException {
		if (stringToEncode == null) {
			return null;
		} else {
			return URLEncoder.encode(stringToEncode, StandardCharsets.UTF_8.name());
		}
	}

	public WebServicesConfig getWebServicesConfig() {
		return webServicesConfig;
	}

	public void setWebServicesConfig(WebServicesConfig webServicesConfig) {
		this.webServicesConfig = webServicesConfig;
	}

	public String getMapData(WebServicesConfig.DataTypes dataType, String dataKey) {
		return getWebServicesConfig().getMapData(getService(), dataType.getName()).get(dataKey);
	}

	public Map<String, String> getMapData(WebServicesConfig.DataTypes dataType) {
		return getWebServicesConfig().getMapData(getService(), dataType.getName());
	}

	public String getFaultDetails(SOAPBody soapBody) throws Exception {
		if (soapBody.hasFault()) {
			return soapBody.getFault().getFaultString();
		} else {
			return "";
		}
	}

	protected void logServiceStatus(String serviceName, boolean isActive) {
		if (logger.isDebugEnabled()) {
			if (!DigitalStringUtil.isEmpty(serviceName)) {
				logger.debug(String.format("%s service is %s", serviceName,
						(isActive == true ? "ACTIVE" : "INACTIVE")));
			}
		}
	}

	protected void logServiceMethodStatus(String serviceName, String methodName, boolean isActive) {
		if (logger.isDebugEnabled()) {
			if (!DigitalStringUtil.isEmpty(serviceName) && !DigitalStringUtil.isEmpty(methodName)) {
				logger.debug(String.format("%s / %s service / method is %s", serviceName, methodName,
						(isActive == true ? "ACTIVE" : "INACTIVE")));
			}
		}
	}
}
