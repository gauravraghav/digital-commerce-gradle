package com.digital.commerce.integration.common.service;

import java.util.Map;

import com.digital.commerce.common.services.ServiceAvailabilityAggregator;

public class IntegrationServiceAvailabilityAggregator implements ServiceAvailabilityAggregator {

	private Service addressValidationService;
	private Service storedValueService;
	private Service authorizationService;
	private Service taxService;
	private Service paymentService;
	private Service rewardService;
	private Service orderManagementService;
	private Service emailService;
	private Service inventoryService;
	private Service yantraInventoryService;

	private Service afterPayService;
	
	public Service getAddressValidationService() {
		return addressValidationService;
	}
	public void setAddressValidationService(Service addressValidationService) {
		this.addressValidationService = addressValidationService;
	}
	
	public Service getStoredValueService() {
		return storedValueService;
	}
	public void setStoredValueService(Service storedValueService) {
		this.storedValueService = storedValueService;
	}
	public Service getAuthorizationService() {
		return authorizationService;
	}
	public void setAuthorizationService(Service authorizationService) {
		this.authorizationService = authorizationService;
	}
	public Service getTaxService() {
		return taxService;
	}
	public void setTaxService(Service taxService) {
		this.taxService = taxService;
	}
	public Service getPaymentService() {
		return paymentService;
	}
	public void setPaymentService(Service paymentService) {
		this.paymentService = paymentService;
	}
	public Service getRewardService() {
		return rewardService;
	}
	public void setRewardService(Service rewardService) {
		this.rewardService = rewardService;
	}
	public Service getOrderManagementService() {
		return orderManagementService;
	}
	public void setOrderManagementService(Service orderManagementService) {
		this.orderManagementService = orderManagementService;
	}
	public Service getEmailService() {
		return emailService;
	}
	public void setEmailService(Service emailService) {
		this.emailService = emailService;
	}
	public Service getInventoryService() {
		return inventoryService;
	}
	public void setInventoryService(Service inventoryService) {
		this.inventoryService = inventoryService;
	}
	public Service getYantraInventoryService() {
		return yantraInventoryService;
	}
	public void setYantraInventoryService(Service yantraInventoryService) {
		this.yantraInventoryService = yantraInventoryService;
	}
	public Service getAfterPayService() {return afterPayService;}
	public void setAfterPayService(Service afterPayService) {this.afterPayService = afterPayService;}



	/* (non-Javadoc)
	 * @see com.dsw.commerce.integration.common.service.ServiceAvailabilityAggregator2#isAddressValidationServiceEnabled()
	 */
	@Override
	public boolean isAddressValidationServiceEnabled() {
		return addressValidationService != null ? addressValidationService.isServiceEnabled() : false;
	}
	
	
	/* (non-Javadoc)
	 * @see com.dsw.commerce.integration.common.service.ServiceAvailabilityAggregator2#isStoredValueServiceEnabled()
	 */
	@Override
	public boolean isStoredValueServiceEnabled() {
		return storedValueService != null ? storedValueService.isServiceEnabled() : false;
	}
	/* (non-Javadoc)
	 * @see com.dsw.commerce.integration.common.service.ServiceAvailabilityAggregator2#isAuthorizationServiceEnabled()
	 */
	@Override
	public boolean isAuthorizationServiceEnabled() {
		return authorizationService != null ? authorizationService.isServiceEnabled() : false;
	}
	/* (non-Javadoc)
	 * @see com.dsw.commerce.integration.common.service.ServiceAvailabilityAggregator2#isTaxServiceEnabled()
	 */
	@Override
	public boolean isTaxServiceEnabled() {
		return taxService != null ? taxService.isServiceEnabled() : false;
	}
	/* (non-Javadoc)
	 * @see com.dsw.commerce.integration.common.service.ServiceAvailabilityAggregator2#isPaymentServiceEnabled()
	 */
	@Override
	public boolean isPaymentServiceEnabled() {
		return paymentService != null ? paymentService.isServiceEnabled() : false;
	}
	/* (non-Javadoc)
	 * @see com.dsw.commerce.integration.common.service.ServiceAvailabilityAggregator2#isRewardServiceEnabled()
	 */
	@Override
	public boolean isRewardServiceEnabled() {
		return rewardService != null ? rewardService.isServiceEnabled() : false;
	}
	/* (non-Javadoc)
	 * @see com.dsw.commerce.integration.common.service.ServiceAvailabilityAggregator2#isOrderManagementServiceEnabled()
	 */
	@Override
	public boolean isOrderManagementServiceEnabled() {
		return orderManagementService != null ? orderManagementService.isServiceEnabled() : false;
	}
	/* (non-Javadoc)
	 * @see com.dsw.commerce.integration.common.service.ServiceAvailabilityAggregator2#isEmailServiceEnabled()
	 */
	@Override
	public boolean isEmailServiceEnabled() {
		return emailService != null ? emailService.isServiceEnabled() : false;
	}
	/* (non-Javadoc)
	 * @see com.dsw.commerce.integration.common.service.ServiceAvailabilityAggregator2#isInventoryServiceEnabled()
	 */
	@Override
	public boolean isInventoryServiceEnabled() {
		return inventoryService != null ? inventoryService.isServiceEnabled() : false;
	}
	/* (non-Javadoc)
	 * @see com.dsw.commerce.integration.common.service.ServiceAvailabilityAggregator2#isYantraInventoryServiceEnabled()
	 */
	@Override
	public boolean isYantraInventoryServiceEnabled() {
		return yantraInventoryService != null ? yantraInventoryService.isServiceEnabled() : false;
	}

	@Override
	public boolean isAfterPayServiceEnabled() {
		return afterPayService != null ? afterPayService.isServiceEnabled() : false;
	}

	public Map<String, Boolean> getAddressValidationServiceMethodsEnabled() {
		return addressValidationService.getServiceMethodsEnabled();
		
	}
	@Override
	public Map<String, Boolean> getStoredValueServiceMethodsEnabled() {
		return storedValueService.getServiceMethodsEnabled();
	}
	@Override
	public Map<String, Boolean> getAuthorizationServiceMethodsEnabled() {
		return authorizationService.getServiceMethodsEnabled();
	}
	@Override
	public Map<String, Boolean> getTaxServiceMethodsEnabled() {
		return taxService.getServiceMethodsEnabled();
	}
	@Override
	public Map<String, Boolean> getPaymentServiceMethodsEnabled() {
		return paymentService.getServiceMethodsEnabled();
	}
	@Override
	public Map<String, Boolean> getRewardServiceMethodsEnabled() {
		return rewardService.getServiceMethodsEnabled();
	}
	@Override
	public Map<String, Boolean> getInventoryServiceMethodsEnabled() {
		return inventoryService.getServiceMethodsEnabled();
	}
	@Override
	public Map<String, Boolean> getYantraInventoryServiceMethodsEnabled() {
		return yantraInventoryService.getServiceMethodsEnabled();
	}
	@Override
	public Map<String, Boolean> getAfterPayServiceMethodsEnabled() {
		return afterPayService.getServiceMethodsEnabled();
	}
	
}
