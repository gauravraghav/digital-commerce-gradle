package com.digital.commerce.integration.common.service;

import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

public interface IntegrationService {

	/**
	 * Will be called when the component is started
	 * 
	 * @throws DigitalIntegrationException
	 */
	public void postStartup() throws DigitalIntegrationException;
	
	/**
	 * Will be called when the component is stopped
	 * 
	 * @throws DigitalIntegrationException
	 */
	public void postShutdown() throws DigitalIntegrationException;


}
