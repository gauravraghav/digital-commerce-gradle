package com.digital.commerce.integration.common.service.client;

import java.util.HashMap;
import java.util.Map;

public class ServiceRequest {
	public enum ServiceContentType {
		XML ("text/xml"),
		JSON ("application/json");

		private String contentType;
		
		ServiceContentType(String contentType) {
			this.contentType = contentType;
		}

		public String getContentType() {
			return contentType;
		}		
	}
	
	public enum ServiceHttpMethod {
		POST,
		PUT,
		GET,
		DELETE
    }
	
	private String serviceUri;
	private String body;
	private Map<String, String> headerEntries = new HashMap<>();
	private ServiceContentType serviceContentType;
	private ServiceHttpMethod serviceMethodType;
	private int connectionTimeout = 0;
	private int requestTimeout = 0;
	private String soapAction;
	private String soapVersion;
	private String serviceMethodName;
	
	public ServiceRequest(int connectionTimeout, int requestTimeout, String serviceUri, ServiceContentType serviceContentType, ServiceHttpMethod serviceMethodType, String serviceMethodName) {
		this.connectionTimeout = connectionTimeout;
		this.requestTimeout = requestTimeout;
		this.serviceUri = serviceUri;
		this.serviceContentType = serviceContentType;
		this.serviceMethodType = serviceMethodType;
		this.serviceMethodName = serviceMethodName;
	}
	
	public String getServiceUri() {
		return serviceUri;
	}
	public void setServiceUri(String serviceUri) {
		this.serviceUri = serviceUri;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Map<String, String> getHeaderEntries() {
		return headerEntries;
	}
	public void setHeaderEntries(Map<String, String> headerEntries) {
		this.headerEntries = headerEntries;
	}
	public ServiceContentType getServiceContentType() {
		return serviceContentType;
	}
	public void setServiceContentType(ServiceContentType serviceContentType) {
		this.serviceContentType = serviceContentType;
	}
	public ServiceHttpMethod getServiceMethodType() {
		return serviceMethodType;
	}
	public void setServiceMethodType(ServiceHttpMethod serviceMethodType) {
		this.serviceMethodType = serviceMethodType;
	}
	public String getSoapAction() {
		return soapAction;
	}
	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}
	public String getSoapVersion() {
		return soapVersion;
	}
	public void setSoapVersion(String soapVersion) {
		this.soapVersion = soapVersion;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public int getRequestTimeout() {
		return requestTimeout;
	}

	public void setRequestTimeout(int requestTimeout) {
		this.requestTimeout = requestTimeout;
	}

	public String getServiceMethodName() {
		return serviceMethodName;
	}

	public void setServiceMethodName(String serviceMethodName) {
		this.serviceMethodName = serviceMethodName;
	}
	
	
	
}
