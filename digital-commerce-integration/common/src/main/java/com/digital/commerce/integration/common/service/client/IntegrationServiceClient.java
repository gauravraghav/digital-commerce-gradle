package com.digital.commerce.integration.common.service.client;

import atg.core.util.Base64;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.common.service.FaultHandler;
import com.digital.commerce.integration.util.PropertyUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.conn.routing.RouteInfo.LayerType;
import org.apache.http.conn.routing.RouteInfo.TunnelType;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.BrowserCompatHostnameVerifier;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.BasicClientConnectionManager;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 * This class serves as the parent of all service clients and provides all the
 * connectivity needed for http / https communication.
 *
 * @author JM406760
 *
 */
public abstract class IntegrationServiceClient extends BaseIntegrationService {
	private static final DigitalLogger logger = DigitalLogger.getLogger(IntegrationServiceClient.class);

	private static final String PROXY_HOST = "host";
	private static final String PROXY_PORT = "port";
	private static final String PROXY_USER = "user";
	private static final String PROXY_PASSWORD = "password";  //NOSONAR

	protected static final String HEADER_SOAP_ACTION = "SOAPAction";

	private static final String CONNECTION_RETRIEVE_TIMEOUT = "connectionRetrieveTimeout";
	private static final String PER_ROUTE_MAX_CONNECTIONS = "perRouteMaxConnections";
	private static final String MAX_CONNECTIONS = "maxConnections";
	private static final String IDLE_CONNECTION_TIMEOUT = "idleConnectionTimeout";

	private Integer insecurePort = 80;
	private Integer securePort = 443;

	private Map<String, HttpRoutePlanner> routePlanners = new HashMap<>();

	private String serviceName = "";

	private WebServicesConfig webServicesConfig;

	public PoolingClientConnectionManager getConnectionManager() {
		return connectionManager;
	}

	public void setConnectionManager(
			PoolingClientConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	private PoolingClientConnectionManager connectionManager;

	private static final String DEFAULT_CHARSET = "UTF-8";

	@Override
	public void postStartup() throws DigitalIntegrationException {
		initialize();
	}

	@Override
	public void postShutdown() throws DigitalIntegrationException {
		connectionManager.closeExpiredConnections();
		connectionManager.closeIdleConnections(Integer.valueOf(
				getWebServicesConfig().getConnectionManagerConfig().get(IDLE_CONNECTION_TIMEOUT)),
				TimeUnit.MILLISECONDS);
	}

	public void initialize() throws DigitalIntegrationException {
    connectionManager = new PoolingClientConnectionManager();
    connectionManager.setDefaultMaxPerRoute(Integer.valueOf(
        getWebServicesConfig().getConnectionManagerConfig().get(PER_ROUTE_MAX_CONNECTIONS)));
    connectionManager.setMaxTotal(Integer.valueOf(
        getWebServicesConfig().getConnectionManagerConfig().get(MAX_CONNECTIONS)));
	}


	protected DefaultHttpClient initializeHttpClient() throws DigitalIntegrationException {
		DefaultHttpClient httpClient = null;
		try {
			if(!doesRequireConnectionPool()) {
				// Create basic connection manager with HTTP scheme
				BasicClientConnectionManager cm = new BasicClientConnectionManager(getSchemeRegistry());

				// Create basic httpclient and set the timeout values
				httpClient = new DefaultHttpClient(cm);
				determineHttpConnectionProxy(httpClient);
				// Set the timeout for the length of time the manager will wait to retrieve a connection
				// before an exception is thrown.

				httpClient.getParams().setParameter(ClientPNames.CONN_MANAGER_TIMEOUT, Long.valueOf(
						getWebServicesConfig().getConnectionManagerConfig().get(CONNECTION_RETRIEVE_TIMEOUT)));

				boolean retryEnabled = false;
				Integer retryCount = getRetryCount();
				if (retryCount != null && retryCount > 0) {
					retryEnabled = true;
				}

				DefaultHttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(retryCount,
						retryEnabled);
				httpClient.setHttpRequestRetryHandler(retryHandler);
			}
			else{
        // Create basic httpclient using connection from pool and set the timeout values
        BasicHttpParams httpParams = new BasicHttpParams();
        httpParams.setParameter(CoreConnectionPNames.SO_LINGER, 0);
        httpParams.setParameter(CoreConnectionPNames.STALE_CONNECTION_CHECK, false);
				httpParams.setParameter(ClientPNames.CONN_MANAGER_TIMEOUT, Long.valueOf(
						getWebServicesConfig().getConnectionManagerConfig().get(CONNECTION_RETRIEVE_TIMEOUT)));
        httpClient = new DefaultHttpClient(connectionManager, httpParams);

        if(doesRequireKeepAliveStratergy()) {
					ConnectionKeepAliveStrategy myStrategy = new ConnectionKeepAliveStrategy() {
						@Override
						public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
							// Honor 'keep-alive' header
							HeaderElementIterator it = new BasicHeaderElementIterator
									(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
							while (it.hasNext()) {
								HeaderElement he = it.nextElement();
								String param = he.getName();
								String value = he.getValue();
								if (value != null && param.equalsIgnoreCase
										("timeout")) {
									return Long.parseLong(value) * 1000;
								}
							}
							// otherwise keep alive duration in ms based on configuration
							int keepAliveTimeout = Integer.valueOf(
									getWebServicesConfig().getKeepAliveTimeout().get(getServiceName()));
							return keepAliveTimeout;
						}
					};

					httpClient.setKeepAliveStrategy(myStrategy);
				}
				determineHttpConnectionProxy(httpClient);
				boolean retryEnabled = false;
				Integer retryCount = getRetryCount();
				if (retryCount != null && retryCount > 0) {
					retryEnabled = true;
				}

				DefaultHttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(retryCount,
						retryEnabled);
				httpClient.setHttpRequestRetryHandler(retryHandler);
				if(logger.isDebugEnabled()) {
					logger.debug("Pool stats : " + connectionManager.getTotalStats());
				}
			}


		} catch (Exception e) {
			throw new DigitalIntegrationSystemException("Exception thrown initializing service: " + getComponentName(),
					ErrorCodes.CONFIG.getCode(), e);
		}
		return httpClient;
	}

	protected SchemeRegistry getSchemeRegistry()
			throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException {
		SchemeRegistry schemeRegistry = new SchemeRegistry();

		if (securePort == null) {
			securePort = 443;
		}
		schemeRegistry.register(new Scheme("https", securePort, getSSLSocketFactory()));

		if (insecurePort == null) {
			insecurePort = 80;
		}

		schemeRegistry.register(new Scheme("http", insecurePort, PlainSocketFactory.getSocketFactory()));

		return schemeRegistry;
	}

	protected void determineHttpConnectionProxy(DefaultHttpClient httpClient) {
		if (doesRequireProxy()) {

			if (getProxyHost() != null && getProxyPort() != null) {
				AuthScope authScope = new AuthScope(getProxyHost(), getProxyPort());

				// set up auth for authenticated proxy
				if (httpClient.getCredentialsProvider() == null  || httpClient.getCredentialsProvider().getCredentials(authScope) == null) {
					CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
					Credentials credentials = new UsernamePasswordCredentials(getProxyUser(), getProxyPassword());
					credentialsProvider.setCredentials(authScope, credentials);

					httpClient.setCredentialsProvider(credentialsProvider);
				}

				String plannerKey = getProxyHost() + "_" + getProxyPort() + "_" + getServiceName();

				HttpRoutePlanner routePlanner;

				if (getRoutePlanners().containsKey(plannerKey)) {
					if (logger.isDebugEnabled()) {
						logger.debug(String.format("%s does require proxy - using existing route planner: %s",
								getComponentName(), plannerKey));
					}
					routePlanner = getRoutePlanners().get(plannerKey);

					if (!httpClient.getRoutePlanner().equals(routePlanner)) {
						httpClient.setRoutePlanner(routePlanner);
					}

				} else {
					if (logger.isDebugEnabled()) {
						logger.debug(String.format("%s does require proxy - creating new route planner",
								getComponentName()));
					}
					HttpHost proxy = new HttpHost(getProxyHost(), getProxyPort());
					routePlanner = new PerConnectionProxyRoutePlanner(proxy);
					getRoutePlanners().put(plannerKey, routePlanner);

					httpClient.setRoutePlanner(routePlanner);
					httpClient.getParams().setParameter("authenticationPreemptive", true);
				}
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug(String.format("%s does not require proxy", getComponentName()));
			}
			HttpRoutePlanner routePlanner;

			if (getRoutePlanners().containsKey(getServiceName())) {
				routePlanner = getRoutePlanners().get(getServiceName());

				if (!httpClient.getRoutePlanner().equals(routePlanner)) {
					httpClient.setRoutePlanner(routePlanner);
				}

			} else {

				routePlanner = new PerConnectionProxyRoutePlanner(null);
				getRoutePlanners().put(getServiceName(), routePlanner);

				httpClient.setRoutePlanner(routePlanner);
				httpClient.getParams().setParameter("authenticationPreemptive", true);
			}
		}
	}

	protected SSLSocketFactory getSSLSocketFactory()
			throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException {

		SSLContext sslContext = null;

		if (doesRequireTLS12()) {
			sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null, null, null);
		} else {
			sslContext = SSLContext.getDefault();
		}

		SSLSocketFactory socketFactory = new SSLSocketFactory(sslContext, new BrowserCompatHostnameVerifier());

		return socketFactory;

	}

	@SuppressWarnings("static-access")
	public HttpServiceResponse processHttpRequest(ServiceRequest serviceRequest)
			throws DigitalIntegrationException {
		HttpUriRequest request = null;
		final String CONTENT_TYPE_HEADER = "Content-Type";

		long startTime = 0l;
		HttpResponse response = null;
		String serviceName= null;
		String methodName = null;
		String serviceURL = null;
		DefaultHttpClient httpClient = null;
		String requestString = null;
		int responseStatus=0;
		try {
			requestString = serviceRequest.getBody();
			serviceName = this.getServiceName();
			methodName = serviceRequest.getServiceMethodName();
			serviceURL = serviceRequest.getServiceUri();

			if (serviceRequest.getServiceMethodType().equals(serviceRequest.getServiceMethodType().POST)) {
				request = createPostRequest(serviceRequest.getConnectionTimeout(), serviceRequest.getRequestTimeout(),
						serviceURL, serviceRequest.getServiceContentType().getContentType(),
						serviceRequest.getHeaderEntries());

				if (!DigitalStringUtil.isEmpty(requestString)) {
					StringEntity bodyEntity = new StringEntity(requestString);
					bodyEntity.setContentType(serviceRequest.getServiceContentType().getContentType());

					((HttpPost) request).setEntity(bodyEntity);
				}
			}else if (serviceRequest.getServiceMethodType().equals(serviceRequest.getServiceMethodType().PUT)) {
				request = createPutRequest(serviceRequest.getConnectionTimeout(), serviceRequest.getRequestTimeout(),
						serviceURL, serviceRequest.getServiceContentType().getContentType(),
						serviceRequest.getHeaderEntries());

				if (!DigitalStringUtil.isEmpty(requestString)) {
					StringEntity bodyEntity = new StringEntity(requestString);
					bodyEntity.setContentType(serviceRequest.getServiceContentType().getContentType());

					((HttpPut) request).setEntity(bodyEntity);
				}
			} else if (serviceRequest.getServiceMethodType().equals(serviceRequest.getServiceMethodType().GET)) {
				request = createGetRequest(serviceRequest.getConnectionTimeout(), serviceRequest.getRequestTimeout(),
						serviceURL, serviceRequest.getHeaderEntries());
			}else if (serviceRequest.getServiceMethodType().equals(serviceRequest.getServiceMethodType().DELETE)) {
				request = createDeleteRequest(serviceRequest.getConnectionTimeout(), serviceRequest.getRequestTimeout(),
						serviceURL, serviceRequest.getHeaderEntries());
			}

			// create httpclient
			httpClient = initializeHttpClient();

			startTime = System.nanoTime();

			if(logger.isDebugEnabled()) {
				if (connectionManager != null) {
					logger.debug(
							"Before - Leased Connections = " + connectionManager.getTotalStats().getLeased());
					logger.debug("Before - Available Connections = " + connectionManager.getTotalStats()
							.getAvailable());
				}
			}

			response = httpClient.execute(request);

			if(logger.isDebugEnabled()) {
				if (connectionManager != null) {
					logger.debug(
							"After - Leased Connections = " + connectionManager.getTotalStats().getLeased());
					logger.debug("After - Available Connections = " + connectionManager.getTotalStats()
							.getAvailable());
				}
			}
		
			HttpServiceResponse httpServiceResponse = new HttpServiceResponse();

			HttpEntity entity = response.getEntity();

			if (null != entity) {
				String content;
				if(null != entity.getContentType()) {
					String contentType = entity.getContentType().getValue();
					String charset = getResponseCharset(contentType);
					content = EntityUtils.toString(entity, charset);
				}
				else{
					content = EntityUtils.toString(entity);
				}
				httpServiceResponse.setEntityString(content);
			}

			/**
			 * Apigee seems to be sending non-JSON response under load (from 08/13/2018 dry run). The content type
			 * header is being read so that we do not cause JSON parser exception all over the place in ATG logs
			 */
			if(response.containsHeader(CONTENT_TYPE_HEADER) && null != response.getFirstHeader(CONTENT_TYPE_HEADER)) {
				httpServiceResponse.setContentType(response.getFirstHeader(CONTENT_TYPE_HEADER).getValue());
			}
			responseStatus = response.getStatusLine().getStatusCode();
			httpServiceResponse.setStatusCode(responseStatus);

			if (doesRequireConnectionPool() && doesRequireGracefulConnectionShutdown()) {
				// detect idle and expired connections and close them
				IdleConnectionMonitorThread staleMonitor = new IdleConnectionMonitorThread(
						connectionManager);
				staleMonitor.start();
			}

			return httpServiceResponse;
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder()
					.append("ProcessHttpRequest Exception: ")
					.append(e.toString()).append(" : ").append(serviceName)
					.append(" : ").append(methodName)
					.append(" : ").append(""); //Never log request wihout PII masking
			logger.error(sb.toString());
			logger.error("Exception from httpclient call ", e);
			if (response != null) {
				EntityUtils.consumeQuietly(response.getEntity());
			}
			throw new DigitalIntegrationSystemException("ProcessHttpRequest Error",
					ErrorCodes.COMM_ERROR.getCode(), e);
		} finally {
			if(logger.isDebugEnabled()) {
				logger.debug("Invoked End Point " + serviceURL + " for method Name " + methodName);
			}
			StringBuilder sb = new StringBuilder().append(serviceName).append(" : ").append(methodName)
					.append(": ").append(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime)).append("ms").append(" : httpStatus ").append(responseStatus);
			logger.info(sb.toString());

			// shutdown connection manager if connection pool is not used
			if(null != httpClient) {
				if(!doesRequireConnectionPool()){
					httpClient.getConnectionManager().shutdown();
				}
			}
		}

	}

	public HttpServiceResponse processHttpRequest(ServiceRequest serviceRequest, String username, String password, boolean includeTrailingColon) throws DigitalIntegrationException {


		String auth = username + ":" + password;
		if(includeTrailingColon) {
			auth += ":";
		}
		return processHttpRequest(serviceRequest, base64Encode(auth));

	}
	public HttpServiceResponse processHttpRequest(ServiceRequest serviceRequest, String username, String password) throws DigitalIntegrationException {
		return processHttpRequest(serviceRequest, username, password, true);
	}

	public String base64Encode(String value) {
		if (value != null) {
			return new String(Base64.encodeToString(value.getBytes(Charset.forName("ISO-8859-1"))));
		} else {
			return null;
		}
	}

	/**
	 *
	 * @param serviceRequest
	 * @param base64EncodedCredentials
	 * @return HttpServiceResponse
	 * @throws DigitalIntegrationException
	 */
	@SuppressWarnings("static-access")
	public HttpServiceResponse processHttpRequest(ServiceRequest serviceRequest, String base64EncodedCredentials) throws DigitalIntegrationException {
		HttpUriRequest request = null;
    final String CONTENT_TYPE_HEADER = "Content-Type";

		long startTime = 0l;
		HttpResponse response = null;
		String serviceName = null;
		String methodName = null;
		String serviceURL = null;
		String requestString = null;
		DefaultHttpClient httpClient = null;
		int responseStatus=0;
		try {

			requestString = serviceRequest.getBody();
			serviceName = this.getServiceName();
			methodName = serviceRequest.getServiceMethodName();
			serviceURL = serviceRequest.getServiceUri();

			if (serviceRequest.getServiceMethodType().equals(serviceRequest.getServiceMethodType().POST)) {
				request = createPostRequest(serviceRequest.getConnectionTimeout(), serviceRequest.getRequestTimeout(),
						serviceURL, serviceRequest.getServiceContentType().getContentType(),
						serviceRequest.getHeaderEntries());

				if (!DigitalStringUtil.isEmpty(requestString)) {
					StringEntity bodyEntity = new StringEntity(requestString);
					bodyEntity.setContentType(serviceRequest.getServiceContentType().getContentType());

					((HttpPost) request).setEntity(bodyEntity);
				}
			} else if (serviceRequest.getServiceMethodType().equals(serviceRequest.getServiceMethodType().PUT)) {
				request = createPutRequest(serviceRequest.getConnectionTimeout(), serviceRequest.getRequestTimeout(),
						serviceURL, serviceRequest.getServiceContentType().getContentType(),
						serviceRequest.getHeaderEntries());

				if (!DigitalStringUtil.isEmpty(requestString)) {
					StringEntity bodyEntity = new StringEntity(requestString);
					bodyEntity.setContentType(serviceRequest.getServiceContentType().getContentType());

					((HttpPut) request).setEntity(bodyEntity);
				}
			} else if (serviceRequest.getServiceMethodType().equals(serviceRequest.getServiceMethodType().GET)) {
				request = createGetRequest(serviceRequest.getConnectionTimeout(), serviceRequest.getRequestTimeout(),
						serviceURL, serviceRequest.getHeaderEntries());
			} else if (serviceRequest.getServiceMethodType().equals(serviceRequest.getServiceMethodType().DELETE)) {
				request = createDeleteRequest(serviceRequest.getConnectionTimeout(), serviceRequest.getRequestTimeout(),
						serviceURL, serviceRequest.getHeaderEntries());
			}

			String authHeader = "Basic " + base64EncodedCredentials;

			request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);

			// create httpclient
			httpClient = initializeHttpClient();

			startTime = System.nanoTime();
      if(logger.isDebugEnabled()) {
        if (connectionManager != null) {
          logger.debug(
              "Before - Leased Connections = " + connectionManager.getTotalStats().getLeased());
          logger.debug("Before - Available Connections = " + connectionManager.getTotalStats()
              .getAvailable());
        }
      }
			response = httpClient.execute(request);

      if(logger.isDebugEnabled()) {
        if (connectionManager != null) {
          logger.debug(
              "After - Leased Connections = " + connectionManager.getTotalStats().getLeased());
          logger.debug("After - Available Connections = " + connectionManager.getTotalStats()
              .getAvailable());
        }
      }

			HttpServiceResponse httpServiceResponse = new HttpServiceResponse();
      HttpEntity entity = response.getEntity();

      if (null != entity) {
        String content;
        if(null != entity.getContentType()) {
          String contentType = entity.getContentType().getValue();
          String charset = getResponseCharset(contentType);
          content = EntityUtils.toString(entity, charset);
        }
        else{
          content = EntityUtils.toString(entity);
        }
        httpServiceResponse.setEntityString(content);
      }
			responseStatus = response.getStatusLine().getStatusCode();
			httpServiceResponse.setStatusCode(responseStatus);
      if (doesRequireConnectionPool() && doesRequireGracefulConnectionShutdown()) {
        // detect idle and expired connections and close them
        IdleConnectionMonitorThread staleMonitor = new IdleConnectionMonitorThread(
            connectionManager);
        staleMonitor.start();
      }
			return httpServiceResponse;
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder()
					.append("ProcessHttpRequest Exception: ")
					.append(e.toString()).append(" : ").append(serviceName)
					.append(" : ").append(methodName)
					.append(" : ").append(requestString);
			logger.error(sb.toString());
			logger.error("Exception from httpclient call ", e);
			if (response != null) {
				EntityUtils.consumeQuietly(response.getEntity());
			}
			throw new DigitalIntegrationBusinessException(
					"ProcessHttpRequest Error",
					ErrorCodes.COMM_ERROR.getCode(), e);
		} finally {
			StringBuilder sbEP = new StringBuilder()
					.append("Invoked End Point: ")
					.append(serviceURL).append(" : ").append(serviceName)
					.append(" : ").append(methodName)
					.append(" : ").append(responseStatus);
			if(logger.isDebugEnabled()) {
				logger.debug(sbEP.toString());
			}
			StringBuilder sb = new StringBuilder().append(serviceName).append(" : ").append(methodName)
					.append(": ").append(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime)).append("ms").append(" : httpStatus ").append(responseStatus);
			logger.info(sb.toString());
			// shutdown connection manager
			if(null != httpClient) {
					if(!doesRequireConnectionPool()) {
						httpClient.getConnectionManager().shutdown();
					}
				}
			}
		}

	/**
	 *
	 * @param connectionTimeout
	 * @param requestTimeout
	 * @param serviceURI
	 * @param contentType
	 * @param headers
	 * @return HttpUriRequest
	 */
	protected HttpUriRequest createPostRequest(int connectionTimeout, int requestTimeout, String serviceURI,
											   String contentType, Map<String, String> headers) {
		HttpPost httpPost = new HttpPost(serviceURI);
		httpPost.setHeader("Content-Type", contentType);
		httpPost.setHeader("Accept", contentType);

		if (logger.isDebugEnabled()) {
			logger.debug(String.format("%s timeouts set - connection: %s, request: %s", getComponentName(),
					connectionTimeout, requestTimeout));
		}

		httpPost.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, requestTimeout);
		httpPost.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, connectionTimeout);

		for (Entry<String, String> headerEntry : headers.entrySet()) {
			httpPost.setHeader(headerEntry.getKey(), headerEntry.getValue());
		}

		return httpPost;
	}

	/**
	 *
	 * @param connectionTimeout
	 * @param requestTimeout
	 * @param serviceURI
	 * @param contentType
	 * @return HttpUriRequest
	 */
	protected HttpUriRequest createPostRequest(int connectionTimeout, int requestTimeout, String serviceURI,
											   String contentType) {
		HttpPost httpPost = new HttpPost(serviceURI);

		if (logger.isDebugEnabled()) {
			logger.debug(String.format("%s timeouts set - connection: %s, request: %s", getComponentName(),
					connectionTimeout, requestTimeout));
		}

		httpPost.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, requestTimeout);
		httpPost.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, connectionTimeout);

		httpPost.setHeader("Content-Type", contentType);
		httpPost.setHeader("Accept", contentType);

		return httpPost;
	}

	/**
	 *
	 * @param connectionTimeout
	 * @param requestTimeout
	 * @param serviceURI
	 * @param contentType
	 * @param headers
	 * @return HttpUriRequest
	 */
	protected HttpUriRequest createPutRequest(int connectionTimeout, int requestTimeout, String serviceURI,
											  String contentType, Map<String, String> headers) {
		HttpPut httpPut = new HttpPut(serviceURI);
		httpPut.setHeader("Content-Type", contentType);
		httpPut.setHeader("Accept", contentType);

		if (logger.isDebugEnabled()) {
			logger.debug(String.format("%s timeouts set - connection: %s, request: %s", getComponentName(),
					connectionTimeout, requestTimeout));
		}

		httpPut.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, requestTimeout);
		httpPut.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, connectionTimeout);

		for (Entry<String, String> headerEntry : headers.entrySet()) {
			httpPut.setHeader(headerEntry.getKey(), headerEntry.getValue());
		}

		return httpPut;
	}

	/**
	 *
	 * @param connectionTimeout
	 * @param requestTimeout
	 * @param serviceURI
	 * @param contentType
	 * @return HttpUriRequest
	 */
	protected HttpUriRequest createPutRequest(int connectionTimeout, int requestTimeout, String serviceURI,
											  String contentType) {
		HttpPut httpPut = new HttpPut(serviceURI);
		httpPut.setHeader("Content-Type", contentType);
		httpPut.setHeader("Accept", contentType);

		if (logger.isDebugEnabled()) {
			logger.debug(String.format("%s timeouts set - connection: %s, request: %s", getComponentName(),
					connectionTimeout, requestTimeout));
		}

		httpPut.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, requestTimeout);
		httpPut.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, connectionTimeout);

		return httpPut;
	}

	/**
	 *
	 * @param connectionTimeout
	 * @param requestTimeout
	 * @param serviceURI
	 * @return HttpUriRequest
	 */
	protected HttpUriRequest createGetRequest(int connectionTimeout, int requestTimeout, String serviceURI) {
		HttpGet httpGet = new HttpGet(serviceURI);

		if (logger.isDebugEnabled()) {
			logger.debug(String.format("%s timeouts set - connection: %s, request: %s", getComponentName(),
					connectionTimeout, requestTimeout));
		}

		httpGet.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, requestTimeout);
		httpGet.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, connectionTimeout);

		return httpGet;
	}

	/**
	 *
	 * @param connectionTimeout
	 * @param requestTimeout
	 * @param serviceURI
	 * @param headers
	 * @return HttpUriRequest
	 */
	protected HttpUriRequest createGetRequest(int connectionTimeout, int requestTimeout, String serviceURI,
											  Map<String, String> headers) {
		HttpGet httpGet = new HttpGet(serviceURI);

		if (logger.isDebugEnabled()) {
			logger.debug(String.format("%s timeouts set - connection: %s, request: %s", getComponentName(),
					connectionTimeout, requestTimeout));
		}

		httpGet.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, requestTimeout);
		httpGet.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, connectionTimeout);

		for (Entry<String, String> headerEntry : headers.entrySet()) {
			httpGet.setHeader(headerEntry.getKey(), headerEntry.getValue());
		}

		return httpGet;
	}

	/**
	 *
	 * @param connectionTimeout
	 * @param requestTimeout
	 * @param serviceURI
	 * @return HttpUriRequest
	 */
	protected HttpUriRequest createDeleteRequest(int connectionTimeout, int requestTimeout, String serviceURI) {
		HttpDelete httpDelete = new HttpDelete(serviceURI);

		if (logger.isDebugEnabled()) {
			logger.debug(String.format("%s timeouts set - connection: %s, request: %s", getComponentName(),
					connectionTimeout, requestTimeout));
		}

		httpDelete.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, requestTimeout);
		httpDelete.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, connectionTimeout);

		return httpDelete;
	}

	/**
	 *
	 * @param connectionTimeout
	 * @param requestTimeout
	 * @param serviceURI
	 * @param headers
	 * @return HttpUriRequest
	 */
	protected HttpUriRequest createDeleteRequest(int connectionTimeout, int requestTimeout, String serviceURI,
												 Map<String, String> headers) {
		HttpDelete httpDelete = new HttpDelete(serviceURI);

		if (logger.isDebugEnabled()) {
			logger.debug(String.format("%s timeouts set - connection: %s, request: %s", getComponentName(),
					connectionTimeout, requestTimeout));
		}

		httpDelete.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, requestTimeout);
		httpDelete.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, connectionTimeout);

		for (Entry<String, String> headerEntry : headers.entrySet()) {
			httpDelete.setHeader(headerEntry.getKey(), headerEntry.getValue());
		}

		return httpDelete;
	}

	/*
	 * This will be refactored... Easiest way of finding the caller in a generic
	 * fashion.
	 *
	 * @return StackTraceElement
	 */
	private static StackTraceElement getCallerInfo() {
		StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
		for (StackTraceElement element : stElements) {
			if (DigitalStringUtil.endsWith(element.getClassName(), "Service")) {
				return element;
			}
		}

		return null;
	}

	/**
	 *
	 * @param serviceRequestString
	 */
	public static void logServiceRequest(String serviceRequestString) {
		if (logger.isDebugEnabled()) {
			String callerClassName = "";
			String callerMethodName = "";
			StackTraceElement element = getCallerInfo();

			if (element != null) {
				callerClassName = element.getClassName();
				callerMethodName = element.getMethodName();
			}

			logger.debug(
					String.format("%s - %s: REQUEST - %s", callerClassName, callerMethodName, serviceRequestString));
		}
	}

	/**
	 *
	 * @param serviceResponseString
	 */
	public static void logServiceResponse(String serviceResponseString) {
		if (logger.isDebugEnabled()) {
			String callerClassName = "";
			String callerMethodName = "";
			StackTraceElement element = getCallerInfo();

			if (element != null) {
				callerClassName = element.getClassName();
				callerMethodName = element.getMethodName();
			}

			logger.debug(
					String.format("%s - %s: RESPONSE - %s", callerClassName, callerMethodName, serviceResponseString));
		}
	}

	/**
	 * Turns a string of valid XML into an xml Document
	 *
	 * @param xmlString
	 * @return Document
	 */
	protected static Document convertStringToDocument(String xmlString) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
			return doc;
		} catch (Exception e) {
			logger.error("Exception: ", e);
		}
		return null;
	}

	/**
	 *
	 * @param classToUnmarshall
	 * @param response
	 * @param <T>
	 * @return T
	 * @throws DigitalIntegrationException
	 */
	@SuppressWarnings("rawtypes")
	public abstract <T> T getUnmarshalledObjectFromHttpServiceResponse(Class classToUnmarshall, HttpServiceResponse response)
			throws DigitalIntegrationException;

	/**
	 *
	 * @param classToUnmarshall
	 * @param response
	 * @param faultHandler
	 * @param <T>
	 * @return T
	 * @throws DigitalIntegrationException
	 */
	@SuppressWarnings("rawtypes")
	public abstract <T> T getUnmarshalledObjectFromHttpServiceResponse(Class classToUnmarshall, HttpServiceResponse response,
																	   FaultHandler faultHandler) throws DigitalIntegrationException;

	/**
	 *
	 * @param classToUnmrashall
	 * @param objectToMarshall
	 * @param <T>
	 * @return T
	 * @throws DigitalIntegrationException
	 */
	@SuppressWarnings("rawtypes")
	public abstract <T> String getMarshalledStringFromObject(Class classToUnmrashall, T objectToMarshall)
			throws DigitalIntegrationException;

	/**
	 * @return ObjectMapper
	 */
	public ObjectMapper getObjectMapper() {
		return null;
	}

	/**
	 *
	 * @return
	 */
	public String getProxyHost() {
		return (PropertyUtil.getMappedProperty(getProxyInfo(), PROXY_HOST)).toString();
	}

	/**
	 *
	 * @param proxyHost
	 */
	public void setProxyHost(String proxyHost) {
		PropertyUtil.setMappedProperty(getProxyInfo(), PROXY_HOST, proxyHost);
	}

	/**
	 *
	 * @return Integer
	 */
	public Integer getProxyPort() {
		return (PropertyUtil.getMappedProperty(getProxyInfo(), PROXY_PORT)).toInteger();
	}

	/**
	 *
	 * @param proxyPort
	 */
	public void setProxyPort(Integer proxyPort) {
		PropertyUtil.setMappedProperty(getProxyInfo(), PROXY_PORT, proxyPort);
	}

	/**
	 *
	 * @return
	 */
	public String getProxyUser() {
		return (PropertyUtil.getMappedProperty(getProxyInfo(), PROXY_USER)).toString();
	}

	/**
	 *
	 * @param proxyUser
	 */
	public void setProxyUser(String proxyUser) {
		PropertyUtil.setMappedProperty(getProxyInfo(), PROXY_USER, proxyUser);
	}

	/**
	 *
	 * @return String
	 */
	public String getProxyPassword() {
		return (PropertyUtil.getMappedProperty(getProxyInfo(), PROXY_PASSWORD)).toString();
	}

	/**
	 *
	 * @param proxyPassword
	 */
	public void setProxyPassword(String proxyPassword) {
		PropertyUtil.setMappedProperty(getProxyInfo(), PROXY_PASSWORD, proxyPassword);
	}

	/**
	 *
	 * @return Map
	 */
	public Map<String, String> getRetryCounts() {
		return getWebServicesConfig().getRetryCount();
	}

	/**
	 *
	 * @return
	 */
	public Map<String, String> getRequireTLS12() {
		return getWebServicesConfig().getRequireTLS12();
	}

	/**
	 *
	 * @return true or false
	 */
	public boolean doesRequireTLS12() {
		return PropertyUtil.getMappedProperty(getRequireTLS12(), getServiceName()).toBoolean();
	}

	/**
	 *
	 * @return true or false
	 */
	public Map<String, String> getRequireConnectionPool() {
		return getWebServicesConfig().getRequiresConnectionPool();
	}

	/**
	 *
	 * @return true or false
	 */
	public boolean doesRequireConnectionPool() {
		return PropertyUtil.getMappedProperty(getRequireConnectionPool(), getServiceName()).toBoolean();
	}

	/**
	 *
	 * @return true or false
	 */
	public Map<String, String> getRequireKeepAliveStratergy() {
		return getWebServicesConfig().getRequiresKeepAliveStratergy();
	}

	/**
	 *
	 * @return true or false
	 */
	public boolean doesRequireKeepAliveStratergy() {
		return PropertyUtil.getMappedProperty(getRequireKeepAliveStratergy(), getServiceName()).toBoolean();
	}

	/**
	 * @return true or false
	 */
	public Map<String, String> getRequireGracefulConnectionShutdown() {
		return getWebServicesConfig().getRequiresGracefulConnectionShutdown();
	}

	/**
	 * @return true or false
	 */
	public boolean doesRequireGracefulConnectionShutdown() {
		return PropertyUtil.getMappedProperty(getRequireGracefulConnectionShutdown(), getServiceName())
				.toBoolean();
	}

	/**
	 *
	 * @return true or false
	 */
	public boolean doesRequireProxy() {
		boolean retValue = PropertyUtil.getMappedProperty(getRequireProxy(), getServiceName()).toBoolean();
		if (logger.isDebugEnabled()) {
			logger.debug("Proxy setting for: " + getServiceName() + " " + retValue);
		}
		return retValue;
	}

	/**
	 *
	 * @param doesRequirePRoxy
	 */
	public void setDoesRequireProxy(Boolean doesRequirePRoxy) {
		PropertyUtil.setMappedProperty(getRequireProxy(), getServiceName(), doesRequirePRoxy);
	}

	/**
	 *
	 * @return String
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 *
	 * @param serviceName
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 *
	 * @return Integer
	 */
	public Integer getRetryCount() {
		return PropertyUtil.getMappedProperty(getRetryCounts(), getServiceName()).toInteger();
	}

	/**
	 *
	 * @return
	 */
	public Integer getInsecurePort() {
		return insecurePort;
	}

	/**
	 *
	 * @param insecurePort
	 */
	public void setInsecurePort(Integer insecurePort) {
		this.insecurePort = insecurePort;
	}

	/**
	 *
	 * @return Integer
	 */
	public Integer getSecurePort() {
		return securePort;
	}

	/**
	 *
	 * @param securePort
	 */
	public void setSecurePort(Integer securePort) {
		this.securePort = securePort;
	}

	/**
	 *
	 * @return
	 */
	public Map<String, String> getProxyInfo() {
		return webServicesConfig.getProxyInfo();
	}

	/**
	 *
	 * @return Map
	 */
	public Map<String, String> getRequireProxy() {
		return getWebServicesConfig().getRequireProxy();
	}

	/**
	 *
	 * @return Map
	 */
	public Map<String, HttpRoutePlanner> getRoutePlanners() {
		return routePlanners;
	}

	/**
	 *
	 * @param routePlanners
	 */
	public void setRoutePlanners(Map<String, HttpRoutePlanner> routePlanners) {
		this.routePlanners = routePlanners;
	}

	/**
	 *
	 * @return WebServicesConfig
	 */
	public WebServicesConfig getWebServicesConfig() {
		return webServicesConfig;
	}

	/**
	 *
	 * @param webServicesConfig
	 */
	public void setWebServicesConfig(WebServicesConfig webServicesConfig) {
		this.webServicesConfig = webServicesConfig;
	}

	public class PerConnectionProxyRoutePlanner implements HttpRoutePlanner {
		private HttpHost proxyHost;

		/**
		 *
		 * @param proxyHost
		 */
		public PerConnectionProxyRoutePlanner(HttpHost proxyHost) {
			super();
			this.proxyHost = proxyHost;
		}

		/**
		 *
		 * @param target
		 * @param request
		 * @param context
		 * @return HttpRoute
		 * @throws HttpException
		 */
		@Override
		public HttpRoute determineRoute(HttpHost target, HttpRequest request, HttpContext context)
				throws HttpException {

			if (proxyHost != null) {
				return new HttpRoute(target, null, proxyHost, "https".equalsIgnoreCase(target.getSchemeName()));
			} else {
				return new HttpRoute(target, null, proxyHost, "https".equalsIgnoreCase(target.getSchemeName()),
						TunnelType.PLAIN, LayerType.PLAIN);
			}
		}
	}

	 public static String getResponseCharset(String ctype) {
		String charset = DEFAULT_CHARSET;

		if (DigitalStringUtil.isNotEmpty(ctype)) {
			String[] params = ctype.split(";");
			for (String param : params) {
				param = param.trim();
				if (param.startsWith("charset")) {
					String[] pair = param.split("=", 2);
					if (pair.length == 2) {
						if (DigitalStringUtil.isNotEmpty(pair[1])) {
							charset = pair[1].trim();
						}
					}
					break;
				}
			}
		}

		return charset;
	}

	public static class IdleConnectionMonitorThread extends Thread {

		DigitalLogger logger = DigitalLogger.getLogger(IdleConnectionMonitorThread.class);

		private final PoolingClientConnectionManager connMgr;
		private volatile boolean shutdown;
		private int runCount = 0;

		public IdleConnectionMonitorThread(final PoolingClientConnectionManager connMgr) {
			super();
			this.connMgr = connMgr;
		}

		@Override
		public void run() {

			if(logger.isDebugEnabled()){
				logger.debug("Current thread " + this.toString());
			}

			try {
				while (!shutdown) {
					synchronized (this) {
						wait(30000);
						// Close expired connections
						connMgr.closeExpiredConnections();
						// Optionally, close connections
						// that have been idle longer than 60 sec
						connMgr.closeIdleConnections(60, TimeUnit.SECONDS);
						runCount++;
						// terminate after 3 runs
						if(runCount > 3){
							if (logger.isDebugEnabled()) {
								logger.debug(
										"Shutting down thread after 3 runs");
							}
							shutdown();
						}
					}
				}
			} catch (InterruptedException ex) {
				// terminate
				shutdown();
			}
		}

		public void shutdown() {
			if(logger.isDebugEnabled()){
				logger.debug("Shutting down current thread " + this.toString());
			}
			shutdown = true;
			synchronized (this) {
				interrupt();
				notifyAll();
			}
		}

	}
}
