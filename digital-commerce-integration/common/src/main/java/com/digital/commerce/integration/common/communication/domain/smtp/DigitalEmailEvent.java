package com.digital.commerce.integration.common.communication.domain.smtp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.digital.commerce.integration.common.communication.domain.Recipient;

public class DigitalEmailEvent implements Serializable {
	public enum ContentType  implements Serializable {
		TEXT("text/plain"), HTML("text/html");
		
		private String mimeType;

		private ContentType(String mimeType) {
			this.mimeType = mimeType;
		}

		public String getMimeType() {
			return mimeType;
		}
	}
	
	private static final long serialVersionUID = 1L;
	private String sender;
	private List<Recipient> recipients; 
	private String subject;
	private List<String> subjectDataElements = new ArrayList<>();
	private String templateKey;
	private Map<String, String> dataElements = new HashMap<>();
	private ContentType contentType;
	private String siteId;
	private Locale locale = Locale.US;
	private Long deliveryDelay = 0l;
	private boolean isTransacted;
	
	public boolean isTransacted() {
		return isTransacted;
	}
	public void setTransacted(boolean isTransacted) {
		this.isTransacted = isTransacted;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public List<Recipient> getRecipients() {
		return recipients;
	}
	public void setRecipients(List<Recipient> recipients) {
		this.recipients = recipients;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
		
	public String getTemplateKey() {
		return templateKey;
	}
	public void setTemplateKey(String templateKey) {
		this.templateKey = templateKey;
	}
	public Map<String, String> getDataElements() {
		return dataElements;
	}
	public void setDataElements(Map<String, String> dataElements) {
		this.dataElements = dataElements;
	}
	public ContentType getContentType() {
		return contentType;
	}
	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}
	public List<String> getSubjectDataElements() {
		return subjectDataElements;
	}
	public void setSubjectDataElements(List<String> subjectDataElements) {
		this.subjectDataElements = subjectDataElements;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public Locale getLocale() {
		return locale;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	public Long getDeliveryDelay() {
		return deliveryDelay;
	}
	public void setDeliveryDelay(Long deliveryDelay) {
		this.deliveryDelay = deliveryDelay;
	}
	
}
