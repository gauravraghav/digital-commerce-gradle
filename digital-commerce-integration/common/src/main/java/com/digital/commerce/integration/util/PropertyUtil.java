package com.digital.commerce.integration.util;

import java.util.Map;

public class PropertyUtil {

	/*
	 * Currently only works with String - but could easily be parameterized to handle any.
	 * 
	 * In this scenario ATG only handles Map<String,String> for injection so it isn't
	 * necessary
	 * 
	 */
	public static MappedProperty getMappedProperty(Map<String, String> propertyMap, String key) {
		if (propertyMap == null) {
			return new MappedProperty("");
		} else {
			if (propertyMap.containsKey(key)) {
				return new MappedProperty(propertyMap.get(key));
			} else {
				return new MappedProperty("");
			}
		}
	}
	
	/*
	 * I don't think the mutator is necessary as mapped properties are configured from the configs
	 * at creation or admin modification. 
	 */
	public static void setMappedProperty(Map<String, String> propertyMap, String key, Object value) {
		if (propertyMap != null && value != null) {
			propertyMap.put(key, String.valueOf(value));
		} 
	}
}
