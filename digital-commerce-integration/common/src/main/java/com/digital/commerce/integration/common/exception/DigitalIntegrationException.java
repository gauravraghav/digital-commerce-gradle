package com.digital.commerce.integration.common.exception;

import com.digital.commerce.common.exception.DigitalAppException;

public class DigitalIntegrationException extends DigitalAppException {
	
	private static final long serialVersionUID = 1L;
	
	private String message;
	private String errorCode;
	private String categoryCode;
	
	public DigitalIntegrationException( String message, String errorCode, String categoryCode ) {		
		super( ExceptionHelper.formatMessage(message, errorCode, categoryCode ));
		this.errorCode = errorCode;
		this.categoryCode = categoryCode;
		this.message = ExceptionHelper.formatMessage(message, errorCode, categoryCode );
	}

	public DigitalIntegrationException( String message, String errorCode, String categoryCode, Throwable ex ) {
		super( ExceptionHelper.formatMessage(message, errorCode, categoryCode ), ex );
		this.errorCode = errorCode;
		this.categoryCode = categoryCode;
		this.message = ExceptionHelper.formatMessage(message, errorCode, categoryCode );
	}

	public String getMessage() {
		return message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	
		
}
