package com.digital.commerce.integration.util;

import org.apache.commons.lang.math.NumberUtils;

import com.digital.commerce.common.util.DigitalStringUtil;

public class MappedProperty {
	String value;

	public MappedProperty(String value) {
		super();
		this.value = value;
	}
	
	public Integer toInteger() {
		if (DigitalStringUtil.isEmpty(value)) {
			return null;
		} else {
			if (NumberUtils.isNumber(value)) {
				return NumberUtils.toInt(value);
			} else {
				return null;
			}			
		}
	}

	public String toString() {
		return value;
	}
	
	public boolean toBoolean() {
		if (value != null && !DigitalStringUtil.isEmpty(value) &&  (DigitalStringUtil.equalsIgnoreCase("y", value) || DigitalStringUtil.equalsIgnoreCase("true", value) )) {
			return true;
		} else {
			return false;
		}
	}
}
