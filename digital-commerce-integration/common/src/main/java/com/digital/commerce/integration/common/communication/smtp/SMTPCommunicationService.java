package com.digital.commerce.integration.common.communication.smtp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.digital.commerce.common.communication.DigitalTemplateEmailInfo;
import com.digital.commerce.common.exception.DigitalRuntimeException;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.communication.CommunicationService;
import com.digital.commerce.integration.common.communication.domain.Recipient;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent.ContentType;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryUtils;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

public class SMTPCommunicationService extends BaseIntegrationService implements CommunicationService {
	private static final DigitalLogger logger = DigitalLogger.getLogger(SMTPCommunicationService.class);
	private static final String LEFT_DELIM = "{";
	private static final String RIGHT_DELIM = "}";

	private SMTPCommunicationServiceSource emailServiceSource;
	private TemplateEmailSender templateEmailSender;
	private ProfileTools profileTools;
	private Map<String, String> emailTemplateMap = new HashMap<>();

	@Override
	protected String getService() {
		return WebServicesConfig.ServicNames.SMTP.getName();
	}
	
	/**
	 * This implementation uses the TemplateEmailSender and
	 * TemplateEmailInfoImpl facilities of ATG to generate and send external
	 * email.
	 * 
	 * @param sender
	 *            Not required if provided via the TemplateEmailInfoImpl
	 * @param recipients
	 *            List of Recipient objects specifying address and type (CC,
	 *            BCC, TO)
	 * @param subject
	 *            Not required if provided via the TemplateEmailInfoImpl. If
	 *            provided does allow token ({0}, {1}, {2}) replacement.
	 * @param subjectDataElements
	 *            Not Required if the TemplateEmailInfoImpl supplies the subject
	 *            or the subject is static
	 * @param templateKey
	 *            String key which points to an element in the emailTemplateMap
	 *            which hold the location of a dynamo component of
	 *            TemplateEmailInfoImpl type
	 * @param dataElements
	 *            Map of data used as token replacements in the templateKEy
	 *            template
	 * @param contentType
	 *            Either TEXT or HTML (as provided by the enum)
	 */
	@Override
	public boolean send(String sender, List<Recipient> recipients, String subject, List<String> subjectDataElements,
			String templateKey, Map<String, String> dataElements, ContentType contentType, String siteId, Locale locale)
			throws DigitalIntegrationException {
		boolean hasException = false;
		String serviceMethodName = ServiceMethod.SEND.getServiceMethodName();

		if (doRunService(serviceMethodName)) {
			startPerformanceMonitorOperation(serviceMethodName);

			try {
				DigitalEmailEvent dswEmailEvent = new DigitalEmailEvent();

				dswEmailEvent.setTemplateKey(templateKey);
				dswEmailEvent.setDataElements(dataElements);
				dswEmailEvent.setRecipients(recipients);
				dswEmailEvent.setSender(sender);
				dswEmailEvent.setSubject(subject);
				dswEmailEvent.setContentType(contentType);
				dswEmailEvent.setSubjectDataElements(subjectDataElements);
				dswEmailEvent.setSiteId(siteId);
				dswEmailEvent.setLocale(locale);

				TemplateEmailInfoImpl templateEmailInfoImpl = getTemplate(templateKey);
				
				if (templateEmailInfoImpl instanceof DigitalTemplateEmailInfo) {
					dswEmailEvent.setDeliveryDelay(((DigitalTemplateEmailInfo)templateEmailInfoImpl).getDeliveryDelay());
				}
				
				// add to queue instead of sending - this let's us build the
				// email async
				emailServiceSource.addToQueue(dswEmailEvent);

				return true;

			} catch (Exception e) {
				hasException = true;

				throw new DigitalIntegrationSystemException("Exception thrown during email send queueing has errored",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}

	}

	@Override
	public boolean send(List<Recipient> recipients, String templateKey, Map<String, String> dataElements,
			ContentType contentType, String siteId, Locale locale) throws DigitalIntegrationException {

		return this.send(null, recipients, null, null, templateKey, dataElements, contentType, siteId, locale);
	}

	@Override
	public boolean send(String sender, List<Recipient> recipients, String subject, List<String> subjectDataElements,
			String templateKey, Map<String, String> dataElements, ContentType contentType, String siteId)
			throws DigitalIntegrationException {

		return send(sender, recipients, subject, subjectDataElements, templateKey, dataElements, contentType, siteId,
				Locale.US);
	}

	@Override
	public boolean send(List<Recipient> recipients, String templateKey, Map<String, String> dataElements,
			ContentType contentType, String siteId) throws DigitalIntegrationException {
		return send(recipients, templateKey, dataElements, contentType, siteId, Locale.US);
	}

	protected TemplateEmailInfoImpl getTemplate(String templateKey) {
		if (!getEmailTemplateMap().containsKey(templateKey)) {			
			logger.error("templateKey does not exist is invalid: " + templateKey);
		}

		String templateComponentPath = getEmailTemplateMap().get(templateKey);

		TemplateEmailInfoImpl templateEmailInfo = ComponentLookupUtil.lookupComponent(templateComponentPath,
				TemplateEmailInfoImpl.class);

		if (templateEmailInfo == null) {			
			logger.error("templateKey component can not be found: " + templateKey);
		}
		
		return templateEmailInfo;
	}
	
	protected void sendSMTPMessage(DigitalEmailEvent dswEmailEvent) {
		String errorReason = "";
		boolean hasError = false;
		String templateKey = null;
		// determine the type(s) of recipients
		List<String> toRecipientList = new ArrayList<>();
		List<String> ccRecipientList = new ArrayList<>();
		List<String> bccRecipientList = new ArrayList<>();

		try {

			/*
			 * Subject should be a string that has been pulled from a
			 * resourceBundle
			 */
			String subject = dswEmailEvent.getSubject();
			templateKey = dswEmailEvent.getTemplateKey();

			if (dswEmailEvent.getRecipients() != null && dswEmailEvent.getRecipients().size() > 0) {
				
				TemplateEmailInfoImpl templateEmailInfo = getTemplate(templateKey);

				templateEmailInfo.setTemplateParameters(dswEmailEvent.getDataElements());
				templateEmailInfo.setSiteId(dswEmailEvent.getSiteId());
				templateEmailInfo.setMessageResourceLocale(dswEmailEvent.getLocale());

				if (!DigitalStringUtil.isEmpty(subject)) {
					subject = replaceTokens(subject, dswEmailEvent.getSubjectDataElements());
					templateEmailInfo.setMessageSubject(subject);
				} else {
					subject = replaceTokens(templateEmailInfo.getMessageSubject(),
							dswEmailEvent.getSubjectDataElements());
					templateEmailInfo.setMessageSubject(subject);
				}

				if (!DigitalStringUtil.isBlank(dswEmailEvent.getSender())) {
					templateEmailInfo.setMessageFrom(dswEmailEvent.getSender());
				}

				for (Recipient recipient : dswEmailEvent.getRecipients()) {
					String emaiAddress = DigitalStringUtil.isNotEmpty(recipient.getName())
							? (recipient.getName() + " <" + recipient.getAddress() + ">") : recipient.getAddress();
					switch (recipient.getRecipientType()) {
						case TO:
							toRecipientList.add(emaiAddress);
							break;
						case CC:
							ccRecipientList.add(emaiAddress);
							break;
						case BCC:
							bccRecipientList.add(emaiAddress);
							break;
						default:
							errorReason = "Recipient missing required type for " + templateKey;
							logger.error(errorReason);
							break;
					}
				}

				templateEmailInfo.setMessageCc(DigitalStringUtil.join(ccRecipientList, ','));
				templateEmailInfo.setMessageBcc(DigitalStringUtil.join(bccRecipientList, ','));

				templateEmailSender.sendEmailMessage(templateEmailInfo, toRecipientList);
				
			} else {
				errorReason = "Recipients is missing for " + templateKey;
				hasError = true;
				logger.error(errorReason);
				throw new DigitalRuntimeException("Cannot generate email: " + errorReason);
			}
		} catch (Exception e) {
			hasError = true;
			throw new DigitalRuntimeException("Cannot generate email: " + errorReason, e);
		} finally {
			if(logger.isInfoEnabled()) {
				if(!hasError) {
					logger.info("Email delivery info ["+ "Template: " + templateKey + ", "+ "Delivery status: " + "SUCCESS" + ", Recipients: " + toRecipientList + "]");
				} else {
					logger.info("Email delivery info ["+ "Template: " + templateKey + ", "+ "Delivery status: " + "FAILED" + ", Recipients: " + toRecipientList + "]");
				}
			}
		}
	}

	/**
	 * The text can contain ordinal tokens {0}, {1}, {...} that will be replaced
	 * by the dataElements.
	 * 
	 * Very simplistic replacement, no regex.
	 * 
	 * @param text
	 * @param dataElements
	 * @return
	 */
	protected String replaceTokens(String text, List<String> dataElements) {

		if (dataElements != null) {
			int i = 0;
			for (String dataElement : dataElements) {
				text = DigitalStringUtil.replace(text, LEFT_DELIM + i + RIGHT_DELIM, dataElement);
			}
		}

		return text;
	}

	protected List<MutableRepositoryItem> lookupProfiles(List<String> emailAddresses) {
		List<MutableRepositoryItem> profiles = new ArrayList<>();
		MutableRepositoryItem mutUsers[] = null;

		try {

			for (String emailAddress : emailAddresses) {
				if (!DigitalStringUtil.isBlank(emailAddress)) {
					RepositoryItem users[] = getProfileTools().getItemsFromEmail(emailAddress);
					if (users != null) {
						mutUsers = new MutableRepositoryItem[users.length];
						for (int i = 0; i < users.length; i++) {
							MutableRepositoryItem mutUser = RepositoryUtils.getMutableRepositoryItem(users[i]);
							if (mutUser != null)
								mutUsers[i] = mutUser;
						}

					}

					if (mutUsers != null) {
						profiles.addAll(Arrays.asList(mutUsers));
					} else {
						logger.error("Error finding user's profile for email address: " + emailAddress);
					}
				}
			}
		} catch (Exception exc) {
			logger.error("Error finding user's profile for email address", exc);
		}
		return profiles;
	}

	public Map<String, String> getEmailTemplateMap() {
		return emailTemplateMap;
	}

	public void setEmailTemplateMap(Map<String, String> emailTemplateMap) {
		this.emailTemplateMap = emailTemplateMap;
	}

	@Override
	public void postStartup() throws DigitalIntegrationException {
	}

	@Override
	public void postShutdown() throws DigitalIntegrationException {
	}

	public TemplateEmailSender getTemplateEmailSender() {
		return templateEmailSender;
	}

	public void setTemplateEmailSender(TemplateEmailSender templateEmailSender) {
		this.templateEmailSender = templateEmailSender;
	}

	public SMTPCommunicationServiceSource getEmailServiceSource() {
		return emailServiceSource;
	}

	public void setEmailServiceSource(SMTPCommunicationServiceSource emailServiceSource) {
		this.emailServiceSource = emailServiceSource;
	}

	public ProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(ProfileTools profileTools) {
		this.profileTools = profileTools;
	}

}
