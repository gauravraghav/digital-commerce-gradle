package com.digital.commerce.integration.common.exception;

public class DigitalIntegrationSystemException extends DigitalIntegrationException {
	private static String CATEGORY_CODE = "SYSTEM";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DigitalIntegrationSystemException( String message, String errorCode ) {
		super( message, errorCode, CATEGORY_CODE );
	}

	public DigitalIntegrationSystemException( String message, String errorCode, Throwable ex ) {
		super( message, errorCode, CATEGORY_CODE, ex );
	}
}
