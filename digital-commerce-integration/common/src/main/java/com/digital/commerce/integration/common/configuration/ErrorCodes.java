package com.digital.commerce.integration.common.configuration;

public enum ErrorCodes {
	CONFIG("00000"), COMM_ERROR("00001"), XFORM("00002"), ERROR_RESPONSE("00003"), INACTIVE("00004"), MISSING_REQUIRED(
			"00005"), GENERATION("00006"), SOURCE_UNAVAILABLE("00007");

	String code;

	ErrorCodes(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

}