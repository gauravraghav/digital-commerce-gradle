package com.digital.commerce.integration.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;

/**
 * Helper class to provide cached instances of JAXBContext for each
 * context path.
 * 
 * Acts as a singleton when exposed as a service. If using this outside of nucleus 
 * take care to wrap it in a singleton
 * 
 * @author NS389061
 *
 */
public final class JAXBContextCache {
	private DigitalLogger logger = DigitalLogger.getLogger(JAXBContextCache.class);

	private static final Map<String, JAXBContext> JAXB_CONTEXT_MAP = new ConcurrentHashMap<>();

	public JAXBContextCache() {

	}

	/**
	 * Looks at a map to see if the there is an existing instance. If the
	 * context has been created, it will be returned, otherwise a new one will
	 * be instantiated and cached and then returned.
	 * 
	 * @param pContextPath
	 *            The context path for the JAXB context
	 * @return
	 * @throws JAXBException
	 */
	public final JAXBContext newJAXBContext(String pContextPath) throws JAXBException {
		if (logger.isDebugEnabled()) {
			logger.debug("getInstance(): " + pContextPath);
		}
		JAXBContext jbContext = null;
		
		// Added a null check as ConcurrentHashMap does not allow null as key
		if (DigitalStringUtil.isNotEmpty(pContextPath)) {
			if (JAXB_CONTEXT_MAP.containsKey(pContextPath)) {
				if (logger.isDebugEnabled()) {
					logger.debug("Returning cached instance of JAXBContext for path: " + pContextPath);
				}
				jbContext = JAXB_CONTEXT_MAP.get(pContextPath);
			} else {
				if (logger.isDebugEnabled()) {
					logger.debug("Creating new instance of JAXBContext for path: " + pContextPath);
				}
				jbContext = JAXBContext.newInstance(pContextPath);
				// Added a null check as ConcurrentHashMap does not allow null
				// as value
				if (null != jbContext) {
					JAXB_CONTEXT_MAP.put(pContextPath, jbContext);
				}
			}
		}
		return jbContext;
	}
	
	public final <T> JAXBContext newJAXBContext(Class<T> clazz) throws JAXBException {
		return newJAXBContext(clazz.getPackage().getName());
	}
	
	public final <T extends Object> JAXBContext newJAXBContext(T object) throws JAXBException {
		return newJAXBContext(object.getClass().getPackage().getName());
	}
}
