package com.digital.commerce.integration.common.communication.domain;

import java.io.Serializable;

public class Recipient implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static enum RecipientType  implements Serializable {
		TO,CC,BCC
	}
	
	private RecipientType recipientType;
	private String address;
	private String name;
	
	public Recipient() {
		
	}
	
	public Recipient(RecipientType recipientType, String address) {
		super();
		this.recipientType = recipientType;
		this.address = address;
	}

	public Recipient(RecipientType recipientType, String address, String name) {
		super();
		this.recipientType = recipientType;
		this.address = address;
		this.name = name;
	}

	public RecipientType getRecipientType() {
		return recipientType;
	}

	public void setRecipientType(RecipientType recipientType) {
		this.recipientType = recipientType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
}
