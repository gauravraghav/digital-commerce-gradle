package com.digital.commerce.integration.common.service;

import java.util.Map;

public interface Service {
    boolean isServiceEnabled();
    boolean isServiceMethodEnabled(String serviceMethodName);
    
    default Map<String, Boolean> getServiceMethodsEnabled(){
    	return null;
    }
}
