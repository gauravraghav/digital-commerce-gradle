package com.digital.commerce.integration.common.exception;

public class DigitalIntegrationBusinessException extends DigitalIntegrationException{
	private static String CATEGORY_CODE = "INTEGRATION_SERVICE_RESPONSE";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DigitalIntegrationBusinessException( String message, String errorCode ) {
		super( message, errorCode, CATEGORY_CODE );
	}

	public DigitalIntegrationBusinessException( String message, String errorCode, Throwable ex ) {
		super( message, errorCode, CATEGORY_CODE, ex );
	}
}
