package com.digital.commerce.integration.common.service.client;

import java.util.Map;

public interface NVP {
	public void add(String name, String value);
	public String get(String name);
	public Map<String, String> getNameValuePairs();
}
