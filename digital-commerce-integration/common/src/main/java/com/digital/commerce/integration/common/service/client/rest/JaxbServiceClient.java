package com.digital.commerce.integration.common.service.client.rest;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.service.FaultHandler;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.util.JAXBContextCache;

/**
 * This class is meant to handle REST requests that utilize posted XML.
 * 
 * Specifically, designed to handle the above plus the unmarshalling from and marshalling to
 * JAXB generated objects.
 * 
 * @author JM406760
 *
 */
@SuppressWarnings({ "unchecked", "static-access", "rawtypes" })
public class JaxbServiceClient  extends IntegrationServiceClient {
	private static final DigitalLogger logger = DigitalLogger.getLogger(JaxbServiceClient.class);
	private JAXBContextCache jaxbContextCache;
	
	
	public JaxbServiceClient() {

	}

	protected String getService() {
		return "";
	}
	
	
	@Override
	public <T> T getUnmarshalledObjectFromHttpServiceResponse(Class classToUnmarshall, HttpServiceResponse response,
			FaultHandler faultHandler) throws DigitalIntegrationException {
		// TODO Auto-generated method stub
		return getUnmarshalledObjectFromHttpServiceResponse(classToUnmarshall, response);
	}


	/**
	 * Provides the means for unmarshalling a HTTPResponse to a proper 
	 * JAXB object.
	 */
	@Override
	public <T> T getUnmarshalledObjectFromHttpServiceResponse(Class classToUnmrashall, HttpServiceResponse response)
			throws DigitalIntegrationException {
		try {
			JAXBContext jaxbContext = getJaxbContextCache().newJAXBContext(classToUnmrashall);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			
			/* necessary for some types of JAXB objects to return the actual JAXB object rather
			 * than a JAXBElement<clazz> object
			 */
			JAXBIntrospector jaxbIntrospector = jaxbContext.createJAXBIntrospector();
			
			String responseString = response.getEntityString();
			
			logServiceResponse(responseString);
			
			T objectToReturn = (T) unmarshaller.unmarshal(new ByteArrayInputStream( responseString.getBytes()));

			return (T)jaxbIntrospector.getValue(objectToReturn);

		} catch (Exception e) {
			logger.error("Error unmarshalling response", e);
			throw new DigitalIntegrationSystemException("UnmarshalledObjectFromHttpResponse Error", ErrorCodes.XFORM.getCode(), e);
		}
	}

	/**
	 * Allows for the marshalling of a JAXB object to a String
	 */
	@Override
	public <T> String getMarshalledStringFromObject(Class classToUnmrashall, T objectToMarshall) throws DigitalIntegrationException {
		try {
			JAXBContext jaxbContext = getJaxbContextCache().newJAXBContext(classToUnmrashall);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			StringWriter bodyContent = new java.io.StringWriter();
			marshaller.marshal(objectToMarshall, bodyContent);

			String content = bodyContent.toString();
			
			logServiceRequest(content);
			
			return content;

		} catch (Exception e) {
			logger.error("Error marshalling request", e);
			throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error", ErrorCodes.XFORM.getCode(), e);
		}
	}

	public JAXBContextCache getJaxbContextCache() {
		return jaxbContextCache;
	}

	public void setJaxbContextCache(JAXBContextCache jaxbContextCache) {
		this.jaxbContextCache = jaxbContextCache;
	}

	
}
