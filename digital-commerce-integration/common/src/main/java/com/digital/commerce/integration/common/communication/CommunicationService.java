package com.digital.commerce.integration.common.communication;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.digital.commerce.integration.common.communication.domain.Recipient;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent.ContentType;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.service.Service;

public interface CommunicationService extends Service {
	public enum ServiceMethod {
		SEND("send");
		
		String serviceMethodName;
		
		ServiceMethod(String serviceMethodName) {
			this.serviceMethodName = serviceMethodName;
		}

		public String getServiceMethodName() {
			return serviceMethodName;
		}
	}
	
	public boolean send(String sender, List<Recipient> recipients, String subject, List<String> subjectDataElements, String templateKey, Map<String, String> dataElements, ContentType contentType, String siteId, Locale locale) throws DigitalIntegrationException;
	public boolean send(String sender, List<Recipient> recipients, String subject, List<String> subjectDataElements, String templateKey, Map<String, String> dataElements, ContentType contentType, String siteId) throws DigitalIntegrationException;
	public boolean send(List<Recipient> recipients, String templateKey, Map<String, String> dataElements, ContentType contentType, String siteId, Locale locale) throws DigitalIntegrationException;
	public boolean send(List<Recipient> recipients, String templateKey, Map<String, String> dataElements, ContentType contentType, String siteId) throws DigitalIntegrationException;
}
