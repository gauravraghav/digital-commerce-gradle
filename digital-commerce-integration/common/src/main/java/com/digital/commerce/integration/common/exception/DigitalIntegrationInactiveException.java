package com.digital.commerce.integration.common.exception;

public class DigitalIntegrationInactiveException extends DigitalIntegrationException {

	private static String CATEGORY_CODE = "INTEGRATION_SERVICE_INACTIVE";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public DigitalIntegrationInactiveException( String message, String errorCode ) {
		super( message, errorCode, CATEGORY_CODE );
	}

	public DigitalIntegrationInactiveException( String message, String errorCode, Throwable ex ) {
		super( message, errorCode, CATEGORY_CODE, ex );

	}
}
