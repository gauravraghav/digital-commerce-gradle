package com.digital.commerce.integration.common.service.client.rest;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.service.FaultHandler;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is meant to handle REST requests that utilize posted JSON.
 *
 * Specifically, designed to handle the above plus the unmarshalling from and marshalling to JSON
 * generated objects.
 */
public class JaxrsJsonServiceClient extends IntegrationServiceClient {

  private static final DigitalLogger logger = DigitalLogger.getLogger(JaxrsJsonServiceClient.class);
  private ObjectMapper objectMapper;

  /**
   * Default constructor
   */
  public JaxrsJsonServiceClient() {

  }

  /**
   * @return String
   */
  protected String getService() {
    return "";
  }


  /**
   * @param classToUnmarshall
   * @param response
   * @param faultHandler
   * @param <T>
   * @return T
   * @throws DigitalIntegrationException
   */
  @Override
  public <T> T getUnmarshalledObjectFromHttpServiceResponse(Class classToUnmarshall,
      HttpServiceResponse response,
      FaultHandler faultHandler) throws DigitalIntegrationException {
    return getUnmarshalledObjectFromHttpServiceResponse(classToUnmarshall, response);
  }


  /**
   * Provides the means for unmarshalling a HTTPResponse to a proper JAXB object.
   *
   * @param classToUnmrashall
   * @param response
   * @param <T>
   * @return
   * @throws DigitalIntegrationException
   */
  @Override
  public <T> T getUnmarshalledObjectFromHttpServiceResponse(Class classToUnmrashall,
      HttpServiceResponse response)
      throws DigitalIntegrationException {
    try {

      String responseString = response.getEntityString();

      logServiceResponse(responseString);

      T objectToReturn = (T) getObjectMapper()
          .readValue(response.getEntityString(), classToUnmrashall);

      return objectToReturn;

    } catch (Exception e) {
      logger.error("Error unmarshalling response", e);
      throw new DigitalIntegrationSystemException("UnmarshalledObjectFromHttpResponse Error",
          ErrorCodes.XFORM.getCode(), e);
    }
  }

  /**
   * Allows for the marshalling of a JAXB object to a String
   *
   * @param classToUnmrashall
   * @param objectToMarshall
   * @param <T>
   * @return String
   * @throws DigitalIntegrationException
   */
  @Override
  public <T> String getMarshalledStringFromObject(Class classToUnmrashall, T objectToMarshall)
      throws DigitalIntegrationException {
    try {
      String bodyContent = getObjectMapper().writeValueAsString(objectToMarshall);

      logServiceRequest(bodyContent);

      return bodyContent;

    } catch (Exception e) {
      logger.error("Error marshalling request", e);
      throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error",
          ErrorCodes.XFORM.getCode(), e);
    }
  }

  /**
   * @return ObjectMapper
   */
  public ObjectMapper getObjectMapper() {
    return objectMapper;
  }

  /**
   * @param objectMapper
   */
  public void setObjectMapper(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }
}
