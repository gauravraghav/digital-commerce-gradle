package com.digital.commerce.integration.common.service;

import javax.xml.soap.SOAPBody;

public interface FaultHandler {
	public String getFaultDetails(SOAPBody soapBody) throws Exception;
}
