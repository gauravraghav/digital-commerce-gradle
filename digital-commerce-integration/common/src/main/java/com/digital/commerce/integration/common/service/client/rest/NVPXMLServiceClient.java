package com.digital.commerce.integration.common.service.client.rest;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.service.FaultHandler;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.common.service.client.NVP;

/**
 * This class is meant to handle REST requests that utilize posted XML.
 * 
 * Specifically, designed to handle the above plus the unmarshalling from and marshalling to
 * JAXB generated objects.
 * 
 * @author JM406760
 *
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class NVPXMLServiceClient extends IntegrationServiceClient {
	private static final DigitalLogger logger = DigitalLogger.getLogger(NVPXMLServiceClient.class);
	private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	
	public NVPXMLServiceClient() {

	}

	
	@Override
	protected String getService() {
		return "";
	}
	
	public <T> T getUnmarshalledObjectFromHttpServiceResponse(Class classToUnmarshall, HttpServiceResponse response,
			FaultHandler faultHandler) throws DigitalIntegrationException {
		
		return getUnmarshalledObjectFromHttpServiceResponse(classToUnmarshall, response);
	}


	/**
	 * Provides the means for unmarshalling a HTTPResponse to a proper 
	 * JAXB object.
	 */
	@Override
	public <T> T getUnmarshalledObjectFromHttpServiceResponse(Class classToUnmrashall, HttpServiceResponse response)
			throws DigitalIntegrationException {
		try {
			
			T classToReturn = (T)classToUnmrashall.newInstance();
			
			if (!( classToReturn instanceof NVP)) {
				throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error: Can only marshall NVP object", ErrorCodes.XFORM.getCode());
			}
			
			String responseString = response.getEntityString();
			
			logServiceResponse(responseString);
			
			// Must be valid xml or an exception will be thrown			
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        Document nvpDocument = builder.parse(new InputSource(new StringReader(responseString)));
	        NodeList nvpEntryNodeList = nvpDocument.getDocumentElement().getChildNodes();
	        
	        /*
	         * cycle over all the child nodes. NVP XML documents cannot be nested
	         */
	        for (int i = 0; i < nvpEntryNodeList.getLength(); i++) {
	        	Node nvpNode = nvpEntryNodeList.item(i);
	        	
	        	if (nvpNode.getChildNodes().getLength() > 1) {
	        		throw new DigitalIntegrationBusinessException("UnmarshalledObjectFromHttpResponse Error: NVP-style XML cannot have nested elements", ErrorCodes.XFORM.getCode());
	        	}
	        	
	        	((NVP)classToReturn).add(nvpNode.getNodeName(), nvpNode.getTextContent());
	        }
	        
			return classToReturn;
		} catch (Exception e) {
			logger.error("Error unmarshalling response", e);
			throw new DigitalIntegrationSystemException("UnmarshalledObjectFromHttpResponse Error",ErrorCodes.XFORM.getCode(), e);
		}
	}

	/**
	 * Allows for the marshalling of a JAXB object to a String
	 */
	@Override	
	public <T> String getMarshalledStringFromObject(Class classToUnmrashall, T objectToMarshall) throws DigitalIntegrationException {
		try {
			
			if (objectToMarshall == null || !( objectToMarshall instanceof NVP)) {
				throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error: Can only marshall NVP object", ErrorCodes.XFORM.getCode());
			}
			
			//create XML doc from NVP pairs
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document nvpDocument = builder.newDocument(); 
	        Element root = nvpDocument.createElement( objectToMarshall.getClass().getSimpleName() );
	        nvpDocument.appendChild(root);
	        
	        Map<String, String> nvpEntries = ((NVP)objectToMarshall).getNameValuePairs();
	        
	        for (Entry<String,String> entry: nvpEntries.entrySet()) {
	        	Element nvpNode = nvpDocument.createElement(entry.getKey());
	        	nvpNode.setTextContent(entry.getValue());
	        	//nvpNode.appendChild(nvpDocument.createTextNode(entry.getValue()));
	        	root.appendChild( nvpNode);
	        }
	        
			// Return XML as a String
	        TransformerFactory tf = TransformerFactory.newInstance();
	        Transformer transformer = tf.newTransformer();
	        
	        //transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	        
	        StringWriter writer = new StringWriter();
	        transformer.transform(new DOMSource(nvpDocument), new StreamResult(writer));
	        
	        String nvpXMLString = writer.getBuffer().toString();
			
	        logServiceRequest(nvpXMLString);
	        
	        return nvpXMLString;
			
		} catch (Exception e) {
			logger.error("Error marshalling request", e);
			throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error",ErrorCodes.XFORM.getCode(), e);
		}
	}

}
