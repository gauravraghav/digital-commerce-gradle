package com.digital.commerce.integration.common.domain;

import java.io.Serializable;

public class Item implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name = "";
    private String description = "";
    private String price = "";
    private String quantity = "";
    private String sku = "";

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }
}
