package com.digital.commerce.integration.common.service;

import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

import atg.naming.NameContext;
import atg.naming.NameContextBindingEvent;
import atg.naming.NameContextBindingListener;
import atg.nucleus.Configuration;
import atg.nucleus.Nucleus;
import atg.nucleus.Service;
import atg.nucleus.ServiceEvent;
import atg.nucleus.ServiceException;

public abstract class BaseService implements Service, IntegrationService, NameContextBindingListener{

	private Nucleus nucleus;
	private Configuration configuration;
	private boolean running = false;
	private String componentName = "";
	private NameContext nameContext;
	
	@Override
	public Nucleus getNucleus() {
		return nucleus;
	}

	@Override
	public Configuration getServiceConfiguration() {
		return configuration;
	}

	@Override
	public boolean isRunning() {
		return running;
	}

	@Override
	public void startService(ServiceEvent ev) throws ServiceException {
		if (ev.getService() == this && !running) {
			nucleus = ev.getNucleus();
			configuration = ev.getServiceConfiguration();
			running = true;
			
			try {
				postStartup();
			} catch (DigitalIntegrationException e) {
				throw new ServiceException(e);
			}
		}
	}

	@Override
	public void stopService() throws ServiceException {
		if (running) {
			running = false;
			try {
				postShutdown();
			} catch (DigitalIntegrationException e) {
				throw new ServiceException(e);
			}
		}
	}

	@Override
	public void nameContextElementBound(NameContextBindingEvent ev) {
		if (ev.getElement() == this) {
			nameContext = ev.getNameContext();
			componentName = ev.getName();
		}

	}

	@Override
	public void nameContextElementUnbound(NameContextBindingEvent ev) {
		if (ev.getElement() == this) {
			nameContext = null;
			componentName = null;
		}
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	
	public NameContext getNameContext() {
		return nameContext;
	}

}
