package com.digital.commerce.integration.common.communication.smtp;

import java.util.HashMap;

import atg.nucleus.GenericService;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent;
import com.digital.commerce.integration.common.jms.DigitalJMSClient;

public class SMTPCommunicationServiceSource extends GenericService {
	private DigitalJMSClient jmsClient;

	public DigitalJMSClient getJmsClient() {
		return jmsClient;
	}

	public void setJmsClient(DigitalJMSClient jmsClient) {
		this.jmsClient = jmsClient;
	}

	/**
	 * @param event
	 */
	protected void addToQueue(DigitalEmailEvent event) {

		if (isRunning()) {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
					.getClass().getName(), "addToQueue");
			if (isLoggingDebug()) {
				logDebug("Preparing to queue emailevent message: "
						+ event.getTemplateKey());
			}
			HashMap<String, Object> jmProperties = null;
			try {
				if (null != event.getDeliveryDelay()
						&& event.getDeliveryDelay() > 0l) {
					if (isLoggingDebug()) {
						logDebug(String
								.format("Adding DigitalEmailEvent to queue: %s with delay: %s ",
										event.getTemplateKey(),
										event.getDeliveryDelay()));
					}
					jmProperties = new HashMap<>();
					jmProperties
							.put("DELIVERY_DELAY", event.getDeliveryDelay());
				}
				jmsClient.sendObjectMessage("queue/EMAILSERVICE_LOCAL_QUEUE",
						event, jmProperties, event.isTransacted());

			} catch (Exception ex) {
				logError(
						"Cannot add DigitalEmailEvent to queue: "
								+ event.getTemplateKey(), ex);
			} finally {
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
						.getClass().getName(), "addToQueue");
			}
		} else {
			logError(String.format(
					"Emailevent message not queued: %s!; mContext : %s",
					event.getTemplateKey(), isRunning()));

		}
	}
}
