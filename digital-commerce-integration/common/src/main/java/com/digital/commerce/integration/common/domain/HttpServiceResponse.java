package com.digital.commerce.integration.common.domain;

public class HttpServiceResponse {

	private String entityString;
	private String contentType;
	private int statusCode;

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getEntityString() {
		return entityString;
	}
	public void setEntityString(String entityString) {
		this.entityString = entityString;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
	
}
