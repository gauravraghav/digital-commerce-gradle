package com.digital.commerce.integration.common.service.client.soap;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.service.FaultHandler;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.common.service.client.ServiceRequest;
import com.digital.commerce.integration.util.JAXBContextCache;

/**
 * This class is meant to handle SOAP requests.
 * 
 * Specifically, designed to handle the above plus the unmarshalling from and
 * marshalling to JAXB generated objects.
 * 
 * @author JM406760
 *
 */
public class JaxbServiceClient extends IntegrationServiceClient {
	private static final DigitalLogger logger = DigitalLogger.getLogger(JaxbServiceClient.class);
	private JAXBContextCache jaxbContextCache;
	
	
	public JaxbServiceClient() {

	}

	
	@Override
	protected String getService() {
		return "";
	}


	/**
	 * Method to generate the soap envelope that will contain the payload
	 * provided by the ServiceRequest body
	 */
	@Override
	public HttpServiceResponse processHttpRequest(ServiceRequest serviceRequest) throws DigitalIntegrationException {

		HttpUriRequest request = createPostRequest(serviceRequest.getConnectionTimeout(),
				serviceRequest.getRequestTimeout(), serviceRequest.getServiceUri(),
				serviceRequest.getServiceContentType().getContentType(), serviceRequest.getHeaderEntries());
		long startTime = 0l;
		DefaultHttpClient httpClient = null;
		HttpResponse response = null;
		String serviceName = null; String methodName = null;
		int responseStatus=0;
		try {
			
			serviceName = this.getServiceName();
			methodName = serviceRequest.getServiceMethodName();

			MessageFactory mf = MessageFactory.newInstance();
			SOAPMessage message = mf.createMessage();

			SOAPPart soapPart = message.getSOAPPart();
			SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
			SOAPBody body = soapEnvelope.getBody();

			/*
			 * Converts the body of the servicerequest to an XML document.
			 * String must be valid XML or an exception will be thrown
			 */

			body.addDocument(IntegrationServiceClient.convertStringToDocument(serviceRequest.getBody()));

			message.saveChanges();

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			message.writeTo(byteArrayOutputStream);

			String requestString = new String(byteArrayOutputStream.toByteArray());

			logServiceRequest(requestString);

			StringEntity bodyEntity = new StringEntity(requestString);

			bodyEntity.setContentType(serviceRequest.getServiceContentType().getContentType());

			((HttpPost) request).setEntity(bodyEntity);

			if (serviceRequest.getSoapAction() != null) {
				request.addHeader(IntegrationServiceClient.HEADER_SOAP_ACTION, serviceRequest.getSoapAction());
			}
			// create http client
			httpClient = initializeHttpClient();
			
			startTime = System.nanoTime();
			response = httpClient.execute(request);
			
			HttpServiceResponse httpServiceResponse = new HttpServiceResponse();
			httpServiceResponse.setEntityString(EntityUtils.toString(response.getEntity()));
			responseStatus  = response.getStatusLine().getStatusCode();
			httpServiceResponse.setStatusCode(responseStatus);

			return httpServiceResponse;
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder()
					.append("ProcessHttpRequest Exception: ")
					.append(e.toString()).append(" : ").append(serviceName)
					.append(" : ").append(methodName);
			logger.error(sb.toString());
			logger.error("Exception from httpclient call ", e);
			if (response != null) {
				EntityUtils.consumeQuietly(response.getEntity());
			}
			throw new DigitalIntegrationSystemException("ProcessHttpRequest Error",
					ErrorCodes.COMM_ERROR.getCode(), e);

		} finally {
			StringBuilder sb = new StringBuilder().append(serviceName).append(" : ")
					.append(methodName).append(": ").
					append(TimeUnit.NANOSECONDS.toMillis( System.nanoTime() - startTime )).append("ms").append(" : httpStatus ").append(responseStatus);
			logger.info(sb.toString());
			if(null != httpClient) {
				httpClient.getConnectionManager().shutdown();
			}
		}

	}

	/**
	 * Method to generate the soap envelope that will contain the payload
	 * provided by the ServiceRequest body. Creates the appropriate elements for
	 * WSSE authentication and adds it to the generated envelope.
	 */
	@Override
	public HttpServiceResponse processHttpRequest(ServiceRequest serviceRequest, String username, String password)
			throws DigitalIntegrationException {
		
		
		HttpUriRequest request = createPostRequest(serviceRequest.getConnectionTimeout(),
				serviceRequest.getRequestTimeout(), serviceRequest.getServiceUri(),
				serviceRequest.getServiceContentType().getContentType(), serviceRequest.getHeaderEntries());

		long startTime = 0l;

		HttpResponse response = null;
		String serviceName = null; String methodName = null;
		DefaultHttpClient httpClient = null;
		int responseStatus=0;
		try {
			
			serviceName = this.getServiceName();
			methodName = serviceRequest.getServiceMethodName();
			
			MessageFactory mf = MessageFactory.newInstance();
			SOAPMessage message = mf.createMessage();

			SOAPPart soapPart = message.getSOAPPart();
			SOAPEnvelope soapEnvelope = soapPart.getEnvelope();

			SOAPBody body = soapEnvelope.getBody();
			SOAPElement header = soapEnvelope.getHeader();

			SOAPElement security = header.addChildElement("Security", "wsse",
					"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");

			SOAPElement usernameToken = security.addChildElement("UsernameToken", "wsse");
			usernameToken.addAttribute(new QName("xmlns:wsu"),
					"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

			SOAPElement usernameNode = usernameToken.addChildElement("Username", "wsse");
			usernameNode.addTextNode(username);

			SOAPElement passwordNode = usernameToken.addChildElement("Password", "wsse");  //NOSONAR
			passwordNode.setAttribute("Type",
					"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
			passwordNode.addTextNode(password);

			body.addDocument(IntegrationServiceClient.convertStringToDocument(serviceRequest.getBody()));

			message.saveChanges();

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			message.writeTo(byteArrayOutputStream);

			String requestString = new String(byteArrayOutputStream.toByteArray());

			logServiceRequest(requestString);

			StringEntity bodyEntity = new StringEntity(requestString);

			bodyEntity.setContentType(serviceRequest.getServiceContentType().getContentType());

			((HttpPost) request).setEntity(bodyEntity);

			if (serviceRequest.getSoapAction() != null) {
				request.addHeader(IntegrationServiceClient.HEADER_SOAP_ACTION, serviceRequest.getSoapAction());
			}
			// create http client
			httpClient = initializeHttpClient();	
			
			startTime = System.nanoTime();


			if(logger.isDebugEnabled()) {
				if (getConnectionManager() != null) {
					logger.debug(
							"Before - Leased Connections = " + getConnectionManager().getTotalStats().getLeased());
					logger.debug("Before - Available Connections = " + getConnectionManager().getTotalStats()
							.getAvailable());
				}
			}

			response = httpClient.execute(request);

			if(logger.isDebugEnabled()) {
				if (getConnectionManager() != null) {
					logger.debug(
							"After - Leased Connections = " + getConnectionManager().getTotalStats().getLeased());
					logger.debug("After - Available Connections = " + getConnectionManager().getTotalStats()
							.getAvailable());
				}
			}
			
			HttpServiceResponse httpServiceResponse = new HttpServiceResponse();

			HttpEntity entity = response.getEntity();

			if (null != entity) {
				String content;
				if(null != entity.getContentType()) {
					String contentType = entity.getContentType().getValue();
					String charset = getResponseCharset(contentType);
					content = EntityUtils.toString(entity, charset);
				}
				else{
					content = EntityUtils.toString(entity);
				}
				httpServiceResponse.setEntityString(content);
			}

			responseStatus = response.getStatusLine().getStatusCode();
			httpServiceResponse.setStatusCode(responseStatus);
			if (doesRequireConnectionPool() && doesRequireGracefulConnectionShutdown()) {
				// detect idle and expired connections and close them
				IdleConnectionMonitorThread staleMonitor = new IdleConnectionMonitorThread(
						getConnectionManager());
				staleMonitor.start();
			}
			return httpServiceResponse;
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder()
					.append("ProcessHttpRequest Exception: ")
					.append(e.toString()).append(" : ").append(serviceName)
					.append(" : ").append(methodName);
			logger.error(sb.toString());
			logger.error("Exception from httpclient call ", e);
			if (response != null) {
				EntityUtils.consumeQuietly(response.getEntity());
			}
			throw new DigitalIntegrationSystemException("ProcessHttpRequest Error",
					ErrorCodes.COMM_ERROR.getCode(), e);

		} finally {
			StringBuilder sb = new StringBuilder().append(serviceName).append(" : ")
					.append(methodName).append(": ").
					append(TimeUnit.NANOSECONDS.toMillis( System.nanoTime() - startTime )).append("ms").append(" : httpStatus ").append(responseStatus);
			logger.info(sb.toString());
			// shutdown connection manager if connection pool is not used
			if(null != httpClient) {
				if(!doesRequireConnectionPool()){
					httpClient.getConnectionManager().shutdown();
				}
			}
		}

	}

	
	@Override
	public <T> T getUnmarshalledObjectFromHttpServiceResponse(@SuppressWarnings("rawtypes") Class classToUnmarshall, HttpServiceResponse response)
			throws DigitalIntegrationException {
		
		return getUnmarshalledObjectFromHttpServiceResponse(classToUnmarshall, response, null);
	}

	/**
	 * Provides the means for unmarshalling a HTTPResponse to a proper JAXB
	 * object from the soap envelope
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T> T getUnmarshalledObjectFromHttpServiceResponse(Class classToUnmrashall, HttpServiceResponse response, FaultHandler faultHandler)
			throws DigitalIntegrationException {
		
		try {
			T objectToReturn = null;
			JAXBContext jaxbContext = getJaxbContextCache().newJAXBContext(classToUnmrashall);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			
			String responseString = response.getEntityString();

			SOAPMessage message = MessageFactory.newInstance().createMessage(null,
					new ByteArrayInputStream(responseString.getBytes()));

			SOAPPart soapPart = message.getSOAPPart();
			SOAPEnvelope soapEnvelope = soapPart.getEnvelope();

			SOAPBody body = soapEnvelope.getBody();

			if (body.hasFault()) {				
				String errorString = "";
				
				if (faultHandler != null) {
					errorString = faultHandler.getFaultDetails(body);
				}
				
				throw new DigitalIntegrationBusinessException(errorString,
						ErrorCodes.ERROR_RESPONSE.getCode());

			} else {
				Document  responseDocument = body.extractContentAsDocument();				
		        
				logServiceResponse(responseString);
				objectToReturn = (T) unmarshaller.unmarshal(responseDocument);
			}

			return objectToReturn;

		} catch (DigitalIntegrationBusinessException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Error unmarshalling response", e);
			throw new DigitalIntegrationSystemException("UnmarshalledObjectFromHttpResponse Error",
					ErrorCodes.XFORM.getCode(), e);

		} 
	}

	/**
	 * Allows for the marshalling of a JAXB object to a String
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> String getMarshalledStringFromObject(Class classToUnmrashall, T objectToMarshall)
			throws DigitalIntegrationException {
		try {
			JAXBContext jaxbContext = getJaxbContextCache().newJAXBContext(classToUnmrashall);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			StringWriter bodyContent = new java.io.StringWriter();
			marshaller.marshal(objectToMarshall, bodyContent);

			if (logger.isDebugEnabled()) {
				logger.debug("Body content from getMarshalledStringFromObject: " + bodyContent);
			}
			
			return bodyContent.toString();

		} catch (Exception e) {
			logger.error("Error marshalling request", e);
			throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error", ErrorCodes.XFORM.getCode(), e);

		}
	}

	public JAXBContextCache getJaxbContextCache() {
		return jaxbContextCache;
	}

	public void setJaxbContextCache(JAXBContextCache jaxbContextCache) {
		this.jaxbContextCache = jaxbContextCache;
	}

}
