package com.digital.commerce.integration.common.communication.smtp;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import com.digital.commerce.common.exception.DigitalRuntimeException;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent;

import atg.dms.patchbay.MessageSink;

public class SMTPCommunicationServiceSink implements MessageSink {
	private static final DigitalLogger logger = DigitalLogger.getLogger(SMTPCommunicationServiceSink.class);
	private SMTPCommunicationService emailService;

	@Override
	public void receiveMessage(String s, javax.jms.Message message) throws JMSException {
		if (logger.isDebugEnabled()) {
			logger.debug("Sending Email for the event recieved");
		}
		try {
			DigitalEmailEvent dswEmailEvent = (DigitalEmailEvent) ((ObjectMessage) message).getObject();
			emailService.sendSMTPMessage(dswEmailEvent);
		} catch (Throwable e) {
			logger.error("Error occurred when reading the email event " + e.getMessage());
			throw new DigitalRuntimeException(e);
		}
	}

	/**
	 * 
	 * @return SMTPCommunicationService
	 */
	public SMTPCommunicationService getEmailService() {
		return emailService;
	}

	/**
	 * 
	 * @param emailService
	 */
	public void setEmailService(SMTPCommunicationService emailService) {
		this.emailService = emailService;
	}

}
