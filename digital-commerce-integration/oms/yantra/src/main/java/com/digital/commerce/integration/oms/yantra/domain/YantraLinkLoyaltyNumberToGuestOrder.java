package com.digital.commerce.integration.oms.yantra.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class YantraLinkLoyaltyNumberToGuestOrder implements Serializable {

  String orderId;
  String loyaltyNumber;
  String loyaltyTier;

}
