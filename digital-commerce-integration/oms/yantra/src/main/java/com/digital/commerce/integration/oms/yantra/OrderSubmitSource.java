/**
 *
 */
package com.digital.commerce.integration.oms.yantra;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilderFactory;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.jms.DigitalJMSClient;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.oms.OrderManagementService;
import com.digital.commerce.integration.oms.yantra.domain.YantraLinkLoyaltyNumberToGuestOrder;
import com.digital.commerce.integration.util.JAXBContextCache;
import com.yantra.service.beans.order.linkloyalty.ObjectFactory;
import com.yantra.service.beans.order.linkloyalty.Order;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderSubmitSource extends BaseIntegrationService implements /*MessageSource,*/
    OrderManagementService {

  private static DigitalLogger logger = DigitalLogger.getLogger(OrderSubmitSource.class);

  private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

  public OrderSubmitSource() {
  }

  @Override
  protected String getService() {
    return WebServicesConfig.ServicNames.YANTRA_OMS.getName();
  }

  private JAXBContextCache jaxbContextCache;

  private DigitalJMSClient jmsClient;

  /**
   * Requires and XML string of the order
   */
  @Override
  public void submitOrder(String orderAsXML) throws DigitalIntegrationException {
    boolean hasException = false;
    String serviceMethodName = ServiceMethod.ORDER_SUBMIT
        .getServiceMethodName();

    if (doRunService(serviceMethodName)) {
      startPerformanceMonitorOperation(serviceMethodName);
      try {
        if (logger.isDebugEnabled()) {
          logger.debug(String.format("%s - %s: REQUEST - %s",
              "OrderSubmitSource", "sendOneMessage", orderAsXML));
          logger.debug("Preparing to send S12 Order XML to YANTRA");
        }
        getJmsClient().sendTextMessage("queue/S12_LOCAL_QUEUE",
            orderAsXML, null, false);
        if (logger.isDebugEnabled()) {
          logger.debug("Successfully sent S12 Order XML to YANTRA");
        }
      } catch (Exception e) {
        logger.error("Exception while sending S12", e);
        hasException = true;
        if (e instanceof DigitalIntegrationSystemException) {
          throw e;
        } else {
          throw new DigitalIntegrationSystemException(
              "Exception Submitting Order",
              ErrorCodes.COMM_ERROR.getCode(), e);
        }
      } finally {
        endPerformanceMonitorOperation(serviceMethodName, hasException);
      }
    } else {
      throw new DigitalIntegrationInactiveException(String.format(
          "Service / Method Is Inactive: %s_%s", getComponentName(),
          serviceMethodName), ErrorCodes.INACTIVE.getCode());
    }

  }

  @Override
  public void linkOrderWithLoyalty(YantraLinkLoyaltyNumberToGuestOrder order)
      throws DigitalIntegrationException {

    boolean hasException = false;
    String serviceMethodName = ServiceMethod.LINK_ORDER_LOYALTY
        .getServiceMethodName();

    ObjectFactory objectFactory = new ObjectFactory();
    Order orderDetails = objectFactory.createOrder();
    orderDetails.setId(order.getOrderId());
    orderDetails.setLoyaltyId(order.getLoyaltyNumber());
    orderDetails.setLoyaltyTier(order.getLoyaltyTier());

    String orderAsXML = getMarshalledStringFromObject(Order.class, orderDetails);

    if (doRunService(serviceMethodName)) {
      startPerformanceMonitorOperation(serviceMethodName);
      try {
        if (logger.isDebugEnabled()) {
          logger.debug(String.format("%s - %s: REQUEST - %s",
              "OrderSubmitSource", "linkOrderWithLoyalty", orderAsXML));
          logger.debug("Preparing to send Link Order With Rewards XML to YANTRA");
        }
        getJmsClient().sendTextMessage("queue/S211_LOCAL_QUEUE",
            orderAsXML, null, false);
        if (logger.isDebugEnabled()) {
          logger.debug("Successfully sent Link Order With Rewards XML to YANTRA");
        }
      } catch (Exception e) {
        logger.error("Exception while sending Link Order With Rewards ", e);
        hasException = true;
        if (e instanceof DigitalIntegrationSystemException) {
          throw e;
        } else {
          throw new DigitalIntegrationSystemException(
              "Exception sending Link Order With Rewards ",
              ErrorCodes.COMM_ERROR.getCode(), e);
        }
      } finally {
        endPerformanceMonitorOperation(serviceMethodName, hasException);
      }
    } else {
      throw new DigitalIntegrationInactiveException(String.format(
          "Service / Method Is Inactive: %s_%s", getComponentName(),
          serviceMethodName), ErrorCodes.INACTIVE.getCode());
    }
  }

  @Override
  public void postStartup() throws DigitalIntegrationException {
    // TODO Auto-generated method stub

  }

  @Override
  public void postShutdown() throws DigitalIntegrationException {
    // TODO Auto-generated method stub

  }

  /**
   * Allows for the marshalling of a JAXB object to a String
   */
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public <T> String getMarshalledStringFromObject(Class classToUnmrashall, T objectToMarshall)
      throws DigitalIntegrationException {
    try {
      JAXBContext jaxbContext = getJaxbContextCache().newJAXBContext(classToUnmrashall);
      Marshaller marshaller = jaxbContext.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      StringWriter bodyContent = new java.io.StringWriter();
      marshaller.marshal(objectToMarshall, bodyContent);

      if (logger.isDebugEnabled()) {
        logger.debug("Body content from getMarshalledStringFromObject: " + bodyContent);
      }

      return bodyContent.toString();

    } catch (Exception e) {
      logger.error("Error marshalling request", e);
      throw new DigitalIntegrationSystemException("MarshalledStringFromObject Error", ErrorCodes.XFORM.getCode(), e);

    }
  }

}
