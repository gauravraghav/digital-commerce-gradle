package com.digital.commerce.integration.oms;

import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.service.Service;
import com.digital.commerce.integration.oms.yantra.domain.YantraLinkLoyaltyNumberToGuestOrder;

import lombok.Getter;

public interface OrderManagementService extends Service {
	@Getter
	public enum ServiceMethod {
		ORDER_SUBMIT("s012"),
		LINK_ORDER_LOYALTY("s211");

		String serviceMethodName;
		
		ServiceMethod(String serviceMethodName) {
			this.serviceMethodName = serviceMethodName;
		}

	}
	
	public void submitOrder( String orderAsXML ) throws DigitalIntegrationException;

	public void linkOrderWithLoyalty( YantraLinkLoyaltyNumberToGuestOrder order ) throws DigitalIntegrationException;
}
