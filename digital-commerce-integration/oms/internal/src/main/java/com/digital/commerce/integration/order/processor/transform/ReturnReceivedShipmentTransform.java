package com.digital.commerce.integration.order.processor.transform;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.OrderTotals;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.order.processor.domain.payload.ReturnReceivedShipmentPayload;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

public class ReturnReceivedShipmentTransform extends OrderStatusTransform {
	private static final String PREFIX = "/Notification/Payload";
	private static final String ORDER_XPATH = PREFIX + "/Order";
	private static XPathExpression orderXPath;	
	private static final String RETURN_ORDER_XPATH = "ReturnOrders/ReturnOrder ";
	private static XPathExpression  returnOrderXPath;
	private static final String BILL_TO_PERSON_XPATH = "PersonInfoBillTo";
	private static XPathExpression billToPersonXpath;
	private static final String SHIP_TO_PERSON_XPATH = "PersonInfoShipTo";
	private static XPathExpression shipToPersonXpath;
	private static final String PAYMENT_METHOD_XPATH = "PaymentMethods/PaymentMethod";
	private static XPathExpression paymentMethodXPath;
	private static final String ORDERLINES_XPATH = "OrderLines/OrderLine";
	private static XPathExpression orderLinesXPath;
	private static final String EXTN_XPATH = "Extn";
	private static XPathExpression extnXPath;

	private static boolean isInit = false;

	@Override
	public OrderStatusPayload transform(Document xmlPayloadDocument, OrderStatusEvent.OrderStatusType orderStatusType)
			throws DigitalIntegrationException {
		ReturnReceivedShipmentPayload payload = new ReturnReceivedShipmentPayload();

		configure();

		populateOmniOrderUpdate(payload, xmlPayloadDocument, orderStatusType.getReceivedType());

		Element order = (Element) getXPathNode(orderXPath, xmlPayloadDocument);

		populateGeneralFields(payload, order);

		payload.setCustomerFirstName(order.getAttribute("CustomerFirstName"));
		payload.setCustomerLastName(order.getAttribute("CustomerLastName"));
		payload.setCustomerEMailID(order.getAttribute("CustomerEMailID"));

		Element personInfoShipToElement = (Element) getXPathNode(
				shipToPersonXpath, order);
		payload.setPersonInfoShipTo(getPersonFromElement(personInfoShipToElement));

		Element personInfoBillToElement = (Element) getXPathNode(
				billToPersonXpath, order);
		payload.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));

		NodeList orderLinesNodes = getXPathNodeList(orderLinesXPath, order);
		payload.setOrderLines(getOrderLinesFromNodeList(orderLinesNodes));

		Element extnLoyalty = (Element) getXPathNode(extnXPath, order);
		if(extnLoyalty != null){
			payload.setLoyaltyId(extnLoyalty.getAttribute("ExtnLoyaltyId"));
			payload.setLoyaltyTier(extnLoyalty.getAttribute("ExtnLoyaltyTier"));
		}

		NodeList paymentMethodNodes = getXPathNodeList(paymentMethodXPath,
				order);
		payload.setPaymentMethods(getPaymentMethodsFromNodeList(paymentMethodNodes));

		populatePayload(payload, order);

		populateOrderSummary(payload, xmlPayloadDocument);
		
		NodeList returnOrders =  getXPathNodeList(returnOrderXPath, order);
		
		List<OrderLine> returnOrderLines = new ArrayList<>();
		for(int i=0; i<returnOrders.getLength();i++){
			org.w3c.dom.Node returnOrder = returnOrders.item(i);

			ReturnReceivedShipmentPayload returnPayload = new ReturnReceivedShipmentPayload();
			returnPayload.setEventType(payload.getEventType());
			populatePayload(returnPayload, (Element)returnOrder);
			returnOrderLines.addAll(returnPayload.getOrderLines());

			populateRefundTotals(payload, (Element)returnOrder);
		}
		filterReturnOrderLine(payload, returnOrderLines);
		return payload;
	}

	List<OrderLine> filterReturnOrderLine(OrderStatusPayload payload, List<OrderLine> returnOrderLines){
		List<OrderLine> payloadOrderaLines = payload.getOrderLines();
		List<OrderLine> removeList = new ArrayList<>();
		
		for(OrderLine orderLine : payload.getOrderLines()){
			String key = orderLine.getKey();
			boolean foundReturnOrderLine=false;
			for(OrderLine returnOrderline : returnOrderLines){
				if(key.equalsIgnoreCase(returnOrderline.getDerivedFromOrderLineKey())){
					foundReturnOrderLine=true;
					break;
				}
			}
			if(!foundReturnOrderLine){
				removeList.add(orderLine);
			}
		}
		
		payloadOrderaLines.removeAll(removeList);
		
		return payloadOrderaLines;
	}

	/**
	 * 
	 * @param payload
	 * @param inOrder
	 * @param order
	 * @throws DigitalIntegrationException
	 */
	protected void populateRefundTotals(OrderStatusPayload payload, Element order)
			throws DigitalIntegrationException {
		OrderTotals totals = payload.getTotals();
		if (totals == null) {
			totals = new OrderTotals();
			payload.setTotals(totals);
		}

		// This logic in only for RETURN_RECEIVED notification type
		if (order != null) {
			Element totalElement = (Element) getXPathNode(totalXPath, order);
			OrderTotals returnOrderTotals = getTotalsFromElement(totalElement);
			totals.setReturnRefundAmount(totals.getReturnRefundAmount() + returnOrderTotals.getGrandTotal());
		}
		
	}	
	
	protected void configure() throws DigitalIntegrationException {
		if (!isInit) {
			try {
				orderXPath = getXpathFactory().newXPath().compile(ORDER_XPATH);
				billToPersonXpath = getXpathFactory().newXPath().compile(
						BILL_TO_PERSON_XPATH);
				shipToPersonXpath = getXpathFactory().newXPath().compile(
						SHIP_TO_PERSON_XPATH);
				paymentMethodXPath = getXpathFactory().newXPath().compile(
						PAYMENT_METHOD_XPATH);
				orderLinesXPath = getXpathFactory().newXPath().compile(
						ORDERLINES_XPATH);				
				returnOrderXPath = getXpathFactory().newXPath().compile(RETURN_ORDER_XPATH);
				extnXPath = getXpathFactory().newXPath().compile(EXTN_XPATH);
				isInit = true;
			} catch (XPathExpressionException e) {
				throw new DigitalIntegrationException(
						"Not able to generate xpath expressions",
						ErrorCodes.XFORM.getCode(), "TRANSFORM", e);
			}
		}
	}
}
