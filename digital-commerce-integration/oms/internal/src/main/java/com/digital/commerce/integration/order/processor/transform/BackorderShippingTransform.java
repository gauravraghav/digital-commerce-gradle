package com.digital.commerce.integration.order.processor.transform;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.payload.BackorderShippingPayload;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

public class BackorderShippingTransform extends OrderStatusTransform {
	
	private static final String ORDER_XPATH = PREFIX + "/MergedDocument/InputDocument/Order";
	private static XPathExpression orderXPath;	
	private static final String ENV_ORDER_XPATH = PREFIX + "/MergedDocument/EnvironmentDocument/Order";
	private static XPathExpression envOrderXPath;	
	private static final String BILL_TO_PERSON = "PersonInfoBillTo";
	private static XPathExpression billToPersonXpath;
	private static final String SHIP_TO_PERSON = "PersonInfoShipTo";
	private static XPathExpression shipToPersonXpath;	
	private static final String ORDERLINES_XPATH = "OrderLines/OrderLine";
	private static XPathExpression orderLinesXPath;
	private static final String RELATIVE_EXTN_XPATH = "Extn";
	private static XPathExpression relativeLinesExtnXPath;

	private static final String TOTAL_XPATH = "OverallTotals";
	private static XPathExpression totalXPath;
	
	private static boolean isInit = false;
	
	@Override
	public OrderStatusPayload transform(Document xmlPayloadDocument, OrderStatusEvent.OrderStatusType orderStatusType) throws DigitalIntegrationException {
		BackorderShippingPayload payload = new BackorderShippingPayload();

		configure();

		populateOmniOrderUpdate(payload, xmlPayloadDocument, orderStatusType.getReceivedType());
		
		Element order = (Element) getXPathNode(orderXPath, xmlPayloadDocument);
		
		populateGeneralFields(payload, order);
		
		Element envOrder = (Element) getXPathNode(envOrderXPath, xmlPayloadDocument);
		
		payload.setMaxOrderStatus(envOrder.getAttribute("MaxOrderStatus"));		
		payload.setCustomerEMailID(order.getAttribute("CustomerEMailID"));		
		payload.setOrderStatus(envOrder.getAttribute("MaxOrderStatus"));
		
		Element personInfoShipToElement = (Element) getXPathNode(shipToPersonXpath, order);
		payload.setPersonInfoShipTo(getPersonFromElement(personInfoShipToElement));
		
		Element personInfoBillToElement = (Element) getXPathNode(billToPersonXpath, order);
		payload.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));
		

		Element orderExtnElement = (Element) getXPathNode(relativeLinesExtnXPath, order);
		if (orderExtnElement != null) {
			payload.setLoyaltyId(orderExtnElement.getAttribute("ExtnLoyaltyId"));
			payload.setLoyaltyTier(orderExtnElement.getAttribute("ExtnLoyaltyTier"));
		}
		
		populatePayload(payload, order);
		
		populateOrderSummary(payload, xmlPayloadDocument);
		
		return payload;

	}

	protected void configure() throws DigitalIntegrationException {
		if (!isInit) {
			try {
				orderXPath = getXpathFactory().newXPath().compile(ORDER_XPATH);
				envOrderXPath = getXpathFactory().newXPath().compile(ENV_ORDER_XPATH);
				
				billToPersonXpath = getXpathFactory().newXPath().compile(BILL_TO_PERSON);
				shipToPersonXpath = getXpathFactory().newXPath().compile(SHIP_TO_PERSON);
				orderLinesXPath = getXpathFactory().newXPath().compile(ORDERLINES_XPATH);
				relativeLinesExtnXPath = getXpathFactory().newXPath().compile(RELATIVE_EXTN_XPATH);	
				totalXPath = getXpathFactory().newXPath().compile(TOTAL_XPATH);
				
				isInit = true;
			} catch (XPathExpressionException e) {
				throw new DigitalIntegrationException("Not able to generate xpath expressions", ErrorCodes.XFORM.getCode(),
						"TRANSFORM", e);
			}
		}
	}

}
