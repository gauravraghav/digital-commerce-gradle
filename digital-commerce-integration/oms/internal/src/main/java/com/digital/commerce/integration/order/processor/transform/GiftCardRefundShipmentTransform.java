package com.digital.commerce.integration.order.processor.transform;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.ShipmentLine;
import com.digital.commerce.integration.order.processor.domain.payload.GiftCardRefundShipmentPayload;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

public class GiftCardRefundShipmentTransform extends OrderStatusTransform {
	private static final String PREFIX = "/Notification/Payload";
	private static final String ENV_ORDER_XPATH = PREFIX+"/MergedDocument/EnvironmentDocument/Order";
	private static XPathExpression envOrderXpath;
	private static final String IN_ORDER_XPATH = PREFIX+"/MergedDocument/InputDocument/Order";
	private static XPathExpression inOrderXpath;
	private static final String BILL_TO_PERSON_XPATH = "PersonInfoBillTo";
	private static XPathExpression billToPersonXpath;
	private static final String SHIP_TO_PERSON_XPATH = "PersonInfoShipTo";
	private static XPathExpression shipToPersonXpath;
	private static final String SHIPMENT_XPATH = "Shipments/Shipment";
	private static XPathExpression shipmentXPath;
	private static final String CONTAINER_XPATH = "Containers/Container";
	private static XPathExpression containerXPath;
	private static final String SHIPMENT_LINE_XPATH = "ShipmentLines/ShipmentLine";
	private static XPathExpression shipmentLineXPath;
	private static final String SHIPMENT_ORDER_LINE_XPATH = "OrderLine";
	private static XPathExpression shipmentOrderLineXPath;	
	private static final String EXTN_XPATH = "Extn";
	private static XPathExpression extnXPath;
	
	private static final String TOTAL_XPATH = "OverallTotals";
	private static XPathExpression totalXPath;
	
	private static boolean isInit = false;

	@Override
	public OrderStatusPayload transform(Document xmlPayloadDocument, OrderStatusEvent.OrderStatusType orderStatusType) throws DigitalIntegrationException {
		GiftCardRefundShipmentPayload payload = new GiftCardRefundShipmentPayload();

		configure();

		populateOmniOrderUpdate(payload, xmlPayloadDocument, orderStatusType.getReceivedType());
		
		
		
		Element envOrder = (Element) getXPathNode(envOrderXpath, xmlPayloadDocument);
		
		populateGeneralFields(payload, envOrder);
		
		payload.setCustomerFirstName(envOrder.getAttribute("CustomerFirstName"));
		payload.setCustomerLastName(envOrder.getAttribute("CustomerLastName"));
		
		Element inOrder = (Element) getXPathNode(inOrderXpath, xmlPayloadDocument);
		Element shipment = (Element) getXPathNode(shipmentXPath, envOrder);
		Element container = (Element) getXPathNode(containerXPath, shipment);

		Element extnLoyalty = (Element) getXPathNode(extnXPath, envOrder);
		if(extnLoyalty != null){
			payload.setLoyaltyId(extnLoyalty.getAttribute("ExtnLoyaltyId"));
			payload.setLoyaltyTier(extnLoyalty.getAttribute("ExtnLoyaltyTier"));
		}

		ShipmentLine shipmentLine;
		NodeList shipmentLineNodes = getXPathNodeList(shipmentLineXPath, shipment);
		for (int i = 0; i < shipmentLineNodes.getLength(); i++) {
			Element shipmentLineElement = (Element) shipmentLineNodes.item(i);

			shipmentLine = new ShipmentLine();
			shipmentLine.setItemId(shipmentLineElement.getAttribute("ItemID"));
			shipmentLine.setOrderLineKey(shipmentLineElement.getAttribute("OrderLineKey"));
			shipmentLine.setQuantity(shipmentLineElement.getAttribute("Quantity"));
			shipmentLine.setShipmentLineNo(shipmentLineElement.getAttribute("ShipmentLineNo"));

			NodeList orderLineNodes = getXPathNodeList(shipmentOrderLineXPath, shipmentLineElement);
			shipmentLine.setOrderLines(getOrderLinesFromNodeList(orderLineNodes));
			
			payload.getShipmentLines().add(shipmentLine);
		}
		
		if(container != null){
			payload.setTrackingNo(container.getAttribute("TrackingNo"));
		}
		
		
		String dateInString = shipment.getAttribute("ActualShipmentDate");
		try {
			if(DigitalStringUtil.isNotEmpty(dateInString)){
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");		        
		        Date date = formatter.parse(dateInString.replaceAll("([\\+\\-]\\d\\d):(\\d\\d)","$1$2"));
		        formatter.format(date);
				payload.setActualShipmentDate((new SimpleDateFormat( "MM/dd/yyyy").format(date)));
			}
		} catch (ParseException e) {
			payload.setActualShipmentDate(dateInString);
		}

		String carrierName = shipment.getAttribute("SCAC");
		if(DigitalStringUtil.isNotBlank(carrierName)) {
			payload.setScac(carrierName.trim());
		}
		
		payload.setOrderNo(inOrder.getAttribute("OrderNo"));
		
		payload.setEmailId(envOrder.getAttribute("CustomerEMailID"));
		
		Element personInfoShipToElement = (Element) getXPathNode(shipToPersonXpath, envOrder);
		payload.setPersonInfoShipTo(getPersonFromElement(personInfoShipToElement));
		
		Element personInfoBillToElement = (Element) getXPathNode(billToPersonXpath, envOrder);	
		payload.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));
		
		Element totalElement = (Element) getXPathNode(totalXPath, envOrder);
		payload.setTotals(getTotalsFromElement(totalElement));
		
		this.populateOrderSummary(payload, xmlPayloadDocument);
		
		populatePayload(payload, envOrder);
		
		populateRefundTotals(payload, inOrder, envOrder);

		return payload;
	}

	protected void configure() throws DigitalIntegrationException {
		if (!isInit) {
			try {
				envOrderXpath = getXpathFactory().newXPath().compile(ENV_ORDER_XPATH);
				inOrderXpath = getXpathFactory().newXPath().compile(IN_ORDER_XPATH);
				billToPersonXpath = getXpathFactory().newXPath().compile(BILL_TO_PERSON_XPATH);
				shipToPersonXpath = getXpathFactory().newXPath().compile(SHIP_TO_PERSON_XPATH);
				shipmentXPath = getXpathFactory().newXPath().compile(SHIPMENT_XPATH);
				containerXPath = getXpathFactory().newXPath().compile(CONTAINER_XPATH);				
				shipmentLineXPath = getXpathFactory().newXPath().compile(SHIPMENT_LINE_XPATH);
				shipmentOrderLineXPath = getXpathFactory().newXPath().compile(SHIPMENT_ORDER_LINE_XPATH);
				extnXPath = getXpathFactory().newXPath().compile(EXTN_XPATH);
				
				totalXPath = getXpathFactory().newXPath().compile(TOTAL_XPATH);
				
				isInit = true;
			} catch (XPathExpressionException e) {
				throw new DigitalIntegrationException("Not able to generate xpath expressions", ErrorCodes.XFORM.getCode(),
						"TRANSFORM", e);
			}
		}
	}

}
