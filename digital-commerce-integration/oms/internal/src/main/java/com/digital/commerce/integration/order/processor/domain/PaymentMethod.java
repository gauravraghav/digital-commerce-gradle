package com.digital.commerce.integration.order.processor.domain;

public class PaymentMethod {

	private String paymentMethodType;
	private String paymentReference1;
	private String paymentReference2;
	private String creditCardType;
	private String displayCreditCardNo;
	private String displaySvcNo;
	private String totalAuthorized;
	private String orderHeaderKey;
	private String billToKey;
	private String totalRefundedAmount;
	private String totalCharged;
	private String totalAltRefundedAmount;
	private String requestedRefundAmount;
	private String requestedChargeAmount;
	private String requestedAuthAmount;
	private String creditCardExpDate;
	private String checkNo;
	private String paymentTypeGroup;
	
	public String getPaymentMethodType() {
		return paymentMethodType;
	}

	public void setPaymentMethodType(String paymentMethodType) {
		this.paymentMethodType = paymentMethodType;
	}

	public String getPaymentReference1() {
		return paymentReference1;
	}

	public void setPaymentReference1(String paymentReference1) {
		this.paymentReference1 = paymentReference1;
	}

	public String getPaymentReference2() {
		return paymentReference2;
	}

	public void setPaymentReference2(String paymentReference2) {
		this.paymentReference2 = paymentReference2;
	}

	public String getCreditCardType() {
		return creditCardType;
	}

	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	public String getDisplayCreditCardNo() {
		return displayCreditCardNo;
	}

	public void setDisplayCreditCardNo(String displayCreditCardNo) {
		this.displayCreditCardNo = displayCreditCardNo;
	}

	public String getDisplaySvcNo() {
		return displaySvcNo;
	}

	public void setDisplaySvcNo(String displaySvcNo) {
		this.displaySvcNo = displaySvcNo;
	}

	public String getTotalAuthorized() {
		return totalAuthorized;
	}

	public void setTotalAuthorized(String totalAuthorized) {
		this.totalAuthorized = totalAuthorized;
	}

	public String getOrderHeaderKey() {
		return orderHeaderKey;
	}

	public void setOrderHeaderKey(String orderHeaderKey) {
		this.orderHeaderKey = orderHeaderKey;
	}

	public String getBillToKey() {
		return billToKey;
	}

	public void setBillToKey(String billToKey) {
		this.billToKey = billToKey;
	}

	public String getTotalRefundedAmount() {
		return totalRefundedAmount;
	}

	public void setTotalRefundedAmount(String totalRefundedAmount) {
		this.totalRefundedAmount = totalRefundedAmount;
	}

	public String getTotalCharged() {
		return totalCharged;
	}

	public void setTotalCharged(String totalCharged) {
		this.totalCharged = totalCharged;
	}

	public String getTotalAltRefundedAmount() {
		return totalAltRefundedAmount;
	}

	public void setTotalAltRefundedAmount(String totalAltRefundedAmount) {
		this.totalAltRefundedAmount = totalAltRefundedAmount;
	}

	public String getRequestedRefundAmount() {
		return requestedRefundAmount;
	}

	public void setRequestedRefundAmount(String requestedRefundAmount) {
		this.requestedRefundAmount = requestedRefundAmount;
	}

	public String getRequestedChargeAmount() {
		return requestedChargeAmount;
	}

	public void setRequestedChargeAmount(String requestedChargeAmount) {
		this.requestedChargeAmount = requestedChargeAmount;
	}

	public String getRequestedAuthAmount() {
		return requestedAuthAmount;
	}

	public void setRequestedAuthAmount(String requestedAuthAmount) {
		this.requestedAuthAmount = requestedAuthAmount;
	}

	public String getCreditCardExpDate() {
		return creditCardExpDate;
	}

	public void setCreditCardExpDate(String creditCardExpDate) {
		this.creditCardExpDate = creditCardExpDate;
	}

	public String getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(String checkNo) {
		this.checkNo = checkNo;
	}

	public String getPaymentTypeGroup() {
		return paymentTypeGroup;
	}

	public void setPaymentTypeGroup(String paymentTypeGroup) {
		this.paymentTypeGroup = paymentTypeGroup;
	}
	
	
}
