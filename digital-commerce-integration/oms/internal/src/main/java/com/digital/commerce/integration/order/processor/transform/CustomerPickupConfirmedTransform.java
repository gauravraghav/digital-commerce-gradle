package com.digital.commerce.integration.order.processor.transform;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.ShipmentLine;
import com.digital.commerce.integration.order.processor.domain.payload.CustomerPickupConfirmedPayload;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

public class CustomerPickupConfirmedTransform extends OrderStatusTransform {
	
	private static final String ORDER_XPATH = OrderStatusTransform.PREFIX+"/Order";
	private static XPathExpression orderXpath;
	private static final String BILL_TO_PERSON = "PersonInfoBillTo";
	private static XPathExpression billToPersonXpath;
	private static final String SHIP_TO_PERSON = "PersonInfoShipTo";
	private static XPathExpression shipToPersonXpath;
	private static final String ORDERLINES_XPATH = "OrderLines/OrderLine";
	private static XPathExpression orderLinesXPath;
	private static final String TOTAL_XPATH = "OverallTotals";
	private static XPathExpression totalXPath;
	private static final String EXTN_XPATH = "Extn";
	private static XPathExpression extnXpath;
	private static final String ORDER_SUMMARY_XPATH = "/Notification/OrderSummary/Order";
	private static XPathExpression orderSummaryXPath;
	private static final String ORDER_SUMMARY_SHIPMENT_XPATH = "/Notification/OrderSummary/Order/Shipments/Shipment";
	private static XPathExpression orderSummaryShipmentXPath;
	private static final String ORDER_SUMMARY_SHIPMENT_LINE_XPATH = "ShipmentLines/ShipmentLine";
	private static XPathExpression orderSummaryShipmentLineXPath;
	private static final String ORDER_SUMMARY_SHIPMENT_CONTAINER_XPATH = "Containers/Container";
	private static XPathExpression orderSummaryContainerXPath;
	private static final String ORDER_SUMMARY_SHIPMENT_ORDER_LINE_XPATH = "OrderLine";
	private static XPathExpression orderSummaryShipmentOrderLineXPath;
	private static boolean isInit = false;
	
	@Override
	public OrderStatusPayload transform(Document xmlPayloadDocument, OrderStatusEvent.OrderStatusType orderStatusType) throws DigitalIntegrationException {
		CustomerPickupConfirmedPayload payload = new CustomerPickupConfirmedPayload();

		configure();

		Element order = (Element) getXPathNode(orderXpath, xmlPayloadDocument);
			
		populateOmniOrderUpdate(payload, xmlPayloadDocument, orderStatusType.getReceivedType());
		
		populateGeneralFields(payload, order);
		
		Element orderLine = (Element) getXPathNode(orderLinesXPath, order);

		payload.setCustomerEmailId(order.getAttribute("CustomerEMailID"));
		payload.setCustomerFirstName(order.getAttribute("CustomerFirstName"));
		payload.setCustomerLastName(order.getAttribute("CustomerLastName"));

		Element orderSummary = (Element) getXPathNode(orderSummaryXPath, xmlPayloadDocument);

		populateGeneralFields(payload, orderSummary);
		
		Element orderExtnElement = (Element) getXPathNode(extnXpath, order);
		if (orderExtnElement != null) {
			payload.setLoyaltyId(orderExtnElement.getAttribute("ExtnLoyaltyId"));
			payload.setLoyaltyTier(orderExtnElement.getAttribute("ExtnLoyaltyTier"));
		}	
		
		Element shipment = (Element) getXPathNode(orderSummaryShipmentXPath, xmlPayloadDocument);
		Element container = (Element) getXPathNode(orderSummaryContainerXPath, shipment);

		ShipmentLine shipmentLine;
		NodeList shipmentLineNodes = getXPathNodeList(orderSummaryShipmentLineXPath, shipment);
		for (int i = 0; i < shipmentLineNodes.getLength(); i++) {
			Element shipmentLineElement = (Element) shipmentLineNodes.item(i);

			shipmentLine = new ShipmentLine();
			shipmentLine.setItemId(shipmentLineElement.getAttribute("ItemID"));
			shipmentLine.setOrderLineKey(shipmentLineElement.getAttribute("OrderLineKey"));
			shipmentLine.setQuantity(shipmentLineElement.getAttribute("Quantity"));
			shipmentLine.setShipmentLineNo(shipmentLineElement.getAttribute("ShipmentLineNo"));

			NodeList orderLineNodes = getXPathNodeList(orderSummaryShipmentLineXPath, shipmentLineElement);
			shipmentLine.setOrderLines(getOrderLinesFromNodeList(orderLineNodes));

			if(container != null){
				shipmentLine.setTrackingNo(container.getAttribute("TrackingNo"));
			}

			payload.getShipmentLines().add(shipmentLine);
		}

		NodeList summaryOrderLinesNodes = getXPathNodeList(orderSummaryShipmentOrderLineXPath, orderSummary);
		payload.setSummaryOrderLines(getOrderLinesFromNodeList(summaryOrderLinesNodes));

		Element personInfoBillToElement = (Element) getXPathNode(billToPersonXpath, orderLine);
		payload.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));

		Element personInfoShipToElement = (Element) getXPathNode(shipToPersonXpath, orderLine);
		payload.setPersonInfoShipTo(getPersonFromElement(personInfoShipToElement));
		
		Element totalElement = (Element) getXPathNode(totalXPath, orderSummary);
		payload.setTotals(getTotalsFromElement(totalElement));		
		
		populatePayload(payload, order);
		
		this.populateOrderSummary(payload, xmlPayloadDocument);

		
		return payload;
	}

	protected void configure() throws DigitalIntegrationException {
		if (!isInit) {
			try {
				orderXpath = getXpathFactory().newXPath().compile(ORDER_XPATH);
				billToPersonXpath = getXpathFactory().newXPath().compile(BILL_TO_PERSON);
				shipToPersonXpath = getXpathFactory().newXPath().compile(SHIP_TO_PERSON);
				orderLinesXPath = getXpathFactory().newXPath().compile(ORDERLINES_XPATH);
				extnXpath = getXpathFactory().newXPath().compile(EXTN_XPATH);
				totalXPath = getXpathFactory().newXPath().compile(TOTAL_XPATH);
				orderSummaryXPath = getXpathFactory().newXPath().compile(ORDER_SUMMARY_XPATH);
				orderSummaryShipmentXPath = getXpathFactory().newXPath().compile(ORDER_SUMMARY_SHIPMENT_XPATH);
				orderSummaryShipmentLineXPath = getXpathFactory().newXPath().compile(ORDER_SUMMARY_SHIPMENT_LINE_XPATH);
				orderSummaryShipmentOrderLineXPath = getXpathFactory().newXPath().compile(ORDER_SUMMARY_SHIPMENT_ORDER_LINE_XPATH);
				orderSummaryContainerXPath = getXpathFactory().newXPath().compile(ORDER_SUMMARY_SHIPMENT_CONTAINER_XPATH);
				isInit = true;
			} catch (XPathExpressionException e) {
				throw new DigitalIntegrationException("Not able to generate xpath expressions", ErrorCodes.XFORM.getCode(),
						"TRANSFORM", e);
			}
		}
	}
}
