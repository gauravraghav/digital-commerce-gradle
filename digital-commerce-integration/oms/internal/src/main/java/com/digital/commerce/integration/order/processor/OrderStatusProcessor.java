package com.digital.commerce.integration.order.processor;

import atg.nucleus.ServiceMap;

import com.digital.commerce.integration.order.listener.LoggingOrderStatusListener;
import com.digital.commerce.integration.order.processor.OrderStatusEvent.OrderStatusType;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.order.processor.transform.OrderStatusTransform;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.util.JAXBContextCache;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.Map;
import java.util.Vector;

/**
 *
 * @startuml
 *
 * class OrderStatusProcessor {
 * __ final fields __
 * 	 {static} -DigitalLogger logger
 * __ fields __
 * 	 {static} -DocumentBuilderFactory factory
 * 	 {static} -JAXBContextCache jaxbContextCache
 *   {static} -XPathExpression notificationTypeXPath
 *   {static} -String NOTIFICATION_TYPE_XPATH_EXPRESSION
 * 	 -Vector<OrderStatusListener> listeners
 * 	 -ServiceMap orderStatusTransforms
 * 	 -Map<String, String> orderStatusTypes
 * .. Simple Getter methods ..
 *   +getJaxbContextCache()
 *   +getOrderStatusTransforms()
 *   +getOrderStatusTypes()
 * .. Some Setter methods ..
 *   +setJaxbContextCache(JAXBContextCache jaxbContextCache)
 *   +setOrderStatusTransforms(ServiceMap orderStatusTransforms)
 *   +setOrderStatusTypes(Map<String, String> orderStatusTypes)
 * .. methods ..
 *   #void processOrderStatus(String orderStatusPayload) throws DigitalIntegrationException
 *   -OrderStatusEvent getUnmarshalledObjectFromXMLPayload(String xmlPayload) throws DigitalIntegrationException
 *   -OrderStatusPayload generateOrderStatusPayload(OrderStatusType orderStatusType, Document xmlPayloadDocument) throws DigitalIntegrationException
 *   -OrderStatusEvent.OrderStatusType getOrderStatusType(Document xmlPayloadDocument)throws DigitalIntegrationException
 * }
 *
 * note top of OrderStatusProcessor
 *      Allowed <b>orderStatusTypes</b> are defined in OrderStatusProcessor properties file.
 *      Any new event type should be added to the list in order to process further
 * end note
 *
 * @enduml
 */
public class OrderStatusProcessor {

    private static final transient DigitalLogger logger = DigitalLogger.getLogger(OrderStatusProcessor.class);

    private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private JAXBContextCache jaxbContextCache;
    private Vector<OrderStatusListener> listeners = new Vector<>();
    private ServiceMap orderStatusTransforms;
    private static XPathExpression notificationTypeXPath;
    private Map<String, String> orderStatusTypes;

    private static final String NOTIFICATION_TYPE_XPATH_EXPRESSION = "/Notification/@Type";

    /**
     * Default constructor
     */
    public OrderStatusProcessor() {

    }

    /**
     * @param orderStatusPayload
     * @throws DigitalIntegrationException
     */
    public void processOrderStatus(String orderStatusPayload) throws DigitalIntegrationException {
        /*
         * 1. take payload and convert to pojo via jaxb
         */
        OrderStatusEvent orderStatusEvent = getUnmarshalledObjectFromXMLPayload(orderStatusPayload);

        if (null == orderStatusEvent) {

            if (logger.isDebugEnabled()) {
                logger.debug(" UNKNOWN OrderStatusEvent received : " + orderStatusPayload);
            }

            logger.error("No further processing as UNKNOWN OrderStatusEvent received");

            return;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Preparing to process OrderStatusEvent of: " + orderStatusEvent.getStatusType().getReceivedType());
        }

        boolean isCandidateListenerFound = false;

        /*
         * 5. Find proper listener to handle event
         */
        Enumeration<OrderStatusListener> e = listeners.elements();
        while (e.hasMoreElements()) {
            OrderStatusListener candidateListener = e.nextElement();
            if (candidateListener.doesHandleOrderStatusEvent(orderStatusEvent)) {

                if (logger.isDebugEnabled()) {
                    logger.debug(String.format("Found candidate listener for OrderStatusEvent of: %s - %s",
                            orderStatusEvent.getStatusType().getReceivedType(), candidateListener.getListenerName()));
                }

                candidateListener.handleOrderStatusEvent(orderStatusEvent);

                if(!(candidateListener instanceof LoggingOrderStatusListener)) {
                    isCandidateListenerFound = true;
                }

            }
        }

        if(!isCandidateListenerFound){
            logger.error("No further processing as no candidate listener found for OrderStatusEvent = "
                    +  orderStatusEvent.getStatusType().getReceivedType());
        }

    }

    /**
     * @param xmlPayload
     * @return OrderStatusEvent
     * @throws DigitalIntegrationException
     */
    private OrderStatusEvent getUnmarshalledObjectFromXMLPayload(String xmlPayload) throws DigitalIntegrationException {
        try {
            /*
             * 2. Get the order status type
             */
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xmlPayloadDocument = builder.parse(new InputSource(new StringReader(xmlPayload)));

            /*
             * 3. Pull out required fields and create correct StatusOrderPayload
             */
            OrderStatusType orderStatusType = getOrderStatusType(xmlPayloadDocument);

            // Check if message has UNKNOWN event type
            if (null == orderStatusType) {
                return null;
            }

            OrderStatusPayload payloadObject = generateOrderStatusPayload(orderStatusType, xmlPayloadDocument);

            /*
             * 4. Create OrderStatusEvent
             */
            OrderStatusEvent orderStatusEvent = new OrderStatusEvent(orderStatusType);
            orderStatusEvent.setPayload(payloadObject);

            return orderStatusEvent;

        } catch (Exception e) {
            logger.error("UnmarshalledObjectFromHttpResponse Error", e);

            throw new DigitalIntegrationSystemException("UnmarshalledObjectFromHttpResponse Error",
                    ErrorCodes.XFORM.getCode(), e);
        }
    }

    /**
     * @param orderStatusType
     * @param xmlPayloadDocument
     * @return OrderStatusPayload
     * @throws DigitalIntegrationException
     */
    private OrderStatusPayload generateOrderStatusPayload(OrderStatusType orderStatusType, Document xmlPayloadDocument) throws DigitalIntegrationException {
        OrderStatusTransform thisTransform = null;

        if (orderStatusType != null && orderStatusTransforms != null) {
            thisTransform = (OrderStatusTransform) orderStatusTransforms.get(orderStatusType.getTemplateKey());
        }

        if (orderStatusType == null || thisTransform == null) {
            throw new DigitalIntegrationException(String.format("Transform for payload type does not exist: %s ",
                    orderStatusType), ErrorCodes.XFORM.getCode(), "TRANSFORMNOTFOUND");
        }

        return thisTransform.transform(xmlPayloadDocument, orderStatusType);

    }

    /**
     * @param xmlPayloadDocument
     * @return OrderStatusEvent.OrderStatusType
     * @throws DigitalIntegrationException
     */
    private OrderStatusEvent.OrderStatusType getOrderStatusType(Document xmlPayloadDocument)
            throws DigitalIntegrationException {

        if (notificationTypeXPath == null) {
            try {
                notificationTypeXPath = XPathFactory.newInstance().newXPath()
                        .compile(NOTIFICATION_TYPE_XPATH_EXPRESSION);
            } catch (XPathExpressionException e) {
                throw new DigitalIntegrationException("Not able to find notification type", ErrorCodes.XFORM.getCode(),
                        "NONOTIFICATIONTYPE");
            }
        }

        try {
            String receivedType = (String) notificationTypeXPath.evaluate(xmlPayloadDocument, XPathConstants.STRING);
            if (orderStatusTypes.containsKey(receivedType)) {
                return new OrderStatusType(receivedType, orderStatusTypes.get(receivedType));
            }
        } catch (XPathExpressionException e) {
            throw new DigitalIntegrationException("Not able to find a valid notification type", ErrorCodes.XFORM.getCode(),
                    "NOVALIDNOTIFICATIONTYPE");
        }
        return null;
    }

    /**
     * @return JAXBContextCache
     */
    public JAXBContextCache getJaxbContextCache() {
        return jaxbContextCache;
    }

    /**
     * @param jaxbContextCache
     */
    public void setJaxbContextCache(JAXBContextCache jaxbContextCache) {
        this.jaxbContextCache = jaxbContextCache;
    }

    /**
     * @return ServiceMap
     */
    public ServiceMap getOrderStatusTransforms() {
        return orderStatusTransforms;
    }

    /**
     * @param orderStatusTransforms
     */
    public void setOrderStatusTransforms(ServiceMap orderStatusTransforms) {
        this.orderStatusTransforms = orderStatusTransforms;
    }

    /**
     * @return Map
     */
    public Map<String, String> getOrderStatusTypes() {
        return orderStatusTypes;
    }

    /**
     * @param orderStatusTypes
     */
    public void setOrderStatusTypes(Map<String, String> orderStatusTypes) {
        this.orderStatusTypes = orderStatusTypes;
    }

    /**
     *
     * @param listener
     */
    public synchronized void addOrderStatusListener(OrderStatusListener listener) {
        listeners.addElement(listener);
    }

    /**
     *
     * @param listener
     */
    public synchronized void removeOrderStatusListener(OrderStatusListener listener) {
        listeners.removeElement(listener);
    }

}
