package com.digital.commerce.integration.order.processor.domain.payload;

import java.util.ArrayList;
import java.util.List;

import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.PaymentMethod;
import com.digital.commerce.integration.order.processor.domain.Person;

public class ShipmentSLAChangePayload extends OrderStatusPayload {
	private String isOrderWhiteGlove;
	private String loyaltyId;
	private String loyaltyTier;
	private List<OrderLine> orderLines = new ArrayList<>();
	private List<OrderLine> changedOrderLines = new ArrayList<>();
	private Person personInfoBillTo;
	private Person personInfoShipTo;
	private Person personInfoMarkFor;
	private String carrierServiceCode;
	private String customerEmailId;
	private String salesOrderNo;
	private String shipNode;
	private List<PaymentMethod> paymentMethods = new ArrayList<>();
	private String isChangeWholeOrder;
	private String customerFirstName;
	private String customerLastName;
	private String customerEMailID;
		
	public String getCustomerEMailID() {
		return customerEMailID;
	}
	public void setCustomerEMailID(String customerEMailID) {
		this.customerEMailID = customerEMailID;
	}
	public String getCustomerLastName() {
		return customerLastName;
	}
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}
	public Person getPersonInfoMarkFor() {
		return personInfoMarkFor;
	}
	public void setPersonInfoMarkFor(Person personInfoMarkFor) {
		this.personInfoMarkFor = personInfoMarkFor;
	}
	public String getCustomerFirstName() {
		return customerFirstName;
	}
	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}
	public String getIsOrderWhiteGlove() {
		return isOrderWhiteGlove;
	}
	public void setIsOrderWhiteGlove(String isOrderWhiteGlove) {
		this.isOrderWhiteGlove = isOrderWhiteGlove;
	}
	public String getLoyaltyId() {
		return loyaltyId;
	}
	public void setLoyaltyId(String loyaltyId) {
		this.loyaltyId = loyaltyId;
	}
	public String getLoyaltyTier() {
		return loyaltyTier;
	}
	public void setLoyaltyTier(String loyaltyTier) {
		this.loyaltyTier = loyaltyTier;
	}
	public List<OrderLine> getOrderLines() {
		return orderLines;
	}
	public void setOrderLines(List<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}
	public Person getPersonInfoBillTo() {
		return personInfoBillTo;
	}
	public void setPersonInfoBillTo(Person personInfoBillTo) {
		this.personInfoBillTo = personInfoBillTo;
	}
	public Person getPersonInfoShipTo() {
		return personInfoShipTo;
	}
	public void setPersonInfoShipTo(Person personInfoShipTo) {
		this.personInfoShipTo = personInfoShipTo;
	}
	public String getCarrierServiceCode() {
		return carrierServiceCode;
	}
	public void setCarrierServiceCode(String carrierServiceCode) {
		this.carrierServiceCode = carrierServiceCode;
	}
	public String getCustomerEmailId() {
		return customerEmailId;
	}
	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}
	
	public String getSalesOrderNo() {
		return salesOrderNo;
	}
	public void setSalesOrderNo(String salesOrderNo) {
		this.salesOrderNo = salesOrderNo;
	}
	public String getShipNode() {
		return shipNode;
	}
	public void setShipNode(String shipNode) {
		this.shipNode = shipNode;
	}
	public List<PaymentMethod> getPaymentMethods() {
		return paymentMethods;
	}
	public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}
	public List<OrderLine> getChangedOrderLines() {
		return changedOrderLines;
	}
	public void setChangedOrderLines(List<OrderLine> changedOrderLines) {
		this.changedOrderLines = changedOrderLines;
	}
	public String getIsChangeWholeOrder() {
		return isChangeWholeOrder;
	}
	public void setIsChangeWholeOrder(String isChangeWholeOrder) {
		this.isChangeWholeOrder = isChangeWholeOrder;
	}
	
	
}
