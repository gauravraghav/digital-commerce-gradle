package com.digital.commerce.integration.order.processor.domain.payload;

import java.util.ArrayList;
import java.util.List;

import com.digital.commerce.integration.order.processor.domain.Award;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.OrderTotals;
import com.digital.commerce.integration.order.processor.domain.Person;
import com.digital.commerce.integration.order.processor.domain.ShipmentLine;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.yantra.webservices.beans.order.omni.OmniOrderUpdate;

public abstract class OrderStatusPayload {

	private String eventType;
	private String siteId;
	private String locale;
	private String documentType;
	private String entryType;
	private String orderComplete;
	private String orderHeaderKey;
	private String orderNo;
	private String orderDate;
	private String maxOrderStatus;
	private boolean isFullOrderCancel = false;
	private OmniOrderUpdate omniOrderUpdate;
	private OrderTotals totals;
	private List<Award> awards = new ArrayList<>();
	private List<OrderLine> summaryOrderLines = new ArrayList<>();
	private List<ShipmentLine> shipmentLines = new ArrayList<>();
	private List<OrderLine> orderLines = new ArrayList<>();
	private List<ShipmentLine> payloadShipmentLines = new ArrayList<>();
	private String loyaltyId;
	private String loyaltyTier;
	private Person personInfoMarkFor;
	
	public Person getPersonInfoMarkFor() {
		return personInfoMarkFor;
	}

	public void setPersonInfoMarkFor(Person personInfoMarkFor) {
		this.personInfoMarkFor = personInfoMarkFor;
	}

	public OrderTotals getTotals() {
		return totals;
	}

	public void setTotals(OrderTotals totals) {
		this.totals = totals;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getEntryType() {
		return entryType;
	}

	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}

	public String getOrderComplete() {
		return orderComplete;
	}

	public void setOrderComplete(String orderComplete) {
		this.orderComplete = orderComplete;
	}

	public String getOrderHeaderKey() {
		return orderHeaderKey;
	}

	public void setOrderHeaderKey(String orderHeaderKey) {
		this.orderHeaderKey = orderHeaderKey;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getMaxOrderStatus() {
		return maxOrderStatus;
	}

	public void setMaxOrderStatus(String maxOrderStatus) {
		this.maxOrderStatus = maxOrderStatus;

		/*
		 * 9000 is full order cancel, 1300 is full order backorder
		 */
		if (DigitalStringUtil.equals(maxOrderStatus, "9000") || DigitalStringUtil.equals(maxOrderStatus, "1300")) {
			setFullOrderCancel(true);
		}
	}

	public boolean isFullOrderCancel() {
		return isFullOrderCancel;
	}

	public void setFullOrderCancel(boolean isFullOrderCancel) {
		this.isFullOrderCancel = isFullOrderCancel;
	}

	public OmniOrderUpdate getOmniOrderUpdate() {
		return omniOrderUpdate;
	}

	public void setOmniOrderUpdate(OmniOrderUpdate omniOrderUpdate) {
		this.omniOrderUpdate = omniOrderUpdate;
	}

	public List<Award> getAwards() {
		return awards;
	}

	public void setAwards(List<Award> awards) {
		this.awards = awards;
	}

	public List<OrderLine> getSummaryOrderLines() {
		return summaryOrderLines;
	}

	public void setSummaryOrderLines(List<OrderLine> summaryOrderLines) {
		this.summaryOrderLines = summaryOrderLines;
	}

	public List<ShipmentLine> getShipmentLines() {
		return shipmentLines;
	}

	public void setShipmentLines(List<ShipmentLine> shipmentLines) {
		this.shipmentLines = shipmentLines;
	}

	public String getLoyaltyId() {
		return loyaltyId;
	}

	public void setLoyaltyId(String loyaltyId) {
		this.loyaltyId = loyaltyId;
	}

	public String getLoyaltyTier() {
		return loyaltyTier;
	}

	public void setLoyaltyTier(String loyaltyTier) {
		this.loyaltyTier = loyaltyTier;
	}

	public List<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<OrderLine> payloadOrderLines) {
		this.orderLines = payloadOrderLines;
	}

	public List<ShipmentLine> getPayloadShipmentLines() {
		return payloadShipmentLines;
	}

	public void setPayloadShipmentLines(List<ShipmentLine> payloadShipmentLines) {
		this.payloadShipmentLines = payloadShipmentLines;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
}
