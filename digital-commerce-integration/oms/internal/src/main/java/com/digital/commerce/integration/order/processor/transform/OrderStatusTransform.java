package com.digital.commerce.integration.order.processor.transform;

import static com.digital.commerce.common.util.DigitalDateUtil.YANTRA_ORDER_STATUS_DATE_TIMEZONE_FORMAT;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang.SerializationUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.Award;
import com.digital.commerce.integration.order.processor.domain.Container;
import com.digital.commerce.integration.order.processor.domain.FromOrderReleaseStatus;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.OrderLineReservation;
import com.digital.commerce.integration.order.processor.domain.OrderStatus;
import com.digital.commerce.integration.order.processor.domain.OrderTotals;
import com.digital.commerce.integration.order.processor.domain.PaymentMethod;
import com.digital.commerce.integration.order.processor.domain.Person;
import com.digital.commerce.integration.order.processor.domain.ShipmentLine;
import com.digital.commerce.integration.order.processor.domain.ToOrderReleaseStatus;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.util.JAXBContextCache;
import com.yantra.webservices.beans.order.omni.OmniOrderUpdate;

/**
 * @startuml
 * abstract class OrderStatusTransform {
 *   {static} XPathFactory xpathFactory
 *   {abstract} +OrderStatusPayload transform(Document xmlPayloadDocument,
 *   				OrderStatusEvent.OrderStatusType orderStatusType) throws DigitalIntegrationException
 *   #OmniOrderUpdate getOmniNode(Document xmlPayloadDocument) throws DigitalIntegrationException
 *   #String getXPathString(XPathExpression xPathExpression, Node node) throws DigitalIntegrationException
 *   #NodeList getXPathNodeList(XPathExpression xPathExpression, Node node) throws DigitalIntegrationException
 *   #Node getXPathNode(XPathExpression xPathExpression, Node node) throws DigitalIntegrationException
 *   #OrderTotals getTotalsFromElement(Element element) throws DigitalIntegrationException
 *   #List<Award> getAwardsFromElement(NodeList nodes) throws DigitalIntegrationException
 *   #List<PaymentMethod> getPaymentMethodsFromNodeList(NodeList nodes) throws DigitalIntegrationException
 *   #List<OrderLine> getOrderLinesFromNodeList(NodeList nodes) throws DigitalIntegrationException
 *   #List<Container> getContainersFromShipment(NodeList elements) throws DigitalIntegrationException
 *   #String getATGMappedOrderLineStatus(String status)
 *   #boolean isValidStatusForNotificationType(OrderStatus orderStatus, String eventType)
 *   #List<OrderStatus> filterByPayloadOrderLineStatus(OrderLine orderLine, String eventType,
 * 			List<ShipmentLine> shipmentLines)
 * 	 #boolean ignore1400Status(List<OrderStatus> orderStatuses, int orderedQty)
 *   #void populateOmniOrderUpdate(OrderStatusPayload orderStatusPayload, Document xmlPayloadDocument, String receivedType) throws DigitalIntegrationException
 *   #String convertDoubleQtyToInt(String qty)
 *   #void populateRefundTotals(OrderStatusPayload payload, Element inOrder, Element order) throws DigitalIntegrationException
 *   +void populateOrderSummary(OrderStatusPayload payload, Document xmlPayloadDocument) throws DigitalIntegrationException
 *   +void populatePayload(OrderStatusPayload payload, Element order) throws DigitalIntegrationException
 *   +void populateGeneralFields(OrderStatusPayload payload, Element orderNode) throws DigitalIntegrationException
 *   -void configureInternal() throws DigitalIntegrationException
 * }
 *
 * note top of OrderStatusTransform
 *   In CS-14 event processing, <size:18>every</size> <u>transform class</u>
 *   <b>extends</b> <i>this</i> one and <b>implement</b> <i>transform</i> method.
 * end note
 *
 * OrderStatusTransform <|-- BackorderBopisTransform
 * OrderStatusTransform <|-- BackorderShippingTransform
 * OrderStatusTransform <|-- CustomerPickupConfirmedTransform
 * OrderStatusTransform <|-- GiftCardRefundShipmentTransform
 * OrderStatusTransform <|-- OrderCancelledTransform
 * OrderStatusTransform <|-- OrderConfirmedTransform
 * OrderStatusTransform <|-- OrderStatusTransform
 * OrderStatusTransform <|-- ReadyForPickupReminderTransform
 * OrderStatusTransform <|-- ReadyForPickupTransform
 * OrderStatusTransform <|-- ReturnReceivedShipmentTransform
 * OrderStatusTransform <|-- ShipmentConfirmedTransform
 * OrderStatusTransform <|-- ShipmentSLAChangeTransform
 * OrderStatusTransform <|-- ShippingForBostsTransform
 *
 * @enduml
 */
public abstract class OrderStatusTransform {

	private static XPathFactory xpathFactory = XPathFactory.newInstance();
	protected static final String PREFIX = "/Notification/Payload";
	private static final String OMNI = "/Notification/OmniOrderUpdate";
	private static XPathExpression omniXPath;
	private static boolean isInitInternal = false;
	private JAXBContextCache jaxbContextCache;

	private static final String TOTAL_XPATH = "OverallTotals";
	protected static XPathExpression totalXPath;

	private static final String STATUS_XPATH = "OrderStatuses/OrderStatus";
	private static XPathExpression statusXPath;
	private static final String STATUS_BREAKUP_XPATH = "StatusBreakupForBackOrderedQty/BackOrderedFrom";
	private static XPathExpression statusBreakupXPath;
	private static final String STATUS_BREAKUP_DETAILS_XPATH = "Details";
	private static XPathExpression statusBreakupDetailsXPath;
	private static final String STATUS_BREAKUP_CANCEL_XPATH = "StatusBreakupForCanceledQty/CanceledFrom";
	private static XPathExpression statusBreakupCancelXPath;

	private static final String STATUS_DETAILS_XPATH = "Details";
	private static XPathExpression statusDetailsXPath;
	private static final String ORDERLINES_ITEM_XPATH = "Item";
	private static XPathExpression orderLinesItemXPath;
	private static final String LINE_PRICE_INFO_XPATH = "LinePriceInfo";
	private static XPathExpression linePriceInfoXPath;
	private static final String EXTN_XPATH = "Extn";
	private static XPathExpression extnXPath;
	private static final String ORDERLINES_RESERVATION_XPATH = "OrderLineReservations/OrderLineReservation";
	private static XPathExpression orderLinesReservationXPath;
	private static final String ORDERLINES_TOTALS_XPATH = "LineOverallTotals";
	private static XPathExpression orderLinesTotalsXPath;
	private static final String TO_ORDER_STATUS_XPATH = "ToOrderReleaseStatuses/ToOrderReleaseStatus";
	private static XPathExpression toOrderStatusXPath;
	private static final String FROM_ORDER_STATUS_XPATH = "FromOrderReleaseStatuses/FromOrderReleaseStatus";
	private static XPathExpression fromOrderStatusXPath;
	private static final String FROM_ORDER_SCHEDULE_XPATH = "FromOrderLineSchedule";
	private static XPathExpression fromOrderScheduleXPath;
	private static final String TO_ORDER_SCHEDULE_XPATH = "ToOrderLineSchedule";
	private static XPathExpression toOrderScheduleXPath;

	private static final String BILL_TO_PERSON = "PersonInfoBillTo";
	private static XPathExpression billToPersonXpath;
	private static final String SHIP_TO_PERSON = "PersonInfoShipTo";
	private static XPathExpression shipToPersonXpath;

	private static final String PRIMARY_INFO_XPATH = "PrimaryInformation";
	private static XPathExpression primaryInfoXPath;
	private static final String ITEMDETAILS_XPATH = "ItemDetails";
	private static XPathExpression itemDetailsXPath;

	private static final String AWARD_XPATH = "Awards/Award";
	private static XPathExpression awardXPath;

	private static final String ORDER_SUMMARY_XPATH = "/Notification/OrderSummary/Order";
	private static XPathExpression orderSummaryXPath;
	private static final String ORDER_SUMMARY_SHIPMENT_XPATH = "/Notification/OrderSummary/Order/Shipments/Shipment";
	private static XPathExpression orderSummaryShipmentXPath;
	private static final String ORDER_SHIPMENT_LINE_XPATH = "ShipmentLines/ShipmentLine";
	private static XPathExpression orderShipmentLineXPath;
	private static final String ORDER_SHIPMENT_ORDER_LINE_XPATH = "OrderLine";
	private static XPathExpression orderShipmentOrderLineXPath;
	private static final String ORDER_ORDER_LINE_XPATH = "OrderLines/OrderLine";
	private static XPathExpression orderOrderLineXPath;
	private static final String ORDER_SHIPMENT_CONTAINER_XPATH = "Containers/Container";
	private static XPathExpression orderContainerXPath;
	private static final String ORDER_SHIPMENT_CONTAINER_DETAIL_XPATH = "ContainerDetails/ContainerDetail";
	private static XPathExpression orderContainerDetailXPath;
	private static final String ORDER_SHIPMENT_CONTAINER_SHIPMENTLINE_XPATH = "ShipmentLine";
	private static XPathExpression orderContainerShipmentLineXPath;
	
	private static final String ORDER_SHIPMENT_XPATH = "Shipments/Shipment";
	private static XPathExpression orderShipmentXPath;

	private static final String ORDERLINES_XPATH = "OrderLine";
	private static XPathExpression orderLinesXPath;

	private static final String MARK_FOR_PERSON = "PersonInfoMarkFor";
	private static XPathExpression markForPersonXpath;	
	
	private static final String PAYMENT_METHOD_XPATH = "PaymentMethods/PaymentMethod";
	private static XPathExpression paymentMethodXPath;

	private static transient DigitalLogger logger = DigitalLogger.getLogger(OrderStatusTransform.class);

	public abstract OrderStatusPayload transform(Document xmlPayloadDocument, OrderStatusEvent.OrderStatusType orderStatusType) throws DigitalIntegrationException;

	/**
	 *
	 * @return
	 */
	public XPathFactory getXpathFactory() {
		return xpathFactory;
	}

	/**
	 *
	 * @param xmlPayloadDocument
	 * @return
	 * @throws DigitalIntegrationException
	 */
	protected OmniOrderUpdate getOmniNode(Document xmlPayloadDocument) throws DigitalIntegrationException {
		configureInternal();

		try {
			Node omniNode = (Node) omniXPath.evaluate(xmlPayloadDocument, XPathConstants.NODE);
			try {
				JAXBContext jaxbContext = getJaxbContextCache().newJAXBContext(OmniOrderUpdate.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

				OmniOrderUpdate omniOrderUpdate = (OmniOrderUpdate) unmarshaller.unmarshal(omniNode);

				return omniOrderUpdate;

			} catch (Exception e) {
				throw new DigitalIntegrationSystemException("UnmarshalledObjectFromHttpResponse Error",
						ErrorCodes.XFORM.getCode(), e);
			}
		} catch (XPathExpressionException e) {
			throw new DigitalIntegrationException("Not able to query xpath expression data", ErrorCodes.XFORM.getCode(),
					"TRANSFORM", e);
		}
	}

	/**
	 *
	 * @param xPathExpression
	 * @param node
	 * @return
	 * @throws DigitalIntegrationException
	 */
	protected String getXPathString(XPathExpression xPathExpression, Node node) throws DigitalIntegrationException {
		try {
			return (String) xPathExpression.evaluate(node, XPathConstants.STRING);
		} catch (XPathExpressionException e) {
			throw new DigitalIntegrationException("Not able to query xpath expression data", ErrorCodes.XFORM.getCode(),
					"TRANSFORM", e);
		}
	}

	/**
	 *
	 * @param xPathExpression
	 * @param node
	 * @return
	 * @throws DigitalIntegrationException
	 */
	protected NodeList getXPathNodeList(XPathExpression xPathExpression, Node node) throws DigitalIntegrationException {
		try {
			return (NodeList) xPathExpression.evaluate(node, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new DigitalIntegrationException("Not able to query xpath expression data", ErrorCodes.XFORM.getCode(),
					"TRANSFORM", e);
		}
	}

	/**
	 *
	 * @param xPathExpression
	 * @param node
	 * @return
	 * @throws DigitalIntegrationException
	 */
	protected Node getXPathNode(XPathExpression xPathExpression, Node node) throws DigitalIntegrationException {
		try {
			return (Node) xPathExpression.evaluate(node, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new DigitalIntegrationException("Not able to query xpath expression data", ErrorCodes.XFORM.getCode(),
					"TRANSFORM", e);
		}
	}

	/**
	 *
	 * @param xpathFactory
	 */
	public static void setXpathFactory(XPathFactory xpathFactory) {
		OrderStatusTransform.xpathFactory = xpathFactory;
	}

	/**
	 *
	 * @param element
	 * @return
	 * @throws DigitalIntegrationException
	 */
	protected Person getPersonFromElement(Element element) throws DigitalIntegrationException {
		configureInternal();

		Person returnPerson = new Person();

		if (element != null) {

			returnPerson.setFirstName(element.getAttribute("FirstName"));
			returnPerson.setLastName(element.getAttribute("LastName"));
			returnPerson.setCity(element.getAttribute("City"));
			returnPerson.setAddressLine1(element.getAttribute("AddressLine1"));
			returnPerson.setAddressLine2(element.getAttribute("AddressLine2"));
			returnPerson.setAddressLine6(element.getAttribute("AddressLine6"));
			returnPerson.setState(element.getAttribute("State"));
			returnPerson.setZipCode(element.getAttribute("ZipCode"));
			returnPerson.setDayPhone(element.getAttribute("DayPhone"));
			returnPerson.setEmailId(element.getAttribute("EMailID"));
			returnPerson.setCountry(element.getAttribute("Country"));
			returnPerson.setPersonInfoKey(element.getAttribute("PersonInfoKey"));
			returnPerson.setCompany(element.getAttribute("Company"));
		}

		return returnPerson;
	}

	/**
	 *
	 * @param element
	 * @return
	 * @throws DigitalIntegrationException
	 */
	public String getMallPlazaNameFromOrgNode(Element element) throws DigitalIntegrationException {
		String mallPlazaName="";
		if (element != null) {
			mallPlazaName= element.getAttribute("AddressLine5");
		}
		return mallPlazaName;
	}

	/**
	 *
	 * @param element
	 * @return
	 * @throws DigitalIntegrationException
	 */
	public String getLoyaltyIdFromOrder(Element element) throws DigitalIntegrationException {
		String loyaltyId="";
		if (element != null) {
			loyaltyId= element.getAttribute("ExtnLoyaltyId");
		}
		return loyaltyId;
	}

	/**
	 *
	 * @param element
	 * @return
	 * @throws DigitalIntegrationException
	 */
	public String getCustomerFirstNameFromOrder(Element element) throws DigitalIntegrationException {
		String customerFirstName="";
		if (element != null) {
			customerFirstName= element.getAttribute("CustomerFirstName");
		}
		return customerFirstName;
	}

	/**
	 *
	 * @param element
	 * @return
	 * @throws DigitalIntegrationException
	 */
	protected OrderTotals getTotalsFromElement(Element element) throws DigitalIntegrationException {
		configureInternal();

		OrderTotals totals = new OrderTotals();

		if (element != null) {

			totals.setGrandCharges(Double.parseDouble(!DigitalStringUtil.isEmpty(element.getAttribute("GrandCharges"))
					? element.getAttribute("GrandCharges") : "0.00"));
			totals.setGrandDiscount(Double.parseDouble(!DigitalStringUtil.isEmpty(element.getAttribute("GrandDiscount"))
					? element.getAttribute("GrandDiscount") : "0.00"));
			totals.setGrandTax(Double.parseDouble(!DigitalStringUtil.isEmpty(element.getAttribute("GrandTax"))
					? element.getAttribute("GrandTax") : "0.00"));
			totals.setGrandTotal(Double.parseDouble(!DigitalStringUtil.isEmpty(element.getAttribute("GrandTotal"))
					? element.getAttribute("GrandTotal") : "0.00"));
			totals.setHdrCharges(Double.parseDouble(!DigitalStringUtil.isEmpty(element.getAttribute("HdrCharges"))
					? element.getAttribute("HdrCharges") : "0.00"));
			totals.setHdrDiscount(Double.parseDouble(!DigitalStringUtil.isEmpty(element.getAttribute("HdrDiscount"))
					? element.getAttribute("HdrDiscount") : "0.00"));
			totals.setHdrTax(Double.parseDouble(
					!DigitalStringUtil.isEmpty(element.getAttribute("HdrTax")) ? element.getAttribute("HdrTax") : "0.00"));
			totals.setHdrTotal(Double.parseDouble(!DigitalStringUtil.isEmpty(element.getAttribute("HdrTotal"))
					? element.getAttribute("HdrTotal") : "0.00"));
			totals.setLineSubTotal(Double.parseDouble(!DigitalStringUtil.isEmpty(element.getAttribute("LineSubTotal"))
					? element.getAttribute("LineSubTotal") : "0.00"));
			totals.setGrandShippingTotal(
					Double.parseDouble(!DigitalStringUtil.isEmpty(element.getAttribute("GrandShippingTotal"))
							? element.getAttribute("GrandShippingTotal") : "0.00"));
		}

		return totals;
	}

	/**
	 *
	 * @param nodes
	 * @return
	 * @throws DigitalIntegrationException
	 */
	protected List<Award> getAwardsFromElement(NodeList nodes) throws DigitalIntegrationException {
		configureInternal();

		List<Award> awards = new ArrayList<>();
		Award award;
		for (int i = 0; i < nodes.getLength(); i++) {
			Element awardElement = (Element) nodes.item(i);

			award = new Award();
			award.setAwardAmount(Double.parseDouble(!DigitalStringUtil.isEmpty(awardElement.getAttribute("AwardAmount"))
					? awardElement.getAttribute("AwardAmount") : "0.00"));
			award.setPromotionId(!DigitalStringUtil.isEmpty(awardElement.getAttribute("PromotionId"))
					? awardElement.getAttribute("PromotionId") : "");
			award.setAwardType(!DigitalStringUtil.isEmpty(awardElement.getAttribute("AwardType"))
					? awardElement.getAttribute("AwardType") : "");
			award.setDescription(!DigitalStringUtil.isEmpty(awardElement.getAttribute("Description"))
					? awardElement.getAttribute("Description") : "");
			awards.add(award);
		}
		return awards;
	}

	/*
	 * <PaymentMethod isHistory="N" OrderHeaderKey="20160303120053769196293"
	 * Modifyuserid="DSWPaymentAgentServer" Modifyts="2016-03-08T16:58:17-05:00"
	 * Modifyprogid="PAYMENT_COLLECTION" Lockid="4" CustomerPONo=""
	 * Createuserid="DSWNewOrderIntServer" Createts="2016-03-03T12:00:54-05:00"
	 * Createprogid="DSWNewOrderIntServer" BillToKey="20160303115751769194620"
	 * UnlimitedCharges="Y" TotalRefundedAmount="171.95" TotalCharged="171.95"
	 * TotalAuthorized="0.00" TotalAltRefundedAmount="0.00"
	 * SuspendAnyMoreCharges="N" RequestedRefundAmount="0.00"
	 * RequestedChargeAmount="0.00" RequestedAuthAmount="0.00"
	 * PrimaryAccountNo="4012000042580026" PaymentTypeGroup="CREDIT_CARD"
	 * PaymentType="CREDIT_CARD" PaymentReference3="1" PaymentReference2=""
	 * PaymentReference1="" PaymentKey="201603031280005769196296"
	 * PayMethodOverride="" MaxChargeLimit="0.00" IncompletePaymentType=""
	 * ExtendedFlag="" DisplaySvcNo="" DisplayPrimaryAccountNo="0026"
	 * DisplayPaymentReference1="" DisplayCustomerAccountNo=""
	 * DisplayCreditCardNo="0026" CreditCardType="VISA"
	 * CreditCardNo="4012000042580026" CreditCardName=""
	 * CreditCardExpDate="012017" CheckReference="" CheckNo=""
	 * ChargeSequence="0" Action=""/>
	 *
	 * @param nodes
	 * @return
	 * @throws DigitalIntegrationException
	 */
	protected List<PaymentMethod> getPaymentMethodsFromNodeList(NodeList nodes) throws DigitalIntegrationException {
		configureInternal();

		List<PaymentMethod> paymentMethods = new ArrayList<>();

		PaymentMethod paymentMethod;
		for (int i = 0; i < nodes.getLength(); i++) {
			Element paymentMethodElement = (Element) nodes.item(i);

			paymentMethod = new PaymentMethod();
			paymentMethod.setPaymentMethodType(paymentMethodElement.getAttribute("PaymentType"));
			paymentMethod.setPaymentReference2(paymentMethodElement.getAttribute("PaymentReference2"));
			paymentMethod.setCreditCardType(paymentMethodElement.getAttribute("CreditCardType"));
			paymentMethod.setDisplayCreditCardNo(paymentMethodElement.getAttribute("DisplayCreditCardNo"));
			paymentMethod.setDisplaySvcNo(paymentMethodElement.getAttribute("DisplaySvcNo"));
			paymentMethod.setTotalAuthorized(paymentMethodElement.getAttribute("TotalAuthorized"));

			paymentMethod.setOrderHeaderKey(paymentMethodElement.getAttribute("OrderHeaderKey"));
			paymentMethod.setBillToKey(paymentMethodElement.getAttribute("BillToKey"));
			paymentMethod.setTotalRefundedAmount(paymentMethodElement.getAttribute("TotalRefundedAmount"));
			paymentMethod.setTotalCharged(paymentMethodElement.getAttribute("TotalCharged"));
			paymentMethod.setTotalAltRefundedAmount(paymentMethodElement.getAttribute("TotalAltRefundedAmount"));
			paymentMethod.setRequestedRefundAmount(paymentMethodElement.getAttribute("RequestedRefundAmount"));
			paymentMethod.setRequestedChargeAmount(paymentMethodElement.getAttribute("RequestedChargeAmount"));
			paymentMethod.setRequestedAuthAmount(paymentMethodElement.getAttribute("RequestedAuthAmount"));
			paymentMethod.setCreditCardExpDate(paymentMethodElement.getAttribute("CreditCardExpDate"));
			paymentMethod.setCheckNo(paymentMethodElement.getAttribute("CheckNo"));
			paymentMethod.setPaymentTypeGroup(paymentMethodElement.getAttribute("PaymentTypeGroup"));

			paymentMethods.add(paymentMethod);
		}

		return paymentMethods;
	}

	/**
	 *
	 * @param nodes
	 * @return
	 * @throws DigitalIntegrationException
	 */
	protected List<OrderLine> getOrderLinesFromNodeList(NodeList nodes) throws DigitalIntegrationException {
		configureInternal();

		List<OrderLine> orderLines = new ArrayList<>();

		OrderLine orderLine;
		for (int i = 0; i < nodes.getLength(); i++) {
			Element orderLineElement = (Element) nodes.item(i);

			orderLine = new OrderLine();
			orderLine.setLineType(orderLineElement.getAttribute("LineType"));
			orderLine.setMinLineStatus(orderLineElement.getAttribute("MinLineStatus"));
			orderLine.setMaxLineStatus(orderLineElement.getAttribute("MaxLineStatus"));
			orderLine.setStatus(orderLineElement.getAttribute("Status"));
			orderLine.setIsSystemCanceled(orderLineElement.getAttribute("IsSystemCancelled"));
			orderLine.setCarrierServiceCode(orderLineElement.getAttribute("CarrierServiceCode"));

			orderLine.setOrderedQty((orderLineElement.getAttribute("OrderedQty").equalsIgnoreCase("0.00"))
					? convertDoubleQtyToInt(orderLineElement.getAttribute("OriginalOrderedQty"))
					: convertDoubleQtyToInt(orderLineElement.getAttribute("OrderedQty")));
			orderLine.setStatusQuantity(convertDoubleQtyToInt(orderLineElement.getAttribute("StatusQuantity")));
			orderLine.setKey(orderLineElement.getAttribute("OrderLineKey"));
			orderLine.setPrimeLineNo(orderLineElement.getAttribute("PrimeLineNo"));
			orderLine.setShipNode(orderLineElement.getAttribute("ShipNode"));
			orderLine.setFulfillmentType(orderLineElement.getAttribute("FulfillmentType"));
			orderLine.setDerivedFromOrderLineKey(orderLineElement.getAttribute("DerivedFromOrderLineKey"));
			orderLine.setReceivingNode(orderLineElement.getAttribute("ReceivingNode"));
			orderLine.setReceivedQty(convertDoubleQtyToInt(orderLineElement.getAttribute("ReceivedQty")));

			String carrierName = orderLineElement.getAttribute("SCAC");
			if(DigitalStringUtil.isNotBlank(carrierName)) {
				orderLine.setSCAC(carrierName.trim());
			}

			Element personInfoShipToElement = (Element) getXPathNode(shipToPersonXpath, orderLineElement);
			orderLine.setPersonInfoShipTo(getPersonFromElement(personInfoShipToElement));
		
			
			Element personInfoMarkForElement = (Element)getXPathNode(markForPersonXpath, orderLineElement);
			orderLine.setPersonInfoMarkFor(getPersonFromElement(personInfoMarkForElement));

			Element personInfoBillToElement = (Element) getXPathNode(billToPersonXpath, orderLineElement);
			orderLine.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));

			Element backorderElement = (Element) getXPathNode(statusBreakupXPath, orderLineElement);
			if (backorderElement != null) {
				orderLine.setBackorderQty(convertDoubleQtyToInt(backorderElement.getAttribute("BackOrderedQuantity")));

				Element backorderDetailsElement = (Element) getXPathNode(statusBreakupDetailsXPath, backorderElement);

				if (backorderDetailsElement != null) {
					orderLine.setBackorderFromStoreId(backorderDetailsElement.getAttribute("ShipNode"));
				}

			}

			Element cancelElement = (Element) getXPathNode(statusBreakupCancelXPath, orderLineElement);
			if (cancelElement != null) {
				orderLine.setCancelledQty(convertDoubleQtyToInt(cancelElement.getAttribute("Quantity")));

				Element backorderDetailsElement = (Element) getXPathNode(statusBreakupDetailsXPath, cancelElement);

				if (backorderDetailsElement != null) {
					orderLine.setCancelledFromStoreId(backorderDetailsElement.getAttribute("ShipNode"));
				}

			}

			Element itemDetailsElement = (Element) getXPathNode(itemDetailsXPath, orderLineElement);
			if (itemDetailsElement != null) {
				orderLine.setMaxModifyTS(itemDetailsElement.getAttribute("MaxModifyTS"));
				Element primaryInfoElement = (Element) getXPathNode(primaryInfoXPath, itemDetailsElement);
				if (primaryInfoElement != null) {
					orderLine.setPrimaryInfoStatus(primaryInfoElement.getAttribute("Status"));
					orderLine.setColorCode(primaryInfoElement.getAttribute("ColorCode"));
					orderLine.setUnitCost(primaryInfoElement.getAttribute("UnitCost"));
				}
			}

			Element linePriceInfoElement = (Element) getXPathNode(linePriceInfoXPath, orderLineElement);
			if (linePriceInfoElement != null) {
				orderLine.setUnitPrice(linePriceInfoElement.getAttribute("UnitPrice"));
			}

			Element itemElement = (Element) getXPathNode(orderLinesItemXPath, orderLineElement);
			if (itemElement != null) {
				orderLine.setItemDescription(itemElement.getAttribute("ItemDesc"));
				orderLine.setItemShortDescription(itemElement.getAttribute("ItemShortDesc"));
				if (DigitalStringUtil.isNotBlank(itemElement.getAttribute("ItemID"))) {
					orderLine.setItemId(itemElement.getAttribute("ItemID"));
				}
				if (DigitalStringUtil.isNotBlank(itemElement.getAttribute("CustomerItem"))) {
					orderLine.setItemStyle(itemElement.getAttribute("CustomerItem"));
				}
				if (DigitalStringUtil.isBlank(orderLine.getUnitCost())) {
					orderLine.setUnitCost(itemElement.getAttribute("UnitCost"));
				}
			}

			Element extn = (Element) getXPathNode(extnXPath, orderLineElement);
			if (extn != null) {
				orderLine.setProductTitle(extn.getAttribute("ExtnProductTitle"));
				if (DigitalStringUtil.isBlank(orderLine.getItemStyle())
						&& DigitalStringUtil.isNotBlank(extn.getAttribute("ExtnItemStyle"))) {
					orderLine.setItemStyle(DigitalStringUtil.stripStart(extn.getAttribute("ExtnItemStyle")));
				}

				if (DigitalStringUtil.isBlank(orderLine.getItemId())) {
					if (DigitalStringUtil.isNotBlank(extn.getAttribute("ExtnItemID"))) {
						orderLine.setItemId(extn.getAttribute("ExtnItemID"));
					}
				}

				orderLine.setItemColor(extn.getAttribute("ExtnItemColor"));
				if (DigitalStringUtil.isNotBlank(extn.getAttribute("ExtnItemSize"))) {
					orderLine.setItemSize(DigitalStringUtil.stripStart(extn.getAttribute("ExtnItemSize")));
				}
				orderLine.setItemWidth(extn.getAttribute("ExtnItemWidth"));
				orderLine.setItemBrand(extn.getAttribute("ExtnItemBrand"));
				orderLine.setSvsCardNumber(extn.getAttribute("ExtnSVSCardNumber"));
				orderLine.setSignatureRequired(extn.getAttribute("ExtnIsItemSignatureReqd"));
				orderLine.setItemName(extn.getAttribute("ExtnItemName"));
				
				// GC related info if present
				orderLine.setRecipientName(extn.getAttribute("ExtnGiftCardTo"));
				orderLine.setRecipientEmail(extn.getAttribute("ExtnGiftCardRecipientEmail"));
				orderLine.setSenderName(extn.getAttribute("ExtnGiftCardFrom"));
				orderLine.setSenderEmail(extn.getAttribute("ExtnGiftCardSenderEmail"));
			}

			if(DigitalStringUtil.isBlank(orderLine.getItemDescription())){
				if(DigitalStringUtil.isNotBlank(orderLine.getItemShortDescription())){
					orderLine.setItemDescription(orderLine.getItemShortDescription());
				}else if(DigitalStringUtil.isNotBlank(orderLine.getItemName())){
					orderLine.setItemDescription(orderLine.getItemName());
				}
			}

			Element totalsElement = (Element) getXPathNode(orderLinesTotalsXPath, orderLineElement);
			if (totalsElement != null) {
				orderLine.setUnitPrice(totalsElement.getAttribute("UnitPrice"));
				orderLine.setTax(totalsElement.getAttribute("Tax"));
			}

			if (DigitalStringUtil.isNotBlank(orderLine.getItemId()) && !orderLine.getItemId().endsWith("-GC")) {
				if (DigitalStringUtil.isBlank(orderLine.getItemStyle())) {
					orderLine.setItemStyle(orderLine.getItemId().substring(10, 16));
				}

				if (DigitalStringUtil.isBlank(orderLine.getColorCode())) {
					orderLine.setColorCode(orderLine.getItemId().substring(16, 19));
				}
			}

			if (orderLine.getItemId().endsWith("-GC")) {
				orderLine.setItemStyle(orderLine.getItemId());
			}

			NodeList fromAwardsNodes = getXPathNodeList(awardXPath, orderLineElement);
			Award award = null;
			for (int z = 0; z < fromAwardsNodes.getLength(); z++) {
				Element fromAwardsElement = (Element) fromAwardsNodes.item(z);

				award = new Award();
				if (DigitalStringUtil.isNotEmpty(fromAwardsElement.getAttribute("AwardAmount"))) {
					award.setAwardAmount(Double.valueOf(fromAwardsElement.getAttribute("AwardAmount")));
				}
				award.setDescription(fromAwardsElement.getAttribute("Description"));
				award.setAwardType(fromAwardsElement.getAttribute("AwardType"));
				orderLine.getAwards().add(award);
			}

			OrderLineReservation orderLineReservation;
			Element orderLineReservationElement = (Element) getXPathNode(orderLinesReservationXPath, orderLineElement);
			if (orderLineReservationElement != null) {
				orderLineReservation = new OrderLineReservation();
				orderLineReservation.setNode(orderLineReservationElement.getAttribute("Node"));
				orderLine.getOrderLineReservations().add(orderLineReservation);
			}

			NodeList fromOrderStatusNodes = getXPathNodeList(fromOrderStatusXPath, orderLineElement);
			FromOrderReleaseStatus fromOrderStatus;
			for (int j = 0; j < fromOrderStatusNodes.getLength(); j++) {
				Element fromOrderStatusElement = (Element) fromOrderStatusNodes.item(j);

				fromOrderStatus = new FromOrderReleaseStatus();
				fromOrderStatus.setStatus(fromOrderStatusElement.getAttribute("Status"));
				fromOrderStatus
						.setTotalQuantity(convertDoubleQtyToInt(fromOrderStatusElement.getAttribute("TotalQuantity")));
				fromOrderStatus
						.setStatusQuantity(convertDoubleQtyToInt(fromOrderStatusElement.getAttribute("StatusQty")));
				fromOrderStatus.setStatusDate(fromOrderStatusElement.getAttribute("StatusDate"));
				fromOrderStatus.setDatesChanged(fromOrderStatusElement.getAttribute("DatesChanged"));
				fromOrderStatus.setMovedQty(convertDoubleQtyToInt(fromOrderStatusElement.getAttribute("MovedQty")));
				fromOrderStatus.setOrderReleaseStatusKey(fromOrderStatusElement.getAttribute("OrderReleaseStatusKey"));
				fromOrderStatus.setPipelineKey(fromOrderStatusElement.getAttribute("PipelineKey"));

				Element schedule = (Element) getXPathNode(fromOrderScheduleXPath, fromOrderStatusElement);
				if (schedule != null) {
					fromOrderStatus.setExpectedDeliveryDate(schedule.getAttribute("ExpectedDeliveryDate"));
					fromOrderStatus.setExpectedShipmentDate(schedule.getAttribute("ExpectedShipmentDate"));
					fromOrderStatus.setLotNumber(schedule.getAttribute("LotNumber"));
					fromOrderStatus.setOrderLineScheduleKey(schedule.getAttribute("OrderLineScheduleKey"));
					fromOrderStatus.setShipNode(schedule.getAttribute("ShipNode"));
				}

				orderLine.getFromOrderReleaseStatus().add(fromOrderStatus);

				NodeList toOrderStatusNodes = getXPathNodeList(toOrderStatusXPath, fromOrderStatusElement);
				ToOrderReleaseStatus toOrderStatus;
				for (int k = 0; k < toOrderStatusNodes.getLength(); k++) {
					Element toOrderStatusElement = (Element) toOrderStatusNodes.item(k);

					toOrderStatus = new ToOrderReleaseStatus();
					toOrderStatus.setStatus(toOrderStatusElement.getAttribute("Status"));
					toOrderStatus.setTotalQuantity(
							convertDoubleQtyToInt(toOrderStatusElement.getAttribute("TotalQuantity")));
					toOrderStatus
							.setStatusQuantity(convertDoubleQtyToInt(toOrderStatusElement.getAttribute("StatusQty")));
					toOrderStatus.setStatusDate(toOrderStatusElement.getAttribute("StatusDate"));
					toOrderStatus.setDatesChanged(toOrderStatusElement.getAttribute("DatesChanged"));
					toOrderStatus.setMovedQty(convertDoubleQtyToInt(toOrderStatusElement.getAttribute("MovedQty")));
					toOrderStatus.setOrderReleaseStatusKey(toOrderStatusElement.getAttribute("OrderReleaseStatusKey"));
					toOrderStatus.setPipelineKey(toOrderStatusElement.getAttribute("PipelineKey"));

					schedule = (Element) getXPathNode(toOrderScheduleXPath, toOrderStatusElement);
					if (schedule != null) {
						toOrderStatus.setExpectedDeliveryDate(schedule.getAttribute("ExpectedDeliveryDate"));
						toOrderStatus.setExpectedShipmentDate(schedule.getAttribute("ExpectedShipmentDate"));
						toOrderStatus.setLotNumber(schedule.getAttribute("LotNumber"));
						toOrderStatus.setOrderLineScheduleKey(schedule.getAttribute("OrderLineScheduleKey"));
						toOrderStatus.setShipNode(schedule.getAttribute("ShipNode"));
					}

					fromOrderStatus.getToOrderReleaseStatus().add(toOrderStatus);
				}

			}

			NodeList orderStatusNodes = getXPathNodeList(statusXPath, orderLineElement);
			OrderStatus orderStatus;
			for (int l = 0; l < orderStatusNodes.getLength(); l++) {
				Element orderStatusElement = (Element) orderStatusNodes.item(l);

				orderStatus = new OrderStatus();

				orderStatus.setOrderReleaseKey(orderStatusElement.getAttribute("OrderReleaseKey"));
				orderStatus.setOrderReleaseStatusKey(orderStatusElement.getAttribute("OrderReleaseStatusKey"));
				orderStatus.setStatus(orderStatusElement.getAttribute("Status"));
				orderStatus.setShipNode(orderStatusElement.getAttribute("ShipNode"));
				orderStatus.setStatusQuantity(convertDoubleQtyToInt(orderStatusElement.getAttribute("StatusQty")));
				orderStatus.setStatusDescription(orderStatusElement.getAttribute("StatusDescription"));
				orderStatus.setStatusDate(orderStatusElement.getAttribute("StatusDate"));
				orderStatus.setIsSystemCancelled(orderStatusElement.getAttribute("IsSystemCancelled"));
				
				Element statusDetailsElement = (Element) getXPathNode(statusDetailsXPath, orderStatusElement);
				if (statusDetailsElement != null) {
					orderStatus.setExpectedDeliveryDate(statusDetailsElement.getAttribute("ExpectedDeliveryDate"));
					orderStatus.setExpectedShipmentDate(statusDetailsElement.getAttribute("ExpectedShipmentDate"));
				}
				//setting OrderLineKey if for RETURN_RECEIVED email.
				if(orderLine.getKey()!=null && orderLine.getKey().length()<=0  ){
					orderLine.setKey(orderStatusElement.getAttribute("OrderLineKey"));
				}

				orderLine.getOrderStatus().add(orderStatus);
			}

			orderLines.add(orderLine);
		}
		return orderLines;
	}

	/**
	 *
	 * @param nodes
	 * @return
	 * @throws DigitalIntegrationException
	 */
	protected Map<String, OrderLine> getOrderLinesMapFromNodeList(NodeList nodes) throws DigitalIntegrationException {
		configureInternal();

		Map<String, OrderLine> orderLines = new HashMap<>();

		OrderLine orderLine;
		for (int i = 0; i < nodes.getLength(); i++) {
			Element orderLineElement = (Element) nodes.item(i);

			orderLine = new OrderLine();
			orderLine.setLineType(orderLineElement.getAttribute("LineType"));
			orderLine.setMinLineStatus(orderLineElement.getAttribute("MinLineStatus"));
			orderLine.setMaxLineStatus(orderLineElement.getAttribute("MaxLineStatus"));
			orderLine.setStatus(orderLineElement.getAttribute("Status"));
			orderLine.setIsSystemCanceled(orderLineElement.getAttribute("IsSystemCancelled"));
			orderLine.setCarrierServiceCode(orderLineElement.getAttribute("CarrierServiceCode"));

			orderLine.setOrderedQty((orderLineElement.getAttribute("OrderedQty").equalsIgnoreCase("0.00"))
					? orderLineElement.getAttribute("OriginalOrderedQty")
					: orderLineElement.getAttribute("OrderedQty"));
			orderLine.setStatusQuantity(convertDoubleQtyToInt(orderLineElement.getAttribute("StatusQuantity")));
			orderLine.setKey(orderLineElement.getAttribute("OrderLineKey"));
			orderLine.setPrimeLineNo(orderLineElement.getAttribute("PrimeLineNo"));
			orderLine.setShipNode(orderLineElement.getAttribute("ShipNode"));
			orderLine.setFulfillmentType(orderLineElement.getAttribute("FulfillmentType"));
			orderLine.setDerivedFromOrderLineKey(orderLineElement.getAttribute("DerivedFromOrderLineKey"));
			orderLine.setReceivingNode(orderLineElement.getAttribute("ReceivingNode"));
			orderLine.setReceivedQty(convertDoubleQtyToInt(orderLineElement.getAttribute("ReceivedQty")));

			String carrierName = orderLineElement.getAttribute("SCAC");
			if(DigitalStringUtil.isNotBlank(carrierName)) {
				orderLine.setSCAC(carrierName.trim());
			}

			Element personInfoShipToElement = (Element) getXPathNode(shipToPersonXpath, orderLineElement);
			orderLine.setPersonInfoShipTo(getPersonFromElement(personInfoShipToElement));

			Element personInfoBillToElement = (Element) getXPathNode(billToPersonXpath, orderLineElement);
			orderLine.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));

			Element backorderElement = (Element) getXPathNode(statusBreakupXPath, orderLineElement);
			if (backorderElement != null) {
				orderLine.setBackorderQty(convertDoubleQtyToInt(backorderElement.getAttribute("BackOrderedQuantity")));

				Element backorderDetailsElement = (Element) getXPathNode(statusBreakupDetailsXPath, backorderElement);

				if (backorderDetailsElement != null) {
					orderLine.setBackorderFromStoreId(backorderDetailsElement.getAttribute("ShipNode"));
				}

			}

			Element cancelElement = (Element) getXPathNode(statusBreakupCancelXPath, orderLineElement);
			if (cancelElement != null) {
				orderLine.setCancelledQty(convertDoubleQtyToInt(cancelElement.getAttribute("Quantity")));

				Element backorderDetailsElement = (Element) getXPathNode(statusBreakupDetailsXPath, cancelElement);

				if (backorderDetailsElement != null) {
					orderLine.setCancelledFromStoreId(backorderDetailsElement.getAttribute("ShipNode"));
				}

			}

			Element itemDetailsElement = (Element) getXPathNode(itemDetailsXPath, orderLineElement);
			if (itemDetailsElement != null) {
				orderLine.setMaxModifyTS(itemDetailsElement.getAttribute("MaxModifyTS"));
				Element primaryInfoElement = (Element) getXPathNode(primaryInfoXPath, itemDetailsElement);
				if (primaryInfoElement != null) {
					orderLine.setPrimaryInfoStatus(primaryInfoElement.getAttribute("Status"));
					orderLine.setColorCode(primaryInfoElement.getAttribute("ColorCode"));
				}
			}

			Element linePriceInfoElement = (Element) getXPathNode(linePriceInfoXPath, orderLineElement);
			if (linePriceInfoElement != null) {
				orderLine.setUnitPrice(linePriceInfoElement.getAttribute("UnitPrice"));
			}

			Element itemElement = (Element) getXPathNode(orderLinesItemXPath, orderLineElement);
			if (itemElement != null) {
				orderLine.setItemDescription(itemElement.getAttribute("ItemDesc"));
				orderLine.setItemShortDescription(itemElement.getAttribute("ItemShortDesc"));
				if (DigitalStringUtil.isNotBlank(itemElement.getAttribute("ItemID"))) {
					orderLine.setItemId(itemElement.getAttribute("ItemID"));
				}
				if (DigitalStringUtil.isNotBlank(itemElement.getAttribute("CustomerItem"))) {
					orderLine.setItemStyle(itemElement.getAttribute("CustomerItem"));
				}
				orderLine.setUnitCost(itemElement.getAttribute("UnitCost"));
			}

			Element extn = (Element) getXPathNode(extnXPath, orderLineElement);
			if (extn != null) {
				orderLine.setProductTitle(extn.getAttribute("ExtnProductTitle"));
				if (DigitalStringUtil.isBlank(orderLine.getItemStyle())
						&& DigitalStringUtil.isNotBlank(extn.getAttribute("ExtnItemStyle"))) {
					orderLine.setItemStyle(DigitalStringUtil.stripStart(extn.getAttribute("ExtnItemStyle")));
				}

				if (DigitalStringUtil.isBlank(orderLine.getItemId())) {
					if (DigitalStringUtil.isNotBlank(extn.getAttribute("ExtnItemID"))) {
						orderLine.setItemId(extn.getAttribute("ExtnItemID"));
					}
				}

				orderLine.setItemColor(extn.getAttribute("ExtnItemColor"));
				if (DigitalStringUtil.isNotBlank(extn.getAttribute("ExtnItemSize"))) {
					orderLine.setItemSize(DigitalStringUtil.stripStart(extn.getAttribute("ExtnItemSize")));
				}
				orderLine.setItemWidth(extn.getAttribute("ExtnItemWidth"));
				orderLine.setItemBrand(extn.getAttribute("ExtnItemBrand"));
				orderLine.setSvsCardNumber(extn.getAttribute("ExtnSVSCardNumber"));
				orderLine.setSignatureRequired(extn.getAttribute("ExtnIsItemSignatureReqd"));
				orderLine.setItemName(extn.getAttribute("ExtnItemName"));

				// GC related info if present
				orderLine.setRecipientName(extn.getAttribute("ExtnGiftCardTo"));
				orderLine.setRecipientEmail(extn.getAttribute("ExtnGiftCardRecipientEmail"));
				orderLine.setSenderName(extn.getAttribute("ExtnGiftCardFrom"));
				orderLine.setSenderEmail(extn.getAttribute("ExtnGiftCardSenderEmail"));
			}

			if (DigitalStringUtil.isBlank(orderLine.getItemStyle()) && DigitalStringUtil.isNotBlank(orderLine.getItemId())) {
				orderLine.setItemStyle(orderLine.getItemId().substring(10, 16));
			}

			if (DigitalStringUtil.isBlank(orderLine.getColorCode())) {
				orderLine.setColorCode(orderLine.getItemId().substring(16, 19));
			}

			Element totalsElement = (Element) getXPathNode(orderLinesTotalsXPath, orderLineElement);
			if (totalsElement != null) {
				orderLine.setUnitPrice(totalsElement.getAttribute("UnitPrice"));
				orderLine.setTax(totalsElement.getAttribute("Tax"));
			}

			NodeList fromAwardsNodes = getXPathNodeList(awardXPath, orderLineElement);
			Award award = null;
			for (int z = 0; z < fromAwardsNodes.getLength(); z++) {
				Element fromAwardsElement = (Element) fromAwardsNodes.item(z);

				award = new Award();
				if (DigitalStringUtil.isNotEmpty(fromAwardsElement.getAttribute("AwardAmount"))) {
					award.setAwardAmount(Double.valueOf(fromAwardsElement.getAttribute("AwardAmount")));
				}
				award.setDescription(fromAwardsElement.getAttribute("Description"));
				award.setAwardType(fromAwardsElement.getAttribute("AwardType"));
				orderLine.getAwards().add(award);
			}

			OrderLineReservation orderLineReservation;
			Element orderLineReservationElement = (Element) getXPathNode(orderLinesReservationXPath, orderLineElement);
			if (orderLineReservationElement != null) {
				orderLineReservation = new OrderLineReservation();
				orderLineReservation.setNode(orderLineReservationElement.getAttribute("Node"));
				orderLine.getOrderLineReservations().add(orderLineReservation);
			}

			NodeList fromOrderStatusNodes = getXPathNodeList(fromOrderStatusXPath, orderLineElement);
			FromOrderReleaseStatus fromOrderStatus;
			for (int j = 0; j < fromOrderStatusNodes.getLength(); j++) {
				Element fromOrderStatusElement = (Element) fromOrderStatusNodes.item(j);

				fromOrderStatus = new FromOrderReleaseStatus();
				fromOrderStatus.setStatus(fromOrderStatusElement.getAttribute("Status"));
				fromOrderStatus
						.setTotalQuantity(convertDoubleQtyToInt(fromOrderStatusElement.getAttribute("TotalQuantity")));
				fromOrderStatus
						.setStatusQuantity(convertDoubleQtyToInt(fromOrderStatusElement.getAttribute("StatusQty")));
				fromOrderStatus.setStatusDate(fromOrderStatusElement.getAttribute("StatusDate"));
				fromOrderStatus.setDatesChanged(fromOrderStatusElement.getAttribute("DatesChanged"));
				fromOrderStatus.setMovedQty(convertDoubleQtyToInt(fromOrderStatusElement.getAttribute("MovedQty")));
				fromOrderStatus.setOrderReleaseStatusKey(fromOrderStatusElement.getAttribute("OrderReleaseStatusKey"));
				fromOrderStatus.setPipelineKey(fromOrderStatusElement.getAttribute("PipelineKey"));

				Element schedule = (Element) getXPathNode(fromOrderScheduleXPath, fromOrderStatusElement);
				if (schedule != null) {
					fromOrderStatus.setExpectedDeliveryDate(schedule.getAttribute("ExpectedDeliveryDate"));
					fromOrderStatus.setExpectedShipmentDate(schedule.getAttribute("ExpectedShipmentDate"));
					fromOrderStatus.setLotNumber(schedule.getAttribute("LotNumber"));
					fromOrderStatus.setOrderLineScheduleKey(schedule.getAttribute("OrderLineScheduleKey"));
					fromOrderStatus.setShipNode(schedule.getAttribute("ShipNode"));
				}

				orderLine.getFromOrderReleaseStatus().add(fromOrderStatus);

				NodeList toOrderStatusNodes = getXPathNodeList(toOrderStatusXPath, fromOrderStatusElement);
				ToOrderReleaseStatus toOrderStatus;
				for (int k = 0; k < toOrderStatusNodes.getLength(); k++) {
					Element toOrderStatusElement = (Element) toOrderStatusNodes.item(k);

					toOrderStatus = new ToOrderReleaseStatus();
					toOrderStatus.setStatus(toOrderStatusElement.getAttribute("Status"));
					toOrderStatus.setTotalQuantity(
							convertDoubleQtyToInt(toOrderStatusElement.getAttribute("TotalQuantity")));
					toOrderStatus
							.setStatusQuantity(convertDoubleQtyToInt(toOrderStatusElement.getAttribute("StatusQty")));
					toOrderStatus.setStatusDate(toOrderStatusElement.getAttribute("StatusDate"));
					toOrderStatus.setDatesChanged(toOrderStatusElement.getAttribute("DatesChanged"));
					toOrderStatus.setMovedQty(convertDoubleQtyToInt(toOrderStatusElement.getAttribute("MovedQty")));
					toOrderStatus.setOrderReleaseStatusKey(toOrderStatusElement.getAttribute("OrderReleaseStatusKey"));
					toOrderStatus.setPipelineKey(toOrderStatusElement.getAttribute("PipelineKey"));

					schedule = (Element) getXPathNode(toOrderScheduleXPath, toOrderStatusElement);
					if (schedule != null) {
						toOrderStatus.setExpectedDeliveryDate(schedule.getAttribute("ExpectedDeliveryDate"));
						toOrderStatus.setExpectedShipmentDate(schedule.getAttribute("ExpectedShipmentDate"));
						toOrderStatus.setLotNumber(schedule.getAttribute("LotNumber"));
						toOrderStatus.setOrderLineScheduleKey(schedule.getAttribute("OrderLineScheduleKey"));
						toOrderStatus.setShipNode(schedule.getAttribute("ShipNode"));
					}

					fromOrderStatus.getToOrderReleaseStatus().add(toOrderStatus);
				}

			}

			NodeList orderStatusNodes = getXPathNodeList(statusXPath, orderLineElement);
			OrderStatus orderStatus;
			for (int l = 0; l < orderStatusNodes.getLength(); l++) {
				Element orderStatusElement = (Element) orderStatusNodes.item(l);

				orderStatus = new OrderStatus();

				orderStatus.setOrderReleaseKey(orderStatusElement.getAttribute("OrderReleaseKey"));
				orderStatus.setStatus(orderStatusElement.getAttribute("Status"));
				orderStatus.setShipNode(orderStatusElement.getAttribute("ShipNode"));
				orderStatus.setStatusQuantity(convertDoubleQtyToInt(orderStatusElement.getAttribute("StatusQty")));
				orderStatus.setStatusDescription(orderStatusElement.getAttribute("StatusDescription"));
				orderStatus.setStatusDate(orderStatusElement.getAttribute("StatusDate"));

				Element statusDetailsElement = (Element) getXPathNode(statusDetailsXPath, orderStatusElement);
				if (statusDetailsElement != null) {
					orderStatus.setExpectedDeliveryDate(statusDetailsElement.getAttribute("ExpectedDeliveryDate"));
					orderStatus.setExpectedShipmentDate(statusDetailsElement.getAttribute("ExpectedShipmentDate"));
				}

				orderLine.getOrderStatus().add(orderStatus);
			}

			orderLines.put(orderLineElement.getAttribute("OrderLineKey"), orderLine);
		}

		return orderLines;
	}

	/**
	 *
	 * @param originalOrderLines
	 * @return
	 */
	protected List<OrderLine> separateOrderLinesByStatus(List<OrderLine> originalOrderLines) {
		List<OrderLine> separatedList = new ArrayList<>();

		for (OrderLine orderLine : originalOrderLines) {
			if (orderLine.getOrderStatus() != null && orderLine.getOrderStatus().size() > 0) {
				OrderLine newOrderLine = null;
				for (OrderStatus orderStatus : orderLine.getOrderStatus()) {
					newOrderLine = (OrderLine) SerializationUtils.clone(orderLine);
					newOrderLine.getOrderStatus().clear();
					newOrderLine.getOrderStatus().add(orderStatus);
					separatedList.add(newOrderLine);
				}
			} else {
				separatedList.add(orderLine);
			}
		}

		return separatedList;
	}

	/**
	 * In case of split scenarios, the OrderLine will have multiple statuses
	 * depending the quantity being split. So this method will create OrderLine
	 * (by cloning) for each status so that the email can render by loop through
	 * the OrderLine.
	 * 
	 * @param payload
	 * @param xmlPayloadDocument
	 * @throws DigitalIntegrationException
	 */
	public void populateOrderSummary(OrderStatusPayload payload, Document xmlPayloadDocument)
			throws DigitalIntegrationException {
		Element orderSummary = (Element) getXPathNode(orderSummaryXPath, xmlPayloadDocument);

		populateGeneralFields(payload, orderSummary);

		NodeList shipments = (NodeList) getXPathNodeList(orderSummaryShipmentXPath, xmlPayloadDocument);
		for (int i = 0; i < shipments.getLength(); i++) {
			Element shipmentElement = (Element) shipments.item(i);
			
			NodeList containerNodes = (NodeList) getXPathNodeList(orderContainerXPath, shipmentElement);
			List<Container> containers = getContainersFromShipment(containerNodes);
			
			String shipmentDate = shipmentElement.getAttribute("ActualShipmentDate");
			String shipmentKey = shipmentElement.getAttribute("ShipmentKey");
			String carrierName = shipmentElement.getAttribute("SCAC");
			String shipmentNo = shipmentElement.getAttribute("ShipmentNo");

			ShipmentLine shipmentLine;
			NodeList shipmentLineNodes = getXPathNodeList(orderShipmentLineXPath, shipmentElement);
			for (int j = 0; j < shipmentLineNodes.getLength(); j++) {
				Element shipmentLineElement = (Element) shipmentLineNodes.item(j);

				shipmentLine = new ShipmentLine();
				shipmentLine.setItemId(shipmentLineElement.getAttribute("ItemID"));
				shipmentLine.setOrderLineKey(shipmentLineElement.getAttribute("OrderLineKey"));
				shipmentLine.setQuantity(convertDoubleQtyToInt(shipmentLineElement.getAttribute("Quantity")));
				shipmentLine.setShipmentLineNo(shipmentLineElement.getAttribute("ShipmentLineNo"));
				shipmentLine.setOrderReleaseKey(shipmentLineElement.getAttribute("OrderReleaseKey"));

				Element shipOrderLine = (Element) getXPathNode(orderShipmentOrderLineXPath, shipmentLineElement);
				shipmentLine.setOrderedQuantity(convertDoubleQtyToInt(shipOrderLine.getAttribute("OrderedQty")));
				shipmentLine.setShippingMethod(shipOrderLine.getAttribute("CarrierServiceCode"));

				NodeList orderLineNodes = getXPathNodeList(orderShipmentOrderLineXPath, shipmentLineElement);
				shipmentLine.setOrderLines(getOrderLinesFromNodeList(orderLineNodes));

				shipmentLine.setShipmentDate(shipmentDate);
				shipmentLine.setShipmentKey(shipmentKey);
				if(DigitalStringUtil.isNotBlank(carrierName)) {
					shipmentLine.setCarrierName(carrierName.trim());
				}
				shipmentLine.setShipmentNo(shipmentNo);

				List<Container> shipmentLineContainer = findContainersForShipmentLine(shipmentLine, containers);
				
				if(shipmentLineContainer.size() > 1){//multi-quantity item with one release and shipped with multiple containers
/*					int k=0;
					for(Container container : shipmentLineContainer){
						ShipmentLine shipmentLineCloned = null;
						if(k != 0){
							shipmentLineCloned = (ShipmentLine) SerializationUtils.clone(shipmentLine);
						}
						shipmentLineCloned.setTrackingNo(shipmentLineContainer.get(0).getTrackingNo());
						shipmentLineCloned.setContainer(shipmentLineContainer.get(0));
						shipmentLineCloned.setQuantity(shipmentLineContainer.get(0).getQuantity());
						payload.getPayloadShipmentLines().add(shipmentLineCloned);
					}*/
					
					shipmentLine.setContainer(shipmentLineContainer);
					
				}else{
					
					shipmentLine.setContainer(shipmentLineContainer);
					if(!shipmentLineContainer.isEmpty()){
						shipmentLine.setTrackingNo(shipmentLineContainer.get(0).getTrackingNo());					
						shipmentLine.setQuantity(shipmentLineContainer.get(0).getQuantity());
					}
				}
				payload.getShipmentLines().add(shipmentLine);
			}
		}
		
		NodeList summaryOrderLinesNodes = getXPathNodeList(orderOrderLineXPath, orderSummary);
		List<OrderLine> orderLines = getOrderLinesFromNodeList(summaryOrderLinesNodes);

		List<OrderLine> toRemove = new ArrayList<>();
		List<OrderLine> toAdd = new ArrayList<>();

		for (OrderLine orderLine : orderLines) {
			boolean removeThisOrderLine = false;
			List<OrderStatus> orderStatuses = orderLine.getOrderStatus();
			if (orderStatuses.size() > 1) {// split quantity
				boolean ignore1400Status = ignore1400Status(orderStatuses, Integer.parseInt(orderLine.getOrderedQty()));
				for (OrderStatus orderStatus : orderStatuses) {
					if (ignore1400Status && DigitalStringUtil.isBlank(orderStatus.getStatus())
							|| "1400".equalsIgnoreCase(orderStatus.getStatus())) {
						continue;
					} else {
						OrderLine newOrderLine = (OrderLine) SerializationUtils.clone(orderLine);
						List<OrderStatus> newOrderStatuses = newOrderLine.getOrderStatus();
						newOrderStatuses.clear();
						OrderStatus newOrderStatus = (OrderStatus) SerializationUtils.clone(orderStatus);
						newOrderStatuses.add(newOrderStatus);
						newOrderLine.setMaxLineStatus(newOrderStatus.getStatus());
						toAdd.add(newOrderLine);
						removeThisOrderLine = true;
					}
				}
			}

			if (removeThisOrderLine) {
				toRemove.add(orderLine);
			}
		}

		if (toRemove.size() > 0) {
			orderLines.removeAll(toRemove);
			orderLines.addAll(toAdd);
		}

		toAdd.clear();
		boolean personInfoMarkForSet = false;
		for (OrderLine orderLine : orderLines) {
			String orderReleaseKey = null;
			if (orderLine.getOrderStatus() != null && !orderLine.getOrderStatus().isEmpty()
					&& orderLine.getOrderStatus().get(0) != null) {
				OrderStatus os = orderLine.getOrderStatus().get(0);
				orderReleaseKey = os.getOrderReleaseKey();
				orderLine.setStatusQuantity(os.getStatusQuantity());
				orderLine.setMaxLineStatus(os.getStatus());
				String status = getATGMappedOrderLineStatus(orderLine.getMaxLineStatus());
				if (DigitalStringUtil.isBlank(status)) {
					status = getATGMappedOrderLineStatus("UNKNOWN");
					logger.info("Defaulting to In Process as mapping was not found for order line status = "
							+ orderLine.getMaxLineStatus());
				}
				orderLine.setStatus(status);
			}
			if (!personInfoMarkForSet && orderLine.getPersonInfoMarkFor() != null
					&& DigitalStringUtil.isNotBlank(orderLine.getPersonInfoMarkFor().getFirstName())
					&& DigitalStringUtil.isNotBlank(orderLine.getPersonInfoMarkFor().getEmailId())) {
				Person person = (Person) SerializationUtils.clone(orderLine.getPersonInfoMarkFor());
				payload.setPersonInfoMarkFor(person);
				personInfoMarkForSet = true;
			}
			
			List<ShipmentLine> usedShipmentLines = new ArrayList<>();
			
			for (ShipmentLine shipmentLine : payload.getShipmentLines()) {
				if(usedShipmentLines.contains(shipmentLine)){
					continue;
				}
				if (orderLine.getKey().equalsIgnoreCase(shipmentLine.getOrderLineKey())
						&& DigitalStringUtil.isNotBlank(shipmentLine.getOrderReleaseKey())
						&& shipmentLine.getOrderReleaseKey().equalsIgnoreCase(orderReleaseKey)
						&& shipmentLine.getQuantity().equalsIgnoreCase(orderLine.getStatusQuantity())
						&& ("3700".equalsIgnoreCase(orderLine.getMaxLineStatus()) || 
						"3200".equalsIgnoreCase(orderLine.getMaxLineStatus()))) {
					
					if(shipmentLine.getContainers().size() > 1 ){//tricky - this is multi-quantity item with same release and shipped with multiple containers
						int i = 0;
						for(Container container : shipmentLine.getContainers()){
							OrderLine newOrderLine = orderLine;
							if(i != 0){
								newOrderLine = (OrderLine) SerializationUtils.clone(orderLine);
								toAdd.add(newOrderLine);
							}newOrderLine.setOrderId(payload.getOrderNo());
							newOrderLine.setTrackingNo(container.getTrackingNo());
							newOrderLine.setStatusQuantity(container.getQuantity());
							newOrderLine.getShipLines().add(shipmentLine);
							newOrderLine.setSCAC(shipmentLine.getCarrierName());
							newOrderLine.setCarrierServiceCode(shipmentLine.getShippingMethod());
							if("3200".equalsIgnoreCase(orderLine.getMaxLineStatus())){
								String status = getATGMappedOrderLineStatus("3700");
								newOrderLine.setStatus(status);
								newOrderLine.setMaxLineStatus("3700");
							}
							i++;
						}

					}else{
						orderLine.getShipLines().add(shipmentLine);
						orderLine.setSCAC(shipmentLine.getCarrierName());
						orderLine.setCarrierServiceCode(shipmentLine.getShippingMethod());
						orderLine.setTrackingNo(shipmentLine.getTrackingNo());
						
						if("3200".equalsIgnoreCase(orderLine.getMaxLineStatus())){
							String status = getATGMappedOrderLineStatus("3700");
							orderLine.setStatus(status);
							orderLine.setMaxLineStatus("3700");
						}
					}
					
					usedShipmentLines.add(shipmentLine);
				}
			}
			orderLine.setOrderId(payload.getOrderNo());
		}
		
		if(toAdd.size() > 0){
			orderLines.addAll(toAdd);
		}
		payload.setSummaryOrderLines(orderLines);

		Element totalElement = (Element) getXPathNode(totalXPath, orderSummary);
		payload.setTotals(getTotalsFromElement(totalElement));

	}
	
	List<Container> findContainersForShipmentLine(ShipmentLine shipmentLine, List<Container> containers){
		List<Container> shipmentLineContainers = new ArrayList<>();
		for(Container container : containers){
			if(container.getOrderLineKey().equals(shipmentLine.getOrderLineKey()) 
					&& container.getShipmentLineNo().equalsIgnoreCase(shipmentLine.getShipmentLineNo())){
				shipmentLineContainers.add(container);
			}
		}
		
		return shipmentLineContainers;
	}
	
	/**
	 * This method is different from above. This will handle payload section,
	 * which will be used display what changed. In case of split quantity scenarios, for
	 * the reasons know to Yantra, instead of sending what changed, they will
	 * send all the statuses associated with quantity in the OrderLine. So we
	 * have to figure out what changed by sorting by date and taking the latest
	 * status. However, in case of Shipped items, there are can be consolidated
	 * event where they can be more than one shipped items (at different
	 * timelines with in the consolidation time period). So this method will
	 * create OrderLine (by cloning) for each status that was changed with
	 * respect to this notification event. so that the email can render by loop
	 * through the OrderLine.
	 * 
	 * @param payload
	 * @param order
	 * @throws DigitalIntegrationException
	 */
	public void populatePayload(OrderStatusPayload payload, Element order) throws DigitalIntegrationException {

		NodeList shipments = getXPathNodeList(orderShipmentXPath, order);
		for (int i = 0; i < shipments.getLength(); i++) {
			Element shipmentElement = (Element) shipments.item(i);
			String shipmentDate = shipmentElement.getAttribute("ActualShipmentDate");
			String shipmentKey = shipmentElement.getAttribute("ShipmentKey");
			String carrierName = shipmentElement.getAttribute("SCAC");
			String shipmentNo = shipmentElement.getAttribute("ShipmentNo");
		
			NodeList containerNodes = getXPathNodeList(orderContainerXPath, shipmentElement);
			List<Container> containers = getContainersFromShipment(containerNodes);
			
			ShipmentLine shipmentLine;
			NodeList shipmentLineNodes = getXPathNodeList(orderShipmentLineXPath, shipmentElement);
			for (int j = 0; j < shipmentLineNodes.getLength(); j++) {
				Element shipmentLineElement = (Element) shipmentLineNodes.item(j);

				shipmentLine = new ShipmentLine();
				shipmentLine.setItemId(shipmentLineElement.getAttribute("ItemID"));
				shipmentLine.setOrderLineKey(shipmentLineElement.getAttribute("OrderLineKey"));
				shipmentLine.setQuantity(convertDoubleQtyToInt(shipmentLineElement.getAttribute("Quantity")));
				shipmentLine.setShipmentLineNo(shipmentLineElement.getAttribute("ShipmentLineNo"));
				shipmentLine.setOrderReleaseKey(shipmentLineElement.getAttribute("OrderReleaseKey"));

				Element shipOrderLine = (Element) getXPathNode(orderShipmentOrderLineXPath, shipmentLineElement);
				shipmentLine.setOrderedQuantity(convertDoubleQtyToInt(shipOrderLine.getAttribute("OrderedQty")));
				shipmentLine.setShippingMethod(shipOrderLine.getAttribute("CarrierServiceCode"));

				NodeList orderLineNodes = getXPathNodeList(orderShipmentOrderLineXPath, shipmentLineElement);
				shipmentLine.setOrderLines(getOrderLinesFromNodeList(orderLineNodes));

				shipmentLine.setShipmentDate(shipmentDate);
				shipmentLine.setShipmentKey(shipmentKey);
				if(DigitalStringUtil.isNotBlank(carrierName)) {
					shipmentLine.setCarrierName(carrierName.trim());
				}
				shipmentLine.setShipmentNo(shipmentNo);
				
				List<Container> shipmentLineContainer = findContainersForShipmentLine(shipmentLine, containers);
				
				if(shipmentLineContainer.size() > 1){//multi-quantity item with one release and shipped with multiple containers
					shipmentLine.setContainer(shipmentLineContainer);
				}else{
					shipmentLine.setContainer(shipmentLineContainer);
					if(!shipmentLineContainer.isEmpty()){
						shipmentLine.setTrackingNo(shipmentLineContainer.get(0).getTrackingNo());					
						shipmentLine.setQuantity(shipmentLineContainer.get(0).getQuantity());
					}
					
				}
				payload.getPayloadShipmentLines().add(shipmentLine);
			}
		}
		
		NodeList summaryOrderLinesNodes = getXPathNodeList(orderOrderLineXPath, order);
		List<OrderLine> orderLines = getOrderLinesFromNodeList(summaryOrderLinesNodes);

		List<OrderLine> toRemove = new ArrayList<>();
		List<OrderLine> toAdd = new ArrayList<>();

		for (OrderLine orderLine : orderLines) {
			boolean removeThisOrderLine = false;
			List<OrderStatus> filteredOrderStatuses = filterByPayloadOrderLineStatus(orderLine, payload.getEventType(),
					payload.getPayloadShipmentLines());
			if (filteredOrderStatuses.size() > 0) {
				for (OrderStatus orderStatus : filteredOrderStatuses) {
					OrderLine newOrderLine = (OrderLine) SerializationUtils.clone(orderLine);
					List<OrderStatus> newOrderStatuses = newOrderLine.getOrderStatus();
					newOrderStatuses.clear();
					OrderStatus newOrderStatus = (OrderStatus) SerializationUtils.clone(orderStatus);
					newOrderStatuses.add(newOrderStatus);
					newOrderLine.setMaxLineStatus(newOrderStatus.getStatus());

					toAdd.add(newOrderLine);
					removeThisOrderLine = true;
				}
			} else {
				removeThisOrderLine = true;
			}

			if (removeThisOrderLine) {
				toRemove.add(orderLine);
			} else {
				orderLine.setStatus(getATGMappedOrderLineStatus(orderLine.getMaxLineStatus()));
			}
		}

		if (toRemove.size() > 0) {
			orderLines.removeAll(toRemove);
			orderLines.addAll(toAdd);
		}
		
		toAdd.clear();
		boolean personInfoMarkForSet = false;
		for (OrderLine orderLine : orderLines) {
			String orderReleaseKey = null;
			if (orderLine.getOrderStatus() != null && !orderLine.getOrderStatus().isEmpty()
					&& orderLine.getOrderStatus().get(0) != null) {
				OrderStatus os = orderLine.getOrderStatus().get(0);
				orderReleaseKey = os.getOrderReleaseKey();
				orderLine.setStatusQuantity(os.getStatusQuantity());
				if(DigitalStringUtil.isNotBlank(os.getIsSystemCancelled())){
					orderLine.setIsSystemCanceled(os.getIsSystemCancelled());
				}
				orderLine.setStatus(getATGMappedOrderLineStatus(orderLine.getMaxLineStatus()));
			}
			
			if(!personInfoMarkForSet && orderLine.getPersonInfoMarkFor() != null && 
					DigitalStringUtil.isNotBlank(orderLine.getPersonInfoMarkFor().getFirstName())){
				Person person = (Person)SerializationUtils.clone(orderLine.getPersonInfoMarkFor());
				payload.setPersonInfoMarkFor(person);
				personInfoMarkForSet = true;
			}
			
			List<ShipmentLine> usedShipmentLines = new ArrayList<>();
			
			for (ShipmentLine shipmentLine : payload.getPayloadShipmentLines()) {
				if(usedShipmentLines.contains(shipmentLine)){
					continue;
				}
				//STH_SHIPPED (status - 3700) or TRANSFER_REIEVED (BOSTS items - status can be 3200)
				if (orderLine.getKey().equalsIgnoreCase(shipmentLine.getOrderLineKey())
						&& DigitalStringUtil.isNotBlank(shipmentLine.getOrderReleaseKey())
						&& shipmentLine.getOrderReleaseKey().equalsIgnoreCase(orderReleaseKey)
						&& shipmentLine.getQuantity().equalsIgnoreCase(orderLine.getStatusQuantity())
						&&("3700".equalsIgnoreCase(orderLine.getMaxLineStatus()) || 
								"3200".equalsIgnoreCase(orderLine.getMaxLineStatus()))) { 
					
					
					if(shipmentLine.getContainers().size() > 1 ){//tricky - this is multi-quantity item with same release and shipped with multiple containers
						int i = 0;
						for(Container container : shipmentLine.getContainers()){
							OrderLine newOrderLine = orderLine;
							if(i != 0){
								newOrderLine = (OrderLine) SerializationUtils.clone(orderLine);
								toAdd.add(newOrderLine);
							}newOrderLine.setOrderId(payload.getOrderNo());
							newOrderLine.setTrackingNo(container.getTrackingNo());
							newOrderLine.setStatusQuantity(container.getQuantity());
							newOrderLine.getShipLines().add(shipmentLine);
							newOrderLine.setSCAC(shipmentLine.getCarrierName());
							newOrderLine.setCarrierServiceCode(shipmentLine.getShippingMethod());
							if (shipmentLine.getOrderLines() != null && !shipmentLine.getOrderLines().isEmpty()) {
								OrderLine shipOrderLine = shipmentLine.getOrderLines().get(0);
								newOrderLine.setRecipientEmail(shipOrderLine.getRecipientEmail());
								newOrderLine.setRecipientName(shipOrderLine.getRecipientName());
								newOrderLine.setSenderEmail(shipOrderLine.getSenderEmail());
								newOrderLine.setSenderName(shipOrderLine.getSenderName());
							}
							if("3200".equalsIgnoreCase(orderLine.getMaxLineStatus())){
								String status = getATGMappedOrderLineStatus("3700");
								newOrderLine.setStatus(status);
								newOrderLine.setMaxLineStatus("3700");
							}
							i++;
						}

					}else{
						orderLine.getShipLines().add(shipmentLine);
						
						orderLine.setSCAC(shipmentLine.getCarrierName());
						orderLine.setCarrierServiceCode(shipmentLine.getShippingMethod());
						orderLine.setTrackingNo(shipmentLine.getTrackingNo());
						
						if (shipmentLine.getOrderLines() != null && !shipmentLine.getOrderLines().isEmpty()) {
							OrderLine shipOrderLine = shipmentLine.getOrderLines().get(0);
							orderLine.setRecipientEmail(shipOrderLine.getRecipientEmail());
							orderLine.setRecipientName(shipOrderLine.getRecipientName());
							orderLine.setSenderEmail(shipOrderLine.getSenderEmail());
							orderLine.setSenderName(shipOrderLine.getSenderName());
						}
						if("3200".equalsIgnoreCase(orderLine.getMaxLineStatus())){
							String status = getATGMappedOrderLineStatus("3700");
							orderLine.setStatus(status);
							orderLine.setMaxLineStatus("3700");
						}
					}
					usedShipmentLines.add(shipmentLine);
				}
			}			
			orderLine.setOrderId(payload.getOrderNo());
		}

		
		if (orderLines.size() == 0) {
			NodeList orderLinesNodes = getXPathNodeList(orderLinesXPath, order);
			orderLines = getOrderLinesFromNodeList(orderLinesNodes);
		}else if(toAdd.size() > 0){
			orderLines.addAll(toAdd);
		}
		payload.setOrderLines(orderLines);

	}

	/**
	 *
	 * @param elements
	 * @return
	 * @throws DigitalIntegrationException
	 */
	protected List<Container> getContainersFromShipment(NodeList elements) throws DigitalIntegrationException {
		configureInternal();

		List<Container> containers = new ArrayList<>();
		
		if (elements != null) {
			for(int i=0;i<elements.getLength();i++){
				Element containerElement = (Element)elements.item(i);
				String containerNo = containerElement.getAttribute("ContainerNo");
				String trackingNo = containerElement.getAttribute("TrackingNo");
				
				NodeList containerDetails = getXPathNodeList(orderContainerDetailXPath, containerElement);
				for(int j=0; j<containerDetails.getLength();j++){
					Element containerDetail = (Element) containerDetails.item(j);
					String quantity = convertDoubleQtyToInt(containerDetail.getAttribute("Quantity"));
					Container container = new Container();
					container.setContainerNo(containerNo);
					if(DigitalStringUtil.isNotBlank(trackingNo)) {
                        container.setTrackingNo(trackingNo.trim());
                    }
					container.setQuantity(quantity);
					
					Element containerShipmentLine = (Element) getXPathNode(orderContainerShipmentLineXPath, containerDetail);
					container.setItemId(containerShipmentLine.getAttribute("ItemID"));
					container.setOrderLineKey(containerShipmentLine.getAttribute("OrderLineKey"));
					container.setShipmentLineNo(containerShipmentLine.getAttribute("ShipmentLineNo"));
					containers.add(container);
				}				
			}
		}

		return containers;
	}

	/**
	 *
	 * @param status
	 * @return
	 */
	protected String getATGMappedOrderLineStatus(String status) {
		DigitalBaseConstants dswConstants = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.DSW_BASE_CONSTANTS,
				DigitalBaseConstants.class);
		Map<String, String> orderLineStatusMap = dswConstants.getOrderLineItemStatusMap();
		return orderLineStatusMap.get(status);

	}

	/**
	 *
	 * @param orderStatus
	 * @param eventType
	 * @return
	 */
	protected boolean isValidStatusForNotificationType(OrderStatus orderStatus, String eventType) {

		boolean validStatus = false;
		DigitalBaseConstants dswConstants = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.DSW_BASE_CONSTANTS,
				DigitalBaseConstants.class);
		Map<String, String> yantraNotifcationStatusMap = dswConstants.getYantraNotificationStatusMap();
		String statusList = yantraNotifcationStatusMap.get(eventType);
		
		if(DigitalStringUtil.isNotBlank(statusList)){
			String[] statusArray = statusList.split(" ");
			
			for(String statusA: statusArray){
				if(statusA.equalsIgnoreCase(orderStatus.getStatus())){
					validStatus = true;
					break;
				}
			}
		}

		return validStatus;
	}

	protected boolean isEventTypeAutomaticTrigger(String eventType) {

		boolean isAutomatic = false;
		DigitalBaseConstants dswConstants = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.DSW_BASE_CONSTANTS,
				DigitalBaseConstants.class);
		List<String> eventTypes = dswConstants.getOrderStatusAutomaticEventTypes();
		if(eventTypes != null){
			for(String event: eventTypes){
				if(event.equalsIgnoreCase(eventType)){
					isAutomatic = true;
					break;
				}
			}
		}
		return isAutomatic;
	}
	/**
	 *
	 * @param orderLine
	 * @param eventType
	 * @param shipmentLines
	 * @return
	 */
	protected List<OrderStatus> filterByPayloadOrderLineStatus(OrderLine orderLine, String eventType,
			List<ShipmentLine> shipmentLines) {
		List<OrderStatus> filteredStatuses = new ArrayList<>();
		List<OrderStatus> orderStatuses = orderLine.getOrderStatus();

		boolean ignore1400Status = ignore1400Status(orderStatuses, Integer.parseInt(orderLine.getOrderedQty()));

		for (OrderStatus orderStatus : orderStatuses) {
			// Check to see if eventType is listed in orderStatusAutomaticEventTypes
			if(this.isEventTypeAutomaticTrigger(eventType)) {
				filteredStatuses.add(orderStatus);
			} else {
				String status = orderStatus.getStatus();
				if (DigitalStringUtil.isBlank(status) || (ignore1400Status &&
						(!isValidStatusForNotificationType(orderStatus, eventType)  && "1400".equalsIgnoreCase(status)))) {
					// 1400 status can't be ignored if the notification type is
					// Backordered
					if(DigitalStringUtil.isBlank(status)){
						logger.error("::::::: OPERATIONAL EMAIL :: Payload has orderlines with blank or missing Status attribute. OrderLine key - " + orderLine.getKey() );
					}
					continue;
				}

				if (this.isValidStatusForNotificationType(orderStatus, eventType)) {
					filteredStatuses.add(orderStatus);
				}
			}
		}


		Collections.sort(filteredStatuses, new Comparator<OrderStatus>() {
			public int compare(OrderStatus o1, OrderStatus o2) {
				String a = o1.getStatusDate();
				String b = o2.getStatusDate();
				Date o1Date = DigitalDateUtil.parseDate(a.replace(":", ""),
						YANTRA_ORDER_STATUS_DATE_TIMEZONE_FORMAT);
				Date o2Date = DigitalDateUtil.parseDate(b.replace(":", ""),
						YANTRA_ORDER_STATUS_DATE_TIMEZONE_FORMAT);
				return DigitalDateUtil.toCalendar(o2Date).compareTo(DigitalDateUtil.toCalendar(o1Date));
			}
		});

		List<OrderStatus> filteredStatusToRemove = new ArrayList<>();
		if (shipmentLines != null && shipmentLines.size() > 0
				&& ("STH_SHIPPED".equalsIgnoreCase(eventType) 
						|| "GIFTCARD_SHIPPED".equalsIgnoreCase(eventType)
							|| "CUST_PICK".equalsIgnoreCase(eventType))
							|| "TRANSFER_RECEIVED".equalsIgnoreCase(eventType)) {
			for (OrderStatus filteredStatus : filteredStatuses) {

				String orderReleaseKey = filteredStatus.getOrderReleaseKey();
				boolean foundReleaseKey = false;
				for (ShipmentLine shipmentLine : shipmentLines) {
					if (DigitalStringUtil.isNotBlank(orderReleaseKey)
							&& orderReleaseKey.equalsIgnoreCase(shipmentLine.getOrderReleaseKey()) 
							&& orderLine.getKey().equalsIgnoreCase(shipmentLine.getOrderLineKey())) {
						foundReleaseKey = true;
						break;
					}
				}

				if (!foundReleaseKey) {
					filteredStatusToRemove.add(filteredStatus);
					//filteredStatuses.remove(filteredStatus);
				}
			}
		} else {
			for (int i = 1; i < filteredStatuses.size(); i++) {
				filteredStatusToRemove.add(filteredStatuses.get(i));
			}
		}
		
		if(filteredStatusToRemove.size() > 0){
			filteredStatuses.removeAll(filteredStatusToRemove);
		}
		return filteredStatuses;
	}

	/**
	 *
	 * @param orderStatuses
	 * @param orderedQty
	 * @return
	 */
	protected boolean ignore1400Status(List<OrderStatus> orderStatuses, int orderedQty) {
		// would sum up status qty and check if sum of status qty is greater
		// than OrderedQty, then we can ignore
		// As per Yantra team; they can't remove this as it is Yarta Out of box
		// API code that generates this Notication xml
		int statusQty = 0;

		for (OrderStatus orderStatus : orderStatuses) {
			statusQty += Integer.parseInt(orderStatus.getStatusQuantity());
		}

		return (statusQty > orderedQty);
	}

	/**
	 *
	 * @param orderLines
	 * @param shipmentLines
	 */
	protected void populateOrderLineshippingInfo(List<OrderLine> orderLines, List<ShipmentLine> shipmentLines) {

		for (OrderLine orderLine : orderLines) {
			for (ShipmentLine shipmentLine : shipmentLines) {
				List<OrderLine> shipOrderLines = shipmentLine.getOrderLines();
				for (OrderLine shipOrderLine : shipOrderLines) {
					if (orderLine.getKey().equalsIgnoreCase(shipOrderLine.getKey())) {
						orderLine.getShipLines().add(shipmentLine);
					}
				}
			}
		}
	}

	/**
	 *
	 * @param payload
	 * @param orderNode
	 * @throws DigitalIntegrationException
	 */
	public void populateGeneralFields(OrderStatusPayload payload, Element orderNode) throws DigitalIntegrationException {
		configureInternal();

		if (DigitalStringUtil.isNotEmpty(orderNode.getAttribute("OrderType"))) {
			payload.setSiteId(orderNode.getAttribute("OrderType"));
		}

		if (DigitalStringUtil.isNotEmpty(orderNode.getAttribute("DocumentType"))) {
			payload.setDocumentType(orderNode.getAttribute("DocumentType"));
		}

		if (DigitalStringUtil.isNotEmpty(orderNode.getAttribute("EntryType"))) {
			payload.setEntryType(orderNode.getAttribute("EntryType"));
		}

		if (DigitalStringUtil.isNotEmpty(orderNode.getAttribute("OrderComplete"))) {
			payload.setOrderComplete(orderNode.getAttribute("OrderComplete"));
		}

		if (DigitalStringUtil.isNotEmpty(orderNode.getAttribute("OrderHeaderKey"))) {
			payload.setOrderHeaderKey(orderNode.getAttribute("OrderHeaderKey"));
		}

		if (DigitalStringUtil.isNotEmpty(orderNode.getAttribute("OrderNo"))) {
			payload.setOrderNo(orderNode.getAttribute("OrderNo"));
		}

		String dateInString = orderNode.getAttribute("OrderDate");
		try {
			if (DigitalStringUtil.isNotEmpty(dateInString)) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				Date date = formatter.parse(dateInString.replaceAll("([\\+\\-]\\d\\d):(\\d\\d)", "$1$2"));
				formatter.format(date);
				payload.setOrderDate((new SimpleDateFormat("MM/dd/yyyy").format(date)));
			}
		} catch (ParseException e) {
			payload.setOrderDate(dateInString);
		}

		if (DigitalStringUtil.isNotEmpty(orderNode.getAttribute("MaxOrderStatus"))) {
			payload.setMaxOrderStatus(orderNode.getAttribute("MaxOrderStatus"));
		}

		Element extNode = (Element) getXPathNode(extnXPath, orderNode);

		if (extNode != null) {
			payload.setLocale(extNode.getAttribute("ExtnLocale"));
		}

	}

	/**
	 *
	 * @throws DigitalIntegrationException
	 */
	private void configureInternal() throws DigitalIntegrationException {
		if (!isInitInternal) {
			try {

				statusXPath = getXpathFactory().newXPath().compile(STATUS_XPATH);
				omniXPath = getXpathFactory().newXPath().compile(OMNI);
				statusBreakupXPath = getXpathFactory().newXPath().compile(STATUS_BREAKUP_XPATH);
				statusBreakupDetailsXPath = getXpathFactory().newXPath().compile(STATUS_BREAKUP_DETAILS_XPATH);
				statusBreakupCancelXPath = getXpathFactory().newXPath().compile(STATUS_BREAKUP_CANCEL_XPATH);
				statusDetailsXPath = getXpathFactory().newXPath().compile(STATUS_DETAILS_XPATH);
				orderLinesItemXPath = getXpathFactory().newXPath().compile(ORDERLINES_ITEM_XPATH);
				linePriceInfoXPath = getXpathFactory().newXPath().compile(LINE_PRICE_INFO_XPATH);
				extnXPath = getXpathFactory().newXPath().compile(EXTN_XPATH);
				orderLinesReservationXPath = getXpathFactory().newXPath().compile(ORDERLINES_RESERVATION_XPATH);
				orderLinesTotalsXPath = getXpathFactory().newXPath().compile(ORDERLINES_TOTALS_XPATH);
				toOrderStatusXPath = getXpathFactory().newXPath().compile(TO_ORDER_STATUS_XPATH);
				fromOrderStatusXPath = getXpathFactory().newXPath().compile(FROM_ORDER_STATUS_XPATH);
				fromOrderScheduleXPath = getXpathFactory().newXPath().compile(FROM_ORDER_SCHEDULE_XPATH);
				toOrderScheduleXPath = getXpathFactory().newXPath().compile(TO_ORDER_SCHEDULE_XPATH);
				primaryInfoXPath = getXpathFactory().newXPath().compile(PRIMARY_INFO_XPATH);
				itemDetailsXPath = getXpathFactory().newXPath().compile(ITEMDETAILS_XPATH);
				billToPersonXpath = getXpathFactory().newXPath().compile(BILL_TO_PERSON);
				shipToPersonXpath = getXpathFactory().newXPath().compile(SHIP_TO_PERSON);
				totalXPath = getXpathFactory().newXPath().compile(TOTAL_XPATH);
				awardXPath = getXpathFactory().newXPath().compile(AWARD_XPATH);
				orderSummaryXPath = getXpathFactory().newXPath().compile(ORDER_SUMMARY_XPATH);
				orderSummaryShipmentXPath = getXpathFactory().newXPath().compile(ORDER_SUMMARY_SHIPMENT_XPATH);
				orderShipmentLineXPath = getXpathFactory().newXPath().compile(ORDER_SHIPMENT_LINE_XPATH);
				orderShipmentOrderLineXPath = getXpathFactory().newXPath().compile(ORDER_SHIPMENT_ORDER_LINE_XPATH);
				orderContainerXPath = getXpathFactory().newXPath().compile(ORDER_SHIPMENT_CONTAINER_XPATH);
				orderContainerDetailXPath = getXpathFactory().newXPath().compile(ORDER_SHIPMENT_CONTAINER_DETAIL_XPATH);
				orderContainerShipmentLineXPath = getXpathFactory().newXPath().compile(ORDER_SHIPMENT_CONTAINER_SHIPMENTLINE_XPATH);
				orderOrderLineXPath = getXpathFactory().newXPath().compile(ORDER_ORDER_LINE_XPATH);
				orderLinesXPath = getXpathFactory().newXPath().compile(ORDERLINES_XPATH);
				orderShipmentXPath = getXpathFactory().newXPath().compile(ORDER_SHIPMENT_XPATH);
				markForPersonXpath = getXpathFactory().newXPath().compile(MARK_FOR_PERSON);				
				paymentMethodXPath = getXpathFactory().newXPath().compile(PAYMENT_METHOD_XPATH);
				
				isInitInternal = true;
			} catch (XPathExpressionException e) {
				throw new DigitalIntegrationException("Not able to generate xpath expressions", ErrorCodes.XFORM.getCode(),
						"TRANSFORM", e);
			}
		}
	}

	/**
	 *
	 * @param orderStatusPayload
	 * @param xmlPayloadDocument
	 * @param receivedType
	 * @throws DigitalIntegrationException
	 */
	protected void populateOmniOrderUpdate(OrderStatusPayload orderStatusPayload,
										   Document xmlPayloadDocument,
										   String receivedType) throws DigitalIntegrationException {
		orderStatusPayload.setEventType(receivedType);
		orderStatusPayload.setOmniOrderUpdate(getOmniNode(xmlPayloadDocument));
	}

	/**
	 *
	 * @return
	 */
	public JAXBContextCache getJaxbContextCache() {
		return jaxbContextCache;
	}

	/**
	 *
	 * @param jaxbContextCache
	 */
	public void setJaxbContextCache(JAXBContextCache jaxbContextCache) {
		this.jaxbContextCache = jaxbContextCache;
	}

	/**
	 *
	 * @param qty
	 * @return
	 */
	protected String convertDoubleQtyToInt(String qty) {
		String quantity = "0";
		if (DigitalStringUtil.isNotEmpty(qty)) {
			quantity = new Integer(Double.valueOf(qty).intValue()).toString();
		}
		return quantity;
	}

	/**
	 * 
	 * @param payload
	 * @param inOrder
	 * @param order
	 * @throws DigitalIntegrationException
	 */
	protected void populateRefundTotals(OrderStatusPayload payload, Element inOrder, Element order)
			throws DigitalIntegrationException {
		OrderTotals totals = payload.getTotals();
		if (totals == null) {
			totals = new OrderTotals();
			payload.setTotals(totals);
		}
		double overallRefundTotal = 0.00;
		if (inOrder != null) {

			NodeList paymentMethodNodes = getXPathNodeList(paymentMethodXPath, inOrder);
			List<PaymentMethod> paymentMethods = getPaymentMethodsFromNodeList(paymentMethodNodes);

			Iterator<PaymentMethod> payMethodIterator = paymentMethods.iterator();
			while (payMethodIterator.hasNext()) {
				PaymentMethod paymentMethod = payMethodIterator.next();
				if ("CREDIT_CARD".equalsIgnoreCase(paymentMethod.getPaymentTypeGroup())
						|| "CREDIT_CARD".equalsIgnoreCase(paymentMethod.getPaymentMethodType())) {
					totals.setCcRefundAmount(
							Double.parseDouble(!DigitalStringUtil.isEmpty(paymentMethod.getTotalRefundedAmount())
									? paymentMethod.getTotalRefundedAmount() : "0.00"));
					overallRefundTotal += totals.getCcRefundAmount();
				} else if ("OTHER".equalsIgnoreCase(paymentMethod.getPaymentTypeGroup())
						|| "PAYPAL".equalsIgnoreCase(paymentMethod.getPaymentMethodType())) {
					totals.setPayPalRefundAmount(
							Double.parseDouble(!DigitalStringUtil.isEmpty(paymentMethod.getTotalRefundedAmount())
									? paymentMethod.getTotalRefundedAmount() : "0.00"));
					overallRefundTotal += totals.getPayPalRefundAmount();
				} else if ("STORED_VALUE_CARD".equalsIgnoreCase(paymentMethod.getPaymentTypeGroup())
						|| "GiftCard".equalsIgnoreCase(paymentMethod.getPaymentMethodType())) {
					totals.setGcRefundAmount(
							Double.parseDouble(!DigitalStringUtil.isEmpty(paymentMethod.getTotalAltRefundedAmount())
									? paymentMethod.getTotalAltRefundedAmount() : "0.00"));
					overallRefundTotal += totals.getGcRefundAmount();
				}
			}
		}
		
		totals.setRefundAmountTotal(overallRefundTotal);

		if (order != null) {
			Element totalElement = (Element) getXPathNode(totalXPath, order);
			OrderTotals returnOrderTotals = getTotalsFromElement(totalElement);
			totals.setReturnRefundAmount(totals.getReturnRefundAmount() + returnOrderTotals.getGrandTotal());
		}
		
	}

}
