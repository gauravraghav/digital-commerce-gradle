package com.digital.commerce.integration.order.listener;

import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.OrderStatusListener;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

public class LoggingOrderStatusListener implements OrderStatusListener {
	private static transient DigitalLogger logger = DigitalLogger.getLogger(LoggingOrderStatusListener.class);

	@Override
	public boolean doesHandleOrderStatusEvent(OrderStatusEvent orderStatusEvent) {
		return true;
	}

	@Override
	// TODO put additional nice-to-have logging here
	public void handleOrderStatusEvent(OrderStatusEvent orderStatusEvent) throws DigitalIntegrationException {
		if (logger.isDebugEnabled()) {
			logger.debug(String.format("%s handling event type %s", this.getClass().getName(),
					orderStatusEvent.getStatusType().getReceivedType()));
		}
	}

	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

}
