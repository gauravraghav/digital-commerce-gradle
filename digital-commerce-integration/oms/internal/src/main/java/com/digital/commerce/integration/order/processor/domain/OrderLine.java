package com.digital.commerce.integration.order.processor.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OrderLine implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String orderId;
	private String lineType;
	private String productTitle;
	private String key;
	private String productStyle;
	private String itemDescription;
	private String itemShortDescription;
	private String itemId;
	private String itemStyle;
	private String itemColor;
	private String itemSize;
	private String itemWidth;
	private String itemBrand;
	private String carrierServiceCode;
	private List<OrderLineReservation> orderLineReservations = new ArrayList<>();
	private String orderedQty;
	private String unitPrice;
	private String unitCost;
	private String tax;
	private String signatureRequired;
	private List<ShipmentLine> shipLines = new ArrayList<>();

	/**
	 * if fulfillmentType = BOPIS / BOSTS then the personinfoShipto will be the
	 * alternate pickup person
	 */
	private Person personInfoMarkFor;
	private Person personInfoShipTo;
	private Person personInfoBillTo;
	private String minLineStatus;
	private String maxLineStatus;
	private String status;
	private String svsCardNumber;
	private String statusQuantity;
	private List<OrderStatus> toOrderStatus = new ArrayList<>();
	private String primeLineNo;
	private String primaryInfoStatus;
	private String backorderQty;
	private String backorderFromStoreId;
	private String cancelledQty;
	private String cancelledFromStoreId;
	private String shipNode;
	private List<FromOrderReleaseStatus> fromOrderReleaseStatus = new ArrayList<>();
	private String fulfillmentType;
	private List<OrderStatus> orderStatus = new ArrayList<>();
	private String maxModifyTS;
	private String derivedFromOrderLineKey;
	private String receivingNode;
	private String receivedQty;
	private String itemKey;
	private String colorCode;
	private String itemName;
	private String lineDiscounts;
	private List<Award> awards = new ArrayList<>();
	private String awardAmount;
	private String awardType;
	private String isSystemCanceled;
	private String SCAC;
	private String trackingNo;
	private String recipientEmail;
	private String recipientName;
	private String senderEmail;
	private String senderName;

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getProductStyle() {
		return productStyle;
	}

	public void setProductStyle(String productStyle) {
		this.productStyle = productStyle;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemColor() {
		return itemColor;
	}

	public void setItemColor(String itemColor) {
		this.itemColor = itemColor;
	}

	public String getItemSize() {
		return itemSize;
	}

	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}

	public String getItemWidth() {
		return itemWidth;
	}

	public void setItemWidth(String itemWidth) {
		this.itemWidth = itemWidth;
	}

	public String getItemStyle() {
		return itemStyle;
	}

	public void setItemStyle(String itemStyle) {
		this.itemStyle = itemStyle;
	}

	public String getLineType() {
		return lineType;
	}

	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	public String getCarrierServiceCode() {
		return carrierServiceCode;
	}

	public void setCarrierServiceCode(String carrierServiceCode) {
		this.carrierServiceCode = carrierServiceCode;
	}

	public List<OrderLineReservation> getOrderLineReservations() {
		return orderLineReservations;
	}

	public void setOrderLineReservations(List<OrderLineReservation> orderLineReservations) {
		this.orderLineReservations = orderLineReservations;
	}

	public String getOrderedQty() {
		return orderedQty;
	}

	public void setOrderedQty(String orderedQty) {
		this.orderedQty = orderedQty;
	}

	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getSignatureRequired() {
		return signatureRequired;
	}

	public void setSignatureRequired(String signatureRequired) {
		this.signatureRequired = signatureRequired;
	}

	public String getMinLineStatus() {
		return minLineStatus;
	}

	public void setMinLineStatus(String minLineStatus) {
		this.minLineStatus = minLineStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSvsCardNumber() {
		return svsCardNumber;
	}

	public void setSvsCardNumber(String svsCardNumber) {
		this.svsCardNumber = svsCardNumber;
	}

	public String getItemBrand() {
		return itemBrand;
	}

	public void setItemBrand(String itemBrand) {
		this.itemBrand = itemBrand;
	}

	public String getStatusQuantity() {
		return statusQuantity;
	}

	public void setStatusQuantity(String statusQuantity) {
		this.statusQuantity = statusQuantity;
	}

	public List<OrderStatus> getToOrderStatus() {
		return toOrderStatus;
	}

	public void setToOrderStatus(List<OrderStatus> toOrderStatus) {
		this.toOrderStatus = toOrderStatus;
	}

	public String getPrimeLineNo() {
		return primeLineNo;
	}

	public void setPrimeLineNo(String primeLineNo) {
		this.primeLineNo = primeLineNo;
	}

	public String getPrimaryInfoStatus() {
		return primaryInfoStatus;
	}

	public void setPrimaryInfoStatus(String primaryInfoStatus) {
		this.primaryInfoStatus = primaryInfoStatus;
	}

	public String getBackorderQty() {
		return backorderQty;
	}

	public void setBackorderQty(String backorderQty) {
		this.backorderQty = backorderQty;
	}

	public String getBackorderFromStoreId() {
		return backorderFromStoreId;
	}

	public void setBackorderFromStoreId(String backorderFromStoreId) {
		this.backorderFromStoreId = backorderFromStoreId;
	}

	public String getItemShortDescription() {
		return itemShortDescription;
	}

	public void setItemShortDescription(String itemShortDescription) {
		this.itemShortDescription = itemShortDescription;
	}

	public String getCancelledQty() {
		return cancelledQty;
	}

	public void setCancelledQty(String cancelledQty) {
		this.cancelledQty = cancelledQty;
	}

	public String getCancelledFromStoreId() {
		return cancelledFromStoreId;
	}

	public void setCancelledFromStoreId(String cancelledFromStoreId) {
		this.cancelledFromStoreId = cancelledFromStoreId;
	}

	public String getShipNode() {
		return shipNode;
	}

	public void setShipNode(String shipNode) {
		this.shipNode = shipNode;
	}

	public List<FromOrderReleaseStatus> getFromOrderReleaseStatus() {
		return fromOrderReleaseStatus;
	}

	public void setFromOrderReleaseStatus(List<FromOrderReleaseStatus> fromOrderReleaseStatus) {
		this.fromOrderReleaseStatus = fromOrderReleaseStatus;
	}

	public Person getPersonInfoShipTo() {
		return personInfoShipTo;
	}

	public void setPersonInfoShipTo(Person personInfoShipTo) {
		this.personInfoShipTo = personInfoShipTo;
	}

	public Person getPersonInfoBillTo() {
		return personInfoBillTo;
	}

	public void setPersonInfoBillTo(Person personInfoBillTo) {
		this.personInfoBillTo = personInfoBillTo;
	}

	public String getFulfillmentType() {
		return fulfillmentType;
	}

	public void setFulfillmentType(String fulfillmentType) {
		this.fulfillmentType = fulfillmentType;
	}

	public List<OrderStatus> getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(List<OrderStatus> orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getMaxLineStatus() {
		return maxLineStatus;
	}

	public void setMaxLineStatus(String maxLineStatus) {
		this.maxLineStatus = maxLineStatus;
	}

	public String getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(String unitCost) {
		this.unitCost = unitCost;
	}

	public String getMaxModifyTS() {
		return maxModifyTS;
	}

	public void setMaxModifyTS(String maxModifyTS) {
		this.maxModifyTS = maxModifyTS;
	}

	public String getDerivedFromOrderLineKey() {
		return derivedFromOrderLineKey;
	}

	public void setDerivedFromOrderLineKey(String derivedFromOrderLineKey) {
		this.derivedFromOrderLineKey = derivedFromOrderLineKey;
	}

	public String getReceivingNode() {
		return receivingNode;
	}

	public void setReceivingNode(String receivingNode) {
		this.receivingNode = receivingNode;
	}

	public String getReceivedQty() {
		return receivedQty;
	}

	public void setReceivedQty(String receivedQty) {
		this.receivedQty = receivedQty;
	}

	public String getItemKey() {
		return itemKey;
	}

	public void setItemKey(String itemKey) {
		this.itemKey = itemKey;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getLineDiscounts() {
		return lineDiscounts;
	}

	public void setLineDiscounts(String lineDiscounts) {
		this.lineDiscounts = lineDiscounts;
	}

	public String getAwardAmount() {
		return awardAmount;
	}

	public void setAwardAmount(String awardAmount) {
		this.awardAmount = awardAmount;
	}

	public String getAwardType() {
		return awardType;
	}

	public void setAwardType(String awardType) {
		this.awardType = awardType;
	}

	public String getIsSystemCanceled() {
		return isSystemCanceled;
	}

	public void setIsSystemCanceled(String isSystemCanceled) {
		this.isSystemCanceled = isSystemCanceled;
	}

	public List<Award> getAwards() {
		return awards;
	}

	public void setAwards(List<Award> awards) {
		this.awards = awards;
	}

	public String getSCAC() {
		return SCAC;
	}

	public void setSCAC(String sCAC) {
		SCAC = sCAC;
	}

	public List<ShipmentLine> getShipLines() {
		return shipLines;
	}

	public void setShipLines(List<ShipmentLine> shipLines) {
		this.shipLines = shipLines;
	}

	public String getRecipientEmail() {
		return recipientEmail;
	}

	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}

	public String getRecipientName() {
		return recipientName;
	}

	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public Person getPersonInfoMarkFor() {
		return personInfoMarkFor;
	}

	public void setPersonInfoMarkFor(Person personInfoMarkFor) {
		this.personInfoMarkFor = personInfoMarkFor;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
