package com.digital.commerce.integration.order.processor.domain;

import java.io.Serializable;

public class OrderLineReservation implements Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String node;

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}
	
	
}
