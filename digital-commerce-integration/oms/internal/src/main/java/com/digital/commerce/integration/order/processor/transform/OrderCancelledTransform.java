package com.digital.commerce.integration.order.processor.transform;

import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.payload.OrderCancelledPayload;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

public class OrderCancelledTransform extends OrderStatusTransform {
	private static final String ORDER_XPATH = OrderStatusTransform.PREFIX+"/Order";
	private static XPathExpression orderXpath;
	private static final String BILL_TO_PERSON = "PersonInfoBillTo";
	private static XPathExpression billToPersonXpath;
	private static final String EXTN_XPATH = "Extn";
	private static XPathExpression extnXPath;
	
	private static final String TOTAL_XPATH = "OverallTotals";
	private static XPathExpression totalXPath;
	
	private static boolean isInit = false;
	
	@Override
	public OrderStatusPayload transform(Document xmlPayloadDocument, OrderStatusEvent.OrderStatusType orderStatusType) throws DigitalIntegrationException {
		OrderCancelledPayload payload = new OrderCancelledPayload();

		configure();

		payload.setEventType(orderStatusType.getReceivedType());
		
		populateOmniOrderUpdate(payload, xmlPayloadDocument, orderStatusType.getReceivedType());
		
		Element order = (Element) getXPathNode(orderXpath, xmlPayloadDocument);
		
		populateGeneralFields(payload, order);
		
		Element personInfoBillToElement = (Element) getXPathNode(billToPersonXpath, order);
		payload.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));

		populatePayload(payload, order);
		
		Element totalElement = (Element) getXPathNode(totalXPath, order);
		payload.setTotals(getTotalsFromElement(totalElement));
		
		Element orderExtnElement = (Element) getXPathNode(extnXPath, order);
		if (orderExtnElement != null) {
			payload.setLoyaltyId(orderExtnElement.getAttribute("ExtnLoyaltyId"));
			payload.setLoyaltyTier(orderExtnElement.getAttribute("ExtnLoyaltyTier"));
		}
		
		this.populateOrderSummary(payload, xmlPayloadDocument);
		
		return payload;
	}

	protected void configure() throws DigitalIntegrationException {
		if (!isInit) {
			try {
				orderXpath = getXpathFactory().newXPath().compile(ORDER_XPATH);
				billToPersonXpath = getXpathFactory().newXPath().compile(BILL_TO_PERSON);
				extnXPath = getXpathFactory().newXPath().compile(EXTN_XPATH);
				totalXPath = getXpathFactory().newXPath().compile(TOTAL_XPATH);
				
				isInit = true;
			} catch (XPathExpressionException e) {
				throw new DigitalIntegrationException("Not able to generate xpath expressions", ErrorCodes.XFORM.getCode(),
						"TRANSFORM", e);
			}
		}
	}
}
