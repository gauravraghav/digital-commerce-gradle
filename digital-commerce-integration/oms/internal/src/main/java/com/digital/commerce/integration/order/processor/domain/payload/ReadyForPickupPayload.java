package com.digital.commerce.integration.order.processor.domain.payload;

import java.util.ArrayList;
import java.util.List;

import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.Person;

public class ReadyForPickupPayload extends OrderStatusPayload {
	private Person personInfoBillTo;
	private Person personInfoShipTo;
	private Person personInfoMarkFor;
	private String organizationName;
	private String pickUpByDate;
	private List<OrderLine> orderLines = new ArrayList<>();
	private String minStdDays;
	private String shipToStore;
	private String customerEMailID;
	private String customerFirstName;
	private String customerLastName;

	public Person getPersonInfoBillTo() {
		return personInfoBillTo;
	}

	public void setPersonInfoBillTo(Person personInfoBillTo) {
		this.personInfoBillTo = personInfoBillTo;
	}

	public Person getPersonInfoShipTo() {
		return personInfoShipTo;
	}

	public void setPersonInfoShipTo(Person personInfoShipTo) {
		this.personInfoShipTo = personInfoShipTo;
	}

	public List<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public Person getPersonInfoMarkFor() {
		return personInfoMarkFor;
	}

	public void setPersonInfoMarkFor(Person personInfoMarkFor) {
		this.personInfoMarkFor = personInfoMarkFor;
	}

	public String getPickUpByDate() {
		return pickUpByDate;
	}

	public void setPickUpByDate(String pickUpByDate) {
		this.pickUpByDate = pickUpByDate;
	}

	public String getMinStdDays() {
		return minStdDays;
	}

	public void setMinStdDays(String minStdDays) {
		this.minStdDays = minStdDays;
	}

	public String getShipToStore() {
		return shipToStore;
	}

	public void setShipToStore(String shipToStore) {
		this.shipToStore = shipToStore;
	}

	public String getCustomerEMailID() {
		return customerEMailID;
	}

	public void setCustomerEMailID(String customerEMailID) {
		this.customerEMailID = customerEMailID;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

}
