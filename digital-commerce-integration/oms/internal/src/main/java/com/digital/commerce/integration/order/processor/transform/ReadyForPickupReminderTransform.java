package com.digital.commerce.integration.order.processor.transform;

import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.ShipmentLine;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.order.processor.domain.payload.ReadyForPickupPayload;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReadyForPickupReminderTransform extends OrderStatusTransform {

    private static final String ORDER_XPATH = PREFIX + "/Order";
    private static XPathExpression orderXPath;
    private static final String ORDER_SUMMARY_XPATH = "/Notification/OrderSummary/Order";
    private static XPathExpression orderSummaryXPath;
    private static final String ORDER_SUMMARY_SHIPMENT_XPATH = "/Notification/OrderSummary/Order/Shipments/Shipment";
    private static XPathExpression orderSummaryShipmentXPath;
    private static final String ORDER_SUMMARY_SHIPMENT_LINE_XPATH = "ShipmentLines/ShipmentLine";
    private static XPathExpression orderSummaryShipmentLineXPath;
    private static final String ORDER_SUMMARY_SHIPMENT_ORDER_LINE_XPATH = "OrderLine";
    private static XPathExpression orderSummaryShipmentOrderLineXPath;
    private static final String ORDER_SUMMARY_SHIPMENT_CONTAINER_XPATH = "Containers/Container";
    private static XPathExpression orderSummaryContainerXPath;
    private static final String BILL_TO_PERSON = "PersonInfoBillTo";
    private static XPathExpression billToPersonXpath;
    private static final String SHIP_TO_PERSON = "PersonInfoShipTo";
    private static XPathExpression shipToPersonXpath;
    private static final String MARK_FOR_PERSON = "PersonInfoMarkFor";
    private static XPathExpression markForPersonXpath;
    private static final String EXTN_XPATH = "Extn";
    private static XPathExpression extnXPath;
    private static final String ORDERLINES_XPATH = "OrderLines/OrderLine";
    private static XPathExpression orderLinesXPath;

    private static final String TOTAL_XPATH = "OverallTotals";

    private static boolean isInit = false;

    @Override
    public OrderStatusPayload transform(Document xmlPayloadDocument, OrderStatusEvent.OrderStatusType orderStatusType) throws DigitalIntegrationException {
        ReadyForPickupPayload payload = new ReadyForPickupPayload();

        configure();

        populateOmniOrderUpdate(payload, xmlPayloadDocument, orderStatusType.getReceivedType());

        Element order = (Element) getXPathNode(orderXPath, xmlPayloadDocument);

        populateGeneralFields(payload, order);

        Element orderLine = (Element) getXPathNode(orderLinesXPath, order);

        String dateInString = order.getAttribute("ExtnPickUpByDate");
        try {
            if (DigitalStringUtil.isNotEmpty(dateInString)) {
                SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
                Date date = formatter.parse(dateInString);
                formatter.format(date);
                payload.setPickUpByDate((new SimpleDateFormat("MM/dd/yyyy").format(date)));
            }
        } catch (ParseException e) {
            payload.setPickUpByDate(dateInString);
        }

        payload.setCustomerEMailID(order.getAttribute("CustomerEMailID"));
        payload.setCustomerFirstName(order.getAttribute("CustomerFirstName"));
        payload.setCustomerLastName(order.getAttribute("CustomerLastName"));

        Element orderSummary = (Element) getXPathNode(orderSummaryXPath, xmlPayloadDocument);

        populateGeneralFields(payload, orderSummary);

        Element orderExtnElement = (Element) getXPathNode(extnXPath, order);
        if (orderExtnElement != null) {
            payload.setLoyaltyId(orderExtnElement.getAttribute("ExtnLoyaltyId"));
            payload.setLoyaltyTier(orderExtnElement.getAttribute("ExtnLoyaltyTier"));
        }

        Element shipment = (Element) getXPathNode(orderSummaryShipmentXPath, xmlPayloadDocument);
        Element container = (Element) getXPathNode(orderSummaryContainerXPath, shipment);

        ShipmentLine shipmentLine;
        NodeList shipmentLineNodes = getXPathNodeList(orderSummaryShipmentLineXPath, shipment);
        for (int i = 0; i < shipmentLineNodes.getLength(); i++) {
            Element shipmentLineElement = (Element) shipmentLineNodes.item(i);

            shipmentLine = new ShipmentLine();
            shipmentLine.setItemId(shipmentLineElement.getAttribute("ItemID"));
            shipmentLine.setOrderLineKey(shipmentLineElement.getAttribute("OrderLineKey"));
            shipmentLine.setQuantity(shipmentLineElement.getAttribute("Quantity"));
            shipmentLine.setShipmentLineNo(shipmentLineElement.getAttribute("ShipmentLineNo"));

            NodeList orderLineNodes = getXPathNodeList(orderSummaryShipmentLineXPath, shipmentLineElement);
            shipmentLine.setOrderLines(getOrderLinesFromNodeList(orderLineNodes));

            if (container != null) {
                shipmentLine.setTrackingNo(container.getAttribute("TrackingNo"));
            }

            payload.getShipmentLines().add(shipmentLine);
        }

        NodeList summaryOrderLinesNodes = getXPathNodeList(orderSummaryShipmentOrderLineXPath, orderSummary);
        payload.setSummaryOrderLines(getOrderLinesFromNodeList(summaryOrderLinesNodes));

        Element personInfoBillToElement = (Element) getXPathNode(billToPersonXpath, orderLine);
        payload.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));

        Element personInfoShipToElement = (Element) getXPathNode(shipToPersonXpath, orderLine);
        payload.setPersonInfoShipTo(getPersonFromElement(personInfoShipToElement));

        Element personInfoMarkForElement = (Element) getXPathNode(markForPersonXpath, orderLine);
        payload.setPersonInfoMarkFor(getPersonFromElement(personInfoMarkForElement));

        populatePayload(payload, order);

        populateOrderSummary(payload, xmlPayloadDocument);

        return payload;
    }

    protected void configure() throws DigitalIntegrationException {
        if (!isInit) {
            try {
                orderXPath = getXpathFactory().newXPath().compile(ORDER_XPATH);
                orderSummaryXPath = getXpathFactory().newXPath().compile(ORDER_SUMMARY_XPATH);
                orderSummaryShipmentXPath = getXpathFactory().newXPath().compile(ORDER_SUMMARY_SHIPMENT_XPATH);
                orderSummaryShipmentLineXPath = getXpathFactory().newXPath().compile(ORDER_SUMMARY_SHIPMENT_LINE_XPATH);
                orderSummaryShipmentOrderLineXPath = getXpathFactory().newXPath()
                        .compile(ORDER_SUMMARY_SHIPMENT_ORDER_LINE_XPATH);
                orderSummaryContainerXPath = getXpathFactory().newXPath()
                        .compile(ORDER_SUMMARY_SHIPMENT_CONTAINER_XPATH);
                billToPersonXpath = getXpathFactory().newXPath().compile(BILL_TO_PERSON);
                shipToPersonXpath = getXpathFactory().newXPath().compile(SHIP_TO_PERSON);
                markForPersonXpath = getXpathFactory().newXPath().compile(MARK_FOR_PERSON);
                extnXPath = getXpathFactory().newXPath().compile(EXTN_XPATH);
                orderLinesXPath = getXpathFactory().newXPath().compile(ORDERLINES_XPATH);
                isInit = true;
            } catch (XPathExpressionException e) {
                throw new DigitalIntegrationException("Not able to generate xpath expressions", ErrorCodes.XFORM.getCode(),
                        "TRANSFORM", e);
            }
        }
    }
}
