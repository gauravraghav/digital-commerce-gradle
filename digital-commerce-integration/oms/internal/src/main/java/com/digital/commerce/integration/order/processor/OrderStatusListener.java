package com.digital.commerce.integration.order.processor;

import java.util.EventListener;

import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

public interface OrderStatusListener extends EventListener {
	public boolean doesHandleOrderStatusEvent(OrderStatusEvent orderStatusEvent);
	public void handleOrderStatusEvent(OrderStatusEvent orderStatusEvent) throws DigitalIntegrationException;
	public String getListenerName();
}
