package com.digital.commerce.integration.order.processor;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import com.digital.commerce.common.exception.DigitalRuntimeException;
import com.digital.commerce.common.logger.DigitalLogger;

import atg.dms.patchbay.MessageSink;

/**
 * @startuml
 * title __CS-14 Order Status Event processing__
 *
 * participant "OrderStatusMessageSink" as A << (C,#ADD1B2) >>
 * participant "OrderStatusProcessor" as B << (C,#ADD1B2) >>
 * participant "DSWOrderStatusListener" as C << (C,#ADD1B2) >>
 * participant "DSW<EventType>Listener" as G << (C,#ADD1B2) >>
 * participant "SMTPCommunicationService" as D << (C,#ADD1B2) >>
 * participant "SMTPCommunicationServiceSource" as E << (C,#ADD1B2) >>
 * participant "DSWJMSClient" as F << (C,#ADD1B2) >>
 *
 * [-> A: receiveMessage
 * activate A
 *
 * A -> A: getOrderStatusProcessor
 * activate A #FFBBBB
 *
 * A -> B : << processOrderStatus >>
 * note right : process order status event if the \nevent type in the request is configured.
 * activate B
 *
 * B -> B : getUnmarshalledObjectFromXMLPayload
 * activate B #FFBBBB
 *
 * B-> C : << handleOrderStatusEvent >>
 * note right : process order status event if the \nevent type listener is configured.
 * activate C #FFBBBB
 *
 * C -> C : handleOrderUpdate
 * C -> G : handleEmailNotification
 * activate G #FFBBBB
 *
 * C -> C : categorizeOrderLines
 * C -> C : removePayloadItemsFromOrderSummary
 * C -> C : handleOrderSummaryEmail
 *
 *
 * C-> D : << send >>
 * note right : send email based on the \ntemplate configured.
 * activate D #FFBBBB
 *
 * D-> E : << addToQueue >>
 * note right : send email message to \nprocess asynchronously.
 * activate E #FFBBBB
 *
 * E-> F : << sendObjectMessage >>
 * note right : publish email message to \nthe JMS queue.
 * activate F #FFBBBB
 *
 * G -[#0000FF]-> C : Process Complete
 * deactivate G
 *
 * F -[#0000FF]-> E : Process Complete
 * deactivate F
 *
 *
 * E -[#0000FF]-> D : Process Complete
 * deactivate E
 *
 * D -[#0000FF]-> C : Process Complete
 * deactivate D
 *
 * C -[#0000FF]-> B : Process Complete
 * deactivate C
 *
 * B -[#0000FF]-> A : Process Complete
 * deactivate B
 * deactivate A
 *
 * [<- A: Ack
 * deactivate A
 *
 * @enduml
 */
public class OrderStatusMessageSink implements MessageSink {
	private static transient DigitalLogger logger = DigitalLogger.getLogger(OrderStatusMessageSink.class);

	private OrderStatusProcessor orderStatusProcessor;

	@Override
	public void receiveMessage(String s, Message message) throws JMSException {
		String orderStatusPayload = ((TextMessage)message).getText();
		logger.debug("CS14 message -------> " + orderStatusPayload);
		try {
			getOrderStatusProcessor().processOrderStatus(orderStatusPayload);
		} catch (Throwable e) {
			logger.error("Error processing order status change - exception in handler", e);			
			throw new DigitalRuntimeException("Error processing order status change - exception in handler ", e);
		}
	}

	public OrderStatusProcessor getOrderStatusProcessor() {
		return orderStatusProcessor;
	}

	public void setOrderStatusProcessor(OrderStatusProcessor orderStatusProcessor) {
		this.orderStatusProcessor = orderStatusProcessor;
	}
	
	
	
}
