package com.digital.commerce.integration.order.processor.domain;

import java.io.Serializable;

public class Container implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String quantity;
	private String containerNo;
	private String trackingNo;
	private String itemId;
	private String orderLineKey;
	private String shipmentLineNo;
	
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	
	public String getContainerNo() {
		return containerNo;
	}
	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}
	public String getTrackingNo() {
		return trackingNo;
	}
	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getShipmentLineNo() {
		return shipmentLineNo;
	}
	public void setShipmentLineNo(String shipmentLineNo) {
		this.shipmentLineNo = shipmentLineNo;
	}
	public String getOrderLineKey() {
		return orderLineKey;
	}
	public void setOrderLineKey(String orderLineKey) {
		this.orderLineKey = orderLineKey;
	}
}
