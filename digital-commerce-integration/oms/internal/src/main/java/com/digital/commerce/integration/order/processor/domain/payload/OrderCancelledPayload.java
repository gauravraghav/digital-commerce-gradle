package com.digital.commerce.integration.order.processor.domain.payload;

import com.digital.commerce.integration.order.processor.domain.Person;

public class OrderCancelledPayload extends OrderStatusPayload {

	private Person personInfoBillTo = new Person();
	/*private List<OrderLine> orderLines = new ArrayList<>();
	private List<OrderLine> summaryOrderLines = new ArrayList<>();*/

	public Person getPersonInfoBillTo() {
		return personInfoBillTo;
	}

	public void setPersonInfoBillTo(Person personInfoBillTo) {
		this.personInfoBillTo = personInfoBillTo;
	}

/*	public List<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}

	public List<OrderLine> getSummaryOrderLines() {
		return summaryOrderLines;
	}

	public void setSummaryOrderLines(List<OrderLine> summaryOrderLines) {
		this.summaryOrderLines = summaryOrderLines;
	}*/
}
