package com.digital.commerce.integration.order.processor.transform;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.order.processor.domain.payload.ShipmentConfirmedPayload;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

public class ShipmentConfirmedTransform extends OrderStatusTransform {
	private static final String PREFIX = "/Notification/Payload";
	private static final String ORDER_XPATH = PREFIX + "/Order";
	private static XPathExpression orderXPath;
	private static final String SHIPMENT_XPATH = "Shipments/Shipment";
	private static XPathExpression shipmentXPath;
	private static final String BILL_TO_PERSON_XPATH = "PersonInfoBillTo";
	private static XPathExpression billToPersonXpath;
	private static final String SHIP_TO_PERSON_XPATH = "PersonInfoShipTo";
	private static XPathExpression shipToPersonXpath;
	private static final String PAYMENT_METHOD_XPATH = "PaymentMethods/PaymentMethod";
	private static XPathExpression paymentMethodXPath;
	private static final String ADDRESS_LINES_XPATH = "DistinctAddresses/Line";
	private static XPathExpression addressLinesXPath;
	private static final String EXTN_XPATH = "Extn";
	private static XPathExpression extnXPath;

	private static final String TOTAL_XPATH = "OverallTotals";
	private static XPathExpression totalXPath;

	private static boolean isInit = false;

	@Override
	public OrderStatusPayload transform(Document xmlPayloadDocument, OrderStatusEvent.OrderStatusType orderStatusType) throws DigitalIntegrationException {
		ShipmentConfirmedPayload payload = new ShipmentConfirmedPayload();

		configure();

		populateOmniOrderUpdate(payload, xmlPayloadDocument, orderStatusType.getReceivedType());

		Element order = (Element) getXPathNode(orderXPath, xmlPayloadDocument);

		populateGeneralFields(payload, order);

		payload.setMinOrderStatus(order.getAttribute("MinOrderStatus"));
		payload.setHasGiftCard(order.getAttribute("HasGC"));
		payload.setHasReg(order.getAttribute("HasReg"));

		Element extnLoyalty = (Element) getXPathNode(extnXPath, order);
		payload.setLoyaltyId(extnLoyalty.getAttribute("ExtnLoyaltyId"));
		payload.setLoyaltyTier(extnLoyalty.getAttribute("ExtnLoyaltyTier"));
		payload.setIsOrderWhiteGlove(extnLoyalty.getAttribute("ExtnIsOrderWhiteGlove"));

		Element shipment = (Element) getXPathNode(shipmentXPath, order);
		if(shipment != null){
			payload.setActualShipmentDate(shipment.getAttribute("ActualShipmentDate"));
			String carrierName = shipment.getAttribute("SCAC");
			if(DigitalStringUtil.isNotBlank(carrierName)) {
				payload.setCarrier(carrierName.trim());
			}
		}

		String dateInString = order.getAttribute("ActualShipmentDate");
		try {
			if (DigitalStringUtil.isNotEmpty(dateInString)) {
				SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
				Date date = formatter.parse(dateInString);
				formatter.format(date);
				payload.setActualShipmentDate((new SimpleDateFormat("MM/dd/yyyy").format(date)));
			}
		} catch (ParseException e) {
			payload.setActualShipmentDate(dateInString);
		}
		
		payload.setCustomerEMailID(order.getAttribute("CustomerEMailID"));
		payload.setCustomerFirstName(order.getAttribute("CustomerFirstName"));
		payload.setCustomerLastName(order.getAttribute("CustomerLastName"));

		populatePayload(payload, order);

		this.populateOrderSummary(payload, xmlPayloadDocument);

		Element personInfoShipToElement = (Element) getXPathNode(shipToPersonXpath, order);
		payload.setPersonInfoShipTo(getPersonFromElement(personInfoShipToElement));

		Element personInfoBillToElement = (Element) getXPathNode(billToPersonXpath, order);
		payload.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));

		NodeList paymentMethodNodes = getXPathNodeList(paymentMethodXPath, order);
		payload.setPaymentMethods(getPaymentMethodsFromNodeList(paymentMethodNodes));

		Element totalElement = (Element) getXPathNode(totalXPath, order);
		payload.setTotals(getTotalsFromElement(totalElement));

		NodeList addressLinePersonInfoShipToNodes = getXPathNodeList(addressLinesXPath, order);
		for (int i = 0; i < addressLinePersonInfoShipToNodes.getLength(); i++) {
			Element addressLinePersonInfoShipToElement = (Element) addressLinePersonInfoShipToNodes.item(i);

			if (addressLinePersonInfoShipToElement != null) {
				Element stpiElement = (Element) getXPathNode(shipToPersonXpath, addressLinePersonInfoShipToElement);

				payload.getAddressLinesPersonInfoShipTo().add(getPersonFromElement(stpiElement));
			}
		}

		return payload;
	}

	protected void configure() throws DigitalIntegrationException {
		if (!isInit) {
			try {
				orderXPath = getXpathFactory().newXPath().compile(ORDER_XPATH);
				shipmentXPath = getXpathFactory().newXPath().compile(SHIPMENT_XPATH);
				billToPersonXpath = getXpathFactory().newXPath().compile(BILL_TO_PERSON_XPATH);
				shipToPersonXpath = getXpathFactory().newXPath().compile(SHIP_TO_PERSON_XPATH);
				paymentMethodXPath = getXpathFactory().newXPath().compile(PAYMENT_METHOD_XPATH);
				addressLinesXPath = getXpathFactory().newXPath().compile(ADDRESS_LINES_XPATH);
				extnXPath = getXpathFactory().newXPath().compile(EXTN_XPATH);

				totalXPath = getXpathFactory().newXPath().compile(TOTAL_XPATH);

				isInit = true;
			} catch (XPathExpressionException e) {
				throw new DigitalIntegrationException("Not able to generate xpath expressions", ErrorCodes.XFORM.getCode(),
						"TRANSFORM", e);
			}
		}
	}
}
