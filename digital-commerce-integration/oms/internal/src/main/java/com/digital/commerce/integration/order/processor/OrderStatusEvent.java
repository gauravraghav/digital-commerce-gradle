package com.digital.commerce.integration.order.processor;

public class OrderStatusEvent {

    public static class OrderStatusType {

        String receivedType;
        String templateKey;

        /**
         * @param receivedType
         * @param templateKey
         */
        public OrderStatusType(String receivedType, String templateKey) {
            this.receivedType = receivedType;
            this.templateKey = templateKey;
        }

        /**
         * @return String
         */
        public String getReceivedType() {
            return receivedType;
        }

        /**
         * @param receivedType
         */
        public void setReceivedType(String receivedType) {
            this.receivedType = receivedType;
        }

        /**
         * @return
         */
        public String getTemplateKey() {
            return templateKey;
        }

        /**
         * @param templateKey
         */
        public void setTemplateKey(String templateKey) {
            this.templateKey = templateKey;
        }
    }

    private OrderStatusType statusType;
    private Object payload;

    /**
     * @param statusType
     */
    public OrderStatusEvent(OrderStatusType statusType) {
        this.statusType = statusType;
    }

    /**
     * @return OrderStatusType
     */
    public OrderStatusType getStatusType() {
        return statusType;
    }

    /**
     * @param statusType
     */
    public void setStatusType(OrderStatusType statusType) {
        this.statusType = statusType;
    }

    /**
     * @return Object
     */
    public Object getPayload() {
        return payload;
    }

    /**
     * @param payload
     */
    public void setPayload(Object payload) {
        this.payload = payload;
    }

}
