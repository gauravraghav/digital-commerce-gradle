package com.digital.commerce.integration.order.processor.transform;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.order.processor.domain.payload.ShipmentSLAChangePayload;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

public class ShipmentSLAChangeTransform extends OrderStatusTransform {
	private static final String PREFIX = "/Notification/Payload";
	private static final String ORDER_RELEASE_XPATH = PREFIX + "/OrderRelease";
	private static XPathExpression orderReleaseXPath;
	private static final String ORDER_SUMMARY_XPATH = "/Notification/OrderSummary";
	private static final String ORDER_XPATH = "Order";
	private static XPathExpression orderXPath;
	private static final String EXTN_XPATH = "Extn";
	private static XPathExpression extnXPath;
	private static final String ORDER_LINE_XPATH = "OrderLines/OrderLine";
	private static XPathExpression orderLineXPath;
	private static final String BILL_TO_PERSON_XPATH = "PersonInfoBillTo";
	private static XPathExpression billToPersonXpath;
	private static final String SHIP_TO_PERSON_XPATH = "PersonInfoShipTo";
	private static XPathExpression shipToPersonXpath;
	private static final String PAYMENT_METHOD_XPATH = "PaymentMethods/PaymentMethod";
	private static XPathExpression paymentMethodXPath;
	
	private static final String TOTAL_XPATH = "OverallTotals";
	
	private static boolean isInit = false;
	@Override
	public OrderStatusPayload transform(Document xmlPayloadDocument, OrderStatusEvent.OrderStatusType orderStatusType) throws DigitalIntegrationException {
		ShipmentSLAChangePayload payload = new ShipmentSLAChangePayload();
		
		configure();
		
		populateOmniOrderUpdate(payload, xmlPayloadDocument, orderStatusType.getReceivedType());
		
		Element orderRelease = (Element) getXPathNode(orderReleaseXPath, xmlPayloadDocument);
		
		payload.setCarrierServiceCode(orderRelease.getAttribute("CarrierServiceCode"));
		payload.setCustomerEmailId(orderRelease.getAttribute("CustomerEMailID"));
		payload.setOrderDate(orderRelease.getAttribute("OrderDate"));
		payload.setSalesOrderNo(orderRelease.getAttribute("SalesOrderNo"));
		payload.setShipNode(orderRelease.getAttribute("ShipNode"));
		payload.setIsChangeWholeOrder(orderRelease.getAttribute("IsLOSChangeForWholeOrder"));
		
		Element order = (Element) getXPathNode(orderXPath, orderRelease);
		populatePayload(payload, orderRelease);
		
		populateGeneralFields(payload, order);
		
		Element extnLoyalty = (Element) getXPathNode(extnXPath, order);
		payload.setLoyaltyId(extnLoyalty.getAttribute("ExtnLoyaltyId"));
		payload.setLoyaltyTier(extnLoyalty.getAttribute("ExtnLoyaltyTier"));
		payload.setIsOrderWhiteGlove(extnLoyalty.getAttribute("ExtnIsOrderWhiteGlove"));
		
		NodeList releaseOrderLineNodes = getXPathNodeList(orderLineXPath, orderRelease);
		payload.setChangedOrderLines(getOrderLinesFromNodeList(releaseOrderLineNodes));
		
		Element personInfoShipToElement = (Element) getXPathNode(shipToPersonXpath, orderRelease);		
		payload.setPersonInfoShipTo(getPersonFromElement(personInfoShipToElement));
		
		Element personInfoBillToElement = (Element) getXPathNode(billToPersonXpath, order);		
		payload.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));
				
		NodeList paymentMethodNodes = getXPathNodeList(paymentMethodXPath, order);
		payload.setPaymentMethods(getPaymentMethodsFromNodeList(paymentMethodNodes));
		
		this.populateOrderSummary(payload, xmlPayloadDocument);
		
		return payload;
	}

	protected void configure() throws DigitalIntegrationException {
		if (!isInit) {
			try {
				orderXPath = getXpathFactory().newXPath().compile(ORDER_XPATH);
				orderReleaseXPath = getXpathFactory().newXPath().compile(ORDER_RELEASE_XPATH);
				extnXPath = getXpathFactory().newXPath().compile(EXTN_XPATH);
				orderLineXPath = getXpathFactory().newXPath().compile(ORDER_LINE_XPATH);				
				billToPersonXpath = getXpathFactory().newXPath().compile(BILL_TO_PERSON_XPATH);
				shipToPersonXpath = getXpathFactory().newXPath().compile(SHIP_TO_PERSON_XPATH);
				paymentMethodXPath = getXpathFactory().newXPath().compile(PAYMENT_METHOD_XPATH);
				isInit = true;
			} catch (XPathExpressionException e) {
				throw new DigitalIntegrationException("Not able to generate xpath expressions", ErrorCodes.XFORM.getCode(),
						"TRANSFORM", e);
			}
		}
	}
	
}
