package com.digital.commerce.integration.order.processor.domain.payload;

import java.util.ArrayList;
import java.util.List;

import com.digital.commerce.integration.order.processor.domain.Container;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.PaymentMethod;
import com.digital.commerce.integration.order.processor.domain.Person;
import com.digital.commerce.integration.order.processor.domain.ShipmentLine;

public class ShippingForBostsPayload extends OrderStatusPayload {
	private String actualShipmentDate;
	private Person personInfoBillTo;
	private Person personInfoShipTo;
	private Person personInfoMarkFor;
	private String minOrderStatus;
	private String hasGiftCard;
	private String hasReg;
	private List<PaymentMethod> paymentMethods = new ArrayList<>();
	private List<Person> addressLinesPersonInfoShipTo = new ArrayList<>();
	private List<ShipmentLine> shipmentLines = new ArrayList<>();
	private String carrier;
	private List<Container> containers = new ArrayList<>();
	private List<OrderLine> orderLines = new ArrayList<>();
	private String isOrderWhiteGlove;
	private String customerEMailID;
	private String customerFirstName;
	private String customerLastName;

	public String getCustomerEMailID() {
		return customerEMailID;
	}

	public void setCustomerEMailID(String customerEMailID) {
		this.customerEMailID = customerEMailID;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	public String getActualShipmentDate() {
		return actualShipmentDate;
	}

	public void setActualShipmentDate(String actualShipmentDate) {
		this.actualShipmentDate = actualShipmentDate;
	}

	public Person getPersonInfoBillTo() {
		return personInfoBillTo;
	}

	public void setPersonInfoBillTo(Person personInfoBillTo) {
		this.personInfoBillTo = personInfoBillTo;
	}

	public String getMinOrderStatus() {
		return minOrderStatus;
	}

	public void setMinOrderStatus(String minOrderStatus) {
		this.minOrderStatus = minOrderStatus;
	}

	public String getHasGiftCard() {
		return hasGiftCard;
	}

	public void setHasGiftCard(String hasGiftCard) {
		this.hasGiftCard = hasGiftCard;
	}

	public String getHasReg() {
		return hasReg;
	}

	public void setHasReg(String hasReg) {
		this.hasReg = hasReg;
	}

	public List<PaymentMethod> getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public Person getPersonInfoShipTo() {
		return personInfoShipTo;
	}

	public void setPersonInfoShipTo(Person personInfoShipTo) {
		this.personInfoShipTo = personInfoShipTo;
	}

	public Person getPersonInfoMarkFor() {
		return personInfoMarkFor;
	}

	public void setPersonInfoMarkFor(Person personInfoMarkFor) {
		this.personInfoMarkFor = personInfoMarkFor;
	}

	public List<Person> getAddressLinesPersonInfoShipTo() {
		return addressLinesPersonInfoShipTo;
	}

	public void setAddressLinesPersonInfoShipTo(List<Person> addressLinesPersonInfoShipTo) {
		this.addressLinesPersonInfoShipTo = addressLinesPersonInfoShipTo;
	}

	public List<ShipmentLine> getShipmentLines() {
		return shipmentLines;
	}

	public void setShipmentLines(List<ShipmentLine> shipmentLines) {
		this.shipmentLines = shipmentLines;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public List<Container> getContainers() {
		return containers;
	}

	public void setContainers(List<Container> containers) {
		this.containers = containers;
	}

	public List<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}

	public String getIsOrderWhiteGlove() {
		return isOrderWhiteGlove;
	}

	public void setIsOrderWhiteGlove(String isOrderWhiteGlove) {
		this.isOrderWhiteGlove = isOrderWhiteGlove;
	}

}
