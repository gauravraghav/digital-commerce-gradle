package com.digital.commerce.integration.order.processor.domain.payload;

import java.util.ArrayList;
import java.util.List;

import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.Person;

public class CustomerPickupConfirmedPayload extends OrderStatusPayload {
	private Person personInfoBillTo = new Person();
	private Person personInfoShipTo = new Person();
	private String organizationName;
	private String mallPlazaName;
	private List<OrderLine> orderLines = new ArrayList<>();
	private String customerEmailId;
	private String customerFirstName;
	private String customerLastName;

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	public String getCustomerEmailId() {
		return customerEmailId;
	}

	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}

	public String getMallPlazaName() {
		return mallPlazaName;
	}

	public void setMallPlazaName(String mallPlazaName) {
		this.mallPlazaName = mallPlazaName;
	}

	public Person getPersonInfoBillTo() {
		return personInfoBillTo;
	}

	public void setPersonInfoBillTo(Person personInfoBillTo) {
		this.personInfoBillTo = personInfoBillTo;
	}

	public Person getPersonInfoShipTo() {
		return personInfoShipTo;
	}

	public void setPersonInfoShipTo(Person personInfoShipTo) {
		this.personInfoShipTo = personInfoShipTo;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public List<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}
	
	
}
