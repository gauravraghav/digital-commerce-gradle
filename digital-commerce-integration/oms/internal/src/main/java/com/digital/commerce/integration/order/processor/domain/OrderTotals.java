package com.digital.commerce.integration.order.processor.domain;

public class OrderTotals {
	private double grandCharges = 0.00;

	private double grandDiscount = 0.00;
	private double grandTax = 0.00;
	private double grandTotal = 0.00;
	private double hdrCharges = 0.00;
	private double hdrDiscount = 0.00;
	private double hdrTax = 0.00;
	private double hdrTotal = 0.00;
	private double lineSubTotal = 0.00;
	private double grandShippingTotal = 0.00;
	private double refundAmountTotal = 0.00;
	private double gcRefundAmount = 0.00;
	private double ccRefundAmount = 0.00;
	private double payPalRefundAmount = 0.00;
	private double returnRefundAmount = 0.00;	
	
	public double getReturnRefundAmount() {
		return returnRefundAmount;
	}

	public void setReturnRefundAmount(double returnRefundAmount) {
		this.returnRefundAmount = returnRefundAmount;
	}

	public double getGrandShippingTotal() {
		return grandShippingTotal;
	}

	public void setGrandShippingTotal(double grandShippingTotal) {
		this.grandShippingTotal = grandShippingTotal;
	}

	public double getGrandCharges() {
		return grandCharges;
	}

	public void setGrandCharges(double grandCharges) {
		this.grandCharges = grandCharges;
	}

	public double getGrandDiscount() {
		return grandDiscount;
	}

	public void setGrandDiscount(double grandDiscount) {
		this.grandDiscount = grandDiscount;
	}

	public double getGrandTax() {
		return grandTax;
	}

	public void setGrandTax(double grandTax) {
		this.grandTax = grandTax;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public double getHdrCharges() {
		return hdrCharges;
	}

	public void setHdrCharges(double hdrCharges) {
		this.hdrCharges = hdrCharges;
	}

	public double getHdrDiscount() {
		return hdrDiscount;
	}

	public void setHdrDiscount(double hdrDiscount) {
		this.hdrDiscount = hdrDiscount;
	}

	public double getHdrTax() {
		return hdrTax;
	}

	public void setHdrTax(double hdrTax) {
		this.hdrTax = hdrTax;
	}

	public double getHdrTotal() {
		return hdrTotal;
	}

	public void setHdrTotal(double hdrTotal) {
		this.hdrTotal = hdrTotal;
	}

	public double getLineSubTotal() {
		return lineSubTotal;
	}

	public void setLineSubTotal(double lineSubTotal) {
		this.lineSubTotal = lineSubTotal;
	}

	public double getRefundAmountTotal() {
		return refundAmountTotal;
	}

	public void setRefundAmountTotal(double refundAmountTotal) {
		this.refundAmountTotal = refundAmountTotal;
	}

	public double getGcRefundAmount() {
		return gcRefundAmount;
	}

	public void setGcRefundAmount(double gcRefundAmount) {
		this.gcRefundAmount = gcRefundAmount;
	}

	public double getCcRefundAmount() {
		return ccRefundAmount;
	}

	public void setCcRefundAmount(double ccRefundAmount) {
		this.ccRefundAmount = ccRefundAmount;
	}

	public double getPayPalRefundAmount() {
		return payPalRefundAmount;
	}

	public void setPayPalRefundAmount(double payPalRefundAmount) {
		this.payPalRefundAmount = payPalRefundAmount;
	}
}
