package com.digital.commerce.integration.order.processor.domain.payload;

import java.util.ArrayList;
import java.util.List;

import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.PaymentMethod;
import com.digital.commerce.integration.order.processor.domain.Person;

public class OrderConfirmedPayload extends OrderStatusPayload {

	private String merchTotal;
	private String shipping;
	private String multiNode;
	private String grandDiscount;
	private String grandTotal;
	private String customerEMailID;
	private String customerFirstName;
	private String customerLastName;

	private List<OrderLine> orderLines = new ArrayList<>();
	private List<PaymentMethod> paymentMethods = new ArrayList<>();
	private Person personInfoBillTo = new Person();
	private Person personInfoShipTo = new Person();

	private String originalTax;

	public String getMerchTotal() {
		return merchTotal;
	}

	public void setMerchTotal(String merchTotal) {
		this.merchTotal = merchTotal;
	}

	public String getShipping() {
		return shipping;
	}

	public void setShipping(String shipping) {
		this.shipping = shipping;
	}

	public String getMultiNode() {
		return multiNode;
	}

	public void setMultiNode(String multiNode) {
		this.multiNode = multiNode;
	}

	public String getGrandDiscount() {
		return grandDiscount;
	}

	public void setGrandDiscount(String grandDiscount) {
		this.grandDiscount = grandDiscount;
	}

	public String getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}

	public String getCustomerEMailID() {
		return customerEMailID;
	}

	public void setCustomerEMailID(String customerEMailID) {
		this.customerEMailID = customerEMailID;
	}

	public List<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}

	public Person getPersonInfoBillTo() {
		return personInfoBillTo;
	}

	public void setPersonInfoBillTo(Person personInfoBillTo) {
		this.personInfoBillTo = personInfoBillTo;
	}

	public List<PaymentMethod> getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public Person getPersonInfoShipTo() {
		return personInfoShipTo;
	}

	public void setPersonInfoShipTo(Person personInfoShipTo) {
		this.personInfoShipTo = personInfoShipTo;
	}

	public String getOriginalTax() {
		return originalTax;
	}

	public void setOriginalTax(String originalTax) {
		this.originalTax = originalTax;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

}
