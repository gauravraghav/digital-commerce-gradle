package com.digital.commerce.integration.order.processor.transform;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.payload.OrderConfirmedPayload;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

public class OrderNotShippedTransform extends OrderStatusTransform {
	private static final String PREFIX = "/Notification/Payload";
	private static final String MERCHTOTAL_XPATH = PREFIX + "/OrderList/OrderTotal/@MerchTotal";
	private static XPathExpression merchTotalXPath;
	private static final String ORDER_XPATH = PREFIX + "/OrderList/Order";
	private static XPathExpression orderXPath;
	private static final String ORDER_SUMMARY_XPATH = "/Notification/OrderSummary/Order";
	private static XPathExpression orderSummaryXPath;
	private static final String GRANDCHARGES_XPATH = "OverallTotals/@GrandCharges";
	private static XPathExpression grandChargesXPath;
	private static final String MULTISHIPMENT_XPATH = PREFIX + "/OrderList/OrderTotal/@MultiNode";
	private static XPathExpression multishipmentXPath;
	private static final String DISCOUNTS_XPATH = "OverallTotals/@GrandDiscount";
	private static XPathExpression discountsXPath;
	private static final String GRANDTOTAL_XPATH = "OverallTotals/@GrandTotal";
	private static XPathExpression grandTotalXPath;
	private static final String BILL_TO_PERSON = "PersonInfoBillTo";
	private static XPathExpression billToPersonXpath;
	private static final String SHIP_TO_PERSON = "PersonInfoShipTo";
	private static XPathExpression shipToPersonXpath;
	private static final String PAYMENT_METHOD_XPATH = "PaymentMethods/PaymentMethod";
	private static XPathExpression paymentMethodXPath;
	private static final String RELATIVE_EXTN_XPATH = "Extn";
	private static XPathExpression relativeLinesExtnXPath;

	private static final String TOTAL_XPATH = "OverallTotals";
	private static XPathExpression totalXPath;
	private static final String AWARD_XPATH = "Awards/Award";
	private static XPathExpression awardXPath;

	private static boolean isInit = false;

	@Override
	public OrderStatusPayload transform(Document xmlPayloadDocument, OrderStatusEvent.OrderStatusType orderStatusType) throws DigitalIntegrationException {
		OrderConfirmedPayload payload = new OrderConfirmedPayload();

		configure();

		populateOmniOrderUpdate(payload, xmlPayloadDocument, orderStatusType.getReceivedType());

		Element order = (Element) getXPathNode(orderXPath, xmlPayloadDocument);

		populateGeneralFields(payload, order);

		Element orderSummary = (Element) getXPathNode(orderSummaryXPath, xmlPayloadDocument);

		populateGeneralFields(payload, orderSummary);

		payload.setMerchTotal(getXPathString(merchTotalXPath, xmlPayloadDocument));
		payload.setShipping(getXPathString(grandChargesXPath, order));
		payload.setMultiNode(getXPathString(multishipmentXPath, xmlPayloadDocument));
		payload.setGrandDiscount(getXPathString(discountsXPath, order));
		payload.setGrandTotal(getXPathString(grandTotalXPath, order));
		payload.setCustomerEMailID(order.getAttribute("CustomerEMailID"));
		payload.setCustomerFirstName(order.getAttribute("CustomerFirstName"));
		payload.setCustomerLastName(order.getAttribute("CustomerLastName"));
		payload.setOriginalTax(order.getAttribute("OriginalTax"));

		Element orderExtnElement = (Element) getXPathNode(relativeLinesExtnXPath, order);
		if (orderExtnElement != null) {
			payload.setLoyaltyId(orderExtnElement.getAttribute("ExtnLoyaltyId"));
			payload.setLoyaltyTier(orderExtnElement.getAttribute("ExtnLoyaltyTier"));
		}

		Element personInfoShipToElement = (Element) getXPathNode(shipToPersonXpath, order);
		payload.setPersonInfoShipTo(getPersonFromElement(personInfoShipToElement));

		Element personInfoBillToElement = (Element) getXPathNode(billToPersonXpath, order);
		payload.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));

		NodeList paymentMethodNodes = getXPathNodeList(paymentMethodXPath, order);
		payload.setPaymentMethods(getPaymentMethodsFromNodeList(paymentMethodNodes));

		Element totalElement = (Element) getXPathNode(totalXPath, order);
		payload.setTotals(getTotalsFromElement(totalElement));

		NodeList awardNodes = getXPathNodeList(awardXPath, order);
		payload.setAwards(getAwardsFromElement(awardNodes));

		populatePayload(payload, order);

		return payload;

	}

	protected void configure() throws DigitalIntegrationException {
		if (!isInit) {
			try {
				orderXPath = getXpathFactory().newXPath().compile(ORDER_XPATH);
				orderSummaryXPath = getXpathFactory().newXPath().compile(ORDER_SUMMARY_XPATH);
				merchTotalXPath = getXpathFactory().newXPath().compile(MERCHTOTAL_XPATH);
				grandChargesXPath = getXpathFactory().newXPath().compile(GRANDCHARGES_XPATH);
				multishipmentXPath = getXpathFactory().newXPath().compile(MULTISHIPMENT_XPATH);
				discountsXPath = getXpathFactory().newXPath().compile(DISCOUNTS_XPATH);
				grandTotalXPath = getXpathFactory().newXPath().compile(GRANDTOTAL_XPATH);
				billToPersonXpath = getXpathFactory().newXPath().compile(BILL_TO_PERSON);
				shipToPersonXpath = getXpathFactory().newXPath().compile(SHIP_TO_PERSON);
				relativeLinesExtnXPath = getXpathFactory().newXPath().compile(RELATIVE_EXTN_XPATH);
				paymentMethodXPath = getXpathFactory().newXPath().compile(PAYMENT_METHOD_XPATH);

				totalXPath = getXpathFactory().newXPath().compile(TOTAL_XPATH);
				awardXPath = getXpathFactory().newXPath().compile(AWARD_XPATH);

				isInit = true;
			} catch (XPathExpressionException e) {
				throw new DigitalIntegrationException("Not able to generate xpath expressions", ErrorCodes.XFORM.getCode(),
						"TRANSFORM", e);
			}
		}
	}

}
