package com.digital.commerce.integration.order.processor.domain.payload;

import java.util.ArrayList;
import java.util.List;

import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.PaymentMethod;
import com.digital.commerce.integration.order.processor.domain.Person;

public class ReturnReceivedShipmentPayload extends OrderStatusPayload {
	private String orderDate;
	private String orderNo;
	private Person personInfoBillTo;
	private Person personInfoShipTo;
	private Person personInfoMarkFor;
	private String customerFirstName;
	private String customerLastName;
	private List<PaymentMethod> paymentMethods = new ArrayList<>();
	private List<OrderLine> orderLines = new ArrayList<>();
	private List<OrderLine> changedOrderLines = new ArrayList<>();
	private List<OrderLine> returnOrderLines = new ArrayList<>();
	private String refundedTaxes;
	private String customerEMailID;
	
	
	public List<OrderLine> getChangedOrderLines() {
		return changedOrderLines;
	}
	public void setChangedOrderLines(List<OrderLine> changedOrderLines) {
		this.changedOrderLines = changedOrderLines;
	}
	public String getCustomerLastName() {
		return customerLastName;
	}
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}
	public Person getPersonInfoMarkFor() {
		return personInfoMarkFor;
	}
	public void setPersonInfoMarkFor(Person personInfoMarkFor) {
		this.personInfoMarkFor = personInfoMarkFor;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public Person getPersonInfoBillTo() {
		return personInfoBillTo;
	}
	public void setPersonInfoBillTo(Person personInfoBillTo) {
		this.personInfoBillTo = personInfoBillTo;
	}
	public Person getPersonInfoShipTo() {
		return personInfoShipTo;
	}
	public void setPersonInfoShipTo(Person personInfoShipTo) {
		this.personInfoShipTo = personInfoShipTo;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getCustomerFirstName() {
		return customerFirstName;
	}
	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}
	public List<PaymentMethod> getPaymentMethods() {
		return paymentMethods;
	}
	public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}
	public List<OrderLine> getOrderLines() {
		return orderLines;
	}
	public void setOrderLines(List<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}
	public String getRefundedTaxes() {
		return refundedTaxes;
	}
	public void setRefundedTaxes(String refundedTaxes) {
		this.refundedTaxes = refundedTaxes;
	}
	public List<OrderLine> getReturnOrderLines() {
		return returnOrderLines;
	}
	public void setReturnOrderLines(List<OrderLine> returnOrderLines) {
		this.returnOrderLines = returnOrderLines;
	}
	
	
	public String getCustomerEMailID() {
		return customerEMailID;
	}
	public void setCustomerEMailID(String customerEMailID) {
		this.customerEMailID = customerEMailID;
	}
}
