package com.digital.commerce.integration.order.processor.domain;

import java.io.Serializable;

public class Award implements Serializable, Cloneable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Double awardAmount;
	private String promotionId;
	private String awardType;
	private String description;
	
	public Double getAwardAmount() {
		return awardAmount;
	}
	public void setAwardAmount(Double awardAmount) {
		this.awardAmount = awardAmount;
	}
	public String getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(String promotionId) {
		this.promotionId = promotionId;
	}
	public String getAwardType() {
		return awardType;
	}
	public void setAwardType(String awardType) {
		this.awardType = awardType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
