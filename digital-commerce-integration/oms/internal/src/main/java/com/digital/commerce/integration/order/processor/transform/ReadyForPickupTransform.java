package com.digital.commerce.integration.order.processor.transform;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.order.processor.domain.payload.ReadyForPickupPayload;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

public class ReadyForPickupTransform extends OrderStatusTransform {
	private static final String ORDER_XPATH = PREFIX + "/Order";
	private static XPathExpression orderXPath;
	private static final String BILL_TO_PERSON = "PersonInfoBillTo";
	private static XPathExpression billToPersonXpath;
	private static final String SHIP_TO_PERSON = "PersonInfoShipTo";
	private static XPathExpression shipToPersonXpath;
	private static final String MARK_FOR_PERSON = "PersonInfoMarkFor";
	private static XPathExpression markForPersonXpath;
	private static final String RELATIVE_EXTN_XPATH = "Extn";
	private static XPathExpression relativeLinesExtnXPath;
	private static final String ORDERLINES_XPATH = "OrderLines/OrderLine";
	private static XPathExpression orderLinesXPath;
	private static final String AWARD_XPATH = "Awards/Award";
	private static XPathExpression awardXPath;

	private static boolean isInit = false;

	@Override
	public OrderStatusPayload transform(Document xmlPayloadDocument, OrderStatusEvent.OrderStatusType orderStatusType) throws DigitalIntegrationException {
		ReadyForPickupPayload payload = new ReadyForPickupPayload();

		configure();

		populateOmniOrderUpdate(payload, xmlPayloadDocument, orderStatusType.getReceivedType());

		Element order = (Element) getXPathNode(orderXPath, xmlPayloadDocument);

		populateGeneralFields(payload, order);

		String dateInString = order.getAttribute("ExtnPickUpByDate");
		try {
			if (DigitalStringUtil.isNotEmpty(dateInString)) {
				SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
				Date date = formatter.parse(dateInString);
				formatter.format(date);
				payload.setPickUpByDate((new SimpleDateFormat("MM/dd/yyyy").format(date)));
			}
		} catch (ParseException e) {
			payload.setPickUpByDate(dateInString);
		}

		payload.setCustomerEMailID(order.getAttribute("CustomerEMailID"));
		payload.setCustomerFirstName(order.getAttribute("CustomerFirstName"));
		payload.setCustomerLastName(order.getAttribute("CustomerLastName"));

		this.populateOrderSummary(payload, xmlPayloadDocument);

		Element orderExtnElement = (Element) getXPathNode(relativeLinesExtnXPath, order);
		if (orderExtnElement != null) {
			payload.setLoyaltyId(orderExtnElement.getAttribute("ExtnLoyaltyId"));
			payload.setLoyaltyTier(orderExtnElement.getAttribute("ExtnLoyaltyTier"));
		}

		Element orderLine = (Element) getXPathNode(orderLinesXPath, order);

		Element personInfoBillToElement = (Element) getXPathNode(billToPersonXpath, orderLine);
		payload.setPersonInfoBillTo(getPersonFromElement(personInfoBillToElement));

		Element personInfoShipToElement = (Element) getXPathNode(shipToPersonXpath, orderLine);
		payload.setPersonInfoShipTo(getPersonFromElement(personInfoShipToElement));
		
		Element personInfoMarkForElement = (Element)getXPathNode(markForPersonXpath, orderLine);
		payload.setPersonInfoMarkFor(getPersonFromElement(personInfoMarkForElement));

		populatePayload(payload, order);
		
		NodeList awardNodes = getXPathNodeList(awardXPath, order);
		payload.setAwards(getAwardsFromElement(awardNodes));

		return payload;
	}

	protected void configure() throws DigitalIntegrationException {
		if (!isInit) {
			try {
				orderXPath = getXpathFactory().newXPath().compile(ORDER_XPATH);
				billToPersonXpath = getXpathFactory().newXPath().compile(BILL_TO_PERSON);
				shipToPersonXpath = getXpathFactory().newXPath().compile(SHIP_TO_PERSON);
				markForPersonXpath = getXpathFactory().newXPath().compile(MARK_FOR_PERSON);
				relativeLinesExtnXPath = getXpathFactory().newXPath().compile(RELATIVE_EXTN_XPATH);
				orderLinesXPath = getXpathFactory().newXPath().compile(ORDERLINES_XPATH);
				awardXPath = getXpathFactory().newXPath().compile(AWARD_XPATH);

				isInit = true;
			} catch (XPathExpressionException e) {
				throw new DigitalIntegrationException("Not able to generate xpath expressions", ErrorCodes.XFORM.getCode(),
						"TRANSFORM", e);
			}
		}
	}
}
