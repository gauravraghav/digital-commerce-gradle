package com.digital.commerce.integration.order.processor.domain;

import java.io.Serializable;
import java.util.List;

public class ShipmentLine implements Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<OrderLine> orderLines;
	private String itemId;
	private String orderLineKey;
	private String quantity;
	private String orderedQuantity;
	private String shipmentLineNo;
	private String shipmentNo;
	private String orderNo;
	private String orderLineUnitPrice;
	private String trackingNo;
	private String shipmentKey;
	private String shipmentDate;
	private String carrierName;
	private String shippingMethod;
	private String orderReleaseKey;
	private List<Container> containers;
	
	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getOrderLineKey() {
		return orderLineKey;
	}

	public void setOrderLineKey(String orderLineKey) {
		this.orderLineKey = orderLineKey;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getShipmentLineNo() {
		return shipmentLineNo;
	}

	public void setShipmentLineNo(String shipmentLineNo) {
		this.shipmentLineNo = shipmentLineNo;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOrderLineUnitPrice() {
		return orderLineUnitPrice;
	}

	public void setOrderLineUnitPrice(String orderLineUnitPrice) {
		this.orderLineUnitPrice = orderLineUnitPrice;
	}

	public List<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}

	public String getShipmentKey() {
		return shipmentKey;
	}

	public void setShipmentKey(String shipmentKey) {
		this.shipmentKey = shipmentKey;
	}

	public String getShipmentDate() {
		return shipmentDate;
	}

	public void setShipmentDate(String shipmentDate) {
		this.shipmentDate = shipmentDate;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getShipmentNo() {
		return shipmentNo;
	}

	public void setShipmentNo(String shipmentNo) {
		this.shipmentNo = shipmentNo;
	}

	public String getOrderedQuantity() {
		return orderedQuantity;
	}

	public void setOrderedQuantity(String orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getOrderReleaseKey() {
		return orderReleaseKey;
	}

	public void setOrderReleaseKey(String orderReleaseKey) {
		this.orderReleaseKey = orderReleaseKey;
	}

	public List<Container> getContainers() {
		return containers;
	}

	public void setContainer(List<Container> container) {
		this.containers = container;
	}
}
