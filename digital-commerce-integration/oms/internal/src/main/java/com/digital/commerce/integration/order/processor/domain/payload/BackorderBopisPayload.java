package com.digital.commerce.integration.order.processor.domain.payload;

import java.util.ArrayList;
import java.util.List;

import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.Person;

public class BackorderBopisPayload extends OrderStatusPayload {
	
	private List<OrderLine> orderLines = new ArrayList<>();
	private List<OrderLine> backOrderLines = new ArrayList<>();	
	private Person personInfoBillTo = new Person();
	private Person personInfoShipTo = new Person();
	
	private String loyaltyId;
	private String loyaltyTier;
	private String orderStatus;
	
	private String customerEMailID;
	

	public List<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}

	public Person getPersonInfoBillTo() {
		return personInfoBillTo;
	}

	public void setPersonInfoBillTo(Person personInfoBillTo) {
		this.personInfoBillTo = personInfoBillTo;
	}

	public Person getPersonInfoShipTo() {
		return personInfoShipTo;
	}

	public void setPersonInfoShipTo(Person personInfoShipTo) {
		this.personInfoShipTo = personInfoShipTo;
	}

	public String getLoyaltyId() {
		return loyaltyId;
	}

	public void setLoyaltyId(String loyaltyId) {
		this.loyaltyId = loyaltyId;
	}

	public String getLoyaltyTier() {
		return loyaltyTier;
	}

	public void setLoyaltyTier(String loyaltyTier) {
		this.loyaltyTier = loyaltyTier;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public List<OrderLine> getBackOrderLines() {
		return backOrderLines;
	}

	public void setBackOrderLines(List<OrderLine> backOrderLines) {
		this.backOrderLines = backOrderLines;
	}

	public String getCustomerEMailID() {
		return customerEMailID;
	}

	public void setCustomerEMailID(String customerEMailID) {
		this.customerEMailID = customerEMailID;
	}
	
		
	
}
