package com.digital.commerce.integration.order.processor.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FromOrderReleaseStatus implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<ToOrderReleaseStatus> toOrderReleaseStatus = new ArrayList<>();
	           
    private String movedQty;
	private String orderReleaseStatusKey;
	private String pipelineKey;
	private String status;
	private String orderReleaseKey;
	private String shipNode;
	private String statusQuantity;
	private String expectedDeliveryDate;
	private String expectedShipmentDate;
	private String statusDescription;
	private String totalQuantity;
	private String statusDate;
	private String datesChanged;
	private String lotNumber;
	private String orderLineScheduleKey;
	             
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOrderReleaseKey() {
		return orderReleaseKey;
	}
	public void setOrderReleaseKey(String orderReleaseKey) {
		this.orderReleaseKey = orderReleaseKey;
	}
	public String getShipNode() {
		return shipNode;
	}
	public void setShipNode(String shipNode) {
		this.shipNode = shipNode;
	}
	public String getStatusQuantity() {
		return statusQuantity;
	}
	public void setStatusQuantity(String statusQuantity) {
		this.statusQuantity = statusQuantity;
	}
	public String getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}
	public void setExpectedDeliveryDate(String expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}
	public String getExpectedShipmentDate() {
		return expectedShipmentDate;
	}
	public void setExpectedShipmentDate(String expectedShipmentDate) {
		this.expectedShipmentDate = expectedShipmentDate;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	public String getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public String getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(String statusDate) {
		this.statusDate = statusDate;
	}
	

	public List<ToOrderReleaseStatus> getToOrderReleaseStatus() {
		return toOrderReleaseStatus;
	}

	public void setToOrderReleaseStatus(List<ToOrderReleaseStatus> toOrderReleaseStatus) {
		this.toOrderReleaseStatus = toOrderReleaseStatus;
	}
	public String getMovedQty() {
		return movedQty;
	}
	public void setMovedQty(String movedQty) {
		this.movedQty = movedQty;
	}
	public String getOrderReleaseStatusKey() {
		return orderReleaseStatusKey;
	}
	public void setOrderReleaseStatusKey(String orderReleaseStatusKey) {
		this.orderReleaseStatusKey = orderReleaseStatusKey;
	}
	public String getPipelineKey() {
		return pipelineKey;
	}
	public void setPipelineKey(String pipelineKey) {
		this.pipelineKey = pipelineKey;
	}
	public String getDatesChanged() {
		return datesChanged;
	}
	public void setDatesChanged(String datesChanged) {
		this.datesChanged = datesChanged;
	}
	public String getLotNumber() {
		return lotNumber;
	}
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public String getOrderLineScheduleKey() {
		return orderLineScheduleKey;
	}
	public void setOrderLineScheduleKey(String orderLineScheduleKey) {
		this.orderLineScheduleKey = orderLineScheduleKey;
	}
	
	
}
