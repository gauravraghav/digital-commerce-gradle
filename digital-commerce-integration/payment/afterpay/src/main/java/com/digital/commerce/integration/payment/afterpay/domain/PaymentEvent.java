package com.digital.commerce.integration.payment.afterpay.domain;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class PaymentEvent implements Serializable
{
    @JsonProperty("id")
    private String created;
    @JsonProperty("type")
    private String type;
    private final static long serialVersionUID = 3079276813544225995L;
}