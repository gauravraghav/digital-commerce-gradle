
package com.digital.commerce.integration.payment.afterpay.domain;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
@Setter
@Getter
public class Discount implements Serializable
{

    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("amount")
    private Money amount;
    private final static long serialVersionUID = -52764301860903959L;

}