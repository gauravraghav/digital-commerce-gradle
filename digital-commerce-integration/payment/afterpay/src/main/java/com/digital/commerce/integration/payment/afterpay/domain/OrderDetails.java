package com.digital.commerce.integration.payment.afterpay.domain;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class OrderDetails implements Serializable
{
    @JsonProperty("consumer")
    private Consumer consumer;
    @JsonProperty("billing")
    private Contact billing;
    @JsonProperty("shipping")
    private Contact shipping;
    @JsonProperty("courier")
    private ShippingCourier courier;
    @JsonProperty("items")
    private List<Item> items = null;
    @JsonProperty("discounts")
    private List<Discount> discounts = null;
    @JsonProperty("taxAmount")
    private Money taxAmount;
    @JsonProperty("shippingAmount")
    private Money shippingAmount;
    private final static long serialVersionUID = -5891365488117965835L;
}