package com.digital.commerce.integration.payment.afterpay.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;


@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class CapturePaymentErrorResponse {
    private final static long serialVersionUID=0L;
    private String errorCode;
    private String errorId;
    private String message;
    private Integer httpStatusCode;

    @Override
    public String toString(){
        return
                "CapturePaymentErrorResponse{" +
                        "errorCode = '" + errorCode + '\'' +
                        ",errorId = '" + errorId + '\'' +
                        ",message = '" + message + '\'' +
                        ",httpStatusCode = '" + httpStatusCode + '\'' +
                        "}";
    }
}
