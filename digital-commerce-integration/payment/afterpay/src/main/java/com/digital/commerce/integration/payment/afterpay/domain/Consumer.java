package com.digital.commerce.integration.payment.afterpay.domain;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Consumer implements Serializable
{
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("givenNames")
    private String givenNames;
    @JsonProperty("surname")
    private String surname;
    @JsonProperty("email")
    private String email;
    private final static long serialVersionUID = 4425808097853574321L;
}