package com.digital.commerce.integration.payment.afterpay.domain;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Merchant implements Serializable
{
    @JsonProperty("redirectConfirmUrl")
    private String redirectConfirmUrl;
    @JsonProperty("redirectCancelUrl")
    private String redirectCancelUrl;
    private final static long serialVersionUID = 2135777517501186690L;
}