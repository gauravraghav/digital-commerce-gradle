
package com.digital.commerce.integration.payment.afterpay.domain;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Money implements Serializable
{
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("currency")
    private String currency;
    private final static long serialVersionUID = 5857566981073829867L;

}