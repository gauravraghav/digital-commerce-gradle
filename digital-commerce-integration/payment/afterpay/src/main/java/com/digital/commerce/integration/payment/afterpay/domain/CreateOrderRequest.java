package com.digital.commerce.integration.payment.afterpay.domain;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
@Setter
@Getter
public class CreateOrderRequest implements Serializable
{

    @JsonProperty("totalAmount")
    private Money totalAmount;
    @JsonProperty("consumer")
    private Consumer consumer;
    @JsonProperty("billing")
    private Contact billing;
    @JsonProperty("shipping")
    private Contact shipping;
    @JsonProperty("courier")
    private ShippingCourier courier;
    @JsonProperty("description")
    private String description;
    @JsonProperty("items")
    private List<Item> items = null;
    @JsonProperty("discounts")
    private List<Discount> discounts = null;
    @JsonProperty("merchant")
    private Merchant merchant;
    @JsonProperty("paymentType")
    private final String paymentType = "PAY_BY_INSTALLMENT";
    @JsonProperty("merchantReference")
    private String merchantReference;
    @JsonProperty("taxAmount")
    private Money taxAmount;
    @JsonProperty("shippingAmount")
    private Money shippingAmount;
    private final static long serialVersionUID = -5891365488117965835L;
}