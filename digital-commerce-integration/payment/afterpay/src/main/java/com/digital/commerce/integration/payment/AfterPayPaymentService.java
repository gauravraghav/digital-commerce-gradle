package com.digital.commerce.integration.payment;

import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.service.Service;
import com.digital.commerce.integration.payment.afterpay.domain.AfterPayConfiguration;
import com.digital.commerce.integration.payment.afterpay.domain.CapturePaymentRequest;
import com.digital.commerce.integration.payment.afterpay.domain.CreateOrderRequest;
import com.digital.commerce.integration.payment.afterpay.domain.CreateOrderResponse;
import com.digital.commerce.integration.payment.afterpay.domain.Payment;

public interface AfterPayPaymentService extends Service {
    public enum ServiceMethod {
        GET_CONFIGURATION("fp111"),
        CREATE_ORDER("fp112"),
        CREATE_PAYMENT("fp113");

        String serviceMethodName;

        ServiceMethod(String serviceMethodName) {
            this.serviceMethodName = serviceMethodName;
        }

        public String getServiceMethodName() {
            return serviceMethodName;
        }
    }


    public AfterPayConfiguration[] getConfiguration() throws DigitalIntegrationException;
    public CreateOrderResponse createOrder(CreateOrderRequest orderRequest)throws DigitalIntegrationException;
    public Payment createPayment(CapturePaymentRequest capturePaymentRequest) throws DigitalIntegrationException;

}
