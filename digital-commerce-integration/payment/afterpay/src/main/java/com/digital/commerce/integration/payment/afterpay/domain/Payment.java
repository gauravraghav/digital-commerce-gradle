package com.digital.commerce.integration.payment.afterpay.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Payment implements Serializable {
    private static final long serialVersionUID = 2792702725458620044L;
    @JsonProperty("id")
    private String id;
    @JsonProperty("token")
    private String token;
    @JsonProperty("created")
    private String created;
    @JsonProperty("status")
    private String status;
    @JsonProperty("totalAmount")
    private Money totalAmount;
    @JsonProperty("merchantReference")
    private String merchantReference;
    @JsonProperty("events")
    private List<PaymentEvent> events = null;
    @JsonProperty("orderDetails")
    private OrderDetails orderDetails;
}
