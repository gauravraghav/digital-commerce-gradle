package com.digital.commerce.integration.payment.afterpay;

import com.digital.commerce.integration.payment.AfterPayPaymentService;
import com.digital.commerce.integration.payment.afterpay.domain.*;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.common.service.client.ServiceRequest;
import com.digital.commerce.integration.util.PropertyUtil;
import lombok.Getter;
import lombok.Setter;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class AfterPayPaymentServiceImpl extends BaseIntegrationService implements AfterPayPaymentService {

    private IntegrationServiceClient serviceClient;

    private String getDefaultUri() {
        return getMapData(WebServicesConfig.DataTypes.STATIC, "defaultUri");
    }

    private String getAccountId() {
        return getMapData(WebServicesConfig.DataTypes.STATIC, "accountId");
    }

    private String getSecretKey() {
        return getWebServicesConfig().getVaultedData(getMapData(WebServicesConfig.DataTypes.STATIC, "secretKey"));
    }

    private String getMerchantId() {
        return getMapData(WebServicesConfig.DataTypes.STATIC, "merchantId");
    }

    private String getGetConfigurationPath() {
        return getDefaultUri() + PropertyUtil.getMappedProperty(getMapData(WebServicesConfig.DataTypes.REST_RESOURCES), ServiceMethod.GET_CONFIGURATION.getServiceMethodName()).toString();
    }

    private String getCreateOrderPath() {
        return getDefaultUri() + PropertyUtil.getMappedProperty(getMapData(WebServicesConfig.DataTypes.REST_RESOURCES), ServiceMethod.CREATE_ORDER.getServiceMethodName()).toString();
    }

    private String getCreatePaymentPath() {
        return getDefaultUri() + PropertyUtil.getMappedProperty(getMapData(WebServicesConfig.DataTypes.REST_RESOURCES), ServiceMethod.CREATE_PAYMENT.getServiceMethodName()).toString();
    }

    @Override
    protected String getService() {
        return WebServicesConfig.ServicNames.AFTER_PAY.getName();
    }

    @Override
    public void postStartup() throws DigitalIntegrationException {

    }

    @Override
    public void postShutdown() throws DigitalIntegrationException {

    }

    private void addHeaderInfo(ServiceRequest serviceRequest) {
        Map<String,String> headers =  new HashMap<>();
        headers.put("User-Agent", getMerchantId());
        serviceRequest.setHeaderEntries(headers);
    }

    private Object processResponse(ServiceRequest serviceRequest, Class clazz) throws Exception{
        HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest,getAccountId(),getSecretKey(),false);
        if(response != null && (response.getStatusCode() == 200 || response.getStatusCode() == 201)) {
            if(clazz != String.class) {
                return serviceClient.getUnmarshalledObjectFromHttpServiceResponse(clazz,response);
            } else {
                return response.getEntityString();
            }
        } else {
            if(response == null){
                throw new Exception("An unknown exception occurred in AfterPay service");
            } else {
                String error = response.getStatusCode() + " - " + response.getEntityString();
                logger.error("Integration with AfterPay failed: " + error);
                throw new Exception("An issue occurred with AfterPay service: " + handleError(response));
            }
        }
    }


    private String handleError(HttpServiceResponse response) throws DigitalIntegrationException {

        String errorMessage=null;
        String errorCode = null;
        CapturePaymentErrorResponse error=null;

        if (DigitalStringUtil.isNotBlank(response.getEntityString()) ) {
            error = serviceClient.getUnmarshalledObjectFromHttpServiceResponse(CapturePaymentErrorResponse.class, response);
            if (null!=error && error.getErrorCode() != null) {
                errorCode = error.getErrorCode();
            }
        }

        errorMessage="AfterPayCapturePaymentService_"+errorCode;

     return errorMessage;
    }

    @Override
    public AfterPayConfiguration[] getConfiguration() throws DigitalIntegrationException {
        String serviceMethodName = ServiceMethod.GET_CONFIGURATION.getServiceMethodName();
        boolean hasException = false;
        if (doRunService(serviceMethodName)) {
            startPerformanceMonitorOperation(serviceMethodName);
            try {
                String endPoint = this.getGetConfigurationPath();
                ServiceRequest serviceRequest = new ServiceRequest(
                        getConnectionTimeout(serviceMethodName),
                        getRequestTimeout(serviceMethodName), endPoint,
                        ServiceRequest.ServiceContentType.JSON, ServiceRequest.ServiceHttpMethod.GET, serviceMethodName);
                addHeaderInfo(serviceRequest);
                return (AfterPayConfiguration[])this.processResponse(serviceRequest,AfterPayConfiguration[].class);
            } catch (Exception e) {
                hasException = true;
                throw new DigitalIntegrationBusinessException("AfterPay service error", ErrorCodes.COMM_ERROR.getCode(), e);
            } finally {
                endPerformanceMonitorOperation(serviceMethodName, hasException);
            }
        } else {
            throw new DigitalIntegrationInactiveException(
                    String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
                    ErrorCodes.INACTIVE.getCode());
        }
    }

    @Override
    public CreateOrderResponse createOrder(CreateOrderRequest orderRequest) throws DigitalIntegrationException {
        String serviceMethodName = ServiceMethod.CREATE_ORDER.getServiceMethodName();
        boolean hasException = false;
        if (doRunService(serviceMethodName)) {
            startPerformanceMonitorOperation(serviceMethodName);
            try {
                String endPoint = this.getCreateOrderPath();
                String bodyContent = serviceClient.getMarshalledStringFromObject(CreateOrderRequest.class, orderRequest);
                ServiceRequest serviceRequest = new ServiceRequest(
                        getConnectionTimeout(serviceMethodName),
                        getRequestTimeout(serviceMethodName), endPoint,
                        ServiceRequest.ServiceContentType.JSON, ServiceRequest.ServiceHttpMethod.POST, serviceMethodName);
                serviceRequest.setBody(bodyContent);
                addHeaderInfo(serviceRequest);
                return (CreateOrderResponse)this.processResponse(serviceRequest,CreateOrderResponse.class);
            } catch (Exception e) {
                hasException = true;
                throw new DigitalIntegrationBusinessException("AfterPay service error", ErrorCodes.COMM_ERROR.getCode(), e);
            } finally {
                endPerformanceMonitorOperation(serviceMethodName, hasException);
            }

        } else {
            throw new DigitalIntegrationInactiveException(
                    String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
                    ErrorCodes.INACTIVE.getCode());
        }
    }

    @Override
    public Payment createPayment(CapturePaymentRequest capturePaymentRequest) throws DigitalIntegrationException {
        String serviceMethodName = ServiceMethod.CREATE_PAYMENT.getServiceMethodName();
        boolean hasException = false;
        if (doRunService(serviceMethodName)) {
            startPerformanceMonitorOperation(serviceMethodName);
            try {
                String endPoint = this.getCreatePaymentPath();
                String bodyContent = serviceClient.getMarshalledStringFromObject(CapturePaymentRequest.class, capturePaymentRequest);
                ServiceRequest serviceRequest = new ServiceRequest(
                        getConnectionTimeout(serviceMethodName),
                        getRequestTimeout(serviceMethodName), endPoint,
                        ServiceRequest.ServiceContentType.JSON, ServiceRequest.ServiceHttpMethod.POST, serviceMethodName);
                serviceRequest.setBody(bodyContent);
                addHeaderInfo(serviceRequest);
                return (Payment)this.processResponse(serviceRequest,Payment.class);
            } catch (Exception e) {
                hasException = true;
                throw new DigitalIntegrationBusinessException("AfterPay CapturePayment ERROR :::", e.getMessage());
            } finally {
                endPerformanceMonitorOperation(serviceMethodName, hasException);
            }

        } else {
            throw new DigitalIntegrationInactiveException(
                    String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
                    ErrorCodes.INACTIVE.getCode());
        }
    }
    
    public Map<String, Boolean> getServiceMethodsEnabled(){
		Map<String, Boolean> methods = new HashMap<>();
		for (ServiceMethod service: ServiceMethod.values()) {
			methods.put(service.getServiceMethodName(), isServiceMethodEnabled(service.getServiceMethodName()));
		}
		
		return methods;
    }
}
