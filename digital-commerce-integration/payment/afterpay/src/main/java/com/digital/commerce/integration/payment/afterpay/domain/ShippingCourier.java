package com.digital.commerce.integration.payment.afterpay.domain;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class ShippingCourier implements Serializable
{
    @JsonProperty("shippedAt")
    private String shippedAt;
    @JsonProperty("name")
    private String name;
    @JsonProperty("tracking")
    private String tracking;
    @JsonProperty("priority")
    private String priority;
    private final static long serialVersionUID = 6530745127753583813L;
}
