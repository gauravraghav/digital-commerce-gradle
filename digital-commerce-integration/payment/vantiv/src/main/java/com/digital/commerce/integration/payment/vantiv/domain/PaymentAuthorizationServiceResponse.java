package com.digital.commerce.integration.payment.vantiv.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class PaymentAuthorizationServiceResponse {

    private BigDecimal amountAuthorized;
    private String authorizationCode;
    private String avsCode;
    private long transactionId;
    private String statusMessage;
    private String cvCode;
    private String decision;
    private String reasonCode;
    private String fraudFlag;
    private String fraudResultCode;
    private String fraudResultCodeDescription;
    private String fraudRule;
    private String fraudRuleDescription;
    private String fraudScore;
    private String threadMetrixSessionId;
    private String token;
    private String tokenBin;
    private String tokenECheckAccountSuffix;
    private String tokenMessage;
    private String tokenResponseCode;
    private String id;
    private String customerId;
    private String reportGroup;
    private Date authorizationExpiration;

}
