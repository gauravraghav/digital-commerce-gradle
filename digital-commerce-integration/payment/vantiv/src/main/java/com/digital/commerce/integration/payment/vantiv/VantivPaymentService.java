package com.digital.commerce.integration.payment.vantiv;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.common.service.client.ServiceRequest;
import com.digital.commerce.integration.payment.PaymentAuthorizationService;
import com.digital.commerce.integration.payment.vantiv.domain.FraudCheckServiceResponse;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentApplepayResponse;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentAuthorizationServiceRequest;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentAuthorizationServiceRequest.GiftCardType;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentAuthorizationServiceResponse;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentRegisterTokenServiceRequest;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentRegisterTokenServiceResponse;
import com.digital.commerce.integration.util.PropertyUtil;
import com.digital.commerce.integration.external.payment.vantiv.AdvancedFraudChecksType;
import com.digital.commerce.integration.external.payment.vantiv.Authentication;
import com.digital.commerce.integration.external.payment.vantiv.Authorization;
import com.digital.commerce.integration.external.payment.vantiv.AuthorizationResponse;
import com.digital.commerce.integration.external.payment.vantiv.CardPaypageType;
import com.digital.commerce.integration.external.payment.vantiv.CardTokenType;
import com.digital.commerce.integration.external.payment.vantiv.CardType;
import com.digital.commerce.integration.external.payment.vantiv.Contact;
import com.digital.commerce.integration.external.payment.vantiv.CountryTypeEnum;
import com.digital.commerce.integration.external.payment.vantiv.EcheckForTokenType;
import com.digital.commerce.integration.external.payment.vantiv.FraudCheck;
import com.digital.commerce.integration.external.payment.vantiv.FraudCheckResponse;
import com.digital.commerce.integration.external.payment.vantiv.LitleOnlineRequest;
import com.digital.commerce.integration.external.payment.vantiv.LitleOnlineResponse;
import com.digital.commerce.integration.external.payment.vantiv.ObjectFactory;
import com.digital.commerce.integration.external.payment.vantiv.ProcessingTypeEnum;
import com.digital.commerce.integration.external.payment.vantiv.RegisterTokenRequestType;
import com.digital.commerce.integration.external.payment.vantiv.RegisterTokenResponse;

import lombok.Getter;
import lombok.Setter;

/**
 * Vantiv service client implementation. Initiates calls to Vantiv service to
 * process Vantiv HTTP requests in Vantiv Litle message format
 */
@Getter
@Setter
public class VantivPaymentService extends BaseIntegrationService implements PaymentAuthorizationService {

	private DigitalLogger logger = DigitalLogger.getLogger(PaymentAuthorizationService.class);

	private static final String DECISION_ACCEPT = "pass";
	private static final String DECISION_APPROVED = "Approved";
	private static final String DECISION_REVIEW = "REVIEW";
	private static final String FRAUD_RESULT_CODE_REJECTED_ERROR_CODE = "1";
	private static final String FRAUD_RESULT_CODE_REJECTED = "FRAUD";
	private static final String DEFAULT_CVV_CODE = "000";

	private static final String DECISION_OFFLINE = "offline";
	private static final String DECISION_UNAVAILABLE = "unavailable";
	private static final String CARD_MATCH = "000";
	private static final String SPACE_SEPARATOR = " ";
	private static final String COMMA_SEPARATOR = ",";
	private static final String COLON_SEPARATOR = ":";
	private static final String DEFAULT_ORDER_SOURCE = "ecommerce";

	private static final int AUTHORIZE_EXPIRE_DAYS = 7;

	private static final String SD_MERCHANT_ID = "merchantId";
	private static final String SD_SERVICE_VERSION = "serviceVersion";
	private IntegrationServiceClient serviceClient;
	private DigitalBaseConstants dswConstants;
	

	public VantivPaymentService() {
	}

	@Override
	protected String getService() {
		return WebServicesConfig.ServicNames.VANTIV.getName();
	}

	/**
	 * Vantiv preAuthorize(FP04) service request
	 * 
	 * @param paymentAuthorizationServiceRequest
	 *            PaymentAuthorizationServiceRequest
	 * @return PaymentAuthorizationServiceResponse
	 * @throws DigitalIntegrationSystemException
	 * @throws DigitalIntegrationInactiveException
	 */

	@Override
	public PaymentAuthorizationServiceResponse preAuth(
			PaymentAuthorizationServiceRequest paymentAuthorizationServiceRequest)
			throws DigitalIntegrationSystemException, DigitalIntegrationBusinessException, DigitalIntegrationInactiveException {

		String serviceMethodName = ServiceMethod.PREAUTH.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			try {

				LitleOnlineRequest litleOnlineRequest = prepareLitleOnlineRequest(paymentAuthorizationServiceRequest);
				String requestBody = serviceClient.getMarshalledStringFromObject(LitleOnlineRequest.class,
						litleOnlineRequest);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri(), ServiceRequest.ServiceContentType.XML,
						ServiceRequest.ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setBody(requestBody);

				HttpServiceResponse serviceResponse = serviceClient.processHttpRequest(serviceRequest);
				validateHttpResponse(serviceResponse);

				LitleOnlineResponse litlOnlineResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(LitleOnlineResponse.class, serviceResponse);
				AuthorizationResponse authorizationResponse = (AuthorizationResponse) litlOnlineResponse
						.getTransactionResponse().getValue();

				PaymentAuthorizationServiceResponse paymentAuthorizationServiceResponse = null;
				paymentAuthorizationServiceResponse = populatePaymentAuthorizationServiceResponse(authorizationResponse,
						paymentAuthorizationServiceRequest.getPurchaseTotal());

				return paymentAuthorizationServiceResponse;

			} catch (DigitalIntegrationSystemException e) {
				hasException = true;
				logger.error("Error during vantiv preAuth request", e);
				throw e;
			} catch (DigitalIntegrationException e) {
				hasException = true;
				logger.error("Error during vantiv preAuth request", e);
				throw new DigitalIntegrationBusinessException("Pre-authorization Error", ErrorCodes.COMM_ERROR.getCode(),
						e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(
					String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}

	
	@Override
	public FraudCheckServiceResponse fraudOnly(PaymentAuthorizationServiceRequest paymentAuthorizationServiceRequest)
			throws DigitalIntegrationSystemException, DigitalIntegrationBusinessException, DigitalIntegrationInactiveException {

		String serviceMethodName = ServiceMethod.FRAUDONLY.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			try {

				LitleOnlineRequest litleOnlineRequest = prepareLitleOnlineFraudOnlyRequest(paymentAuthorizationServiceRequest);
				String requestBody = serviceClient.getMarshalledStringFromObject(LitleOnlineRequest.class,litleOnlineRequest);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri(), ServiceRequest.ServiceContentType.XML,
						ServiceRequest.ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setBody(requestBody);

				HttpServiceResponse serviceResponse = serviceClient.processHttpRequest(serviceRequest);
				validateHttpResponse(serviceResponse);

				LitleOnlineResponse litlOnlineResponse = serviceClient.getUnmarshalledObjectFromHttpServiceResponse(LitleOnlineResponse.class, serviceResponse);
				FraudCheckResponse fraudCheckResponse = (FraudCheckResponse) litlOnlineResponse.getTransactionResponse().getValue();

				FraudCheckServiceResponse fraudCheckServiceResponse = null;
				fraudCheckServiceResponse = populateFraudCheckServiceResponse(fraudCheckResponse);

				return fraudCheckServiceResponse;

			} catch (DigitalIntegrationException e) {
				hasException = true;
				logger.error("Error during vantiv preAuth request", e);
				throw new DigitalIntegrationBusinessException("Pre-authorization Error", ErrorCodes.COMM_ERROR.getCode(),
						e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(
					String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}
	@Override
	public PaymentRegisterTokenServiceResponse registerTokenRequest(
			PaymentRegisterTokenServiceRequest paymentRegisterTokenRequest)
			throws DigitalIntegrationSystemException, DigitalIntegrationBusinessException, DigitalIntegrationInactiveException {

		String serviceMethodName = ServiceMethod.REGISTER_TOKEN_REQUEST.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			try {

				LitleOnlineRequest litleOnlineRequest = prepareLitleOnlineRequestForRegisterToken(
						paymentRegisterTokenRequest);
				String requestBody = serviceClient.getMarshalledStringFromObject(LitleOnlineRequest.class,
						litleOnlineRequest);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri(), ServiceRequest.ServiceContentType.XML,
						ServiceRequest.ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setBody(requestBody);

				HttpServiceResponse serviceResponse = serviceClient.processHttpRequest(serviceRequest);
				validateHttpResponse(serviceResponse);

				LitleOnlineResponse litlOnlineResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(LitleOnlineResponse.class, serviceResponse);
				RegisterTokenResponse registerTokenResponse = (RegisterTokenResponse) litlOnlineResponse
						.getTransactionResponse().getValue();

				PaymentRegisterTokenServiceResponse paymentRegisterTokenServiceResponse = null;
				paymentRegisterTokenServiceResponse = populateRegisterTokenServiceResponse(registerTokenResponse);

				return paymentRegisterTokenServiceResponse;

			} catch (DigitalIntegrationException e) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("registerTokenRequest Error", ErrorCodes.COMM_ERROR.getCode(),
						e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(
					String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}

	@Override
	public void postStartup() throws DigitalIntegrationException {

	}

	@Override
	public void postShutdown() throws DigitalIntegrationException {

	}

	public Map<String, String> getStaticData() {
		return getMapData(WebServicesConfig.DataTypes.STATIC);
	}

	public String getServiceVersion() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_SERVICE_VERSION).toString();
	}

	public void setServiceVersion(String serviceVersion) {
		PropertyUtil.setMappedProperty(getStaticData(), SD_SERVICE_VERSION, serviceVersion);
	}

	public String getMerchantId() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_MERCHANT_ID).toString();
	}

	public void setMerchantId(String merchantId) {
		PropertyUtil.setMappedProperty(getStaticData(), SD_MERCHANT_ID, merchantId);
	}

	public String getUserName() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, "userName");
	}

	public String getPassword() {
		return getWebServicesConfig().getVaultedData(getMapData(WebServicesConfig.DataTypes.STATIC, "password"));
	}

	public String getDefaultUri() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, "defaultUri");
	}


	/**
	 * 
	 * @param paymentAuthorizationServiceRequest
	 *            PaymentAuthorizationServiceRequest
	 * @return LitleOnlineRequest
	 * @throws DigitalIntegrationBusinessException
	 */

	private LitleOnlineRequest prepareLitleOnlineRequest(
			PaymentAuthorizationServiceRequest paymentAuthorizationServiceRequest)
			throws DigitalIntegrationBusinessException {

		ObjectFactory ob = new ObjectFactory();
		LitleOnlineRequest litleOnlineRequest = ob.createLitleOnlineRequest();

		Authentication authentication = ob.createAuthentication();
		authentication.setUser(getUserName());
		authentication.setPassword(getPassword());

		Authorization authorization = ob.createAuthorization();
		authorization.setId(paymentAuthorizationServiceRequest.getId());
		authorization.setReportGroup(paymentAuthorizationServiceRequest.getReportGroup());
		authorization.setCustomerId(paymentAuthorizationServiceRequest.getCustomerId());
		authorization.setAllowPartialAuth(false);

		/*
		 * Vantiv requires dollar amounts to be in cents not a decimal number
		 */
		if (paymentAuthorizationServiceRequest.getPurchaseTotal() != null) {
			authorization.setAmount(paymentAuthorizationServiceRequest.getPurchaseTotal()
					.multiply(new BigDecimal("100").setScale( 2, RoundingMode.HALF_UP )).toBigInteger());
		}

		authorization.setOrderId(paymentAuthorizationServiceRequest.getOrderNumber());
		authorization.setOrderSource(DEFAULT_ORDER_SOURCE);

		Contact billToAddress = new Contact();
		if (paymentAuthorizationServiceRequest.getBillingContact() != null) {
			billToAddress.setName(paymentAuthorizationServiceRequest.getCcHolderName());
			billToAddress.setAddressLine1(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getBillingContact().getAddressLine1(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
			billToAddress.setAddressLine2(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getBillingContact().getAddressLine2(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
			billToAddress.setFirstName(paymentAuthorizationServiceRequest.getBillingContact().getFirstName());
			billToAddress.setLastName(paymentAuthorizationServiceRequest.getBillingContact().getLastName());
			billToAddress.setCity(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getBillingContact().getCity(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
					
			billToAddress.setState(paymentAuthorizationServiceRequest.getBillingContact().getState());

			if (getCountryMap().containsKey(paymentAuthorizationServiceRequest.getBillingContact().getCountry())) {
				billToAddress.setCountry(CountryTypeEnum.fromValue(
						getCountryMap().get(paymentAuthorizationServiceRequest.getBillingContact().getCountry())));
			} else {
				billToAddress.setCountry(
						CountryTypeEnum.fromValue(paymentAuthorizationServiceRequest.getBillingContact().getCountry()));
			}

			billToAddress.setZip(paymentAuthorizationServiceRequest.getBillingContact().getZip());
			billToAddress.setEmail(paymentAuthorizationServiceRequest.getBillingContact().getEmail());
			billToAddress.setPhone(paymentAuthorizationServiceRequest.getBillingContact().getPhone());
		}

		String cardValidationNum = "";
		if (!DigitalStringUtil.isBlank(paymentAuthorizationServiceRequest.getCreditCardCid())
				&& !DigitalStringUtil.isEmpty(paymentAuthorizationServiceRequest.getCreditCardCid())) {
			cardValidationNum = paymentAuthorizationServiceRequest.getCreditCardCid();
		} else {
			if(!dswConstants.isPaTool()){
			cardValidationNum = DEFAULT_CVV_CODE;
			}
		}

		if (!DigitalStringUtil.isBlank(paymentAuthorizationServiceRequest.getTypeOfCard())
				&& !DigitalStringUtil.isBlank(paymentAuthorizationServiceRequest.getCcNumber())
				&& DigitalStringUtil.isBlank(paymentAuthorizationServiceRequest.getPayPageRegistrationId())) {
			CardType card = ob.createCardType();
			card.setType(paymentAuthorizationServiceRequest.getTypeOfCard());
			card.setNumber(paymentAuthorizationServiceRequest.getCcNumber());
			card.setExpDate(paymentAuthorizationServiceRequest.getExpirationDate());
			card.setCardValidationNum(cardValidationNum);
			authorization.setCard(card);
		} else if (!DigitalStringUtil.isBlank(paymentAuthorizationServiceRequest.getLitleToken())) {
			CardTokenType cardTokenType = ob.createCardTokenType();
			cardTokenType.setLitleToken(paymentAuthorizationServiceRequest.getLitleToken());
			cardTokenType.setExpDate(paymentAuthorizationServiceRequest.getExpirationDate());
			cardTokenType.setCardValidationNum(cardValidationNum);
			authorization.setToken(cardTokenType);
			authorization.setProcessingType(ProcessingTypeEnum.CARDHOLDER_INITIATED_COF);
		} else if (!DigitalStringUtil.isBlank(paymentAuthorizationServiceRequest.getPayPageRegistrationId())) {
			CardPaypageType cardPaypageType = ob.createCardPaypageType();
			cardPaypageType.setPaypageRegistrationId(paymentAuthorizationServiceRequest.getPayPageRegistrationId());
			cardPaypageType.setCardValidationNum(cardValidationNum);
			cardPaypageType.setExpDate(paymentAuthorizationServiceRequest.getExpirationDate());
			cardPaypageType.setType(paymentAuthorizationServiceRequest.getTypeOfCard());
			authorization.setPaypage(cardPaypageType);
			if(paymentAuthorizationServiceRequest.isCardBeingSavedToProfile()) {
				authorization.setProcessingType(ProcessingTypeEnum.INITIAL_COF);
			}
		} else {
			throw new DigitalIntegrationBusinessException("Must provide card, paypage, or token information",
					ErrorCodes.MISSING_REQUIRED.getCode());
		}

		if (paymentAuthorizationServiceRequest.getShippingContact() != null) {
			Contact shipToAddress = new Contact();
			shipToAddress.setName(paymentAuthorizationServiceRequest.getShippingContact().getFirstName() + SPACE_SEPARATOR +
					paymentAuthorizationServiceRequest.getShippingContact().getLastName());
			shipToAddress.setAddressLine1(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getShippingContact().getAddressLine1(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
			shipToAddress.setAddressLine2(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getShippingContact().getAddressLine2(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
			shipToAddress.setFirstName(paymentAuthorizationServiceRequest.getShippingContact().getFirstName());
			shipToAddress.setLastName(paymentAuthorizationServiceRequest.getShippingContact().getLastName());
			shipToAddress.setCity(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getShippingContact().getCity(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
			shipToAddress.setState(paymentAuthorizationServiceRequest.getShippingContact().getState());

			if (DigitalStringUtil.isNotBlank(paymentAuthorizationServiceRequest.getShippingContact().getCountry())) {
				if (getCountryMap()
						.containsKey(paymentAuthorizationServiceRequest.getShippingContact().getCountry())) {
					shipToAddress.setCountry(CountryTypeEnum.fromValue(getCountryMap()
							.get(paymentAuthorizationServiceRequest.getShippingContact().getCountry())));
				} else {
					shipToAddress.setCountry(CountryTypeEnum
							.fromValue(paymentAuthorizationServiceRequest.getShippingContact().getCountry()));
				}

			}

			shipToAddress.setZip(paymentAuthorizationServiceRequest.getShippingContact().getZip());
			shipToAddress.setEmail(paymentAuthorizationServiceRequest.getShippingContact().getEmail());
			shipToAddress.setPhone(paymentAuthorizationServiceRequest.getShippingContact().getPhone());
			authorization.setShipToAddress(shipToAddress);
		}

		if (!DigitalStringUtil.isBlank(paymentAuthorizationServiceRequest.getThreatMetrixSessionId())) {
			AdvancedFraudChecksType advancedFraudChecksType = ob.createAdvancedFraudChecksType();
			
			if("n".equalsIgnoreCase(getFraudTestMap().get("enabled"))) {
				advancedFraudChecksType
				.setThreatMetrixSessionId(paymentAuthorizationServiceRequest.getThreatMetrixSessionId());
			} else {
				advancedFraudChecksType
				.setThreatMetrixSessionId(getFraudTestMap().get("threatMetrixSessionId"));
			}

			populateAdvancedFraudChecksTypes(advancedFraudChecksType,paymentAuthorizationServiceRequest, billToAddress);

			authorization.setAdvancedFraudChecks(advancedFraudChecksType);
		}

		authorization.setBillToAddress(billToAddress);

		litleOnlineRequest.setAuthentication(authentication);
		litleOnlineRequest.setVersion(getServiceVersion());
		litleOnlineRequest.setMerchantId(getMerchantId());
		litleOnlineRequest.setTransaction(ob.createAuthorization(authorization));

		return litleOnlineRequest;
	}

	
	private LitleOnlineRequest prepareLitleOnlineFraudOnlyRequest(
			PaymentAuthorizationServiceRequest paymentAuthorizationServiceRequest)
			throws DigitalIntegrationBusinessException {

		ObjectFactory ob = new ObjectFactory();
		LitleOnlineRequest litleOnlineRequest = ob.createLitleOnlineRequest();

		Authentication authentication = ob.createAuthentication();
		authentication.setUser(getUserName());
		authentication.setPassword(getPassword());

		FraudCheck fraudCheck = ob.createFraudCheck();
		fraudCheck.setId(paymentAuthorizationServiceRequest.getId());
		fraudCheck.setReportGroup(paymentAuthorizationServiceRequest.getReportGroup());


		Contact billToAddress = new Contact();
		if (paymentAuthorizationServiceRequest.getBillingContact() != null) {
			billToAddress.setName(paymentAuthorizationServiceRequest.getCcHolderName());
			billToAddress.setAddressLine1(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getBillingContact().getAddressLine1(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
			billToAddress.setAddressLine2(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getBillingContact().getAddressLine2(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
			billToAddress.setFirstName(paymentAuthorizationServiceRequest.getBillingContact().getFirstName());
			billToAddress.setLastName(paymentAuthorizationServiceRequest.getBillingContact().getLastName());
			billToAddress.setCity(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getBillingContact().getCity(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
					
			billToAddress.setState(paymentAuthorizationServiceRequest.getBillingContact().getState());

			if (getCountryMap().containsKey(paymentAuthorizationServiceRequest.getBillingContact().getCountry())) {
				billToAddress.setCountry(CountryTypeEnum.fromValue(
						getCountryMap().get(paymentAuthorizationServiceRequest.getBillingContact().getCountry())));
			} else {
				billToAddress.setCountry(
						CountryTypeEnum.fromValue(paymentAuthorizationServiceRequest.getBillingContact().getCountry()));
			}

			billToAddress.setZip(paymentAuthorizationServiceRequest.getBillingContact().getZip());
			billToAddress.setEmail(paymentAuthorizationServiceRequest.getBillingContact().getEmail());
			billToAddress.setPhone(paymentAuthorizationServiceRequest.getBillingContact().getPhone());
		}


		if (paymentAuthorizationServiceRequest.getShippingContact() != null) {
			Contact shipToAddress = new Contact();
			shipToAddress.setName(paymentAuthorizationServiceRequest.getShippingContact().getFirstName() + SPACE_SEPARATOR +
					paymentAuthorizationServiceRequest.getShippingContact().getLastName());
			shipToAddress.setAddressLine1(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getShippingContact().getAddressLine1(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
			shipToAddress.setAddressLine2(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getShippingContact().getAddressLine2(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
			shipToAddress.setFirstName(paymentAuthorizationServiceRequest.getShippingContact().getFirstName());
			shipToAddress.setLastName(paymentAuthorizationServiceRequest.getShippingContact().getLastName());
			shipToAddress.setCity(DigitalStringUtil.replaceChars(
				paymentAuthorizationServiceRequest.getShippingContact().getCity(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars()));
			shipToAddress.setState(paymentAuthorizationServiceRequest.getShippingContact().getState());

			if (DigitalStringUtil.isNotBlank(paymentAuthorizationServiceRequest.getShippingContact().getCountry())) {
				if (getCountryMap()
						.containsKey(paymentAuthorizationServiceRequest.getShippingContact().getCountry())) {
					shipToAddress.setCountry(CountryTypeEnum.fromValue(getCountryMap()
							.get(paymentAuthorizationServiceRequest.getShippingContact().getCountry())));
				} else {
					shipToAddress.setCountry(CountryTypeEnum
							.fromValue(paymentAuthorizationServiceRequest.getShippingContact().getCountry()));
				}

			}

			shipToAddress.setZip(paymentAuthorizationServiceRequest.getShippingContact().getZip());
			shipToAddress.setEmail(paymentAuthorizationServiceRequest.getShippingContact().getEmail());
			shipToAddress.setPhone(paymentAuthorizationServiceRequest.getShippingContact().getPhone());
			fraudCheck.setShipToAddress(shipToAddress);
		}

		if (!DigitalStringUtil.isBlank(paymentAuthorizationServiceRequest.getThreatMetrixSessionId())) {
			AdvancedFraudChecksType advancedFraudChecksType = ob.createAdvancedFraudChecksType();
			
			if("n".equalsIgnoreCase(getFraudOnlyTestMap().get("enabled"))) {
				advancedFraudChecksType
				.setThreatMetrixSessionId(paymentAuthorizationServiceRequest.getThreatMetrixSessionId());
			} else {
				advancedFraudChecksType
				.setThreatMetrixSessionId(getFraudOnlyTestMap().get("threatMetrixSessionId"));
			}
			
			//Call this method to populate the custom Attributes
			populateAdvancedFraudChecksTypes(advancedFraudChecksType,paymentAuthorizationServiceRequest, billToAddress);

			fraudCheck.setAdvancedFraudChecks(advancedFraudChecksType);
		}

		fraudCheck.setBillToAddress(billToAddress);

		litleOnlineRequest.setAuthentication(authentication);
		litleOnlineRequest.setVersion(getServiceVersion());
		litleOnlineRequest.setMerchantId(getMerchantId());
		litleOnlineRequest.setTransaction(ob.createFraudCheck(fraudCheck));

		return litleOnlineRequest;
	}
	
	
	private void populateAdvancedFraudChecksTypes(AdvancedFraudChecksType advancedFraudChecksType, PaymentAuthorizationServiceRequest paymentAuthorizationServiceRequest, Contact billToAddress){
		
		//Custom Attribute 1
		if (paymentAuthorizationServiceRequest.getShippingContact() != null) {
			
			if(DigitalStringUtil.isNotBlank(paymentAuthorizationServiceRequest.getShippingContact().getFirstName()) ||
					DigitalStringUtil.isNotBlank(paymentAuthorizationServiceRequest.getShippingContact().getLastName())){
				advancedFraudChecksType.setCustomAttribute1(DigitalStringUtil.upperCase(
						paymentAuthorizationServiceRequest.getShippingContact().getFirstName() + SPACE_SEPARATOR
								+ paymentAuthorizationServiceRequest.getShippingContact().getLastName()));
			}
			//Custom Attribute 2
			//Get all commerce items from tjhe order and then get the brand names for each commerce items
			if(null!=paymentAuthorizationServiceRequest.getBrandNames()){
				Set<String> set = new HashSet<>(paymentAuthorizationServiceRequest.getBrandNames());
				String uniqueBrands=DigitalStringUtil.join(set,COMMA_SEPARATOR);
				uniqueBrands=uniqueBrands.substring(0, Math.min(256, uniqueBrands.length()));
				advancedFraudChecksType.setCustomAttribute2(DigitalStringUtil.upperCase(uniqueBrands));
			}
		
			if(DigitalStringUtil.isNotBlank(paymentAuthorizationServiceRequest.getShippingContact().getAddressLine1()) ||DigitalStringUtil.isNotBlank(paymentAuthorizationServiceRequest.getShippingContact().getZip())){
				//Custom Attribute 4
				String postalCode=paymentAuthorizationServiceRequest.getShippingContact().getZip();
				if(DigitalStringUtil.isNotBlank(postalCode)){
					postalCode=postalCode.trim();
					postalCode=postalCode.substring(0, Math.min(5, postalCode.length()));
				}
				advancedFraudChecksType.setCustomAttribute4(DigitalStringUtil
						.upperCase(DigitalStringUtil.replaceChars(paymentAuthorizationServiceRequest.getShippingContact().getAddressLine1(),
								getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars())+COMMA_SEPARATOR+postalCode));
			}
		}
		
		//Custom Attribute 3--
		//For US:                  Send CA3 and CA4 as “Street, Zip5” For INTL:              Send CA3 and CA4 as “Street, FullPostalCode”
		
		String country=(String)paymentAuthorizationServiceRequest.getBillingContact().getCountry();
		if(DigitalStringUtil.isNotBlank(country) && ("US".equalsIgnoreCase(country)|| "USA".equalsIgnoreCase(country))){
			String postalCode=billToAddress.getZip();
			if(DigitalStringUtil.isNotBlank(postalCode)){
				postalCode=postalCode.trim();
				postalCode=postalCode.substring(0, Math.min(5, postalCode.length()));
			}
			advancedFraudChecksType.setCustomAttribute3(DigitalStringUtil.upperCase(DigitalStringUtil.replaceChars(billToAddress.getAddressLine1(),
					getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars())+COMMA_SEPARATOR + postalCode));
		}else{
			advancedFraudChecksType.setCustomAttribute3(DigitalStringUtil.upperCase(DigitalStringUtil.replaceChars(billToAddress.getAddressLine1(),
						getDswConstants().getSpecialChars(),getDswConstants().getReplaceChars())+COMMA_SEPARATOR + billToAddress.getZip()));
		}

		String giftCardType = null;
		String shipType = null;

		if (paymentAuthorizationServiceRequest.getGiftCardType().size() > 0) {
			for (GiftCardType gct : paymentAuthorizationServiceRequest.getGiftCardType()) {
				if (GiftCardType.ELECTRONIC.getGiftCardType().equalsIgnoreCase(gct.getGiftCardType())) {
					giftCardType = gct.getGiftCardType();
					break;
				} else {
					giftCardType = gct.getGiftCardType();
				}
			}
		}

		if (paymentAuthorizationServiceRequest.getShipTypes().size() > 0 ) {
			List<PaymentAuthorizationServiceRequest.ShipType> shipTypeList = paymentAuthorizationServiceRequest.getShipTypes();
			shipType = "";
			for(int i=0; i < shipTypeList.size(); i++){
				PaymentAuthorizationServiceRequest.ShipType srshipType = shipTypeList.get(i);
				if(i < (shipTypeList.size() - 1)){
					shipType += srshipType.getShipType() + COLON_SEPARATOR;
				}else{
					shipType += srshipType.getShipType();
				}
			}
		}
		
		//Custom Attribute 5
		List<String> customAttributes=new ArrayList<>();
		String customAttribute5="";
		if(this.dswConstants.isAvsCodeFraudCheckEnabled() && DigitalStringUtil.isNotEmpty(paymentAuthorizationServiceRequest.getAvsCode())){
			customAttribute5=paymentAuthorizationServiceRequest.getAvsCode()+COLON_SEPARATOR; 
		}
		if(giftCardType != null){
			customAttribute5=customAttribute5+giftCardType+COLON_SEPARATOR; 
		}
		if(shipType != null){
			customAttribute5=customAttribute5+shipType; 
		}
		//Remove the last colon if the shitType is null
		if(DigitalStringUtil.isNotBlank(customAttribute5) && COLON_SEPARATOR.equalsIgnoreCase(customAttribute5.substring(customAttribute5.length()-1,customAttribute5.length()))){
			customAttribute5=customAttribute5.substring(0,customAttribute5.length()-1);
		}
		
		advancedFraudChecksType.setCustomAttribute5(customAttribute5);
		
	}
	/**
	 * 
	 * PaymentAuthorizationServiceRequest
	 * @return LitleOnlineRequest
	 * @throws DigitalIntegrationBusinessException
	 */

	private LitleOnlineRequest prepareLitleOnlineRequestForRegisterToken(
			PaymentRegisterTokenServiceRequest registerTokenRequest) throws DigitalIntegrationBusinessException {

		ObjectFactory ob = new ObjectFactory();
		LitleOnlineRequest litleOnlineRequest = ob.createLitleOnlineRequest();

		String cardValidationNum = "";
		if (!DigitalStringUtil.isBlank(registerTokenRequest.getCardValidationNum())
				&& !DigitalStringUtil.isEmpty(registerTokenRequest.getCardValidationNum())) {
			cardValidationNum = registerTokenRequest.getCardValidationNum();
		} else {
			cardValidationNum = DEFAULT_CVV_CODE;
		}

		Authentication authentication = ob.createAuthentication();
		authentication.setUser(getUserName());
		authentication.setPassword(getPassword());

		RegisterTokenRequestType registerToken = ob.createRegisterTokenRequestType();
		registerToken.setId(registerTokenRequest.getId());
		registerToken.setCustomerId(registerTokenRequest.getCustomerId());
		registerToken.setReportGroup(registerTokenRequest.getReportGroup());
		registerToken.setCardValidationNum(cardValidationNum);
		registerToken.setAccountNumber(registerTokenRequest.getAccountNumber());
		registerToken.setOrderId(registerTokenRequest.getOrderId());
		registerToken.setPaypageRegistrationId(registerTokenRequest.getPayPageRegistrationId());

		if (registerTokenRequest.getECheckForToken() != null
				&& DigitalStringUtil.isNotBlank(registerTokenRequest.getECheckForToken().getAcctNum())
				&& DigitalStringUtil.isNotBlank(registerTokenRequest.getECheckForToken().getRoutingNum())) {
			EcheckForTokenType echeck = ob.createEcheckForTokenType();
			echeck.setAccNum(registerTokenRequest.getECheckForToken().getAcctNum());
			echeck.setRoutingNum(registerTokenRequest.getECheckForToken().getRoutingNum());

			registerToken.setEcheckForToken(echeck);
		}

		litleOnlineRequest.setAuthentication(authentication);
		litleOnlineRequest.setVersion(getServiceVersion());
		litleOnlineRequest.setMerchantId(getMerchantId());
		litleOnlineRequest.setTransaction(ob.createRegisterTokenRequest(registerToken));
		return litleOnlineRequest;
	}

	/**
	 * 
	 *            authorizationResponse
	 * @return PaymentAuthorizationServiceResponse
	 */
	private PaymentAuthorizationServiceResponse populatePaymentAuthorizationServiceResponse(
			AuthorizationResponse authorizationResponse, BigDecimal purchaseTotal) {
		PaymentAuthorizationServiceResponse paymentAuthorizationServiceResponse = new PaymentAuthorizationServiceResponse();
		String cvCode = "";
		String decision = DECISION_ACCEPT;
		String avsResult = "";
		String tokenResponseCode = "";
		if (authorizationResponse != null) {

			/*
			 * The vantiv service returns the amount in cents so we need to
			 * convert it back to a decimal for the response
			 */
			if (authorizationResponse.getApprovedAmount() != null) {
				double amount = authorizationResponse.getApprovedAmount().divide(new BigInteger("100")).doubleValue();
				paymentAuthorizationServiceResponse.setAmountAuthorized(BigDecimal.valueOf(amount).setScale( 2, RoundingMode.HALF_UP ));
			} else {
				paymentAuthorizationServiceResponse.setAmountAuthorized(purchaseTotal);
			}

			paymentAuthorizationServiceResponse.setAuthorizationCode(authorizationResponse.getAuthCode());

			paymentAuthorizationServiceResponse.setTransactionId(authorizationResponse.getLitleTxnId());

			if (authorizationResponse.getFraudResult() != null) {
				avsResult = authorizationResponse.getFraudResult().getAvsResult();
				paymentAuthorizationServiceResponse.setAvsCode(authorizationResponse.getFraudResult().getAvsResult());
				paymentAuthorizationServiceResponse
						.setReasonCode(authorizationResponse.getFraudResult().getCardValidationResult());

				cvCode = authorizationResponse.getFraudResult().getCardValidationResult();
				if(logger.isDebugEnabled()) {
					logger.debug("The CardValidation Result from Vantiv: " + cvCode);
				}
				paymentAuthorizationServiceResponse.setCvCode(cvCode);
				
				if (authorizationResponse.getFraudResult().getAdvancedFraudResults() != null) {
					decision = authorizationResponse.getFraudResult().getAdvancedFraudResults().getDeviceReviewStatus();

					if (authorizationResponse.getFraudResult().getAdvancedFraudResults()
							.getDeviceReputationScore() != null) {
						paymentAuthorizationServiceResponse.setFraudRuleDescription(authorizationResponse
								.getFraudResult().getAdvancedFraudResults().getDeviceReputationScore().toString());
					}

				}

				if (authorizationResponse.getFraudResult().getAdvancedFraudResults() != null) {
					if (authorizationResponse.getFraudResult().getAdvancedFraudResults()
							.getDeviceReputationScore() != null) {
						paymentAuthorizationServiceResponse
								.setFraudResultCodeDescription(String.valueOf(authorizationResponse.getFraudResult()
										.getAdvancedFraudResults().getDeviceReputationScore()));
					}

					if (authorizationResponse.getFraudResult().getAdvancedFraudResults().getTriggeredRule() != null) {
						String fraudRules = DigitalStringUtil.join(
								authorizationResponse.getFraudResult().getAdvancedFraudResults().getTriggeredRule(),
								",");
						paymentAuthorizationServiceResponse.setFraudRuleDescription(fraudRules);
					}
				}
			}

			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.DATE, AUTHORIZE_EXPIRE_DAYS);
			Date authExpirationDate = cal.getTime();

			if (DigitalStringUtil.equals(authorizationResponse.getResponse(), CARD_MATCH)
					&& DigitalStringUtil.equalsIgnoreCase(decision, DECISION_ACCEPT)) {
				paymentAuthorizationServiceResponse.setDecision(DECISION_APPROVED.toUpperCase());
				paymentAuthorizationServiceResponse.setFraudFlag("N");
				paymentAuthorizationServiceResponse.setFraudResultCode("0");
				paymentAuthorizationServiceResponse.setAuthorizationExpiration(authExpirationDate);

			} else if (DigitalStringUtil.equals(authorizationResponse.getResponse(), CARD_MATCH)
					&& DigitalStringUtil.equalsIgnoreCase(decision, DECISION_REVIEW)) {
				paymentAuthorizationServiceResponse.setDecision(DECISION_REVIEW.toUpperCase());

				String tokenCode = (authorizationResponse.getTokenResponse() != null)
						? authorizationResponse.getTokenResponse().getTokenResponseCode() : "";

				if (getSoftDeclineMap().containsKey(tokenCode)) {

					String markAsFraud = getSoftDeclineMap().get(tokenCode);

					if (DigitalStringUtil.equalsIgnoreCase(markAsFraud, "n")) {
						paymentAuthorizationServiceResponse.setFraudFlag("N");
					} else {
						paymentAuthorizationServiceResponse.setFraudFlag("Y");
					}

				} else {
					paymentAuthorizationServiceResponse.setFraudFlag("Y");
				}

				paymentAuthorizationServiceResponse.setFraudResultCode("0");
				paymentAuthorizationServiceResponse.setAuthorizationExpiration(authExpirationDate);

			} else if (DigitalStringUtil.equals(authorizationResponse.getResponse(), CARD_MATCH)
					&& DigitalStringUtil.equalsIgnoreCase(decision, DECISION_OFFLINE)) {
				paymentAuthorizationServiceResponse.setDecision(DECISION_OFFLINE.toUpperCase());
				paymentAuthorizationServiceResponse.setFraudFlag("Y");
				paymentAuthorizationServiceResponse.setFraudResultCode("0");
				paymentAuthorizationServiceResponse.setAmountAuthorized(new BigDecimal(0).setScale( 2, RoundingMode.HALF_UP ));

			} else if (DigitalStringUtil.equals(authorizationResponse.getResponse(), CARD_MATCH)
					&& DigitalStringUtil.equalsIgnoreCase(decision, DECISION_UNAVAILABLE)) {
				paymentAuthorizationServiceResponse.setDecision(DECISION_REVIEW.toUpperCase());
				paymentAuthorizationServiceResponse.setFraudFlag("Y");
				paymentAuthorizationServiceResponse.setFraudResultCode("0");
				paymentAuthorizationServiceResponse.setAuthorizationExpiration(authExpirationDate);
			}else {
				tokenResponseCode = null;

				if (authorizationResponse.getTokenResponse() != null) {
					tokenResponseCode = authorizationResponse.getTokenResponse().getECheckAccountSuffix();
				}
				if (doProcessOffline(authorizationResponse.getResponse(), tokenResponseCode)) {
					paymentAuthorizationServiceResponse.setDecision(DECISION_OFFLINE.toUpperCase());
					paymentAuthorizationServiceResponse.setFraudFlag("Y");
					paymentAuthorizationServiceResponse.setFraudResultCode("0");
					paymentAuthorizationServiceResponse.setAmountAuthorized(new BigDecimal(0).setScale( 2, RoundingMode.HALF_UP ));
				} else {
					paymentAuthorizationServiceResponse.setDecision(FRAUD_RESULT_CODE_REJECTED);
					paymentAuthorizationServiceResponse.setFraudFlag("Y");
					paymentAuthorizationServiceResponse.setFraudResultCode(FRAUD_RESULT_CODE_REJECTED_ERROR_CODE);

					paymentAuthorizationServiceResponse.setAmountAuthorized(null);
				}
			}

			if (authorizationResponse.getTokenResponse() != null) {
				paymentAuthorizationServiceResponse.setTokenBin(authorizationResponse.getTokenResponse().getBin());
				paymentAuthorizationServiceResponse
						.setTokenECheckAccountSuffix(authorizationResponse.getTokenResponse().getECheckAccountSuffix());
				paymentAuthorizationServiceResponse.setToken(authorizationResponse.getTokenResponse().getLitleToken());
				paymentAuthorizationServiceResponse
						.setTokenMessage(authorizationResponse.getTokenResponse().getTokenMessage());
				paymentAuthorizationServiceResponse
						.setTokenResponseCode(authorizationResponse.getTokenResponse().getTokenResponseCode());
			}

			paymentAuthorizationServiceResponse.setId(authorizationResponse.getId());
			paymentAuthorizationServiceResponse.setCustomerId(authorizationResponse.getCustomerId());
			paymentAuthorizationServiceResponse.setReportGroup(authorizationResponse.getReportGroup());
			paymentAuthorizationServiceResponse.setStatusMessage(paymentAuthorizationServiceResponse.getDecision());

		}
		try {
			if (logger.isInfoEnabled()) {
				tokenResponseCode = (authorizationResponse.getTokenResponse() != null)
						? authorizationResponse.getTokenResponse().getTokenResponseCode() : "";
				StringBuilder sb = new StringBuilder();
				sb.append("Vantivresponse: ")
						.append("txId=" + authorizationResponse.getLitleTxnId()
								+ ",")
						.append("orderId=" + authorizationResponse.getOrderId()
								+ ",")
						.append("response="
								+ authorizationResponse.getResponse() + ",")
						.append("avsResult=" + avsResult + ",")
						.append("cardValidationResult=" + cvCode + ",")
						.append("deviceReviewStatus=" + decision + ",")
						.append("tokenResponseCode=" + tokenResponseCode);
				logger.info(sb.toString());
			}
		} catch (Exception ex) {
			logger.error("Exception while logging the vantiv response, doing nothing..."
					+ ex.getMessage());
		}
		return paymentAuthorizationServiceResponse;
	}
	
	
	private FraudCheckServiceResponse populateFraudCheckServiceResponse(FraudCheckResponse fraudCheckResponse) {
		FraudCheckServiceResponse fraudCheckServiceResponse = new FraudCheckServiceResponse();
		String cvCode = "";
		String decision = DECISION_ACCEPT;
		String avsResult = "";
		String tokenResponseCode = "";
		if (fraudCheckResponse != null) {

			fraudCheckServiceResponse.setTransactionId(fraudCheckResponse.getLitleTxnId());

			if (fraudCheckResponse.getAdvancedFraudResults() != null) {
				decision = fraudCheckResponse.getAdvancedFraudResults().getDeviceReviewStatus();

				if (fraudCheckResponse.getAdvancedFraudResults().getDeviceReputationScore() != null) {
					fraudCheckServiceResponse.setFraudRuleDescription(fraudCheckResponse.getAdvancedFraudResults().getDeviceReputationScore().toString());
					fraudCheckServiceResponse.setFraudResultCodeDescription(String.valueOf(fraudCheckResponse.getAdvancedFraudResults().getDeviceReputationScore()));

				}

				if (fraudCheckResponse.getAdvancedFraudResults().getTriggeredRule() != null) {
					String fraudRules = DigitalStringUtil.join(fraudCheckResponse.getAdvancedFraudResults().getTriggeredRule(),",");
					fraudCheckServiceResponse.setFraudRuleDescription(fraudRules);
				}
			}

			if (DigitalStringUtil.equals(fraudCheckResponse.getResponse(), CARD_MATCH) && DigitalStringUtil.equalsIgnoreCase(decision, DECISION_ACCEPT)) {
				fraudCheckServiceResponse.setDecision(DECISION_APPROVED.toUpperCase());
				} else  {
				fraudCheckServiceResponse.setDecision(DECISION_REVIEW.toUpperCase());
				fraudCheckServiceResponse.setFraudFlag("Y");
				fraudCheckServiceResponse.setFraudResultCode("0");
			}

			fraudCheckServiceResponse.setId(fraudCheckResponse.getId());
			fraudCheckServiceResponse.setCustomerId(fraudCheckResponse.getCustomerId());
			fraudCheckServiceResponse.setReportGroup(fraudCheckResponse.getReportGroup());

		}

		try {
			if (logger.isInfoEnabled()) {
				decision=fraudCheckServiceResponse.getDecision();
				long txId = 0L;
				String response=null;
				if(null != fraudCheckResponse) {
					txId = fraudCheckResponse.getLitleTxnId();
					response = fraudCheckResponse.getResponse();
				}
				StringBuilder sb = new StringBuilder();
				sb.append("Vantivresponse: ")
				.append("txId=" + txId
						+ ",")
						.append("response="
								+ response + ",")
								.append("avsResult=" + avsResult + ",")
								.append("cardValidationResult=" + cvCode + ",")
								.append("deviceReviewStatus=" + decision + ",");
				logger.info(sb.toString());
			}
		} catch (Exception ex) {
			logger.error("Exception while logging the vantiv response, doing nothing..."
					+ ex.getMessage());
		}
		return fraudCheckServiceResponse;
	}


	private PaymentRegisterTokenServiceResponse populateRegisterTokenServiceResponse(
			RegisterTokenResponse registerTokenResponse) {

		PaymentRegisterTokenServiceResponse paymentRegisterTokenServiceResponse = new PaymentRegisterTokenServiceResponse();

		if (registerTokenResponse.getApplepayResponse() != null) {
			PaymentApplepayResponse paymentApplepayResponse = new PaymentApplepayResponse();
			paymentApplepayResponse.setApplicationExpirationDate(
					registerTokenResponse.getApplepayResponse().getApplicationExpirationDate());
			paymentApplepayResponse.setApplicationPrimaryAccountNumber(
					registerTokenResponse.getApplepayResponse().getApplicationPrimaryAccountNumber());
			paymentApplepayResponse.setCardholderName(registerTokenResponse.getApplepayResponse().getCardholderName());
			paymentApplepayResponse.setCurrencyCode(registerTokenResponse.getApplepayResponse().getCurrencyCode());
			paymentApplepayResponse.setDeviceManufacturerIdentifier(
					registerTokenResponse.getApplepayResponse().getDeviceManufacturerIdentifier());
			paymentApplepayResponse.setEciIndicator(registerTokenResponse.getApplepayResponse().getEciIndicator());
			paymentApplepayResponse.setOnlinePaymentCryptogram(
					registerTokenResponse.getApplepayResponse().getOnlinePaymentCryptogram());
			paymentApplepayResponse
					.setPaymentDataType(registerTokenResponse.getApplepayResponse().getPaymentDataType());
			paymentApplepayResponse
					.setTransactionAmount(registerTokenResponse.getApplepayResponse().getTransactionAmount());

			paymentRegisterTokenServiceResponse.setApplePayResponse(paymentApplepayResponse);
		}

		paymentRegisterTokenServiceResponse.setBin(registerTokenResponse.getBin());
		paymentRegisterTokenServiceResponse.setCustomerId(registerTokenResponse.getCustomerId());
		paymentRegisterTokenServiceResponse.setECheckAccountSuffix(registerTokenResponse.getECheckAccountSuffix());
		paymentRegisterTokenServiceResponse.setId(registerTokenResponse.getId());
		paymentRegisterTokenServiceResponse.setLitleToken(registerTokenResponse.getLitleToken());
		paymentRegisterTokenServiceResponse.setLitleTxnId(registerTokenResponse.getLitleTxnId());
		paymentRegisterTokenServiceResponse.setMessage(registerTokenResponse.getMessage());
		paymentRegisterTokenServiceResponse.setOrderId(registerTokenResponse.getOrderId());
		paymentRegisterTokenServiceResponse.setReportGroup(registerTokenResponse.getReportGroup());
		paymentRegisterTokenServiceResponse.setResponse(registerTokenResponse.getResponse());
		paymentRegisterTokenServiceResponse.setResponseTime(registerTokenResponse.getResponseTime());
		paymentRegisterTokenServiceResponse.setType(registerTokenResponse.getType());

		return paymentRegisterTokenServiceResponse;
	}

	public Map<String, String> getCountryMap() {
		return getWebServicesConfig().getVantivCountryMapping();
	}

	private Map<String, String> getProcessAsOfflineMap() {
		return getWebServicesConfig().getVantivProcessAsOfflineCombinationMapping();
	}

	private Map<String, String> getFraudTestMap() {
		return getWebServicesConfig().getVantivFraudTestMapping();
	}
	
	private Map<String, String> getFraudOnlyTestMap() {
		return getWebServicesConfig().getVantivFraudOnlyTestMapping();
	}

	private boolean doProcessOffline(String responseCode, String tokenResponseCode) {
		String processAsOffline = "f";

		if (responseCode == null || tokenResponseCode == null) {
			return false;
		}

		String combinationCode = responseCode + "_" + tokenResponseCode;

		if (getProcessAsOfflineMap() != null && getProcessAsOfflineMap().containsKey(combinationCode)) {
			processAsOffline = getProcessAsOfflineMap().get(combinationCode);
		}

		if (!DigitalStringUtil.isBlank(processAsOffline) && (DigitalStringUtil.equalsIgnoreCase(processAsOffline, "t")
				|| DigitalStringUtil.equalsIgnoreCase(processAsOffline, "true")
				|| DigitalStringUtil.equalsIgnoreCase(processAsOffline, "y"))) {
			return true;
		} else {
			return false;
		}
	}

	public Map<String, String> getSoftDeclineMap() {
		return getWebServicesConfig().getVantivSoftDeclineMapping();
	}

    public Map<String, Boolean> getServiceMethodsEnabled(){
		Map<String, Boolean> methods = new HashMap<>();
		for (ServiceMethod service: ServiceMethod.values()) {
			methods.put(service.getServiceMethodName(), isServiceMethodEnabled(service.getServiceMethodName()));
		}
		
		return methods;
    }
}
