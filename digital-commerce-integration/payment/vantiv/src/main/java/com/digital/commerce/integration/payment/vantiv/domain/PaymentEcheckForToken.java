/**
 * 
 */
package com.digital.commerce.integration.payment.vantiv.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class PaymentEcheckForToken {

	private String acctNum;
	
	private String routingNum;

}
