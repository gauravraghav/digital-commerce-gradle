package com.digital.commerce.integration.payment.vantiv.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FraudCheckServiceResponse {

    private long transactionId;
    private String statusMessage;
    private String decision;
    private String reasonCode;
    private String fraudFlag;
    private String fraudResultCode;
    private String fraudResultCodeDescription;
    private String fraudRule;
    private String fraudRuleDescription;
    private String fraudScore;
    private String threadMetrixSessionId;
    private String id;
    private String customerId;
    private String reportGroup;

}
