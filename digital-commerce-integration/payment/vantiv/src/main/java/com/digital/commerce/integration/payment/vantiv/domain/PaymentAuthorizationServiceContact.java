package com.digital.commerce.integration.payment.vantiv.domain;

import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentAuthorizationServiceContact {
	private String addressLine1 = "";
	private String addressLine2 = "";
	private String firstName = "";
	private String lastName = "";
	private String city = "";
	private String country = "";

	private String state = "";
	private String zip = "";

	private String email = "";
	private String phone = "";
	
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = DigitalStringUtil.isEmpty(addressLine1) == true ? "" : addressLine1;
	}
	public String getAddressLine2() {
		return DigitalStringUtil.isEmpty(addressLine2) == true ? "" : addressLine2;
	}
	public String getFirstName() {
		return DigitalStringUtil.isEmpty(firstName) == true ? "" : firstName;
	}
	public String getLastName() {
		return DigitalStringUtil.isEmpty(lastName) == true ? "" : lastName;
	}
	public String getCity() {
		return DigitalStringUtil.isEmpty(city) == true ? "" : city;
	}
	public String getCountry() {
		return DigitalStringUtil.isEmpty(country) == true ? "" : country;
	}
	public String getState() {
		return DigitalStringUtil.isEmpty(state) == true ? "" : state;
	}
	public String getZip() {
		return DigitalStringUtil.isEmpty(zip) == true ? "" : zip;
	}
	public String getEmail() {
		return DigitalStringUtil.isEmpty(email) == true ? "" : email;
	}
	public String getPhone() {
		return DigitalStringUtil.isEmpty(phone) == true ? "" : phone;
	}
	
}
