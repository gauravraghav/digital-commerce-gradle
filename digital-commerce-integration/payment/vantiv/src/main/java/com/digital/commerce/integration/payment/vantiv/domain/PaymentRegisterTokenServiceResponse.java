/**
 * 
 */
package com.digital.commerce.integration.payment.vantiv.domain;

import lombok.Getter;
import lombok.Setter;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * @author mmallipu
 *
 */

@Getter
@Setter
public class PaymentRegisterTokenServiceResponse {

	private String bin;
	
	private String customerId;
	
	private String eCheckAccountSuffix;
	
	private String id;
	
	private String litleToken;
	
	private long litleTxnId;
	
	private String message;
	
	private String orderId;
	
	private String reportGroup;
	
	private String response;
	
	private XMLGregorianCalendar responseTime;
	
	private String type;

	private PaymentApplepayResponse applePayResponse;

}
