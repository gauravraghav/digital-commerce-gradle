package com.digital.commerce.integration.payment.vantiv.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PaymentAuthorizationServiceRequest {

	@Getter
	public enum GiftCardType {
		PERSONAL_STANDARD("GC"), ELECTRONIC("EGC");
		
		private String giftCardType;

		private GiftCardType(String giftCardType) {
			this.giftCardType = giftCardType;
		}

	}

	@Getter
	public enum ShipType {
		SHIP_TO_STORE("STS"), PICKUP("ISPU"), STANDARD("");
		
		private String shipType;

		private ShipType(String shipType) {
			this.shipType = shipType;
		}
	}
	
	private PaymentAuthorizationServiceContact billingContact;
	private PaymentAuthorizationServiceContact shippingContact;
	
    private BigDecimal purchaseTotal;
    private String orderNumber;
    private String orderSource;

    private String ccHolderName;
    private String ccNumber;
    private String expirationDate;
    private String creditCardCid;
    
    private String id;
    private String reportGroup;
    private String customerId;
    private String typeOfCard;
    private String payPageRegistrationId;
    private String litleToken;
    private boolean isCardBeingSavedToProfile = false;
    private String threatMetrixSessionId;
    private String avsCode;
    private List<String> brandNames;
    
    private List<GiftCardType> giftCardType = new ArrayList<>();
    private ShipType shipType = ShipType.STANDARD;
    private List<ShipType> shipTypes = new ArrayList<>(); 

}
