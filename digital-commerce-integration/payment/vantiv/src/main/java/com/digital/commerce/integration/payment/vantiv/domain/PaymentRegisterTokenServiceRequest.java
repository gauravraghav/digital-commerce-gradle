/**
 * 
 */
package com.digital.commerce.integration.payment.vantiv.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class PaymentRegisterTokenServiceRequest {

	private String id;
	
	private String customerId;
	
	private String reportGroup;
	
	private String cardValidationNum;
	
	private String accountNumber;
	
	private String orderId;
	
	private String payPageRegistrationId;
	
	private PaymentEcheckForToken eCheckForToken;

}
