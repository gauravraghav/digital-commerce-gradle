package com.digital.commerce.integration.payment;

import com.digital.commerce.integration.common.service.Service;
import com.digital.commerce.integration.payment.vantiv.domain.FraudCheckServiceResponse;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentAuthorizationServiceRequest;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentAuthorizationServiceResponse;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentRegisterTokenServiceRequest;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentRegisterTokenServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;

import lombok.Getter;


/**
 * Interface to be used by the application to perform payment authorization
 * @author JM406760
 *
 */
public interface PaymentAuthorizationService extends Service{
	@Getter
	public enum ServiceMethod {
		PREAUTH("fp04"),
		FRAUDONLY("fp04b"),
		REGISTER_TOKEN_REQUEST("fp04a");

		
		String serviceMethodName;
		
		ServiceMethod(String serviceMethodName) {
			this.serviceMethodName = serviceMethodName;
		}
	}


    public PaymentAuthorizationServiceResponse preAuth(PaymentAuthorizationServiceRequest paymentAuthorizationServiceRequest) throws DigitalIntegrationException;
    public PaymentRegisterTokenServiceResponse registerTokenRequest(PaymentRegisterTokenServiceRequest paymentRegisterTokenRequest) throws DigitalIntegrationException;
    public FraudCheckServiceResponse fraudOnly(PaymentAuthorizationServiceRequest paymentAuthorizationServiceRequest)throws DigitalIntegrationException;

}
