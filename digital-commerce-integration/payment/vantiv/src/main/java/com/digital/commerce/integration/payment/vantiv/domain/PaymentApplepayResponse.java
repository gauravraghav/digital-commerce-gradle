/**
 * 
 */
package com.digital.commerce.integration.payment.vantiv.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class PaymentApplepayResponse {

	private String applicationExpirationDate;
	
	private String applicationPrimaryAccountNumber;
	
	private String cardholderName;
	
	private String currencyCode;
	
	private String deviceManufacturerIdentifier;
	
	private String eciIndicator;
	
	private byte[] onlinePaymentCryptogram;
	
	private String paymentDataType;
	
	private BigInteger transactionAmount;
	
}
