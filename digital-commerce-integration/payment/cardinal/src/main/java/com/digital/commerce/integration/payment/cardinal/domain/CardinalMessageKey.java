package com.digital.commerce.integration.payment.cardinal.domain;

public final class CardinalMessageKey {

    private CardinalMessageKey(){
    }

    public static final String CARDINAL_ROOT = "CardinalMPI";
    public static final String CARDINAL_MSG_TYPE = "MsgType";
    public static final String CARDINAL_VERSION = "Version";
    public static final String CARDINAL_PROCESSOR_ID = "ProcessorId";
    public static final String CARDINAL_MERCHANT_ID = "MerchantId";
    public static final String CARDINAL_MERCHANT_CONSUME_MSG = "MerchantConsumerMessage";
    public static final String CARDINAL_TRANSACTION_PWD = "TransactionPwd";  //NOSONAR
    public static final String CARDINAL_TRANSACTION_TYPE = "TransactionType";
    public static final String CARDINAL_TRANSACTION_ID = "TransactionId";
    public static final String CARDINAL_TRANSACTION_ACTION = "TransactionAction";
    public static final String CARDINAL_ORDER_ID = "OrderId";
    public static final String CARDINAL_AMOUNT = "Amount";
    public static final String CARDINAL_CURRENCY_CODE = "CurrencyCode";
    public static final String CARDINAL_ORDER_NUM = "OrderNumber";
    public static final String CARDINAL_PA_RES_PAYLOAD = "PAResPayload";
    public static final String CARDINAL_ORDER_DESC = "OrderDescription";
    public static final String CARDINAL_EMAIL = "EMail";
    public static final String CARDINAL_IP_ADDRESS = "IPAddress";
    public static final String CARDINAL_FORCE_ADDRESS = "ForceAddress";
    public static final String CARDINAL_NO_SHIPPING = "NoShipping";
    public static final String CARDINAL_OVERRIDE_ADDRESS = "OverrideAddress";
    public static final String CARDINAL_TRANSACTION_MODE = "TransactionMode";
    public static final String CARDINAL_RECURRING = "Recurring";

    public static final String CARDINAL_CARD_NUMBER = "CardNumber";
    public static final String CARDINAL_EXP_MONTH = "CardExpMonth";
    public static final String CARDINAL_EXP_YEAR = "CardExpYear";

    public static final String CARDINAL_BILLING_FIRST_NAME = "BillingFirstName";
    public static final String CARDINAL_BILLING_LAST_NAME = "BillingLastName";
    public static final String CARDINAL_BILLING_ADDRESS1 = "BillingAddress1";
    public static final String CARDINAL_BILLING_ADDRESS2 = "BillingAddress2";
    public static final String CARDINAL_BILLING_CITY = "BillingCity";
    public static final String CARDINAL_BILLING_STATE = "BillingState";
    public static final String CARDINAL_BILLING_POSTAL_CODE = "BillingPostalCode";
    public static final String CARDINAL_BILLING_COUNTRY_CODE = "BillingCountryCode";
    public static final String CARDINAL_BILLING_PHONE = "BillingPhone";

    public static final String CARDINAL_SHIPPING_FIRST_NAME = "ShippingFirstName";
    public static final String CARDINAL_SHIPPING_LAST_NAME = "ShippingLastName";
    public static final String CARDINAL_SHIPPING_ADDRESS1 = "ShippingAddress1";
    public static final String CARDINAL_SHIPPING_ADDRESS2 = "ShippingAddress2";
    public static final String CARDINAL_SHIPPING_CITY = "ShippingCity";
    public static final String CARDINAL_SHIPPING_STATE = "ShippingState";
    public static final String CARDINAL_SHIPPING_POSTAL_CODE = "ShippingPostalCode";
    public static final String CARDINAL_SHIPPING_COUNTRY_CODE = "ShippingCountryCode";
    public static final String CARDINAL_SHIPPING_PHONE = "ShippingPhone";

    public static final String CARDINAL_ITEM_AMOUNT = "ItemAmount";
    public static final String CARDINAL_MERCHANT_DATA = "MerchantData";

    public static final String CARDINAL_ITEM_NAME_1 = "Item_Name_1";
    public static final String CARDINAL_ITEM_DESC_1 = "Item_Desc_1";
    public static final String CARDINAL_ITEM_PRICE_1 = "Item_Price_1";
    public static final String CARDINAL_ITEM_QUANTITY_1 = "Item_Quantity_1";
    public static final String CARDINAL_ITEM_SKU_1 = "Item_SKU_1";

    public static final String CARDINAL_ITEM_NAME_2 = "Item_Name_2";
    public static final String CARDINAL_ITEM_DESC_2 = "Item_Desc_2";
    public static final String CARDINAL_ITEM_PRICE_2 = "Item_Price_2";
    public static final String CARDINAL_ITEM_QUANTITY_2 = "Item_Quantity_2";
    public static final String CARDINAL_ITEM_SKU_2 = "Item_SKU_2";

    public static final String CARDINAL_RESPONSE_ERR_NO = "ErrorNo";
    public static final String CARDINAL_RESPONSE_ERR_DESC = "ErrorDesc";
    public static final String CARDINAL_RESPONSE_REF_ID = "ReferenceId";
    public static final String CARDINAL_RESPONSE_STATUS_CODE = "StatusCode";
    public static final String CARDINAL_RESPONSE_ORDER_ID = "OrderId";
    public static final String CARDINAL_RESPONSE_ORDER_NUM = "OrderNumber";
    public static final String CARDINAL_RESPONSE_TRANSACTION_ID = "TransactionId";
    public static final String CARDINAL_RESPONSE_REASON_CODE = "ReasonCode";
    public static final String CARDINAL_RESPONSE_REASON_DESC = "ReasonDesc";
    public static final String CARDINAL_RESPONSE_AUTHORIZATION_CODE = "AuthorizationCode";
    public static final String CARDINAL_RESPONSE_PROCESSOR_TID= "ProcessorTransactionId";
    public static final String CARDINAL_RESPONSE_MERCHANT_REF = "MerchantReferenceNumber";
    public static final String CARDINAL_PROCESSOR_ORDER_NUM = "ProcessorOrderNumber";
    public static final String CARDINAL_RESPONSE_AVS_RESULT = "AVSResult";
    public static final String CARDINAL_RESPONSE_MERCHANT_DATA = "MerchantData";
    public static final String CARDINAL_RESPONSE_EFP = "EligibleForProtection";
    public static final String CARDINAL_RESPONSE_CC_RESULT = "CardCodeResult";
    public static final String CARDINAL_RESPONSE_ACS_URL = "ACSUrl";
    public static final String CARDINAL_RESPONSE_ENROLLED = "Enrolled";
    public static final String CARDINAL_RESPONSE_PAYLOAD = "Payload";
    
    public static final String CARDINAL_SHIPPING_AMOUNT = "ShippingAmount";
    public static final String CARDINAL_PA_RES_STATUS = "PAResStatus";
    public static final String CARDINAL_PROCESSOR_BILLING_STATE = "ProcessorBillingState";
    public static final String CARDINAL_PROCESSOR_BILLING_ADDRESS1 = "ProcessorBillingAddress1";
    public static final String CARDINAL_PROCESSOR_BILLING_ADDRESS2 = "ProcessorBillingAddress2";
    public static final String CARDINAL_PROCESSOR_BILLING_PHONE = "ProcessorBillingPhone";
    public static final String CARDINAL_MERCHANT_REF_NUMBER = "MerchantReferenceNumber";
    public static final String CARDINAL_FINANCING_FEE_AMOUNT = "FinancingFeeAmount";
    public static final String CARDINAL_ORDER_TOTAL = "OrderTotal";
    public static final String CARDINAL_PROCESSOR_SHIPPING_CITY = "ProcessorShippingCity";
    public static final String CARDINAL_PROCESSOR_USER_ID = "ProcessorUserId";
    public static final String CARDINAL_FINANCING_MONTHLY_PAYMENT = "FinancingMonthlyPayment";
    public static final String CARDINAL_CART_CHNAGE_TOLERANCE = "CartChangeTolerance";
    public static final String CARDINAL_INSURANCE_AMOUNT = "InsuranceAmount";
    public static final String CARDINAL_SHIPPING_METHOD = "ShippingMethod";
    public static final String CARDINAL_PROCESSOR_SHIPPING_STATE = "ProcessorShippingState";
    public static final String CARDINAL_PROCESSOR_USER_STATUS = "ProcessorUserStatus";
    public static final String CARDINAL_FINANCING_TERM = "FinancingTerm";
    public static final String CARDINAL_PROCESSOR_SHIPPING_ZIP = "ProcessorShippingPostalCode";
    public static final String CARDINAL_PROCESSOR_SHIPPING_PHONE = "ProcessorShippingPhone";
    public static final String CARDINAL_PROCESSOR_USER_LAST_NAME = "ProcessorUserLastName";
    public static final String CARDINAL_PROCESSOR_USER_FIRST_NAME = "ProcessorUserFirstName";
    public static final String CARDINAL_HANDLING_AMOUNT = "HandlingAmount";
    public static final String CARDINAL_PROCESSOR_SHIPPING_FULL_NAME = "ProcessorShippingFullName";
    public static final String CARDINAL_PROCESSOR_SHIPPING_ADDRESS_STATUS = "ProcessorShippingAddressStatus";
    public static final String CARDINAL_SIGNATURE_VERIFICATION = "SignatureVerification";
    public static final String CARDINAL_PAYMENT_PROCESSOR_ORDER_NUM = "PaymentProcessorOrderNumber";
    public static final String CARDINAL_PROCESSOR_BILLING_CITY = "ProcessorBillingCity";
    public static final String CARDINAL_PROCESSOR_USER_EMAIL = "ProcessorUserEmail";
    public static final String CARDINAL_PROCESSOR_BILLING_COUNTRY_CODE = "ProcessorBillingCountryCode";
    public static final String CARDINAL_TAX_AMOUNT = "TaxAmount";
    public static final String CARDINAL_PROCESSOR_BILLING_ADDRESS_STATUS = "ProcessorBillingAddressStatus";
    public static final String CARDINAL_PROCESSOR_SHIPPING_ADDRESS1 = "ProcessorShippingAddress1";
    public static final String CARDINAL_PROCESSOR_SHIPPING_ADDRESS2 = "ProcessorShippingAddress2";
    public static final String CARDINAL_IS_FINANCING = "IsFinancing";
    public static final String CARDINAL_INSURANCE_SELECTED = "InsuranceSelected";
    public static final String CARDINAL_PROCESSOR_BILLING_FULL_NAME = "ProcessorBillingFullName";
    public static final String CARDINAL_PROCESSOR_SHIPPING_COUNTRY_CODE  = "ProcessorShippingCountryCode";
    public static final String CARDINAL_FINANCING_TOTAL_COST = "FinancingTotalCost";
    public static final String CARDINAL_PROCESSOR_BILLING_ZIP = "ProcessorBillingPostalCode";
    public static final String CARDINAL_PROCESSOR_MARKETING_EMAIL = "ProcessorMarketingEmail";
    public static final String CARDINAL_PROCESSOR_CONSUMER_MESSAGE= "ProcessorConsumerMessage";
    public static final String CARDINAL_PROCESSOR_MERCHANT_CALLBACK_STATUS = "ProcessorMerchantCallbackSuccess";
    public static final String CARDINAL_SHIPPING_PROMOTION_AMOUNT = "ShippingPromotionAmount";

}
