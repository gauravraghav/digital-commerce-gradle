/**
 * 
 */
package com.digital.commerce.integration.payment;

import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.service.Service;
import com.digital.commerce.integration.payment.cardinal.domain.PaymentServiceRequest;
import com.digital.commerce.integration.payment.cardinal.domain.PaymentServiceResponse;

import lombok.Getter;


/**
 * @author mmallipu
 *
 */
public interface PaymentService extends Service{
	@Getter
	public enum ServiceMethod {
		LOOKUP("fp071"),
		AUTHENTICATE("fp072"),
		ORDER("fp073"),
		AUTHORIZE("fp074");
		
		String serviceMethodName;
		
		ServiceMethod(String serviceMethodName) {
			this.serviceMethodName = serviceMethodName;
		}
	}

    public PaymentServiceResponse lookUp(PaymentServiceRequest expressCheckoutServiceRequest) throws DigitalIntegrationException;
    public PaymentServiceResponse authenticate(PaymentServiceRequest expressCheckoutServiceRequest) throws DigitalIntegrationException;
    public PaymentServiceResponse order(PaymentServiceRequest expressCheckoutServiceRequest) throws DigitalIntegrationException;
    public PaymentServiceResponse authorize(PaymentServiceRequest expressCheckoutServiceRequest) throws DigitalIntegrationException;
}
