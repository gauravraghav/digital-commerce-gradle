package com.digital.commerce.integration.payment.cardinal;

import static com.digital.commerce.integration.payment.cardinal.domain.CardinalMessageKey.*;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.common.service.client.ServiceRequest;
import com.digital.commerce.integration.common.service.client.ServiceRequest.ServiceContentType;
import com.digital.commerce.integration.common.service.client.ServiceRequest.ServiceHttpMethod;
import com.digital.commerce.integration.payment.PaymentService;
import com.digital.commerce.integration.payment.cardinal.domain.CardinalMPI;
import com.digital.commerce.integration.payment.cardinal.domain.PaymentServiceRequest;
import com.digital.commerce.integration.payment.cardinal.domain.PaymentServiceResponse;
import com.digital.commerce.integration.util.PropertyUtil;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 */
@Setter
@Getter
public class CardinalPaymentService extends BaseIntegrationService
		implements PaymentService {

	private static final String DEFAULT_CURRENCY_CODE = "840";
	private static final String DEFAULT_TRANSACTION_ACTION = "Order";
	private static final String DEFAULT_RECURRING = "N";

	private static final String SD_MSGTYPE = "msgType";
	private static final String SD_VERSION = "version";
	private static final String SD_PROCESSORID = "processorId";
	private static final String SD_MERCHANTID = "merchantId";
	private static final String SD_TRANSACTIONTYPE = "transactionType";

	private IntegrationServiceClient serviceClient;
	private DigitalLogger log = DigitalLogger.getLogger(CardinalPaymentService.class);

	public CardinalPaymentService() {
	}

	@Override
	public void postStartup() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void postShutdown() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getService() {
		return WebServicesConfig.ServicNames.CARDINAL.getName();
	}

	@Override
	public PaymentServiceResponse lookUp(PaymentServiceRequest paymentServiceRequest) throws DigitalIntegrationException {
		String serviceMethodName = ServiceMethod.LOOKUP.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {
			startPerformanceMonitorOperation(serviceMethodName);

			try {
				PaymentServiceResponse paymentServiceResponse;

				if (log.isDebugEnabled()) {
					log.debug("Processing lookUp request # " + paymentServiceRequest.getOrderNumber());
				}

				CardinalMPI cardinalMPI = new CardinalMPI();

				cardinalMPI.add(CARDINAL_MSG_TYPE, CardinalMPI.MsgType.CMPI_LOOKUP.getMsgType());
				cardinalMPI.add(CARDINAL_VERSION, getVersion());
				cardinalMPI.add(CARDINAL_PROCESSOR_ID, getProcessorId());
				cardinalMPI.add(CARDINAL_MERCHANT_ID, getMerchantId());
				cardinalMPI.add(CARDINAL_TRANSACTION_PWD, getTransactionPwd());
				cardinalMPI.add(CARDINAL_TRANSACTION_TYPE, getTransactionType());
				cardinalMPI.add(CARDINAL_FORCE_ADDRESS, paymentServiceRequest.getForceAddress());
				cardinalMPI.add(CARDINAL_NO_SHIPPING, paymentServiceRequest.getNoShipping());
				cardinalMPI.add(CARDINAL_OVERRIDE_ADDRESS, paymentServiceRequest.getOverrideAddress());

				if (paymentServiceRequest.getTransactionMode() != null)
					cardinalMPI.add(CARDINAL_TRANSACTION_MODE, paymentServiceRequest.getTransactionMode().getValue());

				if (DigitalStringUtil.isNotBlank(paymentServiceRequest.getOrderNumber())
						&& paymentServiceRequest.getOrderNumber().length() <= 50) {
					cardinalMPI.add(CARDINAL_ORDER_NUM, paymentServiceRequest.getOrderNumber());
				}
				if (DigitalStringUtil.isNotBlank(paymentServiceRequest.getOrderDescription())
						&& paymentServiceRequest.getOrderDescription().length() <= 125) {
					cardinalMPI.add(CARDINAL_ORDER_DESC, paymentServiceRequest.getOrderDescription());
				}

				if (paymentServiceRequest.getAmount() != null)
					cardinalMPI.add(CARDINAL_AMOUNT, getAmountInString(paymentServiceRequest.getAmount()));

				if (DigitalStringUtil.isNotBlank(paymentServiceRequest.getCurrencyCode())
						&& paymentServiceRequest.getCurrencyCode().length() == 3)
					cardinalMPI.add(CARDINAL_CURRENCY_CODE, paymentServiceRequest.getCurrencyCode());
				else
					cardinalMPI.add(CARDINAL_CURRENCY_CODE, DEFAULT_CURRENCY_CODE);

				if (paymentServiceRequest.getItemAmount() != null)
					cardinalMPI.add(CARDINAL_ITEM_AMOUNT, getAmountInString(paymentServiceRequest.getItemAmount()));
				cardinalMPI.add(CARDINAL_TRANSACTION_ACTION, DEFAULT_TRANSACTION_ACTION);
				/*
				 * cardinalMPI.add(CARDINAL_CARD_NUMBER,
				 * paymentServiceRequest.getCardNumber());
				 * cardinalMPI.add(CARDINAL_EXP_MONTH,
				 * paymentServiceRequest.getCardExpMonth());
				 * cardinalMPI.add(CARDINAL_EXP_YEAR,
				 * paymentServiceRequest.getCardExpYear());
				 */

				if (DigitalStringUtil.isNotBlank(paymentServiceRequest.getRecurring()))
					cardinalMPI.add(CARDINAL_RECURRING, paymentServiceRequest.getRecurring());
				else
					cardinalMPI.add(CARDINAL_RECURRING, DEFAULT_RECURRING);

				cardinalMPI.add(CARDINAL_MERCHANT_DATA, paymentServiceRequest.getMerchantData());
				cardinalMPI.add(CARDINAL_MERCHANT_CONSUME_MSG, paymentServiceRequest.getMerchantConsumerMessage());
				cardinalMPI.add(CARDINAL_IP_ADDRESS, paymentServiceRequest.getIpAddress());

				/*
				 * if(paymentServiceRequest.getBillingAddress() != null &&
				 * DigitalStringUtil.isNotBlank(paymentServiceRequest.
				 * getBillingAddress().getAddress1())){
				 */
				cardinalMPI.add(CARDINAL_BILLING_FIRST_NAME, paymentServiceRequest.getBillingAddress().getFirstName());
				cardinalMPI.add(CARDINAL_BILLING_LAST_NAME, paymentServiceRequest.getBillingAddress().getLastName());
				cardinalMPI.add(CARDINAL_BILLING_ADDRESS1, paymentServiceRequest.getBillingAddress().getAddress1());
				cardinalMPI.add(CARDINAL_BILLING_ADDRESS2, paymentServiceRequest.getBillingAddress().getAddress2());
				cardinalMPI.add(CARDINAL_BILLING_CITY, paymentServiceRequest.getBillingAddress().getCity());
				cardinalMPI.add(CARDINAL_BILLING_STATE, paymentServiceRequest.getBillingAddress().getState());
				cardinalMPI.add(CARDINAL_BILLING_POSTAL_CODE,
						paymentServiceRequest.getBillingAddress().getPostalCode());
				cardinalMPI.add(CARDINAL_BILLING_COUNTRY_CODE,
						paymentServiceRequest.getBillingAddress().getCountryCode());
				cardinalMPI.add(CARDINAL_BILLING_PHONE, paymentServiceRequest.getBillingAddress().getPhone());
				/* } */

				/*
				 * if(paymentServiceRequest.getShippingAddress() != null &&
				 * DigitalStringUtil.isNotBlank(paymentServiceRequest.
				 * getShippingAddress().getAddress1())){
				 */ cardinalMPI.add(CARDINAL_SHIPPING_FIRST_NAME,
						paymentServiceRequest.getShippingAddress().getFirstName());
				cardinalMPI.add(CARDINAL_SHIPPING_LAST_NAME, paymentServiceRequest.getShippingAddress().getLastName());
				cardinalMPI.add(CARDINAL_SHIPPING_ADDRESS1, paymentServiceRequest.getShippingAddress().getAddress1());
				cardinalMPI.add(CARDINAL_SHIPPING_ADDRESS2, paymentServiceRequest.getShippingAddress().getAddress2());
				cardinalMPI.add(CARDINAL_SHIPPING_CITY, paymentServiceRequest.getShippingAddress().getCity());
				cardinalMPI.add(CARDINAL_SHIPPING_STATE, paymentServiceRequest.getShippingAddress().getState());
				cardinalMPI.add(CARDINAL_SHIPPING_POSTAL_CODE,
						paymentServiceRequest.getShippingAddress().getPostalCode());
				cardinalMPI.add(CARDINAL_SHIPPING_COUNTRY_CODE,
						paymentServiceRequest.getShippingAddress().getCountryCode());
				cardinalMPI.add(CARDINAL_SHIPPING_PHONE, paymentServiceRequest.getShippingAddress().getPhone());
				/* } */
				cardinalMPI.add(CARDINAL_EMAIL, paymentServiceRequest.getEmail());

				String xmlString = serviceClient.getMarshalledStringFromObject(CardinalMPI.class, cardinalMPI);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri() + urlEncodeString(xmlString),
						ServiceContentType.XML, ServiceHttpMethod.GET, serviceMethodName);
				serviceRequest.setBody(xmlString);

				HttpServiceResponse serviceResponse = serviceClient.processHttpRequest(serviceRequest);

				CardinalMPI cardinalMPIResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(CardinalMPI.class, serviceResponse);

				paymentServiceResponse = extractPaymentServiceResponse(cardinalMPIResponse);

				return paymentServiceResponse;

			} catch (Exception e2) {
				hasException = true;
				log.error(e2.getMessage(), e2);

				throw new DigitalIntegrationBusinessException("Lookup Error", ErrorCodes.COMM_ERROR.getCode(), e2);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(
					String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}

	}

	@Override
	public PaymentServiceResponse authenticate(PaymentServiceRequest paymentServiceRequest)
			throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.AUTHENTICATE.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {
			PaymentServiceResponse paymentServiceResponse;

			startPerformanceMonitorOperation(serviceMethodName);

			try {

				if (log.isDebugEnabled()) {
					log.debug("Processing authenticate request # " + paymentServiceRequest.getOrderNumber());
				}

				CardinalMPI cardinalMPI = new CardinalMPI();

				cardinalMPI.add(CARDINAL_MSG_TYPE, CardinalMPI.MsgType.CMPI_AUTHENTICATE.getMsgType());
				cardinalMPI.add(CARDINAL_ORDER_ID, paymentServiceRequest.getOrderId());
				cardinalMPI.add(CARDINAL_VERSION, getVersion());
				cardinalMPI.add(CARDINAL_PROCESSOR_ID, getProcessorId());
				cardinalMPI.add(CARDINAL_MERCHANT_ID, getMerchantId());
				cardinalMPI.add(CARDINAL_TRANSACTION_PWD, getTransactionPwd());
				cardinalMPI.add(CARDINAL_TRANSACTION_TYPE, getTransactionType());

				cardinalMPI.add(CARDINAL_PA_RES_PAYLOAD, paymentServiceRequest.getPayload());

				String xmlString = serviceClient.getMarshalledStringFromObject(CardinalMPI.class, cardinalMPI);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri() + urlEncodeString(xmlString),
						ServiceContentType.XML, ServiceHttpMethod.GET, serviceMethodName);

				HttpServiceResponse serviceResponse = serviceClient.processHttpRequest(serviceRequest);

				CardinalMPI cardinalMPIResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(CardinalMPI.class, serviceResponse);

				paymentServiceResponse = extractPaymentServiceResponse(cardinalMPIResponse);

				return paymentServiceResponse;

			} catch (Exception e2) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("Authenticate Error", ErrorCodes.COMM_ERROR.getCode(), e2);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(
					String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}

	}

	/**
	 * Method to handle PayPal order service requests
	 * 
	 * @param paymentServiceRequest
	 *            PaymentServiceRequest
	 * @return PaymentServiceResponse
	 * @throws DigitalIntegrationSystemException
	 */

	@Override
	public PaymentServiceResponse order(PaymentServiceRequest paymentServiceRequest) throws DigitalIntegrationException {
		String serviceMethodName = ServiceMethod.ORDER.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {
			PaymentServiceResponse paymentServiceResponse;

			startPerformanceMonitorOperation(serviceMethodName);

			try {
				if (log.isDebugEnabled()) {
					log.debug(
							"Processing Cardinal order request for Order # " + paymentServiceRequest.getOrderNumber());
				}

				CardinalMPI cardinalMPI = new CardinalMPI();
				cardinalMPI.add(CARDINAL_MSG_TYPE, CardinalMPI.MsgType.CMPI_ORDER.getMsgType());
				cardinalMPI.add(CARDINAL_VERSION, getVersion());
				cardinalMPI.add(CARDINAL_PROCESSOR_ID, getProcessorId());
				cardinalMPI.add(CARDINAL_MERCHANT_ID, getMerchantId());
				cardinalMPI.add(CARDINAL_TRANSACTION_PWD, getTransactionPwd());
				cardinalMPI.add(CARDINAL_IP_ADDRESS, paymentServiceRequest.getIpAddress());

				cardinalMPI.add(CARDINAL_ORDER_ID, paymentServiceRequest.getOrderId());
				cardinalMPI.add(CARDINAL_TRANSACTION_TYPE, getTransactionType());

				cardinalMPI.add(CARDINAL_ORDER_NUM, paymentServiceRequest.getOrderNumber());
				cardinalMPI.add(CARDINAL_ORDER_DESC, paymentServiceRequest.getOrderDescription());
				if (paymentServiceRequest.getAmount() != null)
					cardinalMPI.add(CARDINAL_AMOUNT, getAmountInString(paymentServiceRequest.getAmount()));

				if (paymentServiceRequest.getItemAmount() != null)
					cardinalMPI.add(CARDINAL_ITEM_AMOUNT, getAmountInString(paymentServiceRequest.getItemAmount()));

				if (DigitalStringUtil.isNotBlank(paymentServiceRequest.getCurrencyCode())
						&& paymentServiceRequest.getCurrencyCode().length() == 3)
					cardinalMPI.add(CARDINAL_CURRENCY_CODE, paymentServiceRequest.getCurrencyCode());
				else
					cardinalMPI.add(CARDINAL_CURRENCY_CODE, DEFAULT_CURRENCY_CODE);

				cardinalMPI.add(CARDINAL_MERCHANT_DATA, paymentServiceRequest.getMerchantData());

				cardinalMPI.add(CARDINAL_BILLING_FIRST_NAME, paymentServiceRequest.getBillingAddress().getFirstName());
				cardinalMPI.add(CARDINAL_BILLING_LAST_NAME, paymentServiceRequest.getBillingAddress().getLastName());
				cardinalMPI.add(CARDINAL_BILLING_ADDRESS1, paymentServiceRequest.getBillingAddress().getAddress1());
				cardinalMPI.add(CARDINAL_BILLING_ADDRESS2, paymentServiceRequest.getBillingAddress().getAddress2());
				cardinalMPI.add(CARDINAL_BILLING_CITY, paymentServiceRequest.getBillingAddress().getCity());
				cardinalMPI.add(CARDINAL_BILLING_STATE, paymentServiceRequest.getBillingAddress().getState());
				cardinalMPI.add(CARDINAL_BILLING_POSTAL_CODE,
						paymentServiceRequest.getBillingAddress().getPostalCode());
				cardinalMPI.add(CARDINAL_BILLING_COUNTRY_CODE,
						paymentServiceRequest.getBillingAddress().getCountryCode());
				cardinalMPI.add(CARDINAL_BILLING_PHONE, paymentServiceRequest.getBillingAddress().getPhone());

				cardinalMPI.add(CARDINAL_SHIPPING_FIRST_NAME,
						paymentServiceRequest.getShippingAddress().getFirstName());
				cardinalMPI.add(CARDINAL_SHIPPING_LAST_NAME, paymentServiceRequest.getShippingAddress().getLastName());
				cardinalMPI.add(CARDINAL_SHIPPING_ADDRESS1, paymentServiceRequest.getShippingAddress().getAddress1());
				cardinalMPI.add(CARDINAL_SHIPPING_ADDRESS2, paymentServiceRequest.getShippingAddress().getAddress2());
				cardinalMPI.add(CARDINAL_SHIPPING_CITY, paymentServiceRequest.getShippingAddress().getCity());
				cardinalMPI.add(CARDINAL_SHIPPING_STATE, paymentServiceRequest.getShippingAddress().getState());
				cardinalMPI.add(CARDINAL_SHIPPING_POSTAL_CODE,
						paymentServiceRequest.getShippingAddress().getPostalCode());
				cardinalMPI.add(CARDINAL_SHIPPING_COUNTRY_CODE,
						paymentServiceRequest.getShippingAddress().getCountryCode());
				cardinalMPI.add(CARDINAL_SHIPPING_PHONE, paymentServiceRequest.getShippingAddress().getPhone());
				cardinalMPI.add(CARDINAL_EMAIL, paymentServiceRequest.getEmail());

				String requestBody = serviceClient.getMarshalledStringFromObject(CardinalMPI.class, cardinalMPI);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri() + urlEncodeString(requestBody),
						ServiceRequest.ServiceContentType.XML, ServiceRequest.ServiceHttpMethod.GET, serviceMethodName);

				HttpServiceResponse httpResponse = serviceClient.processHttpRequest(serviceRequest);
				validateHttpResponse(httpResponse);

				CardinalMPI cardinalMPIResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(CardinalMPI.class, httpResponse);
				paymentServiceResponse = extractPaymentServiceResponse(cardinalMPIResponse);

				return paymentServiceResponse;

			} catch (Exception e2) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("Order Error", ErrorCodes.COMM_ERROR.getCode(), e2);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(
					String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}

	/**
	 * Method to handle PayPal authorize service requests
	 * 
	 * @param paymentServiceRequest
	 *            PaymentServiceRequest
	 * @return PaymentServiceResponse
	 * @throws DigitalIntegrationSystemException
	 */

	@Override
	public PaymentServiceResponse authorize(PaymentServiceRequest paymentServiceRequest)
			throws DigitalIntegrationException {
		String serviceMethodName = ServiceMethod.AUTHORIZE.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {
			PaymentServiceResponse paymentServiceResponse;

			startPerformanceMonitorOperation(serviceMethodName);

			try {
				if (log.isDebugEnabled()) {
					log.debug("Processing authorization for order # " + paymentServiceRequest.getOrderNumber());
				}

				CardinalMPI cardinalMPI = new CardinalMPI();

				cardinalMPI.add(CARDINAL_MSG_TYPE, CardinalMPI.MsgType.CMPI_AUTHORIZE.getMsgType());
				cardinalMPI.add(CARDINAL_VERSION, getVersion());
				cardinalMPI.add(CARDINAL_PROCESSOR_ID, getProcessorId());
				cardinalMPI.add(CARDINAL_MERCHANT_ID, getMerchantId());
				cardinalMPI.add(CARDINAL_TRANSACTION_PWD, getTransactionPwd());
				cardinalMPI.add(CARDINAL_TRANSACTION_TYPE, getTransactionType());
				cardinalMPI.add(CARDINAL_TRANSACTION_ID, paymentServiceRequest.getTransactionId());

				cardinalMPI.add(CARDINAL_ORDER_ID, paymentServiceRequest.getOrderId());
				if (paymentServiceRequest.getAmount() != null)
					cardinalMPI.add(CARDINAL_AMOUNT, getAmountInString(paymentServiceRequest.getAmount()));

				if (DigitalStringUtil.isNotBlank(paymentServiceRequest.getCurrencyCode())
						&& paymentServiceRequest.getCurrencyCode().length() == 3)
					cardinalMPI.add(CARDINAL_CURRENCY_CODE, paymentServiceRequest.getCurrencyCode());
				else
					cardinalMPI.add(CARDINAL_CURRENCY_CODE, DEFAULT_CURRENCY_CODE);

				if (paymentServiceRequest.getItemAmount() != null)
					cardinalMPI.add(CARDINAL_ITEM_AMOUNT, getAmountInString(paymentServiceRequest.getItemAmount()));
				cardinalMPI.add(CARDINAL_ORDER_NUM, paymentServiceRequest.getOrderNumber());
				cardinalMPI.add(CARDINAL_ORDER_DESC, paymentServiceRequest.getOrderDescription());
				cardinalMPI.add(CARDINAL_MERCHANT_DATA, "");
				cardinalMPI.add(CARDINAL_EMAIL, paymentServiceRequest.getEmail());
				cardinalMPI.add(CARDINAL_IP_ADDRESS, paymentServiceRequest.getIpAddress());

				cardinalMPI.add(CARDINAL_BILLING_FIRST_NAME, paymentServiceRequest.getBillingAddress().getFirstName());
				cardinalMPI.add(CARDINAL_BILLING_LAST_NAME, paymentServiceRequest.getBillingAddress().getLastName());
				cardinalMPI.add(CARDINAL_BILLING_ADDRESS1, paymentServiceRequest.getBillingAddress().getAddress1());
				cardinalMPI.add(CARDINAL_BILLING_ADDRESS2, paymentServiceRequest.getBillingAddress().getAddress2());
				cardinalMPI.add(CARDINAL_BILLING_STATE, paymentServiceRequest.getBillingAddress().getState());
				cardinalMPI.add(CARDINAL_BILLING_CITY, paymentServiceRequest.getBillingAddress().getCity());
				cardinalMPI.add(CARDINAL_BILLING_POSTAL_CODE,
						paymentServiceRequest.getBillingAddress().getPostalCode());
				cardinalMPI.add(CARDINAL_BILLING_COUNTRY_CODE,
						paymentServiceRequest.getBillingAddress().getCountryCode());
				cardinalMPI.add(CARDINAL_BILLING_PHONE, paymentServiceRequest.getBillingAddress().getPhone());

				cardinalMPI.add(CARDINAL_SHIPPING_FIRST_NAME,
						paymentServiceRequest.getShippingAddress().getFirstName());
				cardinalMPI.add(CARDINAL_SHIPPING_LAST_NAME, paymentServiceRequest.getShippingAddress().getLastName());
				cardinalMPI.add(CARDINAL_SHIPPING_ADDRESS1, paymentServiceRequest.getShippingAddress().getAddress1());
				cardinalMPI.add(CARDINAL_SHIPPING_ADDRESS2, paymentServiceRequest.getShippingAddress().getAddress2());
				cardinalMPI.add(CARDINAL_SHIPPING_STATE, paymentServiceRequest.getShippingAddress().getState());
				cardinalMPI.add(CARDINAL_SHIPPING_CITY, paymentServiceRequest.getShippingAddress().getCity());
				cardinalMPI.add(CARDINAL_SHIPPING_POSTAL_CODE,
						paymentServiceRequest.getShippingAddress().getPostalCode());
				cardinalMPI.add(CARDINAL_SHIPPING_COUNTRY_CODE,
						paymentServiceRequest.getShippingAddress().getCountryCode());
				cardinalMPI.add(CARDINAL_SHIPPING_PHONE, paymentServiceRequest.getShippingAddress().getPhone());

				String requestBody = serviceClient.getMarshalledStringFromObject(CardinalMPI.class, cardinalMPI);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri() + urlEncodeString(requestBody),
						ServiceContentType.XML, ServiceRequest.ServiceHttpMethod.GET, serviceMethodName);

				HttpServiceResponse httpResponse = serviceClient.processHttpRequest(serviceRequest);
				validateHttpResponse(httpResponse);

				CardinalMPI cardinalMPIResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(CardinalMPI.class, httpResponse);
				paymentServiceResponse = extractPaymentServiceResponse(cardinalMPIResponse);

				return paymentServiceResponse;

			} catch (Exception e2) {
				hasException = true;

				throw new DigitalIntegrationBusinessException("Authorization Error", ErrorCodes.COMM_ERROR.getCode(), e2);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			throw new DigitalIntegrationInactiveException(
					String.format("Service / Method is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}
	}

	public Map<String, String> getStaticData() {
		return getMapData(WebServicesConfig.DataTypes.STATIC);
	}

	public String getDefaultUri() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, "defaultUri") + "=";
	}

	public String getMsgType() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_MSGTYPE).toString();
	}

	public void setMsgType(String msgType) {
		PropertyUtil.setMappedProperty(getStaticData(), SD_MSGTYPE, msgType);
	}

	public String getVersion() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_VERSION).toString();
	}

	public void setVersion(String version) {
		PropertyUtil.setMappedProperty(getStaticData(), SD_VERSION, version);
	}

	public String getProcessorId() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_PROCESSORID).toString();
	}

	public void setProcessorId(String processorId) {
		PropertyUtil.setMappedProperty(getStaticData(), SD_PROCESSORID, processorId);
	}

	public String getMerchantId() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_MERCHANTID).toString();
	}

	public void setMerchantId(String merchantId) {
		PropertyUtil.setMappedProperty(getStaticData(), SD_MERCHANTID, merchantId);
	}

	public String getTransactionPwd() {
		return getWebServicesConfig().getVaultedData(getMapData(WebServicesConfig.DataTypes.STATIC, "transactionPwd"));
	}

	public String getTransactionType() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_TRANSACTIONTYPE).toString();
	}

	public void setTransactionType(String transactionType) {
		PropertyUtil.setMappedProperty(getStaticData(), SD_TRANSACTIONTYPE, transactionType);
	}

	/**
	 * Extracts PaymentServiceResponse from CardinalMPI response
	 * 
	 * @param cardinalMPIResponse
	 *            CardinalMPI
	 * @return PaymentServiceResponse
	 */

	private PaymentServiceResponse extractPaymentServiceResponse(CardinalMPI cardinalMPIResponse) {

		PaymentServiceResponse paymentServiceResponse = new PaymentServiceResponse();

		paymentServiceResponse.setAuthorizationCode(cardinalMPIResponse.get(CARDINAL_RESPONSE_AUTHORIZATION_CODE));
		paymentServiceResponse.setStatusCode(cardinalMPIResponse.get(CARDINAL_RESPONSE_STATUS_CODE));
		paymentServiceResponse.setEligibleForProtection(cardinalMPIResponse.get(CARDINAL_RESPONSE_EFP));
		paymentServiceResponse.setReasonCode(cardinalMPIResponse.get(CARDINAL_RESPONSE_REASON_CODE));
		paymentServiceResponse.setErrorDesc(cardinalMPIResponse.get(CARDINAL_RESPONSE_ERR_DESC));
		paymentServiceResponse.setProcessorTransactionId(cardinalMPIResponse.get(CARDINAL_RESPONSE_PROCESSOR_TID));
		paymentServiceResponse.setErrorNo(cardinalMPIResponse.get(CARDINAL_RESPONSE_ERR_NO));
		paymentServiceResponse.setCardCodeResult(cardinalMPIResponse.get(CARDINAL_RESPONSE_CC_RESULT));
		paymentServiceResponse.setTransactionId(cardinalMPIResponse.get(CARDINAL_RESPONSE_TRANSACTION_ID));
		paymentServiceResponse.setOrderNumber(cardinalMPIResponse.get(CARDINAL_RESPONSE_ORDER_NUM));
		paymentServiceResponse.setMerchantData(cardinalMPIResponse.get(CARDINAL_RESPONSE_MERCHANT_DATA));
		paymentServiceResponse.setReasonDesc(cardinalMPIResponse.get(CARDINAL_RESPONSE_REASON_DESC));
		paymentServiceResponse.setAvsResult(cardinalMPIResponse.get(CARDINAL_RESPONSE_AVS_RESULT));
		paymentServiceResponse.setOrderId(cardinalMPIResponse.get(CARDINAL_RESPONSE_ORDER_ID));
		paymentServiceResponse.setProcessorOrderNum(cardinalMPIResponse.get(CARDINAL_PROCESSOR_ORDER_NUM));
		paymentServiceResponse.setAcsUrl(cardinalMPIResponse.get(CARDINAL_RESPONSE_ACS_URL));
		paymentServiceResponse.setEnrolled(cardinalMPIResponse.get(CARDINAL_RESPONSE_ENROLLED));
		paymentServiceResponse.setPayload(cardinalMPIResponse.get(CARDINAL_RESPONSE_PAYLOAD));
		paymentServiceResponse.setShippingPromotionAmount(cardinalMPIResponse.get(CARDINAL_SHIPPING_PROMOTION_AMOUNT));
		paymentServiceResponse.setProcessorMerchantCallbackSuccess(
				cardinalMPIResponse.get(CARDINAL_PROCESSOR_MERCHANT_CALLBACK_STATUS));
		paymentServiceResponse
				.setProcessorConsumerMessage(cardinalMPIResponse.get(CARDINAL_PROCESSOR_CONSUMER_MESSAGE));
		paymentServiceResponse.setProcessorMarketingEmail(cardinalMPIResponse.get(CARDINAL_PROCESSOR_MARKETING_EMAIL));
		paymentServiceResponse.setProcessorBillingPostalCode(cardinalMPIResponse.get(CARDINAL_PROCESSOR_BILLING_ZIP));
		paymentServiceResponse.setFinancingTotalCost(cardinalMPIResponse.get(CARDINAL_FINANCING_TOTAL_COST));
		paymentServiceResponse.setItemAmount(cardinalMPIResponse.get(CARDINAL_ITEM_AMOUNT));
		paymentServiceResponse
				.setProcessorShippingCountryCode(cardinalMPIResponse.get(CARDINAL_PROCESSOR_SHIPPING_COUNTRY_CODE));
		paymentServiceResponse
				.setProcessorBillingFullName(cardinalMPIResponse.get(CARDINAL_PROCESSOR_BILLING_FULL_NAME));
		paymentServiceResponse.setInsuranceSelected(cardinalMPIResponse.get(CARDINAL_INSURANCE_SELECTED));
		paymentServiceResponse.setIsFinancing(cardinalMPIResponse.get(CARDINAL_IS_FINANCING));
		paymentServiceResponse
				.setProcessorShippingAddress1(cardinalMPIResponse.get(CARDINAL_PROCESSOR_SHIPPING_ADDRESS1));
		paymentServiceResponse
				.setProcessorShippingAddress2(cardinalMPIResponse.get(CARDINAL_PROCESSOR_SHIPPING_ADDRESS2));
		paymentServiceResponse
				.setProcessorBillingAddressStatus(cardinalMPIResponse.get(CARDINAL_PROCESSOR_BILLING_ADDRESS_STATUS));
		paymentServiceResponse.setTaxAmount(cardinalMPIResponse.get(CARDINAL_TAX_AMOUNT));
		paymentServiceResponse
				.setProcessorBillingCountryCode(cardinalMPIResponse.get(CARDINAL_PROCESSOR_BILLING_COUNTRY_CODE));
		paymentServiceResponse.setProcessorUserEmail(cardinalMPIResponse.get(CARDINAL_PROCESSOR_USER_EMAIL));
		paymentServiceResponse.setProcessorBillingCity(cardinalMPIResponse.get(CARDINAL_PROCESSOR_BILLING_CITY));
		paymentServiceResponse
				.setPaymentProcessorOrderNumber(cardinalMPIResponse.get(CARDINAL_PAYMENT_PROCESSOR_ORDER_NUM));
		paymentServiceResponse.setSignatureVerification(cardinalMPIResponse.get(CARDINAL_SIGNATURE_VERIFICATION));
		paymentServiceResponse
				.setProcessorShippingAddressStatus(cardinalMPIResponse.get(CARDINAL_PROCESSOR_SHIPPING_ADDRESS_STATUS));
		paymentServiceResponse
				.setProcessorShippingFullName(cardinalMPIResponse.get(CARDINAL_PROCESSOR_SHIPPING_FULL_NAME));
		paymentServiceResponse.setHandlingAmount(cardinalMPIResponse.get(CARDINAL_HANDLING_AMOUNT));
		paymentServiceResponse.setProcessorUserFirstName(cardinalMPIResponse.get(CARDINAL_PROCESSOR_USER_FIRST_NAME));
		paymentServiceResponse.setProcessorUserLastName(cardinalMPIResponse.get(CARDINAL_PROCESSOR_USER_LAST_NAME));
		paymentServiceResponse.setProcessorShippingPhone(cardinalMPIResponse.get(CARDINAL_PROCESSOR_SHIPPING_PHONE));
		paymentServiceResponse.setProcessorShippingPostalCode(cardinalMPIResponse.get(CARDINAL_PROCESSOR_SHIPPING_ZIP));
		paymentServiceResponse.setFinancingTerm(cardinalMPIResponse.get(CARDINAL_FINANCING_TERM));
		paymentServiceResponse.setProcessorUserStatus(cardinalMPIResponse.get(CARDINAL_PROCESSOR_USER_STATUS));
		paymentServiceResponse.setProcessorShippingState(cardinalMPIResponse.get(CARDINAL_PROCESSOR_SHIPPING_STATE));
		paymentServiceResponse.setShippingMethod(cardinalMPIResponse.get(CARDINAL_SHIPPING_METHOD));
		paymentServiceResponse.setInsuranceAmount(cardinalMPIResponse.get(CARDINAL_INSURANCE_AMOUNT));
		paymentServiceResponse.setCartChangeTolerance(cardinalMPIResponse.get(CARDINAL_CART_CHNAGE_TOLERANCE));
		paymentServiceResponse.setFinancingMonthlyPayment(cardinalMPIResponse.get(CARDINAL_FINANCING_MONTHLY_PAYMENT));
		paymentServiceResponse.setCurrencyCode(cardinalMPIResponse.get(CARDINAL_CURRENCY_CODE));
		paymentServiceResponse.setProcessorUserId(cardinalMPIResponse.get(CARDINAL_PROCESSOR_USER_ID));
		paymentServiceResponse.setProcessorShippingCity(cardinalMPIResponse.get(CARDINAL_PROCESSOR_SHIPPING_CITY));
		paymentServiceResponse.setOrderTotal(cardinalMPIResponse.get(CARDINAL_ORDER_TOTAL));
		paymentServiceResponse.setFinancingFeeAmount(cardinalMPIResponse.get(CARDINAL_FINANCING_FEE_AMOUNT));
		paymentServiceResponse
				.setProcessorBillingAddress2(cardinalMPIResponse.get(CARDINAL_PROCESSOR_BILLING_ADDRESS2));
		paymentServiceResponse.setMerchantReferenceNumber(cardinalMPIResponse.get(CARDINAL_MERCHANT_REF_NUMBER));
		paymentServiceResponse
				.setProcessorBillingAddress1(cardinalMPIResponse.get(CARDINAL_PROCESSOR_BILLING_ADDRESS1));
		paymentServiceResponse.setProcessorBillingPhone(cardinalMPIResponse.get(CARDINAL_PROCESSOR_BILLING_PHONE));
		paymentServiceResponse.setProcessorBillingState(cardinalMPIResponse.get(CARDINAL_PROCESSOR_BILLING_STATE));
		paymentServiceResponse.setShippingAmount(cardinalMPIResponse.get(CARDINAL_SHIPPING_AMOUNT));

		if (cardinalMPIResponse.get(CARDINAL_PA_RES_STATUS) != null)
			paymentServiceResponse.setPAResStatus(
					PaymentServiceResponse.PAResStatus.valueOf(cardinalMPIResponse.get(CARDINAL_PA_RES_STATUS)));

		return paymentServiceResponse;

	}

	private String getAmountInString(Double amount) {
		NumberFormat nf = DecimalFormat.getInstance();
		nf.setMaximumFractionDigits(0);
		nf.setGroupingUsed(false);

		amount *= 100;

		String stAmount = nf.format(amount);
		return stAmount;
	}
	
    public Map<String, Boolean> getServiceMethodsEnabled(){
		Map<String, Boolean> methods = new HashMap<>();
		for (ServiceMethod service: ServiceMethod.values()) {
			methods.put(service.getServiceMethodName(), isServiceMethodEnabled(service.getServiceMethodName()));
		}
		
		return methods;
    }
	
}
