package com.digital.commerce.integration.payment.cardinal.domain;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import com.digital.commerce.integration.common.service.client.NVP;

@Getter
@Setter
public class CardinalMPI implements NVP {

	@Getter
	public enum MsgType {
		CMPI_LOOKUP("cmpi_lookup"),
		CMPI_AUTHENTICATE("cmpi_authenticate"),
		CMPI_AUTHORIZE("cmpi_authorize"),
		CMPI_ORDER("cmpi_order");

		private String msgType;
		MsgType(String msgType) {this.msgType = msgType;}
	}

	private Map<String, String> nameValuePairs = new HashMap<>();

	public Map<String, String> getNameValuePairs() {
		return nameValuePairs;
	}

	public void setNameValuePairs(Map<String, String> nameValuePairs) {
		this.nameValuePairs = nameValuePairs;
	}
	
	public void add(String name, String value) {
		nameValuePairs.put(name, value);
	}
	
	public String get(String name) {
		if (nameValuePairs.containsKey(name)) {
			return nameValuePairs.get(name);
		} else {
			return null;
		}
	}

}
