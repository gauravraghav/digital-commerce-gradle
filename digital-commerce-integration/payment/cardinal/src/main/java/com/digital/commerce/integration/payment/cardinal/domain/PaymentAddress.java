package com.digital.commerce.integration.payment.cardinal.domain;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

import com.digital.commerce.integration.common.domain.Address;

@Getter
@Setter
public class PaymentAddress extends Address implements Serializable {

	private static final long serialVersionUID = 1L;

	private String firstName = "";
    private String lastName = "";
    
}
