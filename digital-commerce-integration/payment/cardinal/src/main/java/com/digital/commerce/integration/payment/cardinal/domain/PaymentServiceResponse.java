/**
 * 
 */
package com.digital.commerce.integration.payment.cardinal.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class PaymentServiceResponse {

	@Getter
	public enum PAResStatus{
		Y("SUCCESS"),
		X("CANCELED"),
		E("ERROR"),
		U("UNAVAILABLE");
		
		private String value;

		private PAResStatus(String value) {
			this.value = value;
		}
	}
	
	private PAResStatus pAResStatus;
	
	private String payload;

	private String acsUrl;

	private String enrolled;
	
	private String termURL;

	private String statusCode;

	private String referenceId;

	private String errorNo;

	private String orderId;

	private String errorDesc;

	private String reasonCode;

	private String reasonDesc;

	private String transactionId;

	private String authorizationCode;

	private String eligibleForProtection;

	private String processorTransactionId;

	private String cardCodeResult;

	private String orderNumber;

	private String merchantData;

	private String avsResult;

	private String processorOrderNum;

	private String shippingPromotionAmount;
	private String processorMerchantCallbackSuccess;
	private String processorConsumerMessage;
	private String processorMarketingEmail;
	private String processorBillingPostalCode; 
	private String financingTotalCost;
	private String itemAmount;
	private String processorShippingCountryCode;
	private String processorBillingFullName;
	private String insuranceSelected;
	private String isFinancing;
	private String processorShippingAddress1;
	private String processorShippingAddress2;
	private String processorBillingAddressStatus;
	private String taxAmount;
	private String processorBillingCountryCode;
	private String processorUserEmail;
	private String processorBillingCity;
	private String paymentProcessorOrderNumber;
	private String signatureVerification;
	private String processorShippingAddressStatus;
	private String processorShippingFullName;
	private String handlingAmount;
	private String processorUserFirstName;
	private String processorUserLastName;
	private String processorShippingPhone;
	private String processorShippingPostalCode;
	private String financingTerm;
	private String processorUserStatus;
	private String processorShippingState;
	private String shippingMethod;
	private String insuranceAmount;
	private String cartChangeTolerance;
	private String financingMonthlyPayment;
	private String currencyCode;
	private String processorUserId;
	private String processorShippingCity; 
	private String orderTotal;
	private String financingFeeAmount;
	private String processorBillingAddress2;
	private String merchantReferenceNumber;
	private String processorBillingAddress1;
	private String processorBillingPhone;
	private String processorBillingState;
	private String shippingAmount;

}
