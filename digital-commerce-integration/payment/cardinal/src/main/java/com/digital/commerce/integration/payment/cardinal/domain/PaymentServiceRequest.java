package com.digital.commerce.integration.payment.cardinal.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentServiceRequest implements Serializable {

	@Getter
	public enum TransactionMode {
		MOTO("M"),
		RETAIL("R"),
		E_COMMERCE("S"),
		MOBILE_DEVICE("P");
		
		private String value;

		private TransactionMode(String value) {
			this.value = value;
		}

	}
	
	private static final long serialVersionUID = 1L;

	private String orderNumber = "";

	private String orderDescription = "";

	private Double amount = 0.0;

	private String ipAddress = "";

	private String orderId = "";

	private String currencyCode = "";

	private String cardNumber = "";

	private String cardExpMonth = "";

	private String cardExpYear = "";

	private String email = "";

	private Double itemAmount = 0.0;

	private String merchantData = "";

	private PaymentAddress billingAddress;

	private PaymentAddress shippingAddress;

	private String recurring = "";

	private String transactionId = "";
	
	private String payload = "";

	private String forceAddress;
	
	private String noShipping;
	
	private String overrideAddress;

	private TransactionMode transactionMode;
	
	private String firstName;
	
	private String lastName;
	
	private String merchantConsumerMessage;

	public PaymentAddress getBillingAddress() {
		if (null == billingAddress) {
			return new PaymentAddress();
		}
		return billingAddress;
	}

	public PaymentAddress getShippingAddress() {
		if (null == shippingAddress) {
			return new PaymentAddress();
		}
		return shippingAddress;
	}
}
