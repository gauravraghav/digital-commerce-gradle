package com.digital.commerce.integration.inventory.storenet.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ItemInventory implements Serializable{

	private static final long serialVersionUID = 1L;
	protected BigDecimal itemQuantityAllocated;
	protected BigDecimal itemQuantityMissing;
	protected BigDecimal itemQuantityOnHand;
	protected Short itemQuantityOnHold;
	protected List<ItemStockLevel> itemStockLevels = new ArrayList<>();

}
