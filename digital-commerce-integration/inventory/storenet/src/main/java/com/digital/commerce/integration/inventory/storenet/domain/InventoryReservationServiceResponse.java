/**
 * 
 */
package com.digital.commerce.integration.inventory.storenet.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class InventoryReservationServiceResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	private Boolean approved;
	private ItemInventory itemInventory = new ItemInventory();
	
	public Boolean isApproved() {
		return this.getApproved();
	}
}
