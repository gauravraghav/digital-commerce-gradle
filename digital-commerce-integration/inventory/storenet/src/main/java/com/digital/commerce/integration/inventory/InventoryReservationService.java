/**
 * 
 */
package com.digital.commerce.integration.inventory;

import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.service.Service;
import com.digital.commerce.integration.inventory.storenet.domain.InventoryReservationServiceRequest;
import com.digital.commerce.integration.inventory.storenet.domain.InventoryReservationServiceResponse;

import lombok.Getter;

/**
 * @author mmallipu
 *
 */

public interface InventoryReservationService extends Service{

	@Getter
	public enum ServiceMethod {
		SEARCH("search"),
		ADDHOLD("addHold"),
		RELEASEHOLD("releaseHold");
		
		String serviceMethodName;
		
		ServiceMethod(String serviceMethodName) {
			this.serviceMethodName = serviceMethodName;
		}

	}
	
   public InventoryReservationServiceResponse search(InventoryReservationServiceRequest storenetInventoryServiceRequest) throws DigitalIntegrationException;
   public InventoryReservationServiceResponse addHold(InventoryReservationServiceRequest storenetInventoryServiceRequest) throws DigitalIntegrationException;
   public InventoryReservationServiceResponse releaseHold(InventoryReservationServiceRequest storenetInventoryServiceRequest) throws DigitalIntegrationException;


}
