/**
 * 
 */
package com.digital.commerce.integration.inventory.storenet;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.common.service.client.ServiceRequest;
import com.digital.commerce.integration.common.service.client.ServiceRequest.ServiceHttpMethod;
import com.digital.commerce.integration.inventory.InventoryReservationService;
import com.digital.commerce.integration.inventory.storenet.domain.InventoryReservationServiceRequest;
import com.digital.commerce.integration.inventory.storenet.domain.InventoryReservationServiceResponse;
import com.digital.commerce.integration.util.PropertyUtil;
import com.digital.commerce.integration.external.inventoryreservation.storenet.domain.hold.ItemAddHoldRequest;
import com.digital.commerce.integration.external.inventoryreservation.storenet.domain.hold.ItemAddHoldRequest.ItemLocator;
import com.digital.commerce.integration.external.inventoryreservation.storenet.domain.hold.ItemAddHoldResponse;
import com.digital.commerce.integration.external.inventoryreservation.storenet.domain.hold.ItemAddHoldResponse.Items.ItemEssentials.ItemInventory;
import com.digital.commerce.integration.external.inventoryreservation.storenet.domain.hold.ItemAddHoldResponse.Items.ItemEssentials.ItemInventory.ItemStockLevels.ItemStockLevel;
import com.digital.commerce.integration.external.inventoryreservation.storenet.domain.release.ItemReleaseHoldRequest;
import com.digital.commerce.integration.external.inventoryreservation.storenet.domain.release.ItemReleaseHoldResponse;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class StoreNetInventoryReservationService extends BaseIntegrationService implements InventoryReservationService {
	private static final String ADDHOLD_SOAPACTION = "addHold";
	private static final String RELEASEHOLD_SOAPACTION = "releaseHold";
	
	private IntegrationServiceClient serviceClient;

	public String getUserName() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, "username");
	}

	public String getPassword() {
		return getWebServicesConfig().getVaultedData(getMapData(WebServicesConfig.DataTypes.STATIC, "password"));
	}

	public Map<String, String> getSoapActions() {
		 return getMapData(WebServicesConfig.DataTypes.SOAP_ACTIONS);
	}
	
	public String getAddHoldSOAPAction() {
		return PropertyUtil.getMappedProperty(getSoapActions(), ADDHOLD_SOAPACTION).toString();
	}


	public String getReleaseHoldSOAPAction() {
		return PropertyUtil.getMappedProperty(getSoapActions(), RELEASEHOLD_SOAPACTION).toString();
	}


	public int getExpirationHoldInHours() {
		return Integer.parseInt(getMapData(WebServicesConfig.DataTypes.STATIC, "expirationHoldInHours"));
	}


	@Override
	public void postStartup() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void postShutdown() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getService() {
		return WebServicesConfig.ServicNames.STORENET.getName();
	}
	
	@Override
	public InventoryReservationServiceResponse search(
			InventoryReservationServiceRequest storenetInventoryServiceRequest) throws DigitalIntegrationException {

		InventoryReservationServiceResponse storeNetInventoryReservationServiceResponse = new InventoryReservationServiceResponse();

		return storeNetInventoryReservationServiceResponse;
	}

	@Override
	public InventoryReservationServiceResponse addHold(
			InventoryReservationServiceRequest storenetInventoryServiceRequest) throws DigitalIntegrationException {
		
		String serviceMethodName = ServiceMethod.ADDHOLD.getServiceMethodName();
		boolean hasException = false;
		
		if (doRunService(ServiceMethod.ADDHOLD.getServiceMethodName())) {
			
			startPerformanceMonitorOperation(serviceMethodName);

			try {
				

				com.digital.commerce.integration.external.inventoryreservation.storenet.domain.hold.ObjectFactory objFactory = new com.digital.commerce.integration.external.inventoryreservation.storenet.domain.hold.ObjectFactory();

				GregorianCalendar c = new GregorianCalendar();
				c.setTime(new Date());
				c.add(Calendar.HOUR_OF_DAY, +getExpirationHoldInHours());
				XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

				ItemLocator itemLocator = objFactory.createItemAddHoldRequestItemLocator();
				itemLocator.setSku(storenetInventoryServiceRequest.getSku());

				ItemAddHoldRequest itemAddHoldRequest = objFactory.createItemAddHoldRequest();
				itemAddHoldRequest.setExternalOrderNumber(storenetInventoryServiceRequest.getExternalOrderNumber());
				itemAddHoldRequest.setItemHoldExpiration(xmlGregorianCalendar);
				itemAddHoldRequest.setItemHoldQuantity(storenetInventoryServiceRequest.getItemHoldQuantity());

				if (storenetInventoryServiceRequest.getVendorNumber() != null) {
					itemAddHoldRequest.setVendorNumber(storenetInventoryServiceRequest.getVendorNumber());
				} else {
					itemAddHoldRequest.setVendorNumber(null);
				}

				itemAddHoldRequest.setItemLocator(itemLocator);

				String bodyContent = serviceClient.getMarshalledStringFromObject(ItemAddHoldRequest.class,
						itemAddHoldRequest);

				ServiceRequest serviceRequest = new ServiceRequest(
						getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getAddHoldSOAPAction(),
						ServiceRequest.ServiceContentType.XML, ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setBody(bodyContent);


				HttpServiceResponse serviceResponse = serviceClient.processHttpRequest(serviceRequest, getUserName(),getPassword());

				ItemAddHoldResponse itemAddHoldResponse = new ItemAddHoldResponse();
				itemAddHoldResponse = serviceClient.getUnmarshalledObjectFromHttpServiceResponse(ItemAddHoldResponse.class,
						serviceResponse);

				InventoryReservationServiceResponse inventoryReservationServiceResponse = populateAddHoldResponse(itemAddHoldResponse);

				return inventoryReservationServiceResponse;

			} catch (DatatypeConfigurationException e) {
				hasException = true;
				

				throw new DigitalIntegrationBusinessException("addHold DatatypeConfigurationException Error",
						ErrorCodes.COMM_ERROR.getCode(), e);
			} catch (Exception e) {
				throw new DigitalIntegrationBusinessException("addHold Error", ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}
	}

	protected InventoryReservationServiceResponse populateAddHoldResponse(ItemAddHoldResponse itemAddHoldResponse) {
		InventoryReservationServiceResponse inventoryReservationServiceResponse = new InventoryReservationServiceResponse();
		
		inventoryReservationServiceResponse.setApproved(itemAddHoldResponse.isApproved());
		
		ItemInventory itemInventory = itemAddHoldResponse.getItems().getItemEssentials().getItemInventory();
		
		inventoryReservationServiceResponse.getItemInventory().setItemQuantityAllocated(itemInventory.getItemQuantityAllocated());
		inventoryReservationServiceResponse.getItemInventory().setItemQuantityMissing(itemInventory.getItemQuantityMissing());
		inventoryReservationServiceResponse.getItemInventory().setItemQuantityOnHand(itemInventory.getItemQuantityOnHand());
		inventoryReservationServiceResponse.getItemInventory().setItemQuantityOnHold(itemInventory.getItemQuantityOnHold());
		
		for (ItemStockLevel itemStockLevel: itemInventory.getItemStockLevels().getItemStockLevel() ) {
			com.digital.commerce.integration.inventory.storenet.domain.ItemStockLevel newItemStockLevel = new com.digital.commerce.integration.inventory.storenet.domain.ItemStockLevel();
			
			newItemStockLevel.setItemQuantityAvailable(itemStockLevel.getItemQuantityAvailable());
			newItemStockLevel.setItemSafetyStockCondition(itemStockLevel.getItemSafetyStockCondition());
			newItemStockLevel.setItemSafetyStockQuantity(itemStockLevel.getItemSafetyStockQuantity());
			newItemStockLevel.setItemSafetyStockTheshold(itemStockLevel.getItemSafetyStockTheshold());
			
			inventoryReservationServiceResponse.getItemInventory().getItemStockLevels().add(newItemStockLevel);
		}
		
		return inventoryReservationServiceResponse;
	}
	
	@Override
	public InventoryReservationServiceResponse releaseHold(
			InventoryReservationServiceRequest storenetInventoryServiceRequest) throws DigitalIntegrationException {
		
		String serviceMethodName = ServiceMethod.RELEASEHOLD.getServiceMethodName();
		boolean hasException = false;
		
		if (doRunService(serviceMethodName)) {

			startPerformanceMonitorOperation(serviceMethodName);

			try {
				InventoryReservationServiceResponse storeNetInventoryReservationServiceResponse = new InventoryReservationServiceResponse();

				com.digital.commerce.integration.external.inventoryreservation.storenet.domain.release.ObjectFactory objFactory = new com.digital.commerce.integration.external.inventoryreservation.storenet.domain.release.ObjectFactory();

				com.digital.commerce.integration.external.inventoryreservation.storenet.domain.release.ItemReleaseHoldRequest.ItemLocator itemLocator = objFactory
						.createItemReleaseHoldRequestItemLocator();
				itemLocator.setSku(storenetInventoryServiceRequest.getSku());

				ItemReleaseHoldRequest itemReleaseHoldRequest = objFactory.createItemReleaseHoldRequest();
				itemReleaseHoldRequest.setExternalOrderNumber(storenetInventoryServiceRequest.getExternalOrderNumber());
				itemReleaseHoldRequest.setVendorNumber(storenetInventoryServiceRequest.getVendorNumber());
				itemReleaseHoldRequest.setItemLocator(itemLocator);

				String bodyContent = serviceClient.getMarshalledStringFromObject(ItemReleaseHoldRequest.class,
						itemReleaseHoldRequest);

				ServiceRequest serviceRequest = new ServiceRequest(
						getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getReleaseHoldSOAPAction(),
						ServiceRequest.ServiceContentType.XML, ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setBody(bodyContent);

				HttpServiceResponse serviceResponse = serviceClient.processHttpRequest(serviceRequest, getUserName(),getPassword());

				ItemReleaseHoldResponse itemReleaseHoldResponse = new ItemReleaseHoldResponse();
				itemReleaseHoldResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(ItemReleaseHoldResponse.class, serviceResponse);

				storeNetInventoryReservationServiceResponse.setApproved(itemReleaseHoldResponse.isApproved());

				return storeNetInventoryReservationServiceResponse;

			} catch (Exception e) {
				hasException = true;
				
				throw new DigitalIntegrationBusinessException("releaseHold Error", ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), ServiceMethod.RELEASEHOLD.getServiceMethodName()),
					ErrorCodes.INACTIVE.getCode());
		}
	}
	
    public Map<String, Boolean> getServiceMethodsEnabled(){
		Map<String, Boolean> methods = new HashMap<>();
		for (ServiceMethod service: ServiceMethod.values()) {
			methods.put(service.getServiceMethodName(), isServiceMethodEnabled(service.getServiceMethodName()));
		}
		
		return methods;
    }

}
