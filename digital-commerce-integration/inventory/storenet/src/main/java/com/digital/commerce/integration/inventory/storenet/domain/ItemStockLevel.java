package com.digital.commerce.integration.inventory.storenet.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class ItemStockLevel implements Serializable{

	private static final long serialVersionUID = 1L;
	protected BigDecimal itemQuantityAvailable;
	protected String itemSafetyStockCondition;
	protected BigDecimal itemSafetyStockQuantity;
	protected Short itemSafetyStockTheshold;

}
