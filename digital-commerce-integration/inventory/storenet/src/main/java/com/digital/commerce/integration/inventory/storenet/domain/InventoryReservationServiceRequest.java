/**
 * 
 */
package com.digital.commerce.integration.inventory.storenet.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class InventoryReservationServiceRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private String vendorNumber;
	private String externalOrderNumber;
	private Short itemHoldQuantity;
	private String sku;
}
