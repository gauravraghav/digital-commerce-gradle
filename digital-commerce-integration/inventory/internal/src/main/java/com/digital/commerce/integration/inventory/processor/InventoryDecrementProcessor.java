/**
 * 
 */
package com.digital.commerce.integration.inventory.processor;

import java.util.HashMap;

import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;

public class InventoryDecrementProcessor implements PipelineProcessor {
	private DigitalLogger logger = DigitalLogger.getLogger(InventoryDecrementProcessor.class);

	private InventoryDecrementMessageSource inventoryDecrementMessageSource;
	public InventoryDecrementMessageSource getInventoryDecrementMessageSource() {
		return inventoryDecrementMessageSource;
	}

	public void setInventoryDecrementMessageSource(
			InventoryDecrementMessageSource inventoryDecrementMessageSource) {
		this.inventoryDecrementMessageSource = inventoryDecrementMessageSource;
	}

	private static final int SUCCESS = 1;

	/*
	 * (non-Javadoc)
	 * 
	 * @see atg.service.pipeline.PipelineProcessor#getRetCodes()
	 */
	public int[] getRetCodes() {
		int[] ret = { SUCCESS };
		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(java.lang.Object,
	 * atg.service.pipeline.PipelineResult)
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public int runProcess(Object pParam, PipelineResult pResult) throws Exception {

		HashMap map = (HashMap) pParam;
		Order order = (Order) map.get(PipelineConstants.ORDER);

		try{
			if (inventoryDecrementMessageSource != null) {
				inventoryDecrementMessageSource.sendInventoryDecrementMessage(order.getCommerceItems());
			}else{
				if(logger.isDebugEnabled()){
					logger.debug(" InventoryDecrementMessageSource unavailable for Order Id :: "+order.getId()+" "+ErrorCodes.SOURCE_UNAVAILABLE.getCode());
				}
			}
		}catch(DigitalIntegrationInactiveException ex){
			//catch integration inactive exception and place order treat it like offline order
			logger.info(" Inventory decrementing service inactive at Platform Service Order Id :: "+order.getId()+" "+ErrorCodes.SOURCE_UNAVAILABLE.getCode() +ex);
		}catch(Exception ex){
			logger.info(" Inventory decrementing service failed at Platform Service Order Id :: "+order.getId()+" "+ErrorCodes.COMM_ERROR.getCode() +ex);
		}
		return SUCCESS;
	}
}
