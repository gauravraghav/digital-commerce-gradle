package com.digital.commerce.integration.inventory.processor;

import java.util.List;

import atg.commerce.order.CommerceItem;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.jms.DigitalJMSClient;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.services.order.DigitalCommerceItemWrapper;

public class InventoryDecrementMessageSource extends BaseIntegrationService /*implements MessageSource*/ {
	public enum ServiceMethod {
		INVENTORY_DECREMENT("invDec");

		String serviceMethodName;

		ServiceMethod(String serviceMethodName) {
			this.serviceMethodName = serviceMethodName;
		}

		public String getServiceMethodName() {
			return serviceMethodName;
		}
	}

	private static transient DigitalLogger logger = DigitalLogger.getLogger(InventoryDecrementMessageSource.class);

	private DigitalJMSClient jmsClient;

	public DigitalJMSClient getJmsClient() {
		return jmsClient;
	}

	public void setJmsClient(DigitalJMSClient jmsClient) {
		this.jmsClient = jmsClient;
	}
	
	@Override
	public void postStartup() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void postShutdown() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getService() {
		return WebServicesConfig.ServicNames.INVENTORY_DECREMENT.getName();
	}

	public void sendInventoryDecrementMessage(final List<CommerceItem> commerceItems) throws DigitalIntegrationException {
		boolean hasException = false;
		String serviceMethodName = ServiceMethod.INVENTORY_DECREMENT.getServiceMethodName();

		if (doRunService(serviceMethodName)) {
			startPerformanceMonitorOperation(serviceMethodName);

			try {
				for (CommerceItem commerceItem : commerceItems) {
					sendOneMessage(commerceItem);
				}
			} catch (Exception e) {
				hasException = true;
				if (e instanceof DigitalIntegrationSystemException) {
					throw e;
				} else {
					throw new DigitalIntegrationSystemException(
							"Exception Decrement Inventory",
							ErrorCodes.COMM_ERROR.getCode(), e);
				}
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(
					String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());
		}

	}

	/**
	 * Requires and XML string of the order
	 */
	public void sendOneMessage(final CommerceItem commerceItem) throws DigitalIntegrationException {
		boolean hasException = false;
		String serviceMethodName = ServiceMethod.INVENTORY_DECREMENT.getServiceMethodName();
		if (logger.isInfoEnabled()) {
			logger.info(String.format("%s - %s: REQUEST - Store - %s: %s,%s", "InventoryDecrementMessageSource",
					"sendOneMessage", ((DigitalCommerceItemWrapper)commerceItem).getStoreId(), commerceItem.getCatalogId(), commerceItem.getQuantity()));
		}
		if (doRunService(serviceMethodName)) {
			startPerformanceMonitorOperation(serviceMethodName);
			try {
				jmsClient.sendObjectMessage("queue/INVENTORY_DECREMENT_Q",
						commerceItem, null, false);
				if(logger.isInfoEnabled()) {
					logger.info("Successfully sent Decrement  message to local DECREMENT_Q");	
				}
			} catch (Exception e) {
				logger.error("Exception while sending Decrement  message to local DECREMENT_Q", e);
				hasException = true;
				if (e instanceof DigitalIntegrationSystemException) {
					throw e;
				} else {
					throw new DigitalIntegrationSystemException(
							"Exception Decrement Inventory",
							ErrorCodes.COMM_ERROR.getCode(), e);
				}
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			throw new DigitalIntegrationInactiveException(String.format(
					"Service / Method Is Inactive: %s_%s", getComponentName(),
					serviceMethodName), ErrorCodes.INACTIVE.getCode());
		}

	}
}
