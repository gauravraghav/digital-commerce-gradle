package com.digital.commerce.integration.inventory.processor;

import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.services.inventory.InventoryHelper;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.ShippingGroupUtils;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;

import atg.adapter.gsa.query.Builder;
import atg.commerce.inventory.InventoryException;
import atg.commerce.inventory.RepositoryInventoryManager;
import atg.commerce.order.CommerceItem;
import atg.dms.patchbay.MessageSink;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

public class InventoryDecrementMessageSink implements MessageSink {
	private static transient DigitalLogger logger = DigitalLogger.getLogger(InventoryDecrementMessageSink.class);

	private static final String PROPERTY_PRODUCT_TYPE = "type";

	private RepositoryInventoryManager inventoryManager;
	private InventoryHelper inventoryHelper;
	private String aggregateStoreId;
	private Repository repository;
	private WebServicesConfig webServicesConfig;

	@Override
	/**
	 * 
	 * @param s
	 * @param message
	 * @throws JMSException
	 */
	public void receiveMessage(String s, Message message) throws JMSException {
		try {
			Boolean doDecrement = true;

			CommerceItem commerceItem = (CommerceItem) ((ObjectMessage) message).getObject();
			String sku = commerceItem.getCatalogRefId();
			long qty = commerceItem.getQuantity();

			/*
			 * DSWCommerceItemWrapper exposes getStoreId of DSWCommerceItem
			 * (which is what is received here. This was necessary as to avoid
			 * circular dependencies at compile time
			 */
			String storeId = ShippingGroupUtils.getBopisStoreId(commerceItem);

			/*
			 * determine if it's a giftcard or not before proceeding with
			 * decrement
			 */
			// commerceItem.getAuxiliaryData().getSiteId();
			RepositoryItem product = (RepositoryItem) commerceItem.getAuxiliaryData().getProductRef();

			if (product != null) {
				String type = (String) product.getPropertyValue(PROPERTY_PRODUCT_TYPE);
				if (isGiftCardType(type)) {
					doDecrement = false;
				}
			}

			if (doDecrement) {
				String locationId = lookupLocationId(storeId);

				if (locationId == null) {
					if (logger.isDebugEnabled() && null!=product) {
						logger.debug("Failed to lookup locationId :: store :: " + storeId + " sku :: " + sku
								+ " commerceId :: " + commerceItem.getId() + " productId :: "
								+ product.getRepositoryId());
					}
				}

				long inStockLevel = inventoryManager.queryStockLevel(sku);
				if (inStockLevel <= InventoryHelper.STOCK_LEVEL_INFINITE || inStockLevel >= qty) {
					if (logger.isDebugEnabled()) {
						logger.debug("Purchasing all from stock" + qty);
					}

					inventoryPurchase(sku, qty, locationId);

				} else {
					if (inStockLevel > 0) {
						if (logger.isDebugEnabled()) {
							logger.debug("Purchasing some from stock" + inStockLevel);
						}

						inventoryPurchase(sku, inStockLevel, locationId);

					}

					if (null != inventoryHelper.getDigitalConstants()
							&& inventoryHelper.getDigitalConstants().isBackOrderEnabled()) {
						long boAvailable = inventoryManager.queryBackorderLevel(sku);
						if (boAvailable >= (qty)) {
							boAvailable = qty - inStockLevel;
						}
						if (logger.isDebugEnabled()) {
							logger.debug("Backordering rest " + boAvailable);
						}
						inventoryManager.purchaseOffBackorder(sku, boAvailable);
					}
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Failed to decrement inventory", e);
		}
	}

	/**
	 * 
	 * @param storeId
	 * @return String
	 */
	protected String lookupLocationId(String storeId) {
		if (aggregateStoreId.equalsIgnoreCase(storeId) || storeId == null) {
			return aggregateStoreId;
		}
		String locationId = null;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), "lookupLocationId");
			RepositoryView view = repository.getView("location");
			Builder builder = (Builder) view.getQueryBuilder();
			String storeQuery = "select location_id from  dsw_store where store_number=" + storeId;
			RepositoryItem[] items = view.executeQuery(builder.createSqlPassthroughQuery(storeQuery, null));
			if (null != items && items.length > 0) {
				for (RepositoryItem item : items) {
					locationId = item.getRepositoryId();
					if (locationId != null) {
						break;
					}
				}
			}
		} catch (Exception ex) {
			logger.error("InventoryDecrementSink lookupLocationId", ex);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), "lookupLocationId");
		}
		return locationId;
	}

	/**
	 * 
	 * @param sku
	 * @param qty
	 * @param storeId
	 * @throws InventoryException
	 */
	protected void inventoryPurchase(String sku, long qty, String storeId) throws InventoryException {
		inventoryManager.purchase(sku, qty, storeId);
	}

	/**
	 * 
	 * @param type
	 * @return true or false
	 */
	private boolean isGiftCardType(String type) {
		if (type == null) {
			return false;
		}

		return getGiftCardTypes().containsKey(type);
	}

	/**
	 * 
	 * @return InventoryHelper
	 */
	public InventoryHelper getInventoryHelper() {
		return inventoryHelper;
	}

	/**
	 * 
	 * @param inventoryHelper
	 */
	public void setInventoryHelper(InventoryHelper inventoryHelper) {
		this.inventoryHelper = inventoryHelper;
	}

	/**
	 * 
	 * @return RepositoryInventoryManager
	 */
	public RepositoryInventoryManager getInventoryManager() {
		return inventoryManager;
	}

	/**
	 * 
	 * @param inventoryManager
	 */
	public void setInventoryManager(RepositoryInventoryManager inventoryManager) {
		this.inventoryManager = inventoryManager;
	}

	/**
	 * 
	 * @return Map<String, String>
	 */
	public Map<String, String> getGiftCardTypes() {
		return webServicesConfig.getInventoryDecrementGiftCardTypes();
	}

	/**
	 * 
	 * @return String
	 */
	public String getAggregateStoreId() {
		return aggregateStoreId;
	}

	/**
	 * 
	 * @param aggregateStoreId
	 */
	public void setAggregateStoreId(String aggregateStoreId) {
		this.aggregateStoreId = aggregateStoreId;
	}

	/**
	 * 
	 * @return Repository
	 */
	public Repository getRepository() {
		return repository;
	}

	/**
	 * 
	 * @param repository
	 */
	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	/**
	 * 
	 * @return WebServicesConfig
	 */
	public WebServicesConfig getWebServicesConfig() {
		return webServicesConfig;
	}

	/**
	 * 
	 * @param webServicesConfig
	 */
	public void setWebServicesConfig(WebServicesConfig webServicesConfig) {
		this.webServicesConfig = webServicesConfig;
	}

}