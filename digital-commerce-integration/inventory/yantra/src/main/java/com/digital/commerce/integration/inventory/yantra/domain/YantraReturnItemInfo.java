/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraReturnItemInfo {
	
	private String chargeForExchangeShipping = "";
	private String chargeForReturnShipping = "";
	private String customerReason = "";
	private String customerRequestedExchange = "";
	private String customerUsedARS = "";
	private String holdReturnForCC = "";
	private String inventoryDisposition = "" ;
	private String refundOriginalShipping = "";
	
}
