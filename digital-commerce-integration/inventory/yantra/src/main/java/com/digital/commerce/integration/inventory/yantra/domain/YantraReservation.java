package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraReservation {

	private XMLGregorianCalendar expirationDate;
	private String externalNode = "";
	private String isProcured = "";
	private String isSubstituted = "";
	private String mergeNode = "";
	private String procureToNode = "";
	private String procureFromNode = "";
	private String productAvailabilityDate = "";
	private String quantityFromUnplannedInventory = "";
	private String reservationId = "";
	private Double reservedQty = 0.0;
	private XMLGregorianCalendar shipDate;
	private String shipNode = "";
	private Item item;
	private String fullfillmentType = "";

}
