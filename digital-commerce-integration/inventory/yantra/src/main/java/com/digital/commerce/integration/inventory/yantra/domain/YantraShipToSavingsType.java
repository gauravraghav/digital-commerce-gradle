/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraShipToSavingsType {

	private Double brokenPromotionCost = 0.0;
	private Double handlingSavings = 0.0;
	private Double restockingSavings = 0.0;
	private Double returnSavings = 0.0;
	private Double shippingSavings = 0.0;
	private Double totalSavings = 0.0;
	private String value ="";

}
