package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;
import java.util.List;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraOrderDetailsServiceResponse {

	private String lastRecordSet;
	private String readFromHistory;
	private Integer totalOrderList = 0;
	private BigInteger lastOrderHeaderKey;
	private List<YantraOrder> yantraOrders;

}
