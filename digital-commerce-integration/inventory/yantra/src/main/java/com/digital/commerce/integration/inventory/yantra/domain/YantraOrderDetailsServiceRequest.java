package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraOrderDetailsServiceRequest {
	
	private String orderId = "";
	private Integer maximumRecords = 0;
	private String profileId = "";
	private Date toOrderDate;
	private Date fromOrderDate;
	private String emailId = "";

}
