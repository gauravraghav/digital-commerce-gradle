/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraInventoryReservationServiceRequest {

	private String reservationId = "";
	private List<Item> yantraItems = new ArrayList<>();

}
