/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraOrder {

	private String documentType = "";
	private String minOrderStatus = "";
	private String minOrderStatusDesc = "";
	private XMLGregorianCalendar OrderDate ;
	private String orderNo = "";
	private String status = "";
	private Double grandTotal = 0.0;
	private List<YantraOrderLine> orderLines;

}
