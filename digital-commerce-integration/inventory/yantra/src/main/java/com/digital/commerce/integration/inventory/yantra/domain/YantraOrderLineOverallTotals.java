/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class YantraOrderLineOverallTotals {

	private Double charges;
	private Double discount;
	private Double extendedPrice;
	private Double lineTotal;
	private Double optionPrice;
	private Double pricingQty;
	private Double tax;
	private Double unitPrice;
	
}
