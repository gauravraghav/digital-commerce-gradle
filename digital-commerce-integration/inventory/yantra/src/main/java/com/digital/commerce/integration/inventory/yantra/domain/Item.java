/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class Item {

	@Getter
	public static enum FulfillmentType {
		BOPIS("BOPIS"), BOSTS("BOSTS"), REGULAR("");
		
		private String description = "";

		private FulfillmentType(String description) {
			this.description = description;
		}

	}
	
	private String itemID = "";
	private String productClass = "";
	private Double requiredQty = 0.0;
	private String unitOfMeasure = "";
	private String shipNode = "";
	private FulfillmentType fulfillmentType;
	
}
