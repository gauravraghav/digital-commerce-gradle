/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraOrderItem {

	private String customerItem = "";
	private String itemDesc = "";
	private String itemID = "";

}
