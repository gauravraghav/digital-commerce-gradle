/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraShippingOption {
	
	private String lOS = "";
	private String lOSAllowed = "";
	private String lOSDescription = "";
    private String lOSName = "";
    private Integer optionId = 0;
    private YantraType promotionApplied = new YantraType();
    private List<YantraType> restrictions = new ArrayList<>();
    private YantraShipToSHCosts orderShipToSHCosts;
    private List<YantraShippingGroup> yantraShippingGroup;

}