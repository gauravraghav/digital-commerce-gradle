/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraShippingItem {

	private String itemId = "";
	private Double weight = 0.0;
	private long quantity = 0l;
	private Double unitPrice = 0.0;
	private YantraReturnItemInfo yantraReturnItemInfo;
	private YantraShipToSHCosts itemSHCosts;
}
