/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import java.util.List;
import com.digital.commerce.integration.common.domain.Address;
import lombok.Getter;
import lombok.Setter;

/** @author mmallipu */
@Getter
@Setter
public class YantraOrderLine {

	private String							orderLineKey	= "";
	private String							fulfillmentType	= "";
	private YantraOrderLineExtn				extn;
	private YantraOrderItem					item;
	private String							trackingNumber;
	private String							carrier;
	private List<YantraOrderStatus>			orderStatuses;
	private List<YantraOrderLineTax>		lineTaxes;
	private YantraOrderLineOverallTotals	orderLineOverallTotals;
	private Address personinfoshipto;

}
