/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraInventoryReservationServiceResponse {

	private String checkCapacity = "";
	private String checkInventory = "";
	private String fulfillmentType = "";
	private String ignoreMinNotificationTime = "";
	private Integer maximumRecords = 0;
	private String useLandedCost = "";
	private List<YantraReservation> yantraReservations = new ArrayList<>();

}
