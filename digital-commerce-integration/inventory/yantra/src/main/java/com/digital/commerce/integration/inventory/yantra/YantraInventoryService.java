/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import lombok.Getter;
import lombok.Setter;
import org.apache.http.conn.ConnectTimeoutException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.digital.commerce.integration.inventory.YantraService;
import com.digital.commerce.integration.inventory.yantra.domain.Item;
import com.digital.commerce.integration.inventory.yantra.domain.YantraInventoryReservationServiceRequest;
import com.digital.commerce.integration.inventory.yantra.domain.YantraInventoryReservationServiceResponse;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrder;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderDetailsServiceRequest;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderDetailsServiceResponse;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderItem;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderLine;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderLineExtn;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderLineOverallTotals;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderLineTax;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderStatus;
import com.digital.commerce.integration.inventory.yantra.domain.YantraReservation;
import com.digital.commerce.integration.inventory.yantra.domain.YantraReturnItemInfo;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShipToSHCosts;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShipToSHNetCostsType;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShipToSavingsType;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingGroup;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingItem;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingOption;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingRestrictionsServiceRequest;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingRestrictionsServiceResponse;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingSHGrossCosts;
import com.digital.commerce.integration.inventory.yantra.domain.YantraType;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.domain.Address;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.common.service.client.ServiceRequest;
import com.digital.commerce.integration.common.service.client.ServiceRequest.ServiceHttpMethod;
import com.digital.commerce.integration.util.JAXBContextCache;
import com.digital.commerce.integration.util.PropertyUtil;
import com.yantra.webservices.beans.order.common.YFSEnvironmentType;
import com.yantra.webservices.beans.order.history.ContainerDetailType;
import com.yantra.webservices.beans.order.history.ContainersType;
import com.yantra.webservices.beans.order.history.LineTaxType;
import com.yantra.webservices.beans.order.history.OrderLineType;
import com.yantra.webservices.beans.order.history.OrderListType;
import com.yantra.webservices.beans.order.history.OrderStatusType;
import com.yantra.webservices.beans.order.history.OrderStatusesType;
import com.yantra.webservices.beans.order.history.ShipmentLineType;
import com.yantra.webservices.beans.order.history.ShipmentListType;
import com.yantra.webservices.beans.order.history.ShipmentType;
import com.yantra.webservices.beans.order.inventory.PromiseHeader;
import com.yantra.webservices.beans.order.inventory.PromiseLine;
import com.yantra.webservices.beans.order.inventory.PromiseLines;
import com.yantra.webservices.beans.order.inventory.ReservationParameters;
import com.yantra.webservices.beans.order.inventory.Reservations;
import com.yantra.webservices.beans.order.shipping.ItemSHGrossCostsType;
import com.yantra.webservices.beans.order.shipping.ItemSHNetCostsType;
import com.yantra.webservices.beans.order.shipping.ItemSavingsType;
import com.yantra.webservices.beans.order.shipping.ItemType;
import com.yantra.webservices.beans.order.shipping.ItemsType;
import com.yantra.webservices.beans.order.shipping.OptionType;
import com.yantra.webservices.beans.order.shipping.OrderSHGrossCostsType;
import com.yantra.webservices.beans.order.shipping.OrderSHNetCostsType;
import com.yantra.webservices.beans.order.shipping.OrderSavingsType;
import com.yantra.webservices.beans.order.shipping.OrderType;
import com.yantra.webservices.beans.order.shipping.PersonInfoShipToType;
import com.yantra.webservices.beans.order.shipping.RateQueryRequestType;
import com.yantra.webservices.beans.order.shipping.RateQueryResponseType;
import com.yantra.webservices.beans.order.shipping.RestrictionType;
import com.yantra.webservices.beans.order.shipping.ShipToGroupType;
import com.yantra.webservices.beans.order.shipping.ShipToGroupsType;
import com.yantra.webservices.beans.order.shipping.ShipToSHGrossCostsType;
import com.yantra.webservices.beans.order.shipping.ShipToSHNetCostsType;
import com.yantra.webservices.beans.order.shipping.ShipToSavingsType;

/** @author mmallipu */
@SuppressWarnings("unchecked")
@Getter
@Setter
public class YantraInventoryService extends BaseIntegrationService implements YantraService {

	private static final String			DEFAULT_API_STRING						= "apiString";
	private static final String			DEFAULT_ENV_STRING						= "envString";
	private static final String			DEFAULT_OMIT_XML_DECLARATION			= "yes";
	private static final String			DEFAULT_ENV_LOCALPART					= "YFSEnvironment";
	private static final String			DEFAULT_ENV_NAMESPACE					= "com.yantra.webservices.beans.order.common";
	private static final String			DEFAULT_ROOT_NAMESPACE					= "http://ejb.rpc.webservices.services.interop.yantra.com";
	private static final String			DEFAULT_DATE_FORMAT						= "yyyy-MM-dd'T'HH:mm:ss";
	private static final String			PROMISE_API_LOCALPART					= "Promise";
	private static final String			PROMISE_API_NAMESPACE					= "com.yantra.webservices.beans.order.inventory";
	private static final String			DYNAMIC_RESERVATION_ROOT_QUALIFIED_NAME	= "dswDynamicReservationService";
	private static final String			DYNAMIC_RESERVATION_XPATH_EXPRESSION	= "//*[local-name()='dswDynamicReservationServiceReturn']/text()";
	private static final String			RATEQUERY_API_LOCALPART					= "RateQueryRequest";
	private static final String			RATEQUERY_API_NAMESPACE					= "com.yantra.webservices.beans.order.shipping";
	private static final String			QUERY_LOS_ROOT_QUALIFIED_NAME			= "dSWQueryLOSOptionsService";
	private static final String			QUERY_DETAILS_ROOT_QUALIFIED_NAME		= "dswGetOrderListService";
	private static final String			QUERY_LOS_XPATH_EXPRESSION				= "//*[local-name()='dSWQueryLOSOptionsServiceReturn']/text()";
	private static final String			ORDER_API_NAMESPACE						= "com.yantra.webservices.beans.order.history";
	private static final String			ORDER_API_LOCALPART						= "Order";
	private static final String			ORDER_ROOT_QUALIFIED_NAME				= "dswGetOrderListService";
	private static final String			ORDER_DETAILS_XPATH_EXPRESSION			= "//*[local-name()='dswGetOrderListServiceReturn']/text()";
	private static final String			ORDER_DATE_RANGE						= "DATERANGE";
	private static final String			ORDER_LATEST_FIRST						= "Y";
	private static final String			ORDER_DRAFT_FLAG						= "N";
	private static final String			ORDER_DATE_FORMAT						= "yyyyMMdd'T'HHmmss.SSS'Z'";

	private static final String			SD_PROGID								= "progId";
	private static final String			SD_ENTERPRISECODE						= "enterpriseCode";
	private static final String			SD_ORGANIZATIONCODE						= "organizationCode";
	XPathExpression						reservationXPath;
	XPathExpression						shippingRestrictionsXPath;
	XPathExpression						orderDetailsRestrictionsXPath;
	private IntegrationServiceClient	serviceClient;
	private JAXBContextCache			jaxbContextCache;

	@Override
	protected String getService() {
		return WebServicesConfig.ServicNames.YANTRA_INVENTORY.getName();
	}

	public Map<String, String> getStaticData() {
		return getMapData( WebServicesConfig.DataTypes.STATIC );
	}

	public String getDefaultUri() {
		return getMapData( WebServicesConfig.DataTypes.STATIC, "defaultUri" );
	}

	public String getProgId() {
		return PropertyUtil.getMappedProperty( getStaticData(), SD_PROGID ).toString();
	}

	public void setProgId( String progId ) {
		PropertyUtil.setMappedProperty( getStaticData(), SD_PROGID, progId );
	}

	public String getUserId() {
		return getMapData( WebServicesConfig.DataTypes.STATIC, "userId" );
	}

	public String getEnterpriseCode() {
		return PropertyUtil.getMappedProperty( getStaticData(), SD_ENTERPRISECODE ).toString();
	}

	public void setEnterpriseCode( String enterpriseCode ) {
		PropertyUtil.setMappedProperty( getStaticData(), SD_ENTERPRISECODE, enterpriseCode );
	}

	public String getOrganizationCode() {
		return PropertyUtil.getMappedProperty( getStaticData(), SD_ORGANIZATIONCODE ).toString();
	}

	public void setOrganizationCode( String organizationCode ) {
		PropertyUtil.setMappedProperty( getStaticData(), SD_ORGANIZATIONCODE, organizationCode );
	}

	@Override
	public void postStartup() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void postShutdown() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	public YantraInventoryReservationServiceResponse reservation( YantraInventoryReservationServiceRequest yantraInventoryServiceRequest ) throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.RESERVATION.getServiceMethodName();
		boolean hasException = false;

		if( doRunService( ServiceMethod.RESERVATION.getServiceMethodName() ) ) {

			startPerformanceMonitorOperation( serviceMethodName );

			try {

				DocumentBuilder	builder	 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			
				YantraInventoryReservationServiceResponse yantraInventoryReservationServiceResponse = new YantraInventoryReservationServiceResponse();
				com.yantra.webservices.beans.order.common.ObjectFactory objectFactory3 = new com.yantra.webservices.beans.order.common.ObjectFactory();
				YFSEnvironmentType yFSEnvironmentType = objectFactory3.createYFSEnvironmentType();
				yFSEnvironmentType.setProgId( getProgId() );
				yFSEnvironmentType.setUserId( getUserId() );

				PromiseHeader promiseHeader = populatePromiseHeader( yantraInventoryServiceRequest );

				String envString = marshalYantraObject( yFSEnvironmentType, DEFAULT_ENV_NAMESPACE, DEFAULT_ENV_LOCALPART );

				String apiString = marshalYantraObject( promiseHeader, PROMISE_API_NAMESPACE, PROMISE_API_LOCALPART );
				String request = populateStringRequest( envString, apiString, DYNAMIC_RESERVATION_ROOT_QUALIFIED_NAME );

				IntegrationServiceClient.logServiceRequest( request );

				ServiceRequest serviceRequest = new ServiceRequest( getConnectionTimeout( serviceMethodName ), getRequestTimeout( serviceMethodName ), getDefaultUri(), ServiceRequest.ServiceContentType.XML, ServiceHttpMethod.POST, serviceMethodName );

				serviceRequest.setBody( request );
				serviceRequest.setSoapAction( "" );

				HttpServiceResponse serviceResponse = serviceClient.processHttpRequest( serviceRequest );

				PromiseHeader promiseHeaderResponse = populatePromiseHeaderResponse( serviceResponse );

				yantraInventoryReservationServiceResponse = populateValuesToResponseObject( promiseHeaderResponse );

				return yantraInventoryReservationServiceResponse;

			} catch( Exception e ) {
				hasException = true;

				throw new DigitalIntegrationBusinessException( "Yantra Reservation Error", ErrorCodes.COMM_ERROR.getCode(), e );
			} finally {
				endPerformanceMonitorOperation( serviceMethodName, hasException );
			}
		} else {
			throw new DigitalIntegrationInactiveException( String.format( "Service / Method Is Inactive: %s_%s", getComponentName(), ServiceMethod.RESERVATION.getServiceMethodName() ), ErrorCodes.INACTIVE.getCode() );
		}
	}

	private PromiseHeader populatePromiseHeader( YantraInventoryReservationServiceRequest yantraInventoryServiceRequest ) throws DigitalIntegrationBusinessException {

		com.yantra.webservices.beans.order.inventory.ObjectFactory objectFactory = new com.yantra.webservices.beans.order.inventory.ObjectFactory();

		PromiseLines promiseLines = objectFactory.createPromiseLines();
		for( Item item : yantraInventoryServiceRequest.getYantraItems() ) {
			PromiseLine promiseLine = objectFactory.createPromiseLine();
			promiseLine.setItemID( item.getItemID() );
			promiseLine.setProductClass( item.getProductClass() );
			promiseLine.setRequiredQty( item.getRequiredQty() );
			promiseLine.setUnitOfMeasure( item.getUnitOfMeasure() );

			if( item.getFulfillmentType() == null ) { throw new DigitalIntegrationBusinessException( "Yantra Reservation Error: FulfillmentType is required", ErrorCodes.MISSING_REQUIRED.getCode() ); }

			if( item.getFulfillmentType() != Item.FulfillmentType.REGULAR ) {
				promiseLine.setFulfillmentType( item.getFulfillmentType().getDescription() );

				if( DigitalStringUtil.isBlank( item.getShipNode() ) ) { throw new DigitalIntegrationBusinessException( "Yantra Reservation Error: ShipNode is required for non-shiptohome", ErrorCodes.MISSING_REQUIRED.getCode() ); }
				promiseLine.setShipNode( item.getShipNode() );
			}

			promiseLines.getPromiseLine().add( promiseLine );
		}

		ReservationParameters reservationParameters = objectFactory.createReservationParameters();
		reservationParameters.setReservationID( yantraInventoryServiceRequest.getReservationId() );

		PromiseHeader promiseHeader = objectFactory.createPromiseHeader();
		promiseHeader.setEnterpriseCode( getStaticData().get( "enterpriseCode" ) );
		promiseHeader.setOrganizationCode( getStaticData().get( "organizationCode" ) );
		promiseHeader.setPromiseLines( promiseLines );
		promiseHeader.setReservationParameters( reservationParameters );

		return promiseHeader;
	}

	private PromiseHeader populatePromiseHeaderResponse( HttpServiceResponse serviceResponse ) throws Exception {

		Document bodyContentDocument = getDocumentFromResponse( serviceResponse );

		if( reservationXPath == null ) {
			reservationXPath = XPathFactory.newInstance().newXPath().compile( DYNAMIC_RESERVATION_XPATH_EXPRESSION );
		}

		NodeList nodes = (NodeList)reservationXPath.evaluate( bodyContentDocument.getDocumentElement(), XPathConstants.NODESET );

		if( nodes == null || nodes.getLength() != 1 ) { throw new DigitalIntegrationBusinessException( "YantraReservation Error", ErrorCodes.XFORM.getCode() ); }

		Node responseNode = nodes.item( 0 );
		String promiseText = responseNode.getTextContent();

		DocumentBuilder	builder	 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document promiseDocument = builder.parse( new InputSource( new StringReader( promiseText ) ) );

		JAXBContext jaxbContext = getJaxbContextCache().newJAXBContext( PromiseHeader.class );
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

		return (PromiseHeader)JAXBIntrospector.getValue( unmarshaller.unmarshal( promiseDocument ) );

	}

	protected Document getDocumentFromResponse( HttpServiceResponse response ) throws DigitalIntegrationSystemException {

		try {
			String responseString = response.getEntityString();

			IntegrationServiceClient.logServiceResponse( responseString );

			SOAPMessage message = MessageFactory.newInstance().createMessage( null, new ByteArrayInputStream( responseString.getBytes() ) );

			SOAPPart soapPart = message.getSOAPPart();
			SOAPEnvelope soapEnvelope = soapPart.getEnvelope();

			SOAPBody body = soapEnvelope.getBody();

			Document bodyContentDocument = body.extractContentAsDocument();

			return bodyContentDocument;

		} catch( Exception e ) {

			throw new DigitalIntegrationSystemException( "UnmarshalledObjectFromHttpResponse Error", ErrorCodes.XFORM.getCode(), e );

		}
	}

	private <T> String marshalYantraObject( T objectToMarshall, String nameSpaceURL, String localPart ) throws JAXBException {
		StringWriter stringWriter = new StringWriter();

		JAXBContext jaxbContext = getJaxbContextCache().newJAXBContext( objectToMarshall.getClass() );
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// format the XML output and excludes xml header if one of these
		// operations
		jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
		jaxbMarshaller.setProperty( "com.sun.xml.bind.xmlDeclaration", Boolean.FALSE );

		/* if (!DigitalStringUtil.equalsIgnoreCase(localPart,
		 * DYNAMIC_RESERVATION_ROOT_QUALIFIED_NAME) &&
		 * !DigitalStringUtil.equalsIgnoreCase(localPart,
		 * QUERY_LOS_ROOT_QUALIFIED_NAME) &&
		 * !DigitalStringUtil.equalsIgnoreCase(localPart,
		 * QUERY_DETAILS_ROOT_QUALIFIED_NAME) &&
		 * !DigitalStringUtil.equalsIgnoreCase(localPart, DEFAULT_ENV_LOCALPART) &&
		 * !DigitalStringUtil.equalsIgnoreCase(localPart, ORDER_API_LOCALPART) &&
		 * !DigitalStringUtil.equalsIgnoreCase(localPart, RATEQUERY_API_LOCALPART))
		 * { jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT,
		 * Boolean.FALSE); } */
		QName qName = null;
		if( nameSpaceURL != null ) {
			qName = new QName( nameSpaceURL, localPart );
		} else {
			qName = new QName( localPart );
		}

		JAXBElement<T> root = new JAXBElement<>(qName, (Class<T>) objectToMarshall.getClass(), objectToMarshall);

		jaxbMarshaller.marshal( root, stringWriter );

		String result = stringWriter.toString();

		return result;
	}

	private YantraInventoryReservationServiceResponse populateValuesToResponseObject( PromiseHeader promiseHeader ) {
		YantraInventoryReservationServiceResponse yantraInventoryReservationServiceResponse = new YantraInventoryReservationServiceResponse();

		yantraInventoryReservationServiceResponse.setCheckCapacity( promiseHeader.getCheckCapacity() );
		yantraInventoryReservationServiceResponse.setCheckInventory( promiseHeader.getCheckInventory() );
		yantraInventoryReservationServiceResponse.setFulfillmentType( promiseHeader.getFulfillmentType() );
		yantraInventoryReservationServiceResponse.setIgnoreMinNotificationTime( promiseHeader.getIgnoreMinNotificationTime() );
		yantraInventoryReservationServiceResponse.setMaximumRecords( promiseHeader.getMaximumRecords() );
		yantraInventoryReservationServiceResponse.setUseLandedCost( promiseHeader.getUseLandedCost() );

		List<YantraReservation> yantraReservations = new ArrayList<>();
		for( PromiseLine promiseLine : promiseHeader.getPromiseLines().getPromiseLine() ) {
			Item item = new Item();
			item.setItemID( promiseLine.getItemID() );
			item.setProductClass( promiseLine.getProductClass() );
			item.setRequiredQty( promiseLine.getRequiredQty() );
			item.setUnitOfMeasure( promiseLine.getUnitOfMeasure() );

			if( promiseLine.getReservations() != null ) {
				Reservations reservations = promiseLine.getReservations();

				for( com.yantra.webservices.beans.order.inventory.Reservation reserve : reservations.getReservation() ) {
					YantraReservation yantraReservation = new YantraReservation();
					yantraReservation.setExpirationDate( reserve.getExpirationDate() );
					yantraReservation.setExternalNode( reserve.getExternalNode() );
					yantraReservation.setIsProcured( reserve.getIsProcured() );
					yantraReservation.setIsSubstituted( reserve.getIsSubstitued() );
					yantraReservation.setMergeNode( reserve.getMergeNode() );
					yantraReservation.setProcureToNode( reserve.getProcureToNode() );
					yantraReservation.setProcureFromNode( reserve.getProcureFromNode() );
					yantraReservation.setReservationId( reserve.getReservationID() );
					yantraReservation.setReservedQty( reserve.getReservedQty() );
					yantraReservation.setShipDate( reserve.getShipDate() );
					yantraReservation.setShipNode( reserve.getShipNode() );
					yantraReservation.setItem( item );
					yantraReservation.setFullfillmentType(promiseLine.getFulfillmentType());
					yantraReservations.add( yantraReservation );
				}
			}

		}
		yantraInventoryReservationServiceResponse.setYantraReservations( yantraReservations );

		return yantraInventoryReservationServiceResponse;
	}

	@Override
	public YantraShippingRestrictionsServiceResponse shippingRestrictions( YantraShippingRestrictionsServiceRequest yantrashippingRequest ) throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.SHIPPING_RESTRICTIONS.getServiceMethodName();
		boolean hasException = false;

		if( doRunService( ServiceMethod.SHIPPING_RESTRICTIONS.getServiceMethodName() ) ) {

			startPerformanceMonitorOperation( serviceMethodName );

			try {
				DocumentBuilder	builder	 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				YantraShippingRestrictionsServiceResponse yantraShippingResponse = new YantraShippingRestrictionsServiceResponse();

				com.yantra.webservices.beans.order.common.ObjectFactory objectFactory2 = new com.yantra.webservices.beans.order.common.ObjectFactory();

				YFSEnvironmentType yFSEnvironmentType = objectFactory2.createYFSEnvironmentType();
				yFSEnvironmentType.setProgId( getProgId() );
				yFSEnvironmentType.setUserId( getUserId() );

				RateQueryRequestType rateQueryRequestType = populateRateQueryRequest( yantrashippingRequest );
				String envString = marshalYantraObject( yFSEnvironmentType, null, DEFAULT_ENV_LOCALPART );

				String apiString = marshalYantraObject( rateQueryRequestType, null, RATEQUERY_API_LOCALPART );

				String request = populateStringRequest( envString, apiString, QUERY_LOS_ROOT_QUALIFIED_NAME );

				ServiceRequest serviceRequest = new ServiceRequest( getConnectionTimeout( serviceMethodName ), getRequestTimeout( serviceMethodName ), getDefaultUri(), ServiceRequest.ServiceContentType.XML, ServiceHttpMethod.POST, serviceMethodName );

				serviceRequest.setBody( request );
				serviceRequest.setSoapAction( "" );

				HttpServiceResponse serviceResponse = serviceClient.processHttpRequest( serviceRequest );

				RateQueryResponseType rateQueryResponse = populateRateQueryResponse( serviceResponse );

				yantraShippingResponse = populateValuesFromRateQueryToResponseObject( rateQueryResponse );

				return yantraShippingResponse;

			} catch( Exception e ) {
				hasException = true;
				if(e.getCause() instanceof SocketException || e.getCause() instanceof TimeoutException || e.getCause() instanceof ConnectTimeoutException ){
				    throw new DigitalIntegrationBusinessException( "Shipping Restrictions Error:: TIMEOUT ERROR", ErrorCodes.SOURCE_UNAVAILABLE.getCode(), e );
				}else{
				    throw new DigitalIntegrationBusinessException( "Shipping Restrictions Error", ErrorCodes.COMM_ERROR.getCode(), e );
				}
			} finally {
				endPerformanceMonitorOperation( serviceMethodName, hasException );
			}
		} else {
			throw new DigitalIntegrationInactiveException( String.format( "Service / Method Is Inactive: %s_%s", getComponentName(), ServiceMethod.SHIPPING_RESTRICTIONS.getServiceMethodName() ), ErrorCodes.INACTIVE.getCode() );
		}
	}

	private String populateStringRequest( String envString, String apiString, String rootQualifiedName ) throws TransformerException, DigitalIntegrationBusinessException {

		/* Unfortunately the WSDL isn't the most useful for autogenerating the
		 * SOAP document.
		 * All of this needs done manually and doesn't fit into our generalized
		 * service client */
		String request = "";
		try {
			DocumentBuilder	builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = builder.newDocument();
			Element root = document.createElementNS( DEFAULT_ROOT_NAMESPACE, rootQualifiedName );
	
			document.appendChild( root );
	
			Element apiElement = document.createElement( DEFAULT_API_STRING );
			apiElement.appendChild( document.createCDATASection( apiString ) );
			root.appendChild( apiElement );
	
			Element envElement = document.createElement( DEFAULT_ENV_STRING );
			envElement.appendChild( document.createTextNode( envString ) );
			root.appendChild( envElement );
	
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty( OutputKeys.OMIT_XML_DECLARATION, DEFAULT_OMIT_XML_DECLARATION );
	
			StringWriter writer = new StringWriter();
			transformer.transform( new DOMSource( document ), new StreamResult( writer ) );
			request = writer.getBuffer().toString();
	
			IntegrationServiceClient.logServiceRequest( request );
		}catch( Exception e ) {
			throw new DigitalIntegrationBusinessException( "Yantra Reservation Error", ErrorCodes.COMM_ERROR.getCode(), e );
		} 
		return request;
	}

	private RateQueryRequestType populateRateQueryRequest( YantraShippingRestrictionsServiceRequest yantrashippingRequest ) throws DigitalIntegrationBusinessException {
		com.yantra.webservices.beans.order.shipping.ObjectFactory objectFactory = new com.yantra.webservices.beans.order.shipping.ObjectFactory();

		DateFormat dateFormat = new SimpleDateFormat( DEFAULT_DATE_FORMAT );
		XMLGregorianCalendar driverDate;
		try {
			driverDate = DatatypeFactory.newInstance().newXMLGregorianCalendar( dateFormat.format( new Date() ) );
		} catch( DatatypeConfigurationException e ) {
			throw new DigitalIntegrationBusinessException( "Exception in populateRateQueryRequest", ErrorCodes.COMM_ERROR.getCode(), e );
		}

		RateQueryRequestType rateQueryRequestType = objectFactory.createRateQueryRequestType();

		OrderType orderType = objectFactory.createOrderType();
		orderType.setOrganizationCode( getStaticData().get( "organizationCode" ) );
		orderType.setType( yantrashippingRequest.getType() );
		orderType.setSellingOrganization( getStaticData().get( "organizationCode" ) );
		orderType.setOrderIdentifier( yantrashippingRequest.getOrderIdentifier() );

		if( yantrashippingRequest.getDriverDate() == null )
			orderType.setDriverDate( driverDate );
		else
			orderType.setDriverDate( yantrashippingRequest.getDriverDate() );

		ShipToGroupsType shipToGroupsType = objectFactory.createShipToGroupsType();

		for( YantraShippingGroup yantraShippingGroup : yantrashippingRequest.getYantraShippingGroup() ) {
			ShipToGroupType shipToGroupType = objectFactory.createShipToGroupType();

			shipToGroupType.setShipFromNode( yantraShippingGroup.getShipFromNode() );
			shipToGroupType.setShipToGroupIdentifier( yantraShippingGroup.getShipToGroupIdentifier() );

			PersonInfoShipToType personInfoShipTo = objectFactory.createPersonInfoShipToType();
			personInfoShipTo.setAddressLine1( yantraShippingGroup.getPersonInfoShipTo().getAddress1() );
			personInfoShipTo.setCity( yantraShippingGroup.getPersonInfoShipTo().getCity() );
			personInfoShipTo.setState( yantraShippingGroup.getPersonInfoShipTo().getState() );
			personInfoShipTo.setCountry( yantraShippingGroup.getPersonInfoShipTo().getCountryCode() );
			personInfoShipTo.setZipCode( yantraShippingGroup.getPersonInfoShipTo().getPostalCode() );
			
			if ( yantraShippingGroup.getPersonInfoShipTo().getAddressType()!=null &&  yantraShippingGroup.getPersonInfoShipTo().getAddressType().equals("USA"))
			{
				personInfoShipTo.setAddressType( "USC" );
			}
			else {
				personInfoShipTo.setAddressType( yantraShippingGroup.getPersonInfoShipTo().getAddressType() );
			}

			shipToGroupType.setPersonInfoShipTo( personInfoShipTo );

			ItemsType itemsType = objectFactory.createItemsType();
			for( YantraShippingItem shipItem : yantraShippingGroup.getYantraShippingItems() ) {
				ItemType itemType = objectFactory.createItemType();

				itemType.setItemId( shipItem.getItemId() );
				itemType.setWeight( shipItem.getWeight() );
				itemType.setQuantity( new Double( shipItem.getQuantity() ) );
				itemType.setUnitPrice( shipItem.getUnitPrice() );

				itemsType.getItem().add( itemType );
			}
			shipToGroupType.setItems( itemsType );
			shipToGroupsType.getShipToGroup().add( shipToGroupType );
			orderType.setShipToGroups( shipToGroupsType );
		}
		rateQueryRequestType.setOrder( orderType );

		return rateQueryRequestType;
	}

	private YantraShippingRestrictionsServiceResponse populateValuesFromRateQueryToResponseObject( RateQueryResponseType rateQueryResponse ) {
		YantraShippingRestrictionsServiceResponse yantraShippingRestrictionsServiceResponse = new YantraShippingRestrictionsServiceResponse();

		OrderType orderType = rateQueryResponse.getOrder();
		yantraShippingRestrictionsServiceResponse.setDriverDate( orderType.getDriverDate() );
		yantraShippingRestrictionsServiceResponse.setOrderIdentifier( orderType.getOrderIdentifier() );
		yantraShippingRestrictionsServiceResponse.setOrganizationCode( orderType.getOrganizationCode() );
		yantraShippingRestrictionsServiceResponse.setSellingOrganization( orderType.getSellingOrganization() );
		yantraShippingRestrictionsServiceResponse.setType( orderType.getType() );

		List<YantraShippingOption> yantraShippingOptions = new ArrayList<>();
		if( rateQueryResponse.getOrder() != null && rateQueryResponse.getOrder().getOptions() != null ) {
			for( OptionType optionType : rateQueryResponse.getOrder().getOptions().getOption() ) {
				YantraShippingOption yantraShippingOption = new YantraShippingOption();
				yantraShippingOption.setLOS( optionType.getLOS() );
				yantraShippingOption.setLOSAllowed( optionType.getLOSAllowed() );
				yantraShippingOption.setLOSDescription( optionType.getLOSDescription() );
				yantraShippingOption.setLOSName( optionType.getLOSName() );
				yantraShippingOption.setOptionId( optionType.getOptionId() );

				if( optionType.getPromotionApplied() != null ) {
					yantraShippingOption.getPromotionApplied().setType( optionType.getPromotionApplied().getPromotion() );
					yantraShippingOption.getPromotionApplied().setValue( optionType.getPromotionApplied().getValue() );
				}

				if( optionType.getRestrictionList() != null && optionType.getRestrictionList().getRestriction() != null ) {
					for( RestrictionType restrictionType : optionType.getRestrictionList().getRestriction() ) {
						YantraType restriction = new YantraType();
						restriction.setType( restrictionType.getType() );
						restriction.setValue( restrictionType.getValue() );
						restriction.setValueAttribute( restrictionType.getValueAttribute() );

						yantraShippingOption.getRestrictions().add( restriction );
					}
				}

				if( optionType.getOrderSHCosts() != null ) {
					YantraShipToSHCosts orderShipToSHCosts = new YantraShipToSHCosts();

					YantraShippingSHGrossCosts orderSHGrossCosts = new YantraShippingSHGrossCosts();
					OrderSHGrossCostsType orderSHGrossCostsType = optionType.getOrderSHCosts().getOrderSHGrossCosts();
					orderSHGrossCosts.setHandlingGrossCosts( orderSHGrossCostsType.getHandlingGrossCosts() );
					orderSHGrossCosts.setRestockingGrossCosts( orderSHGrossCostsType.getRestockingGrossCosts() );
					orderSHGrossCosts.setReturnGrossCosts( orderSHGrossCostsType.getReturnGrossCosts() );
					orderSHGrossCosts.setShippingGrossCosts( orderSHGrossCostsType.getShippingGrossCosts() );
					orderSHGrossCosts.setTotalGrossCosts( orderSHGrossCostsType.getTotalGrossCosts() );
					orderSHGrossCosts.setValue( orderSHGrossCostsType.getValue() );

					OrderSavingsType shipToSavingsType = optionType.getOrderSHCosts().getOrderSavings();
					YantraShipToSavingsType orderShipToSavingsType = new YantraShipToSavingsType();
					orderShipToSavingsType.setBrokenPromotionCost( shipToSavingsType.getBrokenPromotionCost() );
					orderShipToSavingsType.setHandlingSavings( shipToSavingsType.getHandlingSavings() );
					orderShipToSavingsType.setRestockingSavings( shipToSavingsType.getRestockingSavings() );
					orderShipToSavingsType.setReturnSavings( shipToSavingsType.getReturnSavings() );
					orderShipToSavingsType.setShippingSavings( shipToSavingsType.getShippingSavings() );
					orderShipToSavingsType.setTotalSavings( shipToSavingsType.getTotalSavings() );
					orderShipToSavingsType.setValue( shipToSavingsType.getValue() );

					OrderSHNetCostsType shipToSHNetCostsType = optionType.getOrderSHCosts().getOrderSHNetCosts();
					YantraShipToSHNetCostsType orderShipToSHNetCostsType = new YantraShipToSHNetCostsType();
					orderShipToSHNetCostsType.setBrokenPromotionCost( shipToSHNetCostsType.getBrokenPromotionCost() );
					orderShipToSHNetCostsType.setHandlingNetCosts( shipToSHNetCostsType.getHandlingNetCosts() );
					orderShipToSHNetCostsType.setRestockingNetCosts( shipToSHNetCostsType.getRestockingNetCosts() );
					orderShipToSHNetCostsType.setReturnNetCosts( shipToSHNetCostsType.getReturnNetCosts() );
					orderShipToSHNetCostsType.setShippingNetCosts( shipToSHNetCostsType.getShippingNetCosts() );
					orderShipToSHNetCostsType.setTotalNetCosts( shipToSHNetCostsType.getTotalNetCosts() );
					orderShipToSHNetCostsType.setValue( shipToSHNetCostsType.getValue() );

					orderShipToSHCosts.setYantraShippingSHGrossCosts( orderSHGrossCosts );
					orderShipToSHCosts.setYantraShipToSHNetCostsType( orderShipToSHNetCostsType );
					orderShipToSHCosts.setYantraShipToSavingsType( orderShipToSavingsType );

					yantraShippingOption.setOrderShipToSHCosts( orderShipToSHCosts );

				}

				if( optionType.getShipToGroups() != null ) {
					List<YantraShippingGroup> yantraShippingGroups = new ArrayList<>();

					YantraShipToSHCosts yantraShipToSHCosts = new YantraShipToSHCosts();
					for( ShipToGroupType shipToGroup : optionType.getShipToGroups().getShipToGroup() ) {
						YantraShippingGroup yantraShippingGroup = new YantraShippingGroup();
						if( shipToGroup.getPersonInfoShipTo() != null ) {
							Address personInfoShipTo = new Address();
							personInfoShipTo.setAddress1( shipToGroup.getPersonInfoShipTo().getAddressLine1() );
							personInfoShipTo.setAddress2( shipToGroup.getPersonInfoShipTo().getAddressLine2() );
							personInfoShipTo.setAddressType( shipToGroup.getPersonInfoShipTo().getAddressType() );
							personInfoShipTo.setCity( shipToGroup.getPersonInfoShipTo().getCity() );
							personInfoShipTo.setState( shipToGroup.getPersonInfoShipTo().getState() );
							personInfoShipTo.setCountryCode( shipToGroup.getPersonInfoShipTo().getCountry() );
							personInfoShipTo.setPostalCode( shipToGroup.getPersonInfoShipTo().getZipCode() );

							yantraShippingGroup.setPersonInfoShipTo( personInfoShipTo );
						}
						yantraShippingGroup.setShipFromNode( shipToGroup.getShipFromNode() );
						yantraShippingGroup.setShipToGroupIdentifier( shipToGroup.getShipToGroupIdentifier() );

						if(null != shipToGroup.getShipToSHCosts()) {
							ShipToSHGrossCostsType shipToSHGrossCostsType = shipToGroup.getShipToSHCosts()
									.getShipToSHGrossCosts();
							YantraShippingSHGrossCosts yantraShippingSHGrossCosts = new YantraShippingSHGrossCosts();
							yantraShippingSHGrossCosts
									.setHandlingGrossCosts(shipToSHGrossCostsType.getHandlingGrossCosts());
							yantraShippingSHGrossCosts
									.setRestockingGrossCosts(shipToSHGrossCostsType.getRestockingGrossCosts());
							yantraShippingSHGrossCosts
									.setReturnGrossCosts(shipToSHGrossCostsType.getReturnGrossCosts());
							yantraShippingSHGrossCosts
									.setShippingGrossCosts(shipToSHGrossCostsType.getShippingGrossCosts());
							yantraShippingSHGrossCosts
									.setTotalGrossCosts(shipToSHGrossCostsType.getTotalGrossCosts());
							yantraShippingSHGrossCosts.setValue(shipToSHGrossCostsType.getValue());

							ShipToSavingsType shipToSavingsType = shipToGroup.getShipToSHCosts()
									.getShipToSavings();
							YantraShipToSavingsType yantraShipToSavingsType = new YantraShipToSavingsType();
							yantraShipToSavingsType
									.setBrokenPromotionCost(shipToSavingsType.getBrokenPromotionCost());
							yantraShipToSavingsType.setHandlingSavings(shipToSavingsType.getHandlingSavings());
							yantraShipToSavingsType
									.setRestockingSavings(shipToSavingsType.getRestockingSavings());
							yantraShipToSavingsType.setReturnSavings(shipToSavingsType.getReturnSavings());
							yantraShipToSavingsType.setShippingSavings(shipToSavingsType.getShippingSavings());
							yantraShipToSavingsType.setTotalSavings(shipToSavingsType.getTotalSavings());
							yantraShipToSavingsType.setValue(shipToSavingsType.getValue());

							ShipToSHNetCostsType shipToSHNetCostsType = shipToGroup.getShipToSHCosts()
									.getShipToSHNetCosts();
							YantraShipToSHNetCostsType yantraShipToSHNetCostsType = new YantraShipToSHNetCostsType();
							yantraShipToSHNetCostsType
									.setBrokenPromotionCost(shipToSHNetCostsType.getBrokenPromotionCost());
							yantraShipToSHNetCostsType
									.setHandlingNetCosts(shipToSHNetCostsType.getHandlingNetCosts());
							yantraShipToSHNetCostsType
									.setRestockingNetCosts(shipToSHNetCostsType.getRestockingNetCosts());
							yantraShipToSHNetCostsType
									.setReturnNetCosts(shipToSHNetCostsType.getReturnNetCosts());
							yantraShipToSHNetCostsType
									.setShippingNetCosts(shipToSHNetCostsType.getShippingNetCosts());
							yantraShipToSHNetCostsType.setTotalNetCosts(shipToSHNetCostsType.getTotalNetCosts());
							yantraShipToSHNetCostsType.setValue(shipToSHNetCostsType.getValue());

							yantraShipToSHCosts.setYantraShippingSHGrossCosts( yantraShippingSHGrossCosts );
							yantraShipToSHCosts.setYantraShipToSavingsType( yantraShipToSavingsType );
							yantraShipToSHCosts.setYantraShipToSHNetCostsType( yantraShipToSHNetCostsType );
						}

						yantraShippingGroup.setYantraShipToSHCosts( yantraShipToSHCosts );

						if( shipToGroup.getItems() != null ) {
							List<YantraShippingItem> yantraShippingItems = new ArrayList<>();
							for( ItemType itemType : shipToGroup.getItems().getItem() ) {
								YantraShippingItem yantraShippingItem = new YantraShippingItem();
								yantraShippingItem.setItemId( itemType.getItemId() );
								yantraShippingItem.setQuantity( itemType.getQuantity().longValue() );
								yantraShippingItem.setUnitPrice( itemType.getUnitPrice() );
								yantraShippingItem.setWeight( itemType.getWeight() );

								YantraReturnItemInfo yantraReturnItemInfo = new YantraReturnItemInfo();
								yantraReturnItemInfo.setChargeForExchangeShipping( itemType.getReturnItemInfo().getChargeForExchangeShipping() );
								yantraReturnItemInfo.setChargeForReturnShipping( itemType.getReturnItemInfo().getChargeForReturnShipping() );
								yantraReturnItemInfo.setCustomerReason( itemType.getReturnItemInfo().getCustomerReason() );
								yantraReturnItemInfo.setCustomerRequestedExchange( itemType.getReturnItemInfo().getCustomerRequestedExchange() );
								yantraReturnItemInfo.setCustomerUsedARS( itemType.getReturnItemInfo().getCustomerUsedARS() );
								yantraReturnItemInfo.setHoldReturnForCC( itemType.getReturnItemInfo().getHoldReturnForCC() );
								yantraReturnItemInfo.setInventoryDisposition( itemType.getReturnItemInfo().getInventoryDisposition() );
								yantraReturnItemInfo.setRefundOriginalShipping( itemType.getReturnItemInfo().getRefundOriginalShipping() );

								yantraShippingItem.setYantraReturnItemInfo( yantraReturnItemInfo );

								ItemSHGrossCostsType itemSHCostType = itemType.getItemSHCosts().getItemSHGrossCosts();
								YantraShipToSHCosts itemSHCosts = new YantraShipToSHCosts();
								YantraShippingSHGrossCosts itemSHGrossCosts = new YantraShippingSHGrossCosts();
								itemSHGrossCosts.setHandlingGrossCosts( itemSHCostType.getHandlingGrossCosts() );
								itemSHGrossCosts.setRestockingGrossCosts( itemSHCostType.getRestockingGrossCosts() );
								itemSHGrossCosts.setReturnGrossCosts( itemSHCostType.getReturnGrossCosts() );
								itemSHGrossCosts.setShippingGrossCosts( itemSHCostType.getShippingGrossCosts() );
								itemSHGrossCosts.setTotalGrossCosts( itemSHCostType.getTotalGrossCosts() );
								itemSHGrossCosts.setValue( itemSHCostType.getValue() );

								ItemSavingsType itemSavingsType = itemType.getItemSHCosts().getItemSavings();
								YantraShipToSavingsType itemShipToSavingsType = new YantraShipToSavingsType();
								itemShipToSavingsType.setBrokenPromotionCost( itemSavingsType.getBrokenPromotionCost() );
								itemShipToSavingsType.setHandlingSavings( itemSavingsType.getHandlingSavings() );
								itemShipToSavingsType.setRestockingSavings( itemSavingsType.getRestockingSavings() );
								itemShipToSavingsType.setReturnSavings( itemSavingsType.getReturnSavings() );
								itemShipToSavingsType.setShippingSavings( itemSavingsType.getShippingSavings() );
								itemShipToSavingsType.setTotalSavings( itemSavingsType.getTotalSavings() );
								itemShipToSavingsType.setValue( itemSavingsType.getValue() );

								ItemSHNetCostsType itemSHNetCostsType = itemType.getItemSHCosts().getItemSHNetCosts();
								YantraShipToSHNetCostsType itemShipToSHNetCostsType = new YantraShipToSHNetCostsType();
								itemShipToSHNetCostsType.setBrokenPromotionCost( itemSHNetCostsType.getBrokenPromotionCost() );
								itemShipToSHNetCostsType.setHandlingNetCosts( itemSHNetCostsType.getHandlingNetCosts() );
								itemShipToSHNetCostsType.setRestockingNetCosts( itemSHNetCostsType.getRestockingNetCosts() );
								itemShipToSHNetCostsType.setReturnNetCosts( itemSHNetCostsType.getReturnNetCosts() );
								itemShipToSHNetCostsType.setShippingNetCosts( itemSHNetCostsType.getShippingNetCosts() );
								itemShipToSHNetCostsType.setTotalNetCosts( itemSHNetCostsType.getTotalNetCosts() );
								itemShipToSHNetCostsType.setValue( itemSHNetCostsType.getValue() );

								itemSHCosts.setYantraShippingSHGrossCosts( itemSHGrossCosts );
								itemSHCosts.setYantraShipToSavingsType( itemShipToSavingsType );
								itemSHCosts.setYantraShipToSHNetCostsType( itemShipToSHNetCostsType );

								yantraShippingItem.setItemSHCosts( itemSHCosts );
								yantraShippingItems.add( yantraShippingItem );

							}

							if( shipToGroup.getRestrictionList() != null ) {
								List<YantraType> restrictionList = new ArrayList<>();
								yantraShippingGroup.setLosAllowed( shipToGroup.getRestrictionList().getLOSAllowed() );
								for( RestrictionType restriction : shipToGroup.getRestrictionList().getRestriction() ) {
									YantraType yantraType = new YantraType();
									yantraType.setType( restriction.getType() );
									yantraType.setValue( restriction.getValue() );
									yantraType.setValueAttribute( restriction.getValueAttribute() );

									restrictionList.add( yantraType );
								}
								yantraShippingGroup.setRestrictionList( restrictionList );
							}

							yantraShippingGroup.setYantraShippingItems( yantraShippingItems );
							yantraShippingGroup.setLos( shipToGroup.getLOS() );
							yantraShippingGroup.setAssociatedShipToReturnGroupIdentifier( shipToGroup.getAssociatedShipToReturnGroupIdentifier() );
							yantraShippingGroup.setAssociatedShipToSalesGroupIdentifier( shipToGroup.getAssociatedShipToSalesGroupIdentifier() );
							yantraShippingGroup.setCarrierCode( shipToGroup.getCarrierCode() );
							yantraShippingGroup.setEstimatedDeliveryDate( shipToGroup.getEstimatedDeliveryDate() );
							yantraShippingGroup.setEstimatedShipDate( shipToGroup.getEstimatedShipDate() );
							yantraShippingGroup.setLosName( shipToGroup.getLOSName() );

							if( shipToGroup.getPromotionApplied() != null ) {
								YantraType yantraType = new YantraType();
								yantraType.setType( shipToGroup.getPromotionApplied().getValue() );
								yantraType.setValue( shipToGroup.getPromotionApplied().getValue() );
								yantraType.setValueAttribute( shipToGroup.getPromotionApplied().getValueAttribute() );

								yantraShippingGroup.setPromotionApplied( yantraType );
							}

						}
						yantraShippingGroups.add( yantraShippingGroup );
					}
					yantraShippingOption.setYantraShippingGroup( yantraShippingGroups );

				}
				yantraShippingOptions.add( yantraShippingOption );
			}
		}
		yantraShippingRestrictionsServiceResponse.setYantraShippingOptions( yantraShippingOptions );
		return yantraShippingRestrictionsServiceResponse;
	}

	private RateQueryResponseType populateRateQueryResponse( HttpServiceResponse serviceResponse ) throws Exception {

		Document responseDocument = null;
		String responseText = null;
		Unmarshaller unmarshaller = null;
		try{
		Document bodyContentDocument = getDocumentFromResponse( serviceResponse );
		if( shippingRestrictionsXPath == null ) {
			shippingRestrictionsXPath = XPathFactory.newInstance().newXPath().compile( QUERY_LOS_XPATH_EXPRESSION );
		}

		NodeList nodes = (NodeList)shippingRestrictionsXPath.evaluate( bodyContentDocument.getDocumentElement(), XPathConstants.NODESET );

		if( nodes == null || nodes.getLength() != 1 ) { throw new DigitalIntegrationBusinessException( "YantraShippingRestrictions populate Error", ErrorCodes.XFORM.getCode() ); }

		Node responseNode = nodes.item( 0 );
		responseText = responseNode.getTextContent();
		DocumentBuilder	builder	 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		responseDocument = builder.parse( new InputSource( new StringReader( responseText ) ) );

		JAXBContext jaxbContext = getJaxbContextCache().newJAXBContext( RateQueryResponseType.class );
		unmarshaller = jaxbContext.createUnmarshaller();
		} catch( Exception e ) {
			logger.error("Exception in populateRateQueryResponse",e);
			logger.error("Exception while parsing response Text"+responseText);
			throw e;
		}

		return (RateQueryResponseType)JAXBIntrospector.getValue( unmarshaller.unmarshal( responseDocument ) );

	}

	@Override
	public YantraOrderDetailsServiceResponse orderDetails( YantraOrderDetailsServiceRequest yantraOrderDetailsServiceRequest ) throws DigitalIntegrationException {

		String serviceMethodName = ServiceMethod.ORDER_DETAILS.getServiceMethodName();
		boolean hasException = false;

		if( doRunService( ServiceMethod.ORDER_DETAILS.getServiceMethodName() ) ) {

			startPerformanceMonitorOperation( serviceMethodName );

			try {

			
				DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

				YantraOrderDetailsServiceResponse yantraOrderDetailsServiceResponse = new YantraOrderDetailsServiceResponse();

				com.yantra.webservices.beans.order.common.ObjectFactory objectFactory2 = new com.yantra.webservices.beans.order.common.ObjectFactory();
				com.yantra.webservices.beans.order.history.ObjectFactory objectFactory = new com.yantra.webservices.beans.order.history.ObjectFactory();

				DateFormat dateFormat = new SimpleDateFormat( ORDER_DATE_FORMAT );

				YFSEnvironmentType yFSEnvironmentType = objectFactory2.createYFSEnvironmentType();
				yFSEnvironmentType.setProgId( getProgId() );
				yFSEnvironmentType.setUserId( getUserId() );

				com.yantra.webservices.beans.order.history.OrderType orderType = objectFactory.createOrderType();

				if( DigitalStringUtil.isNotBlank( yantraOrderDetailsServiceRequest.getOrderId() ) ) {
					orderType.setOrderNo( DigitalStringUtil.trimToEmpty( yantraOrderDetailsServiceRequest.getOrderId() ) );
				}

				orderType.setMaximumRecords( yantraOrderDetailsServiceRequest.getMaximumRecords() );
				orderType.setBillToID( yantraOrderDetailsServiceRequest.getProfileId() );

				if( yantraOrderDetailsServiceRequest.getFromOrderDate() != null || yantraOrderDetailsServiceRequest.getToOrderDate() != null ) {
					orderType.setOrderDateQryType( ORDER_DATE_RANGE );
					orderType.setFromOrderDate( dateFormat.format( yantraOrderDetailsServiceRequest.getFromOrderDate() ) );
					orderType.setToOrderDate( dateFormat.format( yantraOrderDetailsServiceRequest.getToOrderDate() ) );
				}

				if( DigitalStringUtil.isNotBlank( yantraOrderDetailsServiceRequest.getEmailId() ) ) {
					orderType.setOrderDateQryType( ORDER_DATE_RANGE );
					orderType.setCustomerEMailID( yantraOrderDetailsServiceRequest.getEmailId().toUpperCase() );
				}

				orderType.setLatestFirst( ORDER_LATEST_FIRST );
				orderType.setDraftOrderFlag( ORDER_DRAFT_FLAG );

				String envString = marshalYantraObject( yFSEnvironmentType, null, DEFAULT_ENV_LOCALPART );

				String apiString = marshalYantraObject( orderType, null, ORDER_API_LOCALPART );

				String request = populateStringRequest( envString, apiString, ORDER_ROOT_QUALIFIED_NAME );

				ServiceRequest serviceRequest = new ServiceRequest( getConnectionTimeout( serviceMethodName ), getRequestTimeout( serviceMethodName ), getDefaultUri(), ServiceRequest.ServiceContentType.XML, ServiceHttpMethod.POST, serviceMethodName );

				serviceRequest.setBody( request );
				serviceRequest.setSoapAction( "" );

				HttpServiceResponse serviceResponse = serviceClient.processHttpRequest( serviceRequest );

				OrderListType orderListType = populateOrderListType( serviceResponse );

				yantraOrderDetailsServiceResponse = populateValuesFromOrderListTypeToResponseObject( orderListType );

				return yantraOrderDetailsServiceResponse;

			} catch( Exception e ) {
				logger.error(e.getMessage(),e);
				hasException = true;

				throw new DigitalIntegrationBusinessException( "Order Details Error", ErrorCodes.COMM_ERROR.getCode(), e );
			} finally {
				endPerformanceMonitorOperation( serviceMethodName, hasException );
			}
		} else {
			throw new DigitalIntegrationInactiveException( String.format( "Service / Method Is Inactive: %s_%s", getComponentName(), ServiceMethod.ORDER_DETAILS.getServiceMethodName() ), ErrorCodes.INACTIVE.getCode() );
		}
	}

	private YantraOrderDetailsServiceResponse populateValuesFromOrderListTypeToResponseObject( OrderListType orderListType ) {
		YantraOrderDetailsServiceResponse yantraOrderDetailsServiceResponse = new YantraOrderDetailsServiceResponse();

		yantraOrderDetailsServiceResponse.setLastRecordSet( orderListType.getLastRecordSet() );
		yantraOrderDetailsServiceResponse.setReadFromHistory( orderListType.getReadFromHistory() );
		yantraOrderDetailsServiceResponse.setTotalOrderList( orderListType.getTotalOrderList() );
		yantraOrderDetailsServiceResponse.setLastOrderHeaderKey( orderListType.getLastOrderHeaderKey() );

		List<YantraOrder> yantraOrders = new ArrayList<>();
		for( com.yantra.webservices.beans.order.history.OrderType orderType : orderListType.getOrder() ) {
			YantraOrder yantraOrder = new YantraOrder();
			yantraOrder.setDocumentType( orderType.getDocumentType() );
			yantraOrder.setMinOrderStatus( orderType.getMinOrderStatus() );
			yantraOrder.setMinOrderStatusDesc( orderType.getMinOrderStatusDesc() );
			yantraOrder.setOrderDate( orderType.getOrderDate() );
			yantraOrder.setOrderNo( orderType.getOrderNo() );
			yantraOrder.setStatus( orderType.getStatus() );
			yantraOrder.setGrandTotal(orderType.getOverallTotals().getGrandTotal());

			List<YantraOrderLine> orderLines = new ArrayList<>();

			for( OrderLineType orderLineType : orderType.getOrderLines().getOrderLine() ) {

				// Add one Order Line for each Order Status. This is required as it is possible have multiple Statuses for multiple
				// Quantities in the same Order Line. Also, for Split Quantity shipping scenario, Multiple Quantities in the same Order line
				// can be shipped differently with different tracking number
				if( orderLineType.getOrderStatuses() != null ) {
					OrderStatusesType orderStatusesList = orderLineType.getOrderStatuses();
					int orderedQty = orderLineType.getOriginalOrderedQty().intValue();
					
					boolean ignore1400Status = this.ignore1400Status(orderStatusesList, orderedQty);
					
					if( orderLineType.getOrderStatuses().getOrderStatus().size() > 0 ) { // We got some order Statuses to process
						for( OrderStatusType orderStatusType : orderLineType.getOrderStatuses().getOrderStatus() ) {
							
							if(ignore1400Status && "1400".equalsIgnoreCase(orderStatusType.getStatus())){
								continue;
							}
							// ----All attributes at Order Line level ---- STARTS HERE------
							YantraOrderLine yantraOrderLine = new YantraOrderLine();
							yantraOrderLine.setOrderLineKey( orderLineType.getOrderLineKey() );
							yantraOrderLine.setFulfillmentType(orderLineType.getFulfillmentType());
							
							if( orderLineType.getPersonInfoShipTo() != null ) {
								Address personInfoShipTo = new Address();
								personInfoShipTo.setAddress1( orderLineType.getPersonInfoShipTo().getAddressLine1() );
								personInfoShipTo.setAddress2( orderLineType.getPersonInfoShipTo().getAddressLine2() );
								personInfoShipTo.setCity( orderLineType.getPersonInfoShipTo().getCity() );
								personInfoShipTo.setState( orderLineType.getPersonInfoShipTo().getState() );
								personInfoShipTo.setCountryCode( orderLineType.getPersonInfoShipTo().getCountry() );
								personInfoShipTo.setPostalCode( orderLineType.getPersonInfoShipTo().getZipCode() );
								personInfoShipTo.setFirstName(orderLineType.getPersonInfoShipTo().getFirstName());
								personInfoShipTo.setLastName(orderLineType.getPersonInfoShipTo().getLastName());
								personInfoShipTo.setMiddleName(orderLineType.getPersonInfoShipTo().getMiddleName());
								
								yantraOrderLine.setPersoninfoshipto( personInfoShipTo );
							}
							
							if( orderLineType.getExtn() != null ) {
								YantraOrderLineExtn extn = new YantraOrderLineExtn();

								extn.setExtnItemBrand( orderLineType.getExtn().getExtnItemBrand() );
								extn.setExtnItemColor( orderLineType.getExtn().getExtnItemColor() );
								extn.setExtnItemMaterial( orderLineType.getExtn().getExtnItemMaterial() );
								extn.setExtnItemName( orderLineType.getExtn().getExtnItemName() );
								extn.setExtnItemSize( orderLineType.getExtn().getExtnItemSize() );
								extn.setExtnItemWidth( orderLineType.getExtn().getExtnItemWidth() );

								yantraOrderLine.setExtn( extn );
							}

							if( orderLineType.getLineOverallTotals() != null ) {
								YantraOrderLineOverallTotals totals = new YantraOrderLineOverallTotals();
								totals.setCharges( orderLineType.getLineOverallTotals().getCharges() );
								totals.setDiscount( orderLineType.getLineOverallTotals().getDiscount() );
								totals.setExtendedPrice( orderLineType.getLineOverallTotals().getExtendedPrice() );
								totals.setLineTotal( orderLineType.getLineOverallTotals().getLineTotal() );
								totals.setOptionPrice( orderLineType.getLineOverallTotals().getOptionPrice() );
								totals.setPricingQty( orderLineType.getLineOverallTotals().getPricingQty() );
								totals.setTax( orderLineType.getLineOverallTotals().getTax() );
								totals.setUnitPrice( orderLineType.getLineOverallTotals().getUnitPrice() );
								
								yantraOrderLine.setOrderLineOverallTotals( totals );
							}

							if( orderLineType.getItem() != null ) {
								YantraOrderItem item = new YantraOrderItem();

								item.setCustomerItem( orderLineType.getItem().getCustomerItem() );
								item.setItemDesc( orderLineType.getItem().getItemDesc() );
								item.setItemID( orderLineType.getItem().getItemID() );

								yantraOrderLine.setItem( item );
							}

							if( orderLineType.getLineTaxes() != null ) {
								List<YantraOrderLineTax> lineTaxes = new ArrayList<>();

								for( LineTaxType lineTaxType : orderLineType.getLineTaxes().getLineTax() ) {
									YantraOrderLineTax lineTax = new YantraOrderLineTax();
									lineTax.setChargeCategory( lineTaxType.getChargeCategory() );
									lineTax.setChargeName( lineTaxType.getChargeName() );
									lineTax.setInvoicedTax( lineTaxType.getInvoicedTax() );
									lineTax.setRemainingTax( lineTaxType.getRemainingTax() );
									lineTax.setTax( lineTaxType.getTax() );
									lineTax.setTaxName( lineTaxType.getTaxName() );

									lineTaxes.add( lineTax );
								}
								yantraOrderLine.setLineTaxes( lineTaxes );
							}
							// ----All attributes at Order line level ---- ENDS HERE --------

							// Now, process Order Status data and Shipping details
							YantraOrderStatus orderStatus = new YantraOrderStatus();
							orderStatus.setStatusDate( orderStatusType.getStatusDate() );
							orderStatus.setStatusDescription( orderStatusType.getStatusDescription() );
							orderStatus.setStatus( orderStatusType.getStatus() );
							orderStatus.setStatusQty( orderStatusType.getStatusQty() );
							orderStatus.setTotalQuantity( orderStatusType.getTotalQuantity() );
							List<YantraOrderStatus> orderStatuses = new ArrayList<>();
							orderStatuses.add( orderStatus );
							yantraOrderLine.setOrderStatuses( orderStatuses );

							ShipmentListType shipmentList = orderType.getShipmentList();

							for( ShipmentType shipmentType : shipmentList.getShipment() ) {
								String carrierName = shipmentType.getSCAC();
								ContainersType containersTypeList = shipmentType.getContainers();
								for( com.yantra.webservices.beans.order.history.ContainerType containerType : containersTypeList.getContainer() ) {
									String trackingNO = containerType.getTrackingNo();
									for( ContainerDetailType containerDetailType : containerType.getContainerDetails().getContainerDetail() ) {
										ShipmentLineType shipmentLineType = containerDetailType.getShipmentLine();
										// If Order Release Key at Shipment level is same as at the Order Status level AND order Line Key is
										// same at Order line level and Shipment level then use this tracking number and SCAC
										if( shipmentLineType.getOrderReleaseKey().equals( orderStatusType.getOrderReleaseKey() ) && shipmentLineType.getOrderLineKey().equals( yantraOrderLine.getOrderLineKey() ) ) {
											yantraOrderLine.setTrackingNumber( trackingNO );
											if(DigitalStringUtil.isNotEmpty(carrierName)) {
												yantraOrderLine.setCarrier(carrierName.trim());
											}
										}
									}
								}

							}

							// Now, add this Order Line to the orderLines collection
							orderLines.add( yantraOrderLine );

						}
					}
				} // Else condition is not handled for above two IFs. If Order Status doesn't exists then we don't care to add them. After
					// all, I wrote this logic for Order Status page. So, no point in sending Order lines without any Order Status

			} // END OF ORDER LINE FOR LOOP
			yantraOrder.setOrderLines( orderLines );

			yantraOrders.add( yantraOrder );
		}

		yantraOrderDetailsServiceResponse.setYantraOrders( yantraOrders );

		return yantraOrderDetailsServiceResponse;
	}
	
	protected boolean ignore1400Status(OrderStatusesType orderStatuses, int orderedQty) {
		// would sum up status qty and check if sum of status qty is greater
		// than OrderedQty, then we can ignore
		// As per Yantra team; they can't remove this as it is Yarta Out of box
		// API code that generates this Notication xml
		int statusQty = 0;

		for( OrderStatusType orderStatus : orderStatuses.getOrderStatus() ) {
			statusQty += orderStatus.getStatusQty().intValue();
		}

		return (statusQty > orderedQty);
	}

	private OrderListType populateOrderListType( HttpServiceResponse serviceResponse ) throws Exception {

		Document bodyContentDocument = getDocumentFromResponse( serviceResponse );

		if( orderDetailsRestrictionsXPath == null ) {
			orderDetailsRestrictionsXPath = XPathFactory.newInstance().newXPath().compile( ORDER_DETAILS_XPATH_EXPRESSION );
		}

		NodeList nodes = (NodeList)orderDetailsRestrictionsXPath.evaluate( bodyContentDocument.getDocumentElement(), XPathConstants.NODESET );

		if( nodes == null || nodes.getLength() != 1 ) { throw new DigitalIntegrationBusinessException( "OrderListType populate Error", ErrorCodes.XFORM.getCode() ); }

		Node responseNode = nodes.item( 0 );
		String responseText = responseNode.getTextContent();
		DocumentBuilder	builder	 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document responseDocument = builder.parse( new InputSource( new StringReader( responseText ) ) );

		JAXBContext jaxbContext = getJaxbContextCache().newJAXBContext( OrderListType.class );
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

		return (OrderListType)JAXBIntrospector.getValue( unmarshaller.unmarshal( responseDocument ) );

	}
	
    public Map<String, Boolean> getServiceMethodsEnabled(){
		Map<String, Boolean> methods = new HashMap<>();
		for (ServiceMethod service: ServiceMethod.values()) {
			methods.put(service.getServiceMethodName(), isServiceMethodEnabled(service.getServiceMethodName()));
		}
		
		return methods;
    }

}
