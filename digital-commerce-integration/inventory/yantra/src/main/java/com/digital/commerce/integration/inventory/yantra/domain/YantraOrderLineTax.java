/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraOrderLineTax {

	private String chargeCategory;
	private String chargeName;
	private Double invoicedTax;
	private Double remainingTax;
	private Double tax;
	private String taxName;

}
