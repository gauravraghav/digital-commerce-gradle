/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraShippingRestrictionsServiceResponse {

	private XMLGregorianCalendar driverDate;
	private String orderIdentifier = "";
	private String organizationCode = "";
	private String sellingOrganization = "";
	private String type = "";
	private List<YantraShippingOption> yantraShippingOptions = new ArrayList<>();

}
