/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

import javax.xml.datatype.XMLGregorianCalendar;

/** @author mmallipu */
@Getter
@Setter
public class YantraOrderStatus {

	private XMLGregorianCalendar	statusDate;
	private String					statusDescription	= "";
	private String					status	= "";
	private Double					totalQuantity;
	private Double					statusQty;

}
