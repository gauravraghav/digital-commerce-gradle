/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import com.digital.commerce.integration.common.domain.Address;
import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraShippingGroup {

	private String shipToGroupIdentifier = "";
	private String shipFromNode = "";
	private Address personInfoShipTo;
	private YantraShipToSHCosts yantraShipToSHCosts;
	private String los;
	private String associatedShipToReturnGroupIdentifier;
	private String associatedShipToSalesGroupIdentifier;
	private String carrierCode;
	private XMLGregorianCalendar estimatedDeliveryDate;
	private String estimatedShipDate;
	private String losName;
	private YantraType promotionApplied;
	private List<YantraShippingItem> yantraShippingItems = new ArrayList<>();
	private List<YantraType> restrictionList;
	private String losAllowed;

}