/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraShipToSHNetCostsType {

	private Double brokenPromotionCost = 0.0;
	private Double handlingNetCosts = 0.0;
	private Double restockingNetCosts = 0.0;
	private Double returnNetCosts = 0.0;
	private Double shippingNetCosts = 0.0;
	private Double totalNetCosts = 0.0;
	private String value = "";

}
