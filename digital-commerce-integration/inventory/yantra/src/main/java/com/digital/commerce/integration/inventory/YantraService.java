/**
 * 
 */
package com.digital.commerce.integration.inventory;

import com.digital.commerce.integration.inventory.yantra.domain.YantraInventoryReservationServiceRequest;
import com.digital.commerce.integration.inventory.yantra.domain.YantraInventoryReservationServiceResponse;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderDetailsServiceRequest;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderDetailsServiceResponse;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingRestrictionsServiceRequest;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingRestrictionsServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.service.Service;

import lombok.Getter;

/**
 * @author mmallipu
 *
 */
public interface YantraService extends Service{

	@Getter
	public enum ServiceMethod {
		RESERVATION("i32"),
		SHIPPING_RESTRICTIONS("ps03"),
		ORDER_DETAILS("s79");
		
		String serviceMethodName;
		
		ServiceMethod(String serviceMethodName) {
			this.serviceMethodName = serviceMethodName;
		}

	}
	
   public YantraInventoryReservationServiceResponse reservation(YantraInventoryReservationServiceRequest yantraInventoryServiceRequest) throws DigitalIntegrationException;
   public YantraShippingRestrictionsServiceResponse shippingRestrictions(YantraShippingRestrictionsServiceRequest yantraShippingRestrictionsServiceRequest) throws DigitalIntegrationException;
   public YantraOrderDetailsServiceResponse orderDetails(YantraOrderDetailsServiceRequest yantraOrderDetailsServiceRequest) throws DigitalIntegrationException;
  
}
