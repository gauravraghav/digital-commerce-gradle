/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraShipToSHCosts {

	YantraShippingSHGrossCosts yantraShippingSHGrossCosts;
	YantraShipToSavingsType yantraShipToSavingsType;
	YantraShipToSHNetCostsType yantraShipToSHNetCostsType;

}
