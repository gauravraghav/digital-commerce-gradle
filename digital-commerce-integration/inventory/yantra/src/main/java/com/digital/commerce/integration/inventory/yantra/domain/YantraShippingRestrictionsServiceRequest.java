/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraShippingRestrictionsServiceRequest {

	private List<YantraShippingGroup> yantraShippingGroup ;
	private XMLGregorianCalendar driverDate ;
	private String type = "";
	private String orderIdentifier = "";

}
