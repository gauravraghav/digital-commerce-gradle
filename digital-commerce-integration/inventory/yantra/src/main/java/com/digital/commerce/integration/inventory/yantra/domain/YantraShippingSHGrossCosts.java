/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraShippingSHGrossCosts {
	private Double handlingGrossCosts = 0.0;
	private Double restockingGrossCosts = 0.00;
	private Double returnGrossCosts = 0.00;
	private Double shippingGrossCosts = 0.00;
	private Double totalGrossCosts = 0.00;
	private String value = "";
	
}
