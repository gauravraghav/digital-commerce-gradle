/**
 * 
 */
package com.digital.commerce.integration.inventory.yantra.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class YantraOrderLineExtn {
	
	private String extnItemBrand = "";
	private String extnItemColor = "";
	private String extnItemMaterial = "";
	private String extnItemName = "";
	private String extnItemSize = "";
	private String extnItemWidth = "";

}
