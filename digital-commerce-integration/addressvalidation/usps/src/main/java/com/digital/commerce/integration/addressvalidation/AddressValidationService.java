package com.digital.commerce.integration.addressvalidation;

import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceRequest;
import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.service.Service;

import lombok.Getter;


/**
 * Interface to be used by the application to perform address validation
 * @author JM406760
 *
 */
public interface AddressValidationService extends Service{

	@Getter
	public enum ServiceMethod {
		VALIDATE("ps04");
		
		String serviceMethodName;
		
		ServiceMethod(String serviceMethodName) {
			this.serviceMethodName = serviceMethodName;
		}

	}
	
	public AddressValidationServiceResponse validate(AddressValidationServiceRequest addressValidationRequest)  throws DigitalIntegrationException;
}
