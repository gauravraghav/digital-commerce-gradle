package com.digital.commerce.integration.addressvalidation.usps;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.addressvalidation.AddressValidationService;
import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceRequest;
import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceResponse;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.configuration.WebServicesConfig.ServicNames;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.common.service.client.ServiceRequest;
import com.digital.commerce.integration.common.service.client.ServiceRequest.ServiceContentType;
import com.digital.commerce.integration.common.service.client.ServiceRequest.ServiceHttpMethod;
import com.digital.commerce.integration.storedvalue.StoredValueService;
import com.digital.commerce.integration.external.addressvalidation.usps.Address;
import com.digital.commerce.integration.external.addressvalidation.usps.AddressValidateRequest;
import com.digital.commerce.integration.external.addressvalidation.usps.AddressValidateResponse;
import com.digital.commerce.integration.external.addressvalidation.usps.ObjectFactory;

import java.math.BigInteger;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class USPSAddressValidationService extends BaseIntegrationService implements AddressValidationService {

	private static final DigitalLogger logger = DigitalLogger.getLogger(StoredValueService.class);
		
	private IntegrationServiceClient serviceClient;
	
	@Override
	public void postStartup() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void postShutdown() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	@Override
	protected String getService() {
		return ServicNames.USPS.getName();
	}

	@SuppressWarnings("deprecation")
	@Override
	public AddressValidationServiceResponse validate(AddressValidationServiceRequest addressValidationServiceRequest)
			throws DigitalIntegrationException {
		String serviceMethodName = ServiceMethod.VALIDATE.getServiceMethodName();
		boolean hasException = false;
		
		if (doRunService(serviceMethodName)) {
			
			startPerformanceMonitorOperation(serviceMethodName);
			
			try {
				ObjectFactory ob = new ObjectFactory();

				AddressValidateRequest addressValidateRequest = ob.createAddressValidateRequest();
				addressValidateRequest.setUSERID(getUserId());

				Address address = new Address();
				address.setID(new BigInteger(new byte[] { 0 }));

				// USPS reverses address line 1 & 2
				address.setAddress1(addressValidationServiceRequest.getAddressLine2());
				address.setAddress2(addressValidationServiceRequest.getAddressLine1());
				address.setCity(addressValidationServiceRequest.getCity());
				address.setState(addressValidationServiceRequest.getState());
				address.setZip5(addressValidationServiceRequest.getZip5());
				address.setZip4(addressValidationServiceRequest.getZip4());
				addressValidateRequest.setAddress(address);

				String content = serviceClient.getMarshalledStringFromObject(AddressValidateRequest.class, addressValidateRequest);

				/*
				 * Weird REST service in that it wants the request as a URL
				 * encoded XML string as part of the endpoint
				 */

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName), getRequestTimeout(serviceMethodName), getDefaultUri() + URLEncoder.encode(content),
						ServiceContentType.XML, ServiceHttpMethod.POST, serviceMethodName);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				validateHttpResponse(response);

				AddressValidateResponse addressValidateResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(AddressValidateResponse.class, response);

				if (addressValidateResponse.getError() != null) {
					throw new DigitalIntegrationBusinessException("Exception while validating: " + this.getComponentName(),
							ErrorCodes.ERROR_RESPONSE.getCode());
				}

				AddressValidationServiceResponse addressValidationServiceResponse = populateResponse(
						addressValidationServiceRequest, addressValidateResponse);

				return addressValidationServiceResponse;

			} catch (Exception e) {
				hasException = true;
				
				
				throw new DigitalIntegrationBusinessException("AddressValidation Error",
						ErrorCodes.ERROR_RESPONSE.getCode(), e);
				
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}
		} else {
			
			throw new DigitalIntegrationInactiveException(String.format("Service / Method Is Inactive: %s_%s",
					getComponentName(), ServiceMethod.VALIDATE.getServiceMethodName()), ErrorCodes.INACTIVE.getCode());

		}
	}

	/*
	 * on successful call to usps with no suggestion - only return the input
	 * address (decision-accepted) on successful call to usps with a suggestion
	 * return the original address and the suggested address
	 * (descision-suggested) on unsuccessful call return no addresses
	 * (decision-error)
	 */
	protected AddressValidationServiceResponse populateResponse(
			AddressValidationServiceRequest addressValidationServiceRequest,
			AddressValidateResponse addressValidateResponse) {
		AddressValidationServiceResponse addressValidationServiceResponse = new AddressValidationServiceResponse();
		
		// if city is null then we can assume the address isn't valid
		
		if (addressValidateResponse.getError() != null) {
			addressValidationServiceResponse.setSuccess(false);
			addressValidationServiceResponse
					.setAddressValidationDecision(AddressValidationServiceResponse.AddressValidationDecision.ERROR);
			addressValidationServiceResponse
					.setAddressValidationDecisionDescription(addressValidateResponse.getError().getDescription());
			addressValidationServiceResponse
			.setAddressValidationDecisionDescription(addressValidateResponse.getError().getDescription());
		} else if (addressValidateResponse.getAddress().getCity() == null) {
			addressValidationServiceResponse.setSuccess(false);
			addressValidationServiceResponse
					.setAddressValidationDecision(AddressValidationServiceResponse.AddressValidationDecision.ERROR);
			addressValidationServiceResponse
					.setAddressValidationDecisionDescription(addressValidateResponse.getAddress().getReturnText());
		} else {
			// if any of the fields returned differs from the original then it
			// is suggested
			boolean isSuggested = false;

			if (!DigitalStringUtil.equalsIgnoreCase(addressValidationServiceRequest.getAddressLine1(),
					addressValidateResponse.getAddress().getAddress2())) {
				isSuggested = true;
			}

			if (DigitalStringUtil.isNotEmpty(addressValidationServiceRequest.getAddressLine2())
					|| DigitalStringUtil.isNotBlank(addressValidateResponse.getAddress().getAddress1())) {
				if (!DigitalStringUtil.equalsIgnoreCase(addressValidationServiceRequest.getAddressLine2(),
						addressValidateResponse.getAddress().getAddress1())) {
					isSuggested = true;
				}
			}

			if (!DigitalStringUtil.equalsIgnoreCase(addressValidationServiceRequest.getCity(),
					addressValidateResponse.getAddress().getCity())) {
				isSuggested = true;
			}

			if (!DigitalStringUtil.equalsIgnoreCase(addressValidationServiceRequest.getState(),
					addressValidateResponse.getAddress().getState())) {
				isSuggested = true;
			}

			if (!DigitalStringUtil.isEmpty(addressValidationServiceRequest.getZip4())) {
				if (!DigitalStringUtil.equalsIgnoreCase(addressValidationServiceRequest.getZip4(),
						addressValidateResponse.getAddress().getZip4())) {
					isSuggested = true;
				}
			} else {
				addressValidationServiceResponse.setZip4(addressValidateResponse.getAddress().getZip4());
			}

			if (!DigitalStringUtil.equalsIgnoreCase(addressValidationServiceRequest.getZip5(),
					addressValidateResponse.getAddress().getZip5())) {
				isSuggested = true;
			}

			addressValidationServiceResponse.setAddressLine1(addressValidationServiceRequest.getAddressLine1());
			addressValidationServiceResponse.setAddressLine2(addressValidationServiceRequest.getAddressLine2());
			addressValidationServiceResponse.setCity(addressValidationServiceRequest.getCity());
			addressValidationServiceResponse.setState(addressValidationServiceRequest.getState());
			addressValidationServiceResponse.setZip5(addressValidationServiceRequest.getZip5());
			addressValidationServiceResponse.setZip4(addressValidationServiceRequest.getZip4());

			if (isSuggested) {
				addressValidationServiceResponse
						.setSuggestedAddressLine1(addressValidateResponse.getAddress().getAddress2());
				addressValidationServiceResponse
						.setSuggestedAddressLine2(addressValidateResponse.getAddress().getAddress1());
				addressValidationServiceResponse.setSuggestedCity(addressValidateResponse.getAddress().getCity());
				addressValidationServiceResponse.setSuggestedState(addressValidateResponse.getAddress().getState());
				addressValidationServiceResponse.setSuggestedZip5(addressValidateResponse.getAddress().getZip5());
				addressValidationServiceResponse.setSuggestedZip4(addressValidateResponse.getAddress().getZip4());
				addressValidationServiceResponse.setAddressValidationDecision(
						AddressValidationServiceResponse.AddressValidationDecision.SUGGESTED);
				addressValidationServiceResponse
						.setAddressValidationDecisionDescription(addressValidateResponse.getAddress().getReturnText());
			} else {
				addressValidationServiceResponse.setAddressValidationDecision(
						AddressValidationServiceResponse.AddressValidationDecision.ACCEPTED);
			}

			addressValidationServiceResponse.setSuccess(true);
		}

		return addressValidationServiceResponse;
	}

	public String getUserId() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, "userId");
	}


	public String getDefaultUri() {
		return getMapData(WebServicesConfig.DataTypes.STATIC, "defaultUri");
	}
	
    public Map<String, Boolean> getServiceMethodsEnabled(){
		Map<String, Boolean> methods = new HashMap<>();
		for (ServiceMethod service: ServiceMethod.values()) {
			methods.put(service.getServiceMethodName(), isServiceMethodEnabled(service.getServiceMethodName()));
		}
		
		return methods;
    }

}
