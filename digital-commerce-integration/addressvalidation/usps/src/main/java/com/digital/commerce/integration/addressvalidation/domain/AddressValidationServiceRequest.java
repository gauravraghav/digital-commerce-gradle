package com.digital.commerce.integration.addressvalidation.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddressValidationServiceRequest {

  private String addressLine1 = "";
  private String addressLine2 = "";
  private String city = "";
  private String state = "";
  private String zip5 = "";
  private String zip4 = "";

}
