package com.digital.commerce.integration.addressvalidation.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddressValidationServiceResponse {

  @Getter
  public enum AddressValidationDecision {
    ACCEPTED("Accepted"),
    SUGGESTED("Suggested"),
    ERROR("Error");

    private String description;

    AddressValidationDecision(String description) {
      this.description = description;
    }

  }

  private String addressLine1;
  private String addressLine2;
  private String city;
  private String state;
  private String zip5;
  private String zip4;
  private String suggestedAddressLine1;
  private String suggestedAddressLine2;
  private String suggestedCity;
  private String suggestedState;
  private String suggestedZip5;
  private AddressValidationDecision addressValidationDecision;
  private String addressValidationDecisionDescription = "";
  private String addressValidationDecisionCode = "";
  private String suggestedZip4;
  private boolean success;

}
