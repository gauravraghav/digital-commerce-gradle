package com.digital.commerce.integration.tax.domain;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TaxServiceRequest {
	
	private String orderNumber;
	private List<TaxServiceLineItem> lineItems;

}
