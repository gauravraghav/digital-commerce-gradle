/**
 * 
 */
package com.digital.commerce.integration.tax.domain;

/**
 * @author mmallipu
 *
 */
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TaxServiceProduct {

	private String productClass;
	
	private String value;


}
