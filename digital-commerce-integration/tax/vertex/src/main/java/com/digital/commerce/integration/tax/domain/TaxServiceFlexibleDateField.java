package com.digital.commerce.integration.tax.domain;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TaxServiceFlexibleDateField  implements Serializable {

	private static final long serialVersionUID = -1L;
	protected XMLGregorianCalendar value;
	protected int fieldId;

}
