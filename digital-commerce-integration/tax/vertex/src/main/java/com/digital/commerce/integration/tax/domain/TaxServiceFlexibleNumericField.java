package com.digital.commerce.integration.tax.domain;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TaxServiceFlexibleNumericField  implements Serializable {

	private static final long serialVersionUID = -1L;
	protected Double value;
	protected int fieldId;

}
