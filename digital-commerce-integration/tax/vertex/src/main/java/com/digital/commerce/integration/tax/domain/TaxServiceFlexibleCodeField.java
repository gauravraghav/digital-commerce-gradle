package com.digital.commerce.integration.tax.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TaxServiceFlexibleCodeField  implements Serializable {

	private static final long serialVersionUID = -1L;
	protected String value;
	protected int fieldId;
	
}
