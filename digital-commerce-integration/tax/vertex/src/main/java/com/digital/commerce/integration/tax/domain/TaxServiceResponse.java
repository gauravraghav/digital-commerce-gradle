package com.digital.commerce.integration.tax.domain;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TaxServiceResponse {
	private Double subTotal;
	
	private Double total;
	
	private Double amount;

	private String currency;
	
	List<TaxServiceLineItem> lineItems;

}
