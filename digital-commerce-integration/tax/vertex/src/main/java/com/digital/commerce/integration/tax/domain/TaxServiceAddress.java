/**
 * 
 */
package com.digital.commerce.integration.tax.domain;

/**
 * @author mmallipu
 *
 */
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TaxServiceAddress {

	private String streetAddress;
	
	private String city;
	
	private String mainDivision;
	
	private String postalCode;
	
	private String country;
	
	private String subDivision;

}
