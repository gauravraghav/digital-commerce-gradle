/**
 * 
 */
package com.digital.commerce.integration.tax.domain;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */

@Setter
@Getter
public class TaxServiceLineItem {

	private TaxServiceProduct taxServiceProduct;
	
	private TaxServiceAddress destination;
	
	private long quantity = 0L;
	
	private Double amount = 0.0;
	
	private Double countryTax = 0.0;
	
	private Double countyTax = 0.0;
	
	private Double stateTax = 0.0;
	
	private Double cityTax = 0.0;
	
	private Integer lineItemNumber = 0;
	
	private boolean isNonTaxable = false;
	
	private List<TaxServiceFlexibleCodeField> taxServiceFlexibleCodeFields = new ArrayList<>();
	private List<TaxServiceFlexibleDateField> taxServiceFlexibleDateFields = new ArrayList<>();
	private List<TaxServiceFlexibleNumericField> taxServiceFlexibleNumericFields = new ArrayList<>();

}
