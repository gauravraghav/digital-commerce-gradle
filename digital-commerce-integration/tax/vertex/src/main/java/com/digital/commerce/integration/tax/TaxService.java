package com.digital.commerce.integration.tax;

import lombok.Getter;

import com.digital.commerce.integration.tax.domain.TaxServiceRequest;
import com.digital.commerce.integration.tax.domain.TaxServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;;
import com.digital.commerce.integration.common.service.Service;

/**
 * Interface to be used by the application to perform tax calculation
 * @author JM406760
 *
 */

public interface TaxService extends Service{
	@Getter
	public enum ServiceMethod {
		CALCULATE_TAX("fp06");
		
		String serviceMethodName;
		
		ServiceMethod(String serviceMethodName) {
			this.serviceMethodName = serviceMethodName;
		}
	}
	
	public TaxServiceResponse calculateTax(TaxServiceRequest taxServiceRequest) throws DigitalIntegrationException;
	
}
