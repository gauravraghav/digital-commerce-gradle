package com.digital.commerce.integration.tax.vertex;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.configuration.WebServicesConfig;
import com.digital.commerce.integration.common.domain.HttpServiceResponse;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.service.BaseIntegrationService;
import com.digital.commerce.integration.common.service.client.IntegrationServiceClient;
import com.digital.commerce.integration.common.service.client.ServiceRequest;
import com.digital.commerce.integration.common.service.client.ServiceRequest.ServiceContentType;
import com.digital.commerce.integration.common.service.client.ServiceRequest.ServiceHttpMethod;
import com.digital.commerce.integration.tax.TaxService;
import com.digital.commerce.integration.tax.domain.TaxServiceFlexibleCodeField;
import com.digital.commerce.integration.tax.domain.TaxServiceFlexibleDateField;
import com.digital.commerce.integration.tax.domain.TaxServiceFlexibleNumericField;
import com.digital.commerce.integration.tax.domain.TaxServiceLineItem;
import com.digital.commerce.integration.tax.domain.TaxServiceRequest;
import com.digital.commerce.integration.tax.domain.TaxServiceResponse;
import com.digital.commerce.integration.util.PropertyUtil;
import com.digital.commerce.integration.external.tax.vertex.AmountType;
import com.digital.commerce.integration.external.tax.vertex.CustomerCodeType;
import com.digital.commerce.integration.external.tax.vertex.CustomerType;
import com.digital.commerce.integration.external.tax.vertex.FlexibleFields;
import com.digital.commerce.integration.external.tax.vertex.FlexibleFields.FlexibleCodeField;
import com.digital.commerce.integration.external.tax.vertex.FlexibleFields.FlexibleDateField;
import com.digital.commerce.integration.external.tax.vertex.FlexibleFields.FlexibleNumericField;
import com.digital.commerce.integration.external.tax.vertex.LineItemQSIType;
import com.digital.commerce.integration.external.tax.vertex.LineItemQSOType;
import com.digital.commerce.integration.external.tax.vertex.LocationType;
import com.digital.commerce.integration.external.tax.vertex.LoginType;
import com.digital.commerce.integration.external.tax.vertex.MeasureType;
import com.digital.commerce.integration.external.tax.vertex.ObjectFactory;
import com.digital.commerce.integration.external.tax.vertex.Product;
import com.digital.commerce.integration.external.tax.vertex.QuotationRequestType;
import com.digital.commerce.integration.external.tax.vertex.SaleTransactionType;
import com.digital.commerce.integration.external.tax.vertex.SellerType;
import com.digital.commerce.integration.external.tax.vertex.TaxOverride;
import com.digital.commerce.integration.external.tax.vertex.TaxOverrideCodeType;
import com.digital.commerce.integration.external.tax.vertex.Taxes;
import com.digital.commerce.integration.external.tax.vertex.VertexEnvelope;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class VertexTaxService extends BaseIntegrationService implements TaxService {
	private static final DigitalLogger logger = DigitalLogger.getLogger(TaxService.class);

	private static final String ORIGIN_STREETADDRESS = "streetAddress";
	private static final String ORIGIN_CITY = "city";
	private static final String ORIGIN_POSTALCODE = "postalCode";
	private static final String ORIGIN_COUNTRY = "country";
	private static final String ORIGIN_STATE = "state";
	private static final String ORIGIN_COUNTY = "county";

	private static final String SD_DOCUMENTNUMBER = "documentNumber";
	private static final String SD_CUSTOMERCODETYPE = "customerCodeType";

	private IntegrationServiceClient serviceClient;

	public void postStartup() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}

	public void postShutdown() throws DigitalIntegrationException {
		// TODO Auto-generated method stub

	}
		
	protected String getService() {
		return WebServicesConfig.ServicNames.VERTEX.getName();
	}
	
	public TaxServiceResponse calculateTax(TaxServiceRequest taxServiceRequest) throws DigitalIntegrationException {
		String serviceMethodName = ServiceMethod.CALCULATE_TAX.getServiceMethodName();
		boolean hasException = false;

		if (doRunService(serviceMethodName)) {
			startPerformanceMonitorOperation(serviceMethodName);

			try {

				VertexEnvelope vertexEnvelope = null;

				vertexEnvelope = getVertexEnvelopeRequest(taxServiceRequest);

				String content = serviceClient.getMarshalledStringFromObject(VertexEnvelope.class, vertexEnvelope);

				ServiceRequest serviceRequest = new ServiceRequest(getConnectionTimeout(serviceMethodName),
						getRequestTimeout(serviceMethodName), getDefaultUri(), ServiceContentType.XML,
						ServiceHttpMethod.POST, serviceMethodName);
				serviceRequest.setBody(content);

				HttpServiceResponse response = serviceClient.processHttpRequest(serviceRequest);

				validateHttpResponse(response);

				VertexEnvelope vertexEnvelopeResponse = serviceClient
						.getUnmarshalledObjectFromHttpServiceResponse(VertexEnvelope.class, response);

				if (vertexEnvelopeResponse == null) {
					logger.error(String.format("Exception while validating: %s - %s", this.getComponentName(),
							ErrorCodes.ERROR_RESPONSE.getCode()));

					throw new DigitalIntegrationSystemException("Exception while validating: " + this.getComponentName(),
							ErrorCodes.ERROR_RESPONSE.getCode());

				}

				TaxServiceResponse taxServiceResponse = new TaxServiceResponse();

				taxServiceResponse = getVertexResponse(vertexEnvelopeResponse);

				return taxServiceResponse;

			} catch (Exception e) {
				hasException = true;
				logger.error(String.format("Tax calculation Error: %s", ErrorCodes.COMM_ERROR.getCode()), e);

				throw new DigitalIntegrationBusinessException("Tax calculation Error", ErrorCodes.COMM_ERROR.getCode(), e);
			} finally {
				endPerformanceMonitorOperation(serviceMethodName, hasException);
			}

		} else {
			logger.warn(String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName));

			throw new DigitalIntegrationInactiveException(
					String.format("Service / Method Is Inactive: %s_%s", getComponentName(), serviceMethodName),
					ErrorCodes.INACTIVE.getCode());

		}
	}

	public Map<String, String> getStaticData() {
		return getMapData(WebServicesConfig.DataTypes.STATIC);
	}


	public String getDefaultUri() {
		return getMapData(WebServicesConfig.DataTypes.STATIC,"defaultUri");
	}


	public String getTrustedId() {
		return getWebServicesConfig().getVaultedData(getMapData(WebServicesConfig.DataTypes.STATIC,"trustedId"));
	}


	public String getDocumentNumber() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_DOCUMENTNUMBER).toString();
	}

	public void setDocumentNumber(String documentNumber) {
		PropertyUtil.setMappedProperty(getStaticData(), SD_DOCUMENTNUMBER, documentNumber);
	}

	public String getCustomerCodeType() {
		return PropertyUtil.getMappedProperty(getStaticData(), SD_CUSTOMERCODETYPE).toString();
	}

	public void setCustomerCodeType(String customerCodeType) {
		PropertyUtil.setMappedProperty(getStaticData(), SD_CUSTOMERCODETYPE, customerCodeType);
	}

	protected VertexEnvelope getVertexEnvelopeRequest(TaxServiceRequest taxServiceRequest)
			throws DatatypeConfigurationException {
		ObjectFactory objectFactory = new ObjectFactory();

		LoginType loginType = objectFactory.createLoginType();
		loginType.setTrustedId(getTrustedId());

		GregorianCalendar c = new GregorianCalendar();
		c.setTime(new Date());
		XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

		SellerType sellerType = objectFactory.createSellerType();

		//TODO Add these values to vertex's static config
		sellerType.setDivision("DSWDIRECT");
		sellerType.setCompany("DSWINC");
		


		LocationType physicalOrigin = objectFactory.createLocationType();
		physicalOrigin.setStreetAddress1(getPhysicalOriginAddress().get(ORIGIN_STREETADDRESS));
		physicalOrigin.setCity(getPhysicalOriginAddress().get(ORIGIN_CITY));
		physicalOrigin.setMainDivision(getPhysicalOriginAddress().get(ORIGIN_STATE));
		physicalOrigin.setSubDivision(getPhysicalOriginAddress().get(ORIGIN_COUNTY));
		physicalOrigin.setPostalCode(getPhysicalOriginAddress().get(ORIGIN_POSTALCODE));
		physicalOrigin.setCountry(getPhysicalOriginAddress().get(ORIGIN_COUNTRY));

		sellerType.setPhysicalOrigin(physicalOrigin);

		LocationType administrativeOrigin = objectFactory.createLocationType();
		administrativeOrigin.setStreetAddress1(getAdministrativeOriginAddress().get(ORIGIN_STREETADDRESS));
		administrativeOrigin.setCity(getAdministrativeOriginAddress().get(ORIGIN_CITY));
		administrativeOrigin.setMainDivision(getAdministrativeOriginAddress().get(ORIGIN_STATE));
		administrativeOrigin.setSubDivision(getAdministrativeOriginAddress().get(ORIGIN_COUNTY));
		administrativeOrigin.setPostalCode(getAdministrativeOriginAddress().get(ORIGIN_POSTALCODE));
		administrativeOrigin.setCountry(getAdministrativeOriginAddress().get(ORIGIN_COUNTRY));

		sellerType.setAdministrativeOrigin(administrativeOrigin);

		CustomerType customerType = objectFactory.createCustomerType();
		CustomerCodeType customerCodeType = objectFactory.createCustomerCodeType();
		customerCodeType.setValue(getCustomerCodeType());
		customerType.setCustomerCode(customerCodeType);

		QuotationRequestType quotationRequest = objectFactory.createQuotationRequestType();
		quotationRequest.setDocumentDate(xmlGregorianCalendar);
		quotationRequest.setDocumentNumber(taxServiceRequest.getOrderNumber());
		quotationRequest.setTransactionType(SaleTransactionType.SALE);
		quotationRequest.setSeller(sellerType);
		quotationRequest.setCustomer(customerType);

		for (TaxServiceLineItem lineItem : taxServiceRequest.getLineItems()) {
			CustomerType newCustomerType = objectFactory.createCustomerType();

			LocationType destination = objectFactory.createLocationType();
			destination.setStreetAddress1(lineItem.getDestination().getStreetAddress());
			destination.setCity(lineItem.getDestination().getCity());
			destination.setMainDivision(lineItem.getDestination().getMainDivision());
			destination.setPostalCode(lineItem.getDestination().getPostalCode());
			destination.setCountry(lineItem.getDestination().getCountry());
			newCustomerType.setDestination(destination);

			Product newProduct = objectFactory.createProduct();
			newProduct.setProductClass(lineItem.getTaxServiceProduct().getProductClass());
			newProduct.setValue(lineItem.getTaxServiceProduct().getValue());

			LineItemQSIType newLineItem = objectFactory.createLineItemQSIType();
			newLineItem.setCustomer(newCustomerType);
			newLineItem.setProduct(newProduct);
			newLineItem.setLineItemNumber(BigInteger.valueOf(lineItem.getLineItemNumber()));

			if (lineItem.isNonTaxable()) {
				TaxOverride taxOverride = objectFactory.createTaxOverride();
				taxOverride.setOverrideType(TaxOverrideCodeType.NONTAXABLE);
				newLineItem.setTaxOverride(taxOverride);
			}

			if (lineItem.getQuantity() > 0l) {
				MeasureType measureType = objectFactory.createMeasureType();
				measureType.setValue(new BigDecimal(lineItem.getQuantity()));
				newLineItem.setQuantity(measureType);
			}

			FlexibleFields flexibleFields = objectFactory.createFlexibleFields();

			for (TaxServiceFlexibleCodeField field : lineItem.getTaxServiceFlexibleCodeFields()) {
				FlexibleCodeField vField = objectFactory.createFlexibleFieldsFlexibleCodeField();
				vField.setFieldId(field.getFieldId());
				vField.setValue(field.getValue());
				
				flexibleFields.getFlexibleCodeField().add(vField);
				
								
			}
			
			for (TaxServiceFlexibleNumericField field : lineItem.getTaxServiceFlexibleNumericFields()) {
				FlexibleNumericField vField = objectFactory.createFlexibleFieldsFlexibleNumericField();
				vField.setFieldId(field.getFieldId());
				vField.setValue(field.getValue());
				
				flexibleFields.getFlexibleNumericField().add(vField);
			}
			
			for (TaxServiceFlexibleDateField field : lineItem.getTaxServiceFlexibleDateFields()) {
				FlexibleDateField vField = objectFactory.createFlexibleFieldsFlexibleDateField();
				vField.setFieldId(field.getFieldId());
				vField.setValue(field.getValue());
				
				flexibleFields.getFlexibleDateField().add(vField);
			}
			
			newLineItem.setFlexibleFields(flexibleFields);
			
			AmountType newAmount = objectFactory.createAmountType();
			newAmount.setValue(new BigDecimal(lineItem.getAmount()));
			newLineItem.setUnitPrice(newAmount);

			//quotationRequest.getLineItems().add(newLineItem);
		}

		VertexEnvelope vertexEnvelope = objectFactory.createVertexEnvelope();
		vertexEnvelope.setLogin(loginType);
		vertexEnvelope.setQuotationRequest(quotationRequest);
		
		return vertexEnvelope;

	}

	protected TaxServiceResponse getVertexResponse(VertexEnvelope vertexEnvelope) {
		TaxServiceResponse taxServiceResponse = new TaxServiceResponse();

		taxServiceResponse.setSubTotal(DigitalCommonUtil.round(vertexEnvelope.getQuotationResponse().getSubTotal().getValue().doubleValue()));
		taxServiceResponse.setTotal(DigitalCommonUtil.round(vertexEnvelope.getQuotationResponse().getTotal().getValue().doubleValue()));
		taxServiceResponse.setAmount(DigitalCommonUtil.round(vertexEnvelope.getQuotationResponse().getTotalTax().getValue().doubleValue()));

		if (vertexEnvelope.getQuotationResponse().getCurrency() != null)
			taxServiceResponse.setCurrency(vertexEnvelope.getQuotationResponse().getCurrency().toString());

		String jurisdictionLevel = "";
		Double calculatedTax = 0.0;

		List<TaxServiceLineItem> lineItems = new ArrayList<>();
		for (LineItemQSOType lineItem : vertexEnvelope.getQuotationResponse().getLineItems()) {
			calculatedTax = 0.0;
			String flag = "F";
			TaxServiceLineItem taxServicelineItem = new TaxServiceLineItem();
			for (Taxes taxType : lineItem.getTaxes()) {
				jurisdictionLevel = "";
				jurisdictionLevel = taxType.getJurisdiction().getJurisdictionLevel().toString();

				if (DigitalStringUtil.equalsIgnoreCase(jurisdictionLevel, "COUNTRY")) {
					taxServicelineItem.setCountryTax(DigitalCommonUtil.round(taxType.getCalculatedTax().getValue().doubleValue()));
				} else if (DigitalStringUtil.equalsIgnoreCase(jurisdictionLevel, "STATE")) {
					taxServicelineItem.setStateTax(DigitalCommonUtil.round(taxType.getCalculatedTax().getValue().doubleValue()));
				} else if (DigitalStringUtil.equalsIgnoreCase(jurisdictionLevel, "COUNTY")) {
					taxServicelineItem.setCountyTax(DigitalCommonUtil.round(taxType.getCalculatedTax().getValue().doubleValue()));
				} else if (DigitalStringUtil.equalsIgnoreCase(flag, "F")
						&& (DigitalStringUtil.equalsIgnoreCase(jurisdictionLevel, "CITY")
								|| DigitalStringUtil.equalsIgnoreCase(jurisdictionLevel, "DISTRICT"))) {
					flag = "T";
					calculatedTax = taxType.getCalculatedTax().getValue().doubleValue();
					taxServicelineItem.setCityTax(DigitalCommonUtil.round(calculatedTax));
				} else if (DigitalStringUtil.equalsIgnoreCase(flag, "T")
						&& (DigitalStringUtil.equalsIgnoreCase(jurisdictionLevel, "CITY")
								|| DigitalStringUtil.equalsIgnoreCase(jurisdictionLevel, "DISTRICT"))) {
					calculatedTax += taxType.getCalculatedTax().getValue().doubleValue();
					taxServicelineItem.setCityTax(DigitalCommonUtil.round(calculatedTax));
				} else {
					taxServicelineItem.setCityTax(DigitalCommonUtil.round(calculatedTax));
				}
			}
			lineItems.add(taxServicelineItem);
		}
		taxServiceResponse.setLineItems(lineItems);
		return taxServiceResponse;
	}

	public Map<String, String> getPhysicalOriginAddress() {
		return getMapData(WebServicesConfig.DataTypes.PHYSICAL_ADDRESS);
	}


	public Map<String, String> getAdministrativeOriginAddress() {
		return getMapData(WebServicesConfig.DataTypes.ADMIN_ADDRESS);
	}
	
    public Map<String, Boolean> getServiceMethodsEnabled(){
		Map<String, Boolean> methods = new HashMap<>();
		for (ServiceMethod service: ServiceMethod.values()) {
			methods.put(service.getServiceMethodName(), isServiceMethodEnabled(service.getServiceMethodName()));
		}
		
		return methods;
    }

}
