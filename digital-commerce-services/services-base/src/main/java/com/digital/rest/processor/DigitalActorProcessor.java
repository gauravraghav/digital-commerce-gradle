package com.digital.rest.processor;

import atg.rest.RestException;
import atg.rest.processor.ActorProcessor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
@Getter
@Setter
public class DigitalActorProcessor extends ActorProcessor {
    boolean enabled = false;

    /**
     * Override this method to support PUT HTTP method request
     *
     * @param pRequest
     * @param pResponse
     * @throws IOException
     * @throws RestException
     */
    public void doRESTPut(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, RestException {
        if (isEnabled()) {
            if (isLoggingDebug()) {
                this.logDebug("Received PUT request for " + pRequest.getPathInfo() + ". Processing is done by this servlet for a PUT request.");
            }
            this.handleRequest(pRequest, pResponse);
        } else {
            super.doRESTPut(pRequest, pResponse);
        }
    }

    /**
     * Override this method to support DELETE HTTP method request
     *
     * @param pRequest
     * @param pResponse
     * @throws IOException
     * @throws RestException
     */
    public void doRESTDelete(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, RestException {
        if (isEnabled()) {
            if (isLoggingDebug()) {
                this.logDebug("Received DELETE request for " + pRequest.getPathInfo() + ". Processing is done by this servlet for a DELETE request.");
            }
            this.handleRequest(pRequest, pResponse);
        } else {
            super.doRESTDelete(pRequest, pResponse);
        }
    }
}