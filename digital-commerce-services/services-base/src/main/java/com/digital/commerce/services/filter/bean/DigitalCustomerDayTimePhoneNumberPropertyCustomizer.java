package com.digital.commerce.services.filter.bean;

import java.util.Map;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;

/**
 * A property customizer for customizing DaytimePhoneNumber from Home Phone Number.
 *
 */
public class DigitalCustomerDayTimePhoneNumberPropertyCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {


  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   *
   * @throws BeanFilterException
   */
  @Override
  public Object getPropertyValue(Object pTargetObject, String pPropertyName,
      Map<String,String> pAttributes) throws BeanFilterException {

    Object propValue = null;
    String daytimeTelephoneNumber = null;
    try {
      propValue = DynamicBeans.getPropertyValue(pTargetObject, pPropertyName);
      
      if (propValue != null) {
    	  daytimeTelephoneNumber = (String)((RepositoryItem)propValue).getPropertyValue("phoneNumber");
      }
    }
    catch (PropertyNotFoundException e) {
      throw new BeanFilterException(e);
    }
    	return daytimeTelephoneNumber;
  }
  
}
