
package com.digital.commerce.services.scheduledrequest.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CurrentComponentPropertyRequest implements Serializable {

	private static final long serialVersionUID = 5486598257657814290L;
	private String componentName;
	private String componentProperty;
}
