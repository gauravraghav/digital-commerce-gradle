package com.digital.commerce.services.common.validator;

import atg.commerce.order.CreditCard;
import atg.repository.RepositoryItem;
import atg.service.collections.validator.StartEndDateValidator;
import com.digital.commerce.common.util.DigitalStringUtil;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author jvasired
 */

public class DigitalStartEndDateValidator extends StartEndDateValidator {

  private static final String EXP_MONTH = "expirationMonth";
  private static final String EXP_YEAR = "expirationYear";
  private static final String TIME_FMT = "MM-dd-yyyy HH:mm:ss";
  private static final String HOURS_FMT = " 23:59:59";

  /**
   * @param pObject
   * @return true or false
   */
  public boolean validateObject(Object pObject) {
    if (!(pObject instanceof RepositoryItem)) {
      return false;
    }
    RepositoryItem item = (RepositoryItem) pObject;
    Date current = getCurrentDate().getTimeAsTimestamp();
    String expirationMonth = (String) item.getPropertyValue(EXP_MONTH);
    String expirationYear = (String) item.getPropertyValue(EXP_YEAR);
    if (DigitalStringUtil.isBlank(expirationMonth) || DigitalStringUtil.isBlank(expirationYear)) {
      return true;
    }
    DateFormat sd = new SimpleDateFormat(TIME_FMT);
    int maxDays = dateReturn(Integer.parseInt(expirationMonth), Integer.parseInt(expirationYear));
    if (isCCExpired(current, expirationMonth, expirationYear, sd, maxDays)) {
      return true;
    }
    return false;
  }

  /**
   * @param current
   * @param expirationMonth
   * @param expirationYear
   * @param sd
   * @param maxDays
   * @return true or false
   */
  private boolean isCCExpired(Date current, String expirationMonth, String expirationYear,
      DateFormat sd, int maxDays) {
    try {
      Date cardExpiry = sd
          .parse(expirationMonth + "-" + maxDays + "-" + expirationYear + HOURS_FMT);
      if ((cardExpiry != null) && (cardExpiry.before(current))) {
        return true;
      }
    } catch (ParseException e) {
      logError("ParseException ", e);
    }
    return false;
  }

  /**
   * Method used to find if a given credit card is expired
   *
   * @param creditCard
   * @return true or false
   */
  public boolean isCreditCardExpired(CreditCard creditCard) {

    Date current = getCurrentDate().getTimeAsTimestamp();
    String expirationMonth = creditCard.getExpirationMonth();
    String expirationYear = creditCard.getExpirationYear();
    DateFormat sd = new SimpleDateFormat(TIME_FMT);
    int maxDays = dateReturn(Integer.parseInt(expirationMonth), Integer.parseInt(expirationYear));
    if (isCCExpired(current, expirationMonth, expirationYear, sd, maxDays)) {
      return true;
    }
    return false;
  }

  /**
   * @param month
   * @param year
   * @return
   */
  public int dateReturn(int month, int year) {
    String str = " " + month + "-" + year;
    SimpleDateFormat sd = new SimpleDateFormat("MM-yyyy");
    try {
      Date d = sd.parse(str);
      Calendar c = Calendar.getInstance();
      c.setTime(d);
      return c.getActualMaximum(Calendar.DAY_OF_MONTH);
    } catch (ParseException e) {
      logError("ParseException: ", e);
    }
    return 0;
  }
}
