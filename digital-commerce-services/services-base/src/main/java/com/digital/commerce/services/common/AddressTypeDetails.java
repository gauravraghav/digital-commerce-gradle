package com.digital.commerce.services.common;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.builder.ToStringBuilder;

/** Class is responsible for handling address details. */
@Getter
@Setter
@ToString
public class AddressTypeDetails implements Serializable {
	// -----------------------
	private static final long	serialVersionUID	= 5711682353549086121L;
	private String				name;
	private String				contact;
	private String				company;
	private String				addr1;
	private String				addr2;
	private String				addr3;
	private String				city;
	private String				state;
	private String				province;
	private String				zip;
	private String				country;
	private String				phone;
	private String				phoneExt;
	private String				fax;
	private String				email;
}
