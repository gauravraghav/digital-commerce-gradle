/**
 * 
 */
package com.digital.commerce.services.scheduledrequest.domain;

import com.digital.commerce.common.domain.ResponseWrapper;

/**
 * @author mmallipu
 *
 */
public class AddScheduledJobResponse extends ResponseWrapper {

	public AddScheduledJobResponse() {
	}
}
