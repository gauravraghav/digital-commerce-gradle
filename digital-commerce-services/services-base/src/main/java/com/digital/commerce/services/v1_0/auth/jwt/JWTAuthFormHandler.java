package com.digital.commerce.services.v1_0.auth.jwt;


import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import javax.servlet.ServletException;

@Getter
@Setter
public class JWTAuthFormHandler extends GenericFormHandler implements JWTAuthConstants {

  private String userName;
  private String password;
  private String source;
  private JWTAuthManager authManager;
  private MessageLocator messageLocator;
  private String token;

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void handleGenerateJWTToken(DynamoHttpServletRequest request,
      DynamoHttpServletResponse response) throws ServletException, IOException {
    try {
      boolean error = false;
      if (DigitalStringUtil.isEmpty(source) ||
          DigitalStringUtil.isEmpty(userName) ||
          DigitalStringUtil.isEmpty(password)) {
        logError(
            "Input valid validation failed. Please check the credentials for the source: " + source
                + " & userName:" + userName);
        addFormException(new DropletException(messageLocator.getMessageString(ACCESS_DENIED),
            ACCESS_DENIED));
        error = true;
      }
      if (!error) {
        token = authManager.generateToken(userName, password, source);
        // Protect from downstream leaks of password
        request.removeParameter("password");
      }
    } catch (DigitalAppException enEx) {
      String errorCode = enEx.getErrorCode();
      logError("DigitalAppException while generating a JWT Token: ", enEx);
      if (DigitalStringUtil.isNotEmpty(errorCode)) {
        if (ACCESS_DENIED.equalsIgnoreCase(errorCode)) {
          addFormException(new DropletException(messageLocator.getMessageString(ACCESS_DENIED),
              ACCESS_DENIED));
        }
      } else {
        addFormException(new DropletException(enEx.getLocalizedMessage(),
            enEx.getErrorCode()));
      }
    } catch (Exception enEx) {
      logError("Exception while generating a JWT Token: ", enEx);
      addFormException(new DropletException(enEx.getLocalizedMessage(),
          GENERIC_ERROR));
     }
  }

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void handleValidateJWTToken(DynamoHttpServletRequest request,
      DynamoHttpServletResponse response) throws ServletException, IOException {
    try {
      boolean error = false;
      String authHeader = request.getHeader(AUTHORIZATION_HEADER);
      if (DigitalStringUtil.isBlank(authHeader)) {
        addFormException(new DropletException(messageLocator.getMessageString(AUTH_HEADER_MISSING),
            AUTH_HEADER_MISSING));
        error = true;
      }
      if (!error) {
        String[] headers = authHeader.split("\\s+");
        if (headers.length < 2 || !BEARER_CLAIM.equalsIgnoreCase(headers[0].trim())) {
          logError("Auth header should contain claim and token");
          addFormException(
              new DropletException(messageLocator.getMessageString(AUTH_HEADER_FORMAT_ERROR),
                  AUTH_HEADER_FORMAT_ERROR));
          error = true;
        }
        if (!error) {
          authManager.validateToken(headers[1]);
        }
      }
    } catch (DigitalAppException enEx) {
      addFormException(new DropletException(enEx.getLocalizedMessage(),
          enEx.getErrorCode()));
    } catch (Exception enEx) {
      addFormException(new DropletException(enEx.getLocalizedMessage(),
          GENERIC_ERROR));
      logError("Generic Exception while validating a JWT Token: ", enEx);
    }
  }
}
