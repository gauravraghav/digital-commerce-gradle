package com.digital.commerce.services.responses;

import com.digital.commerce.services.common.DigitalContactInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShippingAddressServiceResponse {
	private String decision;
	private DigitalContactInfo suggestedAddress;
	private DigitalContactInfo address;
}
