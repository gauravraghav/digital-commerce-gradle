package com.digital.commerce.services.common;

import java.util.Arrays;
import java.util.Dictionary;
import java.util.Iterator;

import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.google.common.collect.Iterables;

import atg.repository.RepositoryItem;
import lombok.Getter;

/** 
 * The address types supported by the system.
 *
 */
@SuppressWarnings({"rawtypes"})
@Getter
public enum AddressType {
	USA("USA"),
	/** All non-USA, non-Military addresses */
	INTERNATIONAL("INTL"), MILITARY("MIL");

	private final String	value;

	private AddressType( String value ) {
		this.value = value;
	}

	/** Looks up the address type for the given value.
	 * 
	 * @param value
	 * @return */
	public static AddressType valueFor( final String value ) {
		final String trimmedValue = DigitalStringUtil.trimToEmpty( value );
		final Iterator<AddressType> filtered = Iterables.filter( Arrays.asList( values() ), new DigitalPredicate<AddressType>() {

			@Override
			public boolean apply( AddressType input ) {
				return DigitalStringUtil.equalsIgnoreCase( DigitalStringUtil.trimToEmpty( input.getValue() ), trimmedValue );
			}

		} ).iterator();

		return filtered.hasNext() ? filtered.next() : AddressType.USA;
	}

	/** Looks up the address type for the given value.
	 * 
	 * @param value
	 * @return */
	public static AddressType valueFor( final DigitalContactInfo address ) {
		final String state = DigitalStringUtil.isBlank( address.getState() ) ? address.getRegion() : address.getState();
		return valueFor( address.getAddressType(), address.getCountry(), state );
	}

	/** Looks up the address type for the given value.
	 * 
	 * @param value
	 * @return */
/*	public static AddressType valueFor( CustomerAccount customer ) {
		return valueFor( customer.getAddressType(), customer.getCountry(), customer.getState() );
	}*/

	/** Looks up the address type for the given value.
	 * 
	 * @param value
	 * @return */
	public static AddressType valueFor( Dictionary address ) {
		final String state = DigitalStringUtil.isBlank( (String)address.get( "state" ) ) ? (String)address.get( "region" ) : (String)address.get( "state" );
		return valueFor( (String)address.get( "addressType" ), (String)address.get( "country" ), state );
	}

	/** Looks up the address type for the given value.
	 * 
	 * @param value
	 * @return */
	public static com.digital.commerce.services.common.AddressType valueFor( RepositoryItem address ) {
		Object stateObj = address.getPropertyValue( "state" );
		final String state = DigitalStringUtil.isBlank( (String)stateObj ) ? (String)address.getPropertyValue( "region" ) : (String)stateObj;
		return valueFor( (String)address.getPropertyValue( "addressType" ), (String)address.getPropertyValue( "country" ), state );
	}

	/** Looks up the address type for the given value.
	 * 
	 * @param value
	 * @return */
	public static com.digital.commerce.services.common.AddressType valueFor( DigitalRepositoryContactInfo contactInfo ) {
		final String state = DigitalStringUtil.isBlank( contactInfo.getState() ) ? contactInfo.getRegion() : contactInfo.getState();
		return valueFor( contactInfo.getAddressType(), contactInfo.getCountry(), state );
	}


	/** Looks up the address type for the given value.
	 * 
	 * @param value
	 * @return */
	public static AddressType valueFor( String addressType, String country, String state ) {
		AddressType defaultAddressType = AddressType.valueFor( addressType );
		AddressType retVal = null;
		final CountryCode countryCode = CountryCode.valueFor( country );
		final StateCode stateCode = StateCode.valueFor( state );
		final MilitaryRegionCode militaryRegionCode = MilitaryRegionCode.valueFor( state );

		if( AddressType.MILITARY.equals( defaultAddressType ) || AddressType.INTERNATIONAL.equals( defaultAddressType ) ) {
			retVal = defaultAddressType;
		} else if( stateCode != null ) {
			if( CountryCode.USA.equals( stateCode.getCountryCode() ) || CountryCode.US.equals( stateCode.getCountryCode() ) ) {
				retVal = AddressType.USA;
			} else {
				retVal = AddressType.INTERNATIONAL;
			}
		} else if( CountryCode.USA.equals( countryCode )  || CountryCode.US.equals( countryCode ) ) {
			if( militaryRegionCode != null ) {
				retVal = AddressType.MILITARY;
			} else {
				retVal = AddressType.USA;
			}
		} else {
			retVal = AddressType.INTERNATIONAL;
		}

		return retVal;
	}
}
