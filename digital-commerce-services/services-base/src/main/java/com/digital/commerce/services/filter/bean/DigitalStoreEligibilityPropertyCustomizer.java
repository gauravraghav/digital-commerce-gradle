package com.digital.commerce.services.filter.bean;

import java.util.Map;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;

/**
 * A Property customizer used to return a flag (true/false) if a store is
 * BOPIS/BOSTS enabled.
 *
 * @author Shiva
 */
public class DigitalStoreEligibilityPropertyCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {

	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalStoreEligibilityPropertyCustomizer.class);

	public DigitalStoreEligibilityPropertyCustomizer() {
		super(DigitalStoreEligibilityPropertyCustomizer.class.getName());
	}

	/**
	 * @param storeValue
	 * @return true or false.
	 */
	public Boolean getStoreEligibilityFlag(String storeValue) {

		if (null!=storeValue && storeValue.equalsIgnoreCase("1") || storeValue.equalsIgnoreCase("Y")) {
			return true;
		}else{
		return false;
		}
	}

	// ---------------------------------------------------------------------------
	// METHODS
	// ---------------------------------------------------------------------------

	/**
	 *
	 * @param pTargetObject
	 *            The object which the specified property is associated with.
	 * @param pPropertyName
	 *            The name of the property to return.
	 * @param pAttributes
	 *            The key/value pair attributes defined in the
	 *            beanFilteringConfiguration.xml file for this property.
	 *
	 * @return true or false based on the string value passed to be formated.
	 *
	 * @throws BeanFilterException
	 */
	@Override
	public Object getPropertyValue(Object pTargetObject, String pPropertyName, Map<String, String> pAttributes)
			throws BeanFilterException {

		// Get the store value to format
		Object propValue = null;
		String propName = getPropertyName(pPropertyName);
		try {
			propValue = DynamicBeans.getPropertyValue(pTargetObject, propName);
		} catch (PropertyNotFoundException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Exception while retreving property value ::" + e);
			}
			throw new BeanFilterException(e);
		}

		// Return null if we don't get a store value to format.
		if (propValue == null) {
			vlogDebug("Property {0} was not a valid storevalue type: {1}", pPropertyName, propValue);
			return null;
		}

		// Get the formated eligibility flag by passing the store value.
		return this.getStoreEligibilityFlag((String) propValue);
	}
	
	String getPropertyName(String propertyName){
		String actualPropertyName = propertyName;
		if("shipToStore".equalsIgnoreCase(propertyName)){
			actualPropertyName = "shipToStore";
		}else if("pickupInStore".equalsIgnoreCase(propertyName)){
			actualPropertyName = "pickupInStore";
		}
		
		return actualPropertyName;
	}
}
