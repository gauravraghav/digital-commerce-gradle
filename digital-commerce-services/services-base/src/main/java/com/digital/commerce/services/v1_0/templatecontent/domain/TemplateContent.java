package com.digital.commerce.services.v1_0.templatecontent.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

@Getter
@Setter
public class TemplateContent {
	
	/** Common Template Attributes - Begin */
	private String templateId;

	private String rendererJsp;
	
	private String type;
	
	private String keywords;

	private String name;
	
	private HashMap<String,String> valuePair;
	/** Common Template Attributes - End*/
	
	/** Slot Attributes - Begin*/
	private String slotContent;

	private int count;
	/** Slot Attributes - End*/
	
	/** JSPFrag Attributes - Begin*/
	private String description;

	/** JSPFrag Attributes - End*/
	
	/** AdvertisementBanner Attributes - Begin*/
	private String image;

	private String link;
	/** AdvertisementBanner Attributes - End*/
}
