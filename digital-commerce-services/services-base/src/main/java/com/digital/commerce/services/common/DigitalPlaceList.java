/** Changes by corbus team */
package com.digital.commerce.services.common;

import java.util.Map;

import atg.commerce.util.PlaceList;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class DigitalPlaceList extends PlaceList {
	private String	country;
	private Map		states;
}
