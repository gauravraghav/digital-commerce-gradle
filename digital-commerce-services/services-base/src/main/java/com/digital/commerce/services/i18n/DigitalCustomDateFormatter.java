package com.digital.commerce.services.i18n;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.servlet.ServletUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * A component used to format dates with custom locale dependent patterns.
 *
 * @author
 */
@Getter
@Setter
public class DigitalCustomDateFormatter extends ApplicationLoggingImpl {

	// --------------------------------------------------------------------------
	// CONSTANTS
	// --------------------------------------------------------------------------

	/** The default date format to use. */
	private static final int DEFAULT_DATEFORMAT = DateFormat.SHORT;

	// ---------------------------------------------------------------------------
	// PROPERTIES
	// ---------------------------------------------------------------------------

	// -----------------------------------
	// property: customDateFormats
	// -----------------------------------
	private Map<String, String> customDateFormats = null;

	public DigitalCustomDateFormatter() {
		super(DigitalCustomDateFormatter.class.getName());
	}

	// ---------------------------------------------------------------------------
	// METHODS
	// ---------------------------------------------------------------------------

	/**
	 * Returns the DateFormat.SHORT pattern for a particular locale.
	 *
	 * @param pLocale
	 *            A Locale object that will be used to determine the date
	 *            pattern.
	 *
	 * @return A DateFormat.SHORT pattern for a particular locale.
	 */
	public String getDefaultDatePatternForLocale(Locale pLocale) {
		DateFormat df = DateFormat.getDateInstance(DEFAULT_DATEFORMAT, pLocale);

		if (df instanceof SimpleDateFormat) {
			return ((SimpleDateFormat) df).toPattern();
		}

		return null;
	}

	/**
	 * Returns a custom date format pattern for a particular locale. If there is
	 * no custom pattern then the DateFormat.SHORT pattern will be returned.
	 *
	 * @param pLocale
	 *            A Locale object that will be used to determine the date
	 *            pattern.
	 *
	 * @return A custom pattern for this locale, otherwise the default.
	 */
	public String getDatePatternForLocale(Locale pLocale) {
		String pattern = null;

		// Look for custom date formats for this locale.
		if (getCustomDateFormats() != null && !getCustomDateFormats().isEmpty()) {
			String localeString = pLocale.toString();

			pattern = getCustomDateFormats().get(localeString);

			if (pattern == null) {
				int breakAt = 0;

				while ((breakAt = localeString.lastIndexOf("_")) != -1 && pattern == null) {
					localeString = localeString.substring(0, breakAt);
					pattern = getCustomDateFormats().get(localeString);
				}
			}
		}

		return pattern == null ? getDefaultDatePatternForLocale(pLocale) : pattern;
	}

	/**
	 * Returns a localized date string using a custom pattern for this locale,
	 * otherwise the default pattern for this locale.
	 *
	 * @param pDate
	 *            The Date object to be formatted.
	 * @param pLocale
	 *            A Locale object that will be used to determine the date
	 *            pattern.
	 *
	 * @return A formatted String from the date.
	 */
	public String getLocalizedDateString(Date pDate, Locale pLocale) {
		String pattern = getDatePatternForLocale(pLocale);
		DateFormat df = new SimpleDateFormat(pattern, pLocale);

		return df.format(pDate);
	}

	/**
	 * Parses a Date from a localized date string.
	 *
	 * @param pDate
	 *            A string representing a date.
	 * @param pLocale
	 *            A Locale object that will be used to determine the date
	 *            pattern.
	 * @return A parsed Date object.
	 *
	 * @throws ParseException
	 */
	public Date getDateFromLocalizedString(String pDate, Locale pLocale) throws ParseException {
		String pattern = getDatePatternForLocale(pLocale);
		DateFormat df = new SimpleDateFormat(pattern, pLocale);

		return df.parse(pDate);
	}

	/**
	 * Parses a Date from a localized date string.
	 *
	 * @param pDate
	 *            A string representing a date.
	 * @return A parsed Date object.
	 *
	 */
	public String getLocalizedDateString(Date pDate) {
		String dateStr = "";
		try {
			if(null != pDate) {
				Locale pLocale = ServletUtil.getUserLocale();
				String pattern = getDatePatternForLocale(pLocale);
				DateFormat df = new SimpleDateFormat(pattern, pLocale);
				dateStr = df.format(pDate);
			}
		}catch(Exception ex){
			logError(":: Exception parsing Date :: ", ex);
		}
		return dateStr;
	}
}
