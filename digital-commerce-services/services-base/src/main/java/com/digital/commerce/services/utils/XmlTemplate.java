package com.digital.commerce.services.utils;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.digital.commerce.common.logger.DigitalLogger;

/** Template for working with XML documents
 * 
 */
public class XmlTemplate {

	private static final DigitalLogger	logger	= DigitalLogger.getLogger( XmlTemplate.class );

	private Document			document;

	/** Create an xml template from the given file
	 * 
	 * @param f */
	public XmlTemplate( File f ) {

	}

	/** Create an xml template from the given xml stirng
	 * 
	 * @param xml */
	public XmlTemplate( String xml ) {
		assertNotNull( xml, "Unable to create an XML template with null xml" );
		this.document = getDocument( new InputSource( new StringReader( xml ) ) );
	}

	/** Create an xml template from the given DOM
	 * 
	 * @param d */
	public XmlTemplate( Document d ) {
		assertNotNull( d, "Unable to create an XML template with a null document" );
		this.document = d;
	}

	public void query( String xpathQuery, IXpathResultCallback callback ) {
		NodeList result = compileAndExecuteExpression( xpathQuery );
		for( int i = 0; i < result.getLength(); i++ ) {
			callback.doInElement( document, result.item( i ) );
		}
	}

	/** Converts the document to a xml String
	 * 
	 * @return */
	public String getDocumentAsString() {
		StringWriter transformedOrderXML = new StringWriter();
		transformDocument( new DOMSource( document ), new StreamResult( transformedOrderXML ) );
		return transformedOrderXML.toString();
	}

	/** Compiles the expression and build a nodelist
	 * 
	 * @param xpathQuery - The expression
	 * @return The result set */
	private NodeList compileAndExecuteExpression( String xpathQuery ) {
		try {
			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expression = xpath.compile( xpathQuery );
			return (NodeList)expression.evaluate( document, XPathConstants.NODESET );
		} catch( Exception e ) {
			logger.error( e, e );
			throw new RuntimeException( "Unable to compile expression: " + xpathQuery, e );
		}
	}

	/** Transforms the XML document
	 * 
	 * @param s - The source of the document
	 * @param r - The result */
	private void transformDocument( Source s, Result r ) {
		if( document != null ) {
			try {
				TransformerFactory factory = TransformerFactory.newInstance();
				Transformer t = factory.newTransformer();
				t.transform( s, r );
			} catch( Exception e ) {
				logger.error( e, e );
				throw new RuntimeException( "Unable to transform xml document", e );
			}
		}
	}

	/** @param inputSource
	 * @return */
	private Document getDocument( InputSource inputSource ) {
		try {
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			return builder.parse( inputSource );
		} catch( Exception e ) {
			logger.error( e, e );
			throw new RuntimeException( "Unable to parse xml document", e );
		}
	}

	/** Helper for asserting null values
	 * 
	 * @param o
	 * @param message */
	private void assertNotNull( Object o, String message ) {
		if( o == null ) { throw new IllegalStateException( message ); }
	}
}
