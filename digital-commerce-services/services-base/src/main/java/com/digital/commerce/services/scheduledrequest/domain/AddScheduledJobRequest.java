
package com.digital.commerce.services.scheduledrequest.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
@Getter
@Setter
public class AddScheduledJobRequest implements Serializable {

	private static final long	serialVersionUID	= 5486598257657814290L;
	private String				componentName;
	private String				componentProperty;
	private String				componentMethod;
	private String				stringifiedSchedule;
	private Date				schedule;
	private Set<String>			serverNames			= new HashSet<>();
	private String				newValue;
	private Timestamp			creationDate;
	private String              login;
	private String              password;
}
