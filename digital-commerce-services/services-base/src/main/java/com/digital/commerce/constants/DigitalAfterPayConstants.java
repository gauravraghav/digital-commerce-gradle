package com.digital.commerce.constants;

public class DigitalAfterPayConstants {

    public static final String	AFTERPAY_PAYMENT_TYPE	= "afterPayPayment";
    public static final String	MISSING_AFTERPAY_TOKEN  ="missingAfterPayToken";
    public static final String	AFTER_PAY_PAYMENT_TOKEN_PROPERTY  ="token";
}
