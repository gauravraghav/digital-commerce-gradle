package com.digital.commerce.services.common;

import java.util.concurrent.TimeUnit;

import com.digital.commerce.common.logger.LoggingUtil;

import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

/**
 * Calculates the REST service processing time.
 * 
 * @author SK402315
 * 
 */
public class DigitalRESTResponse extends GenericService {

	

	public void setResponseHeaders() {

		if (isLoggingDebug()) {
			logDebug( "In DigitalRESTResponse's setResponseHeaders method" );
		}

		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();

		Object dateValue = request.getAttribute("dateValue");
		long reqInTime = (dateValue != null ? (Long) dateValue : 0);

		if (reqInTime == 0) {
			if (isLoggingInfo()) {
				logInfo("Service " + request.getRequestURI()
						+ " requested time cannot be traced");
			}
		} else {
			if (isLoggingDebug()) {
				final String requestInfo = LoggingUtil.getRequestInfo( request );
				StringBuilder logEntry = new StringBuilder();

				logEntry.append( "[" )
						.append( requestInfo )
						.append( "EXECUTION_TIME: " )
						.append(TimeUnit.NANOSECONDS.toMillis( System.nanoTime() - reqInTime )).append( " ms " );
		
				logDebug( logEntry.toString() );
			}
		}
	}


}
