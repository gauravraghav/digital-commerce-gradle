package com.digital.commerce.constants;

import lombok.Getter;

public final class OrderConstants {
	@Getter
	public enum OrderPropertyManager{
		LOCALE("locale"),
		REWARD_CERTIFICATE("rewardCertificate"),
		REWARD_CERTIFICATE_NUMBER("certificateNumber"),
		REWARD_MARKDOWN_CODE("markdownCode"),
		REWARD_CERTIFICATE_STATUS("state"),
		REWARD_CERTIFICATE_AMT_USED("amount"),
		REWARD_CERTIFICATE_AMT_AUTH("amountAuthorized"),
		REWARD_TYPE("type"),
		ORDER_REWARDS("orderRewards"),
		DSW_ORDER_HISTORY("dsworderhistory"),
		DSW_COMMERCE_ITEM("dswCommerceItem");
		
		private final String value;
	
		private OrderPropertyManager( String value ) {
			this.value = value;
		}

	}
}
