package com.digital.commerce.services.v1_0.templatecontent.domain;

import java.io.Serializable;
import java.util.List;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TemplateContentResponse extends ResponseWrapper implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<TemplateContent> contentList;
}
