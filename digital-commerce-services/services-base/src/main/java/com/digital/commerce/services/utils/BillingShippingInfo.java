package com.digital.commerce.services.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.common.AddressType;

import atg.commerce.util.PlaceList;
import atg.core.i18n.PlaceList.Place;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillingShippingInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PlaceList usBillableStates;
	private PlaceList canShippableStates;
	private PlaceList billableCountries;
	private PlaceList apofpoRegions;
	private Hashtable<String, Object> error;
	private Hashtable<String, Object> billingShippingAddresses = new Hashtable<>();
	private PlaceList apofpoCities;

	public void getSupportedBillingShippingAddresses() {
		final String PERF_NAME = "getSupportedBillingShippingAddreses";
		final String PERFORM_MONITOR_NAME = "BillingShippingInfo";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
					PERFORM_MONITOR_NAME, PERF_NAME);

			HashMap<String, Object> shippingAddressesList = new HashMap<>();
			HashMap<String, Object> billingAddressesList = new HashMap<>();

			// Read US states and add to billable and shippable addresses list
			Place[] statesPlaces = usBillableStates.getPlaces();
			TreeMap<String, String> statesList = new TreeMap<>();
			for (Place p : statesPlaces) {
				statesList.put(p.getCode(), p.getDisplayName());
			}
			shippingAddressesList.put(AddressType.USA.getValue(), statesList);
			billingAddressesList.put(AddressType.USA.getValue(), statesList);

			// Read APO/FPO regions and cities and add to billable and shippable
			// addresses list

			HashMap<String, Object> apoFpoList = new HashMap<>();
			Place[] apoFpoRegionPlaces = apofpoRegions.getPlaces();
			TreeMap<String, String> apofpoRegionList = new TreeMap<>();
			for (Place p : apoFpoRegionPlaces) {
				apofpoRegionList.put(p.getCode(), p.getDisplayName());
			}
			apoFpoList.put("Regions", apofpoRegionList);

			Place[] apoFpoCitiesPlaces = apofpoCities.getPlaces();
			TreeMap<String, String> apofpoCityList = new TreeMap<>();
			for (Place p : apoFpoCitiesPlaces) {
				apofpoCityList.put(p.getCode(), p.getDisplayName());
			}
			apoFpoList.put("Cities", apofpoCityList);

			shippingAddressesList.put("APO/FPO", apoFpoList);
			billingAddressesList.put("APO/FPO", apoFpoList);

			Place[] nonUSCountries = billableCountries.getPlaces();
			TreeMap<String, String> billableCountriesList = new TreeMap<>();
			for (Place p : nonUSCountries) {
				billableCountriesList.put(p.getCode(), p.getDisplayName());
			}
			billingAddressesList.put("INTL", billableCountriesList);

			billingShippingAddresses.put("SupportedShippingAddresses",
					shippingAddressesList);
			billingShippingAddresses.put("SupportedBillingAddresses",
					billingAddressesList);

		} catch (Exception ex) {
			this.getJSONError(ex.getMessage());
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
					PERFORM_MONITOR_NAME, PERF_NAME);
		}
	}

	private void getJSONError(String errorMesage) {
		error = new Hashtable<>();
		Hashtable<String, String> genError = new Hashtable<>();
		if (DigitalStringUtil.isNotBlank(errorMesage)) {
			genError.put("localizedMessage", errorMesage);
		} else {
			genError.put("localizedMessage",
					"Generic error while writing a client message to the log");
		}
		genError.put("errorCode", "BILLING_SHIPPING_LOOKUP_ERROR");
		ArrayList<Hashtable<String, String>> genErrArray = new ArrayList<>();
		genErrArray.add(genError);
		error.put("genericExceptions", genErrArray);
		error.put("formError", false);
	}

}
