package com.digital.commerce.services.region;

import com.digital.commerce.common.util.DigitalNumberUtil;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.servlet.ServletException;

import atg.dtm.UserTransactionDemarcation;
import com.digital.commerce.common.region.RegionManager;
import com.digital.commerce.common.util.DigitalStringUtil;

import atg.rest.servlet.RestPipelineServlet;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.digital.commerce.common.transaction.TransactionUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegionContextPipelineServlet extends RestPipelineServlet {
	private static final String CLASSNAME = RegionContextPipelineServlet.class.getName();
	private RegionContextManager contextManager;
	private RegionManager regionManager;
	private boolean enabled = true;
	private String testRegion;
	private String regionProfileComponentName;
	private String akamaiHeaderKey;
	private String akamaiStateKey;
	private String akamaiLatitudeKey;
	private String akamaiLongitudeKey;

	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws IOException,
			ServletException {
		final String METHOD_NAME = "service";
		if ((!isEnabled())) {
			passRequest(pRequest, pResponse);
			return;
		}

		UserTransactionDemarcation td = null;
		try {
			td = TransactionUtils.startNewTransaction(CLASSNAME,METHOD_NAME);
			//initial regionProfile
			RegionProfile regionProfile = (RegionProfile) pRequest.resolveName(this.regionProfileComponentName);

			if(!(regionProfile.getLatitude() > 0.00) && !(regionProfile.getLongitude() > 0.00)) {
				//by coordinate
				if (DigitalStringUtil.isNotEmpty(pRequest.getParameter("latitude"))
						&& DigitalStringUtil.isNotEmpty(pRequest.getParameter("longitude"))
						&& DigitalNumberUtil.isNumber(pRequest.getParameter("latitude"))
						&& DigitalNumberUtil.isNumber(pRequest.getParameter("longitude"))) {
						double latitude = Double.parseDouble(pRequest.getParameter("latitude"));
						double longitude = Double.parseDouble(pRequest.getParameter("longitude"));
						//state = this.regionManager.getStateByCoordinate(latitude, longitude);
						regionProfile.setLatitude(latitude);
						regionProfile.setLongitude(longitude);
				}

					//even latitude 0.00 does exist, but assuming no user visits from that location
					if (regionProfile.getLatitude() == 0.00) {
						String latStr = this.extractAkamaiHeaderAttribute(pRequest, this.akamaiLatitudeKey);
						String longStr = this.extractAkamaiHeaderAttribute(pRequest, this.akamaiLongitudeKey);
						regionProfile.setLatitude(latStr == null ? 0.00 : Double.parseDouble(latStr));
						regionProfile.setLongitude(longStr == null ? 0.00 : Double.parseDouble(longStr));
					}
			}

		}catch (Exception ex){
			TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
			this.logError("Exception occured while handling ", ex);
		}finally{
			TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
		}
		passRequest(pRequest, pResponse);
		
	}
	
	private String extractAkamaiHeaderAttribute(DynamoHttpServletRequest pRequest, String attributeName) {
		String value = null;
		if(pRequest.getHeader(this.getAkamaiHeaderKey()) != null){
			String akamaiStr = pRequest.getHeader(this.getAkamaiHeaderKey());
			StringTokenizer st = new StringTokenizer(akamaiStr, ",");
			while (st.hasMoreTokens()) {
				String token = st.nextToken().trim();
				if(token.indexOf(attributeName) > -1){
					value = token.substring(attributeName.length()).trim();
				}
			}
		}
		return value;
	}
	
	
}
