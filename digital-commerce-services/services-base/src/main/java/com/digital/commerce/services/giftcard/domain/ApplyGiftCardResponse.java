package com.digital.commerce.services.giftcard.domain;

import atg.commerce.order.PaymentGroupImpl;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplyGiftCardResponse extends ResponseWrapper {
	private String gcStatus;
	private PaymentGroupImpl giftCardPaymentGroup;
	private String	giftCardNumber;
	private String	pinNumber;
	private Double	amountRemaining;
}
