/**
 * 
 */
package com.digital.commerce.services.common;

import java.io.Serializable;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Getter
@Setter
@ToString
public abstract class ServicesStatus implements Serializable {

	private static final long	serialVersionUID	= 3648176100755514828L;
	private String				statusCode;

	private String				statusMessage;

	/** Leaving equals and hashCode instead of converting to lombok.  Avoiding unnecessary risk. **/
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ServicesStatus that = (ServicesStatus) o;
		return Objects.equals(statusCode, that.statusCode) &&
				Objects.equals(statusMessage, that.statusMessage);
	}

	@Override

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode( this );
	}

}
