package com.digital.commerce.services.filter.bean;

import java.util.Map;

import atg.beans.DynamicBeans;
import atg.commerce.claimable.ClaimableTools;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalCouponCodeCustomizer extends ApplicationLoggingImpl
implements PropertyCustomizer {
	
	private ClaimableTools claimableTools;
	
	private String defaultProperty = "coupon";
	
	@Override
	public Object getPropertyValue(Object pTargetObject, String pPropertyName, Map<String, String> pAttributes) throws BeanFilterException {
		// TODO Auto-generated method stub
		
		try{
			Object propValue = DynamicBeans.getPropertyValue(pTargetObject, defaultProperty);
			
			if(propValue != null)
				return propValue;
			
			RepositoryItem promotion = (RepositoryItem)DynamicBeans.getPropertyValue(pTargetObject, pPropertyName);
			RepositoryItem[] coupons = this.claimableTools.getCouponsForPromotion(promotion.getRepositoryId());
			if(coupons == null || coupons.length == 0){
				return null;
			} else {
				return coupons[0];
			}
			
		}catch(Exception e){
			
		}
		
		return null;
		
	}
	
	

}
