package com.digital.commerce.services.order;

import atg.commerce.order.RepositoryContactInfo;

public class DigitalRepositoryContactInfo extends RepositoryContactInfo {
	/**
     *
     */
	private static final long	serialVersionUID	= 7587267366915880330L;

	public void setRank( String rank ) {
		mRepositoryItem.setPropertyValue( "rank", rank );
	}

	public String getRank() {
		return (String)mRepositoryItem.getPropertyValue( "rank" );
	}

	public void setAddressType( String addressType ) {
		mRepositoryItem.setPropertyValue( "addressType", addressType );
	}

	public String getAddressType() {
		return (String)mRepositoryItem.getPropertyValue( "addressType" );
	}

	public void setRegion( String region ) {
		mRepositoryItem.setPropertyValue( "region", region );
	}

	public String getRegion() {
		return (String)mRepositoryItem.getPropertyValue( "region" );
	}

	public Boolean getAddressVerification() {
		return (Boolean)mRepositoryItem.getPropertyValue( "addressVerification" );
	}

	public void setAddressVerification( Boolean av ) {
		mRepositoryItem.setPropertyValue( "addressVerification", av );
	}

	public Boolean getIsPoBox() {
		return (Boolean)mRepositoryItem.getPropertyValue( "isPoBox" );
	}

	public void setIsPoBox( Boolean av ) {
		mRepositoryItem.setPropertyValue( "isPoBox", av );
	}
	
	public void setPobox( String pobox ) {
		// mRepositoryItem.setPropertyValue("address1", pobox);
	}

	public String getPobox() {
		/*if (getIsPoBox().booleanValue())
		 * return getAddress1();
		 * else */
		return "";
	}
	
	public void setShipType( String shipType ) {
		mRepositoryItem.setPropertyValue( "shipType", shipType );
	}

	public String getShipType() {
		return (String)mRepositoryItem.getPropertyValue( "shipType" );
	}	
	
	public void setStoreId( String storeId ) {
		mRepositoryItem.setPropertyValue( "storeId", storeId );
	}

	public String getStoreId() {
		return (String)mRepositoryItem.getPropertyValue( "storeId" );
	}		
}
