package com.digital.commerce.services.servlet;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import atg.rest.RestException;
import atg.rest.servlet.HeadRestServlet;
import atg.rest.util.ModelURI;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author 
 *
 */
@Getter
@Setter
public class DigitalHeadRestServlet extends HeadRestServlet {
	
	private Map<String,String> chainIdToURIMap;
	/**
	 * Override super class method
	 */
	public void serviceRESTRequest(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws IOException,
			ServletException, RestException {
		super.serviceRESTRequest(pRequest, pResponse);
		ModelURI parsedUri = (ModelURI) pRequest.getAttribute("_PARSED_URI");
		if(null != parsedUri) {
			String url = parsedUri.getURI();
			if(isLoggingDebug()){
				logDebug("URL from DigitalHeadRestServlet serviceRESTRequest is ..." + url);
			}
			for ( Map.Entry<String, String> entry : chainIdToURIMap.entrySet() ) {
			    String chainId = entry.getKey();
			    String uri = entry.getValue();
			    if (url.indexOf(chainId) > 0)
				{
			    	parsedUri.setURI(uri);
			    	parsedUri.setChainId(chainId);
					pRequest.setAttribute("_PARSED_URI", parsedUri);
				}
			}
		} else {
			if(isLoggingDebug()){
				logDebug("parsedUri is null");
			}
		}
		
	}
}
