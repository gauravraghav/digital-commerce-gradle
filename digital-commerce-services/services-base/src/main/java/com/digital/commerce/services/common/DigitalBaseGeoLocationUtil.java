package com.digital.commerce.services.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;

import atg.adapter.gsa.query.Builder;
import atg.commerce.inventory.InventoryException;
import atg.commerce.inventory.InventoryInfo;
import atg.commerce.inventory.InventoryManager;
import atg.commerce.inventory.LocationInventoryManager;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalBaseGeoLocationUtil extends GenericService {
	
	private Repository repository;
	private Repository regionRepository;
	//private String storeLocatorQuery;
	private String bostsEnabledQuery;
	private String bospisEnabledQuery;
	//private String locationItemType;
	//private InventoryManager inventoryManager;
	
	//private static final String METHOD_NAME_FIND_BY_SQL = "findBySQL";
	//private static final String METHOD_NAME_FIND_BY_RQL = "findByRQL";
	
	
	public boolean isBOPISEnabled(){
		final String METHOD_NAME = "isBOPISEnabled";
		boolean bopisEnabled = true;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			RepositoryView view = getViewChecked("location", true);
			Builder builder = (Builder) view.getQueryBuilder();
			if(isLoggingDebug()){
				logDebug("DigitalGeoLocationManager isBOPISEnabled query :: " + getBospisEnabledQuery());
			}
			int bopisStores = view.executeCountQuery(builder.createSqlPassthroughQuery(getBospisEnabledQuery(),null));
			if(bopisStores < 1){
				bopisEnabled = false;
			}
		} catch (Exception ex) {
			if(isLoggingError()){
				logError("DigitalGeoLocationManager isBOPISEnabled",ex);
			}
		}finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
		}		
		return bopisEnabled;
	}
	
	public boolean isBOSTSEnabled(){
		final String METHOD_NAME = "isBOPISEnabled";
		boolean bostsEnabled = true;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			RepositoryView view = getViewChecked("location", true);
			Builder builder = (Builder) view.getQueryBuilder();
			if(isLoggingDebug()){
				logDebug("DigitalGeoLocationManager isBOPISEnabled query :: " + getBostsEnabledQuery());
			}
			int bostsStores = view.executeCountQuery(builder.createSqlPassthroughQuery(getBostsEnabledQuery(),null));
			if(bostsStores < 1) {
				bostsEnabled = false;
			}
		} catch (Exception ex) {
			if(isLoggingError()){
				logError("DigitalGeoLocationManager isBOSTSEnabled",ex);
			}
		}finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
		}		
		return bostsEnabled;
	}
	
	/**
	 * 
	 * @param itemDescriptor
	 * @param isLocationRepository
	 * @return
	 */
	protected RepositoryView getViewChecked(String itemDescriptor,boolean isLocationRepository) {
		RepositoryView view = null;
		try {
			if (isLocationRepository) {
				view = repository.getView(itemDescriptor);
			} else {
				view = regionRepository.getView(itemDescriptor);
			}

		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("DSWGeoLocationManager getViewChecked",e);
			}
		}
		if (view == null) {
			return null;
		}
		return view;
	}

}
