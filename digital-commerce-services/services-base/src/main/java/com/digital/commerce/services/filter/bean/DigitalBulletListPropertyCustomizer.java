package com.digital.commerce.services.filter.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;

import com.digital.commerce.services.utils.DigitalServiceConstants;
import lombok.Getter;
import lombok.Setter;

/**
 * A Property customizer used to return formated bullets list.
 *
 * @author Shiva
 */
@Getter
@Setter
public class DigitalBulletListPropertyCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {
       
    private DigitalServiceConstants		digitalConstants;
    
    private static final String EMPTY_SPACE="";
    
    private static ApplicationLogging logger	= ClassLoggingFactory.getFactory().getLoggerForClass( DigitalBulletListPropertyCustomizer.class );


	/**
   * @param bulletList
   * split the bullet list and  return the split list as a collection.
   * 
   * @return arrList collection of bullet list
   */
  public List<String> extractBullets( String bulletList ) {
	 String [] list = bulletList.split(getDigitalConstants().getListItemEndTag());
	 List<String> arrList = new ArrayList<>();
	 for(String lists: list){
		 String bullList = lists.replace(getDigitalConstants().getListItemTag(),EMPTY_SPACE);
		 //get  rid of  the <ul> tags if any in the string
		 bullList=bullList.replace(getDigitalConstants().getUlStartTag(), EMPTY_SPACE);
		//get  rid of  the </ul> tags if any in the string
		 bullList=bullList.replace(getDigitalConstants().getUlEndTag(), EMPTY_SPACE);
		 //finally get rid of any html special characters if there  are any in the string  
		 bullList=bullList.replaceAll("\\<(/?[^\\>]+)\\>",EMPTY_SPACE);
		 //Trim empty spaces before we add it  to the  collection.
		 arrList.add(bullList.replace(getDigitalConstants().getListItemEndTag(), EMPTY_SPACE).trim());
	 }
	 return arrList;
 }

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   *
   * @param pTargetObject
   *   The object which the specified property is associated with.
   * @param pPropertyName
   *   The name of the property to return.
   * @param pAttributes
   *   The key/value pair attributes defined in the beanFilteringConfiguration.xml file
   *   for this property.
   *
   * @return
   *   list of bullets as a collection.
   *
   * @throws BeanFilterException
   */
  @Override
  public Object getPropertyValue(Object pTargetObject, String pPropertyName,
    Map<String,String> pAttributes) throws BeanFilterException {

    // Get bullet list that needs to be formatted.
    Object propValue = null;

    try {
      propValue = DynamicBeans.getPropertyValue(pTargetObject, pPropertyName);
    }
    catch (PropertyNotFoundException e) {
      throw new BeanFilterException(e);
    }

    // Return  null if we dont get a token value to format.
    if (propValue == null){
    	if(logger.isLoggingDebug()){
    		logger.logDebug("Property {0} was not a valid promotion type: {1}" + pPropertyName +"::"+propValue);
    		
    	}
        return null;
    }

    // Get the locale and pattern to use for formatting.
    return extractBullets((String)propValue);
  }
}
