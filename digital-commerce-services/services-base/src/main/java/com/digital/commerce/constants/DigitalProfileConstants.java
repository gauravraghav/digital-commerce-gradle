package com.digital.commerce.constants;

import java.util.regex.Pattern;

/** Constants used to get properties for user profile.
 * 
 * @author $author$
 * @version $Revision: #4 $ */
public interface DigitalProfileConstants {
	public static final String	MSG_RESTRICTED_WORD							= "errRestrictedWord";
	public static final String	MSG_SELECT_GENDER							= "errSelectGender";
	public static final String	MSG_INVALID_EMAIL							= "invalidEmailAddress";
	public static final String  MSG_EMAIL_MISSING							= "missingEmailAddress";
	public static final String	MSG_INVALID_EMAIL_FORMAT					= "invalidEmailFormat";
	public static final String	MSG_INVALID_CONFIRM_EMAIL					= "invalidConfirmEmailAddress";
	public static final String	MSG_EMAIL_ALREADY_EXISTS					= "emailAlreadyExist";
	public static final String	MSG_INVALID_CONFIRM_PASSWORD				= "invalidConfirmPassword";//NOSONAR
	public static final String	MSG_INVALID_OLD_PASSWD						= "invalidOldPassword";//NOSONAR
	public static final String	MSG_EMPTY_OLD_PASSWD						= "emptyOldPassword";//NOSONAR
	public static final String	MSG_INVALID_NEW_PASSWD						= "invalidNewPassword";//NOSONAR
	public static final String	MSG_INVALID_CONF_NEW_PASSWD					= "invalidConfirmNewPassword";//NOSONAR
	public static final String	MSG_INVALID_PASSWD							= "invalidPasswd";//NOSONAR
	public static final String	MSG_INVALID_PASSWD_CRITERIA					= "invalidPasswordCriteria";//NOSONAR
	public static final String	MSG_INVALID_PASSWD_LENGTH					= "invalidPasswdLength";//NOSONAR
	public static final String	MSG_INVALID_PASSWD_FIRST					= "invalidPasswdFirst";//NOSONAR
	public static final String	MSG_INVALID_PASSWD_LAST						= "invalidPasswdLast";//NOSONAR
	public static final String	MSG_INVALID_PASSWD_SEQ						= "invalidPasswordSeq";//NOSONAR
	public static final String	MSG_INVALID_PASSWD_NO_CHANGE				= "invalidPasswordNoChange";//NOSONAR
	public static final String	MSG_REWARDS_ACCOUNT_NOT_FOUND				= "rewardsAccountNotFound";
	public static final String	MSG_REWARDS_ACCOUNT_ALREADY_ASSOCIATED		= "rewardsAccountAlreadyAssociated";
	public static final String	MSG_INVALID_PASSWD_RESERVEKEY				= "invalidPasswordReserveKey";//NOSONAR
	public static final String	MSG_INVALID_PASSWORD_LOYALTY_NUMBER			= "invalidPasswordLoyaltyNo";//NOSONAR
	public static final String	MSG_CONF_PASSWD_SUCCESS						= "changePasswordSuccessMessage";//NOSONAR
	public static final String	MSG_INVALID_PASSWD_EMAIL					= "invalidPasswdEmail";//NOSONAR
	public static final String	MSG_INVALID_FIRSTNAME						= "invalidFirstName";
	public static final String	MSG_INVALID_MIDDLENAME						= "invalidMiddleName";
	public static final String	MSG_INVALID_LASTNAME						= "invalidLastName";
	public static final String	MSG_MISSIGN_BILLING_ADDRESS					= "missingBillingAddress";
	public static final String	MSG_INVALID_BILLING_ADDRESS					= "invalidBillingAddress";
	public static final String	MSG_INVALID_SHIPPING_ADDRESS				= "invalidShippingAddress";
	public static final String	MSG_INVALID_ADDRESS							= "invalidAddress";
	public static final String	MSG_INVALID_ADDRESS_POBOX					= "invalidAddressPobox";
	public static final String	MSG_POBOX_IN_ADDRESS						= "invalidPoboxInAddress";
	public static final String	MSG_INVALID_POBOX							= "invalidPobox";
	public static final String	MSG_SUGGEST_POBOX							= "suggestPobox";
	public static final String	MSG_INVALID_ADDRESS2						= "invalidAddress2";
	public static final String	MSG_INVALID_ADDRESS3						= "invalidAddress3";
	public static final String	MSG_INVALID_CITY							= "invalidCity";
	public static final String	MSG_INVALID_SELECTED_CITY					= "invalidSelectedCity";
	public static final String	MSG_INVALID_SELECTED_PO						= "invalidSelectedPostOffice";
	public static final String	MSG_INVALID_STATE							= "invalidState";
	public static final String	MSG_INVALID_PROVINCE_TERRITORY				= "invalidProvinceTerritory";
	public static final String	MSG_INVALID_COUNTRY							= "invalidCountry";
	public static final String	MSG_INVALID_BUSINESS_NAME					= "invalidbusinessName";
	public static final String	MSG_INVALID_COMPANY_NAME					= "invalidCompanyName";
	public static final String	MSG_INVALID_ZIP_CODE						= "invalidZipCodeLength";
	public static final String	MSG_INVALID_ZIP_CODE_USA					= "invalidZipCodeUSA";								// Added for
																																// 985
	public static final String	MSG_INVALID_STORE_ZIP_CODE					= "invalidStoreZipCode";
	public static final String	MSG_INVALID_ZIP_CODE_INTERNATIONAL			= "invalidZipCodeInternational";
	public static final String	MSG_INVALID_PHONE							= "invalidPhone";
	public static final String	MSG_INVALID_PHONE_NUMBER					= "invalidPhoneNumber";
	public static final String	MSG_INVALID_GENDER							= "invalidGender";
	public static final String	MSG_INVALID_REGION							= "invalidRegion";
	public static final String	MSG_INVALID_LOGIN							= "invalidLogin";
	public static final String	MSG_INVALID_MEMBER							= "invalidMember";
	public static final String	MSG_INVALID_CUSTOMER_PROFILEID				= "invalidCustomerProfileId";
	public static final String	MSG_INVALID_EMAIL_PHONE_ADDRESS1			= "invalidEmailPhoneAddress1";
	public static final String	MSG_NO_CUSTOMER_PROFILEID_FOUND				= "noCustomerProfileIdFound";
	public static final String	MSG_NO_CUSTOMER_LASTNAME_FOUND				= "noCustomerLastNameFound";

	public static final String	MSG_INVALID_SHIPPING						=	"shipping";	
	public static final String	MSG_INVALID_SHIPPING_FIRSTNAME				= MSG_INVALID_SHIPPING + MSG_INVALID_FIRSTNAME;
	public static final String	MSG_INVALID_SHIPPING_LASTNAME				= MSG_INVALID_SHIPPING + MSG_INVALID_LASTNAME;
	public static final String	MSG_INVALID_SHIPPING_ADDRESS1				= MSG_INVALID_SHIPPING + MSG_INVALID_ADDRESS;
	public static final String	MSG_INVALID_SHIPPING_ADDRESS2				= MSG_INVALID_SHIPPING + MSG_INVALID_ADDRESS2;
	public static final String	MSG_INVALID_SHIPPING_CITY					= MSG_INVALID_SHIPPING + MSG_INVALID_CITY;
	public static final String	MSG_INVALID_SHIPPING_SELECTED_CITY			= MSG_INVALID_SHIPPING + MSG_INVALID_SELECTED_CITY;
	public static final String	MSG_INVALID_SHIPPING_STATE					= MSG_INVALID_SHIPPING + MSG_INVALID_STATE;
	public static final String	MSG_INVALID_SHIPPING_PROVINCE_TERRITORY		= MSG_INVALID_SHIPPING + MSG_INVALID_PROVINCE_TERRITORY;
	public static final String	MSG_INVALID_SHIPPING_COUNTRY				= MSG_INVALID_SHIPPING + MSG_INVALID_COUNTRY;
	public static final String	MSG_INVALID_SHIPPING_ZIPCODE				= MSG_INVALID_SHIPPING + MSG_INVALID_ZIP_CODE;
	public static final String	MSG_INVALID_SHIPPING_ZIP_CODE_USA			= MSG_INVALID_SHIPPING + MSG_INVALID_ZIP_CODE;
	public static final String	MSG_INVALID_SHIPPING_STORE_ZIP_CODE			= MSG_INVALID_SHIPPING + MSG_INVALID_STORE_ZIP_CODE;
	public static final String	MSG_INVALID_SHIPPING_ZIP_CODE_INTERNATIONAL	= MSG_INVALID_SHIPPING + MSG_INVALID_ZIP_CODE_INTERNATIONAL;
	public static final String	MSG_INVALID_SHIPPING_REGION					= MSG_INVALID_SHIPPING + MSG_INVALID_REGION;
	public static final String	MSG_INVALID_SHIPPING_RANK					= "shippinginvalidRank";

	public static final String	MSG_INVALID_DATEOFBIRTH						= "invalidDOB";
	public static final String	MSG_INVALID_REGADDRESS						= "invalidRegAddress";
	public static final String	MSG_INVALID_CATEGORY_SIZE_WIDTH				= "invalidCategorySizeWidth";
	public static final String	MSG_INVALID_GIFTCARD_NUMBER					= "invalidGiftCardNumber";
	public static final String	MSG_INVALID_GIFTCARD_PIN_NUMBER				= "invalidGiftCardPinNumber";

	public static final String	MSG_INVALID_CARD_TYPE						= "invalidcreditCardType";
	public static final String	MSG_INVALID_CARD_NAME						= "invalideCardName";
	public static final String	MSG_INVALID_CARD_NUMBER						= "invalidCardNumber";
	public static final String	MSG_INVALID_EXP_MONTH						= "invalidExpMonth";
	public static final String	MSG_INVALID_EXP_YEAR						= "invalidExpYear";
	public static final String	MSG_INVALID_EXP								= "invalidExp";
	public static final String	MSG_INVALID_CVV								= "invalidCVV";
	public static final String	MSG_INVALID_CVV_NUMBER						= "invalidCVVLength";
	public static final String	MSG_CARD_EXPIRED							= "msgCardExpired";
	public static final String	MSG_ERROR_CARD_CREATE						= "msgErrorCreateCard";
	public static final String	MSG_SAVED_CARD_EXPIRED						= "savedCreditCardExpired";
	public static final String	MISSINGNAMEONCARD							= "missingNameOnCard";
	public static final String	MISSING_VANTIV_FIRST_SIX					= "missingVantivFirstSix";
	
	

	public static final String	MSG_INVALID_CREDIT_CARD_NUMBER				= "invalidCardNumber";
	public static final String	MSG_INVALID_CREDIT_CARD_EXPIRATION_YEAR		= "invalidExpYear";
	public static final String	MSG_INVALID_CREDIT_CARD_EXPIRATION_MONTH	= "invalidExpMonth";
	public static final String	MSG_EXPIRED_CREDIT_CARD						= "creditCardExpired";

	public static final String	MSG_INVALID_NICKNAME						= "invalidNickName";
	public static final String	DUPLICATE_ADDRESS_NICKNAME					= "duplicateNickNme";

	public static final String	MSG_INVALID_ITEM							= "invalidItem";
	public static final String	MSG_INVALID_LOYALTY_NUMBER					= "invalidLoyaltyNumber";
	public static final String	MSG_MEMBER_ALREADY_ASSOCIATED				= "memberAlreadyAssociated";
	public static final String	MSG_INVALID_CUSTOMER_REGISTRATION_AS_MEMBER	= "invalidCustomerRegistrationAsMember";
	public static final String	MSG_SERVICES_EXCEPTION						= "servicesException";
	public static final String	MSG_REPOSITORY_EXCEPTION					= "repositoryException";
	public static final String	MSG_PASSWORDCHANGE_EXCEPTION				= "passwordChangeException";//NOSONAR

	public static final String	ERR_PASSWORDCHANGE							= "errPasswordChange";//NOSONAR

	public static final String	MSG_INVALID_PROFILE							= "invalidProfile";
	public static final String	MSG_INVALID_CATEGORY						= "invalidCategory";
	public static final String	MSG_INVALID_BRAND							= "invalidBrand";
	public static final String	MSG_INVALID_PRODUCT							= "invalidProduct";
	public static final String	MSG_INVALID_SIZE							= "invalidSize";
	public static final String	MSG_INVALID_WIDTH							= "invalidWidth";
	public static final String	MSG_CREDIT_CARD_NUMBER_LENGTH				= "invalidCreditCardNumberLength";
	public static final String	MSG_CREDIT_CARD_NAME_LENGTH					= "invalideCardNameLength";
	public static final String	MSG_CREDIT_CARD_EXISTS						= "creditCardExists";

	public static final String	MSG_ERROR_CREATE_ADDRESS					= "errorCreateAddress";
	public static final String	MSG_ERROR_CREATE_PROFILE_PRIMARY_CONTACT	= "errorCreatePrimaryContactProfile";
	public static final String	MSG_ERROR_UPDATE_PROFILE_PRIMARY_CONTACT	= "errorUpdatePrimaryContactProfile";
	public static final String	MSG_ERROR_OVERRIDE_PROFILE_HOME_ADDRESS		= "errorOverrideProfileHomeAddress";
	public static final String	MSG_ERROR_SAVING_ADDRESS					= "errorSavingAddress";
	public static final String	MSG_EMPTY_SUBTOTAL							= "emptySubTotal";
	public static final String	MSG_INVALID_SUBTOTAL						= "invalidSubTotal";
	public static final String	MSG_EMPTY_BARCODE_NUMBER					= "emptyBarcodeNumber";
	public static final String	MSG_INVALID_BARCODE_NUMBER					= "invalidBarcodeNumber";
	public static final String	MSG_INVALID_CERTIFICATE_VALUE				= "invalidCertificateValue";
	public static final String	MSG_ADDRESS_VERIFICATION_ERROR				= "addressVerificationError";
	public static final String	MSG_ADDRESS_VERIFICATION_ERROR_PARAM		= "addressVerificationErrorParam";
	public static final String	MSG_ERROR_UPDATING_ALERT_PREFERENCES		= "alertUpdateServiceException";
	public static final String	MSG_ERROR_REMOVING_CARD						= "msgErrorRemoveCard";
	public static final String   CITY_APO									="APO";  
	public static final String   CITY_FPO									="FPO"; 
	public static final String   USPS_STATUS_ACCEPTED						="Accepted"; 
	public static final String   USPS_STATUS_SUGGESTED						="Suggested";  
	public static final String   USPS_STATUS_ERROR						    ="Error";  
	public static final String   USA_ADDRESS_TYPE						    ="USA";  
	public static final String   USC_ADDRESS_TYPE						    ="USC";  
	public static final String   MIL_ADDRESS_TYPE						    ="MIL"; 
	public static final String   ADDRESS_USER_ACCEPTED						="Accepted"; 
	public static final String   ADDRESS_USER_SUGGESTED						="Suggested";  
	




	/* Specified below are the min and max length for various name fields. */
	public static final int		MIN_FNAME_LENGTH							= 1;
	public static final int		MAX_FNAME_LENGTH							= 40;
	public static final int		MIN_LNAME_LENGTH							= 1;
	public static final int		MAX_LNAME_LENGTH							= 40;
	public static final int		MAX_MNAME_LENGTH							= 40;

	public static final int		MAX_COMPANY_LENGTH							= 40;

	/* Address related Constants */
	public static final int		MAX_ADDR_LENGTH								= 50;
	public static final int		MIN_ADDR_LENGTH								= 1;												// applicable
																																// for
																																// address
																																// line 1
																																// only.
	public static final int		MIN_CITY_LENGTH								= 1;
	public static final int		MAX_CITY_LENGTH								= 30;
	public static final int		MIN_STATE_LENGTH							= 1;
	public static final int		MAX_STATE_LENGTH							= 20;
	public static final int		MIN_US_POSTAL_CODE_LENGTH					= 5;
	public static final int		MAX_US_POSTAL_CODE_LENGTH					= 10;
	public static final int		MIN_POSTAL_CODE_LENGTH						= 0;
	public static final int		MAX_POSTAL_CODE_LENGTH						= 12;
	public static final int		MIN_COUNTRY_LENGTH							= 1;
	public static final int		MAX_COUNTRY_LENGTH							= 40;
	public static final int		MIN_PHONE_LENGTH							= 1;
	public static final int		MAX_PHONE_LENGTH							= 15;
	public static final int		MAX_PHONE_US_LENGTH							= 10;
	public static final int		MIN_EMAIL_LENGTH							= 1;
	public static final int		MAX_EMAIL_LENGTH							= 256;
	public static final int		MIN_REGION_LENGTH							= 1;
	public static final int		MIN_PASSWORD_LENGTH							= 8;
	public static final int		MAX_PASSWORD_LENGTH							= 25;

	public static final int		MIN_LOYALTY_LENGTH							= 8;
	public static final int		MAX_LOYALTY_LENGTH							= 16;

	public static final int		MAX_CREDIT_CARDNUMBER_LENGTH				= 16;
	public static final int		MAX_CREDIT_CARDNUMBER_LENGTH_AMEX			= 15;

	// Added for Credit Card Mandate Project. - Start
	public static final int[]	MAX_CREDIT_CARDNUMBER_LENGTH_DINERS			= { 14, 16 };	//NOSONAR									// The order
																																// of these
																																// values is
																																// important.
																																// Please do
																																// not
																																// change.
	// Added for Credit Card Mandate Project. - End

	public static final int		MIN_CVV_NUMBER_LENGTH						= 3;
	public static final int		MAX_CVV_NUMBER_LENGTH						= 4;

	public static final String	CREDIT_CARD_TYPE_MASTERCARD					= "creditCardTypeMaster";
	public static final String	CREDIT_CARD_TYPE_VISA						= "creditCardTypeVisa";
	public static final String	CREDIT_CARD_TYPE_AMEX						= "creditCardTypeAmex";
	public static final String	CREDIT_CARD_TYPE_DISCOVER					= "creditCardTypeDiscover";
	public static final String	CREDIT_CARD_TYPE_DINERS						= "creditCardTypeDiners";
	public static final String	CREDIT_CARD_TYPE_JCB						= "creditCardTypeJcb";
	public static final String	CREDIT_CARD_TYPE_MC							= "creditCardTypeMc";
	public static final String	CREDIT_CARD_TYPE_DI							= "creditCardTypeDI";
	public static final String	CREDIT_CARD_TYPE_VI							= "creditCardTypeVI";
	public static final String	CREDIT_CARD_TYPE_AX							= "creditCardTypeAX";
	public static final String	CREDIT_CARD_TYPE_DSW_VISA					= "creditCardTypeDSWVisa";
	public static final String	MISSING_LOWVALUE_OR_HIGHVALUE_TOKEN 		= "missingHighOrLowValueToken";

	public static final int		GIFT_CARDNUMBER_LENGTH						= 19;
	public static final int		GIFT_PINNUMBER_LENGTH						= 4;

	public static final int		MAX_FORGOT_PASSWORD_RETRY					= 5;
	public static final int		MAX_ORDERS_ON_MYPROFILE						= 2;
	public static final int		MAX_ORDERS_ON_ORDERHISTORY					= 3;

	public static final String	MSG_INVALID_ORDERNUMBER						= "invalidOrderNumber";
	public static final String	MSG_INVALID_ORDERTIMEFRAME					= "InvalidOrderTimeframe";
	// Rewards Related Constants
	public static final int		MAX_BARCODE_NUMBER_LENGTH					= 24;

	// Defining Constant for Leap Year as 2004 to be used for New registration
	public static final int		LEAPYEAR									= 2004;

	public static final String	MSG_CSC_ORDER_HISTORY_MANDATORY_FIELD		= "cscOrderHistoryMandatoryField";

	// Added to fix defect 783
	public static final int		MAX_RANK_LENGTH								= 100;
	public static final String	MSG_INVALID_RANK							= "invalidRank";

	// Code for fix 706 starts
	public static final String	MSG_INVALID_ENTERED_CREDIT_CARD_NUMBER		= "invalidEnteredCreditCardNumber";
	public static final String	MSG_CREDIT_CARD_NUMBER_LENGTH_AMEX			= "invalidCreditCardNumberLengthAmex";
	public static final int		MAX_NAME_ON_CARD_LENGTH						= 40;
	// Code for fix 706 ends

	// Added for Credit Card Mandate Project. - Start
	public static final String	MSG_CREDIT_CARD_NUMBER_LENGTH_DINERS		= "invalidCreditCardNumberLengthDiners";
	// Added for Credit Card Mandate Project. - End

	public static final String	CREATE_ENTER_MEMBER_ID						= "2";
	public static final String	CREATE_GET_MEMBER_ID						= "1";
	public static final String	CREATE_NO_MEMBER_ID							= "0";
	public static final String	NO_CREATE_ACCOUNT							= "3";

	// Added to fix defect 883 starts
	public static final int		LENGTH_LOYALTY_NUMBER						= 12;
	// Added to fix defect 883 ends

	// Added to fix defect 707 starts
	public static final String	MSG_INVALID_TO_NAME							= "invalidToName";
	public static final String	MSG_INVALID_FROM_NAME						= "invalidFromName";
	public static final String	MSG_INVALID_GIFT_MESSAGE					= "invalidGiftMessage";
	public static final int		MAX_TO_NAME_LENGTH							= 80;
	public static final int		MAX_FROM_NAME_LENGTH						= 80;
	public static final int		MAX_GIFT_MESSAGE_LENGTH						= 100;
	// Added to fix defect 707 ends

	// Added to fix 1167
	public static final String	MSG_INVALID_STORE_RADIUS					= "invalidStoreRadius";
	// added to fix 2109
	public static final String	MSG_CHANGE_PASSWD_SUCCESS					= "changePasswordSuccess";//NOSONAR

	public static final String	MSG_INVALID_PASSWORDS_NO_MATCH				= "invalidPasswordsNoMatch";//NOSONAR

	public static final String	PROFILE_BILLINGADDR							= "billingAddress";
	public static final String	PROFILE_HOMEADDR							= "homeAddress";
	public static final String	PROFILE_FIRSTNAME							= "firstName";
	public static final String	PROFILE_LASTNAME							= "lastName";
	public static final String	PROFILE_EMAIL								= "email";
	public static final String	PROFILE_LOYALTYNUM							= "loyaltyNumber";
	
	public static final String	IS_PAYPAL_EXPRESS 							= "ispaypalexpress";
	public static final String	IS_PAYPAL_REGULAR							= "ispaypalregular";
      public static final String	IS_PAYPAL_REGULAR_OMNITURE				= "isPaypalRegularOmniture";
      
  	// Region Constants
  	public static final String	REGION_AE	= "AE";
  	public static final String	REGION_AP	= "AP";
  	public static final String  REGION_AA	= "AA";
  	
  	//GiftCard 
  	public static final String  GIFTCARD_ERROR_CODES = "giftCardErrorCodes";
  	public static final String  GIFTCARD_INTEGRATION_DEFAULT_ERROR_MESSAGE = "giftcard.integrationservice.defaultMessage";
  	public static final int	GIFT_CARD_SHIPPING_GROUP_STATE	= 8;
	public static final String	GIFTCARD_INSUFFICIENT_FUNDS				= "FailedGiftCardPaymentInsufficientFunds";

  	public static final String DRIVER_DATE_FORMAT = "yyyy-MM-dd'T'HHmmssZ";
	/** Matches strings of the form (PO BOX 1234, P.O. BOX 1234, POST OFFICE BOX
	 * 1234), etc.) */
  	public static final Pattern	PO_BOX_PATTERN = Pattern.compile( "P(OST)?[.?\\s]*[O](FFICE)?[.?\\s]*[B][O][X]\\s*\\d+", Pattern.CASE_INSENSITIVE );
  	public static final int	US_POSTAL_CODE_LENGTH = 5;

  	//Tax
  	public static final String			DETAILED_ITEM_PRICE_TAX_PROPERTY	= "tax";
  	public static final String			SHIPPING_HANDLING_TAX_PROPERTY	= "SHIP";
  	
  	public static final String    CREATE_REWARD_SUCCESS = "CREATE_REWARD_SUCCESS";
  	public static final String    CREATE_REWARD_FAILURE = "CREATE_REWARD_FAILURE";
  	
  	public static final String    UPDATE_REWARD_SUCCESS = "UPDATE_REWARD_SUCCESS";
  	public static final String    UPDATE_REWARD_FAILURE = "UPDATE_REWARD_FAILURE";
  	public static final String    UPDATE_REWARD_USER_FAILURE = "UPDATE_REWARD_USER_FAILURE";
  	public static final String    UPDATE_REWARD_FAILURE_EXISTING_MOBILE_PHONE_NUMBER = "UPDATE_REWARD_FAILURE_EXISTING_MOBILE_PHONE_NUMBER";
  	public static final String    UPDATE_REWARD_FAILURE_EXISTING_EMAIL_ID = "UPDATE_REWARD_FAILURE_EXISTING_EMAIL_ID";
  	
  	public static final String   FIRST_NAME_REQUIRED = "FIRST_NAME_REQUIRED";
  	public static final String   LAST_NAME_REQUIRED = "LAST_NAME_REQUIRED";
  	public static final String   CERT_AMOUNT_REQUIRED = "certAmountRequired";
  	public static final String   PAPERLESS_FLAG_REQUIRED = "paperlessFlagRequired";
  	public static final String   PROFILE_UPDATE_ERROR = "errorUpdateProfile";

  	public static final String   EMAIL_ALERT_FREQUENCY = "NO_PREFERENCE";
  	public static final String   REWARDS_EMAIL_ALERT_FREQUENCY = "1";
  	
  	public static final String  REWARDS_POINTS_BALANCE_MIN="1500";
  	public static final String CUSTOMER_DEFAULT_BIRTH_YEAR = "2004";
  	public static final String	PROFILE_ID							= "id";  	
  	
  	public static final String INVALID_LOGIN_DIFFERENT_USER = "A session already exists, and the username and/or password you have given differ from those of the currently authenticated user.  Logging out the currently authenticated session.";
  	public static final String	UPDATE_FAVSTORE_FAILURE = "UPDATE_FAVSTORE_FAILURE";
  	public static final String	USER_STATUS_LOGGED_IN = "LOGGED_IN";
  	public static final String	USER_STATUS_ANONYMOUS = "ANONYMOUS";
  	public static final String	USER_STATUS_COOKIED = "COOKIED";
  	public static final String	USER_STATUS_ANONYMOUS_COOKIED = "ANONYMOUS_COOKIED";
  	
}
