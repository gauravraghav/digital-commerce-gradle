package com.digital.commerce.constants;

import lombok.Getter;

public final class ProducCatalogConstants {
	@Getter
	public enum ProducCatalogPropertyManager{
		PRODUCT_TITLE("productTitle"),
		PRODUCT_TYPE("productType");
		
		private final String value;
	
		private ProducCatalogPropertyManager( String value ) {
			this.value = value;
		}

	}
}
