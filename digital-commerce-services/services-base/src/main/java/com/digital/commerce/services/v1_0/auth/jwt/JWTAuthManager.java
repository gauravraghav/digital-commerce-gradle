package com.digital.commerce.services.v1_0.auth.jwt;

import atg.nucleus.logging.ApplicationLoggingImpl;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.digital.commerce.common.crypto.DigitalVaultUtil;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.DigitalStringUtil;
//import com.digital.commerce.services.storelocator.DigitalStoreTools;
import lombok.Getter;
import lombok.Setter;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Getter
@Setter
public class JWTAuthManager extends ApplicationLoggingImpl implements JWTAuthConstants {

  private static final String ACCESS_DENIED = "ACCESS_DENIED";

  /*
   * make sure that any getter that is vaulted uses the DigitalVaultUtil to
   * decrypt. Make sure to use the getVaultedData method to return the value
   */
  private boolean useVaultedPasswords = true;
  private Map<String, Map<String, Map<String, String>>> dataMap = new HashMap<>();
  private long expiryInMills = 120000;
  private String issuer;

  public JWTAuthManager() {
    super(JWTAuthManager.class.getName());
  }

  @Getter
  private enum ClientApplicationName {
    STORES("stores");

    String name;

    /**
     * @param name
     */
    ClientApplicationName(String name) {
      this.name = name;
    }
  }

  @Getter
  private enum DataTypes {
    STATIC("static");

    String name;

    /**
     * @param name
     */
    DataTypes(String name) {
      this.name = name;
    }
  }

  /**
   * @param userName
   * @param password
   * @param source
   * @return String
   * @throws DigitalAppException
   */
  public String generateToken(String userName, String password, String source)
      throws DigitalAppException {
    try {
      // validate username and password for the given source
      String authorizedUserName = getUsername(source);
      if (DigitalStringUtil.isEmpty(authorizedUserName) ||
          !userName.equalsIgnoreCase(authorizedUserName)) {
        logError("Not authorized to access this service with the supplied username: " + userName
            + " & source: " + source);
        throw new DigitalAppException(ACCESS_DENIED,
            "Not authorized to access this service with the supplied username & source",
            new Exception());
      }
      String authorizedPassword = getPassword(source);
      if (DigitalStringUtil.isEmpty(authorizedPassword) ||
          !password.equals(getVaultedData(authorizedPassword))) {
        logError(
            "Not authorized to access this service with the supplied password for the user: "
                + userName + " & source: " + source);
        throw new DigitalAppException(ACCESS_DENIED,
            "Not authorized to access this service with the supplied password & source",
            new Exception());
      }

      long issuedAt = System.currentTimeMillis();
      long expiresAt = issuedAt + getExpiryInMills(source);
      Algorithm algorithm = Algorithm.HMAC256("secret");
      return JWT.create()
          .withIssuer(getIssuer(source))
          .withExpiresAt(new Date(expiresAt))
          .withIssuedAt(new Date(issuedAt))
          .withNotBefore(new Date(issuedAt))
          .withJWTId(UUID.randomUUID().toString())
          .withSubject(userName)
          .sign(algorithm);

    } catch (UnsupportedEncodingException enEx) {
      logError("Exception while generating a JWT Token: ", enEx);
      throw new DigitalAppException(ENCODING_ERROR, enEx.getLocalizedMessage(), enEx);
    }
  }

  /**
   * @param token
   * @return true or false
   * @throws DigitalAppException
   */
  public boolean validateToken(String token) throws DigitalAppException {
    try {
      Algorithm algorithm = Algorithm.HMAC256("secret");
      JWTVerifier verifier = JWT.require(algorithm)
          .withIssuer(issuer)
          .build();
      verifier.verify(token);
      return true;
    } catch (UnsupportedEncodingException enEx) {
      logError("UnsupportedEncodingException while validating a JWT Token: ", enEx);
      throw new DigitalAppException(ENCODING_ERROR, enEx.getLocalizedMessage(), enEx);
    } catch (TokenExpiredException enEx) {
      logError("JWTVerificationException while validating a JWT Token: ", enEx);
      throw new DigitalAppException(TOKEN_EXPIRED, enEx.getLocalizedMessage(), enEx);
    } catch (JWTVerificationException enEx) {
      logError("JWTVerificationException while validating a JWT Token: ", enEx);
      throw new DigitalAppException(VERIFICATION_ERROR, enEx.getLocalizedMessage(), enEx);
    }
  }

  /**
   * @return Map
   */
  public Map<String, String> getStoresStaticData() {
    return getMapData(ClientApplicationName.STORES.getName(), DataTypes.STATIC.getName());
  }

  /**
   * @param storesStaticData
   */
  public void setStoresStaticData(Map<String, String> storesStaticData) {
    setMapData(ClientApplicationName.STORES.getName(), DataTypes.STATIC.getName(),
        storesStaticData);
  }

  /**
   * @param clientAppName
   * @param dataType
   * @param data
   */
  public void setMapData(String clientAppName, String dataType, Map<String, String> data) {
    if (!dataMap.containsKey(clientAppName)) {
      dataMap.put(clientAppName, new HashMap<String, Map<String, String>>());
    }

    Map<String, Map<String, String>> serviceData = dataMap.get(clientAppName);

    if (!serviceData.containsKey(dataType)) {
      serviceData.put(dataType, new HashMap<String, String>());
    }

    serviceData.get(dataType).putAll(data);

  }

  /**
   * @param clientAppName
   * @param dataType
   * @return Map
   */
  public Map<String, String> getMapData(String clientAppName, String dataType) {
    if (dataMap.containsKey(clientAppName)) {
      if (dataMap.get(clientAppName).containsKey(dataType)) {
        return dataMap.get(clientAppName).get(dataType);
      }
    }

    return new HashMap<>();
  }

  /**
   * @return String
   */
  public String getIssuer(String source) {
    String issuerForSource = getMapData(source, DataTypes.STATIC, "issuer");
    if(DigitalStringUtil.isEmpty(issuerForSource)){
      return issuer;
    }
    return issuerForSource;
  }

  /**
   * @param source
   * @return long
   */
  public long getExpiryInMills(String source) {
    String expiryPeriod = getMapData(source, DataTypes.STATIC, "expiryInMills");
    if(DigitalStringUtil.isEmpty(expiryPeriod)){
      return expiryInMills;
    }
    return Long.parseLong(expiryPeriod);
  }

  /**
   * @param source
   * @return String
   */
  public String getUsername(String source) {
    return getMapData(source, DataTypes.STATIC, "username");
  }

  /**
   * @param source
   * @return String
   */
  private String getPassword(String source) {
    return getMapData(source, DataTypes.STATIC, "password");
  }

  /**
   * @param clientApplicationName
   * @param dataType
   * @param dataKey
   * @return String
   */
  public String getMapData(String clientApplicationName, DataTypes dataType, String dataKey) {
    return getMapData(clientApplicationName, dataType.getName()).get(dataKey);
  }


  /**
   * This method is to be used in any accessor that uses the DigitalVaultUtil
   *
   * @param value
   * @return String
   */
  public String getVaultedData(String value) {
    if (useVaultedPasswords) {
      try {
        return DigitalVaultUtil.getDecryptedString(value);
      } catch (Exception e) {
        logError("Exception when un-vaulting data", e);
        return null;
      }
    } else {
      return value;
    }
  }
}
