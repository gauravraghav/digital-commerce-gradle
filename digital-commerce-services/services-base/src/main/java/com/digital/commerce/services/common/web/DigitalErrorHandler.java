package com.digital.commerce.services.common.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;

import atg.json.JSONArray;
import atg.json.JSONObject;

public class DigitalErrorHandler extends HttpServlet {

	private static transient DigitalLogger logger = DigitalLogger
			.getLogger(DigitalErrorHandler.class);
	private static final long serialVersionUID = 1L;
	private static final String DYN_SESS_CONF = "_dynSessConf";
	private static final String JSESSION_ID = "JSESSIONID";

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			response.setContentType("application/json");
			// We can't send the cache headers if there is any HTTP errors
			response.setHeader("Cache-Control", "max-age=0, no-cache, no-store");
			response.setHeader("Pragma", "no-cache");
			response.setHeader("Expires", "Thu, 01 Jan 1970 00:00:00 GMT");

			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			JSONObject jsonReponse = this.getJSONError(request, response);
			out.println(jsonReponse.toString());
		} catch (Exception ex) {
			logger.error("Exception in creating JSON response", ex);
			try {
				super.doGet(request, response);
			} catch (Exception e) {
				logger.error("Exception in creating JSON response", e);
			}
		}
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doGet(request, response);
		} catch (Exception e) {
			logger.error("Exception in creating JSON response", e);
		}
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	public void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doGet(request, response);
		} catch (Exception e) {
			logger.error("Exception in creating JSON response", e);
		}
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	public void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doGet(request, response);
		} catch (Exception e) {
			logger.error("Exception in creating JSON response", e);
		}
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private JSONObject getJSONError(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Throwable throwable = null;

		if (request.getAttribute("javax.servlet.error.exception") != null) {
			throwable = (Throwable) request
					.getAttribute("javax.servlet.error.exception");
		}

		String errorMesage = (String) request
				.getAttribute("javax.servlet.error.message");
		Integer statusCode = (Integer) request
				.getAttribute("javax.servlet.error.status_code");
		JSONObject finalJson = new JSONObject();
		JSONObject respJSon = new JSONObject();
		JSONArray genericExArray = new JSONArray();
		JSONObject generixEx = new JSONObject();
		if (DigitalStringUtil.isNotBlank(errorMesage)) {
			if (throwable instanceof SQLException
					|| DigitalStringUtil.containsIgnoreCase(errorMesage, "sql")) {
				generixEx
						.accumulate(
								"localizedMessage",
								"Generic error, unable to get errors details from the source initiating the error");
			} else {
				generixEx.accumulate("localizedMessage", errorMesage);
			}
		} else {
			generixEx
					.accumulate(
							"localizedMessage",
							"Generic error, unable to get errors details from the source initiating the error");
		}

		if (null != statusCode) {
			if(statusCode == 409) {
				Cookie[] cookies = request.getCookies();
				if (null != cookies) {
					for (Cookie cookie : cookies) {
						String cookieName = cookie.getName();
						if (DYN_SESS_CONF.equalsIgnoreCase(cookieName)) {
							cookie.setMaxAge(0);
							cookie.setValue(null);
							cookie.setPath("/");
							response.addCookie(cookie);
						}
						if (JSESSION_ID.equalsIgnoreCase(cookieName)) {
							cookie.setMaxAge(0);
							cookie.setValue(null);
							cookie.setPath("/");
							response.addCookie(cookie);
						}
					}
				}
			}
			else if(statusCode == 500) {
				statusCode = 450;
				response.setStatus(statusCode);
			}
			generixEx.accumulate("errorCode", "HTTP_" + statusCode);
		} else {
			generixEx.accumulate("errorCode", "NON_HTTP_GENERIC_ERROR");
		}
		genericExArray.add(generixEx);
		respJSon.accumulate("genericExceptions", genericExArray);
		respJSon.accumulate("formError", false);
		finalJson.accumulate("Response", respJSon);
		return finalJson;
	}
}