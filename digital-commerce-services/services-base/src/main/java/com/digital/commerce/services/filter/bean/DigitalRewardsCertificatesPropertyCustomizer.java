package com.digital.commerce.services.filter.bean;

import java.util.Map;
import java.util.Objects;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;
import atg.userprofiling.Profile;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.DigitalProfileConstants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalRewardsCertificatesPropertyCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {

	@Override
	 public Object getPropertyValue(Object pTargetObject, String pPropertyName, Map<String, String> pAttributes) throws BeanFilterException {
		Boolean propValue;
	    try {
	      propValue = (Boolean)DynamicBeans.getPropertyValue(pTargetObject, pPropertyName);
	    }
	    catch (PropertyNotFoundException e) {
	      throw new BeanFilterException(e);
	    }
		String  pointBankerFlag = null;
		Long  pointsBalance; 
		if (profile != null) {
			pointBankerFlag = (String) profile.getPropertyValue("pointBankerFlag");
			if (!DigitalStringUtil.isEmpty(pointBankerFlag)	&& !Objects.equals(pointBankerFlag, "true")) {
				pointsBalance = (Long) profile.getPropertyValue("pointsBalance");
				if (pointsBalance != null && pointsBalance.longValue() >= new Long(DigitalProfileConstants.REWARDS_POINTS_BALANCE_MIN)) {
					propValue = true;
				}
			}
		}
		return propValue;
	}
	
	private Profile profile;
}
