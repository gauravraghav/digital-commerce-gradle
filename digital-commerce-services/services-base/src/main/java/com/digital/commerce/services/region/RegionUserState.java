package com.digital.commerce.services.region;

import java.io.IOException;
import java.text.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import atg.servlet.DynamoHttpServletResponse;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.cartridge.handler.DigitalMainRefinementMenuHandler;

import atg.core.util.StringUtils;
import atg.servlet.ServletUtil;
import atg.userprofiling.endeca.ProfileUserState;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegionUserState extends ProfileUserState {

  private static final String USERSTATE_PROFILEGROUP = "UserState.profileGroup:";

  private static final String EMPTY = "";
  private static final String ERROR_TIERNOTMATCHING = "tierNotMatchingwithProfileGroup";
  private MessageLocator messageLocator;

  private boolean regionNeeded = true;

  private String regionPrefix;

  private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

  private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalMainRefinementMenuHandler.class);

  @Override
  public Set<String> getUserSegments() {
    if (isRegionNeeded()) {
      setRegionNeeded(false);
      computeRegions();
    }
    return super.getUserSegments();
  }

  public void computeRegions() {
    String regionId = RegionContextManager.getCurrentRegion();
    if (regionId != null) {
      String delimiter = getPrefixDelimiter();

      StringBuilder siteBuffer = new StringBuilder();
      if (getRegionPrefix() != null) {
        siteBuffer.append(getRegionPrefix());
        if (delimiter != null) {
          siteBuffer.append(delimiter);
        }
      }
      siteBuffer.append(regionId);
      addUserSegments(siteBuffer.toString());

    }
  }

  @Override
  public Date getDate() {
    Date dt = null;
    final String endecaDateParam = "Endeca_date";
    try {
      if (null != ServletUtil.getCurrentRequest()) {
        String endecaDate = ServletUtil.getCurrentRequest().getParameter(endecaDateParam);
        if (DigitalStringUtil.isNotBlank(endecaDate)) {
          dt = dateFormat.parse(endecaDate);
        }
      }
    } catch (ParseException e) {
      logger.error("There was issues with the Date format from the UI param Endeca_date ", e);

    }
    return dt;
  }

  @Override
  public String getDateAsString() {
    return this.getDate() == null ? null : this.dateFormat.format(this.getDate());
  }

  @Override
  public Set<String> getProfileGroups()  {
		final String urlTierKey = "tier";
		final String urlABTestCollectionKey = "abcollection";
		Set<String> profileGroups = new HashSet<>();
		try
        {
          if (null != ServletUtil.getCurrentRequest()) {
            String urlTierValue = ServletUtil.getCurrentRequest().getParameter(urlTierKey);
            String urlABTestCollectionValue = ServletUtil.getCurrentRequest().getParameter(urlABTestCollectionKey);
            if (DigitalStringUtil.isNotBlank(urlTierValue) && !urlTierValue.toUpperCase().equals("GUEST")) {
              profileGroups = super.getProfileGroups();
              if ((profileGroups !=null && !profileGroups.contains(urlTierValue.toUpperCase())) || profileGroups == null) { //User Profile Group not matching with the passed in "tier" Querystring value or ProfileGroup is NULL
                if (logger.isDebugEnabled() && profileGroups !=null) {
                  logger.debug("ProfileGroups is not matching with tier Querystring - ATGProfileGroup=" + profileGroups.toString() + " FromURL=" + urlTierValue);
                } else if(logger.isDebugEnabled()) {
                  logger.debug("ProfileGroups is NULL from ATG. - ATGProfileGroup=NULL" + " FromURL=" + urlTierValue);
                }
                DynamoHttpServletResponse res = ServletUtil.getCurrentResponse();
                String msg = this.getMessageLocator().getMessageString(ERROR_TIERNOTMATCHING);
                res.sendError(403, msg);
                return null;
              }
            } else { //If tier doesn't exist in the URL then don't check anything
              profileGroups = super.getProfileGroups();
            }

            if (DigitalStringUtil.isNotBlank(urlABTestCollectionValue) && urlABTestCollectionValue.equals(DigitalEndecaConstants.ABTEST_B) ) {
              profileGroups.add(DigitalEndecaConstants.ABTEST_COLLECTION);
            }
          }
          return profileGroups;
        }
        catch (Exception ex) {
          return null;
        }

	}


}
