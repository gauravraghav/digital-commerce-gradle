package com.digital.commerce.services.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/** Callback interface for interrogating xpath results
 * 
 */
public interface IXpathResultCallback {

	/** Callback method for inspecting a element after an Xpath result
	 * 
	 * @param match - The node that matched the query
	 * @param parent - The parent of the node that matched the query */
	public void doInElement( Document d, Node match );
}
