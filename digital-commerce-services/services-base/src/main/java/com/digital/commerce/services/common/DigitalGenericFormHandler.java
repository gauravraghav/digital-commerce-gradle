package com.digital.commerce.services.common;

import java.io.IOException;

import javax.servlet.ServletException;

import com.digital.commerce.common.config.MessageLocator;

import atg.commerce.util.ConcurrentUpdateDetector;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class DigitalGenericFormHandler extends GenericFormHandler {
	protected RepeatingRequestMonitor repeatingRequestMonitor;
	private MessageLocator messageLocator;
	boolean concurrentUpdate = false;

	/**
	 * 
	 * @param pException
	 * @param pRequest
	 * @param pResponse
	 * @throws ServletException
	 * @throws IOException
	 */
	public void processException(Exception pException, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		String msg = null;

		if (ConcurrentUpdateDetector.isConcurrentUpdate(pException)) {

			if (isLoggingWarning()) {
				logWarning(pException);
			}
			concurrentUpdate = true;

		} else if (isLoggingError()) {
			logError(pException);
		}
		msg = getMessageLocator().getMessageString("concurrentUpdateRequestAttempt");
		addFormException(new DropletException(msg, "concurrentUpdateRequestAttempt"));
	}
}
