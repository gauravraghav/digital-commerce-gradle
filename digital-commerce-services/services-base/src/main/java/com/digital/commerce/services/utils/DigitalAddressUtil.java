package com.digital.commerce.services.utils;

import static com.digital.commerce.constants.DigitalProfileConstants.MIL_ADDRESS_TYPE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.common.AddressType;
import com.digital.commerce.services.common.CountryCode;
import com.digital.commerce.services.common.DigitalAddress;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.common.StateCode;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.google.common.base.Strings;

import atg.commerce.util.PlaceList;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalAddressUtil extends GenericService {
	private String					validShippingPlaces;
	private String storeDetailsQuery;
	
	private Map<Object, PlaceList>	countryPlaceMap	= new HashMap<>();
	private List<String>			placeLists		= new ArrayList<>();

	/** Returns true if the address is null or all of the major fields are blank.
	 * 
	 * @param address
	 * @return */
	public static boolean isAddressEmpty( Address address ) {
		boolean retVal = false;
		if(address==null){
			return true;
		}
		if( address instanceof DigitalRepositoryContactInfo ) {
			retVal = isAddressEmpty( (DigitalRepositoryContactInfo)address );
		} else if( address instanceof DigitalContactInfo ) {
			retVal = isAddressEmpty( (DigitalContactInfo)address );
		} else {
			retVal = ( address == null || ( DigitalStringUtil.isBlank( address.getFirstName() ) && DigitalStringUtil.isBlank( address.getLastName() ) && DigitalStringUtil.isBlank( address.getAddress1() ) && DigitalStringUtil.isBlank( address.getAddress2() )
					&& DigitalStringUtil.isBlank( address.getAddress3() ) && DigitalStringUtil.isBlank( address.getCity() ) && DigitalStringUtil.isBlank( address.getPostalCode() ) ) );
		}
		return retVal;
	}

	/** Returns true if the address is null or all of the major fields are blank.
	 * 
	 * @param address
	 * @return */
	public static boolean isAddressEmpty( DigitalContactInfo address ) {
		if( address == null ) { return true; }
		if( DigitalStringUtil.isBlank( address.getAddressType() ) ) { return true; }
		if( !AddressType.USA.equals( AddressType.valueFor( address ) ) ) { return false; }
		if( !DigitalStringUtil.isBlank( address.getFirstName() ) ) { return false; }
		if( !DigitalStringUtil.isBlank( address.getLastName() ) ) { return false; }
		if( !DigitalStringUtil.isBlank( address.getAddress1() ) ) { return false; }
		if( !DigitalStringUtil.isBlank( address.getAddress2() ) ) { return false; }
		if( !DigitalStringUtil.isBlank( address.getAddress3() ) ) { return false; }
		if( !DigitalStringUtil.isBlank( address.getCity() ) ) { return false; }
		if( !DigitalStringUtil.isBlank( address.getPostalCode() ) ) { return false; }
		return true;
	}

	/** Returns true if the address is null or address 1, 2, 3, city, and postal code are empty.
	 * 
	 * @param address
	 * @return */
	public static boolean isOnlyAddressEmpty( DigitalContactInfo address ) {
		return address == null
				|| ( ( DigitalStringUtil.isBlank( address.getAddressType() ) || AddressType.USA.equals( AddressType.valueFor( address ) ) ) && DigitalStringUtil.isBlank( address.getAddress1() ) && DigitalStringUtil.isBlank( address.getAddress2() )
						&& DigitalStringUtil.isBlank( address.getAddress3() ) && DigitalStringUtil.isBlank( address.getCity() ) && DigitalStringUtil.isBlank( address.getPostalCode() ) );
	}

	/** Returns true if the address is null or address 1, 2, 3, city, and postal code are empty.
	 * 
	 * @param address
	 * @return */
	public static boolean isOnlyAddressEmpty( DigitalRepositoryContactInfo address ) {
		return address == null
				|| ( ( DigitalStringUtil.isBlank( address.getAddressType() ) || AddressType.USA.equals( AddressType.valueFor( address ) ) ) && DigitalStringUtil.isBlank( address.getAddress1() ) && DigitalStringUtil.isBlank( address.getAddress2() )
						&& DigitalStringUtil.isBlank( address.getAddress3() ) && DigitalStringUtil.isBlank( address.getCity() ) && DigitalStringUtil.isBlank( address.getPostalCode() ) );
	}

	/** Returns true if the address is null or address 1, 2, 3, city, and postal code are empty.
	 * 
	 * @param address
	 * @return */
	public static boolean isOnlyAddressEmpty( Address address ) {
		boolean retVal = false;
		if( address instanceof DigitalRepositoryContactInfo ) {
			retVal = isOnlyAddressEmpty( (DigitalRepositoryContactInfo)address );
		} else if( address instanceof DigitalContactInfo ) {
			retVal = isOnlyAddressEmpty( (DigitalContactInfo)address );
		} else {
			retVal = ( address == null || ( DigitalStringUtil.isBlank( address.getFirstName() ) && DigitalStringUtil.isBlank( address.getLastName() ) && DigitalStringUtil.isBlank( address.getAddress1() ) && DigitalStringUtil.isBlank( address.getAddress2() )
					&& DigitalStringUtil.isBlank( address.getAddress3() ) && DigitalStringUtil.isBlank( address.getCity() ) && DigitalStringUtil.isBlank( address.getPostalCode() ) ) );
		}
		return retVal;
	}

	/** Returns true if the address is null or all of the major fields are blank.
	 * 
	 * @param address
	 * @return */
	public static boolean isAddressEmpty( DigitalRepositoryContactInfo address ) {
		return address == null
				|| ( ( DigitalStringUtil.isBlank( address.getAddressType() ) || AddressType.USA.equals( AddressType.valueFor( address ) ) ) && DigitalStringUtil.isBlank( address.getFirstName() ) && DigitalStringUtil.isBlank( address.getLastName() )
						&& DigitalStringUtil.isBlank( address.getAddress1() ) && DigitalStringUtil.isBlank( address.getAddress2() ) && DigitalStringUtil.isBlank( address.getAddress3() ) && DigitalStringUtil.isBlank( address.getCity() ) && DigitalStringUtil.isBlank( address
						.getPostalCode() ) );
	}

	/** Copies fields in the from address to the to address.
	 * 
	 * @param to
	 * @param from */
	public static DigitalContactInfo copyAddress( DigitalContactInfo to, DigitalContactInfo from ) {
		if( to != null && from != null ) {
			to.setAddressType( from.getAddressType() );
			to.setAddress1( from.getAddress1() );
			to.setAddress2( from.getAddress2() );
			to.setFirstName( from.getFirstName() );
			to.setLastName( from.getLastName() );
			to.setPhoneNumber( from.getPhoneNumber() );
			to.setCity( from.getCity() );
			to.setState( from.getState() );
			to.setRegion( from.getRegion() );
			to.setPostalCode( from.getPostalCode() );
			to.setAddressVerification( from.getAddressVerification() );
		}
		return to;
	}
	


	/** 
	 * Copies fields in the from address to the to address.
	 * 
	 * @param to
	 * @param from */
	public static DigitalAddress copyToDSWAddress( DigitalAddress to, DigitalContactInfo from ) {
		if( to != null && from != null ) {
			to.setAddressType( from.getAddressType() );
			to.setAddress1( from.getAddress1() );
			to.setAddress2( from.getAddress2() );
			to.setFirstName( from.getFirstName() );
			to.setLastName( from.getLastName() );
			to.setCity( from.getCity() );
			to.setState( from.getState() );
			to.setRegion( from.getRegion() );
			to.setPostalCode( from.getPostalCode() );
			to.setAddressVerification( from.getAddressVerification() );
		}
		return to;
	}

	/** 
	 * Copies verified fields in the from address to the to address.
	 * 
	 * @param address
	 * @return */
	public static DigitalContactInfo copyVerifiedAddress( DigitalContactInfo to, DigitalContactInfo from ) {
		if( to != null && from != null ) {
			copyAddress( to, from );
			to.setAddressVerification( true );
		}
		return to;
	}

	/** Defaults a few fields based on the address type. For instance the
	 * AddressType is mapped based on the values of the country and state.
	 * 
	 * @param address */
	public static DigitalContactInfo defaultAddressFields( DigitalContactInfo address ) {
		if( address != null ) {

			final AddressType addressType = AddressType.valueFor( address );
			address.setAddressType( addressType.getValue() );
			if( AddressType.MILITARY.equals( addressType ) ) {
				//address.setState( null );
				address.setCountry( CountryCode.USA.getValue() );
			} else {
				address.setRegion( null );
				StateCode stateCode = StateCode.valueFor( address.getState() );
				if( AddressType.USA.equals( addressType ) ) {
					address.setCountry( CountryCode.USA.getValue() );
				} else if( stateCode != null ) {
					address.setCountry( stateCode.getCountryCode().getValue() );
				}
			}
		}
		return address;
	}

	/** Cleans the adddress fields including the email, po box and the phone
	 * number, and the state/region based on the addressbeing military
	 * 
	 * @param address */
	public static DigitalContactInfo cleanAddress( DigitalContactInfo address ) {
		if( address != null ) {
			defaultAddressFields( address );

			if( DigitalStringUtil.isNotBlank( address.getEmail() ) ) {
				address.setEmail( address.getEmail().toLowerCase() );
			}
			if( DigitalStringUtil.isNotBlank( address.getPhoneNumber() ) ) {
				address.setPhoneNumber( cleanPhoneNumber( address.getPhoneNumber(), AddressType.valueFor( address ) ) );
			}
			if( DigitalStringUtil.isNotBlank( address.getPobox() ) && DigitalStringUtil.isBlank( address.getAddress1() ) ) {
				address.setIsPoBox( Boolean.TRUE );
				address.setAddress1( address.getPobox() );
				address.setPobox( "" );
			} else {
				address.setIsPoBox( Boolean.FALSE );
			}
		}
		return address;
	}

	public static String cleanPhoneNumber( String phoneNumber, AddressType addressType ) {
		String retVal = null;
		if( DigitalStringUtil.isNotBlank( phoneNumber ) ) {
			String cleanPhoneNumber = DigitalStringUtil.stripNonDigit( phoneNumber );
			if( AddressType.USA.equals( addressType ) && cleanPhoneNumber.length() > 0 && cleanPhoneNumber.charAt( 0 ) == '1' ) {
				cleanPhoneNumber = cleanPhoneNumber.substring( 1, cleanPhoneNumber.length() );
			}
			retVal = cleanPhoneNumber;
		}
		return retVal;
	}

	/** Returns a nickname based on the city and address as well as avoiding
	 * collisions with existing address.
	 * 
	 * @param address
	 * @param existingAddressNicknames
	 * @return a new nickname to use for the address */
	public static String createNickname( Address address, Set<String> existingAddressNicknames ) {
		final String base = address.getCity() + address.getAddress1();
		String retVal = base;
		for( int x = existingAddressNicknames.size(); existingAddressNicknames.contains( retVal ); x++ ) {
			retVal = base + x;
		}
		return retVal;
	}

	/** Returns true if two addresses are the same as each other.
	 * 
	 * @param address1
	 * @param address2
	 * @return */
	public static boolean sameAs( DigitalContactInfo address1, DigitalContactInfo address2 ) {
		return ( isAddressEmpty( address1 ) && isAddressEmpty( address2 ) )
				|| ( DigitalStringUtil.trimEqualsIgnoreCase( address1.getAddressType(), address2.getAddressType() ) && DigitalStringUtil.trimEqualsIgnoreCase( address1.getFirstName(), address2.getFirstName() )
						&& DigitalStringUtil.trimEqualsIgnoreCase( address1.getLastName(), address2.getLastName() ) && DigitalStringUtil.trimEqualsIgnoreCase( address1.getAddress1(), address2.getAddress1() )
						&& DigitalStringUtil.trimEqualsIgnoreCase( address1.getAddress2(), address2.getAddress2() ) && DigitalStringUtil.trimEqualsIgnoreCase( address1.getAddress3(), address2.getAddress3() )
						&& DigitalStringUtil.trimEqualsIgnoreCase( address1.getCity(), address2.getCity() ) && DigitalStringUtil.trimEqualsIgnoreCase( address1.getState(), address2.getState() )
						&& DigitalStringUtil.trimEqualsIgnoreCase( address1.getCountry(), address2.getCountry() ) && DigitalStringUtil.trimEqualsIgnoreCase( address1.getPostalCode(), address2.getPostalCode() ) && DigitalStringUtil.trimEqualsIgnoreCase(
						address1.getPhoneNumber(), address2.getPhoneNumber() ) );
	}

	protected void initializeLists() {

		for( int x = 0; x < placeLists.size(); x++ ) {
			if( !countryPlaceMap.containsKey( placeLists.get( x ) ) ) {
				if( isLoggingDebug() ) {
					logDebug( "Resolving component: " + placeLists.get( x ) );
				}
				PlaceList nucleusComponent = (PlaceList)resolveName( placeLists.get( x ) );
				countryPlaceMap.put( placeLists.get( x ), nucleusComponent );
			}
		}
	}

	public boolean isValidShippingState( String state ) {
		if( countryPlaceMap == null || countryPlaceMap.isEmpty() ) {
			initializeLists();
		}
		PlaceList places = (PlaceList)countryPlaceMap.get( this.getValidShippingPlaces() );
		return places.getPlaceForCode( state ) != null;
	}

	public String getCountryForPlace( String placeCode ) {
		if( countryPlaceMap.size() != placeLists.size() ) {
			initializeLists();
		}
		if( DigitalStringUtil.isNotBlank( placeCode ) ) {
			Iterator<PlaceList> i = countryPlaceMap.values().iterator();
			while( i.hasNext() ) {
				PlaceList placeList = i.next();
				if( placeList instanceof DigitalPlaceList ) {
					DigitalPlaceList countryPlaceList = (DigitalPlaceList)placeList;

					if( countryPlaceList.getPlaceForCode( placeCode ) != null ) { return countryPlaceList.getCountry(); }

				} else {
					if( isLoggingError() ) {
						logError( "ERORR: Place lists in DigitalAddressUtil must be of type DSWPlaceList but is of type: " + placeList.getClass() );
					}
				}
			}
		}
		return null;
	}

	/**
	 * Utilty method to check if the given address is empty / blank.
	 * 
	 * @param ContactInfo
	 * @return
	 */
	public boolean isEmptyAddress(ContactInfo address) {
		return (address == null
				|| Strings.isNullOrEmpty(address.getFirstName())
				|| Strings.isNullOrEmpty(address.getLastName())
				|| Strings.isNullOrEmpty(address.getAddress1())
				|| Strings.isNullOrEmpty(address.getCity())
				|| Strings.isNullOrEmpty(address.getState()) || Strings
					.isNullOrEmpty(address.getPostalCode()));
	}

	/**
	 * Utilty method to remove special characters from a given string input
	 * 
	 * @param String
	 * @return
	 */
	public String removeSpecialCharacters(String inString) {
		if (Strings.isNullOrEmpty(inString))
			return "";
		return inString.replaceAll("[-.+]", "");
	}
	
	public static DigitalContactInfo getDSWContactInfo(DigitalRepositoryContactInfo shippingAddress) {
		DigitalContactInfo address = new DigitalContactInfo();
		if(shippingAddress == null){
			return address;
		}
		
		address.setFirstName(shippingAddress.getFirstName());
		address.setLastName(shippingAddress.getLastName());
		address.setAddress1(shippingAddress.getAddress1());
		address.setAddress2(shippingAddress.getAddress2());
		address.setCity(shippingAddress.getCity());
		address.setCountry(shippingAddress.getCountry());
		address.setCounty(shippingAddress.getCounty());
		address.setEmail(shippingAddress.getEmail());
		address.setFaxNumber(shippingAddress.getFaxNumber());
		address.setPhoneNumber(shippingAddress.getPhoneNumber());
		address.setState(shippingAddress.getState());
		address.setPostalCode(shippingAddress.getPostalCode());
		address.setAddressType(shippingAddress.getAddressType());
		address.setAddressVerification(shippingAddress.getAddressVerification());
		if (MIL_ADDRESS_TYPE.equalsIgnoreCase(shippingAddress.getAddressType())) {
			address.setRank(shippingAddress.getRank());
			address.setRegion(shippingAddress.getRegion());
		}
		address.setIsPoBox(shippingAddress.getIsPoBox());
		address.setOwnerId(shippingAddress.getOwnerId());
		return address;
	}
	
	

}
