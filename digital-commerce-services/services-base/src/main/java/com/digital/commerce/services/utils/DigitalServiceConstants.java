package com.digital.commerce.services.utils;

import java.util.HashMap;
import java.util.Map;

import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.services.common.DigitalBaseGeoLocationUtil;

import atg.apache.lang.RandomStringUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalServiceConstants extends DigitalBaseConstants {

	private DigitalBaseGeoLocationUtil locationUtil;


	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllConstants() {
		Map<String, Object> map = super.getAllConstants();

		Map<String, Object> serviceAvailabilityMap = (Map<String, Object>) ((Map<String, Object>) map
				.get("dswEnvironmentSettings")).get("ServicesAvailability");

		Map<String, Object> bpsMap = new HashMap<>();
		if (!isBccPreviewEnabled()) 
		{
			bpsMap.put("BOPIS", locationUtil.isBOPISEnabled());
			bpsMap.put("BOSTS", locationUtil.isBOSTSEnabled());
		}
		else
		{
			bpsMap.put("BOPIS", false);
			bpsMap.put("BOSTS", false);
		}
		serviceAvailabilityMap.put("BOPIS/BOSTS", bpsMap);

		return map;
	}

	/**
	 * @return
	 */
	public String generateRandomPassword() {
		StringBuilder sb = new StringBuilder();
		//KTLO1-875:: SECURICON PEN TEST - Temp password predictable
		//sb.append(RandomStringUtils.random(10, 0, 0, true, true));
		final char mCharacterSet[] =getTemporaryPasswordNumericChars().toCharArray();
		int end = 0;
        if(mCharacterSet != null)
            end = mCharacterSet.length;
        	sb.append(RandomStringUtils.random(getTemporaryPasswordCharLength(), 0, end, false, false,mCharacterSet));
		return sb.toString();
	}

}
