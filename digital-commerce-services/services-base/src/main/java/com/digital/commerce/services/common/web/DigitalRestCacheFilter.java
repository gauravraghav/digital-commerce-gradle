package com.digital.commerce.services.common.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.services.common.DigitalRestCacheController;

public class DigitalRestCacheFilter implements Filter {
	private static final Logger LOGGER = Logger
			.getLogger(DigitalRestCacheFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		try {
			DigitalRestCacheController cacheContoller = (DigitalRestCacheController) ComponentLookupUtil
					.lookupComponent(ComponentLookupUtil.REST_CACHE_CONTROLLER);
			if (cacheContoller != null) {
				cacheContoller.addCacheHeaders((HttpServletRequest) request,
						(HttpServletResponse) response);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception while adding cache headers", ex);
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
