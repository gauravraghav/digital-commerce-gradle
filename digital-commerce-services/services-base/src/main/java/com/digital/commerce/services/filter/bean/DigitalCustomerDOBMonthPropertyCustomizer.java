package com.digital.commerce.services.filter.bean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;

/**
 * A Property customizer used to return a last four digits of users  credit card  number.
 *
 * @author Shiva
 */
public class DigitalCustomerDOBMonthPropertyCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {


  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   *
   * @throws BeanFilterException
   */
  @Override
  public Object getPropertyValue(Object pTargetObject, String pPropertyName,
      Map<String,String> pAttributes) throws BeanFilterException {

    // Get DOB.
    Object propValue = null;

    try {
      propValue = DynamicBeans.getPropertyValue(pTargetObject, pPropertyName);
    }
    catch (PropertyNotFoundException e) {
      throw new BeanFilterException(e);
    }

    // Return  null if we dont get a value.
    if (propValue == null){
    	vlogDebug("Property {0} was not a valid DOB: {1}", pPropertyName, propValue);
        return null;
    }
    	return this.extractMonthDay((Date)propValue);
  }
  
  private int extractMonthDay(Date propValue) {
	  SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
	  return Integer.parseInt(dateFormat.format(propValue));
  }
}
