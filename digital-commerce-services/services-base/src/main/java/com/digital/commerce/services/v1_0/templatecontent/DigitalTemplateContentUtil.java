package com.digital.commerce.services.v1_0.templatecontent;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.v1_0.templatecontent.domain.TemplateContent;
import com.digital.commerce.services.v1_0.templatecontent.domain.TemplateContentResponse;

import atg.adapter.gsa.query.Builder;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalTemplateContentUtil extends ApplicationLoggingImpl {
	private static final String CLASSNAME = "DigitalTemplateContentUtil";
	private MessageLocator messageLocator;
	private Repository templateRepository;
	private Properties contentType;
	

	public DigitalTemplateContentUtil() {
		super(DigitalTemplateContentUtil.class.getName());
	}
	
	public TemplateContentResponse getTemplateContent() {
		TemplateContentResponse response = new TemplateContentResponse();
		final String methodName = "getTemplateContent";
		
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
					CLASSNAME, methodName);
			response = getTemplateContentFromRepository();
		} catch (DigitalAppException e) {
			logError(e.getMessage());
			String msg = getMessageLocator().getMessageString(
					"template.content.generic.error");
			response.getGenericExceptions().add(
					new ResponseError("", msg + " - " + e.getMessage()));
			response.setFormError(true);
			response.setRequestSuccess(false);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
					CLASSNAME, methodName);
			
		}
		return response;
	}
	
	private TemplateContentResponse getTemplateContentFromRepository() throws DigitalAppException {
		TemplateContentResponse templateContentResponse = new TemplateContentResponse();
		try {
			StringBuilder query = new StringBuilder();
			query.append( "select * from dsw_tf_item item ,dsw_tf_marketing_slots ms where ms.marketing_slot_id= item.item_id " );
			Repository mutRepp = getTemplateRepository();
			RepositoryView view = mutRepp.getView("MarketingSlots");
			Builder builder = (Builder)view.getQueryBuilder();
			RepositoryItem[] riList = view.executeQuery( builder.createSqlPassthroughQuery(query.toString(), null ) );
			List<TemplateContent> templateContentList = new ArrayList<>();
		for(RepositoryItem ri : riList){
			TemplateContent templateContent = new TemplateContent(); 
			templateContent.setTemplateId((String)ri.getPropertyValue("itemId"));
			if(ri.getPropertyValue("rendererJSP") != null) {
				templateContent.setRendererJsp((String)ri.getPropertyValue("rendererJSP"));
			}
			if(ri.getPropertyValue("keyWords") != null) {
				templateContent.setKeywords((String)ri.getPropertyValue("keyWords"));
			}
			if(ri.getPropertyValue("name") != null) {
				templateContent.setName((String)ri.getPropertyValue("name"));
			}
			if(ri.getPropertyValue("slot") != null) {
				templateContent.setSlotContent((String)ri.getPropertyValue("slot"));
			}
			if(ri.getPropertyValue("howMany") != null) {
				templateContent.setCount((Integer)ri.getPropertyValue("howMany"));
			}
				templateContentList.add(templateContent);
		}
		templateContentResponse.setContentList(templateContentList);
		} catch (RepositoryException e) {
			if(isLoggingDebug()) {
				logDebug(e.getLocalizedMessage());
			}
			throw new DigitalAppException(e.getLocalizedMessage());
		}
		return templateContentResponse;
	}
}
