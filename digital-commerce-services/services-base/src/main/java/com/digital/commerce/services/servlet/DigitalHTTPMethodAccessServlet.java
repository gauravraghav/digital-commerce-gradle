package com.digital.commerce.services.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;

import atg.rest.servlet.RestPipelineServlet;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import lombok.Getter;
import lombok.Setter;

/**
 * @author HB391569 PipelineServlet to get the URI of the REST service against
 *         the set of configured set of rest URI to make sure its being accessed
 *         ONLY via POST. Though technically any REST service can be accessed
 *         via GET as well, we would like to be compliant on security to avoid
 *         sending any PII via GET. Hence we are disabling the access of defined
 *         set of REST services via GET using this custom pipeline servlet
 */
@Getter
@Setter
public class DigitalHTTPMethodAccessServlet extends RestPipelineServlet {
	boolean enabled = false;
	private List<String> postURIList = new ArrayList<>();
	private static final String PERF_NAME = "service";
	private static final String PERFORM_MONITOR_NAME = "DigitalHTTPMethodAccessServlet";
	/*
	 * (non-Javadoc)
	 * 
	 * @see atg.rest.servlet.RestPipelineServlet#service(atg.servlet.
	 * DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws IOException,
			ServletException {
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, PERF_NAME);
			if (isLoggingDebug()) {
				logDebug("The DigitalHTTPPostAccessServlet enabled: " + enabled);
			}
			if (!enabled) {
				passRequest(pRequest, pResponse);
				return;
			}
			if (!allowAccess(pRequest)) {
				String uri = pRequest.getRequestURI();
				pResponse.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED,
						"The service " + uri
								+ " need to be accessed via POST method only");
				return;
			}
			passRequest(pRequest, pResponse);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, PERF_NAME);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	private boolean allowAccess(DynamoHttpServletRequest request) {
		try {
			String uri = request.getRequestURI();
			String httpMethod = request.getMethod();
			if (isLoggingDebug()) {
				logDebug("Uri from the request: " + uri + ", the http method: "
						+ httpMethod);
			}
			// if the request is not for rest service calls move on
			if (uri != null && !uri.contains("/rest/model/")) {
				if (isLoggingDebug()) {
					logDebug("URL does not contain /rest/model/, hence not doing any check for HTTP access");
				}
				return true;
			} else {
				// if the rest service is not allowed for post
				if (postURIList.contains(uri)
						&& !(httpMethod.equals("POST"))) {
					return false;
				} else {
					if (isLoggingDebug()) {
						logDebug("This REST service can be accessed via GET");
					}
				}
			}
		} catch (Exception ex) {
			// world is not ending if we reach here, log error and move on
			logError("Error while checking the access", ex);
		}
		return true;
	}
}
