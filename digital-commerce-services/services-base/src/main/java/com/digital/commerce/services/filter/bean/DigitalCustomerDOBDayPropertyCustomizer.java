package com.digital.commerce.services.filter.bean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;

/**
 *
 * @author Manju
 */
public class DigitalCustomerDOBDayPropertyCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {


  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   *
   * @throws BeanFilterException
   */
  @Override
  public Object getPropertyValue(Object pTargetObject, String pPropertyName,
      Map<String,String> pAttributes) throws BeanFilterException {

    // Get DOB.
    Object propValue = null;

    try {
      propValue = DynamicBeans.getPropertyValue(pTargetObject, pPropertyName);
    }
    catch (PropertyNotFoundException e) {
      throw new BeanFilterException(e);
    }

    // Return  null if we dont get a value.
    if (propValue == null){
    	vlogDebug("Property {0} was not a valid DOB: {1}", pPropertyName, propValue);
        return null;
    }
    	return this.extractDayOfMonth((Date)propValue);
  }
  
  private int extractDayOfMonth(Date propValue) {
	  SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
	  return Integer.parseInt(dateFormat.format(propValue));
  }
}
