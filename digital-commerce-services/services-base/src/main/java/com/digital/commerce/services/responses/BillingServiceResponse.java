package com.digital.commerce.services.responses;

import java.util.ArrayList;
import java.util.List;

import com.digital.commerce.services.common.DigitalContactInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillingServiceResponse {
    private String result="success";
    private  List<String> errors= new ArrayList<>();
    private String  decison; 
    private DigitalContactInfo suggestedAddress;
    private DigitalContactInfo address;

}
