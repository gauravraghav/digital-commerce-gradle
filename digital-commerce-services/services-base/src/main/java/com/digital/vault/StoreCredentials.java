package com.digital.vault;

import atg.nucleus.GenericService;
import atg.security.opss.csf.CredentialProperties;
import atg.security.opss.csf.CredentialStoreManager;
import atg.security.opss.csf.GenericCredentialProperties;
import atg.security.opss.csf.LoginCredentialProperties;

public class StoreCredentials extends GenericService {

    private CredentialStoreManager credentialStoreManager;

    private String map;

    private String key;

    private String login;

    private String password;

    private String secretData;

	public String getMap() {
		return map;
	}

	public void setMap(String map) {
		this.map = map;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecretData() {
		return secretData;
	}

	public void setSecretData(String secretData) {
		this.secretData = secretData;
	}

	/**
     * @return the credentialStoreManager
     */
    public CredentialStoreManager getCredentialStoreManager() {
	return this.credentialStoreManager;
    }

    /**
     * @param pCredentialStoreManager
     *            the credentialStoreManager to set
     */
    public void setCredentialStoreManager(CredentialStoreManager pCredentialStoreManager) {
	this.credentialStoreManager = pCredentialStoreManager;
    }

    public void storeCredentials() {

	CredentialProperties props = new LoginCredentialProperties();
	((LoginCredentialProperties) props).setUsername(getLogin());
	((LoginCredentialProperties) props).setPassword(getPassword());
	try {
	    this.credentialStoreManager.storeCredential(getMap(), getKey(), props);
	    vlogDebug("credentials stored successfully");
	} catch (Exception e) {
	    vlogError("error while storing credentials");
	}

    }
    
    public void storeGenericCredentials() {
    	GenericCredentialProperties props = new GenericCredentialProperties();
    	props.setSingleCredential(getSecretData());
    	
    	try {
    	    this.credentialStoreManager.storeCredential(getMap(), getKey(), props);
    	    vlogDebug("credentials stored successfully");
    	} catch (Exception e) {
    	    vlogError("error while storing credentials");
    	}
    }

    public void deleteCredential() {

	try {
	    this.credentialStoreManager.deleteCredential(getMap(), getKey());
	    vlogDebug("credentials deleted successfully");
	} catch (Exception e) {
	    vlogError("error while deleting credentials");
	}

    }

    public void retrieveCredential() {

	try {
	    CredentialProperties props;
	    props = this.credentialStoreManager.retrieveCredentialProperties(getMap(), getKey());
	    vlogDebug("credentials retrieved successfully");
	    System.out
		    .println("password	::	" + (((LoginCredentialProperties) props).getPassword()).toString());
	} catch (Exception e) {
	    vlogError("error while retrieving credentials" + e);
	}

    }
    
    public void retrieveGenericCredential() {

    	try {
    	    CredentialProperties props;
    	    props = this.credentialStoreManager.retrieveCredentialProperties(getMap(), getKey());
    	    vlogDebug("credentials retrieved successfully");
    	    System.out
    		    .println("password	::	" + (((GenericCredentialProperties) props).getSingleCredential()).toString());
    	} catch (Exception e) {
    	    vlogError("error while retrieving credentials" + e);
    	}

        }

}