package com.digital.commerce.services.order;

import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;

/**
 * This enumeration identifies the origin of the order submission, whether its Mobile, the Contact Center
 * the Tablet, the Mobile App or the Full Site.
 */
@Getter
public enum OriginOfOrder {

    Default("default"),
    ScheduledOrder("scheduledOrder"),
    ContactCenter("contactCenter"),
    Desktop("default"),
    Mobile("mobile"),
    Tablet("tablet"),
    AndroidApp("androidapp"),
    IOSApp("iosapp"),;

    private final String value;

    /**
     * @param value
     */
    private OriginOfOrder(final String value) {
        this.value = value;
    }

    /**
     * @param value
     * @return OriginOfOrder
     */
    public static OriginOfOrder valueFor(String value) {
        OriginOfOrder retVal = null;
        for (final OriginOfOrder origin : OriginOfOrder.values()) {
            if (DigitalStringUtil.equalsIgnoreCase(origin.getValue(), value)) {
                retVal = origin;
            }
        }
        return retVal;
    }
}
