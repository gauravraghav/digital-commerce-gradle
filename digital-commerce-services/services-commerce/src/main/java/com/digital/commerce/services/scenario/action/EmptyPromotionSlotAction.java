package com.digital.commerce.services.scenario.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.ComponentLookupUtil;

import atg.commerce.promotion.PromotionGrantedMessage;
import atg.commerce.promotion.PromotionRevokedMessage;
import atg.process.ProcessException;
import atg.process.ProcessExecutionContext;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.scenario.action.EmptySlot;
import atg.scenario.targeting.RepositoryItemSlot;
import atg.scenario.targeting.Slot;
@SuppressWarnings({"rawtypes","unchecked"})
public class EmptyPromotionSlotAction extends EmptySlot {

	public String getActionName() {
		return "emptyPromotionSlot";
	}
	
	public void initialize(Map pParameters) throws ProcessException {
		storeRequiredParameter(pParameters, "slot", String.class);
	}

	
	public Slot getSlot(ProcessExecutionContext pContext)
		    throws ProcessException
	{
		    String slot = (String)getParameterValue("slot", pContext);
    return ComponentLookupUtil.lookupComponent(slot, Slot.class);
	 }

	public Collection getValues(Slot pSlot, ProcessExecutionContext pContext)
			throws ProcessException {
		String[] repositoryIds = new String[1];
		if(pContext.getMessage() instanceof PromotionGrantedMessage){
			PromotionGrantedMessage message = (PromotionGrantedMessage) pContext
					.getMessage();
			repositoryIds[0] = message.getPromotionId();
			
		} else {
			PromotionRevokedMessage message = (PromotionRevokedMessage) pContext
					.getMessage();
			repositoryIds[0] = message.getPromotionId();
		}
		if (repositoryIds != null) {
			RepositoryItemSlot slot = (RepositoryItemSlot) pSlot;
			try {
				RepositoryItem[] items = slot.getRepository().getItems(
						repositoryIds, slot.getItemDescriptorName());
				if (items != null) {
					List itemList = new ArrayList(items.length);
					for (int i = 0; i < items.length; i++) {
						if (items[i] != null) {
							itemList.add(items[i]);
						}
					}
					return itemList;
				}
				return Collections.EMPTY_LIST;
			} catch (RepositoryException e) {

			}
		}
		return null;
	}

}
