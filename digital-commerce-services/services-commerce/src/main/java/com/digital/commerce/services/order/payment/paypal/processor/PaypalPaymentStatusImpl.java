package com.digital.commerce.services.order.payment.paypal.processor;

import java.util.Date;

import atg.payment.PaymentStatusImpl;
import lombok.Getter;
import lombok.Setter;

/**
 * Auth Code for paypal transactions.
 * 
 */
@Getter
@Setter
public class PaypalPaymentStatusImpl extends PaymentStatusImpl implements
		PaypalPaymentStatus {
	private static final long serialVersionUID = 1L;

	private String authorizationCode;

	public PaypalPaymentStatusImpl() {

	}

	public PaypalPaymentStatusImpl(String pTransactionId, double pAmount,
			boolean pTransactionSuccess, String pErrorMessage,
			Date pTransactionTimestamp, String pAuthorizationCode) {
		super(pTransactionId, pAmount, pTransactionSuccess, pErrorMessage,
				pTransactionTimestamp);
		this.authorizationCode = pAuthorizationCode;
	}
}
