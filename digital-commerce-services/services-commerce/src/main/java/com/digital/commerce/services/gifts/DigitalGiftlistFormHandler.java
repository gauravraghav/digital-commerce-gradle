package com.digital.commerce.services.gifts;

import atg.commerce.CommerceException;
import atg.commerce.gifts.GiftlistFormHandler;
import atg.commerce.gifts.GiftlistManager;
import atg.commerce.gifts.InvalidDateException;
import atg.commerce.gifts.InvalidGiftQuantityException;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.dtm.UserTransactionDemarcation;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import lombok.Getter;
import lombok.Setter;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.DigitalCommerceItem;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalGiftlistFormHandler extends GiftlistFormHandler {
	protected static final String CLASSNAME = DigitalGiftlistFormHandler.class.getName();
	
	private static final DigitalLogger logger = DigitalLogger
			.getLogger(DigitalGiftlistFormHandler.class);

	private static final String MSG_WISHLIST_REQUEST_FORMAT_ERROR = "wishList.additem.request_format_error";
	private static final String MSG_WISHLIST_EXCEED_LIMIT = "wishList.additem.exceedlimit";
	private static final String MSG_WISHLIST_SKU_PRODUCT_NOTMATCH = "wishList.additem.skuproduct_notmatch";
	private RepeatingRequestMonitor repeatingRequestMonitor;

	// private String[] productIds;

	private boolean wishList = false;
	private boolean removeAll = false;
	private boolean moveFromPreviouseCart = false;

	private Object[] itemList;
	private String itemListStr;
	
	private MessageLocator messageLocator;

	public boolean handleAddItemToGiftlist(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		final String METHOD_NAME = "DigitalGiftlistFormHandler.addItemToGiftlist";
		UserTransactionDemarcation td = null;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		
		if ((rrm == null) || (rrm.isUniqueRequestEntry(METHOD_NAME))) {

			try {

				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
						this.getName(), METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				
				checkGiftlistAccess();
				if (!checkFormRedirect(null, getAddItemToGiftlistErrorURL(),
						pRequest, pResponse)) {
					return false;
				}

				if (DigitalStringUtil.isBlank(this.getItemListStr())) {
					throw new CommerceException("Can not add empty items");
				}

				final JSONArray itemsArray = new JSONArray(
						this.getItemListStr());
				
				HashMap<String, JSONObject> data = ((DigitalGiftlistManager)this.getGiftlistManager()).processRequestData(itemsArray, this.getProfile());
				
				for(Map.Entry<String, JSONObject> entry : data.entrySet()){
					JSONObject item = entry.getValue();
					this.setProductId(item.getString("productId"));
					if (!DigitalStringUtil.isBlank(item.getString("sku")))
						this.setCatalogRefIds(StringUtils
								.splitStringAtCharacter(
										item.getString("sku"),
										','));
					else
						this.setCatalogRefIds(null);
					
					this.setQuantity(item.getLong("quantity"));
					
					preAddItemToGiftlist(pRequest, pResponse);
					if (!this.getFormError() &&validateGiftlistId(pRequest, pResponse)) {
						if (!checkFormRedirect(null,
								getAddItemToGiftlistErrorURL(), pRequest,
								pResponse)) {
							return false;
						}
						addItemToGiftlist(pRequest, pResponse);
						if (!checkFormRedirect(null,
								getAddItemToGiftlistErrorURL(), pRequest,
								pResponse)) {
							return false;
						}
						postAddItemToGiftlist(pRequest, pResponse);
					}

				}
			} catch (InvalidGiftQuantityException exc) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					processException(exc, "quantityLessThanOrEqualToZero",
							pRequest, pResponse);
			} catch (CommerceException oce) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					if(oce.getMessage().equals(DigitalGiftlistManager.ITEM_EXCEED_LIMIT)){
						addFormException( new DropletException( this.getMessageLocator().getMessageString(MSG_WISHLIST_EXCEED_LIMIT, new Object[] { ((DigitalGiftlistManager)this.getGiftlistManager()).getWishlistItemLimit() }) ) );
					}
	                if(oce.getMessage().equals(DigitalGiftlistManager.SKU_PRODUCT_NOT_MATCH)){
	                	addFormException( new DropletException( this.getMessageLocator().getMessageString( MSG_WISHLIST_SKU_PRODUCT_NOTMATCH) ) );
					}
					processException(oce, "errorAddingToGiftlist", pRequest, pResponse);
			} catch (JSONException jse) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					addFormException( new DropletException( this.getMessageLocator().getMessageString( MSG_WISHLIST_REQUEST_FORMAT_ERROR) ) );
			} catch (RepositoryException re) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					processException(re, "errorAddingToGiftlist", pRequest,
							pResponse);
			} catch(Exception ex){
					logError("errorAddingToGiftlist",ex);
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					processException(ex, "errorAddingToGiftlist", pRequest,
							pResponse);
			}finally {
					if (rrm != null) {
						rrm.removeRequestEntry(METHOD_NAME);
					}
					TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
					DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
		return checkFormRedirect(getAddItemToGiftlistSuccessURL(),
				getAddItemToGiftlistErrorURL(), pRequest, pResponse);
	}

	/*
	 * public String[] getProductIds() { return productIds; }
	 * 
	 * public void setProductIds(String[] productIds) { this.productIds =
	 * productIds; }
	 */
	protected void addItemToGiftlist(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		logger.debug("Inside addItemToGiftlist");
		if (getCatalogRefIds() != null && getCatalogRefIds().length > 0) {
			logger.debug("Inside addItemToGiftlist getCatalogRefIds().lengthL:: "
					+ getCatalogRefIds().length);
			super.addItemToGiftlist(pRequest, pResponse);
			return;
		}

		GiftlistManager mgr = getGiftlistManager();

		long quantity = getQuantity();
		if (quantity == 0L) {
			quantity = getQuantity(null, pRequest, pResponse);
		}

		mgr.addCatalogItemToGiftlist(null, getProductId(), getGiftlistId(),
				getSiteId(), quantity);

	}

	public void preAddItemToGiftlist(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		logger.debug("Inside preAddItemToGiftlist");

		try {
			if (getCatalogRefIds() == null
					&& DigitalStringUtil.isEmpty(getProductId())) {
				logger.debug("Inside preAddItemToGiftlist getProductId() is null");
				throw new ServletException("Got null Sku and Produtid ");

			} else if (DigitalStringUtil.isNotEmpty(getProductId())) {

					RepositoryItem product = getCatalogTools().findProduct(
							getProductId());
					if (product == null) {
						logger.debug("Inside preAddItemToGiftlist product id is invalid product:: "
								+ product);

						// addFormException("invalidGiftlistQuantity", pRequest,
						// pResponse);
						String msg = formatUserMessage(
								"invalidGiftlistQuantity", pRequest, pResponse);
						/*
						 * String msgsec =
						 * formatUserMessage("quantityLessThanOrEqualToZero",
						 * pRequest, pResponse);
						 */
						addFormException(new DropletException(msg,
								"invalidGiftlistQuantity"));
						/*
						 * can thow multiple exceptions example to be deleted
						 * later addFormException(new DropletException(msg,
						 * "quantityLessThanOrEqualToZero"));
						 */
						return;
					}
				} else {

				}
			
		} catch (RepositoryException e) {
			throw new ServletException(e);
		}

	}

	public String saveGiftlist(String pProfileId) throws CommerceException,
			InvalidDateException {
		if (!this.isWishList())
			return getGiftlistManager().createGiftlist(pProfileId,
					getIsPublished().booleanValue(), getEventName(),
					getEventDate(), getEventType(), getDescription(),
					getComments(), getShippingAddressId(), getInstructions(),
					getSiteId());
		else {

			if (!this.getProfile().isTransient())
				return ((DigitalGiftlistManager) getGiftlistManager())
						.createWishlist(this.getProfile());
			// else
			// return
			// ((DigitalGiftlistManager)getGiftlistManager()).createWishlist(this.getProfile());
		}

		return null;

	}

	protected void updateGiftlistItems(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (((DigitalGiftlistManager) getGiftlistManager()).updatePermitted(
				getGiftlistId(), this.getProfile().getRepositoryId())) {
			if (this.removeAll) {
				List items = getGiftlistManager().getGiftlistItems(
						getGiftlistId());
				if (items != null) {
					String[] removeIds = new String[items.size()];
					Iterator<RepositoryItem> iter = items.iterator();
					int i = 0;
					while (iter.hasNext()) {
						removeIds[i] = ((RepositoryItem) iter.next())
								.getRepositoryId();
						i++;
					}
					this.setRemoveGiftitemIds(removeIds);
				}
			}
			//pRequest.setParameter("", pValue);
			super.updateGiftlistItems(pRequest, pResponse);
		}
	}
	
	public void preMoveItemsFromCart(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		try {
			if (getItemIds() == null || getItemIds().length < 1) {
				addFormException("errorAddingToGiftlist", pRequest, pResponse);
				return;
			}
			int skuToBeAddedCount = 0;
			DigitalGiftlistManager mgr = (DigitalGiftlistManager) getGiftlistManager();
			for (String itemId : this.getItemIds()) {
				DigitalCommerceItem item = (DigitalCommerceItem)getOrder().getCommerceItem(itemId);
				String skuId = item.getCatalogRefId();
				String productId = item.getAuxiliaryData().getProductId();
				String giftId = mgr.getGiftlistItemId(getGiftlistId(), skuId,
						productId, null);
				if (giftId == null) {
					skuToBeAddedCount++;
				}
				if (this.isMoveFromPreviouseCart()) {
					pRequest.setParameter(itemId, item.getQuantity()-item.getMergedQuantity());
				}
			}

			List existingList = mgr.getGiftlistItems(getGiftlistId());
			if (skuToBeAddedCount
					+ (existingList == null ? 0 : existingList.size()) > mgr
						.getWishlistItemLimit()) {
				addFormException(new DropletException(this.getMessageLocator()
						.getMessageString(
								MSG_WISHLIST_EXCEED_LIMIT,
								new Object[] { ((DigitalGiftlistManager) this
										.getGiftlistManager())
										.getWishlistItemLimit() })));
			}
		} catch (Exception ex) {
			logger.error(ex);
			addFormException("errorAddingToGiftlist", pRequest, pResponse);
		}
	}
	
	public long getQuantity(String id,
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		long quantity = super.getQuantity(id, pRequest, pResponse);
		if(quantity == 0){
			return this.getGiftlistManager().getGiftlistItemQuantityDesired(id);
		} else {
			return quantity;
		}
	}


	/**
	 * @param request
	 * @param response
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 * @throws CommerceException
	 */
	public boolean handleMoveItemsFromCart(DynamoHttpServletRequest request,
			DynamoHttpServletResponse response) throws ServletException,
			IOException, CommerceException {

		final String METHOD_NAME = "DigitalGiftlistFormHandler.moveItemsFromCart";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(METHOD_NAME))) {
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
						this.getName(), METHOD_NAME);
				return super.handleMoveItemsFromCart(request, response);
			} catch (Exception e) {
				if (getFormError()) {
					return false;
				} else {
					logger.error(e);
					addFormException("errorMovingItemsFromCart", request, response);
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
						this.getName(), METHOD_NAME);
			}
		}

		return checkFormRedirect(getMoveItemsFromCartSuccessURL(), getMoveItemsFromCartErrorURL(),
				request, response);

	}
	
	@Override
	public boolean beforeSet(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws DropletFormException{
       	TransactionUtils.acquireTransactionLock(CLASSNAME,"GenericGiftListMethod - beforeSet");
		return super.beforeSet(pRequest, pResponse);
	}
	
	@Override
	public boolean afterSet(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws DropletFormException{
		boolean flag = false;
		try{
			flag = super.afterSet(pRequest, pResponse);
		}finally{
			TransactionUtils.releaseTransactionLock(CLASSNAME, "GenericGiftListMethod - afterSet");
		}
		return flag;
	}
}