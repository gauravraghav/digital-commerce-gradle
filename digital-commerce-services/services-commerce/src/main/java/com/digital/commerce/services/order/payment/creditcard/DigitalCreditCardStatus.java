/**
 * 
 */
package com.digital.commerce.services.order.payment.creditcard;

import atg.payment.creditcard.CreditCardStatusImpl;
import lombok.Getter;
import lombok.Setter;

/** @author djboyd */
@Getter
@Setter
public class DigitalCreditCardStatus extends CreditCardStatusImpl {

	private static final long	serialVersionUID	= 4420049399773906357L;
	private String				fraudResultCode;
	private String				fraudResultCodeDescription;
	private String				fraudRuleDescription;
	private String				fraudRule;
	private String				fraudScore;
	private String				fraudFlag;
	private String				authorizationCode;
	private String				errorNo;
	private String				errorDesc;
	private String				paResStatus;
	private String				signatureVerification;
	private String				cavv;
	private String				eciFlag;
	private String				xid;
	private String				enrolled;
	private String				status;
	private String				payerSecurityLevel;
	private String              threatMetrixId;
	private String 				tokenValue;


	public DigitalCreditCardStatus() {}

	/** @param fraudResultCode
	 * @param fraudResultCodeDescription
	 * @param fraudRuleDescription
	 * @param fraudRule
	 * @param fraudScore
	 * @param fraudFlag */
	public DigitalCreditCardStatus( String fraudResultCode, String fraudResultCodeDescription, String fraudRuleDescription, String fraudRule, String fraudScore, String fraudFlag,String threatMetrixId ) {
		this.fraudResultCode = fraudResultCode;
		this.fraudResultCodeDescription = fraudResultCodeDescription;
		this.fraudRuleDescription = fraudRuleDescription;
		this.fraudRule = fraudRule;
		this.fraudScore = fraudScore;
		this.fraudFlag = fraudFlag;
		this.threatMetrixId=threatMetrixId;
	}
}
