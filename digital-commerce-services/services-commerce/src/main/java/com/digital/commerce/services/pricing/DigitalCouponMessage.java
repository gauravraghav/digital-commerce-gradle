package com.digital.commerce.services.pricing;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
public class DigitalCouponMessage {

  public Set<String> getExcludes() {
    if (excludes == null) {
      excludes = new HashSet<>();
    }
    return excludes;
  }

  private String couponCode;
  private String offerDetailsPage;
  private Set<String> excludes;
  private boolean isPromoApplied;
  private String errorsCode;
  private String promoId;

  public void clearBean() {
    couponCode = null;
    offerDetailsPage = null;
    excludes = null;
    isPromoApplied = false;
    promoId = null;
    errorsCode = null;
    excludes = null;
  }

  public Map<String, Object> displayMessage() {
    Map<String, Object> promoMessage = new HashMap<String, Object>();
    Set<String> finalExcludes = new HashSet();
    promoMessage.put("couponCode", getCouponCode());
    promoMessage.put("offerDetailsPage", getOfferDetailsPage());
    promoMessage.put("isPromoApplied", isPromoApplied());
    promoMessage.put("message_type", "info");
    promoMessage.put("errorsCode", getErrorsCode());
    promoMessage.put("promoId", getPromoId());

    if (getExcludes() != null && getExcludes().size() > 0) {
      Iterator<String> it = getExcludes().iterator();
      while (it.hasNext()) {
        finalExcludes.add(it.next());
      }

    }
    promoMessage.put("excludes", finalExcludes);
    return promoMessage;
  }
}
