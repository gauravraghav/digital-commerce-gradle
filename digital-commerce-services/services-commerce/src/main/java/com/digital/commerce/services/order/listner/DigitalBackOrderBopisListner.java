package com.digital.commerce.services.order.listner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.payload.BackorderBopisPayload;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderlineAttributesDTO;

public class DigitalBackOrderBopisListner extends DigitalOrderCancellationListner {
	protected static final String CLASSNAME = DigitalBackOrderBopisListner.class.getName();

	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

	@Override
	protected String getOrderNo(OrderStatusPayload payLoad) {
		if(payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null ){
			return payLoad.getOmniOrderUpdate().getOrder().getOrderNo();
		}
		return null;
	}

	@Override
	protected String getEmailTo(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((BackorderBopisPayload)payLoad).getPersonInfoBillTo().getEmailId();
	}

	@Override
	protected List<OrderLine> getOrderLines(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((BackorderBopisPayload)payLoad).getOrderLines();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected HashMap<String, Object> handleEmailNotification(
			OrderStatusPayload payLoad,
			DigitalOrderImpl order,
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {
		
		HashMap<String, Object> extraAttributes = new HashMap<>();
		
		BackorderBopisPayload orderCancelleddPayload = (BackorderBopisPayload)payLoad;
	    
	    extraAttributes.put("pickupPerson",orderCancelleddPayload.getPersonInfoBillTo().getFirstName()+
				" "+orderCancelleddPayload.getPersonInfoBillTo().getLastName());
		
	    String customerFirstName = orderCancelleddPayload.getPersonInfoBillTo().getFirstName();

		if (DigitalStringUtil.isEmpty(customerFirstName)) {
			customerFirstName = orderCancelleddPayload.getPersonInfoShipTo().getFirstName();
		}
		
	    extraAttributes.put("customerFirstName", customerFirstName);
	    
	    extraAttributes.put("shippingAddress",orderCancelleddPayload.getPersonInfoBillTo());
		
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOPIS, itemsByFullfilmentType, extraAttributes);
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOSTS, itemsByFullfilmentType, extraAttributes);
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.SHIP, itemsByFullfilmentType, extraAttributes);
		
		//get mall plaza name
		HashMap<String, OrderlineAttributesDTO> lines = (HashMap<String, OrderlineAttributesDTO>)extraAttributes.get("orderLines");
		
		if(lines != null){
			Map.Entry<String,OrderlineAttributesDTO> entry=lines.entrySet().iterator().next();
			extraAttributes.put("mallPlazaName",entry.getValue().getMallPlazaName());
		}
		
		return extraAttributes;
		
		
	}
	
}
