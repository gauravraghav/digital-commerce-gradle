package com.digital.commerce.services.digitalvisa;

import atg.nucleus.logging.ApplicationLoggingImpl;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.crypto.DigitalVaultUtil;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdsAuthCodeCalculator extends ApplicationLoggingImpl {
	public AdsAuthCodeCalculator() {
		super(AdsAuthCodeCalculator.class.getName());
	}
	private boolean encryptPayload;
	private DigitalServiceConstants dswConstants;
	private AdsRequestSigner adsRequestSigner;
	private MessageLocator messageLocator;

	/**
	 * @param authRequest
	 * @return
	 * @throws DigitalAppException
	 * Hashes the string out of loyalty member data using SHA256 and a secret key provided by Ads 
	 */
	public String calculate(AdsAuthRequest authRequest) throws DigitalAppException {
		
		if (isLoggingInfo()) {
			logInfo("Attempting calculate the auth code with encryptPayload: "
					+ encryptPayload);
		}
		String data = this.convert(authRequest);
		if (isLoggingDebug()) {
			logDebug("Data being hashed: " + data);
		}
		return adsRequestSigner.sign(data, this.getAdsSecretkey());
	}

	/**
	 * @return
	 * @throws DigitalAppException
	 * Read the plain text Ads secret key from Vault or DSWConstants
	 */
	private String getAdsSecretkey() throws DigitalAppException {
		String retValue = null;
		try {
			if (encryptPayload) {
				//non-local environments
				retValue = DigitalVaultUtil.getDecryptedString(dswConstants
						.getAdsSecretKey());
				if (isLoggingDebug()) {
					logDebug("Ads secret key from vaulty");
				}
			} else {
				//for local environments
				retValue = dswConstants.getAdsSecretKey();
				if (isLoggingDebug()) {
					logDebug("Ads secret key plain text, may be local environment");
				}
			}
			// Somehow the key is null for a given environment
			if (DigitalStringUtil.isBlank(retValue)) {
				if (isLoggingDebug()) {
					logDebug("Ads secret key null, some problem");
				}
				throw new DigitalAppException(
						AdsConstants.ADS_AUTH_NULL_SECRET_KEY,
						messageLocator
								.getMessageString(AdsConstants.ADS_AUTH_NULL_SECRET_KEY),
						null);
			}
		} catch (DigitalAppException e) {
			// possibly the vault is messed up for the given environment
			throw e;
		}
		return retValue;
	}
	
	/**
	 * @param authResponse
	 * @return
	 */
	private  String convert( AdsAuthRequest authResponse ) {
		StringBuilder buf = new StringBuilder();
		addToBuf( buf, "FirstName", authResponse.getFirstName() );
		addToBuf( buf, "LastName", authResponse.getLastName() );
		addToBuf( buf, "Address", authResponse.getAddress() );
		addToBuf( buf, "City", authResponse.getCity() );
		addToBuf( buf, "State", authResponse.getState() );
		addToBuf( buf, "Zip", authResponse.getZip() );
		addToBuf( buf, "Email", authResponse.getEmail() );
		addToBuf( buf, "PhoneNumber", authResponse.getPhoneNumber() );
		addToBuf( buf, "MemberNumber", authResponse.getMemberNumber() );
		if(isLoggingDebug()) {
			logDebug("Converted String: " + buf.toString());
		}
		return buf.toString();
	}

	/**
	 * @param buf
	 * @param name
	 * @param value
	 */
	private void addToBuf( StringBuilder buf, String name, String value ) {
		if( buf.length() > 0 ) {
			buf.append( "&" );
		}
		buf.append( name ).append( "=" ).append( value );
	}

}
