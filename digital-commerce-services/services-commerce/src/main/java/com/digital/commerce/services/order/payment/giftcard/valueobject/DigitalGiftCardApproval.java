/**
 * 
 */
package com.digital.commerce.services.order.payment.giftcard.valueobject;

import java.io.Serializable;
import java.util.Calendar;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.builder.HashCodeBuilder;

/** Class holds the dsw gift card information. default the approval flag to false along with setting the approval amount to 0
 * 
*/
@Getter
@Setter
@ToString
public class DigitalGiftCardApproval implements Serializable {

	/**
     * 
     */
	private static final long	serialVersionUID	= 795275074138019072L;

	/* flag indicates if the gift card is approved for this transaction. for gift card balances, it is set to true,
	 * for pre-auth all depends the return code that comes back */
	private boolean				approvalFlag		= false;

	private Calendar			authTimeStamp;

	/* amount the card is approved for. for balance checks it will be the balance, for pre auth it will be the amount charged */
	private double				approvalAmount		= 0.0;

	/* this will be the response message or code that comes back from doing a balance check, pre auth, etc. */
	private String				responseCode;
	private String transactionId;
	
	private String				statusCode;
	private String				authorizationCode;

	private String				statusMessage;

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode( this );
	}
}
