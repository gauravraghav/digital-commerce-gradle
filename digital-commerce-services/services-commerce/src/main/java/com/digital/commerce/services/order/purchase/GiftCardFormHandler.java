package com.digital.commerce.services.order.purchase;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.Address;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.common.CountryCode;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.order.GiftCardCommerceItem;
import com.digital.commerce.services.order.GiftCardDTO;
import com.digital.commerce.services.profile.DigitalProfileTools;

import atg.commerce.CommerceException;
import atg.commerce.fulfillment.OrderFulfillmentTools;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.ElectronicShippingGroup;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.OrderTools;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupManager;
import atg.commerce.order.purchase.PurchaseProcessConfiguration;
import atg.commerce.pricing.PricingTools;
import atg.commerce.states.ShippingGroupStates;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.dtm.UserTransactionDemarcation;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

/**
 * Adds gift cards from Arroweye, SVS to the shopping cart.
 * 
 * @author Manju Krishnappa
 */
@SuppressWarnings({"unchecked"})
@Getter
@Setter
public class GiftCardFormHandler extends GenericFormHandler {
	
	private static final String CLASSNAME = GiftCardFormHandler.class.getName();
	
	private static final String		EXCEEDED_SKU_LIMIT			= "exceededSkuLimit";

	private static final String		EXCEEDED_ITEM_IN_CART_LIMIT	= "exceededItemInCartLimit";

	private static final String		EXCEEDED_PURCHASE_LIMIT		= "exceededPurchaseLimit";
	
	
	protected GiftCardDTO[] giftCardDTOList;

	int addItemCount;
	
	protected String standardGiftCardType;

	protected String personalizedGiftCardType;

	protected String eGiftCardType;

	protected String asiStandardGiftCardType;

	protected String asiPersonalizedGiftCardType;

	protected String asiEGiftCardType;

	protected PurchaseProcessConfiguration configuration;
	
	protected TransactionManager transactionManager;

	protected OrderManager orderManager;
	
	protected Repository repository;
	
	protected OrderFulfillmentTools orderFulfillmentTools;

	protected Profile profile;
	
	protected MessageLocator messageLocator;
	
	protected PricingTools pricingTools;
	
	protected String addGiftCardItemSuccessUrl;

	protected String addGiftCardItemErrorUrl;
	
	protected String cartPurchaseLimitQueryParam;
	
	protected String cartItemLimitQueryParam;
	
	protected String cartItemQtyLimitQueryParam;

	public boolean handleAddGiftCardItemToOrder( DynamoHttpServletRequest request, DynamoHttpServletResponse response ) 
			throws ServletException, IOException {
		final String METHOD_NAME = "handleAddGiftCardItemToOrder";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			if(this.validateAddGiftCardItemToOrder()){
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				final UserTransactionDemarcation td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				
				try {
					for(GiftCardDTO giftCardDTO : giftCardDTOList){
						addGiftCardToShoppingCart(giftCardDTO);
					}
				} catch (Exception e) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					if (isLoggingError()) {				
						logError("Caught Exception when trying to add an GiftCard Item to an Order", e);
					}
					addFormException(new DropletException("ADD_GIFT_CARD_ITEM_TO_ORDER_EXCEPTION ::" + e.getMessage()));
				}finally{
					if(this.getFormError()){
						TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					}
					TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
					TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				}  
			}
			
			return checkFormRedirect( this.getAddGiftCardItemSuccessUrl(), this.getAddGiftCardItemErrorUrl(), request, response );
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}
	
	/**
	 * Override the method to stop commit & post commit steps if there is any
	 * form exceptions during validation. Rollback the transaction.
	 * 
	 * @param pSuccessURL
	 * @param pFailureURL
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 */
	@Override
	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL,
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if ((getCheckForValidSession()) && (!isValidSession(pRequest))) {
			addFormException(new DropletException(
					"Your session expired since this form was displayed - please try again.",
					"sessionExpired"));
		}

		if (getFormError()) {

			if (isLoggingDebug()) {
				logDebug("error - redirecting to: " + pFailureURL);
			}
			redirectOrForward(pRequest, pResponse, pFailureURL);
			return false;
		}

		if (DigitalStringUtil.isBlank(pSuccessURL)) {
			if (isLoggingDebug()) {
				logDebug("no form errors - staying on same page.");
			}
			return true;
		}

		if (isLoggingDebug()) {
			logDebug("no form errors - redirecting to: " + pSuccessURL);
		}
		redirectOrForward(pRequest, pResponse, pSuccessURL);
		return false;
	}

	
	/**
	 * adds gift card to the shopping cart
	 * 
	 * @param card
	 * @throws DigitalAppException,CommerceException
	 * @returns Sku of gift card or null if card was not added
	 */
	private String addGiftCardToShoppingCart(GiftCardDTO card)
			throws DigitalAppException, CommerceException {
		logInfo("Inside addGiftCardToShoppingCart");
		String retVal = new String();
		if(validateForMaxOrderLimit()){
			Order order = getOrder();
			RepositoryItem product = getProduct(card.getProductId());
			if (product != null && order != null) {
				synchronized(order){
					List<RepositoryItem> skus = (List<RepositoryItem>) product.getPropertyValue(CHILD_SKUS);
					if (skus != null && skus.size() == 1) {
						RepositoryItem sku = skus.get(0);
						if (isLoggingDebug()) {
							logDebug("Found Product " + sku.getRepositoryId() + ", "
									+ sku.getItemDisplayName()
									+ " matching gift card type of "
									+ card.getProductId());
						}
		
						GiftCardCommerceItem ci = (GiftCardCommerceItem) getCommerceItemManager()
									.createCommerceItem(GIFT_CARD_COMMERCE_ITEM,
										sku.getRepositoryId(),
										product.getRepositoryId(),
										card.getCardQty().longValue());
						if (isLoggingDebug()) {
							logDebug("CREATED COMMERCE ITEM: " + ci.getId());
						}
						updateCommerceItemWithCardAttributes(ci, card);
						ci = (GiftCardCommerceItem) getCommerceItemManager().addAsSeparateItemToOrder(order, ci);
		
						ShippingGroup sg = getShippingGroup(card);
						getShippingGroupManager().addShippingGroupToOrder(order, sg);
						getCommerceItemManager().addItemQuantityToShippingGroup(order, ci.getId(), sg.getId(), ci.getQuantity());
						getOrderManager().updateOrder(order);
						retVal = ci.getCatalogRefId();
						
						persistShoppingCart();
		
					} else {
						throw new DigitalAppException(
								"Cannot determine SKU for the gift card. "
										+ ((skus == null) ? 0 : skus.size())
										+ " attached to the product "
										+ product.getRepositoryId());
					}
				}
			}else{
				if (isLoggingDebug()) {
					logDebug("[Product to be added to card not found]" + card.getProductId());
				}
	
				addFormException(new DropletException("PRODUCT_MISSING :: Product to be added to card not found "));
			}
		}
		return retVal;
	}
	
	private void persistShoppingCart() {
		((DigitalProfileTools) getProfile().getProfileTools())
				.persistShoppingCart(getConfiguration().getShoppingCart());
	}
	
	/**
	 * Returns appropriate order
	 * 
	 * @return
	 * @throws DigitalAppException
	 */
	private Order getOrder() throws DigitalAppException {
		return getConfiguration().getShoppingCart() != null ? getConfiguration()
				.getShoppingCart().getCurrent() : null;
	}

	/**
	 * Updates commerce item with arroweye / reload values
	 * 
	 * @param ci
	 * @param card
	 * @throws DigitalAppException
	 */
	private void updateCommerceItemWithCardAttributes(GiftCardCommerceItem ci,
			GiftCardDTO card) throws DigitalAppException {

		final String DOUBLE_ZERO="0";

		ci.setAsiCardPrice((card.getVendorCardPrice() == null) ? new Double(DOUBLE_ZERO)
				: card.getVendorCardPrice());
		ci.setAsiId(card.getVendorId());
		ci.setAsiInvoiceAmount(card.getVendorInvoiceAmount() == null ? new Double(
				DOUBLE_ZERO) : card.getVendorInvoiceAmount());
		ci.setAsiOrderNumber(card.getVendorOrderNumber());
		ci.setAsiPostage(card.getVendorPostage() == null ? new Double(DOUBLE_ZERO)
				: card.getVendorPostage());
		ci.setBarCode(card.getBarCode());
		ci.setCardPrice(card.getCardPrice() == null ? new Double(DOUBLE_ZERO)
				: card.getCardPrice());
		ci.setCardQty(card.getCardQty() == null ? new Integer(DOUBLE_ZERO)
				: new Integer(card.getCardQty().intValue()));
		ci.setClientOrderNumber(card.getDswOrderNumber());
		ci.setCltAffiliateId(card.getDswAffiliateId() == null ? new Integer(DOUBLE_ZERO)
				: new Integer(card.getDswAffiliateId().intValue()));
		ci.setDollarValue((card.getDollarValue() == null) ? new Integer(DOUBLE_ZERO)
				: new Integer(card.getDollarValue().intValue()));
		ci.setPostage(card.getPostage() == null ? new Double(DOUBLE_ZERO) :
				card.getPostage());
		ci.setProductType(card.getProductId());
		ci.setGiftCardNumber(card.getGiftCardNumber());
		ci.setPin(card.getPin());
		ci.setItemQuantityReserved(ci.getQuantity());
		ci.setStorePickAvailable(false);
		ci.setShipToStoreAvailable(false);
		ci.setItemReserved(true);
		if (isLoggingDebug()) {
			logDebug("[ Commerce Item values set: " + ci + "]");
		}
		
		if(card.getAddress() != null){
			String email = card.getAddress().getEmail();
			if(DigitalStringUtil.isNotBlank(email)){
				ci.setRecipientEmail(email);
			}
			
			String firstName = card.getAddress().getFirstName();
			String middleName = card.getAddress().getMiddleName();
			String lastName = card.getAddress().getLastName();
			String name = DigitalCommonUtil.getName(firstName, middleName, lastName);
			if(DigitalStringUtil.isNotBlank(name)){
				ci.setRecipientName(name);
			}
		}
		
		if(card.getReturnAddress() != null){
			String email = card.getReturnAddress().getEmail();
			if(DigitalStringUtil.isNotBlank(email)){
				ci.setSenderEmail(email);
			}
			
			String firstName = card.getReturnAddress().getFirstName();
			String middleName = card.getReturnAddress().getMiddleName();
			String lastName = card.getReturnAddress().getLastName();
			String name = DigitalCommonUtil.getName(firstName, middleName, lastName);
			if(DigitalStringUtil.isNotBlank(name)){
				ci.setSenderName(name);
			}
		}
	}
	
	
	boolean validateAddGiftCardItemToOrder(){
		boolean ret = true;

		for(GiftCardDTO giftCardDTO : giftCardDTOList){
			if( giftCardDTO == null ){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT)));
				ret = false;
			}else if(giftCardDTO.getVendorId() == null ){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_VENDOR_ID)));
				ret = false;
			}else if(giftCardDTO.getVendorInvoiceAmount() == null){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_VENDOR_INVOICE_AMOUNT)));
				ret = false;
			}else if(giftCardDTO.getVendorOrderNumber() == null){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_VENDOR_ORDER_NUMBER)));
				ret = false;
			}else if(giftCardDTO.getVendorCardPrice() == null){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_VENDOR_CARD_PRICE)));
				ret = false;
			}else if(giftCardDTO.getVendorPostage() == null){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_VENDOR_POSTAGE)));
				ret = false;
			}else if(giftCardDTO.getCardQty() == null){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_CARDQTY)));
				ret = false;
			}else if(giftCardDTO.getDollarValue() == null){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_DOLLAR_VALUE)));
				ret = false;
			}else if(giftCardDTO.getDswOrderNumber() == null){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_DSW_ORDER_NUMBER)));
				ret = false;
			}else if(giftCardDTO.getProductId() == null){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_PRODUCT_ID)));
				ret = false;
			}else if(giftCardDTO.getAddress() == null){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_ADDRESS)));
				ret = false;
			}else if(giftCardDTO.getReturnAddress() == null){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_RETURN_ADDRESS)));
				ret = false;
			}
		}
		if(ret){
			ret = validateEGiftCard();
		}
		
		if(ret){
			
			ret = validateForMaxOrderLimit();
		}
	
		return ret;
	}
	
	
	private boolean validateForMaxOrderLimit() {
		logInfo("validateForMaxOrderLimit::Start");
		boolean returnValue = true;
		try {
			if (MultiSiteUtil.isCartPurchaseLimitExceeded(getOrder(), MultiSiteUtil.getCartPurchaseLimit())) {
				NumberFormat nf = NumberFormat.getCurrencyInstance();
				Object[] params = { nf.format(MultiSiteUtil.getCartPurchaseLimit()) };
				String msg = getMessageLocator().getMessageString(EXCEEDED_PURCHASE_LIMIT, params);
				addFormException(new DropletException(msg));
				returnValue = false;
				StringBuilder addGiftCardItemErrorUrlSB = new StringBuilder();
				addGiftCardItemErrorUrlSB.append(getAddGiftCardItemErrorUrl());
				addGiftCardItemErrorUrlSB.append(getCartPurchaseLimitQueryParam());
				addGiftCardItemErrorUrlSB.append(nf.format(MultiSiteUtil.getCartPurchaseLimit()));
				setAddGiftCardItemErrorUrl(addGiftCardItemErrorUrlSB.toString());
				logInfo("validateForMaxOrderLimit :: isCartPurchaseLimitExceeded " + msg + "returnValue ::"
						+ returnValue);
			} else if (MultiSiteUtil.isGCCartLineLimitExceeded(getOrder(), MultiSiteUtil.getCartLineLimit())) {
				Object[] params = { "" + MultiSiteUtil.getCartLineLimit() };
				String msg = getMessageLocator().getMessageString(EXCEEDED_ITEM_IN_CART_LIMIT, params);
				addFormException(new DropletException(msg));
				returnValue = false;
				StringBuilder addGiftCardItemErrorUrlSB = new StringBuilder();
				addGiftCardItemErrorUrlSB.append(getAddGiftCardItemErrorUrl());
				addGiftCardItemErrorUrlSB.append(getCartItemLimitQueryParam());
				addGiftCardItemErrorUrlSB.append(MultiSiteUtil.getCartLineLimit());
				setAddGiftCardItemErrorUrl(addGiftCardItemErrorUrlSB.toString());
				logInfo("validateForMaxOrderLimit :: isGCCartLineLimitExceeded " + msg + "returnValue ::"
						+ returnValue);
			} else if (MultiSiteUtil.isCartSkuLimitExceeded(getOrder(), MultiSiteUtil.getCartSkuLimit())) {
				Object[] params = { "" + MultiSiteUtil.getCartSkuLimit() };
				String msg = getMessageLocator().getMessageString(EXCEEDED_SKU_LIMIT, params);
				addFormException(new DropletException(msg));
				returnValue = false;
				StringBuilder addGiftCardItemErrorUrlSB = new StringBuilder();
				addGiftCardItemErrorUrlSB.append(getAddGiftCardItemErrorUrl());
				addGiftCardItemErrorUrlSB.append(getCartItemQtyLimitQueryParam());
				addGiftCardItemErrorUrlSB.append(MultiSiteUtil.getCartSkuLimit());
				setAddGiftCardItemErrorUrl(addGiftCardItemErrorUrlSB.toString());
				logInfo("validateForMaxOrderLimit :: isCartSkuLimitExceeded " + msg + "returnValue ::" + returnValue);
			}
		} catch (NumberFormatException | DigitalAppException e) {
			if (this.isLoggingError()) {
				this.logError("Exception Adding GiftCard Commerce Item :: ", e);
			}
			addFormException(
					new DropletException(this.getMessageLocator().getMessageString(INVALID_INPUT_GIFTCARD_TYPE)));
			returnValue = false;
			logInfo("validateForMaxOrderLimit :: isCartPurchaseLimitExceeded ::End" + returnValue);
		}

		return returnValue;

	}
	
	boolean validateEGiftCard(){
		boolean ret = true;
					
		for(GiftCardDTO giftCardDTO : giftCardDTOList){
			String productId = giftCardDTO.getProductId();
			try{
				String giftCardType = getProductType(productId);
				if(null == eGiftCardType){
					addFormException(new DropletException("PLATFORM_CONFIG_MISSING :: eGiftCardType missing in GiftCardFormHandler "));
					ret = false;
				}
				if(ret && eGiftCardType.equalsIgnoreCase(giftCardType) && 
						null == giftCardDTO.getAddress().getEmail()){
					addFormException(new DropletException(this.getMessageLocator().getMessageString(MISSING_INPUT_EMAILID)));
					ret = false;
				}
			}catch(Exception e){
				if(this.isLoggingError()){
					this.logError("Exception Adding GiftCard Commerce Item :: ",e);
				}
				addFormException(new DropletException(this.getMessageLocator().getMessageString(INVALID_INPUT_GIFTCARD_TYPE,productId)));
				ret = false;
			}
		}
		return ret;
	}

	
	/**
	 * Returns SKU matching Gift Card's type. This SKU will be used for creating
	 * commerce item. Throws GiftCardException if cardType, product.
	 * 
	 * @param cardType
	 * @return
	 * @throws DigitalAppException
	 */
	private RepositoryItem getProduct(String cardType) throws DigitalAppException {
			RepositoryItem item = null;
		try {
			// search product matching product type of the card
			RepositoryView rv = getRepository().getView(DESCRIPTOR_PRODUCT);
			QueryBuilder qb = rv.getQueryBuilder();
			QueryExpression productType = qb
					.createPropertyQueryExpression(PRODUCT_TYPE);
			QueryExpression productTypeVal = qb
					.createConstantQueryExpression(getProductType(cardType));
			Query query = qb.createComparisonQuery(productType, productTypeVal,
					QueryBuilder.EQUALS);
			RepositoryItem[] results = rv.executeQuery(query);
			if (results != null && results.length == 1) {
				item = results[0]; // product matching PRODUCT_TYPE
			} else {
				throw new DigitalAppException(
						"Cannot determine Product for the gift card. "
								+ ((results == null) ? 0 : results.length)
								+ " attached to the product type "
								+ productTypeVal);
			}
		} catch (RepositoryException e) {
			logError(e);
			throw new DigitalAppException(e);
		}
		if (isLoggingDebug()) {
			logDebug("Found Product " + item.getRepositoryId() + ", "
					+ item.getItemDisplayName()
					+ " matching gift card type of " + cardType);
		}
		
		return item;
	}

	/**
	 * Returns productType matching gift card type. Throws GiftCardException if
	 * no match found
	 * 
	 * @param cardType
	 * @return
	 * @throws DigitalAppException
	 *             If no gift card match found
	 */
	private String getProductType(String cardType) throws DigitalAppException {
		String productType = null;
		if (getAsiStandardGiftCardType().equals(cardType)) {
			productType = getStandardGiftCardType();
		} else if (getAsiPersonalizedGiftCardType().equals(cardType)) {
			productType = getPersonalizedGiftCardType();
		} else if (getAsiEGiftCardType().equals(cardType)) {
			productType = getEGiftCardType();
		} else {
			throw new DigitalAppException("Unknown Gift Card Type");
		}
		if (isLoggingDebug()) {
			logDebug("[End]");
		}
		return productType;
	}
	
	/**
	 * Returns appropriate hardgood or electronic shipping group depending on
	 * gift card type
	 * 
	 * @param card
	 * @return
	 * @throws DigitalAppException
	 */
	private ShippingGroup getShippingGroup(GiftCardDTO card)
			throws DigitalAppException {

		ShippingGroup sg = null;
		ShippingGroupStates sgs = getOrderFulfillmentTools().getShippingGroupStates();
		try {
			if (getAsiEGiftCardType().equals(card.getProductId())) {
				sg = getShippingGroupManager().createShippingGroup(ELECTRONIC_SHIPPING_GROUP);
				((ElectronicShippingGroup) sg).setEmailAddress(card.getAddress().getEmail()); //set the recipient email information
			}  else {
				sg = getShippingGroupManager().createShippingGroup(HARDGOOD_SHIPPING_GROUP);
				DigitalContactInfo address = new DigitalContactInfo();
				Address addressDTO = card.getAddress(); //card.getAddress() will always have recipient information, while card.getReturnAddress() will have sender address inform
				address.setFirstName(addressDTO.getFirstName());
				address.setMiddleName(addressDTO.getMiddleName());
				address.setLastName(addressDTO.getLastName());
				address.setAddress1(addressDTO.getAddress1());
				address.setAddress2(addressDTO.getAddress2());
				address.setCity(addressDTO.getCity());
				address.setState(addressDTO.getState());
				address.setPostalCode(addressDTO.getZip());
				address.setEmail(addressDTO.getEmail());
				// country for gift card is always "USA" (hardcode it, checked with lauren). 
				// ignore arrow-eye country as response sends back country id
				address.setCountry(CountryCode.USA.getValue());
				address.setPhoneNumber(DigitalStringUtil.stripNonDigit(addressDTO.getPhoneNumber()));
				DigitalRepositoryContactInfo repositoryCI = new DigitalRepositoryContactInfo();
				repositoryCI.setRepositoryItem(((HardgoodShippingGroup) sg).getRepositoryItem());
				OrderTools.copyAddress(address, repositoryCI);
			}
			sg.setState(sgs.getStateValue(GC_SHIPPING_GROUP_STATE));
			if (isLoggingDebug()) {
				logDebug("SET STATE TO: " + sg.getState());
			}
		} catch (CommerceException e) {
			logError("Error Adding GiftCard Shipping Address ", e);
			throw new DigitalAppException(e);
		}
		if (isLoggingDebug()) {
			logDebug("[Created a shipping group with Id: " + sg.getId()
					+ ", Type: " + sg.getShippingGroupClassType()
					+ ", Shipping Method: " + sg.getShippingMethod() + "]");
		}
		return sg;
	}
	
	   public void setAddItemCount(int pAddItemCount)
	   {
	     if (pAddItemCount <= 0) {
	       this.addItemCount = 0;
	       this.giftCardDTOList = null;
	     }
	     else {
	       this.addItemCount = pAddItemCount;
	       this.giftCardDTOList = new GiftCardDTO[this.addItemCount];
	       Throwable caught = null;
	       try {
	         for (int index = 0; index < pAddItemCount; index++) {
	           this.giftCardDTOList[index] = ((GiftCardDTO)Class.forName(GiftCardDTO.class.getName()).newInstance());
	         }
	       }
	       catch (Throwable thrown) {
	         caught = thrown;
	       }
	       if (caught != null) {
	         if (isLoggingError()) {
	           logError(caught);
	         }
	         this.giftCardDTOList = null;
	       }
	     }
	   }

	private CommerceItemManager getCommerceItemManager() {
		return getOrderManager().getCommerceItemManager();
	}

	private ShippingGroupManager getShippingGroupManager() {
		return getOrderManager().getShippingGroupManager();
	}

	private static final String DESCRIPTOR_PRODUCT = "product";
	private static final String GIFT_CARD_COMMERCE_ITEM = "giftCard";
	private static final String PRODUCT_TYPE = "type";
	private static final String CHILD_SKUS = "childSKUs";
	private static final String ELECTRONIC_SHIPPING_GROUP = "electronicShippingGroup";
	private static final String HARDGOOD_SHIPPING_GROUP = "hardgoodShippingGroup";
	private static final String LOCK_NAME_ATTRIBUTE_NAME = "dsw.DSWGiftCardServiceLock";
	private static final String GC_SHIPPING_GROUP_STATE = "giftcard_group";

	private static final String MISSING_INPUT = "validation.add.giftcard.missingInput";
	private static final String MISSING_INPUT_VENDOR_ID = "validation.add.giftcard.missingVendorId";
	private static final String MISSING_INPUT_VENDOR_INVOICE_AMOUNT = "validation.add.giftcard.missingVendorInvoiceAmount";
	private static final String MISSING_INPUT_VENDOR_ORDER_NUMBER = "validation.add.giftcard.missingVendorOrderNumber";
	private static final String MISSING_INPUT_VENDOR_CARD_PRICE = "validation.add.giftcard.missingVendorCardPrice";
	private static final String MISSING_INPUT_VENDOR_POSTAGE = "validation.add.giftcard.missingVendorPostage";
	private static final String MISSING_INPUT_CARDQTY = "validation.add.giftcard.missingCardQty";
	private static final String MISSING_INPUT_DOLLAR_VALUE = "validation.add.giftcard.missingDollarValue";
	private static final String MISSING_INPUT_DSW_ORDER_NUMBER = "validation.add.giftcard.missingDswOrderNumber";
	private static final String MISSING_INPUT_PRODUCT_ID = "validation.add.giftcard.missingProductId";
	private static final String MISSING_INPUT_ADDRESS = "validation.add.giftcard.missingAddress";
	private static final String MISSING_INPUT_RETURN_ADDRESS = "validation.add.giftcard.missingReturnAddress";
	private static final String MISSING_INPUT_EMAILID = "validation.add.giftcard.missingEmailId";
	private static final String INVALID_INPUT_GIFTCARD_TYPE = "validation.add.giftcard.invalidGiftCardType";
}