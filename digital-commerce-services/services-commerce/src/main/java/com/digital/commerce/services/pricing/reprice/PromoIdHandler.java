package com.digital.commerce.services.pricing.reprice;

/* */
import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.digital.commerce.common.util.DigitalStringUtil;
@SuppressWarnings({"rawtypes","unchecked"})
public class PromoIdHandler extends DefaultHandler {
	private final String	PRICEADJ		= "pricingAdjustment";
	private final String	REPOSITORYID	= "repositoryId";
	private final String	PROMOID			= "pricingAdjustment.promoId";
	private Map				adjPromos		= new HashMap( 10 );
	private String			adjId			= "";
	private String			priceAdjName	= "";
	private String			promoIdName		= "";
	private String			errMsg			= null;

	private StringBuffer	characterBuf;

	public String getErrorMessage() {
		return errMsg;
	}

	public Map getAdjPromos() {
		return adjPromos;
	}

	/** Receive notification of the start of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @param atts - The attributes attached to the element. If there are no attributes, it shall be an empty Attributes object.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */

	public void startElement( String namespaceURI, String localName, String qName, Attributes atts ) {
		characterBuf = new StringBuffer();

		try {
			if( PRICEADJ.equals( localName ) ) {
				priceAdjName = PRICEADJ;
			}

			if( DigitalStringUtil.isNotBlank(priceAdjName) ) {
				if( atts.getValue( REPOSITORYID ) != null ) adjId = atts.getValue( REPOSITORYID );
			}

			if( DigitalStringUtil.isNotBlank(priceAdjName) && DigitalStringUtil.isNotBlank(adjId) && PROMOID.trim().equals( localName ) ) {
				promoIdName = PROMOID;
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}

	/** Receive notification of character data inside an element.
	 * 
	 * @param ch - The characters.
	 * @param start - The start position in the character array.
	 * @param length - The number of characters to use from the character array.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void characters( char[] ch, int start, int length ) {
		characterBuf.append( ch, start, length );
	}

	/** Receive notification of the end of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void endElement( String namespaceURI, String localName, String qName ) {
		populateValue();
		try {
			if( PRICEADJ.equals( localName ) ) {
				priceAdjName = "";
				adjId = "";
				promoIdName = "";
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}

	private void populateValue() {
		try {
			String value = characterBuf.toString();

			if( DigitalStringUtil.isNotBlank(priceAdjName) && DigitalStringUtil.isNotBlank(adjId) && DigitalStringUtil.isNotBlank(promoIdName) ) {
				adjPromos.put( adjId, value );
				promoIdName = "";
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}
}
