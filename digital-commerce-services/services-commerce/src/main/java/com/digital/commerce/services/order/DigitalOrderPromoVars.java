package com.digital.commerce.services.order;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalOrderPromoVars implements java.io.Serializable {

	private static final long serialVersionUID = 5077016587572235024L;
	private String	promoName;
	private String	promoDescription;
	private Double	promoAmount;
}
