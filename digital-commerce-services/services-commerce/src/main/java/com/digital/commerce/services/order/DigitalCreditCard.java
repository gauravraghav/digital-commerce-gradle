/**
 *
 */
package com.digital.commerce.services.order;

import com.digital.commerce.constants.DigitalProfileConstants;

import atg.commerce.order.CreditCard;
import atg.commerce.order.PropertyNameConstants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalCreditCard extends CreditCard {
	
	private static final    String PROPERTY_CVV_OPTIONAL            = "cvvOptional";
	private static final    String PROPERTY_NAME_ON_CARD            = "nameOnCard";
	private static final    String PROPERTY_TOKEN_VALUE             = "tokenValue";
	private transient final String PROPERTY_PAYPAGE_REGISTRATION_ID = "paypageRegistrationId";
	private transient final String PROPERTY_VANTIV_LAST_FOUR 	   = "vantivLastFour";
	private transient final String PROPERTY_VANTIV_FIRST_SIX 	   = "vantivFirstSix";
	private transient final String PROPERTY_VANTIV_TRANS_ID	  	   = "vantivTransId";
	private transient final String PROPERTY_CREDIT_CARD_LAST_FOUR   = "creditCardLastFour";
	
	private boolean	isDefaultCreditCard;
	private boolean isNewCreditCard;
	private boolean isCardBeingSavedToProfile = false;
	private static final long serialVersionUID = -4912088362810195465L;

	public String getNameOnCard() {
		return (String)getPropertyValue( PROPERTY_NAME_ON_CARD );
	}

	public void setNameOnCard( String name ) {
		setPropertyValue( PROPERTY_NAME_ON_CARD, name );
	}

	public String getCvvNumber() {
		return (String)getPropertyValue( "cvvNumber" );
	}

	public void setCvvNumber( String cvv ) {
		setPropertyValue( "cvvNumber", cvv );
	}

	public String getTokenValue() {
		return (String)getPropertyValue( PROPERTY_TOKEN_VALUE );
	}

	public void setTokenValue( String tokenValue ) {
		setPropertyValue( PROPERTY_TOKEN_VALUE, tokenValue );
	}

	public String getPaypageRegistrationId() {
		return (String)getPropertyValue( PROPERTY_PAYPAGE_REGISTRATION_ID );
	}

	public void setPaypageRegistrationId( String paypageRegistrationId ) {
		setPropertyValue( PROPERTY_PAYPAGE_REGISTRATION_ID, paypageRegistrationId );
	}

	public String getVantivLastFour() {
		return (String)getPropertyValue( PROPERTY_VANTIV_LAST_FOUR );
	}

	public void setVantivLastFour( String vantivLastFour ) {
		setPropertyValue( PROPERTY_VANTIV_LAST_FOUR, vantivLastFour );
	}
	
	public String getVantivFirstSix() {
		return (String)getPropertyValue( PROPERTY_VANTIV_FIRST_SIX );
	}

	public void setVantivFirstSix( String vantivFirstSix ) {
		setPropertyValue( PROPERTY_VANTIV_FIRST_SIX, vantivFirstSix );
	}
	

	public void setIpAddress( String ipAddress ) {
		setPropertyValue( "ipAddress", ipAddress );
	}

	public String getIpAddress() {
		return (String)getPropertyValue( "ipAddress" );
	}

	public Boolean getCVVOptional() {
		return getPropertyValue( PROPERTY_CVV_OPTIONAL ) != null ? (Boolean)getPropertyValue( PROPERTY_CVV_OPTIONAL ) : false;
	}

	public void setCVVOptional( boolean cvvOptional ) {
		setPropertyValue( PROPERTY_CVV_OPTIONAL, cvvOptional );
	}

	public void setDswVisa( boolean pDswVisa ) {
		setPropertyValue( "dswVisa", Boolean.valueOf( pDswVisa ) );
	}

	public boolean isDswVisa() {
		Boolean returnValue = (Boolean)getPropertyValue( "dswVisa" );
		if( returnValue != null ) { return returnValue.booleanValue(); }
		return false;
	}

	public String getCreditCardType() {
		String cardType = super.getCreditCardType();
		if( DigitalProfileConstants.CREDIT_CARD_TYPE_DSW_VISA.equalsIgnoreCase( cardType ) ) {
			setDswVisa( true );
		} else {
			setDswVisa( false );
		}
		return cardType;
	}

	public void setCreditCardType( String pCreditCardType ) {
		if( DigitalProfileConstants.CREDIT_CARD_TYPE_DSW_VISA.equalsIgnoreCase( pCreditCardType ) ) {
			setDswVisa( true );
		} else {
			setDswVisa( false );
		}
		super.setPropertyValue( PropertyNameConstants.CREDITCARDTYPE, pCreditCardType );
	}
	
	public String getVantivTransId() {
		return (String)getPropertyValue( PROPERTY_VANTIV_TRANS_ID );
	}

	public void setvantivTransId ( String vantivLastFour ) {
		setPropertyValue( PROPERTY_VANTIV_TRANS_ID, vantivLastFour );
	}
	
	public String getcreditCardLastFour() {
		return (String)getPropertyValue( PROPERTY_CREDIT_CARD_LAST_FOUR );
	}

	public void setcreditCardLastFour ( String creditCardLastFour ) {
		setPropertyValue( PROPERTY_CREDIT_CARD_LAST_FOUR, creditCardLastFour );
	}
}
