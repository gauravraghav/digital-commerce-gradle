package com.digital.commerce.services.rewards.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RewardsRetrieveBirthdayGiftsResponse extends ResponseWrapper {

	private List<RewardsGiftItem> items;
	private int usedCount;
	private long allowedCount;
	private boolean isValidLoyaltyMember = true;
}
