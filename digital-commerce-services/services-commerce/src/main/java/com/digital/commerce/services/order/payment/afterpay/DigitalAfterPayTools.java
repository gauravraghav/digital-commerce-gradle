package com.digital.commerce.services.order.payment.afterpay;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.commerce.states.StateDefinitions;
import atg.nucleus.logging.ApplicationLoggingImpl;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.DigitalAfterPayConstants;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.afterpay.valueobject.AfterPayOrderResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Setter
@Getter
public class DigitalAfterPayTools extends ApplicationLoggingImpl {

    private DigitalShippingGroupManager dswShippingGroupManager;
    private DigitalPaymentGroupManager paymentGroupManager;
    private OrderManager orderManager;


    public void updateOrderData(Order order, AfterPayOrderResponse afterPayOrderResponse,String token)
            throws CommerceException, DigitalAppException {
        try {
            //Create afterPay payment group
            createAfterPayPaymentGroup(order,afterPayOrderResponse,token);
            synchronized(order) {
                getOrderManager().updateOrder(order);
            }
        } catch (CommerceException | DigitalAppException cEx) {
            if (isLoggingError()) {
                logError(cEx);
            }
            throw cEx;
        }
    }

    public void createAfterPayPaymentGroup(Order order, AfterPayOrderResponse afterPayOrderResponse, String token)
            throws CommerceException, DigitalAppException {
        AfterPayPayment afterPayGroup=null;
        boolean isAPPGAvailableOnOrder = false;
        List<PaymentGroup> listPaymentGroups = order.getPaymentGroups();
        if(listPaymentGroups!=null){
            for (PaymentGroup paymentGroup : listPaymentGroups){
                if (paymentGroup instanceof AfterPayPayment) {
                    afterPayGroup=(AfterPayPayment) paymentGroup;
                    isAPPGAvailableOnOrder = true;
                    break;
                }
            }
        }
        DigitalContactInfo contactInfo=null;
        if(null==afterPayGroup) {
            contactInfo = this.getPaymentGroupManager().getBillingAddressFromOrder(order);
            getPaymentGroupManager().removeNonGiftCardPaymentGroups(order);
            afterPayGroup = (AfterPayPayment) getPaymentGroupManager().createPaymentGroup(DigitalAfterPayConstants.AFTERPAY_PAYMENT_TYPE);

        }

        if(null!=afterPayGroup && DigitalStringUtil.isNotBlank(token)){
            afterPayGroup.setToken(token);
        }
        if(null!=afterPayOrderResponse ) {
            if (afterPayOrderResponse.getStatusCode().equalsIgnoreCase("APPROVED")) {
                int state = StateDefinitions.PAYMENTGROUPSTATES.getStateValue("approved");
                afterPayGroup.setToken(afterPayOrderResponse.getToken());
                afterPayGroup.setState(state);
            }
        }
        if(!isAPPGAvailableOnOrder) {
            // Add amount from order
            double amountRemaining = getPaymentGroupManager()
                .getAmountRemaining(order, order.getPriceInfo().getTotal());
            amountRemaining = DigitalCommonUtil.formatAmount(amountRemaining);
            afterPayGroup.setAmount(amountRemaining);

            getPaymentGroupManager().addPaymentGroupToOrder(order, afterPayGroup);
            this.getDswShippingGroupManager().setShippingAddress(contactInfo,this.getDswShippingGroupManager().getHardgoodShippingGroups(order));
            getOrderManager().addRemainingOrderAmountToPaymentGroup(order, afterPayGroup.getId());
            getPaymentGroupManager().recalculatePaymentGroupAmounts(order);
        }
    }
}
