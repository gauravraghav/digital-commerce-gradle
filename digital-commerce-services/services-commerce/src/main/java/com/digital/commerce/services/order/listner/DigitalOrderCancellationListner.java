package com.digital.commerce.services.order.listner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.exception.DigitalRuntimeException;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.communication.domain.Recipient;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent.ContentType;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.payload.OrderCancelledPayload;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderlineAttributesDTO;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;

import atg.commerce.CommerceException;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"unchecked","rawtypes"})
@Getter
@Setter
public class DigitalOrderCancellationListner extends DigitalOrderStatusListener {
	protected static final String CLASSNAME = DigitalOrderCancellationListner.class.getName();
	
	private String orderCancelCustomerInitiatedCommKey;
	private String orderItemCancelCustomerInitiatedCommKey;
	private String orderCancelSystemInitiatedCommKey;
	private String orderItemCancelSystemInitiatedCommKey;

	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

	@Override
	protected String getOrderNo(OrderStatusPayload payLoad) {
		if(payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null ){
			return payLoad.getOmniOrderUpdate().getOrder().getOrderNo();
		}
		return null;
	}

	@Override
	protected String getEmailTo(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((OrderCancelledPayload)payLoad).getPersonInfoBillTo().getEmailId();
	}

	@Override
	protected List<OrderLine> getOrderLines(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((OrderCancelledPayload)payLoad).getOrderLines();
	}

	@Override
	protected HashMap<String, Object> handleEmailNotification(
			OrderStatusPayload payLoad,
			DigitalOrderImpl order,
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {
		
		HashMap<String, Object> extraAttributes = new HashMap<>();
		
		OrderCancelledPayload orderCancelleddPayload = (OrderCancelledPayload)payLoad;
	    
	    extraAttributes.put("pickupPerson",orderCancelleddPayload.getPersonInfoBillTo().getFirstName()+
				" "+orderCancelleddPayload.getPersonInfoBillTo().getLastName());
		
	    extraAttributes.put("customerFirstName",orderCancelleddPayload.getPersonInfoBillTo().getFirstName());
	    extraAttributes.put("shippingAddress",orderCancelleddPayload.getPersonInfoBillTo());
		
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOPIS, itemsByFullfilmentType, extraAttributes);
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOSTS, itemsByFullfilmentType, extraAttributes);
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.SHIP, itemsByFullfilmentType, extraAttributes);
		
		//get mall plaza name
		HashMap<String, OrderlineAttributesDTO> lines = (HashMap<String, OrderlineAttributesDTO>)extraAttributes.get("orderLines");
		
		if(lines != null){
			Map.Entry<String,OrderlineAttributesDTO> entry=lines.entrySet().iterator().next();
			extraAttributes.put("mallPlazaName",entry.getValue().getMallPlazaName());
		}
		
		return extraAttributes;
		
		
	}
	
	@Override
	public void handleOrderStatusEvent(OrderStatusEvent orderStatusEvent) throws DigitalIntegrationException {
		OrderStatusPayload payLoad = (OrderStatusPayload)orderStatusEvent.getPayload();	
		String emailAddress = this.getEmailTo(payLoad);	
		if(DigitalStringUtil.isEmpty(emailAddress)){
			throw new DigitalIntegrationException("Email Address is not present in the payload", "EMAIL_ADDRESS_EMPTY_CS14", "EMAIL_ADDRESS_EMPTY_CS14");
		}
		Map params = new HashMap();
		String orderNo = this.getOrderNo(payLoad);
		DigitalOrderImpl order = null;
		try {
			
			MultiSiteUtil.setPushSiteContext(payLoad.getSiteId());
			
			List<OrderLine> orderSummaryLines = payLoad.getSummaryOrderLines();
			
			List<OrderLine> orderLines = this.getOrderLines(payLoad);
			if(DigitalStringUtil.isNotBlank(orderNo) &&
					getOrderTools().getOrderLookupService().getOrderManager().orderExists(orderNo)){
				order = (DigitalOrderImpl)this.getOrderTools().getOrderLookupService().getOrderManager().loadOrder(orderNo);
			}
			
			this.handleOrderUpdate(payLoad, order);
			
			if(orderLines == null || orderLines.size() == 0){
				String error = "Payload doesn't have orderlines or there is issue parsing the payload to POJO OrderLines :: OrderNo - " + orderNo + 
				" StatusType - " + orderStatusEvent.getStatusType();
				this.logError(error);
				throw new DigitalRuntimeException("OPERATIONAL_EMAIL_INVALID_PAYLOAD :: " + error);
			}
			
			RepositoryItem orderHistoryItem = getOrderHistoryItem(orderNo);
			
			this.putLoyaltyNumber(payLoad, orderHistoryItem, (HashMap) params);
			this.putOrderDetail(payLoad, (HashMap) params);
			
			
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType = this.categorizeOrderLines(orderLines, (HashMap)params);
						
			sortByStatusDate(itemsByFullfilmentType, ShippingGroupConstants.ShipType.BOPIS.name());
			sortByStatusDate(itemsByFullfilmentType, ShippingGroupConstants.ShipType.BOSTS.name());
			sortByStatusDate(itemsByFullfilmentType, ShippingGroupConstants.ShipType.SHIP.name());
			
			HashMap<String, Object> payLoadExtraData = this.handleEmailNotification(payLoad, order, itemsByFullfilmentType);
			params.put("payLoadItems", payLoadExtraData);
			
			this.putLocationInformation(itemsByFullfilmentType, (HashMap)params);
			params.put("fullfilmentItems", itemsByFullfilmentType);
			params.put( "locale", payLoad.getLocale());
			params.put( "siteUrl", MultiSiteUtil.getSiteURL() );
			params.put( "findStoreUrl", MultiSiteUtil.getSiteURL() + MultiSiteUtil.getStoreLocatorURI() );
			params.put( "emailSignUpUrl", MultiSiteUtil.getSiteURL() + MultiSiteUtil.getEmailSignUpURI() );
			params.put( "trackingDate", DigitalDateUtil.getOperationEmailsTrackingDate());
			
			ArrayList<Recipient> emailAddrs = new ArrayList<>();//
			Recipient recipient = new Recipient();
			recipient.setAddress(emailAddress);
			recipient.setRecipientType(Recipient.RecipientType.TO);
			emailAddrs.add(recipient);
			
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> summaryItemsByFullfilmentType = this.categorizeOrderLines(orderSummaryLines, (HashMap)params);
			
			removePayloadItemsFromOrderSummary(summaryItemsByFullfilmentType, itemsByFullfilmentType);
			
			determineCancellationTemplate(orderSummaryLines, orderLines, (HashMap)params);
			
			sortByStatusDate(summaryItemsByFullfilmentType, ShippingGroupConstants.ShipType.BOPIS.name());
			sortByStatusDate(summaryItemsByFullfilmentType, ShippingGroupConstants.ShipType.BOSTS.name());
			sortByStatusDate(summaryItemsByFullfilmentType, ShippingGroupConstants.ShipType.SHIP.name());
			
			handleOrderSummaryEmail(payLoad, order, summaryItemsByFullfilmentType, params);
			
			if(params!=null && params.get("notificationType")!=null){
				String notifyType = (String)params.get("notificationType");
				if(notifyType.equalsIgnoreCase("Order Cancelled - Customer Initiated")){
					if(this.getOrderCancelCustomerInitiatedCommKey() == null){
						String error = "OrderCancelCustomerInitiatedCommKey template is null :: 'isSystemCancelled' attribute at orderStatus level may be missing in yantra payload :: "
									+ "OrderId - " + orderNo + " :: Notitification Type " + payLoad.getEventType();
						throw new DigitalRuntimeException(error);
					}
					this.getCommunicationService().send(emailAddrs, this.getOrderCancelCustomerInitiatedCommKey(), params, ContentType.HTML, SiteContextManager.getCurrentSiteId());
				}else if(notifyType.equalsIgnoreCase("Order Item Cancelled - Customer Initiated")){
					if(this.getOrderItemCancelCustomerInitiatedCommKey() == null){
						String error = "OrderItemCancelCustomerInitiatedCommKey template is null :: 'isSystemCancelled' attribute at orderStatus level may be missing in yantra payload :: "
								+ "OrderId - " + orderNo + " :: Notitification Type " + payLoad.getEventType();						
						throw new DigitalRuntimeException(error);
					}					
					this.getCommunicationService().send(emailAddrs, this.getOrderItemCancelCustomerInitiatedCommKey(), params, ContentType.HTML, SiteContextManager.getCurrentSiteId());
				}else if(notifyType.equalsIgnoreCase("Order Cancelled - System Initiated")){
					if(this.getOrderCancelSystemInitiatedCommKey() == null){
						String error = "OrderCancelSystemInitiatedCommKey template is null :: 'isSystemCancelled' attribute at orderStatus level may be missing in yantra payload :: "
								+ "OrderId - " + orderNo + " :: Notitification Type " + payLoad.getEventType();						
						
						throw new DigitalRuntimeException(error);
					}					
					this.getCommunicationService().send(emailAddrs, this.getOrderCancelSystemInitiatedCommKey(), params, ContentType.HTML, SiteContextManager.getCurrentSiteId());
				}else if(notifyType.equalsIgnoreCase("Order Item Cancelled - System Initiated")){
					if(this.getOrderItemCancelSystemInitiatedCommKey() == null){
						String error = "OrderItemCancelSystemInitiatedCommKey template is null :: 'isSystemCancelled' attribute at orderStatus level may be missing in yantra payload :: "
								+ "OrderId - " + orderNo + " :: Notitification Type " + payLoad.getEventType();						
						
						throw new DigitalRuntimeException(error);
					}					
					this.getCommunicationService().send(emailAddrs, this.getOrderItemCancelSystemInitiatedCommKey(), params, ContentType.HTML, SiteContextManager.getCurrentSiteId());
				}
			}
		} catch (CommerceException e) {
			logError("CommerceException while handleOrderStatusEvent :: ", e);
		}
	}
	
	@Override 
	protected void removePayloadItemsFromOrderSummary(HashMap<String, HashMap<String, OrderlineAttributesDTO>> summaryItemsByFullfilmentType,
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType){
		
		for(String fullfilmentType : itemsByFullfilmentType.keySet()){
			Map<String, OrderlineAttributesDTO> payloadItems = itemsByFullfilmentType.get(fullfilmentType);
			for(String payloadKey : payloadItems.keySet()){
				OrderlineAttributesDTO payloadOrderLine = payloadItems.get(payloadKey);
				String payloadTrackingNumber = payloadOrderLine.getTrackingNumber();

				
				for(String orderSummaryFullfilmentTypeKey : summaryItemsByFullfilmentType.keySet()){
					Map<String, OrderlineAttributesDTO> orderSummaryItems = summaryItemsByFullfilmentType.get(orderSummaryFullfilmentTypeKey);
					boolean found = false;
					for(String orderSummaryKey : orderSummaryItems.keySet()){
						OrderlineAttributesDTO orderSummaryLineItem = orderSummaryItems.get(orderSummaryKey);
						String orderSummaryTrackingNumber = orderSummaryLineItem.getTrackingNumber();
						
						if(DigitalStringUtil.isNotBlank(orderSummaryKey) && orderSummaryKey.equalsIgnoreCase(payloadKey)){
							
							if(DigitalStringUtil.isNotBlank(payloadTrackingNumber)){
								if(payloadTrackingNumber.equalsIgnoreCase(orderSummaryTrackingNumber)){
									orderSummaryItems.remove(orderSummaryKey);
								}else{
									//don't remove
								}
							}else{
								orderSummaryItems.remove(orderSummaryKey);
							}
							found=true;
							break;
						}
					}
					
					if(found){
						break;
					}
				}
			}
		}
		
	}
	
	void determineCancellationTemplate(List<OrderLine> orderSummaryLines, List<OrderLine> orderLines, HashMap params){
		boolean allItemsCancelled=false;
		boolean orderItemCancelled=false;
		boolean isSystemCanceled = false;
		String notificationType=null;
		int orderLineCancelCount = 0, summaryLineCancelCount = 0;
		for (OrderLine o : orderLines) {
			String status = o.getStatus();
			if(!DigitalStringUtil.isEmpty(status)){
				if(status.equalsIgnoreCase("Canceled")){
					orderItemCancelled=true;
					orderLineCancelCount++;
				}
			}
			
			String systemCanceled = o.getIsSystemCanceled();
			if(!DigitalStringUtil.isEmpty(systemCanceled) && "Y".equalsIgnoreCase(systemCanceled)){
				isSystemCanceled=true;
			}
		}
		
		for (OrderLine o : orderSummaryLines) {
			String status = o.getStatus();
			if(!DigitalStringUtil.isEmpty(status)){
				if(status.equalsIgnoreCase("Canceled")){
					summaryLineCancelCount++;
				}
			}
		}		
		
		if(orderItemCancelled && (orderLines.size() == orderSummaryLines.size())
				&& (orderLineCancelCount == orderLines.size()) 
				&& (summaryLineCancelCount == orderSummaryLines.size())){
			
			allItemsCancelled = true;
		}
		
		if(allItemsCancelled && !isSystemCanceled){
			notificationType="Order Cancelled - Customer Initiated";
		}else if(!allItemsCancelled && !isSystemCanceled){
			notificationType="Order Item Cancelled - Customer Initiated";
		}else if(allItemsCancelled && isSystemCanceled){
			notificationType="Order Cancelled - System Initiated";
		}else if(!allItemsCancelled && isSystemCanceled){
			notificationType="Order Item Cancelled - System Initiated";
		}
		params.put("notificationType", notificationType);		
	}
}
