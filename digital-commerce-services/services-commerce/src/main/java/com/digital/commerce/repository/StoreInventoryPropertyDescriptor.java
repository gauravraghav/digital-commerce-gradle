package com.digital.commerce.repository;


import static com.digital.commerce.common.util.ComponentLookupUtil.GEO_LOCATION_UTIL;
import static com.digital.commerce.common.util.ComponentLookupUtil.STORE_INVENTORY_MANAGER;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.storelocator.DigitalGeoLocationUtil;
import com.digital.commerce.services.storelocator.DigitalStoreLocatorInventoryManager;
import com.digital.commerce.services.storelocator.SkuInventoryDetailBean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@SuppressWarnings({"rawtypes"})
public class StoreInventoryPropertyDescriptor extends RepositoryPropertyDescriptor {

  /**
   *
   */
  private static final long serialVersionUID = -6258342473415966032L;

  protected static final String TYPE_NAME = "StoreInventoryPropertyDescriptor";


  private volatile transient DigitalStoreLocatorInventoryManager dswStoreLocatorInventoryManager;
  private volatile transient DigitalGeoLocationUtil dswGeoLocationUtil;

  static {
    RepositoryPropertyDescriptor
        .registerPropertyDescriptorClass(TYPE_NAME, StoreInventoryPropertyDescriptor.class);
  }

  /**
   * @return DSWStoreLocatorInventoryManager
   */
  public DigitalStoreLocatorInventoryManager getStoreInventoryManager() {
    DigitalStoreLocatorInventoryManager retVal = this.dswStoreLocatorInventoryManager;
    if (retVal == null) {
      synchronized (this) {
        retVal = this.dswStoreLocatorInventoryManager = ComponentLookupUtil
            .lookupComponent(STORE_INVENTORY_MANAGER, DigitalStoreLocatorInventoryManager.class);
      }
    }
    return retVal;
  }

  /**
   * @return DSWGeoLocationUtil
   */
  public DigitalGeoLocationUtil getDSWGeoLocationUtil() {
    DigitalGeoLocationUtil geoLocationUtil = this.dswGeoLocationUtil;
    if (geoLocationUtil == null) {
      synchronized (this) {
        geoLocationUtil = this.dswGeoLocationUtil = ComponentLookupUtil
            .lookupComponent(GEO_LOCATION_UTIL, DigitalGeoLocationUtil.class);
      }
    }
    return geoLocationUtil;
  }


  /**
   * @param pItem
   * @param pValue
   * @return Object
   */
  public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {

    // if value already cached return from the cache
    if (pValue != null && pValue instanceof Long) {
      return pValue;
    }

    Long stockLevel = null;
    DigitalStoreLocatorInventoryManager dswStoreLocatorInventoryManager = getStoreInventoryManager();

    String shipType = (String) pItem.getPropertyValue("shipType");
    String storeId = (String) pItem.getPropertyValue("StoreId");
    String skuId = (String) pItem.getPropertyValue("catalogRefId");

    if (skuId != null && storeId != null && shipType != null) {
      List<String> skuIdsList = new ArrayList<>();
      skuIdsList.add(skuId);
      RepositoryItem[] locationRepoItem = getDSWGeoLocationUtil().getLocationForStore(storeId);
      if (locationRepoItem != null && locationRepoItem.length > 0) {
        Map<String, Object> productStoreInventory = dswStoreLocatorInventoryManager
            .getInventoryForSkus(skuIdsList, locationRepoItem[0].getRepositoryId());
        if (productStoreInventory != null) {
          if (productStoreInventory.get("skusResult") != null) {
            SkuInventoryDetailBean skuDetails = (SkuInventoryDetailBean)
                ((HashMap) productStoreInventory.get("skusResult")).get(skuId);
            if (skuDetails != null) {
              if (ShippingGroupConstants.ShipType.BOPIS.getValue().equals(shipType)) {
                stockLevel = skuDetails.getStoreLevelStock();
              } else {
                stockLevel = skuDetails.getRolledUpStock();
              }
            }
          }
        }
      }
    }
    if (stockLevel == null) {
      stockLevel = new Long(0);
    }

    return stockLevel;
  }

}
