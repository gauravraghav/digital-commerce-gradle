package com.digital.commerce.services.order.listner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.payload.CustomerPickupConfirmedPayload;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderlineAttributesDTO;


public class DigitalCustomerPickedupConfirmListner extends DigitalOrderStatusListener {
	protected static final String CLASSNAME = DigitalCustomerPickedupConfirmListner.class.getName();
	
	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

	@Override
	protected String getOrderNo(OrderStatusPayload payLoad) {
		if(payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null ){
			return payLoad.getOmniOrderUpdate().getOrder().getOrderNo();
		}
		return null;
	}

	@Override
	protected String getEmailTo(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((CustomerPickupConfirmedPayload)payLoad).getOmniOrderUpdate().getOrder().getCustomerEMailID();
	}

	@Override
	protected List<OrderLine> getOrderLines(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((CustomerPickupConfirmedPayload)payLoad).getOrderLines();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected HashMap<String, Object> handleEmailNotification(
			OrderStatusPayload payLoad,
			DigitalOrderImpl order,
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {
		
		HashMap<String, Object> extraAttributes = new HashMap<>();
		
		CustomerPickupConfirmedPayload customerPickupConfirmPayload = (CustomerPickupConfirmedPayload)payLoad;
	    
	    extraAttributes.put("altPickupPerson",customerPickupConfirmPayload.getPersonInfoShipTo().getMarkForFirstName()+
				" "+customerPickupConfirmPayload.getPersonInfoShipTo().getMarkForFirstName());
	
	    String customerFirstName = customerPickupConfirmPayload.getCustomerFirstName();

		if (DigitalStringUtil.isEmpty(customerFirstName)) {
			customerFirstName = customerPickupConfirmPayload.getPersonInfoBillTo().getFirstName();
			if (DigitalStringUtil.isEmpty(customerFirstName)) {
				customerFirstName = customerPickupConfirmPayload.getPersonInfoShipTo().getFirstName();
			}
		}

		extraAttributes.put("customerFirstName", customerFirstName);
		
		HashMap<String, Object> shippingAddress = new HashMap<>();
		// get Shipping Address Details
		shippingAddress.put("firstName", customerPickupConfirmPayload.getPersonInfoShipTo().getFirstName());
		shippingAddress.put("company", customerPickupConfirmPayload.getPersonInfoShipTo().getCompany());
		shippingAddress.put("addressLine1", customerPickupConfirmPayload.getPersonInfoShipTo().getAddressLine1());
		shippingAddress.put("addressLine2", customerPickupConfirmPayload.getPersonInfoShipTo().getAddressLine2());
		shippingAddress.put("city", customerPickupConfirmPayload.getPersonInfoShipTo().getCity());
		shippingAddress.put("state", customerPickupConfirmPayload.getPersonInfoShipTo().getState());
		shippingAddress.put("zipCode", customerPickupConfirmPayload.getPersonInfoShipTo().getZipCode());
		shippingAddress.put("country", customerPickupConfirmPayload.getPersonInfoShipTo().getCountry());
		shippingAddress.put("dayPhone", customerPickupConfirmPayload.getPersonInfoShipTo().getDayPhone());
		
		extraAttributes.put("shippingAddress", shippingAddress);
		
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOPIS, itemsByFullfilmentType, extraAttributes);
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOSTS, itemsByFullfilmentType, extraAttributes);
		this.putStatusDate(itemsByFullfilmentType, extraAttributes);
		
	
		// get mall plaza name
		HashMap<String, OrderlineAttributesDTO> lines = (HashMap<String, OrderlineAttributesDTO>) extraAttributes
				.get("orderLines");

		if (lines != null) {
			Map.Entry<String, OrderlineAttributesDTO> entry = lines.entrySet().iterator().next();
			extraAttributes.put("mallPlazaName", entry.getValue().getMallPlazaName());
		}
		
		extraAttributes.put("rewardsNumb",customerPickupConfirmPayload.getLoyaltyId());
		
		
		return extraAttributes;
	}

	

}
