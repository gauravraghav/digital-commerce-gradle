/**
 * 
 */
package com.digital.commerce.services.pricing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.services.common.ServicesStatus;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class ShippingRateTable extends ServicesStatus {

	/**
     * 
     */
	private static final long	serialVersionUID	= 2913119130538041118L;

	private Map					shippingRates;

	public ShippingRateTable() {
		shippingRates = new HashMap();
	}

	public List getRatesList( String shippingGroupId ) {
		return (List)shippingRates.get( shippingGroupId );
	}

	public ShippingRate getLowestRate( String shippingGroupId ) {
		List rateList = (List)shippingRates.get( shippingGroupId );
		ShippingRate lowestRate = null;
		Collections.sort( rateList );
		if( rateList.size() > 0 ) {
			lowestRate = (ShippingRate)rateList.get( 0 );
		}

		return lowestRate;

	}

	public void addShippingRate( String shippingGroupId, ShippingRate sRate ) {
		List rateList = null;
		if( shippingRates.containsKey( shippingGroupId ) ) {
			rateList = (List)shippingRates.get( shippingGroupId );
		} else {
			rateList = new ArrayList( 3 );
			shippingRates.put( shippingGroupId, rateList );
		}
		rateList.add( sRate );
	}

}
