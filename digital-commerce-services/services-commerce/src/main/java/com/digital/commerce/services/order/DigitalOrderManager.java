package com.digital.commerce.services.order;

import atg.commerce.CommerceException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.Order;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.SimpleOrderManager;
import atg.dtm.UserTransactionDemarcation;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.address.AddressTools;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.utils.DigitalAddressUtil;
import lombok.Getter;
import lombok.Setter;

import java.beans.IntrospectionException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalOrderManager extends SimpleOrderManager {
	protected static final String EXCEEDED_SKU_LIMIT = "exceededSkuLimit";

	protected static final String EXCEEDED_ITEM_IN_CART_LIMIT = "exceededItemInCartLimit";

	protected static final String EXCEEDED_PURCHASE_LIMIT = "exceededPurchaseLimit";

	private MessageLocator messageLocator;

	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalOrderManager.class);

	private static final String CLASSNAME = DigitalOrderManager.class.getName();

	/**
	 * 
	 * @param srcShippingGroup
	 * @param destShippingGroup
	 * @return true of false
	 */
	public boolean compareHardgoodShippingGroups(ShippingGroup srcShippingGroup, ShippingGroup destShippingGroup) {
		// handle giftcard shipping group during cart merge - There is no
		// consolidation of giftcard shipping groups based on address;
		// it needs to be copied/created as is to the target/destination order.
		if (((DigitalShippingGroupManager) getShippingGroupManager()).isGiftCardShippingGroup(srcShippingGroup)) {
			return false;
		}

		if (super.compareHardgoodShippingGroups(srcShippingGroup, destShippingGroup)) {
			String srcShipType = ((DigitalHardgoodShippingGroup) srcShippingGroup).getShipType();
			String desShipType = ((DigitalHardgoodShippingGroup) destShippingGroup).getShipType();
			String srcStoreId = ((DigitalHardgoodShippingGroup) srcShippingGroup).getStoreId();
			String desStoreId = ((DigitalHardgoodShippingGroup) destShippingGroup).getStoreId();

			boolean sameType = false;
			
			if(DigitalStringUtil.isNotBlank(srcShipType)){
				sameType = srcShipType.equals(desShipType);
			}

			if (DigitalStringUtil.isEmpty(srcStoreId) && DigitalStringUtil.isEmpty(desStoreId)){
				return sameType;
			}
			else {
				return sameType && (DigitalStringUtil.isEmpty(srcStoreId) ? "" : srcStoreId)
						.equals(DigitalStringUtil.isEmpty(desStoreId) ? "" : desStoreId);
			}
		}

		return false;
	}

	/**
	 *
	 * @param srcOrder
	 * @param destOrder
	 * @throws CommerceException
	 */
	public void mergeOrders(Order srcOrder, Order destOrder) throws CommerceException {
		try {
			addPaymentGroupFromCurrent(srcOrder, destOrder);
		} catch (IntrospectionException e) {
			logger.error(e);
		}

		@SuppressWarnings("unused")
		List<ShippingGroup> srcShippingGroups = srcOrder.getShippingGroups();
		List<ShippingGroup> destShippingGroups = destOrder.getShippingGroups();

		boolean removePreShippingGroup = false;
		DigitalHardgoodShippingGroup srcShippingGroup = null;
		DigitalShippingGroupManager sgm = ((DigitalShippingGroupManager) getShippingGroupManager());
		ShippingGroup srcsg = sgm.getFirstNonGiftCardShippingGroup(srcOrder);
		if (srcsg != null && !DigitalAddressUtil.isAddressEmpty(((DigitalHardgoodShippingGroup) srcsg).getShippingAddress())) {
			removePreShippingGroup = true;
			srcShippingGroup = (DigitalHardgoodShippingGroup) srcsg;
		}

		try {
			for (ShippingGroup sgrp : destShippingGroups) {
				if (sgrp instanceof DigitalHardgoodShippingGroup) {
					if (((DigitalHardgoodShippingGroup) sgrp).getShipType() != null && ((DigitalHardgoodShippingGroup) sgrp)
							.getShipType().equals(ShippingGroupConstants.ShipType.SHIP.getValue())) {
						if (removePreShippingGroup && !sgm.isGiftCardShippingGroup(sgrp)) {
							AddressTools.copyAddress(srcShippingGroup.getShippingAddress(),
									((DigitalHardgoodShippingGroup) sgrp).getShippingAddress());
						}

					}
				}
			}
		} catch (IntrospectionException e) {
			logger.error(e);
		}

		if (((DigitalOrderImpl) srcOrder).isDropPrevCartFlag()) {
			this.getCommerceItemManager().removeAllCommerceItemsFromOrder(destOrder);
		}

		mergeOrdersCopyProfileProperties(srcOrder, destOrder);

		super.mergeOrders(srcOrder, destOrder);

		((DigitalShippingGroupManager)getShippingGroupManager()).updateShipAddrNameForBopisBostsOrder(destOrder);

		((OrderImpl) destOrder).invalidateOrder();

	}

	/**
	 * 
	 * @param srcOrder
	 * @param destOrder
	 * @throws CommerceException
	 * @throws IntrospectionException
	 */
	private void addPaymentGroupFromCurrent(Order srcOrder, Order destOrder)
			throws CommerceException, IntrospectionException {
		List<PaymentGroup> destPaymentGroups = destOrder.getPaymentGroups();
		DigitalPaymentGroupManager paymentGroupManager = (DigitalPaymentGroupManager) this.getPaymentGroupManager();
		List <String>payPalPGIds = new ArrayList();
		List <String>exisingGiftCards = new ArrayList();
		for (PaymentGroup pg : destPaymentGroups) {
			if (pg instanceof PaypalPayment){
				pg = paymentGroupManager.findPaypalPayment(destOrder);
				if(null != pg){
					payPalPGIds.add(pg.getId());
				}
			}else if (pg instanceof DigitalGiftCard){
				exisingGiftCards.add(((DigitalGiftCard)pg).getCardNumber());
			}
		}

		List<PaymentGroup> srcPaymentGroups = srcOrder.getPaymentGroups();
		for (PaymentGroup pg : srcPaymentGroups) {
			PaymentGroup paymentGroup = this.copyFromCurrentPaymentGroup(pg, destOrder, exisingGiftCards);
			if (paymentGroup != null) {
				destOrder.addPaymentGroup(paymentGroup);
				if(paymentGroup instanceof PaypalPayment) {
					for (String id : payPalPGIds) {
						paymentGroupManager.removePaymentGroupFromOrder(destOrder, id);
					}
					addRemainingOrderAmountToPaymentGroup(destOrder, paymentGroup.getId());
				}else if (paymentGroup instanceof DigitalGiftCard) {
					addOrderAmountToPaymentGroup(destOrder, paymentGroup.getId(), paymentGroup.getAmount());
				}
			}
		}


	}

	/**
	 * 
	 * @param paymentGroup
	 * @param destOrder
	 * @return PaymentGroup
	 * @throws CommerceException
	 * @throws IntrospectionException
	 */
	private PaymentGroup copyFromCurrentPaymentGroup(PaymentGroup paymentGroup, Order destOrder, List <String>exisingGiftCards)
			throws CommerceException, IntrospectionException {

		PaymentGroup result = null;
		DigitalPaymentGroupManager paymentGroupManager = (DigitalPaymentGroupManager) this.getPaymentGroupManager();
		if (paymentGroup instanceof CreditCard) {
			if (!DigitalAddressUtil.isAddressEmpty(((CreditCard) paymentGroup).getBillingAddress())) {
				result = paymentGroupManager.createPaymentGroup(paymentGroup.getPaymentGroupClassType());
				DigitalContactInfo contactInfo = new DigitalContactInfo();
				AddressTools.copyAddress(((DigitalCreditCard) paymentGroup).getBillingAddress(), contactInfo);
				((DigitalCreditCard) result).setBillingAddress(contactInfo);
				result.setAmount(paymentGroup.getAmount());
				result.setCurrencyCode(paymentGroup.getCurrencyCode());
			}
		} else {
			if (paymentGroup instanceof PaypalPayment) {
				result = paymentGroupManager.createPaymentGroup(paymentGroup.getPaymentGroupClassType());
				result.setAmount(paymentGroup.getAmount());
				DigitalContactInfo addr = new DigitalContactInfo();
				AddressTools.copyAddress(((PaypalPayment) paymentGroup).getBillingAddress(), addr);
				((PaypalPayment) result).setBillingAddress(addr);
				((PaypalPayment) result).setCardinalOrderId(((PaypalPayment) paymentGroup).getCardinalOrderId());
				result.setCurrencyCode(paymentGroup.getCurrencyCode());
				((PaypalPayment) result).setPaypalEmail(((PaypalPayment) paymentGroup).getPaypalEmail());
				((PaypalPayment) result).setPaypalPayerId(((PaypalPayment) paymentGroup).getPaypalPayerId());
				result.setRequisitionNumber(paymentGroup.getRequisitionNumber());

				final DigitalCreditCard creditCard = paymentGroupManager.findCreditCard(destOrder);
				if(null != creditCard) {
					paymentGroupManager.removePaymentGroupFromOrder(destOrder, creditCard.getId());
				}

			} else if (paymentGroup instanceof DigitalGiftCard) {
				if(!exisingGiftCards.contains(((DigitalGiftCard)paymentGroup).getCardNumber())) {
				result = paymentGroupManager.createPaymentGroup(paymentGroup.getPaymentGroupClassType());
				result.setAmount(paymentGroup.getAmount());
				((DigitalGiftCard) result).setCardNumber(((DigitalGiftCard) paymentGroup).getCardNumber());
				result.setCurrencyCode(paymentGroup.getCurrencyCode());
				((DigitalGiftCard) result).setMemberNumber(((DigitalGiftCard) paymentGroup).getMemberNumber());
				((DigitalGiftCard) result).setPinNumber(((DigitalGiftCard) paymentGroup).getPinNumber());
			}
		}
		}

		return result;
	}

	/**
	 * 
	 * @param order
	 */
	public void checkOutMaxCartValidation(DigitalOrderImpl order) {
		synchronized (order) {
			order.setPurchaseLimitExceededMsg(null);
			order.setCartLineLimitExceededMsg(null);
			List<DigitalCommerceItem> commerceItems = order.getCommerceItems();
			for (DigitalCommerceItem item : commerceItems) {
				item.setSkuQtyLimitExceededMsg(null);
			}

			if (MultiSiteUtil.isCartPurchaseLimitExceeded(order, MultiSiteUtil.getCartPurchaseLimit())) {
				NumberFormat nf = NumberFormat.getCurrencyInstance();
				Object[] params = {nf.format(MultiSiteUtil.getCartPurchaseLimit())};
				String msg = getMessageLocator().getMessageString(EXCEEDED_PURCHASE_LIMIT, params);
				order.setPurchaseLimitExceededMsg(msg);
			} else if (MultiSiteUtil
					.isCartLineLimitExceededForCheckout(order, MultiSiteUtil.getCartLineLimit())) {
				Object[] params = {"" + MultiSiteUtil.getCartLineLimit()};
				String msg = getMessageLocator().getMessageString(EXCEEDED_ITEM_IN_CART_LIMIT, params);
				order.setCartLineLimitExceededMsg(msg);
			} else if (MultiSiteUtil.isCartSkuLimitExceeded(order, MultiSiteUtil.getCartSkuLimit())) {
				int qty = MultiSiteUtil.getCartSkuLimit();
				Object[] params = {"" + MultiSiteUtil.getCartSkuLimit()};
				String msg = getMessageLocator().getMessageString(EXCEEDED_SKU_LIMIT, params);
				if (qty != -1) {
					for (DigitalCommerceItem item : commerceItems) {
						if (item.getQuantity() > qty) {
							item.setSkuQtyLimitExceededMsg(msg);
						}
					}
				}
			}
		}
	}
	// END of CHANGE
	
	private void mergeOrdersCopyProfileProperties(Order pSrcOrder,
			Order pDstOrder) throws CommerceException {
		DigitalProfileTools profileTools = (DigitalProfileTools)getOrderTools().getProfileTools();
		
		try {
		
			RepositoryItem scrProfile = profileTools.getProfileItem(pSrcOrder.getProfileId());		
			MutableRepositoryItem dstrProfile = profileTools.getProfileItem(pDstOrder.getProfileId());
			
			Set<RepositoryItem>scrCoupons = (Set<RepositoryItem>) scrProfile.getPropertyValue("grantedCoupons");		
			Set<RepositoryItem>dstrCoupons = (Set<RepositoryItem>) dstrProfile.getPropertyValue("grantedCoupons");		
			
			dstrCoupons.addAll(scrCoupons);
			
			dstrProfile.setPropertyValue( "grantedCoupons", dstrCoupons );
			
			pDstOrder.setOriginOfOrder(pSrcOrder.getOriginOfOrder());
			
			if(((DigitalOrderImpl)pSrcOrder).getPropertyValue("altPickupFirstName")!=null){
				((DigitalOrderImpl)pDstOrder).setPropertyValue("altPickupFirstName",((DigitalOrderImpl)pSrcOrder).getPropertyValue("altPickupFirstName"));
			}
			if(((DigitalOrderImpl)pSrcOrder).getPropertyValue("altPickupLastName")!=null){
				((DigitalOrderImpl)pDstOrder).setPropertyValue("altPickupLastName", ((DigitalOrderImpl)pSrcOrder).getPropertyValue("altPickupLastName"));
			}
			if(((DigitalOrderImpl)pSrcOrder).getPropertyValue("altPickupEmail")!=null){
				((DigitalOrderImpl)pDstOrder).setPropertyValue("altPickupEmail",    ((DigitalOrderImpl)pSrcOrder).getPropertyValue("altPickupEmail"));
			}

		} catch (RepositoryException e) {
			logger.error("Error in merging the profile properties"+e.getMessage());
		} 
	}
}
