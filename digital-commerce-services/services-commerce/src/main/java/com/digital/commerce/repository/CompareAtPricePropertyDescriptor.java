/**
 *
 */
package com.digital.commerce.repository;

import static com.digital.commerce.common.util.ComponentLookupUtil.DSW_BASE_CONSTANTS;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.services.utils.DigitalServiceConstants;


/**
 * @author RH417692
 *
 */
public class CompareAtPricePropertyDescriptor extends RepositoryPropertyDescriptor {
	
	private static final long		serialVersionUID		= -6258342473415966032L;

	private static final String		TYPE_NAME				= "CompareAtPricePropertyDescriptor";
	
	private static final DigitalLogger	logger					= DigitalLogger.getLogger( CompareAtPricePropertyDescriptor.class );
	
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, CompareAtPricePropertyDescriptor.class );
	}
	
	public Object getPropertyValue( RepositoryItemImpl product, Object pValue ) {

		// if value already cached return from the cache
		if( pValue != null && pValue instanceof Double ) { return pValue; }
		
		if( product != null ) {
			try {
				setPriceProperty( product, isDisplayCompareAt(product));
				return isDisplayCompareAt(product);	
			} catch( Exception e ) {
				logger.error( "CompareAt Price exception caught: ", e );
			}
		}
			return false;
	}
	
	/**
	 * Below logic is used to find out if we have to display compare at price for price in cart product
	 * When (“Compare At” Price – “Retail” Price) is <= $2.00, “Compare At” price will not be displayed --> return false. 
	 * When (“Compare At” Price – “Retail” Price) is => $5.00, “Compare At” price will be displayed --> return true.
	 * When (“Compare At” Price – “Retail” Price) is > $2.00, but < $5.00, then:
	 *  - if ("Compare At" Price - "Retail" Price) / by "Retail" Price *(100) >= 10, then display “Compare At” --> return true
	 *  - if ("Compare At" Price - "Retail" Price) / by "Retail" Price *(100) < 10, then do not display “Compare At” --> return false      		
	 * @param product
	 * @return true/false
	 */
	private Boolean isDisplayCompareAt(RepositoryItemImpl product){
		
		double compareAtPrice =0.0;
		double retailPrice =0.0;
		Object nonMemberMSRPObj = product.getPropertyValue("nonMemberMSRP");
		if(nonMemberMSRPObj !=null){
			compareAtPrice= (double)nonMemberMSRPObj;
		}
		Object nonMemberMinPriceObj = product.getPropertyValue("nonMemberMinPrice");
		if(nonMemberMinPriceObj!=null){
			retailPrice = (double)nonMemberMinPriceObj;
		}
		
		DigitalServiceConstants DigitalBaseConstants = (DigitalServiceConstants) ComponentLookupUtil.lookupComponent( DSW_BASE_CONSTANTS, DigitalBaseConstants.class );
		
		Object priceInCartObj = product.getPropertyValue("priceInCart");
		if(priceInCartObj!=null && 
				(boolean)priceInCartObj){
			if((compareAtPrice - retailPrice) <= DigitalBaseConstants.getCompareAtPriceConstTwo()){
				return false;
			}else if ((compareAtPrice - retailPrice) > DigitalBaseConstants.getCompareAtPriceConstTwo() 
						&& (compareAtPrice - retailPrice) < DigitalBaseConstants.getCompareAtPriceConstFive()){
				double percent = (compareAtPrice - retailPrice)/retailPrice*DigitalBaseConstants.getCompareAtPriceConstHundred();
				if (percent < DigitalBaseConstants.getCompareAtPriceConstTen()){
					return false;
				}
			}
		}
		return true;
	}
	

	private void setPriceProperty( RepositoryItemImpl item, boolean displayCompareAtPrice ) {
		item.setPropertyValueInCache( this, displayCompareAtPrice );
	}

}
