package com.digital.commerce.services.order.purchase;

import atg.commerce.CommerceException;
import atg.commerce.catalog.CatalogTools;
import atg.commerce.order.CreditCard;
import atg.commerce.order.ElectronicShippingGroup;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.OrderManager;
import atg.commerce.order.OrderTools;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.payment.PaymentManager;
import atg.commerce.states.OrderStates;
import atg.commerce.states.StateDefinitions;
import atg.core.util.ContactInfo;
import atg.multisite.SiteContextManager;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.payment.PaymentStatus;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;
import atg.userprofiling.email.TemplateEmailInfo;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.taglib.ELFunctions;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.communication.CommunicationService;
import com.digital.commerce.integration.common.communication.domain.Recipient;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent.ContentType;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.jms.DigitalJMSClient;
import com.digital.commerce.integration.reward.domain.RewardServiceRequest;
import com.digital.commerce.integration.reward.domain.RewardsSendOrderData;
import com.digital.commerce.integration.reward.domain.api.RewardsSendOrderRequest;
import com.digital.commerce.integration.order.processor.domain.Person;
import com.digital.commerce.services.common.AddressType;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.common.validator.DigitalStartEndDateValidator;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalCommerceItemAttributes;
import com.digital.commerce.services.order.DigitalCreditCard;
import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderSummary;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.OrderlineAttributesDTO;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.afterpay.AfterPayCheckoutManager;
import com.digital.commerce.services.order.payment.creditcard.DigitalCreditCardServiceStatus;
import com.digital.commerce.services.order.payment.creditcard.DigitalCreditCardStatus;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.payment.DigitalPaymentManager;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.storelocator.DigitalStoreAddress;
import com.digital.commerce.services.storelocator.DigitalStoreTools;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import com.google.common.collect.Iterables;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/** 
 * 
 */
@SuppressWarnings({"unchecked", "rawtypes","unused"})
@Getter
@Setter
public class PostCommitOrderManager extends ApplicationLoggingImpl {

	private AfterPayCheckoutManager		afterPayCheckoutManager;
	private OrderHolder					shoppingCart;
	private DigitalProfileTools				profileTools;
	private OrderManager				orderManager;
	private TemplateEmailSender			templateEmailSender;
	private TemplateEmailInfo			templateEmailInfo;
	private OrderTools					orderTools;
	private DigitalRewardsManager rewardCertificateManager;
	private PaymentManager	paymentManager;
	private DigitalStoreTools storeTools;
	private DigitalStartEndDateValidator startEndDateValidator;
	public final static String SHIP = ShippingGroupConstants.ShipType.SHIP.name();
	
	private String communicationKey;
	
	private static final String OPERATION_NAME = "SendOrderConfirmationEmail";
	
	private String				bopisSubject;

	private DigitalServiceConstants		dswConstants;
	
	private DigitalOrderSummary dswosum;
	
	private CatalogTools 				catalogTools;
	
	private CommunicationService communicationService;

	private DigitalJMSClient jmsClient;
	
	public PostCommitOrderManager() {
		super(PostCommitOrderManager.class.getName());
	}

	public void putLocationInformation(Map<String, HashMap> dcattr, Map params){
		if(dcattr == null)
			return;
		
		HashMap<String, DigitalCommerceItemAttributes> picklines = (HashMap<String, DigitalCommerceItemAttributes>)dcattr.get(ShippingGroupConstants.ShipType.BOPIS.name());
		if(picklines != null && picklines.size() > 0){
			Map.Entry<String,DigitalCommerceItemAttributes> entry=picklines.entrySet().iterator().next();
			DigitalStoreAddress storeAddress = storeTools
					.fetchStoreAddress(entry.getValue().getStoreId());
			if (storeAddress != null) {
				Person p = new Person();
				p.setAddressLine1(storeAddress.getAddress1());
				p.setAddressLine2(storeAddress.getAddress2());
				p.setCity(storeAddress.getCity());
				p.setState(storeAddress.getStateAddress());
				p.setZipCode(storeAddress.getPostalCode());
				params.put("pickupMallPlazaName",storeAddress.getMallPlazaName());
				params.put("pickupAddress",p);
			}
			
		}
		HashMap<String, DigitalCommerceItemAttributes> stslines = (HashMap<String, DigitalCommerceItemAttributes>)dcattr.get(ShippingGroupConstants.ShipType.BOSTS.name());
		if(stslines != null && stslines.size() > 0){
			Map.Entry<String,DigitalCommerceItemAttributes> entry=stslines.entrySet().iterator().next();
			DigitalStoreAddress storeAddress = storeTools
					.fetchStoreAddress(entry.getValue().getStoreId());
			if (storeAddress != null) {
				Person p = new Person();
				p.setAddressLine1(storeAddress.getAddress1());
				p.setAddressLine2(storeAddress.getAddress2());
				p.setCity(storeAddress.getCity());
				p.setState(storeAddress.getStateAddress());
				p.setZipCode(storeAddress.getPostalCode());
				params.put("stsMallPlazaName",storeAddress.getMallPlazaName());
				params.put("stsAddress",p);
			}
		}
	}
	
		public void sendOrderConfirmation( final DigitalOrderImpl order, final Profile profile ) {
		final long startTime = System.currentTimeMillis();
		try {
			Map params = new HashMap();

			String emailAddress = profileTools.getOperationalEmailAddress( profile, order );
			Date tempDate = order.getSubmittedDate();
			params.put( "ORDERID", order.getId() );
			params.put( "EMAILADDRESS", emailAddress);
			params.put( "PNAME", profileTools.getOperationalFirstName( profile, order ) );
			params.put( "SHIPG", (List)order.getShippingGroups() );
			List<HardgoodShippingGroup> gcShippingGroup = ( (DigitalShippingGroupManager)orderManager.getShippingGroupManager() ).getGiftCardShippingGroups( order );
			List<HardgoodShippingGroup> nonGCShippingGroup = ( (DigitalShippingGroupManager)orderManager.getShippingGroupManager() ).getNonGiftCardShippingGroups( order );
			List<ElectronicShippingGroup> eGCShippingGroup = ( (DigitalShippingGroupManager)orderManager.getShippingGroupManager() ).getElectronicShippingGroups( order );
			if(gcShippingGroup!=null && ! gcShippingGroup.isEmpty()){
				params.put( "GCSHIPG", gcShippingGroup.size() );
			}else{
				params.put( "GCSHIPG", "" );
			}
			if(nonGCShippingGroup!=null && !nonGCShippingGroup.isEmpty()){
				params.put( "NONGCSHIPG", nonGCShippingGroup.size() );
			}else{
				params.put( "NONGCSHIPG", "" );
			}
			if(eGCShippingGroup!=null && !eGCShippingGroup.isEmpty()){
				params.put( "EGCSHIPG", eGCShippingGroup.size() );
			}else{
				params.put( "EGCSHIPG", "" );
			}
			
			params.put( "PDATE", new SimpleDateFormat( "MM/dd/yyyy" ).format( tempDate ) );
			params.put( "TDATE", DigitalDateUtil.getOperationEmailsTrackingDate() );
			params.put( "MERCH", Double.valueOf( order.getPriceInfo().getRawSubtotal() ) );
			params.put( "TAX", Double.valueOf( order.getPriceInfo().getTax() ) );
			params.put( "ORDRTOT", DigitalCommonUtil.round(order.getPriceInfo().getTotal() ));
			params.put( "ORDRAMT", order.getPriceInfo().getAmount() );
			params.put( "PSHIPPING", Double.valueOf( order.getTotalShippingCost() ) );
			params.put( "PGCSHIPPING", Double.valueOf( order.getTotalGCShippingCost() ) );
			params.put( "PNONGCSHIPPING", Double.valueOf( order.getTotalNonGCShippingCost() ) );
			params.put( "PSAVINGS", DigitalCommonUtil.round(Double.valueOf( ( (DigitalOrderTools)getOrderTools() ).getTotalSavings( order ) ) ));
			params.put( "PSUB", Double.valueOf( order.getPriceInfo().getRawSubtotal() + order.getPriceInfo().getTax() + order.getTotalShippingCost() ) );
			params.put( "LOYALTYTIER", order.getLoyaltyTier() );
			params.put( "LOCALE", order.getLocale());
			params.put( "siteUrl", MultiSiteUtil.getSiteURL() );
			params.put( "findStoreUrl", MultiSiteUtil.getSiteURL() + MultiSiteUtil.getStoreLocatorURI() );
			params.put( "emailSignUpUrl", MultiSiteUtil.getSiteURL() + MultiSiteUtil.getEmailSignUpURI() );	
			
									
			// For Diners and JCB the card type should be sent as discover.
			if( order.getFirstCreditCardPaymentGroup() != null ) {
				String cardType = order.getFirstCreditCardPaymentGroup().getCreditCardType();
				if( isLoggingDebug() ) {
					logDebug( "CardType Before Mapping :" + cardType );
				}	
				
				String mappedCardType = this.getDswConstants().getCardTypesMap().get(cardType);
				
				if( isLoggingDebug() ) {
					logDebug( "CardType After Mapping :" + mappedCardType );
				}
				
				params.put( "CCTYPE", mappedCardType );
			} else {
				params.put( "CCTYPE", "" );
			}
			if(order.getFirstPayPalPaymentGroup()!=null){
				params.put("PAYPALPAYMENTGROUP", "TRUE");
			}else{
				params.put("PAYPALPAYMENTGROUP", "");
			}
			if(order.getFirstAfterPayPaymentGroup()!=null) {
				params.put("AFTERPAYPAYMENTGROUP", "TRUE");
				double afterPayAmount = order.getAfterPayAmount();
				params.put("AFAMT",Double.valueOf(afterPayAmount));
				params.put("AFINSTALLMENTS",this.getAfterPayCheckoutManager().getConfiguration().getNumberOfPayments());
				params.put("AFINSTALLMENTAMOUNT",this.getAfterPayCheckoutManager().getAmountOwedWithinLimits(afterPayAmount));
			} else {
				params.put("AFTERPAYPAYMENTGROUP", "");
			}
			params.put( "PCCAMT", Double.valueOf( order.getCreditCardAmount() ) );
			params.put( "PCCNUM", ( order.getFirstCreditCardPaymentGroup() != null ) ? order.getFirstCreditCardPaymentGroup().getCreditCardNumber() : "" );
			if(order.getFirstCreditCardPaymentGroup() != null && order
							.getFirstCreditCardPaymentGroup() instanceof DigitalCreditCard){
				String creditCardNumber = ((DigitalCreditCard) order.getFirstCreditCardPaymentGroup()).getTokenValue();
				if(DigitalStringUtil.isNotEmpty(creditCardNumber)){
					params.put("PTOKENVAL",ELFunctions.getCreditCardNumberLast4(creditCardNumber));
				}else{
					params.put("PTOKENVAL","");
				}
								
			}
			if(order.getGiftCardPaymentGroups()!=null){
				List<DigitalGiftCard> giftCards = order.getGiftCardPaymentGroups();
				Map<String, String> gcDetails = new HashMap<>();
				for (DigitalGiftCard giftCard: giftCards ){
					gcDetails.put(ELFunctions.getCreditCardNumberLast4(giftCard.getCardNumber()), String.valueOf(giftCard.getAmount()));
				}
				params.put("PGCNUM",gcDetails);
			}
			params.put( "PVANTIVELASTFOUR", order.getLastFour());
			params.put( "PPPAMT", Double.valueOf( order.getPayPalAmount() ) );
			params.put( "PGCAMT", Double.valueOf( order.getGiftCardAmount() ) );
			params.put("CreditCardPaypalTotal", ((DigitalOrderTools)getOrderTools()).getCreditCardPaypalTotal(order));
			params.put("GiftCardTotal", ((DigitalOrderTools)getOrderTools()).getGiftCardsTotal(order));
			params.put( "ORDERPROMONAMES", order.getOrderPromoNames() );
			params.put( "taxOffline", order.isTaxOffline() );
			params.put( "isLuxury", order.isOrderContainsLuxuryItem() );
			params.put( "isSignatureRequired", order.isOrderContainsSignatureRequiredItem() );
			params.put( "isWhiteGlove", order.isWhiteGloveOrder() );
			params.put( "isUnreturnableToStore", order.isOrderContainsUnreturnableToStoreItem() );
			
			params.put("REWARDSNUMBER", getProfileTools().getLoyaltyNumber(order));
			
			String altPickupFirstName = order.getAltPickupFirstName();
			String altPickupLastName = order.getAltPickupLastName();
			String altPickupName = null;
			if(DigitalStringUtil.isNotEmpty(altPickupFirstName) && DigitalStringUtil.isNotEmpty(altPickupLastName)){
				altPickupName = altPickupFirstName + " " + altPickupLastName;
			}
			params.put( "PROXYPICKUPEMAIL", order.getAltPickupEmail());
			params.put( "PROXYPICKUPFIRSTNAME",altPickupFirstName);
			params.put( "PROXYPICKUPNAME",altPickupName);
			
			if(!DigitalStringUtil.isEmpty((String) profile.getPropertyValue("receivePromo"))){
				params.put( "EMAILOPTINFLAG", (String) profile.getPropertyValue("receivePromo"));
			}else{
				params.put( "EMAILOPTINFLAG", "");
			}
			List<DigitalCommerceItem> items = order.getCommerceItems();
			double amountOwe =0;
			double getCreditCardPaypalTotal =0;
			double giftCardTotal =0;
			double orderTotal = 0;
			double shipping = 0;
			double tax =0;
			double rewardsShare =0;
			double promoSavings =0;
			double savings =0;			
			
			
			amountOwe = dswosum.getAmountOwe(order);
			getCreditCardPaypalTotal = ((DigitalOrderTools)getOrderTools()).getCreditCardPaypalTotal(order);
			giftCardTotal = ((DigitalOrderTools)getOrderTools()).getGiftCardsTotal(order);
			orderTotal = dswosum.getOrderTotal(order);
			shipping=order.getPriceInfo().getShipping();
			tax= order.getPriceInfo().getTax();
			rewardsShare= ((DigitalOrderTools)getOrderTools()).getRewardsShareTotal(order);
			promoSavings= ((DigitalOrderTools)getOrderTools()).getPromotionalSavings(order);
			savings= ((DigitalOrderTools)getOrderTools()).getTotalSavingsWithoutPromotions(order);
			
						
			params.put( "amountOwe", amountOwe );
			params.put("CreditCardPaypalTotal", getCreditCardPaypalTotal);
			params.put("GiftCardTotal", giftCardTotal);
			params.put("OrderTotal", orderTotal);
			params.put("Shipping", shipping);
			params.put("Tax", tax);
			params.put("RewardsShare", rewardsShare);
			params.put("promoSavings", promoSavings);
			params.put("savings", savings);
			params.put("totalDiscounts", promoSavings+rewardsShare);
			params.put("totalCommerceItemCount", order.getTotalCommerceItemCount());
			
			Map<String, HashMap> dcattr = new HashMap<>();
			for(DigitalCommerceItem item: items) {
				String fulfillmentType = item.getShipType();
				
				fulfillmentType = fulfillmentType == null ? "SHIP" : fulfillmentType;
				
				HashMap<String, DigitalCommerceItemAttributes> typeItems = null;
				
				if(dcattr.get(fulfillmentType) != null){
					typeItems = dcattr.get(fulfillmentType);
				} else {
					typeItems = new HashMap<>();
					dcattr.put(fulfillmentType, typeItems);
				}
				
				String sizeDisplayName = " ", colorCode = " ";
				RepositoryItem product = ( (RepositoryItem)item.getAuxiliaryData().getProductRef() );
				RepositoryItem sku = (RepositoryItem)item.getAuxiliaryData().getCatalogRef();
				DigitalCommerceItemAttributes dci = new DigitalCommerceItemAttributes();
				
				String productID = (String)product.getRepositoryId();
				String productDescription = (String)product.getItemDisplayName(); 
				
				if (null!=item.getAuxiliaryData().getCatalogRef()) {
					RepositoryItem size = (RepositoryItem)sku.getPropertyValue("size");
					RepositoryItem color = (RepositoryItem)sku.getPropertyValue("color");
					if(size != null && null != size.getPropertyValue("displayName")) {
						sizeDisplayName = (String)size.getPropertyValue("displayName");
					} 
					if(color != null && null != color.getPropertyValue("colorCode")) {
						colorCode = (String)color.getPropertyValue("colorCode");
					}
					double quantity = item.getQuantity();
					if(sku != null && null != sku.getPropertyValue( "msrp" )) {
						dci.setMsrp( (Double)sku.getPropertyValue( "msrp" ));
					}
					dci.setSize(sizeDisplayName);
					dci.setQuantity(quantity);
					if(sku != null && null != sku.getPropertyValue( "upc" )) {
						dci.setUpc( (String)sku.getPropertyValue( "upc" ));
					}
					dci.setColor(colorCode);
					dci.setPrice(item.getPriceInfo().getRawTotalPrice());
					dci.setDiscountedShare(item.getPriceInfo().getOrderDiscountShare());
					dci.setProductID(productID);
					dci.setProductDescription(productDescription);
					dci.setStoreId((String)item.getStoreId());
				} else {
					logInfo("No SKU information available for product Id " + productID);
				}
					typeItems.put(item.getId(), dci);								
			}			
			
			params.put("itemData", dcattr);
			params.put( "orderNumber", order.getId() );
			params.put( "orderTotal", DigitalCommonUtil.round(order.getPriceInfo().getTotal()));
			params.put("subTotal",order.getPriceInfo().getRawSubtotal());
			params.put("tax",Double.valueOf( order.getPriceInfo().getTax() ) );
			if (Double.valueOf( order.getTotalShippingCost() )  > 0.0) {
					params.put("shipping", Double.valueOf( order.getTotalShippingCost() ) );
			} else {
					params.put("shipping", "0.0");
	
			}
			params.put("orderDate", new SimpleDateFormat( "MM/dd/yyyy" ).format( tempDate ));
			params.put("trackingDate",DigitalDateUtil.getOperationEmailsTrackingDate());
			this.putLocationInformation(dcattr, params);
			setDisplayMultiItemTextFlag(populateOrderlineAttributes(order,gcShippingGroup,nonGCShippingGroup,eGCShippingGroup),params);
			params.put("fullfilmentItems", this.populateOrderlineAttributes(order,gcShippingGroup,nonGCShippingGroup,eGCShippingGroup));
			params.put("customerFirstName", profileTools.getOperationalFirstName( profile, order ));
			templateEmailInfo.setTemplateParameters( params );
			
			ArrayList<Recipient> emailAddrs = new ArrayList<>();
			Recipient recipient = new Recipient();
			params.put("emailAddress", emailAddress);
			recipient.setAddress(emailAddress);
			recipient.setRecipientType(Recipient.RecipientType.TO);
			emailAddrs.add(recipient);
			if( order.isPickupItems() ) {
				((TemplateEmailInfoImpl)templateEmailInfo).setMessageSubject(getBopisSubject());
			}
			communicationService.send(emailAddrs, communicationKey, params, ContentType.HTML, SiteContextManager.getCurrentSiteId());
			//Send alternate email for BOPIS and BOSTS orders if there is one.
			if(null != dcattr.get(ShippingGroupConstants.ShipType.BOPIS.name()) || null != dcattr.get(ShippingGroupConstants.ShipType.BOSTS.name())){
				String alternateEmail = order.getAltPickupEmail();
				if(DigitalStringUtil.isNotBlank(alternateEmail) && !alternateEmail.trim().equalsIgnoreCase(emailAddress)){
					emailAddrs = new ArrayList<>();
					Recipient recipientAlt = new Recipient();
					recipientAlt.setAddress(alternateEmail.trim());
					recipientAlt.setRecipientType(Recipient.RecipientType.TO);
					emailAddrs.add(recipientAlt);
					communicationService.send(emailAddrs, "alternativePickupOrderConfirmation", params, ContentType.HTML, SiteContextManager.getCurrentSiteId());
				}
			}
		} catch (DigitalIntegrationException e) {
			logError( "Digital Integration Exception", e );
		} catch(Exception e){
			logError("Exception: ", e);
		}finally {
			final long endTime = System.currentTimeMillis();
			final long executionTime = endTime - startTime;
			StringBuilder sb = new StringBuilder();
			sb.append(OPERATION_NAME).append(" : ");
			sb.append(executionTime).append("ms");
			logInfo(sb.toString());
		}
	}
	
	
	private void setDisplayMultiItemTextFlag(Map<String, Map<String, OrderlineAttributesDTO>> groupedOrderlineItems,Map params){
		
		if(groupedOrderlineItems.get("SHIPGRN")!=null){
			if(groupedOrderlineItems.get("SHIPGRN").size() >1){
				params.put("DISPLAY_MULTI_ITEMS_TXT_GRN", "YES");
			}else{
				for (Map.Entry<String, OrderlineAttributesDTO> entry : groupedOrderlineItems.get("SHIPGRN").entrySet()) {
					OrderlineAttributesDTO orderLineAttr = groupedOrderlineItems.get("SHIPGRN").get(entry.getKey());
					if(Integer.parseInt(orderLineAttr.getItemQuantity()) > 1){
						params.put("DISPLAY_MULTI_ITEMS_TXT_GRN", "YES");
						break;
					}
				}
			}
		}
		
		if(groupedOrderlineItems.get("SHIP2ND")!=null){
		
			if(groupedOrderlineItems.get("SHIP2ND").size() >1){
				params.put("DISPLAY_MULTI_ITEMS_TXT_2ND", "YES");
			}else{
				for (Map.Entry<String, OrderlineAttributesDTO> entry : groupedOrderlineItems.get("SHIP2ND").entrySet()) {
					OrderlineAttributesDTO orderLineAttr = groupedOrderlineItems.get("SHIP2ND").get(entry.getKey());
					if(Integer.parseInt(orderLineAttr.getItemQuantity()) > 1){
						params.put("DISPLAY_MULTI_ITEMS_TXT_2ND", "YES");
						break;					
					}
				}
			}
		}
		
		if(groupedOrderlineItems.get("SHIPNDA")!=null){
			if(groupedOrderlineItems.get("SHIPNDA").size() >1){
				params.put("DISPLAY_MULTI_ITEMS_TXT_NDA", "YES");
			}else{
				for (Map.Entry<String, OrderlineAttributesDTO> entry : groupedOrderlineItems.get("SHIPNDA").entrySet()) {
					OrderlineAttributesDTO orderLineAttr = groupedOrderlineItems.get("SHIPNDA").get(entry.getKey());
					if(Integer.parseInt(orderLineAttr.getItemQuantity()) > 1){
						params.put("DISPLAY_MULTI_ITEMS_TXT_NDA", "YES");
						break;
					}
				}				
			}
		}

		
	}
	
	protected Map<String, Map<String, OrderlineAttributesDTO>> populateOrderlineAttributes(DigitalOrderImpl ordImp,
				List<HardgoodShippingGroup> gcShippingGroups,List<HardgoodShippingGroup> nonGCShippingGroups,	
					List<ElectronicShippingGroup> eGCShippingGroups){
		Map<String, Map<String, OrderlineAttributesDTO>> itemsByFullfilmentType = new HashMap<>();
		Map<String, OrderlineAttributesDTO> stsOrderLineMap = new HashMap<>();
		Map<String, OrderlineAttributesDTO> ispuOrderLineMap = new HashMap<>();

		if(gcShippingGroups!=null && !gcShippingGroups.isEmpty()){
			Map<String, OrderlineAttributesDTO> shipGCOrderLineMap = new HashMap<>();
			for(HardgoodShippingGroup gcShippingGroup:gcShippingGroups){
				List<ShippingGroupCommerceItemRelationship> commerceItemRel = (List<ShippingGroupCommerceItemRelationship>) ELFunctions.getPropertyValue(gcShippingGroup, "commerceItemRelationships");
				for(ShippingGroupCommerceItemRelationship sgCiRel : commerceItemRel){
					DigitalCommerceItem commerceItem = (DigitalCommerceItem) ELFunctions.getPropertyValue(sgCiRel, "commerceItem");
					String shipType = commerceItem.getShipType();
					if(DigitalStringUtil.isEmpty(shipType)){
						return itemsByFullfilmentType;
					}
					if("SHIP".equalsIgnoreCase(shipType)){
						OrderlineAttributesDTO iattr = populateOrderLines(ordImp,
								gcShippingGroup,null,commerceItem);
						shipGCOrderLineMap.put(commerceItem.getId(), iattr);
					}
					
				}
			}
			itemsByFullfilmentType.put("SHIPGC", shipGCOrderLineMap);
		}
		if(eGCShippingGroups!=null && !eGCShippingGroups.isEmpty()){
			Map<String, OrderlineAttributesDTO> shipeGCOrderLineMap = new HashMap<>();
			for(ElectronicShippingGroup eGCShippingGroup: eGCShippingGroups){
				List<ShippingGroupCommerceItemRelationship> commerceItemRel = (List<ShippingGroupCommerceItemRelationship>) ELFunctions.getPropertyValue(eGCShippingGroup, "commerceItemRelationships");
				for(ShippingGroupCommerceItemRelationship sgCiRel : commerceItemRel){
					DigitalCommerceItem commerceItem = (DigitalCommerceItem) ELFunctions.getPropertyValue(sgCiRel, "commerceItem");
					String shipType = commerceItem.getShipType();
					if(DigitalStringUtil.isEmpty(shipType)){
						return itemsByFullfilmentType;
					}
					if("SHIP".equalsIgnoreCase(shipType)){
						OrderlineAttributesDTO iattr = populateOrderLines(ordImp,
								null,eGCShippingGroup, commerceItem);
						shipeGCOrderLineMap.put(commerceItem.getId(), iattr);
					}
					
				}
			}
			itemsByFullfilmentType.put("SHIPeGC", shipeGCOrderLineMap);
		}
		
		
		if(nonGCShippingGroups != null && !nonGCShippingGroups.isEmpty()){
			Map<String, OrderlineAttributesDTO> shipGRNOrderLineMap = new HashMap<>();
			Map<String, OrderlineAttributesDTO> ship2NDOrderLineMap = new HashMap<>();
			Map<String, OrderlineAttributesDTO> shipNDAOrderLineMap = new HashMap<>();
			for(HardgoodShippingGroup nonGCShippingGroup:nonGCShippingGroups){
				List<ShippingGroupCommerceItemRelationship> commerceItemRel = (List<ShippingGroupCommerceItemRelationship>) ELFunctions.getPropertyValue(nonGCShippingGroup, "commerceItemRelationships");
				for(ShippingGroupCommerceItemRelationship sgCiRel : commerceItemRel){
					DigitalCommerceItem commerceItem = (DigitalCommerceItem) ELFunctions.getPropertyValue(sgCiRel, "commerceItem");
					String shipType = commerceItem.getShipType();
					if(DigitalStringUtil.isEmpty(shipType)){
						return itemsByFullfilmentType;
					}
					if("SHIP".equalsIgnoreCase(shipType)){				
						OrderlineAttributesDTO iattr = populateOrderLines(ordImp,
								nonGCShippingGroup,null, commerceItem);
						if(DigitalStringUtil.isNotEmpty(iattr.getShipCode()) && iattr.getShipCode().equalsIgnoreCase("GRN")){
							shipGRNOrderLineMap.put(commerceItem.getId(), iattr);
						}
						if(DigitalStringUtil.isNotEmpty(iattr.getShipCode()) && iattr.getShipCode().equalsIgnoreCase("2ND")){
							ship2NDOrderLineMap.put(commerceItem.getId(), iattr);
						}
						if(DigitalStringUtil.isNotEmpty(iattr.getShipCode()) && iattr.getShipCode().equalsIgnoreCase("NDA")){
							shipNDAOrderLineMap.put(commerceItem.getId(), iattr);
						}
					}else if("BOPIS".equalsIgnoreCase(shipType)){
						OrderlineAttributesDTO iattr = populateOrderLines(ordImp,
								nonGCShippingGroup,null, commerceItem);
						ispuOrderLineMap.put(commerceItem.getId(), iattr);
						
					}else if("BOSTS".equalsIgnoreCase(shipType)){
						OrderlineAttributesDTO iattr = populateOrderLines(ordImp,
								nonGCShippingGroup,null, commerceItem);
						stsOrderLineMap.put(commerceItem.getId(), iattr);
					}
				}
			}
			itemsByFullfilmentType.put("SHIPGRN", shipGRNOrderLineMap);
			itemsByFullfilmentType.put("SHIP2ND", ship2NDOrderLineMap);
			itemsByFullfilmentType.put("SHIPNDA", shipNDAOrderLineMap);
			itemsByFullfilmentType.put("BOPIS", ispuOrderLineMap);
			itemsByFullfilmentType.put("BOSTS", stsOrderLineMap);
		}

		return itemsByFullfilmentType;
	}

	/**
	 * @param ordImp
	 * @param nonGCShippingGroup
	 * @param commerceItem
	 * @return OrderlineAttributesDTO
	 */
	private OrderlineAttributesDTO populateOrderLines(DigitalOrderImpl ordImp,
			HardgoodShippingGroup nonGCShippingGroup,ElectronicShippingGroup electronicShippingGroup,
			DigitalCommerceItem commerceItem) {
		OrderlineAttributesDTO iattr = new OrderlineAttributesDTO();
		Object size = ELFunctions.getPropertyValue(commerceItem.getAuxiliaryData().getCatalogRef(), "size.displayName");
		if(size!=null){
			iattr.setItemSize((String)size);
		}
		Object itemDescription = ELFunctions.getPropertyValue(commerceItem.getAuxiliaryData().getProductRef(), "productTitle");
		if(itemDescription!=null){
			iattr.setItemDesc((String)itemDescription);
		}
		iattr.setItemQuantity(Long.toString(commerceItem.getQuantity()));
		iattr.setItemUnitPrice(Double.toString(commerceItem.getPriceInfo().getAmount()));
		iattr.setItemStyle(commerceItem.getAuxiliaryData().getProductId());
		Object itemWidth = ELFunctions.getPropertyValue(commerceItem.getAuxiliaryData().getCatalogRef(), "dimension.displayName");
		if(itemWidth!=null){
			iattr.setItemWidth((String)itemWidth);
		}
		iattr.setItemStatusQuantity(Long.toString(commerceItem.getQuantity()));
		String shippingMethod = null;
		String region = null;
		String rank = null;
		if(nonGCShippingGroup!=null){
			if( null != nonGCShippingGroup.getPropertyValue("region")){
				region = (String)nonGCShippingGroup.getPropertyValue("region");
			}
			if(null != nonGCShippingGroup.getPropertyValue("rank")){
				rank = (String)nonGCShippingGroup.getPropertyValue("rank");
			}
			if(commerceItem.getGroundShippingOnly()){
				iattr.setShipCode("GRN");
			}else{
				iattr.setShipCode(nonGCShippingGroup.getShippingMethod());
			}
			
			shippingMethod = (String) ELFunctions.getPropertyValue(nonGCShippingGroup, "shippingMethod");
			if(commerceItem.getGroundShippingOnly()){
				iattr.setItemShippingMethod("GRN");
			}else{
				iattr.setItemShippingMethod(shippingMethod);
			}
	
			if(null != nonGCShippingGroup.getPropertyValue("region")){
				region = (String)nonGCShippingGroup.getPropertyValue("region");
			}
			if(null != nonGCShippingGroup.getPropertyValue("rank")){
				rank = (String)nonGCShippingGroup.getPropertyValue("rank");
			}			
		}
		if(electronicShippingGroup!=null){
			iattr.setShipCode(electronicShippingGroup.getShippingMethod());
			shippingMethod = (String) ELFunctions.getPropertyValue(electronicShippingGroup, "shippingMethod");
			if(commerceItem.getGroundShippingOnly()){
				iattr.setItemShippingMethod("GRN");
			}else{
				iattr.setItemShippingMethod(shippingMethod);
			}
		}
		Object colorCode = ELFunctions.getPropertyValue(commerceItem.getAuxiliaryData().getCatalogRef(), "color.colorCode");
		if(colorCode!=null){
			iattr.setColorCode((String)colorCode);
		}
		
		Object itemBrand = ELFunctions.getPropertyValue(commerceItem.getAuxiliaryData().getProductRef(), "dswBrand.displayName");
		if(itemBrand!=null){
			iattr.setItemBrand((String)itemBrand);
		}
		Object productTitle = ELFunctions.getPropertyValue(commerceItem.getAuxiliaryData().getProductRef(), "productTitle");
		if(productTitle!=null){
			iattr.setProductTitle((String)productTitle);
		}
		
		iattr.setItemPromoList((Map<String, String>) ordImp.listItemPromoDescriptionsMap(commerceItem));
		
		iattr.setItemShipType(commerceItem.getShipType());
	
		iattr.setProductId(commerceItem.getAuxiliaryData().getProductId());
		
		RepositoryItem sku = (RepositoryItem) commerceItem.getAuxiliaryData().getCatalogRef();
		
		if(null != sku){
			iattr.setItemUPC((String)sku.getPropertyValue("upc"));
			Set parentProducts = (Set)sku.getPropertyValue( "parentProducts" );
			if( parentProducts != null && !parentProducts.isEmpty() ) {
				RepositoryItem product = (RepositoryItem)parentProducts.iterator().next();
				
				if(DigitalStringUtil.isBlank(iattr.getItemDesc())){
					iattr.setItemDesc((String)product.getPropertyValue( "displayName" ));
				}
				
				if(DigitalStringUtil.isBlank(iattr.getItemBrand()) &&
						product.getPropertyValue( "dswBrand" ) != null){
					iattr.setItemBrand((String)((RepositoryItem)product.getPropertyValue( "dswBrand" )).getPropertyValue("displayName"));
				}	
			}
			
			if(DigitalStringUtil.isBlank(iattr.getItemSize()) &&
					sku.getPropertyValue( "size" ) != null){
				iattr.setItemSize((String)((RepositoryItem)sku.getPropertyValue( "size" )).getPropertyValue("displayName"));
			}
			if(DigitalStringUtil.isBlank(iattr.getItemColor()) &&
					sku.getPropertyValue( "color" ) != null){
				iattr.setItemColor((String)((RepositoryItem)sku.getPropertyValue( "color" )).getPropertyValue("displayName"));
			}
		}
		
	
			
		
		if(nonGCShippingGroup!=null && nonGCShippingGroup.getShippingAddress() != null){
			Person person = new Person();
			person.setLastName(nonGCShippingGroup.getShippingAddress().getLastName());
			person.setFirstName(nonGCShippingGroup.getShippingAddress().getFirstName());
			person.setAddressLine1(nonGCShippingGroup.getShippingAddress().getAddress1());
			person.setAddressLine2(nonGCShippingGroup.getShippingAddress().getAddress2());
			person.setCity(nonGCShippingGroup.getShippingAddress().getCity());
			person.setState(nonGCShippingGroup.getShippingAddress().getState());
			if(null!= nonGCShippingGroup.getPropertyValue( "addressType" ) 
					&& DigitalStringUtil.isNotEmpty((String)nonGCShippingGroup.getPropertyValue( "addressType" ))
						&& ((String)nonGCShippingGroup.getPropertyValue( "addressType" )).equalsIgnoreCase(AddressType.MILITARY.getValue())){
					if(DigitalStringUtil.isEmpty(person.getState()) 
							&& DigitalStringUtil.isNotEmpty(region)){
						person.setState(region);
					}
					if(DigitalStringUtil.isEmpty(person.getCity())
							&& DigitalStringUtil.isNotEmpty(rank)){
						person.setCity(rank);
					}
			}
			
			person.setZipCode(nonGCShippingGroup.getShippingAddress().getPostalCode());
			iattr.setPerson(person);
			iattr.setMallPlazaName(commerceItem.getMallPlazaName());
		}
		iattr.setFulfillmentType(commerceItem.getShipType());
		Object recipientName = ELFunctions.getPropertyValue(commerceItem, "recipientName");
		if(recipientName!=null){
		iattr.setRecipientName((String)recipientName);
		}
		Object recipientEmail = ELFunctions.getPropertyValue(commerceItem, "recipientEmail");
		if(recipientName!=null){
		iattr.setRecipientEmail((String)recipientEmail);
		}
		
		Object senderName = ELFunctions.getPropertyValue(commerceItem, "senderName");
		if(senderName!=null){
		iattr.setSenderName((String)senderName);
		}
		Object senderEmail = ELFunctions.getPropertyValue(commerceItem, "senderEmail");
		if(senderEmail!=null){
		iattr.setSenderEmail((String)senderEmail);
		}
		return iattr;
	}
	
	public void markIncomplete( DigitalOrderImpl order ) {
		resetShoppingCart( order );
		expireGiftCardAuthorization( order );
		getRewardCertificateManager().unReserveRewardCertificates( order );
	}

	private void resetShoppingCart( DigitalOrderImpl order ) {
		if( shoppingCart != null ) {
			order.setState( StateDefinitions.ORDERSTATES.getStateValue( OrderStates.INCOMPLETE ) );
			order.setSubmittedDate( null );
			shoppingCart.setCurrent( order );
			shoppingCart.setLast( null );
		}
	}

	public boolean findTestOrder( DigitalOrderImpl order ) {

		if (getDswConstants().isEvaluateTestOrders() && order != null) {
			try {
				final RepositoryItem profile = profileTools.getProfileForOrder(order);
				final String orderEmailAddress = profileTools.getOperationalEmailAddress(profile, order);
				final String testEmailAddressPattern = getDswConstants().getTestEmailAddressPattern();
				if (orderEmailAddress != null && orderEmailAddress.endsWith(testEmailAddressPattern)) {
					final StringBuilder sb = new StringBuilder();
					sb.append("Test Order Match For Order ID: ");
					sb.append(order.getId()).append(", ");
					sb.append("E-Mail Address: ");
					sb.append(orderEmailAddress);
					logInfo(sb.toString());
					return true;
				}
			} catch (Exception e) {
				logError("Error occured when evaluating order for test email: " + e.getMessage(), e);
			}
		}
		return false;
	}
	
	public void updateProfile( DigitalOrderImpl order, Profile profile ) {
	
		Date currentDate = new java.util.Date();
		( (MutableRepositoryItem)profile ).setPropertyValue( "lastOrderDate", currentDate );
		final DigitalCreditCard creditCard = ((DigitalPaymentGroupManager)getPaymentGroupManager() ).findCreditCard( order );
		
		if (creditCard != null && !profile.isTransient()
				&& order.isCopyCreditCardToProfile()) {
			
			// If customer requests CC to be saved, check if the CC authorization
			// is successful and save only if so.
			boolean offLineOrder = false;
			List<?> ccStatusList = creditCard.getAuthorizationStatus();
			if (ccStatusList != null && !ccStatusList.isEmpty()) {
				Object ccStatusObj = ccStatusList.iterator().next();
				if (ccStatusObj != null
						&& ccStatusObj instanceof DigitalCreditCardStatus) {
					DigitalCreditCardStatus ccStatus = (DigitalCreditCardStatus) ccStatusObj;
					
					String fraudFlg = ccStatus.getFraudFlag();
					String status = ccStatus.getStatus();
					//Changed FraudFlag as per latest integration values from 0 to "Y"
					if ((DigitalStringUtil.isNotBlank(fraudFlg)
							&& fraudFlg.equalsIgnoreCase(DigitalCreditCardServiceStatus.FRAUD_FLAG_YES ))
								&& (DigitalStringUtil.isNotBlank(status) 
										&& status.equalsIgnoreCase(DigitalCreditCardServiceStatus.DECISION_OFFLINE))){
						if(isLoggingDebug()){
							logDebug("Setting offLineOrder flag to ::"+fraudFlg.equalsIgnoreCase(DigitalCreditCardServiceStatus.FRAUD_FLAG_YES ));
						}
						offLineOrder = true;
					}
				}
			}
			if (!offLineOrder 
					&& !isCreditCardPresentInProfile(profile,creditCard) 
						&& !getProfileTools().isCreditCardMaxLimitExceeded(profile)
							&& !getStartEndDateValidator().isCreditCardExpired(creditCard)) {
				if(isLoggingDebug()){
					logDebug("Copying card to profile since offLineOrder flag is ::"+offLineOrder);
				}
				copyCreditCardToProfile(profile, creditCard);
			}else{
				if(isLoggingDebug()){
					logDebug("Credit card not copied to profile since "
							+ " offLineOrder flag is :: "+offLineOrder
							+"  isCreditCardPresentInProfile(profile,creditCard) returned ::	" +isCreditCardPresentInProfile(profile,creditCard)
							+"  getProfileTools().isCreditCardMaxLimitExceeded(profile) ::	"+getProfileTools().isCreditCardMaxLimitExceeded(profile)
							+"  getStartEndDateValidator().isCreditCardExpired(creditCard) ::	"+getStartEndDateValidator().isCreditCardExpired(creditCard));
				}
			}
		}
	}
	
	
	/** Clean up profile Promotions.
	 * 
	 * @param profile */
	public void cleanUpProfilePromotions(Profile profile)
	{
		if (getDswConstants().isCleanUpPromotionsPostCheckout()) {
			profileTools.cleanPromotions( profile );
		}	
	}
	
	/** Validates for card uniqueness.
	 * 
	 * @param creditCard */
	private boolean isCreditCardPresentInProfile( Profile profile, CreditCard creditCard ) {
		return profileTools.isCreditCardPresentInProfile( profile, creditCard );
	}	
		
	/** Copies the credit cart to the current user's Profile and then sets it as the default credit card.
	 * 
	 * @param creditCard */
	private void copyCreditCardToProfile( Profile profile, CreditCard creditCard ) {
		profileTools.copyCreditCardToProfile( profile, creditCard );
	}		
	
	private DigitalPaymentGroupManager getPaymentGroupManager() {
		return (DigitalPaymentGroupManager)orderManager.getPaymentGroupManager();
	}
	
	


	public void setRewardCertificateManager(
			DigitalRewardsManager rewardCertificateManager) {
		this.rewardCertificateManager = rewardCertificateManager;
	}

	public void retainLastGiftCardAuthorizationStatus( final DigitalOrderImpl order ) {
		boolean hasGiftCards = false;
		for( final DigitalGiftCard giftCard : Iterables.filter( getPaymentGroupManager().getNonCreditCardPaymentGroups( order ), DigitalGiftCard.class ) ) {
			hasGiftCards = true;
			List<PaymentStatus> statuses = giftCard.getAuthorizationStatus();
			if( statuses != null && !statuses.isEmpty() ) {
				Iterables.retainAll( statuses, Arrays.asList( Iterables.getLast( statuses ) ) );
			}
		}
		if( hasGiftCards ) {
			try {
				orderManager.updateOrder( order );
			} catch( CommerceException e ) {
				if( isLoggingError() )
				logError( String.format( "Caught CommerceException attempting to retain gift last gift card authorization status for order: %1$s profile: %2$s", order.getId(), order.getProfileId() ), e );
			}
		}
	}

	private void expireGiftCardAuthorization( DigitalOrderImpl order ) {
		try {
			((DigitalPaymentManager)paymentManager).expireGiftCardAuthorization( order.getPaymentGroups() );
		} catch( CommerceException e ) {
			if( isLoggingError() )
				logError( String.format( "Caught CommerceException attempting to expire gift card authorizations for order %1$s on profile %2$s", order.getId(), order.getProfileId() ) );
		}
	}


	public void sendOptInOptOutData(DigitalOrderImpl orderPlaced, Profile orderPlacedBy) {
		try {
			if (profileTools.isLoggedIn(orderPlacedBy) || profileTools.isCookied(orderPlacedBy)) {
				if (isLoggingInfo()) {
					logInfo("Customer " + orderPlacedBy.getRepositoryId()
							+ " is either logged-in/cookied user, hence not doing anything.");
				}
			} else {
				ContactInfo contactInfo = this.getContactInfo(orderPlaced);
				if (null != contactInfo) {
					
					this.updateEmailSubscriber(contactInfo, orderPlacedBy.getRepositoryId(), orderPlaced.isEmailOptIn());
				} else {
					// should never happen. Just log and continue
					logError("Unable to get billing or shipping info, no calls to rewards for opt-in/opt-out");
				}

			}
		} catch (Exception ex) {
			logError("Exception while sending optInData " + ex.getMessage());
		}
	}

	/**
	 * 
	 * @param order
	 * @return
	 * @throws CommerceException
	 */
	private ContactInfo getContactInfo(DigitalOrderImpl order) throws CommerceException {
		ContactInfo contactInfo = null;
		List<atg.commerce.order.PaymentGroup> paymentGroupList = order.getPaymentGroups();
		if (paymentGroupList != null) {
			for (atg.commerce.order.PaymentGroup pg : paymentGroupList) {
				// If CC/PayPal tender take billing address, otherwise
				// get the first HG shipping address that has email and
				// phoneNumber
				if (pg instanceof DigitalCreditCard) {
					DigitalCreditCard creditCard = (DigitalCreditCard) pg;
					contactInfo = (ContactInfo) creditCard.getBillingAddress();
					if (isLoggingDebug()) {
						logDebug("Using CC billing address for updateEmailSubscriber.");
					}
				} else if (pg instanceof PaypalPayment) {
					PaypalPayment paypalPayment = (PaypalPayment) pg;
					contactInfo = (ContactInfo) paypalPayment.getBillingAddress();
					if (isLoggingDebug()) {
						logDebug("Using PayPal billing address for updateEmailSubscriber.");
					}
				} else {
					// As contact Info is copied to all HG shipping groups by
					// DigitalOrderContactInfoFormHandler
					// we can take the first HG Shipping group that has email
					// and phoneNumber
					List<HardgoodShippingGroup> hgShippingGroups = ((DigitalShippingGroupManager)orderManager.getShippingGroupManager())
							.getHardgoodShippingGroups(order);
					for (HardgoodShippingGroup hgShippingGroup : hgShippingGroups) {
						ContactInfo shipAddr = (ContactInfo) hgShippingGroup.getShippingAddress();
						if (shipAddr != null && DigitalStringUtil.isNotBlank(shipAddr.getPhoneNumber())
								&& DigitalStringUtil.isNotBlank(shipAddr.getEmail())) {
							contactInfo = new ContactInfo();
							contactInfo.setPhoneNumber(shipAddr.getPhoneNumber());
							contactInfo.setEmail(shipAddr.getEmail());
							if (isLoggingDebug()) {
								logDebug("Using GC shipping address with " + shipAddr.getEmail() + ", "
										+ shipAddr.getPhoneNumber() + " for updateEmailSubscriber.");
							}
							break;
						}
					}
				}
			}
		}
		return contactInfo;
	}
	/**
	 * 
	 * @param contact
	 * @param profileId
	 * @param optInFlag
	 */
	private void updateEmailSubscriber(ContactInfo contact, String profileId, boolean optInFlag) {
		try {
			RewardServiceRequest rewardsRequest = createRewardsRequest(contact, profileId, optInFlag);
			jmsClient.sendObjectMessage("queue/REWARDS_SUBSCRIPTION_Q", rewardsRequest, null, false);
		} catch (Exception ex) {
			logInfo("Exception while sending subscription request to queue  REWARDS_SUBSCRIPTION_Q ", ex);
		}
	}

	/**
	 * 
	 * @param billingAddr
	 * @param profileId
	 * @param emailOptIn
	 * @return
	 */
	private RewardServiceRequest createRewardsRequest(ContactInfo billingAddr, String profileId, boolean emailOptIn) {
		RewardServiceRequest btsRewardServiceRequest = new RewardServiceRequest();
		try {
			btsRewardServiceRequest.getPerson().setProfileID(profileId);

			// Add name info to request if available
			if (DigitalStringUtil.isNotBlank(billingAddr.getFirstName())) {
				btsRewardServiceRequest.getPerson().setFirstName(billingAddr.getFirstName());
			}
			if (DigitalStringUtil.isNotBlank(billingAddr.getLastName())) {
				btsRewardServiceRequest.getPerson().setLastName(billingAddr.getLastName());
			}
			// Add address info to request if available
			if (DigitalStringUtil.isNotBlank(billingAddr.getAddress1())) {
				btsRewardServiceRequest.getAddress().setAddress1(billingAddr.getAddress1());
			}
			if (DigitalStringUtil.isNotBlank(billingAddr.getAddress2())) {
				btsRewardServiceRequest.getAddress().setAddress2(billingAddr.getAddress2());
			}
			if (DigitalStringUtil.isNotBlank(billingAddr.getCity())) {
				btsRewardServiceRequest.getAddress().setCity(billingAddr.getCity());
			} else if (billingAddr instanceof DigitalContactInfo) {
				if (DigitalStringUtil.isNotBlank(((DigitalContactInfo) billingAddr).getRank())) {
					btsRewardServiceRequest.getAddress().setCity(((DigitalContactInfo) billingAddr).getRank());
				}
			} else if (billingAddr instanceof DigitalRepositoryContactInfo) {
				if (DigitalStringUtil.isNotBlank(((DigitalRepositoryContactInfo) billingAddr).getRank())) {
					btsRewardServiceRequest.getAddress().setCity(((DigitalRepositoryContactInfo) billingAddr).getRank());
				}
			}
			if (DigitalStringUtil.isNotBlank(billingAddr.getState())) {
				btsRewardServiceRequest.getAddress().setState(billingAddr.getState());
			} else if (billingAddr instanceof DigitalContactInfo) {
				if (DigitalStringUtil.isNotBlank(((DigitalContactInfo) billingAddr).getRegion())) {
					btsRewardServiceRequest.getAddress().setState(((DigitalContactInfo) billingAddr).getRegion());
				}
			} else if (billingAddr instanceof DigitalRepositoryContactInfo) {
				if (DigitalStringUtil.isNotBlank(((DigitalRepositoryContactInfo) billingAddr).getRegion())) {
					btsRewardServiceRequest.getAddress().setState(((DigitalRepositoryContactInfo) billingAddr).getRegion());
				}
			}
			if (DigitalStringUtil.isNotBlank(billingAddr.getCountry())) {
				btsRewardServiceRequest.getAddress().setCountry(billingAddr.getCountry());
			}
			if (DigitalStringUtil.isNotBlank(billingAddr.getPostalCode())) {
				btsRewardServiceRequest.getAddress().setPostCode(billingAddr.getPostalCode());
			}
			btsRewardServiceRequest.getContact().setEmail(billingAddr.getEmail());
			btsRewardServiceRequest.getContact().setPhone1(billingAddr.getPhoneNumber());
			String reciveEmailflag = dswConstants.getEmailUpdateNo();
			if (!emailOptIn) {
				reciveEmailflag = dswConstants.getEmailUpdateYes();
			}
			btsRewardServiceRequest.getFlags().setNoEMail(reciveEmailflag);
			btsRewardServiceRequest.getFlags().setOptOutFashionEmail(reciveEmailflag);

			btsRewardServiceRequest.setEmailSource(dswConstants.getRewardsEmailSource());
			btsRewardServiceRequest.getFlags().setOptOutFashionEmail(reciveEmailflag);
			btsRewardServiceRequest.setFrequencyFashionEmail(dswConstants.getFashionFrequencyEmailFlag());
		} catch (Exception ex) {
			logError("Exception while creating reqrd request for opt-in/opt-out udpates post order placement "
					+ ex.getMessage());
		}
		return btsRewardServiceRequest;
	}


	/**
	 * @param request
	 */
	public void updateSendOrderSubscriber(RewardsSendOrderData request) {
		try {
			RewardsSendOrderRequest sendOrderRequest=((DigitalOrderTools)getOrderTools()).createRewardsSendOrderRequest(request);
			jmsClient.sendObjectMessage("queue/REWARDS_RT_SEND_ORDER_Q", sendOrderRequest, null, false);
		} catch (Exception ex) {
			logInfo("Exception while sending Order request to queue  REWARDS_RT_SEND_ORDER_Q ", ex);
		}
	}
	

}
