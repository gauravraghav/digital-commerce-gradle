package com.digital.commerce.services.order.listner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.order.processor.domain.payload.ReturnReceivedShipmentPayload;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderlineAttributesDTO;

public class DigitalReturnReceivedShipmentListner extends DigitalOrderStatusListener {
	protected static final String CLASSNAME = DigitalReturnReceivedShipmentListner.class.getName();

	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected HashMap<String, Object> handleEmailNotification(
			OrderStatusPayload payLoad,
			DigitalOrderImpl order,
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {
		// TODO Auto-generated method stub
		HashMap<String, Object> extraAttributes = new HashMap<>();
		
		ReturnReceivedShipmentPayload returnReceivedShipmentPayload = (ReturnReceivedShipmentPayload)payLoad;
	    
	    extraAttributes.put("pickupPerson",returnReceivedShipmentPayload.getPersonInfoShipTo().getFirstName()+
				" "+returnReceivedShipmentPayload.getPersonInfoShipTo().getLastName());
	    
	    String customerFirstName = returnReceivedShipmentPayload.getPersonInfoBillTo().getFirstName();

		if (DigitalStringUtil.isEmpty(customerFirstName)) {
			if (DigitalStringUtil.isEmpty(customerFirstName) && returnReceivedShipmentPayload.getPersonInfoShipTo() != null) {
				customerFirstName = returnReceivedShipmentPayload.getPersonInfoShipTo().getFirstName();
				if (DigitalStringUtil.isEmpty(customerFirstName) && returnReceivedShipmentPayload.getPersonInfoMarkFor() != null) {
					customerFirstName = returnReceivedShipmentPayload.getPersonInfoMarkFor().getFirstName();
				}
				
				if(DigitalStringUtil.isEmpty(customerFirstName) && returnReceivedShipmentPayload.getPersonInfoShipTo() != null){
					customerFirstName = returnReceivedShipmentPayload.getPersonInfoShipTo().getFirstName();
				}
				
				if(DigitalStringUtil.isEmpty(customerFirstName)){
					customerFirstName = returnReceivedShipmentPayload.getCustomerFirstName();
				}
			}
		}
		
	    extraAttributes.put("customerFirstName",customerFirstName);
		
	    consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.SHIP, itemsByFullfilmentType, extraAttributes);
	    consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOPIS, itemsByFullfilmentType, extraAttributes);
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOSTS, itemsByFullfilmentType, extraAttributes);
		
		//get mall plaza name
		HashMap<String, OrderlineAttributesDTO> lines = (HashMap<String, OrderlineAttributesDTO>)extraAttributes.get("orderLines");
		
		if(lines != null){
			Map.Entry<String,OrderlineAttributesDTO> entry=lines.entrySet().iterator().next();
			extraAttributes.put("mallPlazaName",entry.getValue().getMallPlazaName());
		}
		
		return extraAttributes;
	}

	@Override
	protected String getOrderNo(OrderStatusPayload payLoad) {
		if(payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null ){
			return payLoad.getOmniOrderUpdate().getOrder().getOrderNo();
		}
		return null;
	}

	@Override
	protected String getEmailTo(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		String emailId = ((ReturnReceivedShipmentPayload)payLoad).getPersonInfoBillTo().getEmailId();
		if(DigitalStringUtil.isBlank(emailId)){
			emailId = ((ReturnReceivedShipmentPayload)payLoad).getCustomerEMailID();
		}
		
		return emailId;
	}

	@Override
	protected List<OrderLine> getOrderLines(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((ReturnReceivedShipmentPayload)payLoad).getOrderLines();
	}
	
}