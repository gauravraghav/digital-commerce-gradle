package com.digital.commerce.services.pricing.reprice;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.digital.commerce.services.promotion.DigitalPromotionTools;

import atg.commerce.claimable.ClaimableTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.commerce.promotion.PromotionTools;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

import static com.digital.commerce.services.pricing.reprice.DigitalPromotionConstants.PROMOTIONS;

/** This class is added for the promotion exclusivity enhancement. This class
 * will check if the promotions added for an order are compatible to be combined
 * together to enable the user to use the promotion coupons.
 * 
 * @author Professional Access */
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class PromotionValidationService extends GenericService {

	/** Property to hold 'promotionTools'. */
	private PromotionTools		promotionTools;

	/** Property to hold 'userRepository'. */
	private MutableRepository	userRepository;

	/** Property to hold 'claimableTools'. */
	private ClaimableTools		claimableTools;

	/** Property to hold profile. */
	private Profile				profile;

	/** Property to hold mSubmittedPromoExclusionList. */
	private Set<RepositoryItem>	submittedPromoExclusionList	= null;

	/** Property to hold mSubmittedPromoInclusionList. */
	private Set<RepositoryItem>	submittedPromoInclusionList	= null;

	/** Property to hold mSubmittedPromotionItem. */
	private RepositoryItem		submittedPromotionItem			= null;

	/** This is main method we are calling for promotion validation logic.
	 * This method will call the method which performs the actual promotion validity check.
	 * 
	 * @param couponCode : coupon code of the current promotion applied by the user.
	 * @return success/error message related to the compatibility
	 * @throws RepositoryException */
	public String promotionValidation( String pCouponCode ) throws RepositoryException {
		if( isLoggingDebug() ) {
			logDebug( "Begin PromotionValidationService.promotionValidation() method ... " );
			logDebug( "Entering to PromotionValidationService.promotionValidation()method and couponCode is" + pCouponCode );
		}
		String promoValidationMsg = null;
//		RepositoryItem grantedPromotionItem = null;
		List<RepositoryItem> existingPromoItems = new ArrayList<>();
		Set<RepositoryItem> grantedCoupons = null;
		RepositoryItem couponItem = null;
		List<RepositoryItem> grantedCouponsList = null;
		RepositoryItem SubmittedCouponItem = getCoupon( pCouponCode );
		if( SubmittedCouponItem != null ) {
			try{
			}
			catch (Exception ex){
				logError(ex.getLocalizedMessage());
			}
		}
		grantedCoupons = getGrantedCoupons( pCouponCode );
		grantedCouponsList = new ArrayList<>(grantedCoupons);
		if( isLoggingDebug() ) {
			logDebug( "Inside promotionValidation() --> GrantedCoupons Are -- " + grantedCouponsList );
		}
		if( grantedCouponsList != null && grantedCouponsList.size() > 0 ) {

			for( int i = 0; i < grantedCouponsList.size(); i++ ) {
				couponItem = (RepositoryItem)grantedCouponsList.get( i );
				if( couponItem != null ) {
					//no longer only have one-to-one relationships with coupons/promotions
//					grantedPromotionItem = (RepositoryItem)couponItem.getPropertyValue( DSWPromotionConstants.PROMOTION );
					Set promotions = (Set)couponItem.getPropertyValue("promotions");
					if (promotions != null){
						Iterator iterPromos = promotions.iterator();
						while(iterPromos.hasNext()){
							RepositoryItem grantedPromotionItem = (RepositoryItem)iterPromos.next();
							if( isLoggingDebug() ) {
								logDebug( "Granted PromotionItem : " + grantedPromotionItem );
							}
							if( grantedPromotionItem != null ) {
								existingPromoItems.add( grantedPromotionItem );
							}
						}
					}
				}
			}
//			promoValidationMsg = checkPromoValidation( existingPromoItems, submittedPromoItem );
			if( isLoggingDebug() ) {
				logDebug( "Promotion Validation msg : " + promoValidationMsg );
			}
		}
		if( isLoggingDebug() ) {
			logDebug( "exiting from PromotionValidationService.promotionValidation()" );
		}
		return promoValidationMsg;
	}

	/** This method checks the promotion validation.
	 * This method will retrieve the inclusion and exclusion list of the current promotion.
	 * It will then iterate through the list of already granted promotions, to retrieve the flag (exclude_all_promo_flag) of those
	 * promotions and
	 * check for compatibility of the existing coupon being iterated with the current coupon.
	 * 
	 * @param pExistingPromoItems : List of the already granted promotions
	 * @param pSubmittedPromoItem : Object of current promotion
	 * @return success/error message related to the compatibility
	 * @throws RepositoryException */
	public String checkPromoValidation( List<RepositoryItem> pExistingPromoItems, RepositoryItem pSubmittedPromoItem ) throws RepositoryException {
		if( isLoggingDebug() ) {
			logDebug( "Begin PromotionValidationService.checkPromoValidation() method ... " );
			logDebug( "Inside checkPromoValidation()method and existingPromoItems are" + pExistingPromoItems );
			logDebug( "Inside checkPromoValidation()method and submittedPromoItems are" + pSubmittedPromoItem );
		}
		Set<RepositoryItem> existingPromoInclusionList = null;
		Set<RepositoryItem> existingPromoExclusionList = null;
		String promoValidationMsg = null;
		RepositoryItem existingPromoItem = null;
		if( pSubmittedPromoItem != null ) {
			this.submittedPromotionItem = pSubmittedPromoItem;
			setSubmittedPromoExclusionList( (Set<RepositoryItem>)submittedPromotionItem.getPropertyValue( DigitalPromotionConstants.EXCLUDE_PROMOS ) );
			setSubmittedPromoInclusionList( (Set<RepositoryItem>)submittedPromotionItem.getPropertyValue( DigitalPromotionConstants.INCLUDE_PROMOS ) );
		}

		for( int i = 0; i < pExistingPromoItems.size(); i++ ) {
			existingPromoItem = (RepositoryItem)pExistingPromoItems.get( i );
			if( ( (Boolean)existingPromoItem.getPropertyValue( DigitalPromotionConstants.ENABLE_ALL_PROMO_EXCLUSION ) ).booleanValue() ) {

				existingPromoInclusionList = (Set<RepositoryItem>)existingPromoItem.getPropertyValue( DigitalPromotionConstants.INCLUDE_PROMOS );

				if( existingPromoInclusionList == null || existingPromoInclusionList.isEmpty() ) {
					if( isLoggingError() ) {
						logError( "promotion cannot be combinable." );
					}
					return promoValidationMsg = DigitalPromotionConstants.NOT_COMBINABLE_1;
				} else if( existingPromoInclusionList.contains( submittedPromotionItem ) ) {
					promoValidationMsg = checkCompatibilityWithCurrentCoupon( existingPromoItem );
				} else {
					if( isLoggingError() ) {
						logError( "promotion cannot be used together." );
					}
					promoValidationMsg = DigitalPromotionConstants.NOT_USED_TOGETHER_1;
				}
			} else {
				existingPromoExclusionList = (Set<RepositoryItem>)existingPromoItem.getPropertyValue( DigitalPromotionConstants.EXCLUDE_PROMOS );
				if( existingPromoExclusionList != null ) {
					if( existingPromoExclusionList.contains( submittedPromotionItem ) ) {
						if( isLoggingError() ) {
							logError( "promotion cannot be used together" );
						}
						return promoValidationMsg = DigitalPromotionConstants.NOT_USED_TOGETHER_2;
					}
				}
				promoValidationMsg = checkCompatibilityWithCurrentCoupon( existingPromoItem );
			}
			if( !DigitalPromotionConstants.SUCCESS.equals( promoValidationMsg ) ) {
				if( isLoggingDebug() ) {
					logDebug( "Promotion Validation Msg is : " + promoValidationMsg );
				}
				break;
			}
		}
		if( isLoggingDebug() ) logDebug( "End of PromotionValidationService.checkPromoValidation() method ... " );
		return promoValidationMsg;
	}

	/** Determines the reverse compatibility of the coupons,that of the submitted coupon with each of the coupon(s)
	 * in the granted coupon list associated with the user profile.
	 * 
	 * 
	 * @param grantedPromotionId
	 * @return String */
	public String checkCompatibilityWithCurrentCoupon( RepositoryItem pGrantedPromotionId ) {
		if( isLoggingDebug() ) {
			logDebug( "Begin PromotionValidationService.checkCompatibilityWithCurrentCoupon() method ... " );
			logDebug( "Inside checkCompatibilityWithCurrentCoupon()method and grantedPromotionId are" + pGrantedPromotionId );
		}

		if( ( (Boolean)submittedPromotionItem.getPropertyValue( DigitalPromotionConstants.ENABLE_ALL_PROMO_EXCLUSION ) ).booleanValue() ) {
			if( getSubmittedPromoInclusionList().contains( pGrantedPromotionId ) ) {
				if( isLoggingDebug() ) {
					logDebug( "promotion added successfully" );
				}
				return DigitalPromotionConstants.SUCCESS;
			} else {
				if( getSubmittedPromoInclusionList() == null || getSubmittedPromoInclusionList().isEmpty() ) {
					if( isLoggingError() ) {
						logError( "promotion cannot be combined." );
					}
					return DigitalPromotionConstants.NOT_COMBINABLE_1;
				} else {
					if( isLoggingError() ) {
						logError( "promotion cannot be used together." );
					}
					return DigitalPromotionConstants.NOT_USED_TOGETHER_1;
				}
			}
		} else {
			if( getSubmittedPromoExclusionList() != null ) {
				if( getSubmittedPromoExclusionList().contains( pGrantedPromotionId ) ) {
					if( isLoggingError() ) {
						logError( "promotion can not be combinable." );
					}
					return DigitalPromotionConstants.NOT_COMBINABLE_2;
				}
			}
			if( isLoggingDebug() ) {
				logDebug( "promotion added successfully" );
			}
			return DigitalPromotionConstants.SUCCESS;
		}
	}

	/** This method is used for getting granted coupons list associated with the user profile.
	 * This list will return all the promotions already applied by the user.
	 * 
	 * @param couponCode
	 * @return Set
	 * @throws RepositoryException */
	public Set<RepositoryItem> getGrantedCoupons( String pCouponCode ) throws RepositoryException {
		if( isLoggingDebug() ) {
			logDebug( "Begin PromotionValidationService.getGrantedCoupons() method ... " );
			logDebug( "Inside getGrantedCoupons()method and couponCode is" + pCouponCode );
		}
		Profile profile = getProfile();
		String profileId = (String)profile.getPropertyValue( DigitalPromotionConstants.ID );
		if( isLoggingDebug() ) {
			logDebug( "PromotionValidationService.getGrantedCoupons ::Fetching Granted Coupons for  id " + profileId );
		}
		MutableRepositoryItem userProfile;
		userProfile = (MutableRepositoryItem)this.getUserRepository().getItemForUpdate( profileId, DigitalPromotionConstants.USER );
		DigitalPromotionTools pTools = (DigitalPromotionTools)getPromotionTools();
		Set<RepositoryItem> grantedCoupons = (Set<RepositoryItem>)userProfile.getPropertyValue( pTools.getCurrentCouponsProperty() );
		if( isLoggingDebug() ) logDebug( "End of PromotionValidationService.getGrantedCoupons() method ... " );
		return grantedCoupons;
	}

	/** This method is used for retrieving the promotion associated with applied coupon
	 * 
	 * @param couponCode
	 * @return RepositoryItem
	 * @throws RepositoryException */
	public RepositoryItem getCoupon( String pCouponCode ) throws RepositoryException {
		if( isLoggingDebug() ) {
			logDebug( "Begin PromotionValidationService.getCoupon() method ... " );
			logDebug( "Inside getCoupon()method and couponCode is" + pCouponCode );
		}
		ClaimableTools cTools = getClaimableTools();
		Repository cRepository = cTools.getClaimableRepository();
		RepositoryItem coupon = cRepository.getItem( pCouponCode, cTools.getClaimableItemDescriptorName() );
		if( isLoggingDebug() ) logDebug( "End of PromotionValidationService.getCoupon() method ... " );
		return coupon;
	}

	/** This method for checking if the promotion is Availed or not for the order it is being applied to.
	 * This method is used to throw an error message to the end user in case the promotion is not valid for the order.
	 * This method will retrieve item, shipping and order promotions and
	 * check if the current promotion being applied is a part of the pricing adjustments.
	 * 
	 * @param pOrderItem : current order
	 * @param pCouponCode : coupon code being applied
	 * @return boolean value : success/failure status of the coupon being applied to the order.
	 * @throws RepositoryException */
	public boolean isPromotionAvailed( Order pOrderItem, String pCouponCode ) throws RepositoryException {

		if( isLoggingDebug() ) logDebug( "Begin PromotionValidationService.isPromotionAvailed() method ... " );

		RepositoryItem submittedCouponItem = getCoupon( pCouponCode );

		if( isLoggingDebug() ) logDebug( "submittedCouponItem.." + submittedCouponItem );
		String currentPromoId = null;
		String promoId = null;
		if( submittedCouponItem != null ) {
			
			if( submittedCouponItem != null && submittedCouponItem.getItemDescriptor().hasProperty(PROMOTIONS)) {
				RepositoryItem currentPromoItem = null;
				Set promotions = (Set) submittedCouponItem.getPropertyValue(PROMOTIONS);
				if (promotions != null){
					Iterator promoIter = promotions.iterator();
					if(promoIter.hasNext()){
						currentPromoItem = (RepositoryItem)promoIter.next();
					}
				}
				if( currentPromoItem != null ) {
					currentPromoId = (String)currentPromoItem.getRepositoryId();
					if( isLoggingDebug() ) logDebug( "currentPromoId is..." + currentPromoId );
				} else {
					return false;
				}
			}			
		}

		OrderPriceInfo orderPriceInfo = (OrderPriceInfo)pOrderItem.getPriceInfo();
		List<RepositoryItem> orderAdjustments = (List<RepositoryItem>)orderPriceInfo.getAdjustments();
		if( isLoggingDebug() ) {
			logDebug( "retrieving orderAdjustments" + orderAdjustments );
		}
		PricingAdjustment adjId = null;
		if( orderAdjustments != null ) {

			Iterator<RepositoryItem> orderAdjustmentsItr = orderAdjustments.iterator();
			while( orderAdjustmentsItr.hasNext() ) {
				adjId = (PricingAdjustment)orderAdjustmentsItr.next();

				if( adjId.getPricingModel() != null ) {
					promoId = (String)adjId.getPricingModel().getRepositoryId();

					if( currentPromoId.equals( promoId ) ) {
						if( isLoggingDebug() ) {
							logDebug( "Current Promotion is of Type Order promo " );
						}
						return true;
					}
				}
			}
		}

		ShippingGroup shippingGroup = null;
		ShippingPriceInfo shippingPriceInfo = null;
		List<RepositoryItem> shippingAdjustments = null;
		List<RepositoryItem> orderShippingGroups = (List<RepositoryItem>)pOrderItem.getShippingGroups();

		if( orderShippingGroups != null ) {

			Iterator<RepositoryItem> orderShippingItr = orderShippingGroups.iterator();
			while( orderShippingItr.hasNext() ) {
				shippingGroup = (ShippingGroup)orderShippingItr.next();

				shippingPriceInfo = (ShippingPriceInfo)shippingGroup.getPriceInfo();

				shippingAdjustments = (List<RepositoryItem>)shippingPriceInfo.getAdjustments();

				Iterator<RepositoryItem> shippingAdjItr = shippingAdjustments.iterator();
				while( shippingAdjItr.hasNext() ) {
					adjId = (PricingAdjustment)shippingAdjItr.next();

					if( adjId.getPricingModel() != null ) {
						promoId = (String)adjId.getPricingModel().getRepositoryId();

						if( currentPromoId.equals( promoId ) ) {
							if( isLoggingDebug() ) {
								logDebug( "Current Promotion is of Type Shipping promo " );
							}
							return true;
						}
					}
				}
			}
		}

		List<RepositoryItem> orderCommerceItems = (List<RepositoryItem>)pOrderItem.getCommerceItems();

		CommerceItem commerceItem = null;
		ItemPriceInfo commerceItemPriceInfo = null;
		List<RepositoryItem> itemAdjustments = null;
		Iterator<RepositoryItem> itemAdjustmentsItr = null;
		if( orderCommerceItems != null ) {
			Iterator<RepositoryItem> orderCommerceItemsItr = orderCommerceItems.iterator();

			while( orderCommerceItemsItr.hasNext() ) {
				commerceItem = (CommerceItem)orderCommerceItemsItr.next();

				commerceItemPriceInfo = (ItemPriceInfo)commerceItem.getPriceInfo();

				itemAdjustments = (List<RepositoryItem>)commerceItemPriceInfo.getAdjustments();

				itemAdjustmentsItr = itemAdjustments.iterator();
				while( itemAdjustmentsItr.hasNext() ) {
					adjId = (PricingAdjustment)itemAdjustmentsItr.next();

					if( adjId.getPricingModel() != null ) {
						promoId = (String)adjId.getPricingModel().getRepositoryId();

						if( currentPromoId.equals( promoId ) ) {

							if( isLoggingDebug() ) {
								logDebug( "Current Promotion is of Type Item Promo " );
							}
							return true;
						}
					}
				}
			}
		}
		if( isLoggingDebug() ) {
			logDebug( "End of PromotionValidationService.isPromotionAvailed() method ... " );
		}

		if( isLoggingDebug() ) {
			logDebug( "Sorry Your Promotion cannot be part of this ORDER :" + pOrderItem.getId() );
		}
		return false;
	}
}
