package com.digital.commerce.services.order;

import atg.commerce.fulfillment.SubmitOrder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalSubmitOrder extends SubmitOrder {

	private static final long serialVersionUID = 4387407875909337773L;
	private String locale;
	private double orderTotal;
}
