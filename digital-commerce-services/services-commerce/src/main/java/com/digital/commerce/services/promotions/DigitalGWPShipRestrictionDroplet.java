package com.digital.commerce.services.promotions;

import java.io.IOException;

import javax.servlet.ServletException;

import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.services.promotion.DigitalGWPManager;

import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.Order;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalGWPShipRestrictionDroplet extends DynamoServlet {
	
	private static final String			OUTPUT						= "output";
	private static final String			SHIPRESTRICTION					= "shipRestriction";
	private Order						order;
	private DigitalGWPManager 				gwpManager;

	private DigitalBaseConstants dswConstants;	
	private CommerceItemManager commerceItemManager;
	/** 
	 * Finds if an order contains a GWP item, if yes it proceeds to verify if the order
	 * contains atleast a commerce item with price greater than 0 and shipped to home. 
	 */	
	public void service( DynamoHttpServletRequest req, DynamoHttpServletResponse res ) throws ServletException, IOException {
		
		Order order = getOrder();
		boolean shipRestriction = getGwpManager().checkShippingRestriction(order);
		shipRestriction = shipRestriction || (!getDswConstants().isPaTool() && getGwpManager().verifyShipRestriction(order));
		req.setParameter( SHIPRESTRICTION,shipRestriction );
		req.serviceParameter(OUTPUT, req, res);
	
	}
}
