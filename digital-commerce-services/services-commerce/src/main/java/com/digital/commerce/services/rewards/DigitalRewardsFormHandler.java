package com.digital.commerce.services.rewards;

import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.dtm.UserTransactionDemarcation;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.domain.ResponseWrapper;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.DigitalProfileConstants;
import com.digital.commerce.integration.reward.domain.BirthdayGift;
import com.digital.commerce.integration.reward.domain.Person;
import com.digital.commerce.integration.reward.domain.RewardServiceRequest;
import com.digital.commerce.integration.reward.domain.Shopfor;
import com.digital.commerce.services.i18n.DigitalCustomDateFormatter;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.profile.rewards.LoyaltyCertificate;
import com.digital.commerce.services.profile.vo.Offer;
import com.digital.commerce.services.promotion.DigitalPromotionTools;
import com.digital.commerce.services.rewards.domain.RetrieveRewardDetailsResponse;
import com.digital.commerce.services.rewards.domain.RewardsAddShopWithoutACardRequest;
import com.digital.commerce.services.rewards.domain.RewardsAddShopWithoutACardResponse;
import com.digital.commerce.services.rewards.domain.RewardsAddUpdateGiftResponse;
import com.digital.commerce.services.rewards.domain.RewardsAddUpdateShopforResponse;
import com.digital.commerce.services.rewards.domain.RewardsCharitiesResponse;
import com.digital.commerce.services.rewards.domain.RewardsDonateCertsRequest;
import com.digital.commerce.services.rewards.domain.RewardsDonateCertsResponse;
import com.digital.commerce.services.rewards.domain.RewardsGiftItem;
import com.digital.commerce.services.rewards.domain.RewardsIncentivesResponse;
import com.digital.commerce.services.rewards.domain.RewardsIssueCertsRequest;
import com.digital.commerce.services.rewards.domain.RewardsIssueCertsResponse;
import com.digital.commerce.services.rewards.domain.RewardsPointsBankingRequest;
import com.digital.commerce.services.rewards.domain.RewardsPointsBankingResponse;
import com.digital.commerce.services.rewards.domain.RewardsRetrieveBirthdayGiftsResponse;
import com.digital.commerce.services.rewards.domain.RewardsRetrievePointsOffersCertificatesRequest;
import com.digital.commerce.services.rewards.domain.RewardsRetrievePointsOffersCertificatesResponse;
import com.digital.commerce.services.rewards.domain.RewardsRetrieveRewardCertificatesResponse;
import com.digital.commerce.services.rewards.domain.RewardsRetrieveShopforResponse;
import com.digital.commerce.services.rewards.domain.RewardsShopforItem;
import com.digital.commerce.services.rewards.domain.RewardsSubscribeEmailFooterRequest;
import com.digital.commerce.services.rewards.domain.RewardsSubscribeEmailFooterResponse;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.time.DateUtils;

/**
 * @startuml
 *
 * title: <u> Create Birthday Gift Item Sequence Diagram </u>
 *
 * participant Client as UI << UI or Mobile App >> #orange
 *
 * box "<u>Internal Service Components</u>" #LightGrey
 * participant DigitalRewardsActor as A << (C,#ADD1B2) >>
 * participant DigitalRewardsFormHandler as B << (C,#ADD1B2) >>
 * participant DigitalRewardsManager as C << (C,#ADD1B2) >>
 * participant DigitalBirthdayGiftTools as E << (C,#ADD1B2) >>
 * end box
 *
 * database DSW_CORE as F #99FF99
 *
 * box "<u>External Service Components</u>" #LightBlue
 * participant BtsRewardService as D << (C,#ADD1B2) >>
 * participant IntegrationServiceClient as G << (C,#ADD1B2) >>
 * participant RewardWebService as H << (W,#ADD1B2) >>
 * end box
 *
 * autonumber "<b>[000]"
 * UI -> A: createGift JSON Request
 * activate A #FFBBBB
 *
 * A -> B: << handleCreateGift >>
 * activate B #FFBBBB
 *
 * B -> B: Validate the input request.\n << validateGiftItemRequest >>
 * activate B #DarkSalmon
 *
 * B -> C: Create new Birthday Gift.\n << createGift >>
 * activate C #FFBBBB
 *
 * C -> D: << addBirthdayGift >>
 * activate D #FFBBBB
 *
 * D -> D: check if service is online \n << doRunService >>
 * activate D #FFBBBB
 *
 * alt if rewards service and addBirthdayGift method is online
 *
 * 	D -> G: << getMarshalledStringFromObject >>
 * 	activate G #FFBBBB
 *
 * 	G -> D: Request created
 * 	deactivate G #FFBBBB
 *
 * 	D -> G: << processHttpRequest >>
 * 	activate G #FFBBBB
 *
 *  G -> H: << addBirthdayGiftForFriend >>
 *  activate H #FFBBBB
 *
 *  	alt if call is successful
 *  		H -> G: on call complete CreateGift Response
 *  	else if communication error
 *  		H -> G: DigitalIntegrationSystemException
 *  	end
 *
 *  deactivate H #FFBBBB
 *
 *  	alt if call is successful
 *  		G -> D: on success Response
 *  	else if communication error
 *  		G -> D: DigitalIntegrationSystemException
 *  	end
 *
 *  deactivate G #FFBBBB
 *
 *  D -> G: << getUnmarshalledObjectFromHttpServiceResponse >>
 *  activate G #FFBBBB
 *
 * 	G -> D: Response
 * 	deactivate G #FFBBBB
 *
 *  D -> C: Add Birthday Gift Response
 *  deactivate C
 *  deactivate D
 *
 *  alt if addBirthdayGift response is not null
 *
 *  	C -> C: << isSyncBDayGiftsPostAddFromRewards >>
 *    activate C #FFBBBB
 *
 * 		alt if isSyncBDayGiftsPostAddFromRewards is true
 *
 * 	    C -> C: Return the count of saved gifts << mergeBirthdayGiftsUsingLoyaltyServiceToProfile >>
 * 	    activate C #FFBBBB
 *
 * 	    C -> C: << retrieveRewardBirthdayGiftsByProfile >>
 * 	    activate C #FFBBBB
 *
 * 	    C -> D: << retrieveBirthdayGifts >>
 *      activate C #FFBBBB
 *
 *      D -> D: check if service is online \n << doRunService >>
 *      activate D #FFBBBB
 *
 *  	alt if rewards service and retrieveBirthdayGifts method is online
 * 			D -> G: << getMarshalledStringFromObject >>
 * 			activate G #FFBBBB
 * 			G -> D: Request created
 * 			deactivate G #FFBBBB
 * 			D -> G: << processHttpRequest >>
 * 			activate G #FFBBBB
 * 			G -> H: << retrieveBirthdayGifts >>
 * 			activate H #FFBBBB
 * 				alt if call is successful
 * 					H -> G: on call complete retrieveBirthdayGifts Response
 * 				else if communication error
 * 					H -> G: on error DigitalIntegrationSystemException
 * 				end
 * 			deactivate H #FFBBBB
 * 				alt if call is successful
 * 					G -> D: on success Response
 * 				else if communication error
 * 					G -> D: on error DigitalIntegrationSystemException
 * 				end
 * 			deactivate G #FFBBBB
 * 			D -> G: << getUnmarshalledObjectFromHttpServiceResp
 * 			activate G #FFBBBB
 * 			G -> D: Response created
 * 			deactivate G
 * 		    D -> C: Response returned
 * 		else if communication error
 * 			D -> C: on error DigitalIntegrationBusinessException
 * 		end
 *      deactivate C
 *      deactivate D
 *
 *      alt if retrieveBirthdayGifts response is not null
 * 	        C -> E: << mergeBirthDayGiftsToProfile >>
 * 	        activate E #FFBBBB
 *
 * 	        E -> F: on save << updateItem >>
 * 	        activate F #FFBBBB
 *
 * 	        F --> E: DB update completed
 * 	        deactivate F
 *
 * 	        E --> C: Merge of BDay Gifts done
 * 	        deactivate E
 *      end
 * 	    D --> C: Birthday Gift Created
 *
 *     else If Offline Service inactive exception
 *
 * 	    D -> C: on error DigitalIntegrationInactiveException
 * 	    deactivate G
 *
 *  else update DB with transactional data (requested and processed)
 *  		C -> C: << populateRewardsGiftItem >>
 *  		activate C #FFBBBB
 *
 *  		C -> E: << addOrUpdateBDayGiftForProfile >>
 *  		activate E #FFBBBB
 *
 *      E -> F: on save << updateItem >>
 *      activate F #FFBBBB
 *
 *      F --> E: DB update completed
 *      deactivate F
 *
 *      E --> C: Save of BDay Gift done
 *      deactivate E
 *
 *      alt if no exception
 *
 *      C -> E: << retrieveBirthDayGiftsFromProfile >>
 *      activate E #FFBBBB
 *
 *      E -> F: lookup << findItem >>
 *      activate F #FFBBBB
 *
 *      F --> E: Saved BDay Gifts if found returned
 *      deactivate F
 *
 * 			E --> C: Saved BDay Gift Count Returned if not empty
 * 		  deactivate E
 *
 *      else return RewardsAddUpdateGiftResponse with error
 *      end
 *      deactivate C
 *  end
 *  deactivate C
 *  end
 *
 *  deactivate D
 *  deactivate C
 * else set RewardsAddUpdateGiftResponse with errors
 * end
 *
 * C --> B: Birthday Gift Created or failed
 * deactivate C
 *
 * deactivate B
 *
 * B --> A: true if BDay Gift was created else false
 * deactivate B
 *
 * A -> UI: createGift JSON Response
 * deactivate A
 *
 * @enduml
 */
@Getter
@Setter
public class DigitalRewardsFormHandler extends GenericFormHandler {
	private static final String CLASSNAME = DigitalRewardsFormHandler.class.getName();

	private static final String SERVICE_ERROR_CODE = "ServiceError";
	private static final String ERR_INVALID_EMAIL = "invalidEmailAddress";
	private static final String ERR_NO_EMAIL = "missingRecipientsEmailAdress";
	private static final String ERR_NO_FIRSTNAME = "missingRecipientsFirstName";
	private static final String ERR_NO_SHOPFOR_NAME = "missingShopforName";
	private static final String ERR_INVALID_FIRSTNAME = "invalidRecipientsFirstName";
	private static final String ERR_INVALID_SHOPFOR_NAME = "invalidShopforName";
	private static final String ERR_MISSING_SHOPFOR_SIZE = "missingShopforSize";
	private static final String ERR_MISSING_SHOPFOR_RELATIONSHIP = "missingShopforRelationship";
	private static final String ERR_MISSING_SHOPFOR_GENDER = "missingShopforGender";
	private static final String ER_INVALID_BIRTH_DAY_MONTH = "invalidDOB";
	private static final String ER_INVALID_BIRTH_DAY_SHOPFOR = "invalidDOBshopFor";
	private static final String DEFAULT_YEAR = "2004";
	private static final String ERR_MAX_BDAY_GIFTS_LIMIT = "exceededBirthDayGiftLimit";
	private static final String ERR_MAX_SHOPFOR_LIMIT = "exceededShopforLimit";
	private static final String ERR_DIMENSION_SHOPFOR = "dimensionShopforLimit";
	private static final String ERR_NO_GIFT_ID = "missingGiftId";
	private static final String ERR_NO_SHOPFOR_ID = "missingShopforId";
	private static final String ERR_GENERIC_BIRTHDAY_GIFT = "rewardsBDayGiftServiceError";
	private static final String ERR_GENERIC_SHOPFOR					= "rewardsShopForServiceError";
	private static final String ERR_NO_CERTS_TO_DONATE = "missingDonationCertificates";
	private static final String ERR_NO_CHARITY_ID = "missingCharityId";
	private static final String ERR_NO_CERT_DENOMINATION = "missingCertDenomination";

	private static final String REWARDS_CERTIFICATE_DENOMINATION = "rewardsCertificateDenomination";
	private static final String REWARDS_POINTS_HISTORY 	= "rewardsPointsHistory";
	private static final String REWARDS_PERKS 			= "rewardsPerks";
	private static final String REWARDS_INCENTIVES			= "rewardsIncentives";

	private static final String OFFERS 					= "Offers";
	private static final String WEB_OFFERS 				= "WebOffers";
	private static final String REWARDS_CERTIFICATES 	= "RewardCertificates";
	private static final String BIRTHDAY_OFFERS 		= "BirthdayOffers";
	private static final String REWARDS_SUMMARY 		= "rewardsDetails";
	private static final String PROFILE_SUMMARY 		= "profile";
	private static final String BIRTHDAY_GIFTS  		= "birthdayGifts";
	private static final String SHOPFOR_ITEMS	  		= "shopforItems";
	private static final String DOUBLE_POINTS_DAYS  	= "doublePointsDays";
	private static final String TRIPLE_POINTS_DAYS  	= "triplePointsDays";

	private static final String FILTER_TYPE_OFFERS 				="offers";
	private static final String FILTER_TYPE_CERTS				="certs";
	private static final String FILTER_TYPE_BD_OFFER			="birthdayoffer";
	private static final String FILTER_TYPE_WEB_OFFER			="weboffers";
	private static final String FILTER_TYPE_BD_GIFTS			="birthdaygifts";
	private static final String FILTER_TYPE_SHOPFOR				="shopfor";
	private static final String FILTER_TYPE_CERT_DENOMINATION	="certdenominations";
	private static final String FILTER_TYPE_DOUBLE_POINTS_DAYS	="doublepointsdays";
	private static final String FILTER_TYPE_TRIPLE_POINTS_DAYS	="triplepointsdays";
	private static final String FILTER_TYPE_CERTS_HISTORY		="certshistory";
	private static final String FILTER_TYPE_REWARDS_SUMMARY		="rewardsdetails";
	private static final String FILTER_TYPE_PROFILE_SUMMARY		="profilesummary";
	private static final String FILTER_TYPE_INCENTIVES			="incentives";

	private DigitalProfileTools 			profileTools;
	private DigitalRewardsManager 			rewardManager;
	private MessageLocator 				messageLocator;
	private DigitalCustomDateFormatter 		customDateFormatter;
	private DigitalPromotionTools			promotionTools;

	private RewardsGiftItem 								rewardsGiftItemRequest;
	private RewardsShopforItem								rewardsShopforItemRequest;
	private RewardsAddShopWithoutACardRequest 				rewardsAddShopWithoutACardRequest;
	private RewardsPointsBankingRequest 					rewardsPointsBankingRequest;
	private RewardsSubscribeEmailFooterRequest 				rewardsSubscribeEmailFooterRequest;
	private RewardsDonateCertsRequest 						rewardsDonateCertsRequest;
	private List<String> 									retrieveRewardDetailsFilter = new ArrayList<>();

	private RewardsAddShopWithoutACardResponse 				rewardsAddShopWithoutACardResponse;
	private RewardsDonateCertsResponse rewardsDonateCertsResponse;
	private RewardsPointsBankingResponse 					rewardsPointsBankingResponse;
	private RewardsSubscribeEmailFooterResponse 			rewardsSubscribeEmailFooterResponse;
	private RewardsAddUpdateGiftResponse 					rewardsAddUpdateGiftResponse;
	private RewardsAddUpdateShopforResponse					rewardsAddUpdateShopforResponse;
	private ResponseWrapper									responseWrapper;
	private RetrieveRewardDetailsResponse 					retrieveRewardDetailsResponse;
	private RewardsCharitiesResponse rewardsCharitiesResponse;
	private RepeatingRequestMonitor 						repeatingRequestMonitor;
  private RewardsIssueCertsRequest rewardsIssueCertsRequest;
  private RewardsIssueCertsResponse rewardsIssueCertsResponse;

	private boolean 					syncRewards = true;
	private boolean 					combineBirthdayOffers = false;

	private String						emailCharsRegEx;
	private String						nameCharsRegEx;

	private String 						filters;
	private String 						shopforID;
	private String 						rewardsShopforID;

	private String startDate;
	private String endDate;

	private String[] defaultDatePattern = { "yyyy-MM-dd", "MM/dd/yyyy" };

	private int defaultMonths = 3;

	public boolean handleAddShopWithoutACardRequest(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		String METHOD_NAME = "ADD_SHOP_WITHOUT_A_CARD_REQUEST";
		boolean ret = true;
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		rewardsAddShopWithoutACardResponse = new RewardsAddShopWithoutACardResponse();

		try {
			rewardsAddShopWithoutACardResponse = rewardManager.addShopWithoutACardRequest(rewardsAddShopWithoutACardRequest);

		} catch (Exception e) {
			logError("Error while retrieving addShopWithoutACardRequest", e);
			ret = false;
			String msg = this.getMessageLocator().getMessageString(SERVICE_ERROR_CODE);
			rewardsAddShopWithoutACardResponse.getGenericExceptions().add(new ResponseError(SERVICE_ERROR_CODE, msg));
			rewardsAddShopWithoutACardResponse.setRequestSuccess(false);

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return ret;
	}

	public boolean handleSetPointsBanking(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {

		String METHOD_NAME = "SET_POINTS_BANKING";
		boolean ret = false;
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		rewardsPointsBankingResponse = new RewardsPointsBankingResponse();

		logError("setPointsBanking Service not supported");
		rewardsPointsBankingResponse.getGenericExceptions().add(new ResponseError(SERVICE_ERROR_CODE, "setPointsBanking Service not supported"));
		rewardsPointsBankingResponse.setRequestSuccess(false);

		return ret;
	
	}

	public boolean handleAnonymousSubscriptionFooter(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {

		String METHOD_NAME = "ANONYMOUS_SUBSCRIPTION_FOOTER";
		boolean ret = true;
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		rewardsSubscribeEmailFooterResponse = new RewardsSubscribeEmailFooterResponse();

		try {

			rewardsSubscribeEmailFooterResponse = this.getRewardManager().anonymousSubscriptionFooter(rewardsSubscribeEmailFooterRequest);

		} catch (Exception e) {
			ret = false;
			logError("Error while retrieving anonymousSubscriptionFooter", e);
			rewardsSubscribeEmailFooterResponse.getGenericExceptions().add(new ResponseError(SERVICE_ERROR_CODE, e.getLocalizedMessage()));
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return ret;
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @return true or false
	 */
	public boolean handleCreateShopfor(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		String METHOD_NAME = "handleCreateShopfor";
		boolean ret = true;
		rewardsAddUpdateShopforResponse = new RewardsAddUpdateShopforResponse();

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			UserTransactionDemarcation td = null;
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				boolean isValid = validateShopforRequest();

				//TODO: Check all Error messages per the copy

				// If all input validations are done then check for business restrictions
				if(isValid && getRewardManager().isMaxShopforLimitReached()){
					Object[] params = { getRewardManager().getMaxShopforLimit() };
					String msg = this.getMessageLocator().getMessageString(ERR_MAX_SHOPFOR_LIMIT, params);
					addFormException(new DropletException(msg,ERR_MAX_SHOPFOR_LIMIT));
					isValid = false;
				}

				if (isValid){
					RewardServiceRequest shopForRequest = populateRewardShopforServiceRequest(true);

					if(!getRewardManager().isDimensionSupported(shopForRequest)){
						String msg = this.getMessageLocator().getMessageString(ERR_DIMENSION_SHOPFOR);
						addFormException(new DropletException(msg, ERR_DIMENSION_SHOPFOR));
						isValid = false;
					}

					if (isValid) {
						rewardsAddUpdateShopforResponse = this.getRewardManager().createShopfor(shopForRequest);

						if (!rewardsAddUpdateShopforResponse.isRequestSuccess()) {
							ret = false;
							for (ResponseError error : rewardsAddUpdateShopforResponse.getGenericExceptions()) {
								addFormException(
										new DropletException(error.getLocalizedMessage(), error.getErrorCode()));
							}
						}
					}
				}
			} catch (Exception ex) {
				ret = false;
				addFormException(new DropletException(ERR_GENERIC_SHOPFOR, ex.getMessage()));
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				if (isLoggingError()) {
					logError(ex);
				}
			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
		return ret;
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @return true or false
	 */
	public boolean handleCreateGift(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		String METHOD_NAME = "handleCreateGift";
		boolean ret = true;
		rewardsAddUpdateGiftResponse = new RewardsAddUpdateGiftResponse();
		
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			UserTransactionDemarcation td = null;
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				boolean isValid = validateGiftItemRequest();
				
				// If all input validations are done then check for business restrictions
				if(isValid && getRewardManager().isMaxBDayGiftsLimitReached()){
					Object[] params = { getRewardManager().getMaxBDayGiftsLimit() };
					String msg = this.getMessageLocator().getMessageString(ERR_MAX_BDAY_GIFTS_LIMIT, params);
					addFormException(new DropletException(msg,ERR_MAX_BDAY_GIFTS_LIMIT));
					isValid = false;
				}
				
				if (isValid){
					RewardServiceRequest giftItemRequest = populateRewardServiceRequest();
					rewardsAddUpdateGiftResponse = this.getRewardManager().createGift(giftItemRequest);
	
					if (!rewardsAddUpdateGiftResponse.isRequestSuccess()) {
						ret = false;
						for (ResponseError error : rewardsAddUpdateGiftResponse.getGenericExceptions()) {
							addFormException(new DropletException(error.getLocalizedMessage(), error.getErrorCode()));
						}
					}
				}
			} catch (Exception ex) {
				ret = false;
				addFormException(new DropletException(ERR_GENERIC_BIRTHDAY_GIFT, ex.getMessage()));
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				if (isLoggingError()) {
					logError(ex);
				}
			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
		return ret;
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @return true or false
	 */
	public boolean handleUpdateShopfor(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		String METHOD_NAME = "handleUpdateShopfor";
		boolean ret = true;

		rewardsAddUpdateShopforResponse = new RewardsAddUpdateShopforResponse();
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			UserTransactionDemarcation td = null;
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

				boolean isValid = validateShopforRequest();

				if (isValid && (DigitalStringUtil.isBlank(rewardsShopforItemRequest.getShopforID())
						|| DigitalStringUtil.isBlank(rewardsShopforItemRequest.getRewardsShopforID()))) {
					String msg = this.getMessageLocator().getMessageString(ERR_NO_SHOPFOR_ID);
					addFormException(new DropletException(msg, ERR_NO_SHOPFOR_ID));
					isValid = false;
				}

				if (isValid) {
					RewardServiceRequest shopforItemRequest = populateRewardShopforServiceRequest(false);

					if(!getRewardManager().isDimensionSupported(shopforItemRequest)){
						String msg = this.getMessageLocator().getMessageString(ERR_DIMENSION_SHOPFOR);
						addFormException(new DropletException(msg, ERR_DIMENSION_SHOPFOR));
						isValid = false;
					}

					if(isValid) {
						rewardsAddUpdateShopforResponse = this.getRewardManager()
								.updateShopfor(shopforItemRequest);

						if (!rewardsAddUpdateShopforResponse.isRequestSuccess()) {
							ret = false;
							for (ResponseError error : rewardsAddUpdateShopforResponse.getGenericExceptions()) {
								addFormException(
										new DropletException(error.getLocalizedMessage(), error.getErrorCode()));
							}
						}
					}
				}
			} catch (Exception ex) {
				ret = false;
				addFormException(new DropletException(ERR_GENERIC_SHOPFOR, ex.getMessage()));
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				if (isLoggingError()) {
					logError(ex);
				}
			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
		return ret;
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @return true or false
	 */
	public boolean handleDeleteShopfor(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		String METHOD_NAME = "handleDeleteShopfor";
		boolean ret = true;

		rewardsAddUpdateShopforResponse = new RewardsAddUpdateShopforResponse();
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			UserTransactionDemarcation td = null;
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

				boolean isValid = true;

				if (DigitalStringUtil.isBlank(getShopforID())
						|| DigitalStringUtil.isBlank(getRewardsShopforID())) {
					String msg = this.getMessageLocator().getMessageString(ERR_NO_SHOPFOR_ID);
					addFormException(new DropletException(msg, ERR_NO_SHOPFOR_ID));
					isValid = false;
				}

				if (isValid) {
					RewardServiceRequest shopforItemRequest = populateRewardShopforDeleteServiceRequest();
					rewardsAddUpdateShopforResponse = this.getRewardManager().deleteShopfor(shopforItemRequest);

					if (!rewardsAddUpdateShopforResponse.isRequestSuccess()) {
						ret = false;
						for (ResponseError error : rewardsAddUpdateShopforResponse.getGenericExceptions()) {
							addFormException(new DropletException(error.getLocalizedMessage(), error.getErrorCode()));
						}
					}
				}
			} catch (Exception ex) {
				ret = false;
				addFormException(new DropletException(ERR_GENERIC_SHOPFOR, ex.getMessage()));
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				if (isLoggingError()) {
					logError(ex);
				}
			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
		return ret;
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @return true or false
	 */
	public boolean handleUpdateGift(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		String METHOD_NAME = "handleUpdateGift";
		boolean ret = true;

		rewardsAddUpdateGiftResponse = new RewardsAddUpdateGiftResponse();
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			UserTransactionDemarcation td = null;
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

				boolean isValid = validateGiftItemRequest();

				if (isValid && DigitalStringUtil.isBlank(rewardsGiftItemRequest.getId())) {
					String msg = this.getMessageLocator().getMessageString(ERR_NO_GIFT_ID);
					addFormException(new DropletException(msg, ERR_NO_GIFT_ID));
					isValid = false;
				}

				if (isValid) {
					RewardServiceRequest giftItemRequest = populateRewardServiceRequest();
					rewardsAddUpdateGiftResponse = this.getRewardManager().updateGift(giftItemRequest);

					if (!rewardsAddUpdateGiftResponse.isRequestSuccess()) {
						ret = false;
						for (ResponseError error : rewardsAddUpdateGiftResponse.getGenericExceptions()) {
							addFormException(new DropletException(error.getLocalizedMessage(), error.getErrorCode()));
						}
					}
				}
			} catch (Exception ex) {
				ret = false;
				addFormException(new DropletException(ERR_GENERIC_BIRTHDAY_GIFT, ex.getMessage()));
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				if (isLoggingError()) {
					logError(ex);
				}
			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
		return ret;
	}

	/**
	 * @param request
	 * @param response
	 * @return true or false
	 */
	public boolean handleDonateCertificates(DynamoHttpServletRequest request,
			DynamoHttpServletResponse response) {
		String METHOD_NAME = "handleDonateCertificates";
		boolean ret = true;

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			UserTransactionDemarcation td = null;
			rewardsDonateCertsResponse = new RewardsDonateCertsResponse();
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

				boolean isValid = validateDonatesCertsRequest();

				if (isValid) {
					rewardsDonateCertsRequest.setProfileId(getProfile().getRepositoryId());
					rewardsDonateCertsRequest.setMemberId(
							(String) getProfile().getPropertyValue( "loyaltyNumber" ));
					rewardsDonateCertsResponse = getRewardManager()
							.donateCertificates(rewardsDonateCertsRequest, (DigitalOrderImpl) getOrder());
				}
				else{
					rewardsDonateCertsResponse.setFormError(true);
				}
				
				if (!rewardsDonateCertsResponse.isRequestSuccess()) {
					rewardsDonateCertsResponse.setFormError(true);
					for (ResponseError error : rewardsDonateCertsResponse.getGenericExceptions()) {
						addFormException(
								new DropletException(error.getLocalizedMessage(), error.getErrorCode()));
					}
				}

			} catch (Exception ex) {
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				ret = false;
				rewardsDonateCertsResponse.getGenericExceptions()
						.add(new ResponseError("", ex.getMessage()));
				rewardsDonateCertsResponse.setFormError(true);
				rewardsDonateCertsResponse.setRequestSuccess(false);
				if (isLoggingError()) {
					logError(ex);
				}
			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
			return ret;
		} else {
			logError(":: RepeatingRequest Error ::");
			return false;
		}
	}

  /**
   * @param request
   * @param response
   * @return true or false
   */
  public boolean handleIssueCertificate(DynamoHttpServletRequest request,
      DynamoHttpServletResponse response) {
    String METHOD_NAME = "handleIssueCertificate";
    boolean ret = true;

    RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
    if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
      UserTransactionDemarcation td = null;
      rewardsIssueCertsResponse = new RewardsIssueCertsResponse();
      try {
        DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
        TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
        td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

        boolean isValid = validateIssueCertsRequest();

        if (isValid) {
          rewardsIssueCertsRequest.setProfileId(getProfile().getRepositoryId());
          rewardsIssueCertsResponse = getRewardManager()
              .issueRewardCertificate(rewardsIssueCertsRequest);
        } else {
          rewardsIssueCertsResponse.setFormError(true);
        }

        if (!rewardsIssueCertsResponse.isRequestSuccess()) {
          rewardsIssueCertsResponse.setFormError(true);
          for (ResponseError error : rewardsIssueCertsResponse.getGenericExceptions()) {
            addFormException(
                new DropletException(error.getLocalizedMessage(), error.getErrorCode()));
          }
        }

      } catch (Exception ex) {
        TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
        ret = false;
        rewardsIssueCertsResponse.getGenericExceptions()
            .add(new ResponseError("", ex.getMessage()));
        rewardsIssueCertsResponse.setFormError(true);
        rewardsIssueCertsResponse.setRequestSuccess(false);
        if (isLoggingError()) {
          logError(ex);
        }
      } finally {
        TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
        TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
        if (rrm != null) {
          rrm.removeRequestEntry(METHOD_NAME);
        }
        DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
      }
      return ret;
    } else {
      logError(":: RepeatingRequest Error ::");
      return false;
    }
  }

	/**
	 * @param request
	 * @param response
	 * @return true or false
	 */
	public boolean handleRetrieveCharities(DynamoHttpServletRequest request,
			DynamoHttpServletResponse response) {
		String METHOD_NAME = "handleRetrieveCharities";
		boolean ret = true;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			UserTransactionDemarcation td = null;
			rewardsCharitiesResponse = new RewardsCharitiesResponse();
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

				rewardsCharitiesResponse = getRewardManager()
						.retrieveRewardCharities(getProfile().getRepositoryId());

				if (!rewardsCharitiesResponse.isRequestSuccess()) {
					rewardsCharitiesResponse.setFormError(true);
					for (ResponseError error : rewardsCharitiesResponse.getGenericExceptions()) {
						addFormException(
								new DropletException(error.getLocalizedMessage(), error.getErrorCode()));
					}
				}

			} catch (Exception ex) {
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				ret = false;
				rewardsCharitiesResponse.getGenericExceptions()
						.add(new ResponseError("", ex.getMessage()));
				rewardsCharitiesResponse.setFormError(true);
				rewardsCharitiesResponse.setRequestSuccess(false);
				if (isLoggingError()) {
					logError(ex);
				}
			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
			return ret;
		} else {
			logError(":: RepeatingRequest Error ::");
			return false;
		}
	}

	private void initializeRetrieveRewardDetails(DynamoHttpServletRequest request){
		String serviceFilters = request.getQueryParameter("filters");
		if (!DigitalStringUtil.isEmpty(serviceFilters)) {
			String[] filtersStr = DigitalStringUtil.split(serviceFilters, ",");
			retrieveRewardDetailsFilter.addAll(Arrays.asList(filtersStr));
		}

		String syncRewards = request.getQueryParameter("syncRewards");
		if (!DigitalStringUtil.isEmpty(syncRewards)) {
			try {
				this.syncRewards = Boolean.parseBoolean(syncRewards);
			}catch(Exception ex){
				logError("Parsing error ", ex);
				this.syncRewards = true;
			}
		}

		String combineBirthdayOffers = request.getQueryParameter("combineBirthdayOffers");
		if (!DigitalStringUtil.isEmpty(combineBirthdayOffers)) {
			try {
				this.combineBirthdayOffers = Boolean.parseBoolean(combineBirthdayOffers);
			}catch(Exception ex){
				logError("Parsing error ", ex);
				this.combineBirthdayOffers = true;
			}
		}

		if (this.getDswConstants().isRewardsCacheEnabled()) {
			request.setParameter("cacheRewardsCalls", "Y");
		}

	}

	/**
	 * By default, if no filters are passed, no data is returned
	 * @param request
	 * @param response
	 * @return
	 */
	public boolean handleRetrieveRewardDetails(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		String METHOD_NAME = "handleRetrieveRewardDetails";
		boolean ret = true;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			UserTransactionDemarcation td = null;
			retrieveRewardDetailsResponse = new RetrieveRewardDetailsResponse();
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

				initializeRetrieveRewardDetails(request);

				Profile profile = getProfile();
				if (profileTools.isDSWAnanymousUser(profile) && DigitalStringUtil.isBlank(profileTools.getLoyaltyNumber(profile))) {
					return ret;
				}

				Map<String, Object> respDetails = retrieveRewardDetailsResponse.getDetails();

				if ((isFiltered(FILTER_TYPE_REWARDS_SUMMARY) || isFiltered(FILTER_TYPE_CERT_DENOMINATION))
						&& isSyncRewards()) {
					//This will sync rewards member details with ATG profile
					getProfileTools().loadBasicLoyaltyInfoIntoProfile(profile, true);
				}

				RewardsRetrieveRewardCertificatesResponse rewardsResp = null;

				if (isSyncCertOfferWithRewards()) {

					//This will sync rewards certs, birthday offer and regular offers with ATG profile and current Order
					rewardsResp = getRewardManager().retrieveRewardCertificatesByProfile(profile);
					getRewardManager().loadProfileAndOrderCertsAndOffersUsingLoyaltyService(profile, getOrder(), rewardsResp);
				}

				if ( isFilterRewardsPerks() || (isFiltered(FILTER_TYPE_TRIPLE_POINTS_DAYS)
						|| isFiltered(FILTER_TYPE_DOUBLE_POINTS_DAYS))) {

					Map<String, List<LoyaltyCertificate>>  offersByType = rewardManager.getProfileOffersByType(profile);

					if (isFilterRewardsPerks()) {
						Map<String, List<?>> rewardPerks = getRewardPerks( offersByType);
						respDetails.put(REWARDS_PERKS, rewardPerks);
					}

					String tier = profileTools.getLoyaltyTier(profile);
					if (isFiltered(FILTER_TYPE_DOUBLE_POINTS_DAYS) &&
							(DigitalBaseConstants.USER_TIER_GOLD.equalsIgnoreCase(tier)
									|| DigitalBaseConstants.USER_TIER_ELITE.equalsIgnoreCase(tier))) {
						List<LoyaltyCertificate> doublePointsOffers = offersByType.get("DOUBLE_POINTS_DAYS");
						respDetails.put(DOUBLE_POINTS_DAYS, doublePointsOffers);
					}

					if (isFiltered(FILTER_TYPE_TRIPLE_POINTS_DAYS) &&
							DigitalBaseConstants.USER_TIER_ELITE.equalsIgnoreCase(tier)) {
						List<LoyaltyCertificate> triplePointsOffers = offersByType.get("TRIPLE_POINTS_DAYS");
						respDetails.put(TRIPLE_POINTS_DAYS, triplePointsOffers);
					}

				}

				if (isFiltered(FILTER_TYPE_REWARDS_SUMMARY)) {
					//already synced with rewards, no need to sync again hence the syncReward flag is false
					Map<String, Object> rewardSummary = getProfileTools().getRewardsSummary(profile, false);
					double certTotal = getCertificateTotal(rewardsResp, respDetails);
					rewardSummary.put("rewardsTotalCertificateValue", certTotal);
					respDetails.put(REWARDS_SUMMARY, rewardSummary);
				}

				if (isFiltered(FILTER_TYPE_CERTS_HISTORY)) {
					RewardsRetrievePointsOffersCertificatesRequest rewardsPointsHistoryRequest = new RewardsRetrievePointsOffersCertificatesRequest();
					rewardsPointsHistoryRequest.setProfileId(profile.getRepositoryId());
					String startDate = getStartDate();
					String endDate = getEndDate();

					if(null != startDate && null != endDate) {
						Date endDT = new Date();
						Date startDT = DateUtils.addMonths(endDT, getDefaultMonths() * -1);

						if (!DigitalStringUtil.isBlank(endDate)) {
							endDT = DateUtils.parseDate(endDate, this.defaultDatePattern);
						}
						if (!DigitalStringUtil.isBlank(startDate)) {
							startDT = DateUtils.parseDate(startDate, this.defaultDatePattern);
						}
						rewardsPointsHistoryRequest.setStartDate(startDT);
						rewardsPointsHistoryRequest.setEndDate(endDT);
					}

					RewardsRetrievePointsOffersCertificatesResponse rewardsPointsHistoryResponse;
					rewardsPointsHistoryResponse = rewardManager.retrieveCertificatePointsHistory(rewardsPointsHistoryRequest);
					respDetails.put(REWARDS_POINTS_HISTORY, rewardsPointsHistoryResponse.getRewardsCertificatePointsHistory());
				}
				if (isFiltered(FILTER_TYPE_PROFILE_SUMMARY)) {
					Map<String, Object> profileDetails = getProfileSummary();
					respDetails.put(PROFILE_SUMMARY, profileDetails);
				}
				if (isFiltered(FILTER_TYPE_BD_GIFTS)) {
					RewardsRetrieveBirthdayGiftsResponse birthdayGiftsResponse = rewardManager.retrieveRewardsBirthDayGifts(profile, true);
					respDetails.put(BIRTHDAY_GIFTS, birthdayGiftsResponse);
				}
				if (isFiltered(FILTER_TYPE_SHOPFOR)) {
					RewardsRetrieveShopforResponse shopforResponse = rewardManager.retrieveRewardsShopforItems(profile, true);
					respDetails.put(SHOPFOR_ITEMS, shopforResponse);
				}

				if (isFiltered(FILTER_TYPE_CERT_DENOMINATION)) {
					Map<String, Object> certDenominationsDetails = getProfileTools().getCertDenominationDetails(profile);
					respDetails.put(REWARDS_CERTIFICATE_DENOMINATION, certDenominationsDetails);
				}

				if (isFiltered(FILTER_TYPE_INCENTIVES)) {
					RewardsIncentivesResponse rewardsIncentivesResponse = rewardManager.retrieveRewardIncentives(profile.getRepositoryId());
					respDetails.put(REWARDS_INCENTIVES, rewardsIncentivesResponse);
				}

				retrieveRewardDetailsResponse.setRequestSuccess(true);
				retrieveRewardDetailsResponse.setFormError(false);

			} catch (Exception ex) {
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				ret = false;
				retrieveRewardDetailsResponse.getGenericExceptions().add(new ResponseError("", ex.getMessage()));
				retrieveRewardDetailsResponse.setFormError(true);
				retrieveRewardDetailsResponse.setRequestSuccess(false);
				if (isLoggingError())
					logError(ex);
			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
			return ret;
		}else{
			logError(":: RepeatingRequest Error ::");
			return false;
		}
	}

	private boolean isSyncCertOfferWithRewards(){
		return isSyncRewards() && (isFiltered(FILTER_TYPE_REWARDS_SUMMARY) || isFiltered(FILTER_TYPE_OFFERS)
				|| isFiltered(FILTER_TYPE_BD_OFFER) || isFiltered(FILTER_TYPE_CERTS)
				|| (isFiltered(FILTER_TYPE_TRIPLE_POINTS_DAYS) || isFiltered(FILTER_TYPE_DOUBLE_POINTS_DAYS)));
	}

	private boolean isFilterRewardsPerks(){
		//adding FILTER_TYPE_REWARDS_SUMMARY, bcoz we are sending certificate total in rewards summary
		return isFiltered(FILTER_TYPE_REWARDS_SUMMARY) || isFiltered(FILTER_TYPE_OFFERS) || isFiltered(FILTER_TYPE_BD_OFFER)
				|| isFiltered(FILTER_TYPE_CERTS) || isFiltered(FILTER_TYPE_WEB_OFFER);
	}

	private double getCertificateTotal(RewardsRetrieveRewardCertificatesResponse rewardsResp, Map<String, Object> rewardDetails){
		double certTotal = 0.0;
		if(rewardsResp != null){
			certTotal = rewardsResp.getTotalCertValue();
		}else if(rewardDetails.get(REWARDS_PERKS) != null) {
			Map<String, List<?>> rewardPerks = (Map<String, List<?>>)rewardDetails.get(REWARDS_PERKS);
			List<LoyaltyCertificate> certs = (List<LoyaltyCertificate>)rewardPerks.get(REWARDS_CERTIFICATES);
			for(LoyaltyCertificate cert : certs){
				certTotal += cert.getValue();
			}
		}

		return certTotal;
	}

	boolean isFiltered(String rewardsFilterType){
		//default if no filters are passed, no data is returned
		boolean filtered = false;

		if(retrieveRewardDetailsFilter != null && !retrieveRewardDetailsFilter.isEmpty()) {
			for (String filter : retrieveRewardDetailsFilter) {
				if (filter.equalsIgnoreCase(rewardsFilterType)){
					filtered = true;
				}
			}
		}

		return filtered;
	}

	/**
	 *
	 * @return Map<String, List<?>>
	 */
	public Map<String, List<?>> getRewardPerks( Map<String, List<LoyaltyCertificate>> rewardOffersByType) {
		Map<String, List<?>> rewardPerks = new HashMap<>();
		try{

			if (isFiltered(FILTER_TYPE_CERTS)) {
				rewardPerks.put(REWARDS_CERTIFICATES, getRewardManager().getProfileCertificates(getProfile()));
			}
			if (!combineBirthdayOffers && (isFiltered(FILTER_TYPE_OFFERS) || isFiltered(FILTER_TYPE_BD_OFFER))) {

				if (isFiltered(FILTER_TYPE_OFFERS)) {
					rewardPerks.put(OFFERS, rewardOffersByType.get("OFFERS"));
				}
				if (isFiltered(FILTER_TYPE_BD_OFFER)) {
					rewardPerks.put(BIRTHDAY_OFFERS, rewardOffersByType.get("BIRTHDAY_OFFER"));
				}
			} else if (combineBirthdayOffers && (isFiltered(FILTER_TYPE_OFFERS) || isFiltered(FILTER_TYPE_BD_OFFER))) {
				List<LoyaltyCertificate> rewardOffers = getRewardManager().getProfileOffers(getProfile());
				rewardPerks.put(OFFERS, rewardOffers);
			}

			if (isFiltered(FILTER_TYPE_WEB_OFFER)) {
				//TODO: Currently the getWebAuthOffers is syncing with rewards system always. Need to work on to do it based on flag
				List<Offer> regularWebOffers = getPromotionTools().getWebAuthOffers(true, getProfile());
				rewardPerks.put(WEB_OFFERS, regularWebOffers);
			}
		}catch (Exception e) {
			logError("Exception getRewardPerks",e);
		}

		return rewardPerks;
	}

	public Map<String, Object> getProfileSummary(){

		Map<String, Object> profileDetails = new HashMap<>();
		try {

			String gender = getProfileTools().getGender(getProfile());
			profileDetails.put("gender", gender);

			Date dob = getProfileTools().getDateOfBirth(getProfile());
			if(null != dob) {
				profileDetails.put("dateOfBirth", customDateFormatter.getLocalizedDateString(dob));
			}

			boolean isProfileComplete = getProfileTools().isProfileComplete(getProfile(), false);
			profileDetails.put("profileComplete", isProfileComplete);

			String profileInitials = getProfileTools().getProfileInitials(getProfile());
			profileDetails.put("profileInitials", profileInitials);

		}catch(Exception ex){
			logError("Error parsing DOB ", ex);
			profileDetails.put("birthDayMonth", null);
			profileDetails.put("profileComplete", null);
		}

		return profileDetails;
	}

	public RewardsGiftItem getRewardsGiftItemRequest() {
		if(null == rewardsGiftItemRequest){
			rewardsGiftItemRequest = new RewardsGiftItem();
		}
		return rewardsGiftItemRequest;
	}

	public RewardsShopforItem getRewardsShopforItemRequest() {
		if(null == rewardsShopforItemRequest){
			rewardsShopforItemRequest = new RewardsShopforItem();
		}
		return rewardsShopforItemRequest;
	}

	public RewardsAddShopWithoutACardRequest getRewardsAddShopWithoutACardRequest() {
		if(null == rewardsAddShopWithoutACardRequest){
			rewardsAddShopWithoutACardRequest = new RewardsAddShopWithoutACardRequest();
		}
		return rewardsAddShopWithoutACardRequest;
	}

	public RewardsPointsBankingRequest getRewardsPointsBankingRequest() {
		if(null == rewardsPointsBankingRequest){
			rewardsPointsBankingRequest = new RewardsPointsBankingRequest();
		}
		return rewardsPointsBankingRequest;
	}

	public RewardsSubscribeEmailFooterRequest getRewardsSubscribeEmailFooterRequest() {
		if(null == rewardsSubscribeEmailFooterRequest){
			rewardsSubscribeEmailFooterRequest = new RewardsSubscribeEmailFooterRequest();
		}
		return rewardsSubscribeEmailFooterRequest;
	}

	public RewardsAddShopWithoutACardResponse getRewardsAddShopWithoutACardResponse() {
		if(null == rewardsAddShopWithoutACardResponse){
			rewardsAddShopWithoutACardResponse = new RewardsAddShopWithoutACardResponse();
		}
		return rewardsAddShopWithoutACardResponse;
	}

	public RewardsPointsBankingResponse getRewardsPointsBankingResponse() {
		if(null == rewardsPointsBankingResponse) {
			rewardsPointsBankingResponse = new RewardsPointsBankingResponse();
		}
		return rewardsPointsBankingResponse;
	}

	public ResponseWrapper getResponseWrapper() {
		if(null == responseWrapper){
			responseWrapper = new ResponseWrapper();
		}
		return responseWrapper;
	}

	public RewardsDonateCertsRequest getRewardsDonateCertsRequest() {
		if(null == rewardsDonateCertsRequest){
			rewardsDonateCertsRequest = new RewardsDonateCertsRequest();
		}
		return rewardsDonateCertsRequest;
	}
	public RewardsAddUpdateGiftResponse getRewardsAddUpdateGiftResponse() {
		if(null == rewardsAddUpdateGiftResponse){
			rewardsAddUpdateGiftResponse = new RewardsAddUpdateGiftResponse();
		}
		return rewardsAddUpdateGiftResponse;
	}

	public RewardsAddUpdateShopforResponse getRewardsAddUpdateShopforResponse() {
		if(null == rewardsAddUpdateShopforResponse){
			rewardsAddUpdateShopforResponse = new RewardsAddUpdateShopforResponse();
		}
		return rewardsAddUpdateShopforResponse;
	}

	public void setRetrieveRewardDetailsResponse(RetrieveRewardDetailsResponse retrieveRewardDetailsResponse) {
		if(retrieveRewardDetailsResponse == null){
			retrieveRewardDetailsResponse = new RetrieveRewardDetailsResponse();
		}
		this.retrieveRewardDetailsResponse = retrieveRewardDetailsResponse;
	}

	public Order getOrder() {
		OrderHolder orderHolder =  ComponentLookupUtil.lookupComponent( ComponentLookupUtil.SHOPPING_CART, OrderHolder.class  );
		if(orderHolder != null){
			return orderHolder.getCurrent();
		}else{
			return null;
		}
	}

	/**
	 *
	 * @return true or false
	 */
	private boolean validateGiftItemRequest() {
		boolean isValid = true;

		if (DigitalStringUtil.isBlank(rewardsGiftItemRequest.getEmail())) {
			String msg = this.getMessageLocator().getMessageString(ERR_NO_EMAIL);
			addFormException(new DropletException(msg, ERR_NO_EMAIL));
			isValid = false;
		} else if (isEmailValid(rewardsGiftItemRequest.getEmail())) {
			String msg = this.getMessageLocator().getMessageString(ERR_INVALID_EMAIL);
			addFormException(new DropletException(msg, ERR_INVALID_EMAIL));
			isValid = false;
		}

		if (DigitalStringUtil.isBlank(rewardsGiftItemRequest.getFirstName())) {
			String msg = this.getMessageLocator().getMessageString(ERR_NO_FIRSTNAME);
			addFormException(new DropletException(msg, ERR_NO_FIRSTNAME));
			isValid = false;
		} else if (isFirstNameValid(rewardsGiftItemRequest.getFirstName())) {
			String msg = this.getMessageLocator().getMessageString(ERR_INVALID_FIRSTNAME);
			addFormException(new DropletException(msg, ERR_INVALID_FIRSTNAME));
			isValid = false;
		}

		String birthDay = rewardsGiftItemRequest.getBirthDay();
		if (DigitalStringUtil.isBlank(birthDay)) {
			rewardsGiftItemRequest.setBirthDay("01");
		}

		if (!DigitalDateUtil.validateDOB(birthDay, rewardsGiftItemRequest.getBirthMonth(), DEFAULT_YEAR)) {
			String msg = this.getMessageLocator().getMessageString(ER_INVALID_BIRTH_DAY_MONTH);
			addFormException(new DropletException(msg, ER_INVALID_BIRTH_DAY_MONTH));
			isValid = false;
		}

		return isValid;
	}

	/**
	 *
	 * @return true or false
	 */
	private boolean validateShopforRequest() {
		boolean isValid = true;

		if (DigitalStringUtil.isBlank(rewardsShopforItemRequest.getShopforName())) {
			String msg = this.getMessageLocator().getMessageString(ERR_NO_SHOPFOR_NAME);
			addFormException(new DropletException(msg, ERR_NO_SHOPFOR_NAME));
			isValid = false;
		} else if (isFirstNameValid(rewardsShopforItemRequest.getShopforName())) {
			String msg = this.getMessageLocator().getMessageString(ERR_INVALID_SHOPFOR_NAME);
			addFormException(new DropletException(msg, ERR_INVALID_SHOPFOR_NAME));
			isValid = false;
		}

		// Validate Relationship
		if (DigitalStringUtil.isBlank(rewardsShopforItemRequest.getRelationship())) {
			String msg = this.getMessageLocator().getMessageString(ERR_MISSING_SHOPFOR_RELATIONSHIP);
			addFormException(new DropletException(msg, ERR_MISSING_SHOPFOR_RELATIONSHIP));
			isValid = false;
		}

		// If webtype doesn't exists then default to Shoe
		if (DigitalStringUtil.isBlank(rewardsShopforItemRequest.getWebType())) {
			rewardsShopforItemRequest.setWebType("shoe");
		}

		String birthDay = rewardsShopforItemRequest.getBirthDay();
		if (DigitalStringUtil.isBlank(birthDay)) {
			rewardsShopforItemRequest.setBirthDay("01");
		}

		if (!DigitalDateUtil.validateDOB(birthDay, rewardsShopforItemRequest.getBirthMonth(), DEFAULT_YEAR)) {
			String msg = this.getMessageLocator().getMessageString(ER_INVALID_BIRTH_DAY_SHOPFOR);
			addFormException(new DropletException(msg, ER_INVALID_BIRTH_DAY_SHOPFOR));
			isValid = false;
		}

		if (rewardsShopforItemRequest.getSizes().length == 0) {
			String msg = this.getMessageLocator().getMessageString(ERR_MISSING_SHOPFOR_SIZE);
			addFormException(new DropletException(msg, ERR_MISSING_SHOPFOR_SIZE));
			isValid = false;
		}

		if (DigitalStringUtil.isBlank(rewardsShopforItemRequest.getGender())) {
			String msg = this.getMessageLocator().getMessageString(ERR_MISSING_SHOPFOR_GENDER);
			addFormException(new DropletException(msg, ERR_MISSING_SHOPFOR_GENDER));
			isValid = false;
		}

		return isValid;
	}

	/**
	 *
	 * @return true or false
	 */
	private boolean validateDonatesCertsRequest() {
		boolean isValid = true;

		if (DigitalStringUtil.isBlank(rewardsDonateCertsRequest.getCharityId())) {
			String msg = this.getMessageLocator().getMessageString(ERR_NO_CHARITY_ID);
			addFormException(new DropletException(msg, ERR_NO_CHARITY_ID));
			isValid = false;
		}

		if (null == rewardsDonateCertsRequest.getCertificateIdsList()
				|| rewardsDonateCertsRequest.getCertificateIdsList().isEmpty()) {
			String msg = this.getMessageLocator().getMessageString(ERR_NO_CERTS_TO_DONATE);
			addFormException(new DropletException(msg, ERR_NO_CERTS_TO_DONATE));
			isValid = false;
		}

		return isValid;
	}

  /**
   *
   * @return true or false
   */
  private boolean validateIssueCertsRequest() {
    boolean isValid = true;

    if (DigitalStringUtil.isBlank(rewardsIssueCertsRequest.getCertDenomination())) {
      String msg = this.getMessageLocator().getMessageString(ERR_NO_CERT_DENOMINATION);
      addFormException(new DropletException(msg, ERR_NO_CERT_DENOMINATION));
      isValid = false;
    }

    return isValid;
  }

	/**
	 * @return RewardServiceRequest
	 */
	private RewardServiceRequest populateRewardShopforServiceRequest(boolean isCreated) {
		RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();

		Shopfor shopFor = new Shopfor();
		shopFor.setBirthDay(rewardsShopforItemRequest.getBirthDay());
		shopFor.setBirthMonth(rewardsShopforItemRequest.getBirthMonth());
		shopFor.setGender(rewardsShopforItemRequest.getGender());
		shopFor.setRelationship(rewardsShopforItemRequest.getRelationship());
		shopFor.setShopforName(rewardsShopforItemRequest.getShopforName());
		shopFor.setWebType(rewardsShopforItemRequest.getWebType());
		shopFor.setSizeGroup(rewardsShopforItemRequest.getSizeGroup());
		//Sizes - Needs some clean up as ATG OOB ArrayTagConverter is putting a double quote for strings
		if (rewardsShopforItemRequest.getSizes() != null) {
			String[] sizes = rewardsShopforItemRequest.getSizes();
			List<String> finalSizes = new ArrayList<>();
			for (String size : sizes) {
				finalSizes.add(size.replace("\"", ""));
			}
			shopFor.setSizes(finalSizes);
		}
		//Widths - Needs some clean up as ATG OOB ArrayTagConverter is putting a double quote for strings
		if (rewardsShopforItemRequest.getWidths() != null) {
			String[] widths = rewardsShopforItemRequest.getWidths();
			List<String> finalWidths = new ArrayList<>();
			for (String width : widths) {
				finalWidths.add(width.replace("\"", ""));
			}
			shopFor.setWidths(finalWidths);
		}
		if (!isCreated) {
			shopFor.setRewardsShopforID(rewardsShopforItemRequest.getRewardsShopforID());
			shopFor.setShopforID(rewardsShopforItemRequest.getShopforID());
		}
		shopFor.setActive(true); // we have no requirement to delete for now
		shopFor.setProfileId(getProfile().getRepositoryId());
		if (isCreated) {
			shopFor.setDateCreated(Calendar.getInstance().getTime());
		}
		else {
			shopFor.setDateCreated(rewardsShopforItemRequest.getDateCreated());
		}

		rewardServiceRequest.setShopfor(shopFor);
		Person person = new Person();
		person.setProfileID(getProfile().getRepositoryId());
		rewardServiceRequest.setPerson(person);

		return rewardServiceRequest;
	}

	/**
	 * @return RewardServiceRequest
	 */
	private RewardServiceRequest populateRewardShopforDeleteServiceRequest() {
		RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();

		Shopfor shopFor = new Shopfor();

		shopFor.setRewardsShopforID(getRewardsShopforID());
		shopFor.setShopforID(getShopforID());

		shopFor.setActive(false);
		shopFor.setProfileId(getProfile().getRepositoryId());

		shopFor.setDateCreated(Calendar.getInstance().getTime());

		rewardServiceRequest.setShopfor(shopFor);
		Person person = new Person();
		person.setProfileID(getProfile().getRepositoryId());
		rewardServiceRequest.setPerson(person);

		return rewardServiceRequest;
	}

	/**
	 * @return RewardServiceRequest
	 */
	private RewardServiceRequest populateRewardServiceRequest() {
		RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();

		BirthdayGift birthdayGift = new BirthdayGift();
		birthdayGift.setEmail(rewardsGiftItemRequest.getEmail());
		birthdayGift.setFriendName(rewardsGiftItemRequest.getFirstName());
		birthdayGift.setMonthOfBirth(rewardsGiftItemRequest.getBirthMonth());
		birthdayGift.setDayOfBirth(rewardsGiftItemRequest.getBirthDay());

		Calendar requestedSendCalendarDate = Calendar.getInstance();
		requestedSendCalendarDate.set(requestedSendCalendarDate.get(Calendar.YEAR),
				Integer.parseInt(rewardsGiftItemRequest.getBirthMonth()) - 1,
				Integer.parseInt(rewardsGiftItemRequest.getBirthDay()), 0, 0);

		String deliveryDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
				.format(requestedSendCalendarDate.getTime());
		birthdayGift.setDeliveryDate(deliveryDate);

		birthdayGift.setMessage(rewardsGiftItemRequest.getGiftMessage());

		birthdayGift.setBirthdayGiftId(rewardsGiftItemRequest.getId());
		birthdayGift.setProfileId(getProfile().getRepositoryId());

		rewardServiceRequest.setBirthdayGift(birthdayGift);
		Person person = new Person();
		person.setProfileID(getProfile().getRepositoryId());
		rewardServiceRequest.setPerson(person);

		return rewardServiceRequest;
	}

	public DigitalServiceConstants getDswConstants() {
		return this.getProfileTools().getDswConstants();
	}
	/**
	 *
	 * @param pEmail
	 * @return true or false
	 */
	private boolean isEmailValid(String pEmail) {
		Pattern EMAIL_PATTERN = Pattern.compile(getEmailCharsRegEx());
		return DigitalStringUtil.isBlank(pEmail)|| pEmail.trim().length() < DigitalProfileConstants.MIN_EMAIL_LENGTH
				|| pEmail.trim().length() > DigitalProfileConstants.MAX_EMAIL_LENGTH
				|| !EMAIL_PATTERN.matcher( pEmail.trim() ).matches();
	}

	/**
	 *
	 * @param pFName
	 * @return true or false
	 */
	private boolean isFirstNameValid( String pFName ) {
		Pattern NAME_PATTERN  = Pattern.compile(getNameCharsRegEx() );
		return DigitalStringUtil.isEmpty( pFName ) || pFName.trim().length() < DigitalProfileConstants.MIN_FNAME_LENGTH ||
				pFName.trim().length() > DigitalProfileConstants.MAX_FNAME_LENGTH
				|| !NAME_PATTERN.matcher( pFName ).matches();
	}
	
	
	private Profile getProfile() {
		return ComponentLookupUtil.lookupComponent( ComponentLookupUtil.PROFILE, Profile.class  );
	}

	public void setFilters(String filters) {
		this.filters = filters;
		if (!DigitalStringUtil.isEmpty(filters)) {
			String[] filtersStr = DigitalStringUtil.split(filters, ",");
			this.retrieveRewardDetailsFilter.addAll(Arrays.asList(filtersStr));
		}
	}

	/**
	 * @param rewardsCharitiesResponse
	 */
	public void setRewardsCharitiesResponse(
			RewardsCharitiesResponse rewardsCharitiesResponse) {
		if (null == rewardsCharitiesResponse) {
			rewardsCharitiesResponse = new RewardsCharitiesResponse();
		}
		this.rewardsCharitiesResponse = rewardsCharitiesResponse;
	}

	/**
	 * @param rewardsDonateCertsResponse
	 */
	public void setRewardsDonateCertsResponse(
			RewardsDonateCertsResponse rewardsDonateCertsResponse) {
		if (null == rewardsDonateCertsResponse) {
			rewardsDonateCertsResponse = new RewardsDonateCertsResponse();
		}
		this.rewardsDonateCertsResponse = rewardsDonateCertsResponse;
	}

	/**
	 * @return RewardsIssueCertsRequest
	 */
	public RewardsIssueCertsRequest getRewardsIssueCertsRequest() {
		if (null == rewardsIssueCertsRequest) {
			rewardsIssueCertsRequest = new RewardsIssueCertsRequest();
		}
		return rewardsIssueCertsRequest;
	}

	/**
	 * @param rewardsIssueCertsResponse
	 */
	public void setRewardsIssueCertsResponse(
			RewardsIssueCertsResponse rewardsIssueCertsResponse) {
		if (null == rewardsIssueCertsResponse) {
			rewardsIssueCertsResponse = new RewardsIssueCertsResponse();
		}
		this.rewardsIssueCertsResponse = rewardsIssueCertsResponse;
	}
}
