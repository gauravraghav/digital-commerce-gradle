/**
 *
 */
package com.digital.commerce.repository;

import java.util.Iterator;
import java.util.List;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

@SuppressWarnings({"rawtypes"})
public class WhiteGloveProductPropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
     *
     */
	private static final long		serialVersionUID	= -6259343473515967032L;

	protected static final String	TYPE_NAME			= "WhiteGloveProductPropertyDescriptor";

	protected static final String	CHILD_SKUS			= "childSKUs";

	protected static final String	WHITE_GLOVE			= "isWhiteGloveEligible";
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, WhiteGloveProductPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		if( pValue != null && pValue instanceof Boolean ) { return pValue; }

		List skus = (List)pItem.getPropertyValue( CHILD_SKUS );

		if( skus != null ) {
			Iterator i = skus.iterator();
			RepositoryItem sku = null;
			while( i.hasNext() ) {
				sku = (RepositoryItem)i.next();
				Boolean whiteGlove = (Boolean)sku.getPropertyValue( WHITE_GLOVE );
				if( Boolean.TRUE.equals( whiteGlove ) ) {
					setPropertyInCache( pItem, Boolean.TRUE );

					return Boolean.TRUE;
				}
			}
			setPropertyInCache( pItem, Boolean.FALSE );

		}
		return Boolean.FALSE;
	}

	private void setPropertyInCache( RepositoryItemImpl item, Boolean flag ) {
		item.setPropertyValueInCache( this, flag );
	}
}
