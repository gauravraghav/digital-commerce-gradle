package com.digital.commerce.services.order;

import atg.commerce.order.HardgoodShippingGroup;

public class DigitalHardgoodShippingGroup extends HardgoodShippingGroup {

	private static final long serialVersionUID = 1L;
	private static final String SHIP_TYPE = "shipType";
	private static final String STORE_ID = "storeId";
	private static final String MALL_NAME = "mallPlazaName";

	public String getMallPlazaName() {
		return (String) getPropertyValue(MALL_NAME);
	}

	public String getStoreId() {
		return (String) getPropertyValue(STORE_ID);
	}

	public String getShipType() {
		return (String) getPropertyValue(SHIP_TYPE);
	}

	public String getLocationId() {
		return null;
	}
	
	public void setMallPlazaName(String mallPlazaName) {
		setPropertyValue(MALL_NAME, mallPlazaName);
	}

	public void setStoreId(String storeId) {
		 setPropertyValue(STORE_ID, storeId);
	}	
}
