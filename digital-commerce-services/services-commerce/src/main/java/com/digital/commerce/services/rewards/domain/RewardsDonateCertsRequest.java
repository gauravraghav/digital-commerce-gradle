package com.digital.commerce.services.rewards.domain;

import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MK402314 on 12/6/2017.
 */
@Getter
@Setter
public class RewardsDonateCertsRequest {

  String charityId;
  String certificateIds;
  List<String> certificateIdsList;
  String profileId;
  String memberId;

  /**
   * @param certificateIds
   */
  public void setCertificateIds(String certificateIds) {
    this.certificateIds = certificateIds;
    if (DigitalStringUtil.isNotBlank(this.certificateIds)) {
      String[] certsArray = DigitalStringUtil.split(this.certificateIds, ",");
      if (certsArray != null && certsArray.length > 0) {
        for (String c : certsArray) {
          c = DigitalStringUtil.remove(c.trim(), "\"");
          c = DigitalStringUtil.remove(c, "[");
          c = DigitalStringUtil.remove(c, "]");
          getCertificateIdsList().add(c);
        }
      }
    }
  }

  /**
   * @return List
   */
  public List<String> getCertificateIdsList() {
    if (null == certificateIdsList) {
      certificateIdsList = new ArrayList<>();
    }
    return certificateIdsList;
  }
}
