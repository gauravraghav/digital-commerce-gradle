package com.digital.commerce.services.order.purchase;

import atg.commerce.CommerceException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.dtm.UserTransactionDemarcation;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import com.digital.commerce.common.csc.ContactCenterUser;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.RequestHeaderAttributesConstant;
import com.digital.commerce.constants.ProducCatalogConstants.ProducCatalogPropertyManager;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalCreditCard;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.OriginOfOrder;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.payment.BillingShippingServices;
import com.digital.commerce.services.pricing.ShippingRate;
import com.digital.commerce.services.pricing.ShippingRateTable;
import com.digital.commerce.services.profile.DigitalCommercePropertyManager;
import com.digital.commerce.services.utils.DigitalAddressUtil;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Pre-commit verifies Order for Valid Shipping, Updated Order with required info before commit order Etc.
 * 
 **/
@Getter
@Setter
public class PreCommitOrderManager extends ApplicationLoggingImpl{
	private BillingShippingServices billingShippingServices;
	private OrderManager 			orderManager;
	private DigitalServiceConstants		dswConstants;
	static final String CLASSNAME = PreCommitOrderManager.class.getName();

	private Profile					profile;
	
	private ContactCenterUser	contactCenterUser;

	/**
	 * Default Constructor
	 * 
	 */
	public PreCommitOrderManager() {
		super(PreCommitOrderManager.class.getName());
	}

  /**
   * Validates that the {@link Order} has {@link HardgoodShippingGroup}s with valid shipping
   * methods.
   *
   * @param order
   * @throws DigitalAppException
   */
  public void validateShippingMethod(Order order)
      throws DigitalAppException {
    final ShippingRateTable shippingRateTable = this
        .getBillingShippingServices().getShippingRateTable(order, "Sales");
    for (HardgoodShippingGroup shippingGroup : getShippingGroupManager()
        .getNonGiftCardShippingGroups(order)) {
      @SuppressWarnings("unchecked") final List<ShippingRate> shippingRates = shippingRateTable
          .getRatesList(shippingGroup.getId());
      if (shippingRates != null && !getShippingGroupManager().validateShippingMethod(
          shippingGroup, shippingRates)) {
        throw new DigitalAppException(shippingGroup.getShippingMethod());
      }
    }
  }

	/**
	 * Validates that the {@link Order} has {@link HardgoodShippingGroup}s with
	 * valid shipping address.
	 * 
	 * @param order
	 * @throws DigitalAppException	 
	 */
	public void validateShippingAddress(Order order) throws DigitalAppException {
		final String INVALID_SHIPPING_ADDRESS = "invalidShippingAddress";
		List<HardgoodShippingGroup> shippingGroups = getShippingGroupManager().getNonGiftCardShippingGroups(order);
		if (shippingGroups == null || shippingGroups.size() == 0) {
			throw new DigitalAppException(INVALID_SHIPPING_ADDRESS);
		}
		boolean retVal = true;
		for (HardgoodShippingGroup shippingGroup : getShippingGroupManager().getNonGiftCardShippingGroups(order)) {
			if (shippingGroup.getShippingAddress() == null) {
				retVal = false;
			} else if (shippingGroup.getShippingAddress() != null
					&& (DigitalStringUtil.isBlank(shippingGroup.getShippingAddress().getAddress1())
							|| DigitalStringUtil.isBlank(shippingGroup.getShippingAddress().getFirstName())
							|| DigitalStringUtil.isBlank(shippingGroup.getShippingAddress().getLastName())
							|| DigitalStringUtil.isBlank(shippingGroup.getShippingAddress().getCity())
							|| DigitalStringUtil.isBlank(shippingGroup.getShippingAddress().getPostalCode()))) {
				retVal = false;
			}
		}

		if (!retVal) {
			throw new DigitalAppException(INVALID_SHIPPING_ADDRESS);
		}
	}
	
	/**
	 * Updates various audit fields on the order based on the profile and
	 * cookies in the request.
	 *
	 * @param order
	 */
	public void updateAuditFields(Order order) {
		if (order instanceof DigitalOrderImpl) {
			DigitalOrderImpl dswOrder = (DigitalOrderImpl) order;
			DigitalCommercePropertyManager pmr = getProfilePropertyManager();
			dswOrder.setLoyaltyId((String) getProfile().getPropertyValue(pmr.getLoyaltyNumberPropertyName()));
			dswOrder.setLoyaltyTier((String) getProfile().getPropertyValue(pmr.getLoyaltyTierItemDescriptorName()));

			this.getOrderTools().determineOriginOfOrder(order);

			String csrID = "";
			if (contactCenterUser != null && DigitalStringUtil.isNotBlank(contactCenterUser.getUserId())) {
				csrID = contactCenterUser.getUserId();
				order.setOriginOfOrder(OriginOfOrder.ContactCenter.getValue());
			} else {
				csrID = this.getDswConstants().getDefaultCsrid();
			}

			((DigitalOrderImpl) order).setCsrRepID(csrID);

			this.getOrderTools().determineWhiteGloveOrder(order);

			if (dswConstants.isChargeSendEnable()) {
				this.getOrderTools().determineMultipleShipmentOrder(order);
			}
		}
	}
	
	/** 
	 * Updates product title for every commerce item
	 * in the order. This is required by Yantra to
	 * send shipping confirmation.
	 *
	 * @param order
	 */
	protected void updateProductTitles( Order order ) {
		if( order instanceof DigitalOrderImpl ) {
			@SuppressWarnings("unchecked")
			List<DigitalCommerceItem> commerceItems = order.getCommerceItems();
			if( commerceItems != null ) {
				for( DigitalCommerceItem item : commerceItems ) {
					if( item != null ) {
						String productTitle = "";
						RepositoryItem product = (RepositoryItem)item.getAuxiliaryData().getProductRef();
						if( product != null ) {
							productTitle = (String)product.getPropertyValue( ProducCatalogPropertyManager.PRODUCT_TITLE.getValue() );
						}
						item.setProductTitle( productTitle );
					}
				}
			}
		}
	}
	
	/** 
	 * Removes credit cards payment groups from the order if they are unused.
	 *
	 * @param order 
	 * @throws CommerceException 
	 */
	public void removeUnusedCreditCard( final Order order ) throws CommerceException {

		String methodName="removeUnusedCreditCard";
		if( order.getPriceInfo() != null ) {
			double amountRemaining = getPaymentGroupManager().getAmountRemaining( order, order.getPriceInfo().getTotal() );
			final DigitalCreditCard creditCard = getPaymentGroupManager().findCreditCard( order );
			if( amountRemaining >= .01 && creditCard != null ) {
				amountRemaining = creditCard.getAmount();
			}
			if( amountRemaining <= 0.00001 ||
					(creditCard != null && DigitalStringUtil.isEmpty(creditCard.getTokenValue())
							&& DigitalStringUtil.isEmpty(creditCard.getPaypageRegistrationId()))) {
				final UserTransactionDemarcation td = TransactionUtils.startNewTransaction(CLASSNAME, methodName);
				
					try {
						synchronized(order){
							getPaymentGroupManager().removePaymentGroupFromOrder( order, creditCard.getId() );
							getOrderManager().updateOrder( order );
						}
					} catch (CommerceException cEx) {
						TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
	                    if (isLoggingError()) {
	                           logError(cEx);
	                    }
	                    throw cEx;
		             } finally {
		                TransactionUtils.endTransaction(td, CLASSNAME, methodName);
		             }

				}
				
			}
		 else {
			if( isLoggingWarning() ) {
				logWarning( String.format( "order %1$s is missing price information.  We do not know what causes it so we are trying to skip the bad data.", order.getId() ) );
			}
		}
	}	

	/** 
	 * Removes Paypal payment groups from the order if they are unused.
	 *
	 * @param order
	 * @param checkAmountRemaining - true or false
	 * @throws CommerceException 
	 */
	public void removeUnusedPaypalPayment(final Order order, boolean checkAmountRemaining) throws CommerceException {
		final String methodName="removeUnusedPaypalPayment";
		if (order.getPriceInfo() != null) {
			double amountRemaining = getPaymentGroupManager()
					.getAmountRemaining(order, order.getPriceInfo().getTotal());
			final PaypalPayment paypalPayment = getPaymentGroupManager()
					.findPaypalPayment(order);
			if (amountRemaining >= .01 && paypalPayment != null) {
				amountRemaining = paypalPayment.getAmount();
			}
			if(checkAmountRemaining && amountRemaining <= 0.00001){
				return;
			}
			if (paypalPayment != null) {

				final UserTransactionDemarcation td = TransactionUtils
						.startNewTransaction(CLASSNAME, methodName);
				try {

					synchronized(order){
						getPaymentGroupManager().removePaymentGroupFromOrder(order, paypalPayment.getId());
						getOrderManager().updateOrder(order);
					}
				} catch (CommerceException cEx) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME,methodName);
					if (isLoggingError()) {
						logError(cEx);
					}
					throw cEx;
				} finally {
					TransactionUtils.endTransaction(td, CLASSNAME, methodName);
				}

			}
		} else {
			if (isLoggingWarning()) {
				logWarning(String
						.format("order %1$s is missing price information.  We do not know what causes it so we are trying to skip the bad data.",
								order.getId()));
			}
		}
	}
	
	/**
	 * 
	 * @param order
	 */
	public void updateOrderForCommitOrder(Order order){
		updateAuditFields(order);
		((DigitalOrderImpl) order).setLocale();
		updateProductTitles(order);
	}
	
	/**
	 * 
	 * @param order
	 * @return HardgoodShippingGroup
	 * @throws DigitalAppException
	 */
	public HardgoodShippingGroup getHardgoodShippingGroup(Order order)
			throws DigitalAppException{
		return this.getShippingGroupManager().getFirstNonGiftHardgoodShippingGroup(order);	
	}
	
	/**
	 * 
	 * @param order
	 * @return DigitalContactInfo
	 * @throws DigitalAppException
	 */
	public DigitalContactInfo getHardgoodShippingGroupAddress(Order order)throws DigitalAppException{		
		DigitalRepositoryContactInfo shippingAddress = new DigitalRepositoryContactInfo();
		try{
			shippingAddress = (DigitalRepositoryContactInfo) getHardgoodShippingGroup(order)
					.getShippingAddress();
		} catch (DigitalAppException e) {
			if (isLoggingError()) {
                logError("Error in getting Shipping Address: "+e);
			}
			throw e;			
		}
		return DigitalAddressUtil.getDSWContactInfo(shippingAddress);
	}
	
	/**
	 * 
	 * @return BillingShippingServices
	 */
	public BillingShippingServices getBillingShippingServices() {
		return billingShippingServices;
	}

	/**
	 * 
	 * @param billingShippingServices
	 */
	public void setBillingShippingServices(
			final BillingShippingServices billingShippingServices) {
		this.billingShippingServices = billingShippingServices;
	}

	/**
	 * 
	 * @return DigitalShippingGroupManager
	 */
	private DigitalShippingGroupManager getShippingGroupManager() {
		return (DigitalShippingGroupManager) getOrderManager()
				.getShippingGroupManager();
	}

	/**
	 *
	 * @return DigitalOrderTools
	 */
	public DigitalOrderTools getOrderTools() {
		return (DigitalOrderTools)getOrderManager().getOrderTools();
	}
	/**
	 *
	 * @return DigitalCommercePropertyManager
	 */
	public DigitalCommercePropertyManager getProfilePropertyManager(){
		return (DigitalCommercePropertyManager)this.getProfile().getProfileTools().getPropertyManager();
	}

	/**
	 *
	 * @return DigitalPaymentGroupManager
	 */
	private DigitalPaymentGroupManager getPaymentGroupManager() {
		return (DigitalPaymentGroupManager)getOrderManager().getPaymentGroupManager();
	}

}
