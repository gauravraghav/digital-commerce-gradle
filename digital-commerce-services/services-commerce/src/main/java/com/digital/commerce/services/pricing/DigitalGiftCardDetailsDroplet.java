package com.digital.commerce.services.pricing;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupManager;
import atg.dtm.UserTransactionDemarcation;
import atg.nucleus.naming.ParameterName;
import atg.repository.RemovedItemException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.giftcard.valueobject.DigitalGiftCardApproval;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;

@Getter
@Setter
public class DigitalGiftCardDetailsDroplet extends DynamoServlet {

  private static final String CLASSNAME = DigitalGiftCardDetailsDroplet.class.getName();
  private static final String OUTPUT = "output";
  private static final String GC = "giftCard";
  private static final String ID = "id";
  private static final String GC_NUMBER = "giftCardNumber";
  private static final String REM_AMOUNT = "remainingAmount";
  private static final String AMOUNT = "amount";
  private Order order;
  private PaymentGroupManager paymentGroupManager;
  private MessageLocator messageLocator;
  private OrderManager orderManager;

  private static final String PERFORM_MONITOR_NAME = "DigitalGiftCardDetailsDroplet";

  public static final ParameterName ORDERID = ParameterName.getParameterName("orderId");

  private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalGiftCardDetailsDroplet.class);

  /**
   * @param req
   * @param res
   * @throws ServletException
   * @throws IOException
   */
  public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res)
      throws ServletException, IOException {
    String orderIdpassed = (String) req.getLocalParameter(ORDERID);
    Order order = getOrder();
    if (null != orderIdpassed) {
      order = getOrderInstance(orderIdpassed);
    }

    final Map<String, Map<String, Object>> giftCardBalanceMap = new LinkedHashMap<>();
    List<String> gcPaymentGroup = new ArrayList();
    double gcTotal = 0.0;
    if (order != null) {

      if (order.getPaymentGroups() != null && order.getPaymentGroups().size() > 0) {
        final String METHOD_NAME = "service";
        DigitalPerformanceMonitorUtil
            .startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
        TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
        UserTransactionDemarcation td = null;
        try {
          td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
          for (int i = 0; i < order.getPaymentGroups().size(); i++) {
            final PaymentGroup pg = (PaymentGroup) order.getPaymentGroups().get(i);
            try {
              if (pg != null && GC.equalsIgnoreCase(pg.getPaymentGroupClassType())) {
                DigitalGiftCardApproval approval = null;
                final DigitalGiftCard gc = (DigitalGiftCard) pg;
                final DigitalPaymentGroupManager mgr = (DigitalPaymentGroupManager) getPaymentGroupManager();
                final String giftCardNumber = gc.getCardNumber();
                final String pin = gc.getPinNumber();
                double remainingAmount = 0;
                try {
                  if (DigitalStringUtil.isNotBlank(giftCardNumber) && DigitalStringUtil.isNotBlank(pin)) {
                    approval = mgr.getGiftCardBalance(giftCardNumber, pin, gc.getCurrencyCode());
                    double approvalAmount = approval.getApprovalAmount();
                    double amount = gc.getAmount();
                    gcTotal = gcTotal + gc.getAmount();
                    if ((approvalAmount - amount) > 0) {
                      remainingAmount = approvalAmount - amount;
                    }
                    Map<String, Object> giftCardHMap = new HashMap<>();
                    giftCardHMap.put(GC_NUMBER, giftCardNumber);
                    giftCardHMap.put(REM_AMOUNT,
                        new Double(DigitalCommonUtil.round(remainingAmount)).toString());
                    giftCardHMap.put(ID, gc.getId());
                    giftCardHMap.put(AMOUNT, new Double(DigitalCommonUtil.round(gc.getAmount())));
                    giftCardBalanceMap.put(gc.getId(), giftCardHMap);
                  }
                } catch (DigitalAppException e) {
                  logError(e.getMessage());
                  gcPaymentGroup.add(pg.getId());
                }
              }
            } catch (RemovedItemException e) {
              logger.info(
                  "Do nothing move on to avoid failing Attempt to use an item which has been "
                      + "removed. This happens when guest user login merge cart happens with "
                      + "previous cart empty cc payment group is removed.");
            }
          }
          if (!gcPaymentGroup.isEmpty()) {
            for (String paymentGroupId : gcPaymentGroup) {
              synchronized (order) {
                paymentGroupManager.removePaymentGroupFromOrder(order, paymentGroupId);
                orderManager.updateOrder(order);
              }
            }
          }
          if (!giftCardBalanceMap.isEmpty()) {
            req.setParameter("giftcards", giftCardBalanceMap);
            req.setParameter("giftcardstotal", DigitalCommonUtil.round(gcTotal));
            req.serviceLocalParameter(OUTPUT, req, res);
          }
        } catch (Exception ex) {
          logError("error :: ", ex);
          TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);

        } finally {
          TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
          TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
          DigitalPerformanceMonitorUtil
              .endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
        }
      }
    }
  }

  /**
   * @param orderId
   * @return OrderImpl
   */
  public OrderImpl getOrderInstance(String orderId) {
    OrderImpl objOrder = null;
    try {
      if (!DigitalStringUtil.isBlank(orderId)) {
        objOrder = (OrderImpl) getOrderManager().loadOrder(orderId);
      }
    } catch (CommerceException ce) {
      logError("Error Loading Order", ce);
    }
    return objOrder;
  }

}
