package com.digital.commerce.services.order;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderLookupService;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PropertyNameConstants;
import atg.commerce.order.ShippingGroup;
import atg.core.util.ContactInfo;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.QueryOptions;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.SortDirective;
import atg.repository.SortDirectives;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.inventory.yantra.YantraInventoryService;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrder;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderDetailsServiceRequest;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderDetailsServiceResponse;
import com.digital.commerce.integration.inventory.yantra.domain.YantraOrderLine;
import com.digital.commerce.integration.reward.bts.BtsRewardService;
import com.digital.commerce.integration.reward.domain.OrderLine;
import com.digital.commerce.integration.reward.domain.RewardServiceRequest;
import com.digital.commerce.integration.reward.domain.RewardServiceResponse;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.time.DateUtils;
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalOrderLookupService extends OrderLookupService {

	static final String CLASS_NAME = "DigitalOrderLookupService";

	private static final String IN_PROCESS = "In Process";

	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalOrderLookupService.class);

	private String source = "DSW";

	private String orderHistoryItemDescriptorName = "dsworderhistory";

	private String sortDateField = "orderPlacementDate";

	private YantraInventoryService service;

	private BtsRewardService rewardService;

	private String defaultUnknownLineItemStatus;

	private String defaultLineItemStatus;

	public List<DigitalOrderHistoryModel> getOrders(String pProfileId, String loyaltyNumber, Date startDate, Date endDate,
			int startIndex, int endIndex) throws CommerceException {

		List<DigitalOrderHistoryModel> result = new ArrayList<>();
		if (startDate.compareTo(DateUtils.addMonths(new Date(), -6)) < 0) {
			result = this.getOrderHistoryFromReward(pProfileId, loyaltyNumber, startDate, endDate);
		} else {
			result = this.getOrdersFromDSWOrderHistory(pProfileId, loyaltyNumber, startDate, endDate, startIndex,
					endIndex);
		}

		return result;
	}

	public int getTotalOrdersCount(String pProfileId, String loyaltyNumber, Date startDate, Date endDate)
			throws CommerceException {

		int totalOrderCount = 0;
		if (!(startDate.compareTo(DateUtils.addMonths(new Date(), -6)) < 0)) {
			totalOrderCount = this.getOrdersCountFromDSWOrderHistory(pProfileId, loyaltyNumber, startDate, endDate);
		}

		return totalOrderCount;
	}

	public List<Order> getOrdersFromATG(String pProfileId, Date startDate, Date endDate) {
		List<Order> result = new ArrayList<>();
		try {
			result = (List<Order>) (this.getOrderManager().loadOrders(this.getOrderManager().getOrderQueries()
					.getOrderIdsForProfileWithinDateRange(pProfileId, startDate, endDate)));
		} catch (CommerceException e) {
			logger.error(e);
		}

		return result;
	}

	public List<DigitalOrderHistoryModel> getOrdersFromDSWOrderHistory(String id, Date startDate, Date endDate,
			boolean loyaltyNumber) throws CommerceException {
		if (id == null) {
			throw new InvalidParameterException();
		}
		if (startDate == null) {
			throw new InvalidParameterException();
		}
		if ((endDate != null) && (startDate.after(endDate))) {
			throw new InvalidParameterException();
		}
		Query[] q = new Query[2];
		RepositoryItem[] items;
		try {
			RepositoryView rv = this.getOrderManager().getOrderTools().getOrderRepository()
					.getView(getOrderHistoryItemDescriptorName());
			QueryBuilder qb = rv.getQueryBuilder();
			q[0] = getDateRangeQuery(startDate, endDate);

			QueryExpression expr1 = qb.createPropertyQueryExpression(PropertyNameConstants.PROFILEID);

			if (loyaltyNumber)
				expr1 = qb.createPropertyQueryExpression("loyaltyNumber");

			QueryExpression expr2 = qb.createConstantQueryExpression(id);
			q[1] = qb.createComparisonQuery(expr1, expr2, QueryBuilder.EQUALS);
			Query query = qb.createAndQuery(q);
			items = rv.executeQuery(query);
		} catch (RepositoryException e) {
			logger.error(e);
			throw new CommerceException(e);
		}
		if (items == null) {
			return null;
		}
		List<DigitalOrderHistoryModel> orders = new ArrayList<>(items.length);
		for (int i = 0; i < items.length; i++) {
			orders.add(this.convertHistoryOrder(items[i]));
		}
		return orders;

	}

	/**
	 * Returns List of Order Histories. Uses both Profile ID and Loyalty Number
	 * to get Order History. This is required as Rewards over-night job is not
	 * merging the order history when merging the loyalty numbers
	 * 
	 * @param pProfileId
	 * @param loyaltyNumber
	 * @param startDate
	 * @param endDate
	 * @param pStartingIndex
	 * @param pEndingIndex
	 * @return List - list of all orders if any match the criteria passed
	 * @throws CommerceException
	 */
	public List<DigitalOrderHistoryModel> getOrdersFromDSWOrderHistory(String pProfileId, String loyaltyNumber,
			Date startDate, Date endDate, int pStartingIndex, int pEndingIndex) throws CommerceException {

		/*
		 * VALIDATION --------------------------------------------------- STARTS
		 * HERE
		 */
		// throw exception if both Profile ID and Loyalty Number are not
		// available
		if (DigitalStringUtil.isEmpty(pProfileId) && DigitalStringUtil.isEmpty(loyaltyNumber)) {
			throw new InvalidParameterException();
		}

		// Date validations
		if (startDate == null) {
			throw new InvalidParameterException();
		}
		if ((endDate != null) && (startDate.after(endDate))) {
			throw new InvalidParameterException();
		}
		/*
		 * VALIDATION --------------------------------------------------- ENDS
		 * HERE
		 */

		Query[] q = new Query[2];
		RepositoryItem[] items;
		try {
			RepositoryView rv = this.getOrderManager().getOrderTools().getOrderRepository()
					.getView(getOrderHistoryItemDescriptorName());
			QueryBuilder qb = rv.getQueryBuilder();
			q[0] = getDateRangeQuery(startDate, endDate);

			// Profile ID query
			QueryExpression expr1 = qb.createPropertyQueryExpression(PropertyNameConstants.PROFILEID);
			QueryExpression expr2 = qb.createConstantQueryExpression(pProfileId);

			// If loyalty number exists then let's OR it with Profile ID
			if (!DigitalStringUtil.isEmpty(loyaltyNumber)) {
				QueryExpression loyalityexpr1 = qb.createPropertyQueryExpression("loyaltyNumber");
				QueryExpression loyalityexpr2 = qb.createConstantQueryExpression(loyaltyNumber);

				q[1] = qb.createOrQuery(new Query[] { qb.createComparisonQuery(expr1, expr2, QueryBuilder.EQUALS),
						qb.createComparisonQuery(loyalityexpr1, loyalityexpr2, QueryBuilder.EQUALS) });

			} else {
				q[1] = qb.createComparisonQuery(expr1, expr2, QueryBuilder.EQUALS);
			}

			Query query = qb.createAndQuery(q);

			SortDirectives sortDirectives = new SortDirectives();
			sortDirectives.addDirective(new SortDirective(this.getSortDateField(), SortDirective.DIR_DESCENDING));

			items = rv.executeQuery(query, new QueryOptions(pStartingIndex, pEndingIndex, sortDirectives, null));
		} catch (RepositoryException e) {
			logger.error(e);
			throw new CommerceException(e);
		}
		if (items == null) {
			return null;
		}
		List<DigitalOrderHistoryModel> orders = new ArrayList<>(items.length);
		for (int i = 0; i < items.length; i++) {
			orders.add(this.convertHistoryOrder(items[i]));
		}
		return orders;

	}

	/**
	 * Returns List of Order Histories. Uses both Profile ID and Loyalty Number
	 * to get Order History. This is required as Rewards over-night job is not
	 * merging the order history when merging the loyalty numbers
	 * 
	 * @param pProfileId
	 * @param loyaltyNumber
	 * @param startDate
	 * @param endDate
	 * @param pStartingIndex
	 * @param pEndingIndex
	 * @return List - list of all orders if any match the criteria passed
	 * @throws CommerceException
	 */
	public List<DigitalOrderHistoryModel> getOrdersFromDSWOrderHistoryForDashboard(String pProfileId, String loyaltyNumber,
			Date startDate, Date endDate, int pStartingIndex, int pEndingIndex) throws CommerceException {

		/*
		 * VALIDATION --------------------------------------------------- STARTS
		 * HERE
		 */
		// throw exception if both Profile ID and Loyalty Number are not
		// available
		if (DigitalStringUtil.isEmpty(pProfileId) && DigitalStringUtil.isEmpty(loyaltyNumber)) {
			throw new InvalidParameterException();
		}

		// Date validations
		if (startDate == null) {
			throw new InvalidParameterException();
		}
		if ((endDate != null) && (startDate.after(endDate))) {
			throw new InvalidParameterException();
		}
		/*
		 * VALIDATION --------------------------------------------------- ENDS
		 * HERE
		 */

		Query[] q = new Query[3];
		RepositoryItem[] items;
		try {
			RepositoryView rv = this.getOrderManager().getOrderTools().getOrderRepository()
					.getView(getOrderHistoryItemDescriptorName());
			QueryBuilder qb = rv.getQueryBuilder();
			q[0] = getDateRangeQuery(startDate, endDate);

			// Profile ID query
			QueryExpression expr1 = qb.createPropertyQueryExpression(PropertyNameConstants.PROFILEID);
			QueryExpression expr2 = qb.createConstantQueryExpression(pProfileId);

			// If loyalty number exists then let's OR it with Profile ID
			if (!DigitalStringUtil.isEmpty(loyaltyNumber)) {
				QueryExpression loyalityexpr1 = qb.createPropertyQueryExpression("loyaltyNumber");
				QueryExpression loyalityexpr2 = qb.createConstantQueryExpression(loyaltyNumber);

				q[1] = qb.createOrQuery(new Query[] { qb.createComparisonQuery(expr1, expr2, QueryBuilder.EQUALS),
						qb.createComparisonQuery(loyalityexpr1, loyalityexpr2, QueryBuilder.EQUALS) });

			} else {
				q[1] = qb.createComparisonQuery(expr1, expr2, QueryBuilder.EQUALS);
			}

			// Demand location query
			QueryExpression demandLocationExpr1 = qb.createPropertyQueryExpression("demandLocation");
			QueryExpression demandLocationExpr2 = qb
					.createConstantQueryExpression(MultiSiteUtil.getWebsiteLocationId());

			q[2] = qb.createOrQuery(new Query[] {
					qb.createComparisonQuery(demandLocationExpr1, demandLocationExpr2, QueryBuilder.EQUALS)});

			Query query = qb.createAndQuery(q);

			SortDirectives sortDirectives = new SortDirectives();
			sortDirectives.addDirective(new SortDirective(this.getSortDateField(), SortDirective.DIR_DESCENDING));

			items = rv.executeQuery(query, new QueryOptions(pStartingIndex, pEndingIndex, sortDirectives, null));
		} catch (RepositoryException e) {
			logger.error(e);
			throw new CommerceException(e);
		}
		if (items == null) {
			return null;
		}
		List<DigitalOrderHistoryModel> orders = new ArrayList<>(items.length);
		for (int i = 0; i < items.length; i++) {
			orders.add(this.convertHistoryOrder(items[i]));
		}
		return orders;

	}

	/**
	 * Returns the number of orders that would be returned by given criteria.
	 * 
	 * @param pProfileId
	 * @param loyaltyNumber
	 * @param startDate
	 * @param endDate
	 * @return int - orderCount based on criteria filter
	 * @throws CommerceException
	 */
	public int getOrdersCountFromDSWOrderHistory(String pProfileId, String loyaltyNumber, Date startDate, Date endDate)
			throws CommerceException {

		/*
		 * VALIDATION --------------------------------------------------- STARTS
		 * HERE
		 */
		// throw exception if both Profile ID and Loyalty Number are not
		// available
		if (DigitalStringUtil.isEmpty(pProfileId) && DigitalStringUtil.isEmpty(loyaltyNumber)) {
			throw new InvalidParameterException();
		}

		// Date validations
		if (startDate == null) {
			throw new InvalidParameterException();
		}
		if ((endDate != null) && (startDate.after(endDate))) {
			throw new InvalidParameterException();
		}
		/*
		 * VALIDATION --------------------------------------------------- ENDS
		 * HERE
		 */

		Query[] q = new Query[2];
		int itemsCount = 0;
		try {
			RepositoryView rv = this.getOrderManager().getOrderTools().getOrderRepository()
					.getView(getOrderHistoryItemDescriptorName());
			QueryBuilder qb = rv.getQueryBuilder();
			q[0] = getDateRangeQuery(startDate, endDate);

			// Profile ID query
			QueryExpression expr1 = qb.createPropertyQueryExpression(PropertyNameConstants.PROFILEID);
			QueryExpression expr2 = qb.createConstantQueryExpression(pProfileId);

			// If loyalty number exists then let's OR it with Profile ID
			if (!DigitalStringUtil.isEmpty(loyaltyNumber)) {
				QueryExpression loyalityexpr1 = qb.createPropertyQueryExpression("loyaltyNumber");
				QueryExpression loyalityexpr2 = qb.createConstantQueryExpression(loyaltyNumber);

				q[1] = qb.createOrQuery(new Query[] { qb.createComparisonQuery(expr1, expr2, QueryBuilder.EQUALS),
						qb.createComparisonQuery(loyalityexpr1, loyalityexpr2, QueryBuilder.EQUALS) });

			} else {
				q[1] = qb.createComparisonQuery(expr1, expr2, QueryBuilder.EQUALS);
			}

			Query query = qb.createAndQuery(q);

			itemsCount = rv.executeCountQuery(query);
		} catch (RepositoryException e) {
			logger.error(e);
			throw new CommerceException(e);
		}

		return itemsCount;

	}

	public List<DigitalOrderHistoryModel> getDSWOrderHistoryByOrderId(String orderId) throws CommerceException {
		if (orderId == null) {
			throw new InvalidParameterException();
		}
		Query[] q = new Query[1];
		RepositoryItem[] items;
		try {
			RepositoryView rv = this.getOrderManager().getOrderTools().getOrderRepository()
					.getView(getOrderHistoryItemDescriptorName());
			QueryBuilder qb = rv.getQueryBuilder();
			QueryExpression expr1 = qb.createPropertyQueryExpression("orderId");
			QueryExpression expr2 = qb.createConstantQueryExpression(orderId);

			q[0] = qb.createComparisonQuery(expr1, expr2, QueryBuilder.EQUALS);
			Query query = qb.createAndQuery(q);
			items = rv.executeQuery(query);
		} catch (RepositoryException e) {
			logger.error(e);
			throw new CommerceException(e);
		}
		if (items == null) {
			return null;
		}
		List<DigitalOrderHistoryModel> orders = new ArrayList<>(items.length);
		for (int i = 0; i < items.length; i++) {
			orders.add(this.convertHistoryOrder(items[i]));
		}
		return orders;

	}

	private Query getDateRangeQuery(Date pFrom, Date pTo) throws CommerceException {
		if (pFrom == null) {
			throw new InvalidParameterException();
		}
		ArrayList<Query> list = new ArrayList<>(2);
		QueryBuilder qb;
		QueryExpression expr1, expr2;
		Query query;
		try {
			qb = this.getOrderManager().getOrderTools().getOrderRepository()
					.getView(getOrderHistoryItemDescriptorName()).getQueryBuilder();
			expr1 = qb.createPropertyQueryExpression(this.getSortDateField());
			expr2 = qb.createConstantQueryExpression(pFrom);
			query = qb.createComparisonQuery(expr1, expr2, QueryBuilder.GREATER_THAN_OR_EQUALS);
		} catch (RepositoryException e) {
			throw new CommerceException(e);
		}
		list.add(query);
		if (pTo != null) {
			try {
				expr1 = qb.createPropertyQueryExpression(this.getSortDateField());
				expr2 = qb.createConstantQueryExpression(pTo);
				query = qb.createComparisonQuery(expr1, expr2, QueryBuilder.LESS_THAN_OR_EQUALS);
			} catch (RepositoryException e) {
				throw new CommerceException(e);
			}
			list.add(query);
		}
		Query andQuery;
		try {
			andQuery = qb.createAndQuery((Query[]) list.toArray(new Query[0]));
		} catch (RepositoryException e) {
			logger.error(e);
			throw new CommerceException(e);
		}
		return andQuery;
	}

	public RepositoryItem orderExists(String pOrderId) throws CommerceException {
		OrderTools orderTools = this.getOrderManager().getOrderTools();
		Repository rep = orderTools.getOrderRepository();
		try {
			RepositoryItem item = rep.getItem(pOrderId, getOrderHistoryItemDescriptorName());
			if (item == null) {
				return null;
			} else {
				return item;
			}
		} catch (RepositoryException e) {
			throw new CommerceException(e);
		}
	}

	public DigitalOrderHistoryModel validateHistoryEmailAddress(String orderIdpassed, String email) {
		try {
			RepositoryView rv = this.getOrderManager().getOrderTools().getOrderRepository()
					.getView(getOrderHistoryItemDescriptorName());
			QueryBuilder qb = rv.getQueryBuilder();
			Query[] q = new Query[2];
			q[0] = qb.createComparisonQuery(qb.createPropertyQueryExpression("orderId"),
					qb.createConstantQueryExpression(orderIdpassed), QueryBuilder.EQUALS);
			q[1] = qb.createComparisonQuery(qb.createPropertyQueryExpression("email"),
					qb.createConstantQueryExpression(email), QueryBuilder.EQUALS);
			Query query = qb.createAndQuery(q);
			RepositoryItem[] items = rv.executeQuery(query);
			if (items != null && items.length > 0) {
				return convertHistoryOrder(items[0]);
			}

		} catch (RepositoryException e) {
			logger.error(e);
		}
		return null;
	}

	public DigitalOrderHistoryModel validateYantraEmailAddress(String orderIdpassed, String email) {
		try {
			YantraOrderDetailsServiceRequest request = new YantraOrderDetailsServiceRequest();
			request.setEmailId(email);
			request.setOrderId(orderIdpassed);
			YantraOrderDetailsServiceResponse response = this.service.orderDetails(request);
			if (response != null && response.getYantraOrders().size() > 0) {
				YantraOrder order = response.getYantraOrders().get(0);
				return convertYantraOrder(order);
			}
		} catch (DigitalIntegrationException e) {
			logger.error(e);
		}
		return null;
	}

	/**
	 * @param atgprofileId
	 * @param loyaltyNumber
	 * @param startDate
	 * @param endDate
	 * @return List
	 */
	private List<DigitalOrderHistoryModel> getOrderHistoryFromReward(String atgprofileId,
			String loyaltyNumber,
			Date startDate, Date endDate) {
		try {
			RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();
			rewardServiceRequest.setHistoryProfileId(atgprofileId);

			rewardServiceRequest.setHistoryStartDate(startDate);
			rewardServiceRequest.setHistoryEndDate(endDate);

			RewardServiceResponse rewardServiceResponse = this.rewardService
					.orderHistory(rewardServiceRequest);

			if (rewardServiceResponse.isSuccess() && rewardServiceResponse != null) {
				if (rewardServiceResponse.getOrders() != null
						&& rewardServiceResponse.getOrders().size() > 0) {
					List<DigitalOrderHistoryModel> result = new ArrayList<>(
							rewardServiceResponse.getOrders().size());
					for (com.digital.commerce.integration.reward.domain.Order order : rewardServiceResponse
							.getOrders()) {
						result.add(this.convertRewardServiceOrder(order));
					}
					return result;
				}
			}

		} catch (DigitalIntegrationException e) {
			logger.error(e);
		}
		return null;
	}

	/**
	 * Transform Yantra response to common order object
	 * 
	 * @param order
	 * @return
	 */
	private DigitalOrderHistoryModel convertYantraOrder(YantraOrder order) {
		DigitalOrderHistoryModel detail = new DigitalOrderHistoryModel();
		detail.setAddDate(order.getOrderDate().toGregorianCalendar().getTime());
		detail.setOrderPlacementDate(order.getOrderDate().toGregorianCalendar().getTime());
		detail.setOrderId(order.getOrderNo());
		detail.setStatus(order.getStatus());
		detail.setTotal(order.getGrandTotal());
		if (order.getOrderLines() != null) {
			List<DigitalOrderHistoryItem> items = new ArrayList<>(order.getOrderLines().size());
			for (YantraOrderLine line : order.getOrderLines()) {
				DigitalOrderHistoryItem item = new DigitalOrderHistoryItem();
				item.setProductId(line.getItem().getCustomerItem());
				item.setSkuId(line.getItem().getItemID());
				if (line.getOrderStatuses() != null && line.getOrderStatuses().size() > 0) {
					item.setStatusDescription(line.getOrderStatuses().get(0).getStatusDescription());// This
																										// is
																										// yantra
																										// provided
																										// description.
																										// Added
																										// this
																										// if
																										// we
																										// need
																										// to
																										// do
																										// anything
																										// with
																										// Yantra's
																										// description
																										// in
																										// future
					String statusText = (null == DigitalCommonUtil
							.getATGMappedOrderLineStatus(line.getOrderStatuses().get(0).getStatus())) ? IN_PROCESS
									: DigitalCommonUtil
											.getATGMappedOrderLineStatus(line.getOrderStatuses().get(0).getStatus());
					item.setStatus(statusText);
					item.setStatusDate(line.getOrderStatuses().get(0).getStatusDate().toGregorianCalendar().getTime());
					// setting the Quantity from Order Status level instead of
					// from line.getOrderLineOverallTotals().getPricingQty()
					if (line.getOrderStatuses().get(0).getStatusQty() != null)
						item.setQuantity(line.getOrderStatuses().get(0).getStatusQty().intValue());
				}
				if (line.getOrderLineOverallTotals() != null) {
					// item.setQuantity(
					// line.getOrderLineOverallTotals().getPricingQty().intValue()
					// );
					item.setUnitPrice(line.getOrderLineOverallTotals().getUnitPrice());
				}
				// adding tracking number and Carrier
				item.setTrackingNumber(line.getTrackingNumber());
				item.setCarrier(DigitalCommonUtil.getCarrierCode(line.getCarrier()));

				if (DigitalStringUtil.equals(line.getFulfillmentType(), "BOPIS")
						|| DigitalStringUtil.equals(line.getFulfillmentType(), "BOSTS")) {
					item.setShipsToStore(true);
				}
				if (line.getPersoninfoshipto() != null) {
					StringBuffer address = new StringBuffer(line.getPersoninfoshipto().getAddress1() + " ");
					address = address.append(line.getPersoninfoshipto().getAddress2() + " ");
					address = address.append(line.getPersoninfoshipto().getCity() + " ");
					address = address.append(line.getPersoninfoshipto().getState() + " ");
					address = address.append(line.getPersoninfoshipto().getPostalCode());

					item.setShipToAddress(address.toString());
					item.setShipToFirstName(line.getPersoninfoshipto().getFirstName());
					item.setShipToLastName(line.getPersoninfoshipto().getLastName());
				}

				item.setFulfillmentType(line.getFulfillmentType());

				items.add(item);
			}
			detail.setCommerceItems(items);
		}
		return detail;
	}

	/**
	 * Transform Reporsitory item to common order object
	 * 
	 * @param order
	 * @return
	 */
	private DigitalOrderHistoryModel convertHistoryOrder(RepositoryItem order) {
		DigitalOrderHistoryModel detail = new DigitalOrderHistoryModel();
		detail.setAddDate((Date) order.getPropertyValue("addDate"));
		detail.setCurrencyCode((String) order.getPropertyValue("currencyCode"));
		detail.setDemandLocation((String) order.getPropertyValue("demandLocation"));
		detail.setLocale((String) order.getPropertyValue("locale"));
		detail.setLoyalNumber((String) order.getPropertyValue("loyaltyNumber"));
		detail.setOrderId((String) order.getPropertyValue("orderId"));
		detail.setOrderPlacementDate((Date) order.getPropertyValue("orderPlacementDate"));
		detail.setSiteId((String) order.getPropertyValue("siteId"));
		detail.setStatus((String) order.getPropertyValue("status"));
		detail.setStatusDate((Date) order.getPropertyValue("statusDate"));
		Object total = order.getPropertyValue("total");
		if(null != total) {
			detail.setTotal((Double) total);
		} else {
			detail.setTotal(0.0);
		}
		if (order.getPropertyValue("commerceItems") != null) {
			List<DigitalOrderHistoryItem> items = new ArrayList<>(
                    ((List) order.getPropertyValue("commerceItems")).size());
			for (RepositoryItem line : (List<RepositoryItem>) order.getPropertyValue("commerceItems")) {
				DigitalOrderHistoryItem item = new DigitalOrderHistoryItem();
				item.setAddDate((Date) order.getPropertyValue("addDate"));
				item.setFulfillLocationId((String) line.getPropertyValue("fulfillLocationId"));
				item.setOrderPlacementDate((Date) line.getPropertyValue("orderPlacementDate"));
				item.setPickupLocationId((String) line.getPropertyValue("pickupLocationId"));
				item.setProductDescription((String) line.getPropertyValue("productDescription"));
				item.setProductId((String) line.getPropertyValue("productId"));
				Object qty = line.getPropertyValue("quantity");
				if(null != qty) {
					item.setQuantity((Integer) qty);
				} else {
					item.setQuantity(0);
				}
				item.setReturnLocationId((String) line.getPropertyValue("returnLocationId"));
				item.setShipToFirstName((String) line.getPropertyValue("shipToFirstName"));
				item.setShipToLastName((String) line.getPropertyValue("shipToLastName"));
				item.setGcFromName((String) line.getPropertyValue("gcFromName"));
				item.setGcToName((String) line.getPropertyValue("gcToName"));
				item.setGcFromEmailAddress((String) line.getPropertyValue("gcFromEmailAddress"));
				item.setGcToEmailAddress((String) line.getPropertyValue("gcToEmailAddress"));
				item.setSkuId((String) line.getPropertyValue("skuId"));
				String lineStatus = (String) line.getPropertyValue("status");
				if (this.getDefaultUnknownLineItemStatus().equalsIgnoreCase(lineStatus)) {
					lineStatus = this.getDefaultLineItemStatus();
				}
				item.setStatus(lineStatus);
				item.setStatusDate((Date) line.getPropertyValue("statusDate"));
				item.setTrackingNumber((String) line.getPropertyValue("trackingNumber"));
				Object unitPrice = line.getPropertyValue("unitPrice");
				if (unitPrice != null) {
					item.setUnitPrice((Double) unitPrice);
				}
				item.setUpdateDate((Date) line.getPropertyValue("updateDate"));
				Object carrier = line.getPropertyValue("carrier");
				if (carrier != null) {
					String carrierCode = DigitalCommonUtil.getCarrierCode((String) carrier);
					item.setCarrier(carrierCode);
				}

				/*
				 * In case of split shipment, Yantra will send Tracking number
				 * and quantity will send as delimiter string in
				 * TrackingNo="409416677654:1.00;409416677890:1.00;" of
				 * OmniOrder TrackingNumber1:Qty1;TrackingNumber2:Qty2 so on so
				 * forth. It will persisted in OrderHistory persisted as is,
				 * while viewing it, the order lines will be cloned with updated
				 * tracking number and quantity.
				 */
				String trackingNumber = item.getTrackingNumber();
				if (DigitalStringUtil.isNotBlank(trackingNumber)
						&& DigitalStringUtil.contains(trackingNumber, new String[] { ":", ";" })) {

					String[] trackingNumberQuantityArray = trackingNumber.split(";");

					for (String trackingNumberQuantity : trackingNumberQuantityArray) {
						String trackingNum = null;
						String[] trqArray = trackingNumberQuantity.split(":");
						if (trackingNumberQuantityArray.length > 1 && trqArray.length < 2) {
							logError("Quantity is missing for split quantity shipment from OrderHistoryItem ::"
									+ " OrderId - " + detail.getOrderId() + " :: TrackingNum - " + trackingNumber);
						} else if (trackingNumberQuantityArray.length == 1) {
							trackingNum = trqArray[0];
							item.setTrackingNumber(trackingNum);
							items.add(item);
						} else {
							trackingNum = trqArray[0];

							int quantity = (int) Double.parseDouble(trqArray[1]);
							DigitalOrderHistoryItem itemCloned = (DigitalOrderHistoryItem) SerializationUtils.clone(item);
							itemCloned.setQuantity(quantity);
							itemCloned.setTrackingNumber(trackingNum);
							items.add(itemCloned);
						}
					}
				} else {
					items.add(item);
				}
			}

			detail.setCommerceItems(items);
		}
		return detail;
	}

	/**
	 * Transform RewardService response to common order object
	 * 
	 * @param order
	 * @return
	 */
	private DigitalOrderHistoryModel convertRewardServiceOrder(
			com.digital.commerce.integration.reward.domain.Order order) {
		DigitalOrderHistoryModel detail = new DigitalOrderHistoryModel();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		detail.setOrderId(order.getOrderNumber());
		try {
			df.parse(order.getOrderDate());
			detail.setAddDate(df.getCalendar().getTime());
			detail.setOrderPlacementDate(df.getCalendar().getTime());
		} catch (ParseException e) {
			logger.error("Parsing Error on the date sent from Rewards system");
		}
		if (order.getOrderLine() != null) {
			List<DigitalOrderHistoryItem> items = new ArrayList<>(order.getOrderLine().size());
			for (OrderLine line : order.getOrderLine()) {
				DigitalOrderHistoryItem item = new DigitalOrderHistoryItem();
				if (DigitalStringUtil.isNotBlank(line.getSkuID()) && !line.getSkuID().endsWith("-GC")
						&& line.getSkuID().length() > 16) {
					item.setProductId(line.getSkuID().substring(10, 16));
				} else if (DigitalStringUtil.isNotBlank(line.getSkuID()) && line.getSkuID().endsWith("-GC")) {
					item.setProductId(line.getSkuID());
				} else {
					logger.error("Unknown Product Id in Rewards Order History for SKU " + line.getSkuID()
							+ " for the order # " + order.getOrderNumber());
				}
				item.setSkuId(line.getSkuID());
				item.setFulfillLocationId(line.getFulfillmentLocationNumber());
				item.setPickupLocationId(line.getPickupLocationNumber());
				if (DigitalStringUtil.isNotBlank(line.getPickupLocationNumber())) {
					item.setShipsToStore(true);
				}
				item.setQuantity(line.getItemQuantity() == null ? 0 : Integer.parseInt(line.getItemQuantity()));
				item.setShipToFirstName(line.getShipToFirstName());
				item.setShipToLastName(line.getShipToLastName());
				item.setStatus(line.getItemFulfillmentStatusCode());
				if(null != line.getItemFulfillmentStatusDate()) {
					try {
						df.parse(line.getItemFulfillmentStatusDate());
						item.setStatusDate(df.getCalendar().getTime());
					} catch (ParseException e) {
						logger.error("Parsing Error on the date sent from Rewards system");
					}
				}
				item.setReturnLocationId(line.getReturnLocationNumber());
				item.setTrackingNumber(line.getShippingTrackingNumber());
				item.setFulfillmentType(this.getFulfillmentTypeFromRewards(line.getFulfillmentTypeCode()));

				if ("BOPIS".equalsIgnoreCase(line.getFulfillmentTypeCode())
						|| "BOSTS".equalsIgnoreCase(line.getFulfillmentTypeCode())
						|| "BIS".equalsIgnoreCase(line.getFulfillmentTypeCode())) {

					item.setShipsToStore(true);
				} else {
					item.setShipsToStore(false);
				}

				items.add(item);
			}
			detail.setTotal(order.getTotalOrderAmount());
			detail.setCommerceItems(items);
		}
		return detail;
	}

	String getFulfillmentTypeFromRewards(String code) {
		String fulfillmentType = ""; // default ship to home
		if ("BOSTS".equalsIgnoreCase(code) || "BOPIS".equalsIgnoreCase(code) || "BIS".equalsIgnoreCase(code)) {// BIS
																												// -
																												// Buy
																												// in
																												// Store
			fulfillmentType = code;
		} // else all other types (SFS,SFC,SDS) are ship to home

		return fulfillmentType;
	}

	/**
	 * @param order
	 * @return String
	 */
	public String lookupEmailFromOrder(DigitalOrderImpl order) {

		final String METHOD_NAME = "lookupEmailFromOrder";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
		long startTime = 0l;
		String emailAddress = "";
		try {
			if (isLoggingDebug()) {
				startTime = System.currentTimeMillis();
			}
			List<PaymentGroup> paymentGroupList = order.getPaymentGroups();
			if (isLoggingDebug()) {
				logDebug("orderIdpassed " + order.getId());
				logDebug("OrderId from paymentGroupList" + paymentGroupList);
			}
			if (paymentGroupList != null) {
				for (PaymentGroup pg : paymentGroupList) {
					if (pg instanceof CreditCard) {
						if (isLoggingDebug()) {
							logDebug("In DigitalOrderLookupService CC pay group --> "
									+ pg.getClass());
						}
						DigitalCreditCard creditCard = (DigitalCreditCard) pg;
						DigitalRepositoryContactInfo cf = (DigitalRepositoryContactInfo) creditCard.getBillingAddress();
						if (cf != null) {
							emailAddress = cf.getEmail();
							if (isLoggingDebug()) {
								logDebug("In DigitalOrderLookupService Email from CC Pay Group" + emailAddress);
							}
							return emailAddress;
						}
					} else if (pg instanceof PaypalPayment) {
						if (isLoggingDebug()) {
							logDebug("In DigitalOrderLookupService PP pay group --> " + pg.getClass());
						}
						PaypalPayment paypalPayment = (PaypalPayment) pg;
						emailAddress = paypalPayment.getBillingAddress().getEmail();
						return emailAddress;
					} else {
						Collection<ShippingGroup> shippingGroups = order.getShippingGroups();
						for (ShippingGroup sg : shippingGroups) {
							if (sg instanceof HardgoodShippingGroup) {
								if (isLoggingDebug()) {
									logDebug("In DigitalOrderLookupService Not found in Billing Address so get " +
											"it from Shipping Address --> " + sg.getClass());
								}
								Collection<CommerceItemRelationship> relationships = sg.getCommerceItemRelationships();
								if (relationships != null && relationships.size() > 0) {
									HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sg;
									ContactInfo shippingAddress = (ContactInfo) hgSg.getShippingAddress();
									emailAddress = shippingAddress.getEmail();
									return emailAddress;
								}
							}
						}
					}
				}
				if (paymentGroupList.size() == 0) {
					Collection<ShippingGroup> shippingGroups = order.getShippingGroups();
					for (ShippingGroup sg : shippingGroups) {
						if (sg instanceof HardgoodShippingGroup) {
							if (isLoggingDebug()) {
								logDebug("In DigitalOrderLookupService Not found in Billing Address so get it " +
										"from Shipping Address --> " + sg.getClass());
							}
							Collection<CommerceItemRelationship> relationships = sg.getCommerceItemRelationships();
							if (relationships != null && relationships.size() > 0) {
								HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sg;
								ContactInfo shippingAddress = (ContactInfo) hgSg.getShippingAddress();
								emailAddress = shippingAddress.getEmail();
								return emailAddress;
							}
						}
					}
				}
			}

		} catch (Exception e) {
			logError("Exception from lookupEmailFromOrder:::", e);
		}finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
			if (isLoggingDebug()) {
				long endTime = System.currentTimeMillis();
				logDebug("METHOD_NAME" + METHOD_NAME + " took " + (endTime - startTime) + " milliseconds to process");
			}
		}
		return emailAddress;
	}

}
