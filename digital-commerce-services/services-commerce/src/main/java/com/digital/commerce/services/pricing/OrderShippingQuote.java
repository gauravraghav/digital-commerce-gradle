/**
 * 
 */
package com.digital.commerce.services.pricing;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class OrderShippingQuote extends ShippingQuote {
	private String	messageSeverity;

	private String	messageText;

	private double	baseRatesAmount;								// used for calcluation

	private String	orderId;

	private Map		shippingDestinationRates	= new HashMap();

	public void addShippingDestinationRate( ShippingDestinationQuote sdr ) {
		getShippingDestinationRates().put( sdr.getShippingDestinationId(), sdr );
	}

}
