package com.digital.commerce.services.order.payment.paypal.processor;

import atg.payment.PaymentStatus;

/**
 *
 * Paypal payment status interface..
 * place holder for any future enhancements.
 */
public interface PaypalPaymentStatus extends PaymentStatus{

    public String getAuthorizationCode();

}
