package com.digital.commerce.services.pricing;

import atg.payment.tax.TaxableItemImpl;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalTaxableItemImpl extends TaxableItemImpl {

	private static final long serialVersionUID = 1L;
	
	private String storeId;
	
	private String shipType;
	
	public DigitalTaxableItemImpl (TaxableItemImpl ti){
		this.setAmount(ti.getAmount());
		this.setCatalogRefId(ti.getCatalogRefId());
		this.setProductId(ti.getProductId());
		this.setQuantity(ti.getQuantity());
		this.setTaxStatus(ti.getTaxStatus());
		
	}
	
	public DigitalTaxableItemImpl (){
		
	}
}