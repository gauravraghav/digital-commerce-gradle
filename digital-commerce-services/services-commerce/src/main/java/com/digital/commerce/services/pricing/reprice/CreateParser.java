package com.digital.commerce.services.pricing.reprice;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import lombok.Getter;
import lombok.Setter;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.digital.commerce.common.logger.DigitalLogger;

/* Class to create a parser and parse an XML file
*/
@Getter
@Setter
public class CreateParser {

	private DefaultHandler		handler;
	private final SAXParser	saxParser;
	private static final SAXParserFactory SAXPARSER_FACTORY = SAXParserFactory.newInstance();
	static {
		SAXPARSER_FACTORY.setNamespaceAware(true);
		SAXPARSER_FACTORY.setValidating(true);
	}
	
	private static final DigitalLogger	logger	= DigitalLogger.getLogger( CreateParser.class );

	/** Constructor
	 * 
	 * @param handler - DefaultHandler for the SAX parser
	 * @throws SAXException
	 * @throws ParserConfigurationException */
	public CreateParser( DefaultHandler handler ) throws ParserConfigurationException, SAXException {
		this.handler = handler;
		saxParser = SAXPARSER_FACTORY.newSAXParser();
	}

	/** Constructor
	 * 
	 * @param handler - DefaultHandler for the SAX parser
	 * @throws SAXException
	 * @throws ParserConfigurationException */
	public CreateParser() throws ParserConfigurationException, SAXException {
		saxParser = SAXPARSER_FACTORY.newSAXParser();
	}

	/** Parse a File
	 * 
	 * @param file - File */
	public void parse( File file ) {
		try {
			saxParser.parse( file, handler );
		} catch( SAXException | IOException e ) {
			logger.error( "Exception: ", e );
		}
  }

	/** Parse a URI
	 * 
	 * @param uri - String */
	public void parse( String uri ) {
		try {
			saxParser.parse( uri, handler );
		} catch( SAXException | IOException e ) {
			logger.error( "Exception: ", e );
		}
  }

	/** Parse a Stream
	 * 
	 * @param stream - InputStream */
	public void parse( InputStream stream ) {
		try {
			saxParser.parse( stream, handler );
		} catch( SAXException | IOException e ) {
			logger.error( "Exception: ", e );
		}
  }
}
