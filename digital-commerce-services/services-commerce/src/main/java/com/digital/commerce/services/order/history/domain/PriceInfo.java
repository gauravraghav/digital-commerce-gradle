
package com.digital.commerce.services.order.history.domain;

import javax.annotation.Generated;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
@Getter
@Setter
@ToString
public class PriceInfo {

    private String currencyCode;
    private Integer amount;
    private String discountable;
    private Integer rawTotalPrice;
    private Integer listPrice;
    private Integer quantity;
    private Double total;

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(currencyCode).append(amount).append(discountable).append(rawTotalPrice).append(listPrice).append(quantity).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PriceInfo) == false) {
            return false;
        }
        PriceInfo rhs = ((PriceInfo) other);
        return new EqualsBuilder().append(currencyCode, rhs.currencyCode).append(amount, rhs.amount).append(discountable, rhs.discountable).append(rawTotalPrice, rhs.rawTotalPrice).append(listPrice, rhs.listPrice).append(quantity, rhs.quantity).isEquals();
    }

}
