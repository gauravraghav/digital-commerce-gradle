package com.digital.commerce.services.promotions.collections.validator;

import atg.repository.RepositoryItem;
import atg.service.collections.validator.CollectionObjectValidator;
import lombok.Getter;
import lombok.Setter;

/**
 * DigitalGlobalPromotionValidator validates an item based on its RestrictionType
 * properties.
 * 
 */
@Getter
@Setter
public class DigitalGlobalPromotionValidator implements
		CollectionObjectValidator {
	
	/**
	 * property: loadOnlyGlobalPromotions
	 */
		
	private boolean loadOnlyGlobalPromotions;

	
	/**
	 * property: gloablPromotionPropertyName
	 */
	protected String globalPromotionPropertyName;

	/**
	 * This method validates the passed in object (repository items) based on
	 * whether or not any of its items exist in the
	 * current restriction type.
	 * 
	 * @param object
	 *            to validate
	 * @return true if the object passes validation or if no validation was
	 *         performed.
	 */
	@Override
	public boolean validateObject(Object pObject) {
		if (loadOnlyGlobalPromotions) {
			if (!(pObject instanceof RepositoryItem)) {
				return false;
			}

			// Perform the filtering
			RepositoryItem item = (RepositoryItem) pObject;

			return (boolean) (Boolean) item.getPropertyValue(getGlobalPromotionPropertyName().trim());

		}
		return true;
	}
}
