package com.digital.commerce.services.order;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DigitalAppliedOffersBean {

  private List<DigitalOfferDetailsBean> orderPromotions;
  private List<DigitalOfferDetailsBean> itemPromotions;
  private List<DigitalOfferDetailsBean> rewardsPromotions;
  private List<DigitalOfferDetailsBean> pointsPromotions;
}
