package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by MK402314 on 12/6/2017.
 */
@Getter
@Setter
public class RewardsGiftItem {

    String firstName;
    String lastName;
    String birthDay;
    String birthMonth;
    String email;
    String giftMessage;
    String id;
    String status;
    String profileId;
    Date addDate;
    Date updateDate;
    String addUsername;
    String updateUsername;
    Date giftSendDate;
}
