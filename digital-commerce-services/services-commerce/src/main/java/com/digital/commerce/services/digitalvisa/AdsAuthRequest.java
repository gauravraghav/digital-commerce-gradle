package com.digital.commerce.services.digitalvisa;

import java.io.Serializable;

import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdsAuthRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6628275199949459592L;
	
	// no idea why these checks are in LiveSite code. Carrying as is
	private static final int FIRST_NAME_MAX_LENGTH = 15;
	private static final int LAST_NAME_MAX_LENGTH = 20;
	private static final int ADDRESS_MAX_LENGTH = 20;
	private static final int CITY_MAX_LENGTH = 20;
	private static final int STATE_MAX_LENGTH = 2;
	private static final int ZIP_MAX_LENGTH = 5;
	private static final int EMAIL_MAX_LENGTH = 80;
	private static final int PHONE_NUMBER_MAX_LENGTH = 10;
	private static final int MEMBER_NUMBER_MAX_LENGTH = 25;

	private String applyFormUrl;
	private String manageUrl;
	private String authCode;
	private String firstName;
	private String lastName;
	private String address="";
	private String city="";
	private String state="";
	private String zip="";
	private String email;
	private String phoneNumber="";
	private String memberNumber;
	private boolean isCardHolder;

	public String getFirstName() {
		return truncate(firstName, FIRST_NAME_MAX_LENGTH);
	}
	public String getLastName() {
		return truncate(lastName, LAST_NAME_MAX_LENGTH);
	}
	public String getAddress() {
		return truncate(address, ADDRESS_MAX_LENGTH);
	}
	public String getCity() {
		return truncate(city, CITY_MAX_LENGTH);
	}
	public String getState() {
		return truncate(state, STATE_MAX_LENGTH);
	}
	public String getZip() {
		return truncate(zip, ZIP_MAX_LENGTH);
	}
	public String getEmail() {
		return truncate(email, EMAIL_MAX_LENGTH);
	}
	public String getPhoneNumber() {
		return truncate(phoneNumber, PHONE_NUMBER_MAX_LENGTH);
	}
	public String getMemberNumber() {
		return truncate(memberNumber, MEMBER_NUMBER_MAX_LENGTH);
	}

	private String truncate(String string, int length) {
		if (DigitalStringUtil.isBlank(string)) {
			return string;
		}
		if (string.length() <= length) {
			return string;
		}
		return string.substring(0, length);
	}
}
