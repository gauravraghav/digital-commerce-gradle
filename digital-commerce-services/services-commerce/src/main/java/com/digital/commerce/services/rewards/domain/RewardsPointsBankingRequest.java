/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RewardsPointsBankingRequest {

	private String profileId;
	private String memberId;
	private String doBankPoints;

}
