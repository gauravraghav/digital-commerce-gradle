package com.digital.commerce.services.order.processor;

import java.util.HashMap;
import java.util.Set;

import atg.commerce.order.Order;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import atg.userprofiling.Profile;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class ProceCheckInvalidRewardCertificate implements PipelineProcessor{ 
	
	private DigitalRewardsManager rewardCertificateManager;
	
	private static final String INVALID_CERTIFICATE_IN_ORDER = "invalidCertificateInOrder";
	
	private MessageLocator messageLocator;
	
	private boolean loadOrder = false;
	
	private DigitalProfileTools	profileTools;

	public int runProcess(Object paramObject, PipelineResult paramPipelineResult)
		    throws Exception{
		HashMap map = (HashMap)paramObject;
	    Order order = (Order)map.get("Order");
	    Profile profile = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PROFILE, Profile.class);
	    if(getOrderTools().isOrderIncompleteState(order) && this.getProfileTools().isLoggedIn(profile) && !this.getProfileTools().isDSWAnanymousUser(profile) 
	    		&& DigitalStringUtil.isNotBlank(this.getProfileTools().getLoyaltyNumber(profile))){
		    Set<RepositoryItem> orderCertificates = this.getRewardCertificateManager().getOrderCertificates( order );
		    int originalCertificatesCount = (orderCertificates != null ? orderCertificates.size() : 0);
		    this.getRewardCertificateManager().checkInvalidCertificates(order);
		    orderCertificates = this.getRewardCertificateManager().getOrderCertificates( order );
		    int newCertificatesCount = (orderCertificates != null ? orderCertificates.size() : 0);
		    if(originalCertificatesCount != newCertificatesCount && !isLoadOrder()){
		    	paramPipelineResult.addError(INVALID_CERTIFICATE_IN_ORDER, this.getMessageLocator().getMessageString(INVALID_CERTIFICATE_IN_ORDER));
		    	return -1;
		    } else {
		    	return 1;
		    }
	    }else{
	    	return 1;
	    }
	}
		  
    public int[] getRetCodes(){
      return new int[]{1};
    }

    public DigitalOrderTools getOrderTools(){
    	return (DigitalOrderTools)this.getProfileTools().getOrderManager().getOrderTools();
    }
}
