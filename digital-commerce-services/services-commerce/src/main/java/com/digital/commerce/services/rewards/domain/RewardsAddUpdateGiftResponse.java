package com.digital.commerce.services.rewards.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by mk402314 on 12/7/2017.
 */
@Getter
@Setter
public class RewardsAddUpdateGiftResponse extends ResponseWrapper {

  int count;
  private RewardsGiftItem item;
}
