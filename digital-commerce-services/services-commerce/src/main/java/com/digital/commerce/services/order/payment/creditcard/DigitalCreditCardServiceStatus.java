/**
 * 
 */
package com.digital.commerce.services.order.payment.creditcard;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DigitalCreditCardServiceStatus extends ServicesStatus {
	
	private static final long	serialVersionUID						= -1693959343864471998L;

	public static final String	DECISION_ACCEPT							= "Approved";

	public static final String	DECISION_ERROR							= "ERROR";

	public static final String	DECISION_REJECT							= "REJECT";

	public static final String	DECISION_REVIEW							= "REVIEW";
	
	public static final String	FRAUD_RESULT_CODE_REJECTED_ERROR_CODE	= "1";
	
	public static final String	FRAUD_RESULT_CODE_REJECTED_ERROR		= "FRAUD";
	
	public static final String	FRAUD_FLAG_YES						= "Y";
	
	public static final String	FRAUD_FLAG_NO						= "N";
	
	public static final String	FRAUD_FLAG_OFFLINE					= "O";
	
	public static final String  DECISION_OFFLINE 						= "offline";
	
	public static final String 	SUCCESS 								= "SUCCESS";
	
	public static final String 	FAILURE 								= "FAILURE";
	
	public static final String AUTHORIZE 								= "AUTHORIZE";

	public static final String DEBIT 									= "DEBIT";

	public static final String CREDIT 									= "CREDIT";
	
	public static final String ELECTORNIC_GC 							= "EGC";
	
	public static final String GIFTCARD 								= "GC";	
	
	public static final String CVCODE 									= "CV";	
	
	public static final String CCFRAUDFAILURE							= "failure";
	
	public static final String APPROVEDFRAUDFAILURE						= "approvedfraudFailure";
	
	public static final String REVIEWFRAUDFAILURE						= "reviewfraudFailure";
	
	public static final String NAME_ON_CARD								= "nameOnCard";
	
	public static final String TOKEN_VALUE								= "tokenValue";
	
	public static final String CREDIT_CARD								= "creditCard";
	
	public static final String PRE_REGISTER 							= "PreRegister";
	
	public static final String PREREG_APPROVALCODE_ONE					= "801";
	
	public static final String PREREG_APPROVALCODE_TWO					= "802";


	private String				fraudFlag;

	/** Identifies the final action code of the rule evaluation process */
	private String				fraudResultCode;

	/** Identifies the final action of the rule evaluation process. FraudResult is return in a response document */
	private String				fraudResultCodeDescription;

	/** Unique identifier for a fraud rule */
	private String				fraudRule;

	/** The text that is to be associated with a fraud rule and displayed to the merchant or store
	 * when the associated fraud rule is Initiated during payment processing */
	private String				fraudRuleDescription;

	/** The sum of the weights for all triggered rules for the current transaction */
	private String				fraudScore;

	/** Total amount on order */
	private double				amountAuthorized;

	/** Credit Card Return Message */
	private String				decision;

	/** Auth Code */
	private String				authorizationCode;

	/** Credit Verification Value Response Code */
	private String				cvCode;

	/** Credit Card Error Code */
	private String				reasonCode;

	/** Address Verification Service Display */
	private String				avsCode;

	/** Holds the order id */
	private String				transactionId;

	private java.util.Date		authorizationExpiration;

	// Cardinal Commerce CMPI Lookup Response
	private String				lookupErrorNo;
	private String				lookupErrorDesc;
	private String				lookupTransactionId;
	private String				lookupPayload;
	private String				lookupEnrolled;
	private String				lookupACSUrl;
	private String				lookupEciFlag;
	private String				lookupPayerSecurityLevel;
	private String				lookupStatus;

	// Cardinal Commerce CMPI Auth Response
	private String				authErrorNo;
	private String				authErrorDesc;
	private String				authPAResStatus;
	private String				authSignatureVerification;
	private String				authCavv;
	private String				authEciFlag;
	private String				authXid;
	private String				authPayerSecurityLevel;
	private String				authStatus;

	// Response type like FP04, FP08 or FP09 + next Auth type
	private String				responseType;
	private String				nextAuthRequest;
	private boolean				gisAvailable;
	private String				tokenValue;
	private String				threatMetrixId;
	private String 				response;

	private boolean communicationError;

}
