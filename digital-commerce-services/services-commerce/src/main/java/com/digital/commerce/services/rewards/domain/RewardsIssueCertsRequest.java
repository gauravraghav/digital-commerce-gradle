package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RewardsIssueCertsRequest {

  private String certDenomination;
  String profileId;
}
