package com.digital.commerce.services.order.payment.creditcard;

import static com.digital.commerce.constants.DigitalProfileConstants.MIL_ADDRESS_TYPE;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.transaction.SystemException;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.DigitalProfileConstants;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.common.validator.DigitalStartEndDateValidator;
import com.digital.commerce.services.order.DigitalCreditCard;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.validator.DigitalBasicAddressValidator;

import atg.commerce.order.purchase.CreateCreditCardFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalCreateCreditCardFormHandler extends CreateCreditCardFormHandler {

	private DigitalCreditCardPaymentService creditCardPaymentService;
	private DigitalProfileTools profileTools;
	private boolean billingSameAsShippingAddress;
	private boolean billingSameAsMailingAddress;
	private MessageLocator messageLocator;
	private DigitalBasicAddressValidator dswBasicAddressValidator;
	private DigitalBaseConstants dswBaseConstants;
	private DigitalStartEndDateValidator startEndDateValidator;

	private static final String SHIPPING_ADDRESS_NOT_FOUND = "shippingAddressNotFound";
	private static final String PAY_PAGE_REGID_NOT_FOUND = "payPageRegIdNotFound";
	private static final String CREDIT_CARD_VALIDITY_NOT_FOUND = "creditCardValidityNotFound";
	private static final String CREDIT_CARD_TYPE_NOT_FOUND = "creditCardTypeNotFound";
	private static final String CREDIT_CARD_NAME_ON_CARD_NOT_FOUND = "creditCardNameOnCardNotFound";
	private static final String DUPLICATE_CREDIT_CARDS = "duplicateCreditCard";
	private static final String CREDIT_CARD_MAXLIMIT_EXCEEDED = "creditCardMaxLimitExceeded";
	private static final String SAVED_CREDIT_CARD_FAILED = "saveCreditCardFailed";
	private static final String MISSING_VANTIV_FIRST_SIX = "missingVantivFirstSix";
	private static final String MAILING_ADDRESS_NOT_FOUND = "mailingAddressNotFound";


	/**
	 * @return DigitalCreditCard
	 */
	public DigitalCreditCard getCreditCard() {
		return (DigitalCreditCard) super.getCreditCard();
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleAddNewCreditCard(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws ServletException, IOException {

		final String METHOD_NAME = "DigitalCreateCreditCardFormHandler.handleUpdateItemToOrder";
		boolean ret = false;
		boolean canSaveCard = true;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		TransactionDemarcation td = new TransactionDemarcation();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			try {
				td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
				if (DigitalStringUtil.isBlank(getCreditCard().getPaypageRegistrationId())) {
					String msg = getMessageLocator().getMessageString(PAY_PAGE_REGID_NOT_FOUND);
					addFormException(new DropletException(msg, PAY_PAGE_REGID_NOT_FOUND));
					canSaveCard = false;
					if (isLoggingDebug()) {
						logDebug("Blank Paypageid supplied:: canSaveCard:: " + canSaveCard);
					}
				}
				if (DigitalStringUtil.isBlank(getCreditCard().getExpirationMonth())
						|| DigitalStringUtil.isBlank(getCreditCard().getExpirationYear())) {
					String msg = getMessageLocator().getMessageString(CREDIT_CARD_VALIDITY_NOT_FOUND);
					addFormException(new DropletException(msg, CREDIT_CARD_VALIDITY_NOT_FOUND));
					canSaveCard = false;
					if (isLoggingDebug()) {
						logDebug("Blank ExpirationMonth or ExpirationYear supplied:: canSaveCard:: " + canSaveCard);
					}

				} else {
					// Accepts year as either four digit or two digits and
					// converts it to four digit before saving against profile,
					// Year will be defaulted to 20th century if two digits are
					// passed in the input, this is picked up from properties
					// file
					// and can be changed once we get past the 20th century
					getCreditCard()
							.setExpirationYear((String) (getCreditCard().getExpirationYear().length() == 2
									? getDswBaseConstants().getDefaultExpirationCentury()
											+ getCreditCard().getExpirationYear()
									: getCreditCard().getExpirationYear()));
				}
				if (getStartEndDateValidator().isCreditCardExpired(getCreditCard())) {
					// Check to find if credit card is expired.
					String msg = getMessageLocator().getMessageString(DigitalProfileConstants.MSG_CARD_EXPIRED);
					addFormException(new DropletException(msg, DigitalProfileConstants.MSG_CARD_EXPIRED));
					canSaveCard = false;
					if (isLoggingDebug()) {
						logDebug("Invalid credit card Expiration date supplied:: canSaveCard:: " + canSaveCard);
					}
				}

				if (DigitalStringUtil.isBlank(getCreditCard().getCreditCardType())) {
					String msg = getMessageLocator().getMessageString(CREDIT_CARD_TYPE_NOT_FOUND);
					addFormException(new DropletException(msg, CREDIT_CARD_TYPE_NOT_FOUND));
					canSaveCard = false;
					if (isLoggingDebug()) {
						logDebug("Invalid credit card type supplied:: canSaveCard:: " + canSaveCard);
					}
				}

				if (DigitalStringUtil.isBlank(getCreditCard().getNameOnCard())) {
					String msg = getMessageLocator().getMessageString(CREDIT_CARD_NAME_ON_CARD_NOT_FOUND);
					addFormException(new DropletException(msg, CREDIT_CARD_NAME_ON_CARD_NOT_FOUND));
					canSaveCard = false;
					if (isLoggingDebug()) {
						logDebug("Blank Credit Card Name supplied:: canSaveCard:: " + canSaveCard);
					}
				}

				if (DigitalStringUtil.isBlank(getCreditCard().getVantivFirstSix())) {
					addFormException(new DropletException(
							getMessageLocator().getMessageString(MISSING_VANTIV_FIRST_SIX), MISSING_VANTIV_FIRST_SIX));
					if (isLoggingDebug()) {
						logDebug("Blank VantivFirstSix supplied:: canSaveCard:: " + canSaveCard);
					}
				}

				if (isBillingSameAsShippingAddress() && isBillingSameAsMailingAddress()) {
					setBillingSameAsShippingAddress(false);
				}

				if (isBillingSameAsShippingAddress()) {
					if (profileTools.getDefaultShippingAddress(getProfile()) != null) {
						DigitalContactInfo shippingAddress = createBillingAddress(
								profileTools.getDefaultShippingAddress(getProfile()));
						getCreditCard().setBillingAddress(shippingAddress);
						if (isLoggingDebug()) {
							logDebug("Successfully set ShippingAddress as Billing Address ");
						}
					} else {
						String msg = getMessageLocator().getMessageString(SHIPPING_ADDRESS_NOT_FOUND);
						addFormException(new DropletException(msg, SHIPPING_ADDRESS_NOT_FOUND));
						canSaveCard = false;
						if (isLoggingDebug()) {
							logDebug("Error while setting ShippingAddress as Billing Address :: canSaveCard:: "
									+ canSaveCard);
						}
					}
				} else if (isBillingSameAsMailingAddress()) {
					if (profileTools.getMailingAddress(getProfile()) != null) {
						DigitalContactInfo mailingAddress = createBillingAddress(
								profileTools.getMailingAddress(getProfile()));
						getCreditCard().setBillingAddress(mailingAddress);
						if (isLoggingDebug()) {
							logDebug("Successfully set MailingAddress as Billing Address ");
						}
					} else {
						String msg = getMessageLocator().getMessageString(MAILING_ADDRESS_NOT_FOUND);
						addFormException(new DropletException(msg, MAILING_ADDRESS_NOT_FOUND));
						canSaveCard = false;
						if (isLoggingDebug()) {
							logDebug("Error while setting ShippingAddress as Mailing Address :: canSaveCard:: "
									+ canSaveCard);
						}
					}
				} else {
					// Process the request and populate the request in
					// BillingContact
					DigitalContactInfo dswContactInfo = populateContactInfo(request);
					List<String> lstValidationErrors = dswBasicAddressValidator.validateAddress(dswContactInfo);
					if (lstValidationErrors.size() > 0) {
						canSaveCard = false;
						for (String errorCode : lstValidationErrors) {
							addFormException(
									new DropletException(this.getMessageLocator().getMessageString(errorCode), errorCode));
						}
					}
				}

				if (canSaveCard) {
					DigitalCreditCardStatus ccStatus = (DigitalCreditCardStatus) getCreditCardPaymentService()
							.registerTokenRequest(getCreditCard());
					if (ccStatus.getTransactionSuccess()) {
						getCreditCard().setTokenValue(ccStatus.getTokenValue());
						getCreditCard().setcreditCardLastFour(extractLastFour(ccStatus.getTokenValue()));
						getCreditCard().setCreditCardType(getProfileTools().getCreditCardType(getCreditCard()));
						/*
						 * setting token value to out of the box creditcard
						 * number field this way the credit card nick name
						 * generation during card saving will be same in both
						 * flows (ie checkout flow and profile flow)
						 */
						getCreditCard().setCreditCardNumber(ccStatus.getTokenValue());
						if (profileTools.isCreditCardPresentInProfile((Profile) getProfile(), getCreditCard())) {
							String msg = getMessageLocator().getMessageString(DUPLICATE_CREDIT_CARDS);
							addFormException(new DropletException(msg, DUPLICATE_CREDIT_CARDS));
						} else if (profileTools.isCreditCardMaxLimitExceeded((Profile) getProfile())) {
							// Check if user can save any more cards to his
							// profile
							String msg = getMessageLocator().getMessageString(CREDIT_CARD_MAXLIMIT_EXCEEDED,
									String.valueOf(MultiSiteUtil.getCreditCardMaxLimit()));
							addFormException(new DropletException(msg, CREDIT_CARD_MAXLIMIT_EXCEEDED));
						} else {
							// copy to profile and set as default card if
							// required only if card is alredy not present.
							profileTools.copyCreditCardToProfile((Profile) getProfile(), getCreditCard());
						}
					} else {
						String msg = getMessageLocator().getMessageString(SAVED_CREDIT_CARD_FAILED);
						addFormException(new DropletException(msg, SAVED_CREDIT_CARD_FAILED));
					}
				}

				// Mark the transaction rollback if there are any form
				// validation errors
				if (this.getFormError()) {
					try {
						this.setTransactionToRollbackOnly();
					} catch (SystemException e) {
						logError("Transaction rollback exception :: ", e);
					}
				}
			} catch (TransactionDemarcationException exc) {
				if (isLoggingError()) {
					logError("Transaction Demarcation Error: ", exc);
				}
			} finally {
				try {
					if (td != null) {
						td.end();
					}
				} catch (TransactionDemarcationException e) {
					if (isLoggingError()) {
						logError("transaction unable to commit ", e);
					}
				}
				if (rrm != null){
					rrm.removeRequestEntry(METHOD_NAME);
				}
			}
		}
		return ret;
	}

	/**
	 * 
	 * @param creditCardNumber
	 * @return
	 */
	protected String extractLastFour(String creditCardNumber) {
		return (creditCardNumber != null) ? creditCardNumber.substring((creditCardNumber.length() - 4)) : null;
	}

	/**
	 * 
	 * @param shippingAddress
	 * @return
	 */
	private DigitalContactInfo createBillingAddress(RepositoryItem shippingAddress) {
		DigitalContactInfo dswContactInfo = new DigitalContactInfo();
		dswContactInfo.setFirstName((String) shippingAddress.getPropertyValue("firstname"));
		dswContactInfo.setLastName((String) shippingAddress.getPropertyValue("lastname"));
		dswContactInfo.setAddress1((String) shippingAddress.getPropertyValue("address1"));
		dswContactInfo.setAddress2((String) shippingAddress.getPropertyValue("address2"));
		dswContactInfo.setCity((String) shippingAddress.getPropertyValue("city"));
		dswContactInfo.setCountry((String) shippingAddress.getPropertyValue("country"));
		dswContactInfo.setPostalCode((String) shippingAddress.getPropertyValue("postalCode"));
		dswContactInfo.setState((String) shippingAddress.getPropertyValue("state"));
		return dswContactInfo;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	private DigitalContactInfo populateContactInfo(DynamoHttpServletRequest request) {
		DigitalContactInfo dswContactInfo = new DigitalContactInfo();

		try {
			/*
			 * Getting billing address containing email and phone number set
			 * earlier in the flow
			 */
			dswContactInfo = ((DigitalPaymentGroupManager) this.getOrderManager().getPaymentGroupManager())
					.getBillingAddressFromOrder(getOrder());
		} catch (DigitalAppException e) {
			if (isLoggingError()) {
				logError("Exception while attempting to get billing address from order", e);
			}
		}

		dswContactInfo.setFirstName(request.getParameter("firstName"));
		dswContactInfo.setLastName(request.getParameter("lastName"));
		dswContactInfo.setAddress1(request.getParameter("address1"));
		dswContactInfo.setAddress2(request.getParameter("address2"));
		dswContactInfo.setCountry(request.getParameter("country"));

		// Update email from request only if its not already present as part of
		// the billing address
		if (DigitalStringUtil.isEmpty(dswContactInfo.getEmail())) {
			dswContactInfo.setEmail(request.getParameter("emailAddress"));
		}
		dswContactInfo.setEmailConfirm(request.getParameter("emailaddressConfirm"));
		dswContactInfo.setPobox(request.getParameter("pobox"));
		dswContactInfo.setPostalCode(request.getParameter("postalCode"));
		dswContactInfo.setAddressType(request.getParameter("addressType"));
		dswContactInfo.setCity(request.getParameter("city"));
		// Update phone number only if its not present as part of the billing
		// address got from the Order
		if (DigitalStringUtil.isEmpty(dswContactInfo.getPhoneNumber())) {
			dswContactInfo.setPhoneNumber(request.getParameter("phoneNumber"));
		}
		if (MIL_ADDRESS_TYPE.equalsIgnoreCase(dswContactInfo.getAddressType())) {
			dswContactInfo.setRegion(request.getParameter("region"));
			dswContactInfo.setState("");
		} else {
			dswContactInfo.setRegion("");
			dswContactInfo.setRank("");
			dswContactInfo.setState(request.getParameter("state"));
		}
		return dswContactInfo;
	}

	/**
	 * Override the method to stop commit & post commit steps if there is any
	 * form exceptions during validation. Rollback the transaction.
	 * 
	 * @param pSuccessURL
	 * @param pFailureURL
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 */
	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if ((getSessionExpirationURL() != null) && (pRequest.getSession().isNew()) && (isFormSubmission(pRequest))) {
			RepositoryItem profile;

			if (((profile = getProfile()) == null) && ((profile = defaultUserProfile(true)) == null)
					&& (isLoggingError())) {
				logError("missingExpiredSessionProfile");
			}

			if ((profile != null) && (profile.isTransient())) {
				if (isLoggingDebug())
					logDebug("Session expired: redirecting to " + getSessionExpirationURL());
				redirectOrForward(pRequest, pResponse, getSessionExpirationURL());
				return false;
			}
		}

		if (isTransactionMarkedAsRollBack()) {
			if (isLoggingDebug()) {
				logDebug("Transaction Marked as Rollback - redirecting to: " + pFailureURL);
			}
			return false;
		}

		return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
	}

}