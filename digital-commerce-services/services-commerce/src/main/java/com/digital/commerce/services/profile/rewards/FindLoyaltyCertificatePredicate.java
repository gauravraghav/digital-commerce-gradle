package com.digital.commerce.services.profile.rewards;

import com.digital.commerce.common.util.DigitalPredicate;

public class FindLoyaltyCertificatePredicate implements DigitalPredicate<LoyaltyCertificate> {

	private final String	certificateId;

	public FindLoyaltyCertificatePredicate( String certificateId ) {
		this.certificateId = certificateId;
	}

	@Override
	public boolean apply( LoyaltyCertificate input ) {
		return certificateId.equalsIgnoreCase( input.getId() );
	}

}
