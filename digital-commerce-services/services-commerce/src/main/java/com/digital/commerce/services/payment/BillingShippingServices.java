package com.digital.commerce.services.payment;

import static com.digital.commerce.common.services.DigitalBaseConstants.SHIPPING_METHODS;
import static com.digital.commerce.common.services.DigitalBaseConstants.STATIC_SHIPPING_RATES;
import static com.digital.commerce.constants.DigitalProfileConstants.DRIVER_DATE_FORMAT;
import static com.digital.commerce.constants.DigitalProfileConstants.GIFT_CARD_SHIPPING_GROUP_STATE;
import static com.digital.commerce.constants.DigitalProfileConstants.PO_BOX_PATTERN;
import static com.digital.commerce.constants.DigitalProfileConstants.US_POSTAL_CODE_LENGTH;

import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.nucleus.logging.ApplicationLoggingImpl;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.domain.Address;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.inventory.YantraService;
import com.digital.commerce.integration.inventory.YantraService.ServiceMethod;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingGroup;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingItem;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingOption;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingRestrictionsServiceRequest;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingRestrictionsServiceResponse;
import com.digital.commerce.services.common.AddressType;
import com.digital.commerce.services.common.CountryCode;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.shipping.ShippingMethod;
import com.digital.commerce.services.pricing.OrderShippingQuote;
import com.digital.commerce.services.pricing.ShippingDestinationQuote;
import com.digital.commerce.services.pricing.ShippingHandlingInfo;
import com.digital.commerce.services.pricing.ShippingRate;
import com.digital.commerce.services.pricing.ShippingRateTable;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 *   Shipping&Handling service etc */

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class BillingShippingServices extends ApplicationLoggingImpl {

	private YantraService	yantraService;
	

	public BillingShippingServices() {
		super(BillingShippingServices.class.getName());
	}
	
	public ShippingRateTable getShippingRateTable( Order order, String orderType, ShippingHandlingInfo shInfo ) {

		return shInfo.getStaticShippingRateTable( order );
	}

	/* ------------------------------------------------------------------------
	 * -- New methods according to new Specs -- -- Older method kept but only
	 * for STUB data --
	 * ------------------------------------------------------------------------ */

	/** Entry point into this service. Makes a Call to Shipping and Handling
	 * engine through GIS to get shipping Quotes. If the order is gift card only
	 * or the GIS is turned off, the static table is returned.,
	 * 
	 * @param webServicesConfig
	 *            WebServicesConfig refrence
	 * @param shippingGroup
	 * @param order
	 *            Order object
	 * @param orderType
	 *            Order type
	 * 
	 * @return ShQueryResponseType with shipping and handling rates
	 * @throws DigitalAppException 
	 * @throws InvalidShippingDestinationException 
	 * 
	 * @throws ServicesException */
	public ShippingRateTable getShippingRateTable( Order order, String orderType ) throws DigitalAppException, DigitalIntegrationException {
		if( ( !( order instanceof DigitalOrderImpl ) || !( ( (DigitalOrderImpl)order ).getIsGiftCardOrderOnly() ) ) && yantraService != null 
				&& yantraService.isServiceEnabled() && yantraService.isServiceMethodEnabled(ServiceMethod.SHIPPING_RESTRICTIONS.getServiceMethodName()) ) {
			YantraShippingRestrictionsServiceResponse responseType = getShippingRates( order, orderType );
			if(responseType != null){
				return toShippingRateTable( order, responseType );
			}else{
				return staticShippingRateTable( order, true ); 
			}
		} else {
			return staticShippingRateTable( order, true ); 
		}
	}

	private YantraShippingRestrictionsServiceResponse getShippingRates( Order order, String orderType ) throws DigitalAppException, DigitalIntegrationException {
		YantraShippingRestrictionsServiceResponse retVal = null;
		final YantraShippingRestrictionsServiceRequest inputQueryRequest = toYantraRateQueryRequest( order, orderType );
		validateShippingRequest( inputQueryRequest.getYantraShippingGroup() );
			try {
				retVal = yantraService.shippingRestrictions( inputQueryRequest );
			} catch (DigitalIntegrationException e) {
				// TODO Auto-generated catch block
				logError(e);
			}
			
		return retVal;
	}
	
	public YantraShippingRestrictionsServiceResponse getShippingMethods( Order order, String orderType ) throws DigitalAppException, DigitalIntegrationException {
		YantraShippingRestrictionsServiceResponse retVal = null;
		final YantraShippingRestrictionsServiceRequest inputQueryRequest = toYantraRateQueryRequest( order, orderType );
		validateShippingRequest( inputQueryRequest.getYantraShippingGroup() );
			try {
				if( ( !( order instanceof DigitalOrderImpl ) || !( ( (DigitalOrderImpl)order ).getIsGiftCardOrderOnly() ) ) && yantraService != null && yantraService.isServiceEnabled() && yantraService.isServiceMethodEnabled(ServiceMethod.SHIPPING_RESTRICTIONS.getServiceMethodName()) ) {
					retVal = yantraService.shippingRestrictions( inputQueryRequest );
				}
			} catch (DigitalIntegrationException e) {
				// TODO Auto-generated catch block
				logError(e);
				throw e;
			}
			if( null == retVal) {
				throw new DigitalAppException( "PAYLOAD Failure : Service did not respond with a valid message" );
			}
			return retVal;
	}
	
	public Boolean isYantraServiceOffline(Order order){
		if( ( !( order instanceof DigitalOrderImpl ) || !( ( (DigitalOrderImpl)order ).getIsGiftCardOrderOnly() ) ) && yantraService != null && yantraService.isServiceEnabled() && yantraService.isServiceMethodEnabled(ServiceMethod.SHIPPING_RESTRICTIONS.getServiceMethodName()) ) {
		return false;
		} else {
		return true;
		}
	}

	private void validateShippingRequest( List<YantraShippingGroup> shippingGroups ) throws DigitalAppException{
		if( shippingGroups == null || shippingGroups.size() == 0 ) { throw new DigitalAppException( "No shipping groups" ); }
		for( YantraShippingGroup shippingGroup: shippingGroups ) {
			if( shippingGroup == null || shippingGroup.getPersonInfoShipTo() == null || DigitalStringUtil.isBlank( shippingGroup.getPersonInfoShipTo().getCity() ) || DigitalStringUtil.isBlank( shippingGroup.getPersonInfoShipTo().getPostalCode() )
					|| DigitalStringUtil.isBlank( shippingGroup.getPersonInfoShipTo().getAddress1() ) || DigitalStringUtil.isBlank( shippingGroup.getPersonInfoShipTo().getState() ) ) { throw new DigitalAppException(
					"Invalid shipping address" ); }
		}
	}

	/**
	 * Converts the Yantra response into a table of {@link ShippingRate}s. The Yantra LOS is the same
	 * as the shipping method in the shipping group. We can use it to filter out invalid Yantra
	 * responses.
	 *
	 * @param order
	 * @param response
	 * @return
	 */
	private ShippingRateTable toShippingRateTable(Order order,
			YantraShippingRestrictionsServiceResponse response) {
		final ShippingRateTable retVal = new ShippingRateTable();
		boolean itemsOrAddressRestricted = false;
		List<YantraShippingOption> shippingOptions = response.getYantraShippingOptions();
		for (YantraShippingOption shippingOption : shippingOptions) {
			if (shippingOption.getYantraShippingGroup() != null
					&& shippingOption.getYantraShippingGroup().size() > 0) {
				for (YantraShippingGroup shippingGroup : shippingOption.getYantraShippingGroup()) {
					if (DigitalStringUtil.isNotBlank(shippingGroup.getShipToGroupIdentifier())) {
						final String method = shippingOption.getLOS();
						if (isAcceptableShippingMethod(method, order)) {
							final ShippingRate shippingRate = new ShippingRate();
							shippingRate.setBaseRateServiceName(method);
							shippingRate.setBaseRateServiceLevel(method);
							shippingRate.setBaseRatesServiceLevelDescription(method);
							shippingRate.setBaseRate(
									shippingGroup.getYantraShipToSHCosts().getYantraShippingSHGrossCosts()
											.getTotalGrossCosts());
							retVal.addShippingRate(shippingGroup.getShipToGroupIdentifier(), shippingRate);
						}
					}
				}
			}
			else if (null != shippingOption.getRestrictions()) {
				itemsOrAddressRestricted = true;
			}
		}
		if(itemsOrAddressRestricted){
			return staticShippingRateTable( order, true );
		}
		return retVal;
	}

	private YantraShippingRestrictionsServiceRequest toYantraRateQueryRequest( Order order, String orderTypeValue ) {
		
		return toOrderType( order, orderTypeValue, null, null, null );
	}

	private YantraShippingRestrictionsServiceRequest toOrderType( Order order, String orderTypeValue, Map returnShipOrder, String defaultShipMethod, String shDriverDate ) {
		final YantraShippingRestrictionsServiceRequest retVal = new YantraShippingRestrictionsServiceRequest();
		retVal.setOrderIdentifier( order.getId() );
		final Calendar driverCalendar = GregorianCalendar.getInstance();
		if( DigitalStringUtil.isBlank( shDriverDate ) ) {
			driverCalendar.setTimeInMillis( order.getCreationTime() );
			driverCalendar.set( Calendar.MILLISECOND, 0 );
		} else {
			try {
				driverCalendar.setTime( new SimpleDateFormat( DRIVER_DATE_FORMAT ).parse( DigitalStringUtil.replace( shDriverDate, ":", "" ) ) );
			} catch( ParseException e ) {
				logError( String.format( "Unable to parse %1$s from %2$s for %3$s", DRIVER_DATE_FORMAT, shDriverDate, order.getId() ) );
			}
		}
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime( driverCalendar.getTime() );
		try {
			XMLGregorianCalendar xmlgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            xmlgc.setFractionalSecond(new BigDecimal(0));
			retVal.setDriverDate( xmlgc );
		} catch( DatatypeConfigurationException ex ) {
			logError("Unable to set Driver Date, couldnt create XMLGregorianCalendar" );
		}
		retVal.setType( orderTypeValue );
		final List<YantraShippingGroup> shippingGroups = new ArrayList<>();
		if( returnShipOrder != null ) {
			Set<String> shippingGroupIdKeys = returnShipOrder.keySet();
			for( final Iterator<String> shippingGroupIdKeyIterator = shippingGroupIdKeys.iterator(); shippingGroupIdKeyIterator.hasNext(); ) {
				final String shippinGroupId = shippingGroupIdKeyIterator.next();
				List returnItemsList = (List)returnShipOrder.get( shippinGroupId );
				shippingGroups.add( toShipToYantraGroupType( getSGfromOrderBySgId( order, shippinGroupId ), order, returnItemsList, defaultShipMethod ) );
			}
		} else {
			if( order.getShippingGroups() != null ) {
				for( Object shippingGroupObject : order.getShippingGroups() ) {
					if( shippingGroupObject instanceof HardgoodShippingGroup ) {
						final HardgoodShippingGroup shippingGroup = (HardgoodShippingGroup)shippingGroupObject;
						// if the sg is not in gift card shipping group state
						if( shippingGroup.getCommerceItemRelationshipCount() > 0 && shippingGroup.getState() != GIFT_CARD_SHIPPING_GROUP_STATE ) {
							shippingGroups.add( toShipToYantraGroupType( shippingGroup, order, null, null ) );
						}
					}
				}
			}
		}
		retVal.setYantraShippingGroup( shippingGroups );
		return retVal;
	}

	private YantraShippingGroup toShipToYantraGroupType( HardgoodShippingGroup shippingGroup, Order order, List returnItemsList, String defaultShipMethod ) {
		final YantraShippingGroup retVal = new YantraShippingGroup();
		
		retVal.setShipToGroupIdentifier( shippingGroup.getId() );
		if( DigitalStringUtil.isNotBlank( defaultShipMethod ) ) {
			//retVal.setLOS( defaultShipMethod );
		} else {
			retVal.setShipFromNode( "3PLDC" );
		}
		final Address personInfoShipTo = new Address();
		personInfoShipTo.setAddress1( shippingGroup.getShippingAddress().getAddress1() );
		personInfoShipTo.setAddress2( shippingGroup.getShippingAddress().getAddress2() );
		final AddressType addressType = AddressType.valueFor( (String)shippingGroup.getPropertyValue( "addressType" ) );
		if( AddressType.USA.equals( addressType ) ) {
			if( isPoBox( shippingGroup.getShippingAddress() ) ) {
				personInfoShipTo.setAddressType("POB");
			} else {
				personInfoShipTo.setAddressType( AddressType.USA.getValue() );
			}
		} else {
			personInfoShipTo.setAddressType( addressType.getValue() );
		}
		personInfoShipTo.setCity( shippingGroup.getShippingAddress().getCity() );
		if( DigitalStringUtil.isBlank( shippingGroup.getShippingAddress().getState() ) && AddressType.MILITARY.equals( addressType ) ) {
			personInfoShipTo.setState( (String)shippingGroup.getPropertyValue( "region" ) );
		} else {
			personInfoShipTo.setState( shippingGroup.getShippingAddress().getState() );
		}
		if( AddressType.MILITARY.equals( addressType ) || AddressType.USA.getValue().equals( shippingGroup.getShippingAddress().getCountry() ) ) {
			personInfoShipTo.setCountryCode( CountryCode.US.getValue() );
		} else {
			personInfoShipTo.setCountryCode( shippingGroup.getShippingAddress().getCountry() );
		}
		// Only send 5 digit zip code in the request
		personInfoShipTo.setPostalCode(normalizePostalCode( shippingGroup.getShippingAddress().getPostalCode() ) );
		retVal.setPersonInfoShipTo( personInfoShipTo );
			retVal.setYantraShippingItems( toItems( shippingGroup ) );
		return retVal;
	}

	/** This method finds a shipping group in the order given by shipping group
	 * id.
	 * 
	 * @param order
	 * @param sgId
	 * @return */
	private HardgoodShippingGroup getSGfromOrderBySgId( Order order, String sgId ) {
		HardgoodShippingGroup retVal = null;
		List<ShippingGroup> shippingGroups = order.getShippingGroups();
		for( Iterator<ShippingGroup> iterator = shippingGroups.iterator(); iterator.hasNext(); ) {
			ShippingGroup sg = (ShippingGroup)iterator.next();
			if( sg.getId().equals( sgId ) ) {
				retVal = (HardgoodShippingGroup)sg;
			}
		}
		if( retVal == null ) {
			logError(String.format( "Unable to find shippingGroupId %1$s in order %2$s", sgId, order.getId() ) );
		}
		return retVal;
	}

	/** Returns true if the address is a po box.
	 * 
	 * @param shippingAddress
	 * @return */
	private boolean isPoBox( atg.core.util.Address shippingAddress ) {
		return ( shippingAddress.getAddress1() != null && PO_BOX_PATTERN.matcher( shippingAddress.getAddress1() ).find() ) || ( shippingAddress.getAddress2() != null && PO_BOX_PATTERN.matcher( shippingAddress.getAddress2() ).find() )
				|| ( shippingAddress.getAddress3() != null && PO_BOX_PATTERN.matcher( shippingAddress.getAddress3() ).find() );
	}

	/** Returns the {@link #US_POSTAL_CODE_LENGTH} portion of the postal code if
	 * it looks like a 9 digit US Postal code (e.g., 43086-2100). This does not
	 * change other postal codes like K1S5B6 a postal code for Ontario or postal
	 * codes that are less than the {@link #US_POSTAL_CODE_LENGTH} length.
	 * 
	 * @param postalCode
	 * @return */
	private String normalizePostalCode( String postalCode ) {

		// Uncomment when start international shipping
		if( postalCode != null ) {
			if( postalCode.length() > US_POSTAL_CODE_LENGTH && postalCode.charAt( US_POSTAL_CODE_LENGTH ) == '-' ) {
				postalCode = postalCode.substring( 0, US_POSTAL_CODE_LENGTH );
			}
			postalCode = Strings.padStart( postalCode, US_POSTAL_CODE_LENGTH, '0' );
		}
		return postalCode;
	}

	private List<YantraShippingItem> toItems( HardgoodShippingGroup shippingGroup ) {
		final List<YantraShippingItem> retVal = new ArrayList<>();
		if( shippingGroup.getCommerceItemRelationships() != null ) {
			for( Object commerceItemRelationshipObject : shippingGroup.getCommerceItemRelationships() ) {
				final CommerceItemRelationship commerceItemRelationship = (CommerceItemRelationship)commerceItemRelationshipObject;
				final YantraShippingItem shippingItem = new YantraShippingItem();
				shippingItem.setItemId( commerceItemRelationship.getCommerceItem().getCatalogRefId() );
				shippingItem.setQuantity( commerceItemRelationship.getCommerceItem().getQuantity()  );
				shippingItem.setWeight( 0.0d );
				if( commerceItemRelationship.getCommerceItem().getPriceInfo() != null ) {
					shippingItem.setUnitPrice( commerceItemRelationship.getCommerceItem().getPriceInfo().getListPrice() );
				}
				retVal.add(shippingItem);
			}
		}
		return retVal;
	}

	/** Returns a static {@link ShippingRateTable} not based on web service calls
	 * to PS03. if validateAcceptableMethod is true it will validate that the
	 * shipping method is acceptable for the shipping address. This method is
	 * only callable during a Servlet request.
	 * 
	 * @param shippingGroups
	 * @param validateAcceptableMethodForAddress
	 * @return */
	private ShippingRateTable staticShippingRateTable( Order order, boolean validateAcceptableMethodForAddress ) {
		ShippingRateTable srTable = new ShippingRateTable();
		String siteId = order.getSiteId();
		Map map = MultiSiteUtil.getShippingRates(siteId);
		if(isLoggingDebug()){
			logDebug("map --> " + map);
		}
		if(((ArrayList)(map.get(SHIPPING_METHODS))) != null && ((ArrayList)(map.get(SHIPPING_METHODS))).size() > 0 && order != null && order.getShippingGroups() != null ) {
			for( HardgoodShippingGroup hsg : Iterables.filter( order.getShippingGroups(), HardgoodShippingGroup.class ) ) {
				String sgId = hsg.getId();
				if(isLoggingDebug()){
					logDebug("sgId-->"+sgId);
				}
				for( String method : Iterables.filter( ((ArrayList)(map.get(SHIPPING_METHODS))), String.class ) ) {
					atg.core.util.Address address =  hsg.getShippingAddress();
					if(isLoggingDebug()){
						logDebug("address-->"+address + " :: method-->"+method);
					}
					if( ( !validateAcceptableMethodForAddress || isAcceptableShippingMethod( method, address ) ) && isAcceptableShippingMethod( method, order ) ) {
						Properties rates = (Properties)(map.get(STATIC_SHIPPING_RATES));
						if(isLoggingDebug()){
							logDebug("rates-->"+rates);
						}
						ShippingRate sr = new ShippingRate();
						sr.setBaseRateServiceLevel( method );
						sr.setBaseRateServiceName( method );
						sr.setBaseRatesServiceLevelDescription( method );
						Double rate = Double.valueOf( rates.getProperty( method ) );
						if(isLoggingDebug()){
							logDebug("rate-->"+rate);
						}
						sr.setBaseRate( rate.doubleValue() );
						srTable.addShippingRate( sgId, sr );
					}
				}
			}
		}

		return srTable;
	}

	/** Returns true if the shipping method is acceptable. These are static rules
	 * that are normally only executed in a development environment or if the
	 * PS03 service is inactive.
	 * 
	 * @param method
	 * @param address
	 * @return */
	private boolean isAcceptableShippingMethod( String method, atg.core.util.Address address ) {
		if(isLoggingDebug()){
			logDebug("in isAcceptableShippingMethod-->");
		}
		boolean addRate = false;
		if( address == null ) {
			addRate = true;
		} else if( CountryCode.USA.getValue().equals( address.getCountry() ) ) {
			if(isLoggingDebug()){
				logDebug("in isAcceptableShippingMethod--> country block");
			}
			boolean pobox = isPoBox( address );
			boolean hawaii = "HI".equals( address.getState() );
			boolean puertoRico = "PR".equals( address.getState() );
			if( ShippingMethod.Ground.equals( method ) ) {
				if( pobox || hawaii || puertoRico ) {
					addRate = true;
				} else if( !pobox && !hawaii && !puertoRico ) {
					addRate = true;
				}
			} else if( ShippingMethod.SecondDay.equals( method ) ) {
				if( !pobox && !puertoRico ) {
					addRate = true;
				}
			} else if( !pobox && !hawaii && !puertoRico ) {
				addRate = true;
			}
		}

		return addRate;
	}

	/** Returns true if the shipping method is acceptable. These are static rules
	 * that are normally only executed in a development environment or if the
	 * PS03 service is inactive.
	 * 
	 * @param method
	 * @param address
	 * @return */
	public boolean isAcceptableShippingMethod( String method, Order order ) {
		final boolean retVal;
		if( order instanceof DigitalOrderImpl && ( (DigitalOrderImpl)order ).isStoreOnlyShipping() ) {
			if(isLoggingDebug()){
				logDebug("in isAcceptableShippingMethod--> isStoreOnlyShipping block :: " 
				+ method + " :: " +  ShippingMethod.Ground.getMappedValue().equalsIgnoreCase( method ));
			}			
			retVal = ShippingMethod.Ground.getMappedValue().equalsIgnoreCase( method );
		} else {
			retVal = true;
		}

		return retVal;
	}

	/** Entry point into this service. No longer calls GIS!?
	 * 
	 * @param webServicesConfig
	 *            WebServicesConfig reference
	 * @param order
	 *            Order object
	 * @param orderType
	 *            Order type
	 * @param shippingPromoMatches
	 * @param shippingHandlingInfo
	 * 
	 * @return OrderShippingQuote with shipping and handling rates */
	public OrderShippingQuote getOrderShippingQuote( Order order, String orderType, List shippingPromoMatches, ShippingHandlingInfo shippingHandlingInfo, Map extraObjectParams ) {
		if( !( order instanceof DigitalOrderImpl ) || !( (DigitalOrderImpl)order ).getIsGiftCardOrderOnly() ) { return staticOrderShippingQuote( order, orderType, shippingPromoMatches ); // STUB
		// data
		}
		return null;
	}

	/** @param order
	 * @param orderType
	 * @param shippingPromoMatches
	 * @return */
	private OrderShippingQuote staticOrderShippingQuote( Order order, String orderType, List shippingPromoMatches ) {
		OrderShippingQuote osQuote = new OrderShippingQuote();
		osQuote.setOrderId( order.getId() );
		List shippingGroups = order.getShippingGroups();
		if(isLoggingDebug()){
			logDebug("inside staticOrderShippingQuote---->"+shippingPromoMatches);
		}
		double orderShippingTotal = 0.0;
		for( int x = 0; x < shippingGroups.size(); x++ ) {
			ShippingGroup sg = (ShippingGroup)shippingGroups.get( x );
			if( sg instanceof HardgoodShippingGroup && sg.getState() != GIFT_CARD_SHIPPING_GROUP_STATE ) {
				HardgoodShippingGroup hsg = (HardgoodShippingGroup)sg;
				ShippingDestinationQuote sdQuote = new ShippingDestinationQuote();
				sdQuote.setShippingDestinationId( hsg.getId() );
				sdQuote.setGrossShippingAndHandling( staticShippingRate( order, hsg.getId(), Objects.firstNonNull( hsg.getShippingMethod(), ShippingMethod.Ground.toString() ) ) );
				sdQuote.setNetShippingAndHandling( sdQuote.getGrossShippingAndHandling() );
				sdQuote.setShippingAndHandlingSavings( 0.0 );
				osQuote.addShippingDestinationRate( sdQuote );
				orderShippingTotal += sdQuote.getNetShippingAndHandling();
			}
		}
		osQuote.setGrossShippingAndHandling( orderShippingTotal );
		return osQuote;
	}

	private double staticShippingRate( Order order, String sgId, String serviceLevel ) {
		if( serviceLevel != null ) {
			if(isLoggingDebug()){
				logDebug("rateTable-->inside staticShippingRate"+sgId);
			}
			ShippingRateTable rateTable = staticShippingRateTable( order, false );
			if(isLoggingDebug()){
				logDebug("rateTable-->"+rateTable);
			}
			List rates = rateTable.getRatesList( sgId );
			if(rates != null){
				for( int x = 0; x < rates.size(); x++ ) {
					if(isLoggingDebug()){
						logDebug("rates.size-->"+rates.size());
					}
					ShippingRate sr = (ShippingRate)rates.get( x );
					if(isLoggingDebug()){
						logDebug("rates.size-->"+sr.getBaseRate());
					}
					if( sr != null && serviceLevel.equals( sr.getBaseRateServiceLevel() ) ) { return sr.getBaseRate(); }
				}
			}
		}
		return 0.0;
	}
}
