package com.digital.commerce.services.order;


import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.OrderLookup;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.core.util.ContactInfo;
import atg.nucleus.naming.ParameterName;
import atg.payment.creditcard.CreditCardStatus;
import atg.payment.creditcard.CreditCardStatusImpl;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.domain.ResponseWrapper;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.reward.RewardService;
import com.digital.commerce.integration.reward.bts.BtsRewardService;
import com.digital.commerce.integration.reward.domain.RewardsEnticementsResponse;
import com.digital.commerce.integration.reward.domain.RewardsSendOrderData;
import com.digital.commerce.integration.reward.domain.api.RewardsSendOrderRequest;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.creditcard.DigitalCreditCardStatus;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalOrderLookup extends OrderLookup {

	private static final ParameterName EMAIL = ParameterName
			.getParameterName("email");
	private static final ParameterName REPRICE_ORDER_FLAG = ParameterName
			.getParameterName("repriceOrderFlag");
	
	private DigitalProfileTools profileTools;
	
	private Repository repository;

	protected MessageLocator messageLocator;
	
	private int basicRewardsPointsPerDollar;
	
	private DigitalRewardsManager		rewardManager;
	
	private BtsRewardService rewardsService;
	
	private boolean softFraud;

	private static final String			GC						= "giftCard";
	private static final String			ID						= "id";
	private static final String			GC_NUMBER				= "giftCardNumber";
	private static final String			AMOUNT						= "amount";
	private static final int DSWCreditCardStatus = 0;


	@Override
	public void service(DynamoHttpServletRequest pReq,
			DynamoHttpServletResponse pRes) throws ServletException,
			IOException {

		final String METHOD_NAME = "service";
		final String NO_INPUT = "ORDER_LOOKUP_NO_INPUT";
		final String NO_ORDER_FOUND = "ORDER_LOOKUP_ORDER_NOT_FOUND";
		final String ORDER_EMAIL_NO_MATCH = "ORDER_LOOKUP_ORDER_EMAIL_DOES_NOT_MATCH";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
					this.getName(), METHOD_NAME);
			String orderIdpassed = (String) pReq.getLocalParameter(ORDERID);
			String email = (String) pReq.getLocalParameter(EMAIL);
			boolean repriceOrderFlag = false;
			Object repriceOrderFlagObj = pReq.getLocalParameter(REPRICE_ORDER_FLAG);
			if(repriceOrderFlagObj != null){
				repriceOrderFlag = new Boolean((String)repriceOrderFlagObj); 
			}
			
			if (isLoggingDebug()) {
				logDebug("In DigitalOrderLookup orderIdpassed --> " + orderIdpassed);
				logDebug("In DigitalOrderLookup email --> " + email);
			}
			if (DigitalStringUtil.isBlank(orderIdpassed)
					|| DigitalStringUtil.isBlank(email)) {
				pReq.setParameter("result",
						this.getResponsErrorWrapper(NO_INPUT));
				pReq.serviceLocalParameter("error", pReq, pRes);
				return;
			}
			if (!orderExists(orderIdpassed)) {
				pReq.setParameter("result",
						this.getResponsErrorWrapper(NO_ORDER_FOUND));
				pReq.serviceLocalParameter("error", pReq, pRes);
				return;
			}
			if (!repriceOrderFlag && !orderAndEmailMatch(orderIdpassed, email)) {
				pReq.setParameter("result",
						this.getResponsErrorWrapper(ORDER_EMAIL_NO_MATCH));
				pReq.serviceLocalParameter("error", pReq, pRes);
				return;
			}
			super.service(pReq, pRes);
		} catch (CommerceException ces) {
			logError("Exception in orderlookup service", ces);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
					this.getName(), METHOD_NAME);
		}
	}

	/**
	 * @param orderIdpassed
	 * @return
	 * @throws CommerceException
	 */
	public boolean orderExists(String orderIdpassed) throws CommerceException {
		boolean orderExists = getOrderManager().orderExists(orderIdpassed);
		if (isLoggingDebug()) {
			logDebug("orderExists --> " + orderExists);
		}
		return orderExists;
	}

	/**
	 * @param orderIdpassed
	 * @param email
	 * @return
	 */
	public boolean orderAndEmailMatch(String orderIdpassed, String email) {
		String emailAddress = "";
		try {
			if (!DigitalStringUtil.isBlank(email)) {
				@SuppressWarnings("unchecked")
				List<PaymentGroup> paymentGroupList = getOrderInstance(
						orderIdpassed).getPaymentGroups();
				if(isLoggingDebug()) {
					logDebug("orderIdpassed " + orderIdpassed);
					logDebug("orderId loaded " + getOrderInstance(orderIdpassed));
					logDebug("OrderId from paymentGroupList" + paymentGroupList);
				}
				if (paymentGroupList != null) {
					for (PaymentGroup pg : paymentGroupList) {
						if (pg instanceof CreditCard) {
							if (isLoggingDebug()) {
								logDebug("In DigitalOrderLookup CC pay group --> "
										+ pg.getClass());
							}
							DigitalCreditCard creditCard = (DigitalCreditCard) pg;
							DigitalRepositoryContactInfo cf = (DigitalRepositoryContactInfo) creditCard.getBillingAddress();
							if(cf != null){
								emailAddress = cf.getEmail();
								if (isLoggingDebug()) {
									logDebug("In DigitalOrderLookup Email from CC Pay Group" + emailAddress);
								}							
								if (email.equalsIgnoreCase(emailAddress)) {
									if (isLoggingDebug()) {
										logDebug("In DigitalOrderLookup Email matched from CC Pay Group");
									}
									return true;
								}
							}
						} else if (pg instanceof PaypalPayment) {
							if (isLoggingDebug()) {
								logDebug("In DigitalOrderLookup PP pay group --> "
										+ pg.getClass());
							}
							PaypalPayment paypalPayment = (PaypalPayment) pg;
							emailAddress = (String) paypalPayment
									.getBillingAddress().getEmail();
							if (email.equalsIgnoreCase(emailAddress)) {
								if (isLoggingDebug()) {
									logDebug("In DigitalOrderLookup Email matched from PP Pay Group");
								}
								return true;
							}
						} else {
							@SuppressWarnings("unchecked")
							Collection<ShippingGroup> shippingGroups = getOrderInstance(
									orderIdpassed).getShippingGroups();
							for (ShippingGroup sg : shippingGroups) {
								if (sg instanceof HardgoodShippingGroup) {
									if (isLoggingDebug()) {
										logDebug("In DigitalOrderLookup Not found in Billing Address so get it from Shipping Address --> "
												+ sg.getClass());
									}
									@SuppressWarnings("unchecked")
									Collection<CommerceItemRelationship> rels = sg
											.getCommerceItemRelationships();
									if (rels != null && rels.size() > 0) {
										HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sg;
										ContactInfo shippingAddress = (ContactInfo) hgSg
												.getShippingAddress();
										emailAddress = shippingAddress
												.getEmail();
										if (email
												.equalsIgnoreCase(emailAddress)) {
											if (isLoggingDebug()) {
												logDebug("In DigitalOrderLookup Email matched from Shipping Group");
											}
											return true;
										}
									}
								}
							}
						}
					}
					if(paymentGroupList.size()==0){
						@SuppressWarnings("unchecked")
						Collection<ShippingGroup> shippingGroups = getOrderInstance(
								orderIdpassed).getShippingGroups();
						for (ShippingGroup sg : shippingGroups) {
							if (sg instanceof HardgoodShippingGroup) {
								if (isLoggingDebug()) {
									logDebug("In DigitalOrderLookup Not found in Billing Address so get it from Shipping Address --> "
											+ sg.getClass());
								}
								@SuppressWarnings("unchecked")
								Collection<CommerceItemRelationship> rels = sg
										.getCommerceItemRelationships();
								if (rels != null && rels.size() > 0) {
									HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sg;
									ContactInfo shippingAddress = (ContactInfo) hgSg
											.getShippingAddress();
									emailAddress = shippingAddress
											.getEmail();
									if (email
											.equalsIgnoreCase(emailAddress)) {
										if (isLoggingDebug()) {
											logDebug("In DigitalOrderLookup Email matched from Shipping Group");
										}
										return true;
									}
								}
							}
						}						
						
					}
				}
			}
		} catch (Exception e) {
			logError("Exception from validateOrderAndEmail:::", e);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see atg.commerce.order.OrderLookup#searchByOrderId(atg.servlet.
	 * DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse,
	 * java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void searchByOrderId(DynamoHttpServletRequest pReq,
			DynamoHttpServletResponse pRes, String orderId)
			throws ServletException, IOException {
		final String METHOD_NAME = "searchByOrderId";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
					this.getName(), METHOD_NAME);

			super.searchByOrderId(pReq, pRes, orderId);
			if (isLoggingDebug()) {
				logDebug("orderId = " + orderId);
			}

			DigitalOrderImpl orderTmp = (DigitalOrderImpl) pReq
					.getObjectParameter("result");
			DigitalDetailedOrder updatedOrder = new DigitalDetailedOrder();
			Map<String, Object> addlOrderDetails = getAddtionalOrderDetails(
					orderTmp, pReq);
			String fname = (String) addlOrderDetails.get("firstname");
			String lname = (String) addlOrderDetails.get("lastname");
			
			updatedOrder.setFirstName(fname);
			updatedOrder.setLastName(lname);
			updatedOrder.setApproxRewardPoints((int) addlOrderDetails
					.get("approxRewardPoints"));
			
			
			updatedOrder.setOrder(orderTmp);

			Map<String, Double> osMap = (HashMap<String, Double>) addlOrderDetails
					.get("orderSummary");
			pReq.setParameter("result", updatedOrder);
			pReq.setParameter("orderSummary", osMap);
			pReq.setParameter("count", Integer.valueOf(1));
			pReq.serviceLocalParameter("output", pReq, pRes);

		} catch (Exception e) {
			logError("Exception from searchByOrderId:::: ", e);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
					this.getName(), METHOD_NAME);
		}
	}

	/**
	 * @param order
	 * @param request
	 * @return
	 */
	private Map<String, Object> getAddtionalOrderDetails(DigitalOrderImpl order,
			DynamoHttpServletRequest request) {
		Map<String, Object> orderAddlDetails = new HashMap<>();
		try {
			String firstName = "Shoe Lover";
			String lastName = "";
			double rewardsprojectedEarnings = 0d;
			
			if (null != order) {
				@SuppressWarnings("unchecked")
				List<PaymentGroup> paymentGroupList = order
						.getPaymentGroups();

				if (paymentGroupList != null) {
					for (PaymentGroup pg : paymentGroupList) {
						if (pg instanceof CreditCard) {
							CreditCard creditCard = (CreditCard) pg;
							lastName = (String) creditCard
									.getBillingAddress().getLastName();
							firstName = (String) creditCard
									.getBillingAddress().getFirstName();
						} else if (pg instanceof PaypalPayment) {
							PaypalPayment paypalPayment = (PaypalPayment) pg;
							firstName = (String) paypalPayment
									.getBillingAddress().getFirstName();
							lastName = (String) paypalPayment
									.getBillingAddress().getLastName();
						}
					}
				}
			} else {
				logInfo("order is null!!");
			}
			
			if(DigitalStringUtil.isBlank(firstName) || DigitalStringUtil.isBlank(lastName)){
				Profile profile = getProfile();
				if ((null != profile) && profileTools.isLoggedIn(profile)) {
					firstName = (String) profile.getPropertyValue("firstName");
					lastName = (String) profile.getPropertyValue("lastName");
				}
			}
			orderAddlDetails.put("firstname", firstName);
			orderAddlDetails.put("lastname", lastName);
			rewardsprojectedEarnings = order.getPriceInfo().getAmount();			
			
			double amountGiftCardsTobeSubtracted = 0d;
			
			@SuppressWarnings("unchecked")
			List<DigitalCommerceItem> listOrderItems = order.getCommerceItems();
			for (DigitalCommerceItem item : listOrderItems) {
				if (item instanceof GiftCardCommerceItem) {
					amountGiftCardsTobeSubtracted = amountGiftCardsTobeSubtracted
							+ item.getPriceInfo().getAmount();
				}
			}
			rewardsprojectedEarnings = rewardsprojectedEarnings
					- amountGiftCardsTobeSubtracted;
			int approxRewardsPoints = (int) Math.floor(rewardsprojectedEarnings
					* basicRewardsPointsPerDollar);
			orderAddlDetails.put("approxRewardPoints", new Integer(
					approxRewardsPoints));
			if (isLoggingDebug()) {
				logDebug("OrderSummary-->"
						+ request
								.resolveName(ComponentLookupUtil.ORDER_SUMMARY));
			}
			
			if (null != request.resolveName(ComponentLookupUtil.ORDER_SUMMARY)) {
				DigitalOrderSummary orderSummary = (DigitalOrderSummary) request
						.resolveName(ComponentLookupUtil.ORDER_SUMMARY);
				Map<String, Object> osMap = new HashMap<>();
				orderSummary.setOrder(order);
				osMap.put("promotionalSavings",
						orderSummary.getPromotionalSavings());
				osMap.put("rewardsShare", orderSummary.getRewardsShare());
				osMap.put("subTotal", orderSummary.getSubTotal());
				osMap.put("tax", orderSummary.getTax());
				osMap.put("shipping", orderSummary.getShipping());
				osMap.put("orderTotal", orderSummary.getOrderTotal());
				osMap.put("giftCardTotal", orderSummary.getGiftCardTotal());
				osMap.put("creditCardPaypalTotal",
						orderSummary.getCreditCardPaypalTotal());
				osMap.put("amountOwe", orderSummary.getAmountOwe());
				osMap.put("savings", orderSummary.getSavings());
				osMap.put("totalDiscounts", orderSummary.getTotalDiscounts());
				osMap.put("regularShippingPrice", orderSummary.getRegularShippingPrice());
				osMap.put("giftCardShippingPrice", orderSummary.getGiftCardShippingPrice());
				osMap.put("giftCardShippingPriceList", orderSummary.getGiftCardShippingPriceList());
				osMap.put("qualifiedOffers", orderSummary.getAppliedCoupons());
				osMap.put("originalShippingPrice", orderSummary.getOriginalShippingPrice());
				osMap.put("nonGCShippingPrice", orderSummary.getNonGCShippingPrice());
				osMap.put("appliedGiftCards", getAppliedGiftCardsFromOrder(order));
				osMap.put("enticementPoints", getEnticementPoints(order));
				osMap.put("isSF", isSoftFraud());
				orderAddlDetails.put("orderSummary", osMap);
				
			}
			
		}

		catch (Exception e) {
			logError("Exception from getAddtionalOrderDetails", e);
		}
		return orderAddlDetails;

	}

	private Object getAppliedGiftCardsFromOrder(DigitalOrderImpl order) {
		/**Start - Add GiftCard Details */
		final Map<String, Map<String, Object>> giftCardBalanceMap = new LinkedHashMap<>();
		if( order != null ) {
			if( order.getPaymentGroups() != null && order.getPaymentGroups().size() > 0 ) {
					for( int i = 0; i < order.getPaymentGroups().size(); i++ ) {
						final PaymentGroup pg = (PaymentGroup)order.getPaymentGroups().get( i );
							if( pg != null && GC.equalsIgnoreCase( pg.getPaymentGroupClassType() ) ) {
								final DigitalGiftCard gc = (DigitalGiftCard)pg;
								final String giftCardNumber = gc.getCardNumber();
									if( DigitalStringUtil.isNotBlank( giftCardNumber )) {
										Map<String, Object> giftCardHMap = new HashMap<>();
										giftCardHMap.put(GC_NUMBER, giftCardNumber);
										giftCardHMap.put(ID, gc.getId());
										giftCardHMap.put(AMOUNT, new Double( DigitalCommonUtil.round(gc.getAmount())));
										giftCardBalanceMap.put( gc.getId(), giftCardHMap );
									}
							}
						} 
					}
		}
		return giftCardBalanceMap;
	}

	/**
	 * @param orderId
	 * @return
	 */
	public DigitalOrderImpl getOrderInstance(String orderId) {
		DigitalOrderImpl objOrder = null;
		try {
			if (!DigitalStringUtil.isBlank(orderId)) {
				objOrder = (DigitalOrderImpl) getOrderManager().loadOrder(orderId);
			}
		} catch (CommerceException ce) {
			logError("Error Loading Order", ce);
		}
		return objOrder;
	}

	/**
	 * 
	 * @return
	 */
	private Profile getProfile() {
		return ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PROFILE, Profile.class);
	}

	/**
	 * @param errorCode
	 * @return
	 */
	private ResponseWrapper getResponsErrorWrapper(String errorCode) {
		ResponseWrapper error = new ResponseWrapper();
		String errMsg = this.getMessageLocator().getMessageString(errorCode);
		ResponseError genError = new ResponseError(errorCode, errMsg);
		error.getGenericExceptions().add(genError);
		error.setFormError(false);
		error.setRequestSuccess(false);
		return error;
	}
	

	public int getEnticementPoints(DigitalOrderImpl order) {
		int points=0;
		//Call GCP to get the realTimePoints to return the actuall points.
		if (isLoggingDebug()){
			logDebug("START: In DigitalOrderLookup getEnticementPoints for Order ID --> " + order.getId());
		}
		Profile profile = getProfile();
		if (getRewardsService().isServiceEnabled() && getRewardsService().isServiceMethodEnabled(
				RewardService.ServiceMethod.RETRIEVE_ENTICEMENTS.getServiceMethodName())) {
			if( null != profile && !profileTools.isLoggedIn(profile) ){
				RewardsSendOrderData request=new RewardsSendOrderData();
				DigitalOrderTools orderTools=((DigitalOrderTools)getOrderManager().getOrderTools());
				if(null!=orderTools){
					orderTools.populateSendOrderRequest(order, request);
					boolean isFraud=isFraud(order,orderTools);
					setSoftFraud(isFraud);
					RewardsSendOrderRequest sendOrderRequest=orderTools.createRewardsSendOrderRequest(request);
					RewardsEnticementsResponse response=new RewardsEnticementsResponse();
					response=getRewardManager().retrieveEnticements(sendOrderRequest);
					if(null!=response && response.isRequestSuccess()){
						points=response.getPoints();
						if (isLoggingDebug()){
							logDebug("END:: In DigitalOrderLookup getEnticementPoints for Order ID WITH Points--> " + points);
						}
					}
				}
			}else{
				if (isLoggingInfo()){
					logInfo("In DigitalOrderLookup getEnticementPoints for LOGGED IN Customer with Order ID --> " + order.getId() );
				}
			}
		}else{
			if (isLoggingInfo()){
				logInfo(" Either REWARDS or Enticements is OFFLINE::: NOT calling Enticements for Order ID --> " + order.getId() );
			}
		}
		return points;
	}
	
	public boolean isFraud(DigitalOrderImpl order, DigitalOrderTools orderTools) {
		// TODO Auto-generated method stub
		boolean fraudFlag = false;
		final DigitalCreditCard creditCard = ((DigitalPaymentGroupManager) orderTools.getPaymentGroupManager())
				.findCreditCard(order);
		if (null != creditCard) {
			List<?> ccStatusList=(List<?>) creditCard.getRepositoryItem().getPropertyValue("authorizationStatus");
			if (ccStatusList != null && !ccStatusList.isEmpty()) {
				Object ccStatusObj = ccStatusList.iterator().next();
				if (ccStatusObj != null) {
					RepositoryItem ccStatus = (RepositoryItem) ccStatusObj;
					String fraudFlg = (String)ccStatus.getPropertyValue("fraudFlag");
          if (fraudFlg != null &&
							("Y".equalsIgnoreCase(fraudFlg) || "O".equalsIgnoreCase(fraudFlg))) {
						fraudFlag = true;
					}
				}
			}
		}
		return fraudFlag;
	}
}