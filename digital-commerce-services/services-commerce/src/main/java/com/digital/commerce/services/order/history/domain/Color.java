
package com.digital.commerce.services.order.history.domain;

import javax.annotation.Generated;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
@Getter
@Setter
@ToString
public class Color {

    private String styleCode;
    private String displayNameDefault;
    private String displayName;
    private String colorCode;

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(styleCode).append(displayNameDefault).append(displayName).append(colorCode).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Color) == false) {
            return false;
        }
        Color rhs = ((Color) other);
        return new EqualsBuilder().append(styleCode, rhs.styleCode).append(displayNameDefault, rhs.displayNameDefault).append(displayName, rhs.displayName).append(colorCode, rhs.colorCode).isEquals();
    }

}
