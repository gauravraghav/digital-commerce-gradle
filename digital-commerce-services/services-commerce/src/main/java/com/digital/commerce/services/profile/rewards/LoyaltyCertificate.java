package com.digital.commerce.services.profile.rewards;

import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

/**
 * Holds some basic information about a certificate
 */
@Getter
@Setter
@ToString
public class LoyaltyCertificate implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public static final Comparator<LoyaltyCertificate> EXPIRATION_DATE_COMPARATOR = new Comparator<LoyaltyCertificate>() {
    @Override
    public int compare(LoyaltyCertificate o1, LoyaltyCertificate o2) {
      int retVal = 0;
      if (o1 != null && o2 != null) {
        retVal = ComparisonChain.start()
            .compare(o1.getExpirationDate(), o2.getExpirationDate(), Ordering.natural().nullsLast())
            .result();
      }
      return retVal;
    }
  };

  public static final Comparator<LoyaltyCertificate> AMOUNT_COMPARATOR_WITH_SAME_DATE = new Comparator<LoyaltyCertificate>() {
    @Override
    public int compare(LoyaltyCertificate o1, LoyaltyCertificate o2) {
      int retVal = 0;
      if (o1 != null && o2 != null) {
        retVal = ComparisonChain.start()
            .compare(o1.getExpirationDate(), o2.getExpirationDate(), Ordering.natural().nullsLast())
            .compare(o1.getValue(), o2.getValue(), Ordering.natural().reverse().nullsLast())
            .result();
      }
      return retVal;
    }
  };


  public static final Comparator<LoyaltyCertificate> ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE = new Comparator<LoyaltyCertificate>() {
    @Override
    public int compare(LoyaltyCertificate o1, LoyaltyCertificate o2) {
      int retVal = 0;
      if (o1 != null && o2 != null) {
        retVal = ComparisonChain.start()
                .compare(o1.getExpirationDate(), o2.getExpirationDate(), Ordering.natural().nullsLast())
                .compare(o1.getValue(), o2.getValue(), Ordering.natural().reverse().nullsLast())
                .compare(o1.getId(), o2.getId(), Ordering.natural().reverse().nullsLast())
                .result();
      }
      return retVal;
    }
  };
  private String id;

  private String certificateNumber;

  private double value;

  private boolean valid;

  private Date expirationDate;

  private Date issueDate;

  private String markdownCode;

  private String status;

  private String type;

  private String offerDisplayName;

  private String offerDetailsPageName;

}
