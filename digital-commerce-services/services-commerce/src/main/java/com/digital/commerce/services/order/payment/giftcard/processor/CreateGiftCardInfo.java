/**
 *
 */
package com.digital.commerce.services.order.payment.giftcard.processor;

import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.payment.giftcard.GiftCardInfo;

import atg.commerce.order.Order;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.nucleus.GenericService;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import lombok.Getter;
import lombok.Setter;


/** @author wibrahim */
@Getter
@Setter
public class CreateGiftCardInfo extends GenericService implements PipelineProcessor {

	private static final int	SUCCESS				= 1;

	String					giftCardInfoClass	= "com.digital.edt.payment.giftcard.GiftCardInfo";

	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#getRetCodes() */
	public int[] getRetCodes() {
    return new int[]{ SUCCESS };
	}

	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(java.lang.Object,
	 * atg.service.pipeline.PipelineResult) */
	public int runProcess( Object pParam, PipelineResult pResult ) throws Exception {
		PaymentManagerPipelineArgs params = (PaymentManagerPipelineArgs)pParam;
		Order order = params.getOrder();
		DigitalGiftCard giftCard = (DigitalGiftCard)params.getPaymentGroup();
		double amount = params.getAmount();

		GiftCardInfo gci = getGiftCardInfo();
		addDataToGiftCardInfo( order, giftCard, amount, params, gci );

		if( isLoggingDebug() ) logDebug( "Putting GiftCardInfo object into pipeline: " + gci.toString() );

		params.setPaymentInfo( gci );

		return SUCCESS;
	}

	protected void addDataToGiftCardInfo( Order pOrder, DigitalGiftCard pPaymentGroup, double pAmount, PaymentManagerPipelineArgs pParams, GiftCardInfo pGiftCardInfo ) {
		pGiftCardInfo.setGiftCardNumber( pPaymentGroup.getCardNumber() );
		pGiftCardInfo.setPinNumber( pPaymentGroup.getPinNumber() );
		pGiftCardInfo.setAmount( pPaymentGroup.getAmount() );
		pGiftCardInfo.setPaymentId( pPaymentGroup.getId() );
	}

	protected GiftCardInfo getGiftCardInfo() throws Exception {
		if( isLoggingDebug() ) logDebug( "Making a new instance of type: " + getGiftCardInfoClass() );

    return (GiftCardInfo)Class.forName( getGiftCardInfoClass() ).newInstance();
	}
}
