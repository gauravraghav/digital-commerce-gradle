package com.digital.commerce.services.rewards.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RewardsCharitiesResponse extends ResponseWrapper {

  int count;
  private List<RewardsCharityItem> items;
}
