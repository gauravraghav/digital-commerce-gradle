package com.digital.commerce.services.promotions;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mk402314 on 12/11/2017.
 */
@SuppressWarnings("rawtypes")
@Getter
@Setter
public class GenericOffersListBean {
	Integer priority;
    Double  totalValue;
	List offers = new ArrayList();
}
