package com.digital.commerce.services.order;

import com.google.common.base.Predicate;

import atg.commerce.order.CommerceItem;
import atg.repository.RepositoryItem;

/** Can be used to find all commerce items that are invisible.
 * 
 * @author knaas */
public class FindCommerceItemWithInvisible implements Predicate<CommerceItem> {

	@Override
	public boolean apply( CommerceItem input ) {
		boolean retVal = false;
		if( input instanceof DigitalCommerceItem ) {
			RepositoryItem product = (RepositoryItem)input.getAuxiliaryData().getProductRef();
			if( product != null ) {
				retVal = !Boolean.TRUE.equals( product.getPropertyValue( "isActive" ) ) || !Boolean.TRUE.equals( product.getPropertyValue( "isVisible" ) );
			}
		}
		return retVal;
	}

}
