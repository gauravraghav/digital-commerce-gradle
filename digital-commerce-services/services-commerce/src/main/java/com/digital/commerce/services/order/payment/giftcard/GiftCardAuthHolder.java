package com.digital.commerce.services.order.payment.giftcard;

import java.util.HashMap;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;
import lombok.Setter;

/**
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
@Getter
@Setter
public class GiftCardAuthHolder {
	private HashMap	giftCardAuthInfos	= null;
	private Boolean loggingDebug;
	private static final DigitalLogger				logger					= DigitalLogger.getLogger( GiftCardPaymentServices.class );

	public GiftCardAuthInfo findGiftCardAuthInfoForPaymentGroupId( String paymentGroupId ) {
		if( giftCardAuthInfos == null ) {
			if( isLoggingDebug() )logger.debug("no GiftCardAuthInfos stored in this session" );
			return null;
		}

		if( DigitalStringUtil.isEmpty( paymentGroupId ) ) {
			if( isLoggingDebug() )logger.debug( "paymentGroupId is null, so can not look for GiftCardAuthInfo object" );
			return null;
		}

		GiftCardAuthInfo giftCardAuthInfo = (GiftCardAuthInfo)giftCardAuthInfos.get( paymentGroupId );

		if( giftCardAuthInfo == null ) {
			if( isLoggingDebug() )logger.debug( "no GiftCardAuthInfo object found for paymentGroupId: " + paymentGroupId );
			return null;
		} else {
			if( isLoggingDebug() )logger.debug( "found GiftCardAuthInfo object: " + giftCardAuthInfo + " for paymentGroupId: " + paymentGroupId );
			return giftCardAuthInfo;
		}
	}

	public void addGiftCardAuthInfo( String paymentGroupId, GiftCardAuthInfo giftCardAuthInfo ) {
		if( giftCardAuthInfos == null ) giftCardAuthInfos = new HashMap();

		if( DigitalStringUtil.isEmpty( paymentGroupId ) || giftCardAuthInfo == null ) {
			if( isLoggingDebug() )logger.debug( "paymentGroupId is empty or giftCardAuthInfo is null so not storing any auth info information." );
			return;
		}
		giftCardAuthInfos.put( paymentGroupId, giftCardAuthInfo );
	}

	public void removeGiftCardAuthInfo( String paymentGroupId ) {
		if( giftCardAuthInfos == null ) {
			if( isLoggingDebug() )logger.debug( "no GiftCardAuthInfos stored in this session" );
			return;
		}

		if( DigitalStringUtil.isEmpty( paymentGroupId ) ) {
			if( isLoggingDebug() )logger.debug( "paymentGroupId is null, so can not look for GiftCardAuthInfo object to remove" );
			return;
		}

		giftCardAuthInfos.remove( paymentGroupId );
	}

	/**
	 * @return the loggingDebug
	 */
	public Boolean isLoggingDebug() {
		return this.getLoggingDebug();
	}
}
