package com.digital.commerce.services.order.payment.afterpay.processor;

        import com.digital.commerce.services.order.payment.afterpay.AfterPayPaymentInfo;

public interface AfterPayPaymentProcessor {
    public AfterPayPaymentStatus createOrder(AfterPayPaymentInfo paymentInfo,String confirmURL, String cancelURL);

    public AfterPayPaymentStatus capturePayment(AfterPayPaymentInfo pAfterPayPaymentInfo);
}
