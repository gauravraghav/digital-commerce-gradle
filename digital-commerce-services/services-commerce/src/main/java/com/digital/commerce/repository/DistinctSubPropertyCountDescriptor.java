/**
 * 
 */
package com.digital.commerce.repository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

@SuppressWarnings({"rawtypes","unchecked"})
public class DistinctSubPropertyCountDescriptor extends RepositoryPropertyDescriptor

{

	private static final long		serialVersionUID	= -8795816015069305986L;

	protected static final String	TYPE_NAME			= "distinctSubPropertyCount";

	protected static final String	COLLECTION			= "collection";

	protected static final String	PROPERTY			= "property";
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, DistinctSubPropertyCountDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		if( pValue != null && pValue instanceof Integer ) { return pValue; }
		Integer count = Integer.valueOf( 0 );
		String property = (String)getValue( COLLECTION );
		String subProperty = (String)getValue( PROPERTY );
		Object list = pItem.getPropertyValue( property );
		if( list instanceof Collection ) {
			Collection propList = (Collection)list;
			if( subProperty == null || subProperty.trim().length() == 0 ) {
				return Integer.valueOf( propList.size() );
			} else {

				Iterator i = propList.iterator();
				Set subPropSet = new HashSet();
				while( i.hasNext() ) {
					RepositoryItem prop = (RepositoryItem)i.next();
					Object subPropVal = prop.getPropertyValue( subProperty );
					subPropSet.add( subPropVal );
				}

				count = Integer.valueOf( subPropSet.size() );
				pItem.setPropertyValueInCache( this, count );
				return count;
			}
		} else {
			throw new IllegalArgumentException( "collection must be an instance of java.util.Collection" );
		}
	}

	public String getTypeName() {
		return TYPE_NAME;
	}

}
