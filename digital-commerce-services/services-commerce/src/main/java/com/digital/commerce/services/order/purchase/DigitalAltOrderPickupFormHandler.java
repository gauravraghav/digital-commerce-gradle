package com.digital.commerce.services.order.purchase;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.DigitalShippingGroupManager;

import atg.commerce.order.OrderHolder;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.OrderManager;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.dtm.UserTransactionDemarcation;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalAltOrderPickupFormHandler extends GenericFormHandler {
	static final String CLASSNAME = DigitalAltOrderPickupFormHandler.class.getName();
	private OrderHolder shoppingCart;
	
	private TransactionManager transactionManager;
	
	private MessageLocator	messageLocator;
	
	private String altPickupFirstName;
	
	private String altPickupLastName;
	
	private String altPickupEmail;
	
	private RepeatingRequestMonitor repeatingRequestMonitor;	
	
	private static final String ALT_PERSON_ADD_EMPTY = "altPersonDetailsEmpty";
	
	private static final String ALT_PERSON_GENERIC_ERROR="altPersonGenericError";

	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleAddAltPickUpPerson(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {		
		final String METHOD_NAME = "DigitalAltOrderPickupFormHandler.handleAddAltPickUpPerson";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( CLASSNAME, METHOD_NAME);
		
		boolean ret = false;		
		
		try{
			RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
	
			if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
				if (isLoggingDebug()) {
					logDebug("DigitalAltOrderPickup.addAltPickUpPerson() ---- >> start");
					logDebug("FirstName-->" + altPickupFirstName + " LastName-->" + altPickupLastName + " Email--->"
							+ altPickupEmail);
				}
	
				if (DigitalStringUtil.isBlank(altPickupFirstName) || DigitalStringUtil.isBlank(altPickupLastName)
						|| DigitalStringUtil.isBlank(altPickupEmail)) {
					if (isLoggingDebug()) {
						logDebug("DigitalAltOrderPickupFormHandler.addAltPickUpPerson() ---- >>Required fields are empty");
					}
					String msg = getMessageLocator().getMessageString(ALT_PERSON_ADD_EMPTY);
					addFormException(new DropletException(msg, ALT_PERSON_ADD_EMPTY));
				}
				if (this.getFormError()) {
					return false;
				}
				
				final UserTransactionDemarcation td = TransactionUtils.startNewTransaction( CLASSNAME, METHOD_NAME);
				TransactionUtils.acquireTransactionLock( CLASSNAME, METHOD_NAME);
				try {
					OrderImpl order = (OrderImpl) this.getShoppingCart().getCurrent();
					synchronized(order){
						order.setPropertyValue("altPickupFirstName", altPickupFirstName);
						order.setPropertyValue("altPickupLastName", altPickupLastName);
						order.setPropertyValue("altPickupEmail", altPickupEmail);
						this.getShippingGroupManager().updateShipAddrNameForBopisBostsOrder(order);
						getOrderManager().updateOrder(order);
						ret = true;
					}
				} catch (Exception e) {
					if (isLoggingError()) {
						logError("Exception Error while adding alt pickup person: ", e);
					}					
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					String msg = getMessageLocator().getMessageString(ALT_PERSON_GENERIC_ERROR);
					addFormException(new DropletException(msg, ALT_PERSON_GENERIC_ERROR));
				} finally {
					TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
					if(null!=rrm) {
					rrm.removeRequestEntry(METHOD_NAME);
					}
					TransactionUtils.releaseTransactionLock( CLASSNAME, METHOD_NAME);
				}
			}else{
				logError("NOT_UNIQUE_REQUEST for :: " + CLASSNAME + " :: " + METHOD_NAME);
			}
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( CLASSNAME, METHOD_NAME);
		}
		return ret;
	}
	
	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleRemoveAltPickUpPerson(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String METHOD_NAME = "DigitalAltOrderPickupFormHandler.handleRemoveAltPickUpPerson";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( CLASSNAME, METHOD_NAME);

		try{
			RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
			boolean ret = false;
			
			if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
				if (isLoggingDebug()) {
					logDebug("DigitalAltOrderPickup.removeAltPickUpPerson() ---- >> start");
				}
	
				final UserTransactionDemarcation td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				TransactionUtils.acquireTransactionLock( CLASSNAME, METHOD_NAME);
				
				try {
					OrderImpl order = (OrderImpl) this.getShoppingCart().getCurrent();
					synchronized(order){
						order.setPropertyValue("altPickupFirstName", "");
						order.setPropertyValue("altPickupLastName", "");
						order.setPropertyValue("altPickupEmail", "");
						this.getShippingGroupManager().updateShipAddrNameForBopisBostsOrder(order);
						getOrderManager().updateOrder(order);
						ret = true;
					}
				} catch (Exception e) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					String msg = getMessageLocator().getMessageString(ALT_PERSON_GENERIC_ERROR);
					addFormException(new DropletException(msg, ALT_PERSON_GENERIC_ERROR));
					if (isLoggingError()) {
						logError("Exception Error while adding alt pickup person: ", e);
					}
				} finally {
					TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
					if(null!=rrm) {
					rrm.removeRequestEntry(METHOD_NAME);
					}
					TransactionUtils.releaseTransactionLock( CLASSNAME, METHOD_NAME);
				}
			}else{
				logError("NOT_UNIQUE_REQUEST for :: " + CLASSNAME + " :: " + METHOD_NAME);
			}
			return ret;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( CLASSNAME, METHOD_NAME);
		}
	}

	
	private OrderManager orderManager;

	/**
	 * 
	 * @return DigitalShippingGroupManager
	 */
	public DigitalShippingGroupManager getShippingGroupManager(){
    return (DigitalShippingGroupManager) this.getOrderManager().getShippingGroupManager();
	}
}
