package com.digital.commerce.services.order;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Relationship;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalCollectionUtil;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.services.order.payment.afterpay.AfterPayCheckoutManager;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.profile.rewards.LoyaltyCertificate;
import com.digital.commerce.services.promotion.DigitalCouponFormHandler;
import com.digital.commerce.services.promotion.DigitalPromotionTools;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
public class DigitalOrderSummary extends ApplicationLoggingImpl {

  private DigitalOrderTools orderTools;

  private DigitalOrderImpl order;

  private double rewardsShare = 0d;

  private double subTotal = 0d;

  private double tax = 0d;

  private double shipping = 0d;

  private double orderTotal = 0d;

  private double amountOwe = 0d;

  private double savings = 0d;

  private double creditCardPaypalTotal = 0d;

  private double giftCardTotal = 0d;

  private double promotionalSavings = 0d;

  private double orderTotalZero = 0;

  private double amountOweZero = 0;

  private double regularShippingPrice = 0;

  private double giftCardShippingPrice = 0;

  private double originalShippingPrice = 0;

  private double nonGCShippingPrice = 0d;

  private DigitalShippingGroupManager dswShippingGroupManager;

  private double totalDiscounts = 0d;

  private DigitalAppliedOffersBean appliedCoupons;

  private DigitalCouponFormHandler dswCouponFormHandler;

  private DigitalPromotionTools promotionTools;

  private List<Double> giftCardShippingPriceList = new ArrayList<>();
  
  private DigitalRewardsManager rewardsCertificateManager;

  private AfterPayCheckoutManager afterPayCheckoutManager;

  private double afterPayInstallmentAmount = 0d;

public double getNonGCShippingPrice() {
    return DigitalCommonUtil.round(this.shipping - this.giftCardShippingPrice);
  }

  public void setNonGCShippingPrice(double nonGCShippingPrice) {
    this.nonGCShippingPrice = nonGCShippingPrice;
  }


  /**
   * Get the Original (raw) shipping price of the Order (exclude promotions) If multiple Shipping
   * group exists (split shipment) then get the highest Shipping price
   *
   * @return double
   */
  public double getOriginalShippingPrice() {
    List<ShippingGroup> lstShipGroups = order.getShippingGroups();
    for (ShippingGroup shipGroup : lstShipGroups) {
      if (shipGroup.getPriceInfo() != null
          && !getDswShippingGroupManager().isBopisBostsShippingGroup(shipGroup)
          && !getDswShippingGroupManager().isGiftCardShippingGroup(shipGroup)
          && shipGroup.getPriceInfo().getRawShipping() > originalShippingPrice) {
        originalShippingPrice = shipGroup.getPriceInfo().getRawShipping();
      }
    }
    return originalShippingPrice;
  }

  /**
   * @return double
   */
  public double getTotalDiscounts() {
	  Double totalDiscountSavings=DigitalCommonUtil.round(orderTools.getTotalDiscountSavings(order));
	  totalDiscounts = DigitalCommonUtil.round(totalDiscountSavings + getRewardsShare());
    return totalDiscounts;
  }

  /**
   * @return double
   */
  public double getSubTotal() {
    if (order.getPriceInfo() != null) {
      subTotal = DigitalCommonUtil.round(order.getPriceInfo().getRawSubtotal());
    }
    return subTotal;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getSubTotal(DigitalOrderImpl orderPassed) {
    if (orderPassed.getPriceInfo() != null) {
      subTotal = DigitalCommonUtil.round(orderPassed.getPriceInfo()
          .getRawSubtotal());
    }
    return subTotal;
  }

  /**
   * @return double
   */
  public double getRewardsShare() {
    rewardsShare = DigitalCommonUtil.round(orderTools
        .getRewardsShareTotal(order));
    return rewardsShare;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getRewardsShare(DigitalOrderImpl orderPassed) {
    rewardsShare = DigitalCommonUtil.round(orderTools
        .getRewardsShareTotal(orderPassed));
    return rewardsShare;
  }

  /**
   * @return double
   */
  public double getTax() {
    if (order.getPriceInfo() != null) {
      tax = DigitalCommonUtil.round(order.getPriceInfo().getTax());
    }
    return tax;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getTax(DigitalOrderImpl orderPassed) {
    if (orderPassed.getPriceInfo() != null) {
      tax = DigitalCommonUtil.round(orderPassed.getPriceInfo().getTax());
    }
    return tax;
  }

  /**
   * @return double
   */
  public double getShipping() {
    if (order.getPriceInfo() != null) {
      shipping = DigitalCommonUtil.round(order.getPriceInfo().getShipping());
    }
    return shipping;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getShipping(DigitalOrderImpl orderPassed) {
    if (orderPassed.getPriceInfo() != null) {
      shipping = DigitalCommonUtil
          .round(orderPassed.getPriceInfo().getShipping());
    }
    return shipping;
  }

  /**
   * @return double
   */
  public double getOrderTotal() {
    if (order.getPriceInfo() != null) {
      orderTotal = DigitalCommonUtil.round(order.getPriceInfo().getTotal());
    }
    if (orderTotal <= 0) {
      return orderTotalZero;
    }
    return orderTotal;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getOrderTotal(DigitalOrderImpl orderPassed) {
    if (orderPassed.getPriceInfo() != null) {
      orderTotal = DigitalCommonUtil.round(orderPassed.getPriceInfo()
          .getRawSubtotal()
          - getPromotionalSavings(orderPassed)
          - getRewardsShare(orderPassed)
          + getTax(orderPassed)
          + getShipping(orderPassed));
    }
    if (orderTotal <= 0) {
      return orderTotalZero;
    }
    return orderTotal;
  }

  /**
   * @return double
   */
  public double getGiftCardTotal() {
    try {
      giftCardTotal = DigitalCommonUtil
          .round(orderTools.getGiftCardsTotal(order));
    }catch (Exception e){
      logError(e);
    }
    return giftCardTotal;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getGiftCardTotal(DigitalOrderImpl orderPassed) {
    giftCardTotal = DigitalCommonUtil.round(orderTools
        .getGiftCardsTotal(orderPassed));
    return giftCardTotal;
  }

  /**
   * @return double
   */
  public double getCreditCardPaypalTotal() {
    creditCardPaypalTotal = DigitalCommonUtil.round(orderTools
        .getCreditCardPaypalTotal(order));
    return creditCardPaypalTotal;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getCreditCardPaypalTotal(DigitalOrderImpl orderPassed) {
    creditCardPaypalTotal = DigitalCommonUtil.round(orderTools
        .getCreditCardPaypalTotal(orderPassed));
    return creditCardPaypalTotal;
  }

  /**
   * @return double
   */
  public double getAmountOwe() {
    amountOwe = DigitalCommonUtil.round(orderTotal
        - giftCardTotal);
    if (amountOwe <= 0) {
      return Math.abs(amountOweZero);
    }
    return amountOwe;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getAmountOwe(DigitalOrderImpl orderPassed) {
    amountOwe = DigitalCommonUtil.round(getOrderTotal(orderPassed)
        - orderTools.getGiftCardsTotal(orderPassed));
    if (amountOwe <= 0) {
      return Math.abs(amountOwe);
    }
    return amountOwe;
  }

  /**
   * @return double
   */
  public double getSavings() {
    savings = DigitalCommonUtil.round(orderTools
        .getTotalSavingsWithoutPromotions(order));
    return savings;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getSavings(DigitalOrderImpl orderPassed) {
    savings = DigitalCommonUtil.round(orderTools
        .getTotalSavingsWithoutPromotions(orderPassed));
    return savings;
  }

  /**
   * @return double
   */
  public double getPromotionalSavings() {
    try {
      promotionalSavings = DigitalCommonUtil.round(orderTools
          .getPromotionalSavings(order));
    }catch (Exception e){
      logError("Error while calculating savings", e);
    }
    return promotionalSavings;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getPromotionalSavings(DigitalOrderImpl orderPassed) {
    promotionalSavings = DigitalCommonUtil.round(orderTools
        .getPromotionalSavings(orderPassed));
    return promotionalSavings;
  }

  /**
   * @return double
   */
  public double getRegularShippingPrice() {
    regularShippingPrice = DigitalCommonUtil
        .round(this.getShipping() - this.getGiftCardShippingPrice());
    return regularShippingPrice;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getRegularShippingPrice(DigitalOrderImpl orderPassed) {
    regularShippingPrice = DigitalCommonUtil
        .round(this.getShipping(orderPassed) - this.getGiftCardShippingPrice(orderPassed));
    return regularShippingPrice;
  }

  /**
   * @return double
   */
  public double getGiftCardShippingPrice() {
    double total = 0.00;
    List<Double> giftcardShippingPriceList = this.getGiftCardShippingPriceList();
    for (Double giftcardShippingPrice : giftcardShippingPriceList) {
      total += DigitalCommonUtil.round(giftcardShippingPrice);
    }
    giftCardShippingPrice = DigitalCommonUtil.round(total);

    return giftCardShippingPrice;
  }

  /**
   * @param orderPassed
   * @return double
   */
  public double getGiftCardShippingPrice(DigitalOrderImpl orderPassed) {
    double total = 0.00;
    List<Double> giftcardShippingPriceList = this.getGiftCardShippingPriceList(orderPassed);
    for (Double giftcardShippingPrice : giftcardShippingPriceList) {
      total += DigitalCommonUtil.round(giftcardShippingPrice);
    }
    giftCardShippingPrice = DigitalCommonUtil.round(total);

    return giftCardShippingPrice;
  }

  /**
   * @return List
   */
  public List<Double> getGiftCardShippingPriceList() {
    giftCardShippingPriceList = new ArrayList<>();
    List<Relationship> relationships = this.order.getRelationships();
    calcGCShippingCost(relationships);
    return giftCardShippingPriceList;
  }

  /**
   * @param relationships
   */
  private void calcGCShippingCost(List<Relationship> relationships) {
    for (Relationship relationship : relationships) {
      if (relationship instanceof ShippingGroupCommerceItemRelationship) {
        ShippingGroupCommerceItemRelationship sgcir = (ShippingGroupCommerceItemRelationship) relationship;
        DigitalCommerceItem ci = (DigitalCommerceItem) sgcir.getCommerceItem();
        if (ci.getCommerceItemClassType() != null && ci.getCommerceItemClassType()
            .equals("giftCard")
            && sgcir.getShippingGroup() != null && sgcir.getShippingGroup().getPriceInfo() != null
            && sgcir.getShippingGroup().getPriceInfo().getAmount() > 0.00) {
          giftCardShippingPriceList
              .add(DigitalCommonUtil.round(sgcir.getShippingGroup().getPriceInfo().getAmount()));
        }
      }
    }
  }

  /**
   * @param orderPassed
   * @return List
   */
  public List<Double> getGiftCardShippingPriceList(DigitalOrderImpl orderPassed) {
    giftCardShippingPriceList = new ArrayList<>();
    List<Relationship> relationships = orderPassed.getRelationships();
    calcGCShippingCost(relationships);
    return giftCardShippingPriceList;
  }

  /**
   * @return DigitalAppliedOffersBean
   */
  public DigitalAppliedOffersBean getAppliedCoupons() {

    DigitalAppliedOffersBean offers = new DigitalAppliedOffersBean();
    //CO-347: Get all ITEM, Order, Rewards promotion details with the discount/promotion value
    getAllPromotionsApplied(offers);
    appliedCoupons = offers;
    return appliedCoupons;
  }

  /**
   * @param offers
   */
  private void getAllPromotionsApplied(DigitalAppliedOffersBean offers) {
    DigitalOrderImpl order = getOrder();

    if (null != order) {
      //ADD REWARDS CERTIFICATES
      List<LoyaltyCertificate> appliedCertificates = getDswCouponFormHandler()
          .getAppliedORReservedOrderCertificates();
      List<DigitalOfferDetailsBean> rewardsPromotions = new ArrayList<>();
      List<DigitalOfferDetailsBean> pointsPromotions = new ArrayList<>();
      if (null != appliedCertificates && !appliedCertificates.isEmpty()) {
    	 //Update offerDisplayName on the certificates
    	  rewardsCertificateManager.updateOfferDisplayNameOnCerts(appliedCertificates);
        for (LoyaltyCertificate cert : appliedCertificates) {
          DigitalOfferDetailsBean offerDetail = new DigitalOfferDetailsBean();
          offerDetail.setDescription(cert.getOfferDisplayName());
          offerDetail.setSavingsValue(-cert.getValue());
          offerDetail.setOfferCode(cert.getMarkdownCode());
          //Check if the cert is 2X or 3X points and if yes; put the cert in pointsPromotion
          if(rewardsCertificateManager.isDoublePointsOffer(cert.getCertificateNumber()) || rewardsCertificateManager.isTriplePointsOffer(cert.getCertificateNumber())){
        	  pointsPromotions.add(offerDetail);
          }else{
        	  rewardsPromotions.add(offerDetail);
          }
          
        }
      }

      

      //ADD ORDER LEVEL PROMOS
      List<DigitalOfferDetailsBean> orderPromotions = new ArrayList<>();
      OrderPriceInfo orderPriceInfo = order.getPriceInfo();
      if (null != orderPriceInfo) {
        List<PricingAdjustment> pricingAdjustments = orderPriceInfo.getAdjustments();
        if (!DigitalCollectionUtil.isEmpty(pricingAdjustments)) {
          getListOfOffers(pricingAdjustments, orderPromotions, pointsPromotions);
        }
      }

      //ADD ITEM LEVEL PROMOS--loop through commerce Items to get the list
      List<DigitalOfferDetailsBean> itemPromotions = new ArrayList<>();
      List<CommerceItem> commerceItems = order.getCommerceItems();
      if (null != commerceItems && !commerceItems.isEmpty()) {
        for (CommerceItem ci : commerceItems) {
          ItemPriceInfo itemPriceInfo = ci.getPriceInfo();
          if (null != itemPriceInfo) {
            List<PricingAdjustment> pricingAdjustments = itemPriceInfo.getAdjustments();
            if (null != pricingAdjustments) {
              getListOfOffers(pricingAdjustments, itemPromotions, pointsPromotions);
            }
          }
        }
      }

      offers.setItemPromotions(itemPromotions);
      offers.setOrderPromotions(orderPromotions);
      offers.setRewardsPromotions(rewardsPromotions);
      offers.setPointsPromotions(pointsPromotions);
    }
  }

  /**
   * @param pricingAdjustments
   * @param qualifiedDiscounts
   * @param zeroDollarQualifiedDiscounts
   */
  private void getListOfOffers(List<PricingAdjustment> pricingAdjustments,
      Collection<DigitalOfferDetailsBean> qualifiedDiscounts,
      Collection<DigitalOfferDetailsBean> zeroDollarQualifiedDiscounts) {

    for (PricingAdjustment pa : pricingAdjustments) {
      String couponId;
      String promotionUserFriendlyName;
      DigitalOfferDetailsBean offerDetail = new DigitalOfferDetailsBean();
      double totalAdjustment = pa.getTotalAdjustment();
      RepositoryItem coupon = pa.getCoupon();
      RepositoryItem promotion = pa.getPricingModel();
      if (coupon == null && promotion != null) {
        try {
          RepositoryItem[] coupons = dswCouponFormHandler.getClaimableTools()
              .getCouponsForPromotion(promotion.getRepositoryId());
          if (coupons != null && coupons.length > 0) {
            coupon = coupons[0];
          }
        } catch (Exception e) {
          logError("Error getting coupons associated with promotion", e);
        }
      }
      if (null != coupon) {
        couponId = coupon.getRepositoryId();
        offerDetail.setOfferCode(couponId);
      } else if (null != promotion) {
        offerDetail.setOfferCode(promotion.getRepositoryId());
      }

      if (null != promotion) {
        promotionUserFriendlyName = (String) promotion.getPropertyValue("description");
        if(null==promotionUserFriendlyName){
        	promotionUserFriendlyName=(String) promotion.getPropertyValue("userFriendlyName");
        }
        offerDetail.setDescription(promotionUserFriendlyName);
        offerDetail.setSavingsValue(totalAdjustment);
        
        boolean duplicateOfferCode = false;
        for (DigitalOfferDetailsBean addedOfferDetail : qualifiedDiscounts) {
          if (addedOfferDetail.getOfferCode().equalsIgnoreCase(offerDetail.getOfferCode())) {
            duplicateOfferCode = true;
            double newSavings = addedOfferDetail.getSavingsValue() + totalAdjustment;
            addedOfferDetail.setSavingsValue(newSavings);
          }
        }
        if (!duplicateOfferCode) {
          if (offerDetail.getSavingsValue() != 0d) {
            qualifiedDiscounts.add(offerDetail);
          } else {
            zeroDollarQualifiedDiscounts.add(offerDetail);
          }
        }
      }
    }
  }


	public DigitalRewardsManager getRewardsCertificateManager() {
			rewardsCertificateManager = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.REWARD_CERTIFICATE_MANAGER, DigitalRewardsManager.class );
		return rewardsCertificateManager;
	}

    public double getAfterPayInstallmentAmount() {
      afterPayInstallmentAmount = getAfterPayCheckoutManager().getInstallmentAmountForCart(order);
      return afterPayInstallmentAmount;
    }
}