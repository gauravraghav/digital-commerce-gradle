package com.digital.commerce.services.order;

import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;

@Getter
public enum GiftCardType {
	STANDARD("STANDARD-GC"), PERSONALIZED("PERSONALIZED-GC"), ELECTRONIC("E-GC");

	private final String	value;

	private GiftCardType( final String value ) {
		this.value = value;
	}

	public static GiftCardType valueFor( String value ) {
		GiftCardType retVal = null;
		for( final GiftCardType type : GiftCardType.values() ) {
			if( DigitalStringUtil.equalsIgnoreCase( type.getValue(), value ) ) {
				retVal = type;
			}
		}
		return retVal;
	}
}
