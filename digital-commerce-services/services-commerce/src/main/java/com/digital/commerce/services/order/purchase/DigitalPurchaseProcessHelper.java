/**
 *
 */
package com.digital.commerce.services.order.purchase;

import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.nucleus.naming.ComponentName;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import com.digital.commerce.common.util.HttpServletUtil;
import com.digital.commerce.services.pricing.DigitalPricingTools;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.DigitalCommerceItem;

import atg.commerce.CommerceException;
import atg.commerce.catalog.CatalogTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.Order;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.order.purchase.PurchaseProcessHelper;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

import javax.servlet.ServletException;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalPurchaseProcessHelper extends PurchaseProcessHelper {

	private String	categoryId;
	private String	categoryName;
	private String	productRefRoot;
	private ComponentName userPricingModelsPath;
	private PricingTools pricingTools;

	static final String CLASS_NAME = "DigitalPurchaseProcessHelper";

	public void storeExtendedCommerceItemProperties( String catId, String catName, String productRefRoot ) {
		this.categoryId = catId;
		this.categoryName = catName;
		this.productRefRoot = productRefRoot;

	}

	/**
	 * @param pUserPricingModelsPath
	 */
	public void setUserPricingModelsPath(String pUserPricingModelsPath) {
		if (pUserPricingModelsPath != null) {
			userPricingModelsPath = ComponentName.getComponentName(pUserPricingModelsPath);
		} else {
			userPricingModelsPath = null;
		}
	}

	/**
	 * @return String
	 */
	public String getUserPricingModelsPath() {
		if (userPricingModelsPath != null) {
			return userPricingModelsPath.getName();
		}
		return null;
	}

	/**
	 * Adjusts auto skus in the cart. If the sku belongs to a collateral bundle then remove it
	 * from the order
	 *
	 * @param order
	 * @return true or false
	 */
	public boolean adjustAutomaticSKUs(Order order) {
		final String METHOD_NAME = "adjustAutomaticSKUs";
		boolean isAutoSKURemoved = false;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			List commerceItems = order.getCommerceItems();
			// loop through all the items in the cart
			for (int x = 0; x < commerceItems.size(); x++) {
				CommerceItem ci = (CommerceItem) commerceItems.get(x);
				RepositoryItem product = (RepositoryItem) ci.getAuxiliaryData().getProductRef();
				// if there's an auto sku in the cart, add it's quantity to the hashmap
				if ("collateral".equals(product.getPropertyValue("productType"))) {
					moveItemToCollateral(ci, order);
					isAutoSKURemoved = true;
				}

			}
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}

		return isAutoSKURemoved;
	}

	/**
	 * @param ci
	 * @param order
	 */
	private void moveItemToCollateral(CommerceItem ci, Order order) {

		RepositoryItem sku = (RepositoryItem) ci.getAuxiliaryData().getCatalogRef();
		try {
			getCommerceItemManager().removeItemFromOrder(order, ci.getId());
			logInfo("As this is a autoSKU (aka collateral bundled item) " + ci.getCatalogRefId()
					+ " removed this item from the order " + order.getId());
			getPricingTools().priceOrderTotal(order,
					getPricingModelHolder(ServletUtil.getCurrentRequest()),
					HttpServletUtil.getUserLocale(ServletUtil.getCurrentRequest()), getProfile(), null);
		} catch (CommerceException | ServletException | IOException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		OrderImpl rOrder = (OrderImpl) order;
		List collaterals = (List) rOrder.getPropertyValue("collateralSkus");
		if (collaterals == null) {
			collaterals = new ArrayList();
		}
		collaterals.add(sku.getRepositoryId());
		// TODO move collateralSkus to property manager
		rOrder.setPropertyValue("collateralSkus", collaterals);
	}

	/**
	 * @param pRequest
	 * @return PricingModelHolder
	 */
	private PricingModelHolder getPricingModelHolder(DynamoHttpServletRequest pRequest) {
		String perfName = "getPricingModelHolder";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASS_NAME, perfName);
		PricingModelHolder holder = null;
		try {
			Collection models = (Collection) pRequest
					.getObjectParameter(ParameterName.getParameterName("pricingModels"));
			if ((models == null) && (userPricingModelsPath != null)) {
				holder = (PricingModelHolder) pRequest.resolveName(userPricingModelsPath);
			}
			return holder;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASS_NAME, perfName);
		}
	}

	/**
	 * @return RepositoryItem
	 */
	private RepositoryItem getProfile() {
		final String METHOD_NAME = "getProfile";
		DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
		try {
			RepositoryItem profile = ServletUtil.getCurrentUserProfile();
			return profile;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
		}
	}

	public void adjustSkuQuantity( Order order, String skuId, long oldQty, long newQty ) throws CommerceException {
		final String METHOD_NAME="lookupUser";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		
			if( isLoggingDebug() ) {
				logDebug( "Adjusting " + skuId + " from " + oldQty + " to " + newQty );
			}
			if( oldQty == newQty ) { return; }
			// if the new quantity is 0, just remove it
			if( newQty == 0 ) {
				List commerceItems = order.getCommerceItemsByCatalogRefId( skuId );
				// get all the commerce items referencing this sku and remove them
				for( int x = 0; x < commerceItems.size(); x++ ) {
					CommerceItem ci = (CommerceItem)commerceItems.get( x );
					getCommerceItemManager().removeItemFromOrder( order, ci.getId() );
				}
	
			} else if( newQty > oldQty ) {
				// if we're adding create the commerce item for the new sku and add
				// it
				long addQty = newQty - oldQty;
				CatalogTools catTools = getOrderManager().getOrderTools().getCatalogTools();
				try {
					RepositoryItem sku = catTools.findSKU( skuId );
					Set parentProducts = (Set)sku.getPropertyValue( "parentProducts" );
					if( parentProducts != null && parentProducts.size() > 0 ) {
						RepositoryItem product = (RepositoryItem)parentProducts.iterator().next();
						CommerceItem ci = getCommerceItemManager().createCommerceItem( skuId, product.getRepositoryId(), addQty );
						getCommerceItemManager().addItemToOrder( order, ci );
					}
				} catch( RepositoryException e ) {
					if( isLoggingError() ) {
						logError( e );
					}
				}
	
			} else if( newQty < oldQty ) {
				// if we're removing
				long remQty = oldQty - newQty;
				List commerceItems = order.getCommerceItemsByCatalogRefId( skuId );
				// loop through the commerce items representing this sku and remove
				// as many as we need. We loop through because there could be two
				// seperate commerce items which represent the same SKU
				for( int x = 0; x < commerceItems.size() && remQty > 0; x++ ) {
					CommerceItem ci = (CommerceItem)commerceItems.get( x );
					if( ci.getQuantity() > remQty ) {
						getCommerceItemManager().adjustItemRelationshipsForQuantityChange( order, ci, remQty );
						ci.setQuantity( remQty );
						remQty = 0;
					} else {
						remQty -= ci.getQuantity();
						getCommerceItemManager().removeItemFromOrder( order, ci.getId() );
					}
				}
	
			}
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}


	
	@Override
	 protected CommerceItem createCommerceItem(AddCommerceItemInfo pItemInfo, String pCatalogKey, Order pOrder)
		     throws CommerceException
		   {
		     CommerceItemManager cimgr = getCommerceItemManager();
		     String siteId = pItemInfo.getSiteId();
		     if (DigitalStringUtil.isBlank(siteId)) {
		       siteId = null;
		     }
		     DigitalCommerceItem dswCI = (DigitalCommerceItem)cimgr.createCommerceItem(pItemInfo.getCommerceItemType(), pItemInfo.getCatalogRefId(), null, pItemInfo.getProductId(), null, pItemInfo.getQuantity(), pCatalogKey, null, siteId, null);
		     dswCI.setShipType((String)pItemInfo.getValue().get("shipType"));
			 dswCI.setStoreId((String)pItemInfo.getValue().get("storeId"));
			//KTLO-592: Adding the locationId details on the commerce item
			 dswCI.setLocationId((String)pItemInfo.getValue().get("locationId"));
			 dswCI.setItemQuantityReserved(-1);
		     if (isLoggingDebug()) {
		       logDebug("Item created with ID " + dswCI.getId());
		     }
		     CommerceItem ciFinal = cimgr.addItemToOrder(pOrder, dswCI);
		     if ((isLoggingDebug()) && (ciFinal != dswCI)) {
		       logDebug("New item " + dswCI.getId() + " merged into existing item " + ciFinal.getId());
		     }

		     return ciFinal;
		   }
	
	/**Reset Merged Quantity
	 * @param order */
	public void resetMergedQuanity(Order order) {
		if(order == null)return;
		List<DigitalCommerceItem> items = order.getCommerceItems();
		for (DigitalCommerceItem item : items) {
			item.setMergedQuantity(-1);
		}
	}

}
