
package com.digital.commerce.services.order.history.domain;

import javax.annotation.Generated;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
@Getter
@Setter
@ToString
public class CommerceItem {

    private String status;
    private String lastModifiedTime;
    private String completedTime;
    private String trackingNumber;
    private String shipToPersonFirstName;
    private String shipToPersonLastName;
    private String productDisplayName;
    private Integer skuStock;
    private String brandName;
    private String shipType;
    private String isSkuActive;
    private Integer msrp;
    private String storeName;
    private String isProductActive;
    private Integer quantity;
    private Integer returnedQuantity;
    private Size size;
    private String productId;
    private Dimension dimension;
    private Color color;
    private PriceInfo priceInfo;
    private HeelHeight heelHeight;
    private Integer productStock;
    private String catalogRefId;
    private String rewardCertificateShare;
    private Integer giftCardNumber;
    private Integer dollarValue;
    private String productTitle;

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(status).append(lastModifiedTime).append(completedTime).append(trackingNumber).append(shipToPersonFirstName).append(shipToPersonLastName).append(productDisplayName).append(skuStock).append(brandName).append(shipType).append(isSkuActive).append(msrp).append(storeName).append(isProductActive).append(quantity).append(returnedQuantity).append(size).append(productId).append(dimension).append(color).append(priceInfo).append(heelHeight).append(productStock).append(catalogRefId).append(rewardCertificateShare).append(giftCardNumber).append(dollarValue).append(productTitle).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CommerceItem) == false) {
            return false;
        }
        CommerceItem rhs = ((CommerceItem) other);
        return new EqualsBuilder().append(status, rhs.status).append(lastModifiedTime, rhs.lastModifiedTime).append(completedTime, rhs.completedTime).append(trackingNumber, rhs.trackingNumber).append(shipToPersonFirstName, rhs.shipToPersonFirstName).append(shipToPersonLastName, rhs.shipToPersonLastName).append(productDisplayName, rhs.productDisplayName).append(skuStock, rhs.skuStock).append(brandName, rhs.brandName).append(shipType, rhs.shipType).append(isSkuActive, rhs.isSkuActive).append(msrp, rhs.msrp).append(storeName, rhs.storeName).append(isProductActive, rhs.isProductActive).append(quantity, rhs.quantity).append(returnedQuantity, rhs.returnedQuantity).append(size, rhs.size).append(productId, rhs.productId).append(dimension, rhs.dimension).append(color, rhs.color).append(priceInfo, rhs.priceInfo).append(heelHeight, rhs.heelHeight).append(productStock, rhs.productStock).append(catalogRefId, rhs.catalogRefId).append(rewardCertificateShare, rhs.rewardCertificateShare).append(giftCardNumber, rhs.giftCardNumber).append(dollarValue, rhs.dollarValue).append(productTitle, rhs.productTitle).isEquals();
    }

}
