package com.digital.commerce.services.promotions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;

import atg.commerce.pricing.PricingModelHolder;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.service.collections.filter.CachedCollectionFilter;
import atg.service.collections.filter.FilterException;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalPromotionService extends ApplicationLoggingImpl {
	
	private static final String    PERFORM_MONITOR_NAME    = "DigitalPromotionService";

	/**
	 * property: allPromotions
	 */
	private Collection allPromotions;

	/**
	 * property: error
	 */
	private Hashtable<String, Object> error;

	/**
	 * property: pricingModelHolder
	 */
	private PricingModelHolder pricingModelHolder;
	

	public DigitalPromotionService() {
		super(DigitalPromotionService.class.getName());
	}
	
	private CachedCollectionFilter filter;

	/**
	 * This method will call existing initalizeAndFilterPromotions method.
	 * 
	 * @return
	 * @throws FilterException
	 */
	public Collection<?> findAllPromotions() throws FilterException{

		String METHOD_NAME = "service";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			
			if (this.allPromotions == null) {
			  this.allPromotions = new HashSet<RepositoryItem>();
			}
			this.allPromotions.clear();
			
			Collection<?> filteredPromotions = initalizeSiteGroupPromotions();
	
			if (!filteredPromotions.isEmpty()) {
				allPromotions.addAll(filteredPromotions);
			}
		
		}
		catch (Exception ex) {
			this.getJSONError(ex.getMessage());
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		
		return this.allPromotions;
	}

	/**
	 * Populates the mSiteGroupOffers Collection
	 * 
	 * @return
	 * @throws FilterException
	 */
	protected Collection<?> initalizeSiteGroupPromotions() throws FilterException {

		String METHOD_NAME = "initalizeSiteGroupPromotions";
		// Result collection
		Collection<?> filteredPromotions = new ArrayList<RepositoryItem>();
		
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			
			// All promotions
			Collection<?> unfilteredCollection = pricingModelHolder.getAllPromotions();
	
			// Check that there are promotions to filter
			if (unfilteredCollection == null || unfilteredCollection.size() == 0) {
				return filteredPromotions;
			}
	
			// Filter all promotions using filter parameter		
			filteredPromotions = getFilter().filterCollection(
						unfilteredCollection, null, null);			
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		return filteredPromotions;
	}

	/**
	 * 
	 * @param errorMesage
	 */
	private void getJSONError(String errorMesage) {
		error = new Hashtable<>();
		Hashtable<String, String> genError = new Hashtable<>();
		if (DigitalStringUtil.isNotBlank(errorMesage)) {
			genError.put("localizedMessage", errorMesage);
		} else {
			genError.put("localizedMessage",
					"Generic error while while lookuo of all available promotions");
		}
		genError.put("errorCode", "PROMOTIONS_LOOKUP_GENERIC_ERROR");
		ArrayList<Hashtable<String, String>> genErrArray = new ArrayList<>();
		genErrArray.add(genError);
		error.put("genericExceptions", genErrArray);
		error.put("formError", false);
	}

}
