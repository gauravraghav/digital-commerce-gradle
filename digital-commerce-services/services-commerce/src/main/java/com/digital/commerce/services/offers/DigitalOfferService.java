package com.digital.commerce.services.offers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.coupons.DigitalCouponService;
import com.digital.commerce.services.promotions.DigitalPromotionService;

import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.service.collections.filter.CachedCollectionFilter;
import atg.service.collections.filter.FilterException;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalOfferService extends ApplicationLoggingImpl{
	
	private static final String    PERFORM_MONITOR_NAME    = "DigitalOfferService";
	
	/**
	 * property: allOffers
	 */
	private Collection allOffers;

	/**
	 * property: mFilter
	 */
	private CachedCollectionFilter filter;
	
	/**
	 * property: mCouponService
	 */
	private DigitalCouponService couponService;
	
	/**
	 * property: mPromotionService
	 */
	private DigitalPromotionService promotionService;

	/**
	 * property: error
	 */
	private Hashtable<String, Object> error;

	public DigitalOfferService() {
		super(DigitalOfferService.class.getName());
	}

	/**
	 * This method will call existing initalizeAndFilterOffers method.
	 * 
	 * @return
	 * @throws FilterException
	 */
	public Collection<?> findAllOffers() throws FilterException{

		String METHOD_NAME = "findAllOffers";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			
			if (this.allOffers == null) {
			  this.allOffers = new HashSet<RepositoryItem>();
			}
			this.allOffers.clear();
			
			Collection<?> filteredOffers = initalizeSiteGroupOffers();
	
			if (!filteredOffers.isEmpty()) {
				this.allOffers.addAll(filteredOffers);
			}
		}
		catch (Exception ex) {
			this.getJSONError(ex.getMessage());
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		
		return this.allOffers;
	}

	/**
	 * Populates the mSiteGroupOffers Collection
	 * 
	 * @return
	 * @throws FilterException
	 */
	protected Collection<?> initalizeSiteGroupOffers() throws FilterException {

		String METHOD_NAME = "initalizeSiteGroupOffers";
		
		// Result collection
		Collection<?> filteredOffers = new ArrayList<RepositoryItem>();
		
		Collection unfilteredCollection = new ArrayList<RepositoryItem>();
		
		// Filter all Offers using filter parameter
		try {
			
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			
			// All Coupons
			unfilteredCollection.addAll(getCouponService().findAllCoupons(false));
			
			// All Global Promotions for Guest user & for logged in it includes all promotions associated with the profile
			unfilteredCollection.addAll(getPromotionService().findAllPromotions());			
	
			// Check that there are Offers to filter
			if (unfilteredCollection == null || unfilteredCollection.size() == 0) {
				return filteredOffers;
			}
		
			filteredOffers = getFilter().filterCollection(
					unfilteredCollection, null, null);
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		return filteredOffers;
	}

	/**
	 * 
	 * @param errorMesage
	 */
	private void getJSONError(String errorMesage) {
		error = new Hashtable<>();
		Hashtable<String, String> genError = new Hashtable<>();
		if (DigitalStringUtil.isNotBlank(errorMesage)) {
			genError.put("localizedMessage", errorMesage);
		} else {
			genError.put("localizedMessage",
					"Generic error while while lookuo of all available promotions");
		}
		genError.put("errorCode", "OFFERS_LOOKUP_GENERIC_ERROR");
		ArrayList<Hashtable<String, String>> genErrArray = new ArrayList<>();
		genErrArray.add(genError);
		error.put("genericExceptions", genErrArray);
		error.put("formError", false);
	}

}
