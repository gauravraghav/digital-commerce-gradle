package com.digital.commerce.services.order.purchase;
import static com.digital.commerce.common.services.DigitalBaseConstants.GIFT_CARD_APPROVAL_RESPONSE_CODE;
import static com.digital.commerce.common.services.DigitalBaseConstants.SVS_BUSINESS_ERROR_CODE;
import static com.digital.commerce.common.services.DigitalBaseConstants.SVS_COMMERCE_ERROR_CODE;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

import javax.servlet.ServletException;

import org.apache.commons.lang.math.NumberUtils;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.HttpServletUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.storedvalue.StoredValueService;
import com.digital.commerce.integration.storedvalue.domain.StoredValueServiceRequest;
import com.digital.commerce.integration.storedvalue.domain.StoredValueServiceResponse;
import com.digital.commerce.services.giftcard.domain.ApplyGiftCardResponse;
import com.digital.commerce.services.giftcard.domain.GiftCardBalanceInquiryResponse;
import com.digital.commerce.services.giftcard.domain.RemoveGiftCardResponse;
import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.giftcard.GiftCardPaymentServices;
import com.digital.commerce.services.order.payment.giftcard.valueobject.DigitalGiftCardApproval;
import com.digital.commerce.services.pricing.DigitalPricingTools;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroupManager;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.dtm.UserTransactionDemarcation;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.nucleus.naming.ComponentName;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class DigitalGCComponent extends ApplicationLoggingImpl {
	private static final String GIFT_CARDNUMBER_LENGTH_KEY = "cardNumberLength";
	private static final String GIFT_PINNUMBER_LENGTH_KEY = "pinNumberLength";
	private static final String ERR_INVALID_GIFTCARD_NUMBER = "SVS03";
	private static final String ERR_INVALID_GIFTCARD_PIN_NUMBER = "SVS20";
	private static final String ERR_VALIDATION_EXCEPTION = "003";

	private StoredValueService storedValueService;
	private Map<String,String> validationSettings = new HashMap<>();
	private static final String CLASSNAME = "DigitalGCController";
	private GiftCardPaymentServices paymentServices;
	private Order mOrder;
	private PaymentGroupManager paymentGroupManager;
	private MessageLocator messageLocator;
	private ComponentName mUserPricingModelsPath;
	private PricingTools pricingTools;
	
	public DigitalGCComponent() {
		super(DigitalGCComponent.class.getName());
	}
	
	public Map<String, String> getValidationSettings() {
		return validationSettings;
	}

	public void setValidationSettings(Map<String, String> validationSettings) {
		this.validationSettings = validationSettings;
	}

	public GiftCardBalanceInquiryResponse balanceInquiry(String currencyName, String cardNumber, String pin) {
		String METHOD_NAME = StoredValueService.ServiceMethod.BALANCE_INQUIRY.getServiceMethodName();
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( CLASSNAME, METHOD_NAME);
		
		GiftCardBalanceInquiryResponse giftCardBalanceInquiryResponse = new GiftCardBalanceInquiryResponse();

		try {
			boolean isValid = true;
			int cardNumberLength = Integer.valueOf(validationSettings.get(GIFT_CARDNUMBER_LENGTH_KEY));
			int pinNumberLength = Integer.valueOf(validationSettings.get(GIFT_PINNUMBER_LENGTH_KEY));
			
			if (DigitalStringUtil.length(cardNumber) != cardNumberLength || !NumberUtils.isDigits(cardNumber)) {
				giftCardBalanceInquiryResponse.getGenericExceptions().add(new ResponseError(ERR_INVALID_GIFTCARD_NUMBER, messageLocator.getMessageString(ERR_INVALID_GIFTCARD_NUMBER)));

				isValid = false;

			}

			if (DigitalStringUtil.length(pin) != pinNumberLength || !NumberUtils.isDigits(pin)) {
				giftCardBalanceInquiryResponse.getGenericExceptions().add(new ResponseError(ERR_INVALID_GIFTCARD_PIN_NUMBER, messageLocator.getMessageString(ERR_INVALID_GIFTCARD_PIN_NUMBER)));
				isValid = false;				
			}

			StoredValueServiceRequest storedValueServiceData = new StoredValueServiceRequest();
			storedValueServiceData.setCardNumber(cardNumber);
			storedValueServiceData.setPin(pin);
			storedValueServiceData.setCurrencyName(currencyName);
			storedValueServiceData.setAmount(0d);

			if (isValid) {
				StoredValueServiceResponse storedValueServiceResponse;
				try {
					storedValueServiceResponse = storedValueService.balanceInquiry(storedValueServiceData);
					if(!storedValueServiceResponse.isSuccess()) {
						giftCardBalanceInquiryResponse.getGenericExceptions()
								.add(new ResponseError("SVS" + storedValueServiceResponse.getReturnCode(),
										messageLocator.getMessageString("SVS"+ storedValueServiceResponse.getReturnCode())));
						giftCardBalanceInquiryResponse.setRequestSuccess(false);
						
					} else {
						giftCardBalanceInquiryResponse.setAmount(storedValueServiceResponse.getAmount());
						giftCardBalanceInquiryResponse.setAuthorizationCode(storedValueServiceResponse.getAuthorizationCode());
						giftCardBalanceInquiryResponse.setTransactionId(storedValueServiceResponse.getTransactionId());
					}
				} catch (DigitalIntegrationException e) {
					logError("Error while retrieving SVS balance inquiry", e);

					giftCardBalanceInquiryResponse.getGenericExceptions()
							.add(new ResponseError(e.getErrorCode(), messageLocator.getMessageString("SVS04")));
					giftCardBalanceInquiryResponse.setRequestSuccess(false);
				}

			} else {
				logWarning("balanceInquiry arguments failed data validation requirements");
				giftCardBalanceInquiryResponse.setRequestSuccess(false);
			}

			return giftCardBalanceInquiryResponse;

		} catch (Exception e) {
			logError("Error while validating SVS balance inquiry arguments", e);
			giftCardBalanceInquiryResponse.getGenericExceptions().add(new ResponseError(ERR_VALIDATION_EXCEPTION, "Error while validating SVS balance inquiry arguments"));
			
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return giftCardBalanceInquiryResponse;
		
	}

	public ApplyGiftCardResponse applyGiftCard(String currencyName, String cardNumber, String pin) {
		final String methodName = "applyGiftCard";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, methodName);
		
		DigitalGiftCardApproval approval = new DigitalGiftCardApproval();
		ApplyGiftCardResponse response = new ApplyGiftCardResponse();
		
		TransactionUtils.acquireTransactionLock( CLASSNAME, methodName);
		UserTransactionDemarcation td = TransactionUtils.startNewTransaction(CLASSNAME, methodName);
		try {

			Order order = getOrder();
			validateGiftCard(cardNumber,pin,"apply");
			
			synchronized(order){
				checkOrderEligibilityForGiftCard(order);
				((DigitalPaymentGroupManager)getPaymentGroupManager()).removeGiftCardFromOrder(order, cardNumber, true);
				
				try{
					((DigitalPricingTools) getPricingTools()).priceOrderTotal(order,
							getPricingModelHolder(ServletUtil.getCurrentRequest()),
							HttpServletUtil.getUserLocale(ServletUtil.getCurrentRequest()), getProfile(), null);
				}catch (PricingException | IOException | ServletException exc) {
					logError(exc);
				}
				
				try {
					approval = getPaymentServices().getGiftCardBalance(cardNumber, pin, currencyName);
				} catch (DigitalIntegrationException e) {
					logError("Error while retrieving SVS balance inquiry", e);
					response.getGenericExceptions().add(new ResponseError(e.getErrorCode(), e.getMessage()));
					response.setRequestSuccess(false);
					return response;
				}
				
				if(DigitalStringUtil.isNotBlank(approval.getResponseCode()) && GIFT_CARD_APPROVAL_RESPONSE_CODE == Integer.parseInt(approval.getResponseCode()) ){
					DigitalPaymentGroupManager pmg = (DigitalPaymentGroupManager) getPaymentGroupManager();
					DigitalGiftCard giftCardPaymentGroup = pmg.addGiftCardPaymentGroup(order, cardNumber, pin, approval);
					if (null != giftCardPaymentGroup) {
						getPaymentGroupManager().recalculatePaymentGroupAmounts(order);
						//update first/last name of shipping group for bopis/bosts only orders from alternate pick-up
						this.getShippingGroupManager().updateShipAddrNameForBopisBostsOrder(order);
						
						orderManager.updateOrder(shoppingCart.getCurrent());
					
					response.setGiftCardPaymentGroup(giftCardPaymentGroup);
					response.setAmountRemaining(DigitalCommonUtil.round(approval.getApprovalAmount()-giftCardPaymentGroup.getAmount()));
					response.setGiftCardNumber(giftCardPaymentGroup.getCardNumber());
					response.setPinNumber(giftCardPaymentGroup.getPinNumber());
					response.setGcStatus(getMessageLocator().getMessageString("SVS"+approval.getResponseCode()));
					}
				} else {
					response.getGenericExceptions().add(new ResponseError(approval.getResponseCode(), getMessageLocator().getMessageString("SVS"+approval.getResponseCode())));
				}
			}
		} catch (DigitalAppException e) {
			logError(e.getMessage());
			TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
			String msg = getMessageLocator().getMessageString(e.getMessage());
			if(!DigitalStringUtil.isBlank(msg)){
				response.getGenericExceptions().add(new ResponseError(SVS_BUSINESS_ERROR_CODE, msg));
			}else{
				response.getGenericExceptions().add( new ResponseError(SVS_BUSINESS_ERROR_CODE, e.getMessage()));
			}
		} catch (RepositoryException | CommerceException  e) {
			logError(e.getMessage());
			TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
			String msg = getMessageLocator().getMessageString("errorAddingGiftCard");
			response.getGenericExceptions().add(new ResponseError(SVS_COMMERCE_ERROR_CODE, msg));
		} finally {
			if(td != null){
				TransactionUtils.endTransaction(td, CLASSNAME, methodName);
			}
			TransactionUtils.releaseTransactionLock(CLASSNAME, methodName);
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, methodName);
		}
		return response;
	}

	public RemoveGiftCardResponse removeGiftCard(String cardNumber) {
		final String methodName = "removeGiftCard";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, methodName);
		
		RemoveGiftCardResponse response = new RemoveGiftCardResponse();
		TransactionUtils.acquireTransactionLock(CLASSNAME, methodName);
		UserTransactionDemarcation td = TransactionUtils.startNewTransaction(CLASSNAME, methodName);
		
		try {
			Order order = getOrder();			
			
			synchronized(order){
				validateGiftCard(cardNumber,null,"remove");
				((DigitalPaymentGroupManager)getPaymentGroupManager()).removeGiftCardFromOrder(order, cardNumber, false);
				getPaymentGroupManager().recalculatePaymentGroupAmounts(order);
				orderManager.updateOrder(order);
				response.setGcStatus("Removed");
			}

			((DigitalPaymentGroupManager) getPaymentGroupManager()).reapplyGiftCard(order);
			
		} catch (DigitalAppException e) {
			logError(e.getMessage());
			TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
			String msg = getMessageLocator().getMessageString(e.getMessage());
			if(!DigitalStringUtil.isBlank(msg)){
				response.getGenericExceptions().add(new ResponseError(SVS_BUSINESS_ERROR_CODE, msg));
			}else{
				response.getGenericExceptions().add(new ResponseError(SVS_BUSINESS_ERROR_CODE, e.getMessage()));
			}
		} catch (RepositoryException | CommerceException e) {
			TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
			String msg = getMessageLocator().getMessageString("errorRemovingGiftCard");
			response.getGenericExceptions().add(new ResponseError(SVS_COMMERCE_ERROR_CODE, msg + e.getMessage()));
			logError(e.getMessage());
		} finally {
			TransactionUtils.endTransaction(td, CLASSNAME, methodName);
			TransactionUtils.releaseTransactionLock(CLASSNAME, methodName);
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( CLASSNAME, methodName);
		}
		return response;
	}

	private OrderManager orderManager;

	public Order getOrder() {
		if (mOrder != null) {
			return mOrder;
		}
		return getShoppingCart().getCurrent();
	}

	private OrderHolder shoppingCart;

	private void validateGiftCard(String cardNumber, String pin, String operation) throws DigitalAppException {
		int cardNumberLength = Integer.valueOf(validationSettings.get(GIFT_CARDNUMBER_LENGTH_KEY));
		int pinNumberLength = Integer.valueOf(validationSettings.get(GIFT_PINNUMBER_LENGTH_KEY));
		if (DigitalStringUtil.isBlank(cardNumber)) {
			throw new DigitalAppException("missingGiftCardNumber");
		}
		if ("apply".equalsIgnoreCase(operation) && DigitalStringUtil.isBlank(pin)) {
			throw new DigitalAppException("missingGiftCardPin");
		}
		if (DigitalStringUtil.length(cardNumber) != cardNumberLength || !NumberUtils.isDigits(cardNumber)) {
			throw new DigitalAppException("invalidGiftCardNumber");
		}
		if ("apply".equalsIgnoreCase(operation) && (DigitalStringUtil.length(pin) != pinNumberLength || !NumberUtils.isDigits(pin))) {
			throw new DigitalAppException("invalidGiftCardPin");
		}
	}

	private void checkOrderEligibilityForGiftCard(Order order) throws DigitalAppException{
		double amountRemaining = 0.0;
		DigitalPaymentGroupManager pmg = (DigitalPaymentGroupManager) getPaymentGroupManager();
		if(order.getPriceInfo() != null) {
			amountRemaining = pmg.getAmountRemaining( order, order.getPriceInfo().getTotal() );
			if(amountRemaining <= 0){
				throw new DigitalAppException("giftcard.nothing.toapply");
			}
		}
		if(((DigitalOrderImpl)getOrder()).isTaxOffline()){
			throw new DigitalAppException("taxoffline.giftcard.payment.notapplicable");
		}
	}

	public void setUserPricingModelsPath(String pUserPricingModelsPath) {
		if (pUserPricingModelsPath != null) {
			mUserPricingModelsPath = ComponentName
					.getComponentName(pUserPricingModelsPath);
		} else {
			mUserPricingModelsPath = null;
		}
	}

	public String getUserPricingModelsPath() {
		if (mUserPricingModelsPath != null) {
			return mUserPricingModelsPath.getName();
		}
		return null;
	}

	private PricingModelHolder getPricingModelHolder(DynamoHttpServletRequest pRequest)
			throws ServletException, IOException {
		String perfName = "getPricingModelHolder";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
				"DigitalGCComponent", perfName);
		PricingModelHolder holder = null;
		try {
			Collection models = (Collection) pRequest
					.getObjectParameter(ParameterName
							.getParameterName("pricingModels"));
			if ((models == null) && (mUserPricingModelsPath != null)) {
				holder = (PricingModelHolder) pRequest
						.resolveName(mUserPricingModelsPath);
				if (holder != null)
					models = holder.getShippingPricingModels();
			}
			return holder;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
					"DigitalGCComponent", perfName);
		}
	}
	
	private RepositoryItem getProfile() throws ServletException, IOException {
		final String METHOD_NAME = "getProfile";
		DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
				"DigitalGCComponent", METHOD_NAME);
		try {
			RepositoryItem profile = null;
			profile = (RepositoryItem) ServletUtil.getCurrentUserProfile();
			return profile;

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
					"DigitalGCComponent", METHOD_NAME);
		}
	}

   public DigitalShippingGroupManager getShippingGroupManager(){
		 return (DigitalShippingGroupManager) this.getOrderManager().getShippingGroupManager();
   }
}
