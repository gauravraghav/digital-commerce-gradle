/**
 * 
 */
package com.digital.commerce.services.order;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalCollectionUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.DigitalCommerceItemWrapper;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShipType;
import com.digital.commerce.services.utils.DigitalServiceConstants;

import atg.commerce.order.CommerceItemImpl;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupRelationship;
import atg.commerce.pricing.DetailedItemPriceInfo;
import atg.repository.RemovedItemException;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalCommerceItem extends CommerceItemImpl implements DigitalCommerceItemWrapper {

	private boolean excludedFromPromotion = false;
	/**
	 * 
	 */
	private static final long serialVersionUID = 3620049345773906357L;

	static final String PRICE_CHANGED_PROPERTY = "priceChanged";

	static final String PREV_PRICE_PROPERTY = "prevPrice";

	static final String CATEGORY_ID_PROPERTY = "categoryId";

	static final String CATEGORY_NAME_PROPERTY = "categoryName";

	static final String PRODUCT_REF_ROOT_PROPERTY = "productRefRoot";

	static final String REWARDS_CERT_SHARE = "rewardCertificateShare";

	static final String ORDER_PROMO_SHARE = "orderPromoShare";

	static final String PRODUCT_TITLE = "productTitle";

	static final String BUCKETS = "buckets";

	static final String ORIGIN_OF_COMMERCE_ITEM = "originOfCommerceItem";

	static final String STORE_HUB_QTY = "storeHubQty";

	static final String SHIP_NODE = "shipNode";

	static final String PROCURE_FROM_NODE = "procureFromNode";

	static final String SHIP_TYPE = "shipType";

	static final String STORE_ID = "storeId";
	
	static final String LOCATION_ID = "locationId";

	static final String ITEM_RESERVED = "itemReserved";
	static final String ITEM_QUANTITY_RESERVED = "itemQuantityReserved";
	static final String INVENTORY_EVALUATION_RESULT = "inventoryEvaluationResult";
	static final String STORE_PICK_AVAILABLE = "storePickAvailable";
	static final String SHIP_TO_STORE_AVAILABLE = "shipToStoreAvailable";

	static final String GIFT_RECEIPT = "giftReceipt";

	static final String GIFT_RECEIPT_MESSAGE = "giftReceiptMessage";

	static final String CART_MERGED_QUANTITY = "mergedQuantity";

	static final String STORE_STOCK = "storeStock";

	static final String MALL_PLAZA_NAME = "mallPlazaName";

	static final String GROUND_SHIPPING = "groundShippingOnly";

	static final String BOPIS_RESERVATION = "bopisReservation";

	static final String REWARD_CERTIFICATE_SHARE = "rewardCertificateShare";

	static final String SKU_QTY_LIMIT_EXCEEDEDMSG = "skuQtyLimitExceededMsg";

	public boolean getGroundShippingOnly() {
		return (getPropertyValue(DigitalCommerceItem.GROUND_SHIPPING) != null)
				? (boolean) getPropertyValue(DigitalCommerceItem.GROUND_SHIPPING) : false;
	}

	public void setGroundShippingOnly(boolean groundShippingOnly) {
		setPropertyValue(DigitalCommerceItem.GROUND_SHIPPING, groundShippingOnly);
	}

	private OrderReservationMsg reservationEvaluation;

	public String getOriginOfCommerceItem() {
		return (String) getPropertyValue(DigitalCommerceItem.ORIGIN_OF_COMMERCE_ITEM);
	}

	public void setOriginOfCommerceItem(String originOfCommerceItem) {
		setPropertyValue(DigitalCommerceItem.ORIGIN_OF_COMMERCE_ITEM, originOfCommerceItem);
	}

	public void setBuckets(List<String> buckets) {
		setPropertyValue(BUCKETS, buckets);
	}

	public List<String> getBuckets() {
		return (List<String>) getPropertyValue(BUCKETS);
	}

	public void setPrevPrice(Double prevPrice) {
		setPropertyValue(PREV_PRICE_PROPERTY, prevPrice);
	}

	public Double getPrevPrice() {
		return (Double) getPropertyValue(PREV_PRICE_PROPERTY);
	}

	public void setPriceChanged(Boolean priceChanged) {
		setPropertyValue(PRICE_CHANGED_PROPERTY, priceChanged);
	}

	public Boolean getPriceChanged() {
		return (Boolean) getPropertyValue(PRICE_CHANGED_PROPERTY);
	}

	public void setRewardCertificateShare(Map<String, Double> certMap) {
		setPropertyValue(REWARDS_CERT_SHARE, certMap);
	}

	public Map<String, Double> getRewardsCertificateShare() {
		return (Map<String, Double>) getPropertyValue(REWARDS_CERT_SHARE);
	}

	public void setOrderPromoShare(Map<String, Double> promoMap) {
		setPropertyValue(ORDER_PROMO_SHARE, promoMap);
	}

	public Map<String, Double> getOrderPromoShare() {
		return (Map<String, Double>) getPropertyValue(ORDER_PROMO_SHARE);
	}

	/** @return Returns the excludedFromPromotion. */
	public boolean isExcludedFromPromotion() {
		return excludedFromPromotion;
	}

	/**
	 * @param excludedFromPromotion
	 *            The excludedFromPromotion to set.
	 */
	public void setExcludedFromPromotion(boolean excludedFromPromotion) {
		this.excludedFromPromotion = excludedFromPromotion;
	}

	/* Commerce Item extension properties... */
	public String getCategoryId() {
		return (String) getPropertyValue(DigitalCommerceItem.CATEGORY_ID_PROPERTY);
	}

	public void setCategoryId(String categoryId) {
		setPropertyValue(DigitalCommerceItem.CATEGORY_ID_PROPERTY, categoryId);
	}

	public String getCategoryName() {
		return (String) getPropertyValue(DigitalCommerceItem.CATEGORY_NAME_PROPERTY);
	}

	public void setCategoryName(String categoryName) {
		setPropertyValue(DigitalCommerceItem.CATEGORY_NAME_PROPERTY, categoryName);
	}

	public String getProductRefRoot() {
		return (String) getPropertyValue(DigitalCommerceItem.PRODUCT_REF_ROOT_PROPERTY);
	}

	public void setProductRefRoot(String productRefRoot) {
		setPropertyValue(DigitalCommerceItem.PRODUCT_REF_ROOT_PROPERTY, productRefRoot);
	}

	public String getProductTitle() {
		return (String) getPropertyValue(DigitalCommerceItem.PRODUCT_TITLE);
	}

	public void setProductTitle(String productTitle) {
		setPropertyValue(DigitalCommerceItem.PRODUCT_TITLE, productTitle);
	}

	public void setStoreHubQty(Integer storeHubQty) {
		setPropertyValue(DigitalCommerceItem.STORE_HUB_QTY, storeHubQty);
	}

	public Integer getStoreHubQty() {
		return (Integer) getPropertyValue(DigitalCommerceItem.STORE_HUB_QTY);
	}

	public boolean isItemReserved() {
		return (Boolean) getPropertyValue(DigitalCommerceItem.ITEM_RESERVED);
	}

	public void setItemReserved(boolean itemReserved) {
		setPropertyValue(DigitalCommerceItem.ITEM_RESERVED, itemReserved);
	}

	public boolean isStorePickAvailable() {
		return (Boolean) getPropertyValue(DigitalCommerceItem.STORE_PICK_AVAILABLE);
	}

	public void setStorePickAvailable(boolean para) {
		setPropertyValue(DigitalCommerceItem.STORE_PICK_AVAILABLE, para);
	}

	public boolean isShipToStoreAvailable() {
		return (Boolean) getPropertyValue(DigitalCommerceItem.SHIP_TO_STORE_AVAILABLE);
	}

	public void setShipToStoreAvailable(boolean para) {
		setPropertyValue(DigitalCommerceItem.SHIP_TO_STORE_AVAILABLE, para);
	}

	public int getInventoryEvaluationResult() {
		return (Integer) getPropertyValue(DigitalCommerceItem.INVENTORY_EVALUATION_RESULT);
	}

	public void setInventoryEvaluationResult(int inventoryEvaluationResult) {
		setPropertyValue(DigitalCommerceItem.INVENTORY_EVALUATION_RESULT, inventoryEvaluationResult);
	}

	public double getItemQuantityReserved() {
		return (Double) getPropertyValue(DigitalCommerceItem.ITEM_QUANTITY_RESERVED);
	}

	public void setItemQuantityReserved(double itemQuantityReserved) {
		setPropertyValue(DigitalCommerceItem.ITEM_QUANTITY_RESERVED, itemQuantityReserved);
	}

	public String getShipNode() {
		return (String) getPropertyValue(DigitalCommerceItem.SHIP_NODE);
	}

	public void setShipNode(String shipNode) {
		setPropertyValue(DigitalCommerceItem.SHIP_NODE, shipNode);
	}

	public String getProcureFromNode() {
		return (String) getPropertyValue(DigitalCommerceItem.PROCURE_FROM_NODE);
	}

	public void setProcureFromNode(String procureFromNode) {
		setPropertyValue(DigitalCommerceItem.PROCURE_FROM_NODE, procureFromNode);
	}

	public String getShipType() {
		String shiptype = (String) getPropertyValue(DigitalCommerceItem.SHIP_TYPE);
		if (DigitalStringUtil.isEmpty(shiptype)) {
			shiptype = ShippingGroupConstants.ShipType.SHIP.getValue();
		}
		return shiptype;
	}

	public void setShipType(String shipType) {
		setPropertyValue(DigitalCommerceItem.SHIP_TYPE, shipType);
	}

	public String getStoreId() {
		return (String) getPropertyValue(DigitalCommerceItem.STORE_ID);
	}

	public void setStoreId(String storeId) {
		setPropertyValue(DigitalCommerceItem.STORE_ID, storeId);
	}
	
	public String getLocationId() {
		return (String) getPropertyValue(DigitalCommerceItem.LOCATION_ID);
	}

	public void setLocationId(String locationId) {
		setPropertyValue(DigitalCommerceItem.LOCATION_ID, locationId);
	}


	public double getCumulativeOrderPromoShare() {
		return getCumulativeAmount(getOrderPromoShare().values());
	}

	public double getCumulativeRewardsCertificateShare() {
		return getCumulativeAmount(getRewardsCertificateShare().values());
	}

	public void setGiftReceipt(boolean giftReceipt) {
		setPropertyValue(GIFT_RECEIPT, giftReceipt);
	}

	public boolean getGiftReceipt() {
		return (boolean) getPropertyValue(GIFT_RECEIPT);
	}

	public String getGiftReceiptMessage() {
		return (String) getPropertyValue(DigitalCommerceItem.GIFT_RECEIPT_MESSAGE);
	}

	public void setGiftReceiptMessage(String giftReceiptMessage) {
		setPropertyValue(DigitalCommerceItem.GIFT_RECEIPT_MESSAGE, giftReceiptMessage);
	}

	public long getMergedQuantity() {
		return (Long) getPropertyValue(DigitalCommerceItem.CART_MERGED_QUANTITY);
	}

	public void setMergedQuantity(long itemReserved) {
		setPropertyValue(DigitalCommerceItem.CART_MERGED_QUANTITY, itemReserved);
	}

	public long getStoreStock() {
		return (long) getPropertyValue(DigitalCommerceItem.STORE_STOCK);
	}

	public void setStoreStock(long storeStock) {
		setPropertyValue(DigitalCommerceItem.STORE_STOCK, storeStock);
	}

	public String getMallPlazaName() {
		if (ShipType.BOPIS.getValue().equalsIgnoreCase(this.getShipType())
				|| ShipType.BOSTS.getValue().equalsIgnoreCase(this.getShipType())) {
			if (this.getShippingGroupRelationshipCount() > 0) {
				List<ShippingGroupRelationship> sgrs = getShippingGroupRelationships();
				ShippingGroupRelationship sgr = sgrs.get(0);
				ShippingGroup sg = sgr.getShippingGroup();
				if (sg instanceof DigitalHardgoodShippingGroup) {
					DigitalHardgoodShippingGroup dswsg = (DigitalHardgoodShippingGroup) sg;
					return dswsg.getMallPlazaName();
				}
			}
		}
		return null;
	}

	public void setMallPlazaName(String mallPlazaName) {
		setPropertyValue(DigitalCommerceItem.MALL_PLAZA_NAME, mallPlazaName);
	}

	public String getBopisReservation() {
		return (getPropertyValue(DigitalCommerceItem.BOPIS_RESERVATION) != null)
				? (String) getPropertyValue(DigitalCommerceItem.BOPIS_RESERVATION) : "";
	}

	public Map getRewardCertificateShare() {
		return (Map) getPropertyValue(DigitalCommerceItem.REWARD_CERTIFICATE_SHARE);
	}

	private double getCumulativeAmount(Collection<Double> coll) {
		double cumulativeAmount = 0;
		if (!DigitalCollectionUtil.isEmpty(coll)) {
			Iterator<Double> i = coll.iterator();
			while (i.hasNext()) {
				Double val = i.next();
				double d = val.doubleValue();
				cumulativeAmount += d;
			}
		}
		return cumulativeAmount;
	}

	/**
	 * @param Item
	 * @return
	 */
	public Double getLowestPriceForCommerceItem() {

		Double lowestPrice = Double.valueOf(0);
		if (this.getPriceInfo() != null) {
			Collection<DetailedItemPriceInfo> c = this.getPriceInfo().getCurrentPriceDetails();
			if (c != null && c.size() > 0) {
				Iterator<DetailedItemPriceInfo> iterator = c.iterator();
				DetailedItemPriceInfo priceInfo = iterator.next();
				if (priceInfo.getItemPriceInfo().getSalePrice() > priceInfo.getItemPriceInfo().getListPrice()) {
					lowestPrice = priceInfo.getItemPriceInfo().getSalePrice();
				} else {
					lowestPrice = priceInfo.getItemPriceInfo().getListPrice();
				}
			}
		}

		return lowestPrice;
	}

	/***  No lombok ***/
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString());
		try {
			sb.append("<--DigitalCommerceItem[");

			String categoryId = getCategoryId();
			sb.append("categoryId:").append(categoryId).append("; ");
			String categoryName = getCategoryName();
			sb.append("categoryName:").append(categoryName).append("; ");
			Map<String, Double> orderPromoShare = getOrderPromoShare();
			sb.append("orderPromoShare:").append(orderPromoShare).append("; ");
			Double prevPrice = getPrevPrice();
			sb.append("prevPrice:").append(prevPrice).append("; ");
			boolean priceChanged = getPriceChanged().booleanValue();
			sb.append("priceChanged:").append(priceChanged).append("; ");
			String productRefRoot = getProductRefRoot();
			sb.append("productRefRoot:").append(productRefRoot).append("; ");
			String productTitle = getProductTitle();
			sb.append("productTitle:").append(productTitle).append("; ");
			DigitalServiceConstants dswConstants = ComponentLookupUtil
					.lookupComponent("/com/dsw/commerce/common/service/DigitalConstants", DigitalServiceConstants.class);
			if (dswConstants.isChargeSendEnable()) {
				String shipNode = getShipNode();
				sb.append("shipNode:").append(shipNode).append("; ");
				boolean itemReserved = isItemReserved();
				sb.append("itemReserved:").append(itemReserved).append("; ");
				Integer storeHubQty = getStoreHubQty();
				sb.append("storeHubQty:").append(storeHubQty).append("; ");
			}
			Map<String, Double> rewardsCertificateShare = getRewardsCertificateShare();
			sb.append("rewardsCertificateShare:").append(rewardsCertificateShare);
		} catch (RemovedItemException exc) {
			sb.append("removed");
		}

		sb.append("]");
		return sb.toString();
	}

	public String getSkuQtyLimitExceededMsg() {
		return (String) getPropertyValue(DigitalCommerceItem.SKU_QTY_LIMIT_EXCEEDEDMSG);
	}

	public void setSkuQtyLimitExceededMsg(String msg) {
		setPropertyValue(DigitalCommerceItem.SKU_QTY_LIMIT_EXCEEDEDMSG, msg);
	}

}
