/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import java.util.List;

import com.digital.commerce.common.domain.ResponseWrapper;
import com.digital.commerce.integration.reward.domain.Certificate;
import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RewardsRetrieveRewardCertificatesResponse extends ResponseWrapper {
	private List<Certificate> certificates;
	private double totalCertValue;
	private Boolean pendingCertificate = false;
	private boolean isValidLoyaltyMember = true;
}
