/**
 *
 */
package com.digital.commerce.repository;

import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

/** @author psinha */
public class KidsProductPropertyDescriptor extends RepositoryPropertyDescriptor {
	private static final long		serialVersionUID	= -6258342475415966032L;

	protected static final String	TYPE_NAME			= "KidsProductPropertyDescriptor";

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, KidsProductPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		if( pValue != null && pValue instanceof Boolean ) { return pValue; }
		String productType = (String)pItem.getPropertyValue( "productTypeInUse" );
		String productGender = (String)pItem.getPropertyValue( "productGender" );
		if( ProductType.KidsShoe.getValue().equalsIgnoreCase( productType ) || ProductGender.Kid.name().equalsIgnoreCase( productGender ) || ProductGender.Girl.name().equalsIgnoreCase( productGender )
				|| ProductGender.Boy.name().equalsIgnoreCase( productGender ) ) {
			setKidsProductProperty( pItem, Boolean.TRUE );
			return Boolean.TRUE;
		} else {
			setKidsProductProperty( pItem, Boolean.FALSE );
			return Boolean.FALSE;
		}
	}

	private void setKidsProductProperty( RepositoryItemImpl item, Boolean flag ) {
		item.setPropertyValueInCache( this, flag );
	}
}
