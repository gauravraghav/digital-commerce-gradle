/**
*
*/
package com.digital.commerce.repository;

import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

/** @author psinha */
public class ProductTypeInUsePropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
	 *
	 */
	private static final long		serialVersionUID	= -6258342475415966132L;

	protected static final String	TYPE_NAME			= "ProductTypeInUsePropertyDescriptor";

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, ProductTypeInUsePropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		if( pValue != null && pValue instanceof String ) { return pValue; }

		String productType = (String)pItem.getPropertyValue( "productType" );

		String productTypeWeb = (String)pItem.getPropertyValue( "productTypeWeb" );

		String pType = "shoe";

		if( productTypeWeb != null ) {
			pType = productTypeWeb;
		} else if( productType != null ) {
			pType = productType;
		}

		setProductTypeProperty( pItem, pType );

		return pType;
	}

	private void setProductTypeProperty( RepositoryItemImpl item, String pType ) {
		item.setPropertyValueInCache( this, pType );
	}
}
