package com.digital.commerce.services.pricing;

import java.text.MessageFormat;
import java.util.*;

import  com.digital.commerce.services.order.DigitalCommerceItem;
import  com.digital.commerce.services.order.DigitalOrderImpl;
import  com.digital.commerce.services.order.DigitalOrderPriceInfo;

import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.RelationshipTypes;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingTools;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import lombok.Getter;
import lombok.Setter;

/** @author shirley.chen
 * 
 *         TODO To change the template for this generated type comment go to Window -
 *         Preferences - Java - Code Style - Code Templates */
@SuppressWarnings({"rawtypes","unchecked","unused"})
@Getter
@Setter
public class DigitalPromotionExlcusionService extends GenericService {

	private OrderManager	orderManager;

	private Repository		pricingModelRepository;

	private PricingTools	pricingTools;

	final static String		EXCLUDE_PRODS			= "excludeProducts";

	final static String		EXCLUDE_BRANDS			= "excludeBrands";

	final static String		EXCLUDE_PRODTYPES		= "excludeProdTypes";

	final static String		EXCLUDE_CLEARANCE_ITEM	= "excludeClearanceItem";

	final static String		EXCLUDES	= "excludes";
	
	private static final String DISPLAY_NAME_PROPERTY="displayName";


	private Set<String> promoExcludesInfo ;
	
	public Set<String> getPromoExcludesInfo() {
		if (promoExcludesInfo==null || promoExcludesInfo.size()==0){
			promoExcludesInfo = new HashSet<String>();			
		}
		return promoExcludesInfo;
	}

	private boolean	filterOnExclusiveList	= false;

	/** @return Returns the filterOnExclusiveList. */
	public boolean isFilterOnExclusiveList( RepositoryItem pPricingModel ) {
		setFilterOnExclusiveList( true );
		return filterOnExclusiveList;
	}

	/** evaluate the promotion exclusion rule and calcualte the qualifiedAmount
	 * for each orderPriceInfo object, that include
	 * orderPriceInfo.getShippingItemsSubtotalPriceInfos(),
	 * orderPriceInfo.getNonTaxableShippingItemsSubtotalPriceInfos() and
	 * orderPriceInfo.getTaxableShippingItemsSubtotalPriceInfos
	 * 
	 * @param pPricingModel
	 * @param pProfile
	 * @param pLocale
	 * @param pOrder
	 * @param pExtraParameters */

	public void evaluatePromoExclusion( RepositoryItem pPricingModel, RepositoryItem pProfile, Locale pLocale, Order pOrder, OrderPriceInfo pOrderPriceInfo, Map pExtraParameters ) throws PricingException {

		if( isLoggingDebug() ) {
			logDebug( "Start evaluate promotion exclusion for " + pPricingModel.getRepositoryId() );
		}

		DigitalOrderImpl dOrder = (DigitalOrderImpl)pOrder;
		DigitalOrderPriceInfo dPriceInfo = (DigitalOrderPriceInfo)pOrderPriceInfo;

		// if not eligible for promotion exclusive rule, reset the amount to
		// original
		boolean noExclusionDefined = false;
		Set excludedSet = null;

		if( !isFilterOnExclusiveList( pPricingModel ) ) {
			if( isLoggingDebug() ) {
				logDebug( "PricingModel " + pPricingModel.getRepositoryId() + " not a promotion exclusion type" );
			}
			noExclusionDefined = true;
		} else {
			// otherwise, we continue to generate the exclusive list and the
			// calculation for qualified amount
			// excludedSet = generateFilterList( pPricingModel );
			excludedSet = generateFilterSet( pPricingModel, pOrder );

			if( excludedSet == null || excludedSet.size() == 0 ) {
				if( isLoggingDebug() ) {
					logDebug( "PricingModel " + pPricingModel.getRepositoryId() + " promo exclusion list is empty" );
				}
				noExclusionDefined = true;
			}
		}

		if( dPriceInfo != null ) {
			if( noExclusionDefined ) {

				dPriceInfo.setQualifiedAmount( dPriceInfo.getAmount() );
				dPriceInfo.setUnqualifiedAmount( 0.0 );
				// this is for the pmdl rule for targetting
				dOrder.setQualifiedOrderAmount( dPriceInfo.getAmount() );

				// reset the shipping group qualified pricing
				if( dPriceInfo.getShippingItemsSubtotalPriceInfos() != null && !dPriceInfo.getShippingItemsSubtotalPriceInfos().isEmpty() ) {

					Set taxableEntries = dPriceInfo.getTaxableShippingItemsSubtotalPriceInfos().entrySet();
					Set nonTaxableEntries = dPriceInfo.getNonTaxableShippingItemsSubtotalPriceInfos().entrySet();
					Set entries = dPriceInfo.getShippingItemsSubtotalPriceInfos().entrySet();

					Iterator taxableEntriesIterator = taxableEntries.iterator();
					Iterator nonTaxableEntriesIterator = nonTaxableEntries.iterator();
					Iterator entriesIterator = entries.iterator();

					while( entriesIterator.hasNext() ) {
						Map.Entry entry = (Map.Entry)entriesIterator.next();
						Map.Entry taxableEntry = (Map.Entry)taxableEntriesIterator.next();
						Map.Entry nonTaxableEntry = (Map.Entry)nonTaxableEntriesIterator.next();

						DigitalOrderPriceInfo taxableShippingGroupSubtotalInfo = (DigitalOrderPriceInfo)taxableEntry.getValue();
						taxableShippingGroupSubtotalInfo.setQualifiedAmount( taxableShippingGroupSubtotalInfo.getAmount() );
						taxableShippingGroupSubtotalInfo.setUnqualifiedAmount( 0.0 );

						DigitalOrderPriceInfo nonTaxableTotalInfo = (DigitalOrderPriceInfo)nonTaxableEntry.getValue();
						nonTaxableTotalInfo.setQualifiedAmount( nonTaxableTotalInfo.getAmount() );
						nonTaxableTotalInfo.setUnqualifiedAmount( 0.0 );

						DigitalOrderPriceInfo entryTotalInfo = (DigitalOrderPriceInfo)entry.getValue();

						entryTotalInfo.setQualifiedAmount( entryTotalInfo.getAmount() );
						entryTotalInfo.setUnqualifiedAmount( 0.0 );
					}
				}
				return;
			}

			// start with orderPriceInfo
			double qualifiedAmount = calculateQualifedPriceFromOrderItems( excludedSet, pOrder );
			dPriceInfo.setQualifiedAmount( qualifiedAmount );
			dPriceInfo.setUnqualifiedAmount( dPriceInfo.getAmount() - qualifiedAmount );
			// this is for the pmdl rule for targetting
			dOrder.setQualifiedOrderAmount( qualifiedAmount );

			// set the shippingGroupPriceInfos
			if( dPriceInfo.getShippingItemsSubtotalPriceInfos() != null && !dPriceInfo.getShippingItemsSubtotalPriceInfos().isEmpty() ) {
				List shippingGroups = pOrder.getShippingGroups();
				Iterator groupIterator = shippingGroups.iterator();
				while( groupIterator.hasNext() ) {
					ShippingGroup group = (ShippingGroup)groupIterator.next();
					calculateShippingGroupQualifiedAmount( group, dPriceInfo, pOrder, pLocale, pProfile, pExtraParameters );
				} // end for each group
			}
		} else {
			logWarning( String.format( "PriceInfo is null for order: %1$s, profile: %2$s", pOrder.getId(), pOrder.getProfileId() ) );
		}
	}

	/** populate the productExclude filter list based on pricing model setting
	 * 
	 * @param pPricingModel
	 * @return */
	public Set generateFilterList( RepositoryItem pPricingModel ) {

		HashSet addedResult = new HashSet();
		RepositoryItem[] result = null;

		String query = (String)pPricingModel.getPropertyValue( "excludeQuery" );

		if( isLoggingDebug() ) {
			logDebug( "exclude query:" + query );
		}
		Repository productCatalog = getPricingModelRepository();

		try {
			RepositoryView skuView = productCatalog.getView( "sku" );
			RqlStatement stat = RqlStatement.parseRqlStatement( query );
			if( isLoggingDebug() ) {
				logDebug( "exclude statement:" + stat );
			}
			result = stat.executeQuery( skuView, null );
		} catch( RepositoryException e ) {
			if( isLoggingError() ) {
				logError( "Exception from loading the product excluded list ", e );
			}
		}

		if( result != null && result.length != 0 ) {
			if( isLoggingDebug() ) {
				logDebug( "Exclude SKU Size:" + result.length );
			}
			addedResult.addAll(Arrays.asList(result));
		}

		return addedResult;

	}

	public boolean isEmpty( Set pSet ) {
		if( pSet == null || pSet.size() == 0 ) { return true; }
		return false;
	}

	/** populate the productExclude filter list based on pricing model setting
	 * 
	 * @param pPricingModel
	 * @return */
	public Set generateFilterSet( RepositoryItem pPricingModel, Order pOrder ) {

		HashSet addedResult = new HashSet();
		Repository productCatalog = getPricingModelRepository();
		try 
		{
			
			
		if (!pPricingModel.getItemDescriptor().getItemDescriptorName().equals("closenessQualifier"))
		{
			
		boolean excludeClearanceItem = ( (Boolean)pPricingModel.getPropertyValue( EXCLUDE_CLEARANCE_ITEM ) ).booleanValue();
		Set excludeBrands = (Set)pPricingModel.getPropertyValue( EXCLUDE_BRANDS );
		Set excludeProds = (Set)pPricingModel.getPropertyValue( EXCLUDE_PRODS );
		Set excludeProdTypes = (Set)pPricingModel.getPropertyValue( EXCLUDE_PRODTYPES );

		if( isLoggingDebug() ) {
			logDebug( "Start generating promo exclusion set for pricing model:" + pPricingModel.getRepositoryId() );
			logDebug( "exclude excludeBrands:" + excludeBrands );
			logDebug( "exclude excludeProds:" + excludeProds );
			logDebug( "exclude excludeProdTypes:" + excludeProdTypes );
		}

		// if nothing defined, stop processing here.
		if( excludeClearanceItem == false && isEmpty( excludeBrands ) && isEmpty( excludeProds ) && isEmpty( excludeProdTypes ) ) { return addedResult; }

		
			RepositoryView skuView = productCatalog.getView( "sku" );
			RepositoryView prodView = productCatalog.getView( "product" );

			// get all sku whose clearance flag is true
			if( excludeClearanceItem == true || isEmpty( excludeBrands ) != true || isEmpty( excludeProdTypes ) != true ) {
				// RepositoryItem[] result = skuView.executeQuery( getAllClearanceSKUsQuery() );
				// Get all the skus in order whose clearance flag is true or belongs to excluded brands
				RepositoryItem[] result = null;
				List<RepositoryItem> aList = new ArrayList<>();
				if( pOrder != null ) {
					List commerceItems = pOrder.getCommerceItems();
					if( commerceItems != null ) {
						for( int c = 0; c < commerceItems.size(); c++ ) {
							DigitalCommerceItem item = (DigitalCommerceItem)commerceItems.get( c );
							if( item != null ) {
								// Sku in clearance
								RepositoryItem sku = (RepositoryItem)item.getAuxiliaryData().getCatalogRef();

								boolean clearance = false;
								if( excludeClearanceItem ) {
									clearance = ( (Boolean)sku.getPropertyValue( "isClearanceItem" ) ).booleanValue();
								}
								// Brand Excluded & ProductTypes Exclude
								boolean belongsToExcludedBrand = false;
								boolean belongsToExcludedProductTypes = false;
								if( isEmpty( excludeBrands ) != true || isEmpty( excludeProdTypes ) != true ) {
									RepositoryItem product = (RepositoryItem)item.getAuxiliaryData().getProductRef();
									if( product == null ) {
										String productId = item.getAuxiliaryData().getProductId();
										product = (RepositoryItem)productCatalog.getItem( productId, "product" );
									}

									if( product != null ) {
										RepositoryItem brand = (RepositoryItem)product.getPropertyValue( "dswBrand" );
										String productTypeInUse = (String)product.getPropertyValue( "productTypeInUse" );
										if( isEmpty( excludeBrands ) != true && brand != null ) {
											if( excludeBrands.contains( brand ) ) {
												belongsToExcludedBrand = true;
											}
										}
										if( isEmpty( excludeProdTypes ) != true && productTypeInUse != null ) {
											Iterator iter = excludeProdTypes.iterator();
											while( iter.hasNext() ) {
												String exclProductType = (String)iter.next();
												if( exclProductType.equalsIgnoreCase( productTypeInUse ) ) belongsToExcludedProductTypes = true;
											}
											// "kids shoes" contains "shoe" if (excludeProdTypes.contains(productTypeInUse)) {
											// belongsToExcludedProductTypes = true;
											// }
										}
									}
								}
								if( ( clearance && excludeClearanceItem ) || belongsToExcludedBrand || belongsToExcludedProductTypes ) {
									aList.add( sku );
								}
							}
						}
					}
				}
				if( aList.size() > 0 ) result = aList.toArray( new RepositoryItem[aList.size()] );

				if( isLoggingDebug() ) {
					if( result == null || result.length == 0 ) {
						logDebug( "found no SKUs to exclude" );
					} else {
						logDebug( "found SKUs to exclude: " + result.length );
					}
				}
				addedResult = addtoFilterSet( result, addedResult );
			}

			// get all product whose excludeBrands is one of listed
			// if (isEmpty( excludeBrands ) != true) {
			// RepositoryItem[] prodResult = prodView.executeQuery( getPropertyBrandsQuery( excludeBrands ) );
			// if (isLoggingDebug()) {
			// if (prodResult == null || prodResult.length == 0) {
			// logDebug( "found no products for the brand exclusion" );
			// }
			// else {
			// logDebug( "found products for the brand exclusion:" + prodResult.length );
			// }
			// }
			// addedResult = getChildSKUsFromProduct( prodResult, addedResult );
			// }

			// get all product whose excludeProdTypes is one of listed
			// if (isEmpty( excludeProdTypes ) != true) {
			// RepositoryItem[] prodResult = prodView.executeQuery( getPropertyProductTypesQuery( excludeProdTypes ) );
			// if (isLoggingDebug()) {
			// if (prodResult == null || prodResult.length == 0) {
			// logDebug( "found no products for the prod type exclusion" );
			// }
			// else {
			// logDebug( "found products for the prod type:" + prodResult.length );
			// }
			// }
			// addedResult = getChildSKUsFromProduct( prodResult, addedResult );
			// }

			// get all product whose excludeProd is one of listed
			if( isEmpty( excludeProds ) != true ) {
				RepositoryItem[] prodResult = (RepositoryItem[])excludeProds.toArray( new RepositoryItem[excludeProds.size()] );
				addedResult = getChildSKUsFromProduct( prodResult, addedResult );
			}
		}
		} catch( RepositoryException e ) {
			if( isLoggingError() ) {
				logError( "generate SKU promotion exclusion list failure", e );
			}
		}

		if( addedResult != null && addedResult.size() != 0 ) {
			if( isLoggingDebug() ) {
				logDebug( "Complete Exclusion SKU List Size:" + addedResult.size() );
				/* Iterator iter = addedResult.iterator();
				 * while ( iter.hasNext() ) {
				 * RepositoryItem item = (RepositoryItem) iter.next();
				 * logDebug( "SKU exclued from promotion: " + item.getItemDisplayName() + "-" + item.getRepositoryId() );
				 * } */
			}

		}

		return addedResult;

	}
	
	
	/** populate the productExclude filter list based on pricing model setting
	 * 
	 * @param pPricingModel
	 * @return */
	public List getFilterDiscountableItems( RepositoryItem pPricingModel, Order pOrder ) {
		HashSet addedResult = new HashSet();
		Repository productCatalog = getPricingModelRepository();
		getPromoExcludesInfo().clear();
		List retList = new ArrayList();
		try
		{			
			boolean excludeClearanceItem =false;
			Set excludeBrands = null;
			Set excludeProds = null;
			Set excludeProdTypes = null;
			
			if (!pPricingModel.getItemDescriptor().getItemDescriptorName().equals("closenessQualifier"))
			{
				excludeClearanceItem = ( (Boolean)pPricingModel.getPropertyValue( EXCLUDE_CLEARANCE_ITEM ) ).booleanValue();
				excludeBrands = (Set)pPricingModel.getPropertyValue( EXCLUDE_BRANDS );
				excludeProds = (Set)pPricingModel.getPropertyValue( EXCLUDE_PRODS );
				excludeProdTypes = (Set)pPricingModel.getPropertyValue( EXCLUDE_PRODTYPES );
				if( isLoggingDebug() ) {
					logDebug( "Start generating promo exclusion set for pricing model:" + pPricingModel.getRepositoryId() );
					logDebug( "exclude excludeBrands:" + excludeBrands );
					logDebug( "exclude excludeProds:" + excludeProds );
					logDebug( "exclude excludeProdTypes:" + excludeProdTypes );
				}
			}
			else{				
				RepositoryItem configuredPromotion = ((RepositoryItem)pPricingModel.getPropertyValue("promotion"));
				if (configuredPromotion!=null) {
					excludeClearanceItem = ( (Boolean)configuredPromotion.getPropertyValue( EXCLUDE_CLEARANCE_ITEM ) ).booleanValue();
					excludeBrands = (Set)configuredPromotion.getPropertyValue( EXCLUDE_BRANDS );
					excludeProds = (Set)configuredPromotion.getPropertyValue( EXCLUDE_PRODS );
					excludeProdTypes = (Set)configuredPromotion.getPropertyValue( EXCLUDE_PRODTYPES );
				}
			}
				// if nothing defined, stop processing here.
			if (excludeClearanceItem == false && isEmpty(excludeBrands) && isEmpty(excludeProds)
					&& isEmpty(excludeProdTypes)) {
				return retList;
			}	
			
				RepositoryView skuView = productCatalog.getView( "sku" );
				RepositoryView prodView = productCatalog.getView( "product" );
				
	
				// get all sku whose clearance flag is true
				if( excludeClearanceItem == true || isEmpty( excludeBrands ) != true || isEmpty( excludeProdTypes ) != true  || isEmpty( excludeProds ) != true ) {
					// RepositoryItem[] result = skuView.executeQuery( getAllClearanceSKUsQuery() );
					// Get all the skus in order whose clearance flag is true or belongs to excluded brands
					RepositoryItem[] result = null;
					List<RepositoryItem> aList = new ArrayList<>();
					if( pOrder != null ) {
						List commerceItems = pOrder.getCommerceItems();
						if( commerceItems != null ) {
							for( int c = 0; c < commerceItems.size(); c++ ) {
								try{
									DigitalCommerceItem item = (DigitalCommerceItem)commerceItems.get( c );
								
								if( item != null ) {
									// Sku in clearance
									RepositoryItem sku = (RepositoryItem)item.getAuxiliaryData().getCatalogRef();
									RepositoryItem product = (RepositoryItem)item.getAuxiliaryData().getProductRef();
									RepositoryItem brand = null;
									if( product == null ) {
										String productId = item.getAuxiliaryData().getProductId();
										product = (RepositoryItem)productCatalog.getItem( productId, "product" );										
									}

									if(product!=null){
										brand = (RepositoryItem)product.getPropertyValue( "dswBrand" );
									}
									
									boolean clearance = false;
									if( excludeClearanceItem ) {
										clearance = ( (Boolean)sku.getPropertyValue( "isClearanceItem" ) ).booleanValue();
									}
									// Brand Excluded & ProductTypes Exclude
									boolean belongsToExcludedBrand = false;
									boolean belongsToExcludedProductTypes = false;
									boolean belongsToExcludedProduct = false;
									
									if( isEmpty( excludeBrands ) != true || isEmpty( excludeProdTypes ) != true || isEmpty(excludeProds) != true ) {
										
	
										if( product != null ) {
											
											String productTypeInUse = (String)product.getPropertyValue( "productTypeInUse" );
											if( isEmpty( excludeProds ) != true ) {
												if( excludeProds.contains( product ) ) {
													belongsToExcludedProduct = true;
												}
											}
											if( isEmpty( excludeBrands ) != true && brand != null ) {
												if( excludeBrands.contains( brand ) ) {
													belongsToExcludedBrand = true;
												}
											}
											if( isEmpty( excludeProdTypes ) != true && productTypeInUse != null ) {
												Iterator iter = excludeProdTypes.iterator();
												while( iter.hasNext() ) {
													String exclProductType = (String)iter.next();
													if( exclProductType.equalsIgnoreCase( productTypeInUse ) ) belongsToExcludedProductTypes = true;
												}
												// "kids shoes" contains "shoe" if (excludeProdTypes.contains(productTypeInUse)) {
												// belongsToExcludedProductTypes = true;
												// }
											}
										}
									}
									
									if( ( clearance && excludeClearanceItem ) || belongsToExcludedBrand || belongsToExcludedProductTypes ||  belongsToExcludedProduct) {
										
										Set<String> excludes = getPromoExcludesInfo();
										String brandDisplayName="";
										if(null!=brand){
											brandDisplayName=(String)brand.getPropertyValue(DISPLAY_NAME_PROPERTY);
										}
										excludes.add(brandDisplayName + " "+(String)product.getPropertyValue(DISPLAY_NAME_PROPERTY));
										setPromoExcludesInfo(excludes);
										item.setExcludedFromPromotion(true);	
										retList.add(item);
									}
									else{
										// Reset to default so that any other previous promotion exclusion should not be accounted for the current promotion evaluation
										item.setExcludedFromPromotion(false);
									}
									
									
								}
							}catch  (ClassCastException cce){
								logError("Error casting commerce item for filter.");
							}
							}
						}
					}
					
				}
	
			
		} catch( RepositoryException e ) {
			if( isLoggingError() ) {
				logError( "generate SKU promotion exclusion list failure", e );
			}
	}
		
		return retList;
	}
	
	

	public HashSet getChildSKUsFromProduct( RepositoryItem[] products, HashSet addedSet ) {

		if( products == null || products.length == 0 ) { return addedSet; }

		for( int i = 0; i < products.length; i++ ) {
			List childSKUList = (List)products[i].getPropertyValue( "childSKUs" );
			addedSet.addAll( childSKUList );
		}

		return addedSet;
	}

	public HashSet addtoFilterSet( RepositoryItem[] items, HashSet filterSet ) {
		if( items == null ) { return filterSet; }

		filterSet.addAll(Arrays.asList(items));

		return filterSet;

	}


	/** calculates a price from the sum of the order's item prices, if the item is
	 * not excluded from the promotion, also mark each item if it is excluded
	 * from the current promotion
	 * 
	 * @param pOrder
	 *            the order whose item prices will be summed
	 * @return the sum of the input order's qualified item prices */
	protected double calculateQualifedPriceFromOrderItems( Set pFilterSet, Order pOrder ) {

		if( isLoggingDebug() ) {
			logDebug( "Start calculateQualifedPriceFromOrderItems" + pFilterSet );
		}
		double price = 0.0;

		if( pOrder == null ) { return price; }

		List commerceItems = pOrder.getCommerceItems();
		if( commerceItems != null ) {
			for( int c = 0; c < commerceItems.size(); c++ ) {
				DigitalCommerceItem item = (DigitalCommerceItem)commerceItems.get( c );

				if( item != null ) {
					// always reset the exclude flag
					item.setExcludedFromPromotion( false );
					ItemPriceInfo info = (ItemPriceInfo)item.getPriceInfo();
					if( info != null ) {
						// if the item is not part of the exclude list, add the amount
						if( pFilterSet == null || pFilterSet.size() == 0 || !pFilterSet.contains( item.getAuxiliaryData().getCatalogRef() ) ) {
							if( isLoggingDebug() ) {
								logDebug( "item is not excluded from the promotion: " + item.getCatalogRefId() );
							}
							price = price + info.getAmount() - info.getOrderDiscountShare();
						} else {
							if( isLoggingDebug() ) {
								logDebug( "item is excluded from the promotion: " + item.getCatalogRefId() );
							}
							item.setExcludedFromPromotion( true );
						}
					}
				}
			}
		}

		if( isLoggingDebug() ) {
			logDebug( "Order qualified amount from item total is : " + price );
		}

		return price;

	}

	/** This method calculates the subtotal of the shipping group. This method
	 * will return the subtotal of the items contained within this shipping
	 * group. In addition it will modify the given pPriceQuote (the Order's price
	 * info) so the following properties include a new entry for pShippingGroup:
	 * 
	 * <pre>
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 *         shippingItemsSubtotalPriceInfos
	 *         taxableShippingItemsSubtotalPriceInfos
	 *         nonTaxableShippingItemsSubtotalPriceInfos
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * </pre>
	 * 
	 * @param pPriceQuote
	 *            The price info for the order
	 * @param pOrder
	 *            The order containing the shipping group
	 * @param pShippingGroup
	 *            The shipping group being priced
	 * @param pItemQuantities
	 *            A map mapping a commerce item to a quantity. The incoming
	 *            quantity will be the total quantity of that item that has been
	 *            included in the subtotal of other shipping groups. The quantity
	 *            will be increased by the quantity contained in pShippingGroup
	 * @param pItemPrices
	 *            A map mapping a commerce item to a price. The price is the total
	 *            price of the item that has been included in the subtotals of
	 *            other shipping groups. Once the pItemQuantities for a given item
	 *            is reached this value is used to resolve any potential rounding
	 *            errors. The pItemPrices for a particular item will be increased
	 *            by the price of the item that pShippingGroup's subtotal used.
	 * @param pLocale
	 *            The locale in which all pricing operations should be performed
	 * @param pProfile
	 *            The owner of pOrder
	 * @param pExtraParameters
	 *            Any extra processing information. Unused by this method.
	 * @return The subtotal of pShippingGroup (this is the sum of the prices of
	 *         the items contained within the shipping group)
	 * @throws PricingException */
	protected double calculateShippingGroupQualifiedAmount( ShippingGroup pShippingGroup, OrderPriceInfo pPriceQuote, Order pOrder, Locale pLocale, RepositoryItem pProfile, Map pExtraParameters ) throws PricingException {

		if( isLoggingDebug() ) {
			logDebug( "Start calculateShippingGroupQualifiedAmount for " + pShippingGroup.getId() );
		}

		Map taxableEntries = pPriceQuote.getTaxableShippingItemsSubtotalPriceInfos();
		Map nonTaxableEntries = pPriceQuote.getNonTaxableShippingItemsSubtotalPriceInfos();
		Map entries = pPriceQuote.getShippingItemsSubtotalPriceInfos();
		String shippingGroupId = pShippingGroup.getId();

		DigitalOrderPriceInfo taxableShippingGroupSubtotalInfo = (DigitalOrderPriceInfo)taxableEntries.get( shippingGroupId );
		DigitalOrderPriceInfo nonTaxableShippingGroupSubtotalInfo = (DigitalOrderPriceInfo)nonTaxableEntries.get( shippingGroupId );
		DigitalOrderPriceInfo shippingGroupSubtotalInfo = (DigitalOrderPriceInfo)entries.get( shippingGroupId );

		double groupQualifiedSubtotal = 0.0;
		double groupQualifiedTaxableSubtotal = 0.0;
		double groupQualifiedNonTaxableSubtotal = 0.0;
		double taxableAmount;
		double nonTaxableAmount;
		long shippingGroupQuantity = 0;
		long taxableShippingGroupQuantity = 0;
		long nonTaxableShippingGroupQuantity = 0;

		// get all the relationships in this group
		List relationships = pShippingGroup.getCommerceItemRelationships();

		// if there are no relationships in this group, there must be a
		// relationship
		// in some other group, because we've already verified that fact using
		// checkRelationships
		if( relationships == null || relationships.isEmpty() ) {
			return 0.0;
		} // end if no relationships
			// else there is at least one relationship
		else {

			if( isLoggingDebug() ) {
				logDebug( "Shipping group : " + pShippingGroup.getId() + " has : " + relationships.size() + " relationships" );
			}

			Iterator relationshipIterator = relationships.iterator();
			while( relationshipIterator.hasNext() ) {

				ShippingGroupCommerceItemRelationship rel = (ShippingGroupCommerceItemRelationship)relationshipIterator.next();

				// get the item and the quantity of the item that is in this
				// relationship.
				// switch on type of relationship to get the actual quantity that
				// the relationship
				// represents

				long relationshipQuantity = 0;
				DigitalCommerceItem item = (DigitalCommerceItem)rel.getCommerceItem();
				// if item is excluded from promotion, skip the calculation
				if( item.isExcludedFromPromotion() ) {
					if( isLoggingDebug() ) {
						logDebug( "Item " + item.getCatalogRefId() + " is excluded from promotion, skip the qualified shippinggroup total calculation" );
					}
					continue;
				}

                switch (rel.getRelationshipType()) {
                    case RelationshipTypes.SHIPPINGQUANTITY:
                        relationshipQuantity = rel.getQuantity();
                        break;
                    case RelationshipTypes.SHIPPINGQUANTITYREMAINING:
                        relationshipQuantity = getOrderManager().getShippingGroupManager().getRemainingQuantityForShippingGroup(item);
                        break;
                    default:
                        throw new PricingException(MessageFormat.format(Constants.BAD_SHIPPING_GROUP_TYPE, new Object[]{Integer.valueOf(rel.getRelationshipType())}));
                }

				if( isLoggingDebug() ) {
					logDebug( "total quantity of item: " + item.getId() + " to add to order total: " + relationshipQuantity );
				}

				// if there's no quantity in this rel, skip it
				if( relationshipQuantity == 0 ) {
					if( isLoggingDebug() ) {
						logDebug( "no quantity of this relationship to add, continuing." );
					}
					continue;
				}

				shippingGroupQuantity += relationshipQuantity;

				/** find the average item price for order summing purposes, since all
				 * items will be accounted for (we just need a breakdown for the
				 * group level) see javadoc for explanation. average price is total
				 * price for the CommerceItem divided by the quantity of the item.
				 * This code also floors the average price to a configurable number
				 * of decimal places, and tacks any remainder on to a single
				 * relationship amount. The remainder itself should automatically be
				 * a max of N decimal places, because item prices get rounded too.
				 * Therefore, an item price with a max of N places minus (average
				 * price rounded to N places - times - CommerceItem quantity) can
				 * yield a number with a maximum of N decimal places:
				 * 
				 * 2 places times 0 places => 2 places 2 places minus 2 places => 2
				 * places */

				// add to the group subtotal the cost of this relationship
				// Now that relationships and details have ranges, we don't need to
				// use
				// averages
				double relationshipSubtotal = 0.0;

				double roundedAverage = 0.0;
				if( ( getPricingTools().isShippingSubtotalUsesAverageItemPrice() ) || ( rel.getRange() == null ) ) {
					double itemOrderDiscountShare = item.getPriceInfo().getOrderDiscountShare();

					double averagePrice = ( item.getPriceInfo().getAmount() - itemOrderDiscountShare ) / item.getQuantity();
					// double averagePrice = getPricingTools().getAverageItemPrice(
					// item );
					roundedAverage = getPricingTools().round( averagePrice );

					if( isLoggingDebug() ) {
						logDebug( "Average price of commerceitem afer taking off the applied order discount share: " + item.getId() + " across all relationships : " + averagePrice );
						logDebug( "rounding average price" );
						logDebug( "rounded average price to: " + roundedAverage );
					}

					relationshipSubtotal = relationshipQuantity * roundedAverage;
				} else {

					double itemOrderDiscountShare = item.getPriceInfo().getOrderDiscountShare();
					double averageShare = ( itemOrderDiscountShare / item.getQuantity() ) * rel.getQuantity();
					relationshipSubtotal = getPricingTools().getShipItemRelPriceTotal( rel, "amount" ) - averageShare;
				}

				// add the relationship subtotal to the group subtotal
				groupQualifiedSubtotal += relationshipSubtotal;

				/** sometimes double math produces 'equivalent', yet not exactly
				 * identical numbers. For example, it appears that the numbers: .03
				 * and .030000000000001137 are numerically equivalent as far as
				 * double calculations are concerned. Double calculations sometimes
				 * produce these strange numbers despite the fact that the number of
				 * decimal places can't possibly be that large. We're going to round
				 * the products of double math to N places, with the understanding
				 * that all numbers in DCS are going to be rounded to N places. */

				groupQualifiedSubtotal = getPricingTools().round( groupQualifiedSubtotal );

				if( isLoggingDebug() ) {
					logDebug( "after rounding, group qualified subtotal: " + groupQualifiedSubtotal );
				}

				// if the current item is taxable, add it to the taxable subtotal
				if( getPricingTools().isTaxable( item, item.getPriceInfo(), pPriceQuote, pOrder, pShippingGroup.getPriceInfo(), pShippingGroup, pLocale, pProfile, pExtraParameters ) ) {
					// see if the item is partially taxable or not

					if( getPricingTools().isShippingSubtotalUsesAverageItemPrice() )
						taxableAmount = getPricingTools().calculateTaxableAmountByAverage( rel, pOrder, pLocale, pProfile, pExtraParameters );
					else
						taxableAmount = getPricingTools().calculateTaxableAmount( rel, pOrder, pLocale, pProfile, pExtraParameters );

					// make sure that our averaging doesn't end up making it so the
					// taxable amount is more than the total amount.
					if( taxableAmount > relationshipSubtotal ) {
						taxableAmount = relationshipSubtotal;
					}

					nonTaxableAmount = relationshipSubtotal - taxableAmount;

					taxableShippingGroupQuantity += relationshipQuantity;
					groupQualifiedTaxableSubtotal += taxableAmount;

					groupQualifiedTaxableSubtotal = getPricingTools().round( groupQualifiedTaxableSubtotal );

					if( isLoggingDebug() ) {
						logDebug( "relationship's subtotal is TAXABLE" );
						logDebug( "after rounding, group taxable subtotal: " + groupQualifiedTaxableSubtotal );
					}

					nonTaxableAmount = getPricingTools().round( nonTaxableAmount );

					if( nonTaxableAmount > 0.0 ) {
						// if we are here, then the item was "partially" taxable
						// each quantity of the item has a taxable part and a non
						// taxable part
						nonTaxableShippingGroupQuantity += relationshipQuantity;
						groupQualifiedNonTaxableSubtotal += nonTaxableAmount;
						groupQualifiedNonTaxableSubtotal = getPricingTools().round( groupQualifiedNonTaxableSubtotal );
						if( isLoggingDebug() ) {
							logDebug( "It was only partially taxable though" );
							logDebug( "after rounding, group non taxable subtotal: " + groupQualifiedNonTaxableSubtotal );
						}
					}

				}
				// if it's not taxable, add it to the nontaxable subtotal
				else {
					nonTaxableShippingGroupQuantity += relationshipQuantity;
					groupQualifiedNonTaxableSubtotal += relationshipSubtotal;

					groupQualifiedNonTaxableSubtotal = getPricingTools().round( groupQualifiedNonTaxableSubtotal );

					if( isLoggingDebug() ) {
						logDebug( "relationship's subtotal is NON-TAXABLE" );
						logDebug( "after rounding, group qualified non taxable subtotal is: " + groupQualifiedNonTaxableSubtotal );
					}

				} // end if not taxable

			} // end for each relationship

			// we should now have all relationships in the group accounted for.

		} // end else there are relationships

		if( isLoggingDebug() ) {
			logDebug( "group qualified nontaxable subtotal: " + groupQualifiedNonTaxableSubtotal );
			logDebug( "group qualified taxable subtotal: " + groupQualifiedTaxableSubtotal );
			logDebug( "group qualified subtotal: " + groupQualifiedSubtotal );
		}

		if( taxableShippingGroupSubtotalInfo != null ) {
			taxableShippingGroupSubtotalInfo.setQualifiedAmount( groupQualifiedTaxableSubtotal );
			taxableShippingGroupSubtotalInfo.setUnqualifiedAmount( taxableShippingGroupSubtotalInfo.getAmount() - groupQualifiedTaxableSubtotal );
		}

		if( nonTaxableShippingGroupSubtotalInfo != null ) {
			nonTaxableShippingGroupSubtotalInfo.setQualifiedAmount( groupQualifiedNonTaxableSubtotal );
			nonTaxableShippingGroupSubtotalInfo.setUnqualifiedAmount( nonTaxableShippingGroupSubtotalInfo.getAmount() - groupQualifiedNonTaxableSubtotal );
		}

		if( shippingGroupSubtotalInfo != null ) {
			shippingGroupSubtotalInfo.setQualifiedAmount( groupQualifiedSubtotal );
			shippingGroupSubtotalInfo.setUnqualifiedAmount( shippingGroupSubtotalInfo.getAmount() - groupQualifiedSubtotal );
		}

		return groupQualifiedSubtotal;
	}

}
