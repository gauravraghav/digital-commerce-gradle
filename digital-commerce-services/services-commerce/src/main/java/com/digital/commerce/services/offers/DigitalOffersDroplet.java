package com.digital.commerce.services.offers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.coupons.DigitalCouponService;
import com.digital.commerce.services.promotions.DigitalPromotionService;

import atg.nucleus.naming.ParameterName;
import atg.service.collections.filter.CachedCollectionFilter;
import atg.service.collections.filter.FilterException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalOffersDroplet extends DynamoServlet {

	private static final ParameterName OUTPUT = ParameterName
			.getParameterName("output");
	private static final ParameterName EMPTY = ParameterName
			.getParameterName("empty");
	private static final ParameterName ERROR_OPARAM = ParameterName
			.getParameterName("error");
	private static final String FILTERED_OFFERS = "filteredOffers";
	
	private static final String    PERFORM_MONITOR_NAME    = "DigitalOffersDroplet";
	
	private CachedCollectionFilter filter;
	
	private DigitalCouponService couponService;
	
	private DigitalPromotionService promotionService;

	/**
	 * This method will call existing initalizeAndFilterOffers method.
	 * Render oparam output with parameter filteredOffers.
	 */
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		String METHOD_NAME = "service";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(getName(), METHOD_NAME);
			
			Collection filteredOffers = initalizeSiteGroupOffers(pRequest,
					pResponse);
	
			if (filteredOffers.isEmpty()) {
				pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			}
			else{
				if (isLoggingDebug()) {
					logDebug("No Offers available");
					logDebug("Returning Offers = " + filteredOffers);
				}
				pRequest.setParameter(FILTERED_OFFERS, filteredOffers);
				pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);			
			}
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
	}

	/**
	 * Populates the mSiteGroupOffers Collection
	 * 
	 * @throws IOException
	 * @throws ServletException
	 */
	protected Collection initalizeSiteGroupOffers(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		String METHOD_NAME = "initalizeSiteGroupOffers";
		
		// Result collection
		Collection filteredOffers = new ArrayList();
		
		Collection unfilteredCollection = new ArrayList();
		
		// Filter all Offers using filter parameter
		try {
			
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			
			// All Coupons
			unfilteredCollection.addAll(getCouponService().findAllCoupons(false));
			
			// All Global Promotions for Guest user & for logged in it includes all promotions associated with the profile
			unfilteredCollection.addAll(getPromotionService().findAllPromotions());			
	
			// Check that there are Offers to filter
			if (unfilteredCollection == null || unfilteredCollection.size() == 0) {
				return filteredOffers;
			}
		
			filteredOffers = getFilter().filterCollection(
					unfilteredCollection, null, null);
		} catch (FilterException e) {			
			String msg = "Item does not have a sites property";
			if (isLoggingError()) {
				logError(msg);
			}
			pRequest.setParameter("errorMsg", msg);
			pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		return filteredOffers;
	}

}
