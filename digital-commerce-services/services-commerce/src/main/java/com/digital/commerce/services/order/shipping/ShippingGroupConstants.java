package com.digital.commerce.services.order.shipping;

import lombok.Getter;

public class ShippingGroupConstants {
	@Getter
	public enum ShippingGroupPropertyManager{
		RANK("rank"),
		REGION("region"),
		ISPOBOX("isPoBox"),
		ADDRESS_VERIFICATION("addressVerification"),
		SHIP_TYPE("shipType"),
		STOREID("storeId");
		
		
		private final String value;
	  		
		private ShippingGroupPropertyManager( String value ) {
			this.value = value;
		}
		
	}
	
	@Getter
	public enum ShipType{
		SHIP("SHIP"),
		BOPIS("BOPIS"),
		BOSTS("BOSTS");
		
		private final String value;
		
		private ShipType( String value ) {
			this.value = value;
		}
		
	}
	
	public static final String	NO_SHIPPING_YES	= "Y";
	public static final String	NO_SHIPPING_NO	= "N";
	public static final String	DEFAULT_SHIPPING_METHOD = "GRN";
	public static final String	SECOND_DAY_SHIPPING_METHOD = "2ND";
	public static final String	NEXT_DAY_SHIPPING_METHOD = "NDA";
	public static final String	SHIPPING_PRICE_MAP= "SHIPPING_PRICE_MAP";
	public static final String	EMAIL= "email";
}
