package com.digital.commerce.services.order.purchase.valueobject;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
import lombok.Getter;
import lombok.Setter;

@Generated("com.robohorse.robopojogenerator")
@Getter
@Setter
public class CreditCard{

	@JsonProperty("newCreditCardPayment")
	private boolean newCreditCardPayment;

	@JsonProperty("cvv")
	private String cvv;

	@JsonProperty("savedCard")
	private SavedCard savedCard;

	@JsonProperty("newCard")
	private NewCard newCard;
}