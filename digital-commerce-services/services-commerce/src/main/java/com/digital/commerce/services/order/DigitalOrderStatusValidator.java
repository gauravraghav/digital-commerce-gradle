package com.digital.commerce.services.order;

import java.util.ArrayList;
import java.util.List;

import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.service.collections.validator.CollectionObjectValidator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalOrderStatusValidator extends ApplicationLoggingImpl implements CollectionObjectValidator {

	private String[] validOrderStates;

	public DigitalOrderStatusValidator() {
		super(DigitalOrderStatusValidator.class.getName());
	}

	@Override
	public boolean validateObject(Object pObject) {
		if (!(pObject instanceof DigitalOrderHistoryModel) && !(pObject instanceof RepositoryItem)) {
			return false;
		}

		List<String> itemStatus = new ArrayList<>();

		if (pObject instanceof DigitalOrderHistoryModel) {

			if (((DigitalOrderHistoryModel) pObject).getCommerceItems() != null) {
				for (DigitalOrderHistoryItem item : ((DigitalOrderHistoryModel) pObject).getCommerceItems()) {
					itemStatus.add(item.getStatus());
				}
			}
		} else {
			itemStatus.add((String) ((RepositoryItem) pObject).getPropertyValue("status"));
		}

		if (itemStatus.size() == 0)
			return false;

		if (validOrderStates != null && validOrderStates.length > 0) {			
			int counter = 0;

			for (@SuppressWarnings("unused") String status : itemStatus) {
				for (String state : validOrderStates) {
					if (itemStatus.contains(state)) {
						counter++;
						break;
					}
				}
			}

			if (counter == itemStatus.size()){
				return true;
			}
			else{
				return false;
			}
		}
		return true;

	}

}
