package com.digital.commerce.services.order.payment.creditcard;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.validator.DigitalBasicAddressValidator;

import atg.commerce.CommerceException;
import atg.commerce.order.processor.BillingAddrValidatorImpl;
import atg.core.util.Address;
import atg.service.pipeline.PipelineResult;
import lombok.Getter;
import lombok.Setter;

/** This validates the billing address using the standard DSW logic if the address is of type {@link DigitalRepositoryContactInfo}.
 * If not it falls back on the existing code.
 * 
 */
@SuppressWarnings({"unchecked","rawtypes"})
@Getter
@Setter
public class BillingAddressValidator extends BillingAddrValidatorImpl {
	private MessageLocator			messageLocator;
	private DigitalBasicAddressValidator	dswBasicaddressValidator;

	/** Copies address fields from one to the other
	 * 
	 * @param address
	 * @param otherAddress */
	private <RA> RA copyAddress( Object address, RA otherAddress ) {
		try {
			return DigitalProfileTools.copyAddress( address, otherAddress );

		} catch( CommerceException e ) {
			if( isLoggingError() ) {
				logError( "Error copying address ", e );
			}
		}
		return otherAddress;
	}

	@Override
	protected void validateAddress( Address address, String id, PipelineResult result, ResourceBundle resourceBundle, Map resourceAndErrorKeyMaps ) {
		Map errors = validateAddress( address, resourceBundle, resourceAndErrorKeyMaps );
		if( errors != null ) {
			Set<Map.Entry<String, ?>> entries = errors.entrySet();
			for( Map.Entry<String, ?> entry : entries ) {
				if( entry != null ) {
					String key = entry.getKey();
					String message = getMessageLocator().getMessageString( key, (String)null );
					if( message == null ) {
						message = resourceBundle.getString( key );
					}
					addHashedError( result, key, id, message );
				}
			}
		}
	}


	@Override
	protected Map validateAddress( Address address, ResourceBundle resourceBundle, Map resourceAndErrorKeyMaps ) {
		Map errors = new HashMap();
		if( address instanceof DigitalRepositoryContactInfo ) {
			//TODO: Copy address need to be checked if we have to do it everytime
			List<String> fields = getDswBasicaddressValidator().validateAddress( copyAddress( address, new DigitalContactInfo() ) );
			for( String field : fields ) {
				errors.put( field, field );
			}
		} else {
			errors.putAll( super.validateAddress( address, resourceBundle, resourceAndErrorKeyMaps ) );
		}
		return errors;
	}
}
