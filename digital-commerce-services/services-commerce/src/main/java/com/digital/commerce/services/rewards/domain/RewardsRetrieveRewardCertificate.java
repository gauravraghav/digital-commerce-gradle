/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import com.digital.commerce.services.profile.rewards.LoyaltyCertificate;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RewardsRetrieveRewardCertificate extends ResponseWrapper {
	private List<LoyaltyCertificate> certificates;
	private double totalCertValue;
	private Boolean pendingCertificate = false;
	private boolean isValidLoyaltyMember = true;
}
