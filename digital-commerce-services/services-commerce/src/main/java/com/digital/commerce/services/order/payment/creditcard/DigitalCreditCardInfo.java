/**
 * 
 */
package com.digital.commerce.services.order.payment.creditcard;

import atg.payment.creditcard.GenericCreditCardInfo;
import lombok.Getter;
import lombok.Setter;

/** @author wibrahim */
@Getter
@Setter
public class DigitalCreditCardInfo extends GenericCreditCardInfo {

	private static final long	serialVersionUID	= 8800434414834673471L;
	private String				nameOnCard;
	private String				cvvNumber;
	private String				ipAddress;
	private String				authType;
	private String				cavv;
	private String				xid;
	private String				paypageRegistrationId;
	private String				tokenValue;
	private String				vantivTransId;
	private boolean				isCardBeingSavedToProfile;

}
