/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RetrieveRewardDetailsResponse extends ResponseWrapper {

	Map<String,Object> details = new HashMap<>();

	public RetrieveRewardDetailsResponse() {
	}
}
