 package com.digital.commerce.services.pricing.reprice;

import atg.commerce.CommerceException;
import atg.commerce.CommercePipelineException;
import atg.commerce.catalog.CatalogTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.OrderManager;
import atg.commerce.order.OrderServices;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.purchase.PurchaseProcessHelper;
import atg.commerce.pricing.PricingConstants;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.commerce.pricing.priceLists.PriceListManager;
import atg.commerce.profile.CommerceProfileTools;
import atg.commerce.promotion.GWPMarkerManager;
import atg.commerce.promotion.PromotionTools;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.NamedQueryView;
import atg.repository.ParameterSupportView;
import atg.repository.Query;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.xml.XMLSchemaConstants;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.exception.RepricingServicesException;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.repricing.IRepricingServices;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.pricing.reprice.ShDriverDateHandler;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.purchase.DigitalOrderXMLPostProcessor;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.profile.rewards.LoyaltyCertificate;
import com.digital.commerce.services.promotion.DigitalPromotionTools;
import com.digital.commerce.services.utils.XmlTemplate;
import com.endeca.infront.shaded.org.apache.commons.lang.exception.ExceptionUtils;

import org.xml.sax.SAXException;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.xml.parsers.ParserConfigurationException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.digital.commerce.common.util.ComponentLookupUtil.GWP_MARKER_MANAGER;
@SuppressWarnings({"rawtypes","unchecked","resource","unused"})
public class RepricingServices extends ApplicationLoggingImpl implements IRepricingServices {

	private static final String			RETURN			= "Return";
	private static final String			EXCHANGE		= "Exchange";
	private static final String			inputFileLoc	= "C:/dsw/dev/ATG/modules/eTailDirect/src/com/dsw/edt/pricing/sample-xml/OrderWithItemAndOrderPromoAndMultCI-1.xml";
	private static final String			RETURNTYPE		= "RepriceReturns";
	private static final String			EXCHANGETYPE	= "RepriceExchanges";
	private static final String			SALESTYPE		= "RepriceSales";
	private static final String			REPRICEDATE		= "RepriceDate";
	private static final String			PREMIERTIER		= "PREMIER";
	private static final String			BASICTIER		= "BASIC";
	private static final String			CLUBTIER		= "CLUB";
	private static final String			GOLDTIER		= "GOLD";
	private static final String			ELITETIER		= "ELITE";
	private static final String			SHDRIVERDATE	= "SHDRIVERDATE";
	
	private static final String			COMPONENT_NAME	= "RepricingServices";
	private static final String			METHOD_NAME		= "RepriceOrder";
	private static final String 		CLASSNAME 		= RepricingServices.class.getName();
	
	private ApplicationLogging			mLogging		= ClassLoggingFactory.getFactory().getLoggerForClass( RepricingServices.class );
	private PricingTools				mPricingTools;
	private PurchaseProcessHelper		mPurchaseProcessHelper;
	private OrderManager				mOrderManager;
	private Repository					mPromotionRepository;
	private PriceListManager			mPriceListManager;
	private CatalogTools				mCatalogTools;
	private PromotionTools				mPromotionTools;
	private MessageLocator				messageLocator;
	private OrderServices				mOrderServices;
	private TransactionManager			mTransactionManager;
	private DigitalRewardsManager mRewardsCertificateManager;
	private RepricingConstants			repricingConstants;
	private DigitalProfileTools				mProfileTools;
	private GWPMarkerManager gwpMarkerManager;

	private static final String SERVICE_NAME = "RepricingServices";

	public RepricingServices() {
		super(RepricingServices.class.getName());
	}
	
	/**
	 * Log the execution time for the service. The end time will be recorded
	 * within this call and execution time will be calculated based on the start
	 * time provided.
	 * 
	 * @param operationName
	 *            The name of the operation
	 * @param startTime
	 *            The start time of the operation
	 */
	private void logExecutionTime(String operationName, final long startTime) {
		final long endTime = System.currentTimeMillis();
		final long executionTime = endTime - startTime;
		final StringBuilder sb = new StringBuilder();
		sb.append(SERVICE_NAME).append(" : ");
		sb.append(operationName).append(" : ");
		sb.append(executionTime).append("ms");
		logInfo(sb.toString());
	}
	///* Modified this code for Transaction demarcation LS-1112*/
	public String repriceOrderEncoded( String pOrderXMLEncoded ) {
		String pOrderXML;
		String response = null;
		try {
			pOrderXML = DigitalCommonUtil.getDecodedString(pOrderXMLEncoded);
			response = repriceOrder(pOrderXML);
		} catch (DigitalAppException e2) {
			logError("Error decoding XML String :: ",e2);
		}
		
		return response;
	}
	
	///* Modified this code for Transaction demarcation LS-1112*/
	@Override
	public String repriceOrder( String pOrderXML ) throws RepricingServicesException {
		if( this.isLoggingDebug()) {
			logDebug( "repriceOrder() :: "+pOrderXML );
		}
		final long startTime = System.currentTimeMillis();
		String repriceOrderString;
		try {
			repriceOrderString=repriceOrder( pOrderXML, SALESTYPE );
				
			if( isLoggingDebug() ) {
				logDebug("repriceOrderString "+repriceOrderString);
			}
			return repriceOrderString;
		} catch( RepricingServicesException e1 ) {
			logError( "RepricingServicesException in repriceOrder Main Method :: " + e1.getMessage(), e1 );
			if( this.isLoggingInfo()) {
				logInfo( ":: repriceOrder Request for RepricingServicesException condition:: "+pOrderXML );
			}			
			throw e1;
		} catch( Exception e ) {
			logError( "Exception in repriceOrder Main Method :: " + e.getMessage(), e );
			if( this.isLoggingInfo()) {
				logInfo( ":: repriceOrder Request for Exception condition:: "+pOrderXML );
			}				
			String msg = e.getMessage();
			if(DigitalStringUtil.isBlank(msg)){
				msg = ExceptionUtils.getRootCauseMessage(e);
			}
			throw new RepricingServicesException( msg, e );
		} finally {
			logExecutionTime("repriceOrder", startTime);
		}
	}

	///* Modified this code for Transaction demarcation LS-1112*/
	public String repriceReturnOrder( String pOrderXML ) throws RepricingServicesException {
		final String METHOD_NAME="repriceReturnOrder";
		if( isLoggingDebug() ) {
			logDebug( "repriceReturnOrder() begin "+pOrderXML );
		}

		final long startTime = System.currentTimeMillis();
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, METHOD_NAME );
			
			String ret = repriceOrder( pOrderXML, RETURNTYPE );

			if( isLoggingDebug() ) {
				logDebug( "repriceReturnOrder() end "+ret);
			}
			return ret;
		} catch( RepricingServicesException e1 ) {
			logError( e1 );
			throw e1;
		} catch( Exception e ) {
			logError("Error executing repriceReturn Order", e);
			throw new RepricingServicesException( e.getMessage(), e );
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, METHOD_NAME );
			logExecutionTime("repriceReturnOrder", startTime);
		}
	}

	///* Modified this code for Transaction demarcation LS-1112*/
	public String repriceExchangeOrder( String pOrderXML ) throws RepricingServicesException {
		if( isLoggingDebug() ) {	
			logDebug( "repriceExchangeOrder() "+pOrderXML );
		}

		final long startTime = System.currentTimeMillis();
		
		String repriceOrderString;
		
		try {
			repriceOrderString=repriceOrder( pOrderXML, EXCHANGETYPE );
			if( isLoggingDebug() ) {
				logDebug( "repriceExchangeOrderString() "+repriceOrderString );
			}
			return repriceOrderString;
		} catch( RepricingServicesException e1 ) {
			logError( e1 );
			throw e1;

		} catch( Exception e ) {
			logError("Error executing repriceExchangeOrder",e);
			throw new RepricingServicesException( e.getMessage(), e );
		} finally {
			logExecutionTime("repriceExchangeOrder", startTime);
		}

	}
	
	private String repriceOrder( String pOrderXML, String repriceType ) throws IllegalStateException,
            ParserConfigurationException, SAXException, RepricingServicesException {

		// get loyalty number - this is used for the call to loyalty services
		LoyaltyTransformationCallback callback = new LoyaltyTransformationCallback();
		XmlTemplate template;
		try{
			template = new XmlTemplate( pOrderXML );
		}catch(Exception e){
			logError("Error Parsing reprice order payload", e);
			String msg = ExceptionUtils.getRootCauseMessage(e);
			if(DigitalStringUtil.isBlank(msg)){
				msg = e.getMessage();
			}
			throw new RepricingServicesException( msg, e );			
		}
		template.query( LoyaltyTransformationCallback.LOYALTY_NUMBER, callback );
		String loyaltyNumber = callback.getLoyaltyNumber();

		OrderServices oServices = getOrderServices();

		// /Start reading from original order
		// xml------------------------------------------------------------

		// Create the parser
		final CreateParser parser = new CreateParser();

        boolean rollback = false;
		boolean repricingProfile = false;
        try (ByteArrayInputStream byteStream = new ByteArrayInputStream(pOrderXML.getBytes())) {

            List returnOrders;
            try {
                // get all the return order elements from the XML
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "getReturnOrdersFromXML");
                returnOrders = getReturnOrdersFromXML(parser, byteStream);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "getReturnOrdersFromXML");
            }

            if (isLoggingDebug()) logDebug("\n\n The Order.xml parsed - found returnOrders in Orders: " + returnOrders);

            // we will need to rearrange this list to group returns by shipping
            // group.
            Map shippingGroupInfo = new HashMap(10);

            for (Object returnOrder : returnOrders) {
                ReturnItem ri = (ReturnItem) returnOrder;

                if (shippingGroupInfo.get(ri.getShippingGroupId()) == null) {
                    List l = new ArrayList(10);
                    l.add(ri);
                    shippingGroupInfo.put(ri.getShippingGroupId(), l);
                } else {
                    ((List) shippingGroupInfo.get(ri.getShippingGroupId())).add(ri);
                }
                if (isLoggingDebug()) {
                    logDebug("lineItemId=" + ((ReturnItem) returnOrder).getLineItemId());
                    logDebug("quantity=" + ((ReturnItem) returnOrder).getQuantity());
                    logDebug("customerReasonCode=" + ((ReturnItem) returnOrder).getCustomerReasonCode());
                    logDebug("shippingGroupId=" + ((ReturnItem) returnOrder).getShippingGroupId());
                }
            }

            if (isLoggingDebug()) logDebug("SHIPPING GROUP INFO " + shippingGroupInfo.size());

            List exchangeOrders;
            try {
                // get all the exchange orders from the XML
                byteStream.reset();
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "getExchangeOrdersFromXML");
                exchangeOrders = getExchangeOrdersFromXML(parser, byteStream);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "getExchangeOrdersFromXML");
            }

            List combinedReturnsAndExchanges = new ArrayList(returnOrders.size() + exchangeOrders.size());

            if (isLoggingDebug()) {
                logDebug("\n\n The Order.xml parsed - found exchangeOrders in Orders: " + exchangeOrders);
                for (Object exchangeOrder : exchangeOrders) {
                    logDebug("lineItemId=" + ((ExchangeItem) exchangeOrder).getLineItemId());
                    logDebug("orderLineKey=" + ((ExchangeItem) exchangeOrder).getOrderLineKey());
                    logDebug("quantity=" + ((ExchangeItem) exchangeOrder).getQuantity());
                    logDebug("returnedLineItemId=" + ((ExchangeItem) exchangeOrder).getReturnedLineItemId());
                    logDebug("shippingGroupId=" + ((ExchangeItem) exchangeOrder).getShippingGroupId());
                    logDebug("evenExchange=" + ((ExchangeItem) exchangeOrder).isEvenExchange());
                }
            }

            // lets sort this list by timestamp
            combinedReturnsAndExchanges.addAll(returnOrders);
            combinedReturnsAndExchanges.addAll(exchangeOrders);

            Collections.sort(combinedReturnsAndExchanges, new SortByTimestampComparator());

            if (isLoggingDebug()) {
                logDebug("-----------------------------------------------");
                logDebug("Sorting combined list");
                for (Object combinedReturnsAndExchange : combinedReturnsAndExchanges) {
                    logDebug("timestamp=" + ((Item) combinedReturnsAndExchange).getTimestamp());
                    logDebug("type=" + ((Item) combinedReturnsAndExchange).getType());
                    if (((Item) combinedReturnsAndExchange).getType().equals(EXCHANGE)) {
                        logDebug("lineItemId=" + ((ExchangeItem) combinedReturnsAndExchange).getLineItemId());
                    } else if (((Item) combinedReturnsAndExchange).getType().equals(RETURN)) {
                        logDebug("lineItemId=" + ((ReturnItem) combinedReturnsAndExchange).getLineItemId());
                    }
                    logDebug("===================================================");
                }
            }

            List newPromos;
            try {
                // get all the new Promos enterd by CSR , just for kicks.
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "getNewPromosFromXML");
                byteStream.reset();
                newPromos = getNewPromosFromXML(parser, byteStream);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "getNewPromosFromXML");
            }

            Map commerceItemPriceLists;
            try {
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "getPriceListIdsFromXML");
                byteStream.reset();
                commerceItemPriceLists = getPriceListIdsFromXML(parser, byteStream);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "getPriceListIdsFromXML");
            }

            Map itemsRewardsShareMap;
            try {
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "getItemRewardsShareFromXML");
                byteStream.reset();
                itemsRewardsShareMap = getItemRewardsShareFromXML(parser, byteStream);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "getItemRewardsShareFromXML");
            }

            Map adjPromos;
            try {
                // holder for current promos on the order
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "getPromoIdsFromXML");
                byteStream.reset();
                adjPromos = getPromoIdsFromXML(parser, byteStream);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "getPromoIdsFromXML");
            }

            String profileId;
            try {
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "getProfileIdFromXML");
                byteStream.reset();
                profileId = getProfileIdFromXML(parser, byteStream);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "getProfileIdFromXML");
            }

            String loyaltyTier;
            try {
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "getOrderTierFromXML");
                byteStream.reset();
                loyaltyTier = getLoyaltyTierFromXML(parser, byteStream);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "getOrderTierFromXML");
            }

            String siteId;
            try {
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "getSiteIdFromXML");
                byteStream.reset();
                siteId = getSiteIdFromXML(parser, byteStream);
                MultiSiteUtil.setPushSiteContext(siteId);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "getSiteIdFromXML");
            }

            String locale;
            try {
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "getLocaleFromXML");
                byteStream.reset();
                locale = getLocaleFromXML(parser, byteStream);
                MultiSiteUtil.setLocale(locale);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "getLocaleFromXML");
            }

            byteStream.reset();

            String orderId = getOrderIdFromXML(parser, byteStream);
            if (isLoggingDebug()) logDebug("orderID before DECODE " + orderId);
            orderId = decodeFromXML(orderId);
            if (isLoggingDebug()) logDebug("orderID After DECODE " + orderId);

            byteStream.reset();
            String shDriverDate = null;
            try {
                shDriverDate = getShDriverDateFromXML(parser, byteStream);
            } catch (Exception e) {
                logError(e);
            }

            // Finish reading from original order
            // xml------------------------------------------------------------

            // the orderXML, has some return, prev returns, exchanges and prev
            // exchanges.
            // so we go to filter them out for the orderXML

            List indexes = new ArrayList(5);

            if (pOrderXML.contains("<order:order.shDriverDate>"))
                indexes.add(Integer.valueOf("" + pOrderXML.indexOf("<order:order.shDriverDate>")));
            if (pOrderXML.contains("<NewPromotions>"))
                indexes.add(Integer.valueOf("" + pOrderXML.indexOf("<NewPromotions>")));
            if (pOrderXML.contains("<ReturnOrder>"))
                indexes.add(Integer.valueOf("" + pOrderXML.indexOf("<ReturnOrder>")));
            if (pOrderXML.contains("<ExchangeOrder>"))
                indexes.add(Integer.valueOf("" + pOrderXML.indexOf("<ExchangeOrder>")));
            if (pOrderXML.contains("<PreviousReturnsAndExchanges>"))
                indexes.add(Integer.valueOf("" + pOrderXML.indexOf("<PreviousReturnsAndExchanges>")));

            if (indexes.size() > 0) {
                Object[] sortedIndexes = indexes.toArray();
                Arrays.sort(sortedIndexes);
                pOrderXML = pOrderXML.substring(0, (Integer) sortedIndexes[0]);
                pOrderXML += "</order:order>";
            }

            if (isLoggingDebug()) logDebug(pOrderXML);

            // when we know what the input XML looks like, then we should create an
            // Order object from the XML.
            // For now, just read from the repository itself
            if ((DigitalStringUtil.isBlank(profileId) || getProfileFromRespository(profileId) == null)
                    && DigitalStringUtil.isNotBlank(loyaltyNumber)) {
                //lookup for profileId based on loyaltyNumber
                profileId = this.getProfileTools().findProfileIdByLoyaltyNumber(loyaltyNumber);
            }

            if (DigitalStringUtil.isBlank(profileId) || getProfileFromRespository(profileId) == null) {
                repricingProfile = true;
                profileId = getRepricingUserId(loyaltyTier);
            }

            if (!repricingProfile) {
                //acquireProfileLock
                TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
            }
            Transaction tr = null;
            Order originalOrder = null;
            String returnItemShipPriceXML = "";
            try {

                tr = ensureTransaction();

                // first remove the existing order in the order repository
                try {
                    DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "removeOrderFromRepository");
                    RepositoryItem order=(RepositoryItem) getOrderManager().getOrderTools().getOrderRepository().getItem(orderId, getOrderManager().getOrderItemDescriptorName());
           
                    if (order != null) {
                        Order o=getOrderInstance(orderId);
                        getGwpMarkerManager().removeOrderMarkers(null , -1, o);
                        getOrderManager().removeOrder(orderId);
                    } else {
                        if (mLogging.isLoggingDebug())
                            mLogging.logDebug("order that was to be removed did not exsist int he Order Repository : " + orderId);
                    }
                } finally {
                    DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "removeOrderFromRepository");
                }


                if (isLoggingDebug()) logDebug("Profile to be added to order " + profileId);


                try {
                    DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "creatingOrderFromXML");
                    originalOrder = getOrderManager().loadOrder(oServices.createOrderFromXML(pOrderXML, profileId));
                } finally {
                    DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "creatingOrderFromXML");
                }

                Repository orderRepository = getOrderManager().getOrderTools().getOrderRepository();

                RepositoryItem orderItem = orderRepository.getItem(originalOrder.getId(), getOrderManager().getOrderItemDescriptorName());

                /** end of the promotion code comment */


                synchronized (originalOrder) {
                    Map returns = new HashMap(10);
                    Map certsValue;

                    // adjust the rewards share of the commerceItems, in case of
                    // canceled items
                    // certsValue=adjustTheItemsShareCertAmount(orderItem,originalOrder,itemsRewardsShareMap);

                    for (Object combinedReturnsAndExchange : combinedReturnsAndExchanges)
                        adjustTheCart(originalOrder, returns, (Item) combinedReturnsAndExchange, commerceItemPriceLists, itemsRewardsShareMap);

                    // adjust the rewards share of the commerceItems, in case of
                    // returned items
                    certsValue = adjustTheItemsShareCertAmount(orderItem, originalOrder, itemsRewardsShareMap);
                    adjustTheOrderRewardsCertAmount(orderItem, originalOrder, certsValue);

                    try {
                        if (originalOrder.getShippingGroups() != null) {
                            for (int i = 0; i < originalOrder.getShippingGroups().size(); i++) {
                                final ShippingGroup shippingGroup = (ShippingGroup) originalOrder.getShippingGroups().get(i);
                            }
                        }
                    } catch (NullPointerException e) {
                        logWarning(String.format("Attempt made to reprice original order %1$s where getShippingGroups() resulted in an NPE", originalOrder.getId()));
                    }
                    List l = originalOrder.getCommerceItems();

                    //Modified as a part of LS-1112 Start
                    if (isLoggingDebug()) {
                        for (Object aL : l) {
                            logDebug("===========Adjusted Cart contains Before Reprice========== " + ((CommerceItem) aL).getCatalogRefId());
                        }
                    }
                    //Modified as a part of LS-1112 End

                    /** The promotion logic has been placed below. */

                    List orderPromotions = new ArrayList(10);
                    List taxPromotions = new ArrayList(10);
                    List itemPromotions = new ArrayList(10);
                    List shippingPromotions = new ArrayList(10);

                    List allExistingPromotions = new ArrayList(10);

                    // need to get the promoId from the adjustment containers of
                    // itemPriceInfo, shippingPriceInfo, OrderPriceInfo, taxPriceInfo

                    // get the order promos
                    RepositoryItem orderPriceInfo = (RepositoryItem) orderItem.getPropertyValue("priceInfo");
                    if (orderPriceInfo == null) {
                        throw new RepricingServicesException("priceInfo is null; check if the order level elements [order:order.orderClassType to order:order.submittedDate] are before <NewPromotions> in the payload or check if <order:orderPriceInfo> exists with all details");
                    }
                    List orderAdjustments = (List) orderPriceInfo.getPropertyValue("adjustments");
                    for (Object orderAdjustment : orderAdjustments) {
                        String adjId = ((RepositoryItem) orderAdjustment).getRepositoryId();
                        String promoId = (String) adjPromos.get(adjId);
                        if (promoId != null && !promoId.trim().equals("")) if (getPromotionById(promoId) != null) {
                            orderPromotions.add(getPromotionById(promoId));
                            allExistingPromotions.add(getPromotionById(promoId));
                        }

                    }

                    // get the tax promos
                    RepositoryItem taxPriceInfo = (RepositoryItem) orderItem.getPropertyValue("taxpriceInfo");
                    if (taxPriceInfo == null) {
                        throw new RepricingServicesException("taxPriceInfo is null; check if the order level elements [order:order.orderClassType to order:order.submittedDate] "
                                + "are before <NewPromotions> in the payload or check if <order:taxpriceInfo> exists with all details");
                    }
                    List taxAdjustments = (List) taxPriceInfo.getPropertyValue("adjustments");
                    for (Object taxAdjustment : taxAdjustments) {
                        String adjId = ((RepositoryItem) taxAdjustment).getRepositoryId();
                        String promoId = (String) adjPromos.get(adjId);
                        if (promoId != null && !promoId.trim().equals("")) if (getPromotionById(promoId) != null) {
                            taxPromotions.add(getPromotionById(promoId));
                            allExistingPromotions.add(getPromotionById(promoId));
                        }
                    }

                    // get the shipping promos
                    List orderShippingGroups = (List) orderItem.getPropertyValue("shippingGroups");
                    if (orderShippingGroups == null) {
                        throw new RepricingServicesException("orderShippingGroups is null; check if the order level elements [order:order.orderClassType to order:order.submittedDate] "
                                + "	are before <NewPromotions> in the payload or check if <order:order.shippingGroups> exists with all details");
                    }
                    for (Object orderShippingGroup : orderShippingGroups) {
                        RepositoryItem shippingGroup = (RepositoryItem) orderShippingGroup;
                        RepositoryItem shippingPriceInfo = (RepositoryItem) shippingGroup.getPropertyValue("priceInfo");
                        List shippingAdjustments = (List) shippingPriceInfo.getPropertyValue("adjustments");
                        for (Object shippingAdjustment : shippingAdjustments) {
                            String adjId = ((RepositoryItem) shippingAdjustment).getRepositoryId();
                            String promoId = (String) adjPromos.get(adjId);
                            if (promoId != null && !promoId.trim().equals("")) if (getPromotionById(promoId) != null) {
                                shippingPromotions.add(getPromotionById(promoId));
                                allExistingPromotions.add(getPromotionById(promoId));
                            }
                        }
                    }

                    // get the commerce item promos
                    List orderCommerceItems = (List) orderItem.getPropertyValue("commerceItems");
                    if (orderCommerceItems == null) {
                        throw new RepricingServicesException("orderCommerceItems is null; check if the order level elements [order:order.orderClassType to order:order.submittedDate] "
                                + "	are before <NewPromotions> in the payload or check if <order:shipItemRel.commerceItem> exists with all details");
                    }
                    for (Object orderCommerceItem : orderCommerceItems) {
                        RepositoryItem commerceItem = (RepositoryItem) orderCommerceItem;
                        RepositoryItem commerceItemPriceInfo = (RepositoryItem) commerceItem.getPropertyValue("priceInfo");
                        List itemAdjustments = (List) commerceItemPriceInfo.getPropertyValue("adjustments");
                        for (Object itemAdjustment : itemAdjustments) {
                            String adjId = ((RepositoryItem) itemAdjustment).getRepositoryId();
                            String promoId = (String) adjPromos.get(adjId);
                            if (promoId != null && !promoId.trim().equals("")) if (getPromotionById(promoId) != null) {
                                itemPromotions.add(getPromotionById(promoId));
                                allExistingPromotions.add(getPromotionById(promoId));
                            }
                        }
                    }

                    // lets add all the new promos being sent along with the order.
                    List certs = new ArrayList(10);
                    DigitalPromotionTools pTools = (DigitalPromotionTools) getPromotionTools();
                    boolean isPromoExclusion = pTools.isEnableAllPromoExclusivity();
                    Date now = new Date();

                    RepositoryItem promo;
                    String promoValidationMsg;

                    for (Object newPromo : newPromos) {
                        if (isPromoExclusion) {
                            promo = getPromotionById((String) newPromo);

                            PromotionValidationService promoValService = new PromotionValidationService();
                            promoValidationMsg = promoValService.checkPromoValidation(allExistingPromotions, promo);

                            if (DigitalPromotionConstants.SUCCESS.equals(promoValidationMsg) || allExistingPromotions.size() == 0) {
                                if (promo != null && !getPromotionTools().checkPromotionExpiration(promo, now)) {
                                    allExistingPromotions.add(promo);
                                    int type = (Integer) promo.getPropertyValue("type");
                                    switch (type) {
                                        case 0:
                                        case 1:
                                        case 2:
                                        case 3:
                                            itemPromotions.add(promo);
                                            break;
                                        case 5:
                                        case 6:
                                        case 7:
                                        case 8:
                                            shippingPromotions.add(promo);
                                            break;
                                        case 9:
                                        case 10:
                                        case 11:
                                        case 12:
                                            orderPromotions.add(promo);
                                            break;
                                    }
                                } else {
                                    // maybe its a cert and not a promo
                                    certs.add(newPromo);
                                }
                            }

                        } else {
                            promo = getPromotionById((String) newPromo);
                            if (promo != null && !getPromotionTools().checkPromotionExpiration(promo, now)) {
                                int type = (Integer) promo.getPropertyValue("type");
                                switch (type) {
                                    case 0:
                                    case 1:
                                    case 2:
                                    case 3:
                                        itemPromotions.add(promo);
                                        break;
                                    case 5:
                                    case 6:
                                    case 7:
                                    case 8:
                                        shippingPromotions.add(promo);
                                        break;
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                        orderPromotions.add(promo);
                                        break;
                                }
                            } else {
                                // maybe its a cert and not a promo
                                certs.add(newPromo);
                            }
                        }

                    }

                    if (isLoggingDebug()) {

                        logDebug("Size of the order promos " + orderPromotions.size());
                        logDebug("Size of the tax promos " + taxPromotions.size());
                        logDebug("Size of the shipping promos " + shippingPromotions.size());
                        logDebug("Size of the item promos " + itemPromotions.size());

                    }
                    getUserPricingModels().setItemPricingModels(itemPromotions);
                    getUserPricingModels().setShippingPricingModels(shippingPromotions);
                    getUserPricingModels().setOrderPricingModels(orderPromotions);
                    getUserPricingModels().setTaxPricingModels(taxPromotions);

                    /** end of promotion logic */

                    try {
                        DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "repriceOrder");
                        originalOrder = repriceOrder(orderItem, originalOrder, newPromos, commerceItemPriceLists, itemsRewardsShareMap, certs, SALESTYPE, shDriverDate, loyaltyTier, loyaltyNumber, repricingProfile);

                        originalOrder = determineWhiteGloveOrder(originalOrder);

                    } catch (Exception | Error e) {
                        logError(e);
                    } finally {
                        DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "repriceOrder");
                    }

                    //Modified as a part of LS-1112 Start
                    if (isLoggingDebug()) {
                        for (Object aL : l) {
                            logDebug("===========Adjusted Cart contains after Reprice========== " + ((CommerceItem) aL).getCatalogRefId());
                        }
                    }
                    //Modified as a part of LS-1112 End

                    if (isLoggingDebug()) logDebug("Committing the order changes to the DB ");
                    MutableRepositoryItem mutItem = (MutableRepositoryItem) orderItem;
                    mutItem.setPropertyValue("state", "SUBMITTED");
                    getOrderManager().updateOrder(originalOrder);

                }
            } catch (RepricingServicesException e1) {
                logError(e1);
                rollback = true;
                throw e1;

            } catch (Exception e) {
                logError("Error executing repriceExchangeOrder", e);
                rollback = true;
                throw new RepricingServicesException(e.getMessage(), e);
            } finally {
                if (tr != null) {
                    commitTransaction(tr, originalOrder, rollback);
                }
            }
            String orderAsXML;
            String orderAsFullXML;
            try {
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(COMPONENT_NAME, "getRepricedOrderAsXML");
                // lets now send back the repriced order.
                orderAsXML = oServices.getOrderAsXML(originalOrder.getId());
                StringBuilder buffer = new StringBuilder();
                buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>");
                buffer.append(orderAsXML);
                orderAsFullXML = buffer.toString();
                orderAsFullXML = orderAsFullXML.replaceAll("s12CommerceItemOrderMap", "commerceItemOrderMap");
                orderAsFullXML = new DigitalOrderXMLPostProcessor().process(orderAsXML);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(COMPONENT_NAME, "getRepricedOrderAsXML");
            }

            if (isLoggingDebug()) {

                logDebug("-------------------------------- SUBMITTING ORDER AS XML --------------------------------");
                logDebug(orderAsFullXML);
                logDebug("-------------------------------- SUBMITTING ORDER AS XML --------------------------------");
            }

            // fit this back into our orderXML (at the end of it)

            if (RETURNTYPE.equalsIgnoreCase(repriceType)) {
                // Lets append the shipping prices for the returned items.

                int index = orderAsFullXML.indexOf("</order:order>");
                if (index > -1) {
                    pOrderXML = orderAsFullXML.substring(0, index);
                    pOrderXML += returnItemShipPriceXML;
                    pOrderXML += "</order:order>";
                }
                return pOrderXML;
            }
            return orderAsFullXML;
        } catch (RepricingServicesException e1) {
            logError(":: RepricingServicesException in repriceOrder Method :: " + e1.getMessage(), e1);
            throw e1;

        } catch (Exception e) {
            logError(":: Exception in repriceOrder Method :: " + e.getMessage(), e);
            throw new RepricingServicesException(e.getMessage(), e);
        } finally {
            if (!repricingProfile) {
                TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
            }
        }
	}
	
	public OrderImpl getOrderInstance(String orderId) {
		OrderImpl objOrder = null;
		try {
			if (!DigitalStringUtil.isBlank(orderId)) {
				objOrder = (OrderImpl) getOrderManager().loadOrder(orderId);
			}
		} catch (CommerceException ce) {
			logError("Error Loading Order", ce);
		}
		return objOrder;
	}

	private void adjustTheCart( Order originalOrder, Map returns, Item item, Map commerceItemPriceLists, Map itemsRewardsShareMap ) throws CommerceException, RepositoryException {

		if( item.getType().equals( RETURN ) ) {

			CommerceItem commerceItemToRemove = null;
			ReturnItem ri = (ReturnItem)item;
			// we have to figure out what commerceitem to remove. The only link
			// we have is the lineItemId.
			List ciItemsToSG = getOrderManager().getCommerceItemManager().getCommerceItemsFromShippingGroup( originalOrder.getShippingGroup( ri.getShippingGroupId() ) );
			// get all the commerce Items that carry that sku.
			List ciItemsWithLineItem = originalOrder.getCommerceItemsByCatalogRefId( ri.getLineItemId() );
			// decide which CommerceItem in the SG, has that sku
            for (Object aCiItemsToSG : ciItemsToSG) {
                if (ciItemsWithLineItem.contains(aCiItemsToSG)) {

                    // I dont like this.... but I got no choice.... checking for
                    // yantra's orderLinekey match
                    if (getOrderManager().getShippingGroupManager().getShippingGroupCommerceItemRelationship(originalOrder, ((CommerceItem) aCiItemsToSG).getId(), ri.getShippingGroupId()).getPropertyValue("orderLineKey")
                            .equals(ri.getOrderLineKey())) {
                        commerceItemToRemove = (CommerceItem) aCiItemsToSG;
                        break;
                    }

                }

            }
			// add to returns map -- we might need this for even exchange, where
			// we need to assign same pricelist as the item being removed for
			// the exchange
			returns.put( ri.getLineItemId(), commerceItemToRemove );

			// now remove the item from the shipping group
			if(null != commerceItemToRemove) {
                getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(originalOrder, commerceItemToRemove.getId(), ri.getShippingGroupId(), ri.getQuantity());

                // if balance qty on commerceitem is zero, then remove item from
                // cart itself.
                if (commerceItemToRemove.getQuantity() - ri.getQuantity() <= 0) {
                    getOrderManager().getCommerceItemManager().removeItemFromOrder(originalOrder, commerceItemToRemove.getId());
                    // also remove the rewardsshare entry for this item from the
                    // itemsRewardsShareMap.
                    itemsRewardsShareMap.remove(commerceItemToRemove.getId());
                } else {
                    commerceItemToRemove.setQuantity(commerceItemToRemove.getQuantity() - ri.getQuantity());

                }
            }

		} else if( item.getType().equals( EXCHANGE ) ) {

			ExchangeItem ei = (ExchangeItem)item;
			// create a commerce item for new item
			CommerceItem ci = getOrderManager().getCommerceItemManager().createCommerceItem( ei.getLineItemId(), getOrderManager().getCommerceItemManager().getProductIdFromSkuId( ei.getLineItemId() ), ei.getQuantity() );
			// check if even exchange, then use the same price list as the item
			// in the cart that is being returned/removed
			// for uneven exchanges, the default price list ( on the profile )
			// will be used.

			if( ei.isEvenExchange() ) {
				if( isLoggingDebug() ) logDebug( "Here is the EVEN EXCHANGE" );
				CommerceItem eri = (CommerceItem)returns.get( ei.getReturnedLineItemId() );
				commerceItemPriceLists.put( ci.getId(), commerceItemPriceLists.get( eri.getId() ) );
				if( isLoggingDebug() ) logDebug( "Here is the EVEN EXCHANGE for " + ci.getCatalogRefId() + " pricelist " + commerceItemPriceLists.get( ci.getId() ) );
			}

			// for all items being exchanged, set it to be exempt from
			// orderRewardsShare
			MutableRepository mutRep = (MutableRepository)getOrderManager().getOrderTools().getOrderRepository();
			MutableRepositoryItem ciMutItem = mutRep.getItemForUpdate( ci.getId(), "commerceItem" );
			ciMutItem.setPropertyValue( "excludeFromRewardsCert", Boolean.valueOf( "true" ) );

			// now that we have created the exchange item, we add to cart and
			// shipping group.
			getOrderManager().getCommerceItemManager().addAsSeparateItemToOrder( originalOrder, ci );
			getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup( originalOrder, ci.getId(), ei.getShippingGroupId(), ei.getQuantity() );
			ShippingGroupCommerceItemRelationship sgci = getOrderManager().getShippingGroupManager().getShippingGroupCommerceItemRelationship( originalOrder, ci.getId(), ei.getShippingGroupId() );
			MutableRepositoryItem sgciMutItem = mutRep.getItemForUpdate( sgci.getId(), "shipItemRel" );
			if( isLoggingDebug() ) logDebug( "===============ORDERLINEKEY============" + ei.getOrderLineKey() );
			sgciMutItem.setPropertyValue( "orderLineKey", ei.getOrderLineKey() );
		}

	}

	private Map adjustTheItemsShareCertAmount( RepositoryItem orderItem, Order originalOrder, Map itemRewardsShareMap ) throws CommerceItemNotFoundException, InvalidParameterException {

		// now lets figure out what the order rewards certificate amount
		// authorized is. For this we will iterate through all the
		// commerce items and see which of them have certs still applied to it.
		// Get the total per cert across all Commerce items.
		// do two things after this. 1) set a flag on the remaining
		// commerceitems to exempt them from Rewards cert distribution. 2)
		// update the
		// orders certs 'amountAuthorized.

		Map certsValue = new HashMap( 10 );
		if( isLoggingDebug() ) {

            for (Object o : itemRewardsShareMap.keySet()) {
                String id = (String) o;
                List l = (List) itemRewardsShareMap.get(id);
                for (Object aL : l) {
                    RewardCertificateItem rci = (RewardCertificateItem) aL;
                    logDebug("CommerceItem " + id);
                    logDebug("" + rci.getCertAmountUsed());
                    logDebug(rci.getCertNumber());
                }
            }
		}

        for (Object o : itemRewardsShareMap.keySet()) {
            String commerceItemId = (String) o;
            List l = (List) itemRewardsShareMap.get(commerceItemId);
            for (Object aL : l) {
                RewardCertificateItem rci = (RewardCertificateItem) aL;

                // this is the scenario where, CSR can cancel one or more qty of
                // "an" item (whose qty in orginal order >1).
                // Yantra will simply reduce the qty on the CommerceItem, by the
                // amount to cancel.
                // Therefore , we have to adjust CI share of the cert (for that
                // item) by the qty change
                // for e.g if 3qty of an item now is reduced to 2 qty for that
                // item (i.e 1 qtyof that item cancelled),
                // then the CI share of the cert is 2/3 of the original CI share
                // for this cert.

                adjustAmountUsedOnCertByCIQty(originalOrder, commerceItemId, rci);
                if (certsValue.get(rci.getCertNumber()) != null) {
                    double currentValue = Double.parseDouble((String) certsValue.get(rci.getCertNumber()));
                    certsValue.put(rci.getCertNumber(), "" + (currentValue + rci.getCertAmountUsed()));

                } else {
                    certsValue.put(rci.getCertNumber(), "" + rci.getCertAmountUsed());
                }
            }
        }
		return certsValue;

	}

	private void adjustTheOrderRewardsCertAmount( RepositoryItem orderItem, Order originalOrder, Map certsValue ) {

		// adjust the orders certs amountAuthorized. Once we set this, the
		// calculator will not adjust the order total and the rewards share
		// across
		// commerce items.
		Object[] orderRewards = ( (Set)orderItem.getPropertyValue( "rewardCertificates" ) ).toArray();
		MutableRepositoryItem rewardCertificate;
		String certNumber;
		String certId;
		String certKey;

        for (Object orderReward : orderRewards) {
            rewardCertificate = (MutableRepositoryItem) orderReward;
            certNumber = (String) rewardCertificate.getPropertyValue("certificateNumber");
            certId = rewardCertificate.getRepositoryId();
            certKey = certNumber + "-" + certId;
            if (certsValue.get(certKey) != null) {
                if (isLoggingDebug())
                    logDebug("CERTNUMBER " + certNumber + " VALUE IS " + Double.valueOf((String) certsValue.get(certKey)));
                rewardCertificate.setPropertyValue("amountAuthorized", Double.valueOf((String) certsValue.get(certKey)));
            } else {
                if (isLoggingDebug()) logDebug("AMT AUTH is 0.0");
                rewardCertificate.setPropertyValue("amountAuthorized", Double.valueOf("0.0"));
            }
        }
	}

	private Order repriceOrder( RepositoryItem orderItem, Order originalOrder, List newPromos, Map commerceItemPriceLists, Map itemRewardsShareMap, 
			List certs, String repriceType, String shDriverDate, String orderTier, String loyaltyNumber, boolean repricingProfile )
			throws RepositoryException, RunProcessException, RepricingServicesException {

		RepositoryItem profile;

		// get our repricing user from the incoming order. if that turns out
		// null, then use the default repricingUser

		profile = getProfileFromRespository( originalOrder.getProfileId() );

		if(null != profile) {
            if (isLoggingDebug()) logDebug("PROFILE USED FOR ORDER " + profile.getRepositoryId());

            MutableRepository oldOrderProfileMutable = (MutableRepository) profile.getRepository();
            MutableRepositoryItem oldOrderProfileMutableItem = oldOrderProfileMutable.getItemForUpdate(profile.getRepositoryId(), profile.getItemDescriptor().getItemDescriptorName());

            // Check to see if the loyalty tier is set correctly in the Profile
            String profileTier = (String) oldOrderProfileMutableItem.getPropertyValue("loyaltyTier");
            if (DigitalStringUtil.isEmpty(profileTier)) {
                oldOrderProfileMutableItem.setPropertyValue("loyaltyTier", orderTier);
            }

            // set the REPRICEDATE key in the pExtraParamtersMap, so that items in
            // the original cart are repriced by the correct
            // startdate and enddate on the pricelist.

            commerceItemPriceLists.put( REPRICEDATE, originalOrder.getSubmittedDate() );

            // check if any new valid rewards certificate that can be added to the
            // order

            if( repriceType.equals( SALESTYPE ) && certs.size() > 0 ) {
                try {
                    if( isLoggingDebug() ) {
                        logDebug( "profileId: " + originalOrder.getProfileId() );
                        logDebug( "loyaltyId: " + loyaltyNumber );
                    }
                    String profileId;
                    if(repricingProfile){
                        profileId = null;
                    }else{
                        profileId = originalOrder.getProfileId();
                    }

                    List certStatuses = this.getRewardsCertificateManager().validateCertificatesForRepricing( (String[])certs.toArray( new String[certs.size()] ), profileId, loyaltyNumber );

                    for (Object certStatuse : certStatuses) {
                        LoyaltyCertificate status = (LoyaltyCertificate) certStatuse;
                        if (isLoggingDebug()) logDebug("Cert: " + status.isValid() + " value " + status.getValue());
                        if (status.isValid()) getRewardsCertificateManager().addCertificateToOrder(originalOrder, status);
                    }
                } catch( Exception e ) {
                    logError("Error validateCertificatesForRepricing :: ",e);
                    //logError( getError( e, "repriceOrder", originalOrder.getProfileId(), originalOrder.getId() ) );
                }
            }

            // reprice the order

            CommercePipelineException ce = new CommercePipelineException();

            commerceItemPriceLists.put( SHDRIVERDATE, shDriverDate );

            getPurchaseProcessHelper().runProcessRepriceOrder( PricingConstants.OP_REPRICE_ORDER_TOTAL, originalOrder, getUserPricingModels(), null, oldOrderProfileMutableItem, commerceItemPriceLists, ce );

            // if repricing failed at any step, we should throw back the exception
            // to the calling client
            if( ce.hasErrors() ) throw new RepricingServicesException( ce.getMessage(), ce );
        }
		if( isLoggingDebug() ) {
			logDebug( "---------------Printing out some statistics on the repriced order---------" );
			logDebug( "" + originalOrder.getPriceInfo().getAmount() );
			logDebug( "updating the order after the repricing" );
			logDebug( "Current Cart size after " + originalOrder.getCommerceItemCount() );
			for(int i = 0; i < originalOrder.getCommerceItems().size(); i++ ) {
				logDebug( "CommerceItem catalogRefId after " + ( (CommerceItem) originalOrder.getCommerceItems().get( i ) ).getCatalogRefId() );
				logDebug( "CommerceItem Amount after " + ( (CommerceItem) originalOrder.getCommerceItems().get( i ) ).getPriceInfo().getAmount() );
			}
		}
		
		return originalOrder;
	}

	private void adjustAmountUsedOnCertByCIQty( Order order, String pCommerceItemId, RewardCertificateItem rci ) throws CommerceItemNotFoundException, InvalidParameterException {

		CommerceItem ci = order.getCommerceItem( pCommerceItemId );
		if( isLoggingDebug() ) logDebug( "COMMERCEITEM " + ci.getId() );
		double rawTotalPrice = ci.getPriceInfo().getRawTotalPrice();
		if( isLoggingDebug() ) logDebug( "RAWTOTALPRICE " + rawTotalPrice );
		double listPrice = ci.getPriceInfo().getListPrice();
		if( isLoggingDebug() ) logDebug( "LISTPRICE " + listPrice );
		int originalQty = (int)Math.ceil( rawTotalPrice / listPrice );
		if( isLoggingDebug() ) logDebug( "ORIGINALQTY " + originalQty );
		int currentQty = (int)ci.getQuantity();
		if( isLoggingDebug() ) logDebug( "CURRENT QTY " + currentQty );

		double originalCertAmountUsed = rci.getCertAmountUsed();
		if( isLoggingDebug() ) logDebug( "ORIGINALCERTAMOUNTUSED " + originalCertAmountUsed );
		if( currentQty < originalQty ) {

			double certAmountLeft = getPricingTools().roundDown( ( (double)currentQty / (double)originalQty ) * originalCertAmountUsed );
			if( isLoggingDebug() ) logDebug( "CERTAMOUNTLEFT " + certAmountLeft );
			rci.setCertAmountUsed( certAmountLeft );
		}

	}

	private RepositoryItem getProfileFromRespository( String profileId ) {
		// KTLO1-1140 : Repricing failing due to invalid profile id
		if(DigitalStringUtil.isBlank(profileId)){
			return null;
		}
		// get our repricing user from the incoming order. if that turns out
		// null, then use the default repricingUser
		try {

			CommerceProfileTools profileTools = getOrderManager().getOrderTools().getProfileTools();
			RepositoryItem profile;
			profile = profileTools.getProfileItem(profileId);
			return profile;
		}catch(Exception ex){
			logError("Invalid profileId :: " + profileId, ex);
		}

		return null;
	}

	private List getReturnOrdersFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) throws RepricingServicesException {

		if( isLoggingDebug() ) {
			logDebug( "getReturnOrdersFromXML()" );
		}

		// Create Order's Handler
		ReturnOrdersHandler oHandler = new ReturnOrdersHandler();

		parser.setHandler( oHandler );
		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( oHandler.getErrorMessage() != null ) throw new RepricingServicesException( "Error reading ReturnOrders from payload :: " + oHandler.getErrorMessage() );

		return oHandler.getReturnOrders();
	}

	private List getExchangeOrdersFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) throws RepricingServicesException {

		if( isLoggingDebug() ) {
			logDebug( "getExchangeOrdersFromXML()" );
		}

		// Create Order's Handler
		ExchangeOrdersHandler oHandler = new ExchangeOrdersHandler();

		parser.setHandler( oHandler );
		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( oHandler.getErrorMessage() != null ) throw new RepricingServicesException( "Error reading ExchangeOrders from payload :: " +  oHandler.getErrorMessage() );

		return oHandler.getExchangeOrders();
	}

	private Map getItemRewardsShareFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) throws RepricingServicesException {

		if( isLoggingDebug() ) {
			logDebug( "getItemRewardsShareFromXML()" );
		}

		// Create Order's Handler
		ItemRewardsShareHandler oHandler = new ItemRewardsShareHandler();

		parser.setHandler( oHandler );
		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( oHandler.getErrorMessage() != null ) throw new RepricingServicesException( "Error reading order:commerceItem.rewardCertificateShare from payload:: " + oHandler.getErrorMessage() );

		return oHandler.getCiCerts();
	}

	private List getNewPromosFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) throws RepricingServicesException {
		if( isLoggingDebug() ) {
			logDebug( "getNewPromosFromXML()" );
		}

		// Create Order's Handler
		NewPromosHandler oHandler = new NewPromosHandler();

		parser.setHandler( oHandler );
		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( oHandler.getErrorMessage() != null ) throw new RepricingServicesException( "Error reading NewPromotions from payload :: " + oHandler.getErrorMessage() );

		return oHandler.getNewPromos();
	}

	private Map getPromoIdsFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) throws RepricingServicesException {

		if( isLoggingDebug() ) {
			logDebug( "getPromoIdsFromXML()" );
		}

		// Create Order's Handler
		PromoIdHandler oHandler = new PromoIdHandler();

		parser.setHandler( oHandler );
		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( isLoggingDebug() ) logDebug( "\n\n The Order.xml parsed - found promoIds in Orders: " + oHandler.getAdjPromos() );

		if( oHandler.getErrorMessage() != null ) throw new RepricingServicesException( "Error reading PromoIds from payload :: " +  oHandler.getErrorMessage() );

		return oHandler.getAdjPromos();
	}

	private String getShDriverDateFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) throws RepricingServicesException {
		if( isLoggingDebug() ) {
			logDebug( "getOrderIdFromXML()" );
		}

		// Create Order's Handler
		ShDriverDateHandler oHandler = new ShDriverDateHandler();

		parser.setHandler( oHandler );
		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( isLoggingDebug() ) logDebug( "\n\n The Order.xml parsed - found repository in Orders: " + oHandler.getShDriverDate() );

		if( oHandler.getErrorMessage() != null ) throw new RepricingServicesException( "Error reading order:order.shDriverDate from payload" + oHandler.getErrorMessage() );

		return oHandler.getShDriverDate();
	}

	private String getOrderIdFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) throws RepricingServicesException {
		if( isLoggingDebug() ) {
			logDebug( "getOrderIdFromXML()" );
		}

		// Create Order's Handler
		OrderRepositoryIdHandler oHandler = new OrderRepositoryIdHandler();

		parser.setHandler( oHandler );
		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( isLoggingDebug() ) logDebug( "\n\n The Order.xml parsed - found repository in Orders: " + oHandler.getOrderRepositoryId() );

		if( oHandler.getErrorMessage() != null ) throw new RepricingServicesException( "Error reading order:order.id from payload" +  oHandler.getErrorMessage() );

		return oHandler.getOrderRepositoryId();
	}

	private String getProfileIdFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) throws RepricingServicesException {
		if( isLoggingDebug() ) {
			logDebug( "getProfileIdFromXML()" );
		}

		// Create Order's Handler
		ProfileIdHandler oHandler = new ProfileIdHandler();

		parser.setHandler( oHandler );
		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( isLoggingDebug() ) logDebug( "\n\n The Order.xml parsed - found Profile Id in Orders: " + oHandler.getProfileId() );

		if( oHandler.getErrorMessage() != null ) throw new RepricingServicesException( "Error reading profileId from payload" + oHandler.getErrorMessage() );

		return oHandler.getProfileId();
	}

	private String getSiteIdFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) throws RepricingServicesException {
		if( isLoggingDebug() ) {
			logDebug( "getProfileIdFromXML()" );
		}

		// Create Order's Handler
		SiteIdHandler oHandler = new SiteIdHandler();

		parser.setHandler( oHandler );
		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( isLoggingDebug() ) {
			logDebug( "\n\n The Order.xml parsed - found Site Id in Orders: " + oHandler.getSiteId() );
		}

		if( oHandler.getErrorMessage() != null ) throw new RepricingServicesException( "Error reading siteId from payload" + oHandler.getErrorMessage() );

		return oHandler.getSiteId();
	}
	
	private String getLocaleFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) throws RepricingServicesException {
		if( isLoggingDebug() ) {
			logDebug( "getProfileIdFromXML()" );
		}

		// Create Order's Handler
		LocaleHandler oHandler = new LocaleHandler();

		parser.setHandler( oHandler );
		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( isLoggingDebug() ) logDebug( "\n\n The Order.xml parsed - found locale in Orders: " + oHandler.getLocale() );

		if( oHandler.getErrorMessage() != null ) throw new RepricingServicesException( "Error reading locale from payload" + oHandler.getErrorMessage() );

		return oHandler.getLocale();
	}
	
	private String getLoyaltyTierFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) throws RepricingServicesException {
		if( isLoggingDebug() ) {
			logDebug( "getLoyaltyTierFromXML()" );
		}

		// Create Order's Handler
		OrderInfoHandler oHandler = new OrderInfoHandler();

		parser.setHandler( oHandler );
		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( isLoggingDebug() ) logDebug( "\n\n The Order.xml parsed - found Loyalty Tier in Orders: " + oHandler.getLoyaltyTier() );

		if( oHandler.getErrorMessage() != null ) throw new RepricingServicesException("Error reading order:order.loyaltyTier from payload" + oHandler.getErrorMessage() );

		return oHandler.getLoyaltyTier();
	}

    private String getRepricingUserId(String loyaltyTier) {
        if (!DigitalStringUtil.isEmpty(loyaltyTier)) {
            switch (loyaltyTier) {
                case PREMIERTIER:
                    return MultiSiteUtil.getRepricingPrimProfileId();
                case BASICTIER:
                    return MultiSiteUtil.getRepricingBasicProfileId();
                case CLUBTIER:
                    return MultiSiteUtil.getRepricingClubProfileId();
                case GOLDTIER:
                    return MultiSiteUtil.getRepricingGoldProfileId();
                case ELITETIER:
                    return MultiSiteUtil.getRepricingEliteProfileId();
                default:
                    return MultiSiteUtil.getRepricingAnonyProfileId();
            }
        }
        return MultiSiteUtil.getRepricingAnonyProfileId();
    }

	private Map getPriceListIdsFromXML( CreateParser parser, ByteArrayInputStream readOrderFromFile ) {
		if( isLoggingDebug() ) {
			logDebug( "getPriceListIdsFromXML()" );
		}

		// Create Order's Handler
		PriceListIdHandler oHandler = new PriceListIdHandler();

		parser.setHandler( oHandler );

		// Parse the XML file, handler generates the output
		parser.parse( readOrderFromFile );

		if( isLoggingDebug() ) logDebug( "\n\n The Order.xml parsed - found priceListIds in  Orders: " + oHandler.getCIPriceLists() );

		return oHandler.getCIPriceLists();
	}

	protected PricingModelHolder getUserPricingModels() {
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();

        return (PricingModelHolder)request.resolveName( getOrderServices().getPricingModelHolderPath() );
	}

	private RepositoryItem getPromotionById( String id ) throws RepositoryException {
		MutableRepository mr = (MutableRepository)getPromotionRepository();
		RepositoryView catRep = getRepositoryView( mr, "promotion" );
		if( catRep == null ) return null;
		Query q = getNamedQuery( catRep, "findPromotion" );
		if( q == null ) return null;
		Object[] params = { id };
		ParameterSupportView psv = (ParameterSupportView)catRep;
		RepositoryItem[] promos = psv.executeQuery( q, params );
		if( promos == null || promos.length <= 0 ) {
			if( isLoggingDebug() ) logDebug( "No Promotion found" );
			return null;
		}
		RepositoryItem item = promos[0];
		if( isLoggingDebug() ) logDebug( "Found Promo :  " + item.getRepositoryId() + "/" + item.getItemDisplayName() );

		return item;
	}

	private RepositoryView getRepositoryView( MutableRepository mr, String viewString ) {
		try {
            return mr.getView( viewString );
		} catch( Exception ex ) {
			if( isLoggingError() ) {
				logError( "There is no view called " + viewString );
				logError( ex );
			}
		}
		return null;
	}

	/** Get NamedQuery by name
	 * 
	 * @param rv
	 * @param name
	 * @return Query */
	private Query getNamedQuery( RepositoryView rv, String name ) {
		if( rv == null ) {
			if( isLoggingDebug() ) logDebug( "RepositoryView is null from" );
			return null;
		}

		if( rv instanceof NamedQueryView ) {
			NamedQueryView nqv = (NamedQueryView)rv;
			return nqv.getNamedQuery( name );
		} else {
			if( isLoggingDebug() ) logDebug( "RepositoryView rv : is not a NamedQeuryView " + rv.getClass().getName() );
		}
		return null;
	}

	/** This method ensures that a transaction exists before returning. If there
	 * is no transaction, a new one is started and returned. In this case, you
	 * must call commitTransaction when the transaction completes.
	 * 
	 * @return a <code>Transaction</code> value
	 * @throws SystemException
	 * @throws NotSupportedException */
	protected Transaction ensureTransaction() throws SystemException, NotSupportedException {
		TransactionManager tm = getTransactionManager();
		Transaction t = tm.getTransaction();
		if( t == null ) {
			tm.begin();
			t = tm.getTransaction();
			return t;
		}
		return null;
	}

	/** Commits the supplied transaction
	 * 
	 * @param pTransaction
	 *            a <code>Transaction</code> value
	 * @throws SystemException
	 * @throws IllegalStateException
	 * @throws HeuristicRollbackException
	 * @throws HeuristicMixedException
	 * @throws RollbackException
	 * @throws SecurityException */
	private void commitTransaction( Transaction pTransaction, Order originalOrder, boolean rollBack ) 
			throws IllegalStateException, SystemException, SecurityException, RollbackException, HeuristicMixedException, HeuristicRollbackException {
		
		boolean exception = false;

		if( pTransaction != null ) {
			try {
				if( isTransactionMarkedAsRollBack() || rollBack) {
                    exception = true;
					pTransaction.rollback(); // PR65109: rollback() before
												// invalidateOrder() prevent
												// deadlocks due to thread
												// synchronization in the
												// invalidateOrder()
					if( originalOrder != null && originalOrder instanceof OrderImpl ) ( (OrderImpl)originalOrder ).invalidateOrder();
				} else {
					pTransaction.commit();
				}
			} finally {
				if( exception ) {
					if( originalOrder != null && originalOrder instanceof OrderImpl ) ( (OrderImpl)originalOrder ).invalidateOrder();

				} // if
			} // finally
		} // if
	}

	/** Returns true if the transaction associated with the current thread is
	 * marked for rollback. This is useful if you do not want to perform some
	 * action (e.g. updating the order) if some other subservice already needs
	 * the transaction rolledback.
	 * 
	 * @return a <code>boolean</code> value */
	private boolean isTransactionMarkedAsRollBack() {
		try {
			TransactionManager tm = getTransactionManager();
			if( tm != null ) {
				int transactionStatus = tm.getStatus();
				if( transactionStatus == javax.transaction.Status.STATUS_MARKED_ROLLBACK ) return true;
			}
		} catch( SystemException exc ) {
			if( isLoggingError() ) logError( exc );
		}
		return false;
	}

	private int hexval( char c ) throws IOException {

		switch( c ) {
		case '0':
			return 0;
		case '1':
			return 1;
		case '2':
			return 2;
		case '3':
			return 3;
		case '4':
			return 4;
		case '5':
			return 5;
		case '6':
			return 6;
		case '7':
			return 7;
		case '8':
			return 8;
		case '9':
			return 9;

		case 'a':
		case 'A':
			return 10;
		case 'b':
		case 'B':
			return 11;
		case 'c':
		case 'C':
			return 12;
		case 'd':
		case 'D':
			return 13;
		case 'e':
		case 'E':
			return 14;
		case 'f':
		case 'F':
			return 15;
		}
		throw new IOException(); // Should never come here
	}

	/** Internal method to decode the XML-compatible encoded string to java
	 * string.
	 * 
	 * @param pString
	 *            an XML identifier that is possibly encoded. This will be
	 *            decoded into a Repository identifier.
	 * @return a repository identifier */
	String decodeFromXML( String pString ) {

		if( pString == null ) return null;

		int length = pString.length();

		StringBuilder buff = new StringBuilder();

		for( int i = 0; i < length; i++ ) {
			int ch = (int)pString.charAt( i );

			// check whether char starts with '_',
			// it may mean two things, either hex value(_XXXX) or '_' in
			// original
			// string(__)
			if( ch == '_' ) {
				++i;
				if( (int)pString.charAt( i ) == '_' ) {
					buff.append( "_" );
				} else {
					// convert from unicode to decimal.

					try {
						buff.append( (char)( hexval( pString.charAt( i++ ) ) << 12 | hexval( pString.charAt( i++ ) ) << 8 | hexval( pString.charAt( i++ ) ) << 4 | hexval( pString.charAt( i ) ) ) );
					} catch( IOException io ) {
						String excMsg = XMLSchemaConstants.ERROR_DECODING_IDENTIFIER;
						Object[] args = { pString };
						throw new Error( MessageFormat.format( excMsg, args ) );
					}

				} // end of else

			} else {
				// regular character.
				buff.append( (char)ch );

			} // end of else

		} // end of for ()

		if( buff.length() > 0 )

		return buff.toString();

		return pString;

	}

	public void setPricingTools( PricingTools pPricingTools ) {
		mPricingTools = pPricingTools;
	}

	public PricingTools getPricingTools() {
		if( mPricingTools == null ) {
			mPricingTools = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PRICE_TOOLS, PricingTools.class);
		}
		return mPricingTools;
	}

	public void setPurchaseProcessHelper( PurchaseProcessHelper pPurchaseProcessHelper ) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	public PurchaseProcessHelper getPurchaseProcessHelper() {
		if( mPurchaseProcessHelper == null ) {
			mPurchaseProcessHelper = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PURCHASE_PROCESS,PurchaseProcessHelper.class);
		}
		return mPurchaseProcessHelper;
	}

	public void setOrderManager( OrderManager pOrderManager ) {
		mOrderManager = pOrderManager;
	}

	public OrderManager getOrderManager() {
		if( mOrderManager == null ) {
			mOrderManager = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.ORDER_MANAGER,OrderManager.class);
		}

		return mOrderManager;
	}

	public void setPromotionRepository( Repository pPromotionRepository ) {
		mPromotionRepository = pPromotionRepository;
	}

	public Repository getPromotionRepository() {
		if( mPromotionRepository == null ) {
			mPromotionRepository = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PROMOTIONS,Repository.class);
		}

		return mPromotionRepository;
	}

	public void setPriceListManager( PriceListManager pPriceListManager ) {
		mPriceListManager = pPriceListManager;
	}

	public PriceListManager getPriceListManager() {
		if( mPriceListManager == null ) {
			mPriceListManager = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PRICE_LIST_MANAGER, PriceListManager.class);
		}
		return mPriceListManager;
	}

	public void setCatalogTools( CatalogTools pCatalogTools ) {
		mCatalogTools = pCatalogTools;
	}

	public CatalogTools getCatalogTools() {
		if( mCatalogTools == null ) {
			mCatalogTools = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.CATALOG_TOOLS, CatalogTools.class);
		}
		return mCatalogTools;
	}

	public void setPromotionTools( PromotionTools pPromotionTools ) {
		mPromotionTools = pPromotionTools;
	}

	public PromotionTools getPromotionTools() {
		if( mPromotionTools == null ) {
			mPromotionTools = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PROMOTION_TOOLS, PromotionTools.class);
		}
		return mPromotionTools;
	}

	public MessageLocator getMessageLocator() {
		if( messageLocator == null ) {
			messageLocator = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.MESSAGE_LOCATOR,MessageLocator.class);
		}
		return messageLocator;
	}

	public void setMessageLocator( MessageLocator messageLocator ) {
		this.messageLocator = messageLocator;
	}

	public void setTransactionManager( TransactionManager pTransactionManager ) {
		mTransactionManager = pTransactionManager;
	}

	public TransactionManager getTransactionManager() {
		if( mTransactionManager == null ) {
			mTransactionManager = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.TRANSACTION_MANAGER, TransactionManager.class);
		}
		return mTransactionManager;
	}

	public void setOrderServices( OrderServices pOrderServices ) {
		mOrderServices = pOrderServices;
	}

	public OrderServices getOrderServices() {
		if( mOrderServices == null ) {
			mOrderServices = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.ORDER_SERVICES,OrderServices.class);
		}
		return mOrderServices;
	}

	public void setRewardsCertificateManager( DigitalRewardsManager pRewardsCertificateManager ) {
		mRewardsCertificateManager = pRewardsCertificateManager;
	}

	public DigitalRewardsManager getRewardsCertificateManager() {
		if( mRewardsCertificateManager == null ) {
			mRewardsCertificateManager = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.REWARD_CERTIFICATE_MANAGER, DigitalRewardsManager.class );
		}
		return mRewardsCertificateManager;
	}

	public RepricingConstants getRepricingConstants() {
		if( repricingConstants == null ) {
			repricingConstants = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.REPRICING_CONSTANTS, RepricingConstants.class);
		}

		return repricingConstants;
	}

	public void setRepricingConstants( RepricingConstants pRepricingConstants ) {
		this.repricingConstants = pRepricingConstants;
	}

	private Order determineWhiteGloveOrder( Order originalOrder ) {

		boolean luxury = false;
		boolean whiteGlove = false;
		boolean whiteGloveOrder = false;

		BigDecimal orderSubTotal = new BigDecimal( originalOrder.getPriceInfo().getRawSubtotal() ).setScale( 2, RoundingMode.HALF_UP );
		BigDecimal orderThreshold = ( (DigitalOrderTools)getOrderManager().getOrderTools() ).getWhiteGloveOrderThreshold().setScale( 2, RoundingMode.HALF_UP );
		int compareValue = orderSubTotal.compareTo( orderThreshold );

		Repository rep = this.getCatalogTools().getCatalog();

		if( originalOrder instanceof DigitalOrderImpl ) {
			List<DigitalCommerceItem> commerceItems = originalOrder.getCommerceItems();
			if( commerceItems != null ) {
				for( DigitalCommerceItem item : commerceItems ) {
					if( item != null ) {
						String skuId = item.getCatalogRefId();
						RepositoryItem sku;
						try {
							sku = rep.getItem( skuId, "sku" );
							Boolean whiteGloveItem = (Boolean)sku.getPropertyValue( "isWhiteGloveEligible" );
							if( Boolean.TRUE.equals( whiteGloveItem ) ) whiteGlove = true;

							Boolean luxuryItem = (Boolean)sku.getPropertyValue( "isLuxury" );
							if( Boolean.TRUE.equals( luxuryItem ) ) luxury = true;

						} catch( RepositoryException re ) {
							if( isLoggingError() ) {
								logError( "Repricing: determineWhiteGloveOrder failed for order " + originalOrder.getId() + "because there is no sku " + skuId + "in ATG" );
								logError( "Cause : " + re.getCause() );
								logError( "Message : " + re.getMessage() );
							}
						} catch( Exception e ) {
							if( isLoggingError() ) {
								logError( "Repricing: determineWhiteGloveOrder failed for order " + originalOrder.getId() );
								logError( "Cause : " + e.getCause() );
								logError( "Message : " + e.getMessage() );
							}
						}
					}
				}

				if( whiteGlove || ( compareValue >= 0 && luxury ) ) whiteGloveOrder = true;
			}
		}
		( (DigitalOrderImpl)originalOrder ).setPropertyValue( "whiteGloveOrder", whiteGloveOrder );
		return originalOrder;
	}
	
	public String getError( Exception e, String methodName, String profileId, String orderNumber, String... detail ) {
		return String.format( "[%1$s {methodName: %2$s, order: %3$s, profile: %4$s, detail: %5$s, failureReason: %6$s, message: %7$s, causeType: %8$s, causeMessage: %9$s}]", e.getClass().getSimpleName(), methodName, DigitalStringUtil
				.trimToEmpty( orderNumber ), DigitalStringUtil.trimToEmpty( profileId ), DigitalStringUtil.join( detail ), e.getMessage(), DigitalStringUtil.trimToEmpty( e.getMessage() ), e.getCause() != null ? e.getCause().getClass().getName()
				: DigitalStringUtil.EMPTY, e.getCause() != null ? DigitalStringUtil.trimToEmpty( e.getCause().getMessage() ) : DigitalStringUtil.EMPTY );
	}

	public DigitalProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(DigitalProfileTools mProfileTools) {
		this.mProfileTools = mProfileTools;
	}
	
	public GWPMarkerManager getGwpMarkerManager() {
		
		if(null!=gwpMarkerManager){
			return gwpMarkerManager;
		}else{
			return ComponentLookupUtil.lookupComponent(GWP_MARKER_MANAGER,GWPMarkerManager.class);
		}
	}

	public void setGwpMarkerManager(GWPMarkerManager gwpMarkerManager) {
		this.gwpMarkerManager = gwpMarkerManager;
	}
}
