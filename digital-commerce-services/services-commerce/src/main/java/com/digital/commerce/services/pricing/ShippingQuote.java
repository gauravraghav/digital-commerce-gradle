/**
 * 
 */
package com.digital.commerce.services.pricing;

import lombok.Getter;
import lombok.Setter;

/** @author wibrahim */
@Getter
@Setter
public class ShippingQuote {
	private double	grossShippingAndHandling;
	private double	netShippingAndHandling;
	private double	shippingAndHandlingSavings;
	private String	serviceLevel;
}
