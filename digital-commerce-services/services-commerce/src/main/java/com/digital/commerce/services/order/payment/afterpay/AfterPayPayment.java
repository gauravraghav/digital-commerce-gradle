package com.digital.commerce.services.order.payment.afterpay;

import atg.commerce.CommerceException;
import atg.commerce.order.*;
import atg.core.util.ContactInfo;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.constants.DigitalAfterPayConstants;
import lombok.Getter;

@Getter
public class AfterPayPayment extends PaymentGroupImpl implements AfterPayPaymentInfo{

    public AfterPayPayment() {
    }

    private String token;

    private ContactInfo billingAddress = null;

    public void setBillingAddress(ContactInfo pBillingAddress) {
        if (pBillingAddress instanceof RepositoryContactInfo) {
            if (this.billingAddress != null)
                this.billingAddress.deleteObservers();
            this.billingAddress = pBillingAddress;
            this.billingAddress.addObserver(this);
        } else {
            try {
                OrderTools.copyAddress(pBillingAddress, this.billingAddress);
            } catch (CommerceException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
        setSaveAllProperties(true);
    }


    @Override
    public Order getOrder() {
        OrderHolder orderHolder =  ComponentLookupUtil.lookupComponent( ComponentLookupUtil.SHOPPING_CART, OrderHolder.class  );
        if(orderHolder != null){
            return orderHolder.getCurrent();
        }else{
            return null;
        }
    }

    public String getToken() {
        return (String)getPropertyValue(DigitalAfterPayConstants.AFTER_PAY_PAYMENT_TOKEN_PROPERTY);
    }

    public void setToken(String token) {
        setPropertyValue(DigitalAfterPayConstants.AFTER_PAY_PAYMENT_TOKEN_PROPERTY, token );
    }
}
