/**
 * 
 */
package com.digital.commerce.services.order.processor;

import atg.commerce.CommerceException;
import atg.commerce.order.processor.ProcSavePaymentStatusObjects;
import atg.payment.PaymentStatus;

import com.digital.commerce.services.order.payment.afterpay.processor.AfterPayPaymentStatus;
import com.digital.commerce.services.order.payment.giftcard.GiftCardStatus;


public class DigitalSavePaymentStatusObjects extends ProcSavePaymentStatusObjects {
	protected String getStatusDescriptorName( PaymentStatus pStatusObject ) throws CommerceException {
		String descName = super.getStatusDescriptorName( pStatusObject );
		if( descName == null || descName.equals( this.getPaymentStatusDescName() ) ) {
			if( pStatusObject instanceof GiftCardStatus ) { return "giftCardStatus"; }
		}
		if( descName == null || descName.equals( this.getPaymentStatusDescName() ) ) {
			if( pStatusObject instanceof AfterPayPaymentStatus) { return "afterPayPaymentStatus"; }
		}
		return null;
	}
}
