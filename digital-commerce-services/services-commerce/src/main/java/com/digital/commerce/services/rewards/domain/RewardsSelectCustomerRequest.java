/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author ravi shankar
 *
 */
@Getter
@Setter
public class RewardsSelectCustomerRequest {
	private String profileId;
}
