/**
 *
 */
package com.digital.commerce.services.order.payment.creditcard;

import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.payment.PaymentAuthorizationService;
import com.digital.commerce.integration.payment.vantiv.domain.FraudCheckServiceResponse;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentAuthorizationServiceContact;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentAuthorizationServiceRequest;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentAuthorizationServiceRequest.GiftCardType;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentAuthorizationServiceResponse;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentRegisterTokenServiceRequest;
import com.digital.commerce.integration.payment.vantiv.domain.PaymentRegisterTokenServiceResponse;
import com.digital.commerce.integration.payment.vantiv.VantivPaymentService;
import com.digital.commerce.services.common.AddressType;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalCommerceItemManager;
import com.digital.commerce.services.order.DigitalCreditCard;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderManager;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.order.GiftCardCommerceItem;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShipType;
import com.digital.commerce.services.utils.DigitalServiceConstants;

import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupNotFoundException;
import atg.commerce.order.ShippingGroup;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

/**
 * Class is responsible for perform required transactions on credit cards
 *
*
 */
 @SuppressWarnings({"unchecked"})
 @Getter
 @Setter
 public class CreditCardServices {
	
	private static final DigitalLogger logger = DigitalLogger.getLogger( CreditCardServices.class );
	
	private DigitalServiceConstants dswServiceConstants;
	
	private VantivPaymentService vantivPaymentService;
	
	private DigitalOrderManager orderManager;
	
	private static final String SERVICE_NAME = "CreditCardServices";
	
	private String				discoverCardTypes;
	
	private String				visaCardTypes;
	
	private String				masterCardTypes;
	
	private String				amexcardCardTypes;
	
	private String 				creditCardTypeCodeVI;
	
	private String 				creditCardTypeCodeDI;
	
	private String 				creditCardTypeCodeMC;
	
	private String 				creditCardTypeCodeAX;
	
	
	/**
	 * method used to perform transaction on a given credit card based on the
	 * transaction type
	 *
	 * @param pTransactionType  string value of the transaction type: debit,credit or
	 *                          authorize
	 * @param pCreditCardInfo   dswcreditcardinfo object
	 * @param pPurchaseQuantity int value of the purchase quantity
	 * @return
	 * @throws DigitalIntegrationException
	 */
	public DigitalCreditCardServiceStatus preAuthorization( String pTransactionType, DigitalCreditCardInfo pCreditCardInfo, int pPurchaseQuantity ) throws DigitalIntegrationException {
		DigitalCreditCardServiceStatus ccStatus = new DigitalCreditCardServiceStatus();
		final String METHOD_NAME = "performTransaction";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
		if( null != pTransactionType && null != pCreditCardInfo && null != vantivPaymentService && getVantivPaymentService().isServiceEnabled()) {
			if( logger.isDebugEnabled() ) {
				logger.debug( "creditcardservices: preformTransaction(): - Start" );
			}
			try {
				//Vantiv request
				PaymentAuthorizationServiceRequest paymentAuthorizationServiceRequest = new PaymentAuthorizationServiceRequest(); 
				paymentAuthorizationServiceRequest.setBillingContact(this.getBillToType(pCreditCardInfo));

				//service needs to know if card is being saved to profile
				paymentAuthorizationServiceRequest.setCardBeingSavedToProfile(pCreditCardInfo.isCardBeingSavedToProfile());

				if (DigitalStringUtil.isNotBlank(pCreditCardInfo.getTokenValue())) {
					paymentAuthorizationServiceRequest.setLitleToken(pCreditCardInfo.getTokenValue());
				} else if (DigitalStringUtil.isNotBlank(pCreditCardInfo.getPaypageRegistrationId())) {
					paymentAuthorizationServiceRequest.setPayPageRegistrationId(pCreditCardInfo.getPaypageRegistrationId());
				}
				//set reporting group for both new card flow and existing flow
				paymentAuthorizationServiceRequest.setReportGroup(dswServiceConstants.getVantivPayPageReportGroup());
				
				// Changes to send an GC or EGC indicator for preAuth Fraud Validation
				DigitalOrderImpl order= (DigitalOrderImpl)pCreditCardInfo.getOrder();
				paymentAuthorizationServiceRequest.setGiftCardType(this.getGiftCardTypes(order));
				paymentAuthorizationServiceRequest.setShipTypes(this.getShipTypes(order));
				
				// To add leading 0 to the month if it's jan - sep
				String expMonth = pCreditCardInfo.getExpirationMonth();
				if( expMonth != null && expMonth.length() < 2 ) {
					expMonth = "0" + expMonth;
				}
				//do check to strip off the last 2 digits of the expiration year as Vantiv expects only the last two digits
				String expYear = getLastTwoDigitsOfYear(pCreditCardInfo.getExpirationYear());
				paymentAuthorizationServiceRequest.setExpirationDate( expMonth + expYear );
				paymentAuthorizationServiceRequest.setCcHolderName(pCreditCardInfo.getNameOnCard());

				// For Diners and JCB the card type should be sent as discover.
				String cardType = pCreditCardInfo.getCreditCardType();
				if(logger.isDebugEnabled()){
					logger.debug("Card Type from request :: " + cardType);
				}
				paymentAuthorizationServiceRequest.setTypeOfCard(cardType);
				paymentAuthorizationServiceRequest.setOrderNumber(pCreditCardInfo.getOrder().getId());
				//setting values for authorization id and customer id assuming this as the order id as found the in sample vanitv request sent from GIS team :)...
				
				//Setting the brand Names for each commerceItems 
				paymentAuthorizationServiceRequest.setBrandNames(getBrandNames(order));
				paymentAuthorizationServiceRequest.setId(pCreditCardInfo.getOrder().getId());
				paymentAuthorizationServiceRequest.setCustomerId(pCreditCardInfo.getOrder().getId());
				
				//Setting CVV number. Platform will not store cvv
				paymentAuthorizationServiceRequest.setCreditCardCid(pCreditCardInfo.getCvvNumber());	
	
				// Added to handle split tender
				double gcPaymentAmount = getGCpaymentAmount( pCreditCardInfo );		
				double splitamount = (gcPaymentAmount > 0) ? (pCreditCardInfo.getOrder().getPriceInfo().getTotal() - gcPaymentAmount) : pCreditCardInfo.getOrder().getPriceInfo().getTotal();
				paymentAuthorizationServiceRequest.setPurchaseTotal(new BigDecimal(splitamount).setScale( 2, RoundingMode.HALF_UP ));

				logger.info( "Vantiv Credit card authorized amount sent =" + paymentAuthorizationServiceRequest.getPurchaseTotal() + ",OrderId=" + paymentAuthorizationServiceRequest.getOrderNumber() + ",cardType=" + cardType );
				
				paymentAuthorizationServiceRequest.setShippingContact( getShipTo( pCreditCardInfo) );

				if(getDswServiceConstants().isOverrideThreatMetrixId()){
					paymentAuthorizationServiceRequest.setThreatMetrixSessionId(getDswServiceConstants().getThreatMetrixId());
				}else{
					paymentAuthorizationServiceRequest.setThreatMetrixSessionId(getDswServiceConstants().getFiveDigitThreatMetrixCode()+"-"+((DigitalOrderImpl)pCreditCardInfo.getOrder()).getId());
				}if( logger.isDebugEnabled() ) {
					logger.debug( "creditcardservices: request.getCreditCardExpirationDate = " + paymentAuthorizationServiceRequest.getExpirationDate());
					logger.debug( "creditcardservices: request.getCreditCardHolderName = " + paymentAuthorizationServiceRequest.getCcHolderName());
					logger.debug( "creditcardservices: request.getCreditCardType = " + paymentAuthorizationServiceRequest.getTypeOfCard());
					logger.debug( "creditcardservices: request.getOrderNumber = " + paymentAuthorizationServiceRequest.getOrderNumber() );
					logger.debug( "creditcardservices: request.getPurchaseTotal = " + paymentAuthorizationServiceRequest.getPurchaseTotal() );
					logger.debug( "creditcardservices: request.getPaypageRegistrationId = " + paymentAuthorizationServiceRequest.getPayPageRegistrationId());
					logger.debug( "creditcardservices: request.getCreditCardCID = " + paymentAuthorizationServiceRequest.getCreditCardCid() );
					logger.debug( "creditcardservices: request.getToken = " + paymentAuthorizationServiceRequest.getLitleToken() );
					logger.debug( "creditcardservices: request.getThreatMetrixSessionId = " + paymentAuthorizationServiceRequest.getThreatMetrixSessionId() );
				}
				//Making the Integration call to get authorization
				PaymentAuthorizationServiceResponse paymentAuthorizationServiceResponse = getVantivPaymentService().preAuth(paymentAuthorizationServiceRequest);
				
				if( paymentAuthorizationServiceResponse != null ) {
					if( logger.isDebugEnabled() ) {
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getAmountAuthorized() = " + paymentAuthorizationServiceResponse.getAmountAuthorized());
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getAuthorizationCode() = " + paymentAuthorizationServiceResponse.getAuthorizationCode());
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getAvsCode() = " + paymentAuthorizationServiceResponse.getAvsCode());
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getDecision() = " + paymentAuthorizationServiceResponse.getDecision() );
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getId() = " + paymentAuthorizationServiceResponse.getId() );
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getAuthorizationExpiration() = " + paymentAuthorizationServiceResponse.getAuthorizationExpiration());
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getFraudFlag() = " + paymentAuthorizationServiceResponse.getFraudFlag() );
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getFraudResultCode() = " + paymentAuthorizationServiceResponse.getFraudResultCode() );
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getFraudResultCodeDescription() = " + paymentAuthorizationServiceResponse.getFraudResultCodeDescription() );
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getFraudRule() = " + paymentAuthorizationServiceResponse.getFraudRule());
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getFraudRuleDescription() = " + paymentAuthorizationServiceResponse.getFraudRuleDescription() );
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getFraudScore() = " + paymentAuthorizationServiceResponse.getFraudScore() );
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getThreadMetrixSessionId() = " + paymentAuthorizationServiceResponse.getThreadMetrixSessionId() );
						logger.debug( "creditcardservices: paymentAuthorizationServiceResponse.getToken() = " + paymentAuthorizationServiceResponse.getToken());
						
					}
					
					if(paymentAuthorizationServiceResponse.getAmountAuthorized()!=null){
						ccStatus.setAmountAuthorized( paymentAuthorizationServiceResponse.getAmountAuthorized().setScale( 2, RoundingMode.HALF_UP ).doubleValue());
					}
					
					ccStatus.setAuthorizationCode( paymentAuthorizationServiceResponse.getAuthorizationCode() );
					ccStatus.setAvsCode( paymentAuthorizationServiceResponse.getAvsCode() );
					ccStatus.setDecision( paymentAuthorizationServiceResponse.getDecision() );
					ccStatus.setReasonCode( paymentAuthorizationServiceResponse.getReasonCode() );
					ccStatus.setTransactionId(String.valueOf(paymentAuthorizationServiceResponse.getTransactionId()));
					if( paymentAuthorizationServiceResponse.getAuthorizationExpiration() != null ) {
							ccStatus.setAuthorizationExpiration(paymentAuthorizationServiceResponse.getAuthorizationExpiration());
					}
					ccStatus.setFraudFlag( paymentAuthorizationServiceResponse.getFraudFlag() );
					ccStatus.setFraudResultCode( paymentAuthorizationServiceResponse.getFraudResultCode() );
					ccStatus.setFraudResultCodeDescription( truncateFraudDesc(paymentAuthorizationServiceResponse.getFraudResultCodeDescription()) );
					ccStatus.setFraudRule( paymentAuthorizationServiceResponse.getFraudRule() );
					ccStatus.setFraudRuleDescription( truncateFraudDesc(paymentAuthorizationServiceResponse.getFraudRuleDescription()) );
					ccStatus.setFraudScore( paymentAuthorizationServiceResponse.getFraudScore()!=null ? 
								paymentAuthorizationServiceResponse.getFraudScore():this.getDswServiceConstants().getDefaultFraudScore());
					ccStatus.setThreatMetrixId(paymentAuthorizationServiceResponse.getThreadMetrixSessionId());
					
					// To set the token value in the commerce tables				
					String tokenValue = "";
					if (DigitalStringUtil.isNotBlank(paymentAuthorizationServiceResponse.getToken())){
						tokenValue = paymentAuthorizationServiceResponse.getToken();
					} else if (DigitalStringUtil.isNotBlank(pCreditCardInfo
							.getTokenValue())) {
						tokenValue = pCreditCardInfo.getTokenValue();
					} else if (DigitalStringUtil.isNotBlank(pCreditCardInfo
							.getPaypageRegistrationId())) {
						tokenValue = pCreditCardInfo.getPaypageRegistrationId();
					}
					ccStatus.setTokenValue(tokenValue);
					
					FraudCheckServiceResponse fraudCheckServiceResponse=null;
					//Check if AVS CODE FRAUD CHECK IS ENABLED
					if(this.getDswServiceConstants().isAvsCodeFraudCheckEnabled()){
						//CALL THIS ONLY IF THE STATUS IS REVIEW OR ACCEPTED REVIEW
						if( paymentAuthorizationServiceResponse != null  && (DigitalCreditCardServiceStatus.DECISION_REVIEW.equalsIgnoreCase(paymentAuthorizationServiceResponse.getDecision()) || DigitalCreditCardServiceStatus.DECISION_ACCEPT.equalsIgnoreCase(paymentAuthorizationServiceResponse.getDecision()) )
								&& DigitalStringUtil.isNotBlank(paymentAuthorizationServiceResponse.getAvsCode())) {
							paymentAuthorizationServiceRequest.setAvsCode(paymentAuthorizationServiceResponse.getAvsCode());
							fraudCheckServiceResponse = getVantivPaymentService().fraudOnly(paymentAuthorizationServiceRequest);
							if( logger.isDebugEnabled() && null!=fraudCheckServiceResponse) {
								logger.debug( "creditcardservices: fraudCheckServiceResponse.getDecision() = " + fraudCheckServiceResponse.getDecision() );
								logger.debug( "creditcardservices: fraudCheckServiceResponse.getId() = " + fraudCheckServiceResponse.getId() );
								logger.debug( "creditcardservices: fraudCheckServiceResponse.getFraudFlag() = " + fraudCheckServiceResponse.getFraudFlag() );
								logger.debug( "creditcardservices: fraudCheckServiceResponse.getFraudResultCode() = " + fraudCheckServiceResponse.getFraudResultCode() );
								logger.debug( "creditcardservices: fraudCheckServiceResponse.getFraudResultCodeDescription() = " + fraudCheckServiceResponse.getFraudResultCodeDescription() );
								logger.debug( "creditcardservices: fraudCheckServiceResponse.getFraudRule() = " + fraudCheckServiceResponse.getFraudRule());
								logger.debug( "creditcardservices: fraudCheckServiceResponse.getFraudRuleDescription() = " + fraudCheckServiceResponse.getFraudRuleDescription() );
								logger.debug( "creditcardservices: fraudCheckServiceResponse.getThreadMetrixSessionId() = " + fraudCheckServiceResponse.getThreadMetrixSessionId() );
								logger.debug( "creditcardservices: fraudCheckServiceResponse..getTransactionId() = " + fraudCheckServiceResponse.getTransactionId());
								
							}
						}
						
						if(null!=fraudCheckServiceResponse ){
							if(!"Approved".equalsIgnoreCase(fraudCheckServiceResponse.getDecision())){
							ccStatus.setFraudFlag(fraudCheckServiceResponse.getFraudFlag());
							ccStatus.setDecision(fraudCheckServiceResponse.getDecision());
							ccStatus.setFraudResultCode(fraudCheckServiceResponse.getFraudResultCode());
							}
							if(null!=fraudCheckServiceResponse.getFraudResultCodeDescription() ){
								String preAuthFraudResultCodeDescription=ccStatus.getFraudResultCodeDescription();
								preAuthFraudResultCodeDescription=fraudCheckServiceResponse.getFraudResultCodeDescription()+preAuthFraudResultCodeDescription;
								ccStatus.setFraudResultCodeDescription(truncateFraudDesc(preAuthFraudResultCodeDescription));
							}
						}
					}
				} else {
					//By any chance if response is null set the fraud flag to 0
					//Need to check if we really need this
					ccStatus.setFraudFlag( DigitalCreditCardServiceStatus.FRAUD_FLAG_YES );
					ccStatus.setDecision( DigitalCreditCardServiceStatus.DECISION_REVIEW );
				}
			} catch( PaymentGroupNotFoundException e ) {
				logger.error( "performTransaction() :: PaymentGroupNotFoundException: "+ e );
				throw new DigitalIntegrationException("Exception in finding Payment  group", "PAYMENTGROUPNOTFOUND", e.getLocalizedMessage());
			} catch( InvalidParameterException e ) {
				logger.error( "performTransaction() :: InvalidParameterException: "+  e );
				throw new DigitalIntegrationException("Exception in finding Payment  group",e.getMessage(), "" );
			} catch (DigitalIntegrationSystemException e) {
				logger.error( "performTransaction() : communication error "+ pCreditCardInfo.getOrder().getProfileId() +""+ pCreditCardInfo.getOrder().getId(), e);
				ccStatus.setFraudFlag( DigitalCreditCardServiceStatus.FRAUD_FLAG_YES );
				ccStatus.setDecision( DigitalCreditCardServiceStatus.DECISION_REVIEW );
				ccStatus.setCommunicationError(true);
			} catch( DigitalIntegrationException e ) {
				logger.error( "performTransaction() : "+ pCreditCardInfo.getOrder().getProfileId() +""+ pCreditCardInfo.getOrder().getId(), e);
				ccStatus.setFraudFlag( DigitalCreditCardServiceStatus.FRAUD_FLAG_YES );
				ccStatus.setDecision( DigitalCreditCardServiceStatus.DECISION_REVIEW );
			}finally{
	        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
	        }
		}
		else{
			// stub do these things if atg team has taken down vantiv service offline
			if( logger.isDebugEnabled() ) {
				logger.debug( "creditcardservices: preformTransaction(): stub data" );
			}
			// set our service's status
			if( pCreditCardInfo.getNameOnCard().equalsIgnoreCase( DigitalCreditCardServiceStatus.CCFRAUDFAILURE ) ) {
				ccStatus.setStatusCode( DigitalCreditCardServiceStatus.FAILURE );
				ccStatus.setAmountAuthorized( pCreditCardInfo.getAmount() );
				ccStatus.setAvsCode(DigitalCreditCardServiceStatus.FAILURE );
				ccStatus.setCvCode(DigitalCreditCardServiceStatus.CVCODE );
				ccStatus.setDecision( DigitalCreditCardServiceStatus.DECISION_REJECT );
			} else if( pCreditCardInfo.getNameOnCard().equalsIgnoreCase( DigitalCreditCardServiceStatus.APPROVEDFRAUDFAILURE ) ) {
				ccStatus.setStatusCode( DigitalCreditCardServiceStatus.FAILURE );
				ccStatus.setAmountAuthorized( pCreditCardInfo.getAmount() );
				ccStatus.setAvsCode( DigitalCreditCardServiceStatus.FAILURE );
				ccStatus.setCvCode(DigitalCreditCardServiceStatus.CVCODE );
				ccStatus.setFraudResultCode(DigitalCreditCardServiceStatus.FRAUD_RESULT_CODE_REJECTED_ERROR_CODE );
				ccStatus.setDecision( DigitalCreditCardServiceStatus.DECISION_ACCEPT );
			} else if( pCreditCardInfo.getNameOnCard().equalsIgnoreCase( DigitalCreditCardServiceStatus.REVIEWFRAUDFAILURE ) ) {
				ccStatus.setStatusCode( DigitalCreditCardServiceStatus.FAILURE );
				ccStatus.setAmountAuthorized( pCreditCardInfo.getAmount() );
				ccStatus.setAvsCode(DigitalCreditCardServiceStatus.FAILURE );
				ccStatus.setCvCode(DigitalCreditCardServiceStatus.CVCODE );
				ccStatus.setFraudResultCode(DigitalCreditCardServiceStatus.FRAUD_RESULT_CODE_REJECTED_ERROR_CODE );
				ccStatus.setDecision( DigitalCreditCardServiceStatus.DECISION_REVIEW );
			} else {
				ccStatus.setStatusCode( DigitalCreditCardServiceStatus.SUCCESS );
				ccStatus.setAmountAuthorized( pCreditCardInfo.getAmount() );
				ccStatus.setAvsCode( DigitalCreditCardServiceStatus.SUCCESS );
				ccStatus.setCvCode(DigitalCreditCardServiceStatus.CVCODE );
				ccStatus.setDecision( DigitalCreditCardServiceStatus.DECISION_REVIEW );
				ccStatus.setFraudFlag( DigitalCreditCardServiceStatus.FRAUD_FLAG_YES);
			}
		}
		if( logger.isDebugEnabled() ) {
			logger.debug( "performTransaction(): ccStatus = " + ccStatus.toString() );
		}
		return ccStatus;
	}

	
	public DigitalCreditCardServiceStatus registerTokenRequest ( String pTransactionType, DigitalCreditCard pCreditCard) throws DigitalIntegrationException {
		DigitalCreditCardServiceStatus ccStatus = new DigitalCreditCardServiceStatus();
		
		final String METHOD_NAME = "performTransaction";
		
		if( null != pTransactionType && null != pCreditCard && null != vantivPaymentService && getVantivPaymentService().isServiceEnabled() && 
				getVantivPaymentService().isServiceMethodEnabled(PaymentAuthorizationService.ServiceMethod.REGISTER_TOKEN_REQUEST.getServiceMethodName())) {
			if( logger.isDebugEnabled() ) {
				logger.debug( "creditcardservices: preformTransaction(): - Start" );
			}
			
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
				
				PaymentRegisterTokenServiceRequest paymentRegTokenReq = new PaymentRegisterTokenServiceRequest();
				
				paymentRegTokenReq.setPayPageRegistrationId(pCreditCard.getPaypageRegistrationId());	
				//set reporting group for both new card flow and existing flow
				paymentRegTokenReq.setReportGroup(dswServiceConstants.getVantivPayPageReportGroup());
						
				paymentRegTokenReq.setId(pCreditCard.getPaypageRegistrationId());
				paymentRegTokenReq.setCustomerId(pCreditCard.getPaypageRegistrationId());
	
				//Making the Integration call to get registerTokenRequest
				PaymentRegisterTokenServiceResponse paymentRegTokenResponse = getVantivPaymentService().registerTokenRequest(paymentRegTokenReq);
				
				ccStatus.setTransactionId( String.valueOf(paymentRegTokenResponse.getLitleTxnId()));
				ccStatus.setTokenValue(paymentRegTokenResponse.getLitleToken());
				ccStatus.setResponse(paymentRegTokenResponse.getResponse());
	
				if( logger.isDebugEnabled() ) {
					logger.debug( "CC Response:\n" + ccStatus.toString() );
				}
	
			} catch( DigitalIntegrationException e ) {
				throw new DigitalIntegrationException("Exception in pre registering the token", "REGISTERTOKENERROR", e.getLocalizedMessage());
				
			}finally{
	        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
	        }
		}
		else {
			throw new DigitalIntegrationException("Vantive integration is offline","VANTIVE_NOT_AVAILABLE", "registerTokenRequest");
		}
		if( logger.isDebugEnabled() ) {
			logger.debug( "performTransaction(): ccStatus = " + ccStatus.toString() );
		}
	return ccStatus;
	
		
		
	}
	/**
	 * Method used to construct the Billing address
	 * 
	 * @param pCreditCardInfo   DigitalCreditCardInfo object
	 * @return billToType 
	 * 
	 */
	private  PaymentAuthorizationServiceContact getBillToType(DigitalCreditCardInfo pCreditCardInfo){
		
		PaymentAuthorizationServiceContact billToType = new PaymentAuthorizationServiceContact();
		if( null != pCreditCardInfo.getBillingAddress() ) {
			DigitalRepositoryContactInfo contactInfo = (DigitalRepositoryContactInfo)pCreditCardInfo.getBillingAddress();
			billToType.setAddressLine1(contactInfo.getAddress1());
			billToType.setAddressLine2(contactInfo.getAddress2());
			billToType.setCity(contactInfo.getCity());
			billToType.setCountry(contactInfo.getCountry());
			billToType.setEmail(contactInfo.getEmail());
			billToType.setFirstName(contactInfo.getFirstName());
			billToType.setLastName(contactInfo.getLastName());
			billToType.setPhone(contactInfo.getPhoneNumber());
			billToType.setZip(contactInfo.getPostalCode());
			
			AddressType addressType = AddressType.valueFor( contactInfo );
			if( AddressType.MILITARY.equals( addressType ) ) {
				billToType.setState( contactInfo.getRegion() );
			} else {
				billToType.setState( contactInfo.getState() );
			}
	
			if( logger.isDebugEnabled() ) {
				logger.debug( "BillToType: city: " + billToType.getAddressLine1() );
				logger.debug( "BillToType: Country: " + billToType.getAddressLine2() );
				logger.debug( "BillToType: Email: " + billToType.getCity() );
				logger.debug( "BillToType: FirstName: " + billToType.getCountry() );
				logger.debug( "BillToType: LastName: " + billToType.getEmail() );
				logger.debug( "BillToType: PostalCode: " + billToType.getFirstName() );
				logger.debug( "BillToType: State: " + billToType.getLastName() );
				logger.debug( "BillToType: Street1: " + billToType.getPhone() );
				logger.debug( "BillToType: Street2: " + billToType.getState() );
				logger.debug( "BillToType:zip: " + billToType.getZip() );
			}

		}
		return billToType;
	}
	
	/**
	 * Method used to construct the Shipping address
	 * 
	 * @param pCreditCardInfo   DigitalCreditCardInfo object
	 * @return billToType 
	 * 
	 */
	private PaymentAuthorizationServiceContact getShipTo( DigitalCreditCardInfo pCreditCardInfo) {

		  PaymentAuthorizationServiceContact shippingAddress = null;

		List<ShippingGroup> shippingGroups = pCreditCardInfo.getOrder().getShippingGroups();
		Iterator<ShippingGroup> iterator = shippingGroups.iterator();
		while( iterator.hasNext() ) {
			ShippingGroup sg = iterator.next();
			if( sg instanceof HardgoodShippingGroup ) {
				HardgoodShippingGroup shippingGroup = (HardgoodShippingGroup)sg;
				DigitalRepositoryContactInfo contactInfo = (DigitalRepositoryContactInfo)shippingGroup.getShippingAddress();
				shippingAddress = new PaymentAuthorizationServiceContact();
				shippingAddress.setAddressLine1(contactInfo.getAddress1());
				shippingAddress.setAddressLine2(contactInfo.getAddress2() != null?contactInfo.getAddress2():"");
				shippingAddress.setCity(contactInfo.getCity());
				shippingAddress.setCountry(contactInfo.getCountry());
				shippingAddress.setEmail(contactInfo.getEmail());
				shippingAddress.setFirstName(contactInfo.getFirstName());
				shippingAddress.setLastName(contactInfo.getLastName());
				shippingAddress.setPhone(contactInfo.getPhoneNumber());
				shippingAddress.setState(contactInfo.getState());
				shippingAddress.setZip(contactInfo.getPostalCode());
				break;
			}
		}
		return shippingAddress;
	}
	
	/**
	 * Method to handle split tender
	 * 
	 * @param pCreditCardInfo   DigitalCreditCardInfo object
	 * @return GCPayAmt GC Payment amount
	 * 
	 */
	private double getGCpaymentAmount( DigitalCreditCardInfo pCreditCardInfo ) throws PaymentGroupNotFoundException, InvalidParameterException {
		double GCPayAmt = 0.00;
		List<PaymentGroup> paymentGroups = pCreditCardInfo.getOrder().getPaymentGroups();
		Iterator<PaymentGroup> iterator = paymentGroups.iterator();
		while( iterator.hasNext() ) {
			PaymentGroup paymentGroup = iterator.next();
			if( "giftCard".equals( paymentGroup.getPaymentGroupClassType() ) ) {
				GCPayAmt = GCPayAmt + paymentGroup.getAmount();
			}

		}
		return GCPayAmt;
	}
	
	/**
	 * Method to extract last 2 digits from the expiration year, 
	 * vantiv expects year in YY format
	 * 
	 * @param expirationYear   String 
	 * @return GCPayAmt GC Payment amount
	 * 
	 */
	private String getLastTwoDigitsOfYear(String expirationYear){
		if(null!=expirationYear){
			return ( expirationYear.length() > 2 ? expirationYear.substring(expirationYear.length() - 2) : expirationYear);
		}else{
			return expirationYear;
		}	
	
	}
	
	private List<GiftCardType> getGiftCardTypes(Order order){
		
		List<GiftCardType> giftCardTypeList = new ArrayList<>();
		DigitalCommerceItemManager ciManager = (DigitalCommerceItemManager)this.getOrderManager().getCommerceItemManager();
		List<GiftCardCommerceItem> gcItems = ciManager.findAllGiftCardItems(order);
		for(GiftCardCommerceItem gcItem : gcItems){
			if("EG".equalsIgnoreCase(gcItem.getProductType())){
				giftCardTypeList.add(GiftCardType.ELECTRONIC);
			}else{
				giftCardTypeList.add(GiftCardType.PERSONAL_STANDARD);
			}
		}
		
		return giftCardTypeList;
	}
	
	
private List<String> getBrandNames(Order order){
		
		List<String> brandNames = new ArrayList<>();
		List<DigitalCommerceItem> cis = order.getCommerceItems();
			for(DigitalCommerceItem ci : cis){
				RepositoryItem product=null;
				product=(RepositoryItem)ci.getAuxiliaryData().getProductRef();
				if(null!=product){
					RepositoryItem dswBrand=(RepositoryItem) product.getPropertyValue("dswBrand");
					if(null!=dswBrand){
						String brandDisplayName=(String)dswBrand.getPropertyValue("displayName");
						if(null!=brandDisplayName){
							brandNames.add(brandDisplayName);
						}
					}
				}
			}
		
		return brandNames;
	}
	public List<PaymentAuthorizationServiceRequest.ShipType> getShipTypes(DigitalOrderImpl order){
		List<PaymentAuthorizationServiceRequest.ShipType> shipTypeList = new ArrayList<>(); 
		List<DigitalCommerceItem> ciItems = order.getCommerceItems();
		boolean hasBopisItem = false, hasBostsItem = false, hasShipToHomeItem = false;
		for(DigitalCommerceItem ciItem : ciItems){
			String shipType = ciItem.getShipType();
			if(ShipType.BOPIS.getValue().equalsIgnoreCase(shipType)){
				hasBopisItem = true;
			}else if(ShipType.BOSTS.getValue().equalsIgnoreCase(shipType)){
				hasBostsItem = true;
			}else if(ShipType.SHIP.getValue().equalsIgnoreCase(shipType)){
				hasShipToHomeItem = true;
			}
		}
		PaymentAuthorizationServiceRequest.ShipType shipType = PaymentAuthorizationServiceRequest.ShipType.STANDARD;
		if(hasBopisItem && !hasBostsItem && !hasShipToHomeItem){
			shipType = PaymentAuthorizationServiceRequest.ShipType.PICKUP;
			shipTypeList.add(shipType);
		}else if(!hasBostsItem && hasShipToHomeItem && hasBopisItem){
			shipType = PaymentAuthorizationServiceRequest.ShipType.PICKUP;
			shipTypeList.add(shipType);
		}else if(!hasBopisItem && hasBostsItem && !hasShipToHomeItem){
			shipType = PaymentAuthorizationServiceRequest.ShipType.SHIP_TO_STORE;
			shipTypeList.add(shipType);
		}else if((hasBopisItem && hasBostsItem) && !hasShipToHomeItem){
			shipType = PaymentAuthorizationServiceRequest.ShipType.SHIP_TO_STORE;
			shipTypeList.add(shipType);
			shipTypeList.add(PaymentAuthorizationServiceRequest.ShipType.PICKUP);
		}else if(hasBostsItem && hasShipToHomeItem && !hasBopisItem){
			shipType = PaymentAuthorizationServiceRequest.ShipType.SHIP_TO_STORE;
		}else if(hasBopisItem && hasBostsItem && hasShipToHomeItem){
			shipType = PaymentAuthorizationServiceRequest.ShipType.SHIP_TO_STORE;
			shipTypeList.add(shipType);
			shipTypeList.add(PaymentAuthorizationServiceRequest.ShipType.PICKUP);
		}
		return shipTypeList;
	}
	/**
	 * Method to truncate fraud rule description if greater than 254 characters
	 * 
	 * @param fraudRuleDesc  String 
	 * @return fraudRuleDesc String
	 * 
	 */
	private String truncateFraudDesc(String fraudRuleDesc){
		
		if(DigitalStringUtil.isNotEmpty(fraudRuleDesc) &&
				fraudRuleDesc.length() > getDswServiceConstants().getFraudRuleDescMaxLength()){
			fraudRuleDesc = fraudRuleDesc.substring(0, getDswServiceConstants().getFraudRuleDescMaxLength());
		}
		return fraudRuleDesc;
		
	}
}
