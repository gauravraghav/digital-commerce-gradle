/**
 * 
 */
package com.digital.commerce.services.pricing;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@Getter
@Setter
public class ShippingMethodInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5891400970442198174L;

	private double				baseRate;									

	private String				shippingMethod;


	@Override
	public boolean equals(Object ci) {
		if (!(ci instanceof ShippingMethodInfo))
            return false;
        if (ci == this)
            return true;

        ShippingMethodInfo rhs = (ShippingMethodInfo) ci;
        return new EqualsBuilder().
            append(shippingMethod, rhs.shippingMethod).isEquals();
	}

	@Override
	public int hashCode() {
		  return new HashCodeBuilder(17, 31). 
		            append(shippingMethod).toHashCode();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
				sb.append("baseRate: ").append(baseRate).append(",")
				  .append("shippingMethod: ").append(shippingMethod);
		return sb.toString();
	}
	
}
