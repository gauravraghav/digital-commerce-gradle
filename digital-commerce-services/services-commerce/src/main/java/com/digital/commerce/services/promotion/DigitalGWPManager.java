/**
 * 
 */
package com.digital.commerce.services.promotion;

import atg.adapter.gsa.query.Clause;
import atg.commerce.CommerceException;
import atg.commerce.catalog.CatalogTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.pricing.DetailedItemPriceInfo;
import atg.commerce.pricing.GWPInfo;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingContext;
import atg.commerce.pricing.PricingException;
import atg.commerce.promotion.GWPManager;
import atg.commerce.promotion.GWPMultiHashKey;
import atg.commerce.promotion.GiftWithPurchaseSelectionChoice;
import atg.core.util.ResourceUtils;
import atg.dtm.UserTransactionDemarcation;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.multisite.SiteTools;
import atg.repository.ParameterSupportView;
import atg.repository.Query;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.web.messaging.UserMessage;
import lombok.Getter;
import lombok.Setter;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.pricing.DigitalItemPricingEngine;
import com.google.common.base.Strings;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Extension of the GWPManager class to implement custom inventory check logic
 * before a GWP is added to a user's cart.
 * 
 * @author NS389061
 * 
 */
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalGWPManager extends GWPManager {

	private static final String CLASSNAME = DigitalGWPManager.class.getName();

	private static final String	PROD_GWPITEM_PROPERTY			= "isGWPItem";
	
	private OrderManager orderManager;

	private boolean enableTransactionLock;
	
	/** Restrict GWP to ship to store **/
	public boolean checkShippingRestriction(Order order){
		List ciList = order.getCommerceItems();
		Boolean shipRestriction = false;		
		// To verify if the commerce items in the order contains a GWP product, if yes it continues to check other conditions
		for(int x = 0; x < ciList.size(); x++ ) {
			DigitalCommerceItem ci = (DigitalCommerceItem)ciList.get(x);
			RepositoryItem productRef = (RepositoryItem)ci.getAuxiliaryData().getProductRef();
			if(productRef != null){				
				if((Boolean)productRef.getPropertyValue(PROD_GWPITEM_PROPERTY) ) {
					if( isLoggingDebug() ) {
						logDebug("GWP Item Found");
					}
					if (!("SHIP").equalsIgnoreCase(ci.getShipType())){
						shipRestriction = true;
						break;
					}

					// check whether this is item from GWP or regular item in cart
					ItemPriceInfo priceInfo = (ci != null) ? ci.getPriceInfo() : null;
					if (priceInfo != null) {
						List<DetailedItemPriceInfo> priceDetails = priceInfo.getCurrentPriceDetails();
						if (priceDetails != null) {
							for (DetailedItemPriceInfo priceDetail : priceDetails) {
								DetailedItemPriceInfo curPriceInfo = priceDetail;
								if (isLoggingDebug()) {
									logDebug(String.valueOf(curPriceInfo.getAmount()));
								}
								if (curPriceInfo.getAmount() > 0) {
									// This order contains atleast 1 commerce item with ship to home or fully priced gwp
									shipRestriction = false;
									break;
								}
							}
						}
					}
					if (shipRestriction) {
						break;
					}
				 }
				}
		}
/**	if (shipRestriction) {
	shipRestriction = verifyShipRestriction(order);
	}**/
	
	return shipRestriction;
	}
	
	
	/**
	* This method verifies all the shippingGroupRelations in the order, and
	* look for an existence of shipping restriction. Which is order contains
	* only GWP shipped to home.
	* 
	* @return true or false
	*/
	@SuppressWarnings("unchecked")
	public Boolean verifyShipRestriction(Order o) {
	
		String shipRest = null;
		Collection<ShippingGroup> shippingGroups = o.getShippingGroups();
		Iterator<ShippingGroup> shippingGrouperator = shippingGroups.iterator();
		ShippingGroup sg = null;
		while (shippingGrouperator.hasNext()) {
			sg = (ShippingGroup) shippingGrouperator.next();
			if (sg instanceof HardgoodShippingGroup) {
				List<?> ciRels = sg.getCommerceItemRelationships();
				if (isLoggingDebug()) {
					logDebug(sg.getId() + " has " + ciRels.size() + " relationships");
				}
				for (int x = 0; x < ciRels.size(); x++) {
					CommerceItemRelationship crel = (CommerceItemRelationship) ciRels.get(x);
					CommerceItem ci = crel.getCommerceItem();
					HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sg;
					String hgShipType = (String) hgSg.getPropertyValue("shipType");
					if (Strings.isNullOrEmpty(hgShipType) || hgShipType.equalsIgnoreCase("ship")) {
						ItemPriceInfo priceInfo = (ci != null) ? ci.getPriceInfo() : null;
						if (priceInfo != null) {
							List<?> priceDetails = priceInfo.getCurrentPriceDetails();
							if (priceDetails != null) {
								for (int y = 0; y < priceDetails.size(); y++) {
									DetailedItemPriceInfo curPriceInfo = (DetailedItemPriceInfo) priceDetails.get(y);
									if (isLoggingDebug()) {
										logDebug(String.valueOf(curPriceInfo.getAmount()));
									}

									RepositoryItem productRef = (RepositoryItem)ci.getAuxiliaryData().getProductRef();
									if(productRef != null){										
										if((Boolean)productRef.getPropertyValue(PROD_GWPITEM_PROPERTY) && curPriceInfo.getAmount() == 0) {
										// This order contains atleast 1
										// commerce item with ship to home
                                            shipRest = "Y";
										}
									}
									if (curPriceInfo.getAmount() > 0) {
										// This order contains atleast 1
										// commerce item with ship to home
										shipRest = "N";
										break;
									}
								}
							}
						}

					}
					if (shipRest!=null && "N".equals(shipRest)){
					    break;
                    }
				}
			}
		}
		return (shipRest!=null && "Y".equals(shipRest));
	}
	
	/**
	 * Override the OOB auto add method for GWPs to do an inventory check. This
	 * will allow an inventory check before the GWP is added to the user's cart.
	 * In case of insufficient inventory, the GWP item will be marked as failed.
	 * The promotion applied to the order will be explicitly removed as well.
	 * 
	 * @param orderMarker
	 *            The repository item for the order
	 * @param quantityToAdd
	 *            The quantity to be auto added
	 * @param newItemInfos
	 *            List of AddCommerceItemInfo objects to be populated with new
	 *            info
	 * @param newItems
	 *            List of CommerceItem objects to be populated
	 * @param pricingContext
	 *            The pricing context for the order
	 * @param extraParameters
	 *            An optional map of any extra parameters
	 * @return The CommerceItem reference to the auto added item if the auto add
	 *         is successfull, null otherwise
	 * @throws PricingException
	 */
	@Override
	public CommerceItem processAutoAdd(final RepositoryItem orderMarker,
			final long quantityToAdd,
			final List<AddCommerceItemInfo> newItemInfos,
			final List<CommerceItem> newItems,
			final PricingContext pricingContext,
			final Map extraParameters)
			throws PricingException {
		return super.processAutoAdd(orderMarker, quantityToAdd, newItemInfos,
				newItems, pricingContext, extraParameters);
	}

	/*
	 * final private void markAddAsFailed(final GWPMarkerManager gwpMarkerMgr,
	 * final RepositoryItem orderMarker, final long quantity) throws
	 * PricingException { if (isLoggingDebug()) {
	 * logDebug("markAddAsFailed:: Order ID : " + orderMarker.getRepositoryId()
	 * + ", Quantity: " + quantity); } try {
	 * gwpMarkerMgr.setFailedQuantity(orderMarker, quantity); } catch
	 * (MarkerException e) { if (isLoggingDebug()) { logError("Error: " +
	 * e.getMessage(), e); } else { logError("Error: " + e.getMessage()); }
	 * throw new PricingException(e); } }
	 */

	public GiftWithPurchaseSelectionChoice[] getGiftSelectionItems(
			String pPromotionId, PricingContext pPricingContext,
			String pGiftType, String pGiftDetail, boolean pReturnSkus,
			Map pExtraParameters) throws CommerceException {
		
		if (isLoggingDebug()) {
			logDebug("Entered getGiftSelectionChoices()");
			logDebug("pPromotionId:" + pPromotionId);
			logDebug("pPricingContext:" + pPricingContext);
			logDebug("pGiftType:" + pGiftType);
			logDebug("pGiftDetail:" + pGiftDetail);
			logDebug("pReturnSkus:" + pReturnSkus);
			logDebug("pExtraParameters:" + pExtraParameters);
		}

		if ((pPromotionId == null) && (pPricingContext != null)
				&& (pPricingContext.getPricingModel() != null)) {
			pPromotionId = pPricingContext.getPricingModel().getRepositoryId();
		}

		CatalogTools tools = getCatalogTools();
		GiftWithPurchaseSelectionChoice choice = null;
		Set<GiftWithPurchaseSelectionChoice> selectionChoices = new HashSet();

		Repository repository = tools.getCatalog();
		GiftWithPurchaseSelectionChoice selectionChoice = null;
		Site currentSite = SiteContextManager.getCurrentSite();
		Set<Site> shareableSites = null;
		
		if (currentSite != null) {
			SiteTools siteTools = getSiteTools();
			shareableSites = siteTools.getShareableSites(currentSite,
					siteTools.getShareableTypeId(),
					siteTools.getIncludeInactiveSites(),
					siteTools.getIncludeDisabledSites());
		}

		if (tools.getBaseSKUItemType().equals(pGiftType)) {
			selectionChoice = new GiftWithPurchaseSelectionChoice();
			RepositoryItem sku;
			try {
				sku = tools.findSKU(pGiftDetail);
			} catch (RepositoryException e) {
				String msg = ResourceUtils.getMsgResource(
						"gwpErrorGettingGiftSelectionChoices",
						"atg.commerce.promotion.PromotionResources",
						sResourceBundle);

				if (isLoggingError()) {
					logError(msg, e);
					if (pGiftDetail != null)
						logError(pGiftDetail);
				}
				throw new CommerceException(msg);
			}
			
			selectionChoice.addSku(sku);
			selectionChoice.setProduct(getProductForSku(sku, shareableSites));
			selectionChoices.add(selectionChoice);
		} else if (tools.getBaseProductItemType().equals(pGiftType)) {
			RepositoryItem product = null;
			try {
				product = repository.getItem(pGiftDetail,
						tools.getBaseProductItemType());
			} catch (RepositoryException e) {
				String msg = ResourceUtils.getMsgResource(
						"gwpErrorGettingGiftSelectionChoices",
						"atg.commerce.promotion.PromotionResources",
						sResourceBundle);

				if (isLoggingError()) {
					logError(msg, e);
					if (pGiftDetail != null)
						logError(pGiftDetail);
				}
				throw new CommerceException(msg);
			}

			if (product == null) {
				throw new CommerceException(
						"Failed to find the product for id: " + pGiftDetail);
			}

			choice = getProductGWPSelectionChoice(product, pReturnSkus,
					shareableSites);
			if (choice != null) {
				selectionChoices.add(choice);
			}
		} else if (tools.getBaseCategoryItemType().equals(pGiftType)) {
			try {
				ParameterSupportView view = (ParameterSupportView) tools
						.getCatalog().getView(tools.getBaseProductItemType());

				if (view != null) {
					Object[] params = { pGiftDetail };
					Query catQuery = getCategoryQuery().getQuery().getQuery(
							view.getQueryBuilder(), view, params);

					if ((catQuery instanceof Clause)) {
						((Clause) catQuery).setContextFilterApplied(true);
					}
					RepositoryItem[] productArray = view.executeQuery(catQuery,
							params);
					if (productArray != null) {
						for (RepositoryItem product : productArray) {
							choice = getProductGWPSelectionChoice(product,
									pReturnSkus, shareableSites);
							if (choice != null) {
								selectionChoices.add(choice);
							}
						}
					}
				}
			} catch (RepositoryException e) {
				String msg = ResourceUtils.getMsgResource(
						"gwpErrorGettingGiftSelectionChoices",
						"atg.commerce.promotion.PromotionResources",
						sResourceBundle);

				if (isLoggingError()) {
					logError(msg, e);
					if (pGiftDetail != null)
						logError(pGiftDetail);
				}
				throw new CommerceException(msg);
			}
		} else if ("skuContentGroup".equals(pGiftType)) {
			try {
				Repository catalog = tools.getCatalog();
				Query query = getRepositoryGroupContainerService().getGroup(
						catalog.getRepositoryName(), pGiftDetail)
						.getGroupQuery();

				if ((query instanceof Clause)) {
					((Clause) query).setContextFilterApplied(true);
				}
				RepositoryItem[] skuArray = catalog.getView(
						tools.getBaseSKUItemType()).executeQuery(query);
				if (skuArray != null) {
					for (RepositoryItem sku : skuArray) {
						selectionChoice = new GiftWithPurchaseSelectionChoice();
						selectionChoice.addSku(sku);
						selectionChoice.setProduct(getProductForSku(sku,
								shareableSites));
						selectionChoices.add(selectionChoice);
					}
				}
			} catch (RepositoryException e) {
				String msg = ResourceUtils.getMsgResource(
						"gwpErrorGettingGiftSelectionChoices",
						"atg.commerce.promotion.PromotionResources",
						sResourceBundle);

				if (isLoggingError()) {
					logError(msg, e);
					if (pGiftDetail != null)
						logError(pGiftDetail);
				}
				throw new CommerceException(msg);
			}
		} else if ("productContentGroup".equals(pGiftType)) {
			try {
				Repository catalog = tools.getCatalog();
				Query query = getRepositoryGroupContainerService().getGroup(
						catalog.getRepositoryName(), pGiftDetail)
						.getGroupQuery();

				if ((query instanceof Clause)) {
					((Clause) query).setContextFilterApplied(true);
				}
				RepositoryItem[] productArray = catalog.getView(
						tools.getBaseProductItemType()).executeQuery(query);
				if (productArray != null) {
					for (RepositoryItem product : productArray) {
						choice = getProductGWPSelectionChoice(product,
								pReturnSkus, shareableSites);
						if (choice != null) {
							selectionChoices.add(choice);
						}
					}
				}
			} catch (RepositoryException e) {
				String msg = ResourceUtils.getMsgResource(
						"gwpErrorGettingGiftSelectionChoices",
						"atg.commerce.promotion.PromotionResources",
						sResourceBundle);

				if (isLoggingError()) {
					logError(msg, e);
					if (pGiftDetail != null)
						logError(pGiftDetail);
				}
				throw new CommerceException(msg);
			}
		} else {
			String msg = ResourceUtils.getMsgResource(
					"gwpErrorGettingGiftSelectionChoices",
					"atg.commerce.promotion.PromotionResources",
					sResourceBundle);

			if (isLoggingError()) {
				logError(msg);
				if (pGiftType != null)
					logError(pGiftType);
			}
			throw new CommerceException(msg);
		}

		GiftWithPurchaseSelectionChoice[] choiceArray = (GiftWithPurchaseSelectionChoice[]) selectionChoices
				.toArray(new GiftWithPurchaseSelectionChoice[0]);

		if (isLoggingDebug()) {
			logDebug("Leaving getGiftSelectionChoices() with: "
					+ selectionChoices);
		}
		return choiceArray;
	}

	public GiftWithPurchaseSelectionChoice getGWPGiftItem(
			String pGiftType, String pGiftDetail, Map pExtraParameters)
			throws CommerceException {
		if (isLoggingDebug()) {
			logDebug("Entered getAutoAddGiftSelectionChoice()");
			logDebug("pGiftType:" + pGiftType);
			logDebug("pGiftDetail:" + pGiftDetail);
			logDebug("pExtraParameters:" + pExtraParameters);
		}

		String promotionId = null;
		PricingContext pricingContext = null;
		if (pExtraParameters != null) {
			promotionId = (String) pExtraParameters.get("gwpPromotionId");
			pricingContext = (PricingContext) pExtraParameters
					.get("pricingContext");
		}

		GiftWithPurchaseSelectionChoice choice = null;
		GiftWithPurchaseSelectionChoice[] choices = getGiftSelectionItems(
				promotionId, pricingContext, pGiftType, pGiftDetail, true,
				pExtraParameters);

		if ((choices != null) && (choices.length == 1)) {
			choice = choices[0];
			if (choice.getSkuCount() != 1) {
				choice = null;
			}
		} else if ((choices == null) || (choices.length <= 0)) {
			String msg = ResourceUtils.getMsgResource(
					"gwpErrorNoGiftSelectionChoices",
					"atg.commerce.promotion.PromotionResources",
					sResourceBundle);

			if (isLoggingError()) {
				logError(msg);
				if (pGiftType != null)
					logError(pGiftType);
				if (pGiftDetail != null)
					logError(pGiftDetail);
			}
			throw new CommerceException(msg);
		}

		if (isLoggingDebug()) {
			logDebug("Leaving getAutoAddGiftSelectionChoice() with: " + choice);
		}
		return choice;
	}
	
	@Override
	public boolean processGWPInfos(Map<GWPMultiHashKey, GWPInfo> pGWPInfos,
			PricingContext pPricingContext, Map pExtraParameters) throws PricingException {
		final String PERFORM_MONITOR_NAME = "DigitalGWPManager_processGWPInfos";
		final String METHOD_NAME = "processGWPInfos";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
		UserTransactionDemarcation td = null;
		if(isEnableTransactionLock()) {
			td = TransactionUtils
					.startNewTransaction(CLASSNAME, METHOD_NAME);
			TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
		}
		boolean result = false;
		try {
			if(isEnableTransactionLock()) {
				synchronized (pPricingContext.getOrder()) {
					result = super.processGWPInfos(pGWPInfos, pPricingContext, pExtraParameters);
					if (pGWPInfos != null && pGWPInfos.size() > 0 && verifyShipRestriction(
							pPricingContext.getOrder())) {
						String msg = ((DigitalItemPricingEngine) getPricingTools().getItemPricingEngine())
								.getMessageLocator().getMessageString("shipRestriction");
						UserMessage message = new UserMessage("GWPShippingRestriction",
								"GWPShippingRestriction", msg,
								-10, "information", null);
						getPricingTools().sendUserMessage(message, null, null);
					}
				}
			}else{
				result = super.processGWPInfos(pGWPInfos, pPricingContext, pExtraParameters);
				if (pGWPInfos != null && pGWPInfos.size() > 0 && verifyShipRestriction(
						pPricingContext.getOrder())) {
					String msg = ((DigitalItemPricingEngine) getPricingTools().getItemPricingEngine())
							.getMessageLocator().getMessageString("shipRestriction");
					UserMessage message = new UserMessage("GWPShippingRestriction",
							"GWPShippingRestriction", msg,
							-10, "information", null);
					getPricingTools().sendUserMessage(message, null, null);
				}
			}
		}
		catch (Exception e){
			logError(e);
		}
		finally {
			try {
				if(isEnableTransactionLock()) {
					TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
					TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			} catch (Throwable th) {
				logError(th);
			}
		}
		return result;
	}
}
