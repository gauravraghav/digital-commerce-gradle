/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RewardsSubscribeEmailFooterRequest {
	private String email;
	private String profileId;
	private String address1;
	private String city;
	private String country;
	private String state;
	private String postalCode;
	private String firstName;
	private String lastName;
	private String emailSource;
}
