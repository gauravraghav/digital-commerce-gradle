/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import java.util.List;

import com.digital.commerce.common.domain.ResponseWrapper;
import com.digital.commerce.integration.reward.domain.Offer;

/**
 * @author mmallipu
 *
 */
public class RewardsRetrieveOffersResponse extends ResponseWrapper {

	private List<Offer> offers;
	
	public List<Offer> getOffers() {
		return offers;
	}

	public void setOffers(List<Offer> offers) {
		this.offers = offers;
	}

	public RewardsRetrieveOffersResponse() {
	}
}
