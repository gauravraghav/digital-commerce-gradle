/**
 * 
 */
package com.digital.commerce.services.order;

import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;

/** This enumeration identifies the origin of the commerce item.
 * For now, it is set up to identify if a commerce item came from a user's own wish list, a shared wish list or a default value.
 * 
 */
@Getter
public enum OriginOfCommerceItem {
	Default("default"), UserWishList("userWishList"), SharedWishList("sharedWishList");

	private final String	value;

	private OriginOfCommerceItem( final String value ) {
		this.value = value;
	}

	public static OriginOfCommerceItem valueFor( String value ) {
		OriginOfCommerceItem retVal = null;
		if( value != null ) {
			for( final OriginOfCommerceItem origin : OriginOfCommerceItem.values() ) {
				if( DigitalStringUtil.equalsIgnoreCase( origin.name(), value ) ) {
					retVal = origin;
				}
			}
		}
		return retVal;
	}
}
