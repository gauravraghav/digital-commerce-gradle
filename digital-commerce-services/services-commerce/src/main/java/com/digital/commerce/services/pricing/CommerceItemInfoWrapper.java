/**
 * 
 */
package com.digital.commerce.services.pricing;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@Getter
@Setter
public class CommerceItemInfoWrapper implements Serializable {

	private static final long serialVersionUID = 2522826906165182724L;
	private String id;
	private String productTitle;
	private long quantity;
	private String brand;
	private String colorCode;
	private String productId;

	@Override
	public boolean equals(Object ci) {
		if (!(ci instanceof CommerceItemInfoWrapper))
            return false;
        if (ci == this)
            return true;

        CommerceItemInfoWrapper rhs = (CommerceItemInfoWrapper) ci;
        return new EqualsBuilder().
            append(id, rhs.id).isEquals();
	}

	@Override
	public int hashCode() {
		  return new HashCodeBuilder(17, 31).
		            append(id).toHashCode();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
				sb.append("id: ").append(id).append(",")
				  .append("quantity: ").append(quantity).append(",")
				  .append("productTitle: ").append(productTitle).append(",")
					.append("brand: ").append(brand).append(",")
					.append("colorCode: ").append(colorCode).append(",")
					.append("productId: ").append(productId);
		;
		return sb.toString();
	}
	
}
