/**
 * 
 */
package com.digital.commerce.services.order.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.services.inventory.InventoryTools;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"unchecked","rawtypes"})
@Getter
@Setter
public class UpdateOrderInventoryStatus extends ApplicationLoggingImpl implements PipelineProcessor {

	private InventoryTools		inventoryTools;

	private static final int	SUCCESS		= 1;

	private static final int[]	RET_CODES	= { SUCCESS };

	private Map					inventoryCodes;

	private MessageLocator		messageLocator;

	public UpdateOrderInventoryStatus() {
		super(UpdateOrderInventoryStatus.class.getName());
	}
	
	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#getRetCodes() */
	public int[] getRetCodes() {
		return RET_CODES;
	}

	/* (non-Javadoc)
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(java.lang.Object,
	 * atg.service.pipeline.PipelineResult) */
	public int runProcess( Object pParam, PipelineResult pResult ) throws Exception {
		HashMap map = (HashMap)pParam;
		Order order = (Order)map.get( PipelineConstants.ORDER );
		this.updateInventoryStatus( order );
		return SUCCESS;
	}

	private void updateInventoryStatus( Order order ) {
		List<CommerceItem> commerceItems = order.getCommerceItems();
		if( commerceItems == null || commerceItems.isEmpty() ) { return; }
		for( CommerceItem commerceItem : commerceItems ) {
			int invStatus = this.getInventoryTools().getInventoryStatus( ( (RepositoryItem)commerceItem.getAuxiliaryData().getCatalogRef() ).getRepositoryId(), commerceItem.getQuantity() );
			commerceItem.setStateDetail( this.getMessageLocator().getMessageString( (String)this.getInventoryCodes().get( String.valueOf( invStatus ) ) ) );
		}
	}
}
