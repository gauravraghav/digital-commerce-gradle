/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import java.util.List;

import com.digital.commerce.common.domain.ResponseWrapper;
import com.digital.commerce.integration.reward.domain.Certificate;
import com.digital.commerce.integration.reward.domain.RewardServiceResponse;
import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RewardsRetrievePointsOffersCertificatesResponse extends ResponseWrapper {
	private RewardServiceResponse rewardsCertificatePointsHistory;
	private RewardServiceResponse rewardsQualifyingPointsHistory;
}
