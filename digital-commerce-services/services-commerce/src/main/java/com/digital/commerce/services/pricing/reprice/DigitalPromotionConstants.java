package com.digital.commerce.services.pricing.reprice;

public final class DigitalPromotionConstants {
	/** Flag which determines if the current promotion has the exclusivity feature turned on or off.
	 * 
	 * If the value of the flag is set to TRUE
	 * then the current promotion can be combined only with the promotions specified in the current promotion's inclusion list.
	 * 
	 * If the value of the flag is set to FALSE,
	 * then the current promotion cannot be combined with the promotions specified in the current promotion's exclusion list. */
	final static String			ENABLE_ALL_PROMO_EXCLUSION	= "enableAllPromoExclusion";

	/** List of promotions with which the current promotion can be combined with,
	 * provided the current promotion's ENABLE_ALL_PROMO_EXCLUSION flag is set to TRUE. */
	final static String			EXCLUDE_PROMOS				= "excludePromotions";

	/** List of promotions with which the current promotion cannot be combined with,
	 * provided the current promotion's ENABLE_ALL_PROMO_EXCLUSION flag is set to FALSE. */
	final static String			INCLUDE_PROMOS				= "includePromotions";

	/** Constant parameter declared for the corresponding error message that is thrown to the user, when
	 * the current promotion's ENABLE_ALL_PROMO_EXCLUSION is set to true and
	 * when the inclusion list of either of the 2 promotions( current promotion and the promotion being combined with the current promotion)
	 * involved in the promotion compatibility check is empty. */
	final static String			NOT_COMBINABLE_1			= "notCombinable1";

	/** Constant declared for the corresponding error message that is thrown to the user,
	 * when the current promotion's ENABLE_ALL_PROMO_EXCLUSION is set to true and
	 * if the inclusion list of either of the 2 promotions( current promotion and the promotion being combined with the current promotion)
	 * does not contain the other promotion. */
	final static String			NOT_USED_TOGETHER_1			= "notUsedTogether1";

	/** Constant declared for the corresponding error message that is thrown to the user,
	 * when the current promotion's ENABLE_ALL_PROMO_EXCLUSION is set to false and
	 * if the promotion which is being combined with the current promotion contains the current promotion in its exclusion list. */
	final static String			NOT_COMBINABLE_2			= "notCombinable2";

	/** Constant declared for the corresponding error message that is thrown to the user,
	 * when the current promotion's ENABLE_ALL_PROMO_EXCLUSION is set to false and
	 * if the current promotion contains the other promotion which it is being combined with in its exclusion list. */
	final static String			NOT_USED_TOGETHER_2			= "notUsedTogether2";

	/** Constant declared for returning success status if the 2 promotions being combined is compatible with one another and
	 * therefore can be combined together. */
	public final static String	SUCCESS						= "success";

	/** This is a constant parameter name which is used to retrieve all the coupons
	 * which are a part of the granted coupon list associated with the user. */
	public final static String	USER						= "user";

	/** This is a constant parameter name which is used to retrieve the profile id of the user who is applying the promotion. */
	public final static String	ID							= "id";

	/** This is a constant parameter used to hold the promotion object. */
	final static String			PROMOTION					= "promotion";
	
	public final static String			PROMOTIONS				= "promotions";
	public final static String 		GRANTED_COUPONS 			= "grantedCoupons";

}
