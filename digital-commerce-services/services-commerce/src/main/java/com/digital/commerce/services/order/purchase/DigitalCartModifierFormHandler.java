package com.digital.commerce.services.order.purchase;

import static com.digital.commerce.common.services.DigitalBaseConstants.GIFT_CARD_APPROVAL_RESPONSE_CODE;

import atg.commerce.CommerceException;
import atg.commerce.catalog.CatalogTools;
import atg.commerce.order.AuxiliaryData;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.ShippingGroupNotFoundException;
import atg.commerce.order.purchase.CartModifierFormHandler;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.commerce.promotion.PromotionTools;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.dtm.UserTransactionDemarcation;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.lockmanager.DeadlockException;
import atg.service.lockmanager.LockManagerException;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileRequestTools;
import atg.userprofiling.ProfileServices;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.exception.DigitalRuntimeException;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.services.inventory.InventoryHelper;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.SessionAttributesConstant;
import com.digital.commerce.constants.PaypalConstants;
import com.digital.commerce.constants.ProducCatalogConstants.ProducCatalogPropertyManager;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.inventory.storenet.StoreNetInventoryReservationService;
import com.digital.commerce.services.common.FindCommerceItemById;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalCommerceItemManager;
import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.OriginOfCommerceItem;
import com.digital.commerce.services.order.ShipNode;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.giftcard.GiftCardPaymentServices;
import com.digital.commerce.services.order.payment.giftcard.valueobject.DigitalGiftCardApproval;
import com.digital.commerce.services.order.payment.paypal.PaypalCheckoutManager;
import com.digital.commerce.services.order.payment.paypal.valueobject.PaypalLookupResponse;
import com.digital.commerce.services.order.shipping.ShippingMethod;
import com.digital.commerce.services.pricing.ShippingOptionsTools;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.promotion.DigitalGWPManager;
import com.digital.commerce.services.storelocator.DigitalGeoLocationUtil;
import com.digital.commerce.services.storelocator.DigitalStoreLocatorInventoryManager;
import com.digital.commerce.services.storelocator.SkuInventoryDetailBean;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.transaction.SystemException;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalCartModifierFormHandler extends CartModifierFormHandler {

	private static final String CLASSNAME = DigitalCartModifierFormHandler.class.getName();

	private static final String EXCEEDED_SKU_LIMIT = "exceededSkuLimit";

	private static final String EXCEEDED_ITEM_IN_CART_LIMIT = "exceededItemInCartLimit";

	private static final String EXCEEDED_PURCHASE_LIMIT = "exceededPurchaseLimit";

	private static final String MSG_NEED_LOGIN = "validation.checkout.needlogin";

	private static final String MSG_OUT_OF_INVENTORY = "msgOutOfInventory";

	private static final String PAYPAL_EXPRESS_CHECKOUT_ERROR = "paypalExpressCheckoutError";

	private static final String PAYPAL_GENERAL_ERROR = "paypalGeneralError";

	private static final String PAYPAL_UNAVAILABLE_ERROR = "paypalUnavailableError";

	private static final String UPDATE_ITEM_ERROR = "updateItemError";

	private static final String COMMERCE_ITEM_NOT_FOUND = "commerceItemNotFound";

	private static final String PAYPAL_ZERO_AMOUNT_CHARGED_ERROR = "validation.checkout.paypal.zeroamountcharged";

	private static final String PAYPAL_ERROR_ALREADY_AUTHENTICATED = "paypalErrorAlreadyAuthenticated";

	private static final String COMMERCE_ITEM_SHIPPING_GROUP_NOT_FOUND = "commerceItemShippingGroupRelNotFound";

	private static final String ORDER_TOTAL_EXCEEDS_BOPIS_LIMIT = "orderTotalExceedsBOPISLimit";

	private static final String LOCATION_DETAILS_UNAVAILABLE = "locationDetailsUnavailable";

	private static final String INVENTORY_DETAILS_UNAVAILABLE = "inventoryDetailsUnavailable";

	private static final String STORE_BOPIS_INELIGIBLE = "storeBopisIneligible";

	private static final String STOCK_UNAVAILABLE = "stockUnavailable";

	private static final String STOCK_PARTIAL_UNAVAILABLE = "stockPartialUnavailable";

	private static final String BOPIS_STOCK_UNAVAILABLE = "bopisStockUnavailable";

	private static final String BOPIS_STOCK_PARTIAL_UNAVAILABLE = "bopisStockPartialUnavailable";

	private static final String STORE_BOSTS_INELIGIBLE = "storeBostsIneligible";

	private static final String PRODUCT_SKU_NOT_FOUND = "productSkuNotFound";

	private static final String PRODUCT_INACTIVE = "productInactive";

	private static final String SKU_BOPIS_INELIGIBLE = "skuBopisIneligible";

	private static final String SKU_BOSTS_INELIGIBLE = "skuBostsIneligible";

	private static final String CATALOG_REF_EMPTY = "catalogRefEmpty";

	private static final String COMMERCE_ITEM_EMPTY = "commerceItemEmpty";

	private static final String INVALID_ADD_ITEM_TO_CART_INPUT = "invalidAddItemToCartInput";

	private static final String SHIPPING_GROUP_REMOVAL_ERROR = "shippingGroupRemovalError";

	List<String> warningMessages = new ArrayList<>();

	private String replaceItem;

	private List<?> userMessages = new ArrayList<Object>();

	private MessageLocator messageLocator;

	private InventoryHelper inventoryTools;

	private int skuQtyLimit = -1;

	private int cartLineLimit = -1;

	private double purchaseLimit = -1;

	private String shippingMethod;

	private PricingModelHolder userPricingModel;

	private PricingTools pricingTools;

	private String storeDetailsQuery;

	private PromotionTools promotionTools;
	private Order order;

	private ProfileServices profileServices;

	private String login;

	private String password;

	private boolean guestCheckout = false;

	private boolean promoCodeOption = false;

	private DigitalProfileTools profileTools;

	private boolean isPromoUnqualified = false;

	private ProfileRequestTools profileRequestTools;

	private boolean cartWasEmpty = true;

	private String size;
	private String color;
	private String width;
	private List<String> savedBuckets = null;

	private String originOfCommerceItem;

	private String ppPayload = null;

	private String ppTermURL = null;

	private String dswSessionId = null;

	private String acsURL = null;

	private boolean payPalCheckout;

	private String payPalLookupSuccessURL;
	private String payPalLookupErrorURL;

	private String shipType;
	private String storeId;
	private String storeLocationId;

	private String cardinalOrderId;
	private String skuId;
	private String item;

	private ShippingOptionsTools shippingOptionsTools;

	private CatalogTools catalogTools;

	private DigitalGeoLocationUtil geoLocationUtil;

	private DigitalStoreLocatorInventoryManager storeLocatorInventoryManager;

	private String rolledUpInventoryId;

	private String contactCenterUserPath;

	private GiftCardPaymentServices paymentServices;
	
	private String vendorPaymentProcessorErrCode;
	
	private Map<String,String> paypalReasonCodeMapping = new HashMap<>();
	
	private boolean isSkuEligibleForStorePickup = false;
	
	private boolean isSkuEligibleForShipToStore = false;
	
	private boolean isStoreEligibleForBOPIS = false;
	
	private boolean isStoreEligibleForBOSTS = false;

	private static final String SHIP_RESTRICTION = "shipRestriction";
	
	private DigitalGWPManager gwpManager;
	private DigitalServiceConstants dswConstants;
	private StoreNetInventoryReservationService storeNetInventoryReservationClient;

	/**
	 * Returns the default shipping method to use for the cart.
	 *
	 * @return String
	 */
	private String getDefaultShippingMethod() {
		// DSWShippingGroupManager sgm =
		// (DSWShippingGroupManager)this.getShippingGroupManager();
		// return sgm.getShippingMethod( getOrder() );
		return null;
	}

	/**
	 * 
	 * @return String
	 */
	public String getShippingMethod() {
		if (DigitalStringUtil.isBlank(shippingMethod)) {
			shippingMethod = getDefaultShippingMethod();
		}

		return shippingMethod;
	}

	/**
	 * 
	 * @param promoCodeOption
	 */
	public void setPromoCodeOption(boolean promoCodeOption) {
		if (isLoggingDebug()) {
			logDebug("setPromoCodeOption(boolean promoCodeOption=" + promoCodeOption + ") - start"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		if (isLoggingDebug()) {
			logDebug("Setting promo code option: " + promoCodeOption);
		}

		this.promoCodeOption = promoCodeOption;

		if (isLoggingDebug()) {
			logDebug("setPromoCodeOption(boolean) - end"); //$NON-NLS-1$
		}
	}

	/**
	 * 
	 * @return DigitalShippingGroupManager
	 */
	public DigitalShippingGroupManager getShippingGroupManager() {
		return (DigitalShippingGroupManager) getOrderManager().getShippingGroupManager();
	}


	@Override
	/**
	 * Add the item to the Order.
	 * 
	 * @author
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             if an error
	 * @throws IOException
	 *             if an error
	 */
	protected void addItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String METHOD_NAME = "DigitalCartModifierFormHandler.addItemToOrder";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				if (this.getFormError()) {
					// This is to avoid adding item to card if there are any
					// validation
					// errors in preAddItemToOrder
					return;
				} else {
					super.addItemToOrder(pRequest, pResponse);
				}
			} catch (Exception e) {
				String msg = formatUserMessage("errorAddingToOrder", pRequest, pResponse);
                addFormException(new DropletException(msg, "errorAddingToOrder"));
			}finally {
				if(rrm != null){
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
	}

	/**
	 * Check the product eligibility. Marking rollback of transaction is done in
	 * the caller method
	 * 
	 */
	private void checkProductEligibility() {
		try {
			String locationId = null;
			String storeName = null;
			boolean shipToStoreBosts=false;
			boolean pickupInStoreBOPIS=false;

			Map<String, SkuInventoryDetailBean> skuResults = null;
			SkuInventoryDetailBean skuInventoryDetailBean = null;
			RepositoryItem item=null;
			RepositoryItem[] items=null;
			
			if(storeLocationId!=null){
				locationId=storeLocationId;
			} 

			if(null!=locationId){
				item=getGeoLocationUtil().findById(locationId);
			}

			if(storeId != null && item==null) {
				String storeDetailsQueryStat = getStoreDetailsQuery() + storeId;
				items = getGeoLocationUtil().findBySQL(storeDetailsQueryStat, "location");
				if (items != null && items.length > 0) {
					item=items[0];
				}
			}	

			if (item == null && (null!=locationId || null!=storeId)) {
				// no location for given store id
				String msg = getMessageLocator().getMessageString(LOCATION_DETAILS_UNAVAILABLE);
				addFormException(new DropletException(msg, LOCATION_DETAILS_UNAVAILABLE));
			}

			if(null!=item ){
				shipToStoreBosts =getBooleanValue((String) item.getPropertyValue("shipToStoreBOSTS"));
				pickupInStoreBOPIS =getBooleanValue((String) item.getPropertyValue("pickupInStoreBOPIS"));

				locationId = (item.getPropertyValue("locationId") != null)? (String) item.getPropertyValue("locationId") : null;
				storeName = (item.getPropertyValue("mallPlazaName") != null)? (String) item.getPropertyValue("mallPlazaName") : null;
			} 
			if(null==locationId ){
				locationId = getRolledUpInventoryId();
			}

			
			//KTLO1-592 set the location ID
			setStoreLocationId(locationId);
			
			Map<String, Object> result = storeLocatorInventoryManager
					.getInventoryForSkus(Arrays.asList(getCatalogRefIds()), locationId);

			if (result == null) {
				String msg = getMessageLocator().getMessageString(INVENTORY_DETAILS_UNAVAILABLE);
				addFormException(new DropletException(msg, INVENTORY_DETAILS_UNAVAILABLE));
			}

			if (result != null && result.get("skusResult") != null) {
				skuResults = (Map<String, SkuInventoryDetailBean>) result.get("skusResult");
				skuInventoryDetailBean = skuResults.get(getCatalogRefIds()[0]);
			}

			// check this condition
			if (skuInventoryDetailBean == null) {
				String msg = getMessageLocator().getMessageString(INVENTORY_DETAILS_UNAVAILABLE);
				addFormException(new DropletException(msg, INVENTORY_DETAILS_UNAVAILABLE));
			}
			else{
				setStoreEligibleForBOPIS(skuInventoryDetailBean.isPickUpInStore());
				setStoreEligibleForBOSTS(skuInventoryDetailBean.isShipToStore());
			}

			if (skuInventoryDetailBean != null && DigitalBaseConstants.SHIP_TYPE_BOPIS.equals(getShipType())) {				
				if (skuInventoryDetailBean.getStoreLevelStock() == 0) {
					// Quantity requested is not available
					String msg = getMessageLocator().getMessageString(BOPIS_STOCK_UNAVAILABLE);
					addFormException(new DropletException(msg, BOPIS_STOCK_UNAVAILABLE));
				} else if (skuInventoryDetailBean.getStoreLevelStock() < getQuantity()) {
					// Quantity requested is partially unavailable
					String msg = getMessageLocator().getMessageString(BOPIS_STOCK_PARTIAL_UNAVAILABLE);
					addFormException(new DropletException(msg, BOPIS_STOCK_PARTIAL_UNAVAILABLE));
				}
			} else {
				if (skuInventoryDetailBean != null) {
					if (skuInventoryDetailBean.getRolledUpStock() == 0) {
						// Quantity requested is not available
						String msg = getMessageLocator().getMessageString(STOCK_UNAVAILABLE);
						addFormException(new DropletException(msg, STOCK_UNAVAILABLE));
					} else if (skuInventoryDetailBean.getRolledUpStock() < getQuantity()) {
						// Quantity requested is partially unavailable
						String msg = getMessageLocator().getMessageString(STOCK_PARTIAL_UNAVAILABLE);
						addFormException(new DropletException(msg, STOCK_PARTIAL_UNAVAILABLE));
					}
				}
			}

			RepositoryItem product = getCatalogTools().findProduct(getProductId());
			RepositoryItem sku = null;
			if (getCatalogRefIds() != null && getCatalogRefIds().length > 0) {
				sku = getCatalogTools().findSKU(getCatalogRefIds()[0]);
			}

			
			if (product == null || sku == null) {
				// product details or sku details not found
				String msg = getMessageLocator().getMessageString(PRODUCT_SKU_NOT_FOUND);
				addFormException(new DropletException(msg, PRODUCT_SKU_NOT_FOUND));
			} else {

				boolean productActive = (Boolean) product.getPropertyValue("isActive");
				boolean skuBopisFlag = getBooleanValue((String) sku.getPropertyValue("bopisEnabled"));
				boolean skuBostsFlag = getBooleanValue((String) sku.getPropertyValue("bostsEnabled"));
				
				setSkuEligibleForShipToStore(skuBostsFlag);
				setSkuEligibleForStorePickup(skuBopisFlag);

				if (!productActive) {
					// product is not active
					String msg = getMessageLocator().getMessageString(PRODUCT_INACTIVE);
					addFormException(new DropletException(msg, PRODUCT_INACTIVE));
				}

				if (DigitalBaseConstants.SHIP_TYPE_BOPIS.equals(getShipType()) && !skuBopisFlag) {
					// sku is not bopis eligible
					String msg = getMessageLocator().getMessageString(SKU_BOPIS_INELIGIBLE);
					addFormException(new DropletException(msg, SKU_BOPIS_INELIGIBLE));
				}
				
				if (DigitalBaseConstants.SHIP_TYPE_BOPIS.equals(getShipType()) && !pickupInStoreBOPIS) {
					// store  is not bosts eligible
					String msg = getMessageLocator().getMessageString(STORE_BOPIS_INELIGIBLE,storeName);
					addFormException(new DropletException(msg, STORE_BOPIS_INELIGIBLE));
				}

				if (DigitalBaseConstants.SHIP_TYPE_BOSTS.equals(getShipType()) && !skuBostsFlag) {
					// sku is not bopis eligible
					String msg = getMessageLocator().getMessageString(SKU_BOSTS_INELIGIBLE);
					addFormException(new DropletException(msg, SKU_BOSTS_INELIGIBLE));
				}
				
				if (DigitalBaseConstants.SHIP_TYPE_BOSTS.equals(getShipType()) && !shipToStoreBosts) {
					// store  is not bosts eligible
					String msg = getMessageLocator().getMessageString(STORE_BOSTS_INELIGIBLE);
					addFormException(new DropletException(msg, STORE_BOSTS_INELIGIBLE));
				}
			}

		} catch (RepositoryException e) {
			logError("Exception occured in checkProductEligibility", e);
			String msg = getMessageLocator().getMessageString(UPDATE_ITEM_ERROR);
			addFormException(new DropletException(msg, UPDATE_ITEM_ERROR));
		}
	}

	
	private void checkProductEligibility(DigitalCommerceItem ci) {
		try {
			String locationId = null;
			String storeName = null;
			boolean shipToStoreBosts=false;
			boolean pickupInStoreBOPIS=false;
			RepositoryItem[] items=null;
			RepositoryItem item=null;

			Map<String, SkuInventoryDetailBean> skuResults = null;
			SkuInventoryDetailBean skuInventoryDetailBean = null;
			
			if(storeLocationId!=null){
				locationId=storeLocationId;
			}
			if(locationId==null && ci!=null){
				locationId=(String)ci.getPropertyValue("locationId");
				if(null!=locationId){
					item=getGeoLocationUtil().findById(locationId);
				}
			}
			if(ci!=null){
				storeId=(String)ci.getPropertyValue("StoreId");
			}
			if (storeId != null && item==null) {
				String storeDetailsQueryStat = getStoreDetailsQuery() + storeId;
				items = getGeoLocationUtil().findBySQL(storeDetailsQueryStat, "location");
				if (items != null && items.length > 0) {
					item=items[0];
				}
			}

			if (item == null && (null!=locationId || null!=storeId)) {
				// no location for given store id
				String msg = getMessageLocator().getMessageString(LOCATION_DETAILS_UNAVAILABLE);
				addFormException(new DropletException(msg, LOCATION_DETAILS_UNAVAILABLE));
			}

			if(null!=item){
				shipToStoreBosts =getBooleanValue((String) item.getPropertyValue("shipToStoreBOSTS"));
				pickupInStoreBOPIS =getBooleanValue((String) item.getPropertyValue("pickupInStoreBOPIS"));
				setStoreEligibleForBOPIS(pickupInStoreBOPIS);
				setStoreEligibleForBOSTS(shipToStoreBosts);
				locationId = (item.getPropertyValue("locationId") != null)? (String) item.getPropertyValue("locationId") : null;
				storeName = (item.getPropertyValue("mallPlazaName") != null)? (String) item.getPropertyValue("mallPlazaName") : null;
			}

			if(null==locationId ){
				locationId = getRolledUpInventoryId();
			}

			//KTLO1-592 set the location ID
			ci.setPropertyValue("locationId", locationId);
			
			String catalogRefId=(String)ci.getPropertyValue("catalogRefId");
			Map<String, Object> result = storeLocatorInventoryManager.getInventoryForSkus(Arrays.asList(catalogRefId), locationId);

			if (result == null) {
				String msg = getMessageLocator().getMessageString(INVENTORY_DETAILS_UNAVAILABLE);
				addFormException(new DropletException(msg, INVENTORY_DETAILS_UNAVAILABLE));
			}

			if (result != null && result.get("skusResult") != null) {
				skuResults = (Map<String, SkuInventoryDetailBean>) result.get("skusResult");
				skuInventoryDetailBean = skuResults.get(catalogRefId);
			}
			
			// check this condition
			if (skuInventoryDetailBean == null) {
				String msg = getMessageLocator().getMessageString(INVENTORY_DETAILS_UNAVAILABLE);
				addFormException(new DropletException(msg, INVENTORY_DETAILS_UNAVAILABLE));
			}else{
				setStoreEligibleForBOPIS(skuInventoryDetailBean.isPickUpInStore());
				setStoreEligibleForBOSTS(skuInventoryDetailBean.isShipToStore());
			}

			if (skuInventoryDetailBean != null && DigitalBaseConstants.SHIP_TYPE_BOPIS.equals(getShipType())) {				
				if (skuInventoryDetailBean.getStoreLevelStock() == 0) {
					// Quantity requested is not available
					String msg = getMessageLocator().getMessageString(BOPIS_STOCK_UNAVAILABLE);
					addFormException(new DropletException(msg, BOPIS_STOCK_UNAVAILABLE));
				} else if (skuInventoryDetailBean.getStoreLevelStock() < getQuantity()) {
					// Quantity requested is partially unavailable
					String msg = getMessageLocator().getMessageString(BOPIS_STOCK_PARTIAL_UNAVAILABLE);
					addFormException(new DropletException(msg, BOPIS_STOCK_PARTIAL_UNAVAILABLE));
				}
			} else {
				if (skuInventoryDetailBean != null) {
					if (skuInventoryDetailBean.getRolledUpStock() == 0) {
						// Quantity requested is not available
						String msg = getMessageLocator().getMessageString(STOCK_UNAVAILABLE);
						addFormException(new DropletException(msg, STOCK_UNAVAILABLE));
					} else if (skuInventoryDetailBean.getRolledUpStock() < getQuantity()) {
						// Quantity requested is partially unavailable
						String msg = getMessageLocator().getMessageString(STOCK_PARTIAL_UNAVAILABLE);
						addFormException(new DropletException(msg, STOCK_PARTIAL_UNAVAILABLE));
					}
				}
			}

			RepositoryItem product = null;
			
			if(ci!=null){
				product=getCatalogTools().findProduct((String) ci.getPropertyValue("productId"));
			}
			
			RepositoryItem sku = null;
			
			if (catalogRefId != null ) {
				sku = getCatalogTools().findSKU(catalogRefId);
			}
	
			
			if(null!=product && null!=sku){

				boolean productActive = (Boolean) product.getPropertyValue("isActive");
				boolean skuBopisFlag = getBooleanValue((String) sku.getPropertyValue("bopisEnabled"));
				boolean skuBostsFlag = getBooleanValue((String) sku.getPropertyValue("bostsEnabled"));
				
				setSkuEligibleForShipToStore(skuBostsFlag);
				setSkuEligibleForStorePickup(skuBopisFlag);

				if (!productActive) {
					// product is not active
					String msg = getMessageLocator().getMessageString(PRODUCT_INACTIVE);
					addFormException(new DropletException(msg, PRODUCT_INACTIVE));
				}

				if (DigitalBaseConstants.SHIP_TYPE_BOPIS.equals(getShipType()) && !skuBopisFlag) {
					// sku is not bopis eligible
					String msg = getMessageLocator().getMessageString(SKU_BOPIS_INELIGIBLE);
					addFormException(new DropletException(msg, SKU_BOPIS_INELIGIBLE));
				}
				
				if (DigitalBaseConstants.SHIP_TYPE_BOPIS.equals(getShipType()) && !pickupInStoreBOPIS) {
					// store  is not bosts eligible
					String msg = getMessageLocator().getMessageString(STORE_BOPIS_INELIGIBLE,storeName);
					addFormException(new DropletException(msg, STORE_BOPIS_INELIGIBLE));
				}

				if (DigitalBaseConstants.SHIP_TYPE_BOSTS.equals(getShipType()) && !skuBostsFlag) {
					// sku is not bopis eligible
					String msg = getMessageLocator().getMessageString(SKU_BOSTS_INELIGIBLE);
					addFormException(new DropletException(msg, SKU_BOSTS_INELIGIBLE));
				}
				
				if (DigitalBaseConstants.SHIP_TYPE_BOSTS.equals(getShipType()) && !shipToStoreBosts) {
					// store  is not bosts eligible
					String msg = getMessageLocator().getMessageString(STORE_BOSTS_INELIGIBLE);
					addFormException(new DropletException(msg, STORE_BOSTS_INELIGIBLE));
				}
			}

		} catch (RepositoryException e) {
			logError("Exception occured in checkProductEligibility", e);
			String msg = getMessageLocator().getMessageString(UPDATE_ITEM_ERROR);
			addFormException(new DropletException(msg, UPDATE_ITEM_ERROR));
		}
	}

	/**
	 * 
	 * @param str
	 * @return true or false
	 */
	private boolean getBooleanValue(String str) {
		if (str != null && str.equalsIgnoreCase("Y")) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param order
	 * @param shipType
	 * @param storeId
	 * @return ShippingGroup
	 * @throws IllegalStateException
	 */
	private ShippingGroup findShippingGroup(Order order, String shipType, String storeId) throws IllegalStateException {
		ShippingGroup retVal = null;
		try {
			if (!Strings.isNullOrEmpty(getShipType()) && !Strings.isNullOrEmpty(getStoreId())) {
				retVal = getShippingGroupManager().getFirstNonGiftCardShippingGroup(order, shipType, storeId);
				if (retVal == null) {
					retVal = getShippingGroupManager().createShippingGroupWithStoreDetails(order,
							(Profile) this.getProfile(), shipType, storeId);
					getShippingGroupMapContainer()
							.addShippingGroup(getShippingGroupMapContainer().getDefaultShippingGroupName(), retVal);
					try {
						// Profile lock acquire & release is already
						// handled in beforeSet() and afterSet() methods
						// of parent ATG PurchaseProcessFormHandler
						// form handlers
						synchronized (order) {
							// Do order updates
							getOrderManager().updateOrder(order);
						}
					} catch (CommerceException e) {
						logError("Unable to update the order", e);
					}
				}
			} else {
				retVal = getShippingGroupManager().getFirstNonBopisBostsHardgoodShippingGroup(order);
			}
		} catch (CommerceException e) {
			throw new IllegalStateException(
					String.format("Failed to create shipping group for order %s", order.getId()), e);
		}
		return retVal;
	}

	/**
	 * Updates the item to the Order. Finds the existing commerce item and
	 * updates the quantity if the catalogRefId is matching or add the new sku
	 * to the order. If storeId and Shiptype is found- corresponding
	 * shippinggroup is added or modified.
	 * 
	 * @author
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             if an error
	 * @throws IOException
	 *             if an error
	 */
	public boolean handleUpdateItemToOrder(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("DigitalCartModifierFormHandler.handleUpdateItemToOrder() ---- >> start");
			logDebug("Setting CI " + getItem() + "to  quantity " + getQuantity());
		}		
		final String METHOD_NAME = "DigitalCartModifierFormHandler.handleUpdateItemToOrder";
		boolean ret = false;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		UserTransactionDemarcation td = null;
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {			
			try {

				td = TransactionUtils.startNewTransaction(this.getName(), METHOD_NAME);
				if (!validateBOPISShipTypeQuantity()) {
					if (isLoggingDebug()) {
						logDebug(
								"DigitalCartModifierFormHandler.handleUpdateItemToOrder() ---- >> validateBOPISShipTypeQuantity() is:: "
										+ validateBOPISShipTypeQuantity());
					}
					addFormException(
							new DropletException(getMessageLocator().getMessageString(ORDER_TOTAL_EXCEEDS_BOPIS_LIMIT),
									ORDER_TOTAL_EXCEEDS_BOPIS_LIMIT));
					throw new DigitalRuntimeException(
							getMessageLocator().getMessageString(ORDER_TOTAL_EXCEEDS_BOPIS_LIMIT));
				}

				if (getCatalogRefIds() == null || !(getCatalogRefIds().length > 0)) {
					if (isLoggingDebug()) {
						logDebug("DigitalCartModifierFormHandler.handleUpdateItemToOrder() ---- >> catalogRef Id is null");
					}
					String msg = getMessageLocator().getMessageString(CATALOG_REF_EMPTY);
					addFormException(new DropletException(msg, CATALOG_REF_EMPTY));
					throw new DigitalRuntimeException(msg);
				}

				if (DigitalStringUtil.isBlank(getItem())) {
					if (isLoggingDebug()) {
						logDebug(
								"DigitalCartModifierFormHandler.handleUpdateItemToOrder() ---- >> commerceItem id is null");
					}
					String msg = getMessageLocator().getMessageString(COMMERCE_ITEM_EMPTY);
					addFormException(new DropletException(msg, COMMERCE_ITEM_EMPTY));
					throw new DigitalRuntimeException(msg);
				}

				if (DigitalStringUtil.isNotBlank(getItem()) && getCatalogRefIds() != null & getCatalogRefIds().length > 0) {
					DigitalCommerceItem commerceItemToUpdate = Iterables
							.tryFind(Iterables.filter(order.getCommerceItems(), DigitalCommerceItem.class),
									new FindCommerceItemById(getItem()))
							.orNull();

					if (commerceItemToUpdate == null) {
						if (isLoggingDebug()) {
							logDebug(
									"DigitalCartModifierFormHandler.handleUpdateItemToOrder() ---- >> requested CI is unavaiable to update");
						}

						String skuIdParam = "";
						skuIdParam = getCatalogRefIds()[0];
						Object[] params = { "" + skuIdParam };

						String msg = getMessageLocator().getMessageString(COMMERCE_ITEM_NOT_FOUND, params);
						addFormException(new DropletException(msg, COMMERCE_ITEM_NOT_FOUND));
						throw new DigitalRuntimeException(msg);
					}
									
					// CHECK FOR QUANTITY AND ITEM
					if (commerceItemToUpdate != null && getCatalogRefIds() != null & getCatalogRefIds().length > 0) {					

						// Profile lock acquire & release is already
						// handled in beforeSet() and afterSet() methods
						// of parent ATG PurchaseProcessFormHandler
						// form handlers
						synchronized (getOrder()) {
							if (getQuantity() > commerceItemToUpdate.getQuantity()) {// increasing
																						// the
																						// quantity
								if (MultiSiteUtil.isItemSkuLimitExceeded(MultiSiteUtil.getCartSkuLimit(),
										commerceItemToUpdate.getQuantity())) {
									Object[] params = { "" + MultiSiteUtil.getCartSkuLimit() };
									String msg = getMessageLocator().getMessageString(EXCEEDED_SKU_LIMIT, params);
									addFormException(new DropletException(msg, EXCEEDED_SKU_LIMIT));
									throw new DigitalRuntimeException("EXCEEDED_SKU_LIMIT :: " + msg);
								} else if (MultiSiteUtil.isCartPurchaseLimitExceeded(getOrder(),
										MultiSiteUtil.getCartPurchaseLimit())) {
									NumberFormat nf = NumberFormat.getCurrencyInstance();
									Object[] params = { nf.format(MultiSiteUtil.getCartPurchaseLimit()) };
									String msg = getMessageLocator().getMessageString(EXCEEDED_PURCHASE_LIMIT, params);
									addFormException(new DropletException(msg, EXCEEDED_PURCHASE_LIMIT));
									throw new DigitalRuntimeException("EXCEEDED_PURCHASE_LIMIT :: " + msg);
								}
							}

							if (getQuantity() == 0) {
								Iterator<?> relationshipIterator = commerceItemToUpdate.getShippingGroupRelationships()
										.iterator();

								getCommerceItemManager().removeItemFromOrder(getOrder(), commerceItemToUpdate.getId());

								while (relationshipIterator.hasNext()) {
									ShippingGroupCommerceItemRelationship rel = (ShippingGroupCommerceItemRelationship) relationshipIterator
											.next();
									ShippingGroup shippingGroupToRemoveCI = rel.getShippingGroup();
									if (shippingGroupToRemoveCI.getCommerceItemRelationshipCount() == 0) {
										getOrder().removeShippingGroup(shippingGroupToRemoveCI.getId());
									}
								}
							} else {
								
								checkProductEligibility();
							
								if (getFormError()) {
									// Transaction if marked for rollback in the
									// finally block
									return false;
								}

								// checks for change in shiptype or store id or
								// bopis/bosts commerce item.
								if (Strings.isNullOrEmpty(getShipType())) {
									setShipType("SHIP");
								}

								changeShipTypeOfCommerceItem(commerceItemToUpdate, request, response);

								updateCommerceItemWithDswExtensions(commerceItemToUpdate);
								Object sku = getCommerceItemManager().getSku(getCatalogRefIds()[0], null);
								commerceItemToUpdate.getAuxiliaryData().setCatalogRef(sku);
								commerceItemToUpdate.setCatalogRefId(getCatalogRefIds()[0]);
							}

							getProfileTools().persistShoppingCarts(getProfile(), getShoppingCart());
							
							if (!getFormError()) {
								// Do order updates
								getOrderManager().updateOrder(getOrder());
								//TODO: KTLO-16
								Order order = this.getShoppingCart().getCurrent();
								validateBopisBostsForOrderItems(order);
								ret = true;
							} else {
								// Transaction if marked for rollback in the
								// finally
								// block
								ret = false;
							}
						}
					}

				}
			} catch (CommerceException | RepositoryException e) {
				this.logError("Exception occured while handling ", e);
				String msg = getMessageLocator().getMessageString(UPDATE_ITEM_ERROR);
				addFormException(new DropletException(msg, UPDATE_ITEM_ERROR));
				TransactionUtils.rollBackTransaction(td, this.getName(), METHOD_NAME);
				ret = false;
			} catch (DigitalRuntimeException exc) {
				if (isLoggingError()) {
					logError("Transaction Demarcation Error: ", exc);
				}
				TransactionUtils.rollBackTransaction(td, this.getName(), METHOD_NAME);
				ret = false;
			} finally {
				// Mark the transaction rollback if there are any form
				// validation errors
				if (getFormError()) {
					try {
						this.setTransactionToRollbackOnly();
					} catch (SystemException e) {
						logError("Transaction rollback exception :: ", e);
					}
				}
				TransactionUtils.endTransaction(td, this.getName(), METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
			}
		}
		return ret;
	}

	/**
	 * Marking of transaction as rollback is done in the caller if there are any
	 * form exceptions
	 * 
	 * @param commerceItemToUpdate
	 * @param request
	 * @param response
	 */
	private void changeShipTypeOfCommerceItem(DigitalCommerceItem commerceItemToUpdate, DynamoHttpServletRequest request,
			DynamoHttpServletResponse response) {
		ShippingGroup shippingGroupToSet = null;
		String ciToUpdate = getItem();
		ShippingGroupCommerceItemRelationship sgcirel = null;
		ShippingGroup sg = null;

		Collection<ShippingGroup> shippingGroups = order.getShippingGroups();
		Iterator<ShippingGroup> shippingGrouperator = shippingGroups.iterator();

		// Below logic is to get the ShippingGroupRelationShip object from the
		// relationshipId
		while (shippingGrouperator.hasNext()) {
			sg = shippingGrouperator.next();
			if (sg instanceof HardgoodShippingGroup) {
				List<?> relationships = sg.getCommerceItemRelationships();
				Iterator<?> relationshipIterator = relationships.iterator();
				while (relationshipIterator.hasNext()) {
					ShippingGroupCommerceItemRelationship rel = (ShippingGroupCommerceItemRelationship) relationshipIterator
							.next();
					if (rel.getCommerceItem().getId().equalsIgnoreCase(ciToUpdate)) {
						sgcirel = rel;
						break;
					}
				}
			}
		}
		try {
			// BOSTS
			if (sgcirel != null) {
				// SHIP
				shippingGroupToSet = findShippingGroup(order, getShipType(), getStoreId());

				ShippingGroup shippingGroupToRemoveCI = sgcirel.getShippingGroup();

				boolean ciExists = false;
				ShippingGroupCommerceItemRelationship rel = null;

				CommerceItem ciToRemove = sgcirel.getCommerceItem();

				if((null != shippingGroupToSet) && (null != ciToRemove)) {
					List<?> existingRelationships = shippingGroupToSet.getCommerceItemRelationships();
					Iterator<?> relationshipIterator = existingRelationships.iterator();

					// String hgShipType = (String)hsg.getPropertyValue("shipType");
					// String hgStoreId = (String)hsg.getPropertyValue("storeId");
					while (relationshipIterator.hasNext()) {
						rel = (ShippingGroupCommerceItemRelationship) relationshipIterator.next();
						if (rel.getCommerceItem().getAuxiliaryData().getCatalogRef() == ciToRemove.getAuxiliaryData()
								.getCatalogRef()) {
							ciExists = true;
							break;
						}
					}
				}
				
				// Profile lock acquire & release is already
				// handled in beforeSet() and afterSet() methods
				// of parent ATG PurchaseProcessFormHandler
				// form handlers
				synchronized (getOrder()) {
					if (ciExists && (null != rel) && (null != ciToRemove)) {
						// long quantity = rel.getQuantity() +
						// sgcirel.getQuantity();
						getCommerceItemManager().adjustItemRelationshipsForQuantityChange(getOrder(),
								rel.getCommerceItem(), getQuantity());
						rel.getCommerceItem().setQuantity(getQuantity());
						rel.getCommerceItem().setCatalogRefId(getCatalogRefIds()[0]);
						((DigitalCommerceItem) rel.getCommerceItem()).setStoreId(getStoreId());
						//KTLO1-592 Setting the locationId ont he commerceItem
						((DigitalCommerceItem) rel.getCommerceItem()).setLocationId(getStoreLocationId());
						((DigitalCommerceItem) rel.getCommerceItem()).setShipType(getShipType());

						rel.setQuantity(getQuantity());
						// KTLO1-1300 Commented below code as it was removing the item when quantity was updated
						/*if (rel.getCommerceItem().getId().equalsIgnoreCase(ciToRemove.getId())) {
							getCommerceItemManager().removeItemFromOrder(getOrder(), ciToRemove.getId());
						}*/
					} else {
						shippingGroupToRemoveCI.removeCommerceItemRelationship(sgcirel.getId());
						// Set the relationship with the SG which is found in
						// order
						// or created.
						sgcirel.setShippingGroup(shippingGroupToSet);
						sgcirel.setQuantity(getQuantity());
						sgcirel.getCommerceItem().setQuantity(getQuantity());
						((DigitalCommerceItem) sgcirel.getCommerceItem()).setStoreId(getStoreId());
						//KTLO1-592 Setting the locationId ont he commerceItem
						((DigitalCommerceItem) sgcirel.getCommerceItem()).setLocationId(getStoreLocationId());
						((DigitalCommerceItem) sgcirel.getCommerceItem()).setShipType(getShipType());
					}

					// Remove shippingGroup from order if there are no commerce
					// Items associated with that SG
					if (shippingGroupToRemoveCI.getCommerceItemRelationshipCount() == 0 && !shippingGroupToRemoveCI
							.getShippingMethod().equals(ShippingMethod.getDefaultShippingMethod().getMappedValue())) {
						getOrder().removeShippingGroup(shippingGroupToRemoveCI.getId());
					}

					// Do order updates
					getOrderManager().updateOrder(getOrder());
				}
			} else {
				this.logError("No shippingGroup commerceItem relationship is found for given commerItem " + ciToUpdate);
				String msg = getMessageLocator().getMessageString(COMMERCE_ITEM_SHIPPING_GROUP_NOT_FOUND);
				addFormException(new DropletException(msg, COMMERCE_ITEM_SHIPPING_GROUP_NOT_FOUND));
			}
		} catch (ShippingGroupNotFoundException e) {
			this.logError("Unable to update ShippingGroup ", e);
			String msg = getMessageLocator().getMessageString(UPDATE_ITEM_ERROR);
			addFormException(new DropletException(msg, UPDATE_ITEM_ERROR));
		} catch (InvalidParameterException e) {
			this.logError("invalid parameter ", e);
			String msg = getMessageLocator().getMessageString(UPDATE_ITEM_ERROR);
			addFormException(new DropletException(msg, UPDATE_ITEM_ERROR));
		} catch (RelationshipNotFoundException e) {
			this.logError("ShippingGroup CommerceItem Relationship not found ", e);
			String msg = getMessageLocator().getMessageString(UPDATE_ITEM_ERROR);
			addFormException(new DropletException(msg, UPDATE_ITEM_ERROR));
		} catch (CommerceException e) {
			this.logError("Unable to update commerceItem ", e);
			String msg = getMessageLocator().getMessageString(UPDATE_ITEM_ERROR);
			addFormException(new DropletException(msg, UPDATE_ITEM_ERROR));
		}
	}

	/**
	 * 
	 * @param ci
	 * @param qty
	 * @param storeHubQty
	 * @param shipNode
	 * @param procureFromNode
	 * @param itemReserved
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void adjustItemsCS(DigitalCommerceItem ci, long qty, int storeHubQty, String shipNode, String procureFromNode,
			boolean itemReserved, DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("adjustItems(CommerceItem ci=" + ci + ", long qty=" + qty + ", ShipNode=" + shipNode //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ ", ProcureToNode=" + procureFromNode + ", DynamoHttpServletRequest request=" + request //$NON-NLS-1$ //$NON-NLS-2$
					+ ", DynamoHttpServletResponse response=" + response + ") - start");
		}
		/* Transaction handling block begins */
		try {
			acquireTransactionLock(request);
		} catch (DeadlockException de) {
			if (isLoggingError())
				logError(de);
		}
		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollbackFlag = false;
		try {
			td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);

			// Profile lock acquire & release is already
			// handled in beforeSet() and afterSet() methods
			// of parent ATG PurchaseProcessFormHandler
			// form handlers
			synchronized (getOrder()) {
				/* Core Order Processing Logic Starts */
				if (qty > 0) {
					if (isLoggingDebug()) {
						logDebug("Adjusting qty from: " + ci.getQuantity() + " to " + qty);
					}
					getCommerceItemManager().adjustItemRelationshipsForQuantityChange(getOrder(), ci, qty);
					ci.setQuantity(qty);
					ci.setShipNode(shipNode);
					ci.setProcureFromNode(procureFromNode);
					ci.setStoreHubQty(storeHubQty);
					ci.setItemReserved(itemReserved);
				} else {
					getCommerceItemManager().removeItemFromOrder(getOrder(), ci.getId());
				}
				getOrderManager().updateOrder(getOrder());
			}
			/* Core Order Processing Logic Ends */
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError("Failed attempting to adjust item relationships while reducing quantity due to inventory", e);
			}
			addFormException(new DropletException(this.formatUserMessage(MSG_ERROR_ADDING_TO_ORDER, request, response),
					MSG_ERROR_ADDING_TO_ORDER));
		} catch (Exception e) {
			rollbackFlag = true;
			logError("");
			logError(e);
			return;
		} finally {
			// Mark the transaction rollback if there are any form
			// validation errors
			if (this.getFormError()) {
				try {
					this.setTransactionToRollbackOnly();
				} catch (SystemException e) {
					logError("Transaction rollback exception :: ", e);
				}
			}
			try {
				if (td != null) {
					if (isLoggingDebug()) {
						logDebug("rollbackFlag=" + rollbackFlag);
						logDebug("ending TX");
					}
					td.end(rollbackFlag);
				}
			} catch (TransactionDemarcationException exc) {
				if (isLoggingError()) {
					logError("Transaction Demarcation Error: ", exc);
				}
			} finally {
				try {
					releaseTransactionLock(request);
				} catch (LockManagerException lme) {
					if (isLoggingError())
						logError(lme);
				}
			}
		}
		/* Transaction handling block ends */
		if (isLoggingDebug()) {
			logDebug("adjustItems(CommerceItem, long, DynamoHttpServletRequest, DynamoHttpServletResponse) - end"); //$NON-NLS-1$
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             if an error
	 * @throws IOException
	 *             if an error
	 */
	public void postAddItemToOrder(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws ServletException, IOException {
		final String METHOD_NAME = "postAddItemToOrder";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			if (isLoggingDebug()) {
				logDebug("postAddItemToOrder(DynamoHttpServletRequest request=" + request //$NON-NLS-1$
						+ ", DynamoHttpServletResponse response=" + response + ") - start"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			String[] skuIds = getCatalogRefIds();
			Order order = this.getShoppingCart().getCurrent();
			
			String shipType = "SHIP";
			
			if(getShipType() != null){
				shipType = getShipType();
			}
			
			//TODO: KTLO-16
			validateBopisBostsForOrderItems(order);
			
			List<CommerceItem> commereItems = order.getCommerceItems();
			for (CommerceItem ci : commereItems) {
				String ciSkuId = ci.getCatalogRefId();
				if (null != ciSkuId) {
					for (String skuId : skuIds) {
						DigitalCommerceItem dci = (DigitalCommerceItem) ci;						
						if (skuId.equalsIgnoreCase(ciSkuId) && shipType.equalsIgnoreCase(dci.getShipType())) {							
							// Add DSW Extensions for SHIP to HOME Commerce Items
							if(getStoreId() == null && DigitalStringUtil.isEmpty(dci.getStoreId())){
								dci.setOriginOfCommerceItem(OriginOfCommerceItem.Default.getValue());
								updateCommerceItemWithDswExtensions(ci);
								break;
							}
							
							// Add DSW Extensions for BOPIS/BOSTS Commerce Items
							if(getStoreId().equalsIgnoreCase(dci.getStoreId())){
								dci.setOriginOfCommerceItem(OriginOfCommerceItem.Default.getValue());
								updateCommerceItemWithDswExtensions(ci);
								break;
							}
						}
					}
				}
			}
			addTestBucketsToCommerceItems(request, response);

			super.postAddItemToOrder(request, response);
			doPostChangeItems(request, response);

			if (isLoggingDebug()) {
				logDebug("postAddItemToOrder(DynamoHttpServletRequest, DynamoHttpServletResponse) - end"); //$NON-NLS-1$
			}

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/**
	 * 
	 * @param bci
	 */
	protected void updateCommerceItemWithDswExtensions(CommerceItem bci) {
		try {
			AuxiliaryData axd = bci.getAuxiliaryData();
			if (isLoggingDebug()) {
				logDebug("Aux data: " + axd);
			}

			RepositoryItem sku = (RepositoryItem) axd.getCatalogRef();
			if (isLoggingDebug()) {
				logDebug("CAT REF: " + sku);
			}
			String productTitle = "";
			RepositoryItem product = (RepositoryItem) axd.getProductRef();
			if (product != null) {
				productTitle = (String) product.getPropertyValue(ProducCatalogPropertyManager.PRODUCT_TITLE.getValue());
			}
			DigitalCommerceItem ci = (DigitalCommerceItem) bci;
			ci.setProductTitle(productTitle);
			ci.setGiftReceipt(this.isGiftReceipt());
			ci.setGiftReceiptMessage(this.getGiftReceiptMessage());

			if (dswConstants.isChargeSendEnable()) {
				int storeHubQty = inventoryTools.guessQtyReserveAtStoreHub(bci.getCatalogRefId(),
						(int) bci.getQuantity());
				ci.setStoreHubQty(storeHubQty);
				ci.setItemReserved(false);
				if (ci.getStoreHubQty() > 0) {
					if (ci.getQuantity() > storeHubQty)
						ci.setShipNode(ShipNode.Multi.getMappedValue());
					else
						ci.setShipNode(ShipNode.Store.getMappedValue());
				} else {
					int dropshipQty = inventoryTools.guessQtyReserveAtDropship(ci.getCatalogRefId(),
							(int) ci.getQuantity());
					if (dropshipQty > 0) {
						if (ci.getQuantity() > dropshipQty)
							ci.setShipNode(ShipNode.Multi.getMappedValue());
						else
							ci.setShipNode(ShipNode.DropShip.getMappedValue());
					} else {
						ci.setShipNode(ShipNode.FulfillmentCenter.getMappedValue());
					}
				}
				ci.setItemQuantityReserved(-1);
				//TODO: KTLO-16
				//ci.setStorePickAvailable(isSkuEligibleForStorePickup && isStoreEligibleForBOPIS);
				//ci.setShipToStoreAvailable(isSkuEligibleForShipToStore && isStoreEligibleForBOSTS);
				// Profile lock acquire & release is already
				// handled in beforeSet() and afterSet() methods
				// of parent ATG PurchaseProcessFormHandler
				// form handlers
				synchronized (getOrder()) {
					this.getOrderManager().updateOrder(getOrder());
				}
			}

		} catch (Exception e) {
			logError("Failed to add item to bag", e);
			String msg = getMessageLocator().getMessageString(UPDATE_ITEM_ERROR);
			addFormException(new DropletException(msg, UPDATE_ITEM_ERROR));
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 */
	private void addTestBucketsToCommerceItems(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		if (!DigitalStringUtil.isEmpty(request.getParameter("testBuckets")) || this.savedBuckets != null) {
			List<String> buckets;
			if (this.savedBuckets != null) {
				buckets = this.savedBuckets;
			} else {
				buckets = Arrays.asList(request.getParameter("testBuckets").split(","));
			}

			if (buckets.size() > 0) {
				String sku = request.getParameter("catalogRefIds");
				if (sku != null) {
					try {
						List<DigitalCommerceItem> items = getOrder().getCommerceItemsByCatalogRefId(sku);
						if (items != null) {
							for (DigitalCommerceItem item : items) {
								item.setBuckets(buckets);
							}
						}
					} catch (Exception ex) {
						if (isLoggingWarning()) {
							logWarning("Unable to find commerce item for a/b testing", ex);
						}
						String msg = getMessageLocator().getMessageString(UPDATE_ITEM_ERROR);
						addFormException(new DropletException(msg, UPDATE_ITEM_ERROR));
					}
				}
			}
		}
	}

	/**
	 * Check the inventory level for the list of skuIds passed. If failed, add
	 * message to the formexception. per defect 787
	 *
	 * @param skuIds
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void inventoryCheck(String[] skuIds, DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Inventory Checking");
		}

		for (String skuId : skuIds) {
			try {
				if (!getInventoryTools().isAvailable(skuId)) {
					String skuName = getOrderManager().getCatalogTools().findSKU(skuId).getItemDisplayName();
					addFormException(new DropletException(
							this.getMessageLocator().getMessageString(MSG_OUT_OF_INVENTORY, new Object[] { skuName }),
							MSG_OUT_OF_INVENTORY));
				}
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError("Unable to find the display Name for item: " + skuId);
				}
			} finally {
				// Mark the transaction rollback if there are any form
				// validation errors
				if (this.getFormError()) {
					try {
						this.setTransactionToRollbackOnly();
					} catch (SystemException e) {
						logError("Transaction rollback exception :: ", e);
					}
				}
			}
		}
	}

	private PreCommitOrderManager preCommitOrderManager;

	/**
	 * After adding or removing an item from the cart, update the inventory, as
	 * well as the shipping options.
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doPostChangeItems(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws ServletException, IOException {
		final String METHOD_NAME = "doPostChangeItems";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			if (isLoggingDebug()) {
				logDebug("doPostAddItems(DynamoHttpServletRequest request=" + request //$NON-NLS-1$
						+ ", DynamoHttpServletResponse response=" + response + ") - start"); //$NON-NLS-1$ //$NON-NLS-2$
			}

			// adjustItemsByInventory( request, response );
			DigitalPurchaseProcessHelper pHelper = (DigitalPurchaseProcessHelper) getPurchaseProcessHelper();
			boolean isAutoSKURemoved = pHelper.adjustAutomaticSKUs(getOrder());
			if(isAutoSKURemoved){
				String msg = formatUserMessage("errorAddingCollateralItemToOrder", request, response);
				addFormException(new DropletException(msg, "errorAddingCollateralItemToOrder"));
			}

			persistProfileAndShoppingCart(request, response);

			if (isLoggingDebug()) {
				logDebug("doPostAddItems(DynamoHttpServletRequest, DynamoHttpServletResponse) - end"); //$NON-NLS-1$
			}
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/**
	 * Anonymous (or transient) profiles as well as their orders will only be
	 * persisted if they contain items. This is based on code in
	 * {@link atg.userprofiling.ProfileRequestServlet}.
	 *
	 * @param request
	 * @param response
	 */
	private void persistProfileAndShoppingCart(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		getProfileTools().persistShoppingCart(getShoppingCart());

	}

	private PaypalCheckoutManager paypalCheckoutManager;
	private boolean paypalPayment;
	boolean paypalExpressCheckout = false;
	/**
	 * Makes lookup service call with PayPal to redirect user to PayPal.
	 * 
	 * @param request
	 * @param response
	 * @return true or false
	 * @throws ServletException
	 *             if an error
	 * @throws IOException
	 *             if an error
	 */
	public boolean handlePayPalLookup(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws ServletException, IOException {
		final String METHOD_NAME = "DigitalCartModifierFormHandler.handlePayPalLookup";
		boolean ret = false;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			UserTransactionDemarcation td = null;
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

				if (this.isPaypalExpressCheckout() || (request.getSession()
						.getAttribute(SessionAttributesConstant.PAYPAL_XPRESS_CHECKOUT.getValue()) != null)) {
					setPayPalCheckout(true);
				}

				this.checkoutValidation(request, response);

				if (this.getFormError()) {
					return ret;
				}

				if (this.isPaypalExpressCheckout()) {
					preMoveToPurchaseInfo(request, response);
				}

				if (this.getFormError()) {
					return ret;
				}
				
				// if the profile is not loggedin, the user should have to log
				// in
				Profile profile = (Profile) getProfile();
				boolean loggedInUser = getProfileTools().isKnown(profile);
				if (!loggedInUser && !this.isGuestCheckout()) {
					String msg = getMessageLocator().getMessageString(MSG_NEED_LOGIN);
					// this.formatUserMessage(MSG_PLS_LOGIN, request, response);
					addFormException(new DropletException(msg, MSG_NEED_LOGIN));
					throw new DigitalRuntimeException("MSG_NEED_LOGIN : " + msg);
				}
				this.setGuestCheckout(!loggedInUser);
				// If PayPal lookup / authentication is already done, skip it
				// and take user directly to the Payment page
				String payPalEmail = DigitalCommonUtil.getSessionAttributeValue(SessionAttributesConstant.PAYPAL_EMAIL);
				if (DigitalStringUtil.isNotBlank(payPalEmail) && PaypalConstants.SELECTED_PAYMENT_PAYPAL.equalsIgnoreCase(
						DigitalCommonUtil.getSessionAttributeValue(SessionAttributesConstant.SELECTED_PAYMENT))) {
					
					String msg = getMessageLocator().getMessageString(PAYPAL_ERROR_ALREADY_AUTHENTICATED);
					this.logInfo(msg);
					addFormException(new DropletException(msg, PAYPAL_ERROR_ALREADY_AUTHENTICATED));
					return false;
				}

				PaypalLookupResponse paypalLookupResponse = getPaypalCheckoutManager().doPaypalLookup(order, profile,
						isPayPalCheckout());
				if (!paypalLookupResponse.isSuccess()) {
					String msg = "";
					boolean foundMsg = false;
					if (paypalLookupResponse.getErrorNo() != null && 
							DigitalStringUtil.isNotBlank(getPaypalReasonCodeMapping().get(paypalLookupResponse.getReasonCode()))) {
						
						String key = getPaypalReasonCodeMapping().get(paypalLookupResponse.getReasonCode());
						try{
							msg = getMessageLocator().getMessageString(key);
							if(DigitalStringUtil.isNotBlank(msg)){
								foundMsg = true;
								logError(msg);
								addFormException(new DropletException(msg, key));
							}else{
								logError(":: Missing error mapping in ServiceResource.properties for paypal reasonCode = " + key);
							}
						}catch(java.util.MissingResourceException exp){
							logError(":: Missing error mapping in resource for paypal reasonCode = " + key);
							logError(exp);
						}
					} 
					
					if (!foundMsg && PaypalConstants.PAYPAL_PAYMENT_UNAVAILABLE
							.equalsIgnoreCase(paypalLookupResponse.getStatusCode())) {
						msg = getMessageLocator().getMessageString(PAYPAL_UNAVAILABLE_ERROR);
						this.logError(msg);
						addFormException(new DropletException(msg, PAYPAL_UNAVAILABLE_ERROR));
					} else if(!foundMsg) {
						if (loggedInUser) {
							msg = getMessageLocator().getMessageString(PAYPAL_GENERAL_ERROR);
							this.logError(msg);
							addFormException(new DropletException(msg, PAYPAL_GENERAL_ERROR));
						} else {
							msg = getMessageLocator().getMessageString(PAYPAL_EXPRESS_CHECKOUT_ERROR);
							this.logError(msg);
							addFormException(new DropletException(msg, PAYPAL_EXPRESS_CHECKOUT_ERROR));
						}
					}
					ret = false;
				} else {
					this.setCardinalOrderId(paypalLookupResponse.getCardinalOrderId());
					this.setAcsURL(paypalLookupResponse.getAcsUrl());
					this.setPpPayload(paypalLookupResponse.getPayload());
					ret = true;
				}
			} catch (DigitalRuntimeException rex) {
				this.logError("Exception occured while handling ", rex);
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				ret = false;
			} catch (Exception e) {
				this.logError("Exception occured while handling ", e);
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				processException(e, "errorUpdatingOrder", request, response);
				String msg = getMessageLocator().getMessageString(PAYPAL_GENERAL_ERROR);
				addFormException(new DropletException(msg, PAYPAL_GENERAL_ERROR));
				ret = false;
			} finally {
				// Mark the transaction rollback if there are any form
				// validation errors
				if (this.getFormError()) {
					try {
						this.setTransactionToRollbackOnly();
					} catch (SystemException e) {
						logError("Transaction rollback exception :: ", e);
					}
				}
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				if(rrm != null){
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
		return ret;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             if an error
	 * @throws IOException
	 *             if an error
	 */
	protected void checkoutValidation(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws ServletException, IOException {
		try {
			if (this.getOrder() != null) {
				double orderTotal = ((DigitalOrderTools) this.getOrderManager().getOrderTools()).calculateOrderTotal(order);
				if (DigitalCommonUtil.equalsZero(orderTotal)) {
					String msg = getMessageLocator().getMessageString(PAYPAL_ZERO_AMOUNT_CHARGED_ERROR);
					addFormException(new DropletException(msg, PAYPAL_ZERO_AMOUNT_CHARGED_ERROR));
					throw new DigitalRuntimeException("PAYPAL_ZERO_AMOUNT_CHARGED_ERROR :: " + msg);
				}
			}
		} finally {
			// Mark the transaction rollback if there are any form
			// validation errors
			if (this.getFormError()) {
				try {
					this.setTransactionToRollbackOnly();
				} catch (SystemException e) {
					logError("Transaction rollback exception :: ", e);
				}
			}
		}
	}

	/**
	 * Utility method to check max cart limit
	 * 
	 */
	private void checkOutMaxCartValidation() {

		if (MultiSiteUtil.isCartPurchaseLimitExceeded(order, MultiSiteUtil.getCartPurchaseLimit())) {
			NumberFormat nf = NumberFormat.getCurrencyInstance();
			Object[] params = { nf.format(MultiSiteUtil.getCartPurchaseLimit()) };
			String msg = getMessageLocator().getMessageString(EXCEEDED_PURCHASE_LIMIT, params);
			addFormException(new DropletException(msg, EXCEEDED_PURCHASE_LIMIT));
		} else if (MultiSiteUtil.isCartLineLimitExceededForCheckout(order, MultiSiteUtil.getCartLineLimit())) {
			Object[] params = { "" + MultiSiteUtil.getCartLineLimit() };
			String msg = getMessageLocator().getMessageString(EXCEEDED_ITEM_IN_CART_LIMIT, params);
			addFormException(new DropletException(msg, EXCEEDED_ITEM_IN_CART_LIMIT));
		} else if (MultiSiteUtil.isCartSkuLimitExceeded(getOrder(), MultiSiteUtil.getCartSkuLimit())) {
			Object[] params = { "" + MultiSiteUtil.getCartSkuLimit() };
			String msg = getMessageLocator().getMessageString(EXCEEDED_SKU_LIMIT, params);
			addFormException(new DropletException(msg, EXCEEDED_SKU_LIMIT));
			throw new DigitalRuntimeException("EXCEEDED_SKU_LIMIT :: " + msg);
		}
	}

	/**
	 * 
	 * @param loginId
	 * @return RepositoryItem
	 */
	public RepositoryItem lookupUser(String loginId) {
		final String METHOD_NAME = "lookupUser";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			RepositoryItemDescriptor usersDesc = getProfile().getRepository().getItemDescriptor("user");
			RepositoryView userRepView = usersDesc.getRepositoryView();

			RqlStatement statement = RqlStatement.parseRqlStatement("login = ?0");
			Object params[] = new Object[1];
			params[0] = new String(loginId);

			RepositoryItem[] userItems = statement.executeQuery(userRepView, params);
			if (userItems == null)
				return null;/* no user with that login id found */

			return userItems[0];
		} catch (RepositoryException e) {
			if (isLoggingDebug())
				logDebug("An exception occurred while determining if the user is a migrated user");
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}

		return null;
	}

	private String paymantPageURL;
	private String changeShipCommerceId;
	boolean giftReceipt = false;
	String giftReceiptMessage;

	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @throws ServletException
	 *             if an error
	 * @throws IOException
	 *             if an error
	 */
	public void preAddItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String METHOD_NAME = "preAddItemToOrder";
		try {

			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			if (DigitalStringUtil.isBlank(getProductId()) || getCatalogRefIds() == null || getCatalogRefIds().length == 0) {
				String msg = getMessageLocator().getMessageString(INVALID_ADD_ITEM_TO_CART_INPUT);
				addFormException(new DropletException(msg, INVALID_ADD_ITEM_TO_CART_INPUT));
			} else {
				Order order = this.getShoppingCart().getCurrent();
				String[] skus = this.getCatalogRefIds();
				DigitalCommerceItem commerceItemToUpdate = ((DigitalCommerceItemManager) getCommerceItemManager())
						.findCommerceItemByCatalogRefId(order, skus[0]);
				long qtyToValidate = this.getQuantity();
				if (commerceItemToUpdate != null) {
					qtyToValidate += commerceItemToUpdate.getQuantity() - 1;
				}

				if (MultiSiteUtil.isCartPurchaseLimitExceeded(order, MultiSiteUtil.getCartPurchaseLimit())
						|| MultiSiteUtil.isCartLineLimitExceeded(order, MultiSiteUtil.getCartLineLimit())
						|| (commerceItemToUpdate != null && MultiSiteUtil
								.isItemSkuLimitExceeded(MultiSiteUtil.getCartSkuLimit(), qtyToValidate))) {
					if (MultiSiteUtil.isCartPurchaseLimitExceeded(order, MultiSiteUtil.getCartPurchaseLimit())) {
						NumberFormat nf = NumberFormat.getCurrencyInstance();
						Object[] params = { nf.format(MultiSiteUtil.getCartPurchaseLimit()) };
						String msg = getMessageLocator().getMessageString(EXCEEDED_PURCHASE_LIMIT, params);
						addFormException(new DropletException(msg, EXCEEDED_PURCHASE_LIMIT));
					} else if (MultiSiteUtil.isCartLineLimitExceeded(order, MultiSiteUtil.getCartLineLimit())) {
						Object[] params = { "" + MultiSiteUtil.getCartLineLimit() };
						String msg = getMessageLocator().getMessageString(EXCEEDED_ITEM_IN_CART_LIMIT, params);
						addFormException(new DropletException(msg, EXCEEDED_ITEM_IN_CART_LIMIT));
					} else if (MultiSiteUtil.isItemSkuLimitExceeded(MultiSiteUtil.getCartSkuLimit(), qtyToValidate)) {
						Object[] params = { "" + MultiSiteUtil.getCartSkuLimit() };
						String msg = getMessageLocator().getMessageString(EXCEEDED_SKU_LIMIT, params);
						addFormException(new DropletException(msg, EXCEEDED_SKU_LIMIT));
						throw new DigitalRuntimeException("EXCEEDED_SKU_LIMIT :: " + msg);
					}

				} else {

						checkProductEligibility();

					if (getShipType() != null && getStoreId() != null) {
						if (!Strings.isNullOrEmpty(getShipType()) && !Strings.isNullOrEmpty(getStoreId())) {
							if (!validateBOPISShipTypeQuantity()) {
								addFormException(new DropletException(
										getMessageLocator().getMessageString(ORDER_TOTAL_EXCEEDS_BOPIS_LIMIT),
										ORDER_TOTAL_EXCEEDS_BOPIS_LIMIT));
							}
							getValue().put("shipType", getShipType());
							getValue().put("storeId", getStoreId());
							//KTLO1-592 Adding locationId details
							getValue().put("locationId", getStoreLocationId());
							if (getFormExceptions().size() == 0) {
								setShippingGroup(findShippingGroup(order, getShipType(), getStoreId()));
							}
						}
					}

					((OrderImpl) order).invalidateOrder();
				}
			}
		} finally {
			// Mark the transaction rollback if there are any form
			// validation errors
			if (this.getFormError()) {
				try {
					this.setTransactionToRollbackOnly();
				} catch (SystemException e) {
					logError("Transaction rollback exception :: ", e);
				}
			}
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/**
	 * 
	 * @return true or false
	 */
	private boolean validateBOPISShipTypeQuantity() {
		if (getQuantity() > DigitalBaseConstants.BOPIS_QUANTITY_LIMIT
				&& DigitalBaseConstants.SHIP_TYPE_BOPIS.equalsIgnoreCase(getShipType())) {
			return false;
		}
		return true;
	}


	/**
	 * Ignores {@link PricingException}s. Reprices the shipping only. It returns
	 * the total for all of the shipping, but the shipping group should be
	 * inspected to determine what the price really is. This should not affect
	 * tax, discounts, or order total.
	 * 
	 * @param order
	 * @param locale
	 * @return double - shipping rate
	 */
	protected double priceShipping(Order order, Locale locale) {
		double shipping = 0.0;
		try {
			synchronized (order) {
				shipping = getPricingTools().priceShippingForOrderTotal(order, userPricingModel, locale, getProfile(), null);
			}
		} catch (PricingException pex) {
			if (isLoggingDebug())
				logDebug("Error occured while processing -------->>>", pex);
		}
		return shipping;
	}

	/**
	 * 
	 * @return the shippingOptionsTools
	 */
	public ShippingOptionsTools getShippingOptionsTools() {
		return shippingOptionsTools;
	}

	/**
	 * 
	 * @param shippingOptionsTools
	 *            the shippingOptionsTools to set
	 */
	public void setShippingOptionsTools(ShippingOptionsTools shippingOptionsTools) {
		this.shippingOptionsTools = shippingOptionsTools;
	}

	@Override
	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return long
	 * @throws ServletException
	 *             if an error
	 * @throws IOException
	 *             if an error
	 */
	public long getQuantity(String pKey, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, NumberFormatException {
		String[] values = pRequest.getParameterValues(pKey);
		if (values == null && pRequest.getAttribute(pKey) != null) {
			values = new String[1];
			values[0] = (String) pRequest.getAttribute(pKey);
		}
		if ((values != null) && (values.length > 0)) {
			long totalQuantity = 0L;
			for (String value : values) {
				totalQuantity += Long.parseLong(value);
			}
			return totalQuantity;
		}
		return getQuantity();
	}

	@Override
	/**
	 * 
	 * @param pOrder
	 * @param pPricingModels
	 * @param pLocale
	 * @param pProfile
	 * @param pExtraParameters
	 */
	protected void runProcessMoveToPurchaseInfo(Order pOrder, PricingModelHolder pPricingModels, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters) throws RunProcessException {
		Map<String, Boolean> map = new HashMap<>(5);
		map.put("ValidateCommerceItemInOrder", Boolean.TRUE);
		map.put("ValidateShippingGroupInOrder", Boolean.FALSE);
		map.put("ValidatePaymentGroupInOrder", Boolean.FALSE);
		map.put("yantraCall", Boolean.TRUE);
		map.put("storeNetCall", Boolean.TRUE);

		PipelineResult result = runProcess(getMoveToPurchaseInfoChainId(), pOrder, pPricingModels, pLocale, pProfile,
				map, pExtraParameters);

		processPipelineErrors(result);
	}
	
	/** @param pRequest
	 * @param pResponse
	 * @throws ServletException
	 *             if an error
	 * @throws IOException
	 *             if an error
	 */
	public void preMoveToPurchaseInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		try {
			checkOutMaxCartValidation();

			if ( getGwpManager().checkShippingRestriction(order)) {
				if (isLoggingDebug()) {
					logDebug("DigitalCartModifierFormHandler.preAddItemToOrder() ---- >> enfore shipping restriction");
				}
				addFormException(new DropletException(getMessageLocator().getMessageString("gwpShippingRestriction"), "gwpShippingRestriction"));
			}
			if (!getDswConstants().isPaTool() && getGwpManager().verifyShipRestriction(order)) {
				String msg = getMessageLocator().getMessageString(SHIP_RESTRICTION);
				addFormException(new DropletException(msg, SHIP_RESTRICTION));
			}
		} finally {
			// Mark the transaction rollback if there are any form
			// validation errors
			if (this.getFormError()) {
				try {
					this.setTransactionToRollbackOnly();
				} catch (SystemException e) {
					logError("Transaction rollback exception :: ", e);
				}
			}
		}
	}

	/**
	 * @param pRequest
	 * @param pResponse
	 * @throws ServletException if an error
	 * @throws IOException      if an error
	 */
	public void postRemoveItemFromOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		try {
			List<String> toRemove = new ArrayList<>();
			Order order = this.getOrder();

			List<?> sgs = order.getShippingGroups();
			for (Object objSG : sgs) {
				if (objSG instanceof HardgoodShippingGroup) {
					ShippingGroup sg = (ShippingGroup) objSG;
					if (sg.getCommerceItemRelationshipCount() == 0 && (ShippingMethod.BOPIS.getMappedValue()
							.equalsIgnoreCase(sg.getShippingMethod())
							|| ShippingMethod.BOSTS.getMappedValue()
							.equalsIgnoreCase(sg.getShippingMethod()))) {
						toRemove.add(sg.getId());
					}
				}
			}

			for (String sgId : toRemove) {
				try {
					order.removeShippingGroup(sgId);
				} catch (ShippingGroupNotFoundException e) {
					this.logError("No ShippingGroup Found to be removed ", e);
					String msg = getMessageLocator()
							.getMessageString(COMMERCE_ITEM_SHIPPING_GROUP_NOT_FOUND);
					addFormException(new DropletException(msg, COMMERCE_ITEM_SHIPPING_GROUP_NOT_FOUND));
				} catch (atg.commerce.order.InvalidParameterException e) {
					this.logError("Unable to update ShippingGroup ", e);
					Object[] params = {"" + sgId};
					String msg = getMessageLocator().getMessageString(SHIPPING_GROUP_REMOVAL_ERROR, params);
					addFormException(new DropletException(msg, SHIPPING_GROUP_REMOVAL_ERROR));
				}
				// If there are form exceptions then return so that loop is
				// ended.
				if (this.getFormError()) {
					return;
				}

				validateBopisBostsForOrderItems(order);
			}
		} finally {
			// Mark the transaction rollback if there are any form
			// validation errors
			if (this.getFormError()) {
				try {
					this.setTransactionToRollbackOnly();
				} catch (SystemException e) {
					logError("Transaction rollback exception :: ", e);
				}
			}
		}
	}

	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @throws ServletException
	 *             if an error
	 * @throws IOException
	 *             if an error
	 */
	public void postMoveToPurchaseInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		Order order = this.getShoppingCart().getCurrent();
		DigitalPaymentGroupManager pmg = (DigitalPaymentGroupManager) getPaymentGroupManager();
		List<DigitalGiftCard> giftcards = pmg.findGiftCards(order);
		for (DigitalGiftCard giftcard : giftcards) {
			reapplyGiftcard(giftcard.getCardNumber(), giftcard.getPinNumber(), giftcard.getCurrencyCode());
		}
		super.postMoveToPurchaseInfo(pRequest, pResponse);
	}

	/**
	 * 
	 * @param cardNumber
	 * @param pin
	 * @param currencyName
	 */
	private void reapplyGiftcard(String cardNumber, String pin, String currencyName) {
		DigitalGiftCardApproval approval = new DigitalGiftCardApproval();
		final UserTransactionDemarcation td = TransactionUtils.startNewTransaction(CLASSNAME, "reapplyGiftcard");
		try {
			// Profile lock acquire & release is already
			// handled in beforeSet() and afterSet() methods
			// of parent ATG PurchaseProcessFormHandler
			// form handlers
			synchronized (getOrder()) {
				((DigitalPaymentGroupManager) getPaymentGroupManager()).removeGiftCardFromOrder(getOrder(), cardNumber, false);
				((DigitalPaymentGroupManager) getPaymentGroupManager()).recalculatePaymentGroupAmounts(getOrder());
				getOrderManager().updateOrder(getOrder());
				try {
					approval = getPaymentServices().getGiftCardBalance(cardNumber, pin, currencyName);
				} catch (DigitalIntegrationException e) {
					logWarning("Error while retrieving SVS balance inquiry", e);
					warningMessages.add("IGNORABLESVSBE-" + e.getMessage());
				}
				if (DigitalStringUtil.isNotBlank(approval.getResponseCode())
						&& GIFT_CARD_APPROVAL_RESPONSE_CODE == Integer.parseInt(approval.getResponseCode())) {
					DigitalPaymentGroupManager pmg = (DigitalPaymentGroupManager) getPaymentGroupManager();
					DigitalGiftCard giftCardPaymentGroup = pmg.addGiftCardPaymentGroup(getOrder(), cardNumber, pin, approval);
					if (null != giftCardPaymentGroup) {
						getPaymentGroupManager().recalculatePaymentGroupAmounts(getOrder());
						// update first/last name of shipping group for
						// bopis/bosts
						// only orders from alternate pick-up
						this.getShippingGroupManager().updateShipAddrNameForBopisBostsOrder(getOrder());

						getOrderManager().updateOrder(getOrder());
					}
				} else {
					warningMessages.add("IGNORABLESVSBE-"
							+ getMessageLocator().getMessageString("SVS" + approval.getResponseCode()));
				}
			}
		} catch (DigitalAppException e) {
			logWarning(e.getMessage());
			String msg = getMessageLocator().getMessageString(e.getMessage());
			if (!DigitalStringUtil.contains(e.getMessage(), "giftcard.nothing.toapply")) {
				if (!DigitalStringUtil.isBlank(msg)) {
					warningMessages.add("IGNORABLESVSBE-" + msg);
				} else {
					warningMessages.add("IGNORABLESVSBE-" + msg);
				}
			}
		} catch (RepositoryException | CommerceException e) {
			logWarning(e.getMessage());
			String msg = getMessageLocator().getMessageString("errorAddingGiftCard");
			warningMessages.add("IGNORABLESVSBE-" + msg);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, "reapplyGiftcard");
			TransactionUtils.endTransaction(td, CLASSNAME, "reapplyGiftcard");
		}
	}

	/**
	 * Override the method to stop commit & post commit steps if there is any
	 * form exceptions during validation. Rollback the transaction.
	 * 
	 * @param pSuccessURL
	 * @param pFailureURL
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 */
	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if ((getSessionExpirationURL() != null) && (pRequest.getSession().isNew()) && (isFormSubmission(pRequest))) {
			RepositoryItem profile;

			if (((profile = getProfile()) == null) && ((profile = defaultUserProfile(true)) == null)
					&& (isLoggingError())) {
				logError("missingExpiredSessionProfile");
			}

			if ((profile != null) && (profile.isTransient())) {
				if (isLoggingDebug())
					logDebug("Session expired: redirecting to " + getSessionExpirationURL());
				redirectOrForward(pRequest, pResponse, getSessionExpirationURL());
				return false;
			}
		}

		if (isTransactionMarkedAsRollBack()) {
			if (isLoggingDebug()) {
				logDebug("Transaction Marked as Rollback - redirecting to: " + pFailureURL);
			}
			return false;
		}

		return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
	}
	

	/**
	 * Validate all Bopis/Bosts commerce items to check if the store/sku are killed
	 * @param order
	 */
	public void validateBopisBostsForOrderItems(Order order){
		List<DigitalCommerceItem> items = order.getCommerceItems();
		Vector formExceptions = this.getFormExceptions();
		if(null!=items && items.size()>0){
			for (DigitalCommerceItem item : items) {
				if(null!=item){
					String shipType=(String)((DigitalCommerceItem)item).getPropertyValue("shipType");
					if(null!=shipType && (DigitalBaseConstants.SHIP_TYPE_BOPIS.equals(shipType)||DigitalBaseConstants.SHIP_TYPE_BOSTS.equals(shipType))){
						checkProductEligibility(item);
						updateCommerceItemWithDswExtensions(item);
						item.setStorePickAvailable(isSkuEligibleForStorePickup && isStoreEligibleForBOPIS);
						item.setShipToStoreAvailable(isSkuEligibleForShipToStore && isStoreEligibleForBOSTS);
						synchronized (getOrder()) {
							try {
								this.getOrderManager().updateOrder(getOrder());
							} catch (CommerceException e) {
								logError("Failed to update BOPIS/BOSTS item to bag", e);
							}
						}
					}
				}
			}
		}
		this.getFormExceptions().clear();
		this.getFormExceptions().addAll(formExceptions);
	}
	

}
