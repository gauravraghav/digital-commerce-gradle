package com.digital.commerce.services.order.processor;

import java.util.HashMap;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.services.inventory.DigitalInventoryManager;
import com.digital.commerce.services.order.DigitalOrderImpl;

import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"unchecked","rawtypes"})
@Getter
@Setter
public class DigitalProcInventoryReservation implements PipelineProcessor{ 
	
	private DigitalInventoryManager inventoryManager;
	
	private MessageLocator messageLocator;
	
	private boolean autoSyncInventory = false;
		
	private final static String ERROR_WITH_SERVICE_CALL="error.with.reservation.service.all";

	public int runProcess(Object paramObject, PipelineResult paramPipelineResult)
		    throws Exception{
		HashMap map = (HashMap)paramObject;
		DigitalOrderImpl order = (DigitalOrderImpl)map.get("Order");
	    Boolean yantraCall = (Boolean)map.get("yantraCall");
	    Boolean storeNetCall = (Boolean)map.get("storeNetCall");
	    if(yantraCall==null)yantraCall=false;
	    if(storeNetCall==null)storeNetCall=false;
	    //empty process result
	    order.setInventoryProcessResult(null);
	    
	    DigitalInventoryManager.ReservationStatus status = this.getInventoryManager().orderReservation(order, yantraCall, storeNetCall);
	    /*
	    if(status == DSWInventoryManager.ReservationStatus.EVALUATED){
	    	return DSWInventoryManager.ReservationStatus.EVALUATED.getValue();
	    }
	    if(status == DSWInventoryManager.ReservationStatus.SUCCESS){
	    	return DSWInventoryManager.ReservationStatus.EVALUATED.getValue();
	    }
	    */
	    if(status == DigitalInventoryManager.ReservationStatus.ERROR){
	    	paramPipelineResult.addError(ERROR_WITH_SERVICE_CALL, this.getMessageLocator().getMessageString(ERROR_WITH_SERVICE_CALL));
	    	return 0;
	    }else{
	    	if(autoSyncInventory){
	    		return DigitalInventoryManager.ReservationStatus.EVALUATED.getValue();
	    	} else if (status == DigitalInventoryManager.ReservationStatus.EVALUATED || status == DigitalInventoryManager.ReservationStatus.SUCCESS) {
	    		//need customer invention , no  more auto drop and sync
	    		map.put("sync", false);
	    		//map.put("sync", true);
	    		return DigitalInventoryManager.ReservationStatus.EVALUATED.getValue();
	    	} else {
	    		//paramPipelineResult.addError(INVENTORY_UNMATCHED, this.getMessageLocator().getMessageString(INVENTORY_UNMATCHED));
	    		map.put("sync", false);
	    		return DigitalInventoryManager.ReservationStatus.EVALUATED.getValue();
	    	}
	    }
	}
		  
    public int[] getRetCodes(){
      return new int[]{DigitalInventoryManager.ReservationStatus.EVALUATED.getValue()};
    }

}
