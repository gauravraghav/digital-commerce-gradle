package com.digital.commerce.services.order;

import java.util.HashMap;
import java.util.Map;

import atg.userprofiling.Profile;

import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.ComponentLookupUtil;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("rawtypes")
@Getter
@Setter
public class DigitalRewardsPointsClosenessQualifier {

	private DigitalOrderTools orderTools;

	private DigitalOrderImpl order;
	
	private DigitalOrderSummary dswosum;
	
	public int getThresholdAmount() {
		int threshold = 0;
		threshold = MultiSiteUtil.getRewardsThresholdAmount();
		return threshold;
	}

	public double getQualifyingMerchTotal(DigitalOrderImpl orderPassed) {
		
		double orderTotal = 0;
		orderTotal = dswosum.getOrderTotal(orderPassed);
		return orderTotal;
	}

	public long getDollarsFutureRewards(DigitalOrderImpl orderPassed) {		
		long dollarsFutureRewards = 0;
		Profile profile = getProfile();
		if (null != profile && profile.getPropertyValue("dollarsFutureReward") != null) {
			dollarsFutureRewards = (Long) profile.getPropertyValue("dollarsFutureReward");
		} 
		return dollarsFutureRewards;
	}

	public Map upsell(DigitalOrderImpl orderPassed) {
		
		Map<String, Object> rtn = new HashMap<>();
		
		boolean upsell = false;
		
		double orderTotal = getQualifyingMerchTotal(orderPassed);
		long dollarsFutureRewards = getDollarsFutureRewards(orderPassed);
		double dollarsRemainingFromNextRewards = dollarsFutureRewards - orderTotal;
		
		if ((dollarsRemainingFromNextRewards <= 0) || dollarsRemainingFromNextRewards >= getThresholdAmount()) {
			upsell = false;
			rtn.put("showUpsell",upsell );
		} else {
			upsell = true;
			rtn.put("orderTotal", orderTotal);
			rtn.put("dollarsFutureRewards", dollarsFutureRewards);
			rtn.put("closenessPointQualifyingDollars", dollarsRemainingFromNextRewards);
			rtn.put("showUpsell", upsell);
		}
			return rtn;
	}
	
	private Profile getProfile() {
		return ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PROFILE, Profile.class);
	}
	
}