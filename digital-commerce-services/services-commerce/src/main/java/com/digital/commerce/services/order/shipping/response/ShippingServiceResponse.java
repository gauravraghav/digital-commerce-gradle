package com.digital.commerce.services.order.shipping.response;

import com.digital.commerce.services.common.DigitalContactInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShippingServiceResponse {
    private String result="success";
    private String  decison;
	private DigitalContactInfo suggestedAddress;
    private DigitalContactInfo address;
}
