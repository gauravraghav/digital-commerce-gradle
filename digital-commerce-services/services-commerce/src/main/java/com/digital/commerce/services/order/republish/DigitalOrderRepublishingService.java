package com.digital.commerce.services.order.republish;
import atg.nucleus.logging.ApplicationLoggingImpl;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.integration.oms.OrderManagementService;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalOrderRepublishingService extends ApplicationLoggingImpl {
	
	private OrderManagementService	orderManagementService	= null;
	
	private static final String SERVICE_NAME = "OrderSubmitService";
	private static final String OPERATION_NAME = "submitOrderAsXML";
	private static final String	ERR_SUBMIT_ORDER_ASXML	= "errorSubmitOrderAsXML";
	
	private MessageLocator messageLocator;


	public DigitalOrderRepublishingService() {
		super(DigitalOrderRepublishingService.class.getName());
	}
	
	public void submitOrderAsXML( String orderAsXML ) throws DigitalAppException {
		final long startTime = System.currentTimeMillis();
		try {
			getOrderManagementService().submitOrder( orderAsXML );
		} catch( Exception e ) {
			//postCommitOrderManager.markIncomplete( order );
			/*if( getShoppingCart() != null ) {
				this.setOrder( null );
			}*/
			String msg = getMessageLocator().getMessageString( ERR_SUBMIT_ORDER_ASXML );
			throw new DigitalAppException(msg, e);
		} finally {
			final long endTime = System.currentTimeMillis();
			final long executionTime = endTime - startTime;
			final StringBuilder sb = new StringBuilder();
			sb.append(SERVICE_NAME).append(" : ");
			sb.append(OPERATION_NAME).append(" : ");
			sb.append(executionTime).append("ms");
			logInfo(sb.toString());
		}
	}

}
