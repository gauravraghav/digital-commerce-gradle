package com.digital.commerce.services.pricing.reprice;

/*  */
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.digital.commerce.common.util.DigitalStringUtil;

public class OrderRepositoryIdHandler extends DefaultHandler {

	private final String	ORDER			= "order";
	private final String	REPOSITORYID	= "repositoryId";
	private String			order			= "";
	private String			orderId			= "";
	private String			errMsg			= null;

	public String getErrorMessage() {
		return errMsg;
	}

	public String getOrderRepositoryId() {
		return orderId;
	}

	/** Receive notification of the start of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @param atts - The attributes attached to the element. If there are no attributes, it shall be an empty Attributes object.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void startElement( String namespaceURI, String localName, String qName, Attributes atts ) {

		try {

			if( ORDER.equals( localName ) ) {
				order = ORDER;
			}

			if( DigitalStringUtil.isNotBlank(order) ) {
				if( atts.getValue( REPOSITORYID ) != null && DigitalStringUtil.isBlank(orderId) ) orderId = atts.getValue( REPOSITORYID );

			}

		} catch( Exception e ) {
			errMsg = e.getMessage();
		}

	}

	/** Receive notification of character data inside an element.
	 * 
	 * @param ch - The characters.
	 * @param start - The start position in the character array.
	 * @param length - The number of characters to use from the character array.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void characters( char[] ch, int start, int length ) throws SAXException {

	}

	/** Receive notification of the end of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void endElement( String namespaceURI, String localName, String qName ) throws SAXException {

	}

}
