package com.digital.commerce.services.order;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalOfferDetailsBean {

  private String description;
  private String offerCode;
  private double savingsValue = 0d;
}
