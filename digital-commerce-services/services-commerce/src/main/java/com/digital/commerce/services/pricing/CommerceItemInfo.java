/**
 * 
 */
package com.digital.commerce.services.pricing;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@Getter
@Setter
public class CommerceItemInfo {

	private String id;
	private String productTitle;
	private long quantity;
	private String brand;
	private String colorCode;
	private String productId;
	private int seqNo;


	@Override
	public boolean equals(Object ci) {
		if (!(ci instanceof CommerceItemInfo))
            return false;
        if (ci == this)
            return true;

        CommerceItemInfo rhs = (CommerceItemInfo) ci;
        return new EqualsBuilder().
            append(id, rhs.id).append(seqNo, rhs.seqNo).isEquals();
	}

	@Override
	public int hashCode() {
		  return new HashCodeBuilder(17, 31).
		            append(id).
		            append(seqNo).toHashCode();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
				sb.append("id: ").append(id).append(",")
				  .append("quantity: ").append(quantity).append(",")
				  .append("productTitle: ").append(productTitle).append(",")
						.append("brand: ").append(brand).append(",")
						.append("colorCode: ").append(colorCode).append(",")
						.append("productId: ").append(productId);
		return sb.toString();
	}
	
}
