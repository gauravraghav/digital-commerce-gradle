package com.digital.commerce.services.order;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

import java.io.Serializable;
@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class DigitalCommerceItemAttributes implements Serializable {
	private static final long serialVersionUID = -6316569280013096169L;
	private double msrp = 0;
	private String size = "";
	private double quantity =0;
	private String upc = "";
	private String color = "";
	private double price = 0;
	private double discountedShare = 0;
	private ArrayList styles;
	private String storeId;
	private String productID = "";
	private String productDescription = "";
}
