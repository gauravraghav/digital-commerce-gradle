package com.digital.commerce.services.pricing.calculators;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.digital.commerce.services.order.DigitalCommerceItem;

import atg.commerce.pricing.BandedDiscountCalculatorHelper;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.definition.DiscountStructure;
import atg.repository.RepositoryItem;

/**
 * 
 *
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalBandedDiscountCalculatorHelper extends
		BandedDiscountCalculatorHelper {

	public void BandedDiscountCalculatorHelper() {
		setLoggingIdentifier(getClass().getSimpleName());
	}

	/**
	 * retrieves the adjuster amount based on the band range of qualified items total amount. 
	 * @param pPricingModel
	 * @param pQualifiedItems list of qualified commerce items on which the adjuster value has to be calculated. 
	 * @param pExtraParameters
	 * @param pDefaultBandingProperty
	 * @param pDefaultBandingPropertyScope
	 * @return adjuster value
	 * @throws PricingException
	 */
	public double getAdjuster(RepositoryItem pPricingModel,
			List pQualifiedItems, Map pExtraParameters,
			String pDefaultBandingProperty, String pDefaultBandingPropertyScope)
			throws PricingException {
		double adjuster = 0.0D;
		DiscountStructure discountStructure = null;
		Double adjusterObj = null;
		Band band = null;
		List bands = null;
		Map objectBindings = null;
		double bandingValue = 0.0D;
		Map attributes = null;
		String bandingProperty = null;
		String bandingPropertyScope = null;
		if (isLoggingDebug()) {
			logDebug("Entered getAdjuster");
			logDebug((new StringBuilder()).append("pPricingModel: ")
					.append(pPricingModel).toString());
			if (pQualifiedItems != null) {
				logDebug((new StringBuilder()).append("pQualifiedItems: ")
						.append(pQualifiedItems).toString());
				Object item;
				for (Iterator i$ = pQualifiedItems.iterator(); i$.hasNext(); logDebug(item
						.toString()))
					item = i$.next();

			}
			logDebug((new StringBuilder()).append("pExtraParameters: ")
					.append(pExtraParameters).toString());
			logDebug((new StringBuilder()).append("pDefaultBandingProperty: ")
					.append(pDefaultBandingProperty).toString());
			logDebug((new StringBuilder())
					.append("pDefaultBandingPropertyScope: ")
					.append(pDefaultBandingPropertyScope).toString());
		}
		if (pExtraParameters == null)
			throw new PricingException(
					Constants.EXTRA_PARAMS_NEEDED_FOR_DISCOUNT);
		discountStructure = (DiscountStructure) pExtraParameters
				.get("discountStructure");
		if (discountStructure == null)
			throw new PricingException(Constants.EXTRA_PARAMS_NEEDS_DISCOUNT);
		adjusterObj = discountStructure.getAdjuster();
		if (adjusterObj != null) {
			adjuster = adjusterObj.doubleValue();
		} else {
			bands = determineBands(discountStructure.getDiscountDetails());
			attributes = discountStructure.getAttributes();
			if (attributes != null) {
				bandingProperty = (String) attributes.get("bandingProperty");
				bandingPropertyScope = (String) attributes
						.get("bandingPropertyScope");
			}
			if (bandingProperty == null)
				bandingProperty = pDefaultBandingProperty;
			objectBindings = createBindings(bandingProperty, pExtraParameters);
			if (bandingPropertyScope == null)
				bandingPropertyScope = pDefaultBandingPropertyScope;
			bandingValue = getBandingValue(bandingPropertyScope, objectBindings,
					pQualifiedItems);
			band = determineBand(bandingValue, bands);
			if (band != null)
				adjuster = band.mAdjuster;
			else
				adjuster = (0.0D / 0.0D);
			discountStructure.setAdjuster(Double.valueOf(adjuster));
		}
		if (isLoggingDebug())
			logDebug((new StringBuilder()).append("Leaving with adjuster: ")
					.append(adjuster).toString());
		return adjuster;
	}

	/**
	 * Calculated the binding value based on the promo qualified Items.
	 * @param pBandingPropertyScope
	 * @param pObjectBindings
	 * @param pQualifiedItems
	 * @return binding value :  total of qualified items price amount
	 * @throws PricingException
	 */
	private double getBandingValue(String pBandingPropertyScope,
			Map pObjectBindings, List pQualifiedItems) throws PricingException {
		double bandingValue = 0.0D;
		if (isLoggingDebug()) {
			logDebug("Entered calculateBandingValue");
			logDebug((new StringBuilder()).append("pBandingPropertyScope: ")
					.append(pBandingPropertyScope).toString());
			logDebug((new StringBuilder()).append("pObjectBindings: ")
					.append(pObjectBindings).toString());
			if (pQualifiedItems != null) {
				logDebug("pQualifiedItems:");
				Object obj;
				for (Iterator i$ = pQualifiedItems.iterator(); i$.hasNext(); logDebug(obj
						.toString()))
					obj = i$.next();

			}
		}
		if (pQualifiedItems != null) {
			Iterator i$ = pQualifiedItems.iterator();
			do {
				if (!i$.hasNext())
					break;
				DigitalCommerceItem obj = (DigitalCommerceItem) i$.next();

				ItemPriceInfo amountInfo = obj.getPriceInfo();
				// bandingPropertyValue =
				// getBandingPropertyValue(pObjectBindings);
				bandingValue += amountInfo.getAmount();

			} while (true);
		}

		if (isLoggingDebug())
			logDebug((new StringBuilder())
					.append("Leaving with bandingValue: ").append(bandingValue)
					.toString());
		return bandingValue;
	}

}
