package com.digital.commerce.services.order.payment.creditcard;

import java.util.List;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.payment.PaymentAuthorizationService;
import com.digital.commerce.integration.payment.vantiv.VantivPaymentService;
import com.digital.commerce.integration.tax.vertex.VertexTaxService;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalCreditCard;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.utils.DigitalServiceConstants;

import atg.commerce.order.Order;
import atg.nucleus.GenericService;
import atg.payment.creditcard.CreditCardInfo;
import atg.payment.creditcard.CreditCardProcessor;
import atg.payment.creditcard.CreditCardStatus;
import atg.payment.creditcard.CreditCardStatusImpl;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class DigitalCreditCardPaymentService extends GenericService implements CreditCardProcessor {
	
	private boolean					contactCenter;
	private CreditCardServices		creditCardServices;
	private DigitalServiceConstants 	dswServiceConstants;
	private VantivPaymentService 	vantivPaymentService;	
	private VertexTaxService 		vertexTaxService;
	private DigitalProfileTools 		dswProfileTools;
	private static final String 	SERVICE_NAME = "DigitalCreditCardPaymentService";	

	public DigitalCreditCardPaymentService() {}


	/** Authorize the amount on the credit card
	 * 
	 * @param pCreditCardInfo
	 *            the CreditCardInfo reference which contains all the
	 *            authorization data
	 * @return a CreditCardStatus object detailing the results of the
	 *         authorization */
	public CreditCardStatus authorize( CreditCardInfo pCreditCardInfo ) {
		final String METHOD_NAME = "authorize";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
		if( isLoggingDebug() ) {
			logDebug( "authorize(CreditCardInfo pCreditCardInfo=" + pCreditCardInfo + ") - start" );
			logDebug( "Authorizing credit card " + pCreditCardInfo.getCreditCardType() + " for " + pCreditCardInfo.getAmount() );
			
			logDebug( "authorize:: getVertexTaxService().isServiceEnabled() " + getVertexTaxService().isServiceEnabled());
			logDebug( "authorize:: (DigitalOrderImpl)pCreditCardInfo.getOrder()).isTaxOffline() :: " + ((DigitalOrderImpl)pCreditCardInfo.getOrder()).isTaxOffline());
			logDebug( "authorize:: getVantivPaymentService().isServiceMethodEnabled(PaymentAuthorizationService.ServiceMethod.PREAUTH.getServiceMethodName()) :: " 
					+ getVantivPaymentService().isServiceMethodEnabled(PaymentAuthorizationService.ServiceMethod.PREAUTH.getServiceMethodName()));
			logDebug( "authorize:: getVantivPaymentService().isServiceEnabled() :: " + getVantivPaymentService().isServiceEnabled());
			logDebug( "authorize:: orderHasTestItem( pCreditCardInfo.getOrder() ) :: " + orderHasTestItem( pCreditCardInfo.getOrder() ));
		}
		// Determine which credit card services to call
		DigitalCreditCardStatus ccStatus = new DigitalCreditCardStatus();
		DigitalCreditCardServiceStatus ccServiceStatus = new DigitalCreditCardServiceStatus();
		DigitalCreditCardInfo ccInfo = (DigitalCreditCardInfo)pCreditCardInfo;
		try {
			if( isLoggingDebug() ) {
				logDebug( "authorize() - Calling authorization service..." ); 
			}
			
			
			if(getVertexTaxService().isServiceEnabled() && !((DigitalOrderImpl)pCreditCardInfo.getOrder()).isTaxOffline()
					&& getVantivPaymentService().isServiceEnabled() 
						&& getVantivPaymentService().isServiceMethodEnabled(PaymentAuthorizationService.ServiceMethod.PREAUTH.getServiceMethodName())
							&& !orderHasTestItem( pCreditCardInfo.getOrder() )){
				
				if( isLoggingDebug() ) {
					logDebug( "authorize() - Calling preAuthorization  :: STARTED processing ONLINE order" ); 
				}

				ccServiceStatus = getCreditCardServices().preAuthorization( DigitalCreditCardServiceStatus.AUTHORIZE, (DigitalCreditCardInfo)pCreditCardInfo, (int)pCreditCardInfo.getOrder().getTotalCommerceItemCount() );
				ccStatus.setAuthorizationCode( ccServiceStatus.getAuthorizationCode() );
				ccStatus.setAmount( ccServiceStatus.getAmountAuthorized() );
				ccStatus.setAvsCode( ccServiceStatus.getAvsCode() );
				ccStatus.setTransactionId( ccServiceStatus.getTransactionId() );
				ccStatus.setAuthorizationExpiration( ccServiceStatus.getAuthorizationExpiration());
				if(DigitalStringUtil.isNotEmpty(ccServiceStatus.getTokenValue())){
					ccStatus.setTokenValue(ccServiceStatus.getTokenValue());
				}else{
					ccStatus.setTokenValue(ccInfo.getPaypageRegistrationId());
				}
				ccStatus.setFraudFlag( ccServiceStatus.getFraudFlag() );
				ccStatus.setFraudResultCode( ccServiceStatus.getFraudResultCode() );
				ccStatus.setFraudResultCodeDescription( ccServiceStatus.getFraudResultCodeDescription() );
				ccStatus.setFraudRule( ccServiceStatus.getFraudRule() );
				ccStatus.setFraudRuleDescription( ccServiceStatus.getFraudRuleDescription() );
				ccStatus.setFraudScore( ccServiceStatus.getFraudScore() );
				ccStatus.setThreatMetrixId(ccServiceStatus.getThreatMetrixId());
				if(ccStatus.getThreatMetrixId()==null){
					//This is a scenario when we dont get threatmetrix id 
					ccStatus.setThreatMetrixId(getDswServiceConstants().getFiveDigitThreatMetrixCode()+"-"+ pCreditCardInfo.getOrder().getId());
				}

			}else{
				logDebug( "Setting DATA for OFFLINE Scenario ");
				//setting flags for offline scenario
				ccServiceStatus.setStatusCode( DigitalCreditCardServiceStatus.SUCCESS );
				ccServiceStatus.setAmountAuthorized( pCreditCardInfo.getAmount() );
				ccServiceStatus.setAvsCode(DigitalCreditCardServiceStatus.SUCCESS );
				ccServiceStatus.setCvCode(DigitalCreditCardServiceStatus.CVCODE );
				ccServiceStatus.setDecision( DigitalCreditCardServiceStatus.DECISION_OFFLINE );
				ccServiceStatus.setFraudFlag( DigitalCreditCardServiceStatus.FRAUD_FLAG_YES);
			}
			//Setting token or paypage id and cardtype for both offline and online scenarios no matter what happens
			setCreditValues(pCreditCardInfo, ccServiceStatus, ccInfo);
			// Process the AuthFraud response for Default FP04
			ccStatus = processAuthFraudResponse( ccStatus, ccServiceStatus,pCreditCardInfo.getOrder().getId(),ccInfo.getTokenValue());
		}catch( DigitalIntegrationException e ) {
			//Setting token or paypage id and cardtype for exception scenario in case of exception
			setCreditValues(pCreditCardInfo, ccServiceStatus, ccInfo);
			//setting flags for offline scenario
			ccServiceStatus.setStatusCode( DigitalCreditCardServiceStatus.SUCCESS );
			ccServiceStatus.setAmountAuthorized( pCreditCardInfo.getAmount() );
			ccServiceStatus.setAvsCode(DigitalCreditCardServiceStatus.SUCCESS );
			ccServiceStatus.setCvCode(DigitalCreditCardServiceStatus.CVCODE );
			ccServiceStatus.setDecision( DigitalCreditCardServiceStatus.DECISION_OFFLINE );
			ccServiceStatus.setFraudFlag( DigitalCreditCardServiceStatus.FRAUD_FLAG_YES);
			ccStatus = processAuthFraudResponse( ccStatus, ccServiceStatus,pCreditCardInfo.getOrder().getId(),ccInfo.getTokenValue());			
			if( isLoggingError() ) {
				logError( "Exception authorizing through credit card services processing it as offline order", e );
			}
		}finally{
        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
        }

		if( isLoggingDebug() ) {
			logDebug( "authorize(CreditCardInfo) - end" ); 
		}
		return ccStatus;
	}


	/**
	 * Method used to set cardtype and tokenvalue / paypageregistration id 
	 * @param pCreditCardInfo
	 * @param ccServiceStatus
	 * @param ccInfo
	 */
	private void setCreditValues(CreditCardInfo pCreditCardInfo,
			DigitalCreditCardServiceStatus ccServiceStatus, DigitalCreditCardInfo ccInfo) {
		List<?> pgList = pCreditCardInfo.getOrder().getPaymentGroups();
		for (Object pg : pgList) {
			if (pg instanceof DigitalCreditCard) {
				DigitalCreditCard cc = (DigitalCreditCard) pg;
				//Vantiv will not be empty only in case of New Credit card request
				//we dont need to set the  type of card again for existing cards.
				if(DigitalStringUtil.isNotEmpty(cc.getVantivFirstSix())){
					cc.setCreditCardType(getDswProfileTools().getCreditCardType(cc));
					
				}
				if(DigitalStringUtil.isNotEmpty(ccServiceStatus.getTokenValue())){
					cc.setTokenValue(ccServiceStatus.getTokenValue());
					//set the last four of the token value
					cc.setcreditCardLastFour(ccServiceStatus.getTokenValue().substring( ccServiceStatus.getTokenValue().length() - 4 ));
				}
				 else if(ccInfo!=null && DigitalStringUtil.isNotEmpty(ccInfo.getPaypageRegistrationId())){
					cc.setTokenValue(ccInfo.getPaypageRegistrationId());
				}
				if(cc.isNewCreditCard()){
					cc.setCvvNumber(getDswServiceConstants().getDefaultCVVNumber());
				}else{
					//logic for existing card goes here
				}
			}
		}
	}	

	public CreditCardStatus registerTokenRequest( DigitalCreditCard pCreditCard ) {
		final String METHOD_NAME = "registerTokenRequest";		
		if( isLoggingDebug() ) {
			logDebug( "registerTokenRequest(CreditCardInfo pCreditCardInfo=" + pCreditCard + ") - start" );			
		}
		// Determine which credit card services to call
		DigitalCreditCardStatus ccStatus = new DigitalCreditCardStatus();
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
			if( isLoggingDebug() ) {
				logDebug( "authorize() - Calling registerTokenRequest service..." ); 
			}
			DigitalCreditCardServiceStatus ccServiceStatus = new DigitalCreditCardServiceStatus();
			//Checking if Vantiv service and  service method both are not offline
			if(getVantivPaymentService().isServiceEnabled() 
					&& getVantivPaymentService().isServiceMethodEnabled(PaymentAuthorizationService.ServiceMethod.REGISTER_TOKEN_REQUEST.getServiceMethodName())){
				ccServiceStatus = getCreditCardServices().registerTokenRequest(DigitalCreditCardServiceStatus.PRE_REGISTER, pCreditCard);
				
				ccStatus.setTransactionId( ccServiceStatus.getTransactionId() );
				ccStatus.setTokenValue(ccServiceStatus.getTokenValue());
				//set transaction to true only if we get response code as 801 or 802
				if(DigitalStringUtil.isNotEmpty(ccServiceStatus.getResponse()) 
							&& ccServiceStatus.getResponse().equals(DigitalCreditCardServiceStatus.PREREG_APPROVALCODE_ONE)
								||ccServiceStatus.getResponse().equals(DigitalCreditCardServiceStatus.PREREG_APPROVALCODE_TWO)){
					ccStatus.setTransactionSuccess( true );
				}else{
					ccStatus.setTransactionSuccess( false );
				}
			}else{
				//setting transaction to  false if  vantiv is offline
				ccStatus.setTransactionSuccess( false );
			}			
		}catch( DigitalIntegrationException e ) {
			ccStatus.setTransactionSuccess( false );
			if( isLoggingError() ) {
				logError( "Exception authorizing through credit card services", e );
			}
		}finally{
        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
        }

		if( isLoggingDebug() ) {
			logDebug( "authorize(CreditCardInfo) - end" ); 
		}
		
		return ccStatus;
	}	
	


	private boolean orderHasTestItem( Order order ) {
		boolean retVal = false;
		if( order != null ) {
			List commerceItems = order.getCommerceItems();
			if( commerceItems != null ) {
				for( int c = 0; c < commerceItems.size(); c++ ) {
					DigitalCommerceItem item = (DigitalCommerceItem)commerceItems.get( c );
					if( item != null ) {
						if( "TESTITEM-NOTFORSALE".equalsIgnoreCase( item.getCatalogRefId() ) ) {
							retVal = true;
							break;
						}
					}
				}
			}
		}
		return retVal;
	}
	
	
	/** Process the authorization response
	 * 
	 * @param ccStatus         
	 * @param ccServiceStatus object detailing the results of the
	 * @param orderId
	 * @param tokenValue
	 */
	private DigitalCreditCardStatus processAuthFraudResponse( DigitalCreditCardStatus ccStatus, DigitalCreditCardServiceStatus ccServiceStatus,String orderId,String tokenValue ) {
		String decision = ccServiceStatus.getDecision();
		String fraudCode = ccServiceStatus.getFraudResultCode();
		String cardType = "NewCreditCard";
		if(DigitalStringUtil.isNotEmpty(tokenValue)){
			cardType="SavedCreditCard";
		}

		try {
			
			if( isLoggingDebug() ) {
				logDebug( " authorize result- DECISION IS: " + decision + " Fraude code is: " + fraudCode + " Fraude reason is: " + ccServiceStatus.getFraudResultCodeDescription() +" Card used is: "+cardType );
			}
			String fraudFlg = ccServiceStatus.getFraudFlag();
			//Offline scenario need to let the order go through
			if( DigitalStringUtil.isBlank( fraudFlg ) || fraudFlg.equalsIgnoreCase( DigitalCreditCardServiceStatus.FRAUD_FLAG_YES ) && decision.equalsIgnoreCase(DigitalCreditCardServiceStatus.DECISION_OFFLINE) ) {
				ccStatus.setFraudFlag(  DigitalCreditCardServiceStatus.FRAUD_FLAG_OFFLINE );
				ccStatus.setTransactionSuccess( true );
				ccStatus.setErrorMessage( "" );
				logInfo("Credit card authorization is successful for offlineOrder.OrderId="+orderId +",Card used="+cardType);
				
			}
			// if decision is review, and communication error, set to success.
			else if( null!=decision && decision.equalsIgnoreCase( DigitalCreditCardServiceStatus.DECISION_REVIEW ) && ccServiceStatus.isCommunicationError() ) {
				ccStatus.setTransactionSuccess( true );
				logInfo("Credit card authorization is successful for onlineOrder.OrderId="+orderId +",Card used="+cardType+",Communication error");
			}
			// if decision is approved or review, and fraud code is not 1/rejected, set to success.
			else if( null!=decision && ( decision.equalsIgnoreCase( DigitalCreditCardServiceStatus.DECISION_ACCEPT ) || decision.equalsIgnoreCase( DigitalCreditCardServiceStatus.DECISION_REVIEW ) ) && !DigitalCreditCardServiceStatus.FRAUD_RESULT_CODE_REJECTED_ERROR_CODE.equals( fraudCode ) ) {
				ccStatus.setTransactionSuccess( true );
				logInfo( "Credit card authorization is successful for onlineOrder.Decison="+decision+",FraudResultCode="+fraudCode+",ReasonCode="+ccServiceStatus.getReasonCode()+",OrderId="+orderId+",Card used="+cardType);
			}else {
				ccStatus.setTransactionSuccess( false );
				
				if( (null!=decision &&( decision.equalsIgnoreCase( DigitalCreditCardServiceStatus.DECISION_ACCEPT ) || decision.equalsIgnoreCase( DigitalCreditCardServiceStatus.DECISION_REVIEW ) ))
						&& DigitalCreditCardServiceStatus.FRAUD_RESULT_CODE_REJECTED_ERROR_CODE.equalsIgnoreCase( fraudCode ) ) {
					logInfo( "Credit card authorization failed in Fraud Check.Decison="+decision+",FraudResultCode="+fraudCode+",ReasonCode="+ccServiceStatus.getReasonCode()+",OrderId="+orderId+",Card used="+cardType);
					ccStatus.setErrorMessage( ccServiceStatus.getFraudResultCode() );

				} else {
					logInfo( "Credit card authorization failed.Decison="+decision+",FraudResultCode="+fraudCode+",ReasonCode="+ccServiceStatus.getReasonCode()+",OrderId="+orderId+",Card used="+cardType);
					ccStatus.setErrorMessage( ccServiceStatus.getReasonCode() );
				}
			}
			if( isLoggingDebug() ) {
				logDebug( "EXITING WITH TRANSACTION: " + ccStatus.getTransactionSuccess() );
			}
		} catch( Exception e ) {
			ccStatus.setTransactionSuccess( false );
			if( isLoggingError() ) {
				logError( "Exception authorizing through credit card services"+",Card used="+cardType+  e );
			}
		}
		return ccStatus;
	}
	

	/** Debit the amount on the credit card after authorization
	 * 
	 * @param pCreditCardInfo
	 *            the CreditCardInfo reference which contains all the debit data
	 * @param pStatus
	 *            the CreditCardStatus object which contains information about
	 *            the transaction. This should be the object which was returned
	 *            from authorize().
	 * @return a CreditCardStatus object detailing the results of the debit */
	public CreditCardStatus debit( CreditCardInfo pCreditCardInfo, CreditCardStatus pStatus ) {
		final String METHOD_NAME = "debit";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
		if( isLoggingDebug() ) {
			logDebug( "debit(CreditCardInfo pCreditCardInfo=" + pCreditCardInfo + ", CreditCardStatus pStatus=" + pStatus + ") - start" ); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if( isLoggingDebug() ) logDebug( "Debiting credit card " + pCreditCardInfo.getCreditCardNumber() + " for " + pCreditCardInfo.getAmount() );
		CreditCardStatusImpl ccStatus = new CreditCardStatusImpl();
		try {
			DigitalCreditCardServiceStatus ccServiceStatus = getCreditCardServices().preAuthorization( DigitalCreditCardServiceStatus.DEBIT, (DigitalCreditCardInfo)pCreditCardInfo, (int)pCreditCardInfo.getOrder().getTotalCommerceItemCount() );
			ccStatus.setTransactionId( ccServiceStatus.getAuthorizationCode() );
			ccStatus.setAmount( ccServiceStatus.getAmountAuthorized() );
			if( ccServiceStatus.getDecision().equals( DigitalCreditCardServiceStatus.DECISION_ACCEPT ) ) {
				ccStatus.setTransactionSuccess( true );
			} else {
				ccStatus.setTransactionSuccess( false );
			}

		} catch( DigitalIntegrationException e ) {
			ccStatus.setTransactionSuccess( false );
			if( isLoggingError() ) {
				logError( "Exception authorizing through credit card services", e );
			}
		}finally{
        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
        }

		if( isLoggingDebug() ) {
			logDebug( "debit(CreditCardInfo, CreditCardStatus) - end" );
		}
		return ccStatus;
		
	}

	/** Credit the amount on the credit card after debiting
	 * 
	 * @param pCreditCardInfo
	 *            the CreditCardInfo reference which contains all the credit
	 *            data
	 * @param pStatus
	 *            the CreditCardStatus object which contains information about
	 *            the transaction. This should be the object which was returned
	 *            from debit().
	 * @return a CreditCardStatus object detailing the results of the credit */
	public CreditCardStatus credit( CreditCardInfo pCreditCardInfo, CreditCardStatus pStatus ) {
		final String METHOD_NAME = "credit";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
		if( isLoggingDebug() ) {
			logDebug( "credit(CreditCardInfo pCreditCardInfo=" + pCreditCardInfo + ", CreditCardStatus pStatus=" + pStatus + ") - start" ); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if( isLoggingDebug() ) logDebug( "Crediting credit card " + pCreditCardInfo.getCreditCardNumber() + " for " + pCreditCardInfo.getAmount() );
		CreditCardStatusImpl ccStatus = new CreditCardStatusImpl();
		try {
			DigitalCreditCardServiceStatus ccServiceStatus = getCreditCardServices().preAuthorization( DigitalCreditCardServiceStatus.CREDIT, (DigitalCreditCardInfo)pCreditCardInfo, (int)pCreditCardInfo.getOrder().getTotalCommerceItemCount() );
			ccStatus.setTransactionId( ccServiceStatus.getAuthorizationCode() );
			ccStatus.setAmount( ccServiceStatus.getAmountAuthorized() );
			if( ccServiceStatus.getDecision().equals( DigitalCreditCardServiceStatus.DECISION_ACCEPT ) ) {
				ccStatus.setTransactionSuccess( true );
			} else {
				
				ccStatus.setTransactionSuccess( false );
				
			}

		} catch( DigitalIntegrationException e ) {
			ccStatus.setTransactionSuccess( false );
			if( isLoggingError() ) {
				logError( "Exception authorizing through credit card services", e );
			}
		}finally{
        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
        }

		if( isLoggingDebug() ) {
			logDebug( "credit(CreditCardInfo, CreditCardStatus) - end" );
		}
		return ccStatus;
		
	}

	/** Credit the amount on the credit card with as a new order
	 * 
	 * @param pCreditCardInfo
	 *            the CreditCardInfo reference which contains all the credit
	 *            data
	 * @return a CreditCardStatus object detailing the results of the credit */
	public CreditCardStatus credit( CreditCardInfo pCreditCardInfo ) {
		final String METHOD_NAME = "credit";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
		if( isLoggingDebug() ) {
			logDebug( "credit(CreditCardInfo pCreditCardInfo=" + pCreditCardInfo + ") - start" ); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if( isLoggingDebug() ) logDebug( "Crediting credit card " + pCreditCardInfo.getCreditCardNumber() + " for " + pCreditCardInfo.getAmount() );

		CreditCardStatusImpl ccStatus = new CreditCardStatusImpl();
		try {
			DigitalCreditCardServiceStatus ccServiceStatus = getCreditCardServices().preAuthorization( DigitalCreditCardServiceStatus.CREDIT, (DigitalCreditCardInfo)pCreditCardInfo, (int)pCreditCardInfo.getOrder().getTotalCommerceItemCount() );
			ccStatus.setTransactionId( ccServiceStatus.getAuthorizationCode() );
			ccStatus.setAmount( ccServiceStatus.getAmountAuthorized() );
			if( ccServiceStatus.getDecision().equals( DigitalCreditCardServiceStatus.DECISION_ACCEPT ) ) {
				ccStatus.setTransactionSuccess( true );
			} else {
				ccStatus.setTransactionSuccess( false );
			}

		} catch( DigitalIntegrationException e ) {
			ccStatus.setTransactionSuccess( false );
			if( isLoggingError() ) {
				logError( "Exception authorizing through credit card services", e );
			}
		}finally{
        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
        }

		if( isLoggingDebug() ) {
			logDebug( "credit(CreditCardInfo) - end" );
		}
		return ccStatus;
	}
}
