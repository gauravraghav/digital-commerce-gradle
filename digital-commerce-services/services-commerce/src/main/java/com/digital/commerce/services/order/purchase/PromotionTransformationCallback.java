package com.digital.commerce.services.order.purchase;

import static com.digital.commerce.common.util.ComponentLookupUtil.PROMOTION_TOOLS;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.services.promotion.DigitalPromotionTools;
import com.digital.commerce.services.utils.IXpathResultCallback;


/** Default Promotion transformation callback
 * 
 * */
public class PromotionTransformationCallback implements IXpathResultCallback {

	private static final DigitalLogger	logger						= DigitalLogger.getLogger( PromotionTransformationCallback.class );

	private DigitalPromotionTools	promotionTools;

	public final static String	ORDER_LEVEL_ADJUSTMENTS		= "//order/order.priceInfo/orderPriceInfo/orderPriceInfo.adjustments/pricingAdjustment/pricingAdjustment.promoId[text()]";
	public final static String	ITEM_LEVEL_ADJUSTMENTS		= "//order/order.relationships/relationshipsshipItemRel/shipItemRel.commerceItem/commerceItem/commerceItem.priceInfo/itemPriceInfo/itemPriceInfo.adjustments/pricingAdjustment/pricingAdjustment.promoId[text()]";
	public final static String	SHIPPING_LEVEL_ADJUSTMENTS	= "//order/order.relationships/relationshipsshipItemRel/shipItemRel.shippingGroup/hardgoodShippingGroup/hardgoodShippingGroup.priceInfo/shippingPriceInfo/shippingPriceInfo.adjustments/pricingAdjustment/pricingAdjustment.promoId[text()]";

	public PromotionTransformationCallback() {
		this.promotionTools = getPromotionTools();
	}

	@Override
	public void doInElement( final Document document, final Node match ) {
		try {
			Node parent = match.getParentNode();

			if( logger.isDebugEnabled() ) {
				logger.debug( "Found match: " + match.getNodeName() );
				logger.debug( "My parent is: " + parent.getNodeName() );
			}

			Element promotionCode = document.createElement( "order:pricingAdjustment.promotionCode" );

			if( logger.isDebugEnabled() ) {
				logger.debug( "Creating character data section for coupon code / description: " + match.getTextContent() );
			}

			CDATASection couponCode = document.createCDATASection( promotionTools.getCouponCodeForPromotion( match.getTextContent() ) );
			promotionCode.appendChild( couponCode );
			// build promotion description char data
			Element promotionCodeDescription = document.createElement( "order:pricingAdjustment.promotionDescription" );

			CDATASection promotionDescription = document.createCDATASection( promotionTools.getDescriptionForPromotion( match.getTextContent() ) );
			promotionCodeDescription.appendChild( promotionDescription );
			parent.appendChild( promotionCode );
			parent.appendChild( promotionCodeDescription );
		} catch( Exception e ) {
			logger.error( e, e );
			throw new RuntimeException( "Unable to transform order xml with new promtion elements.", e );
		}
	}

	private DigitalPromotionTools getPromotionTools() {
		return ComponentLookupUtil.lookupComponent( PROMOTION_TOOLS, DigitalPromotionTools.class );
	}
}
