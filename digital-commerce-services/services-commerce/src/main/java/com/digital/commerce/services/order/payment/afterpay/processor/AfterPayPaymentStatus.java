package com.digital.commerce.services.order.payment.afterpay.processor;

import atg.payment.PaymentStatus;
import java.util.Date;

public interface AfterPayPaymentStatus extends PaymentStatus {

    public static String paymentId=null;
    public String status=null;
    public Date createdDate=null;
    public String merchantReference=null;
}