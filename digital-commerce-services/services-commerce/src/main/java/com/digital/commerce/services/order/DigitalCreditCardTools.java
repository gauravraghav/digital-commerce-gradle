package com.digital.commerce.services.order;

import atg.payment.creditcard.CreditCardInfo;
import atg.payment.creditcard.ExtendableCreditCardTools;


public class DigitalCreditCardTools extends ExtendableCreditCardTools {
	
	/*
	 * This method overrides verfiyCreditCard method form the super 
	 * class returns true with out doing anything as we will not have
	 * any credit card number to validate
	 * @see atg.payment.creditcard.ExtendableCreditCardTools#verifyCreditCard(atg.payment.creditcard.CreditCardInfo)
	 */
	public int verifyCreditCard( CreditCardInfo pCreditCard ) {
		return 0;
	}
}
