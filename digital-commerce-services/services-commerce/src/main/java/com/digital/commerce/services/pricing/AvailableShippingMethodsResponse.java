/**
 * 
 */
package com.digital.commerce.services.pricing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class AvailableShippingMethodsResponse extends ResponseWrapper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2200239888723701040L;

	private String shippingGroupId;

	private Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> shippingMethodsInfos;

	private Map shippingMethodOptions;
	
	private List<String> closenessQualifiers;
	
	private boolean shippingOptionsUnavailable;

	private List<String> warningMessages;

	public List<String> getWarningMessages() {
		if(null==warningMessages){
			warningMessages=new ArrayList<String>();
			
		}
		return warningMessages;
	}
}
