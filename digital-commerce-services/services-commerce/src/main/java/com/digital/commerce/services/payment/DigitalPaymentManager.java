package com.digital.commerce.services.payment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.payment.Constants;

import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.payment.giftcard.GiftCardStatus;
import com.digital.commerce.services.order.payment.giftcard.processor.GiftCardProcessorImpl;
import com.digital.commerce.common.logger.DigitalLogger;
import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupImpl;
import atg.commerce.payment.PaymentManager;
import atg.commerce.states.PaymentGroupStates;
import atg.commerce.states.StateDefinitions;
import atg.payment.PaymentStatus;
import lombok.Getter;
import lombok.Setter;


@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalPaymentManager extends PaymentManager {

	private static final DigitalLogger		logger	= DigitalLogger.getLogger( DigitalPaymentManager.class );

	private GiftCardProcessorImpl	giftCardProcessor;

	public List authorize(Order pOrder, List pPaymentGroups) throws CommerceException {
		if (pPaymentGroups == null) {
			throw new InvalidParameterException(Constants.INVALID_PAYMENT_GROUP_LIST);
		} else {
			List failedGroups = new ArrayList(pPaymentGroups.size());
			Iterator iter = pPaymentGroups.iterator();
			PaymentGroup group = null;

			while(iter.hasNext()) {
				group = (PaymentGroup)iter.next();

				try {
					this.authorize(pOrder, group);
				} catch (CommerceException var7) {
					failedGroups.add(group.getId());
					break;
				}
			}

			// call the expireGCauth method
			expireGiftCardAuthorization( failedGroups, pPaymentGroups );
			return failedGroups;
		}
	}
	/** invoke the preauthcomplete to expire the authorization. If failed, just
	 * log the error, not want to do anything extra here.
	 * 
	 * @param giftCard
	 * @throws CommerceException */
	public void expireGiftCardAuthorization( List failedGroups, List<PaymentGroupImpl> pPaymentGroups ) throws CommerceException {
		boolean cancel = needToCancelGCAuth( failedGroups, pPaymentGroups );
		if( isLoggingDebug() ) {
			logDebug( "needToCancelGCAuth?" + cancel );
		}

		if( cancel ) {
			expireGiftCardAuthorization( pPaymentGroups );
		}

	}

	/** invoke the preauthcomplete to expire the authorization. If failed, just
	 * log the error, not want to do anything extra here.
	 * 
	 * @param giftCard
	 * @throws CommerceException */
	public void expireGiftCardAuthorization( List<PaymentGroupImpl> pPaymentGroups ) throws CommerceException {
		List<DigitalGiftCard> giftCardPaymentGroups = getGCPaymentGroups( pPaymentGroups );

		if( giftCardPaymentGroups != null && giftCardPaymentGroups.size() != 0 ) {
			for( int i = 0; i < giftCardPaymentGroups.size(); i++ ) {
				DigitalGiftCard giftCard = (DigitalGiftCard)giftCardPaymentGroups.get( i );
				PaymentStatus lastPaymentStatus = getLastAuthorizationStatus( giftCard );
				if( lastPaymentStatus.getTransactionSuccess() ) {
					GiftCardStatus status = getGiftCardProcessor().expireAuthorization( giftCard, lastPaymentStatus.getTransactionId() );
					if( isLoggingDebug() ) {
						logDebug( "expire the authorization of GC " + giftCard.getCardNumber() + " result- status:" + status.getTransactionSuccess() + " " + status.getErrorMessage() );
					}
					// need to update the status, so the GC can be used again within the same order
					postProcessExpireAuth( giftCard, status, lastPaymentStatus.getAmount() );

				} else {
					if( isLoggingDebug() ) {
						logDebug( "last authorization for GC " + giftCard.getCardNumber() + " is failure, doesn't need to be expired " );
					}
				}
			}
		} else {
			if( isLoggingDebug() ) {
				logDebug( "no gift card payment groups authorizations need to be expired " );
			}
		}

	}

	// -------------------------------------
	/** This method executes post processing after an authorize() call to a payment processor.
	 * 
	 * @param pPaymentGroup the PaymentGroup which was authorized
	 * @param pStatus the PaymentStatus object returned from the authorize call of the payment processor
	 * @param pAmount the amount of the transaction */
	protected void postProcessExpireAuth( PaymentGroup pPaymentGroup, PaymentStatus pStatus, double pAmount ) throws CommerceException {
		if( isLoggingDebug() )
			logDebug( "Expire Authorized PaymentGroup " + pPaymentGroup.getId() + ". Results[" + pStatus.getTransactionId() + ", " + pStatus.getTransactionSuccess() + "," + pStatus.getErrorMessage() + "," + pStatus.getTransactionTimestamp() + "]" );

		pPaymentGroup.addAuthorizationStatus( pStatus );
		if( pStatus.getTransactionSuccess() ) {
			pPaymentGroup.setAmountAuthorized( pPaymentGroup.getAmountAuthorized() - pAmount );
			int state = StateDefinitions.PAYMENTGROUPSTATES.getStateValue( PaymentGroupStates.INITIAL );
			pPaymentGroup.setState( state );
		} else {// not going to throw erorr messages, if the expire auth failed.
			// String[] msgArgs = { pPaymentGroup.getId(), pStatus.getErrorMessage() };
			int state = StateDefinitions.PAYMENTGROUPSTATES.getStateValue( PaymentGroupStates.CREDIT_FAILED );
			pPaymentGroup.setState( state );
			// pPaymentGroup.setStateDetail(MessageFormat.format(Constants.PAYMENT_GROUP_AUTH_FAILURE, msgArgs));
			// throw new PaymentException(MessageFormat.format(Constants.PAYMENT_GROUP_AUTH_FAILURE, msgArgs));
		}
	}

	/** only need to expire the GC authorization when failedGroup size >1, and GC
	 * is present and not as failed one.
	 * 
	 * @param failedGroups
	 * @param paymentGroups
	 * @return DigitalGiftCard */
	private boolean needToCancelGCAuth( List failedGroups, List<PaymentGroupImpl> paymentGroups ) {

		// if no failedGroups, ignore
		if( failedGroups.size() < 1 ) { return false; }
		// if there is no GC paymentGroup in the order, ignore
		List<DigitalGiftCard> giftCardPaymentGroups = getGCPaymentGroups( paymentGroups );
		if( giftCardPaymentGroups == null || giftCardPaymentGroups.size() == 0 ) { return false; }

		// if there is one GC success case, then set to true
		for( int i = 0; i < giftCardPaymentGroups.size(); i++ ) {
			DigitalGiftCard giftCard = giftCardPaymentGroups.get( i );
			PaymentStatus lastPaymentStatus;
			try {
				lastPaymentStatus = getLastAuthorizationStatus( giftCard );
				if( lastPaymentStatus.getTransactionSuccess() ) { return true; }
			} catch( CommerceException e ) {
				logger.error( "Commerce exception caught: ", e );
			}

		}

		return false;

	}

	/** returnt the GiftCardPaymentGroups as a list
	 * 
	 * @param paymentGroups
	 * @return DigitalGiftCard */
	private List<DigitalGiftCard> getGCPaymentGroups( List<PaymentGroupImpl> paymentGroups ) {
		List<DigitalGiftCard> result = new ArrayList<>();
		Iterator<PaymentGroupImpl> iter = paymentGroups.iterator();

		while( iter.hasNext() ) {
			PaymentGroupImpl pg = iter.next();
			if( pg instanceof DigitalGiftCard ) {
				result.add( (DigitalGiftCard)pg );
			}
		}

		if( isLoggingDebug() ) {
			logDebug( "found gc payment Goups:" + result.size() );
		}
		return result;
	}

}
