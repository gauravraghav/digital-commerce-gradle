/**
 *
 */
package com.digital.commerce.services.order.payment.giftcard;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 */
@Getter
@Setter
public class GiftCardAuthInfo {
	private double		amount;

	private String		transactionId;

	private String		errorMessage;

	private boolean		transactionSuccess;

	private Date		transactionTimestamp;

	private Timestamp	authorizationExpiration;
	private String		authorizationCode;
}
