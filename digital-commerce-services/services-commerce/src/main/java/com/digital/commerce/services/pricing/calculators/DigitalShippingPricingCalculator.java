/**
 *
 */
package com.digital.commerce.services.pricing.calculators;

import static com.digital.commerce.common.services.DigitalBaseConstants.STATIC_SHIPPING_RATES;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingTools;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.commerce.pricing.ShippingPricingCalculator;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.MutableRepository;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingGroup;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingItem;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingOption;
import com.digital.commerce.integration.inventory.yantra.domain.YantraShippingRestrictionsServiceResponse;
import com.digital.commerce.integration.inventory.yantra.domain.YantraType;
import com.digital.commerce.services.inventory.DigitalInventoryManager;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.GiftCardCommerceItem;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.payment.BillingShippingServices;
import com.digital.commerce.services.pricing.AvailableShippingMethodsResponse;
import com.digital.commerce.services.pricing.CommerceItemInfo;
import com.digital.commerce.services.pricing.CommerceItemInfoWrapper;
import com.digital.commerce.services.pricing.DigitalPricingTools;
import com.digital.commerce.services.pricing.ShippingDestinationQuote;
import com.digital.commerce.services.pricing.ShippingHandlingInfo;
import com.digital.commerce.services.pricing.ShippingMethodInfo;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

@SuppressWarnings({"rawtypes", "unchecked"})
@Getter
@Setter
public class DigitalShippingPricingCalculator extends ApplicationLoggingImpl implements
    ShippingPricingCalculator {

  private int giftCardGroupStateCode = 8;

  private BillingShippingServices billingShippingServices;

  private OrderManager orderManager;

  private PricingTools pricingTools;

  private ShippingHandlingInfo pShippingHandlingInfo;

  private DigitalInventoryManager inventoryManager;

  private MutableRepository productRepository;

  public DigitalShippingPricingCalculator() {
    super(DigitalShippingPricingCalculator.class.getName());
  }

  /**
   * @param pMethods
   * @param pShippingGroup
   * @param pPricingModel
   * @param pLocale
   * @param pProfile
   * @param pExtraParameters
   * @throws PricingException
   */
  public void getAvailableMethods(List pMethods, ShippingGroup pShippingGroup,
      RepositoryItem pPricingModel, Locale pLocale, RepositoryItem pProfile, Map pExtraParameters)
      throws PricingException {
    if (isLoggingDebug()) {
      logDebug("Getting order for shipping group: " + pShippingGroup.getId());
    }
    Order order = retrieveOrder(pShippingGroup);
    AvailableShippingMethodsResponse availableShippingMethodsResponse =
        (AvailableShippingMethodsResponse) pExtraParameters.get("availableShippingMethodsResponse");
    if (getBillingShippingServices().isYantraServiceOffline(order)) {
      availableShippingMethodsResponse.setShippingOptionsUnavailable(true);
    }else{
    	availableShippingMethodsResponse.setShippingOptionsUnavailable(false);
    }
    YantraShippingRestrictionsServiceResponse yantraResponse;
    Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> shippingMethods = new HashMap<>();
    Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> shippingMethodRestrictions = new HashMap<>();
    try {
      if (pShippingGroup instanceof HardgoodShippingGroup) {
        if (!isShippingAddressEmpty((HardgoodShippingGroup) pShippingGroup)
            && !getBillingShippingServices().isYantraServiceOffline(order)) {
          yantraResponse = getBillingShippingServices().getShippingMethods(order, "sales");
          shippingMethodRestrictions = getShippingMethodsWithRestrictions(order, yantraResponse);
        }
        shippingMethods = calculateDropShipShippingRestriction(order,
            availableShippingMethodsResponse);
        if (shippingMethodRestrictions != null && shippingMethodRestrictions.size() > 0) {
          shippingMethods = mergeShippingRestrictionsWithInventory(shippingMethodRestrictions,
              shippingMethods, order);
        }
      }
    } catch (DigitalIntegrationException ie) {
      if (ie.getErrorCode() != null) {
        availableShippingMethodsResponse.setShippingOptionsUnavailable(true);
      }
      shippingMethods = calculateDropShipShippingRestriction(order,
          availableShippingMethodsResponse);
      logError(ie);
    } catch (Exception e) {
      shippingMethods = calculateDropShipShippingRestriction(order,
          availableShippingMethodsResponse);
      logError(e);
    }
    pMethods.add(shippingMethods);
  }

  /**
   * @param order
   * @param yantraResponse
   * @return Map
   */
  private Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> getShippingMethodsWithRestrictions(
      Order order, YantraShippingRestrictionsServiceResponse yantraResponse) {
    Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> shippingMethodsInfo = new HashMap<>();
    try {
      List<YantraShippingOption> shippingOptions = yantraResponse.getYantraShippingOptions();
      for (YantraShippingOption shippingOption : shippingOptions) {
        if (null != shippingOption.getYantraShippingGroup()) {
          for (YantraShippingGroup shippingGroup : shippingOption.getYantraShippingGroup()) {
            if (DigitalStringUtil.isNotBlank(shippingGroup.getShipToGroupIdentifier())) {
              final String method = shippingOption.getLOS();
              if (getBillingShippingServices().isAcceptableShippingMethod(method, order)) {
                final ShippingMethodInfo shippingMethodInfo = new ShippingMethodInfo();
                shippingMethodInfo.setShippingMethod(method);
                shippingMethodInfo.setBaseRate(
                    shippingGroup.getYantraShipToSHCosts().getYantraShippingSHGrossCosts()
                        .getTotalGrossCosts());
                List<CommerceItemInfoWrapper> ciList = new ArrayList<>();
                for (YantraShippingItem yantraShippingItem : shippingGroup
                    .getYantraShippingItems()) {
                  CommerceItemInfoWrapper ci = new CommerceItemInfoWrapper();
                  ci.setQuantity(yantraShippingItem.getQuantity());
                  ci.setId(yantraShippingItem.getItemId());
                  ci.setProductTitle(getProductTitle(ci.getId(), order));
                  this.updateCommerceItemInfoWrapper(ci, order);
                  ciList.add(ci);
                }
                if (shippingMethodsInfo.get(shippingMethodInfo) == null) {
                  shippingMethodsInfo.put(shippingMethodInfo, ciList);
                } else {
                  List<CommerceItemInfoWrapper> ciArray = shippingMethodsInfo
                      .get(shippingMethodInfo);
                  ciArray.addAll(ciList);
                }
              }
            }
          }
        } else if (null != shippingOption.getRestrictions()) {
          List<String> restrictedSKUs = new ArrayList<>();
          boolean allItemsRestricted = false;
          for (YantraType yantraType : shippingOption.getRestrictions()) {
            if ("RestrictionBasis.ITEM_ID".equalsIgnoreCase(yantraType.getType())) {
              restrictedSKUs.add(yantraType.getValueAttribute());
              restrictedSKUs.add("e-GC");
              restrictedSKUs.add("STANDARD-GC");
              restrictedSKUs.add("PERSONALIZED-GC");
            }
            if("RestrictionBasis.ADDRESS_TYPE".equalsIgnoreCase(yantraType.getType()) ||
                "RestrictionBasis.COUNTRY".equalsIgnoreCase(yantraType.getType()) ||
                "RestrictionBasis.STATE".equalsIgnoreCase(yantraType.getType()) ||
                "RestrictionBasis.ZIP_CODE".equalsIgnoreCase(yantraType.getType())){
              allItemsRestricted = true;
            }
          }

          // If there is address related restriction then none of the items can be shipped using
          // this LOS method
          if(!allItemsRestricted) {
            if (!restrictedSKUs.isEmpty()) {
              final ShippingMethodInfo shippingMethodInfo = new ShippingMethodInfo();
              shippingMethodInfo.setShippingMethod(shippingOption.getLOS());
              shippingMethodInfo.setBaseRate(0.0);
              List<CommerceItemInfoWrapper> ciList = new ArrayList<>();
              for (Object yantraShippingItem : order.getCommerceItems()) {
                CommerceItem commerceItem = (CommerceItem) yantraShippingItem;
                if (!restrictedSKUs.contains(commerceItem.getCatalogRefId())) {
                  CommerceItemInfoWrapper ci = new CommerceItemInfoWrapper();
                  ci.setQuantity(commerceItem.getQuantity());
                  ci.setId(commerceItem.getCatalogRefId());
                  ci.setProductTitle(getProductTitle(ci.getId(), order));
                  this.updateCommerceItemInfoWrapper(ci, order);
                  ciList.add(ci);
                }
              }
              if (shippingMethodsInfo.get(shippingMethodInfo) == null) {
                shippingMethodsInfo.put(shippingMethodInfo, ciList);
              } else {
                List<CommerceItemInfoWrapper> ciArray = shippingMethodsInfo.get(shippingMethodInfo);
                ciArray.addAll(ciList);
              }
            }
          }
        }
      }
    } catch (Exception ex) {
      if (isLoggingError()) {
        logError(ex);
      }
    }
    return shippingMethodsInfo;
  }

  /**
   * @param sGroup
   * @param shippingMap
   * @return ShippingDestinationQuote
   * @throws PricingException
   */
  private ShippingDestinationQuote getPriceQuoteForGroup(ShippingGroup sGroup, Map shippingMap)
      throws PricingException {
    if (shippingMap.containsKey(sGroup.getId())) {
      return (ShippingDestinationQuote) shippingMap.get(sGroup.getId());
    } else if (!(sGroup instanceof HardgoodShippingGroup)) {
      ShippingDestinationQuote sdQuote = new ShippingDestinationQuote();
      sdQuote.setGrossShippingAndHandling(0.0);
      sdQuote.setNetShippingAndHandling(0.0);
      sdQuote.setShippingAndHandlingSavings(0.0);
    }
    throw new PricingException("Unable to obtain price quote for shipping group: " + sGroup);
  }

  /**
   * @param pOrder
   * @param pPriceQuote
   * @param pShippingGroup
   * @param pPricingModel
   * @param pLocale
   * @param pProfile
   * @param pExtraParameters
   * @throws PricingException
   */
  public void priceShippingGroup(Order pOrder, ShippingPriceInfo pPriceQuote,
      ShippingGroup pShippingGroup, RepositoryItem pPricingModel, Locale pLocale,
      RepositoryItem pProfile, Map pExtraParameters) throws PricingException {
    if (!(pShippingGroup instanceof HardgoodShippingGroup)) {
      return;
    }
    double netSH;
    double grossSH = 0.0d;
    if (pShippingGroup.getState() == this.getGiftCardGroupStateCode()) {
      List cirs = pShippingGroup.getCommerceItemRelationships();
      // Should only really be 1 relationship
      for (Object cir1 : cirs) {
        CommerceItemRelationship cir = (CommerceItemRelationship) cir1;
        CommerceItem ci = cir.getCommerceItem();
        if (!(ci instanceof GiftCardCommerceItem)) {
          // We're in a giftcard shipping group but this is not a
          // giftcard, what happened?!?!
          if (isLoggingError()) {
            logError("Found a non-giftcard commerce item in a giftcard shippinggroup.");
          }
          return;
        }
        GiftCardCommerceItem gci = (GiftCardCommerceItem) ci;
        if (gci != null && gci.getPostage() != null) {
          grossSH += gci.getPostage();
        }

      }
      netSH = grossSH;
    } else {
      Map shippingPriceMap = (Map) pExtraParameters.get(ShippingGroupConstants.SHIPPING_PRICE_MAP);
      if (shippingPriceMap == null || shippingPriceMap.isEmpty()) {
        throw new PricingException("Unable to obtain shipping price list");
      }
      ShippingDestinationQuote sdQuote = getPriceQuoteForGroup(pShippingGroup, shippingPriceMap);
      netSH = sdQuote.getNetShippingAndHandling();
      grossSH = sdQuote.getGrossShippingAndHandling();
    }
    double oldAmount = pPriceQuote.getAmount();
    double newAmount = netSH;

    pPriceQuote.setAmount(DigitalCommonUtil.round(newAmount));
    pPriceQuote.setRawShipping(DigitalCommonUtil.round(grossSH));

    double adjustAmount = pPriceQuote.getAmount() - oldAmount;

    pPriceQuote.getAdjustments().add(
        new PricingAdjustment(Constants.SHIPPING_PRICE_ADJUSTMENT_DESCRIPTION, null,
            getPricingTools().round(adjustAmount), 1));
  }

  /**
   * This method retrieve's order for shipping group.
   *
   * @param pShippingGroup
   * @return Order
   * @throws PricingException
   */
  protected Order retrieveOrder(ShippingGroup pShippingGroup) throws PricingException {
    Order order;
    try {
      order = getOrderManager().getOrderForShippingGroup(pShippingGroup);
    } catch (CommerceException | RepositoryException e) {
      if (isLoggingError()) {
        logError(e);
      }
      throw new PricingException(e);
    }

    return order;
  }

  /**
   * @param id
   * @param order
   * @return String
   */
  private String getProductTitle(String id, Order order) {
    List<DigitalCommerceItem> ciList = order.getCommerceItems();
    String productTitle = "";
    for (CommerceItem ci : ciList) {
      if (id.equalsIgnoreCase(ci.getCatalogRefId())) {
        productTitle = this.getProductDisplayName(ci);
      }
    }
    return productTitle;
  }

  private String getProductDisplayName(CommerceItem ci) {
    RepositoryItem product = (RepositoryItem) ci.getAuxiliaryData().getProductRef();
    RepositoryItem productItem = getRepositoryItemForItemId(product.getRepositoryId(),
            "product");
    // "productTitle" was being used.  It could have brand name included so displayName is used.
    return (String) productItem.getPropertyValue("displayName");
  }

  /**
   * @param order
   * @param availableShippingMethodsResponse
   * @return Map
   */
  private Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> calculateDropShipShippingRestriction(
      Order order, AvailableShippingMethodsResponse availableShippingMethodsResponse) {
    Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> shippingMethodsInfo = new HashMap<>();
    Map<CommerceItemInfo, List<ShippingMethodInfo>> shippingApplicableMethodsInfo = new HashMap<>();
    Map<CommerceItemInfo, List<ShippingMethodInfo>> shippingApplicableMethodsInfoEach;
    try {
      List<DigitalCommerceItem> ciList = order.getCommerceItems();
      for (CommerceItem ci : ciList) {
        shippingApplicableMethodsInfoEach = getApplicableShippingMethod(ci, order);
        shippingApplicableMethodsInfo.putAll(shippingApplicableMethodsInfoEach);
      }
    } catch (Exception ex) {
      if (isLoggingError()) {
        logError(ex);
      }
    }
    Iterator<Map.Entry<CommerceItemInfo, List<ShippingMethodInfo>>> entries = shippingApplicableMethodsInfo
        .entrySet().iterator();
    ShippingMethodInfo shippingMethodInfoDefault = new ShippingMethodInfo();
    ShippingMethodInfo shippingMethodInfoNDA = new ShippingMethodInfo();
    ShippingMethodInfo shippingMethodInfoSD = new ShippingMethodInfo();
    String siteId = order.getSiteId();
    Properties shippingRates = ((Properties) (MultiSiteUtil.getShippingRates(siteId)
        .get(STATIC_SHIPPING_RATES)));
    shippingMethodInfoDefault.setShippingMethod(ShippingGroupConstants.DEFAULT_SHIPPING_METHOD);
    shippingMethodInfoDefault.setBaseRate(
        Double.valueOf(shippingRates.getProperty(shippingMethodInfoDefault.getShippingMethod())));
    if (!availableShippingMethodsResponse.isShippingOptionsUnavailable()) {
      shippingMethodInfoNDA.setShippingMethod(ShippingGroupConstants.NEXT_DAY_SHIPPING_METHOD);
      shippingMethodInfoNDA.setBaseRate(
          Double.valueOf(shippingRates.getProperty(shippingMethodInfoDefault.getShippingMethod())));
      shippingMethodInfoSD.setShippingMethod(ShippingGroupConstants.SECOND_DAY_SHIPPING_METHOD);
      shippingMethodInfoSD.setBaseRate(
          Double.valueOf(shippingRates.getProperty(shippingMethodInfoDefault.getShippingMethod())));
    }
    List<CommerceItemInfo> commerceItemDefaultInfoList = new ArrayList<>();
    List<CommerceItemInfo> commerceItemNDAInfoList = new ArrayList<>();
    List<CommerceItemInfo> commerceItemSDInfoList = new ArrayList<>();
    while (entries.hasNext()) {
      Map.Entry<CommerceItemInfo, List<ShippingMethodInfo>> entry = entries.next();
      CommerceItemInfo commerceItemInfo = entry.getKey();
      List<ShippingMethodInfo> shippingMethodInfoList = entry.getValue();
      for (ShippingMethodInfo shippingMethodInfo : shippingMethodInfoList) {
        if (ShippingGroupConstants.DEFAULT_SHIPPING_METHOD
            .equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
          shippingMethodInfoDefault.setBaseRate(shippingMethodInfo.getBaseRate());
          if (!commerceItemDefaultInfoList.contains(commerceItemInfo)) {
            commerceItemDefaultInfoList.add(commerceItemInfo);
          }
        }
        if (!availableShippingMethodsResponse.isShippingOptionsUnavailable()) {
          if (ShippingGroupConstants.NEXT_DAY_SHIPPING_METHOD
              .equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
            shippingMethodInfoNDA.setBaseRate(shippingMethodInfo.getBaseRate());
            if (!commerceItemNDAInfoList.contains(commerceItemInfo)) {
              commerceItemNDAInfoList.add(commerceItemInfo);
            }
          }
          if (ShippingGroupConstants.SECOND_DAY_SHIPPING_METHOD
              .equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
            shippingMethodInfoSD.setBaseRate(shippingMethodInfo.getBaseRate());
            if (!commerceItemSDInfoList.contains(commerceItemInfo)) {
              commerceItemSDInfoList.add(commerceItemInfo);
            }
          }
        }
      }
    }
    List<CommerceItemInfoWrapper> commerceItemWrapperDefaultInfoList = new ArrayList<>();
    List<CommerceItemInfoWrapper> commerceItemWrapperNDAInfoList = new ArrayList<>();
    List<CommerceItemInfoWrapper> commerceItemWrapperSDInfoList = new ArrayList<>();
    for (Object object : commerceItemDefaultInfoList) {
      CommerceItemInfoWrapper ciInfoWrapper = new CommerceItemInfoWrapper();
      CommerceItemInfo ciInfo = (CommerceItemInfo) object;
      ciInfoWrapper.setId(ciInfo.getId());
      ciInfoWrapper.setProductTitle(getProductTitle(ciInfo.getId(), order));
      ciInfoWrapper.setQuantity(ciInfo.getQuantity());
      ciInfoWrapper.setBrand(ciInfo.getBrand());
      ciInfoWrapper.setColorCode(ciInfo.getColorCode());
      ciInfoWrapper.setProductId(ciInfo.getProductId());
      if (!commerceItemWrapperDefaultInfoList.contains(ciInfoWrapper)) {
        commerceItemWrapperDefaultInfoList.add(ciInfoWrapper);
      } else {
        int index = commerceItemWrapperDefaultInfoList.indexOf(ciInfoWrapper);
        CommerceItemInfoWrapper ciInfoWrapperTemp = commerceItemWrapperDefaultInfoList.get(index);
        commerceItemWrapperDefaultInfoList.remove(ciInfoWrapperTemp);
        ciInfoWrapper.setQuantity(ciInfoWrapper.getQuantity() + ciInfoWrapperTemp.getQuantity());
        commerceItemWrapperDefaultInfoList.add(ciInfoWrapper);
      }
    }
    if (!availableShippingMethodsResponse.isShippingOptionsUnavailable()) {
      for (Object itrNDA : commerceItemNDAInfoList) {
        CommerceItemInfoWrapper ciInfoWrapper = new CommerceItemInfoWrapper();
        CommerceItemInfo ciInfo = (CommerceItemInfo) itrNDA;
        ciInfoWrapper.setId(ciInfo.getId());
        ciInfoWrapper.setProductTitle(getProductTitle(ciInfo.getId(), order));
        ciInfoWrapper.setQuantity(ciInfo.getQuantity());
        ciInfoWrapper.setBrand(ciInfo.getBrand());
        ciInfoWrapper.setColorCode(ciInfo.getColorCode());
        ciInfoWrapper.setProductId(ciInfo.getProductId());
        if (!commerceItemWrapperNDAInfoList.contains(ciInfoWrapper)) {
          commerceItemWrapperNDAInfoList.add(ciInfoWrapper);
        } else {
          int index = commerceItemWrapperNDAInfoList.indexOf(ciInfoWrapper);
          CommerceItemInfoWrapper ciInfoWrapperTemp = commerceItemWrapperNDAInfoList.get(index);
          commerceItemWrapperNDAInfoList.remove(ciInfoWrapperTemp);
          ciInfoWrapper.setQuantity(ciInfoWrapper.getQuantity() + ciInfoWrapperTemp.getQuantity());
          commerceItemWrapperNDAInfoList.add(ciInfoWrapper);
        }
      }
      for (Object itrSD : commerceItemSDInfoList) {
        CommerceItemInfoWrapper ciInfoWrapper = new CommerceItemInfoWrapper();
        CommerceItemInfo ciInfo = (CommerceItemInfo) itrSD;
        ciInfoWrapper.setId(ciInfo.getId());
        ciInfoWrapper.setProductTitle(getProductTitle(ciInfo.getId(), order));
        ciInfoWrapper.setQuantity(ciInfo.getQuantity());
        ciInfoWrapper.setBrand(ciInfo.getBrand());
        ciInfoWrapper.setColorCode(ciInfo.getColorCode());
        ciInfoWrapper.setProductId(ciInfo.getProductId());
        if (!commerceItemWrapperSDInfoList.contains(ciInfoWrapper)) {
          commerceItemWrapperSDInfoList.add(ciInfoWrapper);
        } else {
          int index = commerceItemWrapperSDInfoList.indexOf(ciInfoWrapper);
          CommerceItemInfoWrapper ciInfoWrapperTemp = commerceItemWrapperSDInfoList.get(index);
          commerceItemWrapperSDInfoList.remove(ciInfoWrapperTemp);
          ciInfoWrapper.setQuantity(ciInfoWrapper.getQuantity() + ciInfoWrapperTemp.getQuantity());
          commerceItemWrapperSDInfoList.add(ciInfoWrapper);
        }
      }
    }
    shippingMethodsInfo.put(shippingMethodInfoDefault, commerceItemWrapperDefaultInfoList);
    if (!availableShippingMethodsResponse.isShippingOptionsUnavailable()) {
      shippingMethodsInfo.put(shippingMethodInfoNDA, commerceItemWrapperNDAInfoList);
      shippingMethodsInfo.put(shippingMethodInfoSD, commerceItemWrapperSDInfoList);
    }
    return shippingMethodsInfo;
  }

  /**
   * @param ci
   * @param order
   * @return Map
   */
  private Map<CommerceItemInfo, List<ShippingMethodInfo>> getApplicableShippingMethod(
      CommerceItem ci, Order order) {
    Map<CommerceItemInfo, List<ShippingMethodInfo>> map = new HashMap<>();
    RepositoryItem sku = (RepositoryItem) ci.getAuxiliaryData().getCatalogRef();

    String siteId = order.getSiteId();
    Properties shippingRates = ((Properties) (MultiSiteUtil.getShippingRates(siteId)
        .get(STATIC_SHIPPING_RATES)));
    Integer pFcStockLevel;
    Integer pStockLevel;
    Integer pDropshipStockLevel;
    Integer pRemainingStockLevel;
    pFcStockLevel = getInventoryManager()
        .findStockLevelForNodeWithLocationId(sku.getRepositoryId(), "fcStockLevel",
            MultiSiteUtil.getWebsiteLocationId());
    pStockLevel = getInventoryManager()
        .findStockLevelForNodeWithLocationId(sku.getRepositoryId(), "storeStockLevel",
            MultiSiteUtil.getWebsiteLocationId());
    pDropshipStockLevel = getInventoryManager()
        .findStockLevelForNodeWithLocationId(sku.getRepositoryId(), "dropshipStockLevel",
            MultiSiteUtil.getWebsiteLocationId());
    pRemainingStockLevel = getInventoryManager()
        .findStockLevelForNodeWithLocationId(sku.getRepositoryId(), "stockLevel",
            MultiSiteUtil.getWebsiteLocationId());

    // Calculate FCStockLevel, StoreStockLevel and DropShipStockLevel based on
    // current available inventory (DCS table's stock level)
    pFcStockLevel = pRemainingStockLevel - pStockLevel - pDropshipStockLevel;
    if (pFcStockLevel < 0) {
      pFcStockLevel = 0;
    }
    pStockLevel = pRemainingStockLevel - pFcStockLevel - pDropshipStockLevel;
    if (pStockLevel < 0) {
      pStockLevel = 0;
    }
    pDropshipStockLevel = pRemainingStockLevel - pFcStockLevel - pStockLevel;
    if (pDropshipStockLevel < 0) {
      pDropshipStockLevel = 0;
    }

    long remainingQuantity = ci.getQuantity();
    if (pFcStockLevel > 0) {
      List<ShippingMethodInfo> shippingMethodsInfoList = new ArrayList<>();
      ShippingMethodInfo shippingMethodInfoDefault = new ShippingMethodInfo();
      shippingMethodInfoDefault.setShippingMethod(ShippingGroupConstants.DEFAULT_SHIPPING_METHOD);
      shippingMethodInfoDefault.setBaseRate(
          Double.valueOf(shippingRates.getProperty(shippingMethodInfoDefault.getShippingMethod())));
      shippingMethodsInfoList.add(shippingMethodInfoDefault);
      ShippingMethodInfo shippingMethodNDA = new ShippingMethodInfo();
      shippingMethodNDA.setShippingMethod(ShippingGroupConstants.NEXT_DAY_SHIPPING_METHOD);
      shippingMethodNDA.setBaseRate(
          Double.valueOf(shippingRates.getProperty(shippingMethodNDA.getShippingMethod())));
      shippingMethodsInfoList.add(shippingMethodNDA);
      ShippingMethodInfo shippingMethodInfoSD = new ShippingMethodInfo();
      shippingMethodInfoSD.setShippingMethod(ShippingGroupConstants.SECOND_DAY_SHIPPING_METHOD);
      shippingMethodInfoSD.setBaseRate(
          Double.valueOf(shippingRates.getProperty(shippingMethodInfoSD.getShippingMethod())));
      shippingMethodsInfoList.add(shippingMethodInfoSD);

      CommerceItemInfo commerceItemInfo = new CommerceItemInfo();

      commerceItemInfo.setId(ci.getCatalogRefId());
      commerceItemInfo.setSeqNo(1);
      commerceItemInfo.setProductTitle(this.getProductDisplayName(ci));
      this.updateCommerceItemInfo(commerceItemInfo,ci);
      if (pFcStockLevel >= remainingQuantity) {
        long availableQuantity = remainingQuantity;
        remainingQuantity = 0;
        commerceItemInfo.setQuantity(availableQuantity);
      } else {
        long availableQuantity = pFcStockLevel;
        remainingQuantity = remainingQuantity - availableQuantity;
        commerceItemInfo.setQuantity(availableQuantity);
      }
      map.put(commerceItemInfo, shippingMethodsInfoList);
    }

    if (pStockLevel > 0 && remainingQuantity > 0) {
      List<ShippingMethodInfo> shippingMethodsInfoList = new ArrayList<>();
      ShippingMethodInfo shippingMethodInfoDefault = new ShippingMethodInfo();
      shippingMethodInfoDefault.setShippingMethod(ShippingGroupConstants.DEFAULT_SHIPPING_METHOD);
      shippingMethodInfoDefault.setBaseRate(
          Double.valueOf(shippingRates.getProperty(shippingMethodInfoDefault.getShippingMethod())));
      shippingMethodsInfoList.add(shippingMethodInfoDefault);
      if(((DigitalPricingTools)getPricingTools()).getPromotionTools().getDswConstants().isExpediatedShippingEnabled()){
    	  ShippingMethodInfo shippingMethodNDA = new ShippingMethodInfo();
    	  shippingMethodNDA.setShippingMethod( ShippingGroupConstants.NEXT_DAY_SHIPPING_METHOD );
    	  shippingMethodNDA.setBaseRate( Double.valueOf( shippingRates.getProperty( shippingMethodNDA.getShippingMethod() ) ).doubleValue() );
    	  shippingMethodsInfoList.add( shippingMethodNDA );
    	  ShippingMethodInfo shippingMethodInfoSD = new ShippingMethodInfo();
    	  shippingMethodInfoSD.setShippingMethod( ShippingGroupConstants.SECOND_DAY_SHIPPING_METHOD );
    	  shippingMethodInfoSD.setBaseRate( Double.valueOf( shippingRates.getProperty( shippingMethodInfoSD.getShippingMethod() ) ).doubleValue() );
    	  shippingMethodsInfoList.add( shippingMethodInfoSD );
      }

      CommerceItemInfo commerceItemInfo = new CommerceItemInfo();
      commerceItemInfo.setId(ci.getCatalogRefId());
      commerceItemInfo.setSeqNo(2);
      commerceItemInfo.setProductTitle(this.getProductDisplayName(ci));
      this.updateCommerceItemInfo(commerceItemInfo,ci);
      if (pStockLevel >= remainingQuantity) {
        long availableQuantity = remainingQuantity;
        remainingQuantity = 0;
        commerceItemInfo.setQuantity(availableQuantity);
      } else {
        long availableQuantity = pStockLevel;
        remainingQuantity = remainingQuantity - availableQuantity;
        commerceItemInfo.setQuantity(availableQuantity);
      }
      map.put(commerceItemInfo, shippingMethodsInfoList);
    }

    if (pDropshipStockLevel > 0 && remainingQuantity > 0) {
      List<ShippingMethodInfo> shippingMethodsInfoList = new ArrayList<>();
      RepositoryItem product = (RepositoryItem) ci.getAuxiliaryData().getProductRef();
      RepositoryItem productItem = getRepositoryItemForItemId(product.getRepositoryId(), "product");
      RepositoryItem skuItem = getRepositoryItemForItemId(sku.getRepositoryId(), "sku");
      String dropshipInd = null;
      Boolean dropshipExpediteRestriction = true;
      String dropshipCutoff = null;
      if (null != skuItem && null != skuItem.getPropertyValue("dropShipCapableInd")) {
        dropshipInd = (String) skuItem.getPropertyValue("dropShipCapableInd");
      }
      if (!DigitalStringUtil.isBlank(dropshipInd) && "Y".equalsIgnoreCase(dropshipInd)) {
        dropshipExpediteRestriction =
            productItem.getPropertyValue("dropshipExpediteRestriction") == null ? true
                : (Boolean) productItem.getPropertyValue("dropshipExpediteRestriction");
        dropshipCutoff = (String) productItem.getPropertyValue("dropshipExpediteCutoff");
      }
      ShippingMethodInfo shippingMethodInfoDefault = new ShippingMethodInfo();
      if (!DigitalStringUtil.isBlank(dropshipInd) && "Y".equalsIgnoreCase(dropshipInd)) {
        shippingMethodInfoDefault.setShippingMethod(ShippingGroupConstants.DEFAULT_SHIPPING_METHOD);
        shippingMethodInfoDefault.setBaseRate(
            Double
                .valueOf(shippingRates.getProperty(shippingMethodInfoDefault.getShippingMethod())));
        shippingMethodsInfoList.add(shippingMethodInfoDefault);
      }
      if (!DigitalStringUtil.isBlank(dropshipInd) && "Y".equalsIgnoreCase(dropshipInd)
          && !dropshipExpediteRestriction && checkDropShipCutoffTime(dropshipCutoff)) {
        ShippingMethodInfo shippingMethodInfoNDA = new ShippingMethodInfo();
        shippingMethodInfoNDA.setShippingMethod(ShippingGroupConstants.NEXT_DAY_SHIPPING_METHOD);
        shippingMethodInfoNDA.setBaseRate(
            Double.valueOf(shippingRates.getProperty(shippingMethodInfoNDA.getShippingMethod())));
        shippingMethodsInfoList.add(shippingMethodInfoNDA);
        ShippingMethodInfo shippingMethodInfoSD = new ShippingMethodInfo();
        shippingMethodInfoSD.setShippingMethod(ShippingGroupConstants.SECOND_DAY_SHIPPING_METHOD);
        shippingMethodInfoSD.setBaseRate(
            Double.valueOf(shippingRates.getProperty(shippingMethodInfoSD.getShippingMethod())));
        shippingMethodsInfoList.add(shippingMethodInfoSD);
      }
      CommerceItemInfo commerceItemInfo = new CommerceItemInfo();
      commerceItemInfo.setId(ci.getCatalogRefId());
      commerceItemInfo.setSeqNo(3);
      commerceItemInfo.setProductTitle(this.getProductDisplayName(ci));
      this.updateCommerceItemInfo(commerceItemInfo,ci);
      if (pDropshipStockLevel >= remainingQuantity) {
        long availableQuantity = remainingQuantity;
        commerceItemInfo.setQuantity(availableQuantity);
      } else {
        long availableQuantity = pDropshipStockLevel;
        commerceItemInfo.setQuantity(availableQuantity);
      }
      map.put(commerceItemInfo, shippingMethodsInfoList);
    }
    return map;
  }

  /**
   * @param dropshipCutoff
   * @return true or false
   */
  private Boolean checkDropShipCutoffTime(String dropshipCutoff) {
    Boolean cutoffLimit = false;
    if (DigitalStringUtil.isNotBlank(dropshipCutoff)) {
      SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      Date now = new Date();
      String strDate = sdfDate.format(now);
      int indexHr = strDate.indexOf(":");
      String hr = strDate.substring(indexHr - 2, indexHr);
      String min = strDate.substring(indexHr + 1, indexHr + 3);
      String current = hr + min;
      int tm = Integer.parseInt(current);
      if (!DigitalStringUtil.isBlank(dropshipCutoff) && (Integer.parseInt(dropshipCutoff)) > tm) {
        cutoffLimit = true;
      }
    }
    return cutoffLimit;
  }

  /**
   * Reads dropship Info for a given product id from the the Product catalog.
   *
   * @param repositoryId
   * @param itemDesc
   * @return Product Item
   */
  private RepositoryItem getRepositoryItemForItemId(String repositoryId, String itemDesc) {

    Repository repo = getProductRepository();
    RepositoryItem productItem = null;
    try {
      productItem = repo.getItem(repositoryId, itemDesc);

    } catch (RepositoryException e) {
      logError(e);
    }
    return productItem;
  }

  /**
   * @param shippingMethodRestrictions
   * @param shippingMethods
   * @param order
   * @return Map
   */
  private Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> mergeShippingRestrictionsWithInventory(
      Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> shippingMethodRestrictions,
      Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> shippingMethods, Order order) {
    Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> shippingMethodsMerged = new HashMap<>();
    Iterator<Map.Entry<ShippingMethodInfo, List<CommerceItemInfoWrapper>>> entriesRestrictions = shippingMethodRestrictions
        .entrySet().iterator();
    ShippingMethodInfo shippingMethodInfoDefault = new ShippingMethodInfo();
    ShippingMethodInfo shippingMethodInfoNDA = new ShippingMethodInfo();
    ShippingMethodInfo shippingMethodInfoSD = new ShippingMethodInfo();

    String siteId = order.getSiteId();
    Properties shippingRates = ((Properties) (MultiSiteUtil.getShippingRates(siteId)
        .get(STATIC_SHIPPING_RATES)));
    shippingMethodInfoDefault.setShippingMethod(ShippingGroupConstants.DEFAULT_SHIPPING_METHOD);
    shippingMethodInfoDefault.setBaseRate(
        Double.valueOf(shippingRates.getProperty(shippingMethodInfoDefault.getShippingMethod())));
    shippingMethodInfoNDA.setShippingMethod(ShippingGroupConstants.NEXT_DAY_SHIPPING_METHOD);
    shippingMethodInfoNDA.setBaseRate(
        Double.valueOf(shippingRates.getProperty(shippingMethodInfoDefault.getShippingMethod())));
    shippingMethodInfoSD.setShippingMethod(ShippingGroupConstants.SECOND_DAY_SHIPPING_METHOD);
    shippingMethodInfoSD.setBaseRate(
        Double.valueOf(shippingRates.getProperty(shippingMethodInfoDefault.getShippingMethod())));

    List<CommerceItemInfoWrapper> commerceItemDefaultInfoListRestrictions = new ArrayList<>();
    List<CommerceItemInfoWrapper> commerceItemNDAInfoListRestrictions = new ArrayList<>();
    List<CommerceItemInfoWrapper> commerceItemSDInfoListRestrictions = new ArrayList<>();
    while (entriesRestrictions.hasNext()) {
      Map.Entry<ShippingMethodInfo, List<CommerceItemInfoWrapper>> entry = entriesRestrictions
          .next();
      ShippingMethodInfo shippingMethodInfo = entry.getKey();
      if (ShippingGroupConstants.DEFAULT_SHIPPING_METHOD
          .equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
        commerceItemDefaultInfoListRestrictions = entry.getValue();
        shippingMethodInfoDefault.setBaseRate((shippingMethodInfo.getBaseRate() != 0.0)?
            shippingMethodInfo.getBaseRate() :
            Double.valueOf(shippingRates.getProperty(ShippingGroupConstants.DEFAULT_SHIPPING_METHOD)));
        shippingMethodInfoDefault.setShippingMethod(shippingMethodInfo.getShippingMethod());
      }
      if (ShippingGroupConstants.NEXT_DAY_SHIPPING_METHOD
          .equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
        commerceItemNDAInfoListRestrictions = entry.getValue();
        shippingMethodInfoNDA.setBaseRate((shippingMethodInfo.getBaseRate() != 0.0)?
            shippingMethodInfo.getBaseRate() :
            Double.valueOf(shippingRates.getProperty(ShippingGroupConstants.NEXT_DAY_SHIPPING_METHOD)));
        shippingMethodInfoNDA.setShippingMethod(shippingMethodInfo.getShippingMethod());
      }
      if (ShippingGroupConstants.SECOND_DAY_SHIPPING_METHOD
          .equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
        commerceItemSDInfoListRestrictions = entry.getValue();
        shippingMethodInfoSD.setBaseRate((shippingMethodInfo.getBaseRate() != 0.0)?
            shippingMethodInfo.getBaseRate() :
            Double.valueOf(shippingRates.getProperty(ShippingGroupConstants.SECOND_DAY_SHIPPING_METHOD)));
        shippingMethodInfoSD.setShippingMethod(shippingMethodInfo.getShippingMethod());
      }
    }

    Iterator<Map.Entry<ShippingMethodInfo, List<CommerceItemInfoWrapper>>> entries = shippingMethods
        .entrySet().iterator();
    List<CommerceItemInfoWrapper> commerceItemDefaultInfoList = new ArrayList<>();
    List<CommerceItemInfoWrapper> commerceItemNDAInfoList = new ArrayList<>();
    List<CommerceItemInfoWrapper> commerceItemSDInfoList = new ArrayList<>();
    while (entries.hasNext()) {
      Map.Entry<ShippingMethodInfo, List<CommerceItemInfoWrapper>> entry = entries.next();
      ShippingMethodInfo shippingMethodInfo = entry.getKey();
      if (ShippingGroupConstants.DEFAULT_SHIPPING_METHOD
          .equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
        commerceItemDefaultInfoList = entry.getValue();
      }
      if (ShippingGroupConstants.NEXT_DAY_SHIPPING_METHOD
          .equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
        commerceItemNDAInfoList = entry.getValue();
      }
      if (ShippingGroupConstants.SECOND_DAY_SHIPPING_METHOD
          .equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
        commerceItemSDInfoList = entry.getValue();
      }
    }

    commerceItemDefaultInfoList = mergeCommerceItemList(commerceItemDefaultInfoListRestrictions,
        commerceItemDefaultInfoList);
    commerceItemNDAInfoList = mergeCommerceItemList(commerceItemNDAInfoListRestrictions,
        commerceItemNDAInfoList);
    commerceItemSDInfoList = mergeCommerceItemList(commerceItemSDInfoListRestrictions,
        commerceItemSDInfoList);

    shippingMethodsMerged.put(shippingMethodInfoDefault, commerceItemDefaultInfoList);
    shippingMethodsMerged.put(shippingMethodInfoNDA, commerceItemNDAInfoList);
    shippingMethodsMerged.put(shippingMethodInfoSD, commerceItemSDInfoList);
    return shippingMethodsMerged;
  }

  /**
   * @param commerceItemInfoListRestrictions
   * @param commerceItemInfoList
   * @return List
   */
  private List<CommerceItemInfoWrapper> mergeCommerceItemList(
      List<CommerceItemInfoWrapper> commerceItemInfoListRestrictions,
      List<CommerceItemInfoWrapper> commerceItemInfoList) {
    List<CommerceItemInfoWrapper> commerceItemInfoListMerged = new ArrayList<>();
    List<String> idList = new ArrayList<>();
    List<String> idListRestrictions = new ArrayList<>();
    List<String> idListMerged = new ArrayList<>();
    for (CommerceItemInfoWrapper commerceItemInfoWrapper : commerceItemInfoList) {
      idList.add(commerceItemInfoWrapper.getId());
    }
    for (CommerceItemInfoWrapper commerceItemInfoWrapper : commerceItemInfoListRestrictions) {
      idListRestrictions.add(commerceItemInfoWrapper.getId());
    }

    for (String id : idList) {
      if (idListRestrictions.contains(id)) {
        idListMerged.add(id);
      }
    }
    for (CommerceItemInfoWrapper commerceItemInfoWrapper : commerceItemInfoList) {
      if (idListMerged.contains(commerceItemInfoWrapper.getId())) {
        commerceItemInfoListMerged.add(commerceItemInfoWrapper);
      }
    }

    return commerceItemInfoListMerged;
  }

  /**
   * @param shippingGroup
   * @return true or false
   */
  private Boolean isShippingAddressEmpty(HardgoodShippingGroup shippingGroup) {
    Boolean retVal = false;

    if (shippingGroup.getShippingAddress() == null) {
      retVal = true;
      return retVal;
    }
    if (shippingGroup.getShippingAddress() != null
        && (DigitalStringUtil.isBlank(shippingGroup.getShippingAddress().getAddress1()) || DigitalStringUtil
        .isBlank(shippingGroup.getShippingAddress().getFirstName()) || DigitalStringUtil
        .isBlank(shippingGroup.getShippingAddress().getLastName())
        || DigitalStringUtil.isBlank(shippingGroup.getShippingAddress().getCity()) || DigitalStringUtil
        .isBlank(shippingGroup.getShippingAddress().getPostalCode()))) {
      retVal = true;
    }
    return retVal;
  }

  private String getBrandName(RepositoryItem product) {
    String brandDisplayName = "";
    if(product != null) {
      RepositoryItem dswBrand=(RepositoryItem) product.getPropertyValue("dswBrand");
      if(dswBrand != null){
        brandDisplayName=(String)dswBrand.getPropertyValue("displayName");
      }
    }
    return brandDisplayName;
  }

  private String getProductId(RepositoryItem product) {
    String productId = "";
    if(product != null) {
      productId = (String) product.getPropertyValue("id");
    }
    return productId;
  }

  private String getColorCode(RepositoryItem catalog) {
    String colorCode = "";
    if(catalog != null) {
      RepositoryItem color = (RepositoryItem)catalog.getPropertyValue("color");
      if(color != null) {
        colorCode = (String)color.getPropertyValue("colorCode");
      }
    }
    return colorCode;
  }

  private void updateCommerceItemInfo(CommerceItemInfo info, CommerceItem ci) {
    RepositoryItem product = (RepositoryItem)ci.getAuxiliaryData().getProductRef();
    info.setBrand(this.getBrandName(product));
    info.setProductId(this.getProductId(product));
    info.setColorCode(this.getColorCode((RepositoryItem)ci.getAuxiliaryData().getCatalogRef()));
  }

  private void updateCommerceItemInfoWrapper(CommerceItemInfoWrapper ci, Order order) {
    String id = ci.getId();
    List<DigitalCommerceItem> ciList = order.getCommerceItems();
    for (CommerceItem item : ciList) {
      if (id.equalsIgnoreCase(item.getCatalogRefId())) {
        RepositoryItem product = (RepositoryItem) item.getAuxiliaryData().getProductRef();
        ci.setBrand(this.getBrandName(product));
        ci.setProductId(this.getProductId(product));
        ci.setColorCode(this.getColorCode((RepositoryItem)item.getAuxiliaryData().getCatalogRef()));
      }
    }
  }

}
