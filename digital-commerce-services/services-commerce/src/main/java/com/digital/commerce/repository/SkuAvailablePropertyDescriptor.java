/**
 *
 */
package com.digital.commerce.repository;


import static com.digital.commerce.common.util.ComponentLookupUtil.INVENTORY_TOOLS;

import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.inventory.InventoryHelper;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.services.inventory.InventoryTools;

import atg.commerce.inventory.InventoryManager;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;


/** @author psinha */
public class SkuAvailablePropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
     *
     */
	private static final long					serialVersionUID	= -6258342473415966032L;

	protected static final String				TYPE_NAME			= "SkuAvailablePropertyDescriptor";

	/** thread safe and don't serialize it. */
	private volatile transient InventoryHelper	inventoryTools;

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, SkuAvailablePropertyDescriptor.class );
	}

	/** See <a href="http://www.cs.umd.edu/~pugh/java/memoryModel/DoubleCheckedLocking.html">The "Double-Checked Locking is Broken"
	 * Declaration </a>
	 * for a description of the double checked locking problem and solutions.
	 * 
	 * @return */
	public InventoryHelper getInventoryTools() {
		InventoryHelper retVal = this.inventoryTools;
		if( retVal == null ) {
			synchronized( this ) {
				retVal = this.inventoryTools;
				if( retVal == null ) {

					retVal = this.inventoryTools = ComponentLookupUtil.lookupComponent( INVENTORY_TOOLS, InventoryTools.class );

				}
			}
		}

		return retVal;
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {

		// if value already cached return fron the cache
		if( pValue != null && pValue instanceof Boolean ) { return pValue; }

		Boolean available = Boolean.TRUE;
		InventoryHelper inventoryTools = getInventoryTools();
		if( inventoryTools != null ) {
			int statusCode = inventoryTools.getInventoryStatus( (String)pItem.getPropertyValue( "id" ), MultiSiteUtil.getWebsiteLocationId() );
			available = (InventoryManager.AVAILABILITY_STATUS_IN_STOCK == statusCode) || (InventoryManager.AVAILABILITY_STATUS_OUT_OF_STOCK == statusCode);
			setIsAvailableProperty( pItem, available );
		}
		return available;
	}

	private void setIsAvailableProperty( RepositoryItemImpl item, Boolean flag ) {
		item.setPropertyValueInCache( this, flag );
	}
}
