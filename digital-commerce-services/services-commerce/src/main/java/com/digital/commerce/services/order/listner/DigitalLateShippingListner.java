package com.digital.commerce.services.order.listner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.order.processor.domain.payload.ShipmentSLAChangePayload;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderlineAttributesDTO;

public class DigitalLateShippingListner extends DigitalOrderStatusListener {
	protected static final String CLASSNAME = DigitalLateShippingListner.class.getName();

	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

	@Override
	protected String getOrderNo(OrderStatusPayload payLoad) {
		if(payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null ){
			return payLoad.getOmniOrderUpdate().getOrder().getOrderNo();
		}
		return null;
	}

	@Override
	protected String getEmailTo(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((ShipmentSLAChangePayload)payLoad).getCustomerEmailId();
	}

	@Override
	protected List<OrderLine> getOrderLines(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((ShipmentSLAChangePayload)payLoad).getOrderLines();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected HashMap<String, Object> handleEmailNotification(
			OrderStatusPayload payLoad,
			DigitalOrderImpl order,
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {
		// TODO Auto-generated method stub
		HashMap<String, Object> extraAttributes = new HashMap<>();
		
		ShipmentSLAChangePayload shipmentSLAChangePayload = (ShipmentSLAChangePayload)payLoad;
	    
	    extraAttributes.put("pickupPerson",shipmentSLAChangePayload.getPersonInfoShipTo().getFirstName()+
				" "+shipmentSLAChangePayload.getPersonInfoShipTo().getLastName());
	    String customerFirstName = shipmentSLAChangePayload.getCustomerFirstName();
		if (DigitalStringUtil.isEmpty(customerFirstName)) {
			if (DigitalStringUtil.isEmpty(customerFirstName)) {
				customerFirstName = shipmentSLAChangePayload.getPersonInfoShipTo().getFirstName();
				if (DigitalStringUtil.isEmpty(customerFirstName)) {
					customerFirstName = shipmentSLAChangePayload.getPersonInfoMarkFor().getFirstName();
				}
			}
		}
		
	    extraAttributes.put("customerFirstName",customerFirstName);
	    //get Shipping Address Details
	    extraAttributes.put("company",shipmentSLAChangePayload.getPersonInfoShipTo().getCompany());
	    extraAttributes.put("addressLine1",shipmentSLAChangePayload.getPersonInfoShipTo().getAddressLine1());
	    extraAttributes.put("addressLine2",shipmentSLAChangePayload.getPersonInfoShipTo().getAddressLine2());
	    extraAttributes.put("city",shipmentSLAChangePayload.getPersonInfoShipTo().getCity());
	    extraAttributes.put("state",shipmentSLAChangePayload.getPersonInfoShipTo().getState());
	    extraAttributes.put("zipCode",shipmentSLAChangePayload.getPersonInfoShipTo().getZipCode());
	    extraAttributes.put("country",shipmentSLAChangePayload.getPersonInfoShipTo().getCountry());
	    extraAttributes.put("dayPhone",shipmentSLAChangePayload.getPersonInfoShipTo().getDayPhone());
	    
	    extraAttributes.put("emailId",shipmentSLAChangePayload.getPersonInfoShipTo().getFirstName());
		
	    consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.SHIP, itemsByFullfilmentType, extraAttributes);
	    consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOPIS, itemsByFullfilmentType, extraAttributes);
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOSTS, itemsByFullfilmentType, extraAttributes);
		
		//get mall plaza name
		HashMap<String, OrderlineAttributesDTO> lines = (HashMap<String, OrderlineAttributesDTO>)extraAttributes.get("orderLines");
		
		if(lines != null){
			Map.Entry<String,OrderlineAttributesDTO> entry=lines.entrySet().iterator().next();
			extraAttributes.put("mallPlazaName",entry.getValue().getMallPlazaName());
		}
		return extraAttributes;
	}
}
