package com.digital.commerce.services.profile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.dtm.UserTransactionDemarcation;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.time.DateUtils;

import atg.commerce.CommerceException;
import atg.commerce.order.OrderHolder;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.collections.filter.CachedCollectionFilter;
import atg.service.collections.filter.FilterException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.gifts.DigitalGiftlistManager;
import com.digital.commerce.services.order.DigitalOrderHistoryModel;
import com.digital.commerce.services.order.DigitalOrderLookupService;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.region.RegionProfile;
import com.digital.commerce.services.rewards.domain.RewardsRetrieveRewardCertificatesResponse;
import com.digital.commerce.services.storelocator.DigitalStoreLocatorModel;
@Getter
@Setter
public class DigitalUserInfoDroplet extends DynamoServlet {
	private static final String CLASSNAME = DigitalUserInfoDroplet.class.getName();

	private static final String PERFORM_MONITOR_NAME    		= "DigitalUserInfoDroplet";
	private static final String CDN_CLIENT_SOURCE_IP_HEADER 	= "Client-Source-IP";
	private static final String LOAD_BALANCER_SOURCE_IP_HEADER 	= "SOURCE_IP";
	private static final String XFORWARDED_FOR_HEADER 			= "X-Forwarded-For";


	private DigitalProfileTools 						profileTools;
	private OrderHolder 							shoppingCart;
	private Profile 								profile;
	private RegionProfile 							regionProfile;
	private DigitalRewardsManager 						dswRewardsManager;
	private DigitalGiftlistManager 						giftlistManager;
	private DigitalOrderLookupService 					lookupService;
	private CachedCollectionFilter 					filter;
	private DigitalBaseConstants 						constants;
	private RepeatingRequestMonitor 				repeatingRequestMonitor;
	private boolean hideOrders;

	private boolean skipCerts			=true;
	private boolean skipOrders			=false;
	private boolean skipFavStore		=false;
	private boolean skipRewardDetails	=true;
	private boolean syncReward			=true;

	private int defaultDashboardDays = 14;
	private Double latitude = null;
	private Double longitude = null;
	private String[] ipAddresses;

	void initializeService(DynamoHttpServletRequest pRequest){
		try{
			String syncRewardStr = pRequest.getParameter("syncReward");
			if(DigitalStringUtil.isNotBlank(syncRewardStr)) {
				this.syncReward = Boolean.parseBoolean(syncRewardStr);
			}
		}catch(Exception ex){
			logError("Parse Error", ex);
		}

		try{
			String skipCertsStr = pRequest.getParameter("skipCerts");
			if(DigitalStringUtil.isNotBlank(skipCertsStr)) {
				this.skipCerts = Boolean.parseBoolean(skipCertsStr);
			}
		}catch(Exception ex){
			logError("Parse Error", ex);
		}

		try {
			String skipOrdersStr = pRequest.getParameter("skipOrders");
			if(DigitalStringUtil.isNotBlank(skipOrdersStr)) {
				this.skipOrders = Boolean.parseBoolean(skipOrdersStr);
			}
		}catch(Exception ex){
			logError("Parse Error", ex);
		}

		try {
			String skipFavStoreStr = pRequest.getParameter("skipFavStore");
			if(DigitalStringUtil.isNotBlank(skipFavStoreStr)) {
				this.skipFavStore = Boolean.parseBoolean(skipFavStoreStr);
			}
		}catch(Exception ex){
			logError("Parse Error", ex);
		}

		try{
			String skipRewardDetailsStr = pRequest.getParameter("skipRewardDetails");
			if(DigitalStringUtil.isNotBlank(skipRewardDetailsStr)) {
				this.skipRewardDetails = Boolean.parseBoolean(skipRewardDetailsStr);
			}
		}catch(Exception ex){
			logError("Parse Error", ex);
		}

		try {
			String latitude = pRequest.getParameter("latitude");
			this.latitude = ( DigitalStringUtil.isNotBlank(latitude)) ? Double.parseDouble(latitude) : 0.0;
		}catch(Exception ex){
			logError("Parse Error", ex);
			this.latitude=0.0;
		}
		try {
			String longitude = pRequest.getParameter("longitude");
			this.longitude = ( DigitalStringUtil.isNotBlank(longitude) ? Double.parseDouble(longitude) : 0.0);
		}catch(Exception ex){
			logError("Parse Error", ex);
			this.longitude=0.0;
		}

		if (isLoggingDebug()) {
			logDebug("skipCerts: " + skipCerts+
					" skipOrders: " + skipOrders+
					" skipFavStore: " + skipFavStore+
					" latitude: " + latitude+
					" longitude: " + longitude
			);
		}
	}

	@SuppressWarnings("unchecked")
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		
		String METHOD_NAME = "service";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			UserTransactionDemarcation td = null;
			try{
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

				initializeService(pRequest);

				int profileSecurityStatus = 0;


				Map<String,Object> dswUserInfoObj = new HashMap<>();
				dswUserInfoObj.put("anonymousUser",getProfileTools().isDSWAnanymousUser(getProfile()));
				dswUserInfoObj.put("isDSWInStoreDevice",doesIPAddressMatch(pRequest));

				if (getProfile().getPropertyValue("securityStatus") != null) {
					profileSecurityStatus = (Integer) getProfile().getPropertyValue("securityStatus");
				}
				if (profileSecurityStatus==2)
				{
					dswUserInfoObj.put("cookiedUser",true);
				}
				else {
					dswUserInfoObj.put("cookiedUser",false);
				}

				dswUserInfoObj.put("totalBagCount",getShoppingCart().getCurrent().getTotalCommerceItemCount());
				dswUserInfoObj.put("currentOrderId",getShoppingCart().getCurrent().getId());
				dswUserInfoObj.put("profileId",(String)getProfile().getPropertyValue("id"));
				dswUserInfoObj.put("login",(String)getProfile().getPropertyValue("login"));
				if (!skipFavStore){

					DigitalStoreLocatorModel favoriteStoreDetails= getProfileTools().getUserFavoriteStore(getProfile(), latitude, longitude, getRegionProfile());
					dswUserInfoObj.put("favoriteStoreDetails",favoriteStoreDetails);
				}
				dswUserInfoObj.put("dswVisaMember",getProfileTools().getCCFlag(getProfile()));
				dswUserInfoObj.put("userStatus",(getProfileTools().getUserStatus(getProfile())));

				if (profileSecurityStatus==0){
					HashMap<String, Long> wishlistCountMap = new HashMap<>();
					wishlistCountMap.put("wishListCount", 0L);
					dswUserInfoObj.put("wishList",wishlistCountMap);
				}
        String loyaltyTier = getProfileTools().getLoyaltyTier(getProfile());
        if(DigitalStringUtil.isNotBlank(loyaltyTier)) {
          dswUserInfoObj.put("loyaltyTier",loyaltyTier);
        } else  {
          dswUserInfoObj.put("loyaltyTier","GUEST");
        }

        if (profileSecurityStatus==2  || profileSecurityStatus==4 || profileSecurityStatus==5){
					dswUserInfoObj.put("firstName",(String)getProfile().getPropertyValue("firstName"));

					//cookied user donot return lastname
					if(profileSecurityStatus !=2) {
						dswUserInfoObj.put("lastName", (String) getProfile().getPropertyValue("lastName"));
					}
					dswUserInfoObj.put("email",(String)getProfile().getPropertyValue("email"));

					String loyaltyNumber = getProfileTools().getLoyaltyId(getProfile());
					dswUserInfoObj.put("loyaltyNumber",loyaltyNumber);
					if (loyaltyNumber!=null && !("").equals(loyaltyNumber) && !skipCerts){
						RewardsRetrieveRewardCertificatesResponse rewardCertResponse = getDswRewardsManager().retrieveRewardCertificatesByProfile(getProfile());
						HashMap<String, HashMap<String,Double>> rewardsMap = new HashMap<>();
						HashMap<String,Double> totalCertvalue = new HashMap<>();
						totalCertvalue.put("totalCertvalue",rewardCertResponse.getTotalCertValue());
						rewardsMap.put("rewardsCertificates",totalCertvalue );
						dswUserInfoObj.put("rewardsAmount",rewardsMap);
					}

					if (!skipOrders && !hideOrders) {
						int totalOrderCount = 0;
						Date endDT = new Date();
						List<DigitalOrderHistoryModel> filterOrders;
						Date startDT = DateUtils.addDays(endDT, defaultDashboardDays * -1);

						List<DigitalOrderHistoryModel> orders = this.getLookupService().getOrdersFromDSWOrderHistoryForDashboard(
									getProfile().getRepositoryId(), loyaltyNumber, startDT, endDT, 0,1);
						filterOrders = (List<DigitalOrderHistoryModel>) getFilter().filterCollection(orders, null, null);

						if (filterOrders != null) {
							Collections.sort(filterOrders, new Comparator<DigitalOrderHistoryModel>() {
								public int compare(DigitalOrderHistoryModel o1, DigitalOrderHistoryModel o2) {
									return (-1) * o1.getOrderPlacementDate().compareTo(o2.getOrderPlacementDate());
								}
							});
							totalOrderCount = filterOrders.size();
							dswUserInfoObj.put("myOrders",filterOrders);
							dswUserInfoObj.put("totalOrderCount",totalOrderCount);
						}
					}

					RepositoryItem wishlist =(RepositoryItem) getProfile().getPropertyValue("wishlist");
					Long wishlistCount =  0L;
					if (wishlist!=null) {
						wishlistCount = getGiftlistManager().getWishlistCount((String)wishlist.getPropertyValue("id"));
					}
					HashMap<String, Long> wishlistCountMap = new HashMap<>();
					wishlistCountMap.put("wishListCount", wishlistCount);
					dswUserInfoObj.put("wishList",wishlistCountMap);

					if(!skipRewardDetails){
						Map<String, Object> rewardDetails = getProfileTools().getRewardsSummary(getProfile(), syncReward);
						dswUserInfoObj.put("rewardDetails", rewardDetails);
						dswUserInfoObj.put("loyaltyTier",getProfileTools().getLoyaltyTier(getProfile()));
						loyaltyNumber = getProfileTools().getLoyaltyId(getProfile());
						dswUserInfoObj.put("loyaltyNumber",loyaltyNumber);
					}
				}

				if (isLoggingDebug()) {
					logDebug("dswUserInfo: " + toString(dswUserInfoObj));
				}

				pRequest.setParameter("dswUserInfo", dswUserInfoObj);
				pRequest.serviceLocalParameter("output", pRequest, pResponse);


			} catch (RepositoryException | FilterException | CommerceException e) {
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);

				String msg = "Unable to fetch user info details";
				if (isLoggingError()) {
					logError(e.getMessage());
				}
				pRequest.setParameter("errorMsg", msg);
				pRequest.serviceLocalParameter("error", pRequest, pResponse);
			}

			finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			}
		}else{
			String msg = "Repeating Request Error ";
			if (isLoggingError()) {
				logError(msg);
			}
			pRequest.setParameter("errorMsg", msg);
			pRequest.serviceLocalParameter("error", pRequest, pResponse);
		}
	}

	
	/**
	 * Overrides the default service method and returns back a boolean named
	 * isDSWInStoreDevice in the request. IP Addresses are configured through
	 * 
	 * @link{DigitalConstants .
	 * 
	 *                    Because there are several tiers to our application,
	 *                    Source IP is a little muddy. The code below attempts
	 *                    to get the Source IP first from the header provided by
	 *                    the CDN. If it is unavailable it falls back to the
	 *                    load balancer which should work for origin requests.
	 *                    Finally it will fall back to pulling the IP address
	 *                    directly from the request which should cover
	 *                    development and local environments.
	 * 
	 * @param req
	 */

	private boolean doesIPAddressMatch(DynamoHttpServletRequest req) {
		Boolean doesIPAddressMatch = false;
		try {		
			List<String> requestIPAddress = new ArrayList<>();
			String clientIp = req.getHeader(CDN_CLIENT_SOURCE_IP_HEADER);
			if (isLoggingDebug()) {
				logDebug("Client-Source-IP: " + clientIp);
			}
			if (null != clientIp) {
				requestIPAddress.add(clientIp);
				if (isLoggingDebug()) {
					logDebug("Client-Source-IP found, hence check will be done against this");
				}
			}

			if (requestIPAddress.isEmpty()) {
				clientIp = req.getHeader(LOAD_BALANCER_SOURCE_IP_HEADER);
				if (isLoggingDebug()) {
					logDebug("SOURCE_IP: " + clientIp);
				}
				if (null != clientIp) {
					String[] sourceIpList = clientIp.split(",");
					if (isLoggingDebug()) {
						logDebug("SOURCE_IP found, hence check will be done against this");
					}
					requestIPAddress.addAll(Arrays.asList(sourceIpList));
				}
			}
			if (requestIPAddress.isEmpty()) {
				clientIp = req.getHeader(XFORWARDED_FOR_HEADER);
				if (isLoggingDebug()) {
					logDebug("X-Forwarded-For: " + clientIp);
				}
				if (null != clientIp) {
					String[] xHeaders = clientIp.split(",");
					if (isLoggingDebug()) {
						logDebug("X-Forwarded-For found, hence check will be done against this");
					}
					requestIPAddress.addAll(Arrays.asList(xHeaders));
				}
			}

			// loop through each ip address
			if (ipAddresses != null && (ipAddresses.length > 0)
					&& !requestIPAddress.isEmpty()) {
				for (String ipAddress : ipAddresses) {
					if (requestIPAddress.contains(ipAddress)) {
						doesIPAddressMatch = true;
						break;
					}
				}
			}

		
		} catch (Exception ex) {
			logWarning(
					"Error while determing the device as DSW Store device, doing nothing",
					ex);
		}
		return doesIPAddressMatch;
	}

	public void setConstants(DigitalBaseConstants constants) {
		this.constants = constants;
		if (this.constants != null) {
			this.ipAddresses = this.constants.getDswInStoreDeviceIPAddresses()
					.split(",");
			this.hideOrders = this.constants.isHideOrders();
		}
	}

	public String toString(Map<String,Object> dswUserInfoObj) {
    	ToStringBuilder sb = new ToStringBuilder("dswUserInfoObj Value:");
    	for (String name: dswUserInfoObj.keySet()){
            String key =name.toString();
            String value = dswUserInfoObj.get(name).toString();
            sb.append(key+": " +value + " \n");
           
    	}
    	return sb.toString();
	}
}
