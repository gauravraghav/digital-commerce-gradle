package com.digital.commerce.services.digitalvisa;

import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.DigitalProfileConstants;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdsManager extends ApplicationLoggingImpl {
	public AdsManager() {
		super(AdsManager.class.getName());
	}

	private Profile profile;
	private MessageLocator messageLocator;
	private AdsAuthCodeCalculator adsAuthCodeCalculator;
	private DigitalServiceConstants digitalConstants;

	/**
	 * @return
	 * @throws DigitalAppException
	 *             Capture member data from current session and calculate the
	 *             hash needed for DSW VISA application per Ads needs
	 */
	public AdsAuthRequest getAdsPostData() throws DigitalAppException {
		String userId = null;
		try {
			// Read the DPS_ID for given profile
			userId = (String) profile
					.getPropertyValue(DigitalProfileConstants.PROFILE_ID);
			if (isLoggingDebug()) {
				logDebug("Attempting to getAdsPostData for userId: " + userId);
			}
			String memberNumber = (String) profile
					.getPropertyValue(DigitalProfileConstants.PROFILE_LOYALTYNUM);
			// DSW VISA can be bought by ONLY memebers with Rewards #
			if (DigitalStringUtil.isBlank(memberNumber)) {
				throw new DigitalAppException(
						AdsConstants.ADS_AUTH_NOT_A_LOYALTY__NUMBER,
						messageLocator.getMessageString(
								AdsConstants.ADS_AUTH_NOT_A_LOYALTY__NUMBER,
								userId), null);
			}
			// Set the profile data into an object
			AdsAuthRequest adsAuthRequest = new AdsAuthRequest();
			adsAuthRequest.setMemberNumber(memberNumber);
			adsAuthRequest.setEmail((String) profile.getPropertyValue("login"));
			adsAuthRequest.setFirstName((String) profile
					.getPropertyValue(DigitalProfileConstants.PROFILE_FIRSTNAME));
			adsAuthRequest.setLastName((String) profile
					.getPropertyValue(DigitalProfileConstants.PROFILE_LASTNAME));
			adsAuthRequest.setApplyFormUrl(digitalConstants.getAdsApplyUrl());
			adsAuthRequest.setManageUrl(digitalConstants.getAdsManageUrl());
			Object ccFlag = profile.getPropertyValue("ccFlag");
			if (null != ccFlag) {
				adsAuthRequest.setCardHolder((Boolean) ccFlag);
			} else {
				adsAuthRequest.setCardHolder(false);

			}
			Object mobileObj = profile.getPropertyValue("mobilePhoneNumber");
			if(null != mobileObj) {
				adsAuthRequest.setPhoneNumber((String) mobileObj);
			}
			// check for home address which is optional with SR
			Object objhomeAddress = profile
					.getPropertyValue(DigitalProfileConstants.PROFILE_HOMEADDR);
			if (null != objhomeAddress) {
				RepositoryItem homeAddress = (RepositoryItem) objhomeAddress;
				Object addressLine1 = homeAddress.getPropertyValue("address1");
				if (null != addressLine1) {
					adsAuthRequest.setAddress((String) addressLine1);
				}
				Object cityObj = homeAddress.getPropertyValue("city");
				if (null != cityObj) {
					adsAuthRequest.setCity((String) cityObj);
				}
				Object stateObj = homeAddress.getPropertyValue("state");
				if (null != stateObj) {
					adsAuthRequest.setState((String) stateObj);
				}
				Object postalCodeObj = homeAddress.getPropertyValue("postalCode");
				if (null != postalCodeObj) {
					adsAuthRequest.setZip((String) postalCodeObj);
				}
			}
			// Invoke adsAuthCodeCalculator to hash the customer data
			adsAuthRequest.setAuthCode(adsAuthCodeCalculator
					.calculate(adsAuthRequest));
			return adsAuthRequest;
		} catch (DigitalAppException e) {
			throw e;
		}
	}
}
