/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import com.digital.commerce.integration.reward.domain.RewardServiceResponse;
import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RewardsReserveLoyaltyCertificateResponse  extends ResponseWrapper {
	private RewardServiceResponse rewardServiceResponse;
}
