/**
*
*/
package com.digital.commerce.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.ServletUtil;

/** @author srallabhandi */
public class CategoryListPropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
	 *
	 */
	private static final long		serialVersionUID	= -6358342475445966132L;

	protected static final String	TYPE_NAME			= "CategoryListPropertyDescriptor";

	private static final DigitalLogger		logger				= DigitalLogger.getLogger( CategoryListPropertyDescriptor.class );

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, CategoryListPropertyDescriptor.class );
	}

	private static void getChildren( RepositoryItem parent, List<String> categoryList, Set<RepositoryItem> visited, int level, String hierarchyName ) {
		try {

			if( parent != null ) {
				String catId = (String)parent.getPropertyValue( "id" );
				String catName = (String)parent.getPropertyValue( "displayName" );
				Boolean isExpanded = (Boolean)parent.getPropertyValue( "isExpanded" );
				Boolean isClickable = (Boolean)parent.getPropertyValue( "isClickable" );

				StringBuilder childCatIDs = new StringBuilder();

				StringBuilder categoryValues = new StringBuilder();

				categoryValues.append( level );
				categoryValues.append( "|" );

				categoryValues.append( catName );
				categoryValues.append( "|" );

				categoryValues.append( isExpanded );
				categoryValues.append( "|" );

				categoryValues.append( isClickable );
				categoryValues.append( "|" );

				if( !hierarchyName.equalsIgnoreCase( "" ) ) {
					hierarchyName = hierarchyName + "+" + catName;
				} else {
					hierarchyName = catName;
				}

				categoryValues.append( hierarchyName );
				categoryValues.append( "|" );

				categoryValues.append( catId );

				categoryList.add( categoryValues.toString() );

				@SuppressWarnings("unchecked")
				final Collection<RepositoryItem> childCategories = (Collection<RepositoryItem>)parent.getPropertyValue( "childCategories" );

				if( childCategories != null ) {

					Iterator<RepositoryItem> i = childCategories.iterator();
					while( i.hasNext() ) {
						RepositoryItem child = i.next();
						if( !visited.contains( child ) ) {
							visited.add( child );
							if( isParent( parent, child ) || isParent( (RepositoryItem)parent.getPropertyValue( "aliasOf" ), child ) ) {
								if( !childCatIDs.toString().equalsIgnoreCase( "" ) ) {
									childCatIDs.append( "," );
								}

								childCatIDs.append( (String)child.getPropertyValue( "id" ) );

								getChildren( child, categoryList, visited, level + 1, hierarchyName );
							}
							visited.remove( child );
						}
					}
				}
			}
		} catch( Exception e ) {
			logger.error( "CategoryListPropertyDescriptor exception in getChildren function: " + e );
		}

	}

	/** Returns true if parent is a parent of child
	 * 
	 * @param parent
	 * @param child
	 * @return */
	public static boolean isParent( RepositoryItem parent, RepositoryItem item ) {
		RepositoryItem p = (RepositoryItem)item.getPropertyValue( "parentCategory" );
		if( p != null && p.equals( parent ) ) { return true; }

		@SuppressWarnings("unchecked")
		final Collection<RepositoryItem> parentCategories = (Collection<RepositoryItem>)item.getPropertyValue( "parentCategories" );
		Iterator<RepositoryItem> i = parentCategories.iterator();
		while( i.hasNext() ) {
			p = (RepositoryItem)i.next();
			if( p.equals( parent ) ) { return true; }
		}
		return false;
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		Boolean isRoot = (Boolean)pItem.getPropertyValue( "root" );

		if( isRoot ) {
			if( pValue != null && pValue instanceof List ) {
				logger.debug( "Inside CategoryListPropertyDescriptor 1: CACHED" );
				return pValue;
			}

			logger.debug( "Inside CategoryListPropertyDescriptor 2: " );

			String rootCatId = (String)pItem.getPropertyValue( "id" );

			List<String> categoryList = new ArrayList<>();

			RepositoryItem item = null;
			try {

				item = getCategoryRepositoryItemById( rootCatId );

				if( item != null ) {
					int level = 0;

					getChildren( item, categoryList, new HashSet<RepositoryItem>(), level, "" );
					setCategoryListProperty( pItem, categoryList );
					return categoryList;
				}

			} catch( Exception e ) {
				logger.error( "CategoryListPropertyDescriptor exception: " + e );
			}

			return categoryList;
		} else
			return null;

	}

	private void setCategoryListProperty( RepositoryItemImpl item, Object categoryList ) {
		item.setPropertyValueInCache( this, categoryList );
	}

	private RepositoryItem getCategoryRepositoryItemById( String rootCatId ) throws RepositoryException {

		MutableRepository mr = (MutableRepository)ServletUtil.getCurrentRequest().resolveName( "/atg/commerce/catalog/ProductCatalog" );

		RepositoryView catRep = mr.getView( "category" );

		if( catRep == null ) return null;

		RqlStatement statement = RqlStatement.parseRqlStatement( "id = \"" + rootCatId + "\"" );
		RepositoryItem[] categories = statement.executeQuery( catRep, null );

		if( categories == null || categories.length <= 0 ) {
			logger.debug( "No Categories found" );
			return null;
		}
		RepositoryItem item = (RepositoryItem)categories[0];
		logger.debug( "Found Category :  " + item.getRepositoryId() + "/" + item.getItemDisplayName() );

		return item;
	}

}
