package com.digital.commerce.services.order.payment.afterpay.valueobject;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class AfterPayCreateOrderRequest implements Serializable {

    private static final long serialVersionUID = 8465118424746448145L;
    private String confirmURL;
    private String cancelURL;

}
