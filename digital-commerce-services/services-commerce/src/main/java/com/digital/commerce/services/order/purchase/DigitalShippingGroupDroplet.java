package com.digital.commerce.services.order.purchase;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.purchase.ShippingGroupDroplet;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

/**
 * Extension for HardGood Shipping
 * 
 * @author ad402865
 */
public class DigitalShippingGroupDroplet extends ShippingGroupDroplet {

	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		pRequest.setParameter("defaultShippingGroupName", getShippingGroupMapContainer()
				.getDefaultShippingGroupName());
				super.service(pRequest, pResponse);
	}
}
