package com.digital.commerce.services.catalog;

import com.digital.commerce.services.catalog.DigitalCatalogTools;
import com.digital.commerce.services.profile.DigitalProfileTools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.domain.ResponseWrapper;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;

import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.commerce.catalog.custom.CatalogItemLookupDroplet;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Prabu Ramaraj
 * Used by PDP service to get Product details
 *
 */
@Getter
@Setter
public class DigitalProductLookupDroplet extends CatalogItemLookupDroplet {
	
	private 	final DigitalLogger		logger						= DigitalLogger.getLogger( getClass().getName() );
	private 	final String		IDPARAM						= "id";
	protected MessageLocator messageLocator;
	private DigitalCatalogTools dswCatalogTools;
	private DigitalProfileTools profileTools;
	
	
	 /**
	  * Override OOB service method to transform the output to Map of Maps.
	  */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
		final String METHOD_NAME = "service";
		String productId = null;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
			productId = pRequest.getParameter("productId");
			//If ID is not passed then don't do anything
			if (pRequest.getParameter(IDPARAM) != null) {
			
				//Call the OOB service method to get Product Repository Item
				super.service(pRequest, pResponse);
				
				//Get the Product Repository from the OOB service call above
				Object objproduct = pRequest.getLocalParameter("element");
				RepositoryItem product;
				if (objproduct != null && objproduct instanceof RepositoryItem) {
						product = (RepositoryItem)objproduct;
						//Check if the Product belongs to the Site
						 Collection sites = getSites(pRequest);			
						 Boolean foundSite = true;
						 if (!isItemInSites(product, sites)) {
							//Service output parameter
							 if(logger.isDebugEnabled()) {
								 logger.debug( "Product does not belong to the Site");
							 }
							 pRequest.setParameter("element", getResponsErrorWrapper("PRODUCT_NOT_IN_CURRENT_SITE",productId));
							 pRequest.serviceLocalParameter("error", pRequest, pResponse);
							 foundSite = false;
						 }
						 
						 //Check if the Product belongs to the Catalog
						 Collection catalogs = getCatalogs(pRequest);	
						 Object catalogParam = pRequest.getObjectParameter("catalog");
						 if (foundSite) {
						  if (isLoggingDebug()) {
						   logDebug("Filter by catalog");
						  }

						  if ((catalogs == null) || (catalogs.isEmpty())) {
						   RepositoryItem repItem = null;

						   if (catalogParam instanceof RepositoryItem) {
						    repItem = (RepositoryItem) catalogParam;
						   } else {
						    if ((catalogParam != null) && (isLoggingDebug())) {
						     logDebug("The catalog parameter must be a repository item.");
						    }

						    if (getProfile() != null) {
						     repItem = (RepositoryItem) getProfile().getPropertyValue("catalog");
						    }
						   }
						   if (repItem != null) {
						    catalogs = new ArrayList();
						    catalogs.add(repItem);
						   }
						  }
						  if (!isItemInCatalogs(product, catalogs)) {
							//Service output parameter
							logger.error( "Product does not belong to the Catalog");
							pRequest.setParameter("element", getResponsErrorWrapper("PRODUCT_NOT_IN_CURRENT_CATALOG",productId));
							pRequest.serviceLocalParameter("error", pRequest, pResponse);
						  }
						 }				 
							 				
						//Get locale, giftlist ID and isAnonymousUser from Profile
						Object locale = null;
						Boolean isAnonymousUser;
						String giftListID = null;
						
						//Locale
						Object objpriceList = getProfile().getPropertyValue("priceList");
						if (objpriceList != null && objpriceList instanceof RepositoryItem) {
							RepositoryItem priceList = (RepositoryItem) objpriceList;
							Object objLocale = priceList.getPropertyValue("locale");
							if (objLocale != null)
								locale = objLocale;			
						}
						
						//isNonymousUser
						isAnonymousUser = getProfileTools().isDSWAnanymousUser(getProfile());
						
						//GiftlistID				
						if (pRequest.getParameter("giftlistId") == null && !isAnonymousUser) {
							Object objWishlist = getProfile().getPropertyValue("wishlist");
							if (objWishlist != null && objWishlist instanceof RepositoryItem) {
								RepositoryItem wishList = (RepositoryItem) objWishlist;
								Object objwID = wishList.getPropertyValue("id");
								if (objwID != null && objwID instanceof String)							
									giftListID = objwID.toString();
							}
						}
						else if(!isAnonymousUser) {
							giftListID = pRequest.getParameter("giftlistId").toString();
						}
						
						//Let's transform the RepositoryItem to Map of Maps for PDP page
						Map<String, Object> output = new HashMap<>();
						output = getDswCatalogTools().transformOutputforPDPpage(product, locale, isAnonymousUser, giftListID);
						
						//Service output parameter
						pRequest.setParameter("element", output);
						pRequest.serviceLocalParameter("output", pRequest, pResponse);
				}
				else {					
					logger.error( "Product does not exist");
					pRequest.setParameter("element", getResponsErrorWrapper("INVALID_PRODUCT_ID",productId));
					pRequest.serviceLocalParameter("error", pRequest, pResponse);					
				}
				
			} else {
				pRequest.setParameter("element", getResponsErrorWrapper("INVALID_PRODUCT_ID",productId));
				pRequest.serviceLocalParameter("error", pRequest, pResponse);
			}
			
			
		} catch (Exception e) {
			logger.error( "Exception thrown in DigitalProductLookupDroplet.service() method", e );
			pRequest.setParameter("element", getResponsErrorWrapper("INVALID_PRODUCT_ID",productId));
			pRequest.serviceLocalParameter("error", pRequest, pResponse);
			throw new IOException(e);
		}
		finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}
		
		
		
	}
	
	/**
	 * @param errorCode
	 * @return
	 */
	private ResponseWrapper getResponsErrorWrapper(String errorCode, String productId) {
		ResponseWrapper error = new ResponseWrapper();
		String errMsg = this.getMessageLocator().getMessageString(errorCode,productId);
		ResponseError genError = new ResponseError(errorCode, errMsg);
		error.getGenericExceptions().add(genError);
		error.setFormError(false);
		error.setRequestSuccess(false);
		return error;
	}

}
