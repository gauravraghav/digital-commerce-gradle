/* 
 *  */
package com.digital.commerce.services.order;

import java.util.List;

import com.digital.commerce.common.util.DigitalCollectionUtil;

import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import lombok.Getter;
import lombok.Setter;

/**  */
@Getter
@Setter
public class DigitalOrderPriceInfo extends OrderPriceInfo {

	private static final long	serialVersionUID	= -5537392707682021881L;
	
	private double				qualifiedAmount;

	private double				unqualifiedAmount;

	public double getCumulativeOrderAdjustmentAmount() {
		double cumulativeOrderAdjustmentAmount = 0;
		@SuppressWarnings("unchecked")
		List<PricingAdjustment> pricingAdjustments = getAdjustments();
		if (!DigitalCollectionUtil.isEmpty(pricingAdjustments)) {
			for (PricingAdjustment val : pricingAdjustments) {
				double d = val.getTotalAdjustment();
				if (d < 0) {
					cumulativeOrderAdjustmentAmount += d;
				}
			}
			cumulativeOrderAdjustmentAmount = cumulativeOrderAdjustmentAmount * -1;					
		}
		return cumulativeOrderAdjustmentAmount;
	}

}
