package com.digital.commerce.services.order.republish;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.states.OrderStates;
import atg.commerce.states.StateDefinitions;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.xml.GetException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.ldap.DigitalLdapTools;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.purchase.OrderXMLGenerator;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalOrderRepublishingComponent extends ApplicationLoggingImpl {

	private OrderManager		orderManager;

	private OrderXMLGenerator	orderXMLGenerator;

	private DigitalOrderRepublishingService	orderRepublishingService;

	private MessageLocator messageLocator;

	private Hashtable<String, String> repubFailedOrders = new Hashtable<>();

	private Hashtable<String, String> repubSuccessOrders = new Hashtable<>();

	private String repubErrors;
	
	private DigitalServiceConstants dswConstants;
	
	private DigitalLdapTools ldapTools;

	public DigitalOrderRepublishingComponent() {
		super(DigitalOrderRepublishingComponent.class.getName());
	}
	
	/**
	 * @param orderIds
	 * @return
	 */
	public Hashtable<String, Object> republishOrders(String orderIds, String login, String password) {
		Hashtable<String, Object> repubOrders = new Hashtable<>();
		try {
			if(!verifyDashboardLoginRules(login,password)){
				repubErrors = messageLocator
						.getMessageString("dashboard.invalid.login");
			} else {
		if (DigitalStringUtil.isBlank(orderIds)) {
			repubErrors = messageLocator
					.getMessageString("republish.order.missing.order.ids");
		}
		if (DigitalStringUtil.isNotBlank(orderIds)) {
			final String parsedOrderIds[] = DigitalStringUtil.split(
					orderIds.trim(), ",");
			for (final String parsedOrderId : parsedOrderIds) {
				republishOrder(parsedOrderId);
			}
		}
				setAuditLoggingInfo(login, repubSuccessOrders );
				
				if (repubSuccessOrders.size() > 0)
			repubOrders.put("Republished Success", repubSuccessOrders);
		if (repubFailedOrders.size() > 0)
			repubOrders.put("Republished Failed", repubFailedOrders);
		if (repubSuccessOrders.size() == 0 && repubFailedOrders.size() == 0)
			repubErrors = messageLocator
					.getMessageString("republish.order.missing.order.ids");
			}
		} catch (DigitalAppException e) {
			repubErrors = e.getLocalizedMessage();
		}
		return repubOrders;
	}

	/**
	 * @param orderId
	 */
	public void republishOrder(String orderId) {
		final String methodName = "republishOrder";
		String message = null;
		if (orderManager != null && DigitalStringUtil.isNotBlank(orderId)) {
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
						.getClass().getName(), methodName);
				final Order order = orderManager.loadOrder(orderId.trim());
				if (order != null && isOrderSubmmitted(order)) {
					if (order instanceof DigitalOrderImpl) {
						final DigitalOrderImpl dswOrder = (DigitalOrderImpl) order;
						try {
							final String serializedOrder = orderXMLGenerator
									.getOrderAsXML(dswOrder);
							if (DigitalStringUtil.isNotBlank(serializedOrder)) {
								try {
									orderRepublishingService
											.submitOrderAsXML(serializedOrder);
									repubSuccessOrders.put(orderId,
											"Successfully republished");
								} catch (DigitalAppException e) {
									message = String
											.format("Received ServicesException with message %s attempting to submit order %s as XML",
													e.getMessage(), orderId);
									logInfo(message, e);
									repubFailedOrders.put(orderId, message);
								}
							}
						} catch (GetException e) {
							message = String
									.format("Received GetException with message %s attempting to serialize order %s",
											e.getMessage(), orderId);
							logInfo(message, e);
							repubFailedOrders.put(orderId, message);
						}
					} else {
						repubFailedOrders.put(orderId,
								"Order is not an instance of DigitalOrderImpl");
					}
				} else {
					repubFailedOrders.put(orderId, "Unable to load order");
				}
			} catch (CommerceException e) {
				message = String
						.format("Does Not exist - Received CommerceException with message %s attempting to load order %s",
								e.getMessage(), orderId);
				logInfo(message, e);
				repubFailedOrders.put(orderId, message);
			}  finally {
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
						.getClass().getName(), methodName);
			}
		}
	}

	private boolean isOrderSubmmitted(Order order) {
		return order.getState() == StateDefinitions.ORDERSTATES
				.getStateValue(OrderStates.SUBMITTED);
	}


	public Boolean verifyDashboardLoginRules(String pLogin, String pPassword) throws DigitalAppException {
		if (DigitalStringUtil.isNotBlank(pLogin)
				&& DigitalStringUtil.isNotBlank(pPassword)) {
		try{
			return getLdapTools().doUserAuthetication(pLogin, pPassword,
					null, Arrays.asList(getDswConstants().getDashboardRole() ));
		} catch(DigitalAppException de) {
			throw new DigitalAppException("DASHBOARD_INVALID_USER", getMessageLocator().getMessageString("dashboard.invalid.login"), null);
		}
		} else {
			throw new DigitalAppException("DASHBOARD_LOGIN_INPUT_MISSING", getMessageLocator().getMessageString("dashboard.login.input.missing"), null);
		}
	}
	
	private void setAuditLoggingInfo(String login, Hashtable<String, String> repubSuccessOrders ){
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		String cPath = request.getContextPath();
		if(request != null) {
		logInfo( "Contex Path = " + cPath );
		logInfo( "Source IP = " + request.getRemoteAddr() );
		logInfo( "Remote User = " + request.getRemoteUser() );
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss:SSS" );
		logInfo( "AT " + sdf.format( cal.getTime() ) );
		logInfo( "User By = " + login );
		logInfo( "Orders Successfully Republished = " + repubSuccessOrders.toString() );
		logInfo( "Orders Republishing Failed = " + repubFailedOrders.toString() );
		}
	}
}
