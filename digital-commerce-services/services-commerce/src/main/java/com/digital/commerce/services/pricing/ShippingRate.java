/**
 * 
 */
package com.digital.commerce.services.pricing;

import com.digital.commerce.services.common.ServicesStatus;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class ShippingRate extends ServicesStatus implements Comparable {

	private static final long	serialVersionUID	= 2767812378831762704L;
	private double				baseRate;									// used for rate search
	private String				baseRateServiceLevel;
	private String				baseRateServiceName;
	private String				baseRatesServiceLevelDescription;
	private boolean				valid;
	private boolean				selected;
	private String				errorMessage;

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object) */
	public int compareTo( Object otherRate ) {
		ShippingRate sr = (ShippingRate)otherRate;
		if( sr.getBaseRate() > this.getBaseRate() ) {
			return -1;
		} else if( sr.getBaseRate() == this.getBaseRate() ) {
			return 0;
		} else {
			return 1;
		}
	}

}
