
package com.digital.commerce.services.order.history.domain;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
@Getter
@Setter
@ToString
public class OrderItem {

    private String status;
    private String submittedTime;
    private String id;
    private List<CommerceItem> commerceItems = new ArrayList<CommerceItem>();
    private String siteId;
    private PriceInfo priceInfo;
    private Integer totalCommerceItemCount;

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(status).append(submittedTime).append(id).append(commerceItems).append(siteId).append(priceInfo).append(totalCommerceItemCount).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof OrderItem) == false) {
            return false;
        }
        OrderItem rhs = ((OrderItem) other);
        return new EqualsBuilder().append(status, rhs.status).append(submittedTime, rhs.submittedTime).append(id, rhs.id).append(commerceItems, rhs.commerceItems).append(siteId, rhs.siteId).append(priceInfo, rhs.priceInfo).append(totalCommerceItemCount, rhs.totalCommerceItemCount).isEquals();
    }

}
