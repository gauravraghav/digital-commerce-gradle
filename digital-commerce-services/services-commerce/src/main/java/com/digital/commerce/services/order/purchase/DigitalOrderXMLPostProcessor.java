/** File Name: DigitalOrderXMLPostProcessor.java
 * Author: vnarla
 * Created on: Feb 7, 2008 */
package com.digital.commerce.services.order.purchase;

import com.digital.commerce.services.utils.IXpathResultCallback;
import com.digital.commerce.services.utils.XmlTemplate;

public class DigitalOrderXMLPostProcessor {

	protected static final String	targetSequence		= "commerceItemcommerceItem";
	protected static final String	replacementSequence	= "commerceItem";

	/** Perform modifications on the order xml
	 * 
	 * @param orderAsXML
	 * @return */
	public String process( String orderAsXML ) {
		orderAsXML = orderAsXML.replace( DigitalOrderXMLPostProcessor.targetSequence, DigitalOrderXMLPostProcessor.replacementSequence );
		XmlTemplate template = new XmlTemplate( orderAsXML );
		IXpathResultCallback callback = new PromotionTransformationCallback();
		template.query( PromotionTransformationCallback.ITEM_LEVEL_ADJUSTMENTS, callback );
		template.query( PromotionTransformationCallback.ORDER_LEVEL_ADJUSTMENTS, callback );
		template.query( PromotionTransformationCallback.SHIPPING_LEVEL_ADJUSTMENTS, callback );
		return template.getDocumentAsString();
	}
}
