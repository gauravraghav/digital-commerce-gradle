package com.digital.commerce.services.gifts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import atg.adapter.gsa.query.Builder;
import atg.commerce.CommerceException;
import atg.commerce.catalog.custom.CustomCatalogTools;
import atg.commerce.gifts.GiftObjectCreationException;
import atg.commerce.gifts.GiftlistManager;
import atg.commerce.gifts.GiftlistTools;
import atg.commerce.gifts.InvalidGiftParameterException;
import atg.commerce.gifts.InvalidGiftQuantityException;
import atg.commerce.gifts.InvalidGiftTypeException;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.multisite.SiteContextManager;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalGiftlistManager extends GiftlistManager {

	private static final DigitalLogger logger = DigitalLogger
			.getLogger(DigitalGiftlistManager.class);

	static final String SKU_PRODUCT_NOT_MATCH = "sku_product_not_match";
	static final String ITEM_EXCEED_LIMIT = "item_exceed_limit";
	private DigitalServiceConstants			digitalConstants;

	public int getWishlistItemLimit() {
		return MultiSiteUtil.getWishlistItemLimit();
	}

	public int getCartLimit() {
		return MultiSiteUtil.getCartLineLimit();
	}

	/**
	 * Used by giftlistLookupActor to get the Count of Wishlist items
	 * @param giftlistID
	 * @return
	 */
	
	public Long getWishlistCount(String giftlistID) {
		
		try
		{
			//If wishlist is not enabled then return 0. Stop querying data from DB
			if (!digitalConstants.isWishlistIsEnabled())
			{
				return 0L; 
			}
			RepositoryView view = this.getGiftlistTools().getGiftlistRepository().getView("gift-list");
			Builder builder = (Builder) view.getQueryBuilder();
			String sqlQuery = "SELECT count(*) FROM DSW_CORE.DSW_WISHLIST_ACTIVE_ITEMS_V WHERE GIFTLIST_ID = ?"; //this VIEW has joins to ensure Product isActive and Product-SKU mapping etc.,
			Object params[] = new Object[1];
			params[0] = giftlistID;
			Query query = builder.createSqlPassthroughQuery(sqlQuery, params);
			RepositoryItem[] repositoryItems = view.executeQuery(query);
			Long wishlistCount = 0L;
			
			if(null != repositoryItems && repositoryItems.length > 0){
                RepositoryItem repositoryItem = repositoryItems[0];
                String result = repositoryItem.getRepositoryId();
                if(DigitalStringUtil.isNotEmpty(result)){
                	wishlistCount = Long.parseLong(result);
                }
            }
			return wishlistCount;
		}
		catch (Exception ex) {
			if (isLoggingError()) {
                logError("DigitalGiftlistManager getWishlistCount ", ex);
            }
			return 0L;
		}
	}

	/**
	 * Used by DigitalCatalogTools to find if the Product is in Wishlist or not, for the PDP service
	 * @param giftlistID
	 * @param productID
	 * @return
	 */
	public Boolean isProductinWishlist(String giftlistID, String productID) {
		
		try
		{
			//If wishlist is not enabled then return false. Stop querying data from DB
			if (!digitalConstants.isWishlistIsEnabled())
			{
				return false; 
			}
			RepositoryView view = this.getGiftlistTools().getGiftlistRepository().getView("gift-list");
			Builder builder = (Builder) view.getQueryBuilder();
			String sqlQuery = "select 1 from DSW_CORE.DCS_GIFTLIST_ITEM g, DSW_CORE.DCS_GIFTITEM i where g.GIFTITEM_ID = i.ID  and g.GIFTLIST_ID= ? and i.product_id= ?";
			Object params[] = new Object[2];
			params[0] = giftlistID;
			params[1] = productID;
			Query query = builder.createSqlPassthroughQuery(sqlQuery, params);
			RepositoryItem[] repositoryItems = view.executeQuery(query);
			Boolean isInWishList = false;
			
			if(null != repositoryItems && repositoryItems.length > 0){
                RepositoryItem repositoryItem = repositoryItems[0];
                String result = repositoryItem.getRepositoryId();
                if(DigitalStringUtil.isNotEmpty(result)){
                	if (result.equals("1")) isInWishList = true;
                }
            }
			return isInWishList;
		}
		catch (Exception ex) {
			if (isLoggingError()) {
                logError("DigitalGiftlistManager isProductinWishlist ", ex);
            }
			return false;
		}
	}

	
	public void addCatalogItemToGiftlist(String pSkuId, String pProductId,
			String pGiftlistId, String pSiteId, long pQuantity)
			throws CommerceException {
		logger.debug("Inside addCatalogItemToGiftlist");

		if (pSkuId != null) {
			logger.debug("Inside addCatalogItemToGiftlist skuid is not null pSkuId:: "
					+ pSkuId);
			super.addCatalogItemToGiftlist(pSkuId, pProductId, pGiftlistId,
					pSiteId, pQuantity);
			return;
		}

		try {
			String displayName = null;
			String description = null;
			String productId = pProductId;
			RepositoryItem product = getCatalogTools().findProduct(productId);

			if (product == null) {
				return;
			}

			productId = product.getRepositoryId();
			displayName = (String) product.getPropertyValue(getGiftlistTools()
					.getDisplayNameProperty());
			description = (String) product.getPropertyValue(getGiftlistTools()
					.getDescriptionProperty());

			String giftId = null;

			giftId = getDSWGiftlistItemId(pGiftlistId, null, productId, pSiteId);

			if (giftId != null) {
				increaseGiftlistItemQuantityDesired(pGiftlistId, giftId,
						pQuantity);
			} else if (pQuantity > 0L) {
				String itemId = createDSWGiftlistItem(null, productId,
						pQuantity, displayName, description, pSiteId);
				addItemToGiftlist(pGiftlistId, itemId);
			}
		} catch (RepositoryException exc) {
			throw new CommerceException(exc);
		}
	}

	/**
	 * Override GiftlistManager method due to sku can be null in DSW requirments
	 **/
	public String getGiftlistItemId(String pGiftlistId, String pSkuId,
			String pProductId, String pSiteId) {
		if (isLoggingDebug()) {
			logDebug("Inside getGiftlistItemId: giftlist id = " + pGiftlistId
					+ ", skuId = " + pSkuId + ", productId = " + pProductId
					+ ", siteId = " + pSiteId);
		}
		if (pSiteId == null) {
			pSiteId = SiteContextManager.getCurrentSiteId();
		}
		List items = getGiftlistItems(pGiftlistId);
		if (items != null) {
			Iterator iterator = items.iterator();
			while (iterator.hasNext()) {
				RepositoryItem item = (RepositoryItem) iterator.next();
				if ((item.getPropertyValue(getGiftlistTools()
						.getCatalogRefIdProperty()) == null || item
						.getPropertyValue(
								getGiftlistTools().getCatalogRefIdProperty())
						.equals(pSkuId))
						&& ((pProductId == null) || (item
								.getPropertyValue(getGiftlistTools()
										.getProductIdProperty())
								.equals(pProductId)))
						&& ((pSiteId == null) || (item
								.getPropertyValue(getGiftlistTools()
										.getSiteProperty()).equals(pSiteId)))) {
					return (String) item.getPropertyValue("id");
				}
			}
		}
		return null;
	}

	public String getDSWGiftlistItemId(String pGiftlistId, String pSkuId,
			String pProductId, String pSiteId) {
		logger.debug("Inside getGiftlistItemId: giftlist id = " + pGiftlistId
				+ ", skuId = " + pSkuId + ", productId = " + pProductId
				+ ", siteId = " + pSiteId);

		if (pSiteId == null) {
			pSiteId = SiteContextManager.getCurrentSiteId();
		}

		List items = getGiftlistItems(pGiftlistId);

		if (items != null) {
			Iterator iterator = items.iterator();

			while (iterator.hasNext()) {
				RepositoryItem item = (RepositoryItem) iterator.next();

				if (pProductId == null || ((item.getPropertyValue(getGiftlistTools().getProductIdProperty()) != null
						&& item.getPropertyValue(getGiftlistTools().getProductIdProperty()).equals(pProductId)
						&& item.getPropertyValue(getGiftlistTools().getCatalogRefIdProperty()) == null))
						&& ((pSiteId == null)
								|| (item.getPropertyValue(getGiftlistTools().getSiteProperty()).equals(pSiteId)))) {

					return (String) item.getPropertyValue("id");
				}
			}
		}
		return null;
	}

	public String createDSWGiftlistItem(String pCatalogRefId,
			String pProductId, long pQuantityDesired, String pDisplayName,
			String pDescription, String pSiteId) throws CommerceException {
		if (isLoggingDebug())
			logDebug("Inside creatGiftlistItem");
		GiftlistTools tools = getGiftlistTools();

		// Overrides here to not throw an exception if the catalogRefId is null.
		/*
		 * if (pCatalogRefId == null) throw new
		 * InvalidGiftParameterException(ResourceUtils
		 * .getMsgResource("InvalidCatalogRefIdParameter",
		 * "atg.commerce.gifts.GiftlistResources", sResourceBundle));
		 */
		if (pQuantityDesired <= 0L) {
			throw new InvalidGiftQuantityException(
					ResourceUtils.getMsgResource("InvalidQuantityParameter",
							"atg.commerce.gifts.GiftlistResources",
							sResourceBundle));
		}

		MutableRepositoryItem item = null;
		try {
			item = tools.createGiftlistItem();
			item.setPropertyValue(tools.getCatalogRefIdProperty(),
					pCatalogRefId);
			item.setPropertyValue(tools.getProductIdProperty(), pProductId);
			item.setPropertyValue(tools.getQuantityDesiredProperty(),
					Long.valueOf(pQuantityDesired));
			item.setPropertyValue(tools.getQuantityPurchasedProperty(),
					Long.valueOf(0L));
			item.setPropertyValue(tools.getDisplayNameProperty(), pDisplayName);
			item.setPropertyValue(tools.getDescriptionProperty(), pDescription);
			if (pSiteId == null) {
				pSiteId = SiteContextManager.getCurrentSiteId();
			}
			item.setPropertyValue(tools.getSiteProperty(), pSiteId);
			tools.addItem(item);
		} catch (InvalidGiftTypeException e) {
			throw new GiftObjectCreationException(ResourceUtils.getMsgResource(
					"InvalidGiftlistItemName",
					"atg.commerce.gifts.GiftlistResources", sResourceBundle), e);
		} catch (RepositoryException re) {
			throw new GiftObjectCreationException(ResourceUtils.getMsgResource(
					"GiftlistItemNotCreated",
					"atg.commerce.gifts.GiftlistResources", sResourceBundle),
					re);
		}
		return item.getRepositoryId();
	}

	public String createWishlist(RepositoryItem profile)
			throws CommerceException {
		GiftlistTools tools = getGiftlistTools();
		if (profile == null) {
			throw new InvalidGiftParameterException(
					ResourceUtils.getMsgResource("InvalidProfileIdParameter",
							"atg.commerce.gifts.GiftlistResources",
							sResourceBundle));
		}
		MutableRepositoryItem giftlist = null;
		MutableRepositoryItem gitem = (MutableRepositoryItem) profile
				.getPropertyValue(tools.getWishlistProperty());
		try {
			if (gitem == null) {
				giftlist = tools.createGiftlist();
				giftlist.setPropertyValue(tools.getOwnerProperty(), profile);
				tools.addItem(giftlist);
				if (profile != null)
					this.getProfileTools().updateProperty(
							tools.getWishlistProperty(), giftlist, profile);

				return giftlist.getRepositoryId();

			} else {
				gitem.setPropertyValue(tools.getOwnerProperty(), profile);
				tools.addItem(gitem);
				this.getProfileTools().updateProperty(
						tools.getWishlistProperty(), giftlist, profile);
				return gitem.getRepositoryId();
			}
		} catch (InvalidGiftTypeException | RepositoryException e) {
			throw new GiftObjectCreationException(ResourceUtils.getMsgResource(
					"InvalidGiftlistName",
					"atg.commerce.gifts.GiftlistResources", sResourceBundle), e);
		}

	}

	public boolean updatePermitted(String pGiftlistId, String pProfileId) {
		return this.isGiftlistOwner(pGiftlistId, pProfileId);
	}

	private boolean verifySkusProductPair(String pid, String[] sids)
			throws RepositoryException {
		if (pid == null || sids == null || sids.length == 0)
			return false;

		CustomCatalogTools tools = (CustomCatalogTools) this.getCatalogTools();

		RepositoryItem prod = tools.findProduct(pid);

		ArrayList<String> matched = new ArrayList<>();

		List<String> original = Arrays.asList(sids);

		if (prod == null) {
			return false;
		} else {
			List<RepositoryItem> skus = (List<RepositoryItem>) prod
					.getPropertyValue(tools.getCatalogProperties()
							.getChildSkusPropertyName());
			if (skus == null || skus.size() == 0)
				return false;
			Iterator<RepositoryItem> skuIter = skus.iterator();
			while (skuIter.hasNext()) {
				RepositoryItem item = skuIter.next();
				if ((original.contains(item.getRepositoryId())))
					matched.add(item.getRepositoryId());
			}
		}

		return (original.size() == matched.size());

	}

	public HashMap<String, JSONObject> processRequestData(JSONArray itemsArray,
			RepositoryItem profile) throws CommerceException, JSONException,
			RepositoryException {
		final GiftlistTools tools = this.getGiftlistTools();

		RepositoryItem pWishlist = (RepositoryItem) profile
				.getPropertyValue(tools.getWishlistProperty());
		List<RepositoryItem> gItems = (List<RepositoryItem>) pWishlist
				.getPropertyValue(tools.getGiftlistItemsProperty());

		// build product and sku hashmap for performance improvement and
		// consolidation
		final HashMap<String, JSONObject> productItems = new HashMap<>();
		final HashMap<String, JSONObject> skuItems = new HashMap<>();

		for (Object item : itemsArray) {
			if (!DigitalStringUtil.isBlank(((JSONObject) item).getString("sku"))) {
				String[] skus = StringUtils.splitStringAtCharacter(
						((JSONObject) item).getString("sku"), ',');
				// verify skus match product
				if (!verifySkusProductPair(
						((JSONObject) item).getString("productId"), skus)) {
					throw new CommerceException(SKU_PRODUCT_NOT_MATCH);
				}
				for (String it : skus) {
					if (skuItems.containsKey(it)) {
						JSONObject jobj = skuItems.get(it);
						jobj.put("productId",
								((JSONObject) item).getString("productId"));
						jobj.put("sku", it);
						jobj.put("quantity", jobj.getLong("quantity")
								+ ((JSONObject) item).getLong("quantity"));
					} else {
						JSONObject jobj = new JSONObject();
						jobj.put("productId",
								((JSONObject) item).getString("productId"));
						jobj.put("sku", it);
						jobj.put("quantity",
								((JSONObject) item).getLong("quantity"));
						skuItems.put(it, jobj);
					}
				}
			} else {
				if (productItems.containsKey(((JSONObject) item)
						.getString("productId"))) {
					JSONObject jobj = productItems.get(((JSONObject) item)
							.getString("productId"));
					jobj.put("quantity", jobj.getLong("quantity")
							+ ((JSONObject) item).getLong("quantity"));
				} else {
					productItems.put(
							((JSONObject) item).getString("productId"),
							(JSONObject) item);
				}
			}
		}

		List<RepositoryItem> existItems = Lists.newArrayList();

		DigitalPredicate<RepositoryItem> isExistPredicate = new DigitalPredicate<RepositoryItem>() {
			@Override
			public boolean apply(RepositoryItem item) {
				if (DigitalStringUtil.isBlank((String) item.getPropertyValue(tools
						.getCatalogRefIdProperty()))) {
					if (productItems.containsKey((String) item
							.getPropertyValue(tools.getProductIdProperty()))) {
						return true;
					}

				} else {
					if (skuItems.containsKey((String) item
							.getPropertyValue(tools.getCatalogRefIdProperty()))) {
						return true;
					}

				}
				return false;
			}

		};

		if (gItems != null) {
			Iterables.addAll(existItems,
					Iterables.filter(gItems, isExistPredicate));
		}

		if (productItems.size()
				+ skuItems.size()
				+ ((gItems != null ? gItems.size() : 0) - (existItems != null ? existItems
						.size() : 0)) > MultiSiteUtil.getWishlistItemLimit()) {
			throw new CommerceException(ITEM_EXCEED_LIMIT);
		}

		productItems.putAll(skuItems);

		return productItems;

	}

}