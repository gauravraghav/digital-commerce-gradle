/**
 *
 */
package com.digital.commerce.repository;

import java.util.Iterator;
import java.util.List;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

@SuppressWarnings({"rawtypes"})
public class SignatureRequiredProductPropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
     *
     */
	private static final long		serialVersionUID	= -6259342473515967032L;

	protected static final String	TYPE_NAME			= "SignatureRequiredProductPropertyDescriptor";

	protected static final String	CHILD_SKUS			= "childSKUs";

	protected static final String	SIGNATURE_REQUIRED	= "isSignatureRequired";
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, SignatureRequiredProductPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		if( pValue != null && pValue instanceof Boolean ) { return pValue; }

		List skus = (List)pItem.getPropertyValue( CHILD_SKUS );

		if( skus != null ) {
			Iterator i = skus.iterator();
			RepositoryItem sku = null;
			while( i.hasNext() ) {
				sku = (RepositoryItem)i.next();
				Boolean signatureRequired = (Boolean)sku.getPropertyValue( SIGNATURE_REQUIRED );
				if( Boolean.TRUE.equals( signatureRequired ) ) {
					setPropertyInCache( pItem, Boolean.TRUE );

					return Boolean.TRUE;
				}
			}
			setPropertyInCache( pItem, Boolean.FALSE );

		}
		return Boolean.FALSE;
	}

	private void setPropertyInCache( RepositoryItemImpl item, Boolean flag ) {
		item.setPropertyValueInCache( this, flag );
	}
}
