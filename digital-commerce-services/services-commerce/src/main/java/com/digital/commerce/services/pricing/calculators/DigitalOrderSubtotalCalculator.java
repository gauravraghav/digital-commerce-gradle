package com.digital.commerce.services.pricing.calculators;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.digital.commerce.services.order.DigitalOrderPriceInfo;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.RelationshipTypes;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.OrderSubtotalCalculator;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingException;
import atg.repository.RepositoryItem;


/** 
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates */
@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalOrderSubtotalCalculator extends OrderSubtotalCalculator {

	/** This method calculates the subtotal of the shipping group.
	 * This method will return the subtotal of the items contained within this shipping group.
	 * In addition it will modify the given pPriceQuote (the Order's price info)
	 * so the following properties include a new entry for pShippingGroup:
	 * 
	 * <pre>
	 *   shippingItemsSubtotalPriceInfos
	 *   taxableShippingItemsSubtotalPriceInfos
	 *   nonTaxableShippingItemsSubtotalPriceInfos
	 * </pre>
	 * 
	 * @param pPriceQuote The price info for the order
	 * @param pOrder The order containing the shipping group
	 * @param pShippingGroup The shipping group being priced
	 * @param pItemQuantities A map mapping a commerce item to a quantity. The incoming quantity will be the total
	 *            quantity of that item that has been included in the subtotal of other shipping groups.
	 *            The quantity will be increased by the quantity contained in pShippingGroup
	 * @param pItemPrices A map mapping a commerce item to a price. The price is the total price of the item that has been included
	 *            in the subtotals of other shipping groups. Once the pItemQuantities for a given item is reached
	 *            this value is used to resolve any potential rounding errors. The pItemPrices for a particular item
	 *            will be increased by the price of the item that pShippingGroup's subtotal used.
	 * @param pLocale The locale in which all pricing operations should be performed
	 * @param pProfile The owner of pOrder
	 * @param pExtraParameters Any extra processing information. Unused by this method.
	 * @return The subtotal of pShippingGroup (this is the sum of the prices of the items contained within the shipping group)
	 * @throws PricingException */
	protected double calculateShippingGroupSubtotal( OrderPriceInfo pPriceQuote, Order pOrder, ShippingGroup pShippingGroup, Map pItemQuantities, Map pItemPrices, Locale pLocale, RepositoryItem pProfile, Map pExtraParameters )
			throws PricingException {
		String groupId = pShippingGroup.getId();
		double groupSubtotal = 0.0;
		double groupTaxableSubtotal = 0.0;
		double groupNonTaxableSubtotal = 0.0;
		double taxableAmount;
		double nonTaxableAmount;
		long shippingGroupQuantity = 0;
		long taxableShippingGroupQuantity = 0;
		long nonTaxableShippingGroupQuantity = 0;

		// get all the relationships in this group
		List relationships = pShippingGroup.getCommerceItemRelationships();

		// if there are no relationships in this group, there must be a relationship
		// in some other group, because we've already verified that fact using
		// checkRelationships
		if( relationships == null || relationships.isEmpty() ) {
			return 0.0;
		} // end if no relationships
			// else there is at least one relationship
		else {

			if( isLoggingDebug() ) {
				logDebug( "Shipping group : " + groupId + " has : " + relationships.size() + " relationships" );
			}

			Iterator relationshipIterator = relationships.iterator();
			while( relationshipIterator.hasNext() ) {

				ShippingGroupCommerceItemRelationship rel = (ShippingGroupCommerceItemRelationship)relationshipIterator.next();

				// get the item and the quantity of the item that is in this relationship.
				// switch on type of relationship to get the actual quantity that the relationship
				// represents

				long relationshipQuantity = 0;
				CommerceItem item = rel.getCommerceItem();

                switch (rel.getRelationshipType()) {
                    case RelationshipTypes.SHIPPINGQUANTITY:
                        relationshipQuantity = rel.getQuantity();
                        break;
                    case RelationshipTypes.SHIPPINGQUANTITYREMAINING:
                        relationshipQuantity = getOrderManager().getShippingGroupManager().getRemainingQuantityForShippingGroup(item);
                        break;
                    default:
                        throw new PricingException(MessageFormat.format(Constants.BAD_SHIPPING_GROUP_TYPE, new Object[]{Integer.valueOf(rel.getRelationshipType())}));
                }

				if( isLoggingDebug() ) {
					logDebug( "total quantity of item: " + item.getId() + " to add to order total: " + relationshipQuantity );
				}

				// if there's no quantity in this rel, skip it
				if( relationshipQuantity == 0 ) {
					if( isLoggingDebug() ) {
						logDebug( "no quantity of this relationship to add, continuing." );
					}
					continue;
				}

				shippingGroupQuantity += relationshipQuantity;

				/** find the average item price for order summing purposes, since all items will
				 * be accounted for (we just need a breakdown for the group level)
				 * see javadoc for explanation.
				 * average price is total price for the CommerceItem divided by the quantity of the
				 * item. This code also floors the average price to a configurable number of decimal
				 * places, and tacks any remainder on to a single relationship amount.
				 * The remainder itself should automatically be a max of N decimal places,
				 * because item prices get rounded too. Therefore, an item price with a max of N
				 * places minus (average price rounded to N places - times - CommerceItem quantity)
				 * can yield a number with a maximum of N decimal places:
				 * 
				 * 2 places times 0 places => 2 places
				 * 2 places minus 2 places => 2 places */

				// add to the group subtotal the cost of this relationship
				// Now that relationships and details have ranges, we don't need to use
				// averages
				double relationshipSubtotal = 0.0;

				double roundedAverage = 0.0;
				if( ( getPricingTools().isShippingSubtotalUsesAverageItemPrice() ) || ( rel.getRange() == null ) ) {

					double averagePrice = getPricingTools().getAverageItemPrice( item );
					roundedAverage = getPricingTools().round( averagePrice );

					if( isLoggingDebug() ) {
						logDebug( "Average price of commerceitem: " + item.getId() + " across all relationships : " + averagePrice );
						logDebug( "rounding average price" );
						logDebug( "rounded average price to: " + roundedAverage );
					}

					relationshipSubtotal = relationshipQuantity * roundedAverage;
				} else {
					relationshipSubtotal = getPricingTools().getShipItemRelPriceTotal( rel, DETAILED_ITEM_PRICE_AMOUNT_PROPERTY );
				}

				// used when determining leftovers (when not using averaging method)
				double runningTotal = 0.0;
				Double oldRunningTotal = (Double)pItemPrices.get( item );
				if( oldRunningTotal != null ) runningTotal += oldRunningTotal.doubleValue();

				runningTotal += relationshipSubtotal;

				if( isLoggingDebug() ) {
					logDebug( "subtotal of this relationship for group: " + groupId + " : " + relationshipSubtotal );
				}

				long quantityAlreadyProcessed = 0;
				Long quantityProcessed = (Long)pItemQuantities.get( item );
				if( quantityProcessed != null ) {
					quantityAlreadyProcessed = quantityProcessed.longValue();
				}

				if( isLoggingDebug() ) {
					logDebug( "quantity of the item: " + item.getId() + " already processed: " + quantityAlreadyProcessed );
					logDebug( "quantity in this relationship: " + relationshipQuantity );
					logDebug( "total quantity of the item: " + item.getQuantity() );
				}

				// if we're attributing the final bit of an item to a group's subtotal,
				// add any leftovers
				if( quantityAlreadyProcessed + relationshipQuantity == item.getQuantity() ) {
					if( isLoggingDebug() ) {
						logDebug( "calculating leftovers" );
					}

					if( getPricingTools().isShippingSubtotalUsesAverageItemPrice() ) {
						double leftovers = item.getPriceInfo().getAmount() - ( roundedAverage * item.getQuantity() );
						// the leftovers are the remainders from rounding every unit of the CommerceItem
						if( isLoggingDebug() ) {
							logDebug( "total item amount: " + item.getPriceInfo().getAmount() );
							logDebug( "item quantity: " + item.getQuantity() );
							logDebug( "rounded average price: " + roundedAverage );
							logDebug( "rounded unit price times quantity of: " + item.getQuantity() + " : " + ( roundedAverage * item.getQuantity() ) );
							logDebug( "adding leftovers: " + leftovers );
						}

						relationshipSubtotal += leftovers;
					} else {
						double leftovers = item.getPriceInfo().getAmount() - runningTotal;
						// the leftovers are the remainders from rounding every unit of the CommerceItem
						if( isLoggingDebug() ) {
							logDebug( "total item amount: " + item.getPriceInfo().getAmount() );
							logDebug( "total so far: " + runningTotal );
							logDebug( "adding leftovers: " + leftovers );
						}

						relationshipSubtotal += leftovers;
					}
				} // end if we're processing the last of an item

				// add the relationship subtotal to the group subtotal
				groupSubtotal += relationshipSubtotal;

				/** sometimes double math produces 'equivalent', yet not exactly identical
				 * numbers. For example, it appears that the numbers:
				 * .03
				 * and
				 * .030000000000001137
				 * are numerically equivalent as far as double calculations are concerned.
				 * Double calculations sometimes produce these strange numbers despite the
				 * fact that the number of decimal places can't possibly be that large.
				 * We're going to round the products of double math to N places, with the
				 * understanding that all numbers in DCS are going to be rounded to N places. */

				groupSubtotal = getPricingTools().round( groupSubtotal );

				if( isLoggingDebug() ) {
					logDebug( "after rounding, group subtotal: " + groupSubtotal );
				}

				// if the current item is taxable, add it to the taxable subtotal
				if( getPricingTools().isTaxable( item, item.getPriceInfo(), pPriceQuote, pOrder, pShippingGroup.getPriceInfo(), pShippingGroup, pLocale, pProfile, pExtraParameters ) ) {
					// see if the item is partially taxable or not

					if( getPricingTools().isShippingSubtotalUsesAverageItemPrice() )
						taxableAmount = getPricingTools().calculateTaxableAmountByAverage( rel, pOrder, pLocale, pProfile, pExtraParameters );
					else
						taxableAmount = getPricingTools().calculateTaxableAmount( rel, pOrder, pLocale, pProfile, pExtraParameters );

					// make sure that our averaging doesn't end up making it so the
					// taxable amount is more than the total amount.
					if( taxableAmount > relationshipSubtotal ) {
						taxableAmount = relationshipSubtotal;
					}

					nonTaxableAmount = relationshipSubtotal - taxableAmount;

					taxableShippingGroupQuantity += relationshipQuantity;
					groupTaxableSubtotal += taxableAmount;

					groupTaxableSubtotal = getPricingTools().round( groupTaxableSubtotal );

					if( isLoggingDebug() ) {
						logDebug( "relationship's subtotal is TAXABLE" );
						logDebug( "after rounding, group taxable subtotal: " + groupTaxableSubtotal );
					}

					nonTaxableAmount = getPricingTools().round( nonTaxableAmount );

					if( nonTaxableAmount > 0.0 ) {
						// if we are here, then the item was "partially" taxable
						// each quantity of the item has a taxable part and a non taxable part
						nonTaxableShippingGroupQuantity += relationshipQuantity;
						groupNonTaxableSubtotal += nonTaxableAmount;
						groupNonTaxableSubtotal = getPricingTools().round( groupNonTaxableSubtotal );
						if( isLoggingDebug() ) {
							logDebug( "It was only partially taxable though" );
							logDebug( "after rounding, group non taxable subtotal: " + groupNonTaxableSubtotal );
						}
					}

				}
				// if it's not taxable, add it to the nontaxable subtotal
				else {
					nonTaxableShippingGroupQuantity += relationshipQuantity;
					groupNonTaxableSubtotal += relationshipSubtotal;

					groupNonTaxableSubtotal = getPricingTools().round( groupNonTaxableSubtotal );

					if( isLoggingDebug() ) {
						logDebug( "relationship's subtotal is NON-TAXABLE" );
						logDebug( "after rounding, group non taxable subtotal is: " + groupNonTaxableSubtotal );
					}

				} // end if not taxable

				// register the item as processed
				pItemQuantities.put( item, Long.valueOf( relationshipQuantity + quantityAlreadyProcessed ) );
				// remember amount for leftovers
				pItemPrices.put( item, Double.valueOf( runningTotal ) );

			} // end for each relationship

			// we should now have all relationships in the group accounted for.

		} // end else there are relationships

		if( isLoggingDebug() ) {
			logDebug( "group nontaxable subtotal: " + groupNonTaxableSubtotal );
			logDebug( "group taxable subtotal: " + groupTaxableSubtotal );
			logDebug( "group subtotal: " + groupSubtotal );
		}

		// sanity
		double sanityCheck = getPricingTools().round( groupNonTaxableSubtotal + groupTaxableSubtotal );
		if( sanityCheck != groupSubtotal ) { throw new PricingException( "The sum of the taxable and non-taxable items in this shippingGroup does not equal to the overall subtotal" ); }

		// prepare the taxable subtotal
		// SC: for dsw change, update it here
		OrderPriceInfo taxableShippingItemsInfo = new DigitalOrderPriceInfo();
		taxableShippingItemsInfo.setRawSubtotal( groupTaxableSubtotal );
		taxableShippingItemsInfo.setAmount( groupTaxableSubtotal );

		// initialize the adjustments list
		List adjustments = taxableShippingItemsInfo.getAdjustments();

		if( adjustments.size() > 0 ) {
			adjustments.clear();
		}

		// the first adjustment
		PricingAdjustment taxableAdjustment = new PricingAdjustment( Constants.ORDER_TAXABLE_SHIPPING_ITEMS_SUBTOTAL_PRICE_ADJUSTMENT_DESCRIPTION, null, groupTaxableSubtotal, taxableShippingGroupQuantity );

		adjustments.add( taxableAdjustment );

		// add a new entry into the order's TaxableShippingItemsSubtotalPriceInfos
		pPriceQuote.getTaxableShippingItemsSubtotalPriceInfos().put( groupId, taxableShippingItemsInfo );

		if( isLoggingDebug() ) {
			logDebug( "The taxable subtotal for shipping group: " + groupId + " is: " + groupTaxableSubtotal );
		}

		// prepare the non-taxable subtotal

		OrderPriceInfo nonTaxableShippingItemsInfo = new DigitalOrderPriceInfo();
		nonTaxableShippingItemsInfo.setRawSubtotal( groupNonTaxableSubtotal );
		nonTaxableShippingItemsInfo.setAmount( groupNonTaxableSubtotal );

		// initialize the adjustments list
		adjustments = nonTaxableShippingItemsInfo.getAdjustments();

		if( adjustments.size() > 0 ) {
			adjustments.clear();
		}

		// the first adjustment
		PricingAdjustment nonTaxableAdjustment = new PricingAdjustment( Constants.ORDER_NONTAXABLE_SHIPPING_ITEMS_SUBTOTAL_PRICE_ADJUSTMENT_DESCRIPTION, null, groupNonTaxableSubtotal, nonTaxableShippingGroupQuantity );

		adjustments.add( nonTaxableAdjustment );

		// add a new entry into the order's ShippingItemsSubtotalPriceInfos
		pPriceQuote.getNonTaxableShippingItemsSubtotalPriceInfos().put( groupId, nonTaxableShippingItemsInfo );

		if( isLoggingDebug() ) {
			logDebug( "The non-taxable subtotal for shipping group: " + groupId + " is: " + groupNonTaxableSubtotal );
		}

		// prepare the overall subtotal
		OrderPriceInfo shippingItemsInfo = new DigitalOrderPriceInfo();
		shippingItemsInfo.setRawSubtotal( groupSubtotal );
		shippingItemsInfo.setAmount( groupSubtotal );

		// initialize the adjustments list
		adjustments = shippingItemsInfo.getAdjustments();

		if( adjustments.size() > 0 ) {
			adjustments.clear();
		}

		// the first adjustment
		PricingAdjustment adjustment = new PricingAdjustment( Constants.ORDER_SHIPPING_ITEMS_SUBTOTAL_PRICE_ADJUSTMENT_DESCRIPTION, null, groupSubtotal, shippingGroupQuantity );

		adjustments.add( adjustment );

		// add a new entry into the order's ShippingItemsSubtotalPriceInfos
		pPriceQuote.getShippingItemsSubtotalPriceInfos().put( groupId, shippingItemsInfo );

		return groupSubtotal;
	}

}
