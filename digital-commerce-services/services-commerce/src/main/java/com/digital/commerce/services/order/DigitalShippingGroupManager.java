/**
 *
 */
package com.digital.commerce.services.order;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.ElectronicShippingGroup;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupManager;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShipType;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShippingGroupPropertyManager;
import com.digital.commerce.services.order.shipping.ShippingMethod;
import com.digital.commerce.services.pricing.ShippingRate;
import com.digital.commerce.services.storelocator.DigitalGeoLocationUtil;
import com.digital.commerce.services.storelocator.DigitalStoreAddress;
import com.digital.commerce.services.utils.DigitalAddressUtil;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
@SuppressWarnings({"rawtypes","unchecked"})

@Getter
@Setter
public class DigitalShippingGroupManager extends ShippingGroupManager {
	
	private int	giftCardGroupStateCode;
	
	private DigitalGeoLocationUtil geoLocationUtil;
	
	private String fetchStoreQuery;

	/**
	 * 
	 * @param shippingRates
	 * @param shippingMethod
	 * @return
	 */
	private boolean hasShippingMethod( List shippingRates, String shippingMethod ) {
		for (int x = 0; x < shippingRates.size(); x++) {
			ShippingRate sr = (ShippingRate) shippingRates.get(x);

			if (shippingMethod.equals(sr.getBaseRateServiceLevel())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param sg
	 * @param shippingRates
	 * @return true or false
	 */
	public boolean validateShippingMethod(ShippingGroup sg, List shippingRates) {

		if (sg instanceof HardgoodShippingGroup) {
			HardgoodShippingGroup hsg = (HardgoodShippingGroup) sg;

			String shipType = (String) hsg.getPropertyValue(
					ShippingGroupConstants.ShippingGroupPropertyManager.SHIP_TYPE.getValue());
			String shipMethod = hsg.getShippingMethod();

			if (shipType != null && shipMethod != null) {
				if (ShipType.BOPIS.getValue().equalsIgnoreCase(shipType) && ShippingMethod.BOPIS
						.getMappedValue().equalsIgnoreCase(shipMethod)) {
					return true;
				}
				if (ShipType.BOSTS.getValue().equalsIgnoreCase(shipType) && ShippingMethod.BOSTS
						.getMappedValue().equalsIgnoreCase(shipMethod)) {
					return true;
				}
			}

			if (shippingRates != null && shipMethod != null) {
				return hasShippingMethod(shippingRates, shipMethod);
			} else if (hsg.getCommerceItemRelationshipCount() > 0) {
				return false;
			}
		}

		return true;
	}
	
	/** 
	 * Returns the current shipping method (assumes one hardgood shipping group)
	 * for the order. If there is no shipping method it returns
	 * defaultShippingMethod
	 * 
	 * @param order
	 * @return */
	public String getShippingMethod( Order order, String defaultShippingMethod ) {
		String shippingMethod = null;
		ShippingGroup shippingGroup = getFirstNonGiftCardShippingGroup( order );
		if( shippingGroup instanceof HardgoodShippingGroup ) {
			shippingMethod = shippingGroup.getShippingMethod();
		}
		if( DigitalStringUtil.isBlank( shippingMethod ) || DigitalStringUtil.equalsIgnoreCase( "hardgoodShippingGroup", shippingMethod ) ) {
			shippingMethod = defaultShippingMethod;
		}
		return shippingMethod;
	}
	
	/**
	 * Method used to set all shipping groups address from billing contact info.
	 * 
	 * @param contactInfo
	 * @param order
	 */
	public void setHgShippingAddress(ContactInfo contactInfo, Order order){
		if(contactInfo == null || order == null){
			return;
		}
		List<HardgoodShippingGroup> hgShippingGroups = getHardgoodShippingGroups(order);
		ContactInfo shippingAddr = null;
		if(hgShippingGroups!=null){
			for (HardgoodShippingGroup hgShippingGroup : hgShippingGroups){
				shippingAddr= (ContactInfo)hgShippingGroup.getShippingAddress();
				shippingAddr.setPhoneNumber(contactInfo.getPhoneNumber());
				shippingAddr.setEmail(contactInfo.getEmail());
					hgShippingGroup.setShippingAddress(shippingAddr);
			}
		}
	}

	/**
	 * Method used to set all shipping groups address from billing contact info.
	 *
	 * @param contactInfo
	 * @param order
	 */
	public void setEmailPhoneToShippingAddressIfEmpty(ContactInfo contactInfo, Order order){
		if(contactInfo == null || order == null){
			return;
		}
		@SuppressWarnings("unchecked")
		List<HardgoodShippingGroup> hgShippingGroups = getHardgoodShippingGroups(order);
		ContactInfo shippingAddr = null;
		if(hgShippingGroups!=null){
			for (HardgoodShippingGroup hgShippingGroup : hgShippingGroups){
				shippingAddr= (ContactInfo)hgShippingGroup.getShippingAddress();
				if(DigitalStringUtil.isBlank(shippingAddr.getPhoneNumber())) {
					shippingAddr.setPhoneNumber(contactInfo.getPhoneNumber());
				}
				if(DigitalStringUtil.isBlank(shippingAddr.getEmail())) {
					shippingAddr.setEmail(contactInfo.getEmail());
				}
				hgShippingGroup.setShippingAddress(shippingAddr);
			}
		}
	}

	/** 
	 * Returns the current shipping method (assumes one hardgood shipping group)
	 * for the order. If there is no shipping method it returns {@link DEFAULT_SHIPPING_METHOD}
	 * 
	 * @param order
	 * @return never null */
	public String getShippingMethod( Order order ) {
		return getShippingMethod( order, ShippingGroupConstants.DEFAULT_SHIPPING_METHOD );
	}
	
	/**
	 * 
	 * @param order
	 * @return
	 */
	public ShippingGroup getFirstNonGiftCardShippingGroup( Order order ) {
		if( order != null ) {
			List shippingGroups = order.getShippingGroups();
			for( int x = 0; x < shippingGroups.size(); x++ ) {
				if(!isGiftCardShippingGroup( (ShippingGroup) shippingGroups.get( x ))) {
					ShippingGroup shippingGroup = (ShippingGroup)shippingGroups.get( x );
					String shipType = (String)(((HardgoodShippingGroup) shippingGroup).getPropertyValue("shipType"));
					if ( shipType == null || "ship".equalsIgnoreCase(shipType)) {
							return shippingGroup; 
					}
				}
			}
		}
		return null;
	}
	
	/** 
	 * Returns all non gift card shipping groups for the order
	 * 
	 * @param order
	 * @return */
	public List<HardgoodShippingGroup> getNonGiftCardShippingGroups( Order order ) {
		List<HardgoodShippingGroup> retVal = new ArrayList<>();
		if( order != null ) {
			for( HardgoodShippingGroup shippingGroup : Iterables.filter( order.getShippingGroups(), HardgoodShippingGroup.class ) ) {
				if( !isGiftCardShippingGroup( shippingGroup ) ) {
					retVal.add( shippingGroup );
				}
			}
		}
		return retVal;
	}
	
	
	
	/** 
	 * This method check if there is any shippingGroup is attached to the order with the same shipType and storeId,
	 * if there is no shippingGroup attached to the order it will create one. It also adds the store address details
	 * while creating the shippingGroup
	 * 
	 * @param order
	 * @param shipType
	 * @param storeId */
	public ShippingGroup createShippingGroupWithStoreDetails( Order order, Profile profile, String shipType, String storeId ) throws CommerceException
	{	
		HardgoodShippingGroup firstShippingGroup = (HardgoodShippingGroup)this.getFirstNonGiftCardShippingGroup(order);
		ShippingGroup hShippingGroup = getOrCreateNonDefaultShippingGroup( order, shipType, storeId );
		if( isLoggingDebug() ){
			logDebug( "shipType ------------------>>>" + shipType );
		}
		
		if( hShippingGroup instanceof DigitalHardgoodShippingGroup ) {
			if( shipType.equalsIgnoreCase(ShippingGroupConstants.ShipType.BOPIS.getValue()) ) {
				hShippingGroup.setShippingMethod( ShippingMethod.BOPIS.getMappedValue() );				
			}else if( shipType.equalsIgnoreCase(ShippingGroupConstants.ShipType.BOSTS.getValue()) ) {
				hShippingGroup.setShippingMethod( ShippingMethod.BOSTS.getMappedValue());				
			}else {				
				hShippingGroup.setShippingMethod( ShippingMethod.getDefaultShippingMethod().getMappedValue());
			}

			if (DigitalStringUtil.isBlank(((HardgoodShippingGroup) hShippingGroup).getShippingAddress().getFirstName())
					|| DigitalStringUtil.isBlank(((HardgoodShippingGroup) hShippingGroup).getShippingAddress().getLastName())) {
				
				if(firstShippingGroup != null && firstShippingGroup.getShippingAddress() != null){ 
					
					if(DigitalStringUtil.isNotBlank(firstShippingGroup.getShippingAddress().getFirstName())
						|| DigitalStringUtil.isNotBlank(firstShippingGroup.getShippingAddress().getLastName())){
						((HardgoodShippingGroup) hShippingGroup).getShippingAddress().setFirstName(firstShippingGroup.getShippingAddress().getFirstName());
						((HardgoodShippingGroup) hShippingGroup).getShippingAddress().setLastName(firstShippingGroup.getShippingAddress().getLastName());
					}
					
					DigitalRepositoryContactInfo contactInfo = (DigitalRepositoryContactInfo)firstShippingGroup.getShippingAddress();
					// Setting Email for the bopis SG
					if ( !Strings.isNullOrEmpty(contactInfo.getEmail())) {
						((HardgoodShippingGroup) hShippingGroup).setPropertyValue(ShippingGroupConstants.EMAIL, contactInfo.getEmail());
					}
				}
			
				if (DigitalStringUtil.isBlank(((HardgoodShippingGroup) hShippingGroup).getShippingAddress().getFirstName())
						|| DigitalStringUtil.isBlank(((HardgoodShippingGroup) hShippingGroup).getShippingAddress().getLastName())) {
				
				
					// Setting First Name and Last Name for bopis Shipping Group
					RepositoryItem shippingAddress = (RepositoryItem) profile.getPropertyValue("shippingAddress");
					
					// Setting Email for the bopis SG
					if (shippingAddress != null && !Strings.isNullOrEmpty((String) shippingAddress.getPropertyValue(ShippingGroupConstants.EMAIL))) {
						((HardgoodShippingGroup) hShippingGroup).setPropertyValue(ShippingGroupConstants.EMAIL, (String) shippingAddress.getPropertyValue(ShippingGroupConstants.EMAIL));
					}
					
					if (shippingAddress != null
							&& DigitalStringUtil.isNotBlank((String) shippingAddress.getPropertyValue("firstName"))
							&& DigitalStringUtil.isNotBlank((String) shippingAddress.getPropertyValue("lastName"))) {
	
						((HardgoodShippingGroup) hShippingGroup).getShippingAddress().setFirstName((String) shippingAddress.getPropertyValue("firstName"));
						((HardgoodShippingGroup) hShippingGroup).getShippingAddress().setLastName((String) shippingAddress.getPropertyValue("lastName"));
					} else {
						
						// For PayPal express guest checkout, set it from billing
						// address, if exists
						ContactInfo billingAddr = null;
						List paymentGroupList = order.getPaymentGroups();
						if (paymentGroupList != null) {
							Iterator i = paymentGroupList.iterator();
							while (i.hasNext()) {
								PaymentGroup pg = (PaymentGroup) i.next();
								if (pg instanceof DigitalCreditCard) {
									DigitalCreditCard creditCard = (DigitalCreditCard) pg;
									billingAddr = (ContactInfo) creditCard.getBillingAddress();
								} else if (pg instanceof PaypalPayment) {
									PaypalPayment paypalPayment = (PaypalPayment) pg;
									billingAddr = paypalPayment.getBillingAddress();
								}
							}
						}
						if (billingAddr != null && DigitalStringUtil.isNotBlank(billingAddr.getFirstName())
								&& DigitalStringUtil.isNotBlank(billingAddr.getLastName())) {
							
							((HardgoodShippingGroup) hShippingGroup).getShippingAddress().setFirstName(billingAddr.getFirstName());
							((HardgoodShippingGroup) hShippingGroup).getShippingAddress().setLastName(billingAddr.getLastName());
						}
					}
				}
			}
			
			((DigitalHardgoodShippingGroup) hShippingGroup).setPropertyValue("shipType",shipType.toUpperCase());
			((DigitalHardgoodShippingGroup) hShippingGroup).setPropertyValue("storeId",storeId);

			String searchQuery = getFetchStoreQuery()+storeId+"'";
			RepositoryItem store = null;
			RepositoryItem[] stores = geoLocationUtil.findBySQL( searchQuery, "location" );
			if( stores != null ) {
				store = stores[0];
			}
			if( store != null ) {
				Address shippingAddress= ((DigitalHardgoodShippingGroup) hShippingGroup).getShippingAddress();
				shippingAddress.setAddress1((String)store.getPropertyValue("address1"));
				shippingAddress.setAddress2((String)store.getPropertyValue("address2"));
				shippingAddress.setAddress3((String)store.getPropertyValue("address3"));
				shippingAddress.setCity((String)store.getPropertyValue("city"));
				shippingAddress.setState((String)store.getPropertyValue("stateAddress"));
				shippingAddress.setPostalCode((String)store.getPropertyValue("postalCode"));
				shippingAddress.setCountry((String)store.getPropertyValue("country"));
				
				String phoneNumber = (String) store.getPropertyValue("phoneNumber");
				phoneNumber = phoneNumber.replace("-", "");
				((DigitalHardgoodShippingGroup) hShippingGroup).setPropertyValue("phoneNumber", phoneNumber);
				((DigitalHardgoodShippingGroup) hShippingGroup).setPropertyValue("addressVerification",true);
				String mallPlazaName =  (String) store.getPropertyValue("mallPlazaName");
				((DigitalHardgoodShippingGroup) hShippingGroup).setPropertyValue("mallPlazaName", mallPlazaName);

				String altPickUpFirstName, altPickUpLastName;

				DigitalOrderImpl dswOrder = (DigitalOrderImpl) order;
				altPickUpFirstName = dswOrder.getAltPickupFirstName();
				altPickUpLastName = dswOrder.getAltPickupLastName();

				if(DigitalStringUtil.isNotBlank(altPickUpFirstName)){
					shippingAddress.setFirstName(altPickUpFirstName);
				}

				if(DigitalStringUtil.isNotBlank(altPickUpLastName)){
					shippingAddress.setLastName(altPickUpLastName);
				}
				
			} 
		}
		if( isLoggingDebug() ){
			logDebug( "end of the method -------------->>> createShippingGroupFromProfile()" );
		}
		return hShippingGroup;
	}	
	
	/**
	 * 
	 * @param storeNumber
	 * @return
	 */
	public DigitalStoreAddress fetchStoreAddress(String storeNumber) {
		String searchQuery = getFetchStoreQuery()+ storeNumber+"'";
		RepositoryItem store = null;
		RepositoryItem[] stores = geoLocationUtil.findBySQL( searchQuery, "location" );
		DigitalStoreAddress storeAddress= new DigitalStoreAddress();
		if( stores != null ) {
			store = (RepositoryItem)stores[0];
		}
		if( store != null ) {
			storeAddress.setAddress1((String)store.getPropertyValue("address1"));
			storeAddress.setAddress2((String)store.getPropertyValue("address2"));
			storeAddress.setAddress3((String)store.getPropertyValue("address3"));
			storeAddress.setCity((String)store.getPropertyValue("city"));
			storeAddress.setStateAddress((String)store.getPropertyValue("stateAddress"));
			storeAddress.setPostalCode((String)store.getPropertyValue("postalCode"));
			storeAddress.setCountry((String)store.getPropertyValue("country"));
			String phoneNumber = (String) store.getPropertyValue("phoneNumber");
			phoneNumber = phoneNumber.replace("-", "");
			storeAddress.setPhoneNumber(phoneNumber);
			String mallPlazaName =  (String) store.getPropertyValue("mallPlazaName");
			storeAddress.setMallPlazaName(mallPlazaName);
		}
		return storeAddress;
	}

	/**
	 * 
	 * @param order
	 * @param shipType
	 * @param storeId
	 * @return
	 */
	private ShippingGroup getOrCreateNonDefaultShippingGroup( Order order, String shipType, String storeId) {
		if( isLoggingDebug() ) logDebug( "start of the method -------------->>> getShippingGroup()" );
		ShippingGroup shippingGroup = null;
		shippingGroup = getFirstNonGiftCardShippingGroup( order, shipType, storeId);
		if( !( shippingGroup instanceof HardgoodShippingGroup ) ) {
			try {
				shippingGroup = createShippingGroup();
				if( isLoggingDebug() ) logDebug( "The new non default ShippingGroup created is" + shippingGroup );
				order.addShippingGroup( shippingGroup );
				
				String sgId = shippingGroup.getId();
				
				if( !isShippingGroupInOrder( order, sgId ) ) {
					addShippingGroupToOrder( order, shippingGroup );
				}
			} catch( CommerceException cex ) {
				if( this.isLoggingError() ) logError( "Error occured while creating shippingGroup ----->>>" + cex );
				return null;
			}
		}

		if( isLoggingDebug() ) logDebug( "End of the method ------------->>> getShippingGroup() " );
		return shippingGroup;
	}	
		
	/**
	 * This method finds if a shipping group exists with requested shipType and storeId, else returns null
	 * 
	 * @param order
	 * @param shipType
	 * @param storeId
	 * @return
	 */
	public ShippingGroup getFirstNonGiftCardShippingGroup( Order order, String shipType, String storeId) {
		if( order != null ) {
			List shippingGroups = order.getShippingGroups();
			for( int x = 0; x < shippingGroups.size(); x++ ) {
				ShippingGroup shippingGroup = (ShippingGroup)shippingGroups.get( x );
				if( shippingGroup instanceof HardgoodShippingGroup && !isGiftCardShippingGroup( shippingGroup )) { 
					final HardgoodShippingGroup HGshippingGroup = (HardgoodShippingGroup)shippingGroup;
					String hgShipType = (HGshippingGroup.getPropertyValue("shipType") != null) ? (String)HGshippingGroup.getPropertyValue("shipType") : "";
					String hgStoreId = (HGshippingGroup.getPropertyValue("storeId") != null)? (String)HGshippingGroup.getPropertyValue("storeId") : "";
					if(!Strings.isNullOrEmpty(hgShipType) && hgShipType.equalsIgnoreCase(shipType) && hgStoreId.equalsIgnoreCase(storeId)) {
						if( isLoggingDebug() ) {
							logDebug("Found the existing SG in the order:"+shippingGroup);
						}
						return shippingGroup; 
					}
				}
			}
		}
		return null;
	}	
	
	/**
	 * 
	 * @param order
	 * @param shipType
	 * @return
	 */
	public boolean isAnyShipTypeShippingGroup( Order order, String shipType) {
		boolean found = false;
		if( order != null ) {
			List shippingGroups = order.getShippingGroups();
			for( int x = 0; x < shippingGroups.size(); x++ ) {
				ShippingGroup shippingGroup = (ShippingGroup)shippingGroups.get( x );
				if( !isGiftCardShippingGroup( shippingGroup ) ) { 
					final HardgoodShippingGroup HGshippingGroup = (HardgoodShippingGroup)shippingGroup;
					String hgShipType = (String)HGshippingGroup.getPropertyValue("shipType");
					if(DigitalStringUtil.isBlank(shipType) && DigitalStringUtil.isBlank(hgShipType)){
						found = true;
						break;
					}else  if(!Strings.isNullOrEmpty(hgShipType) && hgShipType.equalsIgnoreCase(shipType)) {
						if( isLoggingDebug() ) {
							logDebug("Found the existing SG in the order:"+shippingGroup);
						}found = true;
						break; 
					}
				}
			}
		}
		return found;
	}
	
	/**
	 * 
	 * @param shippingGroup
	 * @return
	 */
	public boolean isBopisBostsShippingGroup( ShippingGroup shippingGroup) {
		boolean yes = false;
		if( !isGiftCardShippingGroup( shippingGroup ) ) { 
			final HardgoodShippingGroup HGshippingGroup = (HardgoodShippingGroup)shippingGroup;
			String hgShipType = (String)HGshippingGroup.getPropertyValue("shipType");
			if(!Strings.isNullOrEmpty(hgShipType) && !ShipType.SHIP.getValue().equalsIgnoreCase(hgShipType)) {
				if( isLoggingDebug() ) {
					logDebug("Found the existing SG in the order:"+shippingGroup);
				}
				yes = true;
			}
		}
		return yes;
	}
	
	/**
	 * 
	 * @param order
	 * @return
	 */
	public ShippingGroup getFirstNonBopisBostsHardgoodShippingGroup( Order order ) {
		if( order != null ) {
			List<ShippingGroup> shippingGroups = order.getShippingGroups();
			
			for( ShippingGroup shippingGroup : shippingGroups ) {
				
				if(!this.isBopisBostsShippingGroup(shippingGroup) && !this.isGiftCardShippingGroup(shippingGroup)) {
					return shippingGroup;
				}
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param order
	 */
	public void updateShipAddrNameForBopisBostsOrder(Order order) {
		String firstName, lastName, middleName = null;

		DigitalOrderImpl dswOrder = (DigitalOrderImpl) order;
		firstName = dswOrder.getAltPickupFirstName();
		lastName = dswOrder.getAltPickupLastName();

		if (DigitalStringUtil.isBlank(firstName)) {
			HardgoodShippingGroup hg = (HardgoodShippingGroup) this.getFirstNonBopisBostsHardgoodShippingGroup(order);
			if (hg != null && !DigitalAddressUtil.isAddressEmpty(hg.getShippingAddress())) {
				Address addr = hg.getShippingAddress();
				firstName = addr.getFirstName();
				lastName = addr.getLastName();
				middleName = addr.getMiddleName();
			}
		}

		if (DigitalStringUtil.isBlank(firstName)) {
			ContactInfo billingAddress = null;
			try {
				billingAddress = ((DigitalOrderTools) this.getOrderTools()).getOrderBillingAddress(order);
			} catch (CommerceException e) {
				logError("Error getting Order Billing Addr", e);
			}
			if (!DigitalAddressUtil.isAddressEmpty(billingAddress)) {
				firstName = billingAddress.getFirstName();
				lastName = billingAddress.getLastName();
				middleName = billingAddress.getMiddleName();
			}
		}

		List<String> emptyStoreShippingGroupList = new ArrayList<>();
		// After Successful USPS call set the  ContactInfo to the Order Object
		List<ShippingGroup> shippingGroups = order.getShippingGroups();
		for (ShippingGroup sg : shippingGroups) {
			if (!(sg instanceof HardgoodShippingGroup)) {
				continue;
			}

			HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sg;
			String shipType = (String) hgSg.getPropertyValue(ShippingGroupPropertyManager.SHIP_TYPE.getValue());

			Collection<CommerceItemRelationship> relationships = sg.getCommerceItemRelationships();

			if (relationships == null || relationships.size() <= 0) {
				if (shipType != null && !shipType.equalsIgnoreCase(ShipType.SHIP.getValue())) {
					emptyStoreShippingGroupList.add(sg.getId());
				}
				continue;
			}

			ContactInfo shippingAddress = (ContactInfo) hgSg.getShippingAddress();

			if (shipType != null && !shipType.equalsIgnoreCase(ShipType.SHIP.getValue())) {
				//Also, If any BOPIS / BOSTS shipping first / last names are empty, set them from alternate pickup names
				// or hone shipping address or billing address
				shippingAddress.setFirstName(firstName);
				shippingAddress.setLastName(lastName);
				shippingAddress.setMiddleName(middleName);
				continue;
			}
		}

		// Remove if any empty store related shipping groups are present
		for(String id : emptyStoreShippingGroupList){
			try {
				removeShippingGroupFromOrder(dswOrder, id);
			} catch (CommerceException e) {
				logError("Error in removing empty shipping group " + id + " error message "
						+ e.getLocalizedMessage());
			}
		}
	}
	
	/**
	 * 
	 * @param order
	 * @return
	 */
	public List<HardgoodShippingGroup> getGiftCardShippingGroups( Order order ) {
		List<HardgoodShippingGroup> retVal = new ArrayList<>();
		if( order != null ) {
			for( HardgoodShippingGroup shippingGroup : Iterables.filter( order.getShippingGroups(), HardgoodShippingGroup.class ) ) {
				if( isGiftCardShippingGroup( shippingGroup ) ) {
					retVal.add( shippingGroup );
				}
			}
		}
		return retVal;
	}

	/**
	 * 
	 * @param order
	 * @return
	 */
	public List<ElectronicShippingGroup> getElectronicShippingGroups( Order order ) {
		List<ElectronicShippingGroup> retVal = new ArrayList<>();
		if( order != null ) {
			for( ElectronicShippingGroup shippingGroup : Iterables.filter( order.getShippingGroups(), ElectronicShippingGroup.class ) ) {

				retVal.add( shippingGroup );
			}
		}
		return retVal;
	}

	
	/**
	 * 
	 * @param pSrcOrder
	 * @param pDstOrder
	 * @param pGroup
	 * @return
	 */
	@Override
	protected HardgoodShippingGroup mergeOrdersCopyHardgoodShippingGroup(Order pSrcOrder, Order pDstOrder, HardgoodShippingGroup pGroup)
			     throws CommerceException{
	     
		HardgoodShippingGroup hsg = super.mergeOrdersCopyHardgoodShippingGroup(pSrcOrder, pDstOrder, pGroup);
	     if(pGroup instanceof DigitalHardgoodShippingGroup && 
	    		 hsg != null && hsg instanceof DigitalHardgoodShippingGroup){
	    	 
	    	 DigitalHardgoodShippingGroup pSrcSG = (DigitalHardgoodShippingGroup) pGroup;
	    	 DigitalHardgoodShippingGroup pDestSG = (DigitalHardgoodShippingGroup) hsg;
	    	 if(ShipType.BOPIS.getValue().equalsIgnoreCase(pSrcSG.getShipType()) ||
	    			 ShipType.BOSTS.getValue().equalsIgnoreCase(pSrcSG.getShipType())){
	    		 
	    		 pDestSG.setShippingMethod(pSrcSG.getShippingMethod());
	    		 pDestSG.setStoreId(pSrcSG.getStoreId());
	    		 pDestSG.setMallPlazaName(pSrcSG.getMallPlazaName());
	    	 }
	    	 
	    	 pDestSG.setState(pSrcSG.getState());
	     }

	     return hsg;
	}
	
	public boolean isGiftCardShippingGroup( ShippingGroup sg ) {
		return sg.getState() == this.getGiftCardGroupStateCode();
	}

	public void setShippingAddress(ContactInfo contactInfo, List<HardgoodShippingGroup> hgShippingGroups) {

		@SuppressWarnings("unchecked")
		ContactInfo shippingAddr = null;
		if (hgShippingGroups != null) {
			for (HardgoodShippingGroup hgShippingGroup : hgShippingGroups) {
				shippingAddr = (ContactInfo) hgShippingGroup.getShippingAddress();
				shippingAddr.setPhoneNumber(contactInfo.getPhoneNumber());
				if (DigitalStringUtil.isNotEmpty(contactInfo.getEmail())) {
					shippingAddr.setEmail(DigitalStringUtil.lowerCase(contactInfo.getEmail()));
				}
				hgShippingGroup.setShippingAddress(shippingAddr);
			}
		}

	}
}