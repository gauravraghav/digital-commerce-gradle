package com.digital.commerce.services.order.purchase;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.OrderServices;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.xml.GetException;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import lombok.Getter;
import lombok.Setter;
import org.dom4j.io.SAXReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class OrderXMLGenerator extends ApplicationLoggingImpl {
    private static final String XML_ENCODING = "UTF-8";
    private static final String ORDER_XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>";
    private static final String CREDIT_CARD_NUMBER_START_TAG = "<order:creditCard.creditCardNumber xsi:type=\"string\"><![CDATA[";
    private static final String CREDIT_CARD_NUMBER_END_TAG = "]]></order:creditCard.creditCardNumber>";
    private static final String CREDIT_CARD_TYPE_START_TAG = "<order:creditCard.creditCardType xsi:type=\"string\"><![CDATA[";
    private static final String CREDIT_CARD_TYPE_END_TAG = "]]></order:creditCard.creditCardType>";
    private static final String SHIPPINGGROUP_ADDRESS_TYPE_START_TAG = "<order:hardgoodShippingGroup.addressType xsi:type=\"string\"><![CDATA[";
    private static final String CREDIT_CARD_COUNTRY_START_TAG = "<order:creditCard.country xsi:type=\"string\"><![CDATA[";
    private static final String CREDIT_CARD_COUNTRY_END_TAG = "]]></order:creditCard.country>";
    private static final String SHIPPINGGROUP_ADDRESS_TYPE_USA = "<order:hardgoodShippingGroup.addressType xsi:type=\"string\"><![CDATA[USA]]></order:hardgoodShippingGroup.addressType>";
    private static final String SHIPPINGGROUP_ADDRESS_TYPE_USC = "<order:hardgoodShippingGroup.addressType xsi:type=\"string\"><![CDATA[USC]]></order:hardgoodShippingGroup.addressType>";
    private static final String ORIGIN_OF_ORDER_START_TAG = "<order:order.originOfOrder xsi:type=\"string\"><![CDATA[";
    private static final String ORIGIN_OF_ORDER_END_TAG = "]]></order:order.originOfOrder>";
    private static final String ATG_ORDER_RELATIONSHIP_ID = "repositoryId";

    private OrderServices orderServices;

    //Start : Added to Fix for LS-587 - Orders are failing in GIS through S12
    private boolean cleanOrderShippingRelationships; //kill switch
    private boolean cleanOrderPaygroupRelationships; //kill switch
    private boolean useSAXParserForCleanOrderRelationships; //change parser
    private String xpathForOrderShippingRelationships;
    private String xpathForOrderPaygroupRelationships;
    private String namespaceForOrderXML;
    private String namespacePrefixForOrderXML;
    private String s12CommerceItemOrderMapTemplateFile;
    private String s12SearchForString;
    private String s12ReplaceWithString;
    private DigitalServiceConstants dswConstants;
    private static final String SERVICE_NAME = "OrderXMLGenerator";

    public OrderXMLGenerator() {
        super(OrderXMLGenerator.class.getName());
    }

    /**
     * @param order
     * @return String
     * @throws CommerceException
     * @throws GetException
     */
    public String getOrderAsXML(final DigitalOrderImpl order) throws CommerceException, GetException {
        final String METHOD_NAME = "getOrderAsXML";
        try {
            DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);

            String retVal = null;
            final String orderAsXML = orderServices.getOrderAsXML(order.getId(), this.getS12CommerceItemOrderMapTemplateFile());

            final String encryptedOrderAsXML = encryptCreditCardNumber(order, orderAsXML);

            final String updatedOrderAsXML = changeCardType(order, encryptedOrderAsXML);

            final String updatedOriginOfOrderAsXML = changeOriginOfOrder(updatedOrderAsXML);

            final String updatedThreeDigitCountryCodeOrderAsXML = changeTwoDigitCountryCode(updatedOriginOfOrderAsXML);

            final String updatedAddressTypeOrderXML = changeShippingAddressType(updatedThreeDigitCountryCodeOrderAsXML);

            final StringBuilder orderAsFullXMLBuilder = new StringBuilder();
            orderAsFullXMLBuilder.append(ORDER_XML_HEADER);
            orderAsFullXMLBuilder.append(updatedAddressTypeOrderXML);

            final String orderAsFullXML = DigitalStringUtil.replace(orderAsFullXMLBuilder.toString(),
                    getS12SearchForString(), getS12ReplaceWithString());

            if (DigitalStringUtil.isNotBlank(orderAsFullXML)) {
                retVal = (new DigitalOrderXMLPostProcessor()).process(orderAsFullXML);
            }

            //Added to Fix for- Orders are failing in GIS through S12
            //retVal = this.cleanOrderRelationships(order, retVal);

            if (isLoggingDebug()) {
                logDebug("The order xml is:" + retVal);
            }
            return retVal;
        } finally {
            DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
        }
    }

    /**
     * @param orderAsXML
     * @return String - translated OriginOfOrder if special translation is needed
     */
    private String changeOriginOfOrder(String orderAsXML) {
        String retVal = orderAsXML;
        if (DigitalStringUtil.isNotBlank(orderAsXML)) {
            int startIndex = orderAsXML.indexOf(ORIGIN_OF_ORDER_START_TAG);
            int endIndex = orderAsXML.indexOf(ORIGIN_OF_ORDER_END_TAG);
            if (startIndex != -1) {
                StringBuilder buf = new StringBuilder();
                buf.append(orderAsXML.substring(0, startIndex + ORIGIN_OF_ORDER_START_TAG.length()));
                String originOfOrder = orderAsXML.substring(startIndex + ORIGIN_OF_ORDER_START_TAG.length(), endIndex);

                //get S12 mapping for originOfOrder.
                if (isLoggingDebug()) {
                    logDebug("originOfOrder Before Mapping :" + originOfOrder);
                }

                String mappedOriginOfOrder = this.getDswConstants().getOriginOfOrderMap().get(originOfOrder);

                if (isLoggingDebug()) {
                    logDebug("originOfOrder After Mapping :" + mappedOriginOfOrder);
                }

                if (DigitalStringUtil.isBlank(mappedOriginOfOrder)) {
                    if (isLoggingDebug()) {
                        logDebug("there is no special translation required for originOfOrder");
                    }
                    return retVal;
                }

                buf.append(mappedOriginOfOrder);
                buf.append(orderAsXML.substring(endIndex));
                retVal = buf.toString();
            } else {
                if (isLoggingDebug()) {
                    logDebug("failed to find the origin of order in order xml file:" + orderAsXML);
                }
            }
        }
        return retVal;
    }

    /**
     * @param order
     * @param orderAsXML
     * @return String
     */
    private String encryptCreditCardNumber(DigitalOrderImpl order, String orderAsXML) {
        String retVal = orderAsXML;
        if (DigitalStringUtil.isNotBlank(orderAsXML)) {
            int startIndex = orderAsXML.indexOf(CREDIT_CARD_NUMBER_START_TAG);
            int endIndex = orderAsXML.indexOf(CREDIT_CARD_NUMBER_END_TAG);
            if (startIndex != -1) {
                /*try {*/
                String cardNumber = orderAsXML.substring(startIndex + CREDIT_CARD_NUMBER_START_TAG.length(), endIndex);
                //TODO MK: cardNumber encryption to be revisited while working on Submitting the OrderXml to S12 Yantra queue in different JIRA card
                //String encryptedCardNumber = encryptor.encryptString( cardNumber );
                //retVal = orderAsXML.replaceAll( cardNumber, encryptedCardNumber );
                retVal = orderAsXML.replaceAll(cardNumber, cardNumber);
				/*} catch( EncryptorException e ) {
					logger.error( String.format( "Failed to encrypt credit card number in order %1$s of profile %2$s", order.getId(), order.getProfileId() ), e );
				}*/
            } else {
                if (isLoggingDebug()) {
                    logDebug("failed to find the credit card nubmer in order xml file:" + orderAsXML);
                }
            }
        }

        return retVal;
    }

    /**
     * @param order
     * @param orderAsXML
     * @return String
     */
    private String changeCardType(DigitalOrderImpl order, String orderAsXML) {
        String retVal = orderAsXML;
        if (DigitalStringUtil.isNotBlank(orderAsXML)) {
            int startIndex = orderAsXML.indexOf(CREDIT_CARD_TYPE_START_TAG);
            int endIndex = orderAsXML.indexOf(CREDIT_CARD_TYPE_END_TAG);
            if (startIndex != -1) {
                StringBuilder buf = new StringBuilder("");
                buf.append(orderAsXML.substring(0, startIndex + CREDIT_CARD_TYPE_START_TAG.length()));
                String cardType = orderAsXML.substring(startIndex + CREDIT_CARD_TYPE_START_TAG.length(), endIndex);

                //get DiscoverCardTypes and Visa mapping from component properties.
                if (isLoggingDebug()) {
                    logDebug("CardType Before Mapping :" + cardType);
                }

                String mappedCardType = this.getDswConstants().getCardTypesMap().get(cardType);

                if (isLoggingDebug()) {
                    logDebug("CardType After Mapping :" + mappedCardType);
                }

                buf.append(mappedCardType);
                buf.append(orderAsXML.substring(endIndex));
                retVal = buf.toString();
            } else {
                if (isLoggingDebug()) {
                    logDebug("failed to find the credit card type in order xml file:" + orderAsXML);
                }
            }
        }
        return retVal;
    }

    /**
     * Added fix for sending 3 digit country code for credit card country.
     *
     * @param orderAsXML
     * @return String
     */
    private String changeTwoDigitCountryCode(String orderAsXML) {
        String retVal = orderAsXML;
        if (DigitalStringUtil.isNotBlank(orderAsXML)) {
            int startIndex = orderAsXML.indexOf(CREDIT_CARD_COUNTRY_START_TAG);
            int endIndex = orderAsXML.indexOf(CREDIT_CARD_COUNTRY_END_TAG);
            if (startIndex != -1) {
                StringBuilder buf = new StringBuilder("");
                buf.append(orderAsXML.substring(0, startIndex + CREDIT_CARD_COUNTRY_START_TAG.length()));
                String countryCode = orderAsXML.substring(startIndex + CREDIT_CARD_COUNTRY_START_TAG.length(), endIndex);

                if (null != countryCode && DigitalStringUtil.isNotEmpty(countryCode) && countryCode.length() < 3) {
                    Locale locale = new Locale("", countryCode);
                    countryCode = locale.getISO3Country();

                }

                if (isLoggingDebug()) {
                    logDebug("ISO 3 Digit Country Code After Mapping :" + countryCode);
                }

                buf.append(countryCode);
                buf.append(orderAsXML.substring(endIndex));
                retVal = buf.toString();
            } else {
                if (isLoggingDebug()) {
                    logDebug("failed to find the Country Code in order xml file:" + orderAsXML);
                }
            }
        }
        return retVal;
    }

    /**
     * Added fix for sending USC for shipping address type in order xml
     *
     * @param orderAsXML
     * @return String
     */
    private String changeShippingAddressType(String orderAsXML) {
        String retVal = orderAsXML;
        if (DigitalStringUtil.isNotBlank(orderAsXML)) {
            int startIndex = orderAsXML.indexOf(SHIPPINGGROUP_ADDRESS_TYPE_START_TAG);
            if (startIndex != -1) {
                retVal = DigitalStringUtil.replace(retVal, SHIPPINGGROUP_ADDRESS_TYPE_USA, SHIPPINGGROUP_ADDRESS_TYPE_USC);
            } else {
                if (isLoggingDebug()) {
                    logDebug("failed to find the ShippingAddressType in order xml file:" + orderAsXML);
                }
            }
        }
        return retVal;
    }

    /**
     * Added to Fix for - Orders are failing in GIS through S12
     *
     * @param order
     * @param orderXmlStr
     * @return String
     */
    protected String cleanOrderRelationships(Order order, String orderXmlStr) {
        String returnXml = orderXmlStr;

        if (this.isCleanOrderPaygroupRelationships() || this.isCleanOrderShippingRelationships()) {
            if (this.isUseSAXParserForCleanOrderRelationships()) {
                returnXml = cleanOrderRelationshipsSAX(order, orderXmlStr);
            } else {
                returnXml = cleanOrderRelationshipsDOM(order, orderXmlStr);
            }
        }
        return returnXml;
    }

    /**
     * Added to Fix for  - Orders are failing in GIS through S12
     *
     * @param order
     * @param orderXmlStr
     * @return String
     */
    protected String cleanOrderRelationshipsSAX(Order order, String orderXmlStr) {
        String returnXML = orderXmlStr;
        ByteArrayInputStream input = null;
        final long startTime = System.currentTimeMillis();
        final String OPERATION_NAME = "cleanOrderRelationshipsSAX";
        try {
            StringBuilder xmlStringBuilder = new StringBuilder();
            xmlStringBuilder.append(orderXmlStr);
            input = new ByteArrayInputStream(xmlStringBuilder.toString().getBytes(XML_ENCODING));

            SAXReader saxBuilder = new SAXReader();
            org.dom4j.Document document = saxBuilder.read(input);

            Map<String, String> namespaceUris = new HashMap<>();
            namespaceUris.put(namespacePrefixForOrderXML, namespaceForOrderXML);
            boolean xmlChanged = false;
            if (this.isCleanOrderShippingRelationships()) {
                org.dom4j.XPath xpathShipping = document.createXPath(xpathForOrderShippingRelationships);
                xpathShipping.setNamespaceURIs(namespaceUris);
                List nodesShipping = xpathShipping.selectNodes(document);
                for (int i = 0; i < nodesShipping.size(); i++) {
                    org.dom4j.Node node = (org.dom4j.Node) nodesShipping.get(i);
                    if (node.getNodeType() == org.dom4j.Node.ELEMENT_NODE) {
                        org.dom4j.Element eElement = (org.dom4j.Element) node;
                        String repId = eElement.attributeValue(ATG_ORDER_RELATIONSHIP_ID);
                        try {
                            ShippingGroupCommerceItemRelationship rel = (ShippingGroupCommerceItemRelationship) order.getRelationship(repId);
                            CommerceItem ci = rel.getCommerceItem();
                            if (ci == null) {
                                node.detach();
                                xmlChanged = true;
                            }
                        } catch (RelationshipNotFoundException e) {
                            if (isLoggingError()) {
                                logError("OrderXMLGenerator :: cleanOrderRelationshipsSAX - Removing invalid ship-item relationship :: RelationshipID - "
                                        + repId + ", OrderId - " + order.getId());
                                logError(" Exception Message ::  " + e.getMessage());
                            }
                            node.detach();
                            xmlChanged = true;
                        }
                    }
                }
            }

            if (this.isCleanOrderPaygroupRelationships()) {
                org.dom4j.XPath xpathPaygroup = document.createXPath(xpathForOrderPaygroupRelationships);
                xpathPaygroup.setNamespaceURIs(namespaceUris);
                List nodesPaygroup = xpathPaygroup.selectNodes(document);
                org.dom4j.Node node = null;
                for (int i = 0; i < nodesPaygroup.size(); i++) {
                    node = (org.dom4j.Node) nodesPaygroup.get(i);
                    if (node.getNodeType() == org.dom4j.Node.ELEMENT_NODE) {
                        org.dom4j.Element eElement = (org.dom4j.Element) node;
                        String repId = eElement.attributeValue(ATG_ORDER_RELATIONSHIP_ID);
                        try {
                            order.getRelationship(repId);
                        } catch (RelationshipNotFoundException e) {
                            if (isLoggingError()) {
                                logError("OrderXMLGenerator :: cleanOrderRelationshipsSAX - Removing invalid paygroup relationship :: RelationshipID - "
                                        + repId + ", OrderId - " + order.getId());
                                logError(" Exception Message ::  " + e.getMessage());
                            }
                            node.detach();
                            xmlChanged = true;
                        }
                    }
                }
            }
            if (xmlChanged) {
                returnXML = document.asXML();
            }
        } catch (Exception ex) {
            if (isLoggingError()) {
                logError("Exception while checking for InvalidRelationships in the order :: ", ex);
            }
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ioe) {
                    logError("IOException while closing checking for InvalidRelationships in the order :: ", ioe);
                }
            }

            final long endTime = System.currentTimeMillis();
            final long executionTime = endTime - startTime;
            final StringBuilder sb = new StringBuilder();
            sb.append(SERVICE_NAME).append(" : ");
            sb.append(OPERATION_NAME).append(" : ");
            sb.append(executionTime).append("ms");
            logInfo(sb.toString());
        }
        return returnXML;
    }

    /**
     * Added to Fix for  - Orders are failing in GIS through S12
     *
     * @param order
     * @param orderXmlStr
     * @return String
     */
    protected String cleanOrderRelationshipsDOM(Order order, String orderXmlStr) {
        String returnXML = orderXmlStr;
        ByteArrayInputStream input = null;
        final long startTime = System.currentTimeMillis();
        final String OPERATION_NAME = "cleanOrderRelationshipsDOM";

        try {
            StringBuilder xmlStringBuilder = new StringBuilder();
            xmlStringBuilder.append(orderXmlStr);
            input = new ByteArrayInputStream(xmlStringBuilder.toString().getBytes(XML_ENCODING));

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            dbFactory.setNamespaceAware(true);
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            boolean xmlChanged = false;
            if (this.isCleanOrderShippingRelationships()) {
                NodeList nList = doc.getElementsByTagNameNS(namespaceForOrderXML, "relationshipsshipItemRel");

                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        String repId = eElement.getAttribute(ATG_ORDER_RELATIONSHIP_ID);
                        try {
                            ShippingGroupCommerceItemRelationship rel = (ShippingGroupCommerceItemRelationship) order.getRelationship(repId);
                            CommerceItem ci = rel.getCommerceItem();
                            if (ci == null) {
                                Node pNode = nNode.getParentNode();
                                pNode.removeChild(nNode);
                                xmlChanged = true;
                            }
                        } catch (RelationshipNotFoundException e) {
                            if (isLoggingError()) {
                                logError("OrderXMLGenerator :: cleanOrderRelationshipsDOM - Removing invalid ship-item relationship :: RelationshipID - "
                                        + repId + ", OrderId - " + order.getId());
                                logError(" Exception Message ::  " + e.getMessage());
                            }
                            Node pNode = nNode.getParentNode();
                            pNode.removeChild(nNode);
                            xmlChanged = true;
                        }
                    }
                }
            }

            if (this.isCleanOrderPaygroupRelationships()) {
                NodeList nList = doc.getElementsByTagNameNS(namespaceForOrderXML, "relationshipspayOrderRel");

                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        String repId = eElement.getAttribute(ATG_ORDER_RELATIONSHIP_ID);
                        try {
                            order.getRelationship(repId);

                        } catch (RelationshipNotFoundException e) {
                            if (isLoggingError()) {
                                logError("OrderXMLGenerator :: cleanOrderRelationshipsDOM - Removing invalid paygroup relationship :: RelationshipID - "
                                        + repId + ", OrderId - " + order.getId());
                                logError(" Exception Message ::  " + e.getMessage());
                            }
                            Node pNode = nNode.getParentNode();
                            pNode.removeChild(nNode);
                            xmlChanged = true;
                        }
                    }
                }
            }
            if (xmlChanged) {
                DOMSource domSource = new DOMSource(doc);
                try (StringWriter writer = new StringWriter()) {
                    StreamResult result = new StreamResult(writer);
                    TransformerFactory tf = TransformerFactory.newInstance();
                    Transformer transformer = tf.newTransformer();
                    transformer.transform(domSource, result);
                    returnXML = writer.toString();
                }
            }

        } catch (Exception ex) {
            if (isLoggingError()) {
                logError("Exception while checking for cleanOrderRelationships", ex);
            }
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ioe) {
                    logError("IOException while closing checking for InvalidRelationships in the order :: ", ioe);
                }
            }

            final long endTime = System.currentTimeMillis();
            final long executionTime = endTime - startTime;
            final StringBuilder sb = new StringBuilder();
            sb.append(SERVICE_NAME).append(" : ");
            sb.append(OPERATION_NAME).append(" : ");
            sb.append(executionTime).append("ms");
            logInfo(sb.toString());
        }

        return returnXML;
    }
}
