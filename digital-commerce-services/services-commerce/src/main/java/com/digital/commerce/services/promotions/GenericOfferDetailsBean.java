package com.digital.commerce.services.promotions;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by mk402314 on 12/10/2017.
 */
@Getter
@Setter
public class GenericOfferDetailsBean {
    String type ;
    String name;
    String description ;
    String startDate;
    String endDate;
    String offerCode;
    String certificateNumber;
    String value;
    String shopNowLink;
    String offerDetailsPageName;
    String iconType;
    Integer displayOrder;
}
