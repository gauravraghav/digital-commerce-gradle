package com.digital.commerce.services.common.web;

import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.OrderTools;
import atg.dtm.UserTransactionDemarcation;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import atg.servlet.pipeline.InsertableServletImpl;
import atg.userprofiling.Profile;

import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderManager;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import lombok.Getter;
import lombok.Setter;

import javax.servlet.ServletException;

import java.io.IOException;

import static com.digital.commerce.common.util.ComponentLookupUtil.SHOPPING_CART;
import static com.digital.commerce.common.util.ComponentLookupUtil.ORDER_TOOLS;
import static com.digital.commerce.common.util.ComponentLookupUtil.ORDER_MANAGER;

/**
 * This is an attempt at immediately removing tender related payment groups
 * (PayPal and Gift Card) as soon as a session is created.
 *
 * @author RS388534
 */
@Getter
@Setter
public class DigitalOrderCleanUpServlet extends InsertableServletImpl {
    private static final String PERF_NAME = "service";
    private static final String PERFORM_MONITOR_NAME = "DigitalOrderCleanUpServlet";
    private static final String PERFORM_MONITOR_NAME_2 = "DigitalOrderCleanUpServlet_OriginOfOrder";

    static final String CLASSNAME = DigitalOrderCleanUpServlet.class.getName();

    /**
     * To enable/disable the this servlet
     */
    private boolean enabled;
    private DigitalServiceConstants	dswConstants;

    private boolean originOfOrderUpdateEnabled;
    
    private String[] allowedServicesForOriginOfOrder;

    /**
     * Allowed services to do order cleanup
     */
    private String[] allowedServices;


    /**
     * @param pRequest
     * @param pResponse
     * @throws IOException,      if an error
     * @throws ServletException, if an error
     */
    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws IOException, ServletException {
        if (enabled) {

            DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, PERF_NAME);

            try {

                boolean cleanUpPaymentFlag = true;
                if (pRequest.getSession().getAttribute("CLEANUP_PAYMENT") != null) {
                    cleanUpPaymentFlag = (boolean) pRequest.getSession().getAttribute("CLEANUP_PAYMENT");
                }
                if (cleanUpPaymentFlag) {
                    String serviceURL = pRequest.getRequestURL().toString();
                    boolean cleanUpOfOrderAllowed = isCleanUpOfOrderAllowed(serviceURL);

                    if (!cleanUpOfOrderAllowed) {
                        if (isLoggingDebug()) {
                            logDebug("The DigitalOrderCleanUpServlet is not allowed for the service: " + serviceURL);
                        }
                    } else {
                        DigitalOrderImpl order = null;
                        DigitalProfileTools profileTools = (DigitalProfileTools) ComponentLookupUtil
                                .lookupComponent(ComponentLookupUtil.PROFILE_TOOLS);
                        if (profileTools != null) {
                            Profile profile = (Profile) ServletUtil.getCurrentUserProfile();
                            boolean userLoggedInFlag = profileTools.isLoggedIn(profile);
                            // Check if user is logged-in
                            if (!userLoggedInFlag) {
                                if (isLoggingDebug()) {
                                    logDebug("The DigitalOrderCleanUpServlet is attempting to cleanup previous " +
                                            "session payments for the service: " + serviceURL);
                                }
                                OrderHolder orderHolder = (OrderHolder) pRequest
                                        .resolveName(profileTools.getShoppingCartPath());
                                if (orderHolder != null) {
                                    order = (DigitalOrderImpl) orderHolder.getCurrent();
                                }
                                if (order != null) {
                                    profileTools.removePreviousPaymentsFromCurrentOrder(order, profile);
                                } else {
                                    logInfo("There is no order in the previous session no cleanup of payment groups is done");
                                }
                            } else {
                                if (isLoggingDebug()) {
                                    logDebug("The cleanup of payment groups is not allowed for logged-in user");
                                }
                            }
                        }

                        pRequest.getSession().setAttribute("CLEANUP_PAYMENT", false);
                    }
                }

            } catch (Exception e) {
                logError("Clean-up of previous session payment groups associated with the order failed");
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, PERF_NAME);
            }
        }

        if (originOfOrderUpdateEnabled) {
            String serviceUrl = pRequest.getRequestURL().toString();
            boolean orderUpdateAllowed = isCleanUpOfOriginOfOrderAllowed(serviceUrl);

            if (!orderUpdateAllowed) {
                if (isLoggingDebug()) {
                    logDebug("The DigitalOrderCleanUpServlet is not allowed for the service to update Order: " + serviceUrl);
                }
            } else {
                DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME_2, PERF_NAME);
                final String methodName = "originOfOrderUpdateEnabled";
                final UserTransactionDemarcation td = TransactionUtils.startNewTransaction(CLASSNAME, methodName);
                try {

                    boolean cleanUpOriginOfOrder = true;

                    if (pRequest.getSession().getAttribute("CLEANUP_ORIGIN_OF_ORDER") != null) {
                        cleanUpOriginOfOrder = (boolean) pRequest.getSession().getAttribute("CLEANUP_ORIGIN_OF_ORDER");
                    }
                    //TODO: Add logic to update the origin of order and also update tha attribute to false
                    if (cleanUpOriginOfOrder) {
                        OrderHolder shoppingCart = (OrderHolder) ComponentLookupUtil.lookupComponent(SHOPPING_CART, OrderHolder.class);
                        Order o = shoppingCart.getCurrent();
                        if (null != o) {
                            //Update the origin of order
                           DigitalOrderTools ot = (DigitalOrderTools) ComponentLookupUtil.lookupComponent(ORDER_TOOLS, DigitalOrderTools.class);
                           DigitalOrderManager om=(DigitalOrderManager)ComponentLookupUtil.lookupComponent(ORDER_MANAGER, DigitalOrderManager.class);
                            if (null != ot) {
                                ot.determineOriginOfOrder(o);
                                
                            	try {
                					synchronized (o) {
                						// update Order
                						om.updateOrder(o);
                						 pRequest.getSession().setAttribute("CLEANUP_ORIGIN_OF_ORDER", false);
                                         logInfo("CLEANUP_ORIGIN_OF_ORDER ::: DONE for session ID ::: " + pRequest.getSession().getId()+" For Service URL :: "+serviceUrl + " With ORIGIN_OF_ORDER value ::: "+o.getOriginOfOrder()+" For ORDER_ID :: " +o.getId());
                					}
                				} catch (Exception e) {
                					TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
                					throw e;
                				} finally {
                					try {
                						TransactionUtils.releaseTransactionLock(CLASSNAME, methodName);
                						TransactionUtils.endTransaction(td, CLASSNAME, methodName);
                					} catch (Throwable th) {
                						logError(th);
                					}
                				}
                            }
                        }
                    }

                } catch (Exception e) {
                    logError("Clean-up of origin of order with the order failed");
                    TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
                } finally {
        			TransactionUtils.endTransaction(td, CLASSNAME, methodName);
                    DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME_2, PERF_NAME);
                }
            }
        }

        // Move on in the pipeline all the times
        passRequest(pRequest, pResponse);
    }

    /**
     * @param serviceURL
     * @return true or false
     */
    private boolean isCleanUpOfOrderAllowed(String serviceURL) {
        if (DigitalStringUtil.isEmpty(serviceURL) || null == allowedServices) {
            return false;
        }
        for (String allowedServiceName : allowedServices) {
            if (serviceURL.contains(allowedServiceName)) {
                return true;
            }
        }
        return false;
    }

    private boolean isCleanUpOfOriginOfOrderAllowed(String serviceURL) {
        if (DigitalStringUtil.isEmpty(serviceURL) || null == allowedServicesForOriginOfOrder) {
            return false;
        }
        for (String allowedServiceName : allowedServicesForOriginOfOrder) {
            if (serviceURL.contains(allowedServiceName)) {
            	if(getDswConstants().isPaTool() && serviceURL.contains("InSoleLoginActor")){
            		return false;
            	}
                return true;
            }
        }
        return false;
    }
}
