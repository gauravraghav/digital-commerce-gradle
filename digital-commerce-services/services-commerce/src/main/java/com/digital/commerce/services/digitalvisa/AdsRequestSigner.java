package com.digital.commerce.services.digitalvisa;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;

import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.security.Base64Encoder;

public class AdsRequestSigner extends ApplicationLoggingImpl{
	public AdsRequestSigner() {
		super(AdsRequestSigner.class.getName());
	}

	/**
	 * @param postBody
	 * @param secretKey
	 * @return
	 * @throws DigitalAppException
	 */
	public String sign(String postBody, String secretKey)
			throws DigitalAppException {
		final String METHOD_NAME = "sign";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME);
			return unfriendlySign(postBody, secretKey);
		} catch (InvalidKeyException | NoSuchAlgorithmException e) {
			throw new DigitalAppException(e);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME);
		}
	}

	/**
	 * @param plaintext
	 * @param secretKey
	 * @return
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	private String unfriendlySign(String plaintext, String secretKey)
			throws InvalidKeyException, NoSuchAlgorithmException {
		Mac hmac_sha256 = Mac.getInstance("HmacSHA256");
		byte[] secretKeyBytes = secretKey.getBytes();
		SecretKeySpec keySpec = new SecretKeySpec(secretKeyBytes, "HmacSHA256");
		hmac_sha256.init(keySpec);
		byte[] signature = hmac_sha256.doFinal(plaintext.getBytes());
    return new Base64Encoder().encode(signature);
	}

}
