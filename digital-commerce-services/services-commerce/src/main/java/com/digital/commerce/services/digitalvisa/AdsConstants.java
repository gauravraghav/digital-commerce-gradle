package com.digital.commerce.services.digitalvisa;

public interface AdsConstants {
	public static String ADS_AUTH_NOT_A_LOYALTY__NUMBER = "USER_NOT_A_LOYALTY__NUMBER";	
	public static String ADS_AUTH_NULL_SECRET_KEY = "NULL_SECRET_KEY";
}
