package com.digital.commerce.services.order.payment.paypal.valueobject;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PaypalLookupResponse extends PaypalResponse implements Serializable {

	private static final long	serialVersionUID	= -6169394151571515391L;
	String						payload;
	String						acsUrl;
	String						enrolled;
	String 						termURL;
}
