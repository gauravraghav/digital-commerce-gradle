package com.digital.commerce.services.order.payment.paypal;

import static com.digital.commerce.common.util.ComponentLookupUtil.SHOPPING_CART;

import atg.commerce.order.OrderHolder;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.constants.PaypalConstants.PaypalPaymentGroupPropertyManager;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroupImpl;
import atg.commerce.order.RepositoryContactInfo;
import atg.core.util.ContactInfo;
import lombok.Getter;
import lombok.Setter;

/** 
 * New payment group for PayPal.
 */
@Getter
@Setter
public class PaypalPayment extends PaymentGroupImpl implements PaypalPaymentInfo {
	private static final long	serialVersionUID	= 1L;
	
	public PaypalPayment() {
    }
	
	private String ipAddress;
	private ContactInfo billingAddress = null;
	private boolean	newUser	= true;

	public void setBillingAddress(ContactInfo pBillingAddress) {
		if (pBillingAddress instanceof RepositoryContactInfo) {
			if (this.billingAddress != null)
				this.billingAddress.deleteObservers();
			this.billingAddress = pBillingAddress;
			this.billingAddress.addObserver(this);
		} else {
			try {
				OrderTools.copyAddress(pBillingAddress, this.billingAddress);
			} catch (CommerceException e) {
				throw new RuntimeException(e.getMessage());
			}
		}

		setSaveAllProperties(true);
	}

	/** PayerId assigned to the Consumer by PayPal.
	 * 
	 * @return */
	public String getPaypalPayerId() {
		return (String)getPropertyValue( PaypalPaymentGroupPropertyManager.PAYPAL_PAYER_ID.getValue());
	}

	public void setPaypalPayerId( String paypalPayerId ) {
		setPropertyValue( PaypalPaymentGroupPropertyManager.PAYPAL_PAYER_ID.getValue(), paypalPayerId );
	}

	/** EMail address of the Consumer used at PayPal.
	 * 
	 * @return */
	public String getPaypalEmail() {
		return (String)getPropertyValue( PaypalPaymentGroupPropertyManager.PAYPAL_EMAIL.getValue() );
	}

	public void setPaypalEmail( String paypalEmail ) {
		setPropertyValue( PaypalPaymentGroupPropertyManager.PAYPAL_EMAIL.getValue(), paypalEmail );
	}

	/** Cardinal generated order identifier
	 * 
	 * @return */
	public String getCardinalOrderId() {
		return (String)getPropertyValue( PaypalPaymentGroupPropertyManager.CARDINAL_ORDER_ID.getValue() );
	}

	public void setCardinalOrderId( String cardinalOrderId ) {
		setPropertyValue( PaypalPaymentGroupPropertyManager.CARDINAL_ORDER_ID.getValue(), cardinalOrderId );
	}

	public Order getOrder() {
		OrderHolder orderHolder =  ComponentLookupUtil.lookupComponent( SHOPPING_CART, OrderHolder.class  );
		if(orderHolder != null){
			return orderHolder.getCurrent();
		}else{
			return null;
		}
	}

}
