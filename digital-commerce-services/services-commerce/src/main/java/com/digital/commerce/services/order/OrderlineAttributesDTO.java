package com.digital.commerce.services.order;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import com.digital.commerce.integration.order.processor.domain.Person;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderlineAttributesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7993617620991421533L;

	private String itemSize = "";

	private String productId;

	private String itemDesc = "";
	private String itemColor = "";
	private String itemQuantity = "";
	private String itemUnitPrice = "";
	private String itemStyle = "";
	private String itemWidth = "";

	private String itemShipType = "";
	private String itemBrand = "";
	private String itemUPC = "";
	private String itemImageURL = "";
	private Person person;
	private String mallPlazaName = "";
	private String trackingNumber = "";
	private Date updateDate;
	private String shipCode;

	private String locationId;

	private String status;
	private String statusUpdateDate;
	private String statusCode;
	private String orderReleaseStatusKey;
	private String orderReleaseKey;

	private String itemId;
	private String itemName;
	private String colorCode;
	private String lineDiscounts;
	private String itemShippingMethod;

	private String narwarUrl;

	private String itemStatusQuantity;

	private Map<String, String> itemPromoList;

	private String fulfillmentType;
	private String recipientEmail;
	private String recipientName;
	private String senderEmail;
	private String senderName;
	private String productTitle;

}
