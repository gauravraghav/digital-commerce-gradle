package com.digital.commerce.services.rewards.domain;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

/*
    @Author PR412029
 */
@Getter
@Setter
@ToString
public class RewardsShopforItem implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final Comparator<RewardsShopforItem> CREATE_DATE_COMPARATOR =
        new Comparator<RewardsShopforItem>() {
            @Override
            public int compare(RewardsShopforItem shopforItem1, RewardsShopforItem shopforItem2) {
                int retVal = 0;
                if (shopforItem1 != null && shopforItem2 != null) {
                    retVal = ComparisonChain.start()
                        .compare(shopforItem1.getDateCreated(), shopforItem2.getDateCreated(),
                            Ordering.natural().nullsLast())
                        .result();
                }
                return retVal;
            }
        };

    private String  shopforID;
    private String  profileID;
    private boolean isActive;
    private String  rewardsShopforID;
    private String  shopforName;
    private String  relationship;
    private String  birthDay;
    private String  birthMonth;
    private String  webType;
    private String  gender;
    private String sizeGroup;
    private String[] sizes;
    private String[] widths;
    private Date  dateCreated;

}
