package com.digital.commerce.services.order.purchase.valueobject;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Generated("com.robohorse.robopojogenerator")
public class DigitalCommitOrderRequest{

	@JsonProperty("order")
	private DigitalOrderRequest order;
}