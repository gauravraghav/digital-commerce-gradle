package com.digital.commerce.services.order.listner;

import static com.digital.commerce.common.util.DigitalDateUtil.YANTRA_ORDER_STATUS_DATE_TIMEZONE_FORMAT;

import atg.commerce.CommerceException;
import atg.commerce.catalog.CatalogTools;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.OrderTools;
import atg.dtm.UserTransactionDemarcation;
import atg.multisite.SiteContextManager;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.userprofiling.email.TemplateEmailInfo;
import atg.userprofiling.email.TemplateEmailSender;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.communication.CommunicationService;
import com.digital.commerce.integration.common.communication.domain.Recipient;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent.ContentType;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.integration.order.processor.OrderStatusListener;
import com.digital.commerce.integration.order.processor.domain.Award;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.OrderStatus;
import com.digital.commerce.integration.order.processor.domain.Person;
import com.digital.commerce.integration.order.processor.domain.ShipmentLine;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.services.order.DigitalOrderHistoryModel;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderSummary;
import com.digital.commerce.services.order.OrderlineAttributesDTO;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.order.status.DigitalOrderStatusTools;
import com.digital.commerce.services.order.status.DigitalOrderStatusUtil;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.storelocator.DigitalStoreAddress;
import com.digital.commerce.services.storelocator.DigitalStoreTools;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.SerializationUtils;

@SuppressWarnings({"rawtypes", "unchecked"})
@Getter
@Setter
public abstract class DigitalOrderStatusListener extends ApplicationLoggingImpl implements OrderStatusListener {

    public final static String BOPIS = ShippingGroupConstants.ShipType.BOPIS.name();
    public final static String BOSTS = ShippingGroupConstants.ShipType.BOSTS.name();
    public final static String SHIP = ShippingGroupConstants.ShipType.SHIP.name();

    private DigitalProfileTools profileTools;
    private TemplateEmailSender templateEmailSender;
    private TemplateEmailInfo templateEmailInfo;
    private OrderTools orderTools;
    private DigitalOrderSummary dswosum;
    private CommunicationService communicationService;
    private DigitalStoreTools storeTools;
    private CatalogTools catalogTools;
    private Repository orderRepository;
    private DigitalOrderStatusTools orderStatusTools;
    private CommerceItemManager itemManager;

    private boolean cs14OrderUpdateFlag = true;
    private String listenerType;
    private String communicationKey;
    private String alternatePersonCommunicationKey;
    private DigitalServiceConstants digitalConstants;

    private static Map<String, Integer> shipMethodSort = new HashMap<>();

    static {
        shipMethodSort.put("SGRN", 3);
        shipMethodSort.put("GRN", 3);
        shipMethodSort.put("2ND", 2);
        shipMethodSort.put("S2ND", 2);
        shipMethodSort.put("SF2ND", 2);
        shipMethodSort.put("NDA", 1);
        shipMethodSort.put("SNDA", 1);
        shipMethodSort.put("SFND", 1);

        shipMethodSort.put("Cancelled", 1);

        shipMethodSort.put("Return Received", 1);
        shipMethodSort.put("Shipped", 1);

    }

    private List<String> supportedEventTypes;

    /**
     *
     * @param orderStatusEvent
     * @throws DigitalIntegrationException
     */
    @Override
    public void handleOrderStatusEvent(OrderStatusEvent orderStatusEvent) throws DigitalIntegrationException {
        OrderStatusPayload payLoad = (OrderStatusPayload) orderStatusEvent.getPayload();
        String emailAddress = this.getEmailTo(payLoad);
        if (DigitalStringUtil.isEmpty(emailAddress)) {
            throw new DigitalIntegrationException("Email Address is not present in the payload", "EMAIL_ADDRESS_EMPTY_CS14", "EMAIL_ADDRESS_EMPTY_CS14");
        }

        Map params = new HashMap();
        String orderNo = this.getOrderNo(payLoad);
        DigitalOrderImpl order = null;
        try {

            MultiSiteUtil.setPushSiteContext(payLoad.getSiteId());

            List<OrderLine> orderLines = this.getOrderLines(payLoad);

            List<OrderLine> orderSummaryLines = payLoad.getSummaryOrderLines();

            if (DigitalStringUtil.isNotBlank(orderNo) &&
                    getOrderTools().getOrderLookupService().getOrderManager().orderExists(orderNo)) {
                order = (DigitalOrderImpl) this.getOrderTools().getOrderLookupService().getOrderManager().loadOrder(orderNo);
            }

            this.handleOrderUpdate(payLoad, order);

            RepositoryItem orderHistoryItem = getOrderHistoryItem(orderNo);


            this.putLoyaltyNumber(payLoad, orderHistoryItem, (HashMap) params);
            this.putOrderDetail(payLoad, (HashMap) params);

            boolean isSNDAShippingMethod = false;
            boolean isS2NDShippingMethod = false;
            boolean isSF2NDShippingMethod = false;
            boolean isSGRNShippingMethod = false;

            if (orderLines != null) {
                int overAllOrderQnty = 0;
                int overNonEGCOrderQnty = 0;
                int shippedItemQnty = 0;
                for (OrderLine orderLine : orderLines) {
                    if (DigitalStringUtil.isNotEmpty(orderLine.getOrderedQty())) {
                        try {
                            overAllOrderQnty += Double.valueOf(orderLine.getOrderedQty()).intValue();

                            if (!"e-GC".equalsIgnoreCase(orderLine.getItemStyle())) {
                                overNonEGCOrderQnty += Double.valueOf(orderLine.getOrderedQty()).intValue();
                            }
                        } catch (NumberFormatException e) {
                            //Do nothing skip
                        }
                    }

                    if (DigitalStringUtil.isNotEmpty(orderLine.getStatusQuantity())
                            && ("3700".equalsIgnoreCase(orderLine.getMaxLineStatus())
                            || "2160".equalsIgnoreCase(orderLine.getStatus()))
                            || "2160".equalsIgnoreCase(orderLine.getMaxLineStatus())) {
                        try {
                            shippedItemQnty += Double.valueOf(orderLine.getStatusQuantity()).intValue();
                        } catch (NumberFormatException e) {
                            // Do nothing skip
                        }
                    }

                    if (DigitalStringUtil.isNotBlank(orderLine.getItemId()) &&
                            !"STANDARD-GC".equalsIgnoreCase(orderLine.getItemId()) &&
                            !"PERSONALIZED-GC".equalsIgnoreCase(orderLine.getItemId()) &&
                            !"e-GC".equalsIgnoreCase(orderLine.getItemId())) {

                        if (DigitalStringUtil.isNotBlank(orderLine.getCarrierServiceCode())) {
                            params.put("shippingMethod", orderLine.getCarrierServiceCode());
                        }

                    } else if (DigitalStringUtil.isNotEmpty(orderLine.getCarrierServiceCode()) && (params.get("shippingMethod") == null ||
                            DigitalStringUtil.isBlank((String) params.get("shippingMethod")))) {
                        if ("SNDA".equalsIgnoreCase(orderLine.getCarrierServiceCode()) || "SFND".equalsIgnoreCase(orderLine.getCarrierServiceCode())) {
                            isSNDAShippingMethod = true;
                        }

                        if ("S2ND".equalsIgnoreCase(orderLine.getCarrierServiceCode())) {
                            isS2NDShippingMethod = true;
                        }

                        if ("SF2ND".equalsIgnoreCase(orderLine.getCarrierServiceCode())) {
                            isSF2NDShippingMethod = true;
                        }

                        if ("SGRN".equalsIgnoreCase(orderLine.getCarrierServiceCode())) {
                            isSGRNShippingMethod = true;
                        }
                    }
                }

                if (isSNDAShippingMethod) {
                    params.put("shippingMethod", "SNDA");
                } else if (isS2NDShippingMethod) {
                    params.put("shippingMethod", "S2ND");
                } else if (isSGRNShippingMethod) {
                    params.put("shippingMethod", "SGRN");
                } else if (isSF2NDShippingMethod){
                    params.put("shippingMethod", "SF2ND");
                }

                if ((isSNDAShippingMethod && isSGRNShippingMethod) || (isS2NDShippingMethod && isSGRNShippingMethod)) {
                    params.put("showMultiShipmentMsg", true);
                }

                params.put("totalNonEGCCommerceCount", overNonEGCOrderQnty);
                params.put("totalCommerceCount", overAllOrderQnty);
                params.put("totalShippedItemCountInWords", DigitalStringUtil.numToWord(shippedItemQnty) + " (" + shippedItemQnty + ")");
            }

            HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType = this.categorizeOrderLines(orderLines, (HashMap) params);

            sortByStatusDate(itemsByFullfilmentType, ShippingGroupConstants.ShipType.BOPIS.name());
            sortByStatusDate(itemsByFullfilmentType, ShippingGroupConstants.ShipType.BOSTS.name());
            sortByStatusDate(itemsByFullfilmentType, ShippingGroupConstants.ShipType.SHIP.name());

            HashMap<String, Object> payLoadExtraData = this.handleEmailNotification(payLoad, order, itemsByFullfilmentType);

            if (payLoad.getPersonInfoMarkFor() != null) {
                payLoadExtraData.put("pickupPerson", payLoad.getPersonInfoMarkFor().getFirstName() + " "
                        + payLoad.getPersonInfoMarkFor().getLastName());
                if (DigitalStringUtil.isNotEmpty(payLoad.getPersonInfoMarkFor().getFirstName())) {
                    payLoadExtraData.put("altPickupFirstName", payLoad.getPersonInfoMarkFor().getFirstName());
                }
                if (DigitalStringUtil.isNotEmpty(payLoad.getPersonInfoMarkFor().getLastName())) {
                    payLoadExtraData.put("altPickupLastName", payLoad.getPersonInfoMarkFor().getLastName());
                }
            }

            params.put("payLoadItems", payLoadExtraData);

            this.putLocationInformation(itemsByFullfilmentType, (HashMap) params);
            params.put("fullfilmentItems", itemsByFullfilmentType);

            params.put("locale", payLoad.getLocale());
            params.put("siteUrl", MultiSiteUtil.getSiteURL());
            params.put("findStoreUrl", MultiSiteUtil.getSiteURL() + MultiSiteUtil.getStoreLocatorURI());
            params.put("emailSignUpUrl", MultiSiteUtil.getSiteURL() + MultiSiteUtil.getEmailSignUpURI());
            params.put("trackingDate", DigitalDateUtil.getOperationEmailsTrackingDate());

            HashMap<String, HashMap<String, OrderlineAttributesDTO>> summaryItemsByFullfilmentType = this.categorizeOrderLines(orderSummaryLines, null);

            removePayloadItemsFromOrderSummary(summaryItemsByFullfilmentType, itemsByFullfilmentType);

            sortByStatusDate(summaryItemsByFullfilmentType, ShippingGroupConstants.ShipType.BOPIS.name());
            sortByStatusDate(summaryItemsByFullfilmentType, ShippingGroupConstants.ShipType.BOSTS.name());
            sortByStatusDate(summaryItemsByFullfilmentType, ShippingGroupConstants.ShipType.SHIP.name());

            handleOrderSummaryEmail(payLoad, order, summaryItemsByFullfilmentType, params);

            ArrayList<Recipient> emailAddrs = new ArrayList<>();
            Recipient recipient = new Recipient();
            params.put("emailAddress", emailAddress);
            recipient.setAddress(emailAddress);
            if (payLoadExtraData != null) {
                recipient.setName((String) payLoadExtraData.get("customerFirstName"));
            }
            recipient.setRecipientType(Recipient.RecipientType.TO);
            emailAddrs.add(recipient);
            if (payLoadExtraData != null && payLoadExtraData.get("altPickupEmail") != null
                    && !emailAddress.equalsIgnoreCase((String) payLoadExtraData.get("altPickupEmail"))) {
                ArrayList<Recipient> alternativeEmailAddrs = new ArrayList<>();
                Recipient alternamteRecipient = new Recipient();
                alternamteRecipient.setAddress((String) payLoadExtraData.get("altPickupEmail"));
                alternamteRecipient.setRecipientType(Recipient.RecipientType.TO);
                alternativeEmailAddrs.add(alternamteRecipient);
                params.put("isAlternatePickupPerson", "true");
                this.getCommunicationService().send(alternativeEmailAddrs, alternatePersonCommunicationKey, params, ContentType.HTML, SiteContextManager.getCurrentSiteId());
            }
            params.put("eventType", payLoad.getEventType());
            this.getCommunicationService().send(emailAddrs, this.getCommunicationKey(), params, ContentType.HTML, SiteContextManager.getCurrentSiteId());

        } catch (CommerceException e) {
            logError("CommerceException while handleOrderStatusEvent :: ", e);
        }
    }

    /**
     *
     * @param summaryItemsByFullfilmentType
     * @param itemsByFullfilmentType
     */
    protected void removePayloadItemsFromOrderSummary(HashMap<String,
            HashMap<String, OrderlineAttributesDTO>> summaryItemsByFullfilmentType,
                                                      HashMap<String,
                                                              HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {

        for (String fullfilmentType : itemsByFullfilmentType.keySet()) {
            Map<String, OrderlineAttributesDTO> payloadItems = itemsByFullfilmentType.get(fullfilmentType);
            for (String payloadKey : payloadItems.keySet()) {
                OrderlineAttributesDTO payloadOrderLine = payloadItems.get(payloadKey);
                String payloadOrderLineStatus = payloadOrderLine.getStatusCode();
                String payloadStatusUpdateDate = payloadOrderLine.getStatusUpdateDate();
                String payloadReleaseKey = payloadOrderLine.getOrderReleaseKey();
                String payloadTrackingNumber = payloadOrderLine.getTrackingNumber();
                String payloadStatusQuantity = payloadOrderLine.getItemStatusQuantity();


                for (String orderSummaryFullfilmentTypeKey : summaryItemsByFullfilmentType.keySet()) {
                    Map<String, OrderlineAttributesDTO> orderSummaryItems = summaryItemsByFullfilmentType.get(orderSummaryFullfilmentTypeKey);
                    boolean found = false;
                    for (String orderSummaryKey : orderSummaryItems.keySet()) {
                        OrderlineAttributesDTO orderSummaryLineItem = orderSummaryItems.get(orderSummaryKey);
                        String orderSummaryStatus = orderSummaryLineItem.getStatusCode();
                        String orderSummaryStatusUpdateDate = orderSummaryLineItem.getStatusUpdateDate();
                        String orderSummaryReleaseKey = orderSummaryLineItem.getOrderReleaseKey();
                        String orderSummaryTrackingNumber = orderSummaryLineItem.getTrackingNumber();
                        String orderSummaryStatusQuantity = orderSummaryLineItem.getItemStatusQuantity();

                        if (DigitalStringUtil.isNotBlank(payloadOrderLineStatus) && orderSummaryStatus.equalsIgnoreCase(payloadOrderLineStatus) &&
                                DigitalStringUtil.isNotBlank(orderSummaryKey) && orderSummaryKey.equalsIgnoreCase(payloadKey) &&
                                DigitalStringUtil.isNotBlank(orderSummaryStatusUpdateDate) && orderSummaryStatusUpdateDate.equalsIgnoreCase(payloadStatusUpdateDate) &&
                                DigitalStringUtil.isNotBlank(orderSummaryStatusQuantity) && orderSummaryStatusQuantity.equalsIgnoreCase(payloadStatusQuantity) &&
                                DigitalStringUtil.isNotBlank(payloadReleaseKey) && payloadReleaseKey.equalsIgnoreCase(orderSummaryReleaseKey)) {

                            if (DigitalStringUtil.isNotBlank(payloadTrackingNumber)) {
                                if (payloadTrackingNumber.equalsIgnoreCase(orderSummaryTrackingNumber)) {
                                    orderSummaryItems.remove(orderSummaryKey);
                                }
                            } else {
                                orderSummaryItems.remove(orderSummaryKey);
                            }
                            found = true;
                            break;
                        }
                    }

                    if (found) {
                        break;
                    }
                }
            }
        }

    }

    /**
     *
     * @param payLoad
     * @param order
     * @param summaryItemsByFullfilmentType
     * @param params
     */
    protected void handleOrderSummaryEmail(OrderStatusPayload payLoad, DigitalOrderImpl order,
                                           HashMap<String, HashMap<String, OrderlineAttributesDTO>> summaryItemsByFullfilmentType, Map params) {

        HashMap<String, OrderlineAttributesDTO> summaryBopisItems = summaryItemsByFullfilmentType.get(ShippingGroupConstants.ShipType.BOPIS.name());
        HashMap<String, OrderlineAttributesDTO> summaryBostsItems = summaryItemsByFullfilmentType.get(ShippingGroupConstants.ShipType.BOSTS.name());
        HashMap<String, OrderlineAttributesDTO> summaryShipItems = summaryItemsByFullfilmentType.get(ShippingGroupConstants.ShipType.SHIP.name());

        HashMap<String, Object> summaryShipExtraAttributes = new HashMap<>();

        if (summaryShipItems != null && summaryShipItems.size() > 0) {

            for (OrderlineAttributesDTO summaryShipItem : summaryShipItems.values()) {
                if (summaryShipItem.getPerson() != null) {
                    summaryShipExtraAttributes.put("customerFirstName", summaryShipItem.getPerson().getFirstName());
                    // get Shipping Address Details
                    summaryShipExtraAttributes.put("company", summaryShipItem.getPerson().getCompany());
                    summaryShipExtraAttributes.put("addressLine1", summaryShipItem.getPerson().getAddressLine1());
                    summaryShipExtraAttributes.put("addressLine2", summaryShipItem.getPerson().getAddressLine2());
                    summaryShipExtraAttributes.put("city", summaryShipItem.getPerson().getCity());
                    summaryShipExtraAttributes.put("state", summaryShipItem.getPerson().getState());
                    summaryShipExtraAttributes.put("zipCode", summaryShipItem.getPerson().getZipCode());
                    summaryShipExtraAttributes.put("country", summaryShipItem.getPerson().getCountry());
                    summaryShipExtraAttributes.put("dayPhone", summaryShipItem.getPerson().getDayPhone());
                    break;
                }
            }

            summaryShipExtraAttributes.put("orderLines", summaryShipItems);

            boolean isSNDAShippingMethod = false;
            boolean isS2NDShippingMethod = false;
            boolean isSF2NDShippingMethod = false;
            boolean isSGRNShippingMethod = false;

            int overNonEGCOrderQnty = 0;

            Object totalNonEGCCommerceCountValue = params.get("totalNonEGCCommerceCount");

            int overAllOrderQnty = 0;

            Object totalOverAllOrderQntyValue = params.get("totalCommerceCount");

            try {
                if (totalNonEGCCommerceCountValue != null) {
                    overNonEGCOrderQnty = (Integer) totalNonEGCCommerceCountValue;
                }

                if (totalOverAllOrderQntyValue != null) {
                    overAllOrderQnty = (Integer) totalOverAllOrderQntyValue;
                }
            } catch (ClassCastException e) {
                //Do nothing skip
            }

            if (payLoad != null) {
                List<OrderLine> orderLines = payLoad.getSummaryOrderLines();

                for (OrderLine orderLine : orderLines) {
                    if (DigitalStringUtil.isNotEmpty(orderLine.getOrderedQty())) {
                        try {
                            overAllOrderQnty += Double.valueOf(orderLine.getOrderedQty()).intValue();

                            if (!"e-GC".equalsIgnoreCase(orderLine.getItemStyle()) && "In Process".equalsIgnoreCase(orderLine.getStatus())) {
                                overNonEGCOrderQnty += Double.valueOf(orderLine.getOrderedQty()).intValue();
                            }
                        } catch (NumberFormatException e) {
                            //Do nothing skip
                        }
                    }

                    if (DigitalStringUtil.isNotEmpty(orderLine.getCarrierServiceCode())) {
                        if ("SNDA".equalsIgnoreCase(orderLine.getCarrierServiceCode())) {
                            isSNDAShippingMethod = true;
                        }

                        if ("S2ND".equalsIgnoreCase(orderLine.getCarrierServiceCode())) {
                            isS2NDShippingMethod = true;
                        }

                        if ("SF2ND".equalsIgnoreCase(orderLine.getCarrierServiceCode())) {
                            isSF2NDShippingMethod = true;
                        }

                        if ("SGRN".equalsIgnoreCase(orderLine.getCarrierServiceCode())) {
                            isSGRNShippingMethod = true;
                        }
                    }
                }

                if (isSNDAShippingMethod) {
                    summaryShipExtraAttributes.put("shippingMethod", "SNDA");
                } else if (isS2NDShippingMethod) {
                    summaryShipExtraAttributes.put("shippingMethod", "S2ND");
                }  else if (isSF2NDShippingMethod) {
                    summaryShipExtraAttributes.put("shippingMethod", "SF2ND");
                } else {
                    summaryShipExtraAttributes.put("shippingMethod", "SGRN");
                }

                if ((isSNDAShippingMethod && isSGRNShippingMethod)
                    || (isS2NDShippingMethod && isSGRNShippingMethod)
                        || (isSF2NDShippingMethod && isSGRNShippingMethod)
                    ) {
                    summaryShipExtraAttributes.put("showMultiShipmentMsg", true);
                }

                if (payLoad.getTotals() != null &&
                        payLoad.getTotals().getGrandCharges() > 0) {
                    summaryShipExtraAttributes.put("shippingCharge", payLoad.getTotals().getGrandCharges());
                } else {
                    summaryShipExtraAttributes.put("shippingCharge", "0.0");
                }

            }
            params.put("totalNonEGCCommerceCount", overNonEGCOrderQnty);
            params.put("totalCommerceCount", overAllOrderQnty);
            params.put("orderSummarySHIPItems", summaryShipExtraAttributes);
        }

        params.put("orderSummaryBOPISItems", summaryBopisItems);
        params.put("orderSummaryBOSTSItems", summaryBostsItems);

        if (summaryBopisItems != null && summaryBopisItems.size() > 0) {
            params.put("displayOtherItems", "true");
        } else if (summaryBostsItems != null && summaryBostsItems.size() > 0) {
            params.put("displayOtherItems", "true");
        } else if (summaryShipExtraAttributes.size() > 0) {
            params.put("displayOtherItems", "true");
        }

    }

    /**
     *
     * @param itemsByFullfilmentType
     * @param type
     */
    private void sortStoreLocation(HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType, String type) {
        HashMap<String, OrderlineAttributesDTO> lines = itemsByFullfilmentType.get(type);
        if (lines != null && lines.size() > 1) {
            Comparator<Entry<String, OrderlineAttributesDTO>> valueComparator = new Comparator<Entry<String, OrderlineAttributesDTO>>() {
                @Override
                public int compare(Entry<String, OrderlineAttributesDTO> e1, Entry<String, OrderlineAttributesDTO> e2) {
                    OrderlineAttributesDTO v1 = e1.getValue();
                    OrderlineAttributesDTO v2 = e2.getValue();
                    return v1.getLocationId().compareTo(v2.getLocationId());
                }
            };
            Set<Entry<String, OrderlineAttributesDTO>> entries = lines.entrySet();
            List<Entry<String, OrderlineAttributesDTO>> listOfEntries = new ArrayList<>(entries);
            Collections.sort(listOfEntries, valueComparator);
            LinkedHashMap<String, OrderlineAttributesDTO> sortedByValue = new LinkedHashMap<>(listOfEntries.size());
            for (Entry<String, OrderlineAttributesDTO> entry : listOfEntries) {
                sortedByValue.put(entry.getKey(), entry.getValue());
            }
            itemsByFullfilmentType.put(type, sortedByValue);
        }
    }

    /**
     *
     * @param itemsByFullfilmentType
     * @param type
     */
    protected void sortByStatusDate(HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType, final String type) {
        HashMap<String, OrderlineAttributesDTO> lines = itemsByFullfilmentType.get(type);

        if (lines != null && lines.size() > 1) {
            Comparator<Entry<String, OrderlineAttributesDTO>> valueComparator = new Comparator<Entry<String, OrderlineAttributesDTO>>() {
                @Override
                public int compare(Entry<String, OrderlineAttributesDTO> e1, Entry<String, OrderlineAttributesDTO> e2) {
                    OrderlineAttributesDTO v1 = e1.getValue();
                    OrderlineAttributesDTO v2 = e2.getValue();
                    StringBuilder sd1 = new StringBuilder();
                    StringBuilder sd2 = new StringBuilder();

                    sd1.append(shipMethodSort.containsKey(v1.getStatus()) ? shipMethodSort.get(v1.getStatus()) : "2").append(v1.getStatus());
                    sd2.append(shipMethodSort.containsKey(v2.getStatus()) ? shipMethodSort.get(v2.getStatus()) : "2").append(v2.getStatus());

                    if (type.equals(ShippingGroupConstants.ShipType.SHIP.name())) {
                        if (DigitalStringUtil.isEmpty(v1.getShipCode()))
                            sd1.append("3");
                        else
                            sd1.append(shipMethodSort.get(v1.getShipCode()));

                        if (DigitalStringUtil.isEmpty(v2.getShipCode()))
                            sd2.append("3");
                        else
                            sd2.append(shipMethodSort.get(v2.getShipCode()));
                    }

                    //decending
                    if (v1.getUpdateDate() != null && v2.getUpdateDate() != null) {
                        sd1.append(Long.MAX_VALUE - v1.getUpdateDate().getTime());
                        sd2.append(Long.MAX_VALUE - v2.getUpdateDate().getTime());
                    }

                    return sd1.toString().compareTo(sd2.toString());
                }
            };
            Set<Entry<String, OrderlineAttributesDTO>> entries = lines.entrySet();
            List<Entry<String, OrderlineAttributesDTO>> listOfEntries = new ArrayList<>(entries); // sorting HashMap by values using comparator Collections.sort(listOfEntries, valueComparator); LinkedHashMap<String, String> sortedByValue = new LinkedHashMap<String, String>(listOfEntries.size());
            Collections.sort(listOfEntries, valueComparator);
            LinkedHashMap<String, OrderlineAttributesDTO> sortedByValue = new LinkedHashMap<>(listOfEntries.size()); // copying entries from List to Map for(Entry<String, String> entry : listOfEntries){ sortedByValue.put(entry.getKey(), entry.getValue()); }
            for (Entry<String, OrderlineAttributesDTO> entry : listOfEntries) {
                sortedByValue.put(entry.getKey(), entry.getValue());
            }
            itemsByFullfilmentType.put(type, sortedByValue);
        }
    }

    /**
     *
     * @param payLoad
     * @param order
     */
    protected void handleOrderUpdate(OrderStatusPayload payLoad, DigitalOrderImpl order) {
        if (!this.isCs14OrderUpdateFlag()) {
            logDebug("Not processing omniOrderUpdate for communication key  " + this.getCommunicationKey() + " since it is cs14OrderUpdateFlag is configured to ignore");
            return;
        }
        final String METHOD_NAME = "handleOrderStatusEvent";
        final String CLASSNAME = getListenerName();
        UserTransactionDemarcation td = null;
        try {
            RepositoryItem profile = null;

            td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
            if (payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null) {
                if (order != null) {
                    profile = getProfileTools().getProfileForOrder(order);
                } else if (DigitalStringUtil.isNotBlank(payLoad.getOmniOrderUpdate().getOrder().getOrderNo()) &&
                        this.getOrderTools().getOrderLookupService().getOrderManager().orderExists(payLoad.getOmniOrderUpdate().getOrder().getOrderNo())) {
                    order = (DigitalOrderImpl) getOrderTools().getOrderLookupService().getOrderManager().
                            loadOrder(payLoad.getOmniOrderUpdate().getOrder().getOrderNo());
                    profile = getProfileTools().getProfileForOrder(order);
                }

                DigitalOrderHistoryModel orderHistory = DigitalOrderStatusUtil.convertToOrderHistory(payLoad, order, profile);
                this.getOrderStatusTools().updateOrderStatus(orderHistory);
            } else {
                if (order != null) {
                    logError("OmniOrderUpdate Failure :: OmniOrderUpdate is null, check if yantra is sending the OmniOrderUpdate node in payload :: " + order.getId());
                } else {
                    logError("OmniOrderUpdate Failure :: OmniOrderUpdate is null, check if yantra is sending the OmniOrderUpdate node in payload :: ");
                }
            }
        } catch (Exception e) {
            TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
            if (order != null) {
                logError("OmniOrderUpdate Failure :: " + order.getId(), e);
            } else {
                logError("OmniOrderUpdate Failure :: ", e);
            }
        } finally {
            TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
        }
    }

    protected abstract HashMap<String, Object> handleEmailNotification(OrderStatusPayload payLoad, DigitalOrderImpl order,
                                                                       HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType);

    /**
     *
     * @return
     */
    @Override
    public String getListenerName() {
        return this.getClass().getName();
    }

    /**
     *
     * @param payLoad
     * @param orderHistoryItem
     * @param params
     */
    public void putLoyaltyNumber(OrderStatusPayload payLoad, RepositoryItem orderHistoryItem, HashMap params) {
        String loyaltyNumber = "";
        if (DigitalStringUtil.isNotEmpty(payLoad.getLoyaltyId())) {
            loyaltyNumber = payLoad.getLoyaltyId();
        } else if (orderHistoryItem != null) {
            loyaltyNumber = (String) orderHistoryItem.getPropertyValue("loyaltyNumber");
        }

        params.put("REWARDSNUMBER", loyaltyNumber == null ? "" : loyaltyNumber);
    }

    /**
     *
     * @param itemsByFullfilmentType
     * @param params
     */
    public void putLocationInformation(HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType, HashMap params) {
        if (itemsByFullfilmentType == null)
            return;

        HashMap<String, OrderlineAttributesDTO> picklines = itemsByFullfilmentType.get(BOPIS);
        if (picklines != null && picklines.size() > 0) {
            Map.Entry<String, OrderlineAttributesDTO> entry = picklines.entrySet().iterator().next();
            params.put("pickupMallPlazaName", entry.getValue().getMallPlazaName());
            params.put("pickupAddress", entry.getValue().getPerson());
        }
        HashMap<String, OrderlineAttributesDTO> stslines = itemsByFullfilmentType.get(BOSTS);
        if (stslines != null && stslines.size() > 0) {
            Map.Entry<String, OrderlineAttributesDTO> entry = stslines.entrySet().iterator().next();
            params.put("stsMallPlazaName", entry.getValue().getMallPlazaName());
            params.put("stsAddress", entry.getValue().getPerson());
        }
    }

    /**
     *
     * @param itemsByFullfilmentType
     * @param params
     */
    public void putStatusDate(HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType, HashMap params) {
        if (itemsByFullfilmentType == null)
            return;

        //Assumption: As email from Yantra team :Satya, at any given point of time we will get one event for one line item picked up, below logic is based on the assumption.
        HashMap<String, OrderlineAttributesDTO> picklines = itemsByFullfilmentType.get(BOPIS);
        if (picklines != null && picklines.size() > 0) {
            Map.Entry<String, OrderlineAttributesDTO> entry = picklines.entrySet().iterator().next();
            params.put("pickedupDate", entry.getValue().getStatusUpdateDate());
        } else {
            HashMap<String, OrderlineAttributesDTO> stslines = itemsByFullfilmentType.get(BOSTS);
            if (stslines != null && stslines.size() > 0) {
                Map.Entry<String, OrderlineAttributesDTO> entry = stslines.entrySet().iterator().next();
                params.put("pickedupDate", entry.getValue().getStatusUpdateDate());
            }
        }
    }

    /**
     *
     * @param payLoad
     * @param params
     */
    public void putOrderDetail(OrderStatusPayload payLoad, HashMap params) {
        params.put("orderNumber", payLoad.getOrderNo());
        if (payLoad.getTotals() != null) {
            params.put("orderTotal", payLoad.getTotals().getGrandTotal());
            params.put("subTotal", payLoad.getTotals().getLineSubTotal());
            params.put("tax", payLoad.getTotals().getGrandTax());
            params.put("refundAmountTotal", payLoad.getTotals().getRefundAmountTotal());
            params.put("returnRefundAmount", payLoad.getTotals().getReturnRefundAmount());
            params.put("gcRefundAmount", payLoad.getTotals().getGcRefundAmount());
            params.put("ccRefundAmount", payLoad.getTotals().getCcRefundAmount());
            params.put("payPalRefundAmount", payLoad.getTotals().getPayPalRefundAmount());
            if (payLoad.getTotals().getGrandCharges() > 0.0) {
                params.put("shipping", payLoad.getTotals().getGrandCharges());
            } else {
                params.put("shipping", "0.0");
            }
        }
        params.put("orderDate", payLoad.getOrderDate());
        params.put("trackingDate", DigitalDateUtil.getOperationEmailsTrackingDate());
    }

    /**
     *
     * @param order
     * @param params
     * @param orderHistoryItem
     */
    public void putOrderDetail(DigitalOrderImpl order, HashMap params, RepositoryItem orderHistoryItem) {
        params.put("orderNumber", dswosum.getOrderTotal(order));
        params.put("orderTotal", dswosum.getOrderTotal(order));
        params.put("subTotal", dswosum.getSubTotal(order));
        params.put("tax", dswosum.getTax(order));
        params.put("orderDate", orderHistoryItem.getPropertyValue("orderPlacementDate"));
        params.put("saved", dswosum.getSavings(order));
        if (dswosum.getShipping(order) > 0.0) {
            params.put("shipping", dswosum.getShipping(order));
        } else {
            params.put("shipping", "FREE");
        }
        RepositoryItem orderPriceInfoItem = (RepositoryItem) order.getPropertyValue("priceInfo");
        if (orderPriceInfoItem != null) {
            if (orderPriceInfoItem.getPropertyValue("orderPriceInfo") != null) {

                if (order.getPropertyValue("rewardCertificates") != null) {
                    RepositoryItem rewardCertificateItem = (RepositoryItem) order.getPropertyValue("rewardCertificates");
                    params.put("certificates", rewardCertificateItem.getPropertyValue("amount"));
                }
                //TODO Need to add Offers amount
            }
        }
    }

    /**
     *
     * @param orderLines
     * @param params
     * @return
     */
    public HashMap<String, HashMap<String, OrderlineAttributesDTO>> categorizeOrderLines(
            List<OrderLine> orderLines, HashMap params) {
        HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType = new HashMap<>();

        if (orderLines == null || orderLines.size() == 0) {
            return itemsByFullfilmentType;
        }

        String fullfilmentType = SHIP;
        Double offersAwardAmountsTotal = 0.0;
        Double certificatesAwardAmountsTotal = 0.0;
        for (OrderLine o : orderLines) {
            String statusCode = null;
            String statusDate = null;
            List<OrderStatus> statuses = o.getOrderStatus();
            for (OrderStatus os : statuses) {
                statusCode = os.getStatus();
                statusDate = os.getStatusDate();
            }

            String locationId = getPickupStoreId(o);
            String trackingNumber = getTrackingNumber(o);
            Date updateDate = getUpdateDate(o);

            String status = o.getStatus();

            if (!DigitalStringUtil.isEmpty(o.getFulfillmentType())) {
                fullfilmentType = o.getFulfillmentType();
            } else if (DigitalStringUtil.isEmpty(o.getFulfillmentType())) {
                fullfilmentType = SHIP;
            }

            if (fullfilmentType.equals(SHIP)) {
                locationId = null;
            }

            if (DigitalStringUtil.isBlank(o.getFulfillmentType())) {
                o.setFulfillmentType(fullfilmentType);
            }

            OrderlineAttributesDTO iattr = new OrderlineAttributesDTO();

            populateOrderlineAttributes(o, iattr, locationId, trackingNumber, status, updateDate, fullfilmentType);

            if (itemsByFullfilmentType.containsKey(fullfilmentType)) {
                HashMap<String, OrderlineAttributesDTO> dat = itemsByFullfilmentType
                        .get(fullfilmentType);
                dat.put(o.getKey() + "-" + statusCode + "-" + statusDate + "-" + o.getStatusQuantity() + "-" + iattr.getOrderReleaseKey() + "-" + o.getTrackingNo(), iattr);
            } else {
                HashMap<String, OrderlineAttributesDTO> dat = new HashMap<>();
                dat.put(o.getKey() + "-" + statusCode + "-" + statusDate + "-" + o.getStatusQuantity() + "-" + iattr.getOrderReleaseKey() + "-" + o.getTrackingNo(), iattr);
                itemsByFullfilmentType.put(fullfilmentType, dat);
            }
            Map<String, String> itemPromoList = new HashMap<>();
            int i = 0;
            for (Award award : o.getAwards()) {
                if (award.getAwardType() != null && !award.getAwardType().equalsIgnoreCase("RewardCertificate")) {
                    if (award.getAwardAmount() != null) {
                        offersAwardAmountsTotal += award.getAwardAmount();
                    }
                    String promotionId = award.getPromotionId();
                    if (DigitalStringUtil.isBlank(promotionId)) {
                        promotionId = Integer.valueOf(i).toString();
                        i++;
                    }
                    itemPromoList.put(promotionId, award.getDescription());
                }
                //TODO Mapping document has wrong statement
                if (award.getAwardType() != null && award.getAwardType().equalsIgnoreCase("RewardCertificate")) {
                    if (award.getAwardAmount() != null) {
                        certificatesAwardAmountsTotal += award.getAwardAmount();
                    }
                }
            }
            iattr.setItemPromoList(itemPromoList);
        }
        if (params != null) {
            params.put("offersRewardsAmount", offersAwardAmountsTotal);
            params.put("certificatesRewardsAmount", certificatesAwardAmountsTotal);
        }
        return itemsByFullfilmentType;
    }

    /**
     *
     * @param orderLine
     * @return
     */
    private String getPickupStoreId(OrderLine orderLine) {
        List<OrderStatus> orderStatuses = orderLine.getOrderStatus();
        String locationId = null;
        for (OrderStatus orderStatus : orderStatuses) {
            String osShipNode = orderStatus.getShipNode();

            if (DigitalStringUtil.isBlank(osShipNode)) {
                osShipNode = orderLine.getShipNode();
            }

            if (DigitalStringUtil.isNotBlank(osShipNode)) {
                osShipNode = DigitalStringUtil.trim(osShipNode);
                if (osShipNode.startsWith("29_")) {
                    locationId = osShipNode.substring(3);
                } else if (osShipNode.startsWith("s1")) {
                    locationId = osShipNode.substring(2);
                }
            }
        }
        return locationId;
    }

    /**
     *
     * @param orderLine
     * @return
     */
    private String getTrackingNumber(OrderLine orderLine) {
        List<ShipmentLine> shipmentLines = orderLine.getShipLines();
        String trackingNumber = null;
        for (ShipmentLine shipmentLine : shipmentLines) {
            trackingNumber = shipmentLine.getTrackingNo();
            break;
        }
        return trackingNumber;
    }

    /**
     * @param orderLine
     * @return String
     */
    private String getCarrierServiceCode(OrderLine orderLine) {
        String carrierServiceCode = "";
        if (DigitalStringUtil.isNotEmpty(orderLine.getSCAC())) {
            carrierServiceCode = orderLine.getSCAC().trim();
        } else {
            List<ShipmentLine> shipmentLines = orderLine.getShipLines();
            for (ShipmentLine shipmentLine : shipmentLines) {
                if (DigitalStringUtil.isNotEmpty(shipmentLine.getCarrierName())) {
                    carrierServiceCode = shipmentLine.getCarrierName().trim();
                }
                break;
            }
        }
        carrierServiceCode = DigitalCommonUtil.getCarrierCode(carrierServiceCode);

        return carrierServiceCode;
    }

    /**
     *
     * @param orderLine
     * @return
     */
    private Date getUpdateDate(OrderLine orderLine) {
        List<OrderStatus> orderStatuses = orderLine.getOrderStatus();
        String updateDate;
        for (OrderStatus orderStatus : orderStatuses) {
            updateDate = orderStatus.getStatusDate();
            return DigitalDateUtil.parseYantraDateTime(updateDate, YANTRA_ORDER_STATUS_DATE_TIMEZONE_FORMAT);
        }

        return null;
    }

    /**
     *
     * @param payLoad
     * @param fulfillmentType
     * @param itemsByFullfilmentType
     * @param extraAttributes
     */
    protected void consolidateOrderSummaryAndPayload(OrderStatusPayload payLoad,
                                                     String fulfillmentType,
                                                     HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType,
                                                     HashMap<String, Object> extraAttributes) {

        HashMap<String, OrderlineAttributesDTO> items = itemsByFullfilmentType.get(fulfillmentType);

        HashMap<String, OrderlineAttributesDTO> lines = (HashMap<String, OrderlineAttributesDTO>) extraAttributes
                .get("orderLines");

        if (items != null) {
            if (lines != null) {
                items.putAll(lines);
            }
            extraAttributes.put("orderLines", items);
        }
    }

    /**
     *
     * @param orderNo
     * @param sku
     * @return
     */
    protected DigitalStoreAddress getPickupStore(String orderNo, String sku) {
        RepositoryItem history = this.getOrderHistoryItem(orderNo);
        List<RepositoryItem> commerceItems = (List<RepositoryItem>) history.getPropertyValue("commerceItems");
        String locationId = null;
        for (RepositoryItem item : commerceItems) {
            if (item.getPropertyValue("skuId").equals(sku)) {
                locationId = (String) item.getPropertyValue("pickupLocationId");
                if (locationId != null)
                    break;
            }
        }

        if (locationId != null) {
            return storeTools.fetchStoreAddress(locationId);
        }

        return null;

    }

    /**
     *
     * @param orderNo
     * @return
     */
    protected RepositoryItem getOrderHistoryItem(String orderNo) {
        RepositoryItem item = null;
        try {
            RepositoryView view = getOrderRepository().getView("dsworderhistory");
            QueryBuilder builder = view.getQueryBuilder();
            QueryExpression idProp = builder.createPropertyQueryExpression("orderId");
            QueryExpression idValue = builder.createConstantQueryExpression(orderNo);
            Query idMatch = builder.createComparisonQuery(idProp, idValue, QueryBuilder.EQUALS);
            RepositoryItem[] items = view.executeQuery(idMatch);
            if (items != null && items.length > 0) {
                item = items[0];
            }
        } catch (RepositoryException re) {
            re.getMessage();
        }
        return item;
    }

    /**
     * @param o
     * @param iattr
     * @param locationId
     * @param trackingNumber
     * @param status
     * @param updateDate
     * @param fullfilmentType
     */
    private void populateOrderlineAttributes(OrderLine o, OrderlineAttributesDTO iattr, String locationId, String trackingNumber, String status, Date updateDate, String fullfilmentType) {
        iattr.setItemSize(o.getItemSize());
        iattr.setItemDesc(o.getItemDescription());
        iattr.setItemColor(o.getItemColor());
        iattr.setItemQuantity(o.getOrderedQty());
        iattr.setItemUnitPrice(o.getUnitPrice());
        if (DigitalStringUtil.isEmpty(iattr.getItemUnitPrice())) {
            iattr.setItemUnitPrice(o.getUnitCost());
        }
        iattr.setItemStyle(o.getItemStyle());
        iattr.setItemWidth(o.getItemWidth());

        //MK: Commented below lines..peter needs to fix it
        //iattr.setItemBrand(o.getItemBrand());
        if (DigitalStringUtil.isEmpty(trackingNumber)) {
            trackingNumber = o.getTrackingNo();
        }
        iattr.setTrackingNumber(trackingNumber);
        iattr.setStatus(status);
        iattr.setItemStatusQuantity(o.getStatusQuantity());
        if (updateDate != null) {
            iattr.setStatusUpdateDate(new SimpleDateFormat("MM/dd/yyyy").format(updateDate));
        }
        if (o.getOrderStatus() != null && o.getOrderStatus().size() > 0) {
            OrderStatus orderStatus = o.getOrderStatus().get(0);
            iattr.setStatusCode(orderStatus.getStatus());
            iattr.setOrderReleaseStatusKey(orderStatus.getOrderReleaseStatusKey());
            iattr.setOrderReleaseKey(orderStatus.getOrderReleaseKey());
        }

        iattr.setUpdateDate(updateDate);
        iattr.setShipCode(o.getCarrierServiceCode());

        iattr.setItemId(o.getItemId());
        iattr.setColorCode(o.getColorCode());
        iattr.setItemBrand(o.getItemBrand());
        iattr.setItemName(o.getItemName());
        iattr.setProductTitle(o.getProductTitle());
        iattr.setLineDiscounts(o.getLineDiscounts());
        iattr.setItemShipType(o.getLineType());

        iattr.setNarwarUrl(constructNarwarUrl(trackingNumber, getCarrierServiceCode(o), o.getOrderId(), o.getFulfillmentType()));
        if (o.getCarrierServiceCode() != null) {
            if (o.getCarrierServiceCode().equalsIgnoreCase("SGRN") || o.getCarrierServiceCode().equalsIgnoreCase("GRN")) {
                iattr.setItemShippingMethod("Standard Shipping Option");
            } else if (o.getCarrierServiceCode().equalsIgnoreCase("2ND")
                || o.getCarrierServiceCode().equalsIgnoreCase("S2ND")
                    || o.getCarrierServiceCode().equalsIgnoreCase("SF2ND")) {
                iattr.setItemShippingMethod("2 Day Shipping Option");
            } else if (o.getCarrierServiceCode().equalsIgnoreCase("NDA") || o.getCarrierServiceCode().equalsIgnoreCase("SNDA") ||
                    o.getCarrierServiceCode().equalsIgnoreCase("SFND")) {
                iattr.setItemShippingMethod("Next day shipping");
            } else if (o.getCarrierServiceCode().equalsIgnoreCase("BOSTS") || o.getCarrierServiceCode().equalsIgnoreCase("BOPIS")) {
                iattr.setItemShippingMethod("Pick Up In Store");
            }
        }

        try {
            RepositoryItem sku = this.getCatalogTools().findSKU(o.getItemId());
            if (null != sku) {
                iattr.setItemUPC((String) sku.getPropertyValue("upc"));
                Set parentProducts = (Set) sku.getPropertyValue("parentProducts");
                if (parentProducts != null && parentProducts.size() > 0) {
                    RepositoryItem product = (RepositoryItem) parentProducts.iterator().next();
                    iattr.setProductId(product.getRepositoryId());
                    if (DigitalStringUtil.isBlank(iattr.getItemDesc())) {
                        iattr.setItemDesc((String) product.getPropertyValue("displayName"));
                    }

                    if (DigitalStringUtil.isBlank(iattr.getItemBrand()) &&
                            product.getPropertyValue("dswBrand") != null) {
                        iattr.setItemBrand((String) ((RepositoryItem) product.getPropertyValue("dswBrand")).getPropertyValue("displayName"));
                    }
                }

                if (DigitalStringUtil.isBlank(iattr.getItemSize()) &&
                        sku.getPropertyValue("size") != null) {
                    iattr.setItemSize((String) ((RepositoryItem) sku.getPropertyValue("size")).getPropertyValue("displayName"));
                }
                if (DigitalStringUtil.isBlank(iattr.getItemColor()) &&
                        sku.getPropertyValue("color") != null) {
                    iattr.setItemColor((String) ((RepositoryItem) sku.getPropertyValue("color")).getPropertyValue("displayName"));
                }
            }
            //TODO Need to add image URL
        } catch (RepositoryException e) {
            logError(e);
        }

        String storeNumber;
        if (DigitalStringUtil.isBlank(locationId)) {
            storeNumber = getPickupStoreId(o);
        } else {
            storeNumber = locationId;
        }

        if (DigitalStringUtil.isNotBlank(storeNumber) && !"SHIP".equalsIgnoreCase(fullfilmentType)) {
            iattr.setLocationId(storeNumber);
            Person p = new Person();
            DigitalStoreAddress storeAddress = storeTools.fetchStoreAddress(storeNumber);
            if (storeAddress != null) {
                if (o.getPersonInfoShipTo() != null) {
                    p.setLastName(o.getPersonInfoShipTo().getLastName());
                    p.setFirstName(o.getPersonInfoShipTo().getFirstName());
                }
                p.setAddressLine1(storeAddress.getAddress1());
                p.setAddressLine2(storeAddress.getAddress2());
                p.setCity(storeAddress.getCity());
                p.setState(storeAddress.getStateAddress());
                p.setZipCode(storeAddress.getPostalCode());
                iattr.setPerson(p);
                iattr.setMallPlazaName(storeAddress.getMallPlazaName());
            }
        } else if (o.getPersonInfoShipTo() != null) {
            Person p;
            Person op = o.getPersonInfoShipTo();
            p = (Person) SerializationUtils.clone(op);
            iattr.setPerson(p);
        }
        iattr.setFulfillmentType(o.getFulfillmentType());
        List<Award> awards = o.getAwards();

        Map<String, String> itemPromoList = new HashMap<>();
        int i = 0;
        for (Award award : awards) {
            String promoId = award.getPromotionId();
            if (DigitalStringUtil.isEmpty(award.getPromotionId())) {
                promoId = Integer.toString(i);
                i++;
            }
            itemPromoList.put(promoId, award.getDescription());
        }

        iattr.setItemPromoList(itemPromoList);

        //GC related info
        iattr.setRecipientName(o.getRecipientName());
        iattr.setRecipientEmail(o.getRecipientEmail());
        iattr.setSenderName(o.getSenderName());
        iattr.setSenderEmail(o.getSenderEmail());
    }

    /**
     *
     * @param trackingNumber
     * @param scac
     * @param orderId
     * @param fulfillmentType
     * @return
     */
    private String constructNarwarUrl(String trackingNumber, String scac, String orderId, String fulfillmentType) {
        //Ex: NarwarUrl=http://dsw1.narvar.com/dsw1/tracking/
        //http://prod.narvar.com/dsw/tracking/UPS?tracking_numbers=1Z3Y1143YW23400042937&type=b
        // type=b only for BOSTS items
        if (DigitalStringUtil.isNotBlank(trackingNumber)) {
            if (DigitalStringUtil.isNotBlank(fulfillmentType) && "BOSTS".equalsIgnoreCase(fulfillmentType)) {
                return getDigitalConstants().getNarwarUrl() + scac + "?tracking_numbers=" + trackingNumber + "&type=b" + "&order_number=" + orderId;
            } else {
                return getDigitalConstants().getNarwarUrl() + scac + "?tracking_numbers=" + trackingNumber + "&order_number=" + orderId;
            }
        } else {
            return null;
        }

    }

    protected abstract String getOrderNo(OrderStatusPayload payLoad);

    protected abstract String getEmailTo(OrderStatusPayload payLoad);

    protected abstract List<OrderLine> getOrderLines(OrderStatusPayload payLoad);


    /**
     * @param orderStatusEvent
     * @return
     */
    @Override
    public boolean doesHandleOrderStatusEvent(OrderStatusEvent orderStatusEvent) {
        String statusReceived = orderStatusEvent.getStatusType().getReceivedType();
        for (String supportedEventType : supportedEventTypes) {
            if (supportedEventType.equalsIgnoreCase(statusReceived)) {
                return true;
            }
        }
        return false;
    }

}
