package com.digital.commerce.services.pricing.reprice;

/* */
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
@SuppressWarnings({"rawtypes","unchecked"})
public class NewPromosHandler extends DefaultHandler {
	private final String	NEWPROMOTIONS	= "NewPromotions";
	private final String	NEWPROMOID		= "newPromoId";

	@Getter
	private List			newPromos		= new ArrayList( 10 );
	private String			newPromotions	= "";
	private String			newPromoId		= "";

	private StringBuffer	characterBuf;

	private String			errMsg			= null;

	public String getErrorMessage() {
		return errMsg;
	}


	/** Receive notification of the start of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @param atts - The attributes attached to the element. If there are no attributes, it shall be an empty Attributes object.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void startElement( String namespaceURI, String localName, String qName, Attributes atts ) {
		characterBuf = new StringBuffer();

		try {

			if( NEWPROMOTIONS.equals( localName ) ) {
				newPromotions = NEWPROMOTIONS;

			}

			if( !newPromotions.equals( "" ) ) {

				if( NEWPROMOID.equals( localName ) ) {
					newPromoId = NEWPROMOID;
				}
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}

	}

	/** Receive notification of character data inside an element.
	 * 
	 * @param ch - The characters.
	 * @param start - The start position in the character array.
	 * @param length - The number of characters to use from the character array.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void characters( char[] ch, int start, int length ) {
		characterBuf.append( ch, start, length );
	}

	/** Receive notification of the end of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void endElement( String namespaceURI, String localName, String qName ) {
		populateValue();

		try {
			if( newPromotions.equals( localName ) ) newPromotions = "";

		} catch( Exception e ) {
			errMsg = e.getMessage();
		}

	}

	public void populateValue() {
		try {
			String value = "";
			if( !newPromotions.equals( "" ) && !newPromoId.equals( "" ) ) {
				value = characterBuf.toString();
				newPromoId = "";
			}

			if( value != null && !value.equals( "" ) ) newPromos.add( value );

		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}
}
