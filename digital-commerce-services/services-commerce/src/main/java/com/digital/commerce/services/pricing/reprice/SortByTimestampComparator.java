package com.digital.commerce.services.pricing.reprice;

/*  */
import java.util.Comparator;

@SuppressWarnings("rawtypes")
public class SortByTimestampComparator implements Comparator {

	public int compare( Object o1, Object o2 ) {
		long time1 = ( (Item)o1 ).getTimestamp();
		long time2 = ( (Item)o2 ).getTimestamp();

		if( time1 == time2 )
			return 0;
		else if( time1 < time2 )
			return -1;
		else
			return 1;
	}

}
