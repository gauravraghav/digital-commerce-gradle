package com.digital.commerce.services.order.status;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.OrderConstants.OrderPropertyManager;
import com.digital.commerce.services.order.DigitalOrderHistoryItem;
import com.digital.commerce.services.order.DigitalOrderHistoryModel;
import com.digital.commerce.services.order.DigitalOrderLookupService;
import com.digital.commerce.services.order.DigitalOrderTools;

import atg.commerce.CommerceException;
import atg.commerce.order.OrderManager;
import atg.multisite.Site;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalOrderStatusTools extends ApplicationLoggingImpl{
	private DigitalOrderLookupService		orderLookupService;
	private OrderManager				orderManager;
	private List<String>				fullSiteDemandLocationMapping;
	private String 						defaultUnknownLineItemStatus;
	
	public void updateOrderStatus(DigitalOrderHistoryModel orderHistory){
		if(orderHistory == null){
			logError("DigitalOrderHistoryModel is null");
			return;
		}
		try {
				RepositoryItem orderStatus = this.getOrderLookupService().orderExists(orderHistory.getOrderId());
			if(orderStatus == null){
				persistOrderStatus(orderHistory);
			}else{
				updateOrderStatus(orderStatus, orderHistory);
			}			
			
		} catch (CommerceException | RepositoryException e) {
			logError(e);
		} 
	}

	protected void persistOrderStatus(DigitalOrderHistoryModel orderHistory) throws CommerceException, RepositoryException{
		MutableRepositoryItem orderStatusRepo = (MutableRepositoryItem)this.getOrderLookupService().orderExists(orderHistory.getOrderId());
		if(null == orderStatusRepo){
			MutableRepository orderRepo = (MutableRepository)this.getOrderManager().getOrderTools().getOrderRepository();
			orderStatusRepo = orderRepo.createItem(orderHistory.getOrderId(), OrderPropertyManager.DSW_ORDER_HISTORY.getValue());

			Date currentTime = Calendar.getInstance().getTime();
			
			orderStatusRepo.setPropertyValue("addDate", currentTime);
			orderStatusRepo.setPropertyValue("updateDate", currentTime);
			orderStatusRepo.setPropertyValue("currencyCode", orderHistory.getCurrencyCode());
			
			if(DigitalStringUtil.isNotBlank(orderHistory.getDemandLocation())){
				if(this.getFullSiteDemandLocationMapping().contains(orderHistory.getDemandLocation())){
					orderHistory.setDemandLocation("FULLSITE");
				}
				orderStatusRepo.setPropertyValue("demandLocation",orderHistory.getDemandLocation());
			}			

			orderStatusRepo.setPropertyValue("loyaltyNumber", orderHistory.getLoyalNumber());
			orderStatusRepo.setPropertyValue("profileId", orderHistory.getProfileId());
			orderStatusRepo.setPropertyValue("orderId", orderHistory.getOrderId());
			orderStatusRepo.setPropertyValue("orderPlacementDate", orderHistory.getOrderPlacementDate());
			orderStatusRepo.setPropertyValue("email", orderHistory.getEmailId());
			
			String siteId = orderHistory.getSiteId();
			Site site = MultiSiteUtil.getSite(siteId);
			if(site != null){
				siteId = site.getId();
			}
			String locale = orderHistory.getLocale();
			if(DigitalStringUtil.isNotBlank(locale)){
				locale = MultiSiteUtil.getDefaultLocale();
			}
			orderStatusRepo.setPropertyValue("locale", locale);
			orderStatusRepo.setPropertyValue("siteId", siteId);
			
			orderStatusRepo.setPropertyValue("status", orderHistory.getStatus());
			orderStatusRepo.setPropertyValue("statusDate", orderHistory.getStatusDate());
			orderStatusRepo.setPropertyValue("total", orderHistory.getTotal());
			List<RepositoryItem> dswCommerceItems = new ArrayList<>();
			List<DigitalOrderHistoryItem> dswOrderHistoryItems = orderHistory.getCommerceItems();
			int i=0;
			for(DigitalOrderHistoryItem orderHistoryItem : dswOrderHistoryItems){
				MutableRepositoryItem dswCommerceItemRepo = orderRepo.createItem(OrderPropertyManager.DSW_COMMERCE_ITEM.getValue());
				dswCommerceItemRepo.setPropertyValue("addDate",currentTime);
				dswCommerceItemRepo.setPropertyValue("updateDate",currentTime);
				
				dswCommerceItemRepo.setPropertyValue("yantraLineItem", orderHistoryItem.getYantraLineItemId());
				dswCommerceItemRepo.setPropertyValue("atgCommerceItemId",orderHistoryItem.getAtgCommerceId());
				
				dswCommerceItemRepo.setPropertyValue("fulfillLocationId",orderHistoryItem.getFulfillLocationId());
				dswCommerceItemRepo.setPropertyValue("orderPlacementDate",orderHistoryItem.getOrderPlacementDate());
				dswCommerceItemRepo.setPropertyValue("pickupLocationId",orderHistoryItem.getPickupLocationId());
				dswCommerceItemRepo.setPropertyValue("productDescription",orderHistoryItem.getProductDescription());
				dswCommerceItemRepo.setPropertyValue("productId",orderHistoryItem.getProductId());
				dswCommerceItemRepo.setPropertyValue("quantity",orderHistoryItem.getQuantity());
				dswCommerceItemRepo.setPropertyValue("returnLocationId",orderHistoryItem.getReturnLocationId());
				dswCommerceItemRepo.setPropertyValue("shipToFirstName",orderHistoryItem.getShipToFirstName());
				dswCommerceItemRepo.setPropertyValue("shipToLastName",orderHistoryItem.getShipToLastName());
				dswCommerceItemRepo.setPropertyValue("skuId",orderHistoryItem.getSkuId());
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getStatus())){
					String status = this.getLineItemStatus().get(orderHistoryItem.getStatus());
					if(DigitalStringUtil.isBlank(status)){
						logInfo("Could not find line item status for " + orderHistoryItem.getStatus());
						status = getDefaultUnknownLineItemStatus();
					}
					dswCommerceItemRepo.setPropertyValue("status",status);
				}

				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getGcFromName())){
					dswCommerceItemRepo.setPropertyValue("gcFromName",orderHistoryItem.getGcFromName());
				}
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getGcFromEmailAddress())){
					dswCommerceItemRepo.setPropertyValue("gcFromEmailAddress",orderHistoryItem.getGcFromEmailAddress());
				}
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getGcToName())){
					dswCommerceItemRepo.setPropertyValue("gcToName",orderHistoryItem.getGcToName());
				}
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getGcToEmailAddress())){
					dswCommerceItemRepo.setPropertyValue("gcToEmailAddress",orderHistoryItem.getGcToEmailAddress());
				}
				
				dswCommerceItemRepo.setPropertyValue("statusDate",orderHistoryItem.getStatusDate());
				dswCommerceItemRepo.setPropertyValue("trackingNumber",orderHistoryItem.getTrackingNumber());
				dswCommerceItemRepo.setPropertyValue("carrier",orderHistoryItem.getCarrier());
				dswCommerceItemRepo.setPropertyValue("unitPrice",orderHistoryItem.getUnitPrice());
				dswCommerceItemRepo.setPropertyValue("updateDate",Calendar.getInstance().getTime());
				dswCommerceItemRepo.setPropertyValue("orderSysId", orderHistory.getOrderId());
				dswCommerceItemRepo.setPropertyValue("sequenceNum",i++);
				dswCommerceItems.add(dswCommerceItemRepo);
			}
			
			orderStatusRepo.setPropertyValue("commerceItems", dswCommerceItems);
			orderRepo.addItem(orderStatusRepo);
		}		
	}
	
	@SuppressWarnings("unchecked")
	protected void updateOrderStatus(RepositoryItem orderStatus, DigitalOrderHistoryModel orderHistory) throws CommerceException, RepositoryException{
		if( null != orderStatus ){
			MutableRepository orderRepo = (MutableRepository)this.getOrderManager().getOrderTools().getOrderRepository();
			MutableRepositoryItem orderStatusRepo = (MutableRepositoryItem)orderStatus;
			
			Date currentTime = Calendar.getInstance().getTime();
			String siteId = orderHistory.getSiteId();
			Site site = MultiSiteUtil.getSite(siteId);
			if(site != null){
				siteId = site.getId();
			}
			String locale = orderHistory.getLocale();
			if(DigitalStringUtil.isBlank(locale)){
				locale = MultiSiteUtil.getDefaultLocale();
			}			
			orderStatusRepo.setPropertyValue("updateDate", currentTime);
			if(DigitalStringUtil.isNotBlank(orderHistory.getCurrencyCode())){
				orderStatusRepo.setPropertyValue("currencyCode", orderHistory.getCurrencyCode());
			}
			
			if(DigitalStringUtil.isNotBlank(orderHistory.getDemandLocation())){
				if(this.getFullSiteDemandLocationMapping().contains(orderHistory.getDemandLocation())){
					orderHistory.setDemandLocation("FULLSITE");
				}
				orderStatusRepo.setPropertyValue("demandLocation",orderHistory.getDemandLocation());
			}
			
			if(DigitalStringUtil.isNotBlank(locale)){
				orderStatusRepo.setPropertyValue("locale", locale);
			}
			
			if(DigitalStringUtil.isNotBlank(orderHistory.getLoyalNumber())){
				orderStatusRepo.setPropertyValue("loyaltyNumber", orderHistory.getLoyalNumber());
			}
			
			if(DigitalStringUtil.isNotBlank(siteId)){
				orderStatusRepo.setPropertyValue("siteId", siteId);
			}
			orderStatusRepo.setPropertyValue("status", orderHistory.getStatus());
			orderStatusRepo.setPropertyValue("statusDate", orderHistory.getStatusDate());
			
			if(DigitalStringUtil.isNotBlank(orderHistory.getEmailId())){
				orderStatusRepo.setPropertyValue("email", orderHistory.getEmailId());
			}
			
			if(orderHistory.getTotal() > 0){
				orderStatusRepo.setPropertyValue("total", orderHistory.getTotal());
			}
			
			List<RepositoryItem> orderStatusCommerceItemsRepo = (List<RepositoryItem>)orderStatusRepo.getPropertyValue( "commerceItems" );
			
			List<RepositoryItem> dswCommerceItems = new ArrayList<>();
			List<DigitalOrderHistoryItem> dswOrderHistoryItems = orderHistory.getCommerceItems();
			
			//find next sequenceNum
			int i= this.findNextOrderHistorySequenceNum(orderStatusCommerceItemsRepo);
			
			for(DigitalOrderHistoryItem orderHistoryItem : dswOrderHistoryItems){
				MutableRepositoryItem dswCommerceItemRepo = findOrderLineItem(orderStatusCommerceItemsRepo, orderHistoryItem.getYantraLineItemId(), orderHistoryItem.getAtgCommerceId());
				if(dswCommerceItemRepo == null){
					dswCommerceItemRepo = orderRepo.createItem(OrderPropertyManager.DSW_COMMERCE_ITEM.getValue());
					dswCommerceItemRepo.setPropertyValue("addDate", currentTime);
					dswCommerceItemRepo.setPropertyValue("orderPlacementDate", orderHistoryItem.getOrderPlacementDate());
					dswCommerceItemRepo.setPropertyValue("sequenceNum", i++);
					if(DigitalStringUtil.isNotBlank(orderHistory.getOrderId())){
						dswCommerceItemRepo.setPropertyValue("orderSysId", orderHistory.getOrderId());
					}
				}
				
				// CS-14 processing in ATG to only update if the notification is the latest event if not don't update and duplicate is not processed.
				if (dswCommerceItemRepo.getPropertyValue("statusDate") != null && orderHistoryItem.getStatusDate() != null) {
					if (orderHistoryItem.getStatusDate().before((Date) dswCommerceItemRepo.getPropertyValue("statusDate"))
							|| orderHistoryItem.getStatusDate().equals((Date) dswCommerceItemRepo.getPropertyValue("statusDate"))) {
						return;
					}
				}

				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getYantraLineItemId()) && (dswCommerceItemRepo.getPropertyValue("yantraLineItem") == null
						&& DigitalStringUtil.isBlank((String)dswCommerceItemRepo.getPropertyValue("yantraLineItem")))){
					dswCommerceItemRepo.setPropertyValue("yantraLineItem", orderHistoryItem.getYantraLineItemId());
				}
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getAtgCommerceId())){
					dswCommerceItemRepo.setPropertyValue("atgCommerceItemId",orderHistoryItem.getAtgCommerceId());
				}
				dswCommerceItemRepo.setPropertyValue("updateDate", currentTime);
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getFulfillLocationId())){
					dswCommerceItemRepo.setPropertyValue("fulfillLocationId",orderHistoryItem.getFulfillLocationId());
				}
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getPickupLocationId())){
					dswCommerceItemRepo.setPropertyValue("pickupLocationId",orderHistoryItem.getPickupLocationId());
				}
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getProductDescription())){
					dswCommerceItemRepo.setPropertyValue("productDescription",orderHistoryItem.getProductDescription());
				}
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getProductId())){
					dswCommerceItemRepo.setPropertyValue("productId",orderHistoryItem.getProductId());
				}
				dswCommerceItemRepo.setPropertyValue("quantity",orderHistoryItem.getQuantity());
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getReturnLocationId())){
					dswCommerceItemRepo.setPropertyValue("returnLocationId",orderHistoryItem.getReturnLocationId());
				}
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getShipToFirstName())){
					dswCommerceItemRepo.setPropertyValue("shipToFirstName",orderHistoryItem.getShipToFirstName());
				}
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getShipToLastName())){
					dswCommerceItemRepo.setPropertyValue("shipToLastName",orderHistoryItem.getShipToLastName());
				}
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getSkuId())){
					dswCommerceItemRepo.setPropertyValue("skuId",orderHistoryItem.getSkuId());
				}
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getStatus())){
					String status = this.getLineItemStatus().get(orderHistoryItem.getStatus());
					if(DigitalStringUtil.isBlank(status)){
						logInfo("Could not find line item status for " + orderHistoryItem.getStatus());
						status = getDefaultUnknownLineItemStatus();
					}
					dswCommerceItemRepo.setPropertyValue("status",status);
				}
				dswCommerceItemRepo.setPropertyValue("statusDate",orderHistoryItem.getStatusDate());
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getTrackingNumber())){
					validateTrackingNumberQuantityData(orderHistory.getOrderId(), orderHistoryItem.getTrackingNumber());
					dswCommerceItemRepo.setPropertyValue("trackingNumber",orderHistoryItem.getTrackingNumber());
				}
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getCarrier())){
					dswCommerceItemRepo.setPropertyValue("carrier",orderHistoryItem.getCarrier());
				}
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getGcFromName())){
					dswCommerceItemRepo.setPropertyValue("gcFromName",orderHistoryItem.getGcFromName());
				}
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getGcFromEmailAddress())){
					dswCommerceItemRepo.setPropertyValue("gcFromEmailAddress",orderHistoryItem.getGcFromEmailAddress());
				}
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getGcToName())){
					dswCommerceItemRepo.setPropertyValue("gcToName",orderHistoryItem.getGcToName());
				}
				
				if(DigitalStringUtil.isNotBlank(orderHistoryItem.getGcToEmailAddress())){
					dswCommerceItemRepo.setPropertyValue("gcToEmailAddress",orderHistoryItem.getGcToEmailAddress());
				}
				
				dswCommerceItemRepo.setPropertyValue("unitPrice",orderHistoryItem.getUnitPrice());
				
				dswCommerceItems.add(dswCommerceItemRepo);
			}
			
			List<RepositoryItem> unchangedCommerceItem = new ArrayList<>();
			for(RepositoryItem cir :orderStatusCommerceItemsRepo){
				boolean found = false;
				for(RepositoryItem ci : dswCommerceItems){
					if(ci.getRepositoryId().equalsIgnoreCase(cir.getRepositoryId())){
						found = true;
						break;
					}
				}
				if(!found){
					unchangedCommerceItem.add(cir);
				}
			}
			
			dswCommerceItems.addAll(unchangedCommerceItem);
			
			orderStatusRepo.setPropertyValue("commerceItems", dswCommerceItems);
		}
	}
	
	private void validateTrackingNumberQuantityData(String orderId, String trackingNumberQuantities){
		if(DigitalStringUtil.isNotBlank(trackingNumberQuantities) && 
				DigitalStringUtil.contains(trackingNumberQuantities, new String[] {":",";"})){
			
			String[] trackingNumberQuantityArray = trackingNumberQuantities.split(";");

			for(String trackingNumberQuantity : trackingNumberQuantityArray){
				String[] trqArray = trackingNumberQuantity.split(":");
				if(trackingNumberQuantityArray.length > 1 && trqArray.length < 2){
					logError("Quantity is missing for split quantity shipment from OmniOrder ::"
							+ " OrderId - " + orderId + " :: TrackingNum - " + trackingNumberQuantities);
					break;
				}
			}
		}
	}
	
	protected MutableRepositoryItem findOrderLineItem(List<RepositoryItem> orderStatusCommerceItemsRepo, String orderLineKey, String atgCommerceId){
		MutableRepositoryItem repoItem = null;
		if(orderStatusCommerceItemsRepo != null && orderStatusCommerceItemsRepo.size() > 0){
			boolean isYantraLineBlank = false;
			List<RepositoryItem> atgCommerceItMatchingRepos = new ArrayList<>();
			for(RepositoryItem orderStatusCommerceItem : orderStatusCommerceItemsRepo){
				String atgCommerceItemId = (String)orderStatusCommerceItem.getPropertyValue("atgCommerceItemId");
				String yantraLineItemId = (String)orderStatusCommerceItem.getPropertyValue("yantraLineItem");
				if(DigitalStringUtil.isNotBlank(atgCommerceItemId) && atgCommerceItemId.equalsIgnoreCase(atgCommerceId)){// Online Orders
					atgCommerceItMatchingRepos.add(orderStatusCommerceItem);
					if(!isYantraLineBlank && DigitalStringUtil.isBlank(yantraLineItemId)){
						isYantraLineBlank = true;
					}else if(orderLineKey.equalsIgnoreCase(yantraLineItemId)){
						repoItem = (MutableRepositoryItem)orderStatusCommerceItem;
						break;
					}
				}else if(DigitalStringUtil.isBlank(atgCommerceId) && DigitalStringUtil.isBlank(atgCommerceItemId)
						&& orderLineKey.equalsIgnoreCase(yantraLineItemId)){//SOM Orders
					repoItem = (MutableRepositoryItem)orderStatusCommerceItem;
					break;
				}
			}
			
			if(repoItem == null && isYantraLineBlank && atgCommerceItMatchingRepos.size() == 1){
				repoItem = (MutableRepositoryItem)atgCommerceItMatchingRepos.get(0);
			}
		}
		
		return repoItem;
	}

	protected int findNextOrderHistorySequenceNum(List<RepositoryItem> orderStatusCommerceItemsRepo){
		int nextSequenceNum = 0;
		if(orderStatusCommerceItemsRepo != null && orderStatusCommerceItemsRepo.size() > 0){
			List<Integer> sequenceNums = new ArrayList<>();
			for(RepositoryItem orderStatusCommerceItem : orderStatusCommerceItemsRepo){
				Integer sequenceNum = (Integer)orderStatusCommerceItem.getPropertyValue("sequenceNum");
				sequenceNums.add(sequenceNum);
			}
			int max = Collections.max(sequenceNums);
			if(max > 0){
				nextSequenceNum = max + 1;
			}
		}
		
		return nextSequenceNum;
	}
	
	protected DigitalOrderHistoryItem getOrderLineItem(DigitalOrderHistoryModel orderHistoryFromRepo, String yantraLineItemId){
		DigitalOrderHistoryItem ret = null;
		List<DigitalOrderHistoryItem> cis = orderHistoryFromRepo.getCommerceItems();
		for(DigitalOrderHistoryItem ci : cis){
			if(DigitalStringUtil.isNotEmpty(ci.getYantraLineItemId()) && ci.getYantraLineItemId().equalsIgnoreCase(yantraLineItemId)){
				ret = ci;
				break;
			}
		}
		
		return ret;
	}

	public DigitalOrderTools getOrderTools() {
		return (DigitalOrderTools)this.getOrderManager().getOrderTools();
	}

	public Map<String, String> getLineItemStatus() {
		DigitalBaseConstants dswConstants = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.DSW_BASE_CONSTANTS, DigitalBaseConstants.class);
		return dswConstants.getOrderLineItemStatusMap();
	}
}
