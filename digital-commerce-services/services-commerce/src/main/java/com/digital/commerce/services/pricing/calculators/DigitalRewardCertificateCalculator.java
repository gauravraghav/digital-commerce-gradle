package com.digital.commerce.services.pricing.calculators;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.digital.commerce.constants.OrderConstants.OrderPropertyManager;
import com.digital.commerce.constants.ProducCatalogConstants.ProducCatalogPropertyManager;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderPriceInfo;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderDiscountCalculator;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingException;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;


/**
 */
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalRewardCertificateCalculator extends OrderDiscountCalculator {

	private DigitalRewardsManager rewardCertificateManager;

	private String[] undiscountableProductTypes;

	private OrderManager mOrderManager;

	public void setUndiscountableProductTypes(
			String[] undiscountableProductTypes) {
		this.undiscountableProductTypes = undiscountableProductTypes;
		Arrays.sort(undiscountableProductTypes);
	}

	public void priceOrder(OrderPriceInfo pPriceQuote, Order pOrder,
			RepositoryItem pPricingModel, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters)
			throws PricingException {

		if (isLoggingDebug()) {
			logDebug("pricing order");
		}
		if (isLoggingDebug()) {
			logDebug("LOYALTY CERTIFICATE CALCULATOR INVOKED!!!!!!");
		}
		double qualifiedOrderAmount = this.getRewardCertificateManager().getQualifiedOrderAmount(pOrder);
		double orderAmount = pPriceQuote.getAmount();
		double unAdjustedAmount = orderAmount;

		DigitalOrderImpl order = (DigitalOrderImpl) pOrder;
		Set<RepositoryItem> certs = order.getRewardCertificates();
		if (isLoggingDebug()) {
			logDebug("ORDER HAS: " + certs.size() + " CERTIFICATES");
		}
		if (certs != null) {

			clearAllCIShares(order);
			String amountPropName = OrderPropertyManager.REWARD_CERTIFICATE_AMT_USED.getValue();
			String amountAuthPropName = OrderPropertyManager.REWARD_CERTIFICATE_AMT_AUTH.getValue();

			double amountToApply = 0.0;

			for (RepositoryItem cert : certs) {

				Double certAmount = (Double) cert.getPropertyValue(amountAuthPropName);
				amountToApply = certAmount.doubleValue();

				if (isLoggingDebug()) {
					logDebug("applying the certificate:"
							+ cert.getItemDisplayName() + ":" + certAmount);
				}

				if (isLoggingDebug())
					logDebug("PRINTING OUT QUALIFIED ORDER AMOUNT "
							+ qualifiedOrderAmount);

				if (amountToApply > qualifiedOrderAmount) {
					amountToApply = qualifiedOrderAmount;
				}
				if (isLoggingDebug())
					logDebug("PRINTING OUT AMOUNT TO APPLY " + amountToApply);

				if (isLoggingDebug()) {
					logDebug("APPLYING REWARD AMOUNT: " + amountToApply);
				}
				qualifiedOrderAmount = getPricingTools().round(qualifiedOrderAmount - amountToApply);
				orderAmount = getPricingTools().round(orderAmount - amountToApply);

				if (isLoggingDebug()) {
					logDebug("qualified Order amount : " + qualifiedOrderAmount);
					logDebug("amount applied: " + amountToApply);
					logDebug("new order amount: " + orderAmount);
				}

				pPriceQuote.setAmount(orderAmount);
				PricingAdjustment rewardAdjustment = new PricingAdjustment(
						Constants.ORDER_DISCOUNT_PRICE_ADJUSTMENT_DESCRIPTION,
						null, -amountToApply, 1);
				pPriceQuote.setDiscounted(true);
				pPriceQuote.getAdjustments().add(rewardAdjustment);

				MutableRepositoryItem mutCert = (MutableRepositoryItem) cert;
				mutCert.setPropertyValue(amountPropName, new Double(amountToApply));

				distributeRewardsShareAcrossCommerceItems(pPriceQuote, pOrder,
						cert, amountToApply);
			}

			for (RepositoryItem cert : certs) {

				Double certAmount = (Double) cert.getPropertyValue(amountPropName);
				logDebugForTax("cert : " + cert);
				logDebugForTax("certAmount : " + certAmount);				
				amountToApply = certAmount.doubleValue();

				updateItemsDiscountShare(pPriceQuote, pOrder, unAdjustedAmount, amountToApply);
			}

		}
	}
	
	public void logDebugForTax(String str) {
		if (isLoggingDebug()) {
			logDebug(str);
		}
	}

	private void clearAllCIShares(DigitalOrderImpl order) {

		List<DigitalCommerceItem> cis = order.getCommerceItems();
		if (cis != null) {
			for (DigitalCommerceItem ci : cis) {
				if (ci.getRewardsCertificateShare() == null)
					ci.setPropertyValue("rewardCertificateShare", new HashMap(3));
				else
					((Map) ci.getRewardsCertificateShare()).clear();

			}
		}
	}

	private void distributeRewardsShareAcrossCommerceItems(
			OrderPriceInfo pPriceQuote, Order order, RepositoryItem cert,
			double amountToApply) {

		List<DigitalCommerceItem> cis = getItemsToReceiveDiscountShare(order);
		if (cis == null) {
			return;
		}
		DigitalCommerceItem lastEligibleItem = null;

		for (DigitalCommerceItem item : cis) {
			ItemPriceInfo price = (ItemPriceInfo) item.getPriceInfo();

			double oldShare = price.getOrderDiscountShare();
			double remainingItemAmount = price.getAmount() - oldShare;

			if (remainingItemAmount > 0.0) {
				lastEligibleItem = item;
			}
		}

		double appliedCertAmount = 0.0;

		for (DigitalCommerceItem ci : cis) {
			RepositoryItem prod = (RepositoryItem) ci.getAuxiliaryData()
					.getProductRef();
			String prodType = (String) prod.getPropertyValue(ProducCatalogPropertyManager.PRODUCT_TYPE.getValue());
			if (isLoggingDebug()) {
				logDebug("product type: " + prodType);
			}
			if (prodType == null
					|| (prodType != null
							&& getUndiscountableProductTypes() != null
							&& getUndiscountableProductTypes().length > 0 && Arrays
							.binarySearch(getUndiscountableProductTypes(), prodType) < 0)) {

				double ratio= (ci.getPriceInfo().getAmount() - ci.getPriceInfo().getOrderDiscountShare())
						 		/getRewardCertificateManager().getQualifiedOrderAmount(order);

				// if we divided by zero... use zero instead
				if (Double.isNaN(ratio) || Double.isInfinite(ratio)) {
					if (isLoggingDebug()) {
						Object[] arguments = { "updateItemDiscountShare",
								Double.toString(ci.getPriceInfo().getAmount()),
								Double.toString(pPriceQuote.getRawSubtotal()) };

						logDebug(MessageFormat.format(Constants.QUOTIENT_IS_NAN, arguments));
					}
					ratio = 0.0;

				}

				Double certShare = 0.0;

				// adjust penny difference 
				if (ratio != 0.0 && ci.equals(lastEligibleItem)) {
					certShare = Double.valueOf(getPricingTools().round(amountToApply - appliedCertAmount));
				} else {
					certShare = Double.valueOf("" + getPricingTools().round(ratio * amountToApply));
				}

				if (isLoggingDebug()) {
					logDebug("---------------------------------");
					logDebug("ci " + ci);
					logDebug("PRINTING OUT RATIO " + ratio);
					logDebug("PRINTING OUT AMOUNTTOAPPLY " + amountToApply);
					logDebug("PRINTING OUT CERTSHARE " + certShare);
					logDebug("PRINTING OUT appliedCertAmount "
							+ appliedCertAmount);
					logDebug("---------------------------------");
				}

				String certNum = ""
						+ cert.getPropertyValue("certificateNumber") + "-"
						+ cert.getRepositoryId();
				
				Map<String, Double> m = ci.getRewardsCertificateShare();
				m.put(certNum, certShare);
				ci.setRewardCertificateShare(m);

				appliedCertAmount += certShare;
				if (isLoggingDebug()) {
					logDebug("certNum " + certNum);
					logDebug("appliedCertAmount " + appliedCertAmount);
				}
			}
		}
	}

	/**
	 * This method will override the OrderDiscountCalculator's method to update
	 * the orderDiscountShare property of each CommerceItem's priceInfo. This
	 * happens if <code>saveItemsOrderDiscountShare</code> is true. If
	 * <code>saveDetailsOrderDiscountShare</code> is true, then the
	 * orderDiscountShare property of each DetailedItemPriceInfo is also
	 * updated.
	 * 
	 * @param pOrder
	 *            the order that was discounted
	 * @param pUnadjustedPrice
	 *            The original pre-discount price of the order
	 * @param pTotalAdjustment
	 *            This is the total change to the order total, for the current
	 *            promotion
	 **/

	private void updateItemsDiscountShare(OrderPriceInfo pPriceQuote, Order pOrder,
			double pUnadjustedPrice, double pTotalAdjustment)
			throws PricingException {

		List<DigitalCommerceItem> items = getItemsToReceiveDiscountShare(pOrder);
		logDebugForTax("items : " + items);
		double shareSoFar = 0.0;
		logDebugForTax("shareSoFar : " + shareSoFar);
		for (int c = 0; c < items.size(); c++) {
			CommerceItem item = (CommerceItem) items.get(c);
			logDebugForTax("item : " + item);

			RepositoryItem prod = (RepositoryItem) item.getAuxiliaryData().getProductRef();
			logDebugForTax("prod : " + prod);

			ItemPriceInfo price = item.getPriceInfo();
			logDebugForTax("price : " + price);
			
			DigitalCommerceItem dswItem = ((DigitalCommerceItem)item);
			
			logDebugForTax("item.getOrderPromoShare() : " + dswItem.getOrderPromoShare());
			double cumulativeOrderPromoShare = dswItem.getCumulativeOrderPromoShare();
			logDebugForTax("cumulativeOrderPromoShare : " + cumulativeOrderPromoShare);			
			
			logDebugForTax("item.getRewardsCertificateShare() : " + dswItem.getRewardsCertificateShare());
			double cumulativeRewardsCertificateShare = dswItem.getCumulativeRewardsCertificateShare();
			logDebugForTax("cumulativeRewardsCertificateShare : " + cumulativeRewardsCertificateShare);
			
			double roundedCumulativeOrderPromoCertShare = getPricingTools().round(cumulativeOrderPromoShare);
			double roundedCumulativeRewardsCertShare = getPricingTools().round(cumulativeRewardsCertificateShare);
			
			logDebugForTax("roundedCumulativeRewardsCertShare : " + roundedCumulativeRewardsCertShare);
			logDebugForTax("roundedCumulativeOrderPromoCertShare : " + roundedCumulativeOrderPromoCertShare);
			
			shareSoFar = shareSoFar + roundedCumulativeRewardsCertShare + roundedCumulativeOrderPromoCertShare;
			logDebugForTax("shareSoFar : " + shareSoFar);
			
			
			if (c == (items.size() - 1)) {
				logDebugForTax("lastItem in the bag: [Y]");
				double leftovers = 0;
				
				logDebugForTax("pPriceQuote.getAdjustments() : " + pPriceQuote.getAdjustments());
				double cumulativeOrderAdjustmentAmount = ((DigitalOrderPriceInfo)pPriceQuote).getCumulativeOrderAdjustmentAmount();
				logDebugForTax("cumulativeOrderAdjustmentAmount : " + cumulativeOrderAdjustmentAmount);
				leftovers = cumulativeOrderAdjustmentAmount - shareSoFar;
				logDebugForTax("leftovers : " + leftovers);
				price.setOrderDiscountShare(getPricingTools().roundDown(leftovers + roundedCumulativeRewardsCertShare + roundedCumulativeOrderPromoCertShare));
			} else {
				logDebugForTax("lastItem in the bag: [N]");
				price.setOrderDiscountShare(getPricingTools().roundDown(roundedCumulativeRewardsCertShare + roundedCumulativeOrderPromoCertShare));
			}
			logDebugForTax("price.setOrderDiscountShare : " + price.getOrderDiscountShare());
		}
	}

	protected List<DigitalCommerceItem> getItemsToReceiveDiscountShare(Order pOrder) {
		List<DigitalCommerceItem> items = pOrder.getCommerceItems();
		if (items == null)
			return null;
		boolean foundFreeItem = false;
		for (int i = 0; i < items.size(); i++) {
			CommerceItem item = (CommerceItem) items.get(i);
			ItemPriceInfo amountInfo = item.getPriceInfo();
			if (Math.abs(amountInfo.getAmount()) > 1.0000000000000001E-005D)
				continue;
			if (!foundFreeItem) {
				foundFreeItem = true;
				items = new ArrayList<>(items);
			}
			items.remove(i);
			i--;
		}

		List<DigitalCommerceItem> nonGWItems = new ArrayList<>();

		for (DigitalCommerceItem item:items) {
			RepositoryItem prod = (RepositoryItem) item.getAuxiliaryData().getProductRef();
			String prodType = null;
			if (prod != null) {
				prodType = (String) prod.getPropertyValue(ProducCatalogPropertyManager.PRODUCT_TYPE.getValue());
			}
			if (!getRewardCertificateManager().isProductDiscountable(prodType)) {
				nonGWItems.add(item);
			}
		}
		return nonGWItems;
	}
}
