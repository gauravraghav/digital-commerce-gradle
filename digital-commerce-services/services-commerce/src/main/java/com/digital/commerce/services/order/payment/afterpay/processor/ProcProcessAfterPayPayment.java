package com.digital.commerce.services.order.payment.afterpay.processor;

import atg.commerce.CommerceException;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.commerce.payment.processor.ProcProcessPaymentGroup;
import atg.payment.PaymentStatus;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.order.payment.afterpay.AfterPayPaymentInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcProcessAfterPayPayment extends ProcProcessPaymentGroup {

    private AfterPayPaymentProcessor afterPayPaymentProcessor;

    @Override
    public PaymentStatus authorizePaymentGroup(PaymentManagerPipelineArgs pParams) throws CommerceException {
        final String METHOD_NAME = "authorizePaymentGroup";
        try{
            DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

            AfterPayPaymentInfo appi = null;

            try {
                appi = (AfterPayPaymentInfo) pParams.getPaymentInfo();
            } catch (ClassCastException cce) {
                if (isLoggingError()){
                    logError("Expecting class of type AfterPayPaymentInfo but got: " + pParams.getPaymentInfo().getClass().getName());
                }
                throw cce;
            }
            if (isLoggingDebug()){
                logDebug("AfterPayPaymentInfo info is :" + appi.toString());
            }
            return getAfterPayPaymentProcessor().capturePayment(appi);

        }finally{
            DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
        }
    }

    @Override
    public PaymentStatus debitPaymentGroup(PaymentManagerPipelineArgs pParams) throws CommerceException {
        if (isLoggingInfo()){
            logInfo("No requirement to do debit from storefront for DSW");
        }
        return null;
    }

    @Override
    public PaymentStatus creditPaymentGroup(PaymentManagerPipelineArgs paymentManagerPipelineArgs) throws CommerceException {
        if (isLoggingInfo()){
            logInfo("No requirement to do credit from storefront for DSW");
        }
        return null;
    }
}
