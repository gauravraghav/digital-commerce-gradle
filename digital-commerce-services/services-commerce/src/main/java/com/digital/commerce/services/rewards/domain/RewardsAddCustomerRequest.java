package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 */
@Getter
@Setter
public class RewardsAddCustomerRequest {

    private String profileId;
    private String loyaltyNumber;
    private String email;
    private String firstName;
    private String lastName;
    private String gender;
    private String dayOfBirth;
    private String monthOfBirth;
    private String mobilePhone;
    private String homePhone;
    private String optOutEmail;
    private String optOutMobile;
    private String noEMail;
    private String noPhone;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String postCode;
    private String country;
    private String preferredStore;
    private String doBankPoints;
    private String badPhone;
    private String badEmail;
    private String badAddress;

}
