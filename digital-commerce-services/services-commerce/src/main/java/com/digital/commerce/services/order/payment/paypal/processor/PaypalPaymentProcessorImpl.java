package com.digital.commerce.services.order.payment.paypal.processor;

import java.sql.Timestamp;
import java.util.Date;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.constants.PaypalConstants;
import com.digital.commerce.services.order.payment.paypal.PaypalCheckoutManager;
import com.digital.commerce.services.order.payment.paypal.PaypalPaymentInfo;
import com.digital.commerce.services.order.payment.paypal.valueobject.PaypalAuthorizeResponse;

import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.nucleus.logging.ApplicationLoggingImpl;
import lombok.Getter;
import lombok.Setter;


/** Implementation class for PayPal payment processing.
 * 
 */
@Getter
@Setter
public class PaypalPaymentProcessorImpl extends ApplicationLoggingImpl implements PaypalPaymentProcessor {

	private static final String SERVICE_NAME = "PaypalPaymentProcessor";
	private PaypalCheckoutManager	paypalCheckoutManager;

	public PaypalPaymentProcessorImpl() {
		super(PaypalPaymentProcessorImpl.class.getName());
	}
	
	private OrderManager orderManager;

	/** Performs authorize operations on PaypalPayment paymentgroup.
	 * Does both cmpi_order and the cmpi_authorize call
	 * 
	 * @param paypalPaymentInfo
	 * @return */
	public PaypalPaymentStatus authorize( PaypalPaymentInfo paypalPaymentInfo ) {
		final String METHOD_NAME = "authorize";
		PaypalPaymentStatusImpl paymentStatus = null;

		if( isLoggingDebug() ) logDebug( "Cardinal OrderId:" + paypalPaymentInfo.getCardinalOrderId() );

		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
			Order order = paypalPaymentInfo.getOrder();
			
			// PayPal Order call is made only for new users
			if( paypalPaymentInfo.isNewUser() ) {
				logInfo( "Paypal Order Msg for Order:" + order.getId() );
				PaypalAuthorizeResponse orderResponse = getPaypalCheckoutManager().order( paypalPaymentInfo, order );
				if( !PaypalConstants.AUTHORIZE_APPROVED_STATUS.equals( orderResponse.getStatusCode() ) ) {
					// Order response is not success
					String error = generateErrorResponse( orderResponse );
					logInfo( "Paypal Order Msg Error:" + error );
					logInfo( "Paypal Cardinal OrderId:" + paypalPaymentInfo.getCardinalOrderId() );

					paymentStatus = new PaypalPaymentStatusImpl(paypalPaymentInfo.getCardinalOrderId(), paypalPaymentInfo.getAmount(), false, error, new Timestamp( new Date().getTime() ), null );
					return paymentStatus;
				} else {
					logInfo( "Paypal Order Msg is Successful" );
					logInfo( "Paypal Cardinal OrderId:" + paypalPaymentInfo.getCardinalOrderId() );
				}
			}

			// Paypal Auth call
			logInfo( "Paypal Auth Msg for Order:" + order.getId() );
			PaypalAuthorizeResponse authResponse = getPaypalCheckoutManager().authorize( paypalPaymentInfo, order );

			if( !PaypalConstants.AUTHORIZE_APPROVED_STATUS.equals( authResponse.getStatusCode() ) ) {
				// Order response is not success
				String error = generateErrorResponse( authResponse );
				logInfo( "Paypal Auth Msg Error:" + error );
				logInfo( "Paypal Cardinal OrderId:" + paypalPaymentInfo.getCardinalOrderId() );

				paymentStatus = new PaypalPaymentStatusImpl( paypalPaymentInfo.getCardinalOrderId(), paypalPaymentInfo.getAmount(), false, error, new Timestamp( new Date().getTime() ), null );
				return paymentStatus;
			} else {
				logInfo( "Paypal Auth Msg is Successful" );
				logInfo( "Paypal Cardinal OrderId:" + paypalPaymentInfo.getCardinalOrderId() );
			}
			paymentStatus = new PaypalPaymentStatusImpl( paypalPaymentInfo.getCardinalOrderId(), paypalPaymentInfo.getAmount(), true, null, new Timestamp( new Date().getTime() ), authResponse.getAuthorizationCode() );

		} catch( Exception pEx ) {
			logError( pEx );
			String error = pEx.getMessage();
			paymentStatus = new PaypalPaymentStatusImpl( paypalPaymentInfo.getCardinalOrderId(), paypalPaymentInfo.getAmount(), false, error, new Timestamp( new Date().getTime() ), null );
		}finally{
        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
        }
		return paymentStatus;
	}

	
	/** Generates Error Response using Status code, Error & Reason Code
	 * 
	 * @param authRes
	 * @return */

	private String generateErrorResponse( PaypalAuthorizeResponse authRes ) {
		String error = "";
        switch (authRes.getStatusCode()) {
            case PaypalConstants.AUTHORIZE_PENDING_STATUS:
                error = PaypalConstants.ERROR_MSG_PENDING;
                break;
            case PaypalConstants.AUTHORIZE_UNAVAILABLE_STATUS:
                error = PaypalConstants.ERROR_MSG_UNAVAILABLE;
                break;
            default:
                error = PaypalConstants.ERROR_MSG_ERROR;
                break;
        }
		if( authRes.getErrorNo() != null && authRes.getErrorNo().length() > 0 ) {
			error = error + ":" + authRes.getErrorNo();
		}
		if( authRes.getErrorDesc() != null && authRes.getErrorDesc().length() > 0 ) {
			error = error + ":" + authRes.getErrorDesc();
		}
		if( authRes.getReasonCode() != null && authRes.getReasonCode().length() > 0 ) {
			error = error + ":" + authRes.getReasonCode();
		}
		if( authRes.getReasonDesc() != null && authRes.getReasonDesc().length() > 0 ) {
			error = error + ":" + authRes.getReasonDesc();
		}

		return error;
	}

	
	
}
