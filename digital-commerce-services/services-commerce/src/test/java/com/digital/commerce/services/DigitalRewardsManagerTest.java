package com.digital.commerce.services;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;
import junit.framework.TestCase;

public class DigitalRewardsManagerTest extends TestCase {

  private final Logger log = Logger.getLogger("DigitalRewardsManagerTest");
  private final String CLASS_NAME = DigitalRewardsManagerTest.class.getName();

  /**
   * Test to check the derive date after today
   */
  public void testDeriveBirthdayGiftSendDateAfterToday() {
    String METHOD_NAME = "testDeriveBirthdayGiftSendDateAfterToday";
    log.entering(CLASS_NAME, METHOD_NAME);
    Calendar requestDate = Calendar.getInstance();
    requestDate.set(requestDate.get(Calendar.YEAR),
        Calendar.DECEMBER,
        30, 0, 0);
    Calendar result = deriveBirthdayGiftSendDate(Integer.toString(requestDate.get(Calendar.DATE)),
        Integer.toString(requestDate.get(Calendar.MONTH) + 1));
    assertTrue(result.get(Calendar.MONTH) == requestDate.get(Calendar.MONTH));
    assertTrue(result.get(Calendar.DATE) == requestDate.get(Calendar.DATE));
    assertTrue(result.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR));
    log.exiting(CLASS_NAME, METHOD_NAME);
  }

  /**
   * Test to check the derive date today
   */
  public void testDeriveBirthdayGiftSendDateToday() {
    String METHOD_NAME = "testDeriveBirthdayGiftSendDateToday";
    log.entering(CLASS_NAME, METHOD_NAME);
    Calendar requestDate = Calendar.getInstance();
    requestDate.set(requestDate.get(Calendar.YEAR),
        requestDate.get(Calendar.MONTH),
        requestDate.get(Calendar.DATE), 0, 0);
    Calendar result = deriveBirthdayGiftSendDate(Integer.toString(requestDate.get(Calendar.DATE)),
        Integer.toString(requestDate.get(Calendar.MONTH) + 1));
    assertTrue(result.get(Calendar.MONTH) == requestDate.get(Calendar.MONTH));
    assertTrue(result.get(Calendar.DATE) == requestDate.get(Calendar.DATE));
    assertTrue(result.get(Calendar.YEAR) != Calendar.getInstance().get(Calendar.YEAR));
    Calendar resultCal = Calendar.getInstance();
    resultCal.add(Calendar.YEAR, 1);
    assertTrue(result.get(Calendar.YEAR) == resultCal.get(Calendar.YEAR));
    log.exiting(CLASS_NAME, METHOD_NAME);
  }

  /**
   * Test to check the derive date tomorrow
   */
  public void testDeriveBirthdayGiftSendDateTomorrow() {
    String METHOD_NAME = "testDeriveBirthdayGiftSendDateTomorrow";
    log.entering(CLASS_NAME, METHOD_NAME);
    Calendar requestDate = Calendar.getInstance();
    requestDate.set(requestDate.get(Calendar.YEAR),
        requestDate.get(Calendar.MONTH),
        requestDate.get(Calendar.DATE) + 1, 0, 0);
    Calendar result = deriveBirthdayGiftSendDate(Integer.toString(requestDate.get(Calendar.DATE)),
        Integer.toString(requestDate.get(Calendar.MONTH) + 1));
    assertTrue(result.get(Calendar.MONTH) == requestDate.get(Calendar.MONTH));
    assertTrue(result.get(Calendar.DATE) == requestDate.get(Calendar.DATE));
    assertTrue(result.get(Calendar.YEAR) != Calendar.getInstance().get(Calendar.YEAR));
    Calendar resultCal = Calendar.getInstance();
    resultCal.add(Calendar.YEAR, 1);
    assertTrue(result.get(Calendar.YEAR) == resultCal.get(Calendar.YEAR));
    log.exiting(CLASS_NAME, METHOD_NAME);
  }

  /**
   * Test to check the derive date after tomorrow
   */
  public void testDeriveBirthdayGiftSendDateAfterTomorrow() {
    String METHOD_NAME = "testDeriveBirthdayGiftSendDateAfterTomorrow";
    log.entering(CLASS_NAME, METHOD_NAME);
    Calendar requestDate = Calendar.getInstance();
    // Test was originally designed for non-leap year, post February so set the date to March 3
    requestDate.set(Calendar.DATE, 3);
    requestDate.set(Calendar.MONTH,2);
    requestDate.set(requestDate.get(Calendar.YEAR),
        requestDate.get(Calendar.MONTH),
        requestDate.get(Calendar.DATE) + 2, 0, 0);
    Calendar result = deriveBirthdayGiftSendDate(Integer.toString(requestDate.get(Calendar.DATE)),
        Integer.toString(requestDate.get(Calendar.MONTH) + 1));
    assertTrue(result.get(Calendar.MONTH) == requestDate.get(Calendar.MONTH));
    assertTrue(result.get(Calendar.DATE) == requestDate.get(Calendar.DATE));
    assertTrue(result.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR));
    log.exiting(CLASS_NAME, METHOD_NAME);
  }

  /**
   * Test to check the derive date after today
   */
  public void testDeriveBirthdayGiftSendDateDec31() {
    String METHOD_NAME = "testDeriveBirthdayGiftSendDateAfterTodayDec31";
    log.entering(CLASS_NAME, METHOD_NAME);
    Calendar result = deriveBirthdayGiftSendDate("31", "12");
    assertTrue(result.get(Calendar.MONTH) == 11);
    assertTrue(result.get(Calendar.DATE) == 31);
    assertTrue(result.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR));
    log.exiting(CLASS_NAME, METHOD_NAME);
  }

  /**
   * Test to check the derive date before today
   */
  public void testDeriveBirthdayGiftSendDateJan01() {
    String METHOD_NAME = "testDeriveBirthdayGiftSendDateJan01";
    log.entering(CLASS_NAME, METHOD_NAME);
    Calendar result = deriveBirthdayGiftSendDate("01", "01");
    assertTrue(result.get(Calendar.DATE) == 1);
    assertTrue(result.get(Calendar.MONTH) == 0);
    Calendar resultCal = Calendar.getInstance();
    resultCal.add(Calendar.YEAR, 1);
    assertTrue(result.get(Calendar.YEAR) == resultCal.get(Calendar.YEAR));
    log.exiting(CLASS_NAME, METHOD_NAME);
  }

  /**
   * Test to check the derive date before today
   */
  public void testDeriveBirthdayGiftSendDateBeforeFeb01() {
    String METHOD_NAME = "testDeriveBirthdayGiftSendDateBeforeFeb01";
    log.entering(CLASS_NAME, METHOD_NAME);
    Calendar result = deriveBirthdayGiftSendDate("1", "2");
    assertTrue(result.get(Calendar.DATE) == 1);
//    Calendar fictitiousDate = Calendar.getInstance();
//    fictitiousDate.set(Calendar.DATE, 3);
//    fictitiousDate.set(Calendar.MONTH, 3);
    assertTrue(result.get(Calendar.YEAR) != Calendar.getInstance().get(Calendar.YEAR));
    Calendar resultCal = Calendar.getInstance();
    resultCal.add(Calendar.YEAR, 1);
    assertTrue(result.get(Calendar.YEAR) == resultCal.get(Calendar.YEAR));
    log.exiting(CLASS_NAME, METHOD_NAME);
  }

  /**
   * Test to check the derive date before today but a leap year
   */
  public void testDeriveBirthdayGiftSendDateBeforeTodayLeapYear() {
    String METHOD_NAME = "testDeriveBirthdayGiftSendDateBeforeTodayLeapYear";
    log.entering(CLASS_NAME, METHOD_NAME);
    Calendar result = deriveBirthdayGiftSendDate("29", "02");
    // Original test assumed there would be no leap year.
    if (new GregorianCalendar().isLeapYear(result.get(Calendar.YEAR))) {
      assertTrue(result.get(Calendar.DATE) == 29);
    } else {
      assertTrue(result.get(Calendar.DATE) == 28);
    }
    assertTrue(result.get(Calendar.MONTH) == 1);
    //assertTrue(result.get(Calendar.YEAR) != Calendar.getInstance().get(Calendar.YEAR));
    Calendar resultCal = Calendar.getInstance();
    resultCal.add(Calendar.YEAR, 1);
    assertTrue(result.get(Calendar.YEAR) == resultCal.get(Calendar.YEAR));
    log.exiting(CLASS_NAME, METHOD_NAME);
  }

  /**
   * @param dayOfBirth
   * @param monthOfBirth e.g. 01 - Jan, 02 - Feb ...... 12 - Dec
   * @return Calendar
   */
  private Calendar deriveBirthdayGiftSendDate(String dayOfBirth, String monthOfBirth) {
    Calendar baseSendCalendarDate = Calendar.getInstance();
    // Test was originally designed for non-leap year, post February so set the date to March 3
    baseSendCalendarDate.set(Calendar.DATE, 3);
    baseSendCalendarDate.set(Calendar.MONTH,2);
    if (isLoggingDebug()) {
      logDebug("Base Gift Send date is " + baseSendCalendarDate.getTime());
    }

    int birthDayGiftBlackOutDays = 1;

    if (birthDayGiftBlackOutDays > 0) {
      baseSendCalendarDate.add(Calendar.DATE, birthDayGiftBlackOutDays);
      if (isLoggingDebug()) {
        logDebug("Updated Base Gift Send date is " + baseSendCalendarDate.getTime() +
            " after adding birthDayGiftBlackOutDays.");
      }
    }

    // the value used to set the <code>MONTH</code> calendar field in the calendar.
    // Month value is 0-based. e.g., 0 for January.
    int zeroBasedCalendarMonth = Integer.parseInt(monthOfBirth) - 1;

    Calendar requestedSendCalendarDate = Calendar.getInstance();
    requestedSendCalendarDate.set(requestedSendCalendarDate.get(Calendar.YEAR),
        zeroBasedCalendarMonth,
        Integer.parseInt(dayOfBirth), 0, 0);
    if (isLoggingDebug()) {
      logDebug("Gift Send date based on request is " + requestedSendCalendarDate.getTime());
    }

    Calendar derivedSendCalendarDate = Calendar.getInstance();

    // Request date is before today so increment year by 1
    if (requestedSendCalendarDate.getTime().compareTo(baseSendCalendarDate.getTime()) < 0) {
      int numberOfYearsToAdd = 1;
      derivedSendCalendarDate.add(Calendar.YEAR, numberOfYearsToAdd);
      if (isLoggingDebug()) {
        logDebug("Updated derived Gift Send date is " + derivedSendCalendarDate.getTime() +
            " after year increment as requested date was less than today.");
      }
    } else {
      if (isLoggingDebug()) {
        logDebug("Requested Gift Send date was greater than today so use the year as is.");
      }
    }

    // If derived month is Feb & day is > 28
    if (zeroBasedCalendarMonth == Calendar.FEBRUARY && Integer.parseInt(dayOfBirth) > 28) {

      if (!new GregorianCalendar().isLeapYear(derivedSendCalendarDate.get(Calendar.YEAR))) {
        // set the day to 28 as this is not a leap year
        derivedSendCalendarDate.set(derivedSendCalendarDate.get(Calendar.YEAR),
            zeroBasedCalendarMonth, 28, 0, 0);
        if (isLoggingDebug()) {
          logDebug(
              "Updated derived Gift Send date is " + derivedSendCalendarDate.getTime()
                  + " since it is not a leap year.");
        }
      }
      // if it is a leap year & day > 29
      else if (Integer.parseInt(dayOfBirth) > 29) {
        // set the day to 29 as this is a leap year
        derivedSendCalendarDate.set(derivedSendCalendarDate.get(Calendar.YEAR),
            zeroBasedCalendarMonth, 29, 0, 0);
        if (isLoggingDebug()) {
          logDebug(
              "Updated derived Gift Send date is " + derivedSendCalendarDate.getTime()
                  + " since it is a leap year max allowed day is 29.");
        }
      }
      // if it is a leap year & day < 29
      else {
        derivedSendCalendarDate.set(derivedSendCalendarDate.get(Calendar.YEAR),
            zeroBasedCalendarMonth,
            Integer.parseInt(dayOfBirth), 0, 0);
        if (isLoggingDebug()) {
          logDebug(
              "Updated derived Gift Send date is " + derivedSendCalendarDate.getTime()
                  + "since it is a leap year honor the requested day.");
        }
      }
    } else {
      derivedSendCalendarDate.set(derivedSendCalendarDate.get(Calendar.YEAR),
          zeroBasedCalendarMonth,
          Integer.parseInt(dayOfBirth), 0, 0);
    }

    if (isLoggingDebug()) {
      logDebug("Final derived Gift Send date is " + derivedSendCalendarDate.getTime());
    }

    return derivedSendCalendarDate;
  }

  private String getMonth(int month) {
    return new DateFormatSymbols().getMonths()[month-1];
  }

  private boolean isLoggingDebug(){
    return true;
  }

  public void logDebug(String pMessage) {
    log.info(pMessage);
  }
}
