package com.digital.commerce.services.storelocator;

import atg.commerce.locations.Coordinate;
import atg.commerce.locations.GeoLocatorFormHandler;
import atg.commerce.locations.GeoLocatorService;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletFormException;
import atg.dtm.UserTransactionDemarcation;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalNumberUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.region.RegionProfile;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletException;

/**
 * @author jvasired
 *
 */
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalStoreLocatorFormHandler extends GeoLocatorFormHandler {

	RepeatingRequestMonitor repeatingRequestMonitor;

	private String itemId;

	private String locationId;

	private String filterOutOfStock;

	private String skuId;

	private String locateItemsInventory;

	private String geoDistance;

	private String geoLatitude;

	private String geoLongitude;

	private DigitalGeoLocationUtil geoLocationUtil;

	private DigitalStoreLocatorInventoryManager storeLocatorInventoryManager;

	private RegionProfile regionProfile;

	private boolean resultsFromSearch;

	private List storeLocatorResults;

	private static final String SHIP_TO_STORE = "shipToStore";
	private static final String PICKUP_IN_STORE = "pickupInStore";
	private static final String SHIP_TO_STORE_BOSTS = "shipToStoreBOSTS";
	private static final String PICKUP_IN_STORE_BOPIS = "pickupInStoreBOPIS";

	/**
	 * @param request
	 * @param response
	 * @return true or false
	 * @throws DropletFormException
	 */
	public boolean beforeSet(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws DropletFormException {
		this.geoDistance = request.getParameter("distance");
		this.geoLatitude = request.getParameter("latitude");
		this.geoLongitude = request.getParameter("longitude");
		resetSessionAttributes(request, response);
		return super.beforeSet(request, response);
	}

	/**
	 * Override super as maxResultsPerPage is not getting set.
	 */
	public boolean handleCurrentResultPageNum(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "DigitalStoreLocatorFormHandler.handleCurrentResultPageNum";
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			try {
				if (!DigitalStringUtil.isEmpty(pRequest.getQueryParameter("maxResultsPerPage"))) {
					setMaxResultsPerPage(Integer.parseInt(pRequest.getQueryParameter("maxResultsPerPage")));
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return super.handleCurrentResultPageNum(pRequest, pResponse);
	}

	/**
	 * Override OOTB method to list only those stores has inventory on PDP/cart
	 * page.
	 */
	@Override
	protected ArrayList getItems(Coordinate pCoordinate, double pDistance, Collection<String> pSiteIds)
			throws RepositoryException {
		ArrayList<RepositoryItem> result = null;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "DigitalStoreLocatorFormHandler.getItems";
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			try {
				result = super.getItems(pCoordinate, pDistance, pSiteIds);
				Boolean updateTransientStoreValues=false;
				if (locateItemsInventory != null && Boolean.parseBoolean(locateItemsInventory)) {
					result = storeLocatorInventoryManager.getSkuInventoryForLocation(getSkuId(), result);
					updateTransientStoreValues=true;
				} else if (result != null && result.size() > 0 && !DigitalStringUtil.isEmpty(filterOutOfStock)
						&& Boolean.parseBoolean(filterOutOfStock) && !DigitalStringUtil.isEmpty(getSkuId())) {
					RepositoryItem[] results = geoLocationUtil.filterOutOfStock(result, getSkuId(),
							Boolean.parseBoolean(filterOutOfStock));
					if (results != null && results.length > 0) {
						updateStoreTransientProperties(new ArrayList(Arrays.asList(results)));
						return new ArrayList(Arrays.asList(results));
					}
					updateTransientStoreValues=true;
				}
				
				if(!updateTransientStoreValues && result != null && result.size() > 0){
					updateStoreTransientProperties(result);
				}
				
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return result;
	}
	
	private void updateStoreTransientProperties(ArrayList<RepositoryItem> result) {
		// TODO Auto-generated method stub
		for(RepositoryItem item:result){
		       if(isShipToStore(item)){
		        	((MutableRepositoryItem) item).setPropertyValue(SHIP_TO_STORE, "Y");
		        }else{
		        	((MutableRepositoryItem) item).setPropertyValue(SHIP_TO_STORE, "N");
		        }
		        
		        if(isPickupInStore(item)){
		        	((MutableRepositoryItem) item).setPropertyValue(PICKUP_IN_STORE, "Y");
		        }else{
		        	((MutableRepositoryItem) item).setPropertyValue(PICKUP_IN_STORE, "N");
		        }
		}
		
	}
	
	private boolean isShipToStore(RepositoryItem storeItem){
		String sItem=null;
		if(null!=storeItem){
			sItem=(String)storeItem.getPropertyValue(SHIP_TO_STORE_BOSTS);
		}
		if (null!=sItem){
		    if("1".equalsIgnoreCase(sItem) || "Y".equalsIgnoreCase(sItem)){
				if(isLoggingDebug()){
					logDebug("Store is Ship To Store Eligible");
				}
				return true;
			}
		}
		return false;
	}
	
	private boolean isPickupInStore(RepositoryItem storeItem){
		String sItem=null;
		if(null!=storeItem){
			sItem=(String)storeItem.getPropertyValue(PICKUP_IN_STORE_BOPIS);
		}
		if (null!=sItem){
			if ("1".equalsIgnoreCase(sItem) || "Y".equalsIgnoreCase(sItem)) {
				if(isLoggingDebug()){
					logDebug("Store is PickUp In Store Eligible");
				}
				return true;
			}
		}
		return false;
	 }

	/**
	 * Override OOTB as if result is null, it is setting lat&long to session
	 * stored values.
	 */
	@Override
	protected Coordinate getLocation(DynamoHttpServletRequest pRequest) {
		Coordinate result = null;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "DigitalStoreLocatorFormHandler.getLocation";
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			try {
				GeoLocatorService service = getGeoLocatorService();
				if ((service != null) && (service.getProvider() != null)) {
					if ((service.isAllowFreeFormEntry()) && (getAddress() != null)) {
						vlogDebug("Locating by address: {0}", new Object[] { getAddress() });
						result = service.getGeoLocation(getAddress());
					} else if (!DigitalStringUtil.isEmpty(getPostalCode())) {
						vlogDebug("Locating by postal code: {0}", new Object[] { getPostalCode() });
						result = service.getGeoLocation(getPostalCode(), new Locale("en"));
					}

					if ((service.isAllowCityAndState()) && (!DigitalStringUtil.isEmpty(getCity()))
							&& (!DigitalStringUtil.isEmpty(getState()))) {
						vlogDebug("Locating by city & state: {0}, {1}", new Object[] { getCity(), getState() });
						result = service.getGeoLocation(getCity(), getState(), null);
					}
				}
				if (result != null) {
					setLatitude(result.getLatitude());
					setLongitude(result.getLongitude());
					setResultsFromSearch(true);
				} else if (getLatitude() != 0.0 && getLongitude() != 0.0) {
					result = new Coordinate(getLatitude(), getLongitude());
					setResultsFromSearch(true);
				} else if (result == null
						&& (regionProfile.getLatitude() != 0.0 && regionProfile.getLongitude() != 0.0)) {
					// get the latitude and longitude from Akamai headers
					result = new Coordinate(regionProfile.getLatitude(), regionProfile.getLongitude());
					setResultsFromSearch(false);
				}
			} finally {

				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return result;
	}

	/**
	 *
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleLocateItems(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		boolean result = false;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String METHOD_NAME = "handleLocateItems";
		if ((rrm == null) || (rrm.isUniqueRequestEntry(METHOD_NAME))) {
			final UserTransactionDemarcation td = TransactionUtils.startNewTransaction(this.getName(), METHOD_NAME);
			try {
				result = super.handleLocateItems(pRequest, pResponse);
				Collection<RepositoryItem> items = getLocationResults();
				List<DigitalStoreLocatorModel> storeLocations = new ArrayList<>();
				if(null != items) {
					for (RepositoryItem storeLocation : items) {
						storeLocations.add(getGeoLocationUtil().convertStoreLocator(storeLocation));
					}
				}
				setStoreLocatorResults(storeLocations);
				return result;
			} catch (Exception ex) {
				TransactionUtils.rollBackTransaction(td, this.getName(), METHOD_NAME);
				if (isLoggingError()) {
					logError(ex);
				}
			} finally {
				TransactionUtils.endTransaction(td, this.getName(), METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
			}
		}
		return result;
	}

	

	/**
	 * Helps to reset Session values to defaults
	 * 
	 * @param request
	 * @param response
	 */
	private void resetSessionAttributes(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "DigitalStoreLocatorFormHandler.resetSessionAttributes";
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			try {
				if (DigitalStringUtil.isEmpty(request.getParameter("postalCode"))) {
					super.setPostalCode(null);
				}
				if (DigitalStringUtil.isEmpty(request.getParameter("city"))) {
					super.setCity(null);
				}
				if (DigitalStringUtil.isEmpty(request.getParameter("state"))) {
					super.setState(null);
				}
				if (DigitalStringUtil.isEmpty(request.getParameter("itemId"))) {
					setItemId(null);
				}
				if (DigitalStringUtil.isEmpty(request.getParameter("filterOutOfStock"))) {
					setFilterOutOfStock("false");
				}
				if (DigitalStringUtil.isNotEmpty(geoDistance) && DigitalNumberUtil.isNumber(geoDistance)) {
					setDistance(Double.parseDouble(geoDistance));
				}
				else {
					setDistance(0.0);
				}
				if (DigitalStringUtil.isNotEmpty(geoLatitude) && DigitalNumberUtil.isNumber(geoLatitude)) {
					setLatitude(Double.parseDouble(geoLatitude));
				}
				else {
					setLatitude(0.0);
				}
				if (DigitalStringUtil.isNotEmpty(geoLongitude) && DigitalNumberUtil.isNumber(geoLongitude)) {
					setLongitude(Double.parseDouble(geoLongitude));
				}
				else {
					setLongitude(0.0);
				}
				if (DigitalStringUtil.isEmpty(request.getParameter("currentResultPageNum"))) {
					super.setCurrentResultPageNum(0);
				}
				if (DigitalStringUtil.isEmpty(request.getParameter("maxResultsPerPage"))) {
					super.setMaxResultsPerPage(0);
				}
				// Request scoped values not retained when returning from Super
				// methods, explicit set
				if (!DigitalStringUtil.isEmpty(request.getParameter("skuId"))) {
					this.setSkuId(request.getParameter("skuId"));
				} else {
					this.setSkuId(null);
				}
				if (!DigitalStringUtil.isEmpty(request.getParameter("locateItemsInventory"))) {
					this.setLocateItemsInventory(request.getParameter("locateItemsInventory"));
				} else {
					this.setLocateItemsInventory(null);
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}

	}
}