package com.digital.commerce.services.storelocator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.services.common.DigitalBaseGeoLocationUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;

import atg.adapter.gsa.query.Builder;
import atg.commerce.inventory.InventoryException;
import atg.commerce.inventory.InventoryInfo;
import atg.commerce.inventory.InventoryManager;
import atg.commerce.inventory.LocationInventoryManager;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalGeoLocationUtil extends DigitalBaseGeoLocationUtil {
	
	private Repository repository;
	private Repository regionRepository;
	private String storeLocatorQuery;
	private String bostsEnabledQuery;
	private String bospisEnabledQuery;
	private String locationItemType;
	private InventoryManager inventoryManager;
	
	private static final String METHOD_NAME_FIND_BY_SQL = "findBySQL";
	private static final String METHOD_NAME_FIND_BY_RQL = "findByRQL";
	
	/**
	 * 
	 * @param sql
	 * @param itemDescriptor
	 * @return
	 */
	
	public RepositoryItem[] findBySQL(String sql, String itemDescriptor) {
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_FIND_BY_SQL);
			RepositoryView view = getViewChecked(itemDescriptor, true);
			Builder builder = (Builder) view.getQueryBuilder();
			return view.executeQuery(builder.createSqlPassthroughQuery(sql,
					null));
		} catch (Exception ex) {
			if(isLoggingError()){
				logError("DigitalGeoLocationManager findBySQL",ex);
			}
		}finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_FIND_BY_SQL);
		}
		return null;
	}
	
	/**
	 * 
	 * @param itemDescriptor
	 * @param isLocationRepository
	 * @return
	 */
	/*private RepositoryView getViewChecked(String itemDescriptor,boolean isLocationRepository) {
		RepositoryView view = null;
		try {
			if (isLocationRepository) {
				view = repository.getView(itemDescriptor);
			} else {
				view = regionRepository.getView(itemDescriptor);
			}

		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("DigitalGeoLocationManager getViewChecked",e);
			}
		}
		if (view == null) {
			return null;
		}
		return view;
	}*/
	
	public RepositoryItem[] findByRQL(String rql, String itemDescriptor,Object[] params) {
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_FIND_BY_RQL);
			RepositoryView view = getViewChecked( itemDescriptor,false );
			RqlStatement statement = RqlStatement.parseRqlStatement( rql );
			return statement.executeQuery( view, params );
		} catch( Exception ex ) {
			if(isLoggingError()){
				logError("DigitalGeoLocationManager findByRQL",ex);
			}
		}finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_FIND_BY_RQL);
		}
		return null;
	}
	
	/**
	 * Filters stores with inventory
	 * @param repItems
	 * @param skuId 
	 * @return
	 */
	public RepositoryItem[] filterOutOfStock(ArrayList<RepositoryItem> repItems, String pInventoryId,boolean availabilityFilter){
		String METHOD_NAME = "filterOutOfStock";
		RepositoryItem[] storeItemsArray = null;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			List<String> locationIds = new ArrayList<>();
			Map<String,String> locationIdsWithItemInStock = null;
			List<RepositoryItem> listOfStoresWithStock = null;
			for(RepositoryItem  repositoryItem : repItems){
				locationIds.add(repositoryItem.getRepositoryId());
			}
			 if ((locationIds != null) && (!locationIds.isEmpty())) {
		         try {
		        	 Collection<InventoryInfo> infos = ((LocationInventoryManager)inventoryManager).
		        			 queryInventoryInformation(pInventoryId, locationIds);
						if (availabilityFilter) {
							for(InventoryInfo inventoryInfo: filterOutOfStockInventoryItems(infos)){
								if(listOfStoresWithStock==null)
									locationIdsWithItemInStock = new HashMap<>();
								locationIdsWithItemInStock.put(inventoryInfo.getLocationId(),inventoryInfo.getInventoryId());
							}
						}
						for(RepositoryItem  storeItemWithStock : repItems){
							if(null!=locationIdsWithItemInStock) {
							if(!DigitalStringUtil.isBlank(locationIdsWithItemInStock.get(storeItemWithStock.getRepositoryId()))){
								if(listOfStoresWithStock==null)
									listOfStoresWithStock = new ArrayList<>();
								listOfStoresWithStock.add(storeItemWithStock);
							}
							}
						}
		         } catch (InventoryException e) {
		        	 if(isLoggingError()){
		 				logError("DigitalGeoLocationManager filterOutOfStock",e);
		 			}
		         }
		       } 
			storeItemsArray = listOfStoresWithStock.toArray(new RepositoryItem[listOfStoresWithStock.size()]); 
		}
		finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
	    }
		return storeItemsArray;
	}
	
	/**
	 * Filters OutOf Stock Inventory Items
	 * 
	 * @param pInfos
	 * @return
	 */
	private Collection<InventoryInfo> filterOutOfStockInventoryItems(Collection<InventoryInfo> pInfos)
	{
	  String METHOD_NAME = "filterOutOfStockInventoryItems";
	  Collection<InventoryInfo> filteredInfos = null;
	  try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			Integer availabilityStatus;
			if (null != pInfos) {
				availabilityStatus = null;
				for (InventoryInfo info : pInfos) {
					if (info != null) {
						availabilityStatus = info.getAvailabilityStatus();
						if ((availabilityStatus != null) && (availabilityStatus.intValue() != 1001)
								&& (info.getStockLevel() > 0)) {
							if (null == filteredInfos)
								filteredInfos = new ArrayList();
							filteredInfos.add(info);
						}
					}
				}
			}
	  }
	  finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
	    }
	  return filteredInfos;
	}


	
	public RepositoryItem[] getLocationForStore(String storeId){
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_FIND_BY_SQL);
			RepositoryView view = getViewChecked("location", true);
			Builder builder = (Builder) view.getQueryBuilder();
			String storeQuery = "select location_id from  dsw_store where store_number="+storeId;
			return view.executeQuery(builder.createSqlPassthroughQuery(storeQuery,null));
		} catch (Exception ex) {
			if(isLoggingError()){
				logError("DigitalGeoLocationManager findBySQL",ex);
			}
		}finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_FIND_BY_SQL);
		}
		return null;
		
	}
	
	
	public RepositoryItem findById(String id)  {
		RepositoryItem retVal = null;
		try {
			retVal = repository.getItem(id, "location");
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			logError("RepositoryException:", e);
		}
		return retVal;
	}
	
	/*public boolean isBOPISEnabled(){
		final String METHOD_NAME = "isBOPISEnabled";
		boolean bopisEnabled = true;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			RepositoryView view = getViewChecked("location", true);
			Builder builder = (Builder) view.getQueryBuilder();
			if(isLoggingDebug()){
				logDebug("DigitalGeoLocationManager isBOPISEnabled query :: " + getBospisEnabledQuery());
			}
			int bopisStores = view.executeCountQuery(builder.createSqlPassthroughQuery(getBospisEnabledQuery(),null));
			if(bopisStores < 1){
				bopisEnabled = false;
			}
		} catch (Exception ex) {
			if(isLoggingError()){
				logError("DigitalGeoLocationManager isBOPISEnabled",ex);
			}
		}finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
		}		
		return bopisEnabled;
	}*/
	
	/*public boolean isBOSTSEnabled(){
		final String METHOD_NAME = "isBOPISEnabled";
		boolean bostsEnabled = true;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			RepositoryView view = getViewChecked("location", true);
			Builder builder = (Builder) view.getQueryBuilder();
			if(isLoggingDebug()){
				logDebug("DigitalGeoLocationManager isBOPISEnabled query :: " + getBostsEnabledQuery());
			}
			int bostsStores = view.executeCountQuery(builder.createSqlPassthroughQuery(getBostsEnabledQuery(),null));
			if(bostsStores < 1) {
				bostsEnabled = false;
			}
		} catch (Exception ex) {
			if(isLoggingError()){
				logError("DigitalGeoLocationManager isBOSTSEnabled",ex);
			}
		}finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
		}		
		return bostsEnabled;
	}*/
	
	
	
	/**
	 *
	 * @param storeLocation
	 * @return DigitalStoreLocatorModel
	 */
   public DigitalStoreLocatorModel convertStoreLocator(RepositoryItem storeLocation) {
       DigitalStoreLocatorModel model = new DigitalStoreLocatorModel();
       if(null != storeLocation) {
				 model.setRepositoryId(storeLocation.getRepositoryId());
				 Object pickupInStore = storeLocation.getPropertyValue("pickupInStore");
				 if (null != pickupInStore) {
					 model.setPickupInStore((String) pickupInStore);
				 }
				 Object storeHours = storeLocation.getPropertyValue("storeHours");
				 if (null != storeHours) {
					 model.setStoreHours((String) storeLocation.getPropertyValue("storeHours"));
				 }
				 Object dstInd = storeLocation.getPropertyValue("dstInd");
				 if (null != dstInd) {
					 model.setDstInd((Boolean) dstInd);
				 }
				 Object stateAddress = storeLocation.getPropertyValue("stateAddress");
				 if (null != stateAddress) {
					 model.setState((String) stateAddress);
				 }
				 Object locationId = storeLocation.getPropertyValue("locationId");
				 if (null != locationId) {
					 model.setLocationId((String) locationId);
				 }
				 Object address1 = storeLocation.getPropertyValue("address1");
				 if (null != address1) {
					 model.setAddress1((String) address1);
				 }
				 Object address2 = storeLocation.getPropertyValue("address2");
				 if (null != address2) {
					 model.setAddress2((String) address2);
				 }
				 Object country = storeLocation.getPropertyValue("country");
				 if (null != country) {
					 model.setCountry((String) country);
				 }
				 Object city = storeLocation.getPropertyValue("city");
				 if (null != city) {
					 model.setCity((String) city);
				 }
				 Object storeNumber = storeLocation.getPropertyValue("storeNumber");
				 if (null != storeNumber) {
					 model.setStoreNumber((String) storeNumber);
				 }
				 Object distance = storeLocation.getPropertyValue("distance");
				 if (null != distance) {
					 model.setDistance((Double) distance);
				 }
				 Object faxNumber = storeLocation.getPropertyValue("faxNumber");
				 if (null != faxNumber) {
					 model.setFaxNumber((String) faxNumber);
				 }
				 Object postalCode = storeLocation.getPropertyValue("postalCode");
				 if (null != postalCode) {
					 model.setPostalCode((String) postalCode);
				 }
				 Object phoneNumber = storeLocation.getPropertyValue("phoneNumber");
				 if (null != phoneNumber) {
					 model.setPhoneNumber((String) phoneNumber);
				 }
				 Object mallPlazaName = storeLocation.getPropertyValue("mallPlazaName");
				 if (null != mallPlazaName) {
					 model.setName((String) mallPlazaName);
					 model.setMallPlazaName((String) mallPlazaName);
				 }
				 Object shipToStore = storeLocation.getPropertyValue("shipToStore");
				 if (null != shipToStore) {
					 model.setShipToStore((String) shipToStore);
				 }
				 Object rolledUpStockLevel = storeLocation.getPropertyValue("rolledUpStockLevel");
				 if (null != rolledUpStockLevel) {
					 model.setRolledUpStockLevel((Long) rolledUpStockLevel);
				 }
				 Object longitude = storeLocation.getPropertyValue("longitude");
				 if (null != longitude) {
					 model.setLongitude((Double) longitude);
				 }
				 Object latitude = storeLocation.getPropertyValue("latitude");
				 if (null != latitude) {
					 model.setLatitude((Double) latitude);
				 }
				 Object stockLevel = storeLocation.getPropertyValue("stockLevel");
				 if (null != stockLevel) {
					 model.setStockLevel((Long) stockLevel);
				 }
			 }
       return model;
   }
}
