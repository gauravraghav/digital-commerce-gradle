package com.digital.commerce.services.storelocator;

//import com.digital.commerce.services.order.DigitalShippingGroupManager;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalStoreTools {
	
	private DigitalGeoLocationUtil geoLocationUtil;
	
	private String fetchStoreQuery;

	/*DigitalShippingGroupManager dswshipgroupmgr;
	public DigitalStoreAddress fetchStoreAddress( String storeNumber) {
    	return dswshipgroupmgr.fetchStoreAddress(storeNumber);
	}*/
	/**
	 * 
	 * @param storeNumber
	 * @return
	 */
	public DigitalStoreAddress fetchStoreAddress(String storeNumber) {
		String searchQuery = getFetchStoreQuery()+ storeNumber+"'";
		RepositoryItem store = null;
		RepositoryItem[] stores = geoLocationUtil.findBySQL( searchQuery, "location" );
		DigitalStoreAddress storeAddress= new DigitalStoreAddress();
		if( stores != null ) {
			store = (RepositoryItem)stores[0];
		}
		if( store != null ) {
			storeAddress.setAddress1((String)store.getPropertyValue("address1"));
			storeAddress.setAddress2((String)store.getPropertyValue("address2"));
			storeAddress.setAddress3((String)store.getPropertyValue("address3"));
			storeAddress.setCity((String)store.getPropertyValue("city"));
			storeAddress.setStateAddress((String)store.getPropertyValue("stateAddress"));
			storeAddress.setPostalCode((String)store.getPropertyValue("postalCode"));
			storeAddress.setCountry((String)store.getPropertyValue("country"));
			String phoneNumber = (String) store.getPropertyValue("phoneNumber");
			phoneNumber = phoneNumber.replace("-", "");
			storeAddress.setPhoneNumber(phoneNumber);
			String mallPlazaName =  (String) store.getPropertyValue("mallPlazaName");
			storeAddress.setMallPlazaName(mallPlazaName);
		}
		return storeAddress;
	}
}