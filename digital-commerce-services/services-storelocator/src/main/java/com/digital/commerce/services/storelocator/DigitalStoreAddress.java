package com.digital.commerce.services.storelocator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalStoreAddress {
	private String address1;
	private String address2;
	private String address3;
	private String city;
	private String stateAddress;
	private String postalCode;
	private String country;
	private String phoneNumber;
	private String mallPlazaName;
}
