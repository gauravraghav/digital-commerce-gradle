package com.digital.commerce.services.storelocator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;

import atg.commerce.inventory.InventoryManager;
import atg.commerce.locations.Coordinate;
import atg.commerce.locations.CoordinateManager;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalCoordinateManager extends CoordinateManager
{
 
  private static final double	MILES_TO_METERS	= 1609.344;
  private static final String METHOD_NAME_GET_NEAREST = "getNearest";
  private static final String METHOD_NAME_FILTERBY_DISTANCE = "filterByDistance";
  
  
  private Repository regionRepository;
  private DigitalGeoLocationUtil geoLocationUtil;
  private String locationItemType;
  private int freeFormEntryResultsSize;
  private String locationQuery;
  private double defaultRaduis;
  private InventoryManager inventoryManager;
  

@Override  
/**
 * This method overrides OOTB method to get nearest store details 
 */
public RepositoryItem[] getNearest(Coordinate pCoordinate, double pRadius, Collection<String> pSiteIds, String pItemType)
    throws RepositoryException
  {
		RepositoryItem[] items = null;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_GET_NEAREST);
			double lRadius = (pRadius == 0.0) ? defaultRaduis : pRadius;
			if (pCoordinate != null && pCoordinate.getLatitude() != 0.0 && pCoordinate.getLongitude() != 0.0) {
				if(isLoggingDebug()){
				logDebug("Co-Ordinates Latitude "
						+ Double.valueOf(pCoordinate.getLatitude())
						+ " Longitude "
						+ Double.valueOf(pCoordinate.getLongitude())
						+ " Radius " + Double.valueOf(lRadius));
			}
			RepositoryView view = getRepository().getView(locationItemType);
			Query query = getCoordinateRadiusQuery(pCoordinate, lRadius, view,
					getCoordinatePropertyName(), pSiteIds);
			StringBuilder stBuilder = new StringBuilder(locationQuery);
			if (query != null) {
				stBuilder.append(query.toString());
				items = geoLocationUtil.findBySQL(stBuilder.toString(),
						locationItemType);
			} else if ((query == null) && (lRadius < 0.0D)) {
				query = getSiteQuery(view, pSiteIds);
				if (query == null) {
					query = view.getQueryBuilder().createUnconstrainedQuery();
					items = view.executeQuery(query);
				}
			}
			if (query == null) {
				return null;
			}
			if (isLoggingDebug() && query != null) {
				logDebug("DigitalCoordinateManager getNearest Query: "
						+ query.toString() + " " + stBuilder.toString());
			}

			if (items == null) {
				return null;
			}
			items = filterByDistance(items, pCoordinate, lRadius);
		  }
		  if (isLoggingDebug()){
				for(RepositoryItem repItem : items){
					logDebug("DigitalCoordinateManager getNearest RepositoryId :"
							+ repItem.getRepositoryId() + " Distance :"+ repItem.getPropertyValue("distance"));
				}
			}	

		} catch (Exception ex) {
			if (isLoggingError()) {
				logError("DigitalCoordinateManager getNearest ", ex);
			}
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_GET_NEAREST);
		}
		return items;
  }

@Override
protected RepositoryItem[] filterByDistance(RepositoryItem[] pItems, Coordinate pCoordinate, double pDistance)
{
		List<RepositoryItem> filteredItems = new ArrayList<>();
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_FILTERBY_DISTANCE);
			if (pDistance < 0.0D) {
				if (isLoggingDebug()) {
					logDebug("DigitalCoordinateManager filterByDistance pDistance is zero: ");
				}
				return pItems;
			}

			for (RepositoryItem item : pItems) {
				Double distance = Double.valueOf(getDistanceBetweenCoordinates(
						pCoordinate, item)) / MILES_TO_METERS;
				if (isLoggingDebug()) {
					logDebug("DigitalCoordinateManager filterByDistance Distance: "
							+ distance + "Requested/Configured Distance :"
							+ pDistance);
				}
				if (distance.doubleValue() < pDistance) {
					if ((getDistancePropertyName() != null)
							&& ((item instanceof MutableRepositoryItem))) {
						((MutableRepositoryItem) item).setPropertyValue(
								getDistancePropertyName(), distance);
					}
					filteredItems.add(item);
				} else {
					if (isLoggingDebug()) {
						logDebug("DigitalCoordinateManager Filtered distance is greater than the max distance DisplayName:"
								+ item.getItemDisplayName()
								+ "Distance: "
								+ distance
								+ "Requested/Configured Distance :"
								+ pDistance);
					}
				}
			}

			if (filteredItems.size() == 0) {
				return null;
			}
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_FILTERBY_DISTANCE);
		}
		return (RepositoryItem[]) filteredItems.toArray(new RepositoryItem[0]);
}

@Override
  protected Query getCoordinateRadiusQuery(Coordinate pCoordinate, double pRadius, RepositoryView pView, String pItemCoordinatePropertyName, Collection<String> pSiteIds)
    throws RepositoryException
  {
	 //Convert Miles to Meters as OOTB adds meters to Latitude/Longitude
	 double radius=pRadius * MILES_TO_METERS;
	 return super.getCoordinateRadiusQuery(pCoordinate, radius, pView, pItemCoordinatePropertyName, pSiteIds);
  }
}
