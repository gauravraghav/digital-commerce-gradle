package com.digital.commerce.services.storelocator;

import java.util.Locale;

import atg.repository.*;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;

import atg.commerce.locations.Coordinate;
import atg.commerce.locations.GeoLocatorProvider;
import atg.nucleus.GenericService;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalGeoLocatorProvider extends GenericService implements GeoLocatorProvider{

	private Repository	regionRepository;
	private boolean allowCityAndState;
	private DigitalGeoLocationUtil geoLocationUtil;
	private String itemType;
	private String coOrdinateQuery;
	private String coOrdinateQueryStateName;
	private String coOrdinateQueryInCaseNoResults;
	private String coOrdinateQueryStateNameInCaseNoResults;
	private DigitalBaseConstants dswConstants; 
	
	private static final String	POSTAL_CODE	= "postalCode";
	private static final String	LAT	= "latitude";
	private static final String	LONG = "longitude";
	private static final String METHOD_NAME_GET_GEO_LOC = "getGeoLocation";
	private static final String METHOD_NAME_GEO_LOC_CITY_STATE = "getGeoLocationForCityState";
	
	@Override
	public boolean isAllowFreeFormEntry() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * Returns Co-ordinates[latitude&longitude] for a given zip code
	 * 
	 */
	@Override
	public Coordinate getGeoLocation(String zipCode, Locale paramLocale) {
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_GET_GEO_LOC);
			RepositoryView view = regionRepository.getView(itemType );
			RepositoryItem repoItem;
			if( null != view ) {
				QueryBuilder builder = view.getQueryBuilder();
				QueryExpression prop = builder.createPropertyQueryExpression( POSTAL_CODE );
				QueryExpression constant = builder.createConstantQueryExpression( zipCode );
				Query query = builder.createComparisonQuery( prop, constant, QueryBuilder.EQUALS );
				QueryOptions queryOptions = new QueryOptions(0,1, new SortDirectives(), null);
				RepositoryItem[] items = view.executeQuery( query, queryOptions );
				if( items != null && items.length >= 1 ) {
						repoItem = (RepositoryItem)items[0];
						if(isLoggingDebug()){
							logDebug("DigitalGeoLocatorProvider getGeoLocation Latitude "+(double)repoItem.getPropertyValue(LAT)+
									"Longitude "+(double)repoItem.getPropertyValue(LONG));
						}
						Coordinate coOrdinate = new Coordinate();
						coOrdinate.setLatitude((double)repoItem.getPropertyValue(LAT));
						coOrdinate.setLongitude((double)repoItem.getPropertyValue(LONG));
						return coOrdinate;
				}
			}
		} catch( RepositoryException e) {
			if(isLoggingError()){
				logError("DigitalGeoLocatorProvider getGeoLocation",e);
			}
		}finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_GET_GEO_LOC);
		}
		return null;
	}

	/**
	 * Returns Co-ordinates[latitude&longitude] for a given city and State
	 * @param city
	 * @param state
	 * @return
	 */
	@Override
	public Coordinate getGeoLocation(String city, String state,Locale paramLocale)
	{
		try
		{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_GEO_LOC_CITY_STATE);
			Object[] params = new Object[]{city, state, "D"};
			RepositoryItem[] results = getGeoLocationByStateCity(state, params); 
			
			Coordinate coordinate = null;
			if( results != null && results.length > 0 ) {
				// Calculate the average latitude and longitude for all of the zip codes with the given city name
				double latitude = 0, longitude = 0;
				for( int i = 0; i < results.length; i++ ) {
					latitude += (Double)results[i].getPropertyValue( LAT );
					longitude += (Double)results[i].getPropertyValue( LONG );
				}
				latitude = latitude / results.length;
				longitude = longitude / results.length;
				coordinate = new Coordinate();
				coordinate.setLatitude( latitude );
				coordinate.setLongitude( longitude );
			} else {
				// If no results were found among proper cities, search for colloquial names (city_type = N or A) and return the location of the first
				params = new Object[] { city, state, "N", "A" };
				results = getGeoLocationByStateCityNoResults(state, params);
				if( results != null && results.length > 0 ) {
					RepositoryItem result = results[0];
					coordinate = new Coordinate( (Double)result.getPropertyValue( LAT ), (Double)result.getPropertyValue( LONG ) );
				}
			}
			if(isLoggingDebug() && coordinate!=null){
				logDebug("DigitalGeoLocatorProvider getGeoLocation Latitude "+coordinate.getLatitude()+
						"Longitude "+coordinate.getLongitude());
			}
			return coordinate;
		} catch( Exception ex ) {
			if(isLoggingError()){
				logError("DigitalGeoLocatorProvider getGeoLocation city state ",ex);
			}
		}finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME_GEO_LOC_CITY_STATE);
		}
		return null;
	}

	/**
	 * @param state
	 * @param params
	 * @return
	 */
	private RepositoryItem[] getGeoLocationByStateCity(String state, Object[] params) {
		RepositoryItem[] results;
		if(state!=null && state.length()>Integer.parseInt(getDswConstants().getStateCodeLength())){
			results= geoLocationUtil.findByRQL(coOrdinateQueryStateName, itemType, params);
		}else{
			results= geoLocationUtil.findByRQL(coOrdinateQuery, itemType, params);
		}
		return results;
	}
	
	/**
	 * @param state
	 * @param params
	 * @return
	 */
	private RepositoryItem[] getGeoLocationByStateCityNoResults(String state, Object[] params) {
		RepositoryItem[] results;
		if(state!=null && state.length()>Integer.parseInt(getDswConstants().getStateCodeLength())){
			results= geoLocationUtil.findByRQL(coOrdinateQueryStateNameInCaseNoResults, itemType, params);
		}else{
			results= geoLocationUtil.findByRQL(coOrdinateQueryInCaseNoResults, itemType, params);
		}
		return results;
	}
}
