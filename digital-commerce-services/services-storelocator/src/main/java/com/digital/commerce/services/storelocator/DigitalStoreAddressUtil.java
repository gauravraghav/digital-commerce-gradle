package com.digital.commerce.services.storelocator;

import static com.digital.commerce.constants.DigitalProfileConstants.MIL_ADDRESS_TYPE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.common.AddressType;
import com.digital.commerce.services.common.CountryCode;
import com.digital.commerce.services.common.DigitalAddress;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.common.StateCode;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.storelocator.DigitalGeoLocationUtil;
import com.digital.commerce.services.storelocator.DigitalStoreAddress;
import com.google.common.base.Strings;

import atg.commerce.util.PlaceList;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalStoreAddressUtil extends GenericService {
	//private String					validShippingPlaces;
	private String storeDetailsQuery;
	
	//private Map<Object, PlaceList>	countryPlaceMap	= new HashMap<>();
	private DigitalGeoLocationUtil geoLocationUtil;
	//private List<String>			placeLists		= new ArrayList<>();

	
	/**
	 * Get the store details given store number as input
	 * @param storeNumber
	 * @return
	 */
	public DigitalStoreAddress fetchStoreAddress(String storeNumber) {
		DigitalStoreAddress storeAddress = new DigitalStoreAddress();
		final String METHOD_NAME = "fetchStoreAddress";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			if (!DigitalStringUtil.isBlank(storeNumber)) {
				String searchQuery = getStoreDetailsQuery() + storeNumber + "'";
				RepositoryItem store = null;
				RepositoryItem[] stores = geoLocationUtil.findBySQL(searchQuery, "location");
				if (stores != null) {
					store = (RepositoryItem) stores[0];
				}
				if (store != null) {
					storeAddress.setAddress1((String) store.getPropertyValue("address1"));
					storeAddress.setAddress2((String) store.getPropertyValue("address2"));
					storeAddress.setAddress3((String) store.getPropertyValue("address3"));
					storeAddress.setCity((String) store.getPropertyValue("city"));
					storeAddress.setStateAddress((String) store.getPropertyValue("stateAddress"));
					storeAddress.setPostalCode((String) store.getPropertyValue("postalCode"));
					storeAddress.setCountry((String) store.getPropertyValue("country"));
					String phoneNumber = (String) store.getPropertyValue("phoneNumber");
					phoneNumber = phoneNumber.replace("-", "");
					storeAddress.setPhoneNumber(phoneNumber);
					String mallPlazaName = (String) store.getPropertyValue("mallPlazaName");
					storeAddress.setMallPlazaName(mallPlazaName);
				}

			}
		} catch (Exception ex) {
			logError("Excetpion while getting store details for store " + storeNumber);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
		}
		return storeAddress;
	}

}
