package com.digital.commerce.services.storelocator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class DigitalStoreLocatorModel implements Serializable {

    private String repositoryId;
    private String pickupInStore;
    private String storeHours;
    private Boolean dstInd;
    private String state;
    private String locationId;
    private String address1;
    private String address2;
    private String country;
    private String city;
    private String storeNumber;
    private Double distance;
    private String faxNumber;
    private String postalCode;
    private String phoneNumber;
    private String name;
    private String shipToStore;
    private Long rolledUpStockLevel;
    private Double longitude;
    private Double latitude;
    private Long stockLevel;
    private String mallPlazaName;
    private final static long serialVersionUID = 3312501826589144093L;

}