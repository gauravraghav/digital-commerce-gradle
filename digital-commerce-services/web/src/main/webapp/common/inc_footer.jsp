<!-- START FOOTER -->
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
	prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<dsp:page>
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="imagesUrl" bean="DSWConstants.imagesUrl" scope="page" />
	
	<tr>
		<td valign="top"
			style="padding: 25px 25px 15px 25px; border-top: 5px solid #000000;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top" class="mobile-column"
						style="padding-bottom: 20px;">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="top" colspan="2" style="padding-bottom: 10px;">
									<font face="'Roboto', Arial, sans-serif"> <strong>Customer
											Service:</strong>
								</font>
								</td>
							</tr>
							<tr>
								<td valign="middle" class="mobile-column"
									style="padding-bottom: 15px;">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="30"><img width="18" height="30"
												src="${imagesUrl}/is/image/DSWShoes/phone?fmt=png-alpha"
												alt="call DSW"></td>
											<td valign="middle"><a
												style="color: #000000; text-decoration: none;"
												href="tel://18663797463"> <font
													face="'Roboto', Arial, sans-serif">1.866.DSW.SHOES</font>
											</a></td>
										</tr>
									</table>
								</td>
								<td valign="middle" class="mobile-column">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="27" style="padding-right: 5px;"><img
												width="27" height="18"
												src="${imagesUrl}/is/image/DSWShoes/email?fmt=png-alpha"
												alt="email DSW"></td>
											<td valign="middle">											
											<c:choose>
													<c:when test="${isCustomerServiceContact}">
														<a style="color: #000000; text-decoration: none;"
															href="mailto:CustomerService@dsw.com"> <font
															face="'Roboto', Arial, sans-serif">CustomerService@dsw.com</font>
														</a>
													</c:when>
													<c:otherwise>
														<a style="color: #000000; text-decoration: none;"
															href="mailto:Orders@dsw.com"> <font
															face="'Roboto', Arial, sans-serif">Orders@dsw.com</font>
														</a>
													</c:otherwise>
												</c:choose></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td align="right" valign="top" class="mobile-column"><a
						style="color: #000000; padding-right: 15px;"
						href='<dsp:valueof param="siteUrl"/>?cm_mmc=emops-_-${analyticsInfo}'>
							<font face="'Roboto', Arial, sans-serif">DSW.com</font>
					</a>&nbsp;&nbsp;
						<a style="color: #000000;" href='<dsp:valueof param="findStoreUrl"/>&cm_mmc=emops-_-${analyticsInfo}'>
							<font face="'Roboto', Arial, sans-serif">Find a Store</font>
					</a></td>
				</tr>
			</table>
		</td>
	</tr>
	<!-- END FOOTER -->
	</table>

	</td>
	</tr>
	<!-- END FOOTER -->

	</table>
	</body>
	</html>
</dsp:page>