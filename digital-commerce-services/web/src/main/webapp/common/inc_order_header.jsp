<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<dsp:page>
	<c:if test="${empty orderTotal}">
		<dsp:getvalueof var="orderTotal" param="orderTotal" />
	</c:if>
	<c:if test="${empty orderDate}">
		<dsp:getvalueof var="orderDate" param="orderDate" />
	</c:if>
	<c:if test="${empty orderNumber}">
		<dsp:getvalueof var="orderNumber" param="orderNumber" />
	</c:if>
	<c:if test="${empty emailAddress}">
		<dsp:getvalueof var="emailAddress" param="emailAddress" />
	</c:if>

	<font face="'Roboto', Arial, sans-serif">
		<h3 style="margin: 15px 0;">
			Order Number:
			<a href="<dsp:valueof param="siteUrl"/>landing?type=order&id=${orderNumber}&email=${emailAddress}&cm_mmc=emops-_-${analyticsInfo}">${orderNumber}</a>
		</h3>
	</font>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td style="padding-right: 10px;" align="left" valign="top">
                <font face="'Roboto', Arial, sans-serif">
                    <span>Order Date: ${orderDate}</span><br />
				 	<span>
            			<c:if test="${isCanceledOrder eq '1'}">Revised&nbsp;</c:if>Order Total:
            			<strong><fmt:formatNumber value="${orderTotal}" type="currency" /></strong>
          			</span>
			</font>
			</td>
			<td align="right">
                <dsp:getvalueof id="rewardsNumb" param="REWARDSNUMBER" /> <c:if test="${not empty rewardsNumb and rewardsNumb ne 'NA'}">
					<font face="'Roboto', Arial, sans-serif">
                        <span>VIP Member</span><br />
                        <span># ${rewardsNumb}</span>
					</font>
				</c:if></td>
		</tr>
	</table>
</dsp:page>