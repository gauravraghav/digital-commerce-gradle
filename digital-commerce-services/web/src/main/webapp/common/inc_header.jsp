<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<dsp:page>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="imagesUrl" bean="DSWConstants.imagesUrl" scope="page" />
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE"/>
    <dspel:importbean var="config" bean="/atg/web/assetmanager/ConfigurationInfo"/>

    <style type="text/css">

        /*Calling our web font*/
        @import url("https://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Roboto:400,700,700italic,400italic");

        /* Some resets and issue fixes */
        #outlook a {
            /*padding: 0;*/
            color: #000000;
            display: block;
            width: 100%;
            height: 100%;
        }

        #outlook a:active, #outlook a:visited {
            color: #000000;
        }


        a {
            color: #000000;
            /*display: block;*/
            width: 100%;
            height: 100%;
        }

        a:active, a:visited {
            color: #000000;
        }


        #outlook a.primary-button:visited {
            color: #ffffff;
        }

        a:visited {
            color: #000000;
        }

        a.primary-button {
            text-decoration: none;
            color: #ffffff;
            text-align: center;
            display: block;
            width: 100%;
            height: 100%;
        }
        a.primary-button:visited {
            color: #ffffff;
        }

        a.secondary-button {
            text-decoration: none;
            color: #000000;
            background: #ffffff;
           /*display: block;*/
            text-align: center;
            width: 100%;
            height: 100%;
        }
        a.secondary-button:visited {
            color: #000000;
        }

        body {
            width: 100% !important;
            /*-webkit-text;*/
            size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            margin: 0;
            padding: 0;
            font-size: 14px;
            line-height: 142%;
            letter-spacing: 0.8px;
            font-family: "Roboto", Helvetica, Arial, sans-serif;
        }

        h1, h2, h3, h4, h5, h6 {
            margin: 0;
            padding: 0;
            text-align: left;
        }

        h1 {
            font-size: 24px;
            line-height: 125%;
            padding-bottom: 5px;
            text-align: left;
        }

        h2 {
            font-size: 24px;
            line-height: 100%;
            padding-bottom: 10px;
            border-bottom: 1px solid #cccccc;
            letter-spacing: 1px;
            text-align:left;
        }

        h3 {
            font-size: 18px;
            line-height: 133%;
            padding-bottom: 8px;
            letter-spacing: 0.8px;
            text-align:left;
        }
        h4 {
            font-size: 16px;
            line-height: 125%;
            letter-spacing: 0.9px;
            padding-bottom: 5px;
            text-align: left;
        }
        div{
            text-align: left;
        }
        p{
            text-align:left;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .backgroundTable {
            margin: 0 auto;
            padding: 0;
            width: 100%; !important;
        }

        table td {
            border-collapse: collapse;
            padding: 0;
            margin: 0;
        }

        .ExternalClass * {
            line-height: 100%;
        }

        .main-container {
            width: 100%;
        }

        /* End reset */

        /** MOBILE-SPECIFIC STYLING **/
        @media screen and (max-width: 763px) {

            body {
                font-size: 16px;
            }

            h2 {
                font-size: 28px;
            }

            .main-container {
                width: 100%;
            }

            /* Display block allows us to stack elements */
            *[class="mobile-column"] {
                display: block;
                text-align: left;
                width: 100%;
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            /* display TD cell as a block and preserve the padding */
            *[class="mobile-column-preserve-padding"] {
                display: block;
                text-align: left;
                width: auto;
            }

            /* Some more stacking elements */
            *[class="mob-column"] {
                float: none !important;
                width: 100%!important;
            }

            /* Hide stuff */
            *[class="hide"] {
                display: none !important;
            }

            /* This sets elements to 100% width and fixes the height issues too, a god send */
            *[class="100p"] {
                width: 100% !important;
                height: auto !important;
            }

            *[class="condensed"] {
                padding-bottom: 40px !important;
                display: block;
            }

            /* 100percent width section with 20px padding */
            *[class="100pad"] {
                width: 100% !important;
                padding: 20px;
            }

            /* 100percent width section with 20px padding left & right */
            *[class="100padleftright"] {
                width: 100% !important;
                padding: 0 20px 0 20px;
            }

            /* 100percent width section with 20px padding top & bottom */
            *[class="100padtopbottom"] {
                width: 100% !important;
                padding: 20px 0px 20px 0px;
            }

            *[class="sign-up-text"] {
                padding-top: 10px;
                margin-left: 0;
            }

        }

    </style>

</head>
<body style="padding:0; margin:0 auto; text-align: center;">

<font face="'Roboto', Arial, sans-serif">
    <div style="display:none;font-size:1px;color:#333333;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
        ${previewText}
    </div>
</font>

<table class="main-container" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td align="center" valign="top">

            <table class="100p" cellpadding="0" cellspacing="0" border="0">
                <!-- START HEADER -->
                <tr>
                    <td style="border-bottom: 5px solid #000000; padding: 25px 25px 25px 25px;">

                        <a href="<dsp:valueof param="siteUrl"/>?cm_mmc=emops-_-${analyticsInfo}">
                            <img src="${imagesUrl}/is/image/DSWShoes/desktop-d-s-w-logo"  alt="DSW Logo" width="111" height="45"/>
                        </a>
                    </td>
                </tr>
                <!-- END HEADER -->
</dsp:page>