<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<dsp:page>
<tr>
    <td style="padding: 25px 25px 11px 25px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="middle">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="45%" class="mobile-column" valign="middle" style="padding-top: 13px; padding-bottom: 13px;background-color: #000000; color: #ffffff; text-align: center; line-height: 100%;">
                                <a class="primary-button" href="<dsp:valueof param='siteUrl'/>?cm_mmc=emops-_-${analyticsInfo}#signup">
                                    <font face="'P22', Arial, sans-serif"><strong>SIGN UP FOR EMAIL</strong></font>
                                </a>
                            </td>
                            <td valign="middle" class="mobile-column" >
                                <font face="'Roboto', Arial, sans-serif">
                                    <div class="sign-up-text" style="margin-left: 20px;">
                                        Sign up to receive exclusive offers and promotions from DSW.
                                    </div>
                                </font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" colspan="2" class="mobile-column" style="text-align: center; padding: 15px 0; font-size: 12px; color: #7f7f7f;">
                    <font face="'Roboto', Arial, sans-serif">
                        <i>Your card will not be charged until the order has been picked up or shipped.</i>
                    </font>
                </td>
            </tr>
        </table>
    </td>
</tr>

</dsp:page>