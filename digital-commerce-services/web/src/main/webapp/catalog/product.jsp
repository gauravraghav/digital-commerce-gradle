<!DOCTYPE html>
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
	prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<dsp:importbean bean="/atg/targeting/RepositoryLookup" />
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
<dsp:page>
	<html style="height: 100%;">
<body style="height: 100%; text-align: center;">
	<div style="padding-right: 10px; padding-bottom: 8px; text-align: right;">
		<select id="deviceOptions" onchange="updateIframe()">
			<option value="desktop" selected="selected">Desktop</option>
			<option value="mobile">Mobile</option>
			<option value="tablet">Tablet</option>
	</select>
	</div>
	
	<dsp:droplet name="/com/dsw/commerce/catalog/DSWProductLookUpDroplet">
		<dsp:param name="id" param="prodId" />
		<dsp:param name="itemDescriptor" value="product" />
		<dsp:param name="elementName" value="product" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="product" param="product" />
			<dsp:getvalueof var="productName" param="productDisplayName" />
			<dsp:getvalueof var="productId" param="productId" />
			<dsp:getvalueof var="defaultColorCode" param="defaultColorCode" />
		</dsp:oparam>
	</dsp:droplet>

	<dsp:getvalueof var="previewSiteUrl" bean="DSWConstants.previewSiteUrl" />

	<iframe id="websiteIframe" width="100%" height="100%"
		src="<%=pageContext.getAttribute("previewSiteUrl")%>/en/us/product/<%=pageContext.getAttribute("productName")%>/<%=pageContext.getAttribute("productId")%>?activeColor=<%=pageContext.getAttribute("defaultColorCode")%>"></iframe>

	<script type="text/javascript">
	            
					window.onload = initialize;
	                var productJSONPayLoad = <%=pageContext.getAttribute("product")%>;
	                console.log(productJSONPayLoad);
	                var websiteIframe = document.getElementById('websiteIframe').contentWindow;
                    
                    // Adds event handler for the postMessage protocol in order to communicate with the iframe containing the UI
	                function initialize() {
                        console.log("adding event listener for postMessage");
	                	window.addEventListener("message", function (event) {
	                		// post the product JSON to the angular app
                            console.log("sending product data to iframe");
	                        websiteIframe.postMessage(productJSONPayLoad, '<%=pageContext.getAttribute("previewSiteUrl")%>');

	                    	}, false);
	                }
                    
                    function updateIframe() {
                    	var deviceOption = document.getElementById('deviceOptions').value;
                    	
                    	var websiteIframe = document.getElementById('websiteIframe');
                    	
                    	switch(deviceOption) {
                    	case "mobile":
                    		websiteIframe.width = "600";
                    		break;
                    	case "tablet":
                    		websiteIframe.width = "900";
                    		break;
                    	case "desktop":
                    		websiteIframe.width = "100%";
                    		break;
                    	}
                    	
                    }
                    
                </script>
</body>
	</html>
</dsp:page>
