<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="imagesUrl" bean="DSWConstants.imagesUrl" scope="page" />
<dsp:page>
	<dsp:getvalueof var="storePagesUrl" bean="DSWConstants.storePagesUrl" scope="page" />
	<dsp:importbean bean="/com/dsw/commerce/services/MessageLocator" />
	<dsp:getvalueof var="messages" bean="MessageLocator.msgKeyToStringMap" scope="page" />
	<dsp:getvalueof var="orderTotal" param="orderTotal" vartype="java.lang.String" />
	<dsp:getvalueof var="locale" param="locale" vartype="java.lang.String" />
	<dsp:getvalueof var="custFirstName" param="payLoadItems.customerFirstName" vartype="java.lang.String" />
	<dsp:getvalueof var="eventType" param="eventType"/>
	<c:if test="${empty custFirstName}">
		<c:set var="custFirstName" value="DSW Shoe Lover" />
	</c:if>
	<dsp:getvalueof var="altPickupFirstName" param="payLoadItems.altPickupFirstName" vartype="java.lang.String" />
	<dsp:getvalueof var="trackingDate" param="trackingDate" vartype="java.lang.String" />
	<c:if test="${not empty trackingDate}">
		<c:set var="analyticsInfo" value="${trackingDate}_order" />
	</c:if>
	<fmt:setLocale value="${locale}" />
	<!--start header-->
	<dsp:getvalueof var="previewText" value="Your order has been delayed." vartype="java.lang.String" />
	<%@include file="../../common/inc_header.jsp"%>
	<!--end header-->
	
	<dsp:getvalueof var="address1" param="payLoadItems.addressLine1" />
	<dsp:getvalueof var="address2" param="payLoadItems.addressLine2" />
	<dsp:getvalueof var="city" param="payLoadItems.city" />
	<dsp:getvalueof var="state" param="payLoadItems.state" />
	<dsp:getvalueof var="postalCode" param="payLoadItems.zipCode" />
	<tr style="border-bottom: 1px solid #cccccc">
		<td width="100%" style="padding: 25px 25px 25px 25px; border: 0">
			<font face="'Roboto', Arial, sans-serif">
			 <h1>Hi ${custFirstName}! Unfortunately, one or more of your items will arrive later than expected.</h1>
				<div style="margin-top:10px;">If you paid for faster shipping, shoephoria! Customer Service will contact you to update your account.</div>
				<div style="margin-top:10px;">If you have any other questions, call us at 1.866.DSW.SHOES (1.866.379.7463). We apologize for any inconvenience.</div>
			</font>
			<%@include file="../../common/inc_order_header.jsp"%>
		</td>
	</tr>
	<tr>
		<td style="padding: 25px; background-color: #ebebeb; border-top: 1px solid #cccccc;">
			<font face="'Roboto', Arial, sans-serif">
				<h2>Item(s) Ordered:</h2>
			</font>
			<dsp:getvalueof id="payload" param="payLoadItems">
				<dsp:droplet name="/atg/dynamo/droplet/ForEach">
					<dsp:param name="array" param="payLoadItems.orderLines" />
					<dsp:setvalue param="currentLine" paramvalue="element" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="fulfillmentType"
							param="currentLine.fulfillmentType" />
						<dsp:getvalueof var="shippingMethod" param="currentLine.shipCode"/>

						<c:choose>
							<c:when test="${fulfillmentType eq 'BOSTS'}">
								<c:set var="shippingMethod" value="STS" />
							</c:when>
							<c:when test="${fulfillmentType eq 'BOPIS'}">
								<c:set var="shippingMethod" value="ISPU" />
							</c:when>
							<c:when test="${fulfillmentType eq 'SHIP' && eventType eq 'LOS_CHANGE'}">
								<c:set var="shippingMethod" value="GRNS" />
							</c:when>
							<c:otherwise>
								<c:set var="shippingMethod" value="GRN" />
							</c:otherwise>
						</c:choose>

						<c:set var="shippingMethodMessageKey" scope="page"
							value="checkout.delivery.${shippingMethod}" />
						<c:set var="shippingDeliveryMessageKey"
							value="checkout.shipping.${shippingMethod}.delivery.label" />

						<c:choose>
							<c:when test="${not empty messages[shippingMethodMessageKey]}">
								<c:set var="shippingMethodLabel" scope="page"
									value="${messages[shippingMethodMessageKey]}" />
								<c:set var="shippingDeliveryLabel" scope="page"
									value="${messages[shippingDeliveryMessageKey]}" />
							</c:when>
							<c:otherwise>
								<c:set var="defaultShippingMethodLabel" scope="page"
									value="${shippingMethod}" />
							</c:otherwise>
						</c:choose>


						<c:set var="customerFirstName" value="${custFirstName}" />

						<c:if test="${empty customerFirstName}">
							<c:set var="customerFirstName" value="DSW Shoe Lover" />
						</c:if>
						<dsp:getvalueof var="mallPlazaName"
							param="payLoadItems.mallPlazaName" />

						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td colspan="2" style="padding-top: 15px;">
									<font face="'Roboto', Arial, sans-serif">
										<h3>
											Delayed
											<c:choose>
												<c:when test="${not empty shippingMethodLabel}">
												${shippingMethodLabel}
												</c:when>
												<c:otherwise>
												${defaultShippingMethodLabel}
												</c:otherwise>
											</c:choose>
											<c:choose>
												<c:when test="${fulfillmentType eq 'SHIP'}">
													${customerFirstName} at:			
												</c:when>
												<c:otherwise>
													<span>${mallPlazaName} ${shippingDeliveryLabel} </span>
												</c:otherwise>
											</c:choose>
										</h3>
									</font>
								</td>
							</tr>
						<tr>
							<td valign="middle" width="41" style="padding-bottom: 10px;">
									<c:choose>
										<c:when test="${fulfillmentType eq 'BOSTS' }">
											<img
												src="${imagesUrl}/is/image/DSWShoes/store?fmt=png-alpha"
												alt="Store">
										</c:when>
										<c:when test="${fulfillmentType eq 'BOPIS' }">
											<img
												src="${imagesUrl}/is/image/DSWShoes/time?fmt=png-alpha"
												alt="Store Time">
										</c:when>
										<c:otherwise>
											<img
												src="${imagesUrl}/is/image/DSWShoes/shipping-truck?fmt=png-alpha"
												alt="shipping truck">
										</c:otherwise>
									</c:choose>
								</td>
								<td valign="middle"	style="padding-bottom: 10px; padding-left: 15px;">
								<font face="'Roboto', Arial, sans-serif">
									<c:choose>
									<c:when test="${fulfillmentType eq 'BOSTS' or fulfillmentType eq 'BOPIS'}">
										<div>
											<a href="${storePagesUrl}/usa/${fn:toLowerCase(state)}/${fn:toLowerCase(fn:replace(city, " ", ""))}/dsw-designer-shoe-warehouse-${fn:toLowerCase(fn:replace(mallPlazaName, " ", "-"))}.html">
												${address1},
											<c:if test="${not empty address2}">
												${address2},
											</c:if>
												${city}, ${state}&nbsp;${postalCode}
											</a>
										</div>
									</c:when>
									<c:otherwise>
										<div>${address1},
											<c:if test="${not empty address2}">
												${address2},
											</c:if>
												${city}, ${state}&nbsp;${postalCode}
										</div>
									</c:otherwise>
									</c:choose>
									<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
										<dsp:param name="value" param="isAlternatePickupPerson" />
										<dsp:oparam name="false">
											<div>
												<i>Designated pick up person:
												<dsp:valueof param="payLoadItems.altPickupFirstName" />
												&nbsp;
												<dsp:valueof param="payLoadItems.altPickupLastName" />
												</i>
											</div>
										</dsp:oparam>
									</dsp:droplet> 
								</font>
								</td>
							</tr>
						</table>
						<dsp:getvalueof var="productId" param="currentLine.itemStyle" />
						<dsp:getvalueof var="itemId" param="currentLine.itemId" />
						<dsp:getvalueof var="showProductTitle" param="currentLine.productTitle" />
						<c:if test="${empty showProductTitle}">
							<dsp:getvalueof var="showProductTitle" param="currentLine.itemName" />
						</c:if>
						<c:if test="${empty showProductTitle}">
							<dsp:getvalueof var="showProductTitle" param="currentLine.itemDesc" />
						</c:if>
						<dsp:getvalueof var="showBrandName" param="currentLine.itemBrand" />
						<dsp:getvalueof var="showSize" param="currentLine.itemSize" />
						<dsp:getvalueof var="showWidth" param="currentLine.itemWidth" />
						<dsp:getvalueof var="showColor" param="currentLine.itemColor" />
						<dsp:getvalueof var="showUPC" param="currentLine.itemUPC" />
						<dsp:getvalueof var="colorCode" param="currentLine.colorCode" />
						<c:if test="${empty colorCode}">
							<c:set var="colorCode" value="001" />
						</c:if>
						<dsp:getvalueof var="unitPrice" param="currentLine.itemUnitPrice" />
						<dsp:getvalueof var="itemPromoList" param="currentLine.itemPromoList" />
						<dsp:getvalueof var="itemQty" param="currentLine.itemStatusQuantity" />
						<dsp:getvalueof var="itemStatus" param="currentLine.status" />
						<dsp:getvalueof var="itemStatusUpdateDate" param="currentLine.statusUpdateDate" />
						<dsp:getvalueof var="redOutline" value="0" />
						<%@include file="common/inc_commerce_item.jsp"%>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:getvalueof>
		</td>
	</tr>
	<%@include file="common/inc_other_items_ordered.jsp"%>
	<%@include file="../../common/inc_email_signup.jsp"%>
	<!-- END BODY -->
	<!--footer-->
	<%@include file="../../common/inc_footer.jsp"%>
	<!--end footer-->
</dsp:page>