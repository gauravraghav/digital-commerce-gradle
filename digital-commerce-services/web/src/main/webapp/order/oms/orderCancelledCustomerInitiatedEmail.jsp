<!DOCTYPE html>
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>
<%@ taglib prefix="dspel" uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0" %>
<dsp:page>

	<dsp:importbean bean="/com/dsw/commerce/services/MessageLocator" />
	<dsp:getvalueof var="messages" bean="MessageLocator.msgKeyToStringMap" scope="page" />
	<dsp:getvalueof var="orderTotal" param="orderTotal" vartype="java.lang.String" />
	<dsp:getvalueof var="locale" param="locale" vartype="java.lang.String" />
	<dsp:getvalueof var="custFirstName" param="payLoadItems.customerFirstName" vartype="java.lang.String" />
	<dsp:getvalueof var="trackingDate" param="trackingDate" vartype="java.lang.String" />
	<dsp:getvalueof var="pickUpByDate" param="payLoadItems.pickUpByDate" vartype="java.lang.String" />

	<fmt:setLocale value="${locale}" />

	<!--start header-->
	<dsp:getvalueof var="previewText" value="Your order has been canceled per your request." vartype="java.lang.String" />
	<%@include file="../../common/inc_header.jsp"%>
	<!--end header-->

	<!-- START BODY -->
	<tr style="border-bottom: 1px solid #cccccc;">
		<td width="100%" style="padding: 25px 25px 25px 25px; border: 0">
			<font face="'Roboto', Arial, sans-serif">
				<h1>Hi ${custFirstName}! Your order has been canceled.</h1>

				<div>As you have requested, we have canceled this order.</div>

				<div style="margin-top:10px;">You will not be charged for the canceled items.</div>
			</font>

			<dsp:getvalueof var="isCanceledOrder" value="1" />
			<%@include file="../../common/inc_order_header.jsp"%>
		</td>
	</tr>


	<tr>
		<td style="padding: 25px; background-color: #ebebeb; border-top: 1px solid #cccccc;">
			<font face="'Roboto', Arial, sans-serif">
				<h2>Item(s) Canceled:</h2>
			</font>

			<dsp:getvalueof id="payload" param="payLoadItems">
				<dsp:droplet name="/atg/dynamo/droplet/ForEach">
					<dsp:param name="array" param="payLoadItems.orderLines" />
					<dsp:setvalue param="currentLine" paramvalue="element" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="fulfillmentType" param="currentLine.fulfillmentType" />
						<dsp:getvalueof var="shippingMethod" param="currentLine.shipCode" />

						<c:if test="${empty shippingMethod}">
							<c:choose>
								<c:when test="${fulfillmentType eq 'BOSTS' || fulfillmentType eq 'BOPIS' }">
									<c:set var="shippingMethod" value="STS" />
									<c:set var="shippingMethodMessageKey" value="checkout.delivery.${shippingMethod}" />
								</c:when>
								<c:otherwise>
									<c:set var="shippingMethodMessageKey" scope="page" value="checkout.delivery.${shippingMethod}" />
								</c:otherwise>
							</c:choose>
						</c:if>
						<c:set var="defaultShippingMethodKey" scope="page" value="checkout.delivery.${shippingMethod}" />

						<c:choose>
							<c:when test="${not empty messages[shippingMethodMessageKey]}">
								<c:set var="shippingMethodLabel" scope="page" value="${messages[shippingMethodMessageKey]}" />
							</c:when>
							<c:otherwise>
								<c:set var="shippingMethodLabel" scope="page" value="${shippingMethod}" />
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${not empty messages[defaultShippingMethodKey]}">
								<c:set var="defaultShippingMethodLabel" scope="page" value="${messages[defaultShippingMethodKey]}" />
							</c:when>
							<c:otherwise>
								<c:set var="defaultShippingMethodLabel" scope="page" value="${shippingMethod}" />
							</c:otherwise>
						</c:choose>
						<c:set var="customerFirstName" value="${custFirstName}" />

						<c:if test="${empty customerFirstName}">
							<c:set var="customerFirstName" value="DSW Shoe Lover" />
						</c:if>
						<dsp:getvalueof var="address1" param="currentLine.person.addressLine1" />
						<dsp:getvalueof var="address2" param="currentLine.person.addressLine2" />
						<dsp:getvalueof var="city" param="currentLine.person.city" />
						<dsp:getvalueof var="state" param="currentLine.person.state" />
						<dsp:getvalueof var="postalCode" param="currentLine.person.zipCode" />
						<dsp:getvalueof var="mallPlazaName" param="payload.mallPlazaName" />

						<c:if test="${empty address1}">
							<dsp:getvalueof var="address1" param="payLoadItems.shippingAddress.addressLine1" />
							<dsp:getvalueof var="address2" param="payLoadItems.shippingAddress.addressLine2" />
							<dsp:getvalueof var="city" param="payLoadItems.shippingAddress.city" />
							<dsp:getvalueof var="state" param="payLoadItems.shippingAddress.state" />
							<dsp:getvalueof var="postalCode" param="payLoadItems.shippingAddress.zipCode" />
							<dsp:getvalueof var="mallPlazaName" param="payload.shippingAddress.mallPlazaName" />
						</c:if>

						<dsp:getvalueof var="productId" param="currentLine.itemStyle" />
						<dsp:getvalueof var="itemId" param="currentLine.itemId" />
						<dsp:getvalueof var="showProductTitle" param="currentLine.productTitle" />
						<c:if test="${empty showProductTitle}">
							<dsp:getvalueof var="showProductTitle" param="currentLine.itemName" />
						</c:if>
						<c:if test="${empty showProductTitle}">
							<dsp:getvalueof var="showProductTitle" param="currentLine.itemDesc" />
						</c:if>
						<dsp:getvalueof var="showBrandName" param="currentLine.itemBrand" />
						<dsp:getvalueof var="showSize" param="currentLine.itemSize" />
						<dsp:getvalueof var="showWidth" param="currentLine.itemWidth" />
						<dsp:getvalueof var="itemQty" param="currentLine.itemStatusQuantity" />
						<dsp:getvalueof var="showColor" param="currentLine.itemColor" />
						<dsp:getvalueof var="showUPC" param="currentLine.itemUPC" />
						<dsp:getvalueof var="colorCode" param="currentLine.colorCode" />
						<c:if test="${empty colorCode}">
							<c:set var="colorCode" value="001" />
						</c:if>
						<dsp:getvalueof var="unitPrice" param="currentLine.itemUnitPrice" />
						
						<dsp:getvalueof var="redOutline" value="1" />
						<%@include file="../../order/oms/common/inc_commerce_item.jsp"%>
						
					</dsp:oparam>
				</dsp:droplet>
			</dsp:getvalueof>
		</td>
	</tr>

	<%--<%@include file="../../order/oms/common/inc_other_items_ordered.jsp"%>--%>

	<!--order-details-zone-->

	<%--<%@include file="common/inc_confirmation_order_details.jsp" %>--%>

	<%@include file="../../common/inc_email_signup.jsp"%>

	<%@include file="../../common/inc_footer.jsp"%>
</dsp:page>