<!DOCTYPE html>
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>
<%@ taglib prefix="dspel" uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0" %>
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="imagesUrl" bean="DSWConstants.imagesUrl" scope="page" />
<dsp:page>
	<dsp:getvalueof var="storePagesUrl" bean="DSWConstants.storePagesUrl" scope="page" />
	<dsp:importbean bean="/com/dsw/commerce/services/MessageLocator" />
	<dsp:getvalueof var="messages" bean="MessageLocator.msgKeyToStringMap" scope="page" />
	<dsp:getvalueof var="orderTotal" param="orderTotal" vartype="java.lang.String" />
	<dsp:getvalueof var="locale" param="locale" vartype="java.lang.String" />
	<dsp:getvalueof var="custFirstName" param="payLoadItems.customerFirstName" vartype="java.lang.String" />
	<dsp:getvalueof var="altPickupFirstName" param="payLoadItems.altPickupFirstName" vartype="java.lang.String" />
	<dsp:getvalueof var="trackingDate" param="trackingDate" vartype="java.lang.String" />
	<c:if test="${not empty trackingDate}">
		<c:set var="analyticsInfo" value="${trackingDate}_order" />
	</c:if>
	<dsp:getvalueof var="pickUpByDate" param="payLoadItems.pickUpByDate" vartype="java.lang.String" />

	<fmt:setLocale value="${locale}" />

	<!--start header-->
	<c:choose>
		<c:when test="${isAlternatePickUpEmail}">
			<dsp:getvalueof var="previewText" value="This order must be picked up by ${pickUpByDate}." vartype="java.lang.String" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="previewText" value="Your order must be picked up by ${pickUpByDate}." vartype="java.lang.String" />
		</c:otherwise>
	</c:choose>
	<%@include file="../../common/inc_header.jsp"%>
	<!--end header-->

	<!-- START BODY -->

	<tr style="border-bottom: 1px solid #cccccc">
		<td width="100%" style="padding: 25px 25px 25px 25px; border: 0">
			<font face="'Roboto', Arial, sans-serif">
				<c:choose>
					<c:when test="${isAlternatePickUpEmail}">
						<h1 style="margin-bottom: 15px;">Hi ${altPickupFirstName}!</h1>
						<div style="color: #d50642;">Don't forget to pick up ${custFirstName}'s order. Item(s) must be picked up by ${pickUpByDate}.</div>
						<div style="color: #d50642; margin-top:10px;">Please bring the order # and a government issued photo ID.</div>
					</c:when>

					<c:otherwise>
						<h1>Hi ${custFirstName}! Don't forget to pick up your order.</h1>
						<div style="color: #d50642; margin-top:10px;"> Item(s) must be picked up by ${pickUpByDate}.</div>
						<div style="color: #d50642; margin-top:10px;">Please bring your order # and a government issued photo ID.</div>
					</c:otherwise>
				</c:choose>
			</font>

			<%@include file="../../common/inc_order_header.jsp"%>
		</td>
	</tr>

	<tr>
		<td style="padding: 25px; background-color: #ebebeb; border-top: 1px solid #cccccc;">
			<font face="'Roboto', Arial, sans-serif">
				<h2>Item(s) to be Picked Up:</h2>
			</font>
			<dsp:getvalueof id="payload" param="payLoadItems">
			    <dsp:droplet name="/atg/dynamo/droplet/ForEach">
					<dsp:param name="array" param="payLoadItems.orderLines" />
					<dsp:setvalue param="currentLine" paramvalue="element" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="fulfillmentType" param="currentLine.fulfillmentType" />

						<c:if test="${fulfillmentType eq 'BOSTS' || fulfillmentType eq 'BOPIS'}">
							<c:set var="shippingMethod" value="STS" />
							<c:set var="shippingMethodMessageKey" value="checkout.delivery.${shippingMethod}" />
							<c:set var="defaultShippingMethodKey" scope="page" value="checkout.delivery.STS" />
							<c:choose>
								<c:when test="${not empty messages[shippingMethodMessageKey]}">
									<c:set var="shippingMethodLabel" scope="page" value="${messages[shippingMethodMessageKey]}" />
								</c:when>
								<c:otherwise>
									<c:set var="shippingMethodLabel" scope="page" value="${shippingMethod}" />
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${not empty messages[defaultShippingMethodKey]}">
									<c:set var="defaultShippingMethodLabel" scope="page" value="${messages[defaultShippingMethodKey]}" />
								</c:when>
								<c:otherwise>
									<c:set var="defaultShippingMethodLabel" scope="page" value="${shippingMethod}" />
								</c:otherwise>
							</c:choose>
							<c:set var="customerFirstName" value="${payLoadItems.customerFirstName}" />

							<c:if test="${empty customerFirstName}">
								<c:set var="customerFirstName" value="DSW Shoe Lover" />
							</c:if>
							<dsp:getvalueof var="address1" param="currentLine.person.addressLine1" />
							<dsp:getvalueof var="address2" param="currentLine.person.addressLine2" />
							<dsp:getvalueof var="city" param="currentLine.person.city" />
							<dsp:getvalueof var="state" param="currentLine.person.state" />
							<dsp:getvalueof var="postalCode" param="currentLine.person.zipCode" />
							<dsp:getvalueof var="mallPlazaName" param="payLoadItems.mallPlazaName" />

							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td colspan="2" style="padding-top: 15px;">
										<font face="'Roboto', Arial, sans-serif">
											<h3>
												<c:choose>
													<c:when test="${fulfillmentType eq 'BOPIS' or fulfillmentType eq 'BOSTS'}">
														${shippingMethodLabel}
													</c:when>
													<c:otherwise>${defaultShippingMethodLabel}</c:otherwise>
												</c:choose>
												<span>${mallPlazaName} today</span>
											</h3>
										</font>
									</td>
								</tr>
								<tr>
									<td valign="middle" width="41" style="padding-bottom: 10px;">
										<c:choose>
											<c:when test="${fulfillmentType eq 'BOSTS'}">
												<img src="${imagesUrl}/is/image/DSWShoes/store?fmt=png-alpha" alt="Store">
											</c:when>
											<c:otherwise>
												<img src="${imagesUrl}/is/image/DSWShoes/time?fmt=png-alpha" alt="Store Time">
											</c:otherwise>
										</c:choose>
									</td>
									<td valign="middle" style="padding-bottom: 10px; padding-left: 15px;">
										<font face="'Roboto', Arial, sans-serif">
											<div>
												<a href="${storePagesUrl}/usa/${fn:toLowerCase(state)}/${fn:toLowerCase(fn:replace(city, " ", ""))}/dsw-designer-shoe-warehouse-${fn:toLowerCase(fn:replace(mallPlazaName, " ", "-"))}.html">
											${address1},
												<c:if test="${not empty address2}">${address2},</c:if>
                                                ${city}, ${state}&nbsp;${postalCode}
												</a>
											</div>

											<c:if test="${not empty altPickupFirstName}">
												<div>
													<i>Designated pick up person: <dsp:valueof param="payLoadItems.altPickupFirstName" />&nbsp;<dsp:valueof param="payLoadItems.altPickupLastName" /></i>
												</div>
											</c:if>
										</font>
									</td>
								</tr>
							</table>

							<dsp:getvalueof var="productId" param="currentLine.itemStyle" />
							<dsp:getvalueof var="itemId" param="currentLine.itemId" />
							<dsp:getvalueof var="showProductTitle" param="currentLine.productTitle" />
							<c:if test="${empty showProductTitle}">
								<dsp:getvalueof var="showProductTitle" param="currentLine.itemName" />
							</c:if>
							<c:if test="${empty showProductTitle}">
								<dsp:getvalueof var="showProductTitle" param="currentLine.itemDesc" />
							</c:if>
							<dsp:getvalueof var="showBrandName" param="currentLine.itemBrand" />
							<dsp:getvalueof var="showSize" param="currentLine.itemSize" />
							<dsp:getvalueof var="showWidth" param="currentLine.itemWidth" />
							<dsp:getvalueof var="showColor" param="currentLine.itemColor" />
							<dsp:getvalueof var="showUPC" param="currentLine.itemUPC" />
							<dsp:getvalueof var="colorCode" param="currentLine.colorCode" />
							<c:if test="${empty colorCode}">
								<c:set var="colorCode" value="001" />
							</c:if>
							<dsp:getvalueof var="unitPrice" param="currentLine.itemUnitPrice" />
							<dsp:getvalueof var="itemPromoList" param="currentLine.itemPromoList" />
							<dsp:getvalueof var="itemQty" param="currentLine.itemStatusQuantity" />
							<dsp:getvalueof var="itemStatus" param="currentLine.status" />
							<dsp:getvalueof var="itemStatusUpdateDate" param="currentLine.statusUpdateDate" />

							<dsp:getvalueof var="redOutline" value="0" />
							<%@include file="common/inc_commerce_item.jsp"%>
						</c:if>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:getvalueof>
		</td>
	</tr>

	<%@include file="common/inc_other_items_ordered.jsp"%>


	<%@include file="../../common/inc_email_signup.jsp"%>

	<!-- END BODY -->

	<!--footer-->
	<%@include file="../../common/inc_footer.jsp"%>
	<!--end footer-->
</dsp:page>