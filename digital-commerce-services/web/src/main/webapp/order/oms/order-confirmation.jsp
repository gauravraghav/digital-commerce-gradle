<!DOCTYPE html>
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
		   prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>
<%@ taglib prefix="dspel" uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0" %>
<dsp:page>

	<dsp:importbean bean="/com/dsw/commerce/services/MessageLocator" />
	<dsp:getvalueof var="messages" bean="MessageLocator.msgKeyToStringMap" scope="page" />
	<dsp:getvalueof var="orderTotal" param="orderTotal" vartype="java.lang.String" />
	<dsp:getvalueof var="locale" param="locale" vartype="java.lang.String" />
	<dsp:getvalueof var="custFirstName" param="payLoadItems.customerFirstName" vartype="java.lang.String" />
	<dsp:getvalueof var="trackingDate" param="trackingDate" vartype="java.lang.String" />
	<c:if test="${not empty trackingDate}">
		<c:set var="analyticsInfo" value="${trackingDate}_order" />
	</c:if>
	<dsp:getvalueof var="shippingCharge" param="shipping" />
	<dsp:getvalueof var="shippingMethod" param="shippingMethod" />
	
	<c:set var="showMultiShipmentMsg" value="false" />
	<dsp:getvalueof var="totalNonEGCCommerceCount" param="totalNonEGCCommerceCount" />
	<c:if test="${totalNonEGCCommerceCount gt 1.0}">
		<c:set var="showMultiShipmentMsg" value="true" />
	</c:if>
	
	<fmt:setLocale value="${locale}" />
	<!--start header-->
	<dsp:getvalueof var="previewText" value="Your order has been received." vartype="java.lang.String" />
	<%@include file="../../common/inc_header.jsp" %>
	<!--end header-->

	<!-- START BODY -->
	<tr style="border-bottom: 1px solid #cccccc;">
		<td width="100%" style="padding: 25px 25px 25px 25px; border: 0">
			<font face="'Roboto', Arial, sans-serif">
				<h1>Hi ${custFirstName}!</h1>

                <div>Thank you for your order. We'll let you know when it's ready for pick up or when it's on the way.</div>
			</font>

			<%@include file="../../common/inc_order_header.jsp"%>
		</td>
	</tr>
	<tr>
		<td style="padding: 25px; background-color: #ebebeb; border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;">
			<font face="'Roboto', Arial, sans-serif">
				<h2 style="border-bottom: 1px solid #cccccc;">Item(s) Ordered:</h2>
				<dsp:getvalueof var="itemsByFullfilmentType" param="fullfilmentItems.SHIP">
					<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
						<dsp:param name="value" param="fullfilmentItems.SHIP" />
						<dsp:oparam name="false">
							<%@include file="common/inc_confirmation_order_ship_item.jsp" %>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:getvalueof>
			</font>
		</td>
	</tr>

	<%@include file="common/inc_confirmation_order_details.jsp" %>

	<c:if test="${orderTotal gt 0.0}">
	<tr>
		<td style="padding: 15px 25px; border-bottom: 1px solid #cccccc;">
				<dsp:getvalueof var="paymentMethods" param="payLoadItems.paymentMethods" />
				<c:if test="${not empty paymentMethods }">
					<font face="'Roboto', Arial, sans-serif">
						<h3>Payment Method(s):</h3>
					</font>

					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<c:forEach var="currentPM"
								   items="${paymentMethods}">
							<c:set var="paymentTypeGroup"
								   value="${currentPM.paymentTypeGroup}" />
							<c:set var="totalAuthorized"
								   value="${currentPM.totalAuthorized}" />
							<c:if test="${paymentTypeGroup == 'CREDIT_CARD'}">
								<tr>
									<td style="text-align: left;">
										<font face="'Roboto', Arial, sans-serif">
										<span>${currentPM.creditCardType} ending *${currentPM.displayCreditCardNo}</span>
										</font>
									</td>
									<td style="text-align: right;">
										<font face="'Roboto', Arial, sans-serif">
											<fmt:formatNumber value="${totalAuthorized}" type="currency" />
										</font>
									</td>
								</tr>
							</c:if>
							<c:if test="${paymentTypeGroup == 'STORED_VALUE_CARD'}">
								<tr>
									<td style="text-align: left;">
										<font face="'Roboto', Arial, sans-serif">
											Gift Card ending *${currentPM.displaySvcNo}
										</font>
									</td>
									<td style="text-align: right;">
										<font face="'Roboto', Arial, sans-serif">
											<fmt:formatNumber value="${totalAuthorized}" type="currency" />
										</font>
									</td>
								</tr>
							</c:if>
						</c:forEach>
					</table>
				</c:if>
		</td>
	</tr>
	</c:if>

	<%@include file="../../common/inc_email_signup.jsp" %>

	<!-- END BODY -->

	<!--footer-->	
	<%@include file="../../common/inc_footer.jsp" %>
	<!--end footer-->

</dsp:page>