<!DOCTYPE html>
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
	prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>
<%@ taglib prefix="dspel"
	uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0"%>
	<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="imagesUrl" bean="DSWConstants.imagesUrl" scope="page" />
<dsp:page>

	<dsp:importbean bean="/com/dsw/commerce/services/MessageLocator" />
	<dsp:getvalueof var="messages" bean="MessageLocator.msgKeyToStringMap"
		scope="page" />
	<dsp:getvalueof var="orderTotal" param="orderTotal"
		vartype="java.lang.String" />
	<dsp:getvalueof var="locale" param="locale" vartype="java.lang.String" />
	<dsp:getvalueof var="custFirstName"
		param="payLoadItems.customerFirstName" vartype="java.lang.String" />
	<c:if test="${empty custFirstName}">
		<c:set var="custFirstName" value="DSW Shoe Lover" />
	</c:if>
	<dsp:getvalueof var="trackingDate" param="trackingDate"
		vartype="java.lang.String" />
	<c:if test="${not empty trackingDate}">
		<c:set var="analyticsInfo" value="${trackingDate}_order" />
	</c:if>
	<dsp:getvalueof var="shippingCharge" param="shipping" />
	<dsp:getvalueof var="shippingMethod" param="shippingMethod" />
	<dsp:getvalueof var="totalNonEGCCommerceCount"
		param="totalNonEGCCommerceCount" />
	<c:if test="${totalNonEGCCommerceCount gt 1.0}">
		<c:set var="showMultiShipmentMsg" value="true" />
	</c:if>
	<fmt:setLocale value="${locale}" />

	<!--start header-->
	<dsp:getvalueof var="previewText"
		value="Your order has shipped. Track your shipment."
		vartype="java.lang.String" />
	<%@include file="../../common/inc_header.jsp"%>
	<!--end header-->

	<!-- START BODY -->
	<c:set var="shippingMethodMessageKey"
		value="checkout.shipping.${shippingMethod}.delivery.label" />
	<c:set var="defaultShippingMethodKey" scope="page"
		value="checkout.shipping.GRN.delivery.label" />

	<c:choose>
		<c:when test="${not empty messages[shippingMethodMessageKey]}">
			<c:set var="shippingMethodLabel" scope="page"
				value="${messages[shippingMethodMessageKey]}" />
		</c:when>
		<c:when test="${not empty messages[defaultShippingMethodKey]}">
			<c:set var="defaultShippingMethodLabel" scope="page"
				value="${messages[defaultShippingMethodKey]}" />
		</c:when>
		<c:otherwise>
			<c:set var="defaultShippingMethodLabel" scope="page"
				value="${shippingMethod}" />
		</c:otherwise>
	</c:choose>

	<tr style="border-bottom: 1px solid #cccccc;">
		<td width="100%" style="padding: 25px 25px 25px 25px; border: 0">
			<font face="'Roboto', Arial, sans-serif">
				<h1>Hi ${custFirstName}! Your order has shipped.</h1>
				<div>Thank you for ordering from DSW. Your item(s) will be
					arriving soon.</div>
		</font> <%@include file="../../common/inc_order_header.jsp"%>
		</td>
	</tr>
	<tr>
		<td
			style="padding: 25px; background-color: #ebebeb; border-top: 1px solid #cccccc;">
			<font face="'Roboto', Arial, sans-serif">
				<h2>Item(s) Shipped:</h2>
		</font> <dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
				<dsp:param name="value" param="payLoadItems.orderLines" />
				<dsp:oparam name="false">

					<dsp:getvalueof var="customerFirstName"
						param="payLoadItems.shippingAddress.firstName" />

					<c:if test="${empty customerFirstName}">
						<c:set var="customerFirstName" value="${custFirstName}" />
					</c:if>
					<dsp:droplet name="/atg/dynamo/droplet/ForEach">
						<dsp:param name="array" param="payLoadItems.orderLines" />
						<dsp:setvalue param="currentLine" paramvalue="element" />
						<dsp:oparam name="output">

							<dsp:getvalueof var="shipType" param="currentLine.itemShipType" />
							<dsp:getvalueof var="productId" param="currentLine.productId" />
							<c:if test="${empty productId}">
								<dsp:getvalueof var="productId" param="currentLine.itemStyle" />
							</c:if>
							<dsp:getvalueof var="showBrandName" param="currentLine.itemBrand" />
							<dsp:getvalueof var="showSize" param="currentLine.itemSize" />
							<dsp:getvalueof var="showWidth" param="currentLine.itemWidth" />
							<dsp:getvalueof var="showUPC" param="currentLine.itemUPC" />
							<dsp:getvalueof var="colorCode" param="currentLine.colorCode" />
							<dsp:getvalueof var="unitPrice" param="currentLine.itemUnitPrice" />
							<dsp:getvalueof var="showColor" param="currentLine.itemColor" />
							<dsp:getvalueof var="showTrackingUrl"
								param="currentLine.trackingNumber" />
							<dsp:getvalueof var="narwarUrl" param="currentLine.narwarUrl" />
							<dsp:getvalueof var="itemPromoList"
								param="currentLine.itemPromoList" />
							<dsp:getvalueof var="itemQty"
								param="currentLine.itemStatusQuantity" />
							<dsp:getvalueof var="redOutline" value="0" />

							<dsp:getvalueof var="address1"
								param="currentLine.person.addressLine1" />
							<dsp:getvalueof var="address2"
								param="currentLine.person.addressLine2" />
							<dsp:getvalueof var="city"
								param="currentLine.person.city" />
							<dsp:getvalueof var="state" param="currentLine.person.state" />
							<dsp:getvalueof var="postalCode"
								param="currentLine.person.zipCode" />							
							<c:set var="recipientName" value="" />
							
							<c:choose>
								<c:when test="${productId eq 'STANDARD-GC'}">								
									<c:set var="shippingMethodMessageKey"
										value="checkout.delivery.nonEGiftCardSHP" />
									<dsp:getvalueof var="recipientName"
										param="currentLine.recipientName" />
									<c:set var="showProductTitle"
										value="GIFT CARD" />
								</c:when>
								<c:when test="${productId eq 'PERSONALIZED-GC'}">								
									<c:set var="shippingMethodMessageKey"
										value="checkout.delivery.nonEGiftCardSHP" />
									<dsp:getvalueof var="recipientName"
										param="currentLine.recipientName" />
									<c:set var="showProductTitle"
										value="PERSONALIZED GIFT CARD" />
								</c:when>
								<c:otherwise>
									<c:set var="shippingMethodMessageKey"
										value="checkout.delivery.${shippingMethod}" />
									<dsp:getvalueof var="showProductTitle"
										param="currentLine.productTitle" />
									<c:if test="${empty showProductTitle}">
										<dsp:getvalueof var="showProductTitle"
											param="currentLine.itemName" />
									</c:if>
									<c:if test="${empty showProductTitle}">
										<dsp:getvalueof var="showProductTitle"
											param="currentLine.itemDesc" />
									</c:if>
								</c:otherwise>
							</c:choose>

							<c:set var="defaultShippingMethodKey" scope="page"
								value="checkout.delivery.GRN" />
							<c:choose>
								<c:when test="${not empty messages[shippingMethodMessageKey]}">
									<c:set var="shippingMethodLabel" scope="page"
										value="${messages[shippingMethodMessageKey]}" />
								</c:when>
								<c:when test="${not empty messages[defaultShippingMethodKey]}">
									<c:set var="defaultShippingMethodLabel" scope="page"
										value="${messages[defaultShippingMethodKey]}" />
								</c:when>
								<c:otherwise>
									<c:set var="defaultShippingMethodLabel" scope="page"
										value="${shippingMethod}" />
								</c:otherwise>
							</c:choose>

							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td colspan="2" style="padding-top: 15px;"><font
										face="'Roboto', Arial, sans-serif">
											<h3>
												<c:choose>
													<c:when test="${empty shippingMethodLabel}">
														${defaultShippingMethodLabel}
													</c:when>
													<c:otherwise>
														${shippingMethodLabel}
													</c:otherwise>
												</c:choose>
												
												<c:choose>
													<c:when
														test="${productId eq 'STANDARD-GC' or productId eq 'PERSONALIZED-GC'}">
													</c:when>
													<c:otherwise>
														${customerFirstName} at:
													</c:otherwise>
												</c:choose>
											</h3>
									</font></td>
								</tr>

								<tr>
									<td valign="middle" width="41" style="padding-bottom: 10px;">
										<img
										src="${imagesUrl}/is/image/DSWShoes/shipping-truck?fmt=png-alpha"
										alt="shipping truck">
									</td>

									<td valign="middle"
										style="padding-bottom: 10px; padding-left: 15px;"><font
										face="'Roboto', Arial, sans-serif">
											<div>${address1},
												<c:if test="${not empty address2}">
													${address2},
												</c:if>
												${city}, ${state}&nbsp;${postalCode}
											</div> <%-- <c:if test="${showMultiShipmentMsg}">
												<div>
													<i>Items may arrive in multiple shipments</i>
												</div>
											</c:if> --%>

									</font></td>
								</tr>

							</table>
							<%@include file="common/inc_commerce_item.jsp"%>

						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</td>
	</tr>
	<%@include file="common/inc_other_items_ordered.jsp"%>


	<%@include file="../../common/inc_email_signup.jsp"%>

	<!-- END BODY -->

	<!--footer-->
	<%@include file="../../common/inc_footer.jsp"%>
	<!--end footer-->
</dsp:page>