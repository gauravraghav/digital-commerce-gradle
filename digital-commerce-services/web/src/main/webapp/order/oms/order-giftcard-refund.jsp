<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<dsp:page>
    <dsp:importbean bean="/com/dsw/commerce/services/MessageLocator" />
    <dsp:getvalueof var="messages" bean="MessageLocator.msgKeyToStringMap" scope="page" />
    <dsp:getvalueof var="orderTotal" param="orderTotal" vartype="java.lang.String" />
    <dsp:getvalueof var="locale" param="locale" vartype="java.lang.String" />
    <dsp:getvalueof var="custFirstName" param="payLoadItems.customerFirstName" vartype="java.lang.String" />
    <c:if test="${empty custFirstName}">
        <c:set var="custFirstName" value="DSW Shoe Lover" />
    </c:if>
    <dsp:getvalueof var="gcRefundAmount" param="returnRefundAmount"/>
    <dsp:getvalueof var="ccRefundAmount" param="ccRefundAmount"/>
    <dsp:getvalueof var="payPalRefundAmount" param="payPalRefundAmount"/>
    <dsp:getvalueof var="refundAmountTotal" param="refundAmountTotal"/>
    <dsp:getvalueof var="trackingDate" param="trackingDate" vartype="java.lang.String" />
    <c:if test="${not empty trackingDate}">
        <c:set var="analyticsInfo" value="${trackingDate}_order" />
    </c:if>
    <fmt:setLocale value="${locale}" />
    <!--start header-->
    <dsp:getvalueof var="previewText"
        value="Your gift card for returned items was shipped and should be arriving soon."
        vartype="java.lang.String" />
        <%@include file="../../common/inc_header.jsp"%>
        <!--end header-->
<tr style="border-bottom: 1px solid #cccccc;">
	<td width="100%" style="padding: 25px 25px 25px 25px; border: 0">
		<font face="'Roboto', Arial, sans-serif">
			<h1>Hi ${custFirstName}! Your gift card is on its way.</h1>

			<p>Your return gift card for this order was shipped and should be arriving soon.<br/>
                You will be refunded for any other payment method used on this order in 7-10 business days if applicable.</p>

			<c:if test="${gcRefundAmount gt 0.0}">
				<div style="color: #d50642; margin-top: 10px;">
					<strong>Refund Gift Card Amount: <fmt:formatNumber value="${gcRefundAmount}" type="currency" /></strong>
				</div>
			</c:if>
			<%@include file="../../common/inc_order_header.jsp"%>
	</font>
	</td>
</tr>

	<!-- START BODY -->

<%@include file="common/inc_other_items_ordered.jsp"%>
<%@include file="../../common/inc_email_signup.jsp"%>
<!-- END BODY -->
<!--footer-->
<%@include file="../../common/inc_footer.jsp"%>
<!--end footer-->
</dsp:page>