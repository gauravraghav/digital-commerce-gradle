<!DOCTYPE html>
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
		   prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>
<%@ taglib prefix="dspel" uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0" %>
<dsp:page>

	<dsp:importbean bean="/com/dsw/commerce/services/MessageLocator" />
	<dsp:getvalueof var="messages" bean="MessageLocator.msgKeyToStringMap" scope="page" />
	<dsp:getvalueof var="orderTotal" param="orderTotal" vartype="java.lang.String" />
	<dsp:getvalueof var="locale" param="locale" vartype="java.lang.String" />
	<dsp:getvalueof var="custFirstName" param="payLoadItems.customerFirstName" vartype="java.lang.String" />
	<dsp:getvalueof var="trackingDate" param="trackingDate" vartype="java.lang.String" />
	<c:if test="${not empty trackingDate}">
		<c:set var="analyticsInfo" value="${trackingDate}_order" />
	</c:if>
	<dsp:getvalueof var="shippingCharge" param="shipping" />
	<dsp:getvalueof var="shippingMethod" param="shippingMethod" />
	
	<c:set var="showMultiShipmentMsg" value="false" />
	<dsp:getvalueof var="totalNonEGCCommerceCount" param="totalNonEGCCommerceCount" />
	<c:if test="${totalNonEGCCommerceCount gt 1.0}">
		<c:set var="showMultiShipmentMsg" value="true" />
	</c:if>
	
	<fmt:setLocale value="${locale}" />
	<!--start header-->
	<dsp:getvalueof var="previewText" value="Hang tight! Your order is being processed." vartype="java.lang.String" />
	<%@include file="../../common/inc_header.jsp" %>
	<!--end header-->

	<!-- START BODY -->
	<tr style="border-bottom: 1px solid #cccccc;">
		<td width="100%" style="padding: 25px 25px 25px 25px; border: 0">
			<font face="'Roboto', Arial, sans-serif">
				<h1>Hi ${custFirstName}!</h1>

                <div>Hang tight &mdash; we have received and are still processing your recent DSW order. We will notify you as soon as your shipping information is available. Don't worry, your order will still arrive within 4-7 business days from order placement.</div>
			</font>

			<%@include file="../../common/inc_order_header.jsp"%>
		</td>
	</tr>
	<tr>
		<td style="padding: 25px; background-color: #ebebeb; border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;">
			<font face="'Roboto', Arial, sans-serif">
				<h2 style="border-bottom: 1px solid #cccccc;">Item(s) Ordered:</h2>
				<dsp:droplet name="/atg/dynamo/droplet/ForEach">
					<dsp:param name="array" param="fullfilmentItems" />
					<dsp:setvalue param="itemsByFullfilmentType" paramvalue="element" />
					<dsp:oparam name="output">
						<dsp:droplet name="/atg/dynamo/droplet/ForEach">
							<dsp:param name="array" param="itemsByFullfilmentType" />
							<dsp:setvalue param="currentLine" paramvalue="element" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="shipType" param="currentLine.itemShipType" />
								<dsp:getvalueof var="productId" param="currentLine.productId" />
								<dsp:getvalueof var="showProductTitle" param="currentLine.productTitle" />
								<c:if test="${empty showProductTitle}">
									<dsp:getvalueof var="showProductTitle" param="currentLine.itemName" />
								</c:if>
								<c:if test="${empty showProductTitle}">
									<dsp:getvalueof var="showProductTitle" param="currentLine.itemDesc" />
								</c:if>
								<dsp:getvalueof var="showBrandName" param="currentLine.itemBrand" />
								<dsp:getvalueof var="showSize" param="currentLine.itemSize" />
								<dsp:getvalueof var="showWidth" param="currentLine.itemWidth" />
								<dsp:getvalueof var="showUPC" param="currentLine.itemUPC" />
								<dsp:getvalueof var="colorCode" param="currentLine.colorCode" />
								<dsp:getvalueof var="unitPrice" param="currentLine.itemUnitPrice" />
								<dsp:getvalueof var="showColor" param="currentLine.itemColor" />
								<dsp:getvalueof var="itemQty" param="currentLine.itemStatusQuantity" />
								<dsp:getvalueof var="itemPromoList" param="currentLine.itemPromoList" />

								<!-- START OF LINE ITEM -->

								<dsp:getvalueof var="redOutline" value="0" />
								<%@include file="common/inc_commerce_item.jsp"%>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
			</font>
		</td>
	</tr>
	
	<%@include file="../../common/inc_email_signup.jsp" %>

	<!-- END BODY -->

	<!--footer-->	
	<%@include file="../../common/inc_footer.jsp" %>
	<!--end footer-->

</dsp:page>