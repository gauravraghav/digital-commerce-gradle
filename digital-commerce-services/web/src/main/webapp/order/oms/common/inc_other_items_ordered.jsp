<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
    prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.dsw.com/jsp/jstl/functions" prefix="dswfn"%>
<dsp:page>
<dsp:getvalueof var="displayOtherItems" param="displayOtherItems"/>
<dsp:getvalueof var="eventType" param="eventType"/>
<c:if test="${displayOtherItems eq 'true'}">
    <tr>
            <td style="padding:0 20px; background-color: #ebebeb; border-bottom: 1px solid #cccccc;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <c:choose>
                    <c:when test="${eventType eq 'GIFTCARD_SHIPPED'}">
                        <td style="padding: 10px 0;">
                    </c:when>
                    <c:otherwise>
                        <td style="padding-bottom: 10px;">
                    </c:otherwise>
                    </c:choose>
                        <font face="'Roboto', Arial, sans-serif">
                        <c:choose>
                            <c:when test="${eventType eq 'GIFTCARD_SHIPPED'}">
                                <h2>Order Summary:</h2>
                            </c:when>
                             <c:otherwise>
                                 <h2>Other Item(s) Ordered: </h2>
                             </c:otherwise>
                        </c:choose>
                        </font>
                    </td>
                </tr>
            </table>
                <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                <dsp:param name="array" param="orderSummaryBOPISItems" />
                <dsp:setvalue param="currentLine" paramvalue="element" />
                <dsp:getvalueof var="redOutline" value="0" />
                <dsp:oparam name="output">
                    <%@include file="inc_commerce_item_header.jsp"%>
                    <%@include file="inc_commerce_item.jsp"%>
                </dsp:oparam>
            </dsp:droplet>
            <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                <dsp:param name="array" param="orderSummaryBOSTSItems" />
                <dsp:setvalue param="currentLine" paramvalue="element" />
                <dsp:getvalueof var="redOutline" value="0" />
                <dsp:oparam name="output">
                    <%@include file="inc_commerce_item_header.jsp"%>
                    <%@include file="inc_commerce_item.jsp"%>
                </dsp:oparam>
            </dsp:droplet>
            <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                <dsp:param name="array" param="orderSummarySHIPItems.orderLines" />
                <dsp:setvalue param="currentLine" paramvalue="element" />
                <dsp:getvalueof var="redOutline" value="0" />
                <dsp:oparam name="output">
                    <%@include file="inc_commerce_item_header.jsp"%>
                    <%@include file="inc_commerce_item.jsp"%>
                </dsp:oparam>
            </dsp:droplet>
        </td>
    </tr>
 </c:if> 
</dsp:page>