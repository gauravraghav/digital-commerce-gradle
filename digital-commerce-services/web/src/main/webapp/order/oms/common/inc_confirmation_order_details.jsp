<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
           prefix="dsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions" %>

<dsp:page>
	<dsp:getvalueof var="shippingCharge" param="shipping" />
	<dsp:getvalueof var="shippingMethod" param="shippingMethod" />	
	<c:set var="showMultiShipmentMsg" value="false" />
	<dsp:getvalueof var="totalNonEGCCommerceCount" param="totalNonEGCCommerceCount" />
	<c:if test="${totalNonEGCCommerceCount gt 1.0}">
		<c:set var="showMultiShipmentMsg" value="true" />
	</c:if>
	<dsp:getvalueof var="totalCommerceCount" param="totalCommerceCount" />
	<fmt:setLocale value="${locale}" />

    <tr>
        <td width="100%" style="padding: 30px 25px; border-bottom: 2px solid #000000;">
            <font face="'Roboto', Arial, sans-serif">
                <h2>Order Details:</h2>

                <h3 style="margin:15px 0;">ORDER SUMMARY:</h3>
            </font>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" style="text-align: left;">
                        <font face="'Roboto', Arial, sans-serif">
                            <h4>SUBTOTAL: <span style="font-weight: normal;">(${totalCommerceCount} Items)</span></h4>
                        </font>
                    </td>
                    <td valign="top" style="text-align: right;">
                        <font face="'Roboto', Arial, sans-serif">
                            <h4 style="text-align: right;">
						        <dsp:getvalueof var="subTotal" param="subTotal"/>
                                <fmt:formatNumber value="${subTotal}" type="currency"/>
                            </h4>
                        </font>
                    </td>
                </tr>

                <dsp:getvalueof var="offersRewardsAmount" param="offersRewardsAmount"/>
                <c:if test="${offersRewardsAmount gt 0.0}">
                    <tr>
                        <td valign="top" style="padding-left: 10px; text-align: left;">
                            <font face="'Roboto', Arial, sans-serif">
                                <i>Offers</i>
                            </font>
                        </td>
                        <td valign="top" style="text-align: right;">
                            <font face="'Roboto', Arial, sans-serif">
                                <i>
					                <dsp:getvalueof var="offersRewardsAmount" param="offersRewardsAmount"/>
					                -<fmt:formatNumber value="${offersRewardsAmount}" type="currency"/>
				                </i>
                            </font>
                        </td>
                    </tr>
                </c:if>

                <dsp:getvalueof var="certificatesRewardsAmount" param="certificatesRewardsAmount"/>
                <c:if test="${certificatesRewardsAmount gt 0.0}">
                    <tr>
                        <td valign="top" style="padding-left: 10px; text-align: left;">
                            <font face="'Roboto', Arial, sans-serif">
                                <i>Rewards</i>
                            </font>
                        </td>
                        <td valign="top" style="text-align: right;">
                            <font face="'Roboto', Arial, sans-serif">
                                <i>
                                <dsp:getvalueof var="certificatesRewardsAmount" param="certificatesRewardsAmount"/>
                                -<fmt:formatNumber value="${certificatesRewardsAmount}" type="currency"/>
                                </i>
                            </font>
                        </td>
                    </tr>
                </c:if>
                <tr>
                    <td style="padding: 10px 0 0 0; text-align: left;">
                        <font face="'Roboto', Arial, sans-serif">
                            <h4>SHIPPING:</h4>
                        </font>
                    </td>

                    <td style="padding: 10px 0 0 0; text-align: right;">
                        <font face="'Roboto', Arial, sans-serif">
                            <h4 style="text-align: right;">
                                <c:choose>
                                    <c:when test="${shippingCharge gt 0.0}">
                                        <fmt:formatNumber value="${shippingCharge}" type="currency"/>
                                    </c:when>
                                    <c:otherwise>
                                        FREE
                                    </c:otherwise>
                                </c:choose>
                            </h4>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0 0 15px 10px; text-align: left;">
                        <font face="'Roboto', Arial, sans-serif">
                            <i>Taxes</i>
                        </font>
                    </td>
                    <td style="text-align: right;">
                        <font face="'Roboto', Arial, sans-serif">
				            <i>
					            <dsp:getvalueof var="tax" param="tax"/>
					            <fmt:formatNumber value="${tax}" type="currency"/>
				            </i>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:15px; border-top: 1px solid #cccccc; text-align: left;">
                        <font face="'Roboto', Arial, sans-serif">
				            <h3>ORDER TOTAL:</h3>
                        </font>
                    </td>
                    <td style="padding-top:15px; border-top: 1px solid #cccccc; text-align: right;">
                        <font face="'Roboto', Arial, sans-serif">
				            <h3 style="text-align: right;"><fmt:formatNumber value="${orderTotal}" type="currency"/></h3>
                        </font>
                    </td>
                </tr>

                <dsp:getvalueof var="saved" param="saved"/>
                <c:if test="${saved gt 0.0}">
                    <tr>
                        <td style="padding-left: 10px; text-align: left;">
                            <font face="'Roboto', Arial, sans-serif">
                                <i>You Saved:</i>
                            </font>
                        </td>
                        <td style="text-align: right;">
                            <font face="'Roboto', Arial, sans-serif">
					           <i><fmt:formatNumber value="${saved}" type="currency"/></i>
                            </font>
                        </td>
                    </tr>
                </c:if>
            </table>
        </td>
    </tr>
</dsp:page>