<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<dsp:page>
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="imagesUrl" bean="DSWConstants.imagesUrl" scope="page" />
	<dsp:getvalueof var="fulfillmentType" param="currentLine.fulfillmentType" />
	
	<c:choose>
		<c:when test="${fulfillmentType eq 'BOSTS'}">
			<c:set var="shippingMethod" value="STS" />
			<c:set var="shippingMethodMessageKey" value="checkout.delivery.${shippingMethod}" />
			<c:set var="defaultShippingMethodKey" scope="page" value="checkout.delivery.STS" />
		</c:when>
		<c:when test="${fulfillmentType eq 'BOPIS'}">
			<c:set var="shippingMethod" value="ISPU" />
			<c:set var="shippingMethodMessageKey" value="checkout.delivery.${shippingMethod}" />
			<c:set var="defaultShippingMethodKey" scope="page" value="checkout.delivery.ISPU" />
		</c:when>
		<c:otherwise>
			<c:set var="shippingMethod" value="SGRN" />
			<c:set var="shippingMethodMessageKey" value="checkout.delivery.${shippingMethod}" />
			<c:set var="defaultShippingMethodKey" scope="page" value="checkout.delivery.GRN" />
		</c:otherwise>
	</c:choose>


	<c:choose>
		<c:when test="${not empty messages[shippingMethodMessageKey]}">
			<c:set var="shippingMethodLabel" scope="page" value="${messages[shippingMethodMessageKey]}" />
		</c:when>
		<c:otherwise>
			<c:set var="shippingMethodLabel" scope="page" value="${shippingMethod}" />
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${not empty messages[defaultShippingMethodKey]}">
			<c:set var="defaultShippingMethodLabel" scope="page" value="${messages[defaultShippingMethodKey]}" />
		</c:when>
		<c:otherwise>
			<c:set var="defaultShippingMethodLabel" scope="page" value="${shippingMethod}" />
		</c:otherwise>
	</c:choose>

	<dsp:getvalueof var="customerFirstName" param="currentLine.person.firstName" />
	<c:if test="${empty customerFirstName}">
		<c:set var="customerFirstName" value="DSW Shoe Lover" />
	</c:if>

	<dsp:getvalueof var="address1" param="currentLine.person.addressLine1" />
	<dsp:getvalueof var="address2" param="currentLine.person.addressLine2" />
	<dsp:getvalueof var="altPickupFirstName" param="currentLine.person.markForFirstName" />
	<dsp:getvalueof var="altPickupLastName" param="currentLine.person.markForLastName" />
	<dsp:getvalueof var="city" param="currentLine.person.city" />
	<dsp:getvalueof var="state" param="currentLine.person.state" />
	<dsp:getvalueof var="postalCode" param="currentLine.person.zipCode" />
	<dsp:getvalueof var="productId" param="currentLine.itemStyle" />
	<dsp:getvalueof var="itemId" param="currentLine.itemId" />
	<dsp:getvalueof var="showProductTitle" param="currentLine.productTitle" />
	<c:if test="${empty showProductTitle}">
		<dsp:getvalueof var="showProductTitle" param="currentLine.itemName" />
	</c:if>
	<c:if test="${empty showProductTitle}">
		<dsp:getvalueof var="showProductTitle" param="currentLine.itemDesc" />
	</c:if>
	<dsp:getvalueof var="showBrandName" param="currentLine.itemBrand" />
	<dsp:getvalueof var="showSize" param="currentLine.itemSize" />
	<dsp:getvalueof var="showWidth" param="currentLine.itemWidth" />
	<dsp:getvalueof var="showColor" param="currentLine.itemColor" />
	<dsp:getvalueof var="showUPC" param="currentLine.itemUPC" />
	<dsp:getvalueof var="colorCode" param="currentLine.colorCode" />
	<c:if test="${empty colorCode}">
		<c:set var="colorCode" value="001" />
	</c:if>
	<dsp:getvalueof var="unitPrice" param="currentLine.itemUnitPrice" />
	<dsp:getvalueof var="itemPromoList" param="currentLine.itemPromoList" />
	<dsp:getvalueof var="itemQty" param="currentLine.itemStatusQuantity" />
	<dsp:getvalueof var="itemStatus" param="currentLine.status" />
	<dsp:getvalueof var="itemStatusCode" param="currentLine.statusCode" />
	<dsp:getvalueof var="itemStatusUpdateDate"
		param="currentLine.statusUpdateDate" />
	<dsp:getvalueof var="showTrackingUrl" param="currentLine.trackingNumber" />
	<dsp:getvalueof var="narwarUrl" param="currentLine.narwarUrl" />
	<dsp:getvalueof var="recipientName" param="currentLine.recipientName" />
	<dsp:getvalueof var="recipientEmail" param="currentLine.recipientEmail" />
									
	<c:if test="${productId eq 'STANDARD-GC' or productId eq 'e-GC'}">
		<c:set var="showBrandName" value="DSW GIFT CARDS" />
	</c:if>
	
<c:if test="${itemStatusCode ne '9000'}">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		
		<tr>
			<td valign="middle" width="41" style="padding-bottom: 10px;"><c:choose>
					<c:when test="${fulfillmentType eq 'BOPIS'}">
						<img
							src="${imagesUrl}/is/image/DSWShoes/time?fmt=png-alpha"
							alt="Store Time">
							<c:set var="showMultiShipmentMsg" value="false" />
					</c:when>
					<c:when test="${fulfillmentType eq 'BOSTS'}">
						<img
							src="${imagesUrl}/is/image/DSWShoes/store?fmt=png-alpha"
							alt="Store">
							<c:set var="showMultiShipmentMsg" value="false" />
					</c:when>
					<c:when test="${productId eq 'e-GC'}">
						<img src="${imagesUrl}/is/image/DSWShoes/email"
							alt="email envelope">
					</c:when>
					<c:otherwise>
						 <img
							src="${imagesUrl}/is/image/DSWShoes/shipping-truck?fmt=png-alpha"
							alt="shipping truck">
						<c:set var="showMultiShipmentMsg" value="false" />
						<dsp:getvalueof var="totalNonEGCCommerceCount"
							param="totalNonEGCCommerceCount" />
						<c:if
							test="${totalNonEGCCommerceCount gt 1.0 and itemStatus eq 'In Process' and itemId ne 'STANDARD-GC' and itemId ne 'PERSONALIZED-GC'}">
								<c:set var="showMultiShipmentMsg" value="true" />
						</c:if>
					</c:otherwise>
				</c:choose></td>

			<td valign="middle" style="padding: 0 0 10px 15px;">
				<font face="'Roboto', Arial, sans-serif">
					<c:choose>
						<c:when test="${productId eq 'e-GC'}">
							<font face="'Roboto', Arial, sans-serif">
								<div>Electronic Delivery</div>
							</font>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${fulfillmentType eq 'BOSTS' or fulfillmentType eq 'BOPIS'}">
									<div>
										<a href="${storePagesUrl}/usa/${fn:toLowerCase(state)}/${fn:toLowerCase(fn:replace(city, " ", ""))}/dsw-designer-shoe-warehouse-${fn:toLowerCase(fn:replace(mallPlazaName, " ", "-"))}.html">
									  ${address1},
								  <c:if test="${not empty address2}">
									  ${address2},
								  </c:if>
									  ${city}, ${state}&nbsp;${postalCode}</a>
							  </div>
								</c:when>
								<c:otherwise>
									<div>${address1},
										<c:if test="${not empty address2}">
											${address2},
										</c:if>
											${city}, ${state}&nbsp;${postalCode}
									</div>
								</c:otherwise>
							</c:choose>
							<c:if test="${fulfillmentType eq 'BOPIS' or fulfillmentType eq 'BOSTS'}">
								<c:if test="${not empty altPickupFirstName}">
									<div>
										<i>Designated pick up person: ${altPickupFirstName}&nbsp;${altPickupLastName}</i>
									</div>
								</c:if>
							</c:if>
							<c:if test="${showMultiShipmentMsg}">
								<div>
									<i>Items may arrive in multiple shipments</i>
								</div>
							</c:if>
						</c:otherwise>
					</c:choose>
			</font></td>
		</tr>
	</table>
	</c:if>
</dsp:page>