<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.dsw.com/jsp/jstl/functions" prefix="dswfn"%>
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="imagesUrl" bean="DSWConstants.imagesUrl" scope="page" />
<dsp:page>
	<c:if test="${not empty itemsByFullfilmentType}">
		<c:set var="shippingMethodMessageKey" value="checkout.delivery.${shippingMethod}" />
		<c:set var="defaultShippingMethodKey" scope="page" value="checkout.delivery.SGRN" />

		<c:choose>
			<c:when test="${not empty messages[shippingMethodMessageKey]}">
				<c:set var="shippingMethodLabel" scope="page" value="${messages[shippingMethodMessageKey]}" />
			</c:when>
			<c:otherwise>
				<c:set var="shippingMethodLabel" scope="page" value="${shippingMethod}" />
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${not empty messages[defaultShippingMethodKey]}">
				<c:set var="defaultShippingMethodLabel" scope="page" value="${messages[defaultShippingMethodKey]}" />
			</c:when>
			<c:otherwise>
				<c:set var="defaultShippingMethodLabel" scope="page" value="${shippingMethod}" />
			</c:otherwise>
		</c:choose>
		<dsp:getvalueof var="customerFirstName" param="payLoadItems.shippingAddress.firstName" />

		<c:if test="${empty customerFirstName}">
			<c:set var="customerFirstName" value="${custFirstName}" />
		</c:if>

		<dsp:getvalueof var="address1" param="payLoadItems.shippingAddress.addressLine1" />
		<dsp:getvalueof var="address2" param="payLoadItems.shippingAddress.addressLine2" />
		<dsp:getvalueof var="city" param="payLoadItems.shippingAddress.city" />
		<dsp:getvalueof var="state" param="payLoadItems.shippingAddress.state" />
		<c:if test="${empty state}">
			<c:set var="state" value="${state}" />
		</c:if>
		<dsp:getvalueof var="postalCode" param="payLoadItems.shippingAddress.zipCode" />

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2" style="padding-top: 15px;">
					<font face="'Roboto', Arial, sans-serif">
						<h3>
							<c:choose>
								<c:when test="${empty shippingMethodLabel}">
								  ${defaultShippingMethodLabel}
								</c:when>
								<c:otherwise>
									${shippingMethodLabel}
								</c:otherwise>
							</c:choose> ${customerFirstName} at:
						</h3>
					</font>
				</td>
			</tr>
			<tr>
				<td valign="middle" width="41" style="padding-bottom: 10px;">
					<img src="${imagesUrl}/is/image/DSWShoes/shipping-truck?fmt=png-alpha" alt="shipping truck"/>
				</td>
				<td valign="middle" style="padding: 0 0 10px 15px;">
					<font face="'Roboto', Arial, sans-serif">
						<div>${address1},
							<c:if test="${not empty address2}">
								${address2},
							</c:if>
								${city}, ${state}&nbsp;${postalCode}
						</div>
						<c:if test="${showMultiShipmentMsg}">
							<div><i>Items may arrive in multiple shipments</i></div>
						</c:if>
					</font>
				</td>
			</tr>
		</table>

		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" param="fullfilmentItems.SHIP" />
			<dsp:setvalue param="currentLine" paramvalue="element" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="shipType" param="currentLine.itemShipType" />
				<c:if test="${shipType ne 'BOSTS'}">
					<c:if test="${shipType ne 'BOPIS'}">
						<dsp:getvalueof var="productId" param="currentLine.productId" />
						<dsp:getvalueof var="showProductTitle" param="currentLine.productTitle" />
						<c:if test="${empty showProductTitle}">
							<dsp:getvalueof var="showProductTitle" param="currentLine.itemName" />
						</c:if>
						<c:if test="${empty showProductTitle}">
							<dsp:getvalueof var="showProductTitle" param="currentLine.itemDesc" />
						</c:if>
						<dsp:getvalueof var="showBrandName" param="currentLine.itemBrand" />
						<dsp:getvalueof var="showSize" param="currentLine.itemSize" />
						<dsp:getvalueof var="showWidth" param="currentLine.itemWidth" />
						<dsp:getvalueof var="showUPC" param="currentLine.itemUPC" />
						<dsp:getvalueof var="colorCode" param="currentLine.colorCode" />
						<dsp:getvalueof var="unitPrice" param="currentLine.itemUnitPrice" />
						<dsp:getvalueof var="showColor" param="currentLine.itemColor" />
						<dsp:getvalueof var="itemQty" param="currentLine.itemStatusQuantity" />
						<dsp:getvalueof var="itemPromoList" param="currentLine.itemPromoList" />

						<!-- START OF LINE ITEM -->

						<dsp:getvalueof var="redOutline" value="0" />
						<%@include file="inc_commerce_item.jsp"%>
					</c:if>
				</c:if>
			</dsp:oparam>
		</dsp:droplet>
	</c:if>

</dsp:page>