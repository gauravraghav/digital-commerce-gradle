<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
           prefix="dsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.dsw.com/jsp/jstl/functions" prefix="dswfn" %>
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="imagesUrl" bean="DSWConstants.imagesUrl" scope="page" />
<dsp:page>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td style="padding-bottom: 15px;">

    <c:choose>
        <c:when test="${redOutline eq '1'}">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #ffffff; border: 2px solid #d50642;">
        </c:when>
        <c:otherwise>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #ffffff; border: 1px solid #cccccc;">
        </c:otherwise>
    </c:choose>

    <c:set var="firstPromo" value="true"/>
    <c:forEach var="itemPromo" items="${itemPromoList}">
        <c:set var="promoDesc" value="${itemPromo.value}"/>

        <c:if test="${not empty promoDesc}">
            <tr>
                <c:choose>
                    <c:when test="${firstPromo}">
                        <td valign="middle" class="mobile-column-preserve-padding"
                            style="line-height: 100%; padding: 10px 15px; border-top: 4px solid #0d6f96; border-bottom: 1px solid #cccccc;">
                            <font face="'Roboto', Arial, sans-serif">${promoDesc}
                                (applied)</font> <c:set var="firstPromo" value="false"/>
                        </td>
                    </c:when>
                    <c:otherwise>
                        <td valign="middle" class="mobile-column-preserve-padding"
                            style="line-height: 100%; padding: 10px 15px; border-bottom: 1px solid #cccccc;">
                            <font face="'Roboto', Arial, sans-serif">${promoDesc} (applied)</font>
                        </td>
                    </c:otherwise>
                </c:choose>
            </tr>
        </c:if>
    </c:forEach>
    <tr>
        <td style="padding: 25px 15px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="40%" class="mobile-column" valign="top">                        
                        <c:choose>
                            <c:when test="${productId eq 'STANDARD-GC'}">
                                <img width="100%"
                                     src="${imagesUrl}/is/image/DSWShoes/d-s-w-gift-card?fmt=png&wid=253&hei=189"
                                     alt="product image"/>
                            </c:when>
                            <c:when test="${productId eq 'PERSONALIZED-GC'}">
                                <img width="100%"
                                     src="${imagesUrl}/is/image/DSWShoes/PERSONALIZED-GC__ss_01?$search$&wid=253&hei=189"
                                     alt="product image"/>
                            </c:when>
                            <c:when test="${productId eq 'e-GC'}">
                                <img width="100%"
                                     src="${imagesUrl}/is/image/DSWShoes/egiftcard?fmt=png&wid=253&hei=189"
                                     alt="product image"/>
                            </c:when>
                            <c:otherwise>
                            	<a href="<dsp:valueof param="siteUrl"/>product/${dswfn:getSEOProductDisplayName(showBrandName)}-${dswfn:getSEOProductDisplayName(showProductTitle)}/${productId}?activeColor=${colorCode}&cm_mmc=emops-_-${analyticsInfo}">										
                                 <img width="100%"
                                      src="${imagesUrl}/is/image/DSWShoes/${productId}_${colorCode}_ss_01?$search$&wid=253&hei=189"
                                      alt="product image">
                                </a>
                            </c:otherwise>
                        </c:choose>                        
                    </td>
                    <td width="60%" class="mobile-column" valign="top">
                        <font face="'Roboto', Arial, sans-serif">
                            <dsp:getvalueof var="itemStyle" param="currentLine.itemStyle" />
                            <c:if test="${itemStyle ne 'STANDARD-GC' and itemStyle ne 'e-GC' and itemStyle ne 'PERSONALIZED-GC'}">
	                            <div style="text-transform: uppercase;">
	                                <strong>${showBrandName}</strong>
	                            </div>
                            </c:if>
                            <c:choose>
                                <c:when test="${itemStyle ne 'STANDARD-GC' and itemStyle ne 'e-GC' and itemStyle ne 'PERSONALIZED-GC'}">
                            		<div style="text-transform: uppercase;">${showProductTitle}</div>
                                </c:when>
                                <c:otherwise>
                                    <div style="text-transform: uppercase;"><strong>${showProductTitle}</strong></div>
                                </c:otherwise>
                            </c:choose>

                            <div>
                                <strong><fmt:formatNumber value="${unitPrice}" type="currency"/></strong>
                            </div>
                            <br>
                            <c:if test="${not empty showColor}">
                                <div>Color: ${showColor}</div>
                            </c:if> <c:if test="${not empty showSize}">
                            <div>Size: ${showSize}</div>
                        </c:if> <c:if test="${not empty showWidth}">
                            <div>Width: ${showWidth}</div>
                        </c:if>
                            <div>Quantity: ${itemQty}</div>
                            	<c:if test="${not empty recipientName}">
									<div>
										To: ${recipientName}
										<c:if test="${productId eq 'e-GC'}">
											<c:if test="${not empty recipientEmail}">
											 ,${recipientEmail}
											 </c:if>
										</c:if>
									</div>
								</c:if>
							<div>
                                <c:if test="${not empty showUPC}">Item Number: ${showUPC}
                                </c:if>
                            </div>
                            <br> <c:if test="${not empty showTrackingUrl}">
										<span class="mobile-column"
                                              style="border: 1px solid #000000; padding: 15px 30px; line-height: 100%; text-decoration: none; text-align:center;">
                                <a class="secondary-button" href="${narwarUrl}"> <strong valign="top">TRACK THIS ITEM</strong></a>
										</span>
                        </c:if>
                        </font></td>
                </tr>
            </table>
        </td>
    </tr>

    <c:if test="${not empty itemStatus}">
        <tr>
            <c:choose>
                <c:when test="${redOutline eq '1'}">
                     <td valign="middle" class="mobile-column-preserve-padding"
                        style="line-height: 100%; padding: 10px 15px; border-top: 1px solid #cccccc; border-bottom: 2px solid #d50642;">
                </c:when>
                <c:otherwise>
                    <td valign="middle" class="mobile-column-preserve-padding"
                        style="line-height: 100%; padding: 10px 15px; border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;">
                </c:otherwise>
            </c:choose>

                <font face="'Roboto', Arial, sans-serif">
                    <div>
                            ${itemStatus} on ${itemStatusUpdateDate}
                    </div>
                </font>
            </td>
        </tr>
    </c:if>

    </table>
    </td>
    </tr>
    </table>

</dsp:page>



