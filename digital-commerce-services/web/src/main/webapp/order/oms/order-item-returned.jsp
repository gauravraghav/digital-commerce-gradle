<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>
<%@ taglib prefix="dspel" uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0"%>
<dsp:page>
	<dsp:importbean bean="/com/dsw/commerce/services/MessageLocator" />
	<dsp:getvalueof var="messages" bean="MessageLocator.msgKeyToStringMap" scope="page" />
	<dsp:getvalueof var="orderTotal" param="orderTotal" vartype="java.lang.String" />
	<dsp:getvalueof var="locale" param="locale" vartype="java.lang.String" />
	<dsp:getvalueof var="custFirstName"
		param="payLoadItems.customerFirstName" vartype="java.lang.String" />
	<dsp:getvalueof var="altPickupFirstName"
		param="payLoadItems.altPickupFirstName" vartype="java.lang.String" />
	<dsp:getvalueof var="trackingDate" param="trackingDate"
		vartype="java.lang.String" />
		
	<dsp:getvalueof var="gcRefundAmount" param="gcRefundAmount"/>
	<dsp:getvalueof var="ccRefundAmount" param="ccRefundAmount"/>
	<dsp:getvalueof var="payPalRefundAmount" param="payPalRefundAmount"/>
	<dsp:getvalueof var="refundAmountTotal" param="refundAmountTotal"/>
	<dsp:getvalueof var="returnRefundAmount" param="returnRefundAmount"/>
			
	<c:if test="${not empty trackingDate}">
		<c:set var="analyticsInfo" value="${trackingDate}_order" />
	</c:if>
	<dsp:getvalueof var="pickUpByDate" param="payLoadItems.pickUpByDate" vartype="java.lang.String" />

	<fmt:setLocale value="${locale}" />
	<!--start header-->
	<dsp:getvalueof var="previewText" value="Your returned item has been received. Your payment will be refunded in 7-10 business days." vartype="java.lang.String" />
	<%@include file="../../common/inc_header.jsp"%>
	<!--end header-->

	<!-- START BODY -->

	<tr style="border-bottom: 1px solid #cccccc">
		<td width="100%" style="padding: 25px 25px 25px 25px; border: 0">
			<font face="'Roboto', Arial, sans-serif">
				<h1>Hi ${custFirstName}! We've received your returned item.</h1>

				<div style="color: #d50642; margin-top: 10px;">
					<strong>Refund Amount: <fmt:formatNumber value="${returnRefundAmount}" type="currency" /></strong>
				</div>

				<div style="color: #d50642; margin-top:10px;">
					You will be refunded in 7-10 business days.
				</div>

				<div style="margin-top: 10px;">If you've requested an exchange, the transaction will be processed like a return and a new order.
					We'll send you a separate order confirmation when the new order is placed.
					Please allow 7-10 business days for that order to be processed. You will not be charged for return shipping on items you're exchanging.
				</div>
				
		</font> <%@include file="../../common/inc_order_header.jsp"%>
		</td>
	</tr>
	<tr>
		<td style="padding: 25px; background-color: #ebebeb; border-top: 1px solid #cccccc;">
			<font face="'Roboto', Arial, sans-serif">
				<h2>Item(s) Returned:</h2>

				<h3 style="color: #d50642; margin:15px 0;">
					We've received this item(s)
				</h3>
			</font>
			<dsp:getvalueof id="payload" param="payLoadItems">
				<dsp:droplet name="/atg/dynamo/droplet/ForEach">
					<dsp:param name="array" param="payLoadItems.orderLines" />
					<dsp:setvalue param="currentLine" paramvalue="element" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="productId" param="currentLine.itemStyle" />
						<dsp:getvalueof var="itemId" param="currentLine.itemId" />
						<dsp:getvalueof var="showProductTitle" param="currentLine.productTitle" />
						<c:if test="${empty showProductTitle}">
							<dsp:getvalueof var="showProductTitle" param="currentLine.itemName" />
						</c:if>
						<c:if test="${empty showProductTitle}">
							<dsp:getvalueof var="showProductTitle" param="currentLine.itemDesc" />
						</c:if>
						<dsp:getvalueof var="showBrandName" param="currentLine.itemBrand" />
						<dsp:getvalueof var="showSize" param="currentLine.itemSize" />
						<dsp:getvalueof var="showWidth" param="currentLine.itemWidth" />
						<dsp:getvalueof var="showColor" param="currentLine.itemColor" />
						<dsp:getvalueof var="showUPC" param="currentLine.itemUPC" />
						<dsp:getvalueof var="colorCode" param="currentLine.colorCode" />
						<c:if test="${empty colorCode}">
							<c:set var="colorCode" value="001" />
						</c:if>
						<dsp:getvalueof var="unitPrice" param="currentLine.itemUnitPrice" />
						<dsp:getvalueof var="itemPromoList" param="currentLine.itemPromoList" />
						<dsp:getvalueof var="itemQty" param="currentLine.itemStatusQuantity" />
						<dsp:getvalueof var="itemStatus" param="currentLine.status" />
						<dsp:getvalueof var="itemStatusUpdateDate" param="currentLine.statusUpdateDate" />

						<c:choose>
							<c:when
								test="${itemStatus ne 'In Process' and itemStatus ne 'Shipped' and itemStatus ne 'PickedUp'}">
								<dsp:getvalueof var="redOutline" value="1" />
							</c:when>
							<c:otherwise>
								<dsp:getvalueof var="redOutline" value="0" />
							</c:otherwise>
						</c:choose>
						<%@include file="common/inc_commerce_item.jsp"%>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:getvalueof>
		</td>
	</tr>

	<%@include file="common/inc_other_items_ordered.jsp"%>


	<%@include file="../../common/inc_email_signup.jsp"%>

	<!-- END BODY -->

	<!--footer-->
	<%@include file="../../common/inc_footer.jsp"%>
	<!--end footer-->
</dsp:page>