<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
		   prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>

<dsp:page>
	<dsp:getvalueof var="address1" param="currentLine[0].person.addressLine1" />
	<dsp:getvalueof var="address2" param="currentLine[0].person.addressLine2" />
	<dsp:getvalueof var="city" param="currentLine[0].person.city" />
	<dsp:getvalueof var="state" param="currentLine[0].person.state" />
	<dsp:getvalueof var="postalCode" param="currentLine[0].person.zipCode" />
	<dsp:getvalueof var="mallPlazaName" param="currentLine[0].mallPlazaName" /> 
	<dsp:getvalueof var="shippingMethod" param="currentLine[0].shipCode" />
</dsp:page>