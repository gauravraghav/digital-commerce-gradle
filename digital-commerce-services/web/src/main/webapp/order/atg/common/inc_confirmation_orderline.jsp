<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"	prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>
<dsp:page>
	<dsp:getvalueof var="shipType" param="currentLine.itemShipType" />
	<dsp:getvalueof var="productId" param="currentLine.productId" />
	<dsp:getvalueof var="showProductTitle" param="currentLine.productTitle" />
	<c:if test="${empty showProductTitle}">
		<dsp:getvalueof var="showProductTitle" param="currentLine.itemName" />
	</c:if>
	<c:if test="${empty showProductTitle}">
		<dsp:getvalueof var="showProductTitle" param="currentLine.itemDesc" />
	</c:if>
	<dsp:getvalueof var="showBrandName" param="currentLine.itemBrand" />
	<dsp:getvalueof var="showSize" param="currentLine.itemSize" />
	<dsp:getvalueof var="showWidth" param="currentLine.itemWidth" />
	<dsp:getvalueof var="showUPC" param="currentLine.itemUPC" />
	<dsp:getvalueof var="colorCode" param="currentLine.colorCode" />
	<dsp:getvalueof var="unitPrice" param="currentLine.itemUnitPrice" />
	<dsp:getvalueof var="showColor" param="currentLine.itemColor" />
	<dsp:getvalueof var="itemQty" param="currentLine.itemStatusQuantity" />
	<dsp:getvalueof var="itemPromoList" param="currentLine.itemPromoList" />
	<dsp:getvalueof var="itemStyle" param="currentLine.itemStyle" />
	<dsp:getvalueof var="itemPromoList" param="currentLine.itemPromoList" />
	<dsp:getvalueof var="recipientName" param="currentLine.recipientName" />
	<dsp:getvalueof var="recipientEmail" param="currentLine.recipientEmail" />
</dsp:page>