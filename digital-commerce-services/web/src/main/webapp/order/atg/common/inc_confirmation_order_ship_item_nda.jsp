<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
	prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.dsw.com/jsp/jstl/functions" prefix="dswfn"%>
<dsp:page>
	<c:if test="${not empty nonGCShippingGroups }">
		<c:if test="${not empty itemsByFullfilmentType}">
		<dsp:getvalueof var="displayMultiItemtxt" param="DISPLAY_MULTI_ITEMS_TXT_NDA"/>
		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" param="fullfilmentItems.SHIPNDA" />
			<dsp:setvalue param="currentLine" paramvalue="element" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="itemStyle" param="currentLine.itemStyle" />
				<%@include file="inc_confirmation_gcorderline_header.jsp"%>
			</dsp:oparam>
		</dsp:droplet>
		<%@include file="inc_confirmation_orderline_table_header.jsp"%>
		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" param="fullfilmentItems.SHIPNDA" />
			<dsp:setvalue param="currentLine" paramvalue="element" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="itemQty"
					param="currentLine.itemStatusQuantity" />
				<%@include file="inc_confirmation_orderline.jsp"%>
				<%@include file="inc_commerce_item.jsp"%>
			</dsp:oparam>
		</dsp:droplet> 
		</c:if>
	</c:if>
</dsp:page>