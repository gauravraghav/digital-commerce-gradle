<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
	prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.dsw.com/jsp/jstl/functions" prefix="dswfn"%>
<dsp:page>
<c:if test="${not empty egcShippingGroups }">
	<c:if test="${not empty itemsByFullfilmentType}">
		<c:set var="itemStyle" scope="page"	value="e-GC" />
		<c:set var="shippingMethod" scope="page"	value="eGiftCard" />
		<%@include file="inc_confirmation_orderline_table_header.jsp"%>
		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" param="fullfilmentItems.SHIPeGC" />
			<dsp:setvalue param="currentLine" paramvalue="element" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="itemStyle" param="currentLine.itemStyle" />
				<c:if test="${itemStyle eq 'e-GC'}">
					<%@include file="inc_confirmation_orderline.jsp"%>						
					<%@include file="inc_commerce_item.jsp"%>
				</c:if>
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
</c:if>
</dsp:page>