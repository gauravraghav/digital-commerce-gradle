<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
		   prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>

<dsp:page>
	<dsp:getvalueof var="address1"	param="currentLine.person.addressLine1" />
	<dsp:getvalueof var="address2" param="currentLine.person.addressLine2" />
	<dsp:getvalueof var="city" param="currentLine.person.city" />
	<dsp:getvalueof var="state" param="currentLine.person.state" />
	<dsp:getvalueof var="postalCode" param="currentLine.person.zipCode" />		
	<c:choose>
		<c:when test="${itemStyle eq 'PERSONALIZED-GC' || itemStyle eq 'STANDARD-GC'}">
			<c:set var="shippingMethod" scope="page"	value="nonEGiftCard" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="shippingMethod" param="currentLine.shipCode" />
		</c:otherwise>
	</c:choose>			
</dsp:page>