<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.dsw.com/jsp/jstl/functions" prefix="dswfn"%>
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="imagesUrl" bean="DSWConstants.imagesUrl" scope="page" />
<dsp:page>
	<c:if test="${not empty itemsByFullfilmentType}">
	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" param="fullfilmentItems.BOPIS" />
		<dsp:setvalue param="currentLine" paramvalue="element" />
		<dsp:oparam name="output">
			<%@include file="inc_confirmation_gcorderline_header.jsp"%>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="storePagesUrl" bean="DSWConstants.storePagesUrl" scope="page" />
	
	<c:set var="shippingMethodMessageKey" value="checkout.delivery.ISPU" />
	<c:set var="defaultShippingMethodKey" scope="page" value="checkout.delivery.GRN" />
	<c:choose>
			<c:when test="${not empty messages[shippingMethodMessageKey]}">
				<c:set var="shippingMethodLabel" scope="page" value="${messages[shippingMethodMessageKey]}" />
			</c:when>
			<c:otherwise>
				<c:set var="shippingMethodLabel" scope="page" value="${shippingMethod}" />
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${not empty messages[defaultShippingMethodKey]}">
				<c:set var="defaultShippingMethodLabel" scope="page" value="${messages[defaultShippingMethodKey]}" />
			</c:when>
			<c:otherwise>
				<c:set var="defaultShippingMethodLabel" scope="page" value="${shippingMethod}" />
			</c:otherwise>
		</c:choose>
		<c:if test="${empty customerFirstName}">
			<c:set var="customerFirstName" value="${custFirstName}" />
		</c:if>
	<c:if test="${empty state}">
			<c:set var="state" value="${state}" />
		</c:if>
		

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2" style="padding-top: 15px;">
					<font face="'Roboto', Arial, sans-serif">
						<h3>
							
							${shippingMethodLabel}  
							${mallPlazaName} today
							</h3>
					</font>
				</td>
			</tr>
			<tr>
				<td valign="middle" width="41" style="padding-bottom: 10px;">
					   <img src="${imagesUrl}/is/image/DSWShoes/time?fmt=png-alpha" alt="Store Time"/>
				</td>
				<td valign="middle" style="padding: 0 0 15px 15px;">
					<font face="'Roboto', Arial, sans-serif">
						<div>
							<a href="${storePagesUrl}/usa/${fn:toLowerCase(state)}/${fn:toLowerCase(fn:replace(city, " ", ""))}/dsw-designer-shoe-warehouse-${fn:toLowerCase(fn:replace(mallPlazaName, " ", "-"))}.html">
						${address1},
							<c:if test="${not empty address2}">
								${address2},
							</c:if>
								${city}, ${state}&nbsp;${postalCode}
							</a>
						</div>
						<dsp:getvalueof id="altPickUpPersonName" param="PROXYPICKUPNAME" />
						<c:if test="${not empty altPickUpPersonName}">
								<div><i>Designated pick up person: ${altPickUpPersonName}</i></div>
						</c:if>
					</font>
				</td>
			</tr>
		</table>

		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" param="fullfilmentItems.BOPIS" />
			<dsp:setvalue param="currentLine" paramvalue="element" />
			<dsp:oparam name="output">
				<%@include file="inc_confirmation_orderline.jsp"%>				
				<%@include file="inc_commerce_item.jsp"%>
				</dsp:oparam>
		</dsp:droplet>
	</c:if>

</dsp:page>