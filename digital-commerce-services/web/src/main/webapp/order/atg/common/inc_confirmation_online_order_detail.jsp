<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
		   prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>

<dsp:page>
	<tr>
		<td width="100%" style="padding: 30px 25px; border-bottom: 2px solid #000000;">
			<font face="'Roboto', Arial, sans-serif">
				<h2>Order Details:</h2>

				<h3 style="margin:15px 0;">ORDER SUMMARY:</h3>
			</font>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top" style="text-align: left;">
						<font face="'Roboto', Arial, sans-serif">
							<h4>SUBTOTAL: <span style="font-weight: normal;">(${totalCommerceItemCount} Items)</span></h4>
						</font>
					</td>
					<td valign="top" style="text-align: right;">
						<font face="'Roboto', Arial, sans-serif">
							<h4 style="text-align: right; display: inline-block">
								<fmt:formatNumber value="${rawSubTotal}" type="currency" />
							</h4>
						</font>
					</td>
				</tr>

					<%--Order level offers --%>
				<c:if test="${promoSavings gt 0.0}">
					<tr>
						<td valign="top" style="padding-left: 10px; text-align: left;">
							<font face="'Roboto', Arial, sans-serif">
								<i>Offers</i>
							</font>
						</td>
						<td valign="top" style="text-align: right;">
							<font face="'Roboto', Arial, sans-serif">
								<i>
									-<fmt:formatNumber value="${promoSavings}" pattern="$#,###,###.00" type="currency" />
								</i>
							</font>
						</td>
					</tr>
				</c:if>

					<%-- Check Rewards Certificates Applied --%>
				<c:if test="${rewardsShare gt 0.0}">
					<tr>
						<td valign="top" style="padding-left: 10px; text-align: left;">
							<font face="'Roboto', Arial, sans-serif">
								<i>Rewards</i>
							</font>
						</td>
						<td valign="top" style="text-align: right;">
							<font face="'Roboto', Arial, sans-serif">
								<i>-<fmt:formatNumber value="${rewardsShare}" pattern="$#,###,###.00" type="currency" /></i>
							</font>
						</td>
					</tr>
				</c:if>

				<tr>
					<td style="padding: 10px 0 0 0; text-align: left;">
						<font face="'Roboto', Arial, sans-serif">
							<h4>SHIPPING:</h4>
						</font>
					</td>

					<c:if test="${nonGCShippingTotal gt 0.0}">
						<td style="padding: 10px 0 0 0; text-align: right;">
							<font face="'Roboto', Arial, sans-serif">
								<h4 style="text-align: right;">
									<fmt:formatNumber value="${nonGCShippingTotal}" type="currency" />
								</h4>
							</font>
						</td>
					</c:if>
					<c:if test="${nonGCShippingTotal le 0.0}">
						<td style="padding: 10px 0 0 0; text-align: right;">
							<font face="'Roboto', Arial, sans-serif">
								<h4 style="text-align: right;">FREE</h4>
							</font>
						</td>
					</c:if>
				</tr>

				<c:if test="${gcShippingTotal gt 0.0}">
					<tr>
						<td style="padding: 0 0 15px 10px; text-align: left;">
							<font face="'Roboto', Arial, sans-serif">
								<h4>GIFT CARD SHIPPING:</h4>
							</font>
						</td>
						<td style="text-align: right;">
							<font face="'Roboto', Arial, sans-serif">
								<h4 style="text-align: right;">
									<fmt:formatNumber value="${gcShippingTotal}" type="currency" />
								</h4>
							</font>
						</td>
					</tr>
				</c:if>

				<tr>
					<td style="padding: 0 0 15px 10px; text-align: left;">
						<font face="'Roboto', Arial, sans-serif">
							<i>Taxes</i>
						</font>
					</td>
					<c:choose>
						<c:when test="${taxOffline}">
							<td valign="top" style="text-align: right;">
								<font face="'Roboto', Arial, sans-serif">
						<span style="font-size: 14px; background-color: #f1d61e; font-style:italic;">
							<c:out value="${messages.unavailable}" />
						</span>
								</font>
							</td>
						</c:when>
						<c:otherwise>
							<td valign="top" style="text-align: right;">
								<font face="'Roboto', Arial, sans-serif">
									<i><fmt:formatNumber value="${taxTotal}" type="currency" /></i>
								</font>
							</td>
						</c:otherwise>
					</c:choose>

				</tr>

				<tr>
					<td style="padding-top:15px; border-top: 1px solid #cccccc; text-align: left;">
						<font face="'Roboto', Arial, sans-serif">
							<h3>ORDER TOTAL:</h3>
						</font>
					</td>
					<td style="padding-top:15px; border-top: 1px solid #cccccc; text-align: right;">
						<font face="'Roboto', Arial, sans-serif">
							<h3 style="text-align: right;"><fmt:formatNumber value="${orderTotal}" type="currency"/></h3>
						</font>
					</td>
				</tr>

				<c:if test="${savings gt 0.0}">
					<tr>
						<td style="padding-left: 10px; text-align: left;">
							<font face="'Roboto', Arial, sans-serif">
								<i>You Saved:</i>
							</font>
						</td>
						<td style="text-align: right;">
							<font face="'Roboto', Arial, sans-serif">
								<i>
									<fmt:formatNumber type="currency" value="${savings}" />
								</i>
							</font>
						</td>
					</tr>
				</c:if>
			</table>
		</td>
	</tr>
</dsp:page>