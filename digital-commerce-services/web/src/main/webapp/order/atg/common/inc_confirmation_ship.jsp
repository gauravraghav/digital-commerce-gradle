<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
	prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.dsw.com/jsp/jstl/functions" prefix="dswfn"%>
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="imagesUrl" bean="DSWConstants.imagesUrl" scope="page" />
<dsp:page>
	<c:if test="${not empty nonGCShippingGroups }">
		<c:forEach var="shippingGroup" items="${nonGCShippingGroups}"
			varStatus="varStatus">
			<c:set var="shippingAddress"
				value="${dswfn:getPropertyValue(shippingGroup, 'shippingAddress')}" />
			<c:set var="shippingMethod"
				value="${dswfn:getPropertyValue(shippingGroup, 'shippingMethod')}" />
			<c:if test="${shippingMethod ne 'ISPU'}">
				<c:if test="${shippingMethod ne 'STS'}">
					<c:set var="shippingMethodMessageKey"
						value="checkout.delivery.${dswfn:getPropertyValue(shippingGroup, 'shippingMethod')}" />
					<c:set var="defaultShippingMethodKey" scope="page"
						value="checkout.delivery.GRN" />

					<c:choose>
						<c:when test="${not empty messages[shippingMethodMessageKey]}">
							<c:set var="shippingMethodLabel" scope="page"
								value="${messages[shippingMethodMessageKey]}" />
						</c:when>
						<c:otherwise>
							<c:set var="shippingMethodLabel" scope="page"
								value="${shippingMethod}" />
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${not empty messages[defaultShippingMethodKey]}">
							<c:set var="defaultShippingMethodLabel" scope="page"
								value="${messages[defaultShippingMethodKey]}" />
						</c:when>
						<c:otherwise>
							<c:set var="defaultShippingMethodLabel" scope="page"
								value="${shippingMethod}" />
						</c:otherwise>
					</c:choose>

					<c:if test="${empty customerFirstName}">
						<c:set var="customerFirstName" value="DSW Shoe Lover" />
					</c:if>
					<c:set var="address1" value="${dswfn:getPropertyValue(shippingAddress, 'address1')}" />
					<c:set var="address2" value="${dswfn:getPropertyValue(shippingAddress, 'address2')}" />
					<c:set var="city" value="${dswfn:getPropertyValue(shippingAddress, 'city')}" />
					<c:if test="${empty city}">
						<c:set var="city" value="${dswfn:getPropertyValue(shippingAddress, 'rank')}" />
					</c:if>
					<c:set var="state" value="${dswfn:getPropertyValue(shippingAddress, 'state')}" />

					<c:if test="${ empty state}">
						<c:set var="state" value="${dswfn:getPropertyValue(shippingAddress, 'region')}" />
					</c:if>
					<c:set var="postalCode" value="${dswfn:getPropertyValue(shippingAddress, 'postalCode')}" />
					<c:set var="showMultiShipmentMsg" value="false" />
					<c:set var="isFirstCI" value="true" />					
					<c:forEach var="commerceItemRelationship"
						items="${dswfn:getPropertyValue(shippingGroup, 'commerceItemRelationships')}">						
						<c:set var="commerceItem"
							value="${dswfn:getPropertyValue(commerceItemRelationship, 'commerceItem')}" />
						<c:set var="quantity"
							value="${dswfn:getPropertyValue(commerceItem, 'quantity')}" />
						<c:choose>
							<c:when test="${not isFirstCI or quantity gt 1.00}">
								<c:set var="showMultiShipmentMsg" value="true" />
							</c:when>
							<c:otherwise>
								<c:set var="isFirstCI" value="false" />
							</c:otherwise>
						</c:choose>
					</c:forEach>

					<!-- Line Item Header Info -->

					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="2" style="padding-top: 15px;">
								<font face="'Roboto', Arial, sans-serif">
									<h3>
										<c:if test="${shippingMethod eq 'GRN' }">
											${defaultShippingMethodLabel}
                                        </c:if>
										<c:if test="${shippingMethod ne 'GRN' }">
											<c:choose>
												<c:when test="${dswfn:getPropertyValue(commerceItem, 'storeHubQty') gt 0}">
                                                    ${defaultShippingMethodLabel}
                                                </c:when>
												<c:otherwise>
                                                    ${shippingMethodLabel}
                                                </c:otherwise>
											</c:choose>
										</c:if> ${customerFirstName} at:
									</h3>
							</font></td>
						</tr>

						<tr>
							<td valign="middle" width="41" style="padding: 10px 0;">
								<img src="${imagesUrl}/is/image/DSWShoes/shipping-truck?fmt=png-alpha"
								alt="shipping truck" />
							</td>
							<td valign="middle" style="padding: 10px 0; padding-left: 15px;">
								<font face="'Roboto', Arial, sans-serif">
									<div>${address1},
										<c:if test="${not empty address2}">
                                        ${address2},
                                    </c:if>
										${city}, ${state}&nbsp;${postalCode}
									</div> <c:if test="${showMultiShipmentMsg}">
										<i>Items may arrive in multiple shipments</i>
									</c:if>
							</font>
							</td>
						</tr>
					</table>


					<c:forEach var="commerceItemRelationship"
						items="${dswfn:getPropertyValue(shippingGroup, 'commerceItemRelationships')}">
						<c:set var="commerceItem"
							value="${dswfn:getPropertyValue(commerceItemRelationship, 'commerceItem')}" />
						<c:set var="shipType"
							value="${dswfn:getPropertyValue(commerceItem, 'shipType')}" />
						<c:if test="${shipType ne 'BOSTS'}">
							<c:if test="${shipType ne 'BOPIS'}">
								<c:set var="product"
									value="${dswfn:getPropertyValue(commerceItem, 'auxiliaryData.productRef')}" />
								<c:set var="showMultiShipmentMsg"
									value="${dswfn:getPropertyValue(commerceItem, 'groundShippingOnly')}" />
								<c:set var="productId"
									value="${dswfn:getPropertyValue(product, 'id')}" />
								<c:set var="sku"
									value="${dswfn:getPropertyValue(commerceItem, 'auxiliaryData.catalogRef')}" />
								<c:set var="productTitle"
									value="${dswfn:getPropertyValue(product, 'productTitle')}" />
								<c:set var="brandName"
									value="${dswfn:getPropertyValue(product, 'dswBrand.displayName')}" />
								<c:if test="${empty productTitle}">
									<c:set var="productTitle" value="N/A" />
								</c:if>
								<c:set var="showSize"
									value="${dswfn:getPropertyValue(product, 'showSize') and not empty dswfn:getPropertyValue(sku, 'size.displayName')}" />
								<c:set var="showWidth"
									value="${dswfn:getPropertyValue(product, 'showWidth') and not empty dswfn:getPropertyValue(sku, 'dimension.displayName')}" />
								<c:set var="showColor"
									value="${not empty dswfn:getPropertyValue(sku, 'color.displayName')}" />
								<c:set var="showUPC"
									value="${not empty dswfn:getPropertyValue(sku, 'upc')}" />
								<c:set var="colorCode"
									value="${dswfn:getPropertyValue(sku, 'color.colorCode')}" />

								<%
                                    com.dsw.commerce.services.order.DSWOrderImpl order = (com.dsw.commerce.services.order.DSWOrderImpl) pageContext.getAttribute("order");
                                    atg.commerce.order.CommerceItem commerceItem = (atg.commerce.order.CommerceItem) pageContext.getAttribute("commerceItem");
                                    pageContext.setAttribute("itemPromoList", order.listItemPromoDescriptions(commerceItem));
                                %>

								<!-- START OF LINE ITEM -->
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									style="background-color: #fff; border: 1px solid #cccccc;">
									<c:forEach var="itemPromo" items="${itemPromoList}">
										<tr>
											<td valign="middle" class="mobile-column-preserve-padding"
												style="line-height: 100%; padding: 10px 15px; border-top: 4px solid #0d6f96; border-bottom: 1px solid #cccccc;">
												<font face="'Roboto', Arial, sans-serif">${itemPromo}
													(applied)</font>
											</td>
										</tr>
									</c:forEach>
									<tr>
										<td style="padding: 25px 15px;">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td width="40%" class="mobile-column" valign="top">
														<a href="<dsp:valueof param="siteUrl"/>product/${dswfn:getSEOProductDisplayName(brandName)}-${dswfn:getSEOProductDisplayName(productTitle)}/${productId}?activeColor=${colorCode}&cm_mmc=emops-_-${analyticsInfo}">
															<img width="100%"
															src="${imagesUrl}/is/image/DSWShoes/${productId}_${colorCode}_ss_01?$search$&wid=253&hei=189"
															alt="product image" />
														</a>
													</td>
													<td width="60%" class="mobile-column" valign="top">
														<font face="'Roboto', Arial, sans-serif">
															<div style="text-transform: uppercase;">
																<strong>${brandName}</strong>
															</div>
															<div style="text-transform: uppercase;">${productTitle}</div>
															<div>
																<strong><fmt:formatNumber
																		value="${dswfn:getPropertyValue(commerceItem, 'priceInfo.amount')}"
																		type="currency" /></strong>
															</div> <br> <c:if test="${not empty showColor}">
																<div>Color: ${dswfn:getPropertyValue(sku, 'color.displayName')}</div>
															</c:if> <c:if test="${not empty showSize}">
																<div>Size: ${dswfn:getPropertyValue(sku, 'size.displayName')}</div>
															</c:if> <c:if test="${not empty showWidth}">
																<div>Width: ${dswfn:getPropertyValue(sku, 'dimension.displayName')}</div>
															</c:if>
															<div>Quantity:
																${dswfn:getPropertyValue(commerceItem, 'quantity')}</div>
															<c:if test="${not empty showUPC}">
																<div>Item Number: ${dswfn:getPropertyValue(sku, 'upc')}</div>
															</c:if>
													</font></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</c:if>
						</c:if>
					</c:forEach>
				</c:if>
			</c:if>
		</c:forEach>
	</c:if>

	<c:if test="${not empty gcShippingGroups }">
		<c:set var="showMultiShipmentMsg" value="false" />
		<c:set var="isFirstCI" value="true" />
		<c:forEach var="shippingGroup" items="${gcShippingGroups}"
			varStatus="varStatus">
			<c:set var="shippingAddress"
				value="${dswfn:getPropertyValue(shippingGroup, 'shippingAddress')}" />
			<c:set var="GCShippingMethodKey" scope="page"
				value="checkout.delivery.nonEGiftCard" />

			<c:choose>
				<c:when test="${not empty messages[GCShippingMethodKey]}">
					<c:set var="GCShippingMethodLabel" scope="page"
						value="${messages[GCShippingMethodKey]}" />
				</c:when>
				<c:otherwise>
					<c:set var="GCShippingMethodLabel" scope="page"
						value="Gift Card Shipping" />
				</c:otherwise>
			</c:choose>

			<c:if test="${empty customerFirstName}">
				<c:set var="customerFirstName" value="DSW Shoe Lover" />
			</c:if>
			<c:set var="address1"
				value="${dswfn:getPropertyValue(shippingAddress, 'address1')}" />
			<c:set var="address2"
				value="${dswfn:getPropertyValue(shippingAddress, 'address2')}" />
			<c:set var="city"
				value="${dswfn:getPropertyValue(shippingAddress, 'city')}" />
			<c:set var="state"
				value="${dswfn:getPropertyValue(shippingAddress, 'state')}" />
			<c:set var="postalCode"
				value="${dswfn:getPropertyValue(shippingAddress, 'postalCode')}" />
			<c:forEach var="commerceItemRelationship"
				items="${dswfn:getPropertyValue(shippingGroup, 'commerceItemRelationships')}">
				<c:set var="commerceItem"
					value="${dswfn:getPropertyValue(commerceItemRelationship, 'commerceItem')}" />
				<c:set var="quantity"
					value="${dswfn:getPropertyValue(commerceItem, 'quantity')}" />
				<c:choose>
					<c:when test="${not isFirstCI or quantity gt 1.00}">
						<c:set var="showMultiShipmentMsg" value="true" />
					</c:when>
					<c:otherwise>
						<c:set var="isFirstCI" value="false" />
					</c:otherwise>
				</c:choose>
			</c:forEach>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" style="padding-top: 15px;"><font
						face="'Roboto', Arial, sans-serif">
							<h3>${GCShippingMethodLabel}:</h3>
					</font></td>
				</tr>

				<tr>
					<td valign="middle" width="41" style="padding: 10px 0;"><img
						src="${imagesUrl}/is/image/DSWShoes/shipping-truck?fmt=png-alpha"
						alt="shipping truck"></td>
					<td valign="middle" style="padding: 10px 0; padding-left: 15px;">
						<font face="'Roboto', Arial, sans-serif">
							<div>${address1},
								<c:if test="${not empty address2}">
                                    ${address2},
                                </c:if>
								${city}, ${state}&nbsp;${postalCode}
							</div>
							<c:if test="${showMultiShipmentMsg}">
								<i>Items may arrive in multiple shipments</i>
							</c:if>
					</font>
					</td>
				</tr>
			</table>

			<c:forEach var="commerceItemRelationship"
				items="${dswfn:getPropertyValue(shippingGroup, 'commerceItemRelationships')}">
				<c:set var="commerceItem"
					value="${dswfn:getPropertyValue(commerceItemRelationship, 'commerceItem')}" />
				<c:set var="shipType"
					value="${dswfn:getPropertyValue(commerceItem, 'shipType')}" />
				<c:if test="${shipType ne 'BOSTS'}">
					<c:if test="${shipType ne 'BOPIS'}">
						<c:set var="product"
							value="${dswfn:getPropertyValue(commerceItem, 'auxiliaryData.productRef')}" />
						<c:set var="productId"
							value="${dswfn:getPropertyValue(product, 'id')}" />
						<c:set var="sku"
							value="${dswfn:getPropertyValue(commerceItem, 'auxiliaryData.catalogRef')}" />
						<c:set var="productTitle"
							value="${dswfn:getPropertyValue(product, 'productTitle')}" />
						<c:set var="brandName"
							value="${dswfn:getPropertyValue(product, 'dswBrand.displayName')}" />
						<c:if test="${empty productTitle}">
							<c:set var="productTitle" value="N/A" />
						</c:if>

						<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #ffffff; border: 1px solid #cccccc;">
							<tr>
								<td style="padding: 25px 15px">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="40%" valign="top" class="mobile-column">
												<img width="100%" src="${imagesUrl}/is/image/DSWShoes/d-s-w-gift-card?fmt=png&wid=253&hei=189" alt="product image" />
											</td>
											<td width="60%" valign="top" class="mobile-column">
												<font face="'Roboto', Arial, sans-serif">
													<%--<div style="text-transform: uppercase;">--%>
														<%--<strong>${brandName}</strong>--%>
													<%--</div>--%>
													<div style="text-transform: uppercase;"><strong>${productTitle}</strong></div>
													<div>
														<strong> <fmt:formatNumber
																value="${dswfn:getPropertyValue(commerceItem, 'priceInfo.amount')}"
																type="currency" />
														</strong>
													</div> <br>
													<div>Quantity: ${dswfn:getPropertyValue(commerceItem, 'quantity')}</div>
													<div>To: ${dswfn:getPropertyValue(commerceItem, 'recipientName')},
														${dswfn:getPropertyValue(commerceItem, 'recipientEmail')}</div>
											</font></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</c:if>
				</c:if>
			</c:forEach>
		</c:forEach>
	</c:if>

	<c:if test="${not empty egcShippingGroups }">
		<c:forEach var="shippingGroup" items="${egcShippingGroups}"
			varStatus="varStatus">
			<c:set var="shippingAddress"
				value="${dswfn:getPropertyValue(shippingGroup, 'shippingAddress')}" />
			<c:set var="eGCShippingMethodKey" scope="page"
				value="checkout.delivery.eGiftCard" />
			<c:choose>
				<c:when test="${not empty messages[eGCShippingMethodKey]}">
					<c:set var="eGCShippingMethodLabel" scope="page"
						value="${messages[eGCShippingMethodKey]}" />
				</c:when>
				<c:otherwise>
					<c:set var="eGCShippingMethodLabel" scope="page"
						value="Gift Card Shipping" />
				</c:otherwise>
			</c:choose>
			<c:if test="${empty customerFirstName}">
				<c:set var="customerFirstName" value="DSW Shoe Lover" />
			</c:if>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" style="padding-top: 15px;"><font
						face="'Roboto', Arial, sans-serif">
							<h3>${eGCShippingMethodLabel}:</h3>
					</font></td>
				</tr>
				<tr>
					<td valign="middle" width="41" style="padding: 0;"><img
						src="${imagesUrl}/is/image/DSWShoes/email"
						alt="email envelope"></td>
					<td valign="middle" style="padding: 10px 0;"><font
						face="'Roboto', Arial, sans-serif">
							<div>Electronic Delivery</div>
					</font></td>
				</tr>

			</table>

			<c:forEach var="commerceItemRelationship"
				items="${dswfn:getPropertyValue(shippingGroup, 'commerceItemRelationships')}">
				<c:set var="commerceItem"
					value="${dswfn:getPropertyValue(commerceItemRelationship, 'commerceItem')}" />
				<c:set var="shipType"
					value="${dswfn:getPropertyValue(commerceItem, 'shipType')}" />
				<c:if test="${shipType ne 'BOSTS'}">
					<c:if test="${shipType ne 'BOPIS'}">
						<c:set var="product"
							value="${dswfn:getPropertyValue(commerceItem, 'auxiliaryData.productRef')}" />
						<c:set var="productId"
							value="${dswfn:getPropertyValue(product, 'id')}" />
						<c:set var="sku"
							value="${dswfn:getPropertyValue(commerceItem, 'auxiliaryData.catalogRef')}" />
						<c:set var="productTitle"
							value="${dswfn:getPropertyValue(product, 'productTitle')}" />
						<c:set var="brandName"
							value="${dswfn:getPropertyValue(product, 'dswBrand.displayName')}" />
						<c:if test="${empty productTitle}">
							<c:set var="productTitle" value="N/A" />
						</c:if>

						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							style="background-color: #fff; border: 1px solid #ccc; margin-top: 20px;">
							<tr>
								<td style="padding: 25px 15px">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="40%" valign="top" class="mobile-column"><img
												width="100%"
												src="${imagesUrl}/is/image/DSWShoes/egiftcard?fmt=png&wid=253&hei=189"
												alt="product image" /></td>
											<td width="60%" valign="top" class="mobile-column"><font
												face="'Roboto', Arial, sans-serif">
													<%--<div style="text-transform: uppercase;">--%>
														<%--<strong>${brandName}</strong>--%>
													<%--</div>--%>
													<div style="text-transform: uppercase;"><strong>${productTitle}</strong></div>
													<div>
														<strong> <fmt:formatNumber
																value="${dswfn:getPropertyValue(commerceItem, 'priceInfo.amount')}"
																type="currency" />
														</strong>
													</div> <br>

													<div>Quantity: ${dswfn:getPropertyValue(commerceItem, 'quantity')}</div>
													<div>To: ${dswfn:getPropertyValue(commerceItem, 'recipientName')},
														${dswfn:getPropertyValue(commerceItem, 'recipientEmail')}</div>
											</font></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</c:if>
				</c:if>
			</c:forEach>
		</c:forEach>

	</c:if>
</dsp:page>