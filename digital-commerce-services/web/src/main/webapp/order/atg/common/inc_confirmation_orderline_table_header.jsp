<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
		   prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="imagesUrl" bean="DSWConstants.imagesUrl" scope="page" />

<c:set var="shippingMethodMessageKey"
				value="checkout.delivery.${shippingMethod}" />
	<c:set var="defaultShippingMethodKey" scope="page"
		value="checkout.delivery.SGRN" />
	<c:choose>
		<c:when test="${not empty messages[shippingMethodMessageKey]}">
			<c:set var="shippingMethodLabel" scope="page"
				value="${messages[shippingMethodMessageKey]}" />
		</c:when>
		<c:otherwise>
			<c:set var="shippingMethodLabel" scope="page"
				value="${shippingMethod}" />
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${not empty messages[defaultShippingMethodKey]}">
			<c:set var="defaultShippingMethodLabel" scope="page"
				value="${messages[defaultShippingMethodKey]}" />
		</c:when>
		<c:otherwise>
			<c:set var="defaultShippingMethodLabel" scope="page"
				value="${shippingMethod}" />
		</c:otherwise>
	</c:choose>
	<c:if test="${empty customerFirstName}">
		<c:set var="customerFirstName" value="${custFirstName}" />
	</c:if>
	<c:if test="${empty state}">
		<c:set var="state" value="${state}" />
	</c:if>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" style="padding-top: 15px;">
			<font face="'Roboto', Arial, sans-serif">
				<h3>
					<c:choose>
						<c:when test="${empty shippingMethodLabel}">
										  ${defaultShippingMethodLabel}
										</c:when>
						<c:otherwise>
											${shippingMethodLabel}
										</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${itemStyle eq 'PERSONALIZED-GC' || itemStyle eq 'STANDARD-GC' || itemStyle eq 'e-GC'}">
							:
						</c:when>
						<c:otherwise>
							${customerFirstName} at:
						</c:otherwise>
					</c:choose>
				</h3>
			</font>
			</td>
		</tr>
		<c:choose>
			<c:when test="${itemStyle eq 'e-GC'}">
			<tr>
				<td valign="middle" width="41" style="padding-bottom: 10px;">
					<img src="${imagesUrl}/is/image/DSWShoes/email"
					alt="email envelope">			
				</td>
				<td valign="middle" style="padding: 0 0 10px;">
					<font face="'Roboto', Arial, sans-serif">
						<div>Electronic Delivery</div>
					</font>
				</td>
			</tr>
			</c:when>
			<c:otherwise>
				<tr>
					<td valign="middle" width="41" style="padding-bottom: 10px;"><img
						src="${imagesUrl}/is/image/DSWShoes/shipping-truck?fmt=png-alpha"
						alt="shipping truck" /></td>
					<td valign="middle" style="padding: 0 0 10px 15px;">
					<font face="'Roboto', Arial, sans-serif">
							<div>${address1},
								<c:if test="${not empty address2}">
										${address2},
								</c:if>
								${city}, ${state}&nbsp;${postalCode}
						 	</div> 
						 		<c:if test="${displayMultiItemtxt eq 'YES'}">
									<c:choose>
										<c:when test="${itemStyle eq 'PERSONALIZED-GC' || itemStyle eq 'STANDARD-GC' || itemStyle eq 'e-GC'}">
										</c:when>
										<c:otherwise>
											<div>
												<i>Items may arrive in multiple shipments</i>
											</div>
										</c:otherwise>
									</c:choose>
									
								</c:if>
						</font>
					</td>
				</tr>
			</c:otherwise>
		</c:choose>		
	</table>