<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
	prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.dsw.com/jsp/jstl/functions" prefix="dswfn"%>
<dsp:page>
	<c:if test="${not empty gcShippingGroups }">
		<c:if test="${not empty itemsByFullfilmentType}">
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" param="fullfilmentItems.SHIPGC" />
				<dsp:setvalue param="currentLine" paramvalue="element" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="itemStyle" param="currentLine.itemStyle" />
					<c:if test="${itemStyle eq 'STANDARD-GC'}">
						<%@include file="inc_confirmation_orderline_header.jsp"%>	
						<%@include file="inc_confirmation_orderline.jsp"%>
						<%@include file="inc_confirmation_orderline_table_header.jsp"%>
						<%@include file="inc_commerce_item.jsp"%>
					</c:if>
					<c:if test="${itemStyle eq 'PERSONALIZED-GC' }">
						<%@include file="inc_confirmation_orderline_header.jsp"%>	
						<%@include file="inc_confirmation_orderline.jsp"%>
						<%@include file="inc_confirmation_orderline_table_header.jsp"%>
						<%@include file="inc_commerce_item.jsp"%>
					</c:if>
				</dsp:oparam>
			</dsp:droplet>
		</c:if>
	</c:if>
</dsp:page>