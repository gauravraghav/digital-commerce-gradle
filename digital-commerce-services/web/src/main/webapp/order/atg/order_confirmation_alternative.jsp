
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
	prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<dsp:page>
	<c:set var="isAlternatePickUpEmail" scope="page" value="true" />
	<%@include file="order_confirmation.jsp"%>
</dsp:page>