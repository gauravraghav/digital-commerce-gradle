<!DOCTYPE html>
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
		   prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>
<%@ taglib prefix="dspel" uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0" %>
<dsp:page>

	<dsp:importbean bean="/com/dsw/commerce/services/MessageLocator" />
	<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
	<dsp:getvalueof var="messages" bean="MessageLocator.msgKeyToStringMap" scope="page" />
	<dsp:getvalueof var="orderTotal" param="orderTotal" vartype="java.lang.String" />
	<dsp:getvalueof var="locale" param="locale" vartype="java.lang.String" />
	<dsp:getvalueof var="customerFirstName" param="customerFirstName" vartype="java.lang.String" />
	<dsp:getvalueof var="trackingDate" param="trackingDate" vartype="java.lang.String" />
	<dsp:getvalueof var="nonGCShippingGroups" param="NONGCSHIPG" />
	<dsp:getvalueof var="gcShippingGroups" param="GCSHIPG" />
	<dsp:getvalueof var="egcShippingGroups" param="EGCSHIPG" />
	<dsp:getvalueof var="creditCardType" param="CCTYPE" />
	<c:if test="${empty creditCardType}">
		<c:set var="creditCardType" value="Credit Card" />
	</c:if>
	<dsp:getvalueof var="lastFourFromVantiv" param="PVANTIVELASTFOUR" />
	<dsp:getvalueof var="creditCardNumber" param="PCCNUM" />
	<dsp:getvalueof var="tokenValue" param="PTOKENVAL" />
	<dsp:getvalueof var="creditCardAmount" param="PCCAMT" />
	<dsp:getvalueof var="payPalAmount" param="PPPAMT" />
	<dsp:getvalueof var="afterPayAmount" param="AFAMT" />
	<dsp:getvalueof var="afterPayInstallments" param="AFINSTALLMENTS" />
	<dsp:getvalueof var="afterPayInstallmentAmount" param="AFINSTALLMENTAMOUNT" />
	<dsp:getvalueof var="giftCardAmount" param="PGCAMT" />
	<dsp:getvalueof var="payPalPaymentGroup" param="PAYPALPAYMENTGROUP" />
	<dsp:getvalueof var="afterPayPaymentGroup" param="AFTERPAYPAYMENTGROUP" />
	<dsp:getvalueof var="giftCardNumbers" param="PGCNUM" />
	<dsp:getvalueof var="totalSavings" param="PSAVINGS" />
	<dsp:getvalueof var="totalCommerceItemCount" param="totalCommerceItemCount" />
	<dsp:getvalueof id="altPickUpPersonName" param="PROXYPICKUPNAME" />
	<dsp:getvalueof id="altPickUpPersonFirstName" param="PROXYPICKUPFIRSTNAME" />
	
	<c:if test="${empty creditCardAmount}">
		<c:set var="creditCardAmount" value="0" />
	</c:if>
	<c:if test="${empty payPalAmount}">
		<c:set var="payPalAmount" value="0" />
	</c:if>
	<c:if test="${empty afterPayAmount}">
		<c:set var="afterPayAmount" value="0" />
	</c:if>
	<c:if test="${empty giftCardAmount}">
		<c:set var="giftCardAmount" value="0" />
	</c:if>
	
	<dsp:getvalueof var="loyaltyTier" param="LOYALTYTIER" />
	<c:if test="${empty loyaltyTier}">
		<c:set var="loyaltyTier" value="NonMember" />
	</c:if>

	<dsp:getvalueof var="orderTotal" param="ORDRTOT"
		vartype="java.lang.String" />
	<c:if test="${empty orderTotal}">
		<c:set var="orderTotal" value="0" />
	</c:if>

	<dsp:getvalueof var="shippingTotal" param="PSHIPPING" />
	<c:if test="${empty shippingTotal}">
		<c:set var="shippingTotal" value="0" />
	</c:if>

	<dsp:getvalueof var="gcShippingTotal" param="PGCSHIPPING" />
	<c:if test="${empty gcShippingTotal}">
		<c:set var="gcShippingTotal" value="0" />
	</c:if>

	<dsp:getvalueof var="nonGCShippingTotal" param="PNONGCSHIPPING" />
	<c:if test="${empty nonGCShippingTotal}">
		<c:set var="nonGCShippingTotal" value="0" />
	</c:if>

	<dsp:getvalueof var="rawSubTotal" param="MERCH" />
	<c:if test="${empty rawSubTotal}">
		<c:set var="rawSubTotal" value="0" />
	</c:if>

	<dsp:getvalueof var="taxTotal" param="TAX" />
	<c:if test="${empty taxTotal}">
		<c:set var="taxTotal" value="0" />
	</c:if>

	<dsp:getvalueof var="savings" param="savings" />
	<c:if test="${empty savings}">
		<c:set var="savings" value="0" />
	</c:if>

	<dsp:getvalueof var="promoSavings" param="promoSavings" />
	<c:if test="${empty promoSavings}">
		<c:set var="promoSavings" value="0" />
	</c:if>

	<dsp:getvalueof var="rewardsShare" param="RewardsShare" />
	<c:if test="${empty rewardsShare}">
		<c:set var="rewardsShare" value="0" />
	</c:if>

	<dsp:getvalueof var="locale" param="locale" vartype="java.lang.String" />
	<fmt:setLocale value="${locale}" />
	
	<dsp:getvalueof var="orderNumber" param="ORDERID" />
	
	<dsp:getvalueof var="orderDate" param="PDATE" />	
	
		
	<c:if test="${not empty trackingDate}">
		<c:set var="analyticsInfo" value="${trackingDate}_order" />
	</c:if>
	<dsp:getvalueof var="shippingCharge" param="shipping" />
	<dsp:getvalueof var="shippingMethod" param="shippingMethod" />
	
	<c:set var="showMultiShipmentMsg" value="false" />
	<dsp:getvalueof var="totalNonEGCCommerceCount" param="totalNonEGCCommerceCount" />
	<c:if test="${totalNonEGCCommerceCount gt 1.0}">
		<c:set var="showMultiShipmentMsg" value="true" />
	</c:if>
	
	<fmt:setLocale value="${locale}" />
	
	<!--start header-->
	<c:choose>
		<c:when test="${isAlternatePickUpEmail and not empty altPickUpPersonFirstName}">
			<dsp:getvalueof var="previewText" value="A friend has placed an order for you to pick up." vartype="java.lang.String" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="previewText" value="Your order has been received." vartype="java.lang.String" />
	</c:otherwise>
	</c:choose>
	<dsp:getvalueof var="previewText" value="Your order has been received." vartype="java.lang.String" />
	<%@include file="../../common/inc_header.jsp" %>
	<!--end header-->

	<!-- START BODY -->
	<tr style="border-bottom: 1px solid #cccccc;">
		<td width="100%" style="padding: 25px 25px 25px 25px; border: 0">
			<font face="'Roboto', Arial, sans-serif">
			<c:choose>
					<c:when test="${isAlternatePickUpEmail and not empty altPickUpPersonFirstName}">
						<h1 style="padding: 0; margin-bottom: 15px;">
							Hi ${altPickUpPersonFirstName}!
						</h1>
						<span>${customerFirstName} has placed an order on DSW.com for you to pick up. We'll let you know when it's ready.</span>
					</c:when>
					<c:otherwise>
						<h1 style="padding: 0; margin-bottom: 15px;">
							Hi ${customerFirstName}!</strong>
						</h1>
					<span>Thank you for your order. We'll let you know when it's ready for pick up or when it's on the way.</span>
					</c:otherwise>
				</c:choose>
			</font>

			<%@include file="../../common/inc_order_header.jsp"%>
		</td>
	</tr>
	<tr>
		<td style="padding: 25px; background-color: #ebebeb; border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;">
			<font face="'Roboto', Arial, sans-serif">
				<h2 style="border-bottom: 1px solid #cccccc;">Item(s) Ordered:</h2>
				<dsp:getvalueof var="itemsByFullfilmentType" param="fullfilmentItems.BOPIS">
					<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
						<dsp:param name="value" param="fullfilmentItems.BOPIS" />
						<dsp:oparam name="false">
							<%@include file="common/inc_confirmation_order_bopis_item.jsp" %>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:getvalueof>
				<dsp:getvalueof var="itemsByFullfilmentType" param="fullfilmentItems.BOSTS">
					<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
						<dsp:param name="value" param="fullfilmentItems.BOSTS" />
						<dsp:oparam name="false">
							<%@include file="common/inc_confirmation_order_bosts_item.jsp" %>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:getvalueof>
				<dsp:getvalueof var="itemsByFullfilmentType" param="fullfilmentItems.SHIPNDA">
					<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
						<dsp:param name="value" param="fullfilmentItems.SHIPNDA" />
						<dsp:oparam name="false">
							<%@include file="common/inc_confirmation_order_ship_item_nda.jsp" %>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:getvalueof>
				<dsp:getvalueof var="itemsByFullfilmentType" param="fullfilmentItems.SHIP2ND">
					<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
						<dsp:param name="value" param="fullfilmentItems.SHIP2ND" />
						<dsp:oparam name="false">
							<%@include file="common/inc_confirmation_order_ship_item_2nd.jsp" %>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:getvalueof>
				<dsp:getvalueof var="itemsByFullfilmentType" param="fullfilmentItems.SHIPGRN">
					<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
						<dsp:param name="value" param="fullfilmentItems.SHIPGRN" />
						<dsp:oparam name="false">
							<%@include file="common/inc_confirmation_order_ship_item_grn.jsp" %>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:getvalueof>
				<dsp:getvalueof var="itemsByFullfilmentType" param="fullfilmentItems.SHIPeGC">
					<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
						<dsp:param name="value" param="fullfilmentItems.SHIPeGC" />
						<dsp:oparam name="false">
							<%@include file="common/inc_confirmation_order_ship_item_egc.jsp" %>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:getvalueof>				
				<dsp:getvalueof var="itemsByFullfilmentType" param="fullfilmentItems.SHIPGC">
					<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
						<dsp:param name="value" param="fullfilmentItems.SHIPGC" />
						<dsp:oparam name="false">
							<%@include file="common/inc_confirmation_order_ship_item_gc.jsp" %>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:getvalueof>
				
			</font>
		</td>
	</tr>

	<%@include file="common/inc_confirmation_online_order_detail.jsp"%>

	
<c:if test="${orderTotal gt 0.0}">
	<tr>
		<td style="padding: 15px 25px; border-bottom: 1px solid #cccccc;">
			<font face="'Roboto', Arial, sans-serif">
				<h3 style="padding-bottom: 5px;">
					<strong>Payment Method(s):</strong>
				</h3>
		</font>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<c:if test="${not empty payPalPaymentGroup}">
						<td style="text-align: left;"><c:if
								test="${payPalAmount != 0}">PayPal </c:if></td>
					</c:if>
					<c:if test="${not empty afterPayPaymentGroup}">
						<td style="text-align: left;"><c:if
								test="${afterPayAmount != 0}">AfterPay </c:if></td>
					</c:if>
					<c:if test="${not empty creditCardType and empty payPalPaymentGroup and empty afterPayPaymentGroup}">
						<td style="text-align: left;">
							<font face="'Roboto', Arial, sans-serif">
								<c:choose>
									<c:when test="${creditCardType ne 'Credit Card'}">
										<div>${creditCardType}
											ending in *
											<c:choose>
												<c:when test="${not empty lastFourFromVantiv}">${lastFourFromVantiv}</c:when>
												<c:when test="${not empty tokenValue}">${tokenValue}</c:when>
												<c:when test="${not empty creditCardNumber}">${creditCardNumber}</c:when>
											</c:choose>
										</div>
									</c:when>
									<c:when test="${creditCardAmount != 0}">
										<fmt:formatNumber value="${creditCardAmount}" type="currency" />
									</c:when>
								</c:choose>
						</font></td>
					</c:if>

					<td valign="top" style="text-align: right;"><font
						face="'Roboto', Arial, sans-serif"> <c:choose>
								<c:when test="${payPalAmount != 0}">
									<fmt:formatNumber value="${payPalAmount}" type="currency" />
								</c:when>
								<c:when test="${afterPayAmount != 0}">
									<fmt:formatNumber value="${afterPayAmount}" type="currency" />
								</c:when>
								<c:when test="${creditCardAmount != 0}">
									<fmt:formatNumber value="${creditCardAmount}" type="currency" />
								</c:when>
							</c:choose>
					</font></td>
				</tr>
			<c:if test="${not empty afterPayPaymentGroup}">
				<tr>
					<td style="text-align: left;">${afterPayInstallments} installments of <fmt:formatNumber value="${afterPayInstallmentAmount}" type="currency" />
					</td>
				</tr>
			</c:if>

				<c:forEach var="giftCard" items="${giftCardNumbers}">
						<tr>
							<td style="text-align: left;">
								<font face="'Roboto', Arial, sans-serif">
								 <span>
								 	Gift Card ending in * ${giftCard.key}
								  </span>
								</font>
							</td>
							<td style="text-align: right;">
								<font face="'Roboto', Arial, sans-serif">
								 	<fmt:formatNumber value="${giftCard.value}" type="currency" />
								</font>
							</td>
						</tr>
				</c:forEach> 
				</table>
			</td>
		</tr>
	</c:if>	

	<%@include file="../../common/inc_email_signup.jsp" %>

	<!-- END BODY -->

	<!--footer-->	
	<%@include file="../../common/inc_footer.jsp" %>
	<!--end footer-->

</dsp:page>