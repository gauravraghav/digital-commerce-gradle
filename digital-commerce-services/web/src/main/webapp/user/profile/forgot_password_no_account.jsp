<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="dspel" uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0"%>
<%@ page trimDirectiveWhitespaces="true"%>
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />

<dsp:page>

    <dsp:getvalueof var="trackingDate" param="trackingDate" vartype="java.lang.String" />
    <c:if test="${not empty trackingDate}">
        <c:set var="analyticsInfo" value="${trackingDate}_customerservice" />
    </c:if>
    <c:set var="isCustomerServiceContact" value="true" />

    <dsp:getvalueof var="previewText" value="No DSW account found with this email" vartype="java.lang.String" />

    <!--header-->
    <%@include file="../../common/inc_header.jsp"%>
    <!--end header-->

    <!-- START BODY -->
    <tr>
        <td class="100p" style="padding: 25px 25px 25px 25px;">
            <font face="'Roboto', Arial, sans-serif">
                <h1>No DSW account found with this email</h1>
                <p>You requested an email because you have forgotten your password for your DSW registered account. Our records indicate an account for this email address doesn't exist. To link an existing VIP account number to your online profile or Create a new account, please follow the instructions below:</p>
                <ol>
                    <li>Go to the <a href='<dsp:valueof param="createAccountUrl"/>&cm_mmc=emops-_-${analyticsInfo}'>DSW Create Account</a> page</li>
                    <li>Enter your First Name, Last Name, Email Address, Password and Mailing Address</li>
                </ol>
                <p style="margin-bottom: 0;">If you did not request a temporary password through DSW, please contact our customer service department at <a href="mailto:customerservice@dsw.com">customerservice@dsw.com</a> or 1.866.681.7306.</p>
            </font>
        </td>
    </tr>
    <!-- END BODY -->

    <!--footer-->
    <%@include file="../../common/inc_footer.jsp"%>
    <!--end footer-->
</dsp:page>