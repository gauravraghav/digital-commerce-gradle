<!DOCTYPE html>
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
	prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<dsp:page>

	<dsp:getvalueof var="trackingDate" param="trackingDate"
		vartype="java.lang.String" />
	<c:if test="${not empty trackingDate}">
		<c:set var="analyticsInfo" value="${trackingDate}_customerservice" />
	</c:if>
	<c:set var="isCustomerServiceContact" value="true" />

	<!--start header-->
	<dsp:getvalueof var="previewText" value="Your username has been successfully changed." vartype="java.lang.String" />
	<%@include file="../../common/inc_header.jsp"%>
	<!--end header-->

	<tr>
		<td class="100p" style="padding: 25px 25px 25px 25px;">
			<font face="'Roboto', Arial, sans-serif">
			<h1>Hi <dsp:valueof param="firstName" />! Your Username has been changed.</h1>
			<div>
				Your username <dsp:valueof param="oldUserName" /> has been successfully changed to <dsp:valueof param="newUserName" />
			</div>
			<div style="margin-top:10px;">If you did not change your username please contact Customer Service at (866) 379-7463 immediately.</div>
			</font>
		</td>
	</tr>

	<!--footer-->
	<%@include file="../../common/inc_footer.jsp"%>
	<!--end footer-->
</dsp:page>