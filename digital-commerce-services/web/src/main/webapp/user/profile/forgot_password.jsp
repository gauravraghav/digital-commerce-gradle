<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="dspel" uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0"%>
<%@ page trimDirectiveWhitespaces="true"%>
<dsp:importbean bean="/com/dsw/commerce/common/service/DSWConstants" />
<dsp:getvalueof var="temporaryPasswordResetExpiryHours" bean="DSWConstants.temporaryPasswordResetExpiryHours" />

<dsp:page>

	<dsp:getvalueof var="trackingDate" param="trackingDate" vartype="java.lang.String" />
	<c:if test="${not empty trackingDate}">
		<c:set var="analyticsInfo" value="${trackingDate}_customerservice" />
	</c:if>
	<c:set var="isCustomerServiceContact" value="true" />

	<dsp:getvalueof var="previewText" value="Here is your temporary password. Please use this to reset your password on DSW.com." vartype="java.lang.String" />
	<%@include file="../../common/inc_header.jsp"%>

	<!-- START BODY -->

	<tr>
		<td class="100p" style="padding: 25px 25px 25px 25px;">
			<font face="'Roboto', Arial, sans-serif">
				<h1>Hi <dsp:valueof param="firstName" />! Here is your Password Reset Code.</h1>
				<span>Please use this temporary password to reset your password on dsw.com. This password will expire in ${temporaryPasswordResetExpiryHours} hours.</span>
			</font>
		</td>
	</tr>
	<tr>
		<td align="center" valign="top" style="background-color: #ebebeb; border-top: 1px solid #ccc; border-bottom: 5px solid #000000; padding: 0 15px;">

			<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center" style="padding: 35px 0 25px 0;">
						<table align="center" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center" bgcolor="#ffffff" style="border: 1px solid #cccccc; padding: 15px 15px 15px 15px; line-height: 100%;">
									<font face="'Roboto', Arial, sans-serif">
										<span style="font-size: 24px; line-height: 100%;">
											<strong><dsp:valueof param="newpassword" /></strong>
										</span>
								</font>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td align="center" style="padding: 0 20% 50px 20%;" class="mobile-column">
						<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center" style="padding: 13px 25px; background-color: #000000; color: #ffffff; text-align: center; line-height: 100%;">
									<a class="primary-button" href='<dsp:valueof param="loginUrl"/>&cm_mmc=emops-_-${analyticsInfo}'>
										<font face="'P22', Arial, sans-serif"><strong>RETURN TO LOG IN</strong></font>
									</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

		</td>
	</tr>
	<!-- END BODY -->

	<!--footer-->
	<%@include file="../../common/inc_footer.jsp"%>
	<!--end footer-->
</dsp:page>