<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"
	prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<dsp:page>

	<dsp:getvalueof var="trackingDate" param="trackingDate"
		vartype="java.lang.String" />
	<c:if test="${not empty trackingDate}">
		<c:set var="analyticsInfo" value="${trackingDate}_customerservice" />
	</c:if>
	<c:set var="isCustomerServiceContact" value="true" />

	<!--start header-->
	<dsp:getvalueof var="previewText" value="Your password was successfully changed." vartype="java.lang.String" />
	<%@include file="../../common/inc_header.jsp"%>
	<!--end header-->

	<tr align="left">
		<td class="100p" style="padding: 25px 25px 15px 25px;">
			<font face="'Roboto', Arial, sans-serif">
				<h1>Hi <dsp:valueof param="firstName" />! Your Password has been changed.</h1>
				<span>Your password has been successfully changed.</span>
				<p style="margin-top:10px;">If you did not reset your password, please contact Customer Service at 1-866-DSW-SHOES (1-866-379-7463).</p>
			</font>
		</td>
	</tr>

	<tr>
		<td class="100p" style="padding: 0 35px 40px 35px; border-bottom: 5px solid #000000;" align="center">
			<table class="100p" align="center" border="0" cellspacing="0" cellpadding="20">
				<tr style="border-bottom: 1px solid #cccccc">
					<td width="100%" colspan="2" style="padding: 0 25px 25px 25px; border: 0">
				</tr>
				<tr>
					<td class="100p" align="center" width="195" bgcolor="#000000" style="text-decoration: none; line-height: 100%;">
						<font face="'Roboto', Arial, sans-serif">
							<a href="<dsp:valueof param="loginUrl"/>?cm_mmc=emops-_-${analyticsInfo}" style="color: #ffffff; font-weight: bold; text-decoration: none;">
								GO TO LOG IN
							</a>
						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<!--footer-->
	<%@include file="../../common/inc_footer.jsp"%>
	<!--end footer-->
</dsp:page>