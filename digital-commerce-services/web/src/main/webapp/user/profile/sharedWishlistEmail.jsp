<!DOCTYPE html>
<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="dswfn" uri="http://www.dsw.com/jsp/jstl/functions"%>

<dsp:importbean bean="/atg/userprofiling/Profile" />

<dsp:page>

	<dsp:getvalueof var="trackingDate" param="trackingDate" />
	<c:if test="${not empty trackingDate}">
		<c:set var="analyticsInfo" value="${trackingDate}_favorites" />
	</c:if>
	<dsp:getvalueof param="name" var="ownerName" />
	<dsp:getvalueof param="message" var="personalMessage" />
	<dsp:getvalueof var="locale" param="locale" vartype="java.lang.String" />
	<fmt:setLocale value="${locale}" />
	<c:set var="isCustomerServiceContact" value="true" />

	<!--start header-->
	<dsp:getvalueof var="previewText" value="${ownerName} has shared her favorite items from DSW with you." vartype="java.lang.String" />
	<%@include file="../../common/inc_header.jsp"%>
	<!--end header-->


	<tr>
		<td class="100p" style="padding: 25px 25px 25px 25px;">
			<font face="'Roboto', Arial, sans-serif">
				<h1>A Favorites List has been shared with you.</h1>
				<div style="margin:5px 0;"><dsp:valueof param="name" /> (<dsp:valueof param="ownerEmail" />) shared her favorite items from DSW with you.</div>
				<c:if test="${not empty personalMessage}">
					<div style="margin:5px 0;">${personalMessage}</div>
				</c:if>
			</font>

			<table  style="width: 100%; border: 0px;" cellspacing="0" cellpadding="0" align=center>
				<%--<tr>--%>
					<td align=center style="padding:25px 30px 0;">

						<dsp:droplet name="/atg/dynamo/droplet/TableForEach">
							<dsp:param name="array" param="details" />
							<dsp:param name="numColumns" value="2"/>
						<!-- since <table> and </table> in outputStart n outputEnd, a html table is displayed
							with table row as <tr> and </tr> in outputRowStart n outputRowEnd
							and each column (table cell <td></td>) as defined in the output oparam will be displayed.
							The order of oparam being set doesn't matter - outputStart, outputRowStart, outputEnd and outputRowEnd.-->

						<dsp:oparam name="outputStart">
						<table border="1px solid #000000" cellpadding="0" cellspacing="0" style="border-collapse: collapse; vertical-align: middle" class="100p" >
						</dsp:oparam>

						<dsp:oparam name="outputEnd">
						</table>
						</dsp:oparam>

						<dsp:oparam name="outputRowStart">
						<tr>
						</dsp:oparam>

						<dsp:oparam name="outputRowEnd">
						</tr>
						</dsp:oparam>

				<dsp:oparam name="output">

						<td class="100p"  valign="top" style=" width: 255px; height: 430px; border: 1px solid gray; text-align: left; padding:25px 20px;">
							<div style="width: 255px"  valign="top" >
							<dsp:droplet name="/atg/dynamo/droplet/IsNull">
								<dsp:param name="value" param="element" />
								<dsp:oparam name="false">
									<dsp:setvalue param="item" paramvalue="element" />
									<dsp:droplet name="/atg/commerce/catalog/ProductLookup">
										<dsp:param name="id" param="item.productId" />
										<dsp:param name="filterByCatalog" value="false" />
										<dsp:param bean="/OriginatingRequest.requestLocale.localeString" name="repositoryKey" />
										<dsp:param value="product" name="elementName" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="brandName" param="product.dswBrand.displayName" vartype="java.lang.String" />
											<dsp:getvalueof var="productName" param="product.displayName" vartype="java.lang.String" />
											<dsp:getvalueof var="productId" param="product.id" vartype="java.lang.String" />
											<dsp:getvalueof var="defaultColorCode" param="product.defaultSKU.color.colorCode" vartype="java.lang.String" />
											<dsp:getvalueof var="defaultMSRP" param="product.nonMemberMSRP" vartype="java.lang.String" />
											<dsp:getvalueof var="defaultMaxPrice" param="product.nonMemberMaxPrice" vartype="java.lang.String" />
											<dsp:getvalueof var="defaultMinPrice" param="product.nonMemberMinPrice" vartype="java.lang.String" />
										</dsp:oparam>
									</dsp:droplet>
									<dsp:droplet name="/atg/dynamo/droplet/IsNull">
										<dsp:param name="value" param="item.catalogRefId" />
										<dsp:oparam name="false">
											<dsp:droplet name="/atg/commerce/catalog/SKULookup">
												<dsp:param name="id" param="item.catalogRefId" />
												<dsp:param name="useParams" value="true" />
												<dsp:param name="filterByCatalog" value="false" />
												<dsp:setvalue param="sku" paramvalue="element" />
												<dsp:oparam name="output">
													<dsp:getvalueof var="catRefId" param="item.catalogRefId" vartype="java.lang.String" />
													<dsp:getvalueof var="colorCode" param="sku.color.colorCode" vartype="java.lang.String" />
													<dsp:getvalueof var="originalPrice" param="sku.originalPrice" vartype="java.lang.String" />
													<dsp:getvalueof var="msrp" param="sku.msrp" vartype="java.lang.String" />
													<dsp:getvalueof var="productQty" param="item.quantityDesired" vartype="java.lang.String"/>
													<div valign="top" >
														<a href="<dsp:valueof param="siteUrl"/>product/${dswfn:getSEOProductDisplayName(brandName)}-${dswfn:getSEOProductDisplayName(productName)}/${productId}?activeColor=${colorCode}&cm_mmc=emops-_-${analyticsInfo}">
														<img src="${imagesUrl}/is/image/DSWShoes/${productId}_${colorCode}_ss_01?$search$&wid=253&hei=189" alt="Product image" style="margin-bottom:20px;">
														</a>
														<font face="'Roboto', Arial, sans-serif">
															<div><strong>${brandName}</strong></div>
															<div>${productName}</div>
															<div>
																<strong><fmt:formatNumber value="${originalPrice}" type="currency" /></strong>
															</div>
															<div style="color: #7f7f7f;">
																Comp. value <fmt:formatNumber value="${msrp}" type="currency" />
															</div>
															<br>
															<div style="color: #7f7f7f;">
																Color: <dsp:valueof param="sku.color.displayName" />
															</div>
															<div style="color: #7f7f7f;">
																Size: <dsp:valueof param="sku.size.displayName" />
															</div>
															<div style="padding-bottom: 25px; color: #7f7f7f;">
																Quantity: <dsp:valueof param="item.quantityDesired" />
															</div>
														</font>
													</div>
													<br>
													<center>
														<font face="'Roboto', Arial, sans-serif">
															<a href="<dsp:valueof param="siteUrl"/>landing?type=atb&prodid=${productId}&sku=${catRefId}&qty=${productQty}&cm_mmc=emops-_-${analyticsInfo}"
															   style="font-weight: bold; color: #000000; text-align: center; border: 1px solid #000000; padding: 12px 50px; text-decoration: none;" width=100%>
																ADD TO BAG
															</a>

														</font>
													</center>

												</dsp:oparam>
											</dsp:droplet>

										</dsp:oparam>

										<dsp:oparam name="true">
											<div valign="top">
												<a href="<dsp:valueof param="siteUrl"/>product/${dswfn:getSEOProductDisplayName(brandName)}-${dswfn:getSEOProductDisplayName(productName)}/${productId}?activeColor=${defaultColorCode}&cm_mmc=emops-_-${analyticsInfo}">
												<img src="${imagesUrl}/is/image/DSWShoes/${productId}_${defaultColorCode}_ss_01?$search$&wid=253&hei=189" alt="Product image" style="margin-bottom:20px;">
												</a>
													<font face="'Roboto', Arial, sans-serif">
													<div><strong>${brandName}</strong></div>
													<div>${productName}</div>
													<div>
														<dsp:droplet name="/atg/dynamo/droplet/Compare">
															<dsp:param name="obj1" value="${defaultMaxPrice}" />
															<dsp:param name="obj2" value="${defaultMinPrice}" />
															<dsp:oparam name="greaterthan">
																<strong><fmt:formatNumber value="${defaultMinPrice}" type="currency" /> - <fmt:formatNumber value="${defaultMaxPrice}" type="currency" /></strong>
															</dsp:oparam>
															<dsp:oparam name="default">
																<strong><fmt:formatNumber value="${defaultMinPrice}" type="currency" /></strong>
															</dsp:oparam>
														</dsp:droplet>
													</div>
													<div style="color: #7f7f7f;">
														Comp. value<fmt:formatNumber value="${defaultMSRP}" type="currency" />
													</div>
													<br>
													<div style="padding-bottom: 25px; color: #7f7f7f;">
														Quantity: <dsp:valueof param="item.quantityDesired" />
													</div>
												</font>
											</div>
											<br>
											<center>
												<font face="'Roboto', Arial, sans-serif">
													<a href="<dsp:valueof param="siteUrl"/>product/${dswfn:getSEOProductDisplayName(brandName)}-${dswfn:getSEOProductDisplayName(productName)}/${productId}?activeColor=${defaultColorCode}&cm_mmc=emops-_-${analyticsInfo}"
													   style="font-weight: bold; color: #000000; text-align: center; border: 1px solid #000000; padding: 12px 50px; text-decoration: none;" width=100%>
														ADD TO BAG
													</a>

												</font>
											</center>
										</dsp:oparam>

									</dsp:droplet>
									<br>

								</dsp:oparam>
							</dsp:droplet>
							</div>
						</td>

					</dsp:oparam>
				</dsp:droplet>
				</td>
				<%--</tr>--%>
			</table>
		</td>
	</tr>

	<tr>
		<td align="center" style="padding: 0 23% 45px 23%;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" colspan="2" class="mobile-column" valign="middle" style="padding: 13px 20px 13px 20px; background-color: #000000; color: #ffffff; text-align: center; line-height: 100%;">
						<a class="primary-button"  href="<dsp:valueof param="sharedWLUrl"/>?cm_mmc=emops-_-${analyticsInfo}">
							<font face="'P22', Arial, sans-serif"><strong> GO TO SHARED FAVORITES LIST</strong></font>
						</a>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<!--footer-->
	<%@include file="../../common/inc_footer.jsp"%>
	<!--end footer-->

</dsp:page>