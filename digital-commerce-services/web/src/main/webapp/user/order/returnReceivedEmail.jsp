<%@ taglib uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" prefix="dsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<dsp:page>
<html>
<body>
<dsp:getvalueof id="payload" param="payLoadItems">
    <table border="1">
    <tr><td width="20%">First Name</td><td width="10%"><dsp:valueof param="payLoadItems.customerFirstName"/></td> </tr>
    <tr><td width="20%">VIP Member ID*</td><td width="20%"><dsp:valueof param="REWARDSNUMBER"/></td> </tr>
    <tr><td width="20%">Order Number</td><td width="20%"><dsp:valueof param="orderNumber"/></td> </tr>
    <tr><td width="20%">Order Date</td><td width="20%"><dsp:valueof param="orderDate"/></td> </tr>
    <tr><td width="20%">Order Total</td><td width="20%"><dsp:valueof param="orderTotal"/></td> </tr>
   	<tr><td width="20%">Mall Plaza Name</td><td width="20%"><dsp:valueof param="payLoadItems.mallPlazaName"/></td> </tr>
	
   	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
   	    <dsp:param name="array" param="payLoadItems.orderLines" />
   	    <dsp:setvalue param="currentLine" paramvalue="element"/>
   	    <dsp:oparam name="outputStart">
   	        <table>
   	    </dsp:oparam>
   	    <dsp:oparam name="output">
   	        <tr><td width="20%">Style Name</td><td width="20%"><dsp:valueof param="currentLine.itemStyle"/></td> </tr>
			<tr><td width="20%">Style ID</td><td width="20%"><dsp:valueof param="currentLine.itemStyle"/></td> </tr>
			<tr><td width="20%">Color</td><td width="20%"><dsp:valueof param="currentLine.itemColor"/></td> </tr>
			<tr><td width="20%">Size/Dim</td><td width="20%"><dsp:valueof param="currentLine.itemSize"/></td> </tr>
			<tr><td width="20%">Quantity</td><td width="20%"><dsp:valueof param="currentLine.itemQuantity"/></td> </tr>
			<tr><td width="20%">Product Id</td><td width="20%"><dsp:valueof param="currentLine.itemId"/></td> </tr>
			<tr><td width="20%">Color Code</td><td width="20%"><dsp:valueof param="currentLine.colorCode"/></td> </tr>
			<tr><td width="20%">Brand</td><td width="20%"><dsp:valueof param="currentLine.itemBrand"/></td> </tr>
			<tr><td width="20%">Product Name</td><td width="20%"><dsp:valueof param="currentLine.itemName"/></td> </tr>
			<tr><td width="20%">Product Id</td><td width="20%"><dsp:valueof param="currentLine.itemId"/></td> </tr>
			<tr><td width="20%">Item UPC</td><td width="20%"><dsp:valueof param="currentLine.itemUPC"/></td> </tr>
			<tr><td width="20%">Lin Discounts</td><td width="20%"><dsp:valueof param="currentLine.lineDiscounts"/></td> 
			<tr><td width="20%">Item Ship Type</td><td width="20%"><dsp:valueof param="currentLine.itemShipType"/></td> </tr>
			<tr><td width="20%">Item Shipping Method</td><td width="20%"><dsp:valueof param="currentLine.itemShippingMethod"/></td> </tr>
			<tr><td width="20%">Item Status</td><td width="20%"><dsp:valueof param="currentLine.status"/></td> </tr>
	    </dsp:oparam>
   	    <dsp:oparam name="outputEnd">
   	        </table>
   	    </dsp:oparam>
   	</dsp:droplet>
  </table>
  <br/>
   <table border="1">
	   <tr><td width="20%">Shipping Address</td> </tr>
	     <dsp:getvalueof var="companyVar" param="payLoadItems.shippingAddress.company">
    	    <c:if test="${not empty companyVar}">
    		   <tr><td width="20%">Company : </td><td width="20%"><dsp:valueof param="payLoadItems.company"/></td> </tr>
    	    </c:if>
        </dsp:getvalueof>
	  	<tr><td width="20%">Address 1 </td><td width="10%"><dsp:valueof param="payLoadItems.addressLine1"/></td> </tr>
	  	<tr><td width="20%">Address 2 </td><td width="10%"><dsp:valueof param="payLoadItems.addressLine2"/></td> </tr>
	  	<tr><td width="20%">City</td><td width="10%"><dsp:valueof param="payLoadItems.city"/></td> </tr>
	  	<tr><td width="20%">State</td><td width="10%"><dsp:valueof param="payLoadItems.state"/></td> </tr>
	  	<tr><td width="20%">Zip Code</td><td width="10%"><dsp:valueof param="payLoadItems.zipCode"/></td> </tr>
	  	<tr><td width="20%">Phone Number</td><td width="10%"><dsp:valueof param="payLoadItems.dayPhone"/></td> </tr>
	  	<tr><td width="20%">Country</td><td width="10%"><dsp:valueof param="payLoadItems.country"/></td> </tr>
	  	<tr><td width="20%">Company</td><td width="10%"><dsp:valueof param="payLoadItems.company"/></td> </tr>
   </table>  
</dsp:getvalueof>

<%@include file="../../order/oms/common/inc_other_items_ordered.jsp"%>

<%@include file="../../order/oms/common/inc_confirmation_order_details.jsp"%>

</body>
</html>
</dsp:page>