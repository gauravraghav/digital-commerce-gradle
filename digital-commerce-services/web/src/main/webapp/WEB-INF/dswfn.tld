<?xml version="1.0" encoding="UTF-8" ?>

<taglib xmlns="http://java.sun.com/xml/ns/j2ee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
	version="2.0">

	<description>Miscellaneous extra JSTL functions</description>
	<display-name>DSW JSTL functions</display-name>
	<tlib-version>1.1</tlib-version>
	<short-name>dswfn</short-name>
	<uri>http://www.dsw.com/jsp/jstl/functions</uri>

    <function>
        <description>
           Merges two maps  (the original is left alone)
        </description>
        <name>mergedMap</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.util.Map mergedMap(java.util.Map,java.util.Map)</function-signature>
        <example>
            &lt;c:set var="newmap" value="${dswfn:mergedMap(map1, map2)}">
        </example>
    </function>

    <function>
        <description>
           Merges an object (could be a collection, array, single object) into another collection   (the original is left alone)
        </description>
        <name>mergedCollection</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.util.Collection mergedCollection(java.util.Collection,java.lang.Object)</function-signature>
        <example>
            &lt;c:set var="newcollection" value="${dswfn:mergedCollection(collection, object)}">
        </example>
    </function>

    <function>
        <description>
           Reverses a collection  (the original is left alone)
        </description>
        <name>reverse</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.util.List reverse(java.util.Collection)</function-signature>
        <example>
            &lt;c:set var="list" value="${dswfn:reverse(collection)}">
        </example>
    </function>

	<function>
		<description>
           Returns a sub view of the collection (the original is left alone)
        </description>
		<name>subCollection</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>java.util.Collection subCollection(java.util.Collection,int,int)</function-signature>
		<example>
      		&lt;c:set var="list" value="${dswfn:subCollection(collection, 0, fn:length(collection) - 1)}">
    	</example>
	</function>

    <function>
        <description>
           Detects if an object contains an object.  Works on iterables, iterators, arrays, and strings
        </description>
        <name>contains</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>boolean contains(java.lang.Object,java.lang.Object)</function-signature>
        <example>
            &lt;c:if test="${dswfn:contains(object, element)}">
        </example>
    </function>

    <function>
        <description>
           Joins an object (could be a collection, array, single object) into a single string
        </description>
        <name>join</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.lang.String join(java.lang.Object,java.lang.String)</function-signature>
        <example>
            &lt;c:set var="string" value="${dswfn:join(object, ',')}">
        </example>
    </function>
    
    <function>
        <description>
           Joins an object (could be a collection, array, single object) into a single xml escaped string
        </description>
        <name>joinXml</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.lang.String joinXml(java.lang.Object,java.lang.String)</function-signature>
        <example>
            &lt;c:set var="string" value="${dswfn:joinXml(object, ',')}">
        </example>
    </function>
    
    <function>
        <description>
           Joins an object (could be a collection, array, single object) into a single json escaped string
        </description>
        <name>joinJson</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.lang.String joinJson(java.lang.Object,java.lang.String)</function-signature>
        <example>
            &lt;c:set var="string" value="${dswfn:joinJson(object, ',')}">
        </example>
    </function>

    <function>
        <description>
          Tests if an input object matches the supplied regex. If the input object is iterable, applies the regex to each element of the iterable and if any of them satisfy the reg, returns true.
        </description>
        <name>matches</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>boolean matches(java.lang.Object,java.lang.String)</function-signature>
        <example>
            &lt;c:if test="${dswfn:matches(object, regex)}">
        </example>
    </function>

    <function>
        <description>
          Replaces each substring in string that matches the regular expression
        </description>
        <name>replaceAll</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.lang.String replaceAll(java.lang.String, java.lang.String,java.lang.String)</function-signature>
        <example>
            &lt;c:if test="${dswfn:replaceAll(string, regex, replacement)}">
        </example>
    </function>

	<function>
		<description>
	      Formats a credit card number according to DSW styles
	    </description>
		<name>getMaskedCreditCardNumber</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>String getMaskedCreditCardNumber(java.lang.String)</function-signature>
		<example>
	      &lt;c:out value="${dswfn:getMaskedCreditCardNumber(paymentGroup.creditCardNumber)}">
	    </example>
	</function>
	
	<function>
		<description>
	      Returns last 4 digit of credit card number
	    </description>
		<name>getCreditCardNumberLast4</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>String getCreditCardNumberLast4(java.lang.String)</function-signature>
		<example>
	      &lt;c:out value="${getCreditCardNumberLast4(paymentGroup.creditCardNumber)}">
	    </example>
	</function>
	
	<function>
		<description>
	      Returns SEO friendly product display name
	    </description>
		<name>getSEOProductDisplayName</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>String getSEOProductDisplayName(java.lang.String)</function-signature>
		<example>
	      &lt;c:out value="${getSEOProductDisplayName(string)}">
	    </example>
	</function>

	<function>
		<description>
	      Returns last 4 digit of gift card number
	    </description>
		<name>getGiftCardNumberLast4</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>String getGiftCardNumberLast4(java.lang.String)</function-signature>
		<example>
	      &lt;c:out value="${getGiftCardNumberLast4(paymentGroup.GiftCardNumber)}">
	    </example>
	</function>

	<function>
		<description>
			Validates if the provided expiration month / year combination is past the current month / year.
		</description>
		<name>isCreditCardExpired</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>boolean isCreditCardExpired(java.lang.String, java.lang.String)</function-signature>
		<example>
	      &lt;c:out value="${dswfn:isCreditCardExpired(creditCard.expirationMonth, creditCard.expirationYear)}">
		</example>
	</function>

	<function>
		<description>
	      Formats a phone number according to DSW styles
	    </description>
		<name>getFormattedPhoneNumber</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>Object getFormattedPhoneNumber(java.lang.String)</function-signature>
		<example>
	      &lt;c:out value="${dswfn:getFormattedPhoneNumber(address.phoneNumber)}">
	    </example>
	</function>

	<function>
		<description>
          Returns a property from a repository item.  Necessary because EL cannot reference repository
          item's properties using dot notation the way the DSP tag library can.
        </description>
		<name>getPropertyValue</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>Object getPropertyValue(java.lang.Object, java.lang.String)</function-signature>
		<example>
      		&lt;c:out value="${dswfn:getPropertyValue(product, "displayName")}">
    	</example>
	</function>

    <function>
        <description>
          Returns a property from a repository item.  Necessary because EL cannot reference repository
          item's properties using dot notation the way the DSP tag library can.
        </description>
        <name>value</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>Object value(Object, java.lang.String)</function-signature>
        <example>
            &lt;c:out value="${dswfn:value(object, "dot.expression")}">
        </example>
    </function>

	<function>
		<description>
      		Cleans value of any illegal characters. See code for details.
    	</description>
		<name>sanitize</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>String sanitize(java.lang.String)</function-signature>
		<example>
      		&lt;c:out value="${dswfn:sanitize(param.requestParameter)}">
    	</example>
	</function>

	<function>
		<description>
      		Encodes the provided string with the character encoding.
    	</description>
		<name>encodeURL</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>java.lang.String encodeURL(java.lang.String,java.lang.String)</function-signature>
		<example>
      		&lt;c:out value="${dswfn:encodeURL(pageContext.request.queryString, pageContext.response.characterEncoding)}">
    	</example>
	</function>

    <function>
        <description>
            Decodes the provided string with the character encoding.
        </description>
        <name>decodeURL</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.lang.String decodeURL(java.lang.String,java.lang.String)</function-signature>
        <example>
            &lt;c:out value="${dswfn:decodeURL(pageContext.request.queryString, pageContext.response.characterEncoding)}">
        </example>
    </function>


    <function>
        <description>
            Escapes the provided string for JSON by replacing single/double quotes with escaped equivalents
        </description>
        <name>escapeJson</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.lang.String escapeJson(java.lang.String)</function-signature>
        <example>
            &lt;c:out value="${escapeJson(pageContext.request.queryString)}">
        </example>
    </function>

    <function>
        <description>
            Serializes an object graph into a character escaped JSON string
        </description>
        <name>toJson</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.lang.String toJson(java.lang.Object, boolean)</function-signature>
        <example>
            &lt;c:out value="${toJson(objectToSerialize, false)}"&gt;
        </example>
    </function>

    <function>
        <description>
            Unescapes the provided html string so that entities are replaced by the java characters.
        </description>
        <name>unescapeHtml</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.lang.String unescapeHtml(java.lang.String)</function-signature>
        <example>
            &lt;c:out value="${unescapeXml(pageContext.request.queryString)}">
        </example>
    </function>

  <function>
    <description>
      Checks an atg errorMap (DSWUserErrorList) to see if a given error exists.  Useful for highlighting fields that
      contain an error.
    </description>
    <name>errorMapContainsErrorCode</name>
    <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
    <function-signature>boolean errorMapContainsErrorCode (com.dsw.edt.profile.DSWUserErrorList, java.lang.String)</function-signature>
    <example>
      &lt;c:if test="${dswfn:errorMapContainsErrorCode(errorMap, 'firstName')}">
    </example>
  </function>

  <function>
    <description>
     Tests if an object is a Number
    </description>
    <name>isNumber</name>
    <function-class>com.dsw.web.taglib.NumberFunctions</function-class>
    <function-signature>boolean isNumber (java.lang.Object)</function-signature>
    <example>
      &lt;c:out value="${dswfn:isNumber("1.1")}">
    </example>
  </function>
  
  <function>
    <description>
     Converts an object to an Integer
    </description>
    <name>toInt</name>
    <function-class>com.dsw.web.taglib.NumberFunctions</function-class>
    <function-signature>java.lang.Integer toInt (java.lang.Object)</function-signature>
    <example>
      &lt;c:out value="${dswfn:toInt("1")}">
    </example>
  </function>

  <function>
    <description>
     Converts an object to a Double
    </description>
    <name>toDouble</name>
    <function-class>com.dsw.web.taglib.NumberFunctions</function-class>
    <function-signature>java.lang.Double toDouble (java.lang.Object)</function-signature>
    <example>
      &lt;c:out value="${dswfn:toDouble("1.1")}">
    </example>
  </function>
  
  <function>
    <description>
     Returns the Math.ceil of an object.
    </description>
    <name>ceil</name>
    <function-class>com.dsw.web.taglib.NumberFunctions</function-class>
    <function-signature>java.lang.Integer ceil(java.lang.Object)</function-signature>
    <example>
      &lt;c:out value="${dswfn:ceil("1.2")}">
    </example>
  </function>

  <function>
    <description>
     Returns the Math.floor of an object.
    </description>
    <name>floor</name>
    <function-class>com.dsw.web.taglib.NumberFunctions</function-class>
    <function-signature>java.lang.Integer floor (java.lang.Object)</function-signature>
    <example>
      &lt;c:out value="${dswfn:floor("1.2")}">
    </example>
  </function>

  <function>
    <description>
     Returns the brand url.
    </description>
    <name>brandUrl</name>
    <function-class>com.dsw.edt.util.DSWSEOUtil</function-class>
    <function-signature>java.lang.String getBrandURL(java.lang.String,java.lang.String,java.lang.String,int,java.lang.String)</function-signature>
    <example>
      &lt;c:out value="${dswfn:brandUrl("BRAND", "BRANDID", "SECTION", 1, "-price")}">
    </example>
  </function>

  <function>
    <description>
     Returns the category url.
    </description>
    <name>categoryUrl</name>
    <function-class>com.dsw.edt.util.DSWSEOUtil</function-class>
    <function-signature>java.lang.String getCategoryURL(int, java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object, java.lang.String)</function-signature>
    <example>
      &lt;c:out value="${dswfn:categoryUrl(1, "women's shoes'", "boot shop", null, null, "-price")}">
    </example>
  </function>

  <function>
  	<description>
  		Returns the category url.
  	</description>
    <name>categoryUrlUsingName</name>
    <function-class>com.dsw.edt.util.DSWSEOUtil</function-class>
    <function-signature>java.lang.String getCategoryURL(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)</function-signature>
    <example>
      &lt;c:out value="${dswfn:categoryUrlUsingName(1, "women's shoes'", "boot shop", null, null, null, "-price")}">
    </example>
  </function>

  <function>
    <description>
     Returns the default category url for the given page number
    </description>
    <name>defaultCategoryUrl</name>
    <function-class>com.dsw.edt.util.DSWSEOUtil</function-class>
    <function-signature>java.lang.String getCategoryURL(java.lang.Object, int)</function-signature>
    <example>
      &lt;c:out value="${dswfn:defaultCategoryUrl("women's shoes'", 1)}">
    </example>
  </function>

  <function>
    <description>
     Returns the product url given various details about the product (id, type, name, category, brand).
    </description>
    <name>productUrlFromDetails</name>
    <function-class>com.dsw.edt.util.DSWSEOUtil</function-class>
    <function-signature>java.lang.String getProductURL(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)</function-signature>
    <example>
      &lt;c:out value="${dswfn:productUrlFromDetails("1234", "kids shoe", "awesome shoe", "cat123", "")}">
    </example>
  </function>

  <function>
    <description>
     Returns the product url.
    </description>
    <name>productUrl</name>
    <function-class>com.dsw.edt.util.DSWSEOUtil</function-class>
    <function-signature>java.lang.String getProductURL(java.lang.Object, java.lang.String, java.lang.String)</function-signature>
    <example>
      &lt;c:out value="${dswfn:productUrl(product, "cat123", "")}">
    </example>
  </function>

  <function>
    <description>
     Returns the default product url without a category or brand
    </description>
    <name>defaultProductUrl</name>
    <function-class>com.dsw.edt.util.DSWSEOUtil</function-class>
    <function-signature>java.lang.String getProductURL(java.lang.Object)</function-signature>
    <example>
      &lt;c:out value="${dswfn:defaultProductUrl(product)}">
    </example>
  </function>

  <function>
    <description>
     Returns a proper root category (can be expensive)
    </description>
    <name>findRootCategory</name>
    <function-class>com.dsw.edt.util.CategoryUtil</function-class>
    <function-signature>atg.repository.RepositoryItem findRootCategory(java.lang.Object)</function-signature>
    <example>
      &lt;c:out value="${dswfn:findRootCategory(product)}">
    </example>
  </function>
  <function>
    <description>
     Returns a proper parent category (can be expensive)
    </description>
    <name>findParentCategory</name>
    <function-class>com.dsw.edt.util.CategoryUtil</function-class>
    <function-signature>atg.repository.RepositoryItem findParentCategory(java.lang.Object)</function-signature>
    <example>
      &lt;c:out value="${dswfn:findParentCategory(product)}">
    </example>
  </function>
  <function>
    <description>
     Returns a proper child category
    </description>
    <name>findChildCategory</name>
    <function-class>com.dsw.edt.util.CategoryUtil</function-class>
    <function-signature>atg.repository.RepositoryItem findChildCategory(java.lang.Object)</function-signature>
    <example>
      &lt;c:out value="${dswfn:findChildCategory(category)}">
    </example>
  </function>
  <function>
    <description>
     Returns whether an item is a parent of another item
    </description>
    <name>getChildren</name>
    <function-class>com.dsw.edt.util.CategoryUtil</function-class>
    <function-signature>java.util.List getChildren(atg.repository.RepositoryItem)</function-signature>
    <example>
      &lt;c:out value="${dswfn:getChildren(product)}">
    </example>
  </function>
  <function>
    <description>
     Returns whether the first argument is a parent of the second
    </description>
    <name>isParent</name>
    <function-class>com.dsw.edt.util.CategoryUtil</function-class>
    <function-signature>boolean isParent(atg.repository.RepositoryItem, atg.repository.RepositoryItem)</function-signature>
    <example>
      &lt;c:out value="${dswfn:isParent(category, product)}">
    </example>
  </function>

<function>
    <description>
     Returns a cookied parameter and initializes it if necessary
    </description>
    <name>getCookiedParameter</name>
    <function-class>com.dsw.edt.util.DSWCookieUtil</function-class>
    <function-signature>String getCookiedParameter(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.String, int)</function-signature>
    <example>
      &lt;c:out value="${dswfn:getCookiedParameter(pageContext.request, pageContext.response, "showAllColors", 365 * 24 * 60 * 60)}">
    </example>
  </function>

	<function>
	    <description>
	     	Converts a string to upper case.  Will not break escaped XML sequences.
	    </description>
	    <name>toUpperCaseXML</name>
	    <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
	    <function-signature>String toUpperCaseXML(java.lang.String)</function-signature>
	    <example>
	      &lt;c:out value="${dswfn:toUpperCaseXML(someXMLContent)}">
	    </example>
  </function>

	<function>
		<description>
	      Formats a gift card number according to DSW styles
	    </description>
		<name>getMaskedGiftCardNumber</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>String getMaskedGiftCardNumber(java.lang.String)</function-signature>
		<example>
	      &lt;c:out value="${dswfn:getMaskedGiftCardNumber(giftCardNumber)}">
	    </example>
	</function>

	<function>
		<description>
	      Returns the current date/time as a java.util.Date
	    </description>
		<name>getCurrentDate</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>java.util.Date getCurrentDate()</function-signature>
		<example>
	      &lt;c:out value="${dswfn:getCurrentDate()}">
	    </example>
	</function>

	<function>
		<description>
			Returns the mobile hostname equivalent for a full site hostname
		</description>
		<name>getMobileHostname</name>
    	<function-class>com.dsw.edt.util.DSWSEOUtil</function-class>
		<function-signature>java.lang.String getMobileHostname(java.lang.String)</function-signature>
		<example>
			%lt;c:out value="${dswfn:getMobileHostname(hostname)}">
		</example>
	</function>

	<function>
		<description>
			Returns the mobile path for a given product
		</description>
		<name>getMobileProductUri</name>
    	<function-class>com.dsw.edt.util.DSWSEOUtil</function-class>
		<function-signature>java.lang.String getMobileProductUri(java.lang.Object)</function-signature>
		<example>
			%lt;c:out value="${dswfn:getMobileProductUri(product)}">
		</example>
	</function>

	<function>
		<description>
			Returns the number of times a substring appears withing a string
		</description>
		<name>countMatches</name>
    	<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>int countMatches(java.lang.String, java.lang.String)</function-signature>
		<example>
			%lt;c:out value="${dswfn:countMatches(string, substring)}">
		</example>
	</function>

	<function>
		<description>
			Determines whether or not the object passed in is an instance of the class
			represented by the specified class name.
		</description>
		<name>instanceOf</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>boolean instanceOf(java.lang.Object, java.lang.String)</function-signature>
		<example>
			%lt;c:if test="${dswfn:instanceOf(myVar, "java.lang.String")}">
		</example>
	</function>

  <function>
    <description>
     Returns the encoded string for a given input string.
    </description>
    <name>transformStringMobileToFullSiteFormat</name>
    <function-class>com.dsw.edt.util.DSWSEOUtil</function-class>
    <function-signature>java.lang.String transformStringMobileToFullSiteFormat(java.lang.String)</function-signature>
    <example>
      &lt;c:out value="${dswfn:transformStringMobileToFullSiteFormat("women's shoes'")}">
    </example>
  </function>

	<function>
		<description>
      		Does unescapeXML and encodes with the character encoding.
    	</description>
		<name>encodeString</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>java.lang.String encodeString(java.lang.String,java.lang.String)</function-signature>
		<example>
      		&lt;c:out value="${dswfn:encodeString(pageContext.request.queryString, pageContext.response.characterEncoding)}">
    	</example>
	</function>
	
	<function>
		<description>
			Allow capitalization of a given input string.
		</description>
		<name>capitalize</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>java.lang.String capitalize(java.lang.String)</function-signature>
		<example>
			&lt;c:out value="${dswfn:capitalize('women's shoes')}"&gt;
		</example>
	</function>

	<function>
		<description>
			Converts all the whitespace separated words in a String into capitalized words, that is each word is made up of a uppercase character and then a series of lowercase characters.
		</description>
		<name>capitalizeFully</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>java.lang.String capitalizeFully(java.lang.String)</function-signature>
		<example>
			&lt;c:out value="${dswfn:capitalizeFully('women's shoes')}"&gt;
		</example>
	</function>
	
	<function>
		<description>
			Returns the current system time in millis.
		</description>
		<name>currentTimeMillis</name>
		<function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
		<function-signature>long currentTimeMillis()</function-signature>
		<example>
			%lt;c:out value="${dswfn:currentTimeMillis}"&gt;
		</example>
	</function>

    <function>
        <description>
            Returns the value in a map in relation to the key that is passed.
        </description>
        <name>getValueFromMap</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.lang.String getValueFromMap(java.util.Map, java.lang.Object)</function-signature>
        <example>
            %lt;c:out value="${dswfn:currentTimeMillis}"&gt;
        </example>
    </function>

    <function>
        <description>
            Returns the DOUBLE number formatted up to two decimal places.
        </description>
        <name>getFormattedDoubleValue</name>
        <function-class>com.dsw.commerce.common.taglib.ELFunctions</function-class>
        <function-signature>java.lang.String getFormattedDoubleValue(java.lang.Double)</function-signature>
        <example>
            %lt;c:out value="${dswfn:getFormattedDoubleValue}"&gt;
        </example>
    </function>
</taglib>

