/**
 *
 */
package com.digital.commerce.repository;

import java.util.Iterator;
import java.util.List;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

@SuppressWarnings({"unchecked"})
public class LuxuryProductPropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
     *
     */
	private static final long		serialVersionUID	= -6258342473415967032L;

	protected static final String	TYPE_NAME			= "LuxuryProductPropertyDescriptor";

	protected static final String	CHILD_SKUS			= "childSKUs";

	protected static final String	LUXURY				= "isLuxury";
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, LuxuryProductPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		if( pValue != null && pValue instanceof Boolean ) { return pValue; }

		List<RepositoryItem> skus = (List<RepositoryItem>)pItem.getPropertyValue( CHILD_SKUS );

		if( skus != null ) {
			Iterator<RepositoryItem> i = skus.iterator();
			RepositoryItem sku = null;
			while( i.hasNext() ) {
				sku = i.next();
				Boolean luxury = (Boolean)sku.getPropertyValue( LUXURY );
				if( Boolean.TRUE.equals( luxury ) ) {
					setPropertyInCache( pItem, Boolean.TRUE );

					return Boolean.TRUE;
				}
			}
			setPropertyInCache( pItem, Boolean.FALSE );

		}
		return Boolean.FALSE;
	}

	private void setPropertyInCache( RepositoryItemImpl item, Boolean flag ) {
		item.setPropertyValueInCache( this, flag );
	}
}
