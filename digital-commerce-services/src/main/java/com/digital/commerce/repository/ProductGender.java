package com.digital.commerce.repository;

import java.util.Arrays;
import java.util.Iterator;

import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.google.common.collect.Iterables;

public enum ProductGender {
	Female, Male, Unisex, Girl, Boy, Kid;

	public static ProductGender valueFor( final String value ) {
		final Iterator<ProductGender> filtered = Iterables.filter( Arrays.asList( values() ), new DigitalPredicate<ProductGender>() {
			@Override
			public boolean apply( ProductGender input ) {
				return DigitalStringUtil.equalsIgnoreCase( DigitalStringUtil.trimToEmpty( input.name() ), DigitalStringUtil.trimToEmpty( value ) );
			}

		} ).iterator();
		return filtered.hasNext() ? filtered.next() : null;
	}
}
