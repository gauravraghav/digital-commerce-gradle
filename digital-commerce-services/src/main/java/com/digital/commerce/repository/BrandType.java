package com.digital.commerce.repository;

import java.util.Arrays;
import java.util.Iterator;

import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.google.common.collect.Iterables;
import lombok.Getter;

@Getter
public enum BrandType {
	Unknown("UNKNOWN"), Shoe("shoe"), Handbag("handbag"), Accessories("accessories"), Other("other"), LuxuryShoe("luxury-shoe"), LuxuryHandbag("luxury-handbag"), LuxuryAccessories("luxury-accessories"), LuxuryOther("luxury-other");

	private final String	value;

	private BrandType( final String value ) {
		this.value = value;
	}


	public static BrandType valueFor( final String value ) {
		final Iterator<BrandType> filtered = Iterables.filter( Arrays.asList( values() ), new DigitalPredicate<BrandType>() {
			@Override
			public boolean apply( BrandType input ) {
				return DigitalStringUtil.equalsIgnoreCase( DigitalStringUtil.trimToEmpty( input.getValue() ), DigitalStringUtil.trimToEmpty( value ) );
			}

		} ).iterator();
		return filtered.hasNext() ? filtered.next() : Unknown;
	}
}
