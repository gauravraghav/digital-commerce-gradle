package com.digital.commerce.repository;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.repository.MutableRepository;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.QueryOptions;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;
import atg.repository.RepositoryView;
import atg.service.cache.Cache;
import atg.servlet.ServletUtil;

/** @author psinha */
public class BrandTypePropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
	 *
	 */
	private static final long		serialVersionUID			= -6358342475415966132L;

	protected static final String	TYPE_NAME					= "BrandTypePropertyDescriptor";

	static final String				PRODUCT_TYPE_SHOE			= "shoe";
	static final String				PRODUCT_TYPE_HANDBAG		= "handbag";
	static final String				PRODUCT_TYPE_ACCESSORIES	= "accessories";
	static final String				PRODUCT_TYPE_KIDS_SHOES		= "kids shoes";

	private static final DigitalLogger		logger						= DigitalLogger.getLogger( BrandTypePropertyDescriptor.class );

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, BrandTypePropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		BrandType bType = BrandType.Unknown;
		try {
			String brandId = (String)pItem.getPropertyValue( "id" );
			Cache cache = (Cache)ServletUtil.getCurrentRequest().resolveName( "/com/digital/commerce/services/cache/BrandTypeCache" );
			
			if(cache.get(brandId) != null){
				setBrandTypeProperty( pItem, (String)cache.get(brandId) );
				return (String)cache.get(brandId);
			}
			
			RepositoryItem[] items = null;
			
			MutableRepository rep = (MutableRepository)ServletUtil.getCurrentRequest().resolveName( "/atg/commerce/catalog/ProductCatalog" );
						
			RepositoryView rv = rep.getView( "product" );
			
			QueryBuilder qb = rv.getQueryBuilder();
			
			Query[] q = new Query[2];
			
			QueryExpression expr1 = qb
					.createPropertyQueryExpression("isActive");
			
			QueryExpression expr2 = qb
						.createConstantQueryExpression(true);
			q[0] = qb.createComparisonQuery(expr1, expr2, QueryBuilder.EQUALS);
			
			expr1 = qb
					.createPropertyQueryExpression("dswBrand");
			
			expr2 = qb
						.createConstantQueryExpression(pItem);
			q[1] = qb.createComparisonQuery(expr1, expr2, QueryBuilder.EQUALS);
		    Query query = qb.createAndQuery(q);
		    
			items = rv.executeQuery(query, new QueryOptions(0, 1, null,
		    		null));
			
			if( items != null && items.length > 0 ) {
				RepositoryItem firstProduct = items[0];
				String productType = (String)firstProduct.getPropertyValue( "productTypeInUse" );
				Boolean isLuxury = (Boolean)firstProduct.getPropertyValue( "isLuxury" );
				if( isLuxury.booleanValue() ) {
					if( PRODUCT_TYPE_SHOE.equalsIgnoreCase( productType ) || PRODUCT_TYPE_KIDS_SHOES.equalsIgnoreCase( productType ) ) {
						bType = BrandType.LuxuryShoe;
					} else if( PRODUCT_TYPE_HANDBAG.equalsIgnoreCase( productType ) ) {
						bType = BrandType.LuxuryHandbag;
					} else if( PRODUCT_TYPE_ACCESSORIES.equalsIgnoreCase( productType ) ) {
						bType = BrandType.LuxuryAccessories;
					} else {
						bType = BrandType.LuxuryOther;
					}
				} else {
					if( PRODUCT_TYPE_SHOE.equalsIgnoreCase( productType ) || PRODUCT_TYPE_KIDS_SHOES.equalsIgnoreCase( productType ) ) {
						bType = BrandType.Shoe;
					} else if( PRODUCT_TYPE_HANDBAG.equalsIgnoreCase( productType ) ) {
						bType = BrandType.Handbag;
					} else if( PRODUCT_TYPE_ACCESSORIES.equalsIgnoreCase( productType ) ) {
						bType = BrandType.Accessories;
					} else {
						bType = BrandType.Other;
					}
				}
			}
			setBrandTypeProperty( pItem, bType.getValue() );
			cache.put(brandId, bType.getValue());
		} catch( Exception e ) {
			logger.error( "BrandType Property Descriptor : " + e );
		}
		return bType.getValue();
	}

	private void setBrandTypeProperty( RepositoryItemImpl item, String bType ) {
		item.setPropertyValueInCache( this, bType );
	}
}
