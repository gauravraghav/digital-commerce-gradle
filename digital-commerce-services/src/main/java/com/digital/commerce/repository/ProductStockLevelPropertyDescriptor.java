package com.digital.commerce.repository;

import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalStringUtil;

import static com.digital.commerce.common.util.ComponentLookupUtil.DSW_BASE_CONSTANTS;

public class ProductStockLevelPropertyDescriptor extends RepositoryPropertyDescriptor {

    private static final long		serialVersionUID	= -2795816089069305900L;
    private static final String TYPE_NAME = "ProductStockLevelPropertyDescriptor";
    private static final String OVERALL_STOCK_LEVEL = "overallStockLevel";
    private static final String INFINITE_STOCK_LEVEL = "99999";
    private static final String ID = "id";

    static {
        RepositoryPropertyDescriptor.registerPropertyDescriptorClass(TYPE_NAME, ProductStockLevelPropertyDescriptor.class);
    }

    /**
     * @param pItem
     * @param pValue
     * @return Object - overall product stock level
     */
    public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {

        Long productStockLevel = 0L;
        DigitalBaseConstants DigitalBaseConstants = (DigitalBaseConstants) ComponentLookupUtil.lookupComponent(DSW_BASE_CONSTANTS);
        // pValue will get the data from Cache. Check if stock level has to come from Cache
        if (null != DigitalBaseConstants) {
            if (DigitalBaseConstants.isProductSkuStockCacheEnabled()) {
                if (pValue != null && pValue instanceof Long) {
                    return pValue;
                }
            }

            String productId = (String) pItem.getPropertyValue(ID);

            if (DigitalStringUtil.isEmpty(productId)) {
                return productStockLevel;
            }

            String[] productSkuGiftCardIdList = DigitalBaseConstants.getProductSkuGiftCardIdList();

            if (null != productSkuGiftCardIdList && productSkuGiftCardIdList.length > 0) {
                for (String productSkuGiftCardId : productSkuGiftCardIdList) {
                    if (productSkuGiftCardId.equalsIgnoreCase(productId)) {
                        pItem.setPropertyValueInCache(this, INFINITE_STOCK_LEVEL);
                        return INFINITE_STOCK_LEVEL;
                    }
                }
            }
        }

        Object overallStockLevel = pItem.getPropertyValue(OVERALL_STOCK_LEVEL);
        if (null != overallStockLevel && overallStockLevel instanceof String) {
            productStockLevel = Long.valueOf((String) overallStockLevel);
            pItem.setPropertyValueInCache(this, productStockLevel);
        }
        return productStockLevel;
    }


    /**
     * @return String
     */
    public String getTypeName() {
        return TYPE_NAME;
    }

}
