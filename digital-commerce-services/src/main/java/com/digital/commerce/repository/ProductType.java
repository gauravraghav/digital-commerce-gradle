package com.digital.commerce.repository;

import java.util.Arrays;
import java.util.Iterator;

import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.google.common.collect.Iterables;
import lombok.Getter;

@Getter
public enum ProductType {
	Unknown("UNKNOWN"), Shoe("shoe"), Handbag("handbag"), Accessory("accessories"), KidsShoe("kids shoes"), Other("other");

	private final String	value;

	private ProductType( final String value ) {
		this.value = value;
	}

	public static ProductType valueFor( final String value ) {
		final Iterator<ProductType> filtered = Iterables.filter( Arrays.asList( values() ), new DigitalPredicate<ProductType>() {
			@Override
			public boolean apply( ProductType input ) {
				return DigitalStringUtil.equalsIgnoreCase( DigitalStringUtil.trimToEmpty( input.getValue() ), DigitalStringUtil.trimToEmpty( value ) );
			}

		} ).iterator();
		return filtered.hasNext() ? filtered.next() : Unknown;
	}
}
