/**
 *
 */
package com.digital.commerce.repository;

import java.util.Iterator;
import java.util.List;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

@SuppressWarnings({"rawtypes"})
public class ReturnableToStoreProductPropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
     *
     */
	private static final long		serialVersionUID	= -6258342473515967032L;

	protected static final String	TYPE_NAME			= "ReturnableToStoreProductPropertyDescriptor";

	protected static final String	CHILD_SKUS			= "childSKUs";

	protected static final String	RETURNABLE_TO_STORE	= "isReturnableToStore";
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, ReturnableToStoreProductPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		if( pValue != null && pValue instanceof Boolean ) { return pValue; }

		List skus = (List)pItem.getPropertyValue( CHILD_SKUS );

		if( skus != null ) {
			Iterator i = skus.iterator();
			RepositoryItem sku = null;
			while( i.hasNext() ) {
				sku = (RepositoryItem)i.next();
				Boolean returnableToStore = (Boolean)sku.getPropertyValue( RETURNABLE_TO_STORE );
				if( Boolean.FALSE.equals( returnableToStore ) ) {
					setPropertyInCache( pItem, Boolean.FALSE );

					return Boolean.FALSE;
				}
			}
			setPropertyInCache( pItem, Boolean.TRUE );

		}
		return Boolean.TRUE;
	}

	private void setPropertyInCache( RepositoryItemImpl item, Boolean flag ) {
		item.setPropertyValueInCache( this, flag );
	}
}
