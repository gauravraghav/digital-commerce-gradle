/**
 *
 */
package com.digital.commerce.repository;

import static com.digital.commerce.common.util.ComponentLookupUtil.PRICE_LIST_MANAGER;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.services.pricing.DigitalPriceListManager;
import com.digital.commerce.common.util.ComponentLookupUtil;

import atg.commerce.pricing.priceLists.PriceListException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;
import atg.servlet.ServletUtil;

@SuppressWarnings({"rawtypes"})
public class NonMemberPricePropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
     *
     */
	private static final long		serialVersionUID		= -6258342473415966032L;

	protected static final String	TYPE_NAME				= "NomMemberPricePropertyDescriptor";

	protected static final String	BASE_PRICE_LIST_NAME	= "DSW Base price list";

	protected static final String	SALE_PRICE_LIST_NAME	= "DSW Sale price list";

	protected static final String	BASE_PRICE_LIST			= "plist30003";

	protected static final String	SALE_PRICE_LIST			= "plist60003";

	private static final DigitalLogger	logger					= DigitalLogger.getLogger( NonMemberPricePropertyDescriptor.class );

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, NonMemberPricePropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {

		// if value already cached return fron the cache
		if( pValue != null && pValue instanceof Double ) { return pValue; }

		// Declare all required variable
		Double nonMemberPrice = null;
		Double doublePrice = null;
		RepositoryItem basePriceList = null;
		RepositoryItem salePriceList = null;
		RepositoryItem tempPriceList = null;

		// Get Base and Sale Price List
		DigitalPriceListManager pm = ComponentLookupUtil.lookupComponent( PRICE_LIST_MANAGER, DigitalPriceListManager.class );

		Collection plists = null;
		if( pm != null ) {
			try {
				RepositoryItem userProfileRepo = ServletUtil.getCurrentUserProfile();
				if(null != userProfileRepo){
					basePriceList = (RepositoryItem) userProfileRepo.getPropertyValue("priceList");
					salePriceList = (RepositoryItem) userProfileRepo.getPropertyValue("salePriceList");
				}
				
				if(salePriceList == null){
					salePriceList = pm.getPriceListForSite(pm.getSalePriceListPropertyName());
				}
				if(basePriceList == null){
					basePriceList = pm.getPriceListForSite(pm.getBasePriceListPropertyName());
				}
				
				if(salePriceList == null || basePriceList == null){
					plists = pm.getPriceLists();
					Iterator i = plists.iterator();
					while( i.hasNext() ) {
						tempPriceList = (RepositoryItem)i.next();
						String displayName = (String)tempPriceList.getPropertyValue( "displayName" );
						if( basePriceList == null && displayName.equalsIgnoreCase( BASE_PRICE_LIST_NAME ) ) {
							basePriceList = tempPriceList;
						}
						if( salePriceList == null && displayName.equalsIgnoreCase( SALE_PRICE_LIST_NAME ) ) {
							salePriceList = tempPriceList;
						}
					}
				}
				
			} catch( RepositoryException | PriceListException e ) {
				logger.error( "No Price Lists found : Repository or PriceListException Caught " +e.getMessage());

			}
		}

		// Get Parent Product
		RepositoryItem product = null;

		double lowList = Double.MAX_VALUE;
		double lowSale = Double.MAX_VALUE;

		Set parentProducts = (Set)pItem.getPropertyValue( "parentProducts" );
		if( parentProducts != null && !parentProducts.isEmpty() ){
			product = (RepositoryItem)parentProducts.toArray()[0];
		}

		// Get Lowest price
		if( product != null && pItem != null ) {
			try {

				// get the list price
				
				String pId=(String)product.getPropertyValue( "id" );
				String itemId=(String)pItem.getPropertyValue( "id" );

				if( basePriceList != null ) {
					doublePrice = pm.getPriceFromPriceList( basePriceList, pId, itemId);
					if( doublePrice != null ) {
						lowList = doublePrice.doubleValue();
					}
				}

				// get the sale price
				if( salePriceList != null ) {
					doublePrice = pm.getPriceFromPriceList( salePriceList, pId, itemId );
					if( doublePrice != null ) {
						lowSale = doublePrice.doubleValue();
					}
				}

				// set the absolute highest and lowest price
				double lowPrice = ( lowSale < lowList ? lowSale : lowList );

				if( lowPrice == Double.MAX_VALUE ) {
					nonMemberPrice = null;
				} else {
					nonMemberPrice = Double.valueOf( lowPrice );
				}
				setPriceProperty( pItem, nonMemberPrice );
			} catch( Exception e ) {
				logger.error( "Price list exception caught: ", e );
			}
		}
		return nonMemberPrice;
	}

	private void setPriceProperty( RepositoryItemImpl item, Double price ) {
		item.setPropertyValueInCache( this, price );
	}
}
