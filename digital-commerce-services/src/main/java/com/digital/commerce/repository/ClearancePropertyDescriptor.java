package com.digital.commerce.repository;

import java.util.Iterator;
import java.util.List;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

@SuppressWarnings({"rawtypes"})
public class ClearancePropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
     *
     */
	private static final long serialVersionUID = -6258342473415966032L;

	protected static final String TYPE_NAME = "clearancePropertyDescriptor";

	protected static final String CHILD_SKUS = "childSKUs";

	protected static final String CLEARANCE = "isClearanceItem";

	protected static final String SKU_IN_STOCK = "skuInStock";

	protected static final String IN_STOCK = "Y";
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass(TYPE_NAME,
				ClearancePropertyDescriptor.class);
	}

	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		if (pValue != null && pValue instanceof Boolean) {
			return pValue;
		}

		List skus = (List) pItem.getPropertyValue(CHILD_SKUS);

		if (skus != null) {
			Iterator i = skus.iterator();
			RepositoryItem sku = null;
			while (i.hasNext()) {
				sku = (RepositoryItem) i.next();
				Boolean clearance = (Boolean) sku.getPropertyValue(CLEARANCE);
				Object instock = sku.getPropertyValue(SKU_IN_STOCK);

				if (Boolean.TRUE.equals(clearance) && instock != null
						&& IN_STOCK.equals(instock.toString().toUpperCase())) {
					setClearanceProperty(pItem, Boolean.TRUE);

					return Boolean.TRUE;
				}
			}
			setClearanceProperty(pItem, Boolean.FALSE);

		}
		return Boolean.FALSE;
	}

	private void setClearanceProperty(RepositoryItemImpl item, Boolean flag) {
		// this.setPropertyValue(item, flag);
		item.setPropertyValueInCache(this, flag);
	}
}
