/**
 *
 */
package com.digital.commerce.repository;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.digital.commerce.common.services.inventory.InventoryHelper;
import com.digital.commerce.services.inventory.InventoryTools;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;


/** This is used to determine whether or not size/width property should be shown.
 * 
 * 
 * 
 * @author knaas */
@SuppressWarnings({"unchecked"})
public class ShowSizeOrWidthPropertyDescriptor extends RepositoryPropertyDescriptor {

	private static final long				serialVersionUID	= -5284084973522258764L;

	protected static final String			TYPE_NAME			= ShowSizeOrWidthPropertyDescriptor.class.getSimpleName();

	/** don't serialize it. */
	private transient InventoryHelper		inventoryTools;

	/** don't serialize it. */
	private transient Collection<String>	allowedPropertyValues;

	private String							skuPropertyName;

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, ShowSizeOrWidthPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl item, Object value ) {
		if( value != null && value instanceof Boolean ) { return value; }

		Boolean show = Boolean.TRUE;
		ProductType productType = ProductType.valueFor( (String)item.getPropertyValue( "productTypeInUse" ) );
		if( !ProductType.Shoe.equals( productType ) && !ProductType.KidsShoe.equals( productType ) ) {
			show = Boolean.TRUE.equals( item.getPropertyValue( "alwaysShowSizeAndWidth" ) );
			if( !show && inventoryTools != null ) {
				Set<String> propertyValues = new HashSet<>();
				Collection<RepositoryItem> skus = (Collection<RepositoryItem>)item.getPropertyValue( "childSKUs" );
				for( RepositoryItem sku : skus ) {
					RepositoryItem property = (RepositoryItem)sku.getPropertyValue( skuPropertyName );
					if( property != null ) {
						final String propertyValue = property.getItemDisplayName();
						if( !propertyValues.contains( propertyValue ) && inventoryTools.isAvailable( sku.getRepositoryId() ) ) {
							propertyValues.add( propertyValue );
						}
					}
				}
				show = propertyValues.size() > 1 || ( propertyValues.size() == 1 && allowedPropertyValues != null && !allowedPropertyValues.containsAll( propertyValues ) );
			}
		}
		item.setPropertyValueInCache( this, show );
		return show;
	}

	@Override
	public void setValue( String attributeName, Object value ) {
		super.setValue( attributeName, value );

		if( "inventoryTools".equals( attributeName ) && value instanceof InventoryTools ) {
			inventoryTools = (InventoryHelper)value;
		} else if( "skuPropertyName".equals( attributeName ) && value instanceof String ) {
			skuPropertyName = (String)value;
		} else if( "allowedPropertyValues".equals( attributeName ) && value instanceof String ) {
			allowedPropertyValues = Arrays.asList( ( (String)value ).split( "," ) );
		}

	}

}
