/**
 * 
 */
package com.digital.commerce.repository;

import java.util.Collection;
import java.util.Iterator;

import atg.adapter.gsa.GSAPropertyDescriptor;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

@SuppressWarnings({"rawtypes"})
public class DefaultSKUPropertyDescriptor extends GSAPropertyDescriptor {

	/**
     * 
     */
	private static final long			serialVersionUID	= 232447549378038420L;

	protected static final String		TYPE_NAME			= "defaultSKU";

	protected static final String		PRICE_LIST_MANAGER	= "priceListManager";

	private static final String			SKU_LIST			= "childSKUs";

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, DefaultSKUPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {

		if( pValue != null && pValue instanceof RepositoryItem ) { return pValue; }

		Collection col = (Collection)pItem.getPropertyValue( SKU_LIST );
		Iterator i = col.iterator();
		if( i.hasNext() ) {

		return i.next(); }

		// RepositoryItem priceList = getPriceList();
		// if (priceList != null)
		// {
		// Object col = pItem.getPropertyValue(SKU_LIST);
		//
		// DSWSKUPriceListComparator comp = new DSWSKUPriceListComparator();
		// comp.setPriceListManager(getPriceListManager());
		//
		// comp.setPriceList(this.getPriceList());
		// Collection list = (Collection) col;
		// if (list != null && list.size() > 0)
		// {
		// return Collections.min(list, comp);
		// }
		// else
		// {
		// return null;
		// }
		// }
		return null;
	}

	public String getTypeName() {
		return TYPE_NAME;
	}

	public void setValue( String pAttributeName, Object pValue ) {
		/* super.setValue(pAttributeName, pValue);
		 * if (pValue == null || pAttributeName == null)
		 * return;
		 * if (Nucleus.getGlobalNucleus() != null)
		 * {
		 * if (pAttributeName.equalsIgnoreCase(PRICE_LIST_MANAGER))
		 * {
		 * this.setPriceListManager((PriceListManager) Nucleus.getGlobalNucleus().resolveName(pValue.toString()));
		 * }
		 * } */
	}

	/* public PriceListManager getPriceListManager()
	 * {
	 * if (priceListManager == null)
	 * {
	 * String pManPath = (String) getValue(PRICE_LIST_MANAGER);
	 * priceListManager = (PriceListManager) Nucleus.getGlobalNucleus().resolveName(pManPath);
	 * }
	 * return priceListManager;
	 * }
	 * public void setPriceListManager(PriceListManager priceListManager)
	 * {
	 * this.priceListManager = priceListManager;
	 * }
	 * public RepositoryItem getPriceList()
	 * {
	 * atg.repository.RepositoryItem profile = ServletUtil.getCurrentUserProfile();
	 * if (profile != null)
	 * {
	 * RepositoryItem priceList = null;
	 * try
	 * {
	 * if (DynamicBeans.getBeanInfo(profile).hasProperty(PRICE_LIST))
	 * {
	 * priceList = (RepositoryItem) profile.getPropertyValue(PRICE_LIST);
	 * }
	 * }
	 * catch (IntrospectionException e)
	 * {
	 * return null;
	 * }
	 * return priceList;
	 * }
	 * else
	 * {
	 * return null;
	 * }
	 * } */
}
