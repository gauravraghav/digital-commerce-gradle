/**
 * 
 */
package com.digital.commerce.repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

@SuppressWarnings({"rawtypes","unchecked"})
public class MapConvertorPropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
     * 
     */
	private static final long		serialVersionUID	= -4062179148233753066L;

	protected static final String	COLLECTION			= "collection";

	protected static final String	TYPE_NAME			= "mapConvertor";

	protected static final String	MAP_KEY				= "keyProperty";

	protected static final String	MAP_VALUE			= "valueProperty";
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, MapConvertorPropertyDescriptor.class );
	}

	private static final DigitalLogger	logger				= DigitalLogger.getLogger( MapConvertorPropertyDescriptor.class );

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {

		// if we already have a value passed to us and it's a map, then just
		// return that
		if( pValue != null && pValue instanceof Map ) { return pValue; }
		// we didn't have a value, so we need to recreate the Map
		String colName = (String)getValue( COLLECTION );
		Object oCol = pItem.getPropertyValue( colName );
		// make sure it's a collection
		if( !( oCol instanceof Collection ) ) {
			logger.error( "ERROR: [" + pItem.getRepository().getRepositoryName() + "] collection on MapConvertor property descriptor is either not defined or not a collection" );
			return null;
		}
		Collection col = (Collection)oCol;
		String keyName = (String)getValue( MAP_KEY );
		String valueName = (String)getValue( MAP_VALUE );
		Iterator i = col.iterator();
		RepositoryItem item = null;
		Map convertedMap = new HashMap();

		while( i.hasNext() ) {
			item = (RepositoryItem)i.next();
			convertedMap.put( item.getPropertyValue( keyName ), item.getPropertyValue( valueName ) );
		}
		pItem.setPropertyValueInCache( this, convertedMap );

		return convertedMap;
	}
}
