package com.digital.commerce.services.order;

import java.util.ArrayList;
import java.util.List;

import atg.commerce.CommerceException;
import atg.commerce.catalog.CatalogTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.Order;
import atg.repository.RepositoryItem;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"unchecked"})
@Getter
@Setter
public class DigitalCommerceItemManager extends CommerceItemManager {

	private final static String ITEM_INACTIVE_CODE = "00001";
	private final static String ITEM_INACTIVE_MSG = "item_inactive_msg";

	private final static String ITEM_NOT_AVAILABLE_PICKUP_CODE = "00002";
	private final static String ITEM_NOT_AVAILABLE_PICKUP_MSG = "item_not_available_pickup_msg";

	private final static String ITEM_NOT_AVAILABLE_STS_CODE = "00003";
	private final static String ITEM_NOT_AVAILABLE_STS_MSG = "item_not_available_sts_msg";

	private final static String ITEM_PARTIAL_CODE = "00004";
	private final static String ITEM_PARTIAL_MSG = "item_partial_msg";

	private final static String ITEM_OOS_CODE = "00005";
	private final static String ITEM_OOS_MSG = "item_oos_msg";
	
	private MessageLocator messageLocator;

	public List<OrderReservationMsg> syncOrderItemReservation(Order order,
			boolean sync) throws Exception {
		// List<OrderReservationMsg> result = new
		// ArrayList<OrderReservationMsg>();
		ArrayList<OrderReservationMsg> result = new ArrayList<>();
		List<DigitalCommerceItem> items = order.getCommerceItems();
		List<DigitalCommerceItem> copyItems = new ArrayList<>(items);
		CatalogTools catalogTools = getOrderTools().getCatalogTools();
		for (DigitalCommerceItem item : copyItems) {
			RepositoryItem product = catalogTools.findProduct(item
					.getAuxiliaryData().getProductId());
			if (!(Boolean) product.getPropertyValue("isActive")) {
				if (!sync) {
					OrderReservationMsg msg = new OrderReservationMsg();
					msg.setProductId(product.getRepositoryId());
					msg.setProductName((String) product
							.getPropertyValue("displayName"));
					msg.setBrandName(((RepositoryItem) product
							.getPropertyValue("dswBrand")) == null ? ""
							: (String) ((RepositoryItem) product
									.getPropertyValue("dswBrand"))
									.getPropertyValue("displayName"));
					msg.setErrorCode(ITEM_INACTIVE_CODE);
					msg.setErrorMsg(messageLocator
							.getMessageString(ITEM_INACTIVE_MSG));
					item.setReservationEvaluation(msg);
					result.add(msg);
				} else
					this.removeItemFromOrder(order, item.getId());

			} else {
				if (item.getItemQuantityReserved() < 1.00) {
					if (sync)
						this.removeItemFromOrder(order, item.getId());
					else {
						OrderReservationMsg msg = new OrderReservationMsg();
						msg.setProductId(product.getRepositoryId());
						msg.setProductName((String) product
								.getPropertyValue("displayName"));
						msg.setBrandName(((RepositoryItem) product
								.getPropertyValue("dswBrand")) == null ? ""
								: (String) ((RepositoryItem) product
										.getPropertyValue("dswBrand"))
										.getPropertyValue("displayName"));
						msg.setErrorCode(ITEM_OOS_CODE);
						msg.setErrorMsg(messageLocator
								.getMessageString(ITEM_OOS_MSG));
						result.add(msg);
						item.setReservationEvaluation(msg);

					}

				} else {
					// Ship To Store
					boolean stsEligible = true;
					// In Store PickUp
					boolean ispEligible = true;
					if (item.getShipType() != null
							&& item.getShipType().equals(
									ShippingGroupConstants.ShipType.BOSTS
											.getValue())) {
						stsEligible = item.isShipToStoreAvailable();
					}

					else if (item.getShipType() != null
							&& item.getShipType().equals(
									ShippingGroupConstants.ShipType.BOPIS
											.getValue())) {
						ispEligible = item.isStorePickAvailable();
					}

					if ((Boolean) product.getPropertyValue("isActive")) {

						if (!stsEligible) {
							OrderReservationMsg msg = new OrderReservationMsg();
							msg.setProductId(product.getRepositoryId());
							msg.setProductName((String) product
									.getPropertyValue("displayName"));
							msg.setBrandName(((RepositoryItem) product
									.getPropertyValue("dswBrand")) == null ? ""
									: (String) ((RepositoryItem) product
											.getPropertyValue("dswBrand"))
											.getPropertyValue("displayName"));

							msg.setErrorCode(ITEM_NOT_AVAILABLE_STS_CODE);
							msg.setErrorMsg(messageLocator
									.getMessageString(ITEM_NOT_AVAILABLE_STS_MSG));

							if (sync) {
								this.removeItemFromOrder(order, item.getId());
							} else {
								item.setReservationEvaluation(msg);
							}

							result.add(msg);

						} else if (!ispEligible) {
							OrderReservationMsg msg = new OrderReservationMsg();
							msg.setProductId(product.getRepositoryId());
							msg.setProductName((String) product
									.getPropertyValue("displayName"));
							msg.setBrandName(((RepositoryItem) product
									.getPropertyValue("dswBrand")) == null ? ""
									: (String) ((RepositoryItem) product
											.getPropertyValue("dswBrand"))
											.getPropertyValue("displayName"));

							msg.setErrorCode(ITEM_NOT_AVAILABLE_PICKUP_CODE);
							msg.setErrorMsg(messageLocator
									.getMessageString(ITEM_NOT_AVAILABLE_PICKUP_MSG));

							if (sync) {
								this.removeItemFromOrder(order, item.getId());
							} else {
								item.setReservationEvaluation(msg);
							}

							result.add(msg);
						} else {
							if (item.getQuantity() > item
									.getItemQuantityReserved()) {
								if (sync)
									item.setQuantity((long) item
											.getItemQuantityReserved());
								else {
									OrderReservationMsg msg = new OrderReservationMsg();
									msg.setProductId(product.getRepositoryId());
									msg.setProductName((String) product
											.getPropertyValue("displayName"));
									msg.setBrandName(((RepositoryItem) product
											.getPropertyValue("dswBrand")) == null ? ""
											: (String) ((RepositoryItem) product
													.getPropertyValue("dswBrand"))
													.getPropertyValue("displayName"));
									msg.setErrorCode(ITEM_PARTIAL_CODE);
									msg.setErrorMsg(messageLocator
											.getMessageString(ITEM_PARTIAL_MSG));
									result.add(msg);
									item.setReservationEvaluation(msg);
								}
								// errorCodes.add(ITEM_PARTIAL_MSG);
							} else {
								item.setReservationEvaluation(null);
							}
						}
					}
				}
			}
		}

		return result;
	}
	
	
	public boolean isGiftCartItem(CommerceItem citem) {
		/*
		RepositoryItem item = (RepositoryItem) citem.getAuxiliaryData()
				.getProductRef();
		String prdType = (String) item.getPropertyValue("productType");
		if (prdType != null && prdType.endsWith("GiftCard")) {
			return true;
		} else {
			return false;
		}
		*/
		return citem.getCommerceItemClassType() != null && citem.getCommerceItemClassType().equals("giftCard");

	}

	public boolean hasGiftCartItem(Order pOrder) {
		if (pOrder.getCommerceItemCount() > 0) {
			List<CommerceItem> commerceItems = pOrder.getCommerceItems();
			for (CommerceItem ci : commerceItems) {
				if (isGiftCartItem(ci)) {
					return true;
				}
			}
		}

		return false;
	}

	@Override
	protected boolean shouldMergeItems(Order pOrder,
			CommerceItem pExistingItem, CommerceItem pNewItem) {
		boolean shouldMergeItems = false;

		if (this.isGiftCartItem(pNewItem))
			return false;

		if (shouldMergeItems(pExistingItem, pNewItem)) {
			DigitalCommerceItem newCI = ((DigitalCommerceItem) pNewItem);
			DigitalCommerceItem oldCI = ((DigitalCommerceItem) pExistingItem);
			if ((!DigitalStringUtil.isEmpty(newCI.getShipType()) && newCI
					.getShipType().equals(oldCI.getShipType()))) {
				if (DigitalStringUtil.isEmpty(newCI.getStoreId())
						&& DigitalStringUtil.isEmpty(oldCI.getStoreId())) {
					shouldMergeItems = true;
				} else {
					if (!DigitalStringUtil.isEmpty(newCI.getStoreId())
							&& newCI.getStoreId().equals(oldCI.getStoreId())) {
						shouldMergeItems = true;
					}
				}
			}
		}
		return shouldMergeItems;
	}

	@Override
	protected CommerceItem mergeOrdersCopyCommerceItem(Order pSrcOrder,
			Order pDstOrder, CommerceItem pItem) throws CommerceException {
		DigitalCommerceItem item = (DigitalCommerceItem) super
				.mergeOrdersCopyCommerceItem(pSrcOrder, pDstOrder, pItem);

		if (this.isGiftCartItem(pItem)) {
			copyGiftCardItem((GiftCardCommerceItem)pItem, (GiftCardCommerceItem)item);
		}

		item.setShipType(((DigitalCommerceItem) pItem).getShipType());
		item.setStoreId(((DigitalCommerceItem) pItem).getStoreId());
		//KTLO1-592 Setting the locationId ont he commerceItem
		item.setLocationId(((DigitalCommerceItem) pItem).getLocationId());
		item.setShipNode(((DigitalCommerceItem) pItem).getShipNode());
		item.setItemQuantityReserved(((DigitalCommerceItem) pItem).getItemQuantityReserved());
		item.setItemReserved(((DigitalCommerceItem) pItem).isItemReserved());
		
		return item;
	}

	private void copyGiftCardItem(GiftCardCommerceItem srcItem,
			GiftCardCommerceItem destItem) {
		destItem.setAsiCardPrice(srcItem.getAsiCardPrice());
		destItem.setAsiId(srcItem.getAsiId());
		destItem.setAsiInvoiceAmount(srcItem.getAsiInvoiceAmount());
		destItem.setAsiOrderNumber(srcItem.getAsiOrderNumber());
		destItem.setAsiPostage(srcItem.getAsiPostage());
		destItem.setBarCode(srcItem.getBarCode());
		destItem.setCardPrice(srcItem.getCardPrice());
		destItem.setCardQty(srcItem.getCardQty());
		destItem.setCltAffiliateId(srcItem.getCltAffiliateId());
		destItem.setDbId(srcItem.getDbId());
		destItem.setDollarValue(srcItem.getDollarValue());
		destItem.setGiftCardNumber(srcItem.getGiftCardNumber());
		destItem.setPin(srcItem.getPin());
		destItem.setPostage(srcItem.getPostage());
		destItem.setRecipientEmail(srcItem.getRecipientEmail());
		destItem.setRecipientName(srcItem.getRecipientName());
		destItem.setSenderEmail(srcItem.getSenderEmail());
		destItem.setSenderName(srcItem.getSenderName());
		destItem.setProductId(srcItem.getProductId());
		destItem.setProductType(srcItem.getProductType());
		destItem.setProductTitle(srcItem.getProductTitle());
		destItem.setItemQuantityReserved(srcItem.getItemQuantityReserved());
		destItem.setItemReserved(srcItem.isItemReserved());
	}
	
	public DigitalCommerceItem findCommerceItemByCatalogRefId(Order order, String catalogRefId){
		List<DigitalCommerceItem> cis = order.getCommerceItems();
		for(DigitalCommerceItem ci: cis){
			String sku = ci.getCatalogRefId();
			if(sku.equalsIgnoreCase(catalogRefId)){
				return ci;
			}
		}
		
		return null;
	}
	
	public List<GiftCardCommerceItem> findAllGiftCardItems(Order order){
		List<GiftCardCommerceItem> giftCardItemList = new ArrayList<>();
		List<DigitalCommerceItem> cis = order.getCommerceItems();
		for(DigitalCommerceItem ci: cis){
			if(isGiftCartItem(ci)){
				giftCardItemList.add((GiftCardCommerceItem)ci);
			}
		}
		return giftCardItemList;
	}

}