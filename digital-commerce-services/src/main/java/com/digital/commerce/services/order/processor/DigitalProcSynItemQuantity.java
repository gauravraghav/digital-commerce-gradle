package com.digital.commerce.services.order.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.services.order.DigitalCommerceItemManager;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderReservationMsg;

import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class DigitalProcSynItemQuantity implements PipelineProcessor {
	private DigitalCommerceItemManager itemManager;
	private MessageLocator messageLocator;
	private PricingTools pricingTools;
	private final static String PRICING_OP = "ORDER_TOTAL";
	
	private static final DigitalLogger logger = DigitalLogger
			.getLogger(DigitalProcSynItemQuantity.class);

	@Override
	public int[] getRetCodes() {
    return new int[]{ 1 };
	}

	@Override
	public int runProcess(Object arg0, PipelineResult arg1) throws Exception {
		HashMap map = (HashMap) arg0;
		DigitalOrderImpl order = (DigitalOrderImpl) map.get("Order");
		PricingModelHolder pricingModels = (PricingModelHolder)map.get("PricingModels");
		Locale locale = (Locale)map.get("Locale");
		boolean sync = (Boolean)map.get("sync");
		RepositoryItem profile = (RepositoryItem)map.get("Profile");
		Map extraParameters = (Map)map.get("ExtraParameters");
		try{
			List<OrderReservationMsg> result = itemManager.syncOrderItemReservation(order, sync);
			order.setInventoryProcessResult(result);
			this.pricingTools.performPricingOperation(PRICING_OP, order, pricingModels, locale, profile, extraParameters);
			/*
			if(synIssues != null && synIssues.size() > 0) {
				for(String issue : synIssues){
					arg1.addError(issue, this.getMessageLocator().getMessageString(issue));
				}
				return -1;
			}
			*/
		}catch(Exception e){
			logger.error(e);
			//arg1.addError(ERROR_WITH_SERVICE_CALL, this.getMessageLocator().getMessageString(ERROR_WITH_SERVICE_CALL));
			//return -1;
			return 1; 
			//to prevent any exception blow checkout process
		}
		return 1;
	}

}
