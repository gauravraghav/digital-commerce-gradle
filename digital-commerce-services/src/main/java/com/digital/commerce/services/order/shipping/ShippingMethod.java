/**
 * Shipping Methods 
 */
package com.digital.commerce.services.order.shipping;

import lombok.Getter;

@Getter
public enum ShippingMethod {
	Ground("GRN"), SecondDay("2ND"), NextDay("NDA"),BOPIS("ISPU"),BOSTS("STS");

	private final String	mappedValue;

	private ShippingMethod( String mappedValue ) {
		this.mappedValue = mappedValue;
	}

	public static ShippingMethod fromMappedValue( String value ) {
		ShippingMethod retVal = ShippingMethod.Ground;
		for( ShippingMethod shippingMethod : ShippingMethod.values() ) {
			if( shippingMethod.getMappedValue().equalsIgnoreCase( value ) ) {
				retVal = shippingMethod;
			}
		}
		return retVal;
	}
	
	public static  ShippingMethod getDefaultShippingMethod() {		
		return ShippingMethod.Ground;
	}
}
