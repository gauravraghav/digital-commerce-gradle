package com.digital.commerce.services.validator;

import java.util.regex.Pattern;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.DigitalProfileConstants;

public class DigitalFormValidationService {
	
	static final String			EMAIL_CHARS				= "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
	static final Pattern		EMAIL_PATTERN			= Pattern.compile( EMAIL_CHARS );
	
	static final String         MOBILE_CHARS            = "[\\d]+";
	static final Pattern        MOBILE_PATTERN        =   Pattern.compile( MOBILE_CHARS );
	// Start Password Section.
	static final  String        PASSWORD_ALPHANUMERIC_CHARS=   "[a-zA-Z0-9]+";//NOSONAR
	static final  Pattern        PASSWORD_ALPHANUMERIC_PATTERN    = Pattern.compile( PASSWORD_ALPHANUMERIC_CHARS );
	
	static final  String        PASSWORD_NUMERIC_CHARS   = ".*[0-9].*";//NOSONAR
	static final  Pattern       PASSWORD_NUMERIC_PATTERN    = Pattern.compile( PASSWORD_NUMERIC_CHARS );
	
	static final  String        PASSWORD_ALPHABETIC_CHARS   = ".*[A-Za-z].*";//NOSONAR
	static final  Pattern       PASSWORD_ALPHABETIC_PATTERN    = Pattern.compile( PASSWORD_ALPHABETIC_CHARS);
	
	static final  String        PASSWORD_SPECIAL_ALPHABETIC_CHARS = "[a-zA-Z0-9!#$&@?%-]+";//NOSONAR
	static final  Pattern       PASSWORD_SPECIAL_ALPHABETIC_PATTERN    = Pattern.compile( PASSWORD_SPECIAL_ALPHABETIC_CHARS);
	
	static final  String        PASSWORD_UPPER_ALPHABETIC_CHARS = ".*[A-Z].*";//NOSONAR
	static final  Pattern        PASSWORD_UPPER_ALPHABETIC_PATTERN    = Pattern.compile(  PASSWORD_UPPER_ALPHABETIC_CHARS);
	
	
	static final  String        PASSWORD_LOWER_ALPHABETIC_CHARS = ".*[a-z].*";//NOSONAR
	static final  Pattern        PASSWORD_LOWER_ALPHABETIC_PATTERN    = Pattern.compile(  PASSWORD_LOWER_ALPHABETIC_CHARS);
	
	
	
    /**
     * 
     * @param pEmail
     * @return
     */
	public boolean validateEmailAddress(String pEmail) {
		
		if(DigitalStringUtil.isEmpty(pEmail))
			 return false;
		
		if( pEmail.trim().length() < DigitalProfileConstants.MIN_EMAIL_LENGTH || pEmail.trim().length() > DigitalProfileConstants.MAX_EMAIL_LENGTH || !EMAIL_PATTERN.matcher( (CharSequence)pEmail.trim() ).matches() ) 
		{ 
			 return false; 
		}
		
		  return true;
	}

     /**
      * 
      * @return
      */
     public boolean validateMobilePhoneNumber(String pMobile){
    	 
       if(DigitalStringUtil.isEmpty(pMobile))
			 return true;
		
	   
       if( pMobile.trim().length() > DigitalProfileConstants.MAX_PHONE_US_LENGTH || !MOBILE_PATTERN.matcher( (CharSequence)pMobile.trim() ).matches() ) 
		{ 
			 return false; 
		}
		
		    return true;
     }

	

}