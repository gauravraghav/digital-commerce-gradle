package com.digital.commerce.services.order;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class DigitalOrderHistoryModel {
	
	private double total;
	private String currencyCode = "USD";
	private String status;
	private String locale;
	private String orderId;
	private String loyalNumber;
	private List<DigitalOrderHistoryItem> commerceItems;
	private String siteId;
	private String demandLocation;
	private Date statusDate;
	private Date addDate;
	private Date updateDate;
	private Date orderPlacementDate;
	private String emailId;
	private String profileId;
}
