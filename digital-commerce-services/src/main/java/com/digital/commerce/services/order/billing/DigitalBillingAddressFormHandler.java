package com.digital.commerce.services.order.billing;

import static com.digital.commerce.constants.DigitalProfileConstants.MIL_ADDRESS_TYPE;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import atg.commerce.CommerceException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.purchase.PurchaseProcessConfiguration;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.droplet.DropletException;
import atg.dtm.UserTransactionDemarcation;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.DigitalProfileConstants;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceResponse;
import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceResponse.AddressValidationDecision;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.common.DigitalGenericFormHandler;
import com.digital.commerce.services.order.DigitalCreditCard;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.purchase.PreCommitOrderManager;
import com.digital.commerce.services.responses.BillingServiceResponse;
import com.digital.commerce.services.validator.DigitalBasicAddressValidator;
import com.digital.commerce.services.validator.DigitalUSPSAddressValidator;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("unchecked")
@Getter
@Setter
public class DigitalBillingAddressFormHandler extends DigitalGenericFormHandler {

	private static final String CLASSNAME = DigitalBillingAddressFormHandler.class.getName();

	private BillingServiceResponse billingServiceResponse;

	private DigitalUSPSAddressValidator digitaluspsAddressValidator;

	private DigitalBasicAddressValidator digitalBasicAddressValidator;

	private MessageLocator messageLocator;

	private Set<String> errorFields = new LinkedHashSet<>();

	protected PurchaseProcessConfiguration configuration;

	private RepeatingRequestMonitor repeatingRequestMonitor;

	private PreCommitOrderManager preCommitOrderManager;

	protected static final String USPS_UNABLETO_VALIDATE_ADDRESS = "uspsUnabletoValidateAddress";
	
	protected static final String ADD_BILLINGADDRESS_EXCEPTION = "addBillingAddressException";

	/**
	 * Step 1: Accepts the request and populate the request in the POJO. 
	 * Step 2: Basic Validation check to see if the mandatory fields are passed. If not
	 * throw the appropriate error messages 
	 * Step 3: If Step<2> is passed, call the USPS Integration Service to validate the Address
	 * 
	 * @param request
	 * @param response
	 * @throws DigitalAppException
	 */
	public void handleBillingAddress(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws DigitalAppException {
		final String METHOD_NAME = "DigitalBillingAddressFormHandler.handleBillingAddress";
		UserTransactionDemarcation td = null;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(METHOD_NAME))) {
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				// Process the request and populate the request in
				// BillingContact
				DigitalContactInfo digitalContactInfo = processRequest(request);
				List<String> lstValidationErrors = digitalBasicAddressValidator.validateAddress(digitalContactInfo);

				logInfo("Basic Validation Errors for Billing Address" + lstValidationErrors);
				if (billingServiceResponse == null) { // This logic is not
														// need.But just in
														// case...
					billingServiceResponse = new BillingServiceResponse();
				}

				if (lstValidationErrors.size() > 0) {
					for (String errorMessage : lstValidationErrors) {
						addFormException(new DropletException(this.getMessageLocator().getMessageString(errorMessage),
								errorMessage));
					}

				} else if (digitalContactInfo.isDoAddressVerification()) {
					// USPS Service Call only if we pass true as part of the
					// json request
					AddressValidationServiceResponse addressValidationServiceResponse = digitaluspsAddressValidator
							.validateAddress(digitalContactInfo);
					AddressValidationDecision addressValidationDecision = addressValidationServiceResponse
							.getAddressValidationDecision();
					if (addressValidationDecision != null && addressValidationDecision.getDescription()
							.equalsIgnoreCase(DigitalProfileConstants.USPS_STATUS_SUGGESTED)) {
						DigitalContactInfo suggestedContactInfo = getSuggestedAddress(addressValidationServiceResponse);
						billingServiceResponse.setSuggestedAddress(suggestedContactInfo);
						DigitalContactInfo acceptedContactInfo = getAddress(addressValidationServiceResponse);
						billingServiceResponse.setAddress(acceptedContactInfo);
						addBillingAddressToPG(acceptedContactInfo);
					} else if (addressValidationDecision != null && addressValidationDecision.getDescription()
							.equalsIgnoreCase(DigitalProfileConstants.USPS_STATUS_ACCEPTED)) {
						DigitalContactInfo acceptedContactInfo = getAddress(addressValidationServiceResponse);
						billingServiceResponse.setAddress(acceptedContactInfo);
						addBillingAddressToPG(acceptedContactInfo);
					} else {
						addFormException(new DropletException(
								this.getMessageLocator().getMessageString(USPS_UNABLETO_VALIDATE_ADDRESS),
								USPS_UNABLETO_VALIDATE_ADDRESS));
					}
				} else {
					// setting the contact info without USPS validation but
					// basic validation done earlier, to the response object as
					// we got flag as false
					billingServiceResponse.setAddress(digitalContactInfo);
					addBillingAddressToPG(digitalContactInfo);
					this.getShippingGroupManager().updateShipAddrNameForBopisBostsOrder(getOrder());
				}
			} catch (DigitalIntegrationBusinessException e) {
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				logError("Caught Exception when trying to add Billing Address", e);
				addFormException(new DropletException(
						this.getMessageLocator().getMessageString(ADD_BILLINGADDRESS_EXCEPTION) + "::" + e.getMessage(),
						ADD_BILLINGADDRESS_EXCEPTION));

			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
	}

	/**
	 * 
	 * @param dswContactInfo
	 * @throws DigitalAppException
	 */
	private void addBillingAddressToPG(DigitalContactInfo digitalContactInfo) throws DigitalAppException {
		boolean found = false;
		Order order = getOrder();
		// After Successful USPS call set the ContactInfo to the Order Object
		// @SuppressWarnings("unchecked")
		if (null != order) {
			
			List<PaymentGroup> listPaymentGroups = order.getPaymentGroups();
			for (PaymentGroup paymentGroup : listPaymentGroups) {
				if (paymentGroup instanceof CreditCard) {
					((CreditCard) paymentGroup).setBillingAddress((Address) digitalContactInfo);
					found = true;
				}
			}
		}
		
		if(!found){
			/* KTLO1-316: Fix for S12 country code issue with flipping between paypal and credit card payment group.
			- Creating New CreditCard Payment Group and set the CC billing address when there is no CC payment group, 
			  which will be the case in case user goes through paypal and switches to credit card.
			- We are creating New CreditCard Payment Group  without any relationship as we can have only one ORDERAMOUNTREMAINING relationship, 
			  which in case paypal payment group will have that relationship.
			*/
			getCreditCardFromProfile(digitalContactInfo);
		}
	}
	
	/**
	 * Gets the saved card from profile & updates with the billing address
	 * provided in the order if present.
	 * 
	 * @param pRequest
	 * @return DigitalCreditCard
	 */
	private DigitalCreditCard getCreditCardFromProfile(DigitalContactInfo dswContactInfo) throws DigitalAppException {
		DigitalCreditCard creditCard = null;
		DigitalPaymentGroupManager pmgr = (DigitalPaymentGroupManager) this.getOrderManager().getPaymentGroupManager();
		Profile profile = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PROFILE, Profile.class);

		creditCard = pmgr.createCreditCardFromProfileNoRelationship(getOrder(), profile);
		if (creditCard != null) {
			creditCard.setBillingAddress((Address) dswContactInfo);
			try {
				this.getOrderManager().updateOrder(getOrder());
			} catch (CommerceException e) {
				logError(":: UpdateOrder() filed updating credit card with Billing Address ::", e);
			}
		}

		return creditCard;
	}

	/**
	 * 
	 * @param request
	 * @return DigitalContactInfo
	 */
	private DigitalContactInfo processRequest(DynamoHttpServletRequest request) {
		DigitalContactInfo digitalContactInfo = new DigitalContactInfo();

		try {
			/*
			 * Getting billing address containing email and phone number set
			 * earlier in the flow
			 */
			digitalContactInfo = ((DigitalPaymentGroupManager) this.getOrderManager().getPaymentGroupManager())
					.getBillingAddressFromOrder(getOrder());
		} catch (DigitalAppException e) {
			if (isLoggingError()) {
				logError("Exception while attempting to get billing address from order", e);
			}
		}

		digitalContactInfo.setFirstName(request.getParameter("firstname"));
		digitalContactInfo.setLastName(request.getParameter("lastname"));
		digitalContactInfo.setAddress1(request.getParameter("address1"));
		digitalContactInfo.setAddress2(request.getParameter("address2"));
		digitalContactInfo.setCountry(request.getParameter("country"));

		// Update email from request only if its not already present as part of
		// the billing address
		if (DigitalStringUtil.isEmpty(digitalContactInfo.getEmail())) {
			digitalContactInfo.setEmail(request.getParameter("emailaddress"));
		}
		digitalContactInfo.setEmailConfirm(request.getParameter("emailaddressconfirm"));
		digitalContactInfo.setPobox(request.getParameter("pobox"));
		digitalContactInfo.setPostalCode(request.getParameter("postalcode"));
		digitalContactInfo.setAddressType(request.getParameter("addresstype"));
		digitalContactInfo.setCity(request.getParameter("city"));
		
		// Update phone number only if its not present as part of the billing
		// address got from the Order
		if (DigitalStringUtil.isEmpty(digitalContactInfo.getPhoneNumber())) {
			digitalContactInfo.setPhoneNumber(request.getParameter("phonenumber"));
		}
		if (MIL_ADDRESS_TYPE.equalsIgnoreCase(digitalContactInfo.getAddressType())) {
			digitalContactInfo.setRegion(request.getParameter("region"));
			digitalContactInfo.setState("");
		} else {
			digitalContactInfo.setRegion("");
			digitalContactInfo.setRank("");
			digitalContactInfo.setState(request.getParameter("state"));
		}
		return digitalContactInfo;
	}

	/**
	 * 
	 * @param addressValidationServiceResponse
	 * @return DigitalContactInfo
	 */
	private DigitalContactInfo getSuggestedAddress(AddressValidationServiceResponse addressValidationServiceResponse) {
		DigitalContactInfo suggestedContactInfo = new DigitalContactInfo();
		String zipcode = null;
		suggestedContactInfo.setAddress1(addressValidationServiceResponse.getSuggestedAddressLine1());
		suggestedContactInfo.setAddress2(addressValidationServiceResponse.getSuggestedAddressLine2());
		suggestedContactInfo.setCity(addressValidationServiceResponse.getSuggestedCity());
		suggestedContactInfo.setState(addressValidationServiceResponse.getSuggestedState());
		if (!DigitalStringUtil.isEmpty(addressValidationServiceResponse.getZip4())) {
			zipcode = addressValidationServiceResponse.getZip5() + "-" + addressValidationServiceResponse.getZip4();
		} else {
			zipcode = addressValidationServiceResponse.getZip5();
		}
		suggestedContactInfo.setPostalCode(zipcode);
		return suggestedContactInfo;
	}

	/**
	 * 
	 * @param addressValidationServiceResponse
	 * @return DigitalContactInfo
	 */
	private DigitalContactInfo getAddress(AddressValidationServiceResponse addressValidationServiceResponse) {
		DigitalContactInfo acceptedContactInfo = new DigitalContactInfo();
		String zipcode = null;
		acceptedContactInfo.setAddress1(addressValidationServiceResponse.getAddressLine1());
		acceptedContactInfo.setAddress2(addressValidationServiceResponse.getAddressLine2());
		acceptedContactInfo.setCity(addressValidationServiceResponse.getCity());
		acceptedContactInfo.setState(addressValidationServiceResponse.getState());
		if (!DigitalStringUtil.isEmpty(addressValidationServiceResponse.getZip4())) {
			zipcode = addressValidationServiceResponse.getZip5() + "-" + addressValidationServiceResponse.getZip4();
		} else {
			zipcode = addressValidationServiceResponse.getZip5();
		}
		acceptedContactInfo.setPostalCode(zipcode);
		return acceptedContactInfo;
	}

	/**
	 * 
	 * @param messageKey
	 * @param params
	 */
	public void addFormError(String messageKey, Object... params) {
		final String message = this.getMessageLocator().getMessageString(messageKey, params);
		getErrorFields().add(message);
	}

	/**
	 * Returns appropriate order
	 * 
	 * @return Order
	 * @throws CommerceException
	 */
	private Order getOrder() throws DigitalAppException {
		return getConfiguration().getShoppingCart() != null ? getConfiguration().getShoppingCart().getCurrent() : null;
	}

	private OrderManager orderManager;

	/**
	 *
	 * @return DigitalShippingGroupManager
	 */
	public DigitalShippingGroupManager getShippingGroupManager() {
    return (DigitalShippingGroupManager) this.getOrderManager().getShippingGroupManager();
	}
}
