package com.digital.commerce.services.order.payment.giftcard;

import static com.digital.commerce.common.services.DigitalBaseConstants.GIFT_CARD_APPROVAL_RESPONSE_CODE;
import static com.digital.commerce.common.services.DigitalBaseConstants.GIFT_CARD_INSUFFICIENT_FUNDS_RESPONSE_CODE;
import static com.digital.commerce.common.services.DigitalBaseConstants.SUCCESS_AUTHRETURN_CODE;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import lombok.Getter;
import lombok.Setter;
import com.digital.commerce.integration.storedvalue.StoredValueService;
import com.digital.commerce.integration.storedvalue.StoredValueService.ServiceMethod;
import com.digital.commerce.integration.storedvalue.domain.StoredValueServiceRequest;
import com.digital.commerce.integration.storedvalue.domain.StoredValueServiceResponse;
import com.digital.commerce.services.order.payment.giftcard.valueobject.DigitalGiftCardApproval;
/**
 * some of the services that would go in this class gift card preauth, gift card
 * auth, rewards certificate auth etc */
@Getter
@Setter
public class GiftCardPaymentServices {
	private StoredValueService			giftCardServicesClient;
	private MessageLocator messageLocator;	
	/** Pre authorize a gift card
	 * 
	 * @param giftCardNumber
	 * @param giftCardPin
	 * @param orderAmount
	 * @return populated digitalgiftcardapproval object or empty if unsuccessful
	 * @throws ServicesException */
	public DigitalGiftCardApproval authorizeGiftCardPayment( String giftCardNumber, String giftCardPin, double orderAmount ) throws DigitalIntegrationException, DigitalAppException {
		DigitalGiftCardApproval status = new DigitalGiftCardApproval();
		if( null != giftCardServicesClient &&  (giftCardServicesClient).isServiceEnabled() && (giftCardServicesClient).isServiceMethodEnabled(ServiceMethod.PREAUTHORIZE.getServiceMethodName()) ) {
			StoredValueServiceRequest storedValueServiceData = new StoredValueServiceRequest(); 
			storedValueServiceData.setAmount(orderAmount);
			storedValueServiceData.setCardNumber(giftCardNumber);
			storedValueServiceData.setPin(giftCardPin);
			storedValueServiceData.setCurrencyName(StoredValueServiceRequest.USD);
			StoredValueServiceResponse response = giftCardServicesClient.preAuthorize(storedValueServiceData);
			if( null != response ) {
				status.setApprovalAmount( response.getAmount() );
				status.setResponseCode( response.getAuthorizationCode() );
				status.setTransactionId( response.getTransactionId() );
                if (null != response.getAuthorizationExpirationDate()) {
                    status.setAuthTimeStamp(response.getAuthorizationExpirationDate().toGregorianCalendar());
                }
				if( Integer.parseInt(response.getReturnCode()) == GIFT_CARD_APPROVAL_RESPONSE_CODE ) {
					status.setApprovalFlag( true );
					status.setStatusMessage( response.getReturnDescription() );
					status.setStatusCode( response.getReturnCode());
				} else{
					status.setApprovalFlag( false );
					status.setStatusMessage( response.getReturnDescription() );
					status.setStatusCode( response.getReturnCode());
				}
			}
		} else {
			throw new DigitalAppException("giftcard.service.offline");
		}
		return status;
	}

	/** check the balance of a gift card, set the approval flag to true if the response code is 1, otherwise it will
	 * be false
	 * 
	 * @param giftCardNumber String value of the gift card number
	 * @param giftCardPin String value of the 4 digit pin - should be all 0-9 as valid values
	 * @return a populate digital gift card approval object or one that is empty if unsuccessful
	 * @throws ServicesException */
	public DigitalGiftCardApproval getGiftCardBalance( String giftCardNumber, String giftCardPin, String currency ) throws DigitalIntegrationException, DigitalAppException{
		DigitalGiftCardApproval status = new DigitalGiftCardApproval();
		if( null != giftCardServicesClient &&  (giftCardServicesClient).isServiceEnabled() && (giftCardServicesClient).isServiceMethodEnabled(ServiceMethod.BALANCE_INQUIRY.getServiceMethodName()) ) {
			StoredValueServiceRequest storedValueServiceData = new StoredValueServiceRequest(); 
			storedValueServiceData.setCardNumber(giftCardNumber);
			storedValueServiceData.setPin(giftCardPin);
			storedValueServiceData.setCurrencyName(currency);
			StoredValueServiceResponse response = giftCardServicesClient.balanceInquiry( storedValueServiceData );
			if( null != response ) {
				status.setApprovalAmount( response.getAmount());
				status.setTransactionId( response.getTransactionId() );
				if( Integer.parseInt(response.getReturnCode()) == GIFT_CARD_APPROVAL_RESPONSE_CODE ) {
					if(!(status.getApprovalAmount() > 0d)){
						throw new DigitalAppException("giftcard.nobalance");
					}
					status.setApprovalFlag( true );
					status.setResponseCode( response.getReturnCode() );
					status.setStatusMessage( response.getReturnDescription() );
					status.setStatusCode( response.getReturnCode());
					status.setAuthorizationCode(response.getAuthorizationCode());
					status.setTransactionId(response.getTransactionId());
					// Shouldn't do this. But we check giftcard balance on commit and it returns with a
					// status of 1 (approved) but with 0.00 amount. This breaks the messaging.
					if( !( response.getAmount() > 0.00 ) ) {
						status.setStatusCode( "x1" );
					}
				} else {
					status.setApprovalFlag( false );
					status.setResponseCode( response.getReturnCode() );
					status.setStatusMessage( response.getReturnDescription() );
					status.setStatusCode( response.getReturnCode());
					status.setAuthorizationCode(response.getAuthorizationCode());
					status.setTransactionId(response.getTransactionId());
				}
			}
		} else {
			throw new DigitalAppException("giftcard.service.offline");
		}
		return status;
	}

	/** expire the authorize of a gift card, by calling the preauthcomplete method.
	 * 
	 * @param merchantId
	 * @param divisionId
	 * @param orderNumber
	 * @param orderAmount
	 * @param currency
	 * @param giftCardNumber
	 * @param giftCardPin
	 * @return populated digitalgiftcardapproval object or empty if unsuccessful
	 * @throws ServicesException */
	public DigitalGiftCardApproval expireAuthorizeGiftCardPayment( String giftCardNumber, String giftCardPin, String preAuthorizationId, Double amount ) throws DigitalIntegrationException, DigitalAppException{
		DigitalGiftCardApproval status = new DigitalGiftCardApproval();
		if( null != giftCardServicesClient &&  (giftCardServicesClient).isServiceEnabled() && (giftCardServicesClient).isServiceMethodEnabled(ServiceMethod.AUTHORIZE_COMPLETE.getServiceMethodName()) ) {
			StoredValueServiceRequest storedValueServiceData = new StoredValueServiceRequest(); 
			//storedValueServiceData.setTransactionId(transactionId);
			storedValueServiceData.setCardNumber(giftCardNumber);
			storedValueServiceData.setPin(giftCardPin);
			storedValueServiceData.setCurrencyName(StoredValueServiceRequest.USD);
			//DIG-1229: Setting the amount to be 0.0 for expireGC Auth
			storedValueServiceData.setAmount(0.0);
			StoredValueServiceResponse response = giftCardServicesClient.authorizeComplete( storedValueServiceData );
			if( null != response ) {
				status.setTransactionId( response.getTransactionId() );
				status.setAuthorizationCode(response.getAuthorizationCode());
				status.setApprovalAmount( response.getAmount() );
				status.setResponseCode( response.getAuthorizationCode() );
				status.setStatusMessage( response.getReturnDescription() );
				status.setStatusCode( response.getReturnCode() );
				if( SUCCESS_AUTHRETURN_CODE.equalsIgnoreCase( response.getReturnCode()) ) {
				if(response.isSuccess()){
					status.setApprovalFlag( true );
				} else {
					status.setApprovalFlag( false );
				}
			}
			}
		} else {
			throw new DigitalAppException("giftcard.service.offline");
		}
		return status;
	}
}
