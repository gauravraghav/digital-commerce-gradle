package com.digital.commerce.services.storelocator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SkuInventoryDetailBean {
    private long storeLevelStock;
    private long rolledUpStock;
	private boolean shipToStore;
	private boolean pickUpInStore;
}
