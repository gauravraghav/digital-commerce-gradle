/**
 * 
 */
package com.digital.commerce.services.pricing.tax;

import java.util.Date;
import java.util.Map;

import atg.payment.tax.TaxStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/** @author wibrahim */
@Getter
@Setter
@ToString
public class DigitalTaxStatus implements TaxStatus {

	private static final long	serialVersionUID	= -2909834813494222407L;
	private double				cityTax;
	private double				countryTax;
	private double				countyTax;
	private double				districtTax;
	private double				stateTax;
	private double				amount;
	private String				currency;
	private String				transactionId;
	private Date				transactionTimestamp;
	private boolean				transactionSuccess;
	private String				transactionErrorMessage;
	private String				errorMessage		= "";

	public boolean getTransactionSuccess() {
		return this.transactionSuccess;
	}
	@Override
	public Map<String, Double> getDetailedTaxInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getIsTaxIncluded() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public double getMiscTax() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Map<String, Double> getMiscTaxInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double getValueAddedTax() {
		// TODO Auto-generated method stub
		return 0;
	}
	/* (non-Javadoc)
	 * @see atg.payment.PaymentStatus#getAmount()
	 */
	@Override
	public double getAmount() {
		// TODO Auto-generated method stub
		return 0;
	}
	/* (non-Javadoc)
	 * @see atg.payment.PaymentStatus#getErrorMessage()
	 */
	@Override
	public String getErrorMessage() {
		// TODO Auto-generated method stub
		return null;
	}
	/* (non-Javadoc)
	 * @see atg.payment.PaymentStatus#getTransactionId()
	 */
	@Override
	public String getTransactionId() {
		// TODO Auto-generated method stub
		return null;
	}
	/* (non-Javadoc)
	 * @see atg.payment.PaymentStatus#getTransactionTimestamp()
	 */
	@Override
	public Date getTransactionTimestamp() {
		// TODO Auto-generated method stub
		return null;
	}
	/* (non-Javadoc)
	 * @see atg.payment.tax.TaxStatus#getCityTax()
	 */
	@Override
	public double getCityTax() {
		// TODO Auto-generated method stub
		return 0;
	}
	/* (non-Javadoc)
	 * @see atg.payment.tax.TaxStatus#getCountryTax()
	 */
	@Override
	public double getCountryTax() {
		// TODO Auto-generated method stub
		return 0;
	}
	/* (non-Javadoc)
	 * @see atg.payment.tax.TaxStatus#getCountyTax()
	 */
	@Override
	public double getCountyTax() {
		// TODO Auto-generated method stub
		return 0;
	}
	/* (non-Javadoc)
	 * @see atg.payment.tax.TaxStatus#getDistrictTax()
	 */
	@Override
	public double getDistrictTax() {
		// TODO Auto-generated method stub
		return 0;
	}
	/* (non-Javadoc)
	 * @see atg.payment.tax.TaxStatus#getStateTax()
	 */
	@Override
	public double getStateTax() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
