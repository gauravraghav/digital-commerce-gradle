package com.digital.commerce.services.order;

import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.profile.DigitalProfileTools;

import atg.commerce.order.CreditCard;
import atg.commerce.order.PaymentGroup;
import atg.nucleus.logging.ApplicationLoggingImpl;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalNewUserEligibility extends ApplicationLoggingImpl {
	
	public DigitalNewUserEligibility() {
		super(DigitalNewUserEligibility.class.getName());
	}
	private DigitalOrderTools orderTools;
	
	private DigitalProfileTools profileTools;

	private DigitalOrderImpl order;

	/**
	 * @param email
	 * @return
	 */
	public Map<String, Object> getNewUserEligibility(String email) {
		boolean retvalue = false;
		Map<String, Object> returnMap = profileTools.lookupUserByLogin(email);
		if(isLoggingDebug()) {
			logDebug("The map from lookupUserByLogin " + returnMap);
		}
		if (DigitalStringUtil.isBlank((String)returnMap.get("userID"))) {
			retvalue = true;
		}
		returnMap.put("newUserEligibility", retvalue);
		return returnMap;
	}

	/**
	 * @param email
	 * @param orderPassed
	 * @return
	 */
	public Map<String, Object> getNewUserEligibility(String email, DigitalOrderImpl orderPassed) {
		boolean retvalue = false;
		Map<String, Object> returnMap = profileTools.lookupUserByLogin(email);
		if(isLoggingDebug()) {
			logDebug("The map from lookupUserByLogin " + returnMap);
		}
		if (DigitalStringUtil.isNotBlank((String)returnMap.get("userID"))) {
			retvalue = false;
		} else if (orderPassed.getIsGiftCardOrderOnly() && !checkFirstAndLastNameInPaymentGroups(orderPassed)) {
			retvalue = false;
		} else {
			retvalue = true;
		}
		returnMap.put("newUserEligibility", retvalue);
		return returnMap;
	}

	/**
	 * @param orderPassed
	 * @return
	 */
	public boolean checkFirstAndLastNameInPaymentGroups(DigitalOrderImpl orderPassed) {
		@SuppressWarnings("unchecked")
		List<PaymentGroup> paymentGroupList = orderPassed.getPaymentGroups();
		String lastName = "";
		String firstName = "";
		boolean accountEligibility = false;
		if (paymentGroupList != null) {
			for (PaymentGroup pg : paymentGroupList) {
				if (pg instanceof CreditCard) {
					CreditCard creditCard = (CreditCard) pg;
					lastName = (String) creditCard.getBillingAddress().getLastName();
					firstName = (String) creditCard.getBillingAddress().getFirstName();
					if(DigitalStringUtil.isNotBlank(firstName) && DigitalStringUtil.isNotBlank(lastName)) {
						accountEligibility = true;
					}
				} else if (pg instanceof PaypalPayment) {
					PaypalPayment paypalPayment = (PaypalPayment) pg;
					firstName = (String) paypalPayment.getBillingAddress().getFirstName();
					lastName = (String) paypalPayment.getBillingAddress().getLastName();
					if(DigitalStringUtil.isNotBlank(firstName) && DigitalStringUtil.isNotBlank(lastName)) {
						accountEligibility = true;
					}
				}
			}
		}
		return accountEligibility;
	}
}