package com.digital.commerce.services.rewards;

import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.dtm.UserTransactionDemarcation;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.userprofiling.Profile;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.profile.rewards.LoyaltyCertificate;
import com.digital.commerce.services.rewards.domain.RewardsRetrievePointsOffersCertificatesRequest;
import com.digital.commerce.services.rewards.domain.RewardsRetrievePointsOffersCertificatesResponse;
import com.digital.commerce.services.rewards.domain.RewardsRetrieveRewardCertificate;
import com.digital.commerce.services.rewards.domain.RewardsRetrieveRewardCertificatesResponse;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalRewardsComponent extends ApplicationLoggingImpl {
	private static final String CLASSNAME = "DigitalRewardsComponent";
	private static final String SERVICE_ERROR_CODE = "ServiceError";

	private DigitalProfileTools 			profileTools;
	private DigitalRewardsManager rewardCertificateManager;
	private MessageLocator 				messageLocator;

	public DigitalRewardsComponent() {
		super(DigitalRewardsComponent.class.getName());
	}


	/**
	 * 
	 * @param rewardsRetrievePointsOffersCertificatesRequest
	 * @return
	 */
	public RewardsRetrievePointsOffersCertificatesResponse retrieveCertificatePointsHistory(
			RewardsRetrievePointsOffersCertificatesRequest rewardsRetrievePointsOffersCertificatesRequest) {

		String METHOD_NAME = "RETRIEVE_POINTS_OFFERS_CERTIFICATES";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsRetrievePointsOffersCertificatesResponse response = new RewardsRetrievePointsOffersCertificatesResponse();

		try {

			response = this.getRewardCertificateManager().retrieveCertificatePointsHistory(rewardsRetrievePointsOffersCertificatesRequest);

		} catch (Exception e) {
			logError("Error while retrieving retrievePointsOffersCertificates", e);
			response.getGenericExceptions().add(new ResponseError(SERVICE_ERROR_CODE, e.getLocalizedMessage()));
			response.setRequestSuccess(false);

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return response;

	}

	/**
	 *
	 * @param profile
	 * @return
	 */
	public RewardsRetrieveRewardCertificate retrieveRewardCertificatesByProfile(Profile profile) {

		//TODO: This is called in login response and getUserInfo to get the certificate total.

		String METHOD_NAME = "RETRIEVE_REWARDS_CERTIFICATES_PROFILE";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsRetrieveRewardCertificatesResponse response = new RewardsRetrieveRewardCertificatesResponse();
		RewardsRetrieveRewardCertificate rewardCerts = new RewardsRetrieveRewardCertificate();

		try {
			response = rewardCertificateManager.retrieveRewardCertificatesByProfile(profile);
			getRewardCertificateManager().loadProfileAndOrderCertsAndOffersUsingLoyaltyService(profile, getOrder(), response);
			List<LoyaltyCertificate> certs = getRewardCertificateManager().getProfileCertificates(getProfile());

			rewardCerts.setCertificates(certs);
			rewardCerts.setValidLoyaltyMember(response.isValidLoyaltyMember());
			rewardCerts.setFormError(false);

		} catch (Exception e) {
			logError("Error while retrieving addShopWithoutACardRequest", e);
			rewardCerts.getGenericExceptions().add(new ResponseError(SERVICE_ERROR_CODE, e.getLocalizedMessage()));
			rewardCerts.setRequestSuccess(false);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}
		return rewardCerts;
	}

    public Map<String, Object> retrieveRewardDetails(){

    	return null;
	}

	/**
	 * 
	 * @return
	 */
	private Profile getProfile() {
		return ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PROFILE, Profile.class);
	}

	/**
	 *
	 * @return
	 */
	public Order getOrder() {
		OrderHolder orderHolder =  ComponentLookupUtil.lookupComponent( ComponentLookupUtil.SHOPPING_CART, OrderHolder.class  );
		if(orderHolder != null){
			return orderHolder.getCurrent();
		}else{
			return null;
		}
	}
	/**
	 * Reload rewards data
	 */
	public void reloadRewardsData(){
		UserTransactionDemarcation td = null;
		String METHOD_NAME = "reloadRewardsData";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
			TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
			td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
		getProfileTools().loadLoyaltyMemberIntoProfile(getProfile(), true);
		} finally {
			TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
			TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
	}
	}

}
