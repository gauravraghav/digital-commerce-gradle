package com.digital.commerce.services.profile;

import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;

import atg.commerce.order.PaymentGroupRelationship;


public class FindPaymentGroupRelationshipWithPayPal implements DigitalPredicate<Object> {
	@Override
	public boolean apply( Object input ) {
		boolean retVal = false;
		if( input instanceof PaymentGroupRelationship ) {
			retVal = ( (PaymentGroupRelationship)input ).getPaymentGroup() instanceof PaypalPayment;
		}
		return retVal;
	}
}
