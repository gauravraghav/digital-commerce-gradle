package com.digital.commerce.services.inventory;

import java.util.HashMap;
import java.util.Map;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.repository.RepositoryItem;

/**
 * Created by ihartney on 2/24/2015.
 */
public class InventoryCache extends GenericService {

	private final Map<String, RepositoryItem> cache = new HashMap<>(
            40);
	private final Map<String, Integer> accessCount = new HashMap<>(40);
	private int cacheMisses = 0;
	private int cacheHits = 0;

	@Override
	public void doStartService() throws ServiceException {
		super.doStartService();
	}

	@Override
	public void doStopService() throws ServiceException {
		super.doStopService();
		if (isLoggingDebug()) {
			logDebug("------ Request Inventory Cache Stats ------");
			logDebug("Total Requests: [" + (cacheHits + cacheMisses)
					+ "] Hits: [" + cacheHits + "] Misses [" + cacheMisses
					+ "]");
			for (String key : accessCount.keySet()) {
				logDebug("   SKU [" + key + "] Hit Count {"
						+ accessCount.get(key).intValue() + "}");
			}
		}
		cache.clear();
		accessCount.clear();
	}

	public RepositoryItem get(String key) {
		if (cache.containsKey(key)) {
			if (isLoggingDebug()) {
				Integer hitCount = accessCount.get(key);
				if (hitCount == null) {
					hitCount = new Integer(1);
					accessCount.put(key, hitCount);
				} else {
					hitCount = new Integer(hitCount.intValue() + 1);
					accessCount.put(key, hitCount);
				}
				cacheHits++;
			}
			return cache.get(key);
		} else {
			if (isLoggingDebug()) {
				cacheMisses++;
			}
			return null;
		}
	}

	public void put(String key, RepositoryItem item) {
		cache.put(key, item);
	}

}
