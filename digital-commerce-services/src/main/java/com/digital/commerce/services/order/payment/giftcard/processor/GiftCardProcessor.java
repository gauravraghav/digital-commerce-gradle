/**
 * 
 */
package com.digital.commerce.services.order.payment.giftcard.processor;

import com.digital.commerce.services.order.payment.giftcard.GiftCardInfo;
import com.digital.commerce.services.order.payment.giftcard.GiftCardStatus;

/** This class defines an interface for gift card processing over different GiftCard payment systems. Any class which
 * implements this interface needs to provide an implementation for all the methods, authorize, debit, and credit.
 * 
 * @author djboyd */
public interface GiftCardProcessor {
	/** Authorize the amount in GiftCard
	 * 
	 * @param pGiftCardInfo the GiftCardInfo reference which contains
	 *            all the authorization data
	 * @return a GiftCardStatus object detailing the results of the
	 *         authorization */
	public GiftCardStatus authorize( GiftCardInfo pGiftCardInfo );

	/** Debit the amount in GiftCard after authorization
	 * 
	 * @param pGiftCardInfo the GiftCardInfo reference which contains
	 *            all the debit data
	 * @param pStatus the GiftCardStatus object which contains
	 *            information about the transaction. This should be the object
	 *            which was returned from authorize().
	 * @return a GiftCardStatus object detailing the results of the debit */
	public GiftCardStatus debit( GiftCardInfo pGiftCardInfo, GiftCardStatus pStatus );

	/** Credit the amount in GiftCard after debiting
	 * 
	 * @param pGiftCardInfo the GiftCardInfo reference which contains
	 *            all the credit data
	 * @param pStatus the GiftCardStatus object which contains
	 *            information about the transaction. This should be the object
	 *            which was returned from debit().
	 * @return a GiftCardStatus object detailing the results of the
	 *         credit */
	public GiftCardStatus credit( GiftCardInfo pGiftCardInfo, GiftCardStatus pStatus );

	/** Credit the amount in GiftCard outside the context of an Order
	 * 
	 * @param pGiftCardInfo the GiftCardInfo reference which contains
	 *            all the credit data
	 * @return a GiftCardStatus object detailing the results of the
	 *         credit */
	public GiftCardStatus credit( GiftCardInfo pGiftCardInfo );
}
