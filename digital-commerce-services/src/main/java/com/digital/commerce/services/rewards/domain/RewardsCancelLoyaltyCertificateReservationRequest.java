/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RewardsCancelLoyaltyCertificateReservationRequest {

	private String orderNumber;
	private String profileId;

}
