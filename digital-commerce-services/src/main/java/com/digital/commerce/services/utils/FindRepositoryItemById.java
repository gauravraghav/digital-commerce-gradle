package com.digital.commerce.services.utils;

import com.google.common.base.Predicate;

import atg.repository.RepositoryItem;

public class FindRepositoryItemById implements Predicate<RepositoryItem> {
	private final String	repositoryId;

	public FindRepositoryItemById( String repositoryId ) {
		this.repositoryId = repositoryId;
	}

	@Override
	public boolean apply( RepositoryItem input ) {
		return repositoryId.equalsIgnoreCase( input.getRepositoryId() );
	}
}
