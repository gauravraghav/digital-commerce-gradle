package com.digital.commerce.services.giftcard.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiftCardBalanceInquiryResponse extends ResponseWrapper {
	private String transactionId;
	private Double amount;
	private String authorizationCode;
	private String currencyName;
	private String giftCardNumber;
	private String pin;
}
