package com.digital.commerce.services.version;

import java.util.HashMap;

import com.digital.commerce.common.version.DigitalVersionController;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalServiceVersionController extends DigitalVersionController{

	String integrationModuleVersion="@bamboo.integrationModVersion@";
	String schemaVersion="@bamboo.schemaVersion@";
	String endecaModuleVersion="@bamboo.endecaModuleVersion@";
	public void getVersionDetails(){
		super.getVersionDetails();
		HashMap<String, String> versionMap = this.getVersionDetailsMap().get("dswBuildVersion");
		if(null != versionMap){
			versionMap.put("integrationModuleVersion", getIntegrationModuleVersion());
			versionMap.put("schemaVersion", getSchemaVersion());
			versionMap.put("endecaModuleVersion", getEndecaModuleVersion());
		}
	}
}
