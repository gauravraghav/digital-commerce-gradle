package com.digital.commerce.services.order.purchase;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.claimable.ClaimableException;
import atg.commerce.claimable.ClaimableTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CreditCard;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupRelationship;
import atg.commerce.order.purchase.CommitOrderFormHandler;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.profile.CommercePropertyManager;
import atg.commerce.states.PaymentGroupStates;
import atg.commerce.states.StateDefinitions;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.ContactInfo;
import atg.droplet.DropletException;
import atg.dtm.UserTransactionDemarcation;
import atg.markers.MarkerException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.scenario.targeting.Slot;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalObjectMapper;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.RequestHeaderAttributesConstant;
import com.digital.commerce.common.util.SessionAttributesConstant;
import com.digital.commerce.constants.DigitalAfterPayConstants;
import com.digital.commerce.constants.DigitalProfileConstants;
import com.digital.commerce.constants.PaypalConstants.PaypalPaymentGroupPropertyManager;
import com.digital.commerce.integration.common.communication.CommunicationService;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.oms.OrderManagementService;
import com.digital.commerce.integration.reward.RewardService;
import com.digital.commerce.integration.reward.bts.BtsRewardService;
import com.digital.commerce.integration.reward.domain.RewardsSendOrderData;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.DigitalCreditCard;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.GiftCardCommerceItem;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.afterpay.AfterPayCheckoutManager;
import com.digital.commerce.services.order.payment.afterpay.AfterPayPayment;
import com.digital.commerce.services.order.payment.afterpay.DigitalAfterPayTools;
import com.digital.commerce.services.order.payment.afterpay.valueobject.AfterPayOrderResponse;
import com.digital.commerce.services.order.payment.creditcard.DigitalCreditCardServiceStatus;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.order.purchase.valueobject.DigitalOrderRequest;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.promotion.DigitalGWPManager;
import com.digital.commerce.services.promotion.DigitalPromotionTools;
import com.digital.commerce.services.utils.DigitalAddressUtil;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.transaction.SystemException;

@SuppressWarnings({"unused", "rawtypes", "unchecked"})
@Getter
@Setter
public class DigitalCommitOrderFormHandler extends CommitOrderFormHandler {

	private static final String ERR_SUBMIT_ORDER_ASXML = "errorSubmitOrderAsXML";
	private static final String PAYPAL_GENERAL_ERROR = "paypalGeneralError";
	private static final String AFTERPAY_GENERAL_ERROR="afterPayGeneralError";
	private static final String USER_NOT_LOGGED_IN = "userNotLoggedIn";
	private static final String CONTACTINFO_EMPTY = "contactInfoEmpty";
	private static final String INVALID_CERTIFICATE_IN_ORDER = "invalidCertificateInOrder";
	private static final String TAX_OFFLINE_GC_TENDER = "taxoffline.giftcard.payment.notapplicable";
	private static final String PHONE_EMAIL_MISSING = "invalidEmailPhone";
  private static final String SEND_ORDER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
  private static final DateFormat sdf = new SimpleDateFormat(SEND_ORDER_DATE_FORMAT);

	private static final String	MSG_UNKNOWN_COUPON_EXCEPTION	= "unknownCouponException";
  private static final String PERFORM_MONITOR_NAME = "DigitalCommitOrderFormHandler_updateSourceOfOrder";
    private static final String PERF_NAME = "method";
  private static final String BOSTS_PROPERTY = "BOSTS";
  private static final String BOPIS_PROPERTY = "BOPIS";
    static final String CLASSNAME = DigitalCommitOrderFormHandler.class.getName();
	private MessageLocator messageLocator;
	private PreCommitOrderManager preCommitOrderManager;
	private PostCommitOrderManager postCommitOrderManager;
	private OrderXMLGenerator orderXMLGenerator;
	private OrderManagementService orderManagementService;
	private DigitalRewardsManager rewardCertificateManager;
	private DigitalProfileTools dswProfileTools;
	private DigitalShippingGroupManager dswShippingGroupManager;
	private DigitalBaseConstants digitalBaseConstants;
	private AfterPayCheckoutManager afterPayCheckoutManager;
	private DigitalAfterPayTools afterPayTools;

	private String giftCardNumber;
	private String giftCardPin;
	private String currency;

	private DigitalCreditCard creditCard;
	private String creditCardKey;
	private String creditCardType;
	private String nameOnCard;
	private String creditCardNumber;
	private String expirationMonth;
	private String expirationYear;
	private boolean newCreditCardPayment;
	private String vantivPaypageRegistrationId;
	private String paymentType;
	private String vantivBin;
	private String vantivCode;
	private String vantivMessage;
	private String vantivTime;
	private String vantivType;
	private String vantivLitleTxnId;
	private String vantivFirstSix;
	private String vantivLastFour;
	private boolean copyCreditCardToProfile = false;
	private boolean makeAsPrimary = false;
	private boolean billingSameAsShippingAddress = false;
	private String LOS_BUSINESS_ERROR = "LOSBE";
	private String cvvNumber;
	private boolean exchange;
	private DigitalGWPManager gwpManager;
	private Slot qualifierSlot;
  private String DEFAULT_CVV_CODE = "000";
	private DigitalOrderTools orderTools;
  private boolean validateLOSMethod;
	private Integer businessUnitId;
	private Integer locationNumber;
	private Profile profile;
	private String afterPayToken;
	private String commitOrderRequest;
	private DigitalOrderRequest request;

  List<String> warningMessages = new ArrayList<>();

  private static final String PAYMENT_TYPE_CC = "CreditCard";
  private static final String PAYMENT_TYPE_PAYPAL = "PayPal";
  private static final String PAYMENT_TYPE_AFTER_PAY = "AfterPay";
  private static final String PAYMENT_TYPE_GC = "GiftCard";
  private static final String PAYMENT_TYPE_NONE = "None";

	private CommunicationService communicationService;
	private DigitalAddressUtil addressUtil;

	/**
	 * @return
	 */
	public String getGiftCardPaymentGroupType() {
		return "giftCard";
	}

	/**
	 * @return
	 */
	public String getCreditCardPaymentGroupType() {
		return DigitalCreditCardServiceStatus.CREDIT_CARD;
	}

	/**
	 * @return
	 */
	public String getPaypalPaymentGroupType() {
		return PaypalPaymentGroupPropertyManager.PAYPAL_PAYMENT.getValue();
	}

  /**
   * @param pRequest
   * @param pResponse
   * @throws ServletException, IOException
   */
  public void preCommitOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
      throws ServletException, IOException {

    preCommitShippingMethod();

    // Mark the transaction rollback if there are any form validation errors
    if (this.getFormError()) {
      try {
        this.setTransactionToRollbackOnly();
      } catch (SystemException e) {
        logError("Transaction rollback exception :: ", e);
      }
      return;
    }

    DigitalCreditCard creditCard = getCreditCardFromOrder();

    // If Credit card is not found in the order then get it from profile
    if (creditCard == null) {
      if (getPaymentType() != null && PAYMENT_TYPE_CC.equals(getPaymentType())) {
        setCreditCard(getCreditCardFromProfile(pRequest));
      }
    } else {
      setCreditCard(creditCard);
    }

    if (getGwpManager().checkShippingRestriction(getOrder())) {
      if (isLoggingDebug()) {
        logDebug(
        		"DigitalCartModifierFormHandler.preAddItemToOrder() ---- >> enfore shipping restriction");
      }
      addFormException(
          new DropletException(getMessageLocator().getMessageString("gwpShippingRestriction"),
              "gwpShippingRestriction"));
    }
    if (!digitalBaseConstants.isPaTool() && getGwpManager().verifyShipRestriction(getOrder())) {
      String msg = getMessageLocator().getMessageString("shipRestriction");
      addFormException(new DropletException(msg, "shipRestriction"));
    }

    DigitalContactInfo tmpOrderContactInfo = null;

    try {
      /*
       * Getting billing address containing email and phone number set
       * by addorUpdateOrderContactInfo service earlier in the flow
       */
      tmpOrderContactInfo = ((DigitalPaymentGroupManager) this.getPaymentGroupManager())
          .getBillingAddressFromOrder(getOrder());
      // Setting email and phone from contact info to the shipping address.
      if (tmpOrderContactInfo != null && !PAYMENT_TYPE_AFTER_PAY.equals(getPaymentType())) {
        getDswShippingGroupManager().setHgShippingAddress(tmpOrderContactInfo, getOrder());
      }
    } catch (DigitalAppException e) {
      if (isLoggingError()) {
        logError("Exception while attempting to get billing address from order", e);
      }
    }

    if (isLoggingDebug()) {
      logDebug("tmpOrderContactInfo  "  + tmpOrderContactInfo);
    }

    if (getCreditCard() != null) {

      try {
        /*
         * Getting billing address containing email and phone number set
         * by addorUpdateOrderContactInfo service earlier in the flow
         */
        tmpOrderContactInfo = ((DigitalPaymentGroupManager) this.getPaymentGroupManager())
            .getBillingAddressFromOrder(getOrder());
      } catch (DigitalAppException e) {
        if (isLoggingError()) {
          logError("Exception while attempting to get billing address from order", e);
        }
      }
      if (isLoggingInfo()) {
        logInfo("Billing same as shipping  "  + isBillingSameAsShippingAddress());
      }
      if (isBillingSameAsShippingAddress()) {

        /*
         * Setting the billing address obtained from shipping address as
         * we want to set the billing address same as shipping address
         */
        getCreditCard().setBillingAddress(getShippingAddress());

        if (isLoggingDebug()) {
          logDebug(
              "Billing address from credit card before copying phone and email  " + getCreditCard()
                  .getBillingAddress());
          logDebug("tmpOrderContactInfo  "  + tmpOrderContactInfo);
        }
        if (null != tmpOrderContactInfo) {
          // get the billing address finally if its not earlier and
          // set it to the billing address
          DigitalRepositoryContactInfo dswCtInfo = (DigitalRepositoryContactInfo) getCreditCard()
              .getBillingAddress();
          if (DigitalStringUtil.isNotEmpty(tmpOrderContactInfo.getEmail())) {
            dswCtInfo.setEmail(DigitalStringUtil.lowerCase(tmpOrderContactInfo.getEmail()));
          }
          if (DigitalStringUtil.isNotEmpty(tmpOrderContactInfo.getPhoneNumber())) {
            dswCtInfo.setPhoneNumber(tmpOrderContactInfo.getPhoneNumber());
          }
          if (isLoggingDebug()) {
            logDebug("dswCtInfo  "  + dswCtInfo);
          }
          getCreditCard().setBillingAddress(DigitalAddressUtil.getDSWContactInfo(dswCtInfo));
          if (isLoggingDebug()) {
            logDebug("Billing address from credit card  "  + getCreditCard().getBillingAddress());
          }
        } else {
          if (isLoggingInfo()) {
            logInfo("tmpOrderContactInfo is empty ");
          }
        }
        if (isLoggingInfo()) {
          logInfo("Successfully set ShippingAddress as Billing Address ");
        }

      }

      preCommitCreditCard();
    }

    if (isOnlyGiftCardInPaymentMethod(getOrder()) && ((DigitalOrderImpl) getOrder()).isTaxOffline()) {
      addFormException(
          new DropletException(getMessageLocator().getMessageString(TAX_OFFLINE_GC_TENDER),
              TAX_OFFLINE_GC_TENDER));
    }

		if (!this.getFormError()) {
			try {
				double creditCardPaypalTotal =
            DigitalCommonUtil.round(orderTools.getCreditCardPaypalTotal((DigitalOrderImpl) getOrder()));
        if(PAYMENT_TYPE_AFTER_PAY.equals(getPaymentType())){
					getPreCommitOrderManager().removeUnusedPaypalPayment(getOrder(), false);
				}
        else if (newCreditCardPayment || this.getCreditCardKey() != null
            || creditCardPaypalTotal < 0.00001) {
          getPreCommitOrderManager().removeUnusedPaypalPayment(getOrder(), true);
        }
      } catch (CommerceException e) {
        if (isLoggingError()) {
          this.logError("Error removing unused PaypalPayment", e);
        }
      }

		if (getPaymentType() != null &&  !PAYMENT_TYPE_AFTER_PAY.equals(getPaymentType())) {
			if (!getDswProfileTools().isContactInfoPresent(getOrder())) {
				addFormException(
						new DropletException(getMessageLocator().getMessageString(CONTACTINFO_EMPTY),
								CONTACTINFO_EMPTY));
			}
		}
      synchronized (getOrder()) {
        getPreCommitOrderManager().updateOrderForCommitOrder(getOrder());
        try {

          getOrderManager().updateOrder(getOrder());
        } catch (CommerceException e) {
          if (isLoggingError()) {
            logError(e);
          }
        }
      }

      claimBatchCoupon((DigitalOrderImpl) getOrder());

      // Mark the transaction rollback if there are any form validation errors
      if (this.getFormError()) {
        try {
          this.setTransactionToRollbackOnly();
        } catch (SystemException e) {
          logError("Transaction rollback exception :: ", e);
        }
        return;
      }

      super.preCommitOrder(pRequest, pResponse);
    }

    // Mark the transaction rollback if there are any form validation errors
    if (this.getFormError()) {
      try {
        this.setTransactionToRollbackOnly();
      } catch (SystemException e) {
        logError("Transaction rollback exception :: ", e);
      }
    }
  }

	/**
	 * Method to validate reward certificates applied on the order is still active not yet reserved
	 */
	private void preCommitValidateRewards() {
		if(null == profile) {
			profile = ComponentLookupUtil
					.lookupComponent(ComponentLookupUtil.PROFILE, Profile.class);
		}
		if (getOrderTools().isOrderIncompleteState(getOrder()) && this.getDswProfileTools()
				.isLoggedIn(profile) && !this.getDswProfileTools().isDSWAnanymousUser(profile)) {
			try {
				if (DigitalStringUtil.isNotBlank(this.getDswProfileTools().getLoyaltyNumber(profile))) {
					Set<RepositoryItem> orderCertificates = this.getRewardCertificateManager()
							.getOrderCertificates(getOrder());
					int originalCertificatesCount = orderCertificates != null ? orderCertificates.size() : 0;
					this.getRewardCertificateManager().checkInvalidCertificates(getOrder());
					orderCertificates = this.getRewardCertificateManager().getOrderCertificates(getOrder());
					int newCertificatesCount = orderCertificates != null ? orderCertificates.size() : 0;
					if (originalCertificatesCount != newCertificatesCount) {
						addFormException(
								new DropletException(
										getMessageLocator().getMessageString(INVALID_CERTIFICATE_IN_ORDER),
										INVALID_CERTIFICATE_IN_ORDER));
					}
				}
			} catch (RepositoryException e) {
				logError("Unable to validate rewards on the order");
			}
		}
	}


	/**
	 * Ensures that all of the loyalty, marketing and csr fields are updated.
	 */
	private void preCommitAuditFields() {
		getPreCommitOrderManager().updateAuditFields(getOrder());

	}

	/**
	 * Get the Credit Card from the Order
   *
	 * @return DigitalCreditCard
	 */
	private DigitalCreditCard getCreditCardFromOrder() {
		DigitalPaymentGroupManager pmgr = (DigitalPaymentGroupManager) this.getPaymentGroupManager();
    DigitalCreditCard creditCard = pmgr.findCreditCard(getOrder());
		// In case of PayPal Payment Group; CC group is not present
		if (creditCard == null && this.getCreditCardKey() != null) {
			setPaymentType(PAYMENT_TYPE_CC);
		}
		return creditCard;
	}

	/**
   * Gets the saved card from profile & updates with the billing address provided in the order if
   * present
   *
	 * @param pRequest
	 * @return DigitalCreditCard
	 */
	private DigitalCreditCard getCreditCardFromProfile(DynamoHttpServletRequest pRequest) {
		DigitalCreditCard creditCard = null;
		ContactInfo billingAddress = null;
		DigitalPaymentGroupManager pmgr = (DigitalPaymentGroupManager) this.getPaymentGroupManager();
    Profile profile = ComponentLookupUtil
        .lookupComponent(ComponentLookupUtil.PROFILE, Profile.class);

		List paymentGroupList = getOrder().getPaymentGroups();
		if (paymentGroupList != null) {
			Iterator i = paymentGroupList.iterator();
			while (i.hasNext()) {
				PaymentGroup pg = (PaymentGroup) i.next();
				if (pg instanceof PaypalPayment) {
					PaypalPayment paypalPayment = (PaypalPayment) pg;
					billingAddress = new ContactInfo();
					billingAddress.setFirstName(paypalPayment.getBillingAddress().getFirstName());
					billingAddress.setLastName(paypalPayment.getBillingAddress().getLastName());
					billingAddress.setAddress1(paypalPayment.getBillingAddress().getAddress1());
					billingAddress.setAddress2(paypalPayment.getBillingAddress().getAddress2());
					billingAddress.setCity(paypalPayment.getBillingAddress().getCity());
					billingAddress.setState(paypalPayment.getBillingAddress().getState());
					billingAddress.setCountry(paypalPayment.getBillingAddress().getCountry().trim());
					billingAddress.setPostalCode(paypalPayment.getBillingAddress().getPostalCode());
					billingAddress.setPhoneNumber(paypalPayment.getBillingAddress().getPhoneNumber());
					billingAddress.setEmail(paypalPayment.getBillingAddress().getEmail());
				}
			}
		}

		if (getAddressUtil().isEmptyAddress(billingAddress)) {
			billingAddress = null;
		}
		creditCard = pmgr.createCreditCardFromProfile(getOrder(), profile);
    if (getAddressUtil().isEmptyAddress((ContactInfo) creditCard.getBillingAddress())
        || billingAddress != null) {
			if (null != billingAddress) {
				creditCard.setBillingAddress(billingAddress);
			}
		}

		// Set amount
		creditCard.setAmount(getOrder().getPriceInfo().getTotal());
		return creditCard;
	}

	/**
   * This takes the currently entered data or select card and adds it to the order.
	 */
	private void preCommitCreditCard() {
		DigitalCreditCard creditCard = getCreditCard();
		// set ip address to be included as part of s12 xml
		creditCard.setIpAddress(ServletUtil.getCurrentRequest().getHeader("Client-Source-IP"));

    creditCard.setCvvNumber(this.getCvvNumber());

		if (newCreditCardPayment || this.getCreditCardKey() != null) {
			if (DigitalStringUtil.isEmpty(this.getCvvNumber())) {
				addFormException(
            new DropletException(
                getMessageLocator().getMessageString(DigitalBaseConstants.MISSINGCVVNUMBER),
								DigitalBaseConstants.MISSINGCVVNUMBER));
			}

			if (newCreditCardPayment) {
				handleNewCreditCardPayment();
			} else {
				handleSavedCreditCardPayment();
			}

			//REW-1128: Setting this property to for orders placed using existing credit cards on file
      if (null == getPaymentType()) {
				setPaymentType("CreditCard");
			}

			if (this.exchange && digitalBaseConstants.isPaTool()) {
				if (isLoggingInfo()) {
          logInfo("This is an EXCHANGE ORDER with OrderID :: " + getOrder().getId());
				}
			}
			//KTLO1-957
      if (null != this.getCvvNumber() && DEFAULT_CVV_CODE.equals(this.getCvvNumber())
          && this.exchange && digitalBaseConstants.isPaTool()) {
				this.setCvvNumber("");
        creditCard.setCvvNumber(this.getCvvNumber());
			}

			// Mark the transaction rollback if there are any form validation
			// errors
			if (this.getFormError()) {
				try {
					this.setTransactionToRollbackOnly();
				} catch (SystemException e) {
					logError("Transaction rollback exception :: ", e);
				}
				return;
			}

			DigitalPaymentGroupManager pmg = (DigitalPaymentGroupManager) getPaymentGroupManager();
			try {

				pmg.assignIndividualCreditCard(getOrder(), creditCard);

				getPreCommitOrderManager().removeUnusedCreditCard(getOrder());

			} catch (CommerceException e) {
				logError("Exception while attempting to assign a new credit card to the order", e);
			}
    } else {
			try {
				// this is to handle removal of empty default cc payment group when PayPal or GiftCard is present
        if (getOrder().getPaymentGroupCount() > 1) {
					getPreCommitOrderManager().removeUnusedCreditCard(getOrder());
				}
			} catch (CommerceException e) {
				logError("Exception while attempting to assign a new credit card to the order", e);
			}
		}
	}

	/**
	 * Handle New Credit card payment for the order
	 */
	private void handleNewCreditCardPayment() {
		DigitalOrderImpl order = (DigitalOrderImpl) getOrder();
		if (creditCard != null) {

			if (DigitalStringUtil.isNotEmpty(this.getCvvNumber())) {
				if (!digitalBaseConstants.getCreditCardTypeAX().equals(this.getCreditCardType())
						&& this.getCvvNumber().trim().length() != 3) {
					addFormException(new DropletException(
							getMessageLocator().getMessageString(DigitalBaseConstants.INVALID_CVV_LENGTH),
							DigitalBaseConstants.INVALID_CVV_LENGTH));
				}

				if (digitalBaseConstants.getCreditCardTypeAX().equals(this.getCreditCardType())
						&& this.getCvvNumber().trim().length() < 3) {
					addFormException(new DropletException(
							getMessageLocator().getMessageString(DigitalBaseConstants.INVALID_CVV_LENGTH_AMEX),
							DigitalBaseConstants.INVALID_CVV_LENGTH_AMEX));
				}
			}

			creditCard.setNameOnCard(this.getNameOnCard());
			creditCard.setPaypageRegistrationId(this.getVantivPaypageRegistrationId());
			creditCard.setExpirationMonth(this.getExpirationMonth());
			if (this.getExpirationYear() != null) {
				// Accepts year as either four digit or two digits format and
				// converts it to four digit for further processing
				// Year will be defaulted to 20th century if two digits are
				// passed in the input, this is picked up from properties file
				// and can be changed once we get past the 20th century
				creditCard.setExpirationYear(getExpirationYear().length() == 2
						? getDigitalBaseConstants().getDefaultExpirationCentury() + getExpirationYear()
						: getExpirationYear());
			}
			creditCard.setCreditCardType(this.getCreditCardType());

			if (!creditCard.getAuthorizationStatus().isEmpty()) {
				creditCard.getAuthorizationStatus().clear();
			}
			// Setting last four on the order for use in the confirmation page.
			order.setLastFour(getVantivLastFour());
			creditCard.setvantivTransId(this.vantivLitleTxnId);
			if (creditCard.getcreditCardLastFour() == null) {
				creditCard.setcreditCardLastFour(order.getLastFour());
			}
			creditCard.setVantivFirstSix(getVantivFirstSix());
			// Make the credit card as default card if user has chosen to so
			if (this.isMakeAsPrimary()) {
				creditCard.setDefaultCreditCard(isMakeAsPrimary());
			}

		}

		if (this.isCopyCreditCardToProfile()) {
			if (!this.getProfile().isTransient()) {
				order.setCopyCreditCardToProfile(!this.getProfile().isTransient());
				creditCard.setCardBeingSavedToProfile(true);
			} else {
				// we need a logged in user to save the card to profile
        addFormException(
            new DropletException(getMessageLocator().getMessageString(USER_NOT_LOGGED_IN),
						USER_NOT_LOGGED_IN));
			}
		}

		if (isLoggingInfo() && DigitalStringUtil.isNotEmpty(this.vantivLitleTxnId)) {
			logInfo(String.format("Order %s generated Vantiv Litle Transaction ID %s", order.getId(),
					this.vantivLitleTxnId));
		}
	}

	/**
	 * Handle Saved Credit card payment for the order
	 */
	private void handleSavedCreditCardPayment() {
		if (creditCard != null) {
			if (!creditCard.getAuthorizationStatus().isEmpty()) {
				creditCard.getAuthorizationStatus().clear();
			}
		}
		RepositoryItem ccRepItem = getCommerceProfileTools().getCreditCardById(getCreditCardKey());

		if (ccRepItem != null && creditCard != null) {
			CommercePropertyManager cpm = getCommercePropertyManager();

			if (DigitalStringUtil.isNotEmpty(this.getCvvNumber())) {
				if (!digitalBaseConstants.getCreditCardTypeAmex()
						.equals((String) ccRepItem.getPropertyValue(cpm.getCreditCardTypePropertyName()))
						&& this.getCvvNumber().trim().length() != 3) {
					addFormException(new DropletException(
							getMessageLocator().getMessageString(DigitalBaseConstants.INVALID_CVV_LENGTH),
							DigitalBaseConstants.INVALID_CVV_LENGTH));
				}

				if (digitalBaseConstants.getCreditCardTypeAmex()
						.equals((String) ccRepItem.getPropertyValue(cpm.getCreditCardTypePropertyName()))
						&& this.getCvvNumber().trim().length() < 4) {
					addFormException(new DropletException(
							getMessageLocator().getMessageString(DigitalBaseConstants.INVALID_CVV_LENGTH_AMEX),
							DigitalBaseConstants.INVALID_CVV_LENGTH_AMEX));
				}
			}

      creditCard.setNameOnCard(
          (String) ccRepItem.getPropertyValue(DigitalCreditCardServiceStatus.NAME_ON_CARD));
      creditCard.setTokenValue(
          (String) ccRepItem.getPropertyValue(DigitalCreditCardServiceStatus.TOKEN_VALUE));
      creditCard.setCreditCardType(
          (String) ccRepItem.getPropertyValue(cpm.getCreditCardTypePropertyName()));
			creditCard.setExpirationMonth(
					(String) ccRepItem.getPropertyValue(cpm.getCreditCardExpirationMonthPropertyName()));
			creditCard.setExpirationYear(
					(String) ccRepItem.getPropertyValue(cpm.getCreditCardExpirationYearPropertyName()));
			if (creditCard.getTokenValue() != null) {
				creditCard.setcreditCardLastFour(extractLastFour(creditCard.getTokenValue()));
			}

			try {
				/*
				 * Use credit card key to get billing address from the saved
				 * credit card as this flow will not have option to add billing
				 * address
				 */
        ContactInfo contactInfo = getDswProfileTools()
            .getCreditCardBillingAddress(getCreditCardKey());
				if (null != contactInfo) {
					creditCard.setBillingAddress(getContactInfoFromOrder(contactInfo));

				}
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError("Exception while attempting to get billing address from Credit card key", e);
				}
			}
		}
	}

	/**
	 * @param contactInfo
	 * @return ContactInfo
	 */
	private ContactInfo getContactInfoFromOrder(ContactInfo contactInfo) {

		Order order = getOrder();
		List<PaymentGroup> listPaymentGroups = order.getPaymentGroups();
		if (listPaymentGroups != null) {
			for (PaymentGroup paymentGroup : listPaymentGroups) {

				if (paymentGroup instanceof CreditCard) {
					// get the payment group if its exists
					CreditCard ccCard = (CreditCard) paymentGroup;
					ContactInfo billingAddr = (ContactInfo) ccCard.getBillingAddress();
					if (billingAddr != null) {
						if (DigitalStringUtil.isNotEmpty(billingAddr.getPhoneNumber())) {
							contactInfo.setPhoneNumber(billingAddr.getPhoneNumber());
						}
						if (DigitalStringUtil.isNotEmpty(billingAddr.getEmail())) {
							contactInfo.setEmail(billingAddr.getEmail());
						}
					} else {
						addFormException(new DropletException(
                this.getMessageLocator().getMessageString(PHONE_EMAIL_MISSING),
                PHONE_EMAIL_MISSING));
					}
				}
			}
		}
		return contactInfo;
	}

	/**
   * Validate the Shipping Address fields & optionally LOS for the order
	 */
	private void preCommitShippingMethod() {
		if (((DigitalOrderImpl) getOrder()).isNonGCShipToHomeOrder()) {
			try {
				getPreCommitOrderManager().validateShippingAddress(getOrder());
			} catch (DigitalAppException e) {
				String msg = this.getMessageLocator().getMessageString("selectShippingAddress");
				this.addFormException(new DropletException(msg, "INVALID_SHIPPING_ADDRESS"));
				return;
			}

      if (isValidateLOSMethod()) {
			try {
				getPreCommitOrderManager().validateShippingMethod(getOrder());
			} catch (DigitalAppException e) {
          String msg = this.getMessageLocator().getMessageString("invalidLOS",
						this.getMessageLocator().getMessageString("invalidShippingMethod"));
				this.addFormException(new DropletException(msg, LOS_BUSINESS_ERROR));
			}
		}
	}
  }


	/**
	 * Method to get shipping address from order.
	 *
	 * @return DigitalContactInfo
	 */
	private DigitalContactInfo getShippingAddress() {
		DigitalContactInfo shippingAdress = new DigitalContactInfo();
		if (!((DigitalOrderImpl) getOrder()).getIsGiftCardOrderOnly()) {
			try {
				shippingAdress = getPreCommitOrderManager().getHardgoodShippingGroupAddress(getOrder());
			} catch (DigitalAppException e) {
				this.addFormException(new DropletException(
						getMessageLocator().getMessageString(DigitalProfileConstants.MSG_ERROR_SAVING_ADDRESS),
						DigitalProfileConstants.MSG_ERROR_SAVING_ADDRESS));
			}
		}
    if (isLoggingDebug()) {
			logDebug("The first non-GC shipping address " + shippingAdress);
		}
		return shippingAdress;
	}

	/**
   * @param pRequest  the request object
   * @param pResponse the response object
	 * @return true or false
   * @throws ServletException if an error occurs
   * @throws IOException      if an error occurs
	 */
  public boolean handleCommitOrder(DynamoHttpServletRequest pRequest,
      DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String METHOD_NAME = "handleCommitOrder";
		final String HANDLE_METHOD = "DigitalCommitOrderFormHandler." + METHOD_NAME;
		boolean retValue = false;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(HANDLE_METHOD)) {

      final UserTransactionDemarcation td = TransactionUtils
          .startNewTransaction(this.getName(), METHOD_NAME);
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				super.resetFormExceptions();

				// Moved vantivfirstsix mandatory check from validation happening in the
				// pipeline as we have to have this check
				// only in case of new credit card scenario as we don't need this field
				// for existing card flows
				// Vantiv first six is used to determine the creditcard type
				if (this.isNewCreditCardPayment() && DigitalStringUtil.isEmpty(getVantivFirstSix())) {
					addFormException(new DropletException(
							getMessageLocator().getMessageString(DigitalProfileConstants.MISSING_VANTIV_FIRST_SIX),
							DigitalProfileConstants.MISSING_VANTIV_FIRST_SIX));
					return true;
				}

				if (isGiftCardInGiftCardPaymentMethod(getOrder())) {
					String msg = getMessageLocator().getMessageString("giftcard.payment.onlygiftcardinorder");
					addFormException(new DropletException(msg, "giftcard.payment.onlygiftcardinorder"));
					return true;
				}
				preCommitValidateRewards();
				if (this.getFormError()){
					return true;
				}

				retValue = super.handleCommitOrder(pRequest, pResponse);

				if (this.getFormError() || this.isTransactionMarkedAsRollBack()) {
					DigitalOrderImpl pOrder = (DigitalOrderImpl) getOrder();
					List<PaymentGroup> paymentGroupList = pOrder.getPaymentGroups();
					if (paymentGroupList != null) {
						for (PaymentGroup pg : paymentGroupList) {
							if (pg != null && pg instanceof PaypalPayment
									&& pg.getState() == StateDefinitions.PAYMENTGROUPSTATES
											.getStateValue(PaymentGroupStates.AUTHORIZE_FAILED)) {

								super.resetFormExceptions();
								String msg = getMessageLocator().getMessageString(PAYPAL_GENERAL_ERROR);
								addFormException(new DropletException(msg, PAYPAL_GENERAL_ERROR));
							}
						}
					}
				} // if there is no form error, sending out CS13 message.

				if (((DigitalOrderImpl) getOrder()).isTaxOffline()) {
					warningMessages.add(getMessageLocator().getMessageString("taxOffline"));
				}

			} catch (Exception ex) {
				TransactionUtils.rollBackTransaction(td, this.getName(), METHOD_NAME);
				if (isLoggingError()) {
					logError(ex);
				}
			} finally {
				TransactionUtils.endTransaction(td, this.getName(), METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(HANDLE_METHOD);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
		return retValue;
	}

	/**
	 * @param pRequest  the request object
	 * @param pResponse the response object
	 * @return true or false
	 * @throws ServletException if an error occurs
	 * @throws IOException      if an error occurs
	 */
	public boolean handleCommitOrderV2(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse)
			throws IOException {
		final String METHOD_NAME = "handleCommitOrderV2";
		final String HANDLE_METHOD = "DigitalCommitOrderFormHandler." + METHOD_NAME;
		boolean retValue = false;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(HANDLE_METHOD)) {
			DigitalObjectMapper mapper = new DigitalObjectMapper();
			request = mapper.readValue(commitOrderRequest, DigitalOrderRequest.class);
			initFormFields();

			super.resetFormExceptions();


			if (PAYMENT_TYPE_AFTER_PAY.equalsIgnoreCase(request.getPaymentType())) {

				String token=(String)request.getAfterPay().getToken();
				DigitalOrderImpl order=((DigitalOrderImpl) getOrder());
				if(DigitalStringUtil.isNotEmpty(token)){
					//Call capture payment and create afterPayPayment Group and update on order
					AfterPayOrderResponse afterPayOrderResponse=null;
					try {
						getAfterPayTools().updateOrderData(order,afterPayOrderResponse,token);
					} catch (CommerceException | DigitalAppException e) {
						if (isLoggingError()) {
							logError(e);
						}
					}

				}else{
					addFormException(new DropletException(
							getMessageLocator().getMessageString(DigitalAfterPayConstants.MISSING_AFTERPAY_TOKEN),
							DigitalAfterPayConstants.MISSING_AFTERPAY_TOKEN));
				}
				}

			if (PAYMENT_TYPE_CC.equalsIgnoreCase(request.getPaymentType())) {
				// Moved vantivfirstsix mandatory check from validation happening in the
				// pipeline as we have to have this check only in case of new credit card scenario as
				// we don't need this field for existing card flows Vantiv first six is used to
				// determine the creditcard type
				if (request.getCreditCard().isNewCreditCardPayment()
						&& DigitalStringUtil
						.isEmpty(request.getCreditCard().getNewCard().getVantivFirstSix())) {
					addFormException(new DropletException(
							getMessageLocator().getMessageString(DigitalProfileConstants.MISSING_VANTIV_FIRST_SIX),
							DigitalProfileConstants.MISSING_VANTIV_FIRST_SIX));
					return true;
				}
			}

			final UserTransactionDemarcation td = TransactionUtils
					.startNewTransaction(this.getName(), METHOD_NAME);
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				if (isGiftCardInGiftCardPaymentMethod(getOrder())) {
					String msg = getMessageLocator().getMessageString("giftcard.payment.onlygiftcardinorder");
					addFormException(new DropletException(msg, "giftcard.payment.onlygiftcardinorder"));
					return true;
				}

				preCommitValidateRewards();

				if (this.getFormError()){
					return true;
				}

				retValue = super.handleCommitOrder(pRequest, pResponse);

				if (this.getFormError() || this.isTransactionMarkedAsRollBack()) {
					DigitalOrderImpl pOrder = (DigitalOrderImpl) getOrder();
					List<PaymentGroup> paymentGroupList = pOrder.getPaymentGroups();
					if (paymentGroupList != null) {
						for (PaymentGroup pg : paymentGroupList) {
							if (pg != null && pg instanceof PaypalPayment
									&& pg.getState() == StateDefinitions.PAYMENTGROUPSTATES
									.getStateValue(PaymentGroupStates.AUTHORIZE_FAILED)) {
								super.resetFormExceptions();
								String msg = getMessageLocator().getMessageString(PAYPAL_GENERAL_ERROR);
								addFormException(new DropletException(msg, PAYPAL_GENERAL_ERROR));
							}
						}
					}
				}

				if (this.getFormError() || this.isTransactionMarkedAsRollBack()) {
					DigitalOrderImpl pOrder = (DigitalOrderImpl) getOrder();
					List<PaymentGroup> paymentGroupList = pOrder.getPaymentGroups();
					if (paymentGroupList != null) {
						for (PaymentGroup pg : paymentGroupList) {
							if (pg != null && pg instanceof AfterPayPayment
									&& pg.getState() == StateDefinitions.PAYMENTGROUPSTATES
									.getStateValue(PaymentGroupStates.AUTHORIZE_FAILED)) {
								super.resetFormExceptions();
								String msg = getMessageLocator().getMessageString(AFTERPAY_GENERAL_ERROR);
								addFormException(new DropletException(msg, AFTERPAY_GENERAL_ERROR));
							}
						}
					}
				}

				if (((DigitalOrderImpl) getOrder()).isTaxOffline()) {
					warningMessages.add(getMessageLocator().getMessageString("taxOffline"));
				}

			} catch (Exception ex) {
				TransactionUtils.rollBackTransaction(td, this.getName(), METHOD_NAME);
				if (isLoggingError()) {
					logError(ex);
				}
			} finally {
				TransactionUtils.endTransaction(td, this.getName(), METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(HANDLE_METHOD);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
		return retValue;
	}

	/**
	 * Initialize the form fields based on input request
	 */
	private void initFormFields() {
		if (request != null) {
			setExchange(request.isExchange());
			setPaymentType(request.getPaymentType());
			switch (paymentType) {
				case PAYMENT_TYPE_CC:
					setNewCreditCardPayment(request.getCreditCard().isNewCreditCardPayment());
					setCvvNumber(request.getCreditCard().getCvv());
					if (request.getCreditCard().isNewCreditCardPayment()) {
						setCreditCardType(request.getCreditCard().getNewCard().getCreditCardType());
						setNameOnCard(request.getCreditCard().getNewCard().getNameOnCard());
						setExpirationYear(request.getCreditCard().getNewCard().getExpirationYear());
						setExpirationMonth(request.getCreditCard().getNewCard().getExpirationMonth());
						setVantivLitleTxnId(request.getCreditCard().getNewCard().getVantivLitleTxnId());
						setVantivPaypageRegistrationId(
								request.getCreditCard().getNewCard().getVantivPaypageRegistrationId());
						setVantivFirstSix(request.getCreditCard().getNewCard().getVantivFirstSix());
						setVantivLastFour(request.getCreditCard().getNewCard().getVantivLastFour());
						setVantivCode(request.getCreditCard().getNewCard().getVantivResponse());
						setBillingSameAsShippingAddress(
								request.getCreditCard().getNewCard().isBillingSameAsShippingAddress());
						setCopyCreditCardToProfile(
								request.getCreditCard().getNewCard().isCopyCreditCardToProfile());
						setMakeAsPrimary(request.getCreditCard().getNewCard().isDefaultCreditCard());
					} else {
						setCreditCardKey(request.getCreditCard().getSavedCard().getCreditCardKey());
					}
					break;
				case PAYMENT_TYPE_PAYPAL:
					break;
				case PAYMENT_TYPE_AFTER_PAY:
					setAfterPayToken(request.getAfterPay().getToken());
					break;
				case PAYMENT_TYPE_GC:
					break;
				case PAYMENT_TYPE_NONE:
					break;
			}
		}
	}

  /**
   * Override the method, so it does the submit order to DSW backend. It gets called after all
   * processing is done by the handleCommitOrder method. The default implementation does nothing.
   * This method is called from within a synchronized block that synchronizes on the current order
   * object.
	 * <p/>
   * By the time this method is called the order has been committed to the database by calling
   * <tt>OrderManager.processOrder</tt>. If an application modifies the order within its own
   * <tt>postCommitOrder</tt> method, that method is responsible for saving the new state of the
   * order by calling
	 * <tt>OrderManager.updateOrder</tt>.
	 *
   * @param pRequest  the request object
   * @param pResponse the response object
   * @throws ServletException if an error occurs
   * @throws IOException      if an error occurs
	 */
  public void postCommitOrder(DynamoHttpServletRequest pRequest,
      DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		// if there are any form errors doen't proceed further
		if (this.getFormError()) {
			try {
				this.setTransactionToRollbackOnly();
			} catch (SystemException e) {
				logError("Transaction rollback exception :: ", e);
			}
			return;
		}

		DigitalOrderImpl order = (DigitalOrderImpl) getShoppingCart().getLast();
		if (order == null) {
			addFormException(new DropletException("Order is missing", "ORDER_NOT_FOUND"));
			return;
		}

		try {
			rewardCertificateManager.reserveRewardCertificates((DigitalOrderImpl) getOrder());
			postCommitOrderManager.retainLastGiftCardAuthorizationStatus((DigitalOrderImpl) getOrder());
		} catch (Exception e) {
			logError("FAILED_RESERVE_CERTIFICATE :: reserveRewardCertificates failed :: ", e);
		}

	//Check the sumitted date on the order and if its null and state is submited-- set the submitted date to current sys date
		String state=order.getStateAsString();
		Date submittedDate=order.getSubmittedDate();
		if(null==submittedDate && DigitalStringUtil.isNotEmpty(state) &&  state.equalsIgnoreCase("SUBMITTED")  ){
			logInfo("CLEANUP_SUBMITTED_DATE_ON_ORDER ::: DONE in POST Commit for ORDER ### ::: "+order.getId());
			order.setSubmittedDate(new Date());
		}


		//getFraudFlag
    boolean fraudFlag;
    fraudFlag = getOrderTools().isFraud(order);

    // Enabled ONLY in Perf to support sendOrder API call to GCP even in case on Vantiv fraud
    if (null != digitalBaseConstants && digitalBaseConstants.isByPassFraudTestOrders()) {
      fraudFlag = false;
    }

		//TODO: REW:1128 POST the GCP ordermessage to new Queue
    BtsRewardService rewardService = getPostCommitOrderManager().getProfileTools()
        .getRewardService();
    if (null != rewardService && rewardService.isServiceEnabled() && rewardService
        .isServiceMethodEnabled(
            RewardService.ServiceMethod.SEND_ORDER.getServiceMethodName())) {
      if (!fraudFlag) {
		try {
			postGCPMessage();
			order.setPointsCalculated(true);
		} catch (Exception e1) {
          if (isLoggingError()) {
            logError(String.format(
                "Caught Exception attempting to send order to GCP postGCPMsg: %1$s profile: %2$s",
                order.getId(), order.getProfileId()), e1);
			}
		}
      } else {
        logInfo("NOT POSTING ORDER TO GCP:::  For Order :: " + order.getId()
            + " ::  FRAUD ?? :: " + fraudFlag);
		}
    } else {
      logInfo("NOT POSTING ORDER TO GCP:::  For Order :: " + order.getId()
          + " ::  AS EitheR REWARDS IS OFFLINE or SEND_ORDER METHOD IS OFFLINE ?? :: ");
    }

    //this might fix some of the S12 translation errors
	  DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, PERF_NAME);
	  final String methodName = "postCommitOrder";
	  final UserTransactionDemarcation td = TransactionUtils
			  .startNewTransaction(CLASSNAME, methodName);
			try {
				synchronized (order) {
					this.getOrderManager().updateOrder(order);
				}
    } catch (CommerceException e) {
      if (isLoggingError()) {
        logError(String.format(
            "Caught CommerceException attempting to update order before postS12Msg: %1$s profile: %2$s",
            order.getId(), order.getProfileId()), e);
			}
		}finally {
			try {
				TransactionUtils.releaseTransactionLock(CLASSNAME, methodName);
				TransactionUtils.endTransaction(td, CLASSNAME, methodName);
			} catch (Throwable th) {
				logError(th);
			}
		}

		try {

			postS12Message();

      Profile profile = ComponentLookupUtil
          .lookupComponent(ComponentLookupUtil.PROFILE, Profile.class);
			postCommitOrderManager.sendOrderConfirmation(order, profile);
			postCommitOrderManager.sendOptInOptOutData(order, profile);
		} catch (Exception exc) {
			postCommitOrderManager.markIncomplete(order);
			if (getShoppingCart() != null) {
				this.setOrder(order);
			}
			String msg = getMessageLocator().getMessageString(ERR_SUBMIT_ORDER_ASXML);
			addFormException(new DropletException(msg, ERR_SUBMIT_ORDER_ASXML));
		}

		try {
			getGwpManager().getGwpMarkerManager().removeOrderMarkers(null, -1, order);
		} catch (MarkerException e) {
      if (isLoggingError()) {
        logError(String
            .format("Caught MarkerException attempting to remove GWP markers on Order:: %1$s",
                order.getId()), e);
			}
		}

		if (this.getFormError()) {
			try {
				this.setTransactionToRollbackOnly();
			} catch (SystemException e) {
				logError("Transaction rollback exception :: ", e);
			}
			return;
		}
		// clean up promotions
		postCommitOrderManager.cleanUpProfilePromotions((Profile) getProfile());
		// Save the card to profile if order is marked as complete and vantiv is
		// online
		postCommitOrderManager.updateProfile(order, (Profile) getProfile());

		updateSourceOfOrderOnCurrentOrder();

		//clear Promotions from slots
		getQualifierSlot().clear();

		// Invalidate PayPal Session attributes
    pRequest.getSession()
        .removeAttribute(SessionAttributesConstant.PAYPAL_CARDINAL_ORDER_ID.getValue());
		pRequest.getSession().removeAttribute(SessionAttributesConstant.PAYPAL_ACS_URL.getValue());
		pRequest.getSession().removeAttribute(SessionAttributesConstant.PAYPAL_PAYLOAD.getValue());
		pRequest.getSession().removeAttribute(SessionAttributesConstant.DSW_SESSION_ID.getValue());
		pRequest.getSession().removeAttribute(SessionAttributesConstant.SELECTED_PAYMENT.getValue());
		pRequest.getSession().removeAttribute(SessionAttributesConstant.PAYPAL_EMAIL.getValue());

		super.postCommitOrder(pRequest, pResponse);

	}

	private void updateSourceOfOrderOnCurrentOrder() {
		// TODO Auto-generated method stub
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, PERF_NAME);
		final String methodName = "originOfOrderUpdateEnabled";
    final UserTransactionDemarcation td = TransactionUtils
        .startNewTransaction(CLASSNAME, methodName);
		try {

			DigitalOrderImpl order = (DigitalOrderImpl) getShoppingCart().getLast();
			DigitalOrderImpl currentOrder = (DigitalOrderImpl) getShoppingCart().getCurrent();

      if (order != null && currentOrder != null) {

				try {
					synchronized (currentOrder) {
						currentOrder.setOriginOfOrder(order.getOriginOfOrder());
						this.getOrderManager().updateOrder(currentOrder);
						logInfo("CLEANUP_ORIGIN_OF_ORDER ::: DONE in POST Commit ::: ");
					}
				} catch (Exception e) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
					throw e;
				} finally {
					try {
						TransactionUtils.releaseTransactionLock(CLASSNAME, methodName);
						TransactionUtils.endTransaction(td, CLASSNAME, methodName);
					} catch (Throwable th) {
						logError(th);
					}
				}
			}
    } catch (Exception e) {
			logError("Clean-up of origin of order with the order failed");
			TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
		} finally {
			TransactionUtils.endTransaction(td, CLASSNAME, methodName);
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, PERF_NAME);
		}

	}

	private void claimBatchCoupon(DigitalOrderImpl order) {
		// TODO Auto-generated method stub
		DigitalPromotionTools pTools = (DigitalPromotionTools) getOrderTools().getPromotionTools();
    List promos = pTools.getAllPromotions(order, true);
		List<String> coupons = new ArrayList<>();
    if (null != promos && !promos.isEmpty()) {
      for (int x = 0; x < promos.size(); x++) {
        PricingAdjustment pAdj = (PricingAdjustment) promos.get(x);
			RepositoryItem claimable;
        if (pAdj.getCoupon() != null) {
          String coupon = pAdj.getCoupon().getRepositoryId();
          ClaimableTools claimableTools = getClaimableManager().getClaimableTools();
					try {

						claimable = claimableTools.getClaimableItem(coupon);
            if (null != getClaimableManager().getCouponBatchTools().getCouponBatch(coupon.trim())
                && null != claimable) {
              Integer value = (Integer) claimable
                  .getPropertyValue(claimableTools.getUsesPropertyName());
							int uses = value == null ? 0 : value.intValue();
              DynamicBeans.setPropertyValue(claimable, claimableTools.getUsesPropertyName(),
                  Integer.valueOf(uses + 1));
              ((MutableRepository) claimableTools.getClaimableRepository())
                  .updateItem((MutableRepositoryItem) claimable);
						}
          } catch (ClaimableException e) {
            logError(
                "ClaimableException:::: Exception while attempting to mark the coupon as claimed",
                e);
            String msg = this.getMessageLocator().getMessageString(MSG_UNKNOWN_COUPON_EXCEPTION,
								this.getMessageLocator().getMessageString(MSG_UNKNOWN_COUPON_EXCEPTION));
						this.addFormException(new DropletException(msg, MSG_UNKNOWN_COUPON_EXCEPTION));
          } catch (RepositoryException e) {
            logError(
                "RepositoryException:::: Exception while attempting to mark the coupon as claimed",
                e);
            String msg = this.getMessageLocator().getMessageString(MSG_UNKNOWN_COUPON_EXCEPTION,
								this.getMessageLocator().getMessageString(MSG_UNKNOWN_COUPON_EXCEPTION));
						this.addFormException(new DropletException(msg, MSG_UNKNOWN_COUPON_EXCEPTION));
          } catch (PropertyNotFoundException e) {
            logError(
                "PropertyNotFoundException:::: Exception while attempting to mark the coupon as claimed",
                e);
					}
				}
			}
		}
	}

	/**
	 * @throws Exception
	 */
	protected void postS12Message() throws Exception {
		String orderAsFullXML = null;
		DigitalOrderImpl order = (DigitalOrderImpl) getShoppingCart().getLast();
		if (order == null) {
			addFormException(new DropletException("Order is missing", "ORDER_NOT_FOUND"));
		}

		try {
			/*
			 * Getting billing address containing email and phone number
			 */
      DigitalContactInfo tmpOrderContactInfo = ((DigitalPaymentGroupManager) this.getPaymentGroupManager())
          .getBillingAddressFromOrder(order);
			// Setting email and phone from contact info to the shipping address if it is empty.
      if (tmpOrderContactInfo != null) {
        getDswShippingGroupManager()
            .setEmailPhoneToShippingAddressIfEmpty(tmpOrderContactInfo, order);
			}
		} catch (DigitalAppException e) {
			if (isLoggingError()) {
				logError("Exception while attempting to get billing address from order", e);
			}
		}
		try {

			orderAsFullXML = orderXMLGenerator.getOrderAsXML(order);

      if (null != order && isLoggingInfo()) {
				String deviceName = ServletUtil
						.getCurrentRequest()
						.getHeader(RequestHeaderAttributesConstant.AKAMAI_DEVICE_KEY.getValue());
				logInfo("Shipping type counts in Full Site for Order: " + order.getId() + ": "
						+ ((DigitalOrderImpl) getOrder()).getShipTypeCounts() + ", DeviceName = " + deviceName);
			}

			if (isLoggingDebug()) {
				logDebug("Order Submit S12 XML::" + orderAsFullXML);
			}
		} catch (Exception exc) {
			logError("Exception while generating order XML", exc);
		}

		try {
			// Submitting the OrderXml to Local S12 queue
			boolean testOrder = postCommitOrderManager.findTestOrder(order);
			if (!testOrder) {
				getOrderManagementService().submitOrder(orderAsFullXML);
			} else {
        if (null != order && isLoggingInfo()) {
					logInfo("Order has test item... no S12 for this order... order # = " + order.getId());
				}
			}
    } catch (DigitalIntegrationInactiveException ie) {
			logError(":: S12_DISABLED - ORDER_NO - " + order.getId() + " :: ");
			logError(ie.getMessage(), ie);
		} catch (Exception e1) {
			logError("Exception while submitting the order to S12 queue", e1);
			throw e1;
		}
	}


	/**
	 * @throws Exception
	 */
	protected void postGCPMessage() throws Exception {
		DigitalOrderImpl order = (DigitalOrderImpl) getShoppingCart().getLast();
		if (order == null) {
			addFormException(new DropletException("Order is missing", "ORDER_NOT_FOUND"));
		}

    RewardsSendOrderData request = new RewardsSendOrderData();
		orderTools.populateSendOrderRequest(order, request);
		postCommitOrderManager.updateSendOrderSubscriber(request);
    if (isLoggingDebug() && null != order) {
      logDebug(
          "GCP Order submitted for real Time poinsts calculations for Order ::  " + order.getId());
		}

	}

	/**
	 * Method to get the last four digits of the credit card number
   *
	 * @param creditCardNumber
	 * @return
	 */
	protected String extractLastFour(String creditCardNumber) {
    return (creditCardNumber != null) ? creditCardNumber.substring((creditCardNumber.length() - 4))
        : null;
	}

	/**
	 * @param order
	 * @return
	 */
	private Boolean isGiftCardInGiftCardPaymentMethod(Order order) {
		List pgrels = order.getPaymentGroupRelationships();
		int gcPgCount = 0;
		for (int x = 0; x < pgrels.size(); x++) {
			PaymentGroupRelationship rel = (PaymentGroupRelationship) pgrels.get(x);
			if (getGiftCardPaymentGroupType().equals(rel.getPaymentGroup().getPaymentGroupClassType())) {
				gcPgCount++;
			}
		}
		int gCItemCount = 0;
		List<CommerceItem> ciList = ((DigitalOrderImpl) getOrder()).getCommerceItems();
		for (CommerceItem ci : ciList) {
			if (ci != null && (ci instanceof GiftCardCommerceItem)) {
				gCItemCount++;
			}
		}
		if (gcPgCount > 0 && gCItemCount > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param order
	 * @return
	 */
	private Boolean isOnlyGiftCardInPaymentMethod(Order order) {
		List pgrels = order.getPaymentGroupRelationships();
		int gcPgCount = 0;
		for (int x = 0; x < pgrels.size(); x++) {
			PaymentGroupRelationship rel = (PaymentGroupRelationship) pgrels.get(x);
			if (getGiftCardPaymentGroupType().equals(rel.getPaymentGroup().getPaymentGroupClassType())) {
				gcPgCount++;
			}
		}
		if (gcPgCount > 0 && pgrels.size() == gcPgCount) {
			return true;
		} else {
			return false;
		}
	}

	/**
   *
	 * @param orderPlaced
	 * @param orderPlacedBy
	 */


	/**
   * Override the method to stop commit & post commit steps if there is any form exceptions during
   * validation. Rollback the transaction.
   *
	 * @param pSuccessURL
	 * @param pFailureURL
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 */
  public boolean checkFormRedirect(String pSuccessURL, String pFailureURL,
      DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
    if ((getSessionExpirationURL() != null) && (pRequest.getSession().isNew()) && (isFormSubmission(
        pRequest))) {
			RepositoryItem profile;

			if (((profile = getProfile()) == null) && ((profile = defaultUserProfile(true)) == null)
					&& (isLoggingError())) {
				logError("missingExpiredSessionProfile");
			}

			if ((profile != null) && (profile.isTransient())) {
        if (isLoggingDebug()) {
					logDebug("Session expired: redirecting to " + getSessionExpirationURL());
        }
				redirectOrForward(pRequest, pResponse, getSessionExpirationURL());
				return false;
			}
		}

		if (isTransactionMarkedAsRollBack()) {
			if (isLoggingDebug()) {
				logDebug("Transaction Marked as Rollback - redirecting to: " + pFailureURL);
			}
			return false;
		}

		return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
	}
}