package com.digital.commerce.services.common;

import java.util.Arrays;
import java.util.Iterator;

import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.google.common.collect.Iterables;
import lombok.Getter;

/** An enum of states including those of the USA and Canada.
 * 
 * @author <a href="mailto:knaas@dswinc.com">knaas</a> */
@Getter
public enum StateCode {

	ALABAMA("AL", CountryCode.USA), ALASKA("AK", CountryCode.USA), ARIZONA("AZ", CountryCode.USA), ARKANSAS("AR", CountryCode.USA), CALIFORNIA("CA", CountryCode.USA), COLORADO("CO", CountryCode.USA), CONNECTICUT("CT", CountryCode.USA), DELAWARE(
			"DE", CountryCode.USA), DISTRICT_OF_COLUMBIA("DC", CountryCode.USA), FLORIDA("DC", CountryCode.USA), GEORGIA("GA", CountryCode.USA), HAWAII("HI", CountryCode.USA), IDAHO("ID", CountryCode.USA), ILLINOIS("IL", CountryCode.USA), INDIANA(
			"IN", CountryCode.USA), IOWA("IA", CountryCode.USA), KANSAS("KS", CountryCode.USA), KENTUCKY("KY", CountryCode.USA), LOUISIANA("LA", CountryCode.USA), MAINE("ME", CountryCode.USA), MARYLAND("MD", CountryCode.USA), MASSACHUSETTS("MA",
			CountryCode.USA), MICHIGAN("MI", CountryCode.USA), MINNESOTA("MN", CountryCode.USA), MISSISSIPPI("MS", CountryCode.USA), MISSOURI("MO", CountryCode.USA), MONTANA("MT", CountryCode.USA), NEBRASKA("NE", CountryCode.USA), NEVADA("NV",
			CountryCode.USA), NEW_HAMPSHIRE("NH", CountryCode.USA), NEW_JERSEY("NJ", CountryCode.USA), NEW_MEXICO("NM", CountryCode.USA), NEW_YORK("NY", CountryCode.USA), NORTH_CAROLINA("NC", CountryCode.USA), NORTH_DAKOTA("ND", CountryCode.USA), OHIO(
			"OH", CountryCode.USA), OKLAHOMA("OK", CountryCode.USA), OREGON("OR", CountryCode.USA), PENNSYLVANIA("PA", CountryCode.USA), PUERTO_RICO("PR", CountryCode.USA), RHODE_ISLAND("RI", CountryCode.USA), SOUTH_CAROLINA("SC", CountryCode.USA), SOUTH_DAKOTA(
			"SD", CountryCode.USA), TENNESSEE("TN", CountryCode.USA), TEXAS("TX", CountryCode.USA), UTAH("UT", CountryCode.USA), VERMONT("VT", CountryCode.USA), VIRGINIA("VA", CountryCode.USA), WASHINGTON("WA", CountryCode.USA), WEST_VIRGINIA("WV",
			CountryCode.USA), WISCONSIN("WI", CountryCode.USA), WYOMING("WY", CountryCode.USA)

	, ALBERTA("AB", CountryCode.CANADA), BRITISH_COLUMBIA("BC", CountryCode.CANADA), MANITOBA("MB", CountryCode.CANADA), NEW_BRUNSWICK("NB", CountryCode.CANADA), NEWFOUNDLAND("NF", CountryCode.CANADA), NOVA_SCOTIA("NS", CountryCode.CANADA), NORTHWEST_TERRITORIES(
			"NT", CountryCode.CANADA), ONTARIO("ON", CountryCode.CANADA), PRINCE_EDWARD_ISLAND("PE", CountryCode.CANADA), QUEBEC("QC", CountryCode.CANADA), SASKATCHEWAN("SK", CountryCode.CANADA), YUKON("YT", CountryCode.CANADA);

	private final String		value;
	private final CountryCode	countryCode;

	private StateCode( String value, CountryCode countryCode ) {
		this.value = value;
		this.countryCode = countryCode;
	}
	

	/** Looks up the state code for the given value.
	 * 
	 * @param value
	 * @return */
	public static StateCode valueFor( final String value ) {
		final String trimmedValue = DigitalStringUtil.trimToEmpty( value );
		final Iterator<StateCode> filtered = Iterables.filter( Arrays.asList( values() ), new DigitalPredicate<StateCode>() {

			@Override
			public boolean apply( StateCode input ) {
				return DigitalStringUtil.equalsIgnoreCase( DigitalStringUtil.trimToEmpty( input.getValue() ), trimmedValue );
			}

		} ).iterator();
		return filtered.hasNext() ? filtered.next() : null;
	}
}
