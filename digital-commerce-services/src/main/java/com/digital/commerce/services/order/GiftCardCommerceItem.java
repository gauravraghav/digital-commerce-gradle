package com.digital.commerce.services.order;

import java.math.BigDecimal;
import java.math.RoundingMode;

import atg.repository.RemovedItemException;

public class GiftCardCommerceItem extends DigitalCommerceItem {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 6750665577295618478L;
	private static final String	ASI_CARD_PRICE		= "asiCardPrice";
	private static final String	ASI_ID				= "asiId";
	private static final String	ASI_INVOICE_AMOUNT	= "asiInvoiceAmount";
	private static final String	ASI_ORDER_NUMBER	= "asiOrderNumber";
	private static final String	ASI_POSTAGE			= "asiPostage";
	private static final String	BAR_CODE			= "barCode";
	private static final String	CARD_PRICE			= "cardPrice";
	private static final String	CARD_QTY			= "cardQty";
	private static final String	CLIENT_ORDER_NUMBER	= "clientOrderNumber";
	private static final String	CLT_AFFILIATE_ID	= "cltAffiliateId";
	private static final String	DOLLAR_VALUE		= "dollarValue";
	private static final String	POSTAGE				= "postage";
	private static final String	PRODUCT_ID			= "productId";
	private static final String	DB_ID				= "dbId";
	private static final String	PRODUCT_TYPE		= "productType";
	private static final String	GIFT_CARD_NUMBER	= "giftCardNumber";
	private static final String	PIN					= "pin";
	private static final String	RECIPIENT_NAME 		= "recipientName";
	private static final String	RECIPIENT_EMAIL		= "recipientEmail";
	private static final String	SENDER_NAME 		= "senderName";
	private static final String	SENDER_EMAIL		= "senderEmail";

	public Double getAsiCardPrice() {
		return (Double)getPropertyValue( ASI_CARD_PRICE );
	}

	public void setAsiCardPrice( Double asiCardPrice ) {
		setPropertyValue( ASI_CARD_PRICE, asiCardPrice );
	}

	public String getAsiId() {
		return (String)getPropertyValue( ASI_ID );
	}

	public void setAsiId( String asiId ) {
		setPropertyValue( ASI_ID, asiId );
	}

	public Double getAsiInvoiceAmount() {
		return (Double)getPropertyValue( ASI_INVOICE_AMOUNT );
	}

	public void setAsiInvoiceAmount( Double asiInvoiceAmount ) {
		setPropertyValue( ASI_INVOICE_AMOUNT, asiInvoiceAmount );
	}

	public String getAsiOrderNumber() {
		return (String)getPropertyValue( ASI_ORDER_NUMBER );
	}

	public void setAsiOrderNumber( String asiOrderNumber ) {
		setPropertyValue( ASI_ORDER_NUMBER, asiOrderNumber );
	}

	public Double getAsiPostage() {
		return (Double)getPropertyValue( ASI_POSTAGE );
	}

	public void setAsiPostage( Double asiPostage ) {
		setPropertyValue( ASI_POSTAGE, asiPostage );
	}

	public String getBarCode() {
		return (String)getPropertyValue( BAR_CODE );
	}

	public void setBarCode( String barCode ) {
		setPropertyValue( BAR_CODE, barCode );
	}

	public Double getCardPrice() {
		return (Double)getPropertyValue( CARD_PRICE );
	}

	public void setCardPrice( Double cardPrice ) {
		setPropertyValue( CARD_PRICE, cardPrice );
	}

	public Integer getCardQty() {
		return (Integer)getPropertyValue( CARD_QTY );
	}

	public void setCardQty( Integer cardQty ) {
		setPropertyValue( CARD_QTY, cardQty );
	}

	public String getClientOrderNumber() {
		return (String)getPropertyValue( CLIENT_ORDER_NUMBER );
	}

	public void setClientOrderNumber( String clientOrderNumber ) {
		setPropertyValue( CLIENT_ORDER_NUMBER, clientOrderNumber );
	}

	public Integer getCltAffiliateId() {
		return (Integer)getPropertyValue( CLT_AFFILIATE_ID );
	}

	public void setCltAffiliateId( Integer cltAffiliateId ) {
		setPropertyValue( CLT_AFFILIATE_ID, cltAffiliateId );
	}

	public Integer getDollarValue() {
		return (Integer)getPropertyValue( DOLLAR_VALUE );
	}

	public void setDollarValue( Integer dollarValue ) {
		setPropertyValue( DOLLAR_VALUE, dollarValue );
	}

	public Double getPostage() {
		return (Double)getPropertyValue( POSTAGE );
	}

	public void setPostage( Double postage ) {
		setPropertyValue( POSTAGE, postage );
	}

	public String getProductId() {
		return (String)getPropertyValue( PRODUCT_ID );
	}

	public void setProductId( String productId ) {
		setPropertyValue( PRODUCT_ID, productId );
	}

	public Integer getDbId() {
		return (Integer)getPropertyValue( DB_ID );
	}

	public void setDbId( Integer dbId ) {
		setPropertyValue( DB_ID, dbId );
	}

	public String getProductType() {
		return (String)getPropertyValue( PRODUCT_TYPE );
	}

	public void setProductType( String productType ) {
		setPropertyValue( PRODUCT_TYPE, productType );
	}

	public String getPin() {
		return (String)getPropertyValue( PIN );
	}

	public void setPin( String pin ) {
		setPropertyValue( PIN, pin );
	}

	public String getGiftCardNumber() {
		return (String)getPropertyValue( GIFT_CARD_NUMBER );
	}

	public void setGiftCardNumber( String giftCardNumber ) {
		setPropertyValue( GIFT_CARD_NUMBER, giftCardNumber );
	}

	public String getRecipientEmail() {
		return (String)getPropertyValue( RECIPIENT_EMAIL );
	}

	public void setRecipientEmail( String recipientEmail ) {
		setPropertyValue( RECIPIENT_EMAIL, recipientEmail );
	}
	
	public String getRecipientName() {
		return (String)getPropertyValue( RECIPIENT_NAME );
	}

	public void setRecipientName( String recipientName ) {
		setPropertyValue( RECIPIENT_NAME, recipientName );
	}
	
	public String getSenderEmail() {
		return (String)getPropertyValue( SENDER_EMAIL );
	}

	public void setSenderEmail( String senderEmail ) {
		setPropertyValue( SENDER_EMAIL, senderEmail );
	}
	
	public String getSenderName() {
		return (String)getPropertyValue( SENDER_NAME );
	}

	public void setSenderName( String senderName ) {
		setPropertyValue( SENDER_NAME, senderName );
	}
	
	public double getGiftCardPriceTotal() {
		//As per new requirement (PLAT-1580) take Total amount of the gift cards should be mapped to "DollarValue + Card price" times CardQty 
		BigDecimal amount = ( getCardPrice() == null ) ? new BigDecimal( "0" ).setScale( 2, RoundingMode.HALF_UP ) : new BigDecimal( getCardPrice().doubleValue() ).setScale( 2, RoundingMode.HALF_UP );
		amount = amount.add( ( getDollarValue() == null ) ? new BigDecimal( "0" ).setScale( 2, RoundingMode.HALF_UP ) : new BigDecimal( getDollarValue().intValue() ) ).setScale( 2, RoundingMode.HALF_UP );
		amount = amount.multiply( ( getCardQty() == null ) ? new BigDecimal( "0" ).setScale( 2, RoundingMode.HALF_UP ) : new BigDecimal( getCardQty().intValue() ) ).setScale( 2, RoundingMode.HALF_UP );
		return amount.doubleValue();
		
	}

	public String toString() {
		StringBuilder sb = new StringBuilder( super.toString() );
		try {
			sb.append( "<--GiftCardCommerceItem[" );

			Double asiCardPrice = getAsiCardPrice();
			sb.append( "asiCardPrice:" ).append( asiCardPrice ).append( "; " );
			String asiId = getAsiId();
			sb.append( "asiId:" ).append( asiId ).append( "; " );
			Double asiInvoiceAmount = getAsiInvoiceAmount();
			sb.append( "asiInvoiceAmount:" ).append( asiInvoiceAmount ).append( "; " );
			String asiOrderNumber = getAsiOrderNumber();
			sb.append( "asiOrderNumber:" ).append( asiOrderNumber ).append( "; " );
			Double asiPostage = getAsiPostage();
			sb.append( "asiPostage:" ).append( asiPostage ).append( "; " );
			String barCode = getBarCode();
			sb.append( "barCode:" ).append( barCode ).append( "; " );
			Double cardPrice = getCardPrice();
			sb.append( "cardPrice:" ).append( cardPrice ).append( "; " );
			Integer cardQty = getCardQty();
			sb.append( "cardQty:" ).append( cardQty ).append( "; " );
			String clientOrderNumber = getClientOrderNumber();
			sb.append( "clientOrderNumber:" ).append( clientOrderNumber ).append( "; " );
			Integer cltAffiliateId = getCltAffiliateId();
			sb.append( "cltAffiliateId:" ).append( cltAffiliateId ).append( "; " );
			Integer dbId = getDbId();
			sb.append( "dbId:" ).append( dbId ).append( "; " );
			Integer dollarValue = getDollarValue();
			sb.append( "dollarValue:" ).append( dollarValue ).append( "; " );
			String giftCardNumber = getGiftCardNumber();
			sb.append( "giftCardNumber:" ).append( giftCardNumber ).append( "; " );
			double giftCardPriceTotal = getGiftCardPriceTotal();
			sb.append( "giftCardPriceTotal:" ).append( giftCardPriceTotal ).append( "; " );
			String pin = getPin();
			sb.append( "pin:" ).append( pin ).append( "; " );
			Double postage = getPostage();
			sb.append( "postage:" ).append( postage ).append( "; " );
			String productId = getProductId();
			sb.append( "productId:" ).append( productId ).append( "; " );
			String productType = getProductType();
			sb.append( "productType:" ).append( productType );
		} catch( RemovedItemException exc ) {
			sb.append( "removed" );
		}
		sb.append( "]" );
		return sb.toString();
	}

}
