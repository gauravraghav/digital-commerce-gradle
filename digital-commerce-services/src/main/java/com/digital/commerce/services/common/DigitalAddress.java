package com.digital.commerce.services.common;

import com.google.common.base.Objects;

import atg.core.util.Address;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DigitalAddress extends Address {

	/**
    *
    */
	private static final long serialVersionUID = -4661166814510786799L;

	private String rank;

	private String addressType = AddressType.USA.getValue();

	private String region;

	private Boolean addressVerification = Boolean.FALSE;

	private Boolean isPoBox = Boolean.FALSE;
	private String pobox;

	private String businessName;

	private boolean giftCardAddress = false;
	private String emailConfirm;

	/** Hook that forces address verification from the jsp */
	private boolean doAddressVerification = false;

	public void setRegion(String region) {
		this.region = region;
		if (null != region) {
			setState(region);
		}
	}
}
