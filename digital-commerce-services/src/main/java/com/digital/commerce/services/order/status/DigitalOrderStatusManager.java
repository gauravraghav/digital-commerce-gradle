package com.digital.commerce.services.order.status;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.commerce.CommerceException;
import atg.commerce.order.OrderManager;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.digital.commerce.constants.OrderConstants.OrderPropertyManager;
import com.digital.commerce.integration.order.processor.OrderStatusEvent;
import com.digital.commerce.services.order.DigitalOrderHistoryItem;
import com.digital.commerce.services.order.DigitalOrderHistoryModel;
import com.digital.commerce.services.order.DigitalOrderLookupService;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.profile.DigitalProfileTools;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalOrderStatusManager extends ApplicationLoggingImpl{
	
	private DigitalOrderLookupService		orderLookupService;
	private DigitalProfileTools				profileTools;
	private OrderManager				orderManager;
	
	public void updateOrderStatus(DigitalOrderHistoryModel orderHistory, OrderStatusEvent orderStatusEvent){
		if(orderHistory == null){
			logError("DigitalOrderHistoryModel is null");
			return;
		}
		try {
			
			RepositoryItem orderStatus = this.getOrderLookupService().orderExists(orderHistory.getOrderId());
			if(orderStatus == null){
				persistOrderStatus(orderHistory);
			}else{
				updateOrderStatus(orderStatus, orderHistory );
			}
		} catch (CommerceException | RepositoryException e) {
			logError(e);
		}
	}

	protected void persistOrderStatus(DigitalOrderHistoryModel orderHistory) throws CommerceException, RepositoryException{
		MutableRepositoryItem orderStatusRepo = (MutableRepositoryItem)this.getOrderLookupService().orderExists(orderHistory.getOrderId());
		if(null == orderStatusRepo){
			MutableRepository orderRepo = (MutableRepository)this.getOrderManager().getOrderTools().getOrderRepository();
			orderStatusRepo = orderRepo.createItem(OrderPropertyManager.DSW_ORDER_HISTORY.getValue());
			
			orderStatusRepo.setPropertyValue("addDate", orderHistory.getAddDate() );
			orderStatusRepo.setPropertyValue("currencyCode", orderHistory.getCurrencyCode());
			orderStatusRepo.setPropertyValue("demandLocation",orderHistory.getDemandLocation());
			orderStatusRepo.setPropertyValue("locale", orderHistory.getLocale());
			orderStatusRepo.setPropertyValue("loyaltyNumber", orderHistory.getLoyalNumber());
			orderStatusRepo.setPropertyValue("profileId", orderHistory.getProfileId());
			orderStatusRepo.setPropertyValue("orderId", orderHistory.getOrderId());
			orderStatusRepo.setPropertyValue("orderPlacementDate", orderHistory.getOrderPlacementDate());
			orderStatusRepo.setPropertyValue("siteId", orderHistory.getSiteId());
			orderStatusRepo.setPropertyValue("status", orderHistory.getStatus());
			orderStatusRepo.setPropertyValue("statusDate", orderHistory.getStatusDate());
			orderStatusRepo.setPropertyValue("total", orderHistory.getTotal());
			Set<RepositoryItem> dswCommerceItems = new HashSet<>();
			List<DigitalOrderHistoryItem> dswOrderHistoryItems = orderHistory.getCommerceItems();
			for(DigitalOrderHistoryItem orderHistoryItem : dswOrderHistoryItems){
				MutableRepositoryItem dswCommerceItemRepo = orderRepo.createItem(OrderPropertyManager.DSW_COMMERCE_ITEM.getValue());
				dswCommerceItemRepo.setPropertyValue("addDate",orderHistoryItem.getAddDate());
				dswCommerceItemRepo.setPropertyValue("fulfillLocationId",orderHistoryItem.getFulfillLocationId());
				dswCommerceItemRepo.setPropertyValue("orderPlacementDate",orderHistoryItem.getOrderPlacementDate());
				dswCommerceItemRepo.setPropertyValue("pickupLocationId",orderHistoryItem.getPickupLocationId());
				dswCommerceItemRepo.setPropertyValue("productDescription",orderHistoryItem.getProductDescription());
				dswCommerceItemRepo.setPropertyValue("productId",orderHistoryItem.getProductId());
				dswCommerceItemRepo.setPropertyValue("quantity",orderHistoryItem.getQuantity());
				dswCommerceItemRepo.setPropertyValue("returnLocationId",orderHistoryItem.getReturnLocationId());
				dswCommerceItemRepo.setPropertyValue("shipToFirstName",orderHistoryItem.getShipToFirstName());
				dswCommerceItemRepo.setPropertyValue("shipToLastName",orderHistoryItem.getShipToLastName());
				dswCommerceItemRepo.setPropertyValue("skuId",orderHistoryItem.getSkuId());
				dswCommerceItemRepo.setPropertyValue("status",orderHistoryItem.getStatus());
				dswCommerceItemRepo.setPropertyValue("statusDate",orderHistoryItem.getStatusDate());
				dswCommerceItemRepo.setPropertyValue("trackingNumber",orderHistoryItem.getTrackingNumber());
				dswCommerceItemRepo.setPropertyValue("unitPrice",orderHistoryItem.getUnitPrice());
				dswCommerceItemRepo.setPropertyValue("updateDate",orderHistoryItem.getUpdateDate());				
				dswCommerceItems.add(dswCommerceItemRepo);
			}
			
			orderStatusRepo.setPropertyValue("commerceItems", dswCommerceItems);
		}
		//MutableRepositoryItem orderHistoryRepo = (MutableRepositoryItem)this.getOrderManager().getOrderTools().getOrderRepository().getView("dsworderhistory");
	}
	
	@SuppressWarnings("unchecked")
	protected void updateOrderStatus(RepositoryItem orderStatus, DigitalOrderHistoryModel orderHistory) throws CommerceException, RepositoryException{
		if( null != orderStatus ){
			MutableRepository orderRepo = (MutableRepository)this.getOrderManager().getOrderTools().getOrderRepository();
			MutableRepositoryItem orderStatusRepo = (MutableRepositoryItem)orderStatus;
			
			orderStatusRepo.setPropertyValue("updateDate", Calendar.getInstance().getTime());
			orderStatusRepo.setPropertyValue("currencyCode", orderHistory.getCurrencyCode());
			orderStatusRepo.setPropertyValue("demandLocation",orderHistory.getDemandLocation());
			orderStatusRepo.setPropertyValue("locale", orderHistory.getLocale());
			orderStatusRepo.setPropertyValue("loyaltyNumber", orderHistory.getLoyalNumber());
			//orderStatusRepo.setPropertyValue("orderId", orderHistory.getOrderId());
			orderStatusRepo.setPropertyValue("orderPlacementDate", orderHistory.getOrderPlacementDate());
			orderStatusRepo.setPropertyValue("siteId", orderHistory.getSiteId());
			orderStatusRepo.setPropertyValue("status", orderHistory.getStatus());
			orderStatusRepo.setPropertyValue("statusDate", orderHistory.getStatusDate());
			if(orderHistory.getTotal() > 0){
				orderStatusRepo.setPropertyValue("total", orderHistory.getTotal());
			}
			
			Set<RepositoryItem> orderStatusCommerceItemsRepo = (Set<RepositoryItem>)orderStatusRepo.getPropertyValue( OrderPropertyManager.DSW_COMMERCE_ITEM.getValue() );
			
			Set<RepositoryItem> dswCommerceItems = new HashSet<>();
			List<DigitalOrderHistoryItem> dswOrderHistoryItems = orderHistory.getCommerceItems();
			for(DigitalOrderHistoryItem orderHistoryItem : dswOrderHistoryItems){
				MutableRepositoryItem dswCommerceItemRepo = findOrderLineItem(orderStatusCommerceItemsRepo, orderHistoryItem.getYantraLineItemId(), orderHistoryItem.getAtgCommerceId());
				if(dswCommerceItemRepo == null){
					dswCommerceItemRepo = orderRepo.createItem(OrderPropertyManager.DSW_COMMERCE_ITEM.getValue());
					dswCommerceItemRepo.setPropertyValue("addDate", Calendar.getInstance().getTime());
				}
				
				dswCommerceItemRepo.setPropertyValue("yantraLineItem", orderHistoryItem.getYantraLineItemId());
				dswCommerceItemRepo.setPropertyValue("atgCommerceItemId",orderHistoryItem.getAtgCommerceId());
				
				dswCommerceItemRepo.setPropertyValue("updateDate", Calendar.getInstance().getTime());
				dswCommerceItemRepo.setPropertyValue("fulfillLocationId",orderHistoryItem.getFulfillLocationId());
				dswCommerceItemRepo.setPropertyValue("orderPlacementDate",orderHistoryItem.getOrderPlacementDate());
				dswCommerceItemRepo.setPropertyValue("pickupLocationId",orderHistoryItem.getPickupLocationId());
				dswCommerceItemRepo.setPropertyValue("productDescription",orderHistoryItem.getProductDescription());
				dswCommerceItemRepo.setPropertyValue("productId",orderHistoryItem.getProductId());
				dswCommerceItemRepo.setPropertyValue("quantity",orderHistoryItem.getQuantity());
				dswCommerceItemRepo.setPropertyValue("returnLocationId",orderHistoryItem.getReturnLocationId());
				dswCommerceItemRepo.setPropertyValue("shipToFirstName",orderHistoryItem.getShipToFirstName());
				dswCommerceItemRepo.setPropertyValue("shipToLastName",orderHistoryItem.getShipToLastName());
				dswCommerceItemRepo.setPropertyValue("skuId",orderHistoryItem.getSkuId());
				dswCommerceItemRepo.setPropertyValue("status",orderHistoryItem.getStatus());
				dswCommerceItemRepo.setPropertyValue("statusDate",orderHistoryItem.getStatusDate());
				dswCommerceItemRepo.setPropertyValue("trackingNumber",orderHistoryItem.getTrackingNumber());
				dswCommerceItemRepo.setPropertyValue("unitPrice",orderHistoryItem.getUnitPrice());
				
				dswCommerceItems.add(dswCommerceItemRepo);
			}
			
			orderStatusRepo.setPropertyValue("commerceItems", dswCommerceItems);
		}
	}
	
	protected MutableRepositoryItem findOrderLineItem(Set<RepositoryItem> orderStatusCommerceItemsRepo, String orderLineKey, String atgCommerceId){
		MutableRepositoryItem repoItem = null;
		if(orderStatusCommerceItemsRepo != null && orderStatusCommerceItemsRepo.size() > 0){
			for(RepositoryItem orderStatusCommerceItem : orderStatusCommerceItemsRepo){
				String yantraLineItemiD = (String)orderStatusCommerceItem.getPropertyValue("yantraLineItem");
				if(orderLineKey.equalsIgnoreCase(yantraLineItemiD)){
					repoItem = (MutableRepositoryItem)orderStatusCommerceItem;
					break;
				}
			}
			
			if(repoItem == null){
				for(RepositoryItem orderStatusCommerceItem : orderStatusCommerceItemsRepo){
					String atgCommerceItemId = (String)orderStatusCommerceItem.getPropertyValue("atgCommerceItemId");
					if(atgCommerceId.equalsIgnoreCase(atgCommerceItemId)){
						repoItem = (MutableRepositoryItem)orderStatusCommerceItem;
						break;
					}
				}
			}
		}
		
		return repoItem;
	}

	public DigitalOrderTools getOrderTools() {
		return (DigitalOrderTools)this.getOrderManager().getOrderTools();
	}
}
