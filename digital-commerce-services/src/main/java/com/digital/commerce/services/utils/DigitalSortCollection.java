package com.digital.commerce.services.utils;

import atg.service.actor.ModelBeanImpl;
import atg.service.actor.ModelMap;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalSortCollection {

    /**
     * records the service requested time in response header.
     *
     * @param data
     * @param attribute
     * @return Map
     */
    public Map sortMapData(ModelMap data, String attribute) {
        Map result = new HashMap();
        if (data != null && data.size() > 0){
            if(data.containsKey("resultsAreFromSearch")) {
                data.remove("resultsAreFromSearch");
            }
            if(data.containsKey("resultSetSize")) {
                data.remove("resultSetSize");
            }
        }
        Map sortedMap = new TreeMap(new AttributeComparator(data, attribute));
        sortedMap.putAll(data);
        result.put("sortedlist", sortedMap);
        return result;
    }

    class AttributeComparator implements Comparator<String> {
        String attribute;
        ModelMap map;

        /**
         * @param map
         * @param attribute
         */
        public AttributeComparator(ModelMap map, String attribute) {
            this.attribute = attribute;
            this.map = map;
        }

        /**
         * @param id1
         * @param id2
         * @return int
         */
        public int compare(String id1, String id2) {
            if (this.map.get(id1) instanceof ModelBeanImpl
                    && this.map.get(id2) instanceof ModelBeanImpl) {
                ModelBeanImpl v1 = (ModelBeanImpl) this.map.get(id1);
                ModelBeanImpl v2 = (ModelBeanImpl) this.map.get(id2);
                if (v1.get(this.attribute) instanceof java.lang.Double) {
                    return ((Double) v1.get(this.attribute))
                            .compareTo((Double) v2.get(this.attribute));
                }
                if (v1.get(this.attribute) instanceof java.lang.Long) {
                    return ((Long) v1.get(this.attribute)).compareTo((Long) v2
                            .get(this.attribute));
                } else {
                    return (v1.get(this.attribute).toString())
                            .compareTo(v2.get(this.attribute).toString());
                }
            } else {
                return 0;
            }
        }
    }

}
