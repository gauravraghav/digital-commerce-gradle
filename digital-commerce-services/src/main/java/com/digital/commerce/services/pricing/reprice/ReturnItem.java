package com.digital.commerce.services.pricing.reprice;

import lombok.Getter;
import lombok.Setter;

/*  */
@Getter
@Setter
public class ReturnItem extends Item {

	private String	type	= "Return";
	private String	lineItemId;
	private int		quantity;
	private String	shippingGroupId;
	private String	customerReasonCode;
	private String	dispositionCode;
	private String	entryType;
	private String	refundOriginalShipping;
	private String	chargeForReturnShipping;
	private String	holdReturnForCC;
	private String	orderLineKey;
	private String	customerRequestExchange;

}
