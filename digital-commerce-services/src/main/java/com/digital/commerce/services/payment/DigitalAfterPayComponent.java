package com.digital.commerce.services.payment;

import atg.nucleus.logging.ApplicationLoggingImpl;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.DigitalObjectMapper;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.payment.afterpay.AfterPayCheckoutManager;
import com.digital.commerce.services.order.payment.afterpay.valueobject.AfterPayConfigurationResponse;
import com.digital.commerce.services.order.payment.afterpay.valueobject.AfterPayCreateOrderRequest;
import com.digital.commerce.services.order.payment.afterpay.valueobject.AfterPayOrderResponse;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalAfterPayComponent extends ApplicationLoggingImpl {

  private final String className = DigitalAfterPayComponent.class.getName();
  private AfterPayCheckoutManager checkoutManager;
  private DigitalOrderImpl order;
  private MessageLocator messageLocator;

  private static final String AFTER_PAY_LOOKUP_CONFIRM_URL_REQUIRED = "afterPayLookupConfirmUrlInvalid";
  private static final String AFTER_PAY_LOOKUP_CANCEL_URL_REQUIRED = "afterPayLookupCancelUrlInvalid";
  private static final String AFTER_PAY_LOOKUP_REQUEST_REQUIRED = "afterPayLookupRequestInvalid";
  private static final String AFTER_PAY_LOOKUP_ORDER_GIFTCARD_FORBIDDEN = "afterPayLookupOrderGiftcardForbidden";
  private static final String AFTER_PAY_LOOKUP_ORDER_AMOUNT_INVALID = "afterPayLookupOrderAmountInvalid";

  public DigitalAfterPayComponent() {
    super(DigitalAfterPayComponent.class.getName());
  }

  /**
   * @return AfterPayConfigurationResponse
   */
  public AfterPayConfigurationResponse getConfiguration() {
    String METHOD_NAME = "AFTERPAY_GET_CONFIGURATION";
    DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(className, METHOD_NAME);
    AfterPayConfigurationResponse response = this.getCheckoutManager().getConfiguration();
    DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(className, METHOD_NAME);
    return response;

  }

  /**
   * @param orderRequest
   * @return AfterPayOrderResponse
   */
  public AfterPayOrderResponse createOrder(String orderRequest) {
    String METHOD_NAME = "AFTERPAY_CREATE_ORDER";
    DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(className, METHOD_NAME);
    AfterPayOrderResponse response = new AfterPayOrderResponse();
    try {
      DigitalObjectMapper mapper = new DigitalObjectMapper();
      AfterPayCreateOrderRequest request =
          mapper.readValue(orderRequest, AfterPayCreateOrderRequest.class);
      List<ResponseError> genericExceptions = new ArrayList<>();
      if (DigitalStringUtil.isEmpty(request.getConfirmURL())) {
        addError(response, genericExceptions, AFTER_PAY_LOOKUP_CONFIRM_URL_REQUIRED);
      }
      if (DigitalStringUtil.isEmpty(request.getCancelURL())) {
        addError(response, genericExceptions, AFTER_PAY_LOOKUP_CANCEL_URL_REQUIRED);
      }
      if(this.getCheckoutManager().isGiftCardPresentInOrder(order)) {
        addError(response, genericExceptions, AFTER_PAY_LOOKUP_ORDER_GIFTCARD_FORBIDDEN);
      }
      if(!this.getCheckoutManager().isAmountOwedWithinLimits(order)) {
        String[] values = new String[2];
        AfterPayConfigurationResponse config = this.getCheckoutManager().getConfiguration();
        values[0] = config.getMinimumAmount();
        values[1] = config.getMaximumAmount();
        addError(response, genericExceptions, AFTER_PAY_LOOKUP_ORDER_AMOUNT_INVALID,values);
      }
      if (!genericExceptions.isEmpty()) {
        response.setGenericExceptions(genericExceptions);
        return response;
      }
      response = this.getCheckoutManager().createOrder(order, request);
      return response;
    } catch (IOException e) {
      List<ResponseError> genericExceptions = new ArrayList<>();
      addError(response, genericExceptions, AFTER_PAY_LOOKUP_REQUEST_REQUIRED);
      response.setGenericExceptions(genericExceptions);
    } catch (DigitalAppException e) {
      List<ResponseError> genericExceptions = new ArrayList<>();
      addError(response, genericExceptions, e.getErrorCode());
      response.setGenericExceptions(genericExceptions);
    } finally {
      DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(className, METHOD_NAME);
    }
    return response;
  }

  /**
   * @param response
   * @param genericExceptions
   * @param errorCode
   */
  private void addError(AfterPayOrderResponse response, List<ResponseError> genericExceptions,
                        String errorCode) {
    this.addError(response,genericExceptions,errorCode,null);
  }

    /**
     * @param response
     * @param genericExceptions
     * @param errorCode
     */
  private void addError(AfterPayOrderResponse response, List<ResponseError> genericExceptions,
      String errorCode, String[] values) {
    response.setFormError(false);
    response.setSuccess(false);
    String msg = getMessageLocator().getMessageString(errorCode);
    if(values != null && values.length > 0) {
      msg = MessageFormat.format(msg, values);
    }
    ResponseError error = new ResponseError(errorCode, msg);
    genericExceptions.add(error);
  }
}

