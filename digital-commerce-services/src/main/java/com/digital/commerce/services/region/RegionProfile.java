package com.digital.commerce.services.region;

import java.util.List;

import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class RegionProfile extends GenericService{
	
	private String state;
	private Profile profile;
	private double latitude;
	private double longitude;
	private String regionName;

	public boolean checkStateChanged(){
		if(this.profile != null){
			List<RepositoryItem> favorStores = (List)this.profile.getPropertyValue("favoriteStores");
			if(favorStores != null && favorStores.size() > 0){ //favorStores.get(0) throws array index out of bound exception
                RepositoryItem obj = favorStores.get(0);
                String state = null;
                Object stateAddrObj = obj.getPropertyValue("stateAddress");
				if(stateAddrObj != null){
                    state = (String)stateAddrObj;
                }
			    if(this.state == null){
			    	this.state = state;
			    	return true;
			    }
			    if(this.state != null && !this.state.equalsIgnoreCase(state)){
			    	this.state = state;
			    	return true;
			    }
			}
			/*
			String state = (String)(((RepositoryItem)this.profile.getPropertyValue("storeLocatorId")).getPropertyValue("state"));
			if(state != null && this.state == null){
		    	this.state = state;
		    	return true;
		    }
		    if(state != null && this.state != null && !this.state.equalsIgnoreCase(state)){
		    	this.state = state;
		    	return true;
		    }
		    */
		}
		
		return false;
	}
	
	//public getRegion
	
	

}
