/**
 *
 */
package com.digital.commerce.services.order.payment.giftcard;

import lombok.Getter;
import lombok.Setter;

/** It represents all the information which designates a gift card payment
 * 
 * @author djboyd */
@Getter
@Setter
public class GiftCardInfo {
	private String	merchantId;
	private String	divisionId;
	private String	orderNumber;
	private double	orderAmount;
	private String	currency;
	private String	giftCardNumber;
	private String	pinNumber;
	private double	amount;
	private String	memberNumber;
	private String	paymentId;
	/* Indicator to suggest PreAuth or Auth operation */
	private String	transactionType;

}
