package com.digital.commerce.services.pricing.reprice;

import lombok.Getter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.digital.commerce.services.utils.IXpathResultCallback;
@Getter
public class LoyaltyTransformationCallback implements IXpathResultCallback {

	public final static String	LOYALTY_NUMBER	= "//order/order.loyaltyId[text()]";

	private String				loyaltyNumber;

	@Override
	public void doInElement( Document d, Node match ) {
		this.loyaltyNumber = match.getTextContent();
	}
}
