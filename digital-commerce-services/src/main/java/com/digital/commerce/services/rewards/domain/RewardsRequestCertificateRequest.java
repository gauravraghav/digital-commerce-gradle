/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RewardsRequestCertificateRequest {

	private String rewardsNumber;
	private Integer rewardPoints;
}
