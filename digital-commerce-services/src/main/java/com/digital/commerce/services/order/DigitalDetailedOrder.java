package com.digital.commerce.services.order;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalDetailedOrder implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String firstName;
	private DigitalOrderImpl order;
	private int approxRewardPoints;
	private String lastName;
}