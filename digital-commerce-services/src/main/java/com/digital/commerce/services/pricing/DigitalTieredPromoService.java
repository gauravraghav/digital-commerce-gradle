package com.digital.commerce.services.pricing;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.digital.commerce.services.order.DigitalOrderPriceInfo;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.definition.MatchingObject;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
@SuppressWarnings({"rawtypes"})
public class DigitalTieredPromoService extends GenericService {
	final static String	TIER_PROD			= "tierProduct";
	final static String	TIER_BRANDS			= "tierBrands";
	final static String	TIER_CATEGORIES		= "tierCategories";
	final static int	ORDER_AMOUNT_TYPE	= 11;
	final static int	ORDER_PERCENT_TYPE	= 10;
	final static int	LOYALTY_TIER_TYPE	= 1;
	final static int	ORDER_TIER_TYPE		= 2;
	final static int	ITEM_TIER_TYPE		= 3;

	public boolean isTieredPromotion( RepositoryItem pPricingModel ) {
		if( isLoggingDebug() ) {
			logDebug( "Inside isTieredPromotion" );
		}
		Integer type = (Integer)pPricingModel.getPropertyValue( "type" );
		if( isLoggingDebug() ) {
			if( type != null ){
				logDebug( "type" + type );
			}
		}
		if( null!=type && (type.intValue() == ORDER_AMOUNT_TYPE || type.intValue() == ORDER_PERCENT_TYPE )) {
			Integer tierType = (Integer)pPricingModel.getPropertyValue( "tierType" );

			if( tierType != null ) {
				if( isLoggingDebug() ) {
					logDebug( "tierType" + tierType );
				}
				if( tierType.intValue() == LOYALTY_TIER_TYPE || tierType.intValue() == ORDER_TIER_TYPE || tierType.intValue() == ITEM_TIER_TYPE ) return true;
			}
		}
		return false;
	}

	public MatchingObject evaluateTierPromo( java.util.List pItems, RepositoryItem pPricingModel, RepositoryItem pProfile, java.util.Locale pLocale, Order pOrder, OrderPriceInfo pOrderPriceInfo,
			java.util.Map pExtraParameters ) throws PricingException {
		if( matchTierLevel( pPricingModel, pProfile, pItems, pOrder, pOrderPriceInfo ) == null ) {
			return null;
		} else {
      return new MatchingObject( pOrder, 1 );
		}
	}

	private boolean isProfileTierPromotion( RepositoryItem pPricingModel ) {
		Integer tierType = (Integer)pPricingModel.getPropertyValue( "tierType" );
		if( tierType.intValue() == LOYALTY_TIER_TYPE )
			return true;
		else
			return false;
	}

	private boolean isOrderAmountTierPromotion( RepositoryItem pPricingModel ) {
		if( isLoggingDebug() ) {
			logDebug( "insdie isOrderAmountTierPromotion()" );
		}
		Integer tierType = (Integer)pPricingModel.getPropertyValue( "tierType" );
		if( isLoggingDebug() ) {
			if( tierType != null ) {
				logDebug( "tierType" + tierType );
			}
		}
		if( null!=tierType && tierType.intValue() == ORDER_TIER_TYPE )
			return true;
		else
			return false;
	}

	private boolean isItemTierPromotions( RepositoryItem pPricingModel ) {
		if( isLoggingDebug() ) {
			logDebug( "insdie isItemTierPromotions()" );
		}
		Integer tierType = (Integer)pPricingModel.getPropertyValue( "tierType" );
		if( isLoggingDebug() ) {
			if( tierType != null ) logDebug( "tierType" + tierType );
		}
		if( null!=tierType && tierType.intValue() == ITEM_TIER_TYPE )
			return true;
		else
			return false;
	}

	private Object matchTierLevel( RepositoryItem pPricingModel, RepositoryItem pProfile, java.util.List pItems, Order pOrder, OrderPriceInfo pOrderPriceInfo ) {
		Set tierLevels = getTierLevels( pPricingModel );
		if(null==tierLevels){
			return null;
		}
		if( isProfileTierPromotion( pPricingModel ) ) {
			return matchProfileTierLevel( tierLevels, pProfile );
		} else if( isOrderAmountTierPromotion( pPricingModel ) ) {
			return matchOrderAmountTierLevel( tierLevels, pOrder, pOrderPriceInfo );
		} else if( isItemTierPromotions( pPricingModel ) ) {
			long numberOfItems = findNumberOfItems( pPricingModel, pItems );
			return matchItemTierLevel( tierLevels, numberOfItems );
		} else
			return null;
	}

	private String matchProfileTierLevel( Set pTierLevels, RepositoryItem pProfile ) {
		if( pTierLevels == null ) return null;
		String userLoyaltyTier = (String)pProfile.getPropertyValue( "LoyaltyTier" );
		Iterator itr = pTierLevels.iterator();
		while( itr.hasNext() ) {
			String tierLevel = (String)itr.next();
			if( userLoyaltyTier == null ) return null;
			if( userLoyaltyTier.equalsIgnoreCase( tierLevel ) ) return tierLevel;
		}
		return null;
	}

	private String matchOrderAmountTierLevel( Set pTierLevels, Order pOrder, OrderPriceInfo pOrderPriceInfo ) {
		String topTier = null;
		int iTopTier = 0;
		DigitalOrderPriceInfo dPriceInfo = (DigitalOrderPriceInfo)pOrderPriceInfo;
		Double qualifiedAmount = new Double( dPriceInfo.getQualifiedAmount() );

		Iterator itr = pTierLevels.iterator();
		while( itr.hasNext() ) {
			String tierLevel = (String)itr.next();
			int iTierLevel = ( new Integer( tierLevel ) ).intValue();
			if( qualifiedAmount.intValue() >= iTierLevel && iTierLevel > iTopTier ) {
				topTier = tierLevel;
				iTopTier = ( new Integer( topTier ) ).intValue();
			}
		}
		return topTier;
	}

	private String matchItemTierLevel( Set pTierLevels, long pNumberOfItems ) {
		if( isLoggingDebug() ) {
			logDebug( "insdie matchItemTierLevel()" );
		}
		String topTier = null;
		int iTopTier = 0;
		if(null==pTierLevels){
			return topTier;
		}
		Iterator itr = pTierLevels.iterator();
		while( itr.hasNext() ) {
			String tierLevel = (String)itr.next();
			int iTierLevel = ( new Integer( tierLevel ) ).intValue();
			if( pNumberOfItems >= iTierLevel && iTierLevel > iTopTier ) {
				topTier = tierLevel;
				iTopTier = ( new Integer( topTier ) ).intValue();
			}
		}

		if( isLoggingDebug() ) {
			if( topTier != null ){
				logDebug( "topTier" + topTier );
			}
		}
		return topTier;
	}

	private long findNumberOfItems( RepositoryItem pPricingModel, List pItems ) {
		long totalQuant = 0;
		Set matchProducts = (Set)pPricingModel.getPropertyValue( "tierProducts" );
		Set matchBrands = (Set)pPricingModel.getPropertyValue( "tierBrands" );
		Set matchCategories = (Set)pPricingModel.getPropertyValue( "tierCategories" );
		Set excludeProducts = (Set)pPricingModel.getPropertyValue( "tierExclusionProducts" );
		Set excludeBrands = (Set)pPricingModel.getPropertyValue( "tierExclusionBrands" );
		Set excludeCategories = (Set)pPricingModel.getPropertyValue( "tierExclusionCategories" );
		Iterator ciItr = pItems.iterator();
		while( ciItr.hasNext() ) {
			CommerceItem ci = (CommerceItem)ciItr.next();
			RepositoryItem product = (RepositoryItem)ci.getAuxiliaryData().getProductRef();
			long quantity = ci.getQuantity();
			if( isItemTierMatch( product, matchProducts, matchBrands, matchCategories ) && !isItemTierMatch( product, excludeProducts, excludeBrands, excludeCategories ) ) {
				totalQuant = totalQuant + quantity;
			}
		}
		return totalQuant;
	}

	private Set getTierLevels( RepositoryItem pPricingModel ) {
		if( isLoggingDebug() ) {
			logDebug( "insdie getTierLevels()" );
		}
		Map tiers = getTiers( pPricingModel );
		Set tierLevels = null;
		if(null==tiers){
			return null;
		}
		tierLevels=tiers.keySet();
		if( isLoggingDebug() ) {
			if( tierLevels != null ){
				logDebug( "tierLevels" + tierLevels );
			}
		}
		return tierLevels;
	}

	private Map getTiers( RepositoryItem pPricingModel ) {
		if( isLoggingDebug() ) {
			logDebug( "inside getTiers()" );
		}
		Map tiers = (Map)pPricingModel.getPropertyValue( "tier" );
		if( isLoggingDebug() ) {
			if( tiers != null ) logDebug( "tiers" + tiers );
		}
		return tiers;
	}

	public double getAdjuster( java.util.List pPriceQuotes, java.util.List pItems, RepositoryItem pPricingModel, RepositoryItem pProfile, java.util.Locale pLocale, Order pOrder, OrderPriceInfo pOrderPriceInfo, java.util.Map pExtraParameters ) {
		if( isLoggingDebug() ) {
			logDebug( "inside getAdjuster()" );
		}
		double adjuster = 0.0;
		Object tierLevel = matchTierLevel( pPricingModel, pProfile, pItems, pOrder, pOrderPriceInfo );
		if( tierLevel != null ) {
			Map tiers = getTiers( pPricingModel );
			if(null!=tiers){
				adjuster = ( (Double)tiers.get( tierLevel ) ).doubleValue();
			}
		}
		if( isLoggingDebug() ) {
			logDebug( "tiers" + adjuster );
		}
		return adjuster;
	}

	boolean isItemTierMatch( RepositoryItem pProduct, Set pProducts, Set pBrands, Set pCategories ) {
		if( isLoggingDebug() ) {
			logDebug( "insdie isItemTierMatch()" );
		}
		if( pProducts.contains( pProduct ) ) { return true; }
		RepositoryItem brand = (RepositoryItem)pProduct.getPropertyValue( "dswBrand" );
		if( brand != null ) {
			if( pBrands.contains( brand ) ) {
				return true;
				}
		}
		Set prodCats = (Set)pProduct.getPropertyValue( "ancestorCategories" );
		if( isLoggingDebug() ) {
			if( prodCats != null ) {
				logDebug( "prodCats" + prodCats );
			}
		}
		Iterator itr = prodCats.iterator();
		while( itr.hasNext() ) {
			RepositoryItem cat = (RepositoryItem)itr.next();
			if( isLoggingDebug() ) {
				if( cat != null ) {
					logDebug( "cat" + cat );
				}
			}
			if( pCategories.contains( cat ) ) { return true; }
		}
		return false;
	}
}
