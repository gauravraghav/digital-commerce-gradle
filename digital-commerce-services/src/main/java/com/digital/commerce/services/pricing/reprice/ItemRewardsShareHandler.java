package com.digital.commerce.services.pricing.reprice;

/*  */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.digital.commerce.common.util.DigitalStringUtil;
@SuppressWarnings({"rawtypes","unchecked"})
public class ItemRewardsShareHandler extends DefaultHandler {
	private final String			COMMERCEITEM			= "commerceItem";
	private final String			REPOSITORYID			= "repositoryId";
	private final String			REWARDCERTIFICATESHARE	= "commerceItem.rewardCertificateShare";
	private final String			STRING					= "string";
	private final String			DOUBLE					= "double";
	@Getter
	private Map						ciCerts					= new HashMap( 10 );
	private String					commerceItem			= "";
	private String					rewardCertificateShare	= "";
	private String					svalue					= "";
	private String					dvalue					= "";
	private String					commId					= "";
	private RewardCertificateItem	rc						= null;

	private StringBuffer			characterBuf;

	private String					errMsg					= null;

	public String getErrorMessage() {
		return errMsg;
	}

	/** Receive notification of the start of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @param atts - The attributes attached to the element. If there are no attributes, it shall be an empty Attributes object.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */

	public void startElement( String namespaceURI, String localName, String qName, Attributes atts ) {
		characterBuf = new StringBuffer();

		try {
			if( COMMERCEITEM.equals( localName ) ) commerceItem = COMMERCEITEM;

			if( DigitalStringUtil.isNotBlank(commerceItem) ) {
				if( atts.getValue( REPOSITORYID ) != null && commId.equals( "" ) ) commId = atts.getValue( REPOSITORYID );
			}

			if( DigitalStringUtil.isNotBlank(commerceItem) ) {
				if( REWARDCERTIFICATESHARE.equals( localName ) ) rewardCertificateShare = REWARDCERTIFICATESHARE;
			}

			if( DigitalStringUtil.isNotBlank(commerceItem) && DigitalStringUtil.isNotBlank(rewardCertificateShare) ) {
				if( STRING.equals( localName ) ) {
					/* start defect 2454 */
					rc = new RewardCertificateItem();
					/* end defect 2454 */
					svalue = STRING;
				}
				if( DOUBLE.equals( localName ) ) dvalue = DOUBLE;
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}

	/** Receive notification of character data inside an element.
	 * 
	 * @param ch - The characters.
	 * @param start - The start position in the character array.
	 * @param length - The number of characters to use from the character array.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */

	public void characters( char[] ch, int start, int length ) {
		characterBuf.append( ch, start, length );
	}

	/** Receive notification of the end of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */

	public void endElement( String namespaceURI, String localName, String qName ) {
		populateValue();
		try {
			if( REWARDCERTIFICATESHARE.equals( localName ) ) {
				rewardCertificateShare = "";
				rc = null;
			}

			if( COMMERCEITEM.equals( localName ) ) {
				commerceItem = "";
				commId = "";
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}

	public void populateValue() {
		try {
			if( DigitalStringUtil.isNotBlank(commerceItem) && DigitalStringUtil.isNotBlank(rewardCertificateShare) && DigitalStringUtil.isNotBlank(svalue) ) {
				String certNumber = characterBuf.toString();
				// rc.setCertNumber(extractCertNumber(certNumber));
				rc.setCertNumber( certNumber );
				svalue = "";
			}

			if( DigitalStringUtil.isNotBlank(commerceItem) && DigitalStringUtil.isNotBlank(rewardCertificateShare) && DigitalStringUtil.isNotBlank(commId) && DigitalStringUtil.isNotBlank(dvalue) ) {
				double certValueUsed = Double.parseDouble( characterBuf.toString() );
				rc.setCertAmountUsed( certValueUsed );

				if( ciCerts.get( commId ) != null ) {
					List l = (List)ciCerts.get( commId );
					l.add( rc );
					ciCerts.put( commId, l );
				} else {
					List l = new ArrayList( 10 );
					l.add( rc );
					ciCerts.put( commId, l );
				}
				dvalue = "";
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}
}
