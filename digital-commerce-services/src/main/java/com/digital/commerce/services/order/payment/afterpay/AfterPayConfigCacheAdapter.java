package com.digital.commerce.services.order.payment.afterpay;

import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.cache.CacheAdapter;
import com.digital.commerce.integration.payment.AfterPayPaymentService;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AfterPayConfigCacheAdapter extends ApplicationLoggingImpl implements CacheAdapter {

  private AfterPayPaymentService afterPayService;

  /**
   * @param pKey
   * @return Object
   * @throws Exception
   */
  @Override
  public Object getCacheElement(Object pKey) throws Exception {
    if (pKey != null) {
      return afterPayService.getConfiguration();
    }
    return null;
  }

  /**
   * @param pKeys
   * @return
   * @throws Exception
   */
  @Override
  public Object[] getCacheElements(Object[] pKeys) throws Exception {
    vlogDebug("Entering Object[] getCacheElements(Object[] pKeys) : ");
    if (pKeys != null) {
      int keyLength = pKeys.length;
      vlogDebug("Key Length :: " + keyLength);
      Object[] elements = new Object[keyLength];
      for (int i = 0; i < keyLength; i++) {
        elements[i] = getCacheElement(pKeys[i]);
      }
      return elements;
    } else {
      return null;
    }
  }

  /**
   * @param pKey
   * @return int
   */
  @Override
  public int getCacheKeySize(Object pKey) {
    return 0;
  }

  /**
   * @param pElement
   * @param pKey
   * @return int
   */
  @Override
  public int getCacheElementSize(Object pElement, Object pKey) {
    return 0;
  }

  /**
   * @param pElement
   * @param pKey
   */
  @Override
  public void removeCacheElement(Object pElement, Object pKey) {

  }
}
