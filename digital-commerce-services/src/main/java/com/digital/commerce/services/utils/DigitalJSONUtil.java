package com.digital.commerce.services.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalStringUtil;
@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalJSONUtil {

	public static <K, V extends Object> Map<String, Map<String, Object>> convertToJSONArrayFromMap(
			Map<K, V> map, String keyTag, String valueTag,
			String primaryKeyProperty, Boolean filter) {
		// ArrayList<JSONObject> options = new ArrayList<JSONObject>();

		HashMap<String, Map<String, Object>> result = new HashMap<>();
		for (Map.Entry<K, V> entry : map.entrySet()) {
			int sizeOfCI = 0;
			Map<String, Object> jo = new HashMap<>();
			if (keyTag == null)
				jo.put(entry.getKey().getClass().getName(), entry.getKey());
			else
				jo.put(keyTag, entry.getKey());

			Object value = null;
			
			if (entry.getValue() instanceof List) {
				value = new LinkedHashMap(5,0.75f,false);
				int index1 = ((List) entry.getValue()).size()-1;
				sizeOfCI = ((List) entry.getValue()).size();
				Iterator iter = ((List) entry.getValue()).iterator();
				while (iter.hasNext()) {
					((HashMap) value).put("item"+index1,
							iter.next());
					index1--;
				}
			} else {
				value = entry.getValue();
			}

			if (valueTag == null)
				jo.put(entry.getValue().getClass().getName(), value);
			else
				jo.put(valueTag, value);
			// jo.put(entry.getValue().getClass().getName(), entry.getValue());
			String keyValue = null;
			if (primaryKeyProperty != null) {
				// resolve value from object

				try {
					Method method = entry.getKey().getClass().getMethod("get"+primaryKeyProperty);
					Object propertyValue =  method.invoke(entry.getKey());
					if(propertyValue != null)
						keyValue = propertyValue.toString();
				} catch (IllegalArgumentException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
					// TODO Auto-generated catch block
					//no need fallback to index
				} 

			}
			if(filter) {
				if(DigitalStringUtil.isNotBlank(keyValue) && sizeOfCI > 0 ) {
					result.put(keyValue, jo);
				}
			} else {
				if(DigitalStringUtil.isNotBlank(keyValue) ) {
					result.put(keyValue, jo);
				}
			}
		}

		// JSONArray js = new JSONArray(options);

		return result;
	}
	
	

}
