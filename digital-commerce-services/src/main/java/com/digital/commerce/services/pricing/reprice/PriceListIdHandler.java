package com.digital.commerce.services.pricing.reprice;

/* */
import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.digital.commerce.common.util.DigitalStringUtil;
@SuppressWarnings({"rawtypes","unchecked"})
public class PriceListIdHandler extends DefaultHandler {

	private final String	COMMERCEITEM	= "commerceItem";
	private final String	REPOSITORYID	= "repositoryId";
	private final String	PRICELISTID		= "itemPriceInfo.priceListId";
	private Map				ciPriceLists	= new HashMap( 10 );
	private String			commerceItem	= "";
	private String			priceListId		= "";
	private String			commId			= "";
	private String			errMsg			= null;

	private StringBuffer	characterBuf;

	public String getErrorMessage() {
		return errMsg;
	}

	public Map getCIPriceLists() {
		return ciPriceLists;
	}

	/** Receive notification of the start of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @param atts - The attributes attached to the element. If there are no attributes, it shall be an empty Attributes object.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void startElement( String namespaceURI, String localName, String qName, Attributes atts ) {
		characterBuf = new StringBuffer();

		try {

			if( COMMERCEITEM.equals( localName ) ) {
				commerceItem = COMMERCEITEM;
			}

			if( DigitalStringUtil.isNotBlank(commerceItem) ) {
				if( atts.getValue( REPOSITORYID ) != null && DigitalStringUtil.isBlank(commId) ) commId = atts.getValue( REPOSITORYID );
			}

			if( DigitalStringUtil.isNotBlank(commerceItem) && DigitalStringUtil.isNotBlank(commId) && PRICELISTID.equals( localName ) ) {
				priceListId = PRICELISTID;
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}

	}

	/** Receive notification of character data inside an element.
	 * 
	 * @param ch - The characters.
	 * @param start - The start position in the character array.
	 * @param length - The number of characters to use from the character array.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void characters( char[] ch, int start, int length ) {
		characterBuf.append( ch, start, length );
	}

	/** Receive notification of the end of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void endElement( String namespaceURI, String localName, String qName ) {
		populateValue();

		try {
			if( COMMERCEITEM.equals( localName ) ) {
				commerceItem = "";
				priceListId = "";
				commId = "";
			}

		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}

	private void populateValue() {
		try {
			String value = characterBuf.toString();

			if( DigitalStringUtil.isNotBlank(commerceItem) && DigitalStringUtil.isNotBlank(commId) && DigitalStringUtil.isNotBlank(priceListId) ) {
				ciPriceLists.put( commId, value );
				priceListId = "";
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}
}
