package com.digital.commerce.services.v2_0.promotions;

import static com.digital.commerce.services.profile.rewards.LoyaltyCertificate.AMOUNT_COMPARATOR_WITH_SAME_DATE;

import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.dtm.UserTransactionDemarcation;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.collections.filter.FilterException;
import atg.service.collections.filter.ValidatorFilter;
import atg.service.collections.validator.CollectionObjectValidator;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.coupons.DigitalCouponService;
import com.digital.commerce.services.i18n.DigitalCustomDateFormatter;
import com.digital.commerce.services.profile.DigitalCommercePropertyManager;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.profile.rewards.LoyaltyCertificate;
import com.digital.commerce.services.profile.vo.Offer;
import com.digital.commerce.services.promotion.DigitalPromotionTools;
import com.digital.commerce.services.promotions.DigitalPromotionService;
import com.digital.commerce.services.promotions.GenericOfferDetailsBean;
import com.digital.commerce.services.promotions.GenericOffersListBean;
import com.digital.commerce.services.promotions.collections.validator.DigitalGlobalPromotionValidator;
import com.digital.commerce.services.rewards.domain.RewardsIncentivesResponse;
import com.digital.commerce.services.rewards.domain.RewardsRetrieveRewardCertificatesResponse;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletException;

@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalAvailableSavingsDroplet extends DynamoServlet {
	private static final String CLASSNAME = DigitalAvailableSavingsDroplet.class.getName();

	enum PromotionType {ITEM_DISCOUNT, ORDER_DISCOUNT, SHIPPING_DISCOUNT}

	private static final String OFFER_RIBBON_TYPE 					= "OFFERS";
	private static final String BIRTHDAY_OFFER_RIBBON_TYPE 					= "BIRTHDAY_OFFER";
	private static final String CERTIFICATES_RIBBON_TYPE 			= "CERTIFICATES";
	private static final String VIP_SIGN_UP_RIBBON_TYPE 			= "VIP_SIGN_UP";
	private static final String WEB_OFFERS_RIBBON_TYPE 				= "WEB_OFFERS";
	private static final String DOUBLE_POINTS_DAYS_RIBBON_TYPE 		= "DOUBLE_POINTS_DAYS";
	private static final String TRIPLE_POINTS_DAYS_RIBBON_TYPE 		= "TRIPLE_POINTS_DAYS";
	private static final String GREETING_MSG_RIBBON_TYPE 			= "GREETING_MSG";
	private static final String TIER_STATUS_RIBBON_TYPE 			= "TIER_STATUS";
	private static final String REWARDS_METER_RIBBON_TYPE 			= "REWARDS_METER";
	private static final String REWARDS_NOW_RIBBON_TYPE 			= "REWARDS_NOW";
	private static final String INCENTIVES			= "incentives";

	private static final String ICON_TYPE_2X 			= "2X";
	private static final String ICON_TYPE_3X 			= "3X";
	private static final String ICON_TYPE_SAVINGS			= "SAVINGS";

	private static final int ITEM_DISCOUNT_TYPE 	= 0;
	private static final int ORDER_DISCOUNT_TYPE 	= 9;
	private static final int SHIPPING_DISCOUNT_TYPE = 5;

	private static final String DEPLOYABLE_PROMOTION_CLAIMABLE = "DeployablePromotionClaimable";
	private static final String ICONTYPE_NONE="NONE";

	private static final ParameterName OUTPUT 			= ParameterName.getParameterName("output");
	private static final ParameterName EMPTY 			= ParameterName.getParameterName("empty");
	private static final ParameterName ERROR_OPARAM 	= ParameterName.getParameterName("error");

	private static final String PERFORM_MONITOR_NAME = "DigitalAvailableSavingsDroplet";

	private DigitalPromotionService promotionService;

	private Profile profile;

	private DigitalProfileTools profileTools;

	private DigitalRewardsManager rewardsManager;

	private DigitalCustomDateFormatter customDateFormatter;

	private DigitalCouponService mCouponService;

	/**
	 * @return
	 */
	public DigitalCouponService getCouponService() {
		return mCouponService;
	}

	/**
	 * @param pCouponService
	 */
	public void setCouponService(DigitalCouponService pCouponService) {
		this.mCouponService = pCouponService;
	}


	/**
	 * property: filter
	 */
	private ValidatorFilter mFilter;

	/**
	 * @return
	 */
	public DigitalPromotionService getPromotionService() {
		return promotionService;
	}

	/**
	 * @param promotionService
	 */
	public void setPromotionService(DigitalPromotionService promotionService) {
		this.promotionService = promotionService;
	}

	/**
	 * @return filter that should be applied for promotions
	 */
	public ValidatorFilter getFilter() {
		return mFilter;
	}

	/**
	 * @param pFilter the CachedCollectionFilter to be set
	 */
	public void setFilter(ValidatorFilter pFilter) {
		mFilter = pFilter;
	}

	public DigitalProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(DigitalProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	/**
	 * @return
	 */
	public DigitalRewardsManager getRewardsManager() {
		return rewardsManager;
	}

	/**
	 * @param rewardsManager
	 */
	public void setRewardsManager(DigitalRewardsManager rewardsManager) {
		this.rewardsManager = rewardsManager;
	}

	/**
	 * @return
	 */
	public Profile getProfile() {
		return profile;
	}

	/**
	 * @param profile
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	/**
	 * @return
	 */
	public DigitalServiceConstants getDswConstants() {
		return getProfileTools().getDswConstants();
	}




	/**
	 * This method will call existing initializeAndFilterPromotions method.
	 * Render oparam output with parameter filteredPromotions.
	 *
	 * @param pRequest
	 * @param pResponse
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		String METHOD_NAME = "service";
		UserTransactionDemarcation td = null;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
			td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

			Map<String, Object> availableSavingMap = new HashMap<>();

			Collection<RepositoryItem> filteredPromotions;

			filteredPromotions = (Collection<RepositoryItem>) initializeSiteGroupPromotions(pRequest, pResponse);

			if (!filteredPromotions.isEmpty()) {
				availableSavingMap.putAll(groupPromotionsByType(filteredPromotions, ITEM_DISCOUNT_TYPE));
				availableSavingMap.putAll(groupPromotionsByType(filteredPromotions, ORDER_DISCOUNT_TYPE));
				availableSavingMap.putAll(groupPromotionsByType(filteredPromotions, SHIPPING_DISCOUNT_TYPE));
			}

			try {
				if (!profileTools.isDSWAnanymousUser(profile) && profileTools.getLoyaltyNumber(profile) != null) {

					RewardsRetrieveRewardCertificatesResponse rewardsResp;
					String tier = profileTools.getLoyaltyTier(profile);

					// Load Certs and Offers to the profile using loyalty service
					rewardsResp = rewardsManager.retrieveRewardCertificatesByProfile(profile);
					if(rewardsResp.isRequestSuccess()) {
						getRewardsManager().loadProfileCertsAndOffersUsingLoyaltyService(profile, rewardsResp);

						Map<String, List<LoyaltyCertificate>> rewardOffersByType = rewardsManager.getProfileOffersByType(profile);

						availableSavingMap.putAll(
							this.getRewardOffersByType(rewardOffersByType.get(OFFER_RIBBON_TYPE),
								OFFER_RIBBON_TYPE));
						Map<String, GenericOffersListBean> birthDayOffersMap = this
							.getRewardOffersByType(rewardOffersByType.get(BIRTHDAY_OFFER_RIBBON_TYPE),
								OFFER_RIBBON_TYPE);
						if (birthDayOffersMap != null && !birthDayOffersMap.isEmpty()) {
						  if (availableSavingMap.get(OFFER_RIBBON_TYPE) != null &&
							  birthDayOffersMap.get(OFFER_RIBBON_TYPE) != null) {
							GenericOffersListBean bean = (GenericOffersListBean) availableSavingMap
								.get(OFFER_RIBBON_TYPE);
							List<GenericOfferDetailsBean> genericOfferDetailsBeans = bean.getOffers();
							genericOfferDetailsBeans
								.addAll(birthDayOffersMap.get(OFFER_RIBBON_TYPE).getOffers());
						  } else {
							availableSavingMap.putAll(birthDayOffersMap);
						  }
						}

						// 2x and 3x points days needs to be returned only for GOLD or ELITE members. Need to check the tier status
						if(DigitalBaseConstants.USER_TIER_GOLD.equalsIgnoreCase(tier)
								|| DigitalBaseConstants.USER_TIER_ELITE.equalsIgnoreCase(tier)) {
							availableSavingMap.putAll(this.getRewardOffersByType(rewardOffersByType.get(DOUBLE_POINTS_DAYS_RIBBON_TYPE),
									DOUBLE_POINTS_DAYS_RIBBON_TYPE));
						}

						if(DigitalBaseConstants.USER_TIER_ELITE.equalsIgnoreCase(tier)) {
							availableSavingMap.putAll(this.getRewardOffersByType(rewardOffersByType.get(TRIPLE_POINTS_DAYS_RIBBON_TYPE),
									TRIPLE_POINTS_DAYS_RIBBON_TYPE));
						}
					}else{
						GenericOffersListBean offerBean = new GenericOffersListBean();
						offerBean.setPriority(getRibbonOrder(OFFER_RIBBON_TYPE));
						availableSavingMap.put(OFFER_RIBBON_TYPE, offerBean);

						GenericOffersListBean certBean = new GenericOffersListBean();
						certBean.setPriority(getRibbonOrder(CERTIFICATES_RIBBON_TYPE));
						availableSavingMap.put(CERTIFICATES_RIBBON_TYPE, certBean );

						if(DigitalBaseConstants.USER_TIER_GOLD.equalsIgnoreCase(tier)
								|| DigitalBaseConstants.USER_TIER_ELITE.equalsIgnoreCase(tier)) {
							GenericOffersListBean doublePointsBean = new GenericOffersListBean();
							doublePointsBean.setPriority(getRibbonOrder(DOUBLE_POINTS_DAYS_RIBBON_TYPE));
							availableSavingMap.put(DOUBLE_POINTS_DAYS_RIBBON_TYPE, doublePointsBean);
						}

						if(DigitalBaseConstants.USER_TIER_ELITE.equalsIgnoreCase(tier)) {
							GenericOffersListBean triplePointsBean = new GenericOffersListBean();
							triplePointsBean.setPriority(getRibbonOrder(TRIPLE_POINTS_DAYS_RIBBON_TYPE));
							availableSavingMap.put(TRIPLE_POINTS_DAYS_RIBBON_TYPE, triplePointsBean);
						}
					}

					availableSavingMap.putAll(this.getRewardCertificates(rewardsResp));

					availableSavingMap.putAll(this.getRewardWebOffers(rewardsResp));

					availableSavingMap.putAll(this.getRewardsSummary());

					availableSavingMap.putAll(this.getMsgGreetings());

					availableSavingMap.putAll(this.getRewardsNow());

					if(isLoggingDebug()){
						logDebug(":: AvailableSavingMap :: " + availableSavingMap.toString());
					}

				} else {
					if (isLoggingDebug()) {
						logDebug("User profile " + profile.getRepositoryId()
								+ " is not a rewards or cookied user, no need to calls rewards");
					}

					if(!profileTools.isDSWAnanymousUser(profile)) {
						if (isLoggingDebug()) {
							logDebug("User profile " + profile.getRepositoryId()
									+ " is a non-member");
						}
						availableSavingMap.putAll(this.getMsgGreetings());
					}

					if(profileTools.isDSWAnanymousUser(profile)){
						Map<String, Object> vipSignUp = getVipSignUp();
						availableSavingMap.putAll(vipSignUp);
					}
				}

			} catch (Exception e) {
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				String msg = "Unable to query loyalty number for the profile";
				if (isLoggingError()) {
					logError(msg, e);
				}
				pRequest.setParameter("errorMsg", msg);
				pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
			}

			pRequest.setParameter("sortedPromotions", availableSavingMap);

			if (availableSavingMap.isEmpty()) {
				if (isLoggingDebug()) {
					logDebug("No promotions available");
				}
				pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			} else {
				if (isLoggingDebug()) {
					logDebug("sortedPromotions  --> size" + availableSavingMap.size());
				}
				pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
			}
		} catch (RepositoryException e) {
			TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
			String msg = "Unable to fetch available savings";
			if (isLoggingError()) {
				logError(e.getMessage());
			}
			pRequest.setParameter("errorMsg", msg);
			pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
		} finally {
			TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
			TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
		}
	}

	protected Map<String, Object> getRewardsSummary(){
		Map<String, Object> rewardsSummary = new HashMap<>();

		Map<String, Object> rewardDetails = profileTools.getRewardsSummary(profile, true);
		Map<String, Object> tierStatus = new HashMap<>();
		Map<String, Object> rewardsMeter = new HashMap<>();

		if (rewardDetails != null) {
			//tierOMeter related logic

			//read the tier and loyalty expiration date after sync with rewards
			int currentYear = DigitalDateUtil.getCurrentYear();
			Date expirationDate = profileTools.getLoyaltyExpirationDate(profile);
			int expirationYear = currentYear;
			if(expirationDate != null) {
				expirationYear = DigitalDateUtil.getYear(expirationDate);
			}
			String loyaltyTier = (String)rewardDetails.get("loyaltyTier");

			long dollarsNextTier = (long)rewardDetails.get("dollarsNextTier");

			Double tierOMeterCTGThreshold = MultiSiteUtil.getRewardsTierometerCTGThreshold();
			Double tierOMeterGTEThreshold = MultiSiteUtil.getRewardsTierometerGTEThreshold();

			boolean returnTierOMeterRibbon = false;
			if(DigitalBaseConstants.USER_TIER_CLUB.equalsIgnoreCase(loyaltyTier) &&
					dollarsNextTier < tierOMeterCTGThreshold.longValue()){
				returnTierOMeterRibbon = true;
			}else if(DigitalBaseConstants.USER_TIER_GOLD.equalsIgnoreCase(loyaltyTier) &&
					expirationYear > currentYear &&
					dollarsNextTier < tierOMeterGTEThreshold.longValue()){
				returnTierOMeterRibbon = true;
			}

			if(returnTierOMeterRibbon) {
				tierStatus.put("id", rewardDetails.get("id"));
				tierStatus.put("loyaltyTierExpirationDate", rewardDetails.get("loyaltyTierExpirationDate"));
				tierStatus.put("loyaltyNumber", rewardDetails.get("loyaltyNumber"));
				tierStatus.put("dollarsNextTierMinimum", rewardDetails.get("dollarsNextTierMinimum"));
				tierStatus.put("dollarsNextTier", rewardDetails.get("dollarsNextTier"));
				tierStatus.put("nextTier", rewardDetails.get("nextTier"));
				tierStatus.put("loyaltyTier", rewardDetails.get("loyaltyTier"));
				tierStatus.put("tierOMeter", rewardDetails.get("tierOMeter"));

				tierStatus.put("priority", getRibbonOrder(TIER_STATUS_RIBBON_TYPE));
				rewardsSummary.put(TIER_STATUS_RIBBON_TYPE, tierStatus);

				if(isLoggingDebug()){
					logDebug(":: TierStatus :: " + tierStatus.toString());
				}
			}

			//certOMeter related logic
			Double certOMeterThreshold = MultiSiteUtil.getRewardsCertometerThreshold();
			long dollarsNextReward = (long)rewardDetails.get("dollarsNextReward");
			if(dollarsNextReward < certOMeterThreshold.longValue()) {
				rewardsMeter.put("dollarsFutureReward", rewardDetails.get("dollarsFutureReward"));
				rewardsMeter.put("dollarsNextReward", rewardDetails.get("dollarsNextReward"));
				rewardsMeter.put("pointsNextReward", rewardDetails.get("pointsNextReward"));
				rewardsMeter.put("displayRewardsCertClosenessQualifier", rewardDetails.get("displayRewardsCertClosenessQualifier"));
				rewardsMeter.put("certOMeter", rewardDetails.get("certOMeter"));

				rewardsMeter.put("priority", getRibbonOrder(REWARDS_METER_RIBBON_TYPE));
				rewardsSummary.put(REWARDS_METER_RIBBON_TYPE, rewardsMeter);
				if(isLoggingDebug()){
					logDebug(":: RewardsMeter :: " + rewardsMeter.toString());
				}
			}

			if(isLoggingDebug()){
				logDebug(":: RewardsSummary :: " + rewardsSummary.toString());
			}
		}

		return rewardsSummary;
	}

	/**
	 * @return Map
	 */
	private Map<String, Object> getMsgGreetings() {
		return getSavingsRibbonMap(GREETING_MSG_RIBBON_TYPE);
	}

	/**
	 * @return Map
	 */
	private Map<String, Object> getRewardsNow() {
		Map<String, Object> msgRibbonMap = new HashMap<>();
		Map<String, Object> rewardsNowMap = new HashMap<>();
		RewardsIncentivesResponse rewardsIncentivesResponse = getRewardsManager()
				.retrieveRewardIncentives(profile.getRepositoryId());
		rewardsNowMap.put("priority", getRibbonOrder(REWARDS_NOW_RIBBON_TYPE));
		rewardsNowMap.put(INCENTIVES, rewardsIncentivesResponse.getItems());
		msgRibbonMap.put(REWARDS_NOW_RIBBON_TYPE, rewardsNowMap);
		return msgRibbonMap;
	}

	/**
	 * @return Map
	 */
	private Map<String, Object> getVipSignUp() {
		return getSavingsRibbonMap(VIP_SIGN_UP_RIBBON_TYPE);
	}

	/**
	 * @param msgRibbonType
	 * @return
	 */
	private Map<String, Object> getSavingsRibbonMap(String msgRibbonType) {
		Map<String, Object> msgRibbonMap = new HashMap<>();
		Map<String, Object> msgRibbon = new HashMap<>();

		msgRibbon.put("priority", getRibbonOrder(msgRibbonType));

		msgRibbonMap.put(msgRibbonType, msgRibbon);

		return msgRibbonMap;
	}

	/**
	 * @param rewardOffers
	 * @param offerType
	 * @return Map
	 */
	protected Map<String, GenericOffersListBean> getRewardOffersByType(List<LoyaltyCertificate> rewardOffers, String offerType) {
		Map<String, GenericOffersListBean> sortedPromoMap = new HashMap<>();
		List<GenericOfferDetailsBean> genericOffers = new ArrayList<>();
		Double totalValue = 0.0;
		for (LoyaltyCertificate rewardOffer : rewardOffers) {

			GenericOfferDetailsBean offerDetailsBean = new GenericOfferDetailsBean();
			offerDetailsBean.setCertificateNumber(rewardOffer.getCertificateNumber());
			Date expDate = rewardOffer.getExpirationDate();
			String expDateStr = customDateFormatter.getLocalizedDateString(expDate);
			offerDetailsBean.setEndDate(expDateStr);

			Date issueDate = rewardOffer.getIssueDate();
			String issueDateStr = customDateFormatter.getLocalizedDateString(issueDate);
			offerDetailsBean.setStartDate(issueDateStr);

			offerDetailsBean.setOfferCode(rewardOffer.getMarkdownCode());

			offerDetailsBean.setName(rewardOffer.getOfferDisplayName());

			offerDetailsBean.setOfferDetailsPageName(rewardOffer.getOfferDetailsPageName());

			offerDetailsBean.setValue(String.valueOf(rewardOffer.getValue()));
			if(rewardOffer.getValue() > 0.00) {
				totalValue += rewardOffer.getValue();
			}
			offerDetailsBean.setType(offerType);

			if(TRIPLE_POINTS_DAYS_RIBBON_TYPE.equalsIgnoreCase(offerType)){
				offerDetailsBean.setIconType(ICON_TYPE_3X);
			}else if(DOUBLE_POINTS_DAYS_RIBBON_TYPE.equalsIgnoreCase(offerType)) {
				offerDetailsBean.setIconType(ICON_TYPE_2X);
			}else{
				offerDetailsBean.setIconType(ICON_TYPE_SAVINGS);
			}

			genericOffers.add(offerDetailsBean);

		}

		GenericOffersListBean genericOffersListBean = new GenericOffersListBean();
		genericOffersListBean.setPriority(getRibbonOrder(offerType));
		genericOffersListBean.setOffers(genericOffers);
		genericOffersListBean.setTotalValue(totalValue);

		sortedPromoMap.put(offerType, genericOffersListBean);
		if(isLoggingDebug()){
			logDebug(":: getRewardOffers :: " + sortedPromoMap.toString());
		}
		return sortedPromoMap;
	}

	protected Map<String, GenericOffersListBean> getRewardWebOffers(RewardsRetrieveRewardCertificatesResponse rewardsResp)
			throws DigitalAppException {
		Map<String, GenericOffersListBean> sortedPromoMap = new HashMap<>();
		boolean isValidLoyaltyMember = rewardsResp.isValidLoyaltyMember();
		List<Offer> rewardWebOffers = getPromotionTools().getWebAuthOffers(isValidLoyaltyMember, getProfile());

		GenericOffersListBean genericOffersListBean = transformWebOfferToGenericOffer(rewardWebOffers, WEB_OFFERS_RIBBON_TYPE);

		sortedPromoMap.put(WEB_OFFERS_RIBBON_TYPE, genericOffersListBean);
		if(isLoggingDebug()){
			logDebug(":: getRewardWebOffers :: " + sortedPromoMap.toString());
		}
		return sortedPromoMap;
	}

	protected GenericOffersListBean transformWebOfferToGenericOffer(List<Offer> rewardWebOffers, String ribbonType){
		List<GenericOfferDetailsBean> genericOffers = new ArrayList<>();
		for (Offer rewardOffer : rewardWebOffers) {
			GenericOfferDetailsBean offerDetailsBean = new GenericOfferDetailsBean();
			Date expDate = rewardOffer.getEndDate();
			String expDateStr = customDateFormatter.getLocalizedDateString(expDate);
			offerDetailsBean.setEndDate(expDateStr);
			Date issueDate = rewardOffer.getStartDate();
			String issueDateStr = customDateFormatter.getLocalizedDateString(issueDate);
			offerDetailsBean.setStartDate(issueDateStr);
			//REW-1208
			offerDetailsBean.setOfferDetailsPageName(rewardOffer.getOfferDetailsPageName());
			offerDetailsBean.setOfferCode(rewardOffer.getCouponCode());
			offerDetailsBean.setType(ribbonType);
			offerDetailsBean.setName(rewardOffer.getUserFriendlyName());
			offerDetailsBean.setDescription(rewardOffer.getUserFriendlyDescription());
			//REW-1258
			offerDetailsBean.setIconType(rewardOffer.getDiscountIconType());

			//REW-1257
			offerDetailsBean.setDisplayOrder(rewardOffer.getDisplayOrder());
			genericOffers.add(offerDetailsBean);

		}

		//REW-1257
		if( !genericOffers.isEmpty()) {
			Collections.sort(genericOffers, PROMOS_SORT_BY_DISPLAY_ORDER);
		}

		GenericOffersListBean genericOffersListBean = new GenericOffersListBean();
		genericOffersListBean.setPriority(getRibbonOrder(ribbonType));
		genericOffersListBean.setOffers(genericOffers);

		return genericOffersListBean;
	}

	protected Map<String, GenericOffersListBean> getRewardCertificates(RewardsRetrieveRewardCertificatesResponse rewardsResp) {
		Map<String, GenericOffersListBean> sortedPromoMap = new HashMap<>();
		List<GenericOfferDetailsBean> genericOffers = new ArrayList<>();
		GenericOffersListBean genericOffersListBean = new GenericOffersListBean();
		genericOffersListBean.setPriority(getRibbonOrder(CERTIFICATES_RIBBON_TYPE));

		if(rewardsResp == null || !rewardsResp.isRequestSuccess()){
			genericOffersListBean.setOffers(genericOffers);
			sortedPromoMap.put(CERTIFICATES_RIBBON_TYPE, genericOffersListBean);
			return sortedPromoMap;
		}

		List<LoyaltyCertificate> rewardCerts = getRewardsManager().getProfileCertificates(profile);
		Double totalValue = 0.0;
		if(rewardCerts == null){
			genericOffersListBean.setOffers(genericOffers);
			genericOffersListBean.setTotalValue(totalValue);
			sortedPromoMap.put(CERTIFICATES_RIBBON_TYPE, genericOffersListBean);
			return sortedPromoMap;
		}

		Collections.sort( rewardCerts, AMOUNT_COMPARATOR_WITH_SAME_DATE );

		for (LoyaltyCertificate rewardCert : rewardCerts) {
			GenericOfferDetailsBean offerDetailsBean = new GenericOfferDetailsBean();
			offerDetailsBean.setCertificateNumber(rewardCert.getCertificateNumber());

			Date expDate = rewardCert.getExpirationDate();
			String expDateStr = customDateFormatter.getLocalizedDateString(expDate);
			offerDetailsBean.setEndDate(expDateStr);

			Date issueDate = rewardCert.getIssueDate();
			String issueDateStr = customDateFormatter.getLocalizedDateString(issueDate);
			offerDetailsBean.setStartDate(issueDateStr);

			offerDetailsBean.setOfferCode(rewardCert.getMarkdownCode());
			offerDetailsBean.setType(CERTIFICATES_RIBBON_TYPE);
			offerDetailsBean.setName(rewardCert.getOfferDisplayName());
			offerDetailsBean.setOfferDetailsPageName(rewardCert.getOfferDetailsPageName());
			offerDetailsBean.setValue(String.valueOf(rewardCert.getValue()));

			if(rewardCert.getValue() > 0.00) {
				totalValue += rewardCert.getValue();
			}

			genericOffers.add(offerDetailsBean);

		}
		genericOffersListBean.setOffers(genericOffers);
		genericOffersListBean.setTotalValue(totalValue);

		sortedPromoMap.put(CERTIFICATES_RIBBON_TYPE, genericOffersListBean);
		if(isLoggingDebug()){
			logDebug(":: getRewardCertificates :: " + sortedPromoMap.toString());
		}
		return sortedPromoMap;
	}

	/**
	 * Groups the promotions based on type
	 *
	 * @throws RepositoryException
	 * @throws IOException
	 * @throws ServletException
	 */
	protected Map<String, GenericOffersListBean> groupPromotionsByType(Collection<RepositoryItem> filteredPromotions, int type) throws RepositoryException {
		Map<String, GenericOffersListBean> sortedPromoMap = new HashMap<>();
		String reqPromotType = getPromotionTypeString(type);
		DigitalCommercePropertyManager commercePropertyManager = getCommercePropertyManager();
		for (RepositoryItem promoItem : filteredPromotions) {

			String promoType;
			if (DEPLOYABLE_PROMOTION_CLAIMABLE.equals(promoItem.getItemDescriptor().getItemDescriptorName())) {
				if ((boolean) promoItem.getPropertyValue(commercePropertyManager.getPromotionHasPromotionsPropertyName())) {
					Set<RepositoryItem> couponPromotions = (Set<RepositoryItem>) promoItem.getPropertyValue(commercePropertyManager.getPromotionsPropertyName());

					if (couponPromotions != null && !couponPromotions.isEmpty()) {
						for (RepositoryItem couponPromotion : couponPromotions) {
							promoType = getPromotionTypeString((Integer) couponPromotion.getPropertyValue(commercePropertyManager.getPromotionTypePropertyName()));
							if (promoType.equalsIgnoreCase(reqPromotType)) {
								if (sortedPromoMap.get(promoType) != null) {
									GenericOffersListBean genericOffersListBean = sortedPromoMap.get(promoType);
									List offers = genericOffersListBean.getOffers();
									if (offers != null && !offers.isEmpty()) {
										offers.add(convertCouponPromotionToBean(promoItem, couponPromotion));
									}
								} else {
									GenericOffersListBean genericOffersListBean = new GenericOffersListBean();
									genericOffersListBean.setPriority(getRibbonOrder(promoType));
									ArrayList<GenericOfferDetailsBean> promoList = new ArrayList<>();
									promoList.add(convertCouponPromotionToBean(promoItem, couponPromotion));
									genericOffersListBean.setOffers(promoList);
									sortedPromoMap.put(promoType, genericOffersListBean);
								}
							}
						}
					}
				}
			} else {
				promoType = getPromotionTypeString((Integer) promoItem.getPropertyValue(commercePropertyManager.getPromotionTypePropertyName()));
				if (promoType.equalsIgnoreCase(getPromotionTypeString(type))) {
					if (sortedPromoMap.get(promoType) != null) {
						GenericOffersListBean genericOffersListBean = sortedPromoMap.get(promoType);
						List offers = genericOffersListBean.getOffers();
						if (offers != null && !offers.isEmpty()) {
							offers.add(convertPromotionToBean(promoItem));
						}
					} else {
						GenericOffersListBean genericOffersListBean = new GenericOffersListBean();
						genericOffersListBean.setPriority(getRibbonOrder(promoType));
						ArrayList<GenericOfferDetailsBean> promoList = new ArrayList<>();
						promoList.add(convertPromotionToBean(promoItem));
						genericOffersListBean.setOffers(promoList);
						sortedPromoMap.put(promoType, genericOffersListBean);
					}
				}
			}

		}
		GenericOffersListBean genericOffersListBean = sortedPromoMap.get(reqPromotType);
		if(genericOffersListBean != null ){
			List<GenericOfferDetailsBean> offers = genericOffersListBean.getOffers();
			if(offers != null && !offers.isEmpty()){
				Collections.sort( offers, PROMOS_SORT_BY_DISPLAY_ORDER );
			}
		}
		return sortedPromoMap;
	}

	protected GenericOfferDetailsBean convertCouponPromotionToBean(RepositoryItem coupon, RepositoryItem couponPromotion) {
		DigitalCommercePropertyManager commercePropertyManager = getCommercePropertyManager();
		GenericOfferDetailsBean genericOfferDetailsBean = new GenericOfferDetailsBean();
		genericOfferDetailsBean.setOfferCode(coupon.getRepositoryId());
		genericOfferDetailsBean.setDescription((String) couponPromotion.getPropertyValue(commercePropertyManager.getPromotionUserFriendlyDescPropertyName()));
		genericOfferDetailsBean.setName((String) couponPromotion.getPropertyValue(commercePropertyManager.getPromotionUserFriendlyNamePropertyName()));
		RepositoryItem shopNowLinkRepItem =  (RepositoryItem)couponPromotion.getPropertyValue(commercePropertyManager.getPromotionShopNowLinkPropertyName());
		String shopNowLink=null;
		if (shopNowLinkRepItem!=null){
			shopNowLink=(String)shopNowLinkRepItem.getPropertyValue("url");
		}
		genericOfferDetailsBean.setShopNowLink(shopNowLink );
		genericOfferDetailsBean.setOfferDetailsPageName((String) couponPromotion.getPropertyValue(commercePropertyManager.getPromotionOfferDetailsPageNamePropertyName()));
		genericOfferDetailsBean.setType(getPromotionTypeString((Integer) couponPromotion.getPropertyValue(commercePropertyManager.getPromotionTypePropertyName())));

		String iconType = (String) couponPromotion.getPropertyValue(commercePropertyManager.getPromotionDiscountIconTypePropertyName());

		if (DigitalStringUtil.isBlank(iconType)) {
			iconType = ICONTYPE_NONE;
		}
		genericOfferDetailsBean.setIconType(iconType);

		Date endDate = (Date) coupon.getPropertyValue(commercePropertyManager.getPromotionExpirationDatePropertyName());
		String expDateStr = customDateFormatter.getLocalizedDateString(endDate);
		genericOfferDetailsBean.setEndDate(expDateStr);

		Date startDate = (Date) coupon.getPropertyValue(commercePropertyManager.getPromotionStartDatePropertyName());
		String issueDateStr = customDateFormatter.getLocalizedDateString(startDate);
		genericOfferDetailsBean.setStartDate(issueDateStr);

		Object displayOrder = couponPromotion.getPropertyValue("displayOrder");
		if(displayOrder instanceof Integer){
			genericOfferDetailsBean.setDisplayOrder((Integer) displayOrder);
		}

		return genericOfferDetailsBean;
	}

	protected GenericOfferDetailsBean convertPromotionToBean(RepositoryItem promotion) {
		DigitalCommercePropertyManager commercePropertyManager = getCommercePropertyManager();
		GenericOfferDetailsBean genericOfferDetailsBean = new GenericOfferDetailsBean();
		if(!getPromotionTools().isImplicitPromotion(promotion)) {
			genericOfferDetailsBean.setOfferCode(promotion.getRepositoryId());
		}
		genericOfferDetailsBean.setDescription((String) promotion.getPropertyValue(commercePropertyManager.getPromotionUserFriendlyDescPropertyName()));
		genericOfferDetailsBean.setName((String) promotion.getPropertyValue(commercePropertyManager.getPromotionUserFriendlyNamePropertyName()));
		RepositoryItem shopNowLinkRepItem =  (RepositoryItem)promotion.getPropertyValue(commercePropertyManager.getPromotionShopNowLinkPropertyName());
		String shopNowLink=null;
		if (shopNowLinkRepItem!=null){
			shopNowLink=(String)shopNowLinkRepItem.getPropertyValue("url");
		}
		genericOfferDetailsBean.setShopNowLink(shopNowLink);

		genericOfferDetailsBean.setOfferDetailsPageName((String) promotion.getPropertyValue(commercePropertyManager.getPromotionOfferDetailsPageNamePropertyName()));
		genericOfferDetailsBean.setType(getPromotionTypeString((Integer) promotion.getPropertyValue(commercePropertyManager.getPromotionTypePropertyName())));

		String iconType = (String) promotion.getPropertyValue(commercePropertyManager.getPromotionDiscountIconTypePropertyName());
		if (DigitalStringUtil.isBlank(iconType)) {
			iconType = ICONTYPE_NONE;
		}
		genericOfferDetailsBean.setIconType(iconType);

		Date endDate = (Date) promotion.getPropertyValue(commercePropertyManager.getPromotionEndUsablePropertyName());
		String expDateStr = customDateFormatter.getLocalizedDateString(endDate);
		genericOfferDetailsBean.setEndDate(expDateStr);

		Date startDate = (Date) promotion.getPropertyValue(commercePropertyManager.getPromotionBeginUsablePropertyName());
		String issueDateStr = customDateFormatter.getLocalizedDateString(startDate);
		genericOfferDetailsBean.setStartDate(issueDateStr);

		Object displayOrder = promotion.getPropertyValue("displayOrder");
		if(displayOrder instanceof Integer){
			genericOfferDetailsBean.setDisplayOrder((Integer) displayOrder);
		}
		return genericOfferDetailsBean;
	}

	/**
	 * @param type
	 * @return
	 */
	private String getPromotionTypeString(Integer type) {
		if (type == 0 || type == 1 || type == 2 || type == 3) {
			return PromotionType.ITEM_DISCOUNT.toString();
		}

		if (type == 5 || type == 6 || type == 7 || type == 8) {
			return PromotionType.SHIPPING_DISCOUNT.toString();
		}

		if (type == 9 || type == 10 || type == 11 || type == 12) {
			return PromotionType.ORDER_DISCOUNT.toString();
		}
		return Integer.toString(type);
	}

	/**
	 * populates all the promotions
	 *
	 * @throws IOException
	 * @throws ServletException
	 */
	protected Collection<?> initializeSiteGroupPromotions(DynamoHttpServletRequest pRequest,
														 DynamoHttpServletResponse pResponse) throws ServletException, IOException {

		String METHOD_NAME = "initializeSiteGroupPromotions";

		// Result collection
		Collection filteredSavings = new ArrayList();

		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);

			CollectionObjectValidator[] validators = getFilter().getValidators();

			Collection unfilteredCollection = new ArrayList();

			for (CollectionObjectValidator validator : validators) {
				if (validator instanceof DigitalGlobalPromotionValidator) {
					((DigitalGlobalPromotionValidator) validator).setLoadOnlyGlobalPromotions(false);
				}
			}

			// All promotions
			unfilteredCollection.addAll(getPromotionService().findAllPromotions());

			// Check that there are promotions to filter
			// Filter all promotions using filter parameter
			filteredSavings = getFilter().filterCollection(unfilteredCollection, null, null);

			// All coupons
			filteredSavings.addAll(getCouponService().findAllCoupons(true));

		} catch (FilterException e) {
			String msg = "Item does not have a sites property";
			if (isLoggingError()) {
				logError(msg);
			}
			pRequest.setParameter("errorMsg", msg);
			pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
		}
		return filteredSavings;
	}

	Integer getRibbonOrder(String ribbonType) {
		Map<String, String> ribbonOrders = getDswConstants().getRibbonOrder();
		if (ribbonOrders != null) {
			return new Integer(ribbonOrders.get(ribbonType));
		} else {
			logError("Missing RibbonOrder config in DigitalConstants");
			return null;
		}
	}

	public static final Comparator<GenericOfferDetailsBean> PROMOS_SORT_BY_DISPLAY_ORDER	= new Comparator<GenericOfferDetailsBean>() {
		@Override
		public int compare( GenericOfferDetailsBean o1, GenericOfferDetailsBean o2 ) {
			int retVal = 0;
			if( o1 != null && o2 != null) {
				retVal = ComparisonChain.start()
						.compare( o1.getDisplayOrder(), o2.getDisplayOrder(), Ordering.natural().nullsLast() )
						.result();
			}
			return retVal;
		}
	};


	public DigitalCustomDateFormatter getCustomDateFormatter() {
		return customDateFormatter;
	}

	public void setCustomDateFormatter(DigitalCustomDateFormatter customDateFormatter) {
		this.customDateFormatter = customDateFormatter;
	}

	public DigitalCommercePropertyManager getCommercePropertyManager() {

		return (DigitalCommercePropertyManager) this.getProfileTools().getPropertyManager();
	}

	public Order getOrder() {
		OrderHolder orderHolder =  ComponentLookupUtil.lookupComponent( ComponentLookupUtil.SHOPPING_CART, OrderHolder.class  );
		if(orderHolder != null){
			return orderHolder.getCurrent();
		}else{
			return null;
		}
	}

	public DigitalPromotionTools getPromotionTools(){
		return (DigitalPromotionTools)this.getCouponService().getPromotionTools();
	}

}
