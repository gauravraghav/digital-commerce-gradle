package com.digital.commerce.services.promotions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.coupons.DigitalCouponService;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.promotion.DigitalPromotionTools;
import com.digital.commerce.services.promotions.collections.validator.DigitalGlobalPromotionValidator;
import com.digital.commerce.services.rewards.domain.RewardsRetrieveRewardCertificatesResponse;
import com.digital.commerce.services.utils.DigitalServiceConstants;

import atg.dtm.UserTransactionDemarcation;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.collections.filter.FilterException;
import atg.service.collections.filter.ValidatorFilter;
import atg.service.collections.validator.CollectionObjectValidator;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalAvailableSavingsDroplet extends DynamoServlet {
	protected static final String CLASSNAME = DigitalAvailableSavingsDroplet.class.getName();
	enum PromotionType { ITEM_DISCOUNT, ORDER_DISCOUNT, SHIPPING_DISCOUNT }

	private static final ParameterName OUTPUT = ParameterName
			.getParameterName("output");
	private static final ParameterName EMPTY = ParameterName
			.getParameterName("empty");
	private static final ParameterName ERROR_OPARAM = ParameterName
			.getParameterName("error");

	private static final String    PERFORM_MONITOR_NAME    = "DigitalPromotionFilterDroplet";

	private DigitalPromotionService promotionService;

	private Profile profile;

	private DigitalProfileTools profileTools;

	private DigitalServiceConstants dswConstants;

	private DigitalRewardsManager rewardCertificateManager;

	private boolean loadOnlyGlobalPromo = false;

	private boolean loadOffersAndCerts = false;

	private boolean displayRewardCerts = false;

	private static final ParameterName LOAD_OFFERS_AND_CERTS = ParameterName.getParameterName("loadOffersAndCerts");

	private static final ParameterName LOAD_ONLY_GLOBAL_PROMO = ParameterName.getParameterName("loadOnlyGlobalPromo");

	private static final ParameterName DISPLAY_REWARDS_AND_CERTS = ParameterName.getParameterName("checkout");

	private DigitalCouponService couponService;


	/**
	 * property: filter
	 */
	private ValidatorFilter filter;

	/**
	 *
	 * @param pRequest
	 */
	protected void initializeRequestParameters(DynamoHttpServletRequest pRequest) {
		String loadOffersAndCerts = pRequest.getParameter(LOAD_OFFERS_AND_CERTS);
		if (loadOffersAndCerts != null) {
			setLoadOffersAndCerts(Boolean.valueOf(loadOffersAndCerts).booleanValue());
		}

		String loadOnlyGlobalPromo = pRequest.getParameter(LOAD_ONLY_GLOBAL_PROMO);
		if (loadOnlyGlobalPromo != null) {
			setLoadOnlyGlobalPromo(Boolean.valueOf(loadOnlyGlobalPromo).booleanValue());
		}

		String displayRewardsCerts = pRequest.getParameter(DISPLAY_REWARDS_AND_CERTS);
		if(displayRewardsCerts!=null){
			this.setDisplayRewardCerts(Boolean.valueOf(displayRewardsCerts).booleanValue());
		}
	}




	/**
	 * This method will call existing initalizeAndFilterPromotions method.
	 * Render oparam output with parameter filteredPromotions.
	 *
	 * @param pRequest
	 * @param pResponse
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		String METHOD_NAME = "service";
		UserTransactionDemarcation td = null;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
			td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);


			initializeRequestParameters(pRequest);

			LinkedHashMap<String, LinkedHashMap> sortedPromoMap = new LinkedHashMap<>();

			Collection<RepositoryItem> filteredPromotions = null;

			filteredPromotions = (Collection<RepositoryItem>) initalizeSiteGroupPromotions(pRequest, pResponse);

			if (!filteredPromotions.isEmpty()) {
				sortedPromoMap.put("1", groupPromotionsByType(filteredPromotions, 0));
				sortedPromoMap.put("2", groupPromotionsByType(filteredPromotions, 9));
			}
				try {
					if (!profileTools.isDSWAnanymousUser(profile) && profileTools.getLoyaltyNumber(profile) != null) {
						LinkedHashMap offerMap = new LinkedHashMap<String, List>();

						boolean isValidLoyaltyMember = false;

						final RewardsRetrieveRewardCertificatesResponse rewardsResp = getRewardCertificateManager().retrieveRewardCertificatesByProfile(getProfile());

						isValidLoyaltyMember = rewardsResp.isValidLoyaltyMember();

						double totalCertValue = rewardsResp.getTotalCertValue();
						List certValue = new ArrayList<>();
						certValue.add(totalCertValue);

						offerMap.put("offers", rewardCertificateManager.getProfileOffers(profile, rewardsResp));
						sortedPromoMap.put("3", offerMap);

						LinkedHashMap webOffers = new LinkedHashMap<String, List>();
						webOffers.put("webOffers", getPromotionTools().getWebAuthOffers(isValidLoyaltyMember, profile));
						sortedPromoMap.put("4", webOffers);

						if (isLoadOffersAndCerts()) {
							// Load Certs and Offers to the profile using loyalty service
							getRewardCertificateManager().loadProfileCertsAndOffersUsingLoyaltyService(profile, rewardsResp);
						}

						LinkedHashMap rewardsAmount = new LinkedHashMap<String, List>();
						rewardsAmount.put("rewardsAmount", certValue);
						rewardsAmount.put("dollarsFutureReward", profile.getPropertyValue("dollarsFutureReward"));
						rewardsAmount.put("dollarsNextReward", profile.getPropertyValue("dollarsNextReward"));
						sortedPromoMap.put("5", rewardsAmount);

						if(isDisplayRewardCerts()){
							//load rewards certificates if checkout is true
							LinkedHashMap rewardPerks = new LinkedHashMap<String, List>();
							rewardPerks.put("rewardCertificates", rewardCertificateManager.getProfileCertificates(profile));
							sortedPromoMap.put("7", rewardPerks);
						}

						Object obj = profile.getPropertyValue("dollarsNextReward");
						if(obj != null){
							long dollarNextReward = (long)obj;
							int rewardsThresholdAmount = MultiSiteUtil.getRewardsThresholdAmount();
							if(rewardsThresholdAmount > 0 && dollarNextReward <= rewardsThresholdAmount){
								rewardsAmount.put("displayRewardsCertClosenessQualifier", true);
							}else{
								rewardsAmount.put("displayRewardsCertClosenessQualifier", false);
							}

						}else{//if dollarsNextReward is null then there is no point is displaying the cert closeness Qualifier
							rewardsAmount.put("displayRewardsCertClosenessQualifier", false);
						}

						sortedPromoMap.put("5", rewardsAmount);
					} else {
						if (isLoggingDebug()) {
							logDebug("User profile " + profile.getRepositoryId()
									+ " is not a rewards or cookied user, no need to calls rewards");
						}
					}

				} catch (Exception e) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					String msg = "Unable to query loyalty number for the profile";
					if (isLoggingError()) {
						logError(msg);
					}
					pRequest.setParameter("errorMsg", msg);
					pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
				}
				if (!filteredPromotions.isEmpty()) {
					sortedPromoMap.put("6", groupPromotionsByType(filteredPromotions, 5));
				}


			pRequest.setParameter("sortedPromotions", sortedPromoMap);

			if (sortedPromoMap == null || sortedPromoMap.isEmpty()) {
				if (isLoggingDebug()) {
					logDebug("No promotions available");
				}
				pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			} else {
				if (isLoggingDebug()) {
					logDebug("sortedPromotions  --> size"+sortedPromoMap.size());
				}
				pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
			}
		} catch (RepositoryException e) {
			TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
			String msg = "Unable to fetch available savings";
			if (isLoggingError()) {
				logError(e.getMessage());
			}
			pRequest.setParameter("errorMsg", msg);
			pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
		}finally {
			TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
			TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
		}
	}

	/**
	 * Groups the promotions based on type
	 * @throws RepositoryException
	 *
	 * @throws IOException
	 * @throws ServletException
	 */
	protected LinkedHashMap<String, List> groupPromotionsByType(Collection<RepositoryItem> filteredPromotions,int type) throws RepositoryException {
		LinkedHashMap<String, List> sortedPromoMap = new LinkedHashMap<>();
			for (RepositoryItem promoItem : filteredPromotions) {

				String promoType = null;
				if(("DeployablePromotionClaimable").equals(promoItem.getItemDescriptor().getItemDescriptorName())){
					if ((boolean)promoItem.getPropertyValue("hasPromotions"))
					{
						Set<RepositoryItem> couponPromotions = (Set<RepositoryItem>)promoItem.getPropertyValue("promotions");

						if (couponPromotions!=null && couponPromotions.size()!=0){
							promoType =getPromotionTypeString((Integer)((RepositoryItem)couponPromotions.toArray()[0]).getPropertyValue("type"));
							if (promoType.equalsIgnoreCase(getPromotionTypeString(type))){
								if (sortedPromoMap.get(promoType)!=null && sortedPromoMap.get(promoType).size()>0){
									sortedPromoMap.get(promoType).add(promoItem);
								}
								else
								{
									ArrayList<RepositoryItem> promoList = new ArrayList<>();
									promoList.add(promoItem);
									sortedPromoMap.put(promoType , promoList);
								}
							}
						}
					}
				}
				else {
					promoType = getPromotionTypeString((Integer)promoItem.getPropertyValue("type"));
					if (promoType.equalsIgnoreCase(getPromotionTypeString(type))){
						if (sortedPromoMap.get(promoType)!=null && sortedPromoMap.get(promoType).size()>0){
							sortedPromoMap.get(promoType).add(promoItem);
						}
						else
						{
							ArrayList<RepositoryItem> promoList = new ArrayList<>();
							promoList.add(promoItem);
							sortedPromoMap.put(promoType , promoList);
						}
					}
				}

			}
		return sortedPromoMap;
	}

	/**
	 *
	 * @param type
	 * @return
	 */
	private String getPromotionTypeString(Integer type){
		if(type.intValue() == 0 || type.intValue() == 1 || type.intValue() == 2 || type.intValue() == 3){
			return PromotionType.ITEM_DISCOUNT.toString();
		}

		if(type.intValue() == 5 || type.intValue() == 6 || type.intValue() == 7 || type.intValue() == 8){
			return PromotionType.SHIPPING_DISCOUNT.toString();
		}

		if(type.intValue() == 9 || type.intValue() == 10 || type.intValue() == 11 || type.intValue() == 12){
			return PromotionType.ORDER_DISCOUNT.toString();
		}
		return Integer.toString(type);
	}

	/**
	 * populates all the promotions
	 *
	 * @throws IOException
	 * @throws ServletException
	 */
	protected Collection<?> initalizeSiteGroupPromotions(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {

		String METHOD_NAME = "initalizeSiteGroupPromotions";

		// Result collection
		Collection filteredSavings = new ArrayList();

		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);

			CollectionObjectValidator[] validators = getFilter().getValidators();

			Collection unfilteredCollection = new ArrayList();

			for (CollectionObjectValidator validator : validators) {
				if (validator instanceof DigitalGlobalPromotionValidator) {
					((DigitalGlobalPromotionValidator) validator).setLoadOnlyGlobalPromotions(isLoadOnlyGlobalPromo());
				}
			}

			// All promotions
			unfilteredCollection.addAll(getPromotionService().findAllPromotions());

			// Check that there are promotions to filter
			if (unfilteredCollection != null || !unfilteredCollection.isEmpty()) {
				// Filter all promotions using filter parameter
				filteredSavings = getFilter().filterCollection(unfilteredCollection, null, null);
			}

			// All coupons
			filteredSavings.addAll(getCouponService().findAllCoupons(true));

		} catch (FilterException e) {
			String msg = "Item does not have a sites property";
			if (isLoggingError()) {
				logError(msg);
			}
			pRequest.setParameter("errorMsg", msg);
			pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
		}
		return filteredSavings;
	}

	public DigitalPromotionTools getPromotionTools(){
		return (DigitalPromotionTools)this.getCouponService().getPromotionTools();
	}

}
