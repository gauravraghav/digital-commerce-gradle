package com.digital.commerce.services.promotion;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import lombok.Getter;
import lombok.Setter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelProperties;
import atg.commerce.pricing.definition.PMDLParser;
import atg.commerce.pricing.definition.PricingModelElem;
import atg.commerce.pricing.definition.QualifierElem;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.service.collections.validator.CollectionObjectValidator;
import atg.userprofiling.Profile;

import com.digital.commerce.common.logger.DigitalLogger;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalPromotionProfileValidator extends ApplicationLoggingImpl implements
		CollectionObjectValidator {
	private static final DigitalLogger logger = DigitalLogger
			.getLogger(DigitalPromotionProfileValidator.class);
	
	private Profile profile;
	
	private PMDLParser pmdlParser;
	
	private PricingModelProperties pricingModelProperties;
	
	private boolean enabled;
	
	private final static String START = "<qualifier>";
	private final static String END = "</qualifier>";

	@Override
	public boolean validateObject(Object pObject) {
		
		if(!isEnabled())
			return true;
				
		if (!(pObject instanceof RepositoryItem)) {
			return false;
		}
		
		RepositoryItem promo = (RepositoryItem)pObject;
		String xmlrule = (String)promo.getPropertyValue(getPricingModelProperties().getPMDLRule());
		
		
		try {
			if (xmlrule.indexOf(START)==-1 || xmlrule.indexOf(END)+END.length()==-1)
			{
				return true;
			}
			String older = xmlrule.substring(xmlrule.indexOf(START), xmlrule.indexOf(END)+END.length());
			String profileQualifier = removeNoneProfileQualifier(older);
			
			if(profileQualifier == null)
				return true;
			
			PricingModelElem model = null;
			try(StringReader inreader = new StringReader(xmlrule.replace(older, profileQualifier)))
			{
				InputSource pin = new InputSource(inreader);
				model = (PricingModelElem)pmdlParser.parse(pin);
			}
			
			if(model != null){
				QualifierElem qElem = model.getQualifier();
				Map pObjectBindings = new HashMap();
				pObjectBindings.put("profile", this.getProfile());
				return (boolean) (Boolean)qElem.evaluate(null, this, pObjectBindings);
			}else{
				return true;
			}
			
		} catch (SAXException | IOException | ParserConfigurationException
				| TransformerFactoryConfigurationError
				| TransformerException |PricingException e) {
			logger.error(e);
		} 
		return true;
		
	}
	
	public String removeNoneProfileQualifier(String qualifier) throws SAXException, IOException, ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException{
		StringBuilder qStr = new StringBuilder("<qualifier><or>");
		
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
		        .newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(qualifier));
		Document document = docBuilder.parse(is);
		ArrayList<Node> profileNodes = new ArrayList<>();
		extractProfileNode(document.getDocumentElement(), profileNodes);
		
		if(profileNodes.size() == 0)
			return null;
		
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		StringWriter sw = new StringWriter();
		for(Node node : profileNodes){
			transformer.transform(new DOMSource(node), new StreamResult(sw));
			qStr.append(sw.toString());
		}
		
		qStr.append("</or></qualifier>");
		
		return qStr.toString();
		
	}
	
	public void extractProfileNode(Node node, ArrayList<Node> profileNodes) {
	    if(node.getNodeName().equals("value") && node.getTextContent().startsWith("profile.")){
	    	profileNodes.add(node.getParentNode());
	    }

	    NodeList nodeList = node.getChildNodes();
	    for (int i = 0; i < nodeList.getLength(); i++) {
	        Node currentNode = nodeList.item(i);
	        if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
	            //calls this method for all the children which is Element
	        	extractProfileNode(currentNode, profileNodes);
	        }
	    }
	}
    
	
}
