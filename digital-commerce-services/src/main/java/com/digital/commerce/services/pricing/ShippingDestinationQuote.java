/**
 * 
 */
package com.digital.commerce.services.pricing;

import lombok.Getter;
import lombok.Setter;

/** @author wibrahim */
@Getter
@Setter
public class ShippingDestinationQuote extends ShippingQuote {
	private String	shippingDestinationId;
	private String	oldServiceLevel;
	private String	serviceLevel;
}
