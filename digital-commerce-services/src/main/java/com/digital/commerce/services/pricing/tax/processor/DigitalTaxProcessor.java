/**
 *
 */
package com.digital.commerce.services.pricing.tax.processor;

import static com.digital.commerce.constants.DigitalProfileConstants.SHIPPING_HANDLING_TAX_PROPERTY;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import atg.commerce.order.CommerceItem;
import atg.commerce.payment.DummyTaxStatus;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.payment.tax.ShippingDestination;
import atg.payment.tax.TaxProcessor;
import atg.payment.tax.TaxRequestInfo;
import atg.payment.tax.TaxRequestInfoImpl;
import atg.payment.tax.TaxStatus;
import atg.payment.tax.TaxableItem;
import atg.repository.MutableRepository;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.tax.TaxService;
import com.digital.commerce.integration.tax.domain.TaxServiceAddress;
import com.digital.commerce.integration.tax.domain.TaxServiceFlexibleCodeField;
import com.digital.commerce.integration.tax.domain.TaxServiceLineItem;
import com.digital.commerce.integration.tax.domain.TaxServiceProduct;
import com.digital.commerce.integration.tax.domain.TaxServiceRequest;
import com.digital.commerce.integration.tax.domain.TaxServiceResponse;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.purchase.DigitalGCComponent;
import com.digital.commerce.services.pricing.DigitalTaxableItemImpl;
import com.digital.commerce.services.pricing.tax.DigitalTaxStatus;
import lombok.Getter;
import lombok.Setter;

/** @author */
@Getter
@Setter
public class DigitalTaxProcessor extends ApplicationLoggingImpl implements TaxProcessor {
	private MutableRepository		productRepository;
	private TaxService				taxService;
	private static final String		GIFT_CARD_TYPE	= "giftCard";
	private static final DigitalLogger	logger			= DigitalLogger.getLogger( DigitalGCComponent.class );

	public DigitalTaxProcessor() {
		super( DigitalTaxProcessor.class.getName() );
	}

	public TaxStatus calculateTax( TaxRequestInfo pTaxInfo ) {
		if( isLoggingDebug() ) {
			logDebug( "calculateTax(TaxRequestInfo pTaxInfo=" + pTaxInfo + ") - start" );
		}
		final DigitalOrderImpl order = (DigitalOrderImpl)pTaxInfo.getOrder();
		DigitalTaxStatus ret = new DigitalTaxStatus();
		TaxRequestInfoImpl taxInfo = (TaxRequestInfoImpl)pTaxInfo;

		TaxServiceRequest taxServiceRequest = new TaxServiceRequest();
		taxServiceRequest.setOrderNumber(taxInfo.getOrderId());
		
		ShippingDestination[] shippingDestinations = taxInfo.getShippingDestinations();
		List<TaxServiceLineItem> taxServiceLineItemList = new ArrayList<>();
		Boolean containsShippingAddress = false;
		for( ShippingDestination shippingDestination : shippingDestinations ) {

			if( shippingDestination.getShippingAddress() != null
					&& DigitalStringUtil.isNotBlank( shippingDestination.getShippingAddress().getAddress1() )
					&& DigitalStringUtil.isNotBlank( shippingDestination.getShippingAddress().getPostalCode() )
					&& DigitalStringUtil.isNotBlank( shippingDestination.getShippingAddress().getCity() ) ) {
				TaxServiceAddress taxServiceAddress = new TaxServiceAddress();
				containsShippingAddress = true;
				taxServiceAddress.setStreetAddress( shippingDestination.getShippingAddress().getAddress1() );
				taxServiceAddress.setCity( shippingDestination.getShippingAddress().getCity() );
				taxServiceAddress.setCountry( shippingDestination.getShippingAddress().getCountry() );
				taxServiceAddress.setMainDivision( shippingDestination.getShippingAddress().getState() );
				taxServiceAddress.setSubDivision( shippingDestination.getShippingAddress().getCounty() );
				taxServiceAddress.setPostalCode( shippingDestination.getShippingAddress().getPostalCode() );

				TaxServiceLineItem li = new TaxServiceLineItem();

				li.setAmount( shippingDestination.getShippingAmount() );
				li.setQuantity( 1 );
				li.setDestination( taxServiceAddress );
				TaxServiceProduct taxServiceProduct = new TaxServiceProduct();
				taxServiceProduct.setProductClass( SHIPPING_HANDLING_TAX_PROPERTY );
				li.setTaxServiceProduct( taxServiceProduct );
				boolean isShippingLineItemAdded = false;

				for( TaxableItem taxableItem : shippingDestination.getTaxableItems() ) {
					TaxServiceLineItem taxli = new TaxServiceLineItem();
					double taxableItemAmount = taxableItem.getAmount();
					double unitPrice = taxableItemAmount / taxableItem.getQuantity();
					/** GiftCard Item -Start */
					try {
						@SuppressWarnings("unchecked")
						// I know getCommerceItems will return List of CommerceItems
						List<CommerceItem> ciList = (List<CommerceItem>)order.getCommerceItems();

						for( Iterator<CommerceItem> citerator = ciList.iterator(); citerator.hasNext(); ) {
							CommerceItem ci = citerator.next();
							if( ci.getCatalogRefId().equals( taxableItem.getCatalogRefId() )
									&& GIFT_CARD_TYPE.equals( ci.getCommerceItemClassType() ) ) {
								taxli.setNonTaxable( true );
								break;
							}
						}
					} catch( Exception e ) {
						logger.error( "Exception: While checking Gift Cards in Order - " + order.getId(), e );
					}
					/** GiftCard Item -End */

					taxli.setAmount( unitPrice );
					taxli.setQuantity( taxableItem.getQuantity() );
					taxli.setDestination( taxServiceAddress );
					TaxServiceProduct taxServiceProd = new TaxServiceProduct();
					taxServiceProd.setValue( taxableItem.getCatalogRefId() );
					taxServiceProd.setProductClass( getTaxCategoryForProduct( taxableItem.getProductId() ) );
					taxli.setTaxServiceProduct( taxServiceProd );

					// For BOPIS / BOSTS
					DigitalTaxableItemImpl dswTaxableItem = (DigitalTaxableItemImpl)taxableItem;

					if( DigitalStringUtil.isNotBlank( dswTaxableItem.getShipType() )
							&& !dswTaxableItem.getShipType().trim().equalsIgnoreCase( "ship" ) ) {
						String shipNode = dswTaxableItem.getStoreId();
						if(DigitalStringUtil.isNotBlank(shipNode)) {
							List<TaxServiceFlexibleCodeField> fFields = new ArrayList<>();
							TaxServiceFlexibleCodeField fField = new TaxServiceFlexibleCodeField();
							fField.setFieldId(3);
							fField.setValue(shipNode);
							fFields.add(fField);
							taxli.setTaxServiceFlexibleCodeFields(fFields);
						}
					}
					if(!isShippingLineItemAdded) {
						taxServiceLineItemList.add(li);
						isShippingLineItemAdded = true;
					}
					taxServiceLineItemList.add( taxli );
				}
			}
		}
		taxServiceRequest.setLineItems( taxServiceLineItemList );
		TaxServiceResponse taxServiceResponse = null;

		if( shippingDestinations.length > 0 && containsShippingAddress ) {
			try {
				taxServiceResponse = taxService.calculateTax( taxServiceRequest );

				ret = getDSWTaxStatus( taxServiceResponse );
				ret.setTransactionSuccess( true );
				if(order.isTaxOffline()) {
					order.setTaxOffline(false);
				}
			} catch( DigitalIntegrationException e ) {
				if( isLoggingDebug() ) {
					logger.error( "DigitalTAXPROCESSOR-DigitalIntegrationException--->" + e.getMessage() );
				}
				if( isLoggingError() ) {
					logger.error( "DigitalTAXPROCESSOR-DigitalIntegrationException--->" + e );
				}
				order.setTaxOffline( true );
				ret.setTransactionSuccess( true );
			}
		} else {
			if( isLoggingDebug() ) {
				logDebug( "Nothing to tax!!!" );
			}
			ret.setAmount( 0.0 );
			ret.setCityTax( 0.0 );
			ret.setCountryTax( 0.0 );
			ret.setCountyTax( 0.0 );
			ret.setDistrictTax( 0.0 );
			ret.setErrorMessage( "" );
			ret.setStateTax( 0.0 );
			ret.setTransactionErrorMessage( "" );
			ret.setTransactionId( "" );
			ret.setTransactionSuccess( true );
			ret.setTransactionTimestamp( new Date() );
		}
		if( isLoggingDebug() ) {
			logDebug( "calculateTax(TaxRequestInfo) - end" ); //$NON-NLS-1$
		}
		return ret;
	}

	/** This method creates DigitalTaxStatus from taxServiceResponse received in the
	 * response of Vertex Tax Service
	 * 
	 * @param taxInfo
	 * @return */
	private DigitalTaxStatus getDSWTaxStatus( TaxServiceResponse taxServiceResponse ) {
		if( isLoggingDebug() ) {
			logDebug( "DigitalTaxProcessor:getDSWTaxStatus : " + taxServiceResponse );
		}
		DigitalTaxStatus dSWTaxStatus = new DigitalTaxStatus();
		dSWTaxStatus.setCurrency( taxServiceResponse.getCurrency() );
		double stateTax = 0d;
		double cityTax = 0d;
		double countryTax = 0d;
		double countyTax = 0d;
		for( TaxServiceLineItem taxServiceLineItem : taxServiceResponse.getLineItems() ) {
			countyTax = DigitalCommonUtil.round( countyTax + taxServiceLineItem.getCountyTax() );
			cityTax = DigitalCommonUtil.round( cityTax + taxServiceLineItem.getCityTax() );
			stateTax = DigitalCommonUtil.round( stateTax + taxServiceLineItem.getStateTax() );
			countryTax = DigitalCommonUtil.round( countryTax + taxServiceLineItem.getCountryTax() );
		}
		dSWTaxStatus.setCityTax( cityTax );
		dSWTaxStatus.setCountryTax( countryTax );
		dSWTaxStatus.setCountyTax( countyTax );
		dSWTaxStatus.setStateTax( stateTax );
		dSWTaxStatus.setAmount( DigitalCommonUtil.round( taxServiceResponse.getAmount() ) );
		dSWTaxStatus.setTransactionSuccess( true );
		dSWTaxStatus.setDistrictTax( 0.0 );
		dSWTaxStatus.setTransactionTimestamp( new Date() );
		dSWTaxStatus.setErrorMessage( "" );
		dSWTaxStatus.setTransactionErrorMessage( "" );
		dSWTaxStatus.setTransactionId( "" );

		return dSWTaxStatus;
	}

	/** Reads taxCategory for a given product id from the the Product catalog.
	 * 
	 * @param productId
	 *            the product id
	 * @return Product tax category */
	private String getTaxCategoryForProduct( String productId ) {

		Repository repo = getProductRepository();
		try {
			RepositoryItem productItem = repo.getItem( productId, "product" );
			String taxCategory = "";
			if( productItem != null ) {
				taxCategory = (String)productItem.getPropertyValue( "taxCategory" );
			}
			return ( taxCategory == null ? "" : taxCategory );
		} catch( RepositoryException e ) {
			logError( e );
			return ""; // @ TODO : what if this happens? Tax category is sent
			// as an empty string
		}

	}

	/* (non-Javadoc)
	 * @see atg.payment.tax.TaxProcessor#calculateTaxByShipping(atg.payment.tax.
	 * TaxRequestInfo) */
	public TaxStatus[] calculateTaxByShipping( TaxRequestInfo pTaxInfo ) {
		if( isLoggingDebug() ) {
			logDebug( "WE DON'T CALCULATE TAX BY SHIPPING!!!" );
		}
		if( isLoggingDebug() ) {
			logDebug( "calculateTaxByShipping(TaxRequestInfo pTaxInfo=" + pTaxInfo + ") - start" ); //$NON-NLS-1$ //$NON-NLS-2$
		}

		DummyTaxStatus[] ret = new DummyTaxStatus[pTaxInfo.getShippingDestinations().length];

		for( int i = 0; i < pTaxInfo.getShippingDestinations().length; i++ ) {
			if( isLoggingDebug() ) {
				ShippingDestination dest = pTaxInfo.getShippingDestinations()[i];
				logDebug( "DUMMY TAX PROCESSOR taxing a GROUP with amount : " + dest.getTaxableItemAmount() );
			}
			ret[i] = new DummyTaxStatus();
			ret[i].setTransactionSuccess( true );
		}

		if( isLoggingDebug() ) {
			logDebug( "calculateTaxByShipping(TaxRequestInfo) - end" ); //$NON-NLS-1$
		}
		return ret;
	}
}
