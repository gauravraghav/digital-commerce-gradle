package com.digital.commerce.services.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import lombok.Getter;
import lombok.Setter;

/**
 * Service for logging INFO level messages to the client
 * 
 * @author MS394496
 */
@Getter
@Setter
public final class ClientLogger {

	private static transient DigitalLogger logger = DigitalLogger
			.getLogger(ClientLogger.class);
	private static final int MAX_PAGE_LENGTH = 256;
	private static final int MAX_METHOD_LENGTH = 256;
	private static final int MAX_PRODUCT_ID_LENGTH = 40;
	private static final int MAX_ORDER_ID_LENGTH = 40;
	private static final int MAX_MESSAGE_LENGTH = 512;
	private static final int MAX_TIMESTAMP_LENGTH = 40;

	private static final String PREPEND_PAGE_PARTIAL = "Page: ";
	private static final String PREPEND_METHOD_PARTIAL = "Method: ";
	private static final String PREPEND_PRODUCT_ID_PARTIAL = "Product ID: ";
	private static final String PREPEND_ORDER_ID_PARTIAL = "Order ID: ";
	private static final String PREPEND_MESSAGE_PARTIAL = "Message: ";
	private static final String PREPEND_TIMESTAMP_PARTIAL = "Timestamp: ";
	private Map<String, Object> error;
	private DigitalServiceConstants			dswConstants;
	


	

	/**
	 * Logs an INFO log level message
	 * 
	 * @param page
	 *            The name of the page or script making the logging request and
	 *            not the full path. Limit 256 characters.
	 * @param method
	 *            The name of the method making the logging request. Limit 256
	 *            characters.
	 * @param productId
	 *            The product ID on the page making the logging request. Limit
	 *            40 characters.
	 * @param orderId
	 *            The order ID on the page making the logging request. Limit 40
	 *            characters.
	 * @param message
	 *            The message to log. Limit 512 characters. Required.
	 * @param timestamp
	 *            The JavaScript epoch timestamp to log. Limit 40 characters.
	 * @throws Exception
	 */
	public void logMessage(String page, String method, String productId,
			String orderId, String message, String timestamp) throws Exception {
		try {
			List<String> allowedMethods = (dswConstants != null && dswConstants
					.getAllowedLoggingMethods() != null) ? dswConstants
					.getAllowedLoggingMethods() : null;
			if (logger.isDebugEnabled()) {
				logger.debug("allowed logging methods: " + allowedMethods);
			}
			if (logger.isInfoEnabled() && allowedMethods != null && allowedMethods.contains(method) && DigitalStringUtil.isNotBlank(message)) {
				StringBuilder logEntry = new StringBuilder();

				logEntry.append(buildPartialMessage(logEntry.length() == 0,
						page, PREPEND_PAGE_PARTIAL, MAX_PAGE_LENGTH));
				logEntry.append(buildPartialMessage(logEntry.length() == 0,
						method, PREPEND_METHOD_PARTIAL, MAX_METHOD_LENGTH));
				logEntry.append(buildPartialMessage(logEntry.length() == 0,
						productId, PREPEND_PRODUCT_ID_PARTIAL,
						MAX_ORDER_ID_LENGTH));
				logEntry.append(buildPartialMessage(logEntry.length() == 0,
						orderId, PREPEND_ORDER_ID_PARTIAL,
						MAX_PRODUCT_ID_LENGTH));
				logEntry.append(buildPartialMessage(logEntry.length() == 0,
						message, PREPEND_MESSAGE_PARTIAL, MAX_MESSAGE_LENGTH));
				logEntry.append(buildPartialMessage(logEntry.length() == 0,
						timestamp, PREPEND_TIMESTAMP_PARTIAL,
						MAX_TIMESTAMP_LENGTH));

				if (logEntry.length() > 0 && message != null
						&& !message.isEmpty()) {
					logger.info(logEntry.toString());
				}
			}
		} catch (Exception ex) {
				logger.error("LogMessage Exception  " + ex.getMessage());
			this.getJSONError(ex.getMessage());
		}
	}

	/**
	 * Used to build partials for the final log message
	 * 
	 * @param first
	 *            Is this the first partial?
	 * @param partial
	 *            Partial is either page, method, productId, orderId or message.
	 * @param prependPartial
	 *            Text to prepend to the partial. Ex. For page you might prepend
	 *            "Page = " or "Page: ".
	 * @param length
	 *            The max length for the partial.
	 * @return
	 */
	private String buildPartialMessage(boolean first, String partial,
			String prependPartial, int length) {
		String out = new String();

		if (partial == null || partial.isEmpty()) {
			return out;
		}
		if (!first) {
			out += ", ";
		}
		out += prependPartial + DigitalStringUtil.left(partial, length);

		return out;
	}

	private void getJSONError(String errorMesage) {
		error = new HashMap<>();
		HashMap<String, String> genError = new HashMap<>();
		if (DigitalStringUtil.isNotBlank(errorMesage)) {
			genError.put("localizedMessage", errorMesage);
		} else {
			genError.put("localizedMessage",
					"Generic error while writing a client message to the log");
		}
		genError.put("errorCode", "CLIENT_LOGGING_GENERIC_ERROR");
		ArrayList<HashMap<String, String>> genErrArray = new ArrayList<>();
		genErrArray.add(genError);
		error.put("genericExceptions", genErrArray);
		error.put("formError", false);
	}
}
