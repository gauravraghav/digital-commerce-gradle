package com.digital.commerce.services.common;

import java.util.Arrays;
import java.util.Iterator;

import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.google.common.collect.Iterables;
import lombok.Getter;

@Getter
public enum MilitaryRegionCode {
	ARMED_FORCES_AMERICA("AA"), ARMED_FORCES_EUROPE("AE"), ARMED_FORCES_PACIFIC("AP");

	private final String	value;

	private MilitaryRegionCode( String value ) {
		this.value = value;
	}

	/** Looks up the address type for the given value.
	 * 
	 * @param value
	 * @return */
	public static MilitaryRegionCode valueFor( final String value ) {
		final String trimmedValue = DigitalStringUtil.trimToEmpty( value );
		final Iterator<MilitaryRegionCode> filtered = Iterables.filter( Arrays.asList( values() ), new DigitalPredicate<MilitaryRegionCode>() {

			@Override
			public boolean apply( MilitaryRegionCode input ) {
				return DigitalStringUtil.equalsIgnoreCase( DigitalStringUtil.trimToEmpty( input.getValue() ), trimmedValue );
			}

		} ).iterator();
		return filtered.hasNext() ? filtered.next() : null;
	}
}
