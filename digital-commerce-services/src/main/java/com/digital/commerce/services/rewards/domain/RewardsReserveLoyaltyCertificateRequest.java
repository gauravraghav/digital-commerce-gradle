/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RewardsReserveLoyaltyCertificateRequest {
	
	private String profileId;
	private String orderNumber;
	private List<String> certificates;
}
