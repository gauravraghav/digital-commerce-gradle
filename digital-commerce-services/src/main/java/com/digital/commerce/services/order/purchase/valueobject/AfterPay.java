package com.digital.commerce.services.order.purchase.valueobject;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
import lombok.Getter;
import lombok.Setter;

@Generated("com.robohorse.robopojogenerator")
@Getter
@Setter
public class AfterPay{

	@JsonProperty("token")
	private String token;
}