package com.digital.commerce.services.order.payment.afterpay.processor;

import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.nucleus.logging.ApplicationLoggingImpl;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.payment.afterpay.AfterPayCheckoutManager;
import com.digital.commerce.services.order.payment.afterpay.AfterPayPaymentInfo;
import com.digital.commerce.services.order.payment.afterpay.valueobject.AfterPayOrderResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class AfterPayPaymentProcessorImpl extends ApplicationLoggingImpl implements AfterPayPaymentProcessor {
    private static final String SERVICE_NAME = "AfterPayPaymentProcessor";
    private AfterPayCheckoutManager	afterPayCheckoutManager;
    private OrderManager orderManager;

    public AfterPayPaymentProcessorImpl() {
        super(AfterPayPaymentProcessorImpl.class.getName());
    }


    @Override
    public AfterPayPaymentStatus createOrder(AfterPayPaymentInfo paymentInfo, String confirmURL, String cancelURL) {
        final String METHOD_NAME = "createOrder";
        AfterPayPaymentStatusImpl paymentStatus = null;
        Order order = paymentInfo.getOrder();
        try {
            DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);

            AfterPayOrderResponse afterPayOrderResponse = getAfterPayCheckoutManager().order(order, confirmURL, cancelURL);
            paymentStatus = new AfterPayPaymentStatusImpl(order.getId(),order.getPriceInfo().getAmount(),afterPayOrderResponse.isSuccess(),null, new Date() , null, afterPayOrderResponse.getStatusCode(), null, null);
        } catch(Exception e) {
            paymentStatus = new AfterPayPaymentStatusImpl(order.getId(),order.getPriceInfo().getAmount(),false, null,new Date(),
                     null,null, null, null);
        } finally {
            DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
        }
        return paymentStatus;
    }

    @Override
    public AfterPayPaymentStatus capturePayment(AfterPayPaymentInfo pAfterPayPaymentInfo) {


        final String METHOD_NAME="capturePayment";

        AfterPayPaymentStatusImpl afterPayPaymentStatus=null;

        if( isLoggingDebug() ) logDebug( " AfterPay OrderId:" + pAfterPayPaymentInfo.getOrder()+ "  with the TOKEN  ::" +pAfterPayPaymentInfo.getToken() );
        DigitalOrderImpl order=(DigitalOrderImpl) pAfterPayPaymentInfo.getOrder();
        try {
            DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
            AfterPayOrderResponse afterPayOrderResponse = getAfterPayCheckoutManager().capturePayment(order, pAfterPayPaymentInfo.getToken());
            afterPayPaymentStatus = new AfterPayPaymentStatusImpl(order.getId(),order.getPriceInfo().getTotal(),true,null, new Date(), afterPayOrderResponse.getId(),afterPayOrderResponse.getStatusCode(), afterPayOrderResponse.getCreatedDate(), null);
        }catch(Exception e) {
            afterPayPaymentStatus = new AfterPayPaymentStatusImpl(order.getId(),order.getPriceInfo().getTotal(),false,e.getLocalizedMessage(), new Date(),
                         null,null, null, null);
            } finally {
                DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
            }
        return afterPayPaymentStatus;
    }
}
