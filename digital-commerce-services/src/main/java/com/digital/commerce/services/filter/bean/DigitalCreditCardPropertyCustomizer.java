package com.digital.commerce.services.filter.bean;

import java.util.Map;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;

/**
 * A Property customizer used to return a last four digits of users credit card
 * number.
 *
 * @author Shiva
 */
public class DigitalCreditCardPropertyCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {

	public DigitalCreditCardPropertyCustomizer() {
		super(DigitalCreditCardPropertyCustomizer.class.getName());
	}

	/**
	 * @param creditCardNumber
	 *            extract last four digits of the credit card number.
	 */
	public String extractLastFour(String creditCardNumber) {
		return (creditCardNumber != null) ? creditCardNumber.substring((creditCardNumber.length() - 4)) : null;
	}

	// ---------------------------------------------------------------------------
	// METHODS
	// ---------------------------------------------------------------------------

	/**
	 *
	 * @param pTargetObject
	 *            The object which the specified property is associated with.
	 * @param pPropertyName
	 *            The name of the property to return.
	 * @param pAttributes
	 *            The key/value pair attributes defined in the
	 *            beanFilteringConfiguration.xml file for this property.
	 *
	 * @return Last four degits of credit card extracted from the token.
	 *
	 * @throws BeanFilterException
	 */
	@Override
	public Object getPropertyValue(Object pTargetObject, String pPropertyName, Map<String, String> pAttributes)
			throws BeanFilterException {

		// Get credit card number that needs to be formatted.
		Object propValue = null;

		try {
			propValue = DynamicBeans.getPropertyValue(pTargetObject, pPropertyName);
		} catch (PropertyNotFoundException e) {
			throw new BeanFilterException(e);
		}

		// Return null if we dont get a token value to format.
		if (propValue == null) {
			vlogDebug("Property {0} was not a valid promotion type: {1}", pPropertyName, propValue);
			return null;
		}

		// Get the locale and pattern to use for formatting.
		return this.extractLastFour((String) propValue);
	}
}
