package com.digital.commerce.services.order.payment.afterpay;

import atg.commerce.order.Order;

import java.io.Serializable;

public interface AfterPayPaymentInfo extends Serializable {

    public abstract Order getOrder();

    public abstract String getToken();

}
