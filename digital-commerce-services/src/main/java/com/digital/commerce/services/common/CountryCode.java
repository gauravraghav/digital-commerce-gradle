package com.digital.commerce.services.common;

import java.util.Arrays;
import java.util.Iterator;

import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.google.common.collect.Iterables;
import lombok.Getter;

/** Represents possible country codes that are important.
 * 
 * @author <a href="mailto:knaas@dswinc.com">knaas</a> */
@Getter
public enum CountryCode {
	CANADA("CAN"), USA("USA"), JAPAN("JPN"), US("US");

	private final String	value;

	private CountryCode( String value ) {
		this.value = value;
	}

	/** Looks up the address type for the given value.
	 * 
	 * @param value
	 * @return */
	public static CountryCode valueFor( final String value ) {
		final String trimmedValue = DigitalStringUtil.trimToEmpty( value );
		final Iterator<CountryCode> filtered = Iterables.filter( Arrays.asList( values() ), new DigitalPredicate<CountryCode>() {

			@Override
			public boolean apply( CountryCode input ) {
				return DigitalStringUtil.equalsIgnoreCase( DigitalStringUtil.trimToEmpty( input.getValue() ), trimmedValue );
			}

		} ).iterator();
		return filtered.hasNext() ? filtered.next() : DigitalStringUtil.isBlank( trimmedValue ) ? CountryCode.USA : null;
	}
}
