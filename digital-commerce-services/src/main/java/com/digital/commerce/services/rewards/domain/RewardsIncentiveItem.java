package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RewardsIncentiveItem {

  int denomination;
  int pointCost;
  boolean toBeRedeemed;
}
