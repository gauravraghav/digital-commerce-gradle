package com.digital.commerce.services.common.web;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.RequestParametersConstant;

import atg.dtm.UserTransactionDemarcation;
import atg.multisite.SiteManager;
import atg.repository.RepositoryItem;
import atg.rest.servlet.RestPipelineServlet;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MultiXRequestCheckerServlet extends RestPipelineServlet {
	private static final String PERF_NAME = "service";
	private static final String PERFORM_MONITOR_NAME = "MultiXRequestCheckerServlet";
	private SiteManager siteManager;

	/**
	 * To enable/disable the this servlet
	 */
	private boolean enabled;

	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, PERF_NAME);
			if (enabled) {
				if (DigitalStringUtil.isNotBlank(pRequest.getParameter(RequestParametersConstant.PUSH_SITE.getValue()))) {
					validateSiteId(pRequest, pResponse);
				}
			}
			/* Move on in the pipeline all the times */
			passRequest(pRequest, pResponse);

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, PERF_NAME);
		}

	}

	private void validateSiteId(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException {
		final String CLASS_NAME = this.getClass().getName();
		final String methodName = "validateSiteId";
		UserTransactionDemarcation td = null;
		try {
			td = TransactionUtils.startNewTransaction(CLASS_NAME, methodName);
			RepositoryItem[] activeSites = siteManager.getActiveSites();
			if (activeSites != null && activeSites.length > 0) {
				ArrayList<String> sites = new ArrayList<>(activeSites.length);
				for (RepositoryItem site : activeSites) {
					sites.add((String) site.getPropertyValue("id"));
				}
				if (isLoggingDebug()) {
					logDebug("Supported sites are: " + sites);
				}
				String requestSiteId = pRequest.getParameter(RequestParametersConstant.PUSH_SITE.getValue());
				if (!sites.contains(requestSiteId)) {
					pResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, "The siteId specified in pushSite '"
							+ requestSiteId
							+ "' is not an active and/or not a supported site. Please contact Site Administrator to get the correct siteId and try again");

					return;

				}
			}

		} catch (Exception e) {
			TransactionUtils.rollBackTransaction(td, this.getClass().getName(), methodName);
			logError("Exception while retrieving active sites data", e);
		} finally {
			TransactionUtils.endTransaction(td, CLASS_NAME, methodName);
		}

	}
}
