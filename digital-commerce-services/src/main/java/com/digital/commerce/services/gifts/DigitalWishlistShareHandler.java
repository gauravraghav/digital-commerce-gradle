package com.digital.commerce.services.gifts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.integration.common.communication.CommunicationService;
import com.digital.commerce.integration.common.communication.domain.Recipient;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent.ContentType;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
//import com.digital.commerce.services.validator.DSWBasicAddressValidator;
import com.digital.commerce.services.validator.DigitalFormValidationService;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;

import atg.commerce.CommerceException;
import atg.commerce.gifts.GiftlistFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.dtm.UserTransactionDemarcation;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalWishlistShareHandler extends GiftlistFormHandler {
	protected static final String CLASSNAME = DigitalWishlistShareHandler.class.getName();
	
	private RepeatingRequestMonitor repeatingRequestMonitor;
	
	private CommunicationService communicationService;
	
	private static final DigitalLogger logger = DigitalLogger
			.getLogger(DigitalWishlistShareHandler.class);
	
	private static final String EMAIL_LIMITS_EXCEED = "wishList.share.emails_exceed_limits";
	private static final String MSG_LIMITS_EXCEED = "wishList.share.msg_exceedlimit";
	private static final String EMAIL_FORMAT_ERROR = "wishList.share.email_format_error";
	private static final String EMAIL_NOT_PROVIDED = "wishList.share.email_notprovided";
	private static final String DIRTY_WORDS_EXISTS = "wishList.share.dirty_words_exists";
	private static final String EMAIL_PROCESS_ERROR = "wishList.share.email_process_error";
	private static final String GIFTLIST_NOT_EXISTS = "wishList.share.not_exists";
	
    private String emailTemplate = "wishlistShare";
	
	private DigitalFormValidationService validator;
	
	private String[] emails;
	
	private boolean includeOwner;
	
	private String sharedMessage;
	
	private String emailShareLink;

    private MessageLocator messageLocator;

	public boolean handleEmailShare(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		final String METHOD_NAME = "DigitalWishlistShareHandler.emailShare";
		UserTransactionDemarcation td = null;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(METHOD_NAME))) {

			try {

				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
						this.getName(), METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				
				checkGiftlistAccess();
				
				if (!checkFormRedirect(null, getAddItemToGiftlistErrorURL(),
						pRequest, pResponse)) {
					return false;
				}

				if (validateUserInput(pRequest, pResponse)) {
					emailWishlist();
				}
				
			} catch (CommerceException oce) {
				logger.error(oce);
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				processException(oce, "errorsharingwishilst", pRequest,
						pResponse);
			} catch (Exception e) {
				logger.error(e);
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				addFormException(new DropletException(this.getMessageLocator()
						.getMessageString(EMAIL_PROCESS_ERROR)));
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
						this.getName(), METHOD_NAME);
			}
		}
		return checkFormRedirect(getAddItemToGiftlistSuccessURL(),
				getAddItemToGiftlistErrorURL(), pRequest, pResponse);
	}
	
	private boolean validateUserInput(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		if (this.getEmails() == null || this.getEmails().length < 1) {
			addFormException(new DropletException(this.getMessageLocator()
					.getMessageString(EMAIL_NOT_PROVIDED)));
		} else {
			if (this.getEmails().length > MultiSiteUtil.getEmailSharedLimits()) {
				addFormException( new DropletException( this.getMessageLocator().getMessageString(EMAIL_LIMITS_EXCEED, new Object[] { MultiSiteUtil.getEmailSharedLimits() }) ) );
			} else {
				boolean isEmailValid = true;			
				List<String> inValidEmailList = new ArrayList<>();
				
				for (String email : this.getEmails()) {
					if (!this.validator.validateEmailAddress(email)) {
						isEmailValid = false;
						inValidEmailList.add(email);
					}
				}
				
				StringBuilder params = new StringBuilder();
				int size = inValidEmailList.size();
				int i = 1;
				for(String email : inValidEmailList){
					params.append(email);
					if(i < size){
						params.append(";");
					}
					else{
						params.append(".");
					}
					i++;
				}
				
				if(!isEmailValid){
					addFormException(new DropletException(this.getMessageLocator().getMessageString(EMAIL_FORMAT_ERROR, params.toString())));
				}
			}
			if(this.getSharedMessage() != null && this.getSharedMessage().length() > MultiSiteUtil.getMessageMaxLength()){
				addFormException( new DropletException( this.getMessageLocator().getMessageString(MSG_LIMITS_EXCEED, new Object[] { MultiSiteUtil.getMessageMaxLength() }) ) );
				
			} else {
				String explicitPattern = "\\b(" + Joiner.on( '|' ).join(MultiSiteUtil.getExcludeWords()) + ")\\b";
				Pattern explicitPatternMatcher = Pattern.compile( explicitPattern, Pattern.CASE_INSENSITIVE );
				Matcher m = explicitPatternMatcher.matcher(this.getSharedMessage());
				int matches = 0;
				String msg = this.getSharedMessage();
				while( m.find() ) {
					// reject the message
					if( matches == 0 ) {
						addFormException(new DropletException(this
								.getMessageLocator().getMessageString(
										DIRTY_WORDS_EXISTS)));
					}

					for( int i = 1; i < m.groupCount(); i++ ) {
						String s = m.group( i );
						if( s != null ) {
							String asterisks = Strings.repeat( "*", s.length() );
							msg = msg.replaceAll( "\\b" + s + "\\b", asterisks);
						}
					}
					matches++;
				}
				
				this.setSharedMessage(msg);

			}
		}		
		
		return !this.getFormError() || this.getFormExceptions() == null || this.getFormExceptions().size() == 0;

	}
	
	private void emailWishlist() throws CommerceException, DigitalIntegrationException  {
		List<Recipient> recipients = new ArrayList<>(this.emails.length + 1);
		
		for(String email : this.emails){
			Recipient recipient = new Recipient();
            recipient.setAddress(email);
			recipient.setRecipientType(Recipient.RecipientType.TO);
			recipients.add(recipient);			
		}
		
		if(this.includeOwner){
			Recipient recipient = new Recipient();
            recipient.setAddress((String)this.getProfile().getPropertyValue("email"));
			recipient.setRecipientType(Recipient.RecipientType.TO);
			recipients.add(recipient);
		}
		
		//parameters for the email content
		RepositoryItem listRep = this.getGiftlistManager().getGiftlist(this.getGiftlistId());
		
		if(listRep != null && ((List<RepositoryItem>)listRep.getPropertyValue("giftlistItems")).size() > 0){			
			Map dataTemplate = new HashMap();
			dataTemplate.put("name", this.getProfile().getPropertyValue("firstName"));
			dataTemplate.put("ownerEmail", (String)this.getProfile().getPropertyValue("email"));
			dataTemplate.put("giftListId", this.getGiftlistId());
			dataTemplate.put("message", this.getSharedMessage());
			dataTemplate.put("sharedWLUrl", MultiSiteUtil.getSiteURL() + MultiSiteUtil.getShareWLURI() + this.getGiftlistId());
			dataTemplate.put("siteUrl", MultiSiteUtil.getSiteURL());
			dataTemplate.put("locale", MultiSiteUtil.getDefaultLocale() );
			dataTemplate.put("findStoreUrl", MultiSiteUtil.getSiteURL() + MultiSiteUtil.getStoreLocatorURI());
			dataTemplate.put("addToBagUrl", MultiSiteUtil.getSiteURL());
			dataTemplate.put("trackingDate", DigitalDateUtil.getOperationEmailsTrackingDate());
			dataTemplate.put("details", ((List<RepositoryItem>)listRep.getPropertyValue("giftlistItems")));

			communicationService.send(recipients, this.emailTemplate,
					dataTemplate, ContentType.HTML, getSiteId());
		} else {
			addFormException(new DropletException(this
					.getMessageLocator().getMessageString(
							GIFTLIST_NOT_EXISTS)));
		}
	
	}

}
