package com.digital.commerce.services.order.processor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.Order;
import atg.nucleus.GenericService;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationSystemException;
import com.digital.commerce.integration.common.jms.DigitalJMSClient;
import com.digital.commerce.services.order.DigitalSubmitOrder;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class DigitalSendCreateOrderHistoryMessage extends GenericService implements PipelineProcessor{
	private DigitalLogger logger = DigitalLogger.getLogger(DigitalSendCreateOrderHistoryMessage.class);
	private DigitalJMSClient jmsClient;

	public int runProcess(Object paramObject, PipelineResult paramPipelineResult)
		    throws Exception{
		Serializable soMessage = createMessageToSend(paramObject, paramPipelineResult);
		if (soMessage == null) {
            return 1;
        }
		submitOrder(soMessage);
		return 1;
	}
	
	/**
	 * 
	 * @param pParam
	 * @param pResult
	 * @throws Exception
	 */
	public Serializable createMessageToSend(Object pParam, PipelineResult pResult) {
		String orderId = null;
		try{
			Map<?, ?> map = (HashMap<?, ?>) pParam;
			Order order = (Order) map.get("Order");
			if(order != null){
				orderId = order.getId();
			}
			DigitalSubmitOrder soMessage = new DigitalSubmitOrder();
			soMessage.setOrder(order);
			soMessage.setSiteId(getSiteId(pParam));
			Locale locale = (Locale) map.get("Locale");
			soMessage.setLocale(locale.toString());
			if(order != null){
			soMessage.setOrderTotal(order.getPriceInfo().getTotal());
			}
			return soMessage;
		}catch(Exception e){
			logger.error(String.format("%s - %s: OrderId - %s",
					"OrderSubmitSource", "Failed to send message to OrderHistory Queue", orderId));

			return null;
		}
	}

	public void submitOrder(Serializable dswSubmitOrder) throws DigitalIntegrationSystemException {
		final String METHOD_NAME = "submitOrder";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			if (logger.isDebugEnabled()) {
				logger.debug(String.format("%s - %s: REQUEST - %s",
						"OrderSubmitSource", "sendOneMessage", dswSubmitOrder));
				logger.debug("Preparing to send OrderHistory Message to OrderHistory Queue");
			}
			jmsClient.sendObjectMessage("queue/ORDER_HISTORY_UPDATE_Q",dswSubmitOrder, null, false);
			if (logger.isDebugEnabled()) {
				logger.debug("Successfully OrderHistory Message to OrderHistory Queue");
			}
		} catch (Exception e) {
			logger.error("Exception Details :: ",e);
			logger.error(String.format("%s - %s: Request - %s",
					"OrderSubmitSource", "Failed to send message to OrderHistory Queue", dswSubmitOrder));
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}
	
    protected String getSiteId(Object pParams) {
        return (String)((Map)pParams).get("SiteId");
    }

	@Override
	public int[] getRetCodes() {
    return new int[]{1, 2};
	}
}
