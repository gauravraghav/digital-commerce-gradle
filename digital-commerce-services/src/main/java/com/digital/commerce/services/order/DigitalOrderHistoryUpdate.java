package com.digital.commerce.services.order;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import com.digital.commerce.common.exception.DigitalRuntimeException;
import com.digital.commerce.common.logger.DigitalLogger;

import atg.dms.patchbay.MessageSink;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalOrderHistoryUpdate implements MessageSink {

	private DigitalOrderTools tools;

	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalOrderHistoryUpdate.class);

	@Override
	/**
	 * 
	 * @param port
	 * @param pMessage
	 * @throws JMSException
	 */
	public void receiveMessage(String port, Message pMessage) throws JMSException {
		logger.debug("Consuming submitted order message");
	
			try {
				DigitalSubmitOrder sorder = (DigitalSubmitOrder) ((ObjectMessage) pMessage).getObject();
				if (sorder.getOrder() != null) {
					DigitalOrderImpl order = (DigitalOrderImpl) sorder.getOrder();
					String locale = sorder.getLocale();
					double orderTotal = sorder.getOrderTotal();
					getTools().updateOrderHistory(order, orderTotal, locale);
				}
			}catch (Throwable e) {//NOSONAR
				logger.error("Error processing order history update message", e);
				throw new DigitalRuntimeException("Error processing order history update message", e);
			}

	}

}
