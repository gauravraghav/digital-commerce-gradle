package com.digital.commerce.services.common.web;

import atg.rest.servlet.RestPipelineServlet;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.HTTPUtils;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import javax.servlet.ServletException;

/**
 * @author HB391569 A new rest servlet that adds an single digit segment id based on the timestamp.
 * The segment id is used to offer a percentage of promotion to the users as mystsery offer
 */
@Getter
@Setter
public class DigitalMysteryOfferServlet extends RestPipelineServlet {

  private static final String PERF_NAME = "service";
  private static final String PERFORM_MONITOR_NAME = "DigitalMysteryOfferServlet";
  /**
   * Some junk name to avoid guessing and messing with it
   */
  private static final String MYSTERY_OFFER_COOKIE_NAME = "MOUS";

  private int maxCookieAge;
  private String cookieDomain;
  private boolean secure;

  /**
   * To enable/disable the this servlet
   */
  private boolean enabled;

  @Override
  public void service(DynamoHttpServletRequest pRequest,
      DynamoHttpServletResponse pResponse) throws IOException,
      ServletException {
    if (enabled) {
      DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
          PERFORM_MONITOR_NAME, PERF_NAME);

      if (isLoggingDebug()) {
        logDebug("The DigitalMysteryOfferServlet enabled: " + enabled);
      }
      try {
        Profile profile = (Profile) ServletUtil.getCurrentUserProfile();
        String profileId = (String) profile.getPropertyValue("id");
        String profileOfferSeg = (String) profile
            .getPropertyValue("mysteryOfferUserSeg");
        if (isLoggingDebug()) {
          logDebug("mysteryOfferUserSeg for current profile: "
              + profileId + " " + profileOfferSeg);
        }
        if (DigitalStringUtil.isBlank(profileOfferSeg)) {
          if (isLoggingDebug()) {
            logDebug("mysteryOfferUserSeg for current profile seems blank, attempting to set it ");
          }
          // get from cookies
          String cookieOfferSeg = HTTPUtils.getCookieValue(
              MYSTERY_OFFER_COOKIE_NAME, pRequest);
          if (isLoggingDebug()) {
            logDebug("mysteryOfferUserSeg from cookie: "
                + cookieOfferSeg);
          }
          if (DigitalStringUtil.isNotBlank(cookieOfferSeg)) {
            if (isLoggingDebug()) {
              logDebug("Setting  mysteryOfferUserSeg to the profile from cookie");
            }
            profile.setPropertyValue("mysteryOfferUserSeg",
                cookieOfferSeg);

          } else {
            if (isLoggingDebug()) {
              logDebug("Setting  mysteryOfferUserSeg cookie to http response");
            }
            final long time = System.nanoTime();
            String lastDigit = Long.toString(time % 10);
            profile.setPropertyValue("mysteryOfferUserSeg",
                lastDigit);
            HTTPUtils.createCookie(
                MYSTERY_OFFER_COOKIE_NAME, lastDigit, "/",
                maxCookieAge, isSecure(), cookieDomain, true, pResponse);
          }
        }

      } catch (Exception ex) {
        // World does not end if this fails, just log it for support
        // needs and move on

        if (isLoggingInfo()) {
          logInfo("Errror while creating mystery offer segment", ex);
        }
      } finally {
        DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
            PERFORM_MONITOR_NAME, PERF_NAME);
      }
    }
    /* Move on in the pipeline all the times */
    passRequest(pRequest, pResponse);
  }
}
