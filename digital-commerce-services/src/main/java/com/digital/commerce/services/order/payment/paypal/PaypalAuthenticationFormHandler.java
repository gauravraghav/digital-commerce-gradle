package com.digital.commerce.services.order.payment.paypal;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.SessionAttributesConstant;
import com.digital.commerce.constants.PaypalConstants;
import com.digital.commerce.services.common.DigitalGenericFormHandler;
import com.digital.commerce.services.order.payment.paypal.valueobject.PaypalAuthenticateResponse;
import com.digital.commerce.services.profile.DigitalProfileTools;

import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.dtm.UserTransactionDemarcation;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaypalAuthenticationFormHandler extends DigitalGenericFormHandler{
	protected static final String CLASSNAME = PaypalAuthenticationFormHandler.class.getName();

	private static final String	PAYPAL_GENERAL_ERROR			= "paypalGeneralError";
	private static final String	PAYPAL_EXPRESS_CHECKOUT_ERROR	= "paypalExpressCheckoutError";
	private static final String	PAYPAL_MISSINGLOOKUP_ERROR		= "paypalMissingLookupOrRedirectError";
	private static final String	PAYPAL_UNAVAILABLE_ERROR		= "paypalUnavailableError";

	private static final String PAYPAL_STATUS_SUCCESS 			= "Success";
	private static final String PAYPAL_STATUS_CANCELED 			= "Canceled";

	protected static final String MSG_ERROR_PAYPAL_PAYMENT = "errorWithPaypalPayment";
	protected static final String MSG_NO_INTL_SHIPPING = "errorNoInternationalShipping";
	protected static final String MSG_INVALID_SHIPPING_ADDR = "paypalInvalidShippingAddress";
	protected static final String MSG_PAYPAL_USER_CANCEL = "paypalUserCancel";

	protected PaypalCheckoutManager paypalCheckoutManager;
	protected OrderManager orderManager;
	protected Order	order;
	protected DigitalProfileTools profileTools;
	protected Profile profile;
	protected List<String> orderRespSupportedApps;
	protected DigitalBaseConstants dswConstants;
	protected String responseStatus = null;
	boolean paypalExpressCheckout = false;
	boolean guestCheckout = false;

	/**
	 * @param pRequest
	 * @param pResponse
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handlePaypalAuthentication( DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse )
			throws ServletException, IOException {
		boolean ret = false;
		final String METHOD_NAME = "PaypalAuthenticationFormHandler.handlePaypalAuthentication";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry( METHOD_NAME )) {

			UserTransactionDemarcation td = null;

			if(isLoggingDebug()) logDebug("Execution started in PaypalAuthenticationDroplet.service()");

			String cardinalOrderId = DigitalCommonUtil.getSessionAttributeValue(pRequest, SessionAttributesConstant.PAYPAL_CARDINAL_ORDER_ID);
			String ppPayload = DigitalCommonUtil.getSessionAttributeValue(pRequest, SessionAttributesConstant.PAYPAL_PAYLOAD);
			// Authentication call only if the referrer is PayPal
			if (cardinalOrderId != null && ppPayload != null) {

				try{
					DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
					TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
					td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

					PaypalAuthenticateResponse res = getPaypalCheckoutManager().doPaypalAuthenticate(getOrder(),getProfile());

					if(res.isSuccess()){
						//Authentication Success
						if (res != null && DigitalStringUtil.isNotBlank(res.getPaypalEmail())) {
							DigitalCommonUtil.setSessionAttributeValue(pRequest, SessionAttributesConstant.PAYPAL_EMAIL, res.getPaypalEmail());
							DigitalCommonUtil.setSessionAttributeValue(pRequest, SessionAttributesConstant.SELECTED_PAYMENT, PaypalConstants.SELECTED_PAYMENT_PAYPAL);
							ret = true;
							setResponseStatus(PAYPAL_STATUS_SUCCESS);
						}
					}else{
						boolean existingUser = getProfileTools().isKnown(profile);

						if (PaypalConstants.AUTHENTICATE_CANCEL_STATUS.equals(res.getStatusCode())) {
							//Express or regular paypal checkout cancel flow and don't populate the formError
							if (isLoggingDebug()) {
								logDebug("paypalXCheckout="+ this.isPaypalExpressCheckout());
							}
							ret=true;
							setResponseStatus(PAYPAL_STATUS_CANCELED);
						}else if(PaypalConstants.AUTHENTICATE_UNAVAILABLE_STATUS.equalsIgnoreCase(res.getStatusCode())){
							String msg = getMessageLocator().getMessageString(PAYPAL_UNAVAILABLE_ERROR);
							this.logError(msg);
							addFormException( new DropletException( msg,PAYPAL_UNAVAILABLE_ERROR ) );
							ret=false;
						}else if(PaypalConstants.AUTHENTICATE_ERROR_STATUS.equals(res.getStatusCode())){
							String msg ="";
							if(this.isPaypalExpressCheckout() && !existingUser){
								//Guest express checkout error
								msg = getMessageLocator().getMessageString(PAYPAL_EXPRESS_CHECKOUT_ERROR);
								addFormException( new DropletException( msg, PAYPAL_EXPRESS_CHECKOUT_ERROR ) );
							}else{
								//Member checkout & guest with paypal payment error
								msg = getMessageLocator().getMessageString(PAYPAL_GENERAL_ERROR);
								addFormException( new DropletException( msg, PAYPAL_GENERAL_ERROR ) );
							}
							ret=false;
						}else if(PaypalConstants.AUTHENTICATE_INT_ADDR_ERROR_STATUS.equals(res.getStatusCode())){
							String msg = getMessageLocator().getMessageString(MSG_NO_INTL_SHIPPING);
							addFormException( new DropletException( msg, MSG_NO_INTL_SHIPPING ) );
						}else if(PaypalConstants.AUTHENTICATE_ADDR_ERROR_STATUS.equals(res.getStatusCode())){
							String msg = getMessageLocator().getMessageString(MSG_INVALID_SHIPPING_ADDR);
							addFormException( new DropletException( msg, MSG_INVALID_SHIPPING_ADDR  ) );
						}
					}
				}  catch(Exception ex) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					logError(ex);
					pRequest.serviceLocalParameter(PAYPAL_GENERAL_ERROR, pRequest, pResponse);
					processException(ex, pRequest, pResponse);
				}finally{
					TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
					TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
					if(rrm != null){
						rrm.removeRequestEntry(METHOD_NAME);
					}
					DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				}
			}else{//needed paypal lookup and redirected to paypal site before calling paypal authenticate service
				String msg = getMessageLocator().getMessageString(PAYPAL_MISSINGLOOKUP_ERROR);
				addFormException( new DropletException( msg, PAYPAL_MISSINGLOOKUP_ERROR ) );
			}
		}
		return ret;
	}

	public boolean getOrderRespRequiredFlag(){
		if(orderRespSupportedApps != null && orderRespSupportedApps.size() > 0){

			if(this.getDswConstants().isAndriodAppRequest()){
				return orderRespSupportedApps.contains(DigitalBaseConstants.ANDROID_APP_TYPE);
			}else if(this.getDswConstants().isIosAppRequest()){
				return orderRespSupportedApps.contains(DigitalBaseConstants.IOS_APP_TYPE);
			}else{
				return orderRespSupportedApps.contains(DigitalBaseConstants.FULLSITE_APP_TYPE);
			}
		}

		return true;
	}
}