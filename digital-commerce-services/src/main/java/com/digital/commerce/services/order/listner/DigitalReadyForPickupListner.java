package com.digital.commerce.services.order.listner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.order.processor.domain.payload.ReadyForPickupPayload;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderlineAttributesDTO;

public class DigitalReadyForPickupListner extends DigitalOrderStatusListener {
	protected static final String CLASSNAME = DigitalReadyForPickupListner.class.getName();

	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected HashMap<String, Object> handleEmailNotification(OrderStatusPayload payLoad, DigitalOrderImpl order,
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {
		HashMap<String, Object> extraAttributes = new HashMap<>();

		ReadyForPickupPayload readyForPickupPayload = (ReadyForPickupPayload) payLoad;

		String customerFirstName = readyForPickupPayload.getPersonInfoBillTo().getFirstName();

		if (DigitalStringUtil.isEmpty(customerFirstName)) {
			customerFirstName = readyForPickupPayload.getPersonInfoShipTo().getFirstName();
			if (DigitalStringUtil.isEmpty(customerFirstName)) {
				customerFirstName = readyForPickupPayload.getPersonInfoMarkFor().getFirstName();
			}
		}
		
		extraAttributes.put("pickUpByDate", readyForPickupPayload.getPickUpByDate());
		
		extraAttributes.put("customerFirstName", customerFirstName);
				
		HashMap<String, Object> shippingAddress = new HashMap<>();
		// get Shipping Address Details
		shippingAddress.put("firstName", readyForPickupPayload.getPersonInfoShipTo().getFirstName());
		shippingAddress.put("company", readyForPickupPayload.getPersonInfoShipTo().getCompany());
		shippingAddress.put("addressLine1", readyForPickupPayload.getPersonInfoShipTo().getAddressLine1());
		shippingAddress.put("addressLine2", readyForPickupPayload.getPersonInfoShipTo().getAddressLine2());
		shippingAddress.put("city", readyForPickupPayload.getPersonInfoShipTo().getCity());
		shippingAddress.put("state", readyForPickupPayload.getPersonInfoShipTo().getState());
		shippingAddress.put("zipCode", readyForPickupPayload.getPersonInfoShipTo().getZipCode());
		shippingAddress.put("country", readyForPickupPayload.getPersonInfoShipTo().getCountry());
		shippingAddress.put("dayPhone", readyForPickupPayload.getPersonInfoShipTo().getDayPhone());
		extraAttributes.put("shippingAddress", shippingAddress);

		if (DigitalStringUtil.isNotEmpty(readyForPickupPayload.getPersonInfoMarkFor().getEmailId())) {
			extraAttributes.put("altPickupEmail", readyForPickupPayload.getPersonInfoMarkFor().getEmailId());
		}

		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOPIS, itemsByFullfilmentType, extraAttributes);
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOSTS, itemsByFullfilmentType, extraAttributes);

		// get mall plaza name
		HashMap<String, OrderlineAttributesDTO> lines = (HashMap<String, OrderlineAttributesDTO>) extraAttributes
				.get("orderLines");

		if (lines != null) {
			Map.Entry<String, OrderlineAttributesDTO> entry = lines.entrySet().iterator().next();
			extraAttributes.put("mallPlazaName", entry.getValue().getMallPlazaName());
		}

		/*List<ShipmentLine> shipmentLines = readyForPickupPayload.getShipmentLines();
		if (shipmentLines.size() > 0) {
			extraAttributes.put("shipmentLines", shipmentLines);
		}*/
		return extraAttributes;
	}

	@Override
	protected String getOrderNo(OrderStatusPayload payLoad) {
		if (payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null) {
			return payLoad.getOmniOrderUpdate().getOrder().getOrderNo();
		}
		return null;
	}

	@Override
	protected String getEmailTo(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		// return
		// ((ReadyForPickupPayload)payLoad).getPersonInfoShipTo().getEmailId();
		return ((ReadyForPickupPayload) payLoad).getOmniOrderUpdate().getOrder().getCustomerEMailID();
	}

	@Override
	protected List<OrderLine> getOrderLines(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((ReadyForPickupPayload) payLoad).getOrderLines();
	}

}
