package com.digital.commerce.services.promotion;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalRuntimeException;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.RewardsCertificateConstants;
import com.digital.commerce.constants.RewardsCertificateConstants.ProfileRewardCertificateStatus;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.pricing.DigitalCouponMessage;
import com.digital.commerce.services.profile.DigitalCommercePropertyManager;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.profile.rewards.LoyaltyCertificate;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.claimable.ClaimableException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.commerce.promotion.CouponFormHandler;
import atg.commerce.promotion.PromotionException;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.dtm.UserTransactionDemarcation;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.collections.filter.ValidatorFilter;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalCouponFormHandler extends CouponFormHandler {
	private static final String CLASSNAME = DigitalCouponFormHandler.class.getName();

	private static final String			MSG_LOGIN_FOR_CERT				= "msgLoginForCert";
	private static final String			ERR_REMOVE_PROMO				= "errorRemovingPromo";
	private static final String			ERR_REMOVE_CERT					= "errorRemovingCert";
	private static final String			ERR_ADD_CERT					= "errorAddCertificate";
	private static final String			ERR_ADD_CERT_TO_ORDER			= "errAddCertToOrder";
	private static final String			MSG_COUPON_CODE_REQUIRED		= "couponCodeRequired";
	private static final String			MSG_UNKNOWN_COUPON_EXCEPTION	= "unknownCouponException";
	private static final String			MSG_MISSING_COUPON_EXCEPTION	= "missingCouponException";
	private static final String			MSG_APP_ONLY_COUPON_EXCEPTION	= "appOnlyCouponException";
	private static final String			MSG_DUPLICATE_COUPON        	= "duplicatePromotionUserResource";
	private static final String			MSG_DUPLICATE_CERT				= "duplicateCertificateNumber";
	private static final String			MSG_GIFT_CARD_ONLY_PROMO		= "promoCodeGiftCArdOnly";
	private static final String			INVALID_CERTIFICATE_VALUE		= "invalidCertificateValue";
	private static final String			MSG_COUPON_CODE_TARGETED		= "couponTargetedCode";
	private static final String			MSG_COUPON_CODE_PRIVATE			= "couponPrivateCode";
	private static final String			MSG_COUPON_CODE_GOLD_PRIVATE			="couponGoldMembersOnly";
	private static final String			MSG_COUPON_CODE_GOLD_ELITE_PRIVATE ="couponGoldEliteMembersOnly";
	private static final String			MSG_COUPON_CODE_ELITE_PRIVATE	="couponEliteMembersOnly=Sorry";
	
	private String							rewardCertClaimCode;
	private boolean 						rewardCertFlag=false;					
	private String							rewardCertificateId;
	private String							removePromotion;
	private String 							removeCoupon;

	private PricingModelHolder				pricingModelHolder;
	private PricingTools					pricingTools;
	private MessageLocator					messageLocator;

	private MutableRepository				userRepository;
	private RepeatingRequestMonitor			repeatingRequestMonitor;

	private Collection<RepositoryItem>		profileActivePromotions;
	private Set<RepositoryItem>				profileGrantedCoupons;
	private String 							orderId;//required during certificate and coupon's orderLookup for completed order
	
	private OrderManager					orderManager;
	private DigitalRewardsManager rewardCertificateManager;
	private DigitalServiceConstants				dswConstants;
	private DigitalProfileTools					profileTools;
	private Profile 						profile;
	private Order 							order;	

	private Map<String, List<?>> rewardPerks = new HashMap<>();
	
	private ValidatorFilter filter;
	
	private boolean restrictTargetedPromotions = false;
	
	private DigitalCouponMessage couponMessage;

	

	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleClaimCoupon(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		final String METHOD_NAME = "handleClaimRewardCertificate";
		final String HANDLE_METHOD = "DigitalCouponFormHandler." + METHOD_NAME;
		boolean ret = true;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();

		if (rrm == null || rrm.isUniqueRequestEntry(HANDLE_METHOD)) {

			UserTransactionDemarcation td = null;
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME,METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME,METHOD_NAME);
				String claimCode = this.getCouponClaimCode();
				getCouponMessage().clearBean();
				if (!this.validate(claimCode)) {
					return false;
				}

				if (this.isCoupon(claimCode)) {
					Collection<String> couponList = new ArrayList<>();
					couponList.add(this.getCouponClaimCode());
					this.addCoupons(pRequest, pResponse, couponList);
				} else {
					addRewardCertificates(pRequest, pResponse);
				}
			}catch (ClaimableException ce) {//handles any promotion related exception
				this.logError("Exception occured while handling for claimCode :: " + getCouponClaimCode() + " :: ", ce);
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				ret = false;
				Throwable sourceException = ce.getSourceException();
				if ((sourceException instanceof PromotionException)) {
					PromotionException pe = (PromotionException)sourceException;
					processError(pe.getErrorCode(), pe, pRequest);
				}
				else {
					processError(ce.getErrorCode(), ce, pRequest);
				}
				
			}catch(DigitalRuntimeException ex){
				if(RewardsCertificateConstants.INVALID_CERTIFICATE.equalsIgnoreCase(ex.getMessage())){
					addFormException(new DropletException(getMessageLocator().getMessageString(INVALID_CERTIFICATE_VALUE)), getCouponClaimCode());
				}else{
					addFormException(new DropletException(getMessageLocator().getMessageString(ERR_ADD_CERT)), getCouponClaimCode());
				}
				this.logError("Exception occured while handling ", ex);
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				ret = false;				
			} catch (Exception e) {
				this.logError("Exception occured while handling ", e);
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				addFormException(new DropletException(getMessageLocator().getMessageString(ERR_ADD_CERT)), getCouponClaimCode());
				ret = false;
			} finally {
				if(this.getFormError()){
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				}
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(HANDLE_METHOD);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
			
		}else {
			logError("Its not a UniqueRequestEntry in handleClaimCoupon ");
			ret=false;
		}
		if(ret){
			ret = checkFormRedirect(null, null, pRequest, pResponse);
		}

		return ret;
	}

	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleClaimRewardCertificate(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		
		final String METHOD_NAME = "handleClaimRewardCertificate";
		final String HANDLE_METHOD = "DigitalCouponFormHandler." + METHOD_NAME;
		boolean ret=true;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		
		if (rrm == null || rrm.isUniqueRequestEntry( HANDLE_METHOD )) {
			
			UserTransactionDemarcation td = null;
			try{
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

				String claimCode = this.getRewardCertClaimCode();
				if(!this.validate(claimCode)){
					return false;
				}
				
				if(this.isRewardCertFlag()){
					addRewardCertificates( pRequest, pResponse);
				}else{
					addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_UNKNOWN_COUPON_EXCEPTION, new Object[] { claimCode } ), MSG_UNKNOWN_COUPON_EXCEPTION), getCouponClaimCode());
				}
			}catch(Exception e){
	        	this.logError("Exception occured while handling ", e);
	        	TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
	        	addFormException( new DropletException( getMessageLocator().getMessageString(ERR_ADD_CERT), ERR_ADD_CERT), getCouponClaimCode() );
	        	ret=false;
	        }finally{
				if(this.getFormError()){
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				}	        	
	       		TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
	       		TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
	       		if (rrm != null) {
					rrm.removeRequestEntry(HANDLE_METHOD);
				}
	        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
	        }
		}else {
			logError("Its not a UniqueRequestEntry in handleClaimRewardCertificate ");
			ret=false;
		}
		if(ret){
			ret = checkFormRedirect(null, null, pRequest, pResponse);
		}		
		return ret;
	}
	
	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleRemovePromotion( DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse ) throws ServletException, IOException
	{
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String METHOD_NAME = "handleRemovePromotion";
		final String HANDLE_METHOD = "DigitalCouponFormHandler." + METHOD_NAME;
		boolean ret = true;
		if (rrm == null || rrm.isUniqueRequestEntry(HANDLE_METHOD)) {
			UserTransactionDemarcation td = null;
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				
				String promotionId = DigitalStringUtil.trimToEmpty(getRemovePromotion());
				if (DigitalStringUtil.isNotBlank(promotionId)) {
					revokePromotion(promotionId);
					clearData();
				}
			} catch ( Exception ex) {
				if (isLoggingError()) {
					logError("Caught Exception in handleRemovePromotion "+ ex.getMessage());
				}
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_REMOVE_PROMO), ERR_REMOVE_PROMO), getCouponClaimCode());
				ret = false;
			} finally {
				if(this.getFormError()){
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				}
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if(rrm != null){
					rrm.removeRequestEntry(HANDLE_METHOD); 
				}
		        DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		} else {
			logError("Its not a UniqueRequestEntry in handleRemovePromotion ");
			ret=false;
		}
		if(ret){
			ret = checkFormRedirect(null, null, pRequest, pResponse);
		}
		return ret;
	}
	
	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleRemoveCoupon( DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse ) throws ServletException, IOException
	{
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String METHOD_NAME = "handleRemoveCoupon";
		final String HANDLE_METHOD = "DigitalCouponFormHandler." + METHOD_NAME;
		boolean ret = true;
		if (rrm == null || rrm.isUniqueRequestEntry( HANDLE_METHOD )) {
			 UserTransactionDemarcation td = null;
			 try{
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				String coupon = DigitalStringUtil.trimToEmpty( getRemoveCoupon() );
				String msgInvalidCoupon = "Entered coupon " + coupon + "is not valid.";
				if( DigitalStringUtil.isNotBlank( coupon ) ) {
					if((true == isCoupon(coupon))) {
						Set promotionItems = getCommercePromotionTools().getPromotion( coupon );
						if (promotionItems !=null){
							Iterator itPromos = promotionItems.iterator();
							while (itPromos.hasNext()){
								RepositoryItem promotionItem = (RepositoryItem)itPromos.next();
								if( promotionItem != null ) {
									revokePromotion( promotionItem.getRepositoryId() );
									clearData();
								}
							}
						}
					} else {
						addFormException(new DropletException(msgInvalidCoupon + this.getMessageLocator().getMessageString(ERR_REMOVE_PROMO), ERR_REMOVE_PROMO), getCouponClaimCode());
						ret=false;
					}
				}
			 }catch(Exception ex) {
				 if (isLoggingError()) {				
					logError("Caught Exception in handleRemoveCoupon " +ex.getMessage());
				  }
				 TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				 addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_REMOVE_PROMO), ERR_REMOVE_PROMO), getCouponClaimCode());
				 ret=false;
			 }finally {
				if(this.getFormError()){
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				}				 
			 	TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
			 	TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
			 	if(rrm != null){
			 		rrm.removeRequestEntry(HANDLE_METHOD); 
			 	}
		        DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			 } 
		} else {
			logDebug("Its not a UniqueRequestEntry in handleRemoveCoupon ");
			ret=false;
		}
		
		if(ret){
			ret = checkFormRedirect(null, null, pRequest, pResponse);
		}
		return ret;
	}
	

	/**
	 * Revokes the promotion, updates pricing models and reprices the order?
	 *
	 * @param promotionId
	 * @throws PromotionException
	 */
	private void revokePromotion( String promotionId ) throws PromotionException {
		getCommercePromotionTools().revokePromotion( getProfile(), promotionId, true, getPricingModelHolder() );
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleRemoveRewardCertificate( final DynamoHttpServletRequest request, final DynamoHttpServletResponse response ) 
			throws ServletException, IOException{
		
		if( DigitalStringUtil.isBlank( this.getRewardCertificateId()) ) {
			addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_REMOVE_CERT), ERR_REMOVE_CERT), getCouponClaimCode());
			return false;
		}
		
		final String METHOD_NAME = "handleRemoveCertificate";
		final String HANDLE_METHOD = "DigitalCouponFormHandler." + METHOD_NAME;
		boolean ret=true;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry( HANDLE_METHOD )) {
			UserTransactionDemarcation td = null;
			try{
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				
				getRewardCertificateManager().removeCertificateFromOrder( getOrder(), this.getRewardCertificateId() );
				getOrderManager().updateOrder( getOrder() );
	
				getRewardCertificateManager().changeCertificateStatus( getProfile(), this.getRewardCertificateId(), ProfileRewardCertificateStatus.INITIAL.getValue() );
				clearData();
			}catch(DigitalRuntimeException ex){
				if(RewardsCertificateConstants.INVALID_CERTIFICATE.equalsIgnoreCase(ex.getMessage())){
					addFormException(new DropletException(this.getMessageLocator().getMessageString(INVALID_CERTIFICATE_VALUE), INVALID_CERTIFICATE_VALUE), getCouponClaimCode());
				}else{
					addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_REMOVE_CERT), ERR_REMOVE_CERT), getCouponClaimCode());
				}
				logError("Exception removing certificate",ex);
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				ret= false;
			}catch (Exception e) {
				logError("Exception removing certificate",e);
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_REMOVE_CERT), ERR_REMOVE_CERT), getCouponClaimCode());
				ret= false;
			}finally{
				if(this.getFormError()){
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				}				
	       		TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
	       		TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
	       		if(rrm != null){
	       			rrm.removeRequestEntry(HANDLE_METHOD);
	       		}
	        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
	        }
		}else {
			logError("Its not a UniqueRequestEntry in handleRemoveRewardCertificate ");
			ret=false;
		}
		if(ret){
			ret = checkFormRedirect(null, null, request, response);
		}
		return ret;
	}
	
	/**
	 * 
	 * @param claimCode
	 * @return true or false
	 */
	private boolean validate(String claimCode) {
		if( DigitalStringUtil.isBlank( claimCode ) ) {
			addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_COUPON_CODE_REQUIRED), MSG_COUPON_CODE_REQUIRED), claimCode);
		} else if( ( (DigitalOrderImpl)getOrder() ).getIsGiftCardOrderOnly() ) {
			addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_GIFT_CARD_ONLY_PROMO), MSG_GIFT_CARD_ONLY_PROMO), claimCode);
		} else {

			if(isCoupon(claimCode)){
				setRewardCertFlag(false);
			}else if( getRewardCertificateManager().isRewardCertificate( claimCode ) ) {
				setRewardCertFlag(true);
				String rewardCertificate = claimCode;
				if( rewardCertificate.length() == getDswConstants().getRewardCertificateMarkdownLength() ) {
					String certNumber = getRewardCertificateManager().findCertificateNumberUsingMarkdownCode( getProfile(), rewardCertificate );
					if( certNumber != null ) rewardCertificate = certNumber;
					setRewardCertClaimCode(rewardCertificate);
				}
				else if( rewardCertificate.length() == getDswConstants().getRewardCertificateLength()){
					setRewardCertClaimCode(rewardCertificate);
				}else{
					//unable to find certificate number for markdown code
					if(!getProfileTools().isLoggedIn( getProfile() )){
						addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_LOGIN_FOR_CERT), MSG_LOGIN_FOR_CERT), claimCode);
					}
					else{
						addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_ADD_CERT), ERR_ADD_CERT), claimCode);
					}
				}
			} else {
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_MISSING_COUPON_EXCEPTION, new Object[] { claimCode } ), MSG_MISSING_COUPON_EXCEPTION), claimCode);
			}

			if(!getFormError() && isRewardCertFlag() && DigitalStringUtil.isBlank(getRewardCertClaimCode())){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_ADD_CERT), ERR_ADD_CERT), claimCode);
			}else if(!getFormError() && !isRewardCertFlag() && DigitalStringUtil.isBlank(this.getCouponClaimCode())){
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_COUPON_CODE_REQUIRED), MSG_COUPON_CODE_REQUIRED), claimCode);
			}
			
			if( !DigitalStringUtil.isBlank(getRewardCertClaimCode()) && !getProfileTools().isLoggedIn( getProfile() ) ) {
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_LOGIN_FOR_CERT ), MSG_LOGIN_FOR_CERT), claimCode);
			}
		}
		
		return !this.getFormError();
	}
	
	/** 
	 * This method process the reward certificates and also checks for the
	 * number of the reward certificates used.
	 *
	 * @param request
	 * @param response
	 */
	protected Set<String> addRewardCertificates( final DynamoHttpServletRequest request, final DynamoHttpServletResponse response) {
		Set<String> retVal = new LinkedHashSet<>();
		if( !getProfileTools().isLoggedIn( getProfile() ) ) {
			addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_LOGIN_FOR_CERT), MSG_LOGIN_FOR_CERT), getCouponClaimCode());
			return retVal;
		} 
		
		final String memberId = (String)getProfile().getPropertyValue( this.getProfilePropertyManager().getLoyaltyNumberPropertyName() );
		
		Set<String>	rewardCertificates	= new LinkedHashSet<>();
		rewardCertificates.add(this.getRewardCertClaimCode());
		
		Set<LoyaltyCertificate> allCertificates = getRewardCertificateManager()
				.getAllValidatedCertificates(rewardCertificates, memberId);

		if (allCertificates == null || allCertificates.isEmpty() || !getRewardCertificateManager().isAnyValidCertifcateFound(allCertificates)) {
			addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_ADD_CERT), ERR_ADD_CERT), getCouponClaimCode());
			return retVal;
		}

		if (getRewardCertificateManager().isRewardCertificateAlreadyApplied(allCertificates, rewardCertificates, memberId)) {
			String claimCode = this.getCouponClaimCode();
			if(DigitalStringUtil.isEmpty(claimCode)){
				claimCode = this.getRewardCertClaimCode();
			}
			addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_DUPLICATE_CERT,
					new Object[] { claimCode }), MSG_DUPLICATE_CERT), getCouponClaimCode());
			return retVal;
		}
		
		Set<LoyaltyCertificate> validatedCertificates = getRewardCertificateManager()
				.findValidatedAvailableCertificates(allCertificates, rewardCertificates, memberId);
		Set<String> appliedClaimCodes = new LinkedHashSet<>();
		if( validatedCertificates != null && validatedCertificates.size() > 0 ) {
			for( LoyaltyCertificate validatedCertificate : validatedCertificates ) {
				String certificateId = validatedCertificate.getId();
				String claimCode = validatedCertificate.getCertificateNumber();

				if( ( DigitalStringUtil.isNotBlank( getRewardCertificateId() ) && getRewardCertificateId().equals( certificateId ) )
						|| ( DigitalStringUtil.isBlank( getRewardCertificateId() ) 
								&& !Iterables.filter( appliedClaimCodes, Predicates.equalTo( claimCode ) ).iterator().hasNext() ) ) {
					if( !validatedCertificate.isValid() ) {
						addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_ADD_CERT, new Object[] { certificateId }), ERR_ADD_CERT), getCouponClaimCode());
					} else if( getRewardCertificateManager().isCertificateAlreadyApplied( getOrder(), certificateId ) ) {
						addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_DUPLICATE_CERT, new Object[] { certificateId }), MSG_DUPLICATE_CERT), getCouponClaimCode());
					} else {
						try {
							getRewardCertificateManager().addCertificateToOrder( getOrder(), validatedCertificate );
							getRewardCertificateManager().changeCertificateStatus( getProfile(), certificateId, ProfileRewardCertificateStatus.APPLIED.getValue() );
							appliedClaimCodes.add( claimCode );
							retVal.add( certificateId );
						} catch( RepositoryException e ) {
							addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_ADD_CERT_TO_ORDER, new Object[] { certificateId }), ERR_ADD_CERT_TO_ORDER), getCouponClaimCode());
							if( isLoggingError() ) {
								logError( e );
							}
						}
					}
				}
			}
		}
		if( !retVal.isEmpty() ) {
			try {
				getOrderManager().updateOrder( getOrder() );
				this.clearMessages();
			} catch( CommerceException e ) {
				addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_ADD_CERT), ERR_ADD_CERT), getCouponClaimCode());
			}
		}else{
			addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_ADD_CERT), ERR_ADD_CERT), getCouponClaimCode());
		}
		return retVal;
	}
	

	

	/**
	 * This method is for claiming DSW Coupons. this method also validates
	 * promotion exclusion by calling promotionValidation() method of
	 * PromotionValidationService
	 *
	 * @param request
	 * @param response
	 * @param couponList
	 * @throws ServletException
	 * @throws IOException
	 */
	protected Set<String> addCoupons( DynamoHttpServletRequest request, DynamoHttpServletResponse response, Collection<String> couponList ) 
			throws ServletException, IOException, ClaimableException {
		Set<String> retVal = new LinkedHashSet<>();

		Set<String> validCoupons = new LinkedHashSet<>();
		for( String couponCode : couponList ) {
			couponCode = couponCode.toUpperCase();
			if( getCommercePromotionTools().isPromotionAlreadyApplied( getProfile(), couponCode ) ) {
				processError(MSG_DUPLICATE_COUPON, null, request); 
				logError("Coupon Already Applied :: " + couponCode + " ::");
			} else 
			if( isCouponRestricted( couponCode ) ) {
				if(getCommercePromotionTools().isAppOnlyCoupon(couponCode)){
					addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_APP_ONLY_COUPON_EXCEPTION, new Object[] { couponCode }),MSG_APP_ONLY_COUPON_EXCEPTION), getCouponClaimCode());
				}else{
					addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_MISSING_COUPON_EXCEPTION, new Object[] { couponCode })), getCouponClaimCode());
				}
				
			} else if( isCouponValidForProfile( couponCode ) ) {
				/* getPromotionValidationService().promotionValidation( couponCode ); - this method in live site doesn't do any validation. 
				   The actual validation step (PromotionValidationService.checkPromoValidation) is commented out, which checks for inclusion and exclusion list of the current promotion.
				   This validation is performed in RepricingService */
				validCoupons.add( couponCode );
			}
		}
		for( String couponCode : validCoupons ) {
			claimCoupon( couponCode );
			if(!this.getFormError()){
			RepositoryItem claimable;
			try {
				claimable = getClaimableTools().getClaimableItem(couponCode);
				if(null!=getClaimableManager().getCouponBatchTools().getCouponBatch(couponCode.trim()) && null!=claimable){
					Integer value = (Integer)claimable.getPropertyValue(getClaimableTools().getUsesPropertyName());
					int uses = value == null ? 0 : value.intValue();
					DynamicBeans.setPropertyValue(claimable, getClaimableTools().getUsesPropertyName(), Integer.valueOf(uses - 1));
					((MutableRepository)getClaimableTools().getClaimableRepository()).updateItem((MutableRepositoryItem)claimable);
				}
			}catch (PropertyNotFoundException e) {
				logError( e );
			}catch (RepositoryException e) {
				logError( e );
			} 
			}
		}
		return retVal;

	}

	/**
	 * 
	 * @param claimCode
	 * @throws ClaimableException
	 */
	protected void claimCoupon( String claimCode ) throws ClaimableException {
		String promoId= null;
		getClaimableManager().claimCoupon( getProfile().getRepositoryId(), claimCode );
		try {
			if( !getCommercePromotionTools().addPromotion( getProfile(), claimCode ) ) {
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_UNKNOWN_COUPON_EXCEPTION, new Object[] { claimCode }), MSG_UNKNOWN_COUPON_EXCEPTION), claimCode);
			}
			else {
				Set<RepositoryItem> promotions = getCommercePromotionTools().getPromotion(claimCode);
				if (promotions!=null && promotions.size()>0) {
					RepositoryItem promoItem = (RepositoryItem) promotions.toArray()[0];
					if (!promoItem.getItemDescriptor().getItemDescriptorName().equals("Shipping Discount")) {
						promoId = promoItem.getRepositoryId();
						if (isLoggingDebug()){
							logDebug("Before clearing and before Setting the promo Id and coupon Id to the Coupon Message component -- "+getCouponMessage().getPromoId() + " Coupon Id# "+getCouponMessage().getCouponCode());
						}
						getCouponMessage().clearBean();
						if (isLoggingDebug()){
							logDebug("Before clearing and before Setting the promo Id and coupon Id to the Coupon Message component -- "+getCouponMessage().getPromoId() + " Coupon Id# "+getCouponMessage().getCouponCode());
						}
						if (isLoggingDebug()){
							logDebug("Setting the promo Id and coupon Id to the Coupon Message component -- "+promoId + " Coupon Id# "+claimCode);
						}
						getCouponMessage().setPromoId(promoId);
						getCouponMessage().setCouponCode(claimCode);
					}
				}
			}

		} catch( RepositoryException e ) {
			addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_UNKNOWN_COUPON_EXCEPTION, new Object[] { claimCode }), MSG_UNKNOWN_COUPON_EXCEPTION), claimCode);
			if( isLoggingError() ) {
				logError( e );
			}
		}

	}
	
	/**
	 * Returns true if the coupon is valid for the state of the current profile.
	 * For instance if the coupon requires the user to be logged in, it will
	 * return true only if they are logged in. If it is an anonymous coupon it
	 * will return true always.
	 *
	 * @param couponCode
	 *            this should be a code for a valid coupon.
	 * @return true of false
	 */
	protected boolean isCouponValidForProfile( String couponCode ) {
		boolean retVal = true;
		if( getCommercePromotionTools().isTargetedOffer( couponCode ) ) {
			if( !getCommercePromotionTools().validateTargettedOfferLists( couponCode ) ) {
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_UNKNOWN_COUPON_EXCEPTION, new Object[] { couponCode }), MSG_UNKNOWN_COUPON_EXCEPTION), couponCode);
				retVal = false;
			} else {
				if( getProfileTools().isLoggedIn( getProfile() ) ) {
					if( !getCommercePromotionTools().isTargetedCouponValidForProfile( getProfile(), couponCode ) ) {
						addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_COUPON_CODE_TARGETED, new Object[] { couponCode }), MSG_COUPON_CODE_TARGETED), couponCode);
						retVal = false;
					}
					
				} else {
					addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_COUPON_CODE_PRIVATE, new Object[] { couponCode }), MSG_COUPON_CODE_PRIVATE), couponCode);
					retVal = false;
				}
			}
		} else if( getCommercePromotionTools().isValidCoupon(couponCode)){ 
			if(!getCommercePromotionTools().isCouponValidForProfile( getProfile(), couponCode ) ) {
				addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_COUPON_CODE_PRIVATE, new Object[] { couponCode }), MSG_COUPON_CODE_PRIVATE), couponCode);
				retVal = false;
			}
			Map<String, Boolean> tieredPromoValidation=new HashMap();
			tieredPromoValidation=getCommercePromotionTools().isTieredCouponValidForProfile( getProfile(), couponCode);
			Iterator it=tieredPromoValidation.entrySet().iterator();
			while(it.hasNext()){
				Map.Entry entry = (Map.Entry)it.next();
					if( entry.getValue().equals(Boolean.FALSE) ) {
						addFormException(new DropletException(this.getMessageLocator().getMessageString((String)entry.getKey(), new Object[] { couponCode }), (String)entry.getKey()), couponCode);
						retVal = false;
					}
			}
		} else{
			addFormException(new DropletException(this.getMessageLocator().getMessageString(MSG_UNKNOWN_COUPON_EXCEPTION, new Object[] { couponCode }), MSG_UNKNOWN_COUPON_EXCEPTION), couponCode);
			retVal = false;			
		}
		return retVal;
	}

	public void addFormException(DropletException exc, String code) {
		if(isLoggingError() && exc != null){
			if(DigitalStringUtil.isNotBlank(code)) {
				logError(exc.getMessage() + ":: CODE :: " + code);
			}else{
				logError(exc.getMessage());
			}
		}
		super.addFormException(exc);
	}

	/**
	 * Returns true if the coupon is restricted
	 *
	 * @param couponId
	 * @return
	 */
	protected boolean isCouponRestricted( String couponId ) {
		return getCommercePromotionTools().isCouponRestricted( couponId );
	}
	
	/**
	 * 
	 * @return List<LoyaltyCertificate>
	 */
	public List<LoyaltyCertificate> getProfileCertificates(){
		List<LoyaltyCertificate> list = new ArrayList<>();
		final String METHOD_NAME = "getProfileCertificates";
		UserTransactionDemarcation td = null;
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
		
			list = this.getRewardCertificateManager().getProfileCertificates(profile);
		}catch (Exception e) {
			logError("Exception getProfileCertificates",e);
			TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
			addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_REMOVE_CERT), ERR_REMOVE_CERT), getCouponClaimCode());
		}finally{
       		TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
        }
		
		return list;
	}

	/**
	 * Return list of Applied Rewards Certificates (applied but NOT reserved).
	 * @return List<LoyaltyCertificate>
	 */
	public List<LoyaltyCertificate> getAppliedORReservedOrderCertificates(){
		List<LoyaltyCertificate> list = new ArrayList<>();
		final String METHOD_NAME = "getAppliedOrderCertificates";
		UserTransactionDemarcation td = null;
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
			DigitalOrderImpl order = null;
			if(DigitalStringUtil.isNotBlank(this.getOrderId())){
				order = (DigitalOrderImpl)getOrderManager().loadOrder(orderId);
			}else{
				order = (DigitalOrderImpl)this.getOrder();
			}
			list = this.getRewardCertificateManager().getAppliedORReservedOrderCertificates(order);

		}catch (Exception e) {
			logError("Exception getAppliedOrderCertificates",e);
			TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
			addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_REMOVE_CERT), ERR_REMOVE_CERT), getCouponClaimCode());
		}finally{
			TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}

		return list;
	}
	
	/**
	 * 
	 * @return List<LoyaltyCertificate>
	 */
	public List<LoyaltyCertificate> getRedeemedOrderCertificates(){
		List<LoyaltyCertificate> list = new ArrayList<>();
		final String METHOD_NAME = "getRedeemedOrderCertificates";
		UserTransactionDemarcation td = null;
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
			DigitalOrderImpl order = null;
			if(DigitalStringUtil.isNotBlank(this.getOrderId())){
				order = (DigitalOrderImpl)getOrderManager().loadOrder(orderId);
			}else{
				order = (DigitalOrderImpl)this.getOrder();
			}
			list = this.getRewardCertificateManager().getRedeemedOrderCertificates(order);
			
		}catch (Exception e) {
			logError("Exception getRedeemedOrderCertificates",e);
			TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
			addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_REMOVE_CERT), ERR_REMOVE_CERT), getCouponClaimCode());
		}finally{
       		TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
        }
		
		return list;
	}

	/**
	 *
	 * @return Set<AppliedOffer>
	 */
	public Set<AppliedOffer> getAppliedCoupons() {
		Set<AppliedOffer> offers = new HashSet<>();

		final String METHOD_NAME = "getAppliedCoupons";
		UserTransactionDemarcation td = null;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

			offers = this.getCommercePromotionTools().getAppliedCoupons(profile);
		}catch (Exception e) {
			logError("Exception getAppliedCoupons",e);
			TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
			addFormException(new DropletException(this.getMessageLocator().getMessageString(ERR_REMOVE_CERT), ERR_REMOVE_CERT), getCouponClaimCode());
		}finally{
			TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
		return offers;
	}

	/**
	 * Override the method to stop commit & post commit steps if there is any
	 * form exceptions during validation. Rollback the transaction.
	 * 
	 * @param pSuccessURL
	 * @param pFailureURL
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 */
	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL,
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if ((getCheckForValidSession()) && (!isValidSession(pRequest))) {
			addFormException(new DropletException(
					"Your session expired since this form was displayed - please try again.",
					"sessionExpired"), getCouponClaimCode());
		}

		if (getFormError()) {

			if (isLoggingDebug()) {
				logDebug("error - redirecting to: " + pFailureURL);
			}
			//redirectOrForward(pRequest, pResponse, pFailureURL);
			return false;
		}

		if (DigitalStringUtil.isBlank(pSuccessURL)) {
			if (isLoggingDebug()) {
				logDebug("no form errors - staying on same page.");
			}
			return true;
		}

		if (isLoggingDebug()) {
			logDebug("no form errors - redirecting to: " + pSuccessURL);
		}
		//redirectOrForward(pRequest, pResponse, pSuccessURL);
		return false;
	}

	/**
	 * This method validate whether the given code is for a coupon
	 *
	 * @param code
	 * @return
	 */
	protected boolean isCoupon( String code ) {
		return null != getCommercePromotionTools().getCoupon( code );
	}

	/**  */
	@Override
	public void afterGet( DynamoHttpServletRequest request, DynamoHttpServletResponse response ) {
		clearMessages();
		super.afterGet( request, response );
	}

	protected void clearMessages() {
		super.getFormExceptions().clear();
	}

	/** */
	@Override
	public void beforeGet( DynamoHttpServletRequest request, DynamoHttpServletResponse response ) {
		//getRewardCertificateManager().checkInvalidCertificates( getOrder() );
		super.beforeGet( request, response );
	}
	
	/** 
	 * Clears all of the session state. 
	 */
	protected void clear() {
		//clearMessages();
		clearData();
	}

	/**
	 * Clear all data
	 */
	private void clearData() {
		setCouponClaimCode(null );
		setRewardCertClaimCode(null);
		setRewardCertificateId( null );
		setRemovePromotion( null );
	}

	/**
	 * 
	 * @return String
	 */
	public String getRepeatingRequestMonitorName() {
		return "DigitalCouponFormHandler.save";
	}

	/**
	 * 
	 * @return DigitalPromotionTools
	 */
	private DigitalPromotionTools getCommercePromotionTools() {
		return (DigitalPromotionTools)getPromotionTools();
	}

	/**
	 * 
	 * @return DigitalCommercePropertyManager
	 */
	public DigitalCommercePropertyManager getProfilePropertyManager(){
		return (DigitalCommercePropertyManager)this.getProfileTools().getPropertyManager();
	}


}
