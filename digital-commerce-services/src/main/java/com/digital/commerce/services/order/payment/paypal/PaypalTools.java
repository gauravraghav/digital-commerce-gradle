package com.digital.commerce.services.order.payment.paypal;

import static com.digital.commerce.common.util.ComponentLookupUtil.PRICING_MODEL_COMP;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.DigitalProfileConstants;
import com.digital.commerce.constants.PaypalConstants;
import com.digital.commerce.services.common.AddressType;
import com.digital.commerce.services.common.AddressTypeDetails;
import com.digital.commerce.services.common.CountryCode;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.paypal.valueobject.PaypalAuthenticateResponse;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShipType;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShippingGroupPropertyManager;
import com.digital.commerce.services.profile.DigitalCommercePropertyManager;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.utils.DigitalAddressUtil;
import com.digital.commerce.services.validator.DigitalBasicAddressValidator;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.core.util.ContactInfo;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaypalTools extends ApplicationLoggingImpl{
	
	static final String CLASSNAME = PaypalTools.class.getName();
	
	static final String REPRICE_SHIPPING = "SHIPPING";

	public PaypalTools() {
		super(PaypalTools.class.getName());
	}

	private DigitalOrderTools orderTools;
	private DigitalPaymentGroupManager paymentGroupManager;
	private OrderManager orderManager;
	private DigitalAddressUtil addressUtil;
	private PricingTools pricingTools;
	private PricingModelHolder	userPricingModel;
	private DigitalProfileTools profileTools;
	private DigitalBasicAddressValidator addressValidator;

	/**
	 * Modifies order to add PayPal payment group, update shipping address for
	 * guest user and reprices the order.
	 * 
	 * @param order
	 * @param expressCheckoutDetails
	 * @param profile
	 * @throws CommerceException
	 */

	public void updateOrderData(Order order, PaypalAuthenticateResponse authenticateResponse, Profile profile)
			throws CommerceException {
		boolean repriceShipCost = false;
		try {
			//Create Paypal payment group
			createPaypalPaymentGroup(order,authenticateResponse,profile);
			
			if (getOrderManager().getShippingGroupManager().isAnyHardgoodShippingGroups(order)) {
				
				boolean validPaypalShipAddr = validatePaypalShippingAddress(authenticateResponse);
				
				@SuppressWarnings("unchecked")
				Collection<ShippingGroup> shippingGroups = order.getShippingGroups();
				for(ShippingGroup sg:shippingGroups){
					if (sg instanceof HardgoodShippingGroup) {
						@SuppressWarnings("unchecked")
						Collection<CommerceItemRelationship> rels = sg.getCommerceItemRelationships();
						
						if (rels != null && rels.size() > 0) {
							
							HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sg;
							String shipType = (String) hgSg.getPropertyValue(ShippingGroupPropertyManager.SHIP_TYPE.getValue());
							DigitalRepositoryContactInfo shippingAddress = (DigitalRepositoryContactInfo)hgSg.getShippingAddress();

							//Don't update BOPIS / BOSTS shipping addresses with PayPal address
							if (shipType != null && !shipType.equalsIgnoreCase(ShipType.SHIP.getValue())) {
								//Also, If any BOPIS / BOSTS shipping first / last names are empty, set them from billing address
								if (DigitalStringUtil.isBlank(shippingAddress.getFirstName())
										|| DigitalStringUtil.isBlank(shippingAddress.getLastName())) {
									ContactInfo billingAddress = this.getBillingAddress(order, profile);
									shippingAddress.setFirstName(billingAddress.getFirstName());
									shippingAddress.setLastName(billingAddress.getLastName());
									shippingAddress.setMiddleName(billingAddress.getMiddleName());
								}
								continue;
							}
							
							if(getAddressUtil().isEmptyAddress(shippingAddress) && validPaypalShipAddr){
								if(shippingAddress == null){
									shippingAddress = new DigitalRepositoryContactInfo();
								}
								String fullName = authenticateResponse.getShippingAddress().getName();
								
								if(fullName != null && fullName.length() > 1){
									StringTokenizer stok = new StringTokenizer(fullName);
								    String firstName = stok.nextToken();

								    StringBuilder lastName = new StringBuilder();
								    while (stok.hasMoreTokens())
								    {
								    	lastName.append(stok.nextToken() + " ");
								    }			    
									
								    shippingAddress.setFirstName(firstName);
								    shippingAddress.setLastName(lastName.toString().trim());		
								}
								else {
									shippingAddress.setFirstName(authenticateResponse.getFirstName());
									shippingAddress.setLastName(authenticateResponse.getLastName());
								}
								
								shippingAddress.setAddress1(authenticateResponse.getShippingAddress().getAddr1());
								shippingAddress.setAddress2(authenticateResponse.getShippingAddress().getAddr2());
								shippingAddress.setCity(authenticateResponse.getShippingAddress().getCity());
								shippingAddress.setState(authenticateResponse.getShippingAddress().getState());
								shippingAddress.setCountry(authenticateResponse.getShippingAddress().getCountry().trim());
								shippingAddress.setPostalCode(authenticateResponse.getShippingAddress().getZip());
								shippingAddress.setPhoneNumber(getAddressUtil().removeSpecialCharacters(authenticateResponse
																.getShippingAddress().getPhone()));
								
								String city = authenticateResponse.getShippingAddress().getCity();
								if(DigitalProfileConstants.CITY_APO.equalsIgnoreCase(city) || DigitalProfileConstants.CITY_FPO.equalsIgnoreCase(city)){
									shippingAddress.setRank(authenticateResponse.getShippingAddress().getCity());
									shippingAddress.setRegion(authenticateResponse.getShippingAddress().getState());
								}
								
								AddressType addressType = AddressType.valueFor(shippingAddress);
								if(addressType != null){
									shippingAddress.setAddressType(addressType.getValue());
								}
								hgSg.setShippingAddress(shippingAddress);
								repriceShipCost=true;
							}else if(!getAddressUtil().isEmptyAddress(shippingAddress)){
								repriceShipCost=true;
							}
							
						}
					}
				}
				
				if(repriceShipCost){
					//priceModelHolder is session scoped component, while PaypalTools has global scope hence lookup the component
					PricingModelHolder pricingModelHolder = ComponentLookupUtil.lookupComponent(PRICING_MODEL_COMP, PricingModelHolder.class); 
					//Recalculate shipping price and DSW shipping method validations
					getPricingTools().performPricingOperation( REPRICE_SHIPPING, order, pricingModelHolder, Locale.ENGLISH, profile, null );
				}
			}
			//profile locks are acquired in the called class PaypalAuthenticationFormHandler, hence not acquiring locks here
			synchronized(order) {
				getOrderManager().updateOrder(order);
			}

		} catch (CommerceException cEx) {
			if (isLoggingError()) {
				logError(cEx);
			}
			throw cEx;
		} 
	}
	
	/**
	 * Creates new PayPal Payment, sets billing address & amount
	 * 
	 * @param authenticateResponse
	 * @param order
	 * @param profile
	 */

	public void createPaypalPaymentGroup(Order order,
			PaypalAuthenticateResponse authenticateResponse, Profile profile)
			throws CommerceException {
		ContactInfo oldBillingAddress = this.getOrderTools().getOrderBillingAddress(order);
		getPaymentGroupManager().removeAllPaymentGroupsFromOrder(order, getPaymentGroupManager().getGiftCardPaymentGroups(order));
		PaypalPayment paypalGroup = (PaypalPayment) getPaymentGroupManager()
				.createPaymentGroup(PaypalConstants.PAYPAL_PAYMENT_TYPE);

		paypalGroup.setCardinalOrderId(authenticateResponse.getCardinalOrderId());
		paypalGroup.setPaypalPayerId(authenticateResponse.getPaypalPayerId());
		paypalGroup.setPaypalEmail(authenticateResponse.getPaypalEmail());

		ContactInfo billingAddress = new ContactInfo();

		// Set billing address from order
		if (!getAddressUtil().isEmptyAddress(oldBillingAddress)) {
			billingAddress = getProfileTools().createBillingAddress(oldBillingAddress);
		} else {
			// Set Billing Address from Profile's default credit card billing address, if not set in Order
			RepositoryItem defaultCreditCard = this.getProfileTools().getDefaultCreditCard(profile);
			if(null != defaultCreditCard){
				DigitalCommercePropertyManager pm = (DigitalCommercePropertyManager)this.getProfileTools().getPropertyManager();
				RepositoryItem profileBillingAddress = (RepositoryItem)defaultCreditCard.getPropertyValue(pm.getCreditCardBillingAddressPropertyName());
				if (null != profileBillingAddress) {
					DigitalOrderTools.copyAddress(profileBillingAddress, billingAddress);
				}
			}
		}
		
		if(oldBillingAddress != null && (DigitalStringUtil.isNotBlank(oldBillingAddress.getEmail()) 
				|| DigitalStringUtil.isNotBlank(oldBillingAddress.getPhoneNumber()))){
			billingAddress.setPhoneNumber(oldBillingAddress.getPhoneNumber());
			billingAddress.setEmail((oldBillingAddress.getEmail()));
		}
		
		// Set billing address from Paypal,if not set in Order or Profile
		if (getAddressUtil().isEmptyAddress(billingAddress)) {
			String fullName = authenticateResponse.getBillingAddress().getName();

			if (fullName != null && fullName.length() > 1) {

				StringTokenizer stok = new StringTokenizer(fullName);
				String firstName = stok.nextToken();

				StringBuilder lastName = new StringBuilder();
				while (stok.hasMoreTokens()) {
					lastName.append(stok.nextToken() + " ");
				}

				billingAddress.setFirstName(firstName);
				billingAddress.setLastName(lastName.toString().trim());
			} else {
				billingAddress.setFirstName(authenticateResponse.getFirstName());
				billingAddress.setLastName(authenticateResponse.getLastName());
			}

			billingAddress.setAddress1(authenticateResponse.getBillingAddress().getAddr1());
			billingAddress.setAddress2(authenticateResponse.getBillingAddress().getAddr2());
			billingAddress.setCity(authenticateResponse.getBillingAddress().getCity());
			billingAddress.setState(authenticateResponse.getBillingAddress().getState());
			billingAddress.setCountry(authenticateResponse.getBillingAddress().getCountry().trim());
			billingAddress.setPostalCode(authenticateResponse.getBillingAddress().getZip());

		}
		if(billingAddress.getEmail() == null &&
				DigitalStringUtil.isNotBlank(authenticateResponse.getPaypalEmail())){
			// For billing email use paypalemail.
			billingAddress.setEmail(authenticateResponse.getPaypalEmail());
		}
		
		if(billingAddress.getPhoneNumber() == null && authenticateResponse.getBillingAddress() != null &&
				(DigitalStringUtil.isNotBlank(authenticateResponse.getBillingAddress().getPhone()) ||
						DigitalStringUtil.isNotBlank(authenticateResponse.getShippingAddress().getPhone())	)){
			// For billing phone if they are not present, use ones from
			// shipping.
			String phoneNumber = Strings.isNullOrEmpty(authenticateResponse
					.getBillingAddress().getPhone()) ? authenticateResponse
					.getShippingAddress().getPhone() : authenticateResponse
					.getBillingAddress().getPhone();

			billingAddress.setPhoneNumber(getAddressUtil().removeSpecialCharacters(phoneNumber));
		}
		paypalGroup.setBillingAddress(billingAddress);
		paypalGroup.setCurrencyCode(order.getPriceInfo().getCurrencyCode());

		// Add amount from order
		double amountRemaining = getPaymentGroupManager().getAmountRemaining( order, order.getPriceInfo().getTotal() );
		amountRemaining = DigitalCommonUtil.formatAmount(amountRemaining);
		paypalGroup.setAmount(amountRemaining);
		getPaymentGroupManager().addPaymentGroupToOrder(order, paypalGroup);
		getOrderManager().addRemainingOrderAmountToPaymentGroup(order,paypalGroup.getId());
		getPaymentGroupManager().recalculatePaymentGroupAmounts( order );

	}
	
	protected ContactInfo getBillingAddress(Order order, Profile profile) throws CommerceException{
		ContactInfo billingAddr = this.getOrderTools().getOrderBillingAddress(order);
		
		if(getAddressUtil().isEmptyAddress(billingAddr) || null == billingAddr){
			billingAddr = this.getProfileTools().getProfileBillingAddress(profile);		
		}
		
		return billingAddr;
	}
	
	public ContactInfo getShippingAddress(Order order, Profile profile) throws CommerceException{
		ContactInfo shippingAddress =null;
		//Set Shipping address from order
		if (!getOrderManager().getShippingGroupManager().isAnyHardgoodShippingGroups(order)) {
			return shippingAddress;
		}
		@SuppressWarnings("unchecked")
		Collection<ShippingGroup> shippingGroups = order.getShippingGroups();
		for(ShippingGroup sg: shippingGroups){
			if (sg instanceof HardgoodShippingGroup) {
				@SuppressWarnings("unchecked")
				Collection<CommerceItemRelationship> rels = sg.getCommerceItemRelationships();
				if (rels != null && rels.size() > 0) {
					HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sg;
					
					//Don't send BOPIS / BOSTS address to PayPal
					String shipType = (String) hgSg.getPropertyValue(ShippingGroupPropertyManager.SHIP_TYPE.getValue());
					if (shipType != null && !shipType.equalsIgnoreCase(ShipType.SHIP.getValue())) {
						continue;
					}
					ContactInfo shippingAddressTemp = (ContactInfo)hgSg.getShippingAddress();
					if(!getAddressUtil().isEmptyAddress(shippingAddressTemp)){
						shippingAddress = shippingAddressTemp;						
						break;
					}

				}
			}
		}
		
		if(shippingAddress == null){
			//No shipping address in order, get it from profile
			RepositoryItem defaultShippingAddress = getProfileTools().getDefaultShippingAddress(profile);
			if (defaultShippingAddress != null) {
				shippingAddress = new ContactInfo();
				try {
					DigitalOrderTools.copyAddress(defaultShippingAddress, shippingAddress);
				} catch (CommerceException cEx) {
					if (isLoggingError()) {
						logError(cEx);
					}
					throw cEx;
				}
				if(getAddressUtil().isEmptyAddress(shippingAddress)){
					shippingAddress = null;
				}
			}
		}
		
		return shippingAddress;
	}
	
	protected ContactInfo setNameToAddress(ContactInfo shippingAddress, PaypalAuthenticateResponse authenticateResponse){
		if(shippingAddress == null){
			shippingAddress = new ContactInfo();
		}
		String fullName = authenticateResponse.getShippingAddress().getName();
		
		if(fullName != null && fullName.length() > 1){
			StringTokenizer stok = new StringTokenizer(fullName);
		    String firstName = stok.nextToken();

		    StringBuilder lastName = new StringBuilder();
		    while (stok.hasMoreTokens())
		    {
		    	lastName.append(stok.nextToken() + " ");
		    }			    
			
		    shippingAddress.setFirstName(firstName);
		    shippingAddress.setLastName(lastName.toString().trim());		
		}
		else {
			shippingAddress.setFirstName(authenticateResponse.getFirstName());
			shippingAddress.setLastName(authenticateResponse.getLastName());
		}
		
		return shippingAddress;
	}
	
	public boolean isPaypalInternationalShippingAddress(PaypalAuthenticateResponse authenticateResponse){
		DigitalContactInfo address = getShippingAddressFromPaypal(authenticateResponse);

		AddressType addressType = AddressType.valueFor( address );
		if (isLoggingDebug()) {
			logDebug("AddressType=" + addressType);
		}
		
		if( AddressType.INTERNATIONAL.equals( addressType ) || PaypalConstants.INTERNATIONAL_ADDR_IND.equalsIgnoreCase(address.getFirstName())) {
			return true;
		}
		
		return false;
	}
	
	public boolean validatePaypalShippingAddress(PaypalAuthenticateResponse authenticateResponse){
	
		if(this.isPaypalInternationalShippingAddress(authenticateResponse)){
			return false;
		}
		DigitalContactInfo address = getShippingAddressFromPaypal(authenticateResponse);

		List<String> shippingAddValidationErrors = getAddressValidator().validateShippingAddress(address);
		
		if (isLoggingDebug()) {
			logDebug("shippingAddValidationErrors=" + shippingAddValidationErrors);
		}

		if (!Iterables.isEmpty(shippingAddValidationErrors)) {
			return false;
		}
		return true;		
	}
	
	protected DigitalContactInfo getShippingAddressFromPaypal(PaypalAuthenticateResponse authenticateResponse){
		AddressTypeDetails addressDTO = authenticateResponse.getShippingAddress();
		DigitalContactInfo address = new DigitalContactInfo();
		setNameToAddress(address, authenticateResponse);
		address.setAddress1( addressDTO.getAddr1() );
		address.setAddress2( addressDTO.getAddr2() );
		address.setCity( addressDTO.getCity() );
		address.setState( addressDTO.getState() );
	    if(null!=addressDTO.getState()){
	    	if(addressDTO.getState().equalsIgnoreCase(PaypalConstants.PAYPAL_REGION_AA)||addressDTO.getState().equalsIgnoreCase(PaypalConstants.PAYPAL_REGION_AP)||
	    			addressDTO.getState().equalsIgnoreCase(PaypalConstants.PAYPAL_REGION_AE)){
	    		address.setRegion(addressDTO.getState());	
	    		address.setState(null);
	    	}
	    }
		address.setPostalCode( addressDTO.getZip() );
		if(DigitalStringUtil.isNotBlank(addressDTO.getCountry()) && CountryCode.US.getValue().equalsIgnoreCase(addressDTO.getCountry())){
			address.setCountry( CountryCode.USA.getValue() );
		}else{
			address.setCountry( addressDTO.getCountry());
		}
		
		address.setPhoneNumber( addressDTO.getPhone());	
		
		return address;
	}
	
	public boolean isValidShippingAddressFromOrder(Order order){
		boolean flag = false;
		if (!getOrderManager().getShippingGroupManager().isAnyHardgoodShippingGroups(order)) {
			return flag;
		}
		@SuppressWarnings("unchecked")
		Collection<ShippingGroup> shippingGroups = order.getShippingGroups();
		for(ShippingGroup sg:shippingGroups){
			if (sg instanceof HardgoodShippingGroup) {
				HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sg;
				String shipType = (String) hgSg.getPropertyValue(ShippingGroupPropertyManager.SHIP_TYPE.getValue());
				
				if (shipType == null || shipType.equalsIgnoreCase(ShipType.SHIP.getValue())) {
					ContactInfo shippingAddress = (ContactInfo)hgSg.getShippingAddress();
					if(!getAddressUtil().isEmptyAddress(shippingAddress)){
						flag=true;
						break;
					}
				}
			}
		}
		
		return flag;
	}
}
