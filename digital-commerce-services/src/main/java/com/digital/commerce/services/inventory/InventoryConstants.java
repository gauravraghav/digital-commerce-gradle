/**
 * 
 */
package com.digital.commerce.services.inventory;

/** @author djboyd */
public class InventoryConstants {
	public static final int		INSTOCK								= 1000;
	public static final int		OUTOFSTOCK							= 1001;
	public static final int		BACKORDERABLE						= 1003;
	public static final int		DISCONTINUED						= 1005;
	public static final int		DERIVED								= 1004;
	public static final int		NO_INVENTORY_RECORD					= 1006;

	// atg table
	public static final String	INVENTORY_PROPERTY					= "inventory";
	public static final String	CATALOG_PROPERTY					= "catalogRefId";
	public static final String	AVAIL_STATUS_PROPERTY				= "availabilityStatus";
	public static final String	AVAIL_DATE							= "availabilityDate";
	public static final String	STOCK_LEVEL_PROPERTY				= "stockLevel";
	public static final String	BACK_ORDER_LEVEL_PROPERTY			= "backorderLevel";
	public static final String	PRE_ORDER_LEVEL_PROPERTY			= "preorderLevel";
	public static final String	STOCK_THRESHOLD_PROPERTY			= "stockThreshold";
	public static final String	BACK_ORDER_THRESHOLD_PROPERTY		= "backorderThreshold";
	public static final String	PRE_ORDER_THRESHOLD_PROPERTY		= "preorderThreshold";
	public static final String	DISPLAY_NAME_PROPERTY				= "displayName";

	// dsw aux table
	public static final String	UNIT_OF_MEASURE_PROPERTY			= "unitOfMeasure";
	public static final String	ALERT_LEVEL_PROPERTY				= "alertLevel";
	public static final String	ALERT_QUANTITY_PROPERTY				= "alertQuantity";
	public static final String	AVAILABILITY_STATUS_MSG_PROPERTY	= "availabilityStatusMsg";
	public static final String	AVAILABILITY_STATUS_CODE_PROPERTY	= "availablityStatusCode";
	public static final String	LAST_PO_DATE_PROPERTY				= "lastPODate";
	public static final String	MONITOR_OPTION_PROPERTY				= "monitorOption";
	public static final String	ORGANIZATION_CODE_PROPERTY			= "organizationCode";
	public static final String	PO_DATE_PROPERTY					= "poDate";
	public static final String	FC_STOCK_LEVEL_PROPERTY				= "fcStockLevel";
	public static final String	STORE_STOCK_LEVEL_PROPERTY			= "storeStockLevel";
	public static final String	DROPSHIP_STOCK_LEVEL_PROPERTY		= "dropshipStockLevel";

}
