package com.digital.commerce.services.utils;

import java.util.Map;

import atg.commerce.util.PlaceList;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalPlaceList extends PlaceList {
	private String	country;
	private Map<String,String>		states;
}
