/**
 * 
 */
package com.digital.commerce.services.order.payment.giftcard.processor;

import com.digital.commerce.services.order.payment.giftcard.GiftCardInfo;
import com.digital.commerce.services.order.payment.giftcard.GiftCardStatus;

import atg.commerce.CommerceException;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.commerce.payment.processor.ProcProcessPaymentGroup;
import atg.payment.PaymentStatus;
import lombok.Getter;
import lombok.Setter;

/** This pipeline processor element is called to authorize, debit, and credit
 * GiftCard payment groups. It calls through to a GiftCardProcessor object to
 * perform these operations.
 * 
*/
@Getter
@Setter
public class ProcessGiftCard extends ProcProcessPaymentGroup {

	private GiftCardProcessor	giftCardProcessor;

	public ProcessGiftCard() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see atg.commerce.payment.processor.ProcProcessPaymentGroup#authorizePaymentGroup(atg.commerce.payment.PaymentManagerPipelineArgs) */
	public PaymentStatus authorizePaymentGroup( PaymentManagerPipelineArgs pParams ) throws CommerceException {
		if( isLoggingDebug() ) {
			logDebug( "ProcProcessGiftCard: authorizePaymentGroup(PaymentManagerPipelineArgs pParams=" + pParams + ") - start" ); //$NON-NLS-1$ //$NON-NLS-2$
		}

		PaymentStatus returnPaymentStatus = getGiftCardProcessor().authorize( (GiftCardInfo)pParams.getPaymentInfo() );

		if( isLoggingDebug() ) {
			logDebug( "ProcProcessGiftCard: authorizePaymentGroup(PaymentManagerPipelineArgs) - end" ); //$NON-NLS-1$
		}
		return returnPaymentStatus;
	}

	/* (non-Javadoc)
	 * @see atg.commerce.payment.processor.ProcProcessPaymentGroup#creditPaymentGroup(atg.commerce.payment.PaymentManagerPipelineArgs) */
	public PaymentStatus creditPaymentGroup( PaymentManagerPipelineArgs pParams ) throws CommerceException {
		if( isLoggingDebug() ) {
			logDebug( "creditPaymentGroup(PaymentManagerPipelineArgs pParams=" + pParams + ") - start" ); //$NON-NLS-1$ //$NON-NLS-2$
		}

		PaymentStatus returnPaymentStatus = getGiftCardProcessor().credit( (GiftCardInfo)pParams.getPaymentInfo() );

		if( isLoggingDebug() ) {
			logDebug( "creditPaymentGroup(PaymentManagerPipelineArgs) - end" ); //$NON-NLS-1$
		}
		return returnPaymentStatus;
	}

	/* (non-Javadoc)
	 * @see atg.commerce.payment.processor.ProcProcessPaymentGroup#debitPaymentGroup(atg.commerce.payment.PaymentManagerPipelineArgs) */
	public PaymentStatus debitPaymentGroup( PaymentManagerPipelineArgs pParams ) throws CommerceException {
		if( isLoggingDebug() ) {
			logDebug( "debitPaymentGroup(PaymentManagerPipelineArgs pParams=" + pParams + ") - start" ); //$NON-NLS-1$ //$NON-NLS-2$
		}

		PaymentStatus returnPaymentStatus = getGiftCardProcessor().debit( (GiftCardInfo)pParams.getPaymentInfo(), (GiftCardStatus)pParams.getPaymentManager().getLastAuthorizationStatus( pParams.getPaymentGroup() ) );

		if( isLoggingDebug() ) {
			logDebug( "debitPaymentGroup(PaymentManagerPipelineArgs) - end" ); //$NON-NLS-1$
		}
		return returnPaymentStatus;
	}
}
