package com.digital.commerce.services.coupons;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;

import atg.commerce.promotion.PromotionTools;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.collections.filter.CachedCollectionFilter;
import atg.service.collections.filter.FilterException;
import atg.service.collections.filter.ValidatorFilter;
import atg.service.util.CurrentDate;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"unchecked"})
@Getter
@Setter
public class DigitalCouponService extends GenericService {

	private Repository claimableRepository;
	private PromotionTools promotionTools;
	private RepositoryItemDescriptor[] claimableItemDescriptors;
	private RqlStatement couponsQuery;
	private Collection<?> allCoupons;
	private CachedCollectionFilter filter;
	private ValidatorFilter displayDateFilter;
	private String[] couponItemTypes;
	private CurrentDate currentDate;
	private Hashtable<String, Object> error;

	/**
	 * 
	 * @param pCouponItemTypes
	 */
	public void setCouponItemTypes(String[] pCouponItemTypes)
	{
	  this.couponItemTypes = pCouponItemTypes;
	  initializeCouponItemDescriptors();
	}

	public void setClaimableRepository(Repository mClaimableRepository) {
		this.claimableRepository = mClaimableRepository;
		initializeCouponItemDescriptors();
	}
	
	/**
	 * 
	 * @throws ServiceException
	 */
	public void doStartService()
			  throws ServiceException
	{
	  String METHOD_NAME = "doStartService";
	  try
	  {
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(getName(), METHOD_NAME);
	    super.doStartService();
	    initializeCouponItemDescriptors();
	    return;
	  }
	  finally {
		 DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(getName(), METHOD_NAME);
	  }
	}
	
	/**
	 * Find All Valid Coupons
	 * 
	 * @return
	 * @throws FilterException
	 */
	public Collection<?> findAllCoupons(boolean filterByDisplayDate) throws FilterException{
		String METHOD_NAME = "findAllCoupons";		
		try
		{
		  DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(getName(), METHOD_NAME);	
		  vlogDebug("Entered findAllCoupons", new Object[0]);
		  
		  initalizeSiteGroupCoupons(filterByDisplayDate);
		  
		  if (isLoggingDebug()) {
		    logDebug("Leaving findAllCoupons with: " + allCoupons);
		  }
		}
		catch (Exception ex) {
			this.getJSONError(ex.getMessage());
		}
		finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(getName(), METHOD_NAME);
		}
		
		return this.allCoupons;
	}
	
	/**
	 * Initialize the SiteGroup related coupons
	 * 
	 * @throws FilterException
	 */
	private void initalizeSiteGroupCoupons(boolean filterByDisplayDate) throws FilterException {

		String METHOD_NAME = "initalizeSiteGroupOffers";

		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(getName(), METHOD_NAME);
			if (this.allCoupons == null) {
				this.allCoupons = new HashSet<RepositoryItem>();
			}
			this.allCoupons.clear();

			List<RepositoryItem> allCoupons = new ArrayList<>();

			if (isLoggingDebug()) {
				logDebug("ClaimableRepository=" + getClaimableRepository() + "; ClaimableItemDescriptors="
						+ this.claimableItemDescriptors + "; CouponsQuery='" + getCouponsQuery() + "'");
			}

			int length;
			if (getClaimableRepository() != null && this.claimableItemDescriptors != null
					&& getCouponsQuery() != null) {

				length = this.claimableItemDescriptors.length;
				for (int c = 0; c < length; c++) {
					RepositoryView view = null;
					if (this.claimableItemDescriptors[c] != null) {
						if ((this.claimableItemDescriptors[c] instanceof RepositoryView)) {
							view = (RepositoryView) this.claimableItemDescriptors[c];
						} else {
							try {
								String name = this.claimableItemDescriptors[c].getItemDescriptorName();
								view = getClaimableRepository().getView(name);
							} catch (RepositoryException exc) {
								if (isLoggingError()) {
									logError(exc);
								}
							}
						}
						if (view == null) {
							if (isLoggingDebug()) {
								logDebug("Searching RepositoryView 'null'");
							}
						} else {
							if (isLoggingDebug()) {
								logDebug("Searching RepositoryView '" + view.getViewName() + "'");
							}
							try {
								if (isLoggingDebug()) {
									logDebug("Coupons Query='" + getCouponsQuery() + "'");
								}
								Timestamp queryTimestamp = getPromotionTools().getCurrentDate().getTimeAsTimestamp();

								if (isLoggingDebug()) {
									logDebug("Using timestamp " + queryTimestamp + " for Coupons query");
								}
								Object[] params = { queryTimestamp };
								RepositoryItem[] couponArray = getCouponsQuery().executeQuery(view, params);

								if (couponArray != null) {
									List<RepositoryItem> coupons = Arrays.asList(couponArray);

									if (!filterByDisplayDate) {
										allCoupons.addAll(coupons);
									} else {
										Collection<RepositoryItem> couponItem = getDisplayDateFilter()
												.filterCollection(coupons, null, null);
										if (couponItem != null && !couponItem.isEmpty()) {
											allCoupons.addAll(couponItem);
										}
									}
								}

							} catch (RepositoryException e) {
								if (isLoggingError())
									logError(e);
							}
						}
					}
				}
			}

			// Check that there are Offers to filter
			if (allCoupons == null || allCoupons.isEmpty()) {
				return;
			}

			// Filter all Offers using filter parameter
			this.allCoupons = getFilter().filterCollection(allCoupons, null, null);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(getName(), METHOD_NAME);
		}
	}
	
	/**
	 * Initialize the Claimable Repository related item descriptors (e.g. DeployablePromotionClaimable)
	 * 
	 */
	private void initializeCouponItemDescriptors() {
	  String METHOD_NAME = "initializeCouponItemDescriptors";
	  
	  try
	  {
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(getName(), METHOD_NAME);
	    if (getClaimableRepository() != null) {
	      int length = getCouponItemTypes().length;
	      this.claimableItemDescriptors = new RepositoryItemDescriptor[length];
	      for (int c = 0; c < length; c++) {
	        try {
	          String name = getCouponItemTypes()[c].trim();
	          this.claimableItemDescriptors[c] = getClaimableRepository().getItemDescriptor(name);
	        }
	        catch (RepositoryException exc) {
	          if (isLoggingError()) {
	            logError(exc);
	          }
	        }
	      }
	    } else {
	      this.claimableItemDescriptors = null;
	    }
	    return;
	  }
	  finally {
		  DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(getName(), METHOD_NAME);
	  }
	}

	/**
	 * 
	 * @param errorMesage
	 */
	private void getJSONError(String errorMesage) {
		error = new Hashtable<>();
		Hashtable<String, String> genError = new Hashtable<>();
		if (DigitalStringUtil.isNotBlank(errorMesage)) {
			genError.put("localizedMessage", errorMesage);
		} else {
			genError.put("localizedMessage",
					"Generic error while lookuo of all available offers");
		}
		genError.put("errorCode", "COUPON_LOOKUP_GENERIC_ERROR");
		ArrayList<Hashtable<String, String>> genErrArray = new ArrayList<>();
		genErrArray.add(genError);
		error.put("genericExceptions", genErrArray);
		error.put("formError", false);
	}
}
