package com.digital.commerce.services.pricing.calculators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import atg.commerce.pricing.BandedDiscountCalculatorHelper;
import atg.commerce.pricing.CalculatorInfo;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.DiscountAttributeInfo;
import atg.commerce.pricing.PricingContext;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.QualifiedItem;
import atg.commerce.pricing.Qualifier;
import atg.commerce.pricing.definition.DiscountDetail;
import atg.core.util.Range;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 *
 */
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalBandedOrderQtyDiscountCalculatorHelper extends
		BandedDiscountCalculatorHelper {
	
	Qualifier qualifierService;
	
	public CalculatorInfo getCalculatorInfo(String pCalculatorType) {
		if (isLoggingDebug()) {
			logDebug("Entered getCalculatorInfo");
			logDebug("pCalculatorType: " + pCalculatorType);
		}

		CalculatorInfo info = new CalculatorInfo(pCalculatorType);

		DiscountAttributeInfo[] structAtts = new DiscountAttributeInfo[2];
		structAtts[0] = new DiscountAttributeInfo("bandingProperty");
		structAtts[0].setDataType(String.class);
		structAtts[0].setRequired(false);

		structAtts[1] = new DiscountAttributeInfo("bandingPropertyScope");
		structAtts[1].setDataType(String.class);
		structAtts[1].setEnumeratedValues(new String[] { "QualifiedItem",
				"DetailedItemPriceInfo" });

		structAtts[1].setRequired(false);

		info.setDiscountStructureAttributeInfos(structAtts);

		DiscountAttributeInfo[] atts = new DiscountAttributeInfo[2];
		atts[0] = new DiscountAttributeInfo("quantity");
		atts[0].setDataType(Number.class);

		atts[1] = new DiscountAttributeInfo("adjuster");
		atts[1].setDataType(Double.class);

		info.setDiscountDetailAttributeInfos(atts);

		CalculatorInfo.DiscountDetailInfo[] detailsInfos = new CalculatorInfo.DiscountDetailInfo[1];
		detailsInfos[0] = info.createDiscountDetailInfo();
		detailsInfos[0].setDetailAttributes(atts);

		if (isLoggingDebug()) {
			logDebug("Leaving with info: " + info);
		}

		return info;
	}

	protected List<Band> determineBands(List<DiscountDetail> pDetails) throws PricingException {
		Map<String, Object> attributes = null;
		String bandString = null;
		String adjusterString = null;
		Band band = null;
		List<Band> bands = new ArrayList();

		if (isLoggingDebug()) {
			logDebug("Entered determineBands");
			if (pDetails != null) {
				logDebug("pDetails:");
				for (DiscountDetail detail : pDetails) {
					logDebug(detail.toString());
				}
			}
		}

		if (pDetails == null) {
			throw new PricingException(Constants.NO_DETAILS_IN_DISCOUNT);
		}

		for (DiscountDetail detail : pDetails) {
			attributes = detail.getAttributes();
			if (attributes != null) {

				bandString = (String) attributes.get("quantity");
				if (bandString != null) {
					adjusterString = (String) attributes.get("adjuster");
					if (adjusterString == null) {
						throw new PricingException(Constants.DISCOUNT_DETAIL_NO_ADJUSTER);
					}
					try {
						band = new Band(Double.parseDouble(bandString),
								Double.parseDouble(adjusterString));
					} catch (NumberFormatException nfe) {
						throw new PricingException(Constants.BAND_OR_ADJUSTER_NOT_A_DOUBLE);
					}

					bands.add(band);
				}
			}
		}
		if (bands.size() <= 0) {
			throw new PricingException(Constants.NO_DISCOUNT_BANDS);
		}
		Collections.sort(bands);

		if (isLoggingDebug()) {
			logDebug("Leaving with bands:");
			if (bands != null) {
				for (Band bandObj : bands) {
					logDebug(bandObj.toString());
				}
			}
		}

		return bands;
	}
	
	private long findNumberOfItems(PricingContext pricingContext, Collection pQualifiedItems) throws PricingException {
		long totalQuant = 0;
		
		QualifiedItem item = null;
		long quantity = 0L;
		Map.Entry entry = null;
		List markedRanges = null;
		boolean dipiScoped = false;
		double bandingValue = 0.0D;

		for (Object obj : pQualifiedItems) {
			item = (QualifiedItem) obj;
			quantity = 0L;

			for (Object entryObj : item.getQualifyingDetailsMap().entrySet()) {
				entry = (Map.Entry) entryObj;
				markedRanges = (List) entry.getValue();
				if (markedRanges != null) {
					for (Object rangeObj : markedRanges) {
						quantity += ((Range) rangeObj).getSize();
					}
				}

				if (dipiScoped) {

					bandingValue += quantity;
					quantity = 0L;
				}
			}

			if (!dipiScoped) {
				bandingValue += item.getItem().getQuantity(); 
			}
			
			totalQuant = (long)bandingValue;
		}
		return totalQuant;
	}
	
	
	@Override
	protected double calculateBandingValue(java.lang.String pBandingPropertyScope,
            java.util.Map<java.lang.String,java.lang.Object> pObjectBindings,
            java.util.Collection pQualifiedItems)
     throws PricingException{
		
		double bandingValue = 0.0D;
		if (isLoggingDebug()) {
			logDebug("Entered calculateBandingValue");
			logDebug("pBandingPropertyScope: " + pBandingPropertyScope);
			logDebug("pObjectBindings: " + pObjectBindings);
			if (pQualifiedItems != null) {
				logDebug("pQualifiedItems:");
				for (Object obj : pQualifiedItems) {
					logDebug(obj.toString());
				}
			}
		}
		
		PricingContext pricingContext = (PricingContext)pObjectBindings.get("bandingBean");
		if(pricingContext != null){
			long numberOfItems = findNumberOfItems(pricingContext, pQualifiedItems);
			if(numberOfItems > 0){
				bandingValue = numberOfItems * 1.00;
			}
		}
		
		if (isLoggingDebug()) {
			logDebug("Leaving with bandingValue: " + bandingValue);
		}
		
		return bandingValue;
	}
}
