/**
 * 
 */
package com.digital.commerce.services.payment;

import com.digital.commerce.services.order.DigitalCreditCard;
import com.digital.commerce.services.order.payment.creditcard.DigitalCreditCardInfo;

import atg.commerce.order.CreditCard;
import atg.commerce.order.Order;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.commerce.payment.processor.ProcCreateCreditCardInfo;
import atg.payment.creditcard.GenericCreditCardInfo;

/** @author wibrahim */
public class DigitalProcCreateCreditCardInfo extends ProcCreateCreditCardInfo {

	protected void addDataToCreditCardInfo( Order pOrder, CreditCard pPaymentGroup, double pAmount, PaymentManagerPipelineArgs pParams, GenericCreditCardInfo pCreditCardInfo ) {
		super.addDataToCreditCardInfo( pOrder, pPaymentGroup, pAmount, pParams, pCreditCardInfo );
		DigitalCreditCardInfo ccInfo = (DigitalCreditCardInfo)pCreditCardInfo;
		DigitalCreditCard creditCard = (DigitalCreditCard)pPaymentGroup;
		ccInfo.setNameOnCard( creditCard.getNameOnCard() );
		ccInfo.setIpAddress( creditCard.getIpAddress() );
		ccInfo.setPaypageRegistrationId( creditCard.getPaypageRegistrationId() );
		ccInfo.setTokenValue( creditCard.getTokenValue() );
		ccInfo.setVantivTransId(creditCard.getVantivTransId());
		ccInfo.setCvvNumber(creditCard.getCvvNumber());
		ccInfo.setCardBeingSavedToProfile(creditCard.isCardBeingSavedToProfile());
	}

}
