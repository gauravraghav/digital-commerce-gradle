package com.digital.commerce.services.pricing.reprice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import lombok.Getter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.digital.commerce.common.util.DigitalStringUtil;

/*  */
@SuppressWarnings({"rawtypes","unchecked"})
public class ExchangeOrdersHandler extends DefaultHandler {
	private final String	EXCHANGEORDER		= "ExchangeOrder";
	private final String	PREVIOUSEXCHANGE	= "PreviousExchange";
	private final String	RETURNORDERID		= "returnOrderID";
	private final String	TIMESTAMP			= "timestamp";
	private final String	ORDERLINEKEY		= "orderLineKey";
	private final String	LINEITEM			= "lineItem";
	private final String	LINEITEMID			= "lineItemId";
	private final String	RETURNLINEITEMID	= "returnedLineItemId";
	private final String	QUANTITY			= "quantity";
	private final String	SHIPPINGGROUPID		= "shippingGroupID";
	private final String	EVENEXCHANGE		= "evenExchange";
	private Calendar		calendar			= Calendar.getInstance();

	@Getter
	private List			exchangeOrders		= new ArrayList( 10 );
	private String			exchangeOrder		= "";
	private String			returnOrderId		= "";
	private String			timestamp			= "";
	private String			lineItem			= "";
	private String			orderLineKey		= "";
	private String			lineItemId			= "";
	private String			returnLineItemId	= "";
	private String			quantity			= "";
	private String			shippingGroupId		= "";
	private String			evenExchange		= "";

	ExchangeItem			ei					= null;
	String					returnOrderIdValue	= null;

	private StringBuffer	characterBuf;

	private String			errMsg				= null;

	public String getErrorMessage() {
		return errMsg;
	}


	/** Receive notification of the start of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @param atts - The attributes attached to the element. If there are no attributes, it shall be an empty Attributes object.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void startElement( String namespaceURI, String localName, String qName, Attributes atts ) {
		characterBuf = new StringBuffer();

		try {

			if( EXCHANGEORDER.equals( localName ) || PREVIOUSEXCHANGE.equals( localName ) ) {
				exchangeOrder = EXCHANGEORDER;
			}
			if( DigitalStringUtil.isNotBlank(exchangeOrder) ) {
				if( RETURNORDERID.equals( localName ) ) {
					returnOrderId = RETURNORDERID;
				}
				if( LINEITEM.equals( localName ) ) {
					lineItem = LINEITEM;
				}
			}

			if( DigitalStringUtil.isNotBlank(exchangeOrder) && DigitalStringUtil.isNotBlank(lineItem) ) {
				if( ei == null ) ei = new ExchangeItem();

				if( LINEITEMID.equals( localName ) ) {
					lineItemId = LINEITEMID;
				}
				if( TIMESTAMP.equals( localName ) ) {
					timestamp = TIMESTAMP;
				}
				if( ORDERLINEKEY.equals( localName ) ) {
					orderLineKey = ORDERLINEKEY;
				}
				if( RETURNLINEITEMID.equals( localName ) ) {
					returnLineItemId = RETURNLINEITEMID;
				}
				if( QUANTITY.equals( localName ) ) {
					quantity = QUANTITY;
				}
				if( SHIPPINGGROUPID.equals( localName ) ) {
					shippingGroupId = SHIPPINGGROUPID;
				}
				if( EVENEXCHANGE.equals( localName ) ) {
					evenExchange = EVENEXCHANGE;
				}
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}

	/** Receive notification of character data inside an element.
	 * 
	 * @param ch - The characters.
	 * @param start - The start position in the character array.
	 * @param length - The number of characters to use from the character array.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void characters( char[] ch, int start, int length ) {
		characterBuf.append( ch, start, length );
	}

	private long format( String value ) {
		int year = Integer.parseInt( value.substring( 0, 4 ) );
		int month = Integer.parseInt( value.substring( 5, 7 ) );
		int date = Integer.parseInt( value.substring( 8, 10 ) );
		int hour = Integer.parseInt( value.substring( 11, 13 ) );
		int minute = Integer.parseInt( value.substring( 14, 16 ) );
		int second = Integer.parseInt( value.substring( 17 ) );

		calendar.set( year, month, date, hour, minute, second );
		return calendar.getTimeInMillis();
	}

	/** Receive notification of the end of an element.
	 * 
	 * @param namespaceURI - The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing is not
	 *            being performed.
	 * @param localName - The local name (without prefix), or the empty string if Namespace processing is not being performed.
	 * @param qName - The qualified name (with prefix), or the empty string if qualified names are not available.
	 * @throws SAXException - Any SAX exception, possibly wrapping another exception. */
	public void endElement( String namespaceURI, String localName, String qName ) {
		populateValue();
		try {
			if( DigitalStringUtil.isNotBlank(exchangeOrder) && DigitalStringUtil.isNotBlank(lineItem) && LINEITEM.equals( localName ) ) {
				if( ei != null ) {
					ei.setType( "Exchange" );
					exchangeOrders.add( ei );
				}

				ei = null;
				lineItem = "";
				lineItemId = "";
				timestamp = "";
				orderLineKey = "";
				returnLineItemId = "";
				quantity = "";
				shippingGroupId = "";
				evenExchange = "";
				lineItem = "";
			}

			if( EXCHANGEORDER.equals( localName ) || PREVIOUSEXCHANGE.equals( localName ) ) {
				returnOrderId = "";
				exchangeOrder = "";
				returnOrderIdValue = null;
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}

	private void populateValue() {
		try {
			String value = characterBuf.toString();

			if( DigitalStringUtil.isNotBlank(exchangeOrder) && DigitalStringUtil.isNotBlank(returnOrderId)) {
				returnOrderIdValue = value;
				returnOrderId = "";
			}

			if( DigitalStringUtil.isNotBlank(exchangeOrder) && DigitalStringUtil.isNotBlank(lineItem) ) {
				if( DigitalStringUtil.isNotBlank(lineItemId) ) {
					ei.setLineItemId( value );
					lineItemId = "";
				}
				if( DigitalStringUtil.isNotBlank(returnLineItemId) ) {
					ei.setReturnedLineItemId( value );
					returnLineItemId = "";
				}
				if( DigitalStringUtil.isNotBlank(timestamp) ) {
					ei.setTimestamp( format( value ) );
					timestamp = "";
				}
				if( DigitalStringUtil.isNotBlank(orderLineKey) ) {
					ei.setOrderLineKey( value );
					orderLineKey = "";
				}
				if( DigitalStringUtil.isNotBlank(quantity) ) {
					ei.setQuantity( Integer.parseInt( value ) );
					quantity = "";
				}
				if( DigitalStringUtil.isNotBlank(shippingGroupId)) {
					ei.setShippingGroupId( value );
					shippingGroupId = "";
				}
				if( DigitalStringUtil.isNotBlank(evenExchange) ) {
					if( value.trim().equalsIgnoreCase( "true" ) )
						ei.setEvenExchange( true );
					else
						ei.setEvenExchange( false );
					evenExchange = "";
				}
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}
}
