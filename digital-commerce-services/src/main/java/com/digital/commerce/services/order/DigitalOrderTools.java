/**
 * 
 */
package com.digital.commerce.services.order;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.pricing.DetailedItemPriceInfo;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.commerce.promotion.PromotionTools;
import atg.commerce.states.OrderStates;
import atg.commerce.states.StateDefinitions;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.multisite.SiteManager;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.RunProcessException;
import atg.servlet.ServletUtil;

import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.RequestHeaderAttributesConstant;
import com.digital.commerce.integration.reward.domain.BillToItem;
import com.digital.commerce.integration.reward.domain.DiscountMarkDownCodes;
import com.digital.commerce.integration.reward.domain.ItemsItem;
import com.digital.commerce.integration.reward.domain.RewardsSendOrderData;
import com.digital.commerce.integration.reward.domain.ShipToItem;
import com.digital.commerce.integration.reward.domain.TenderItem;
import com.digital.commerce.integration.reward.domain.api.BillTo;
import com.digital.commerce.integration.reward.domain.api.DiscountMarkDownCodesItem;
import com.digital.commerce.integration.reward.domain.api.Items;
import com.digital.commerce.integration.reward.domain.api.RewardsSendOrderRequest;
import com.digital.commerce.integration.reward.domain.api.ShipTo;
import com.digital.commerce.integration.reward.domain.api.Tender;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.creditcard.DigitalCreditCardStatus;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.order.shipping.ShippingMethod;
import com.digital.commerce.services.promotion.DigitalPromotionTools;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalOrderTools extends OrderTools {
	protected String		giftCardPaymentGroupName	= "giftCard";
	private double			whiteGloveOrderThreshold;

	private PromotionTools	promotionTools;

	private String			syncChain					= "syncInventoryReservation";

	private SiteManager		siteManager;
	
	private DigitalServiceConstants	dswConstants;
	
	private Integer businessUnitId;
	private Integer locationNumber;
	private static final String SEND_ORDER_DATE_FORMAT="yyyy-MM-dd'T'HH:mm:ss.SSS";
	private static final DateFormat sdf = new SimpleDateFormat(SEND_ORDER_DATE_FORMAT);
	private static final String BOSTS_PROPERTY="BOSTS";
    private static final String BOPIS_PROPERTY="BOPIS";
    
    private DigitalShippingGroupManager dswShippingGroupManager;

	private static final DigitalLogger	logger	= DigitalLogger.getLogger( DigitalOrderTools.class );

	public BigDecimal getWhiteGloveOrderThreshold() {
		// Fix for startup error related to loading BigDecimal values
		return BigDecimal.valueOf( whiteGloveOrderThreshold );
		// return whiteGloveOrderThreshold;
	}

	private DigitalPaymentGroupManager	paymentGroupManager	= new DigitalPaymentGroupManager();

	/**
     * 
     */
	public DigitalOrderTools() {
		super();
	}

	public boolean determineWhiteGloveOrder( Order order ) {
		boolean retVal = false;
		if( order instanceof DigitalOrderImpl ) {
			boolean whiteGlove = ( (DigitalOrderImpl)order ).isOrderContainsWhiteGloveItem();
			boolean luxury = ( (DigitalOrderImpl)order ).isOrderContainsLuxuryItem();
			BigDecimal orderSubTotal = new BigDecimal( ( (DigitalOrderImpl)order ).getPriceInfo().getRawSubtotal() ).setScale( 2, RoundingMode.HALF_UP );
			BigDecimal orderThreshold = this.getWhiteGloveOrderThreshold().setScale( 2, RoundingMode.HALF_UP );
			int compareValue = orderSubTotal.compareTo( orderThreshold );
			if( whiteGlove || ( compareValue >= 0 && luxury ) ) retVal = true;
			( (DigitalOrderImpl)order ).setWhiteGloveOrder( retVal );
		}
		return retVal;
	}

	public boolean determineMultipleShipmentOrder( Order order ) {
		boolean retVal = false;
		if( order instanceof DigitalOrderImpl ) {
			retVal = ( (DigitalOrderImpl)order ).isOrderContainsMultipleShipNodes();
			( (DigitalOrderImpl)order ).setMultipleShipmentOrder( retVal );
		}
		return retVal;
	}

	public double getTotalSavings( Order order ) {
		double total = 0;
		List commerceItems = order.getCommerceItems();
		double basePriceSaved = 0.0;
		for( int x = 0; x < commerceItems.size(); x++ ) {
			CommerceItem ci = (CommerceItem)commerceItems.get( x );
			RepositoryItem sku = (RepositoryItem)ci.getAuxiliaryData().getCatalogRef();
			Object basePriceField = sku.getPropertyValue( "msrp" );
			if( basePriceField != null ) {
				double basePrice = ( (Double)basePriceField ).doubleValue() * ci.getQuantity();
				if( ( ci.getPriceInfo() != null ) ) {
					if( basePrice > ci.getPriceInfo().getRawTotalPrice() ) {
						basePriceSaved -= basePrice - ci.getPriceInfo().getRawTotalPrice();
					}
				}
			}
		}

		DigitalPromotionTools pTools = (DigitalPromotionTools)getPromotionTools();
		List promos = pTools.getAllPromotions( order, true );
		double youSaved = 0.0;
		for( int x = 0; x < promos.size(); x++ ) {
			PricingAdjustment pAdj = (PricingAdjustment)promos.get( x );

			youSaved += pAdj.getTotalAdjustment();
		}
		youSaved += basePriceSaved;
		total = Math.abs( youSaved );
		return total;
	}

	/** Calculates the order total = item total+shipping+tax */

	public double calculateOrderTotal( Order pOrder ) {
		DigitalOrderImpl order = (DigitalOrderImpl)pOrder;
		double orderTotal = 0.0;
		double startingTotal = order.getPriceInfo() == null ? 0 : order.getPriceInfo().getAmount();
		orderTotal = getPaymentGroupManager().getAmountRemaining( order, startingTotal );
		// Add Shipping
		orderTotal += order.getTotalShippingCost();
		orderTotal += order.getPriceInfo() == null ? 0 : order.getPriceInfo().getTax();
		orderTotal += order.getPriceInfo() == null ? 0 : order.getPriceInfo().getManualAdjustmentTotal();
		return orderTotal;
	}

	@Override
	public ShippingGroup createShippingGroup( String pType, ShippingPriceInfo pShippingPriceInfo ) throws CommerceException {
		ShippingGroup sg = super.createShippingGroup( pType, pShippingPriceInfo );
		sg.setShippingMethod( ShippingMethod.getDefaultShippingMethod().getMappedValue() );
		if( sg instanceof HardgoodShippingGroup ) {
			HardgoodShippingGroup hsg = (HardgoodShippingGroup)sg;
			hsg.setPropertyValue( ShippingGroupConstants.ShippingGroupPropertyManager.SHIP_TYPE.getValue(), ShippingGroupConstants.ShipType.SHIP.getValue() );
		}

		return sg;
	}

	/**
	 *
	 * @param pOrder
	 */
	public void determineOriginOfOrder( Order pOrder ) {

		String originOfOrder = OriginOfOrder.Default.getValue();

		String deviceName = ServletUtil.getCurrentRequest().getHeader(
				RequestHeaderAttributesConstant.AKAMAI_DEVICE_KEY.getValue());

		if(DigitalStringUtil.isNotBlank(deviceName)) {
			if(!"desktop".equalsIgnoreCase(deviceName)){
				originOfOrder = deviceName;
			}
		}
		
		//CO:335 SET SOURCE OF ORDER AS contactCenter if its PATool
		if(getDswConstants().isPaTool()){
			originOfOrder="contactCenter";
		}
		DigitalOrderImpl order = (DigitalOrderImpl)pOrder;
		order.setOriginOfOrder( originOfOrder );
	}

	public ContactInfo getOrderBillingAddress( Order order ) throws CommerceException {
		ContactInfo billingAddr = null;
		
		List<PaymentGroup> paymentGroupList = order.getPaymentGroups();
		if( paymentGroupList != null ) {
			for( PaymentGroup pg : paymentGroupList ) {
				if( pg instanceof DigitalCreditCard ) {
					DigitalCreditCard creditCard = (DigitalCreditCard)pg;
					billingAddr = (ContactInfo)creditCard.getBillingAddress();
				} else if( pg instanceof PaypalPayment ) {
					PaypalPayment paypalPayment = (PaypalPayment)pg;
					billingAddr = (ContactInfo)paypalPayment.getBillingAddress();
				}
			}
		}
		return billingAddr;
	}

	public void syncOrderItems( Order pOrder ) throws RunProcessException {
		Map params = new HashMap();
		params.put( "Order", pOrder );
		this.getPipelineManager().runProcess( this.syncChain, params );
	}

	public double getRewardsShareTotal( DigitalOrderImpl order ) {
		double orderCertTotal = 0.0;
		Set<RepositoryItem> rewardCerts = order.getRewardCertificates();
		for( RepositoryItem rewardCert : rewardCerts ) {
			String type = (String)rewardCert.getPropertyValue( "type" );
			if(DigitalBaseConstants.REWARD_CERT_TYPE.equalsIgnoreCase( type )||
					DigitalBaseConstants.REWARD_REWARD_TYPE.equalsIgnoreCase( type )) {
				if( rewardCert.getPropertyValue( "amount" ) != null ) {
					orderCertTotal += (double)rewardCert.getPropertyValue( "amount" );
				}
			}
		}
		return orderCertTotal;
	}

	/**
	 * @param orderPassed
	 * @return double
	 */
	public double getPromotionalSavings( DigitalOrderImpl orderPassed ) {
		double promoSavings = 0.0;
		promoSavings = ( orderPassed.getPriceInfo() != null ) ? ( orderPassed.getPriceInfo().getDiscountAmount() - getRewardsShareTotal( orderPassed ) ) : promoSavings;
		return promoSavings;
	}
		
	public double getTotalDiscountSavings(DigitalOrderImpl orderPassed) {
		double promoSavings = 0.0D;
		promoSavings =
				(orderPassed.getPriceInfo() != null) ? (
						orderPassed.getPriceInfo().getDiscountAmount() + getItemDiscountSavings(orderPassed)
								- getRewardsShareTotal(orderPassed)) : promoSavings;
		return promoSavings;
	}
	/**
	 * @param order
	 * @return double
	 */
	public double getItemDiscountSavings(DigitalOrderImpl order) {
		double totalItemDiscounts = 0.0D;
		List<CommerceItem> commerceItems = order.getCommerceItems();
		if (null != commerceItems && !commerceItems.isEmpty()) {
			for (CommerceItem ci : commerceItems) {
				totalItemDiscounts += getItemDiscountAmount(ci.getPriceInfo());
			}
		}
		return totalItemDiscounts;
	}

	/**
	 * @param itemPriceInfo
	 * @return double
	 */
	public double getItemDiscountAmount(ItemPriceInfo itemPriceInfo) {
		double discounts = itemPriceInfo.getRawTotalPrice() - itemPriceInfo.getAmount();
		return discounts < 0.0D ? 0.0D : discounts;
	}

	public double getGiftCardsTotal( DigitalOrderImpl order ) {
		List<PaymentGroup> paymentGroupList = order.getPaymentGroups();
		double giftCardAmount = 0;
		if( paymentGroupList != null ) {
			for( PaymentGroup pg : paymentGroupList ) {
				if( pg instanceof DigitalGiftCard ) {
					DigitalGiftCard giftCard = (DigitalGiftCard)pg;
					giftCardAmount = giftCardAmount + giftCard.getAmount();
				}
			}
		}
		return giftCardAmount;
	}

	public double getCreditCardPaypalTotal( DigitalOrderImpl order ) {
		List<PaymentGroup> paymentGroupList = order.getPaymentGroups();
		double ccAmount = 0;
		if( paymentGroupList != null ) {
			for( PaymentGroup pg : paymentGroupList ) {
				if( pg instanceof DigitalCreditCard ) {
					DigitalCreditCard cc = (DigitalCreditCard)pg;
					ccAmount = ccAmount + cc.getAmount();
				} else if( pg instanceof PaypalPayment ) {
					PaypalPayment cc = (PaypalPayment)pg;
					ccAmount = ccAmount + cc.getAmount();
				}
			}
		}
		return ccAmount;

	}

	public double getTotalSavingsWithoutPromotions(Order order) {
		double total = 0;
		List commerceItems = order.getCommerceItems();
		double basePriceSaved = 0.0;
		for (int x = 0; x < commerceItems.size(); x++) {
			CommerceItem ci = (CommerceItem) commerceItems.get(x);
			RepositoryItem sku = (RepositoryItem) ci.getAuxiliaryData().getCatalogRef();
			if (sku != null) {
				Object basePriceField = sku.getPropertyValue("msrp");
				if (basePriceField != null) {
					double basePrice = ((Double) basePriceField).doubleValue() * ci.getQuantity();
					if ((ci.getPriceInfo() != null)) {
						if (basePrice > ci.getPriceInfo().getRawTotalPrice()) {
							basePriceSaved -= basePrice - ci.getPriceInfo().getRawTotalPrice();
						}
					}
				}
			}
		}

		// double youSaved = 0.0;
		// youSaved += basePriceSaved;
		total = Math.abs(basePriceSaved);
		return total;
	}

	public boolean isBopisOrBostsOnlyOrder( Order order ) {
		boolean bopisBostsFound = false, nonBopisBostsFound = false;
		
		List<DigitalCommerceItem> commerceItems = order.getCommerceItems();
		for( DigitalCommerceItem commerceItem : commerceItems ) {
			String shipType = commerceItem.getShipType();
			if( ShippingGroupConstants.ShipType.BOPIS.getValue().equalsIgnoreCase( shipType ) || ShippingGroupConstants.ShipType.BOSTS.getValue().equalsIgnoreCase( shipType ) ) {
				bopisBostsFound = true;
			} else {
				nonBopisBostsFound = true;
			}
		}

		return ( bopisBostsFound && !nonBopisBostsFound );
	}

	public boolean isOrderIncompleteState( Order order ) {
		int orderState = StateDefinitions.ORDERSTATES.getStateValue( OrderStates.SUBMITTED );
		if( order != null && orderState != order.getState() ) {
			return true;
		} else {
			return false;
		}
	}

	private String	orderHistoryDescName	= "dsworderhistory";

	private String	orderHistoryItemName	= "dswCommerceItem";

	private String getEmailForOrder( RepositoryItem order ) {
		String emailAddress = "";
		try {
			
			List<RepositoryItem> paymentGroupList = (List<RepositoryItem>)order.getPropertyValue( "paymentGroups" );

			if( paymentGroupList != null ) {
				for( RepositoryItem pg : paymentGroupList ) {
					String type = (String)pg.getPropertyValue( "type" );
                    switch (type) {
                        case "creditCard":
                            emailAddress = (String) pg.getPropertyValue("email");
                            break;
                        case "paypalPayment":
                            if (isLoggingInfo()) {
                                emailAddress = (String) pg.getPropertyValue("paypalEmail");
                            }
                            break;
                        default:

                            List<RepositoryItem> shippingGroups = (List<RepositoryItem>) order.getPropertyValue("shippingGroups");
                            for (RepositoryItem sg : shippingGroups) {
                                if (((String) sg.getPropertyValue("type")).equals("hardgoodShippingGroup")) {
                                    emailAddress = (String) sg.getPropertyValue("email");
                                }
                            }
                            break;
                    }
				}
			}
		} catch( Exception e ) {
			logError( "Exception from validateOrderAndEmail:::", e );
		}
		return emailAddress;
	}

	
	private Map<String, RepositoryItem> getHardgoodShippingGroupRelations( RepositoryItem order ) {
		List<RepositoryItem> relationShips = (List<RepositoryItem>)order.getPropertyValue( "relationships" );
		Map<String, RepositoryItem> result = new HashMap<>();
		for( RepositoryItem rs : relationShips ) {
			if("shipItemRel".equals( rs.getPropertyValue( "type" ) ) ) {
				Object propertyValue = rs.getPropertyValue( "commerceItem" );
				if(null != propertyValue) {
					result.put(((RepositoryItem) propertyValue).getRepositoryId(), rs);
				}
			}
		}
		return result;
	}

	
	private List<RepositoryItem> getCommerceItems( RepositoryItem order ) {
    return (List<RepositoryItem>)order.getPropertyValue( "commerceItems" );

	}

	
	private RepositoryItem getOrderPrice( RepositoryItem order ) {
    return (RepositoryItem)order.getPropertyValue( "priceInfo" );

	}

	
	private String[] getNameOfShipment( RepositoryItem order, RepositoryItem item ) {
		Map<String, RepositoryItem> rels = getHardgoodShippingGroupRelations( order );
		String catalogRefId = (String)item.getPropertyValue( "catalogRefId" );
		String[] name = new String[3];
		if( rels.containsKey( item.getRepositoryId() ) ) {
			if( GiftCardType.ELECTRONIC.getValue().equalsIgnoreCase( catalogRefId ) 
					|| GiftCardType.PERSONALIZED.getValue().equalsIgnoreCase( catalogRefId )
							|| GiftCardType.STANDARD.getValue().equalsIgnoreCase( catalogRefId )) {
				name[0] = ( item.getPropertyValue( "recipientName" ) != null ) ? (String)item.getPropertyValue( "recipientName" ) : "";
				name[1] = ( item.getPropertyValue( "senderName" ) != null ) ? (String)item.getPropertyValue( "senderName" ) : "";
				name[2] = "giftcard";
			} else {
				name[0] = (String)( ( (RepositoryItem)( rels.get( item.getRepositoryId() ).getPropertyValue( "shippingGroup" ) ) ).getPropertyValue( "firstName" ) );
				name[1] = (String)( ( (RepositoryItem)( rels.get( item.getRepositoryId() ).getPropertyValue( "shippingGroup" ) ) ).getPropertyValue( "lastName" ) );
				name[2] = "none_giftcard";
			}
			return name;
		}
		return null;
	}

	private String[] getGCEmails( RepositoryItem order, RepositoryItem item ) {
		String catalogRefId = (String)item.getPropertyValue( "catalogRefId" );
		String[] name = new String[2];
		if( GiftCardType.ELECTRONIC.getValue().equalsIgnoreCase( catalogRefId ) 
				|| GiftCardType.PERSONALIZED.getValue().equalsIgnoreCase( catalogRefId )
					|| GiftCardType.STANDARD.getValue().equalsIgnoreCase( catalogRefId )) {
			name[0] = ( item.getPropertyValue( "recipientEmail" ) != null ) ? (String)item.getPropertyValue( "recipientEmail" ) : "";
			name[1] = ( item.getPropertyValue( "senderEmail" ) != null ) ? (String)item.getPropertyValue( "senderEmail" ) : "";
			return name;
		}
		return null;
	}


	/**
	 * 
	 * @param order
	 * @param orderTotal
	 * @param locale
	 * @throws RepositoryException
	 */
	public void updateOrderHistory( DigitalOrderImpl order, double orderTotal, String locale ) throws RepositoryException {
		if( order == null ) return;
		
		String profileId = order.getProfileId();
		String loyaltyNumber = order.getLoyaltyId() != null ? order.getLoyaltyId() : "";
		MutableRepository mutRep = (MutableRepository)getOrderRepository();
		MutableRepositoryItem mutItem = mutRep.createItem( order.getId(), orderHistoryDescName );

		RepositoryItem orderRepo = order.getRepositoryItem();		
		mutItem.setPropertyValue( "orderId", order.getId() );
		mutItem.setPropertyValue( "profileId", profileId );		
		mutItem.setPropertyValue( "loyaltyNumber", loyaltyNumber );
		mutItem.setPropertyValue( "status", "SUBMITTED" );
		mutItem.setPropertyValue( "statusDate", new Date() );
		if(logger.isDebugEnabled()){
			logger.debug("statusDate set to: "+new Date());
		}
		mutItem.setPropertyValue( "email", this.getEmailForOrder( orderRepo ) );
		mutItem.setPropertyValue( "orderPlacementDate", new Date() );
		if(logger.isDebugEnabled()){
			logger.debug("orderPlacementDate set to: "+new Date());
		}
		mutItem.setPropertyValue( "updateDate", new Date() );
		mutItem.setPropertyValue( "addDate", new Date() );
		mutItem.setPropertyValue( "total", DigitalCommonUtil.round(orderTotal)); 
		mutItem.setPropertyValue( "locale", locale );
		mutItem.setPropertyValue( "siteId", order.getSiteId() );
		mutItem.setPropertyValue( "demandLocation", MultiSiteUtil.getWebsiteLocationId( this.siteManager.getSite( order.getSiteId() ) ) );
		mutItem.setPropertyValue( "currencyCode", getOrderPrice( orderRepo ).getPropertyValue( "currencyCode" ) != null ? (String)getOrderPrice( orderRepo ).getPropertyValue( "currencyCode" ) : "" );

		List<RepositoryItem> items = new ArrayList<>(getCommerceItems(orderRepo).size());

		int i = 0;
		for( RepositoryItem item : getCommerceItems( orderRepo ) ) {
			MutableRepositoryItem citem = mutRep.createItem( orderHistoryItemName );
			citem.setPropertyValue( "skuId", (String)item.getPropertyValue( "catalogRefId" ) );
			citem.setPropertyValue( "productId", (String)item.getPropertyValue( "productId" ) );
			citem.setPropertyValue( "orderSysId", order.getId() );
			citem.setPropertyValue( "orderPlacementDate", new Date() );
			if(logger.isDebugEnabled()){
				logger.debug("orderPlacementDate set to citem as: "+new Date());
			}
			citem.setPropertyValue( "unitPrice", (Double)( ( (RepositoryItem)item.getPropertyValue( "priceInfo" ) ).getPropertyValue( "listPrice" ) ) );
			citem.setPropertyValue( "quantity", ( (Long)item.getPropertyValue( "quantity" ) ).intValue() );
			citem.setPropertyValue( "updateDate", new Date() );
			citem.setPropertyValue( "addDate", new Date() );

			// For BOPIS and BOSTS orders, update the pickupLocationId
			if( item.getPropertyValue( "shipType" ) != null && item.getPropertyValue( "StoreId" ) != null ) {
				if( ShippingGroupConstants.ShipType.BOPIS.getValue().equals( (String)item.getPropertyValue( "shipType" ) ) || ShippingGroupConstants.ShipType.BOSTS.getValue().equals( (String)item.getPropertyValue( "shipType" ) ) ) {
					citem.setPropertyValue( "pickupLocationId", (String)item.getPropertyValue( "StoreId" ) );

				}
			}

			String[] name = getNameOfShipment( orderRepo, item );
			if( name != null ) {
				if( name[2].equals( "giftcard" ) ) {
					citem.setPropertyValue( "gcFromName", name[1] );
					citem.setPropertyValue( "gcToName", name[0] );
				} else {
					citem.setPropertyValue( "shipToFirstName", name[0] );
					citem.setPropertyValue( "shipToLastName", name[1] );
				}

			}
			String[] gcemails = getGCEmails( orderRepo, item );
			if( gcemails != null ) {
				citem.setPropertyValue( "gcFromEmailAddress", gcemails[1] );
				citem.setPropertyValue( "gcToEmailAddress", gcemails[0] );
			}

			citem.setPropertyValue( "status", (String)item.getPropertyValue( "state" ) );
			citem.setPropertyValue( "statusDate", new Date() );
			citem.setPropertyValue( "atgCommerceItemId", item.getRepositoryId() );
			citem.setPropertyValue( "sequenceNum", i );
			items.add( citem );
			i++;
		}

		mutItem.setPropertyValue( "commerceItems", items );

		mutRep.addItem( mutItem );
	}
	
	public void populateSendOrderRequest(DigitalOrderImpl order, RewardsSendOrderData request) {
		// TODO Auto-generated method stub
		//Populate the OrderItem object to send all the required details to GCP
		DigitalContactInfo tmpOrderContactInfo=null;
		try {
			//Getting billing address containing email and phone number
			tmpOrderContactInfo = ((DigitalPaymentGroupManager) this.getPaymentGroupManager()).getBillingAddressFromOrder(order);
		} catch (DigitalAppException e) {
			if (isLoggingError()) {
				logError("Exception while attempting to get billing address from order", e);
			}
		}//orderItem.setAccountNumber(order.getProfileId());
		request.setAtgID(order.getProfileId());
		request.setTransactionNumber(order.getId());
		request.setLocationNumber(getLocationNumber());
		//request.setLoyaltyAccountNumber(Long.valueOf(order.getLoyaltyId()));
		Date oSubmittedDate=order.getSubmittedDate();
		String transactionDate=null;
		if(null!=oSubmittedDate) {
			transactionDate = sdf.format(oSubmittedDate);
		}else{
			transactionDate = sdf.format(new Date());
		}
		request.setTransactionDate(transactionDate);
		String  channelCode=getMappedChannelCode(order.getOriginOfOrder());
		request.setChannelCode(channelCode);
		request.setBaseShippingAmount(order.getTotalShippingCost());
		request.setTotalQuantity(order.getCommerceItemCount());
		request.setBusinessUnitNumber(getBusinessUnitId());
		//TODO: Add appropriate tax here
		request.setTotalSalesTax(order.getTaxPriceInfo().getAmount());
		request.setTotalSalesAmount(order.getOrderTotal().doubleValue());

		//Set billTo with the billing address
		if(null!=tmpOrderContactInfo){
			BillToItem billTo=new BillToItem();
			billTo.setFirstName(tmpOrderContactInfo.getFirstName());
			billTo.setLastName(tmpOrderContactInfo.getLastName());
			billTo.setBillToAddress1(tmpOrderContactInfo.getAddress1());
			billTo.setBillToAddress2(tmpOrderContactInfo.getAddress2());
			billTo.setBillTocity(tmpOrderContactInfo.getCity());
			billTo.setBillToState(tmpOrderContactInfo.getState());
			billTo.setBillToCountry(tmpOrderContactInfo.getCounty());
			billTo.setBillToPostCode(tmpOrderContactInfo.getPostalCode());
			request.setBillTo(billTo);
		}



		//Set Tender Type; check what will be the payment type if the tender is both cc and gift
		List<TenderItem> tenders=new ArrayList<>();
		TenderItem tenderItem=new TenderItem();
		String pType=getPaymentTypeOnOrder(order);
		tenderItem.setType(pType);
		tenders.add(tenderItem);
		request.setTender(tenders);

		//Set discountMarkDownCodes
		List<DiscountMarkDownCodes> discountCodes=new ArrayList<>();
		List<PricingAdjustment> pAdjustments=new ArrayList<>();
		pAdjustments=order.getPriceInfo().getAdjustments();
		for(int i=1; i<pAdjustments.size(); i++){
			DiscountMarkDownCodes discountCode=new DiscountMarkDownCodes();
			if(null!=pAdjustments.get(i).getPricingModel()){
				if(pAdjustments.get(i).getAdjustment()<0){
					discountCode.setDiscountAmount((pAdjustments.get(i).getAdjustment())*-1);
				}else{
					discountCode.setDiscountAmount((pAdjustments.get(i).getAdjustment()));
				}
				discountCode.setDiscountMarkDownCode(pAdjustments.get(i).getPricingModel().getRepositoryId());
				discountCodes.add(discountCode);
			}
		}

		//ADD order level certificates
		Set<RepositoryItem> orderRewards=order.getRewardCertificates();
		if(null!=orderRewards && orderRewards.size()>0){
			for(RepositoryItem item:orderRewards){
				DiscountMarkDownCodes discountCode=new DiscountMarkDownCodes();
				discountCode.setDiscountAmount((Double)item.getPropertyValue("amountAuthorized"));
				discountCode.setDiscountMarkDownCode((String)item.getPropertyValue("certificateNumber"));
				discountCodes.add(discountCode);
			}
		}

		request.setDiscountMarkDownCodes(discountCodes);

		//Set Items
		//ItemsItem items =new ItemsItem();
		List<ItemsItem> items =new ArrayList();
		items=getOrderCommerceItems(order);
		request.setItems(items);

	}
	
	private List<ItemsItem> getOrderCommerceItems(DigitalOrderImpl order) {
		// TODO Auto-generated method stub
		List<DigitalCommerceItem> cItems = order.getCommerceItems();
		List<ItemsItem> items =new ArrayList();
		//Loop through each commerce items and set the ItemsItem.
		for(DigitalCommerceItem ci:cItems){
			ItemsItem item=new ItemsItem();
			item.setCommerceLineNumber(ci.getId());
			DigitalContactInfo tmpOrderContactInfo=null;
			Address tmpOrderShippingInfo=null;
			try {
				//Getting billing address containing email and phone number
				tmpOrderContactInfo = ((DigitalPaymentGroupManager) this.getPaymentGroupManager()).getBillingAddressFromOrder(order);
			} catch (DigitalAppException e) {
				if (isLoggingError()) {
					logError("Exception while attempting to get billing address from order", e);
				}
			}
			//TODO:Set the shipping details
			ShipToItem shipTo=new ShipToItem();
			//Getting shipping address containing email and phone number
			List<HardgoodShippingGroup> hgShippingGroups = new ArrayList();
			hgShippingGroups = getDswShippingGroupManager().getHardgoodShippingGroups(order);

			ShippingGroupCommerceItemRelationship rel = null;
			if(hgShippingGroups!=null){
				for (HardgoodShippingGroup hgShippingGroup : hgShippingGroups){
					tmpOrderShippingInfo = (Address)hgShippingGroup.getShippingAddress();
					try {
						rel=getDswShippingGroupManager().getShippingGroupCommerceItemRelationship(order, ci.getId(), hgShippingGroup.getId());
					} catch (CommerceException e) {
						if (isLoggingError()) {
							logError("Exception while attempting to get getShippingGroupCommerceItemRelationship from order/commerceItem", e);
						}
					}
				}
			}

			if(null!=tmpOrderShippingInfo){
				shipTo.setFirstName(tmpOrderShippingInfo.getFirstName());
				shipTo.setLastName(tmpOrderShippingInfo.getLastName());
				shipTo.setShipToAddress1(tmpOrderShippingInfo.getAddress1());
				shipTo.setShipToAddress2(tmpOrderShippingInfo.getAddress2());
				shipTo.setShipTocity(tmpOrderShippingInfo.getCity());
				shipTo.setShipToCountry(tmpOrderShippingInfo.getCountry());
				shipTo.setShipToPostCode(tmpOrderShippingInfo.getPostalCode());
				shipTo.setShipToState(tmpOrderShippingInfo.getState());
				item.setShipTo(shipTo);
			}
			item.setLongSku(ci.getCatalogRefId());
			item.setFulfillmentStatusCode("Created");
			//TODO: Get the promoshare and adjust the itemLineTotal
			double itemPrice=ci.getPriceInfo().getAmount()-ci.getCumulativeOrderPromoShare()-ci.getCumulativeRewardsCertificateShare();
			itemPrice=getPromotionTools().getPricingTools().round(itemPrice);
			item.setLineTotalWithoutTax(itemPrice);
			item.setSaleItemQuantity((int)ci.getQuantity());
			item.setFulfillmentLocationNumber(getLocationNumber());
			String shipType=ci.getShipType();
			if(null!=shipType && (shipType.equalsIgnoreCase(BOPIS_PROPERTY) || shipType.equalsIgnoreCase(BOSTS_PROPERTY))){
				item.setFulfillmentTypeCode(shipType);
			}
			item.setShippingMethodCode(ci.getShipType());
			List details = ci.getPriceInfo().getCurrentPriceDetails();
			DetailedItemPriceInfo detail = (DetailedItemPriceInfo)details.get( 0);
			item.setSaleTaxAmount(detail.getTax());
			if(null!=rel && null!=rel.getShippingGroup() && null!=rel.getShippingGroup().getPriceInfo()){
				item.setShippingCost(rel.getShippingGroup().getPriceInfo().getAmount());
			}
			List<DiscountMarkDownCodes> discountCodes=new ArrayList<>();
			List<PricingAdjustment> pAdjustments=new ArrayList<>();
			pAdjustments=ci.getPriceInfo().getAdjustments();
			for(int i=1; i<pAdjustments.size(); i++){
				DiscountMarkDownCodes discountCode=new DiscountMarkDownCodes();
				if(pAdjustments.get(i).getAdjustment()<0){
					discountCode.setDiscountAmount((pAdjustments.get(i).getAdjustment())*-1);
				}else{
					discountCode.setDiscountAmount((pAdjustments.get(i).getAdjustment()));
				}
				if(null!=pAdjustments.get(i).getPricingModel()){
					discountCode.setDiscountMarkDownCode(pAdjustments.get(i).getPricingModel().getRepositoryId());
				}
				discountCodes.add(discountCode);
			}

			Map<String,Double> rewardsPromoShare=ci.getRewardCertificateShare();

			if(null!=rewardsPromoShare && !rewardsPromoShare.isEmpty()){
				for (String key : rewardsPromoShare.keySet()) {
					DiscountMarkDownCodes rewardsDiscountCode=new DiscountMarkDownCodes();
					if(null!=rewardsPromoShare && null!=rewardsPromoShare.get(key) ){
						String[] rewardPromoshare=String.valueOf(key).split("-");
						String certNumber=rewardPromoshare[0];
						if(null!=certNumber){
							certNumber=certNumber.replace("{", "");
							rewardsDiscountCode.setDiscountMarkDownCode(certNumber);
						}
						rewardsDiscountCode.setDiscountAmount(rewardsPromoShare.get(key));
					}
					discountCodes.add(rewardsDiscountCode);
				}
			}

			Map<String,Double> orderPromoShare=ci.getOrderPromoShare();

			if(null!=orderPromoShare && !orderPromoShare.isEmpty()){
				for (String key : orderPromoShare.keySet()) {
					DiscountMarkDownCodes orderPromoDiscountCode=new DiscountMarkDownCodes();
					orderPromoDiscountCode.setDiscountAmount(orderPromoShare.get(key));
					orderPromoDiscountCode.setDiscountMarkDownCode(key);
					discountCodes.add(orderPromoDiscountCode);
				}
			}
			item.setDiscountMarkDownCodes(discountCodes);
			items.add(item);
		}
		return items;
	}
	
	private String getMappedChannelCode(String originOfOrder) {
		// TODO Auto-generated method stub
		String channelCode = getDswConstants().getChannelCodeMap().get(originOfOrder);
		return channelCode;
	}
	
	private String getPaymentTypeOnOrder(DigitalOrderImpl order) {
		// TODO Auto-generated method stub
		String pType=null;
		List<PaymentGroup> paymentGroupList = order.getPaymentGroups();
		if( paymentGroupList != null ) {
			for( PaymentGroup paymentGroup : paymentGroupList ) {
				if (paymentGroup instanceof CreditCard) {
					pType="CreditCard";
				}else if( paymentGroup instanceof PaypalPayment ) {
					pType="PayPal";
				}else if( paymentGroup instanceof DigitalGiftCard ) {
					pType="GiftCard";
				}
			}
		}
		return pType;
	}
	
	public RewardsSendOrderRequest createRewardsSendOrderRequest(
			RewardsSendOrderData request) {
		RewardsSendOrderRequest req=new RewardsSendOrderRequest();
		req.setAtgID(request.getAtgID());
		req.setTransactionNumber(request.getTransactionNumber());
		req.setLocationNumber(request.getLocationNumber());
		req.setTransactionDate(request.getTransactionDate());
		req.setTransactionNumber(request.getTransactionNumber());
		req.setChannelCode(request.getChannelCode());
		req.setBaseShippingAmount(request.getBaseShippingAmount());
		req.setTotalQuantity(request.getTotalQuantity());
		req.setBusinessUnitNumber(request.getBusinessUnitNumber());
		req.setTotalSalesTax(request.getTotalSalesTax());
		req.setTotalSalesAmount(request.getTotalSalesAmount());
		if(null!=request.getBillTo()){
		req.setBillTo(getBillTo(request.getBillTo()));
		}
		if(null!=request.getTender()){
		req.setTender(getTender(request.getTender()));
		}
		req.setDiscountMarkDownCodes(getDiscountdiscountCodes(request.getDiscountMarkDownCodes()));
		req.setItems(getItems(request.getItems()));
		
		return req;
	}
	

	private List<Items> getItems(
			List<ItemsItem> itemsItem) {
		List<Items> items=new ArrayList();
		for(ItemsItem iItem:itemsItem){
			Items item=new Items();
			item.setCommerceLineNumber(iItem.getCommerceLineNumber());
			if(null!=iItem.getShipTo()){
			item.setShipTo(getShipTo(iItem.getShipTo()));
			}
			item.setLongSku(iItem.getLongSku());
			item.setFulfillmentStatusCode(iItem.getFulfillmentStatusCode());
			item.setFulfillmentTypeCode(iItem.getFulfillmentTypeCode());
			item.setLineTotalWithoutTax(iItem.getLineTotalWithoutTax());
			item.setSaleItemQuantity(iItem.getSaleItemQuantity());
			item.setFulfillmentLocationNumber(iItem.getFulfillmentLocationNumber());
			item.setShippingMethodCode(iItem.getShippingMethodCode());
			item.setSaleTaxAmount(iItem.getSaleTaxAmount());
			item.setShippingCost(iItem.getShippingCost());
			item.setDiscountMarkDownCodes(getDiscountdiscountCodes(iItem.getDiscountMarkDownCodes()));
			items.add(item);
		}
	return items;
	}

	private ShipTo getShipTo(ShipToItem pShipTo) {
		ShipTo shipTo=new ShipTo();
		shipTo.setShipToAddress1(pShipTo.getShipToAddress1());
		shipTo.setShipToAddress2(pShipTo.getShipToAddress2());
		shipTo.setShipTocity(pShipTo.getShipTocity());
		shipTo.setShipToCountry(pShipTo.getShipToCountry());
		shipTo.setShipToPostCode(pShipTo.getShipToPostCode());
		shipTo.setShipToState(pShipTo.getShipToState());
		shipTo.setFirstName(pShipTo.getFirstName());
		shipTo.setLastName(pShipTo.getLastName());
		return shipTo;
	}

	private List<DiscountMarkDownCodesItem> getDiscountdiscountCodes(
			List<DiscountMarkDownCodes> discountMarkDownCodes) {
		List<DiscountMarkDownCodesItem> DiscountMarkDownCodesItems=new ArrayList();
		for(DiscountMarkDownCodes discountItem:discountMarkDownCodes){
			DiscountMarkDownCodesItem discountMarkDownCodesItem=new DiscountMarkDownCodesItem();
			discountMarkDownCodesItem.setDiscountAmount(discountItem.getDiscountAmount());
			discountMarkDownCodesItem.setDiscountMarkDownCode(discountItem.getDiscountMarkDownCode());
			discountMarkDownCodesItem.setPartyOfferId(discountItem.getPartyOfferId());
			DiscountMarkDownCodesItems.add(discountMarkDownCodesItem);
		}
	return DiscountMarkDownCodesItems;
	}

	private List<Tender> getTender(
			List<TenderItem> tender) {
		List<Tender> tenderItems=new ArrayList();
			for(TenderItem ti:tender){
				Tender tenderItem=new Tender();
				tenderItem.setType(ti.getType());
				tenderItems.add(tenderItem);
			}
		
		return tenderItems;
	}

	private BillTo getBillTo(
			BillToItem pBillTo) {
		BillTo billTo=new BillTo();
		billTo.setBillToAddress1(pBillTo.getBillToAddress1());
		billTo.setBillToAddress2(pBillTo.getBillToAddress2());
		billTo.setBillTocity(pBillTo.getBillTocity());
		billTo.setBillToCountry(pBillTo.getBillToCountry());
		billTo.setBillToPostCode(pBillTo.getBillToPostCode());
		billTo.setBillToState(pBillTo.getBillToState());
		billTo.setFirstName(pBillTo.getFirstName());
		billTo.setLastName(pBillTo.getLastName());
		return billTo;
	}
	
	
	public boolean isFraud(DigitalOrderImpl order) {
		// TODO Auto-generated method stub
		boolean fraudFlag = false;
		final DigitalCreditCard creditCard = ((DigitalPaymentGroupManager) getPaymentGroupManager())
				.findCreditCard(order);
		if (null != creditCard) {
			List<?> ccStatusList = creditCard.getAuthorizationStatus();
			if (ccStatusList != null && !ccStatusList.isEmpty()) {
				Object ccStatusObj = ccStatusList.iterator().next();
				if (ccStatusObj != null && ccStatusObj instanceof DigitalCreditCardStatus) {
					DigitalCreditCardStatus ccStatus = (DigitalCreditCardStatus) ccStatusObj;
					String fraudFlg = ccStatus.getFraudFlag();
          if (fraudFlg != null &&
							("Y".equalsIgnoreCase(fraudFlg) || "O".equalsIgnoreCase(fraudFlg))) {
						fraudFlag = true;
					}
				}
			}
		}
		return fraudFlag;
	}
}
