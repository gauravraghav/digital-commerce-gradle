package com.digital.commerce.services.profile;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import atg.adapter.gsa.ChangeAwareSet;
import atg.adapter.gsa.GSAItem;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.reward.RewardService;
import com.digital.commerce.integration.reward.domain.RewardServiceRequest;
import com.digital.commerce.integration.reward.domain.RewardServiceResponse;
import com.digital.commerce.services.profile.vo.Offer;
import com.google.common.collect.Iterables;
import lombok.Getter;
import lombok.Setter;

import static com.digital.commerce.common.util.DigitalDateUtil.OFFER_DATE_FORMAT;

/** 
 * Responsible for merging a new set of offers for a customer profile with their existing offer set.
 */
@Getter
@Setter
public class DigitalWebOfferTools {
	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalWebOfferTools.class);
	static final String USER_PROPERTY = "user";

	private RewardService rewardService;

	public void mergeOffersToProfile(MutableRepositoryItem profile, boolean isValidLoyaltyMember) {
		String profileId = profile.getRepositoryId();
		List<Offer> currentOfferList = new ArrayList<>();

		if (isValidLoyaltyMember) {
			currentOfferList = retrieveRewardServiceOffersByProfileId(profileId);
		}

		final MutableRepository profileRepository = (MutableRepository) profile.getRepository();
		final ChangeAwareSet offerChangeAwareSet = ((ChangeAwareSet) profile.getPropertyValue(DigitalWebOfferConstants.OFFERS_PROPERTY));
		Set<RepositoryItem> mergedOfferSet = new HashSet<>();
		if (offerChangeAwareSet != null && !offerChangeAwareSet.isEmpty()) {
			final Iterator<?> iterator = offerChangeAwareSet.iterator();
			while (iterator.hasNext()) {
				final GSAItem repositoryItem = (GSAItem) iterator.next();
				final Offer repositoryItemAsOffer = OfferFactory.from(repositoryItem);
				final int existingOfferIndex = Iterables.indexOf(currentOfferList, new ExistingOfferPredicate(repositoryItemAsOffer));
				if (existingOfferIndex != -1) {
					repositoryItem.setPropertyValue(DigitalWebOfferConstants.START_DATE_PROPERTY, currentOfferList.get(existingOfferIndex).getStartDate());
					repositoryItem.setPropertyValue(DigitalWebOfferConstants.END_DATE_PROPERTY, currentOfferList.get(existingOfferIndex).getEndDate());
					mergedOfferSet.add(repositoryItem);
					currentOfferList.remove(existingOfferIndex);
				}
			}
		}
		for (final Offer offer : currentOfferList) {
			mergedOfferSet.add(OfferFactory.to(profile, profileRepository, offer));
		}
		profile.setPropertyValue(DigitalWebOfferConstants.OFFERS_PROPERTY, mergedOfferSet);
	}

	public List<Offer> retrieveWebOffersFromProfile(MutableRepositoryItem profile) {
		List<Offer> offers = new ArrayList<>();
		final ChangeAwareSet offerChangeAwareSet = ((ChangeAwareSet) profile.getPropertyValue(DigitalWebOfferConstants.OFFERS_PROPERTY));

		if (offerChangeAwareSet != null && !offerChangeAwareSet.isEmpty()) {
			final Iterator<?> iterator = offerChangeAwareSet.iterator();
			while (iterator.hasNext()) {
				final GSAItem repositoryItem = (GSAItem) iterator.next();
				final Offer repositoryItemAsOffer = OfferFactory.from(repositoryItem);
				offers.add(repositoryItemAsOffer);
			}
		}

		return offers;
	}

	public List<Offer> retrieveRewardServiceOffersByProfileId(String profileId) {
		List<Offer> offers = new ArrayList<>();

		try {

			RewardServiceRequest retrieveOffersRequest = new RewardServiceRequest();
			RewardServiceResponse retrieveOffersResponse = null;

			retrieveOffersRequest.getPerson().setProfileID(profileId);

			retrieveOffersResponse = rewardService.retrieveOffersByProfileId(retrieveOffersRequest);
			if (retrieveOffersResponse != null && retrieveOffersResponse.isSuccess()) {
				List<com.digital.commerce.integration.reward.domain.Offer> customerOffers = retrieveOffersResponse.getOffers();
				for (com.digital.commerce.integration.reward.domain.Offer offerBean : customerOffers) {
					final Offer offer = new Offer();
					offer.setCampaignId(offerBean.getCampaignID().trim());
					offer.setPromotionId(offerBean.getPromoID().trim());
					offer.setSegmentId(offerBean.getSegmentID().trim());
					offer.setCollateralId(offerBean.getOfferID().trim());
					if (DigitalStringUtil.isNotBlank(offerBean.getStartDate())) {
						try {
							Date date = DigitalDateUtil.parseDate(offerBean.getStartDate().trim(), new String[]{OFFER_DATE_FORMAT});
							offer.setStartDate(date);
						} catch (ParseException e) {
							logger.error(String.format("Caught exception attempting to parse offer startDate %1$s for profileId %2$s", offerBean.getStartDate(), profileId));
						}
					}
					if (DigitalStringUtil.isNotBlank(offerBean.getEndDate())) {
						try {
							Date date = DigitalDateUtil.parseDate(offerBean.getEndDate().trim(), new String[]{OFFER_DATE_FORMAT});
							offer.setEndDate(date);
						} catch (ParseException e) {
							logger.error(String.format("Caught exception attempting to parse offer endDate %1$s for profileId %2$s", offerBean.getEndDate(), profileId));
						}
					}
					offers.add(offer);
				}

			} else {
				logger.error("Error while retrieving retrieveOffersByProfileId");
			}

		} catch (DigitalIntegrationException e) {
			logger.error("Error while retrieving retrieveOffersByProfileId", e);
		}

		return offers;

	}

	private static class OfferFactory {
		static Offer from(GSAItem item) {
			final Offer retVal = new Offer();
			retVal.setCampaignId((String) item.getPropertyValue(DigitalWebOfferConstants.CAMPAIGN_PROPERTY));
			retVal.setPromotionId((String) item.getPropertyValue(DigitalWebOfferConstants.PROMOTION_PROPERTY));
			retVal.setSegmentId((String) item.getPropertyValue(DigitalWebOfferConstants.SEGMENT_PROPERTY));
			retVal.setCollateralId((String) item.getPropertyValue(DigitalWebOfferConstants.COLLATERAL_PROPERTY));
			retVal.setStartDate((Date) item.getPropertyValue(DigitalWebOfferConstants.START_DATE_PROPERTY));
			retVal.setEndDate((Date) item.getPropertyValue(DigitalWebOfferConstants.END_DATE_PROPERTY));
			return retVal;
		}

		static MutableRepositoryItem to(MutableRepositoryItem repositoryItem, MutableRepository repository, Offer offer) {
			MutableRepositoryItem retVal = null;
			try {
				retVal = repository.createItem(DigitalWebOfferConstants.OFFER_ITEM_DESCRIPTOR);
				if (repositoryItem instanceof Profile) {
					retVal.setPropertyValue(USER_PROPERTY, ((Profile) repositoryItem).getDataSource());
				} else {
					retVal.setPropertyValue(USER_PROPERTY, repositoryItem);
				}
				retVal.setPropertyValue(DigitalWebOfferConstants.CAMPAIGN_PROPERTY, offer.getCampaignId());
				retVal.setPropertyValue(DigitalWebOfferConstants.PROMOTION_PROPERTY, offer.getPromotionId());
				retVal.setPropertyValue(DigitalWebOfferConstants.SEGMENT_PROPERTY, offer.getSegmentId());
				retVal.setPropertyValue(DigitalWebOfferConstants.COLLATERAL_PROPERTY, offer.getCollateralId());
				retVal.setPropertyValue(DigitalWebOfferConstants.START_DATE_PROPERTY, offer.getStartDate());
				retVal.setPropertyValue(DigitalWebOfferConstants.END_DATE_PROPERTY, offer.getEndDate());
			} catch (RepositoryException e) {
				logger.error(String.format("Unable to create offer for %1$s", offer), e);
			}
			return retVal;
		}
	}

	private static class ExistingOfferPredicate implements DigitalPredicate<Offer> {
		private final Offer existingOffer;

		ExistingOfferPredicate(Offer existingOffer) {
			this.existingOffer = existingOffer;
		}

		@Override
		public boolean apply(Offer input) {
			return DigitalStringUtil.equals(existingOffer.getCampaignId(), input.getCampaignId()) && DigitalStringUtil.equals(existingOffer.getCollateralId(), input.getCollateralId())
					&& DigitalStringUtil.equals(existingOffer.getPromotionId(), input.getPromotionId()) && DigitalStringUtil.equals(existingOffer.getSegmentId(), input.getSegmentId());
		}

	}
}
