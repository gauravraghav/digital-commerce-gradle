/**
 * 
 */
package com.digital.commerce.services.pricing;

import java.util.*;

import atg.commerce.order.Order;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.OrderPricingEngineImpl;
import atg.commerce.pricing.PricingException;
import atg.repository.RepositoryItem;
import atg.web.messaging.UserMessage;

import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalOrderImpl;

import static com.digital.commerce.common.util.ComponentLookupUtil.COUPON_MESSAGE;


/** */
public class DigitalOrderPricingEngine extends OrderPricingEngineImpl {
	@SuppressWarnings("rawtypes")
	public OrderPriceInfo priceOrder( Order pOrder, Collection pPricingModels, Locale pLocale, RepositoryItem pProfile, Map pExtraParameters ) throws PricingException {
		DigitalOrderImpl order = (DigitalOrderImpl)pOrder;
		@SuppressWarnings("unchecked")
		Iterator<DigitalCommerceItem> i = order.getCommerceItems().iterator();
		// null out order promo share so it doesn't stick around if an order no longer qualifies for this promo
		DigitalCommerceItem curItem = null;
		while( i.hasNext() ) {
			curItem = i.next();
			curItem.setOrderPromoShare( null );
		}
		OrderPriceInfo op = super.priceOrder( pOrder, pPricingModels, pLocale, pProfile, pExtraParameters );
		@SuppressWarnings("unchecked")
		HashMap<String,UserMessage> stackingRuleMessage=(HashMap<String,UserMessage>)pExtraParameters.get("stackingRuleAppMessages");
		DigitalCouponMessage couponMessage = (DigitalCouponMessage) ComponentLookupUtil.lookupComponent(COUPON_MESSAGE);

		if (stackingRuleMessage!=null && couponMessage!=null && couponMessage.getPromoId()!=null && couponMessage.getErrorsCode()==null)
		{
			for (Map.Entry<String,UserMessage> stackMessages : stackingRuleMessage.entrySet()) {
				UserMessage userMessage = stackMessages.getValue();
				Object[] params = (Object[])userMessage.getParams();
				if (params!=null && params.length>1){
					for (Object stackingPromoId : params){
						if (stackingPromoId.equals(couponMessage.getPromoId())){
							couponMessage.setErrorsCode("not_qualified_order_level");
							 couponMessage.setPromoApplied(false);
							break;
						}
					}
				}
			}
		}
		op.setAmount(DigitalCommonUtil.round(op.getAmount()));
		op.setShipping(DigitalCommonUtil.round(op.getShipping()));
		op.setRawSubtotal(DigitalCommonUtil.round(op.getRawSubtotal()));
		op.setTax(DigitalCommonUtil.round(op.getTax()));
		op.setManualAdjustmentTotal(DigitalCommonUtil.round(op.getManualAdjustmentTotal()));
		
		return op;
	}

}
