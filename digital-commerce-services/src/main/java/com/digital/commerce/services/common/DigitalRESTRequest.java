package com.digital.commerce.services.common;

import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

/**
 * logic to log service requested time.
 * 
 * @author SK402315
 * 
 */
public class DigitalRESTRequest extends GenericService {
	private final static char PAIR_DELIMITER = ':';
	private final static String PARAM_DELIMITER = ",";

	/**
	 * records the service requested time in response header.
	 */
	public void setRequestedTime() {

		if (isLoggingDebug()) {
			logDebug("In DigitalRESTRequest's setRequestedTime method");
		}

		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();

		long reqTime = System.nanoTime();

		if (isLoggingDebug()) {
			logDebug("setting requested time to the header-->" + reqTime);
		}
		request.setAttribute("dateValue", reqTime);
	}
	
	public void setRequestKeyValue(String pairsStr) {

		String[] pairs = pairsStr.split(PARAM_DELIMITER);
		if (pairs == null || pairs.length == 0) {
			return;
		}

		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();

		for(String pair : pairs){
			int pos = pair.indexOf(PAIR_DELIMITER);
			if(pos > -1)
				request.setAttribute(pair.substring(0, pos), pair.substring(pos+1));
		}
	}
}