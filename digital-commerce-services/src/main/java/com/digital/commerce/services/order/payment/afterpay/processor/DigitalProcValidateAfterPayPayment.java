package com.digital.commerce.services.order.payment.afterpay.processor;

import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.processor.ValidatePaymentGroupPipelineArgs;
import atg.nucleus.GenericService;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.payment.afterpay.AfterPayCheckoutManager;
import com.digital.commerce.services.order.payment.afterpay.AfterPayPayment;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalProcValidateAfterPayPayment extends GenericService implements PipelineProcessor {

    private static int SUCCESS_CODE = 1;
    private static int[] RETURN_CODES = { SUCCESS_CODE };
    private AfterPayCheckoutManager afterPayCheckoutManager;

    @Override
    public int runProcess(Object object, PipelineResult pipelineResult) throws Exception {

        ValidatePaymentGroupPipelineArgs args;
        args=(ValidatePaymentGroupPipelineArgs) object;

        PaymentGroup pg=args.getPaymentGroup();

        AfterPayPayment app =(AfterPayPayment)pg;

        Order order=(Order)args.getOrder();
        if(order instanceof DigitalOrderImpl) {
        	DigitalOrderImpl o=(DigitalOrderImpl) order;
            if (!getAfterPayCheckoutManager().isOrderEligible(o)) {
                pipelineResult .addError("orderNotEligibileForAfterPayPayment",  "Order Not eligible for AfterPayPayment");
            }
            if (isLoggingDebug()) {
                logDebug("Validating the afterPay eligibility for Order" + order.getId());
            }
        }

        return SUCCESS_CODE;
    }

    @Override
    public int[] getRetCodes() {
        return RETURN_CODES;
    }
}
