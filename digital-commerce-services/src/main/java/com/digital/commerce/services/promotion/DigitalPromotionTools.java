package com.digital.commerce.services.promotion;


import atg.commerce.claimable.ClaimableException;
import atg.commerce.claimable.ClaimableTools;
import atg.commerce.claimable.CouponBatchTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingModelProperties;
import atg.commerce.pricing.definition.CompoundPricingModelExpression;
import atg.commerce.pricing.definition.PMDLParser;
import atg.commerce.pricing.definition.PricingModelElem;
import atg.commerce.pricing.definition.PricingModelExpression;
import atg.commerce.pricing.definition.QualifierElem;
import atg.commerce.promotion.PromotionException;
import atg.commerce.promotion.PromotionTools;
import atg.droplet.DropletException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.scenario.targeting.Slot;
import atg.service.collections.filter.ValidatorFilter;
import atg.service.util.CurrentDate;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.exception.DigitalRuntimeException;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.RewardsCertificateConstants;
import com.digital.commerce.services.pricing.PromotionRestrictionType;
import com.digital.commerce.services.profile.DigitalCommercePropertyManager;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.vo.Offer;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import com.digital.commerce.services.utils.FindRepositoryItemById;
import com.google.common.collect.Iterables;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.SerializationUtils;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;


@SuppressWarnings({"rawtypes","unchecked","deprecation"})
@Getter
@Setter
public class DigitalPromotionTools extends PromotionTools {

	private static final String CLASSNAME = DigitalPromotionTools.class.getName();

	private String			currentCouponsProperty;

	private String			currentRewardsProperty;

	private String			currentActivePromotionsProperty;

	private String			promotionRestrictionTypeProperty;

	private ClaimableTools	claimableTools;

	private CouponBatchTools couponBatchTools;
		
	private DigitalProfileTools	profileTools;

	private boolean			enableAllPromoExclusivity;

	private DigitalServiceConstants	dswConstants;
	
	private final static String START = "<qualifier>";
	private final static String END = "</qualifier>";
	private final static String ATG_PROFILE_PROPERTY="profile";
	private final static String ATG_PROFILE_LOYALTYTIER_PROPERTY="loyaltyTier";
	private final static String ATG_PROFILE_ELITE_LOYALTYTIER_PROPERTY="ELITE";
	private final static String ATG_PROFILE_GOLD_LOYALTYTIER_PROPERTY="GOLD";
	
	
	private DigitalPromotionProfileValidator dswPromotionProfileValidator;
	
	@Override
	 public CurrentDate getCurrentDate()
	 {
		CurrentDate currentDate = super.getCurrentDate();
		if(currentDate==null)
		{
			currentDate = new CurrentDate();
		}		
	   return currentDate;
	 }
	 
	 
	/** Returns true if the coupon is valid for the profile
	 * 
	 * @param couponId
	 * @return
	 * @throws RepositoryException */
	public boolean isCouponValidForProfile( Profile profile, String couponId ) {
		boolean valid = false;
		Set promotions = getPromotion( couponId );
		if( promotions != null ) {
			Iterator iterPromo = promotions.iterator();
			while (iterPromo.hasNext()){
					RepositoryItem promotion= (RepositoryItem)iterPromo.next();
					final boolean anonymousCoupon = Boolean.TRUE.equals( promotion.getPropertyValue( getGiveToAnonymousProfilesProperty() ) );
					final boolean loggedIn = profileTools.isLoggedIn( profile );
					valid = anonymousCoupon || loggedIn;
			}
		}
		return valid;

	}

	/** Returns true if the coupon is valid for the profile
	 * 
	 * @param couponId
	 * @return
	 * @throws RepositoryException */
	public boolean isValidCoupon( String couponId ) {
		boolean valid = false;
		Set promotions = getPromotion( couponId );
		if( promotions != null && !promotions.isEmpty() ) {
			//The coupon is associated with promotions
			valid=true;
		}
		return valid;

	}
	
	/** Returns true if the coupon is restricted
	 * 
	 * @param couponId
	 * @return
	 * @throws RepositoryException */
	public boolean isCouponRestricted( String couponId ) {
		boolean restricted = false;
		//due to capability or multiple promotions allowed for a single coupon, this may need to be revisited 
		Set promotions = getPromotion( couponId );
		if( promotions != null ) {
			Iterator iterPromo = promotions.iterator();
			while (iterPromo.hasNext()){
				RepositoryItem promotion = (RepositoryItem) iterPromo.next();
				restricted = isPromotionRestricted( promotion ); //MK:Revist this - Should we consider coupon restricted if any promotion in the coupon is restricted??   
			}
		}
		return restricted;
	}

	/** Returns true if the promotion is restricted
	 * 
	 * @param promotion
	 * @return */
	public boolean isPromotionRestricted( RepositoryItem promotion ) {
		boolean restricted = false;
		if( promotion != null && PromotionRestrictionType.CSC==PromotionRestrictionType.valueFor( promotion )) {
				restricted = !getDswConstants().isPaTool();
		}
		
		if(promotion != null && PromotionRestrictionType.APP==PromotionRestrictionType.valueFor( promotion )){
			//TODO: Get order and get the origin of Order value; if the value is androidapp or iosapp; its not restricted
			Order order = ((OrderHolder)ServletUtil.getCurrentRequest().resolveName("/atg/commerce/ShoppingCart")).getCurrent();
			String originOfOrder=order.getOriginOfOrder();
			if(originOfOrder!=null && ("androidapp".equalsIgnoreCase(originOfOrder) || "iosapp".equalsIgnoreCase(originOfOrder) || getDswConstants().isPaTool())){
				restricted=false;
			}else{
				restricted=true;
			}
		}
		return restricted;

	}
	
	
	/** Returns true if the coupon is restricted
	 * 
	 * @param couponId
	 * @return
	 * @throws RepositoryException */
	public boolean isCSRCouponRestricted( String couponId ) {
		boolean restricted = false;
		//due to capability or multiple promotions allowed for a single coupon, this may need to be revisited 
		Set promotions = getPromotion( couponId );
		if( promotions != null ) {
			Iterator iterPromo = promotions.iterator();
			while (iterPromo.hasNext()){
				RepositoryItem promotion = (RepositoryItem) iterPromo.next();
				restricted = isCSRPromotionRestricted( promotion ); //MK:Revist this - Should we consider coupon restricted if any promotion in the coupon is restricted??   
			}
		}
		return restricted;
	}

	/** Returns true if the promotion is restricted
	 * 
	 * @param promotion
	 * @return */
	public boolean isCSRPromotionRestricted( RepositoryItem promotion ) {
		boolean restricted = false;
		if( promotion != null && PromotionRestrictionType.CSC==PromotionRestrictionType.valueFor( promotion )) {
				restricted = !getDswConstants().isPaTool();
		}
		
		return restricted;

	}
	
	/**
	 * This checks if the promotion restriction type is APP only
	 * @param couponId
	 * @return
	 */
	
	public boolean isAppOnlyCoupon( String couponId ) {
		boolean appOnlyCoupon = false;
		Set promotions = getPromotion( couponId );
		if( promotions != null ) {
			Iterator iterPromo = promotions.iterator();
			while (iterPromo.hasNext()){
				RepositoryItem promotion = (RepositoryItem) iterPromo.next();
				appOnlyCoupon = isPromotionAppOnly( promotion ); 
				if(appOnlyCoupon)
					break;
			}
		}
		return appOnlyCoupon;
	}

	private boolean isPromotionAppOnly(RepositoryItem promotion) {
		boolean appOnlyPromotion = false;
		if(promotion != null && PromotionRestrictionType.APP==PromotionRestrictionType.valueFor( promotion )){
			appOnlyPromotion=true;
		}
		return appOnlyPromotion;
	}


	/** Returns true if the coupon is already applied
	 * 
	 * @param couponId
	 * @return */
	protected boolean isPromotionAlreadyApplied( Profile profile, String couponId ) {
		// v11 OOTB tools should be running validation for this. Will revisit if issues come up with duplicate promos being applied
		// also, the current platform allows coupons to be linked to multiple promos:ECOMM-2139
		final Set<RepositoryItem> currentCoupons = getCurrentCoupons( profile );
		return currentCoupons != null && Iterables.filter( currentCoupons, new FindRepositoryItemById( couponId ) ).iterator().hasNext();
	}

	/** Adds the promotion to the profile
	 * 
	 * @param profile
	 * @param claimCode
	 * @return
	 * @throws ClaimableException
	 * @throws RepositoryException */
	public boolean addPromotion( Profile profile, String claimCode ) throws RepositoryException {
		boolean retVal = false;
		try {
			String profileId = (String)profile.getPropertyValue( "id" );
			MutableRepositoryItem userProfile = (MutableRepositoryItem)( (MutableRepository)profile.getRepository() ).getItemForUpdate( profileId, "user" );
			Set<RepositoryItem> currentCoupons = getCurrentCoupons( profile );
			RepositoryItem coupon = getCouponToAddPromotion( claimCode );
			if( coupon != null ) {
				currentCoupons.add( coupon );
				userProfile.setPropertyValue( getCurrentCouponsProperty(), currentCoupons );
				retVal = true;
			}
		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("Error adding promotion ",e);
			}
		}
		
		
		return retVal;
	}

	/**
	 *
	 * @return Set<AppliedPromotion>
	 */
	public Set<AppliedOffer.AppliedPromotion> getProfileActivePromotions(Profile profile){
		Set<AppliedOffer.AppliedPromotion> appliedPromotions = new HashSet<>();
		Collection<RepositoryItem> activePromotions = getActivePromotions( profile );
		for( RepositoryItem activePromotionStatus : activePromotions ) {
			final RepositoryItem activePromotion = (RepositoryItem)activePromotionStatus.getPropertyValue(
					getProfilePropertyManager().getPromotionPropertyName() );
			if( activePromotion != null && !isCSRPromotionRestricted( activePromotion )) {
				final String description = (String)activePromotion.getPropertyValue(
						getProfilePropertyManager().getPromotionDescPropertyName());
				final String id = activePromotion.getRepositoryId();
				AppliedOffer appliedCoupon = new AppliedOffer();
				AppliedOffer.AppliedPromotion appliedPromotion = appliedCoupon.new AppliedPromotion();
				appliedPromotion.setPromotionId(id);
				appliedPromotion.setDescription(description);
				appliedPromotions.add(appliedPromotion);
			}
		}

		return appliedPromotions;
	}

	/**
	 *
	 * @return Set<AppliedOffer>
	 */
	public Set<AppliedOffer> getAppliedCoupons(Profile profile) {
		Map<String, AppliedOffer> retVal = new TreeMap<>();
		Collection<RepositoryItem> activePromos = getActivePromotions(profile);
		for( RepositoryItem activePromotionStatus :  activePromos) {
			final RepositoryItem activePromotion = (RepositoryItem)activePromotionStatus.getPropertyValue(
					getProfilePropertyManager().getPromotionPropertyName() );

			if (isLoggingDebug()){
				logDebug("activePromotion-->"+activePromotion);
			}


			if( activePromotion != null && !isCSRPromotionRestricted( activePromotion )) {
				final String description = (String)activePromotion.getPropertyValue(
						getProfilePropertyManager().getPromotionDescPropertyName());

				final String userFriendlyName =  (String)activePromotion.getPropertyValue(
						getProfilePropertyManager().getPromotionUserFriendlyNamePropertyName());

				final String userFriendlyDescription =  (String)activePromotion.getPropertyValue(
						getProfilePropertyManager().getPromotionUserFriendlyDescPropertyName());

				RepositoryItem shopNowLinkRepItem =  (RepositoryItem)activePromotion.getPropertyValue(
						getProfilePropertyManager().getPromotionShopNowLinkPropertyName());
				String shopNowLink=null;

				if (shopNowLinkRepItem!=null){
					shopNowLink=(String)shopNowLinkRepItem.getPropertyValue("url");
				}

				final String offerDetailsPageName =  (String)activePromotion.getPropertyValue(
						getProfilePropertyManager().getPromotionOfferDetailsPageNamePropertyName());

				final String discountType =  (String)activePromotion.getPropertyValue(
						getProfilePropertyManager().getPromotionDiscountTypePropertyName());

				final String id = activePromotion.getRepositoryId();

				final boolean targetedOffer = Boolean.TRUE.equals( activePromotion.getPropertyValue( "targetedOffer" ) );

				String code = null;

				if (isLoggingDebug()){
					logDebug("activePromotion id-->"+id);
				}


				for( RepositoryItem grantedCoupon : getProfileGrantedCoupons(profile) ) {

					if (isLoggingDebug()){
						logDebug("grantedCoupon id-->"+grantedCoupon);
					}
					

					RepositoryItem claimable=null;
					Set<RepositoryItem> promotions=null;
					promotions = (Set<RepositoryItem>)grantedCoupon.getPropertyValue(
							getProfilePropertyManager().getPromotionsPropertyName());
					
					//TODO: If granted coupon is a of type coupon batch. 
					//Get the set of promotions associated with it
					if(null!=promotions && promotions.isEmpty()){
						String coupon=grantedCoupon.getRepositoryId();
						promotions=getPromotionsFromCouponBatchCoupon(coupon);
					}
					
					for (RepositoryItem promotion : promotions){
						if( activePromotion.equals(promotion) ) {
							code = grantedCoupon.getRepositoryId();
							if (isLoggingDebug()){
								logDebug("Coupon code of active promotions-->"+code);
							}
							break;
						}
					}
				}

				if(code != null && !restrictTargetedPromotions(code)){
					AppliedOffer appliedCoupon = retVal.get(code);
					if(appliedCoupon == null ){
						appliedCoupon = new AppliedOffer();
						appliedCoupon.setCouponCode(code);
					}

					AppliedOffer.AppliedPromotion appliedPromotion = appliedCoupon.new AppliedPromotion();
					appliedPromotion.setPromotionId(id);
					appliedPromotion.setDescription(description);
					appliedPromotion.setOfferDetailsPageName(offerDetailsPageName);
					appliedPromotion.setShopNowLink(shopNowLink);
					appliedPromotion.setUserFriendlyDescription(userFriendlyDescription);
					appliedPromotion.setUserFriendlyName(userFriendlyName);
					appliedPromotion.setDiscountIconType(discountType);
					appliedPromotion.setTargetedOffer(targetedOffer);

					Set<AppliedOffer.AppliedPromotion> appliedPromotions = appliedCoupon.getAppliedPromotions();
					if(!appliedPromotions.contains(appliedPromotion)){
						appliedPromotions.add(appliedPromotion);
					}
					if(!retVal.containsKey(code)){
						retVal.put(code, appliedCoupon);
					}

					if (isLoggingDebug()){
						logDebug("Adding the coupon and promotion to the list :: promo##-->"+id);
					}
				}
			}
		}

		if (isLoggingDebug()){
			logDebug("final consolidated offers id-->"+retVal.values());
		}

		return new HashSet<>(retVal.values());
	}

	public Set<RepositoryItem> getPromotionsFromCouponBatchCoupon(String coupon) {
		ClaimableTools claimableTools=getClaimableTools();
		RepositoryItem claimable=null;
		Set<RepositoryItem> promotions=null;
		try {
			claimable = claimableTools.getClaimableItem(coupon);
			if(null!=getCouponBatchTools().getCouponBatch(coupon.trim()) && null!=claimable){
				//getPromotions associated with this coupon Id
				RepositoryItem couponBatch = getCouponBatchTools().getCouponBatch(coupon);
				promotions=(Set<RepositoryItem>)couponBatch.getPropertyValue("promotions");
			}
		} catch (RepositoryException e1) {
			logError("RepositoryException occured while trying to get promotions from batchCoupon"+e1);
		}catch (ClaimableException e2) {
			logError("ClaimableException occured while trying to get promotions from batchCoupon"+e2);
		}
		return promotions;
	}


	/**
	 *
	 * @param couponCode
	 * @return true if restricted, false if it is not restricted
	 */
	private boolean restrictTargetedPromotions(String couponCode){
		return isTargetedOffer(couponCode);
	}

	/** Returns the active promotions on the profile
	 * 
	 * @param profile
	 * @return */
	public Collection<RepositoryItem> getActivePromotions( Profile profile ) {
		Collection<RepositoryItem> retVal = (Collection<RepositoryItem>)profile.getPropertyValue( getCurrentActivePromotionsProperty() );
		if( retVal == null ) {
			retVal = new LinkedHashSet<>();
		}

		if (isLoggingDebug()){
			logDebug("PromotionTools getActivePromotions-->"+retVal.size());
		}
		
		return retVal;
	}

	/** Returns the active coupons on the profile
	 * 
	 * @param profile
	 * @return */
	public Set<RepositoryItem> getCurrentCoupons( Profile profile ) {

		Set<RepositoryItem> retVal = (Set<RepositoryItem>)profile.getPropertyValue( getCurrentCouponsProperty() );
		if( retVal == null ) {
			retVal = new LinkedHashSet<>();
		}
		return retVal;
	}

	/** Returns the coupon for the coupon (if it exists)
	 * 
	 * @param claimCode
	 * @return
	 * @throws RepositoryException */
	public RepositoryItem getCoupon( String claimCode ) {
		RepositoryItem retVal = null;
		claimCode=claimCode.trim().toUpperCase();
		if( DigitalStringUtil.isNotBlank( claimCode ) ) {
			try {
				retVal = getClaimableTools().getClaimableRepository().getItem( claimCode, getClaimableTools().getClaimableItemDescriptorName() );
				if(null!=getCouponBatchTools().getCouponBatch(claimCode)){
					retVal= getCouponBatchTools().getCouponBatch(claimCode);
				}
				if (retVal==null){
					retVal = getCouponBatchTools().getCouponBatch(claimCode.trim());
				}
			} catch( ClaimableException e ) {
				if( isLoggingDebug() ) {
					logDebug( String.format( "Unable to retrieve batch coupon %s", claimCode ), e );
				}
			}
			catch( RepositoryException e ) {
				if( isLoggingDebug() ) {
					logDebug( String.format( "Unable to retrieve coupon %s", claimCode ), e );
				}
			}
		}
		return retVal;
	}

	
	public RepositoryItem getCouponToAddPromotion( String claimCode ) {
		RepositoryItem retVal = null;
		claimCode=claimCode.trim().toUpperCase();
		if( DigitalStringUtil.isNotBlank( claimCode ) ) {
			try {
				retVal = getClaimableTools().getClaimableRepository().getItem( claimCode, getClaimableTools().getClaimableItemDescriptorName() );
				if (retVal==null){
					retVal = getCouponBatchTools().getCouponBatch(claimCode.trim());
				}
			} catch( ClaimableException e ) {
				if( isLoggingDebug() ) {
					logDebug( String.format( "Unable to retrieve batch coupon %s", claimCode ), e );
				}
			}
			catch( RepositoryException e ) {
				if( isLoggingDebug() ) {
					logDebug( String.format( "Unable to retrieve coupon %s", claimCode ), e );
				}
			}
		}
		return retVal;
	}
	/** Returns the promotion for the coupon (if it exists)
	 * 
	 * @param claimCode
	 * @return
	 * @throws RepositoryException */
	public Set getPromotion( String claimCode ) {
		Set retVal = null;
		RepositoryItem couponItem = getCoupon( claimCode );
		if( couponItem != null ) {
			retVal = (Set)couponItem.getPropertyValue( "promotions" );
		}
		return retVal;
	}

	/** Gets a list of promotions from the order and attempts to match them to
	 * coupon codes which the user had entered in. This might seem like overkill
	 * 
	 * 
	 * @param promos
	 *            the order to get promos from, example: ShoppingCart.current or
	 *            ShoppingCart.last
	 * @param profile
	 *            user's profile
	 * @return */
	public Map getCouponPromotions( List promos, Profile profile ) {
		Map coupons = new HashMap();
		Set<RepositoryItem> appliedCoupons = (Set<RepositoryItem>)profile.getPropertyValue( getCurrentCouponsProperty() );

		if( appliedCoupons != null && !appliedCoupons.isEmpty() ) {
			for( int x = 0; x < promos.size(); x++ ) {

				PricingAdjustment adj = (PricingAdjustment)promos.get( x );
				
				RepositoryItem coupon = getCouponWithPromo( appliedCoupons, adj.getPricingModel().getRepositoryId() );

				if( coupon != null ) {
					coupons.put( adj.getPricingModel().getPropertyValue( "promotion" ), coupon.getRepositoryId() );
				}
			}
		}
		return coupons;
	}

	public boolean isCouponPromo( String promoId, Profile profile ) {
		return isCouponPromo( promoId, profile, (Set<RepositoryItem>)profile.getPropertyValue( getCurrentCouponsProperty() ) );
	}

	public boolean isCouponPromo( String promoId, RepositoryItem profileItem, Set<RepositoryItem> appliedCoupons ) {
		return getCouponWithPromo( appliedCoupons, promoId ) != null;
	}

	protected RepositoryItem getCouponWithPromo( Set<RepositoryItem> coupons, String promoId ) {
		for( RepositoryItem coupon : coupons ) {
			Set promotions = (Set)coupon.getPropertyValue( "promotions");
			if(null==promotions){
				promotions=getPromotionsFromCouponBatchCoupon(coupon.getRepositoryId());
			}
			if(promotions != null){
				Iterator iterPromo = promotions.iterator();
				while (iterPromo.hasNext()){
					RepositoryItem promotion= (RepositoryItem)iterPromo.next();
					if( promotion.getRepositoryId().equals( promoId ) ) { 
						return coupon;
						}
				}
			}
		}
		return null;
	}

	protected RepositoryItem getAppliedCouponWithPromo( Profile profile, String promoId ) {
		Set<RepositoryItem> appliedCoupons = (Set<RepositoryItem>)profile.getPropertyValue( getCurrentCouponsProperty() );

		if( appliedCoupons != null && !appliedCoupons.isEmpty()) {
			for( int x = 0; x < appliedCoupons.size(); x++ ) {
				RepositoryItem coupon = getCouponWithPromo( appliedCoupons, promoId );
				if( coupon != null ) {
					return coupon; 
					}
			}
		}

		return null;
	}

	/** 
	 * 
	 * @param promotionId
	 * @return
	 * @throws Exception 
	 */
	public String getCouponCodeForPromotion( String promotionId ) throws RepositoryException {
		String couponCode = "";
		RepositoryItem[] coupons = getClaimableCouponsForPromotion( promotionId );
		if( coupons != null && coupons.length > 0 ) {
			couponCode = (String)coupons[0].getPropertyValue( "id" );
		}
		return couponCode;
	}
	
	/** 
	 * 
	 * @param promotionId
	 * @return
	 * @throws Exception 
	 */
	public RepositoryItem[] getCouponsForPromotion( String promotionId ) {
		return getClaimableCouponsForPromotion( promotionId );
	}

	/** @param promotionId
	 * @return
	 * @throws Exception */
	public String getDescriptionForPromotion( String promotionId ) throws RepositoryException {
		String description = "";
		Repository repository = getPromotions();
		RepositoryView repositoryView = repository.getView( "promotion" );
		RqlStatement rqlStatement = RqlStatement.parseRqlStatement( "id EQUALS ?0" );
		Object[] params = new Object[] { promotionId };
		RepositoryItem[] result = rqlStatement.executeQuery( repositoryView, params );
		if( result != null && result.length > 0 ) {
			description = (String)result[0].getPropertyValue( "description" );
		}
		return ( description != null ) ? description : "";
	}

	/** @param promotionId
	 * @return
	 * @throws RepositoryException */
	private RepositoryItem[] getClaimableCouponsForPromotion( String promotionId ) {
        try {
            return getClaimableTools().getCouponsForPromotion(promotionId);
        }
		catch(ClaimableException cex){
                if(isLoggingError()){
                	logError("Error getting coupon for promo",cex);
                	}
            }
        return new RepositoryItem[0];
	}

	public List getAllPromotions( Order order, boolean pGetAdjustments ) {
		List orderPromos = new LinkedList();
		List taxPromos = new LinkedList();
		List itemPromos = new LinkedList();
		List shippingPromos = new LinkedList();

		List allPromos = new LinkedList();
		getOrderPromotions( order, orderPromos, taxPromos, itemPromos, shippingPromos, pGetAdjustments );
		allPromos.addAll( orderPromos );
		allPromos.addAll( itemPromos );
		return allPromos;
	}

	public List getItemPromotions( Order pOrder, boolean pGetAdjustments ) {
		List pItemPromotions = new LinkedList();
		Iterator iter = pOrder.getCommerceItems().iterator();
		while( iter.hasNext() ) {
			CommerceItem item = (CommerceItem)iter.next();
			getItemPromotions( item, pItemPromotions, pGetAdjustments );
		}
		return pItemPromotions;
	}

	/** Removes the promotion from the profile's active promotions and also removes it from the coupons.
	 * Finally it will reinitialize the pricingModelHolder if it was passed in.
	 * 
	 * @param profile
	 * @param promotionId
	 * @param removeAll
	 * @param pricingModelHolder
	 * @return
	 * @throws PromotionException */
	public boolean revokePromotion( Profile profile, String promotionId, boolean removeAll, PricingModelHolder pricingModelHolder ) throws PromotionException {
		boolean retVal = super.revokePromotion( profile.getRepositoryId(), promotionId, removeAll );

		// remove it from the grantedCoupon as well
		Set<RepositoryItem> grantedCoupons = (Set<RepositoryItem>)profile.getPropertyValue( getCurrentCouponsProperty() );
		if( grantedCoupons != null ) {
			RepositoryItem coupon = getAppliedCouponWithPromo( profile, promotionId );
			if( coupon != null ) {
				grantedCoupons.remove( coupon );
			}
		}

		if( pricingModelHolder != null ) {
			pricingModelHolder.initializeOrderPricingModels();
			pricingModelHolder.initializeShippingPricingModels();
			pricingModelHolder.initializeItemPricingModels();
		}

		return retVal;
	}

	public boolean isTargetedOffer( String couponCode ) {
		boolean targetedOffer = false;
		
		Set promotions = getPromotion( couponCode );
		if (promotions != null){
			Iterator promoIter = promotions.iterator();
			while(promoIter.hasNext()){
				RepositoryItem promotion = (RepositoryItem) promoIter.next();
				if( promotion != null ) {
					targetedOffer = Boolean.TRUE.equals( promotion.getPropertyValue( "targetedOffer" ) ); //MK:Revist this - Should we consider coupon targeted offer if any promotion in the coupon is restricted??
				}
			}
		}
		return targetedOffer;
	}

	public boolean validateTargettedOfferLists( String couponCode ) {
		Set promotions = getPromotion( couponCode );
		if (promotions != null){
			Iterator itPromos = promotions.iterator();
			while(itPromos.hasNext()){
				RepositoryItem promotion = (RepositoryItem) itPromos.next();
				if( promotion != null ) {
					Set<RepositoryItem> exclusionSet = (Set<RepositoryItem>)promotion.getPropertyValue( "excludedCollaterals" );
					Set<RepositoryItem> inclusionSet = (Set<RepositoryItem>)promotion.getPropertyValue( "includedCollaterals" );
		
					if( inclusionSet.isEmpty() && exclusionSet.isEmpty() ) { 
						return false;
						}
				}
			}
		}
		return true;
	}

	public boolean isTargetedCouponValidForProfile( Profile profile, String couponCode ) {
		boolean valid = false;
		// due to new v11 functionality, multiple promos can be tied to a single coupon. For now, we'll assume one valid promo is 
		// enough to set this flag
		Set promos = getPromotion( couponCode );
		if (promos !=null){
			Iterator promoIter = promos.iterator();
			while (promoIter.hasNext()){
				RepositoryItem promotion = (RepositoryItem) promoIter.next();
				if( promotion != null ) {
					valid = checkTargetedPromotionRule( profile, promotion );
					//if true, quit and return
					if(valid){
						return valid;
					}
				}
			}
		}
		return valid;
	}
	
	
	
	public Map<String, Boolean> isTieredCouponValidForProfile( Profile profile, String couponCode ) {
		Map<String, Boolean> tierCouponValidation=new HashMap();
		tierCouponValidation.put("VALID", Boolean.TRUE);
		Set promos = getPromotion( couponCode );
		for(Object po:promos){
			RepositoryItem promo = (RepositoryItem)po;
			String xmlrule = (String)promo.getPropertyValue(getPricingModelProperties().getPMDLRule());
			try {
				if (xmlrule.indexOf(START)==-1 || xmlrule.indexOf(END)+END.length()==-1){
					return tierCouponValidation;
				}
				String older = xmlrule.substring(xmlrule.indexOf(START), xmlrule.indexOf(END)+END.length());
				String profileQualifier = getDswPromotionProfileValidator().removeNoneProfileQualifier(older);

				if(profileQualifier == null){
					return tierCouponValidation;
				}

				PricingModelElem model = null;
				try(StringReader inreader = new StringReader(xmlrule.replace(older, profileQualifier)))
				{
					InputSource pin = new InputSource(inreader);
					model = (PricingModelElem)getDswPromotionProfileValidator().getPmdlParser().parse(pin);
				}

				if(model != null){
					QualifierElem qElem = model.getQualifier();
					Map pObjectBindings = new HashMap();
					pObjectBindings.put("profile", profile);
					if(!(boolean) (Boolean)qElem.evaluate(null, this, pObjectBindings)){
						String getValidationErrorCode=getLoyaltyTierCode(qElem, profile);
						logInfo(" PROFILE TIER VALUE ::: "+profile.getPropertyValue("loyaltyTier")+" :: While adding Promo :::  "+promo.getRepositoryId());
						tierCouponValidation.put(getValidationErrorCode, Boolean.FALSE);
						return tierCouponValidation;
					}
				}else{
					return tierCouponValidation;
				}

			} catch (SAXException | IOException | ParserConfigurationException
					| TransformerFactoryConfigurationError
					| TransformerException |PricingException e) {
				logError(e);
			} 
			return tierCouponValidation;

		}
		return tierCouponValidation;
	}
	

	private String getLoyaltyTierCode(QualifierElem qElem, Profile profile) {
		
		String code=null;
		boolean goldMember=false;
		boolean eliteMember=false;
		
		eliteMember=checkLoyaltyTier(qElem,profile,ATG_PROFILE_ELITE_LOYALTYTIER_PROPERTY);
		goldMember=checkLoyaltyTier(qElem,profile,ATG_PROFILE_GOLD_LOYALTYTIER_PROPERTY);
				
		if(goldMember){
			code="couponGoldMembersOnly";
		}
		
		if(eliteMember){
			code="couponEliteMembersOnly";
		}
		
		if(goldMember && eliteMember ){
			code="couponGoldEliteMembersOnly";
		}
		// TODO Auto-generated method stub
		return code;
	}


	private boolean checkLoyaltyTier(QualifierElem qElem, Profile profile, String tier) {
		
		Map pObjectBindings = new HashMap();
		
		profile.setPropertyValue(ATG_PROFILE_LOYALTYTIER_PROPERTY, tier);
		pObjectBindings.put(ATG_PROFILE_PROPERTY, profile);
		try {
			if((boolean) (Boolean)qElem.evaluate(null, this, pObjectBindings)){
				return true;
			}
		} catch (PricingException e) {
			logError("Error while checking the LoyaltyTier validation in method :: checkLoyaltyTier" +e);
		}
		// TODO Auto-generated method stub
		return false;
	}


	public Collection validateImplicitTargetedPromotions( Collection pPricingModels, RepositoryItem profile ) {

		Collection<RepositoryItem> priceModels = new ArrayList<>();

		if( pPricingModels != null ) {
			Iterator itr = pPricingModels.iterator();
			while( itr.hasNext() ) {
				RepositoryItem promotion = (RepositoryItem)itr.next();

				if( promotion != null ) {
					Boolean isImplicit = (Boolean)promotion.getPropertyValue( "global" );
					Boolean isTargeted = (Boolean)promotion.getPropertyValue( "targetedOffer" );

					if( isImplicit.booleanValue() && isTargeted.booleanValue() ) {
						boolean valid = false;
						Set<RepositoryItem> exclusionSet = (Set<RepositoryItem>)promotion.getPropertyValue( "excludedCollaterals" );
						Set<RepositoryItem> inclusionSet = (Set<RepositoryItem>)promotion.getPropertyValue( "includedCollaterals" );

						if( inclusionSet.isEmpty() && exclusionSet.isEmpty() ) {
							valid = false;
						} else {
							valid = checkTargetedPromotionRule( profile, promotion );
						}
						if( !valid ) {
							priceModels.add( promotion );
						}
					}
				}
			}
		}

		if(null!=pPricingModels){
		pPricingModels.removeAll( priceModels );
		}
		return pPricingModels;

	}

	public boolean isImplicitPromotion(RepositoryItem promotion){
		if(promotion != null) {
			return (Boolean) promotion.getPropertyValue("global");
		}else{
			throw new DigitalRuntimeException("promotion item can't be null");
		}
	}

	/** @param profile
	 * @param promotion
	 * @return */
	private boolean checkTargetedPromotionRule( RepositoryItem profile, RepositoryItem promotion ) {
		boolean valid = false;
		Set<RepositoryItem> exclusionSet = (Set<RepositoryItem>)promotion.getPropertyValue( "excludedCollaterals" );
		Set<RepositoryItem> inclusionSet = (Set<RepositoryItem>)promotion.getPropertyValue( "includedCollaterals" );

		if( inclusionSet.isEmpty() && !exclusionSet.isEmpty() ) {
			valid = true;
		}

		Set<RepositoryItem> userOffers = (Set<RepositoryItem>)profile.getPropertyValue( "offers" );

		for( RepositoryItem userOffer : userOffers ) {
			String collateralId = (String)userOffer.getPropertyValue( "collateralId" );

			if( exclusionSet.toString().toUpperCase().contains( collateralId.toUpperCase() ) ) {
				boolean confirmExclusion = false;
				Iterator exclusionIterator = exclusionSet.iterator();
				while( exclusionIterator.hasNext() ) {
					String exclCollateral = (String)exclusionIterator.next();
					if( exclCollateral.equalsIgnoreCase( collateralId ) ) {
						valid = false;
						confirmExclusion = true;
						break;
					}
				}
				if( !valid && confirmExclusion ) {
					break;
				}
			}
			if( inclusionSet.toString().toUpperCase().contains( collateralId.toUpperCase() ) ) {
				Iterator inclusionIterator = inclusionSet.iterator();
				while( inclusionIterator.hasNext() ) {
					String inclCollateral = (String)inclusionIterator.next();
					if( inclCollateral.equalsIgnoreCase( collateralId ) ) {
						valid = true;
					}
				}
			}
		}
		return valid;
	}
	
	/**
	 * @param collateralList
	 * @return
	 * @throws Exception
	 */
	public Map<String,RepositoryItem> retreiveTargetedPromotions(List<String> collateralList) {
		Map<String,RepositoryItem> collateralToTargetedPromosMap = new HashMap<>();
		List<RepositoryItem> targetedPromos = new ArrayList<>();
		try{
			Object[] params = new Object[collateralList.size()];
			
			StringBuilder query = new StringBuilder();
			String rqlQuery=" targetedOffer=true and giveToAnonymousProfiles=false and restrictionType !=\"CSC\" and includedCollaterals INCLUDES ANY { ";
			query.append(rqlQuery);
			for(int i=0;i<collateralList.size();i++){
				params[i]=collateralList.get(i);
				query.append("\"").append(params[i]).append("\"");
				if(i != (collateralList.size() - 1)){
					query.append(",");
				}
				
			}
			query.append(" }");
			
			Repository repository = getPromotions();
			RepositoryView repositoryView = repository.getView( "promotion" );
			RqlStatement rqlStatement = RqlStatement.parseRqlStatement( query.toString());
			
			RepositoryItem[] result = rqlStatement.executeQuery( repositoryView, null );
			if(result!=null){
				targetedPromos = new ArrayList<>(Arrays.asList(result));
			}
			for(String collateralId : collateralList){
				for(RepositoryItem targetedPromo:targetedPromos){
					Set<RepositoryItem> inclusionSet = (Set<RepositoryItem>)targetedPromo.getPropertyValue( "includedCollaterals" );
					if( inclusionSet.toString().toUpperCase().contains( collateralId.toUpperCase() ) ) {
						Iterator inclusionIterator = inclusionSet.iterator();
						while( inclusionIterator.hasNext() ) {
							String inclCollateral = (String)inclusionIterator.next();
							if( inclCollateral.equalsIgnoreCase( collateralId ) ) {
								String key = collateralId + "-" + targetedPromo.getRepositoryId();
								collateralToTargetedPromosMap.put(key, targetedPromo);								
							}
						}
					}
				}
			}
		}catch(Exception ex){
			this.logError("Error retrieveing targeted promotions", ex);
		}
		
		return collateralToTargetedPromosMap;
	}

	/**
	 *
	 * @param isValidLoyaltyMember
	 * @return List<Offer>
	 */
	public List<Offer> getWebAuthOffers(boolean isValidLoyaltyMember, Profile profile) throws DigitalAppException{

		List<Offer> offers = new ArrayList<>();

		final String METHOD_NAME = "getWebAuthOffers";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			Set<AppliedOffer.AppliedPromotion> activePromotions = new HashSet<>();

			List<Offer> profileOffers = this.getProfileTools().retrieveWebOffersFromProfile(profile, isValidLoyaltyMember);
			if(!profileOffers.isEmpty()){
				activePromotions = this.getProfileActivePromotions(profile);
			}else{
				return offers;
			}

			List<String> collateralList = new ArrayList<>();

			for(Offer offer : profileOffers ){
				String collateralId = offer.getCollateralId();
				collateralList.add(collateralId);
			}

			//find promotions associated with collateralId
			Map<String,RepositoryItem> targetedPromosMap = retreiveTargetedPromotions(collateralList);


			for(Offer offer : profileOffers ){

				String collateralId = offer.getCollateralId();
				Iterator<Map.Entry<String, RepositoryItem>> it = targetedPromosMap.entrySet().iterator();
				while(it.hasNext()){
					Map.Entry<String, RepositoryItem> entry =  (Map.Entry<String, RepositoryItem>) it.next();
					if(entry.getKey().contains(collateralId)){

						RepositoryItem targetedPromo = targetedPromosMap.get(entry.getKey());
						String promotionId = targetedPromo.getRepositoryId();

						RepositoryItem[] couponArray = getCouponsForPromotion(promotionId);

						if (couponArray != null) {
							Collection<RepositoryItem> allCoupons = Arrays.asList(couponArray);
							allCoupons = getWebOfferFilter().filterCollection(allCoupons, null, null);

							Offer newOffer = (Offer) SerializationUtils.clone(offer);
							newOffer.setPromoStatus(RewardsCertificateConstants.WebOfferStatus.INITIAL.getValue());
							// Find status of promotion - ACTIVE ( Applied,
							// it is in activePromotion list) / INIT (not
							// applied)
							for (AppliedOffer.AppliedPromotion activePromotion : activePromotions) {
								if (activePromotion.getPromotionId().equalsIgnoreCase(promotionId)) {
									newOffer.setPromoStatus(RewardsCertificateConstants.WebOfferStatus.APPLIED.getValue());
									break;
								}
							}

							String description = null;
							String userFriendlyDescription = null;
							String userFriendlyName = null;
							String discountIconType = null;
							String shopNowLink = null;
							String offerDetailsPageName = null;

							if (null != targetedPromo
									.getPropertyValue(getProfilePropertyManager().getPromotionDescPropertyName())) {
								description = (String) targetedPromo.getPropertyValue(
										getProfilePropertyManager().getPromotionDescPropertyName());
							}

							if (null != targetedPromo.getPropertyValue("userFriendlyDescriptionDefault")) {
								userFriendlyDescription = (String) targetedPromo
										.getPropertyValue("userFriendlyDescriptionDefault");
							}

							if (null != targetedPromo.getPropertyValue("userFriendlyNameDefault")) {
								userFriendlyName = (String) targetedPromo
										.getPropertyValue("userFriendlyNameDefault");
							}

							if (null != targetedPromo.getPropertyValue("discountIconType")) {
								discountIconType = (String) targetedPromo.getPropertyValue("discountIconType");
							}

							RepositoryItem shopNowLinkRepItem =  (RepositoryItem)targetedPromo.getPropertyValue("shopNowLink");
							if (shopNowLinkRepItem!=null){
								shopNowLink=(String)shopNowLinkRepItem.getPropertyValue("url");
							}

							if (null != targetedPromo.getPropertyValue("offerDetailsPageName")) {
								offerDetailsPageName = (String) targetedPromo.getPropertyValue("offerDetailsPageName");
							}

							if (DigitalStringUtil.isNotEmpty(promotionId)) {
								String couponCode = getCouponCodeForPromotion(promotionId);
								newOffer.setCouponCode(couponCode);
							}

							newOffer.setPromoCode(promotionId);
							newOffer.setPromoDescription(description);
							newOffer.setUserFriendlyDescription(userFriendlyDescription);
							newOffer.setUserFriendlyName(userFriendlyName);
							newOffer.setDiscountIconType(discountIconType);
							newOffer.setShopNowLink(shopNowLink);
							newOffer.setOfferDetailsPageName(offerDetailsPageName);

							//REW-1257
							Object displayOrder = targetedPromo.getPropertyValue("displayOrder");
							if(displayOrder instanceof Integer){
								newOffer.setDisplayOrder((Integer) displayOrder);
							}

							final boolean targetedOffer = Boolean.TRUE
									.equals(targetedPromo.getPropertyValue("targetedOffer"));

							newOffer.setTargetedOffer(targetedOffer);

							if (!allCoupons.isEmpty()) {
								offers.add(newOffer);
							}
							else if(RewardsCertificateConstants.WebOfferStatus.APPLIED.getValue().equalsIgnoreCase(newOffer.getPromoStatus())){
								offers.add(newOffer);
							}else {
								this.logDebug(
										"Coupon validation failed thus targeted offer cannot be granted as it is invalid for this user. Coupon Code = "
												+ newOffer.getCouponCode() + " Promotion Id = "
												+ newOffer.getPromoCode());
							}
						}
					}
				}
			}
		}catch(Exception ex) {
			if (isLoggingError()) {
				logError("Caught Exception in getWebAuthOffers " +ex.getMessage(), ex);
			}
			throw new DigitalAppException(ex);
		}finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
		return offers;
	}

	public void updatePromotionsForOrder(Order pOrder, RepositoryItem profile, Slot qualifierPromotions)
	{
		Collection currentPromotions = qualifierPromotions.getCurrentItems();

		List<PricingAdjustment> orderPromotions = new ArrayList<>();
	    List<PricingAdjustment> itemPromotions = new ArrayList<>();
	    List<PricingAdjustment> shippingPromotions = new ArrayList<>();
	    List<PricingAdjustment> taxPromotions = new ArrayList<>();
	    this.getOrderPromotions(pOrder, orderPromotions, itemPromotions, shippingPromotions, taxPromotions, true);
	    
	    List<PricingAdjustment> all = new ArrayList<>();
	    all.addAll(orderPromotions);
	    all.addAll(itemPromotions);
	    all.addAll(shippingPromotions);
	    all.addAll(taxPromotions);
	    
	    if(all.isEmpty() && (currentPromotions != null && !currentPromotions.isEmpty())){
	    	for(Object o : currentPromotions){
	    		this.sendPromotionRevokedEvent(profile, ((RepositoryItem)o));
	    	}
	    }
	    
		if (currentPromotions != null && !currentPromotions.isEmpty()) {
			for (Object o : currentPromotions) {
				boolean matched = false;
				for (PricingAdjustment pa : all) {
					if (((RepositoryItem) o).getRepositoryId().equals(
							pa.getPricingModel().getRepositoryId())) {
						matched = true;
					}
				}
				if (!matched) {
					this.sendPromotionRevokedEvent(profile,
							((RepositoryItem) o));
				}
			}
		}
	    
		if (currentPromotions != null && !currentPromotions.isEmpty()){
			for(PricingAdjustment pa : all){
				boolean matched = false;
				for(Object o : currentPromotions){
					if(((RepositoryItem)o).getRepositoryId().equals(pa.getPricingModel().getRepositoryId())){
						matched = true;
					}
				}
				if(!matched){
					this.sendPromotionGrantedEvent((MutableRepositoryItem)profile, ((RepositoryItem)pa.getPricingModel()));
				}
			}
		}else{
			for(PricingAdjustment pa : all){
				this.sendPromotionGrantedEvent((MutableRepositoryItem)profile, ((RepositoryItem)pa.getPricingModel()));
	    	}
		}
	
	}

	/**
	 *
	 * @return DigitalCommercePropertyManager
	 */
	public DigitalCommercePropertyManager getProfilePropertyManager(){
		return (DigitalCommercePropertyManager)this.getProfileTools().getPropertyManager();
	}



	/**
	 *
	 * @return Set<RepositoryItem>
	 */
	public Set<RepositoryItem> getProfileGrantedCoupons(Profile profile) {
		return getDefaultProfileGrantedCoupons(profile);
	}

	/**
	 *
	 * @return Set<RepositoryItem>
	 */
	protected Set<RepositoryItem> getDefaultProfileGrantedCoupons(Profile profile) {
		return getCurrentCoupons( profile );
	}

	private ValidatorFilter getWebOfferFilter(){
		return ComponentLookupUtil.lookupComponent(ComponentLookupUtil.WEB_OFFER_FILTER, ValidatorFilter.class);
	}


	public DigitalPromotionProfileValidator getDswPromotionProfileValidator() {
		
		if(null!=dswPromotionProfileValidator){
			return dswPromotionProfileValidator;
		}else{
			return ComponentLookupUtil.lookupComponent(ComponentLookupUtil.DSW_PROMOTION_PROFILE_VALIDATOR, DigitalPromotionProfileValidator.class);
		}
	}
}
