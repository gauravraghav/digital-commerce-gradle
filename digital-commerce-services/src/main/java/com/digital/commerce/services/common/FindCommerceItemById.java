package com.digital.commerce.services.common;

import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;

import atg.commerce.order.CommerceItem;

/** Predicate returns true if {@link atg.commerce.order.CommerceItem}'s identifier is equal to the identifier passed in the constructor.
 * 
 * @author kbajaj */
public class FindCommerceItemById implements DigitalPredicate<CommerceItem> {
	private final String	commerceItemId;

	public FindCommerceItemById( final String commerceItemId ) {
		this.commerceItemId = commerceItemId;
	}

	@Override
	public boolean apply( CommerceItem input ) {
		return DigitalStringUtil.equalsIgnoreCase( commerceItemId, input.getId() );
	}

}
