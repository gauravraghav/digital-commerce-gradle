package com.digital.commerce.services.pricing;

import static com.digital.commerce.common.util.ComponentLookupUtil.DSW_BASE_CONSTANTS;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.FilteredCommerceItem;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingContext;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.QualifiedItem;
import atg.commerce.pricing.Qualifier;
import atg.commerce.pricing.definition.DiscountStructure;
import atg.commerce.pricing.definition.MatchingObject;
import atg.commerce.promotion.PromotionAnalysisManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.ServletUtil;

import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalHardgoodShippingGroup;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import lombok.Getter;
import lombok.Setter;

import static com.digital.commerce.common.util.ComponentLookupUtil.COUPON_MESSAGE;

/* Created on Dec 17, 2007
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates */

/** This attempts to somehow determine the highest cost shipping group for the order.
 * It only checks {@link HardgoodShippingGroup} that are not for gift cards since gift
 * card shipping is charged by a different entity.
 * 
 * @author shirley.chen */
@SuppressWarnings({"rawtypes","unchecked","unused"})
@Getter
@Setter
public class DigitalQualifierService extends Qualifier {

	private DigitalPromotionExlcusionService	exclusionService;
	private DigitalShippingGroupManager			shippingGroupManager;
	private DigitalTieredPromoService			tieredPromoService;
	private List<String>					templateListToEvaluateTarget;


	public Collection findQualifyingItems(PricingContext pPricingContext, Map pExtraParametersMap) throws PricingException {
		if (isLoggingDebug()){
			logDebug("in DigitalQualifierService findQualifyingItems method during processing promo#"+pPricingContext.getPricingModel().getRepositoryId());
		}
		DigitalCouponMessage couponMessage = (DigitalCouponMessage) ComponentLookupUtil.lookupComponent(COUPON_MESSAGE);

		if (isLoggingDebug()){
			logDebug("In findQualifyingOrder method, processing the CouponMessage promo#"+couponMessage .getPromoId() +" processing the CouponCode# "+ couponMessage.getCouponCode());
		}
		Collection<QualifiedItem> qualifiedItems =  super.findQualifyingItems(pPricingContext, pExtraParametersMap);
		try {
			if (!pPricingContext.getPricingModel().getItemDescriptor().getItemDescriptorName().equals("closenessQualifier") && !(Boolean)pPricingContext.getPricingModel().getPropertyValue("global") &&
					(couponMessage.getPromoId()!=null && couponMessage.getPromoId().equals((String) pPricingContext.getPricingModel().getRepositoryId())))
			{
				String promoName = (String) pPricingContext.getPricingModel().getPropertyValue("displayName");
                couponMessage.setOfferDetailsPage((String) pPricingContext.getPricingModel().getPropertyValue("offerDetailsPageName"));
				if (qualifiedItems != null && qualifiedItems.size() > 0) {
                    couponMessage.setPromoApplied(true);
				} else {
                    couponMessage.setPromoApplied(false);
                    couponMessage.setErrorsCode("not_qualified_item_level");
				}

				if (isLoggingDebug()){
					logDebug("in DigitalQualifierService findQualifyingItems method, pricingModel matched. Setting the values to CouponMessage, promoName# "
							+promoName + " qualifiedItems present "+(qualifiedItems != null && qualifiedItems.size() > 0)
							+ " did promo applied "+ couponMessage.isPromoApplied());
				}
			}
		}
		catch (RepositoryException e) {
			logError(e);
		}
		return qualifiedItems;
	}
	
	public MatchingObject findQualifyingOrder(PricingContext pPricingContext,
			Map pExtraParametersMap) throws PricingException {
		if (isLoggingDebug()){
			logDebug("in DigitalQualifierService findQualifyingOrder method during processing promo#"+pPricingContext.getPricingModel().getRepositoryId());
		}
		// Initialize to empty exclusions on the order before each promotion qualification evaluation
		((DigitalOrderImpl)pPricingContext.getOrder()).setExclusionItem(new ArrayList<DigitalCommerceItem>());
		// Flag non-discountable items for pricing service
		List<DigitalCommerceItem> filteredItems = getExclusionService().getFilterDiscountableItems(pPricingContext.getPricingModel(), pPricingContext.getOrder());
		Set<String> promoExcludesInfo = getExclusionService().getPromoExcludesInfo();
		((DigitalOrderImpl) pPricingContext.getOrder()).setExclusionItem(filteredItems);
		
		DigitalCouponMessage couponMessage = (DigitalCouponMessage) ComponentLookupUtil.lookupComponent(COUPON_MESSAGE);

		if (isLoggingDebug()){
			logDebug("In findQualifyingOrder method, processing the CouponMessage promo#"+ couponMessage.getPromoId() +" processing the CouponCode# "+ couponMessage.getCouponCode());
		}
		MatchingObject matObj = super.findQualifyingOrder(pPricingContext, pExtraParametersMap);
		try {
			if (!pPricingContext.getPricingModel().getItemDescriptor().getItemDescriptorName().equals("closenessQualifier") && !(Boolean)pPricingContext.getPricingModel().getPropertyValue("global") &&
					(couponMessage.getPromoId()!=null && couponMessage.getPromoId().equals((String) pPricingContext.getPricingModel().getRepositoryId())))
			{
				String promoName = (String) pPricingContext.getPricingModel().getPropertyValue("displayName");
				couponMessage.setOfferDetailsPage((String) pPricingContext.getPricingModel().getPropertyValue("offerDetailsPageName"));
				if (filteredItems != null && filteredItems.size() > 0) {
					couponMessage.getExcludes().addAll(promoExcludesInfo);
				} 
				if (matObj == null  || (filteredItems!=null && filteredItems.size() == pPricingContext.getOrder().getCommerceItemCount())) {
					couponMessage.setPromoApplied(false);
					couponMessage.setErrorsCode("not_qualified_order_level");
				} else {
					couponMessage.setPromoApplied(true);
					if (promoExcludesInfo!=null && promoExcludesInfo.size()>0) {
						couponMessage.setErrorsCode("qualified_with_exclusions");
                    }
				}
				if (isLoggingDebug()){
					logDebug("in DigitalQualifierService findQualifyingOrder method, pricingModel matched. Setting the values to CouponMessage, promoName# "
							+promoName + " filteredItems present "+ (filteredItems != null && filteredItems.size() > 0)
							+ " did promo applied "+ couponMessage.isPromoApplied());
				}
			}
		}
			catch (RepositoryException e) {
				logError(e);
			}
		return matObj;
		
		/*if( ret != null && tieredPromoService.isTieredPromotion( pPricingContext.getPricingModel() ) ) {
			ret = tieredPromoService.evaluateTierPromo(pPricingContext.getItems(), pPricingContext.getPricingModel(), 
					pPricingContext.getProfile(), pPricingContext.getLocale(), pPricingContext.getOrder(), pPricingContext.getOrderPriceQuote(), pExtraParametersMap );
		}*/
		
		/**	Removed to fix SR-5003
		 * 	 try {
		if (ret !=null && filteredItems !=null && ret.getMatchingObject() instanceof DigitalOrderImpl && pPricingContext.getPricingModel().getItemDescriptor().getItemDescriptorName().equals("closenessQualifier")) {
			  Order obj = (Order)ret.getMatchingObject();
			  for (DigitalCommerceItem ci : filteredItems){
				 if(obj.getCommerceItems().contains(ci))
				 {
					 ret = null;

				 }
			  }
			 }
		} catch (RepositoryException e) {
			logError(e);
		}*/	
	}
	
	/**
	 *  Overriding this method to get the qualifiedItems for tier Order Qty promos. By default, for Order level promos, evaluateTarget method will not have pFilteredTargetItems (null).
	 *  The override will (Only for promos with template - /order/dswTieredOrderQtyDiscount.pmdt)
	 *   - get the filter commerce items and then call evaluateTarget passing the filteredItems, which will return qualified items.
	 *   - add the dswQualifiedTargetItems to pExtraParametersMap (which will be used in DigitalBulkQtyDiscountHelper)
	 *   - however still need to return MatchObject not List<MatchObject> , hence the response is derived from evaluateTarget with filteredItems 
	 */
	protected Object evaluateTarget(PricingContext pPricingContext, Map pExtraParametersMap, List pFilteredTargetItems) throws PricingException {
		RepositoryItem pricingModel = pPricingContext.getPricingModel();
		if(null == pFilteredTargetItems && null != pricingModel && !isClosenessQualifier(pricingModel)){
			try{
				String template = (String)pricingModel.getPropertyValue("template");
				List<String> templateList = getTemplateListToEvaluateTarget();
				if(templateList == null){
					//templateListToEvaluateTarget config is missing in QualifierServices. Defaulting to
					this.logError("templateListToEvaluateTarget config is missing in QualifierServices. Using Defaulting config");
					templateList = new ArrayList<>();
					templateList.add("/order/dswTieredOrderQtyDiscount.pmdt");
				}
				
				//if("/order/dswTieredOrderQtyDiscount.pmdt".equalsIgnoreCase(template)){
				if(templateList != null && templateList.contains(template)){
					ArrayList filteredQualifierItems = new ArrayList();
		        	// save the OriginalItemQuantities before calling evaluateTarget
		        	Map originalItemQuantities = this.saveOriginalItemQuantities(pPricingContext.getItems());
		        	 try{		        	 

						HashMap detailsTaggedToReceiveDiscount = new HashMap();
				        this.doFilters(1, pPricingContext, pExtraParametersMap, null, detailsTaggedToReceiveDiscount, filteredQualifierItems);
				        
				        if(filteredQualifierItems != null && filteredQualifierItems.size() > 0){
	
				        	 Object targetRet;
			        		 targetRet = super.evaluateTarget(pPricingContext, pExtraParametersMap, filteredQualifierItems);
			        		 if(targetRet instanceof List ){
			        			 ArrayList<QualifiedItem> qualifiedItems = new ArrayList<>();
			        	         HashMap<String, Map<String, QualifiedItem>> qualifiedItemsDetails = new HashMap<>();
			        	         this.processQualifiedItemTarget(targetRet, pPricingContext, pExtraParametersMap, qualifiedItems, qualifiedItemsDetails, detailsTaggedToReceiveDiscount);
			        			 pExtraParametersMap.put("dswQualifiedTargetItems", qualifiedItems);
			        			 List targetList = (List)targetRet;
			        			 MatchingObject mobj = new MatchingObject(null, 1);
			        			 
			        			 MatchingObject mobjFromTarget = (MatchingObject)targetList.get(0);
			        			 mobj.setDiscounts(mobjFromTarget.getDiscounts());
			        			 return mobj;
			        		 }
				        }
			        }finally{
		        		 // restore the OriginalItemQuantities before calling evaluateTarget as evaluateTarget might change quantities based on 
                       this.setOriginalItemQuantities(filteredQualifierItems, originalItemQuantities);
		        	}
				}
			}catch(Exception ex){
				this.logError(ex);
				//ignore the exception and proceed to default evaluateTarget logic
			}
		}
    return super.evaluateTarget(pPricingContext, pExtraParametersMap, pFilteredTargetItems);
	}

	protected void processQualifiedItemTarget( Object pTargetRet, PricingContext pPricingContext,
			Map pExtraParametersMap, Collection<QualifiedItem> pQualifiedItems, Map<String, Map<String, QualifiedItem>> pQualifiedItemsDetails,
			Map pDetailsTaggedToReceiveDiscount) throws PricingException {
		
		UUID targetSetID = UUID.randomUUID();

		if ((pTargetRet instanceof List)) {
			List<PromotionAnalysisManager.ItemDetails> items = new ArrayList();

			Iterator itemIterator = ((List) pTargetRet).iterator();

			while (itemIterator.hasNext()) {
				MatchingObject match = (MatchingObject) itemIterator.next();

				if ((match.getMatchingObject() instanceof FilteredCommerceItem)) {
					FilteredCommerceItem item = (FilteredCommerceItem) match.getMatchingObject();
					long matchingQuantity = match.getQuantity() == -1L ? item.getQuantity() : match.getQuantity();
					ItemPriceInfo itemPrice = item.getPriceQuote();

					if (isLoggingDebug()) {
						logDebug("matching item: " + ((FilteredCommerceItem) match.getMatchingObject()).getId());
						logDebug("quantity that matched: " + matchingQuantity);
					}

					Map<String, QualifiedItem> qualifiedItemDiscountMap = (Map) pQualifiedItemsDetails.get(item.getId());

					if (qualifiedItemDiscountMap == null) {
						qualifiedItemDiscountMap = new HashMap();
						pQualifiedItemsDetails.put(item.getId(), qualifiedItemDiscountMap);
					}

					String discountId = null;
					DiscountStructure discount = null;
					List<DiscountStructure> matchedDiscounts = match.getDiscounts();

					if ((matchedDiscounts == null) || (matchedDiscounts.size() <= 0)) {
						discountId = "null";
					} else {
						if ((matchedDiscounts.size() > 1) && (isLoggingDebug())) {
							logDebug("item promotion: " + pPricingContext.getPricingModel() + " is an item "
									+ "promotion but has a matching object with " + matchedDiscounts.size()
									+ " discounts. Only the first discount will be applied.");
						}

						discount = (DiscountStructure) matchedDiscounts.get(0);
						discountId = "" + discount.getDiscountIndex();
					}

					QualifiedItem qualifiedItem = (QualifiedItem) qualifiedItemDiscountMap.get(discountId);

					if (qualifiedItem == null) {
						qualifiedItem = new QualifiedItem(item);
						qualifiedItem.setTargetSetUUID(targetSetID);
						qualifiedItem.setDiscount(discount);
						qualifiedItem.setIteratorIndexMap(match.getIteratorIndexMap());
						qualifiedItemDiscountMap.put(discountId, qualifiedItem);
						qualifiedItem.setGroupIndexQuantityMap(match.getGroupIndexQuantityMap());
						qualifiedItemDiscountMap.put(discountId, qualifiedItem);
						pQualifiedItems.add(qualifiedItem);
					} else {
						int offerCount = qualifiedItem.getOfferCount();
						offerCount++;
						qualifiedItem.setOfferCount(offerCount);
					}

					Map qualifyingDetailsMap = null;

					qualifyingDetailsMap = tagDetailsAsDiscounted(itemPrice, item, matchingQuantity, qualifiedItem,
							pDetailsTaggedToReceiveDiscount);

					qualifiedItem.setQualifyingDetailsMap(qualifyingDetailsMap);

					if (isLoggingDebug()) {
						logDebug("updated the qualified item: " + qualifiedItem + " in the details map");
					}
				} else {
					if (isLoggingDebug()) {
						logDebug("matching object is not a FilteredCommerceItem.");
					}
					if ((match.getMatchingObject() instanceof DiscountStructure)) {
						QualifiedItem qualifiedItem = new QualifiedItem(null);
						qualifiedItem.setTargetSetUUID(targetSetID);
						qualifiedItem.setDiscount((DiscountStructure) match.getMatchingObject());
						pQualifiedItems.add(qualifiedItem);
					}
				}
			}
		} else if ((pTargetRet instanceof MatchingObject)) {
			MatchingObject match = (MatchingObject) pTargetRet;
			if ((match.getDiscounts() != null) && (match.getDiscounts().size() > 0)) {
				if (pQualifiedItems == null) {
					pQualifiedItems = new ArrayList();
				}
				QualifiedItem qualifiedItem = new QualifiedItem(null);
				qualifiedItem.setTargetSetUUID(targetSetID);
				qualifiedItem.setDiscount((DiscountStructure) match.getDiscounts().get(0));
				pQualifiedItems.add(qualifiedItem);
			}
		} else {
			throw new PricingException(MessageFormat.format(
					Constants.INVALID_TARGET_RETURN, new Object[] { pTargetRet.getClass().getName() }));
		}

		return;
	}
	
	
	private void setOriginalItemQuantities(List pItems, Map pOriginalItemQuantities) {
		if ((pItems == null) || (pOriginalItemQuantities == null)) {
			if (isLoggingDebug()) {
				logDebug("setOriginalItemQuantities: one of these is null:: items: "
						+ pItems + " original quantities: " + pOriginalItemQuantities);
			}
	
		} else {
			ListIterator it = pItems.listIterator();
			while (it.hasNext()) {
				FilteredCommerceItem item = (FilteredCommerceItem) it.next();
				Long originalItemQuantity = (Long) pOriginalItemQuantities.get(item);
	
				if (originalItemQuantity != null) {
					if (isLoggingDebug()) {
						logDebug("setOriginalItemQuantities: setting quantity of item: " + item.getId()
								+ " to: " + originalItemQuantity);
					}
	
					item.setQuantity(originalItemQuantity.longValue());
				}
			}
		}
		return;
	}
	
	private Map saveOriginalItemQuantities(List pItems) {
		Map originalItemQuantities = null;
		Iterator itr;
		if (pItems != null) {
			originalItemQuantities = new HashMap();
			itr = pItems.iterator();
			while (itr.hasNext()) {
				FilteredCommerceItem item = (FilteredCommerceItem) itr.next();
				originalItemQuantities.put(item,Long.valueOf(item.getQuantity()));
			}
		}
		return originalItemQuantities;
	}
	
	public MatchingObject findQualifyingShipping(PricingContext pPricingContext, Map pExtraParametersMap)  throws PricingException {

		// Flag non-discountable items for pricing service
		List filteredItems = getExclusionService().getFilterDiscountableItems(pPricingContext.getPricingModel(), pPricingContext.getOrder());
		((DigitalOrderImpl)pPricingContext.getOrder()).setExclusionItem(filteredItems);
		MatchingObject ret = super.findQualifyingShipping(pPricingContext, pExtraParametersMap);
		 if (ret !=null && filteredItems !=null && ret.getMatchingObject() instanceof DigitalHardgoodShippingGroup ) {
			DigitalHardgoodShippingGroup sg =(DigitalHardgoodShippingGroup) ret.getMatchingObject();
			if (sg!=null){
		        List<ShippingGroupCommerceItemRelationship> relationships = sg.getCommerceItemRelationships();
		        for(ShippingGroupCommerceItemRelationship relationship : relationships){
              DigitalCommerceItem  ci = (DigitalCommerceItem) ((ShippingGroupCommerceItemRelationship)relationship)
                  .getCommerceItem();
	               if (filteredItems.contains(ci)){	            	   
	            	   ret =null; 
	               }
		        }
			}
		 }

		return ret;
	}
}
