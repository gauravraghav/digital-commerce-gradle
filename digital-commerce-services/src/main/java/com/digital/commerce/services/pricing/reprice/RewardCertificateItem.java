package com.digital.commerce.services.pricing.reprice;

import lombok.Getter;
import lombok.Setter;

/*  */
@Getter
@Setter
public class RewardCertificateItem {

	private String	certNumber;
	private double	certAmountUsed;
}
