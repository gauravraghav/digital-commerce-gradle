package com.digital.commerce.services.order.payment.paypal.valueobject;

import java.io.Serializable;

import com.digital.commerce.services.common.AddressTypeDetails;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;

import atg.commerce.order.ShippingGroup;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaypalAuthenticateResponse extends PaypalResponse implements Serializable{

	private static final long serialVersionUID = -3281808700058789030L;
    private String paypalPayerId;
    private String paypalEmail;
    private String firstName;
    private String lastName;
    private ShippingGroup shippingGroup;
    private PaypalPayment paypalGroup;
    private boolean internationalShippingAddress = false;
    private boolean shippingAddressValidationError = false;
    private AddressTypeDetails shippingAddress;
	private AddressTypeDetails billingAddress;
}
