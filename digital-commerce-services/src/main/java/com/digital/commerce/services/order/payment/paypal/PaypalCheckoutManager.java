package com.digital.commerce.services.order.payment.paypal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.transaction.TransactionManager;

import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.SessionAttributesConstant;
import com.digital.commerce.constants.PaypalConstants;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.payment.PaymentService;
import com.digital.commerce.integration.payment.cardinal.domain.PaymentAddress;
import com.digital.commerce.integration.payment.cardinal.domain.PaymentServiceRequest;
import com.digital.commerce.integration.payment.cardinal.domain.PaymentServiceResponse;
import com.digital.commerce.integration.payment.cardinal.domain.PaymentServiceResponse.PAResStatus;
import com.digital.commerce.services.common.AddressType;
import com.digital.commerce.services.common.AddressTypeDetails;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.order.payment.paypal.valueobject.PaypalAuthenticateResponse;
import com.digital.commerce.services.order.payment.paypal.valueobject.PaypalAuthorizeResponse;
import com.digital.commerce.services.order.payment.paypal.valueobject.PaypalLookupResponse;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShipType;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShippingGroupPropertyManager;
import com.digital.commerce.services.profile.DigitalProfileTools;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.OrderPriceInfo;
import atg.core.util.ContactInfo;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaypalCheckoutManager extends GenericService {
	
	static final String CLASSNAME = PaypalCheckoutManager.class.getName();
	
	private TransactionManager transactionManager;
	private String defaultPaypalConsumerMessage;
	private PaypalTools paypalTools;
	private DigitalProfileTools profileTools;
	private PaymentService paypalService;

	public PaymentService getPaypalService() {
		return paypalService;
	}

	public void setPaypalService(PaymentService paypalService) {
		this.paypalService = paypalService;
	}

	private OrderManager orderManager;

	public OrderManager getOrderManager() {
		return orderManager;
	}

	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	public PaypalLookupResponse doPaypalLookup(Order order, Profile profile, boolean expressCheckout) {
		final String METHOD_NAME = "doPaypalLookup";
		PaymentServiceRequest paypalRequest = new PaymentServiceRequest();
		PaypalLookupResponse lookupResponse = new PaypalLookupResponse();
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(getName(), METHOD_NAME);
			
			// If PayPal lookup / authentication is already done, skip it and take
			// user directly to the Payment page
			final DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			if (request != null) {
				Object payPalEmail = request.getSession().getAttribute(SessionAttributesConstant.PAYPAL_EMAIL.getValue());
				if (payPalEmail != null && !((String) payPalEmail).equals("")) {
					//Existing user with Paypal
					lookupResponse.setSuccess(true);
					lookupResponse.setStatusCode("existing");
					return lookupResponse;
				}
			}else if(!getPaypalService().isServiceEnabled() || 
					!getPaypalService().isServiceMethodEnabled(PaymentService.ServiceMethod.LOOKUP.getServiceMethodName())){
				lookupResponse.setSuccess(false);
				lookupResponse.setStatusCode(PaypalConstants.PAYPAL_PAYMENT_UNAVAILABLE);
				logInfo( "Paypal Lookup service inactive at Platform Service");
				return lookupResponse;
			}
			
			logInfo( "Paypal Lookup Msg for Order:" + order.getId() );
			
			//Determine guest user flag based on on the session login info 
			boolean loggedInUser = getProfileTools().isKnown(profile);
			
			setPayPalLookupRequest(paypalRequest, order, profile, loggedInUser, expressCheckout);
			
			PaymentServiceResponse paypalLookupRes = getPaypalService().lookUp(paypalRequest);
			lookupResponse = transformLookupResponse(paypalLookupRes);
			if (PaypalConstants.PAYPAL_PAYMENT_AVAILABLE.equals(lookupResponse.getEnrolled())) {
				lookupResponse.setSuccess(true);
				logInfo( "Paypal Lookup Msg is Successful" );
				logInfo( "Paypal Cardinal OrderId:" + lookupResponse.getCardinalOrderId() );
	    		DigitalCommonUtil.setSessionAttributeValue(SessionAttributesConstant.PAYPAL_CARDINAL_ORDER_ID, 
	    				lookupResponse.getCardinalOrderId());
	    		DigitalCommonUtil.setSessionAttributeValue(SessionAttributesConstant.PAYPAL_ACS_URL,
	    				lookupResponse.getAcsUrl());
	    		DigitalCommonUtil.setSessionAttributeValue(SessionAttributesConstant.PAYPAL_PAYLOAD, 
	    				lookupResponse.getPayload());
	    		if(null!=request){
	    		DigitalCommonUtil.setSessionAttributeValue(SessionAttributesConstant.DSW_SESSION_ID, 
	    				request.getSession().getId());
	    		}
	    		DigitalCommonUtil.setSessionAttributeValue(SessionAttributesConstant.PAYPAL_TRANSACTION_ID, 
	    				paypalLookupRes.getTransactionId());
			}else{
				lookupResponse.setSuccess(false);
				logInfo( "Paypal Lookup Msg Error:" + lookupResponse.getErrorNo()+"::"+lookupResponse.getErrorDesc()
						+"::"+lookupResponse.getReasonDesc() );
			}
			
		} catch(DigitalIntegrationInactiveException ex){
			logError("paypallookup :: Exception from Integration Service during :: ", ex);
			lookupResponse.setSuccess(false);
			lookupResponse.setStatusCode(PaypalConstants.PAYPAL_PAYMENT_UNAVAILABLE);
			logInfo( "Paypal Lookup service inactive at Platform Service");
        }  catch (DigitalIntegrationException eX) {
			logError("paypallookup:: Exception from Integration Service during :: ", eX);
			lookupResponse = new PaypalLookupResponse();
			lookupResponse.setSuccess(false);
		} catch (Exception rEx){
			logError(rEx);
			lookupResponse = new PaypalLookupResponse();
			lookupResponse.setSuccess(false);
		} finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(getName(), METHOD_NAME);
		}
		return lookupResponse;
	}
	
	protected PaypalLookupResponse transformLookupResponse(PaymentServiceResponse paypalLookupRes){
		PaypalLookupResponse lookupResponse = new PaypalLookupResponse();
		lookupResponse.setCardinalOrderId(paypalLookupRes.getOrderId());
		lookupResponse.setAcsUrl(paypalLookupRes.getAcsUrl());
		lookupResponse.setEnrolled(paypalLookupRes.getEnrolled());
		lookupResponse.setPayload(paypalLookupRes.getPayload());
		lookupResponse.setErrorNo(paypalLookupRes.getErrorNo());
		lookupResponse.setErrorDesc(paypalLookupRes.getErrorDesc());
		lookupResponse.setReasonCode(paypalLookupRes.getReasonCode());
		lookupResponse.setReasonDesc(paypalLookupRes.getReasonDesc());		
		return lookupResponse;
	}
	
	protected void setPayPalLookupRequest(PaymentServiceRequest paypalRequest,  Order order, Profile profile,
			boolean loggedInUser, boolean expressCheckout) throws CommerceException{
		//Set address from profile for lookup call, or guest regular checkout
		setPaypalAddressOverrides(paypalRequest,order,profile,loggedInUser,expressCheckout);			
		
		logInfo( "Paypal Lookup Msg for Order:" + order.getId() );
		//get remote ipaddress
    	paypalRequest.setIpAddress(DigitalCommonUtil.getRemoteIpAddress());
			
		paypalRequest.setOrderNumber(order.getId());
		paypalRequest.setCurrencyCode(PaypalConstants.PAYPAL_USD_CURRENCY_CODE);
		paypalRequest.setTransactionMode(PaymentServiceRequest.TransactionMode.E_COMMERCE);

		paypalRequest.setOrderDescription(order.getDescription());
    	double orderTotal = ((DigitalOrderTools)this.getOrderManager().getOrderTools()).calculateOrderTotal(order);
    	paypalRequest.setAmount(DigitalCommonUtil.formatAmount(orderTotal));
    	paypalRequest.setItemAmount(paypalRequest.getAmount());
    	// if bostis - value should be true;
    	paypalRequest.setNoShipping(((DigitalOrderImpl)order).isNonShippingOrder());
    	
	}

	public PaypalAuthenticateResponse doPaypalAuthenticate(Order order, Profile profile)
			throws CommerceException {
		
		final String METHOD_NAME = "doPaypalAuthenticate";
		PaypalAuthenticateResponse authenticateResponse = null;
		PaymentServiceRequest req = new PaymentServiceRequest();
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(getName(), METHOD_NAME);
			if(!getPaypalService().isServiceEnabled() || 
					!getPaypalService().isServiceMethodEnabled(PaymentService.ServiceMethod.AUTHENTICATE.getServiceMethodName())){
				authenticateResponse = new PaypalAuthenticateResponse();
				authenticateResponse.setSuccess(false);
				authenticateResponse.setStatusCode(PaypalConstants.AUTHENTICATE_UNAVAILABLE_STATUS);
				logInfo( "Paypal Authenticate is service inactive at Platform Service");
				return authenticateResponse;
			}else{
				logInfo( "Paypal Authenticate Msg for Order:" + order.getId() );
				String cardinalOrderId = DigitalCommonUtil.getSessionAttributeValue(SessionAttributesConstant.PAYPAL_CARDINAL_ORDER_ID);
		        cardinalOrderId = cardinalOrderId!=null?cardinalOrderId:"";
		        req.setOrderId(cardinalOrderId);
		        
		        String ppPayload = DigitalCommonUtil.getSessionAttributeValue(SessionAttributesConstant.PAYPAL_PAYLOAD);
				req.setPayload(ppPayload);

				String transId = DigitalCommonUtil.getSessionAttributeValue(SessionAttributesConstant.PAYPAL_TRANSACTION_ID);
				req.setTransactionId(transId);
				
	        	PaymentServiceResponse paypalAuthentRes = this.getPaypalService().authenticate(req);
	        	authenticateResponse = transformAuthenticateResponse(paypalAuthentRes);
			}

			// Update order with PayPal payment
			if (PaypalConstants.AUTHENTICATE_SUCCESS_STATUS.equals(
					authenticateResponse.getStatusCode())) {

				logInfo( "Paypal Authenticate Msg is Successful" );
				logInfo( "Paypal Cardinal OrderId:" + authenticateResponse.getCardinalOrderId() );

				getPaypalTools().updateOrderData(order, authenticateResponse, profile);
				boolean validOrderAddr = getPaypalTools().isValidShippingAddressFromOrder(order);
				if(!validOrderAddr && getPaypalTools().isPaypalInternationalShippingAddress(authenticateResponse)){
					authenticateResponse.setStatusCode(PaypalConstants.AUTHENTICATE_INT_ADDR_ERROR_STATUS);
					authenticateResponse.setSuccess(false);
				}else{
					authenticateResponse.setSuccess(true);
				}
				
			}else if(!PaypalConstants.AUTHENTICATE_CANCEL_STATUS.equals(
					authenticateResponse.getStatusCode())){
				authenticateResponse.setSuccess(false);
				authenticateResponse.setStatusCode(PaypalConstants.AUTHENTICATE_ERROR_STATUS);
				logInfo( "Paypal Authenticate Msg Error:" + authenticateResponse.getErrorNo()+":"+authenticateResponse.getErrorDesc() );

			}
		} catch(DigitalIntegrationInactiveException ex){
			logError("paypalAuthencation :: Exception from Integration Service during :: ", ex);
			authenticateResponse = new PaypalAuthenticateResponse();
			authenticateResponse.setSuccess(false);
			authenticateResponse.setStatusCode(PaypalConstants.AUTHENTICATE_UNAVAILABLE_STATUS);
			logInfo( "Paypal Authenticate is service inactive at Platform Service");        	
        } catch (DigitalIntegrationException eX) {
			logError("paypalAuthencation :: Exception from Integration Service during :: ", eX); 
			authenticateResponse = new PaypalAuthenticateResponse();
			authenticateResponse.setStatusCode(PaypalConstants.AUTHENTICATE_ERROR_STATUS);
			authenticateResponse.setSuccess(false);
		} catch(Exception ex){
			logError(ex);
			authenticateResponse = new PaypalAuthenticateResponse();
			authenticateResponse.setStatusCode(PaypalConstants.AUTHENTICATE_ERROR_STATUS);
			authenticateResponse.setSuccess(false);
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(getName(), METHOD_NAME);
		}

		return authenticateResponse;
	}
	
	protected PaypalAuthenticateResponse transformAuthenticateResponse(PaymentServiceResponse paypalAuthentRes){
		PaypalAuthenticateResponse authenticateResponse = new PaypalAuthenticateResponse();
		authenticateResponse.setCardinalOrderId(paypalAuthentRes.getOrderId());
		authenticateResponse.setErrorNo(paypalAuthentRes.getErrorNo());
		authenticateResponse.setErrorDesc(paypalAuthentRes.getErrorDesc());
		authenticateResponse.setReasonCode(paypalAuthentRes.getReasonCode());
		authenticateResponse.setReasonDesc(paypalAuthentRes.getReasonDesc());
		PAResStatus resStatus = paypalAuthentRes.getPAResStatus();
		if(null != resStatus){
			authenticateResponse.setStatusCode(resStatus.getValue());
		}else{
			authenticateResponse.setStatusCode(PAResStatus.E.getValue());
		}
    	authenticateResponse.setPaypalPayerId(paypalAuthentRes.getProcessorUserId());
    	authenticateResponse.setPaypalEmail(paypalAuthentRes.getProcessorUserEmail()); 
    	authenticateResponse.setFirstName(paypalAuthentRes.getProcessorUserFirstName());
    	authenticateResponse.setLastName(paypalAuthentRes.getProcessorUserLastName());	
    	
    	//Shipping Address
    	AddressTypeDetails shippingAddress = new AddressTypeDetails();
    	shippingAddress.setAddr1(paypalAuthentRes.getProcessorShippingAddress1());
    	shippingAddress.setAddr2(paypalAuthentRes.getProcessorShippingAddress2());
    	shippingAddress.setCity(paypalAuthentRes.getProcessorShippingCity());
    	shippingAddress.setState(paypalAuthentRes.getProcessorShippingState());
    	shippingAddress.setCountry(paypalAuthentRes.getProcessorShippingCountryCode());
    	shippingAddress.setZip(paypalAuthentRes.getProcessorShippingPostalCode());
    	shippingAddress.setPhone(paypalAuthentRes.getProcessorShippingPhone());
    	shippingAddress.setName(paypalAuthentRes.getProcessorShippingFullName());
    	authenticateResponse.setShippingAddress(shippingAddress);
    	//Billing Address
    	AddressTypeDetails billingAddress = new AddressTypeDetails();
	    	String address1=paypalAuthentRes.getProcessorBillingAddress1();
	  	if(address1!=null){
	  		address1 = address1.substring(0, Math.min(address1.length(), 50));
	 	}
	 	billingAddress.setAddr1(address1);	
    	billingAddress.setAddr2(paypalAuthentRes.getProcessorBillingAddress2());
    	billingAddress.setCity(paypalAuthentRes.getProcessorBillingCity());
    	billingAddress.setState(paypalAuthentRes.getProcessorBillingState());
    	billingAddress.setCountry(paypalAuthentRes.getProcessorBillingCountryCode());
    	billingAddress.setZip(paypalAuthentRes.getProcessorBillingPostalCode());
    	billingAddress.setPhone(paypalAuthentRes.getProcessorBillingPhone());
    	billingAddress.setName(paypalAuthentRes.getProcessorBillingFullName());
    	authenticateResponse.setBillingAddress(billingAddress);
    	
		return authenticateResponse;
	}
	
	private void setPaypalAddressOverrides(PaymentServiceRequest req, Order order, 
			Profile profile, boolean loggedInUser, boolean expressCheckout)throws CommerceException {
		
		if(!loggedInUser && expressCheckout){
    		req.setForceAddress(PaypalConstants.FORCE_ADDRESS_YES);
    		req.setOverrideAddress(PaypalConstants.OVERRIDE_ADDRESS_NO);			
			// guest user and express checkout - NO address overrides, 
			//take shipping and billing the address from Paypal
			return;
		}else{
			// logged-in user and express checkout - check for address overrides
			// logged-in user and regular checkout - check for address overrides
			// guest user and regular checkout - check for address overrides			
		}

		try {
			//get shipping address from current Order or from profile default shipping address if no shipping address in order
			// NOTE: no address validation flag is checked while getting the shipping address
			ContactInfo shippingAddress = this.getPaypalTools().getShippingAddress(order, profile);
			if(null != shippingAddress ){
				PaymentAddress paypalShipAddress = getPaypalAddress(shippingAddress);
				req.setShippingAddress(paypalShipAddress);	
				req.setForceAddress(PaypalConstants.FORCE_ADDRESS_NO);
				req.setOverrideAddress(PaypalConstants.OVERRIDE_ADDRESS_YES);
			}else{
        		req.setForceAddress(PaypalConstants.FORCE_ADDRESS_YES);
        		req.setOverrideAddress(PaypalConstants.OVERRIDE_ADDRESS_NO);				
			}
			

			//Set billing address from order				
			ContactInfo billingAddr = this.getPaypalTools().getBillingAddress(order, profile);
			if(null != billingAddr){
				PaymentAddress paypalBillAddress = getPaypalAddress(billingAddr);
				req.setBillingAddress(paypalBillAddress);
				req.setEmail(billingAddr.getEmail());
			}
			
		} catch (CommerceException cEx) {
			throw cEx;
		}  
	}
	
	public PaypalAuthorizeResponse order( PaypalPaymentInfo paypalPaymentInfo, Order pOrder) throws DigitalIntegrationException{
		final String METHOD_NAME = "order";
		PaypalAuthorizeResponse authorizeResponse = new PaypalAuthorizeResponse();
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(getName(), METHOD_NAME);		

			if (this.isLoggingDebug()){
				logDebug("PaypalCheckoutService: order(): - using web services");
			}
	    	if(!getPaypalService().isServiceEnabled() || 
					!getPaypalService().isServiceMethodEnabled(PaymentService.ServiceMethod.ORDER.getServiceMethodName())){
	    		authorizeResponse.setSuccess(false);
	    		authorizeResponse.setStatusCode(PaypalConstants.AUTHORIZE_UNAVAILABLE_STATUS);
				logInfo( "Paypal Order service is inactive at Platform Service");
				return authorizeResponse;
			}		

        	PaymentServiceRequest paypalOrderReq = new PaymentServiceRequest();
        	paypalOrderReq.setOrderNumber(pOrder.getId());
        	paypalOrderReq.setOrderId(paypalPaymentInfo.getCardinalOrderId());
        	paypalOrderReq.setCurrencyCode(PaypalConstants.PAYPAL_USD_CURRENCY_CODE);
        	paypalOrderReq.setIpAddress(DigitalCommonUtil.getRemoteIpAddress());
        	paypalOrderReq.setOrderDescription(pOrder.getDescription());
        
        	//Order total
        	paypalOrderReq.setAmount(DigitalCommonUtil.formatAmount(paypalPaymentInfo.getAmount()));
        	paypalOrderReq.setItemAmount(paypalOrderReq.getAmount());
        	//Shipping address
			ContactInfo shippingAddress = (ContactInfo)this.getShippingAddressFromOrderForPaypal(pOrder);
			if(shippingAddress!=null){
				PaymentAddress addr = getPaypalAddress(shippingAddress);
				paypalOrderReq.setShippingAddress(addr);
			}
        	//Billing address
			ContactInfo billingAddress = (ContactInfo)this.getBillingAddressFromOrder(pOrder);
			if(billingAddress!=null){
				PaymentAddress addr = getPaypalAddress(billingAddress);
				paypalOrderReq.setBillingAddress(addr);
				paypalOrderReq.setEmail(billingAddress.getEmail());
			}
			paypalOrderReq.setTransactionId(DigitalCommonUtil.getSessionAttributeValue(SessionAttributesConstant.PAYPAL_TRANSACTION_ID));
			// Do lookup call
			PaymentServiceResponse paypalOrderRes =  this.getPaypalService().order(paypalOrderReq);
			
			//Set the response
			authorizeResponse.setCardinalOrderId(paypalOrderRes.getOrderId());
			authorizeResponse.setStatusCode(paypalOrderRes.getStatusCode());
			authorizeResponse.setErrorNo(paypalOrderRes.getErrorNo());
			authorizeResponse.setErrorDesc(paypalOrderRes.getErrorDesc());
			authorizeResponse.setReasonCode(paypalOrderRes.getReasonCode());
			authorizeResponse.setReasonDesc(paypalOrderRes.getReasonDesc());
        }catch(DigitalIntegrationInactiveException ex){
    		authorizeResponse.setSuccess(false);
    		authorizeResponse.setStatusCode(PaypalConstants.AUTHORIZE_UNAVAILABLE_STATUS);
			logInfo( "Paypal Order service is inactive at Platform Service", ex);
			return authorizeResponse;        	
        }catch (DigitalIntegrationException e){   
        	this.logError(e);
        	throw e;
        }finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(getName(), METHOD_NAME);
		}
		return authorizeResponse;
	}
	
	public PaypalAuthorizeResponse authorize(PaypalPaymentInfo paypalPaymentInfo, Order pOrder) throws DigitalIntegrationException{
		final String METHOD_NAME = "authorize";
		PaypalAuthorizeResponse authorizeResponse = new PaypalAuthorizeResponse();
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(getName(), METHOD_NAME);		
		

			if (this.isLoggingDebug()){
				logDebug("PaypalCheckoutService: authorize(): - using web services");
			}
			if(!getPaypalService().isServiceEnabled() || 
					!getPaypalService().isServiceMethodEnabled(PaymentService.ServiceMethod.AUTHORIZE.getServiceMethodName())){
	    		authorizeResponse.setSuccess(false);
	    		authorizeResponse.setStatusCode(PaypalConstants.AUTHORIZE_UNAVAILABLE_STATUS);
				logInfo( "Paypal authorize service is inactive at Platform Service");
				return authorizeResponse;
			}
        	
			PaymentServiceRequest paypalAuthrReq = new PaymentServiceRequest();
        	paypalAuthrReq.setOrderNumber(pOrder.getId());
        	paypalAuthrReq.setOrderId(paypalPaymentInfo.getCardinalOrderId());
        	paypalAuthrReq.setCurrencyCode(PaypalConstants.PAYPAL_USD_CURRENCY_CODE);
        	paypalAuthrReq.setIpAddress(DigitalCommonUtil.getRemoteIpAddress());
        	paypalAuthrReq.setOrderDescription(pOrder.getDescription());

        	//Order total
        	//double orderTotal = getOrderTools().calculateOrderTotal(pOrder);
        	paypalAuthrReq.setAmount(DigitalCommonUtil.formatAmount(paypalPaymentInfo.getAmount()));
        	paypalAuthrReq.setItemAmount(paypalAuthrReq.getAmount());
        	//Shipping address
			ContactInfo shippingAddress = (ContactInfo)this.getShippingAddressFromOrderForPaypal(pOrder);
			if(shippingAddress!=null){
				PaymentAddress addr = getPaypalAddress(shippingAddress);
				paypalAuthrReq.setShippingAddress(addr);
			}
        	//Billing address
			ContactInfo billingAddress = (ContactInfo)this.getBillingAddressFromOrder(pOrder);
			if(billingAddress!=null){
				PaymentAddress addr = getPaypalAddress(billingAddress);
				paypalAuthrReq.setBillingAddress(addr);
				paypalAuthrReq.setEmail(billingAddress.getEmail());
			}
			
			paypalAuthrReq.setTransactionId(DigitalCommonUtil.getSessionAttributeValue(SessionAttributesConstant.PAYPAL_TRANSACTION_ID));
			
			// Do lookup call
			PaymentServiceResponse paypalAuthRes = this.getPaypalService().authorize(paypalAuthrReq);
			//Set the response
			authorizeResponse.setStatusCode(paypalAuthRes.getStatusCode());
			authorizeResponse.setAuthorizationCode(paypalAuthRes.getAuthorizationCode());
			authorizeResponse.setEligibleForProtection(paypalAuthRes.getEligibleForProtection());
			authorizeResponse.setCardinalOrderId(paypalAuthRes.getOrderId());
			authorizeResponse.setErrorNo(paypalAuthRes.getErrorNo());
			authorizeResponse.setErrorDesc(paypalAuthRes.getErrorDesc());
			authorizeResponse.setReasonCode(paypalAuthRes.getReasonCode());
			authorizeResponse.setReasonDesc(paypalAuthRes.getReasonDesc());
 
        }catch(DigitalIntegrationInactiveException ex){
    		authorizeResponse.setSuccess(false);
    		authorizeResponse.setStatusCode(PaypalConstants.AUTHORIZE_UNAVAILABLE_STATUS);
			logInfo( "Paypal authorize service is inactive at Platform Service", ex);
			return authorizeResponse;        	
        }catch (DigitalIntegrationException e){   
        	this.logError(e);
        	throw e;
        }finally{
        	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
        }
		return authorizeResponse;
	}
	
	private PaymentAddress getPaypalAddress(ContactInfo address){
		PaymentAddress paypalAddress = new PaymentAddress();
		if(address!=null){
			paypalAddress.setFirstName(address.getFirstName());
			paypalAddress.setLastName(address.getLastName());
			paypalAddress.setAddress1(address.getAddress1());
			paypalAddress.setAddress2(address.getAddress2());
			paypalAddress.setCity(address.getCity());
			paypalAddress.setState(address.getState());
			paypalAddress.setCountryCode(DigitalCommonUtil.getCountryCode(address.getCountry()));
			paypalAddress.setPostalCode(address.getPostalCode());
			paypalAddress.setPhone(address.getPhoneNumber());
		}
		
		return paypalAddress;
	}
	
	/** Gets Shipping & Billing address from order
	 * 
	 * @param authRes
	 * @return */
	private ContactInfo getShippingAddressFromOrderForPaypal(Order order){
		ContactInfo addr = null;
		//Shipping Address
		if (getOrderManager().getShippingGroupManager()
				.isAnyHardgoodShippingGroups(order)) {
			
			addr = getShippingAddressForPayPal(order);
		}

		return addr;
	}
	
	/** Gets Shipping & Billing address from order
	 * 
	 * @param authRes
	 * @return */
	@SuppressWarnings("unchecked")
	private ContactInfo getBillingAddressFromOrder(Order order){
		ContactInfo addr = null;
		//Billing address
		List<PaymentGroup> paymentGroupList = order.getPaymentGroups();
		if( paymentGroupList != null ) {
			for( PaymentGroup pg : paymentGroupList) {
				if( pg instanceof PaypalPayment ) {
					PaypalPayment paypalPayment= (PaypalPayment)pg;
					addr = (ContactInfo)paypalPayment.getBillingAddress();
				}
			}
		}

		return addr;
	}
	
	/**
	 * Finds the right shipping group based on amount, address type and store name.
	 * 
	 * Also, if the shipping group's address is a BOPIS / BOSTS, 
	 * updates name by prefixing "S2S" for PayPal's seller protection
	 * 
	 * @param Current order object
	 * @return ContactInfo to be used as shipping address for PayPal's order and authorize calls. 
	 */
	@SuppressWarnings("unchecked")
	private ContactInfo getShippingAddressForPayPal(Order order) {
		final String SG_ADDR_CHARGESENT_PREFIX = "S2S ";
		List <ShippingGroup> shippingGroupListUM = order.getShippingGroups();

		if (shippingGroupListUM == null || shippingGroupListUM.size() < 1) {
			return null;
		}
		
		// Create a modifiable list from the original unmodifiable one
		List<ShippingGroup> shippingGroupList = new ArrayList<>(shippingGroupListUM);
		
		final Map<?, ?> priceInfos = order.getPriceInfo().getShippingItemsSubtotalPriceInfos();

		Collections.sort(shippingGroupList, new Comparator<Object>() {

			public int compare(Object obj1, Object obj2) {

				if (!(obj1 instanceof HardgoodShippingGroup)
						|| !(obj2 instanceof HardgoodShippingGroup)) {
					return 0;
				}

				HardgoodShippingGroup o1 = (HardgoodShippingGroup) obj1, o2 = (HardgoodShippingGroup) obj2;
	
				OrderPriceInfo pi1 = (OrderPriceInfo) priceInfos.get(o1.getId()), pi2 = (OrderPriceInfo) priceInfos
						.get(o2.getId());

				Object shipTypeObj1 = o1.getPropertyValue(ShippingGroupPropertyManager.SHIP_TYPE.getValue()), shipTypeObj2 = o2
						.getPropertyValue(ShippingGroupPropertyManager.SHIP_TYPE.getValue());

				if ((pi1 != null && pi2 != null && (pi1.getAmount() != pi2
						.getAmount()))) {
					// 1. Highest amounts
					return Double.compare(pi1.getAmount(), pi2.getAmount());

				} else if (shipTypeObj1 != null
						&& shipTypeObj2 != null
						&& (!((String) shipTypeObj1)
								.equals(((String) shipTypeObj2)))) {

					// 2. For equal amounts, check if any is home address
					if (((String) shipTypeObj1).equalsIgnoreCase(ShipType.SHIP.getValue())) {
						return +1;

					} else if (((String) shipTypeObj2).equalsIgnoreCase(ShipType.SHIP.getValue())) {
						return -1;

					}
				} 
				
				// 3. If amount and type are the same, ordering is irrelevant between these two. 				
				return 0;
			}
		});

		if (shippingGroupList.size() < 1) {
			return null;
		}

		ContactInfo shippingAddress = null;

		ShippingGroup sortedShippingGroup = shippingGroupList
				.get(shippingGroupList.size() - 1);
		if (sortedShippingGroup instanceof HardgoodShippingGroup) {
			Collection<CommerceItemRelationship> rels = sortedShippingGroup
					.getCommerceItemRelationships();
			if (rels != null && rels.size() > 0) {
				HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sortedShippingGroup;
				ContactInfo shippingAddressOriginal = (ContactInfo) hgSg.getShippingAddress();
				AddressType addressType = AddressType.valueFor((DigitalRepositoryContactInfo)shippingAddressOriginal);
				// If this sorted shipping group is BOPIS / BOSTS,
				// modify the address for PayPal seller protection
				Object shipType = hgSg.getPropertyValue(ShippingGroupPropertyManager.SHIP_TYPE.getValue());
				if (shipType != null
						&& !((String) (shipType)).equalsIgnoreCase(ShipType.SHIP.getValue())) {
					
					//Create a copy so that the original address is restored for S012 and subsequent operations. 
					//shippingAddress = (ContactInfo) SerializationUtils.clone(shippingAddressOriginal);
					shippingAddress = new ContactInfo();
					
					//Shipping address to PayPal will be updated to 
					//Shipping Name: S2S Consumer name
					//Address1: DSW Store #
					shippingAddress.setFirstName(SG_ADDR_CHARGESENT_PREFIX + shippingAddressOriginal.getFirstName());
					shippingAddress.setMiddleName(shippingAddressOriginal
							.getMiddleName());
					shippingAddress.setLastName(shippingAddressOriginal
							.getLastName());
					shippingAddress.setAddress1(""
							+ hgSg.getPropertyValue(ShippingGroupPropertyManager.STOREID.getValue()));
					shippingAddress.setAddress2(shippingAddressOriginal.getAddress2());
					shippingAddress.setAddress3(shippingAddressOriginal.getAddress3());
					shippingAddress.setCity(shippingAddressOriginal.getCity());
					shippingAddress.setCompanyName(shippingAddressOriginal.getCompanyName());
					shippingAddress.setCountry(shippingAddressOriginal.getCountry());
					shippingAddress.setCounty(shippingAddressOriginal.getCounty());
					shippingAddress.setEmail(shippingAddressOriginal.getEmail());
					shippingAddress.setFaxNumber(shippingAddressOriginal.getFaxNumber());
					shippingAddress.setJobTitle(shippingAddressOriginal.getJobTitle());
					shippingAddress.setOwnerId(shippingAddressOriginal.getOwnerId());
					shippingAddress.setPhoneNumber(shippingAddressOriginal.getPhoneNumber());
					shippingAddress.setPostalCode(shippingAddressOriginal.getPostalCode());
					shippingAddress.setPrefix(shippingAddressOriginal.getPrefix());
					shippingAddress.setState(shippingAddressOriginal.getState());
					shippingAddress.setSuffix(shippingAddressOriginal.getSuffix());
				} else if(AddressType.MILITARY.getValue().equalsIgnoreCase(addressType.getValue())){
					shippingAddress = new ContactInfo();
					DigitalRepositoryContactInfo shippingAddressRepo = (DigitalRepositoryContactInfo) hgSg.getShippingAddress();					
					shippingAddress.setFirstName(shippingAddressOriginal.getFirstName());
					shippingAddress.setMiddleName(shippingAddressOriginal.getMiddleName());
					shippingAddress.setLastName(shippingAddressOriginal.getLastName());
					shippingAddress.setAddress1(shippingAddressOriginal.getAddress1());
					shippingAddress.setAddress2(shippingAddressOriginal.getAddress2());
					shippingAddress.setAddress3(shippingAddressOriginal.getAddress3());
					shippingAddress.setCity(shippingAddressOriginal.getCity());
					shippingAddress.setCompanyName(shippingAddressOriginal.getCompanyName());
					shippingAddress.setCountry(shippingAddressOriginal.getCountry());
					shippingAddress.setCounty(shippingAddressOriginal.getCounty());
					shippingAddress.setEmail(shippingAddressOriginal.getEmail());
					shippingAddress.setFaxNumber(shippingAddressOriginal.getFaxNumber());
					shippingAddress.setJobTitle(shippingAddressOriginal.getJobTitle());
					shippingAddress.setOwnerId(shippingAddressOriginal.getOwnerId());
					shippingAddress.setPhoneNumber(shippingAddressOriginal.getPhoneNumber());
					shippingAddress.setPostalCode(shippingAddressOriginal.getPostalCode());
					shippingAddress.setPrefix(shippingAddressOriginal.getPrefix());
					if(DigitalStringUtil.isBlank(shippingAddressOriginal.getState())){
						shippingAddress.setState(shippingAddressRepo.getRegion());
					}else{
						shippingAddress.setState(shippingAddressOriginal.getState());
					}
					shippingAddress.setSuffix(shippingAddressOriginal.getSuffix());
				} else {
					//If the shipping address is not a store address, don't modify anything. 
					shippingAddress = shippingAddressOriginal;
				}
			}
		}

		return shippingAddress;
	}
	
}
