/**
 * 
 */
package com.digital.commerce.services.order;

import atg.commerce.order.PaymentGroupImpl;
import lombok.Getter;
import lombok.Setter;

/** Extension to the payment group with additional properties for
 * gift cards
 **/
@Getter
@Setter
public class DigitalGiftCard extends PaymentGroupImpl// implements DSWOrderEmailContainer
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -2977969071491742128L;

	private static final String	CARD_NUMBER			= "giftCardNumber";
	
	private static final String	PIN_NUMBER			= "pinNumber";
	
	private Double	amountRemaining = 0.0;
	
	private static final String	MEMBER_NUMBER		= "memberNumber";

	
	/** Gets the cardNumber
	 * 
	 * @return the cardNumber */

	public String getMemberNumber() {
		return (String)getPropertyValue( MEMBER_NUMBER );
	}

	public void setMemberNumber( String memNumber ) {
		setPropertyValue( MEMBER_NUMBER, memNumber );
	}

	public String getCardNumber() {
		return (String)getPropertyValue( CARD_NUMBER );
	}

	/** Sets the cardNumber
	 * 
	 * @param cardNumber the cardNumber */
	public void setCardNumber( String cardNumber ) {
		setPropertyValue( CARD_NUMBER, cardNumber );
	}

	/** Gets the pinNumber
	 * 
	 * @return the pinNumber */
	public String getPinNumber() {
		return (String)getPropertyValue( PIN_NUMBER );
	}

	/** Sets the pinNumber
	 * 
	 * @param pinNumber the pinNumber */
	public void setPinNumber( String pinNumber ) {
		setPropertyValue( PIN_NUMBER, pinNumber );
	}


	// Properties for CS13 emails. These should not be saved to the OrderRepository unless the Yantra XSD supports them!
	// protected String orderEmail;
	// protected String orderFirstName;
	// public String getOrderEmail() {
	// return orderEmail;
	// }
	// public void setOrderEmail(String orderEmail) {
	// this.orderEmail = orderEmail;
	// }
	// public String getOrderFirstName() {
	// return orderFirstName;
	// }
	// public void setOrderFirstName(String orderFirstName) {
	// this.orderFirstName = orderFirstName;
	// }

}
