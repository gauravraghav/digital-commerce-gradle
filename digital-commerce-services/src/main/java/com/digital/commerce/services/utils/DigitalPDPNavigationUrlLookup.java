package com.digital.commerce.services.utils;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;

import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.assembler.AssemblerException;
import com.endeca.navigation.AssocDimLocations;
import com.endeca.navigation.AssocDimLocationsList;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValIdList;
import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.ENEQuery;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.ERecSearch;
import com.endeca.navigation.ERecSearchList;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.Navigation;

import atg.endeca.assembler.AssemblerTools;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalPDPNavigationUrlLookup extends DynamoServlet {
	private static final ParameterName DIMENSIONNAME = ParameterName.getParameterName("dimensionName");
	private static final ParameterName PRODUCTID = ParameterName.getParameterName("productId");
	private static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	private String currentMdexHostName;
	private int currentMdexPort;
	private AssemblerTools assemblerTools;
	private int eneQueryTimeout;

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		long start = System.currentTimeMillis();
		final String dimensionName = pRequest.getParameter(DIMENSIONNAME);
		final String productId = pRequest.getParameter(PRODUCTID);
		ExecutorService executor = Executors.newCachedThreadPool();
		String navStringUrl = "";
		String refinementName = "";
		Callable<ENEQueryResults> task = new Callable<ENEQueryResults>() {
			@SuppressWarnings("unchecked")
			@Override
			public ENEQueryResults call() throws AssemblerException, ENEQueryException {
				ENEQuery eneQuery = new ENEQuery();
				ENEConnection eneConnection = new HttpENEConnection(currentMdexHostName, currentMdexPort);
				DimValIdList paramDimValIdList = new DimValIdList("0");
				eneQuery.setNavDescriptors(paramDimValIdList);
				eneQuery.setNavAllRefinements(true);
				ERecSearch eRecSearch = new ERecSearch("product.repositoryId", productId);
				ERecSearchList eRecSearchList = new ERecSearchList();
				eRecSearchList.add(eRecSearch);
				eneQuery.setNavERecSearches(eRecSearchList);
				eneQuery.setAggrERecSpec("product.repositoryId");
				return eneConnection.query(eneQuery);
			}
		};
		Future<ENEQueryResults> future = null;
		try {
			future = executor.submit(task);
			ENEQueryResults eneQueryResults = future.get(eneQueryTimeout, TimeUnit.MILLISECONDS);
			Navigation nav = eneQueryResults.getNavigation();
			ERecList recs = nav.getERecs();
			for (int i = 0; i < recs.size(); i++) {
				ERec rec = (ERec) recs.get(i);
				AssocDimLocationsList assocDimLocList = (AssocDimLocationsList) rec.getDimValues();
				for (int j = 0; j < assocDimLocList.size(); j++) {
					AssocDimLocations assocDimLoc = (AssocDimLocations) assocDimLocList.get(j);
					DimVal root = assocDimLoc.getDimRoot();
					for (int k = 0; k < assocDimLoc.size(); k++) {
						DimLocation dimLoc = (DimLocation) assocDimLoc.get(k);
						if (root != null && dimensionName.equalsIgnoreCase(root.getName())) {
							DimVal dimVal = dimLoc.getDimValue();
							navStringUrl = generateSEONvalue(Long.toString(dimVal.getId()));
							refinementName=dimVal.getName();
							break;
						}
					}
				}
			}
		} catch (TimeoutException | InterruptedException | ExecutionException ex) {
			// ExecutionException will be the wrapped ENE exception
			logError("Exception while reading the dimension value from MDEX response", ex);
		} finally {
			future.cancel(true);
			executor.shutdown();
			long took = System.currentTimeMillis() - start;
			logInfo("Time taken to get brand dimension from product: " + productId + " - " + took + "ms");
		}
		pRequest.setParameter("refinementName", refinementName);
		pRequest.setParameter("navStringUrl", navStringUrl);
		pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
	}

	private String generateSEONvalue(String queryStr) {
		String finalSeoString = null;
		String seoVar = Long.toString(Long.parseLong(queryStr), 36);
		finalSeoString = DigitalEndecaConstants.SEODELIMITER + DigitalEndecaConstants.SEOPREFIX + seoVar;
		return finalSeoString;
	}

}
