package com.digital.commerce.services.profile;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileRequestServlet;

public class DigitalProfileRequestServlet extends ProfileRequestServlet {
	

	@Override
	public void sendProfileCookies(Profile pProfile, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			/*     */     throws ServletException, IOException {
		super.sendProfileCookies(pProfile, pRequest, pResponse);

		((DigitalProfileTools)getProfileTools()).createOrUpdateTierCookie(pProfile, pRequest, pResponse);
	}

}