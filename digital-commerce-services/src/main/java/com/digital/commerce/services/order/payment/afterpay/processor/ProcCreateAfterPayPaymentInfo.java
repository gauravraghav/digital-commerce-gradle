package com.digital.commerce.services.order.payment.afterpay.processor;

import atg.commerce.order.Order;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.order.payment.afterpay.AfterPayPayment;
import com.digital.commerce.services.order.payment.afterpay.GenericAfterPayPaymentInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcCreateAfterPayPaymentInfo extends ApplicationLoggingImpl implements PipelineProcessor {
    private static final int SUCCESS = 1;
    private static final String SERVICE_NAME = "CreateAfterPayPaymentInfo";
    private String afterPayPaymentInfoClass;

    @Override
    public int runProcess(Object pParam, PipelineResult pipelineResult) throws Exception {
        final String METHOD_NAME = "runProcess";
        try{
            DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
            PaymentManagerPipelineArgs params = (PaymentManagerPipelineArgs) pParam;
            Order order = params.getOrder();
            AfterPayPayment afterPayPayment = (AfterPayPayment) params.getPaymentGroup();
            // create and populate afterpay payment info class
            GenericAfterPayPaymentInfo afterPayPaymentInfo =  (GenericAfterPayPaymentInfo)getAfterPayPaymentInfo();
            this.addDataToAfterPayPaymentInfo(order,afterPayPayment,params,afterPayPaymentInfo);
            if (this.isLoggingDebug()) {
                this.logDebug("Putting AfterPayPaymentInfo object into pipeline: " + afterPayPaymentInfo.toString());
            }
            params.setPaymentInfo(afterPayPaymentInfo);
            return SUCCESS;
        }finally{
            DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
        }
    }

    @Override
    public int[] getRetCodes() {
        return new int[]{SUCCESS};
    }

    protected GenericAfterPayPaymentInfo getAfterPayPaymentInfo() throws Exception {
        if (this.isLoggingDebug()) {
            this.logDebug("Making a new instance of type: " + this.getAfterPayPaymentInfoClass());
        }
        GenericAfterPayPaymentInfo api = (GenericAfterPayPaymentInfo)Class.forName(this.getAfterPayPaymentInfoClass()).newInstance();
        return api;
    }

    protected void addDataToAfterPayPaymentInfo(Order pOrder, AfterPayPayment pPaymentGroup, PaymentManagerPipelineArgs pParams, GenericAfterPayPaymentInfo afterPayPaymentInfo) {
        afterPayPaymentInfo.setOrder(pOrder);
        afterPayPaymentInfo.setToken(pPaymentGroup.getToken());
    }
}
