package com.digital.commerce.services.storelocator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductInventoryDetailBean {
	private int productLevelStock;
	private boolean shipToStore;
	private boolean pickUpInStore;
}
