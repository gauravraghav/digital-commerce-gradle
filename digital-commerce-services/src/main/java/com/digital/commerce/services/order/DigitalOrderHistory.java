package com.digital.commerce.services.order;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.time.DateUtils;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.domain.ResponseWrapper;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;

import atg.commerce.CommerceException;
import atg.commerce.order.OrderLookup;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.service.collections.filter.CachedCollectionFilter;
import atg.service.collections.filter.FilterException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

@Getter
@Setter
public class DigitalOrderHistory extends OrderLookup {

	private static final ParameterName ORDERID = ParameterName.getParameterName("orderId");
	private static final ParameterName EMAIL = ParameterName.getParameterName("email");
	private static final ParameterName CHAIN = ParameterName.getParameterName("chain");
	private static final ParameterName SOURCE = ParameterName.getParameterName("sourceOriginate");

	private static final String NO_INPUT = "ORDER_LOOKUP_NO_INPUT";
	private static final String NO_ORDER_FOUND = "ORDER_LOOKUP_ORDER_NOT_FOUND";
	private static final String ORDER_LOOKUP_ORDER_START_INDEX_ERROR = "ORDER_LOOKUP_ORDER_START_INDEX_ERROR";
	private static final ParameterName STARTDATE = ParameterName.getParameterName("startDate");
	private static final ParameterName ENDDATE = ParameterName.getParameterName("endDate");
	private static final ParameterName MONTHS = ParameterName.getParameterName("months");
	private static final ParameterName START_INDEX = ParameterName.getParameterName("startIndex");
	private static final ParameterName END_INDEX = ParameterName.getParameterName("endIndex");

	private DigitalOrderLookupService lookupService;
	protected MessageLocator messageLocator;
	private int defaultMonths = 6;
	private int defaultDashboardDays = 14;
	private int maxDashboardOrderDisplayCount = 1;
	private int startIndex = 0;
	private int endIndex = -1;
	private String[] defaultDatePattern = { "yyyy-MM-dd", "MM/dd/yyyy" };
	private CachedCollectionFilter filter;
	private boolean atgDataSource = false;
	private String orderId;
	private String email;
	private String chain;
	private String source;
	private String startDate;
	private String endDate;
	private String months;
	private boolean paginationEnabled;

	@SuppressWarnings("unchecked")
	@Override
	/**
	 * 
	 * @param pReq
	 * @param pRes
	 * @throws ServletException
	 * @throws IOException
	 */
	public void service(DynamoHttpServletRequest pReq, DynamoHttpServletResponse pRes)
			throws ServletException, IOException {
		final String METHOD_NAME = "service";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			initializeRequestParameters(pReq);
			String orderIdpassed = getOrderId();
			String email = getEmail();

			String chain = getChain();
			String source = getSource();

			RepositoryItem profile = (RepositoryItem) pReq.resolveName(this.mProfilePath);
			String loyaltyNumber = (String) profile.getPropertyValue("loyaltyNumber");
			int totalOrderCount = 0;
			if (chain != null && "history".equalsIgnoreCase(chain)) {
				String startDate = getStartDate();
				String endDate = getEndDate();
				String months = getMonths();

				Date endDT = new Date();
				Date startDT = DateUtils.addMonths(endDT, defaultMonths * -1);

				if (!DigitalStringUtil.isBlank(months)) {
					startDT = DateUtils.addMonths(endDT, Integer.parseInt(months) * -1);
				} else {
					if (!DigitalStringUtil.isBlank(endDate))
						endDT = DateUtils.parseDate(endDate, this.defaultDatePattern);
					if (!DigitalStringUtil.isBlank(startDate))
						startDT = DateUtils.parseDate(startDate, this.defaultDatePattern);
				}

				List<DigitalOrderHistoryModel> filterOrders;
				
				if (source != null && source.equals("dashboard")) {
					endDT = new Date();
					startDT = DateUtils.addDays(endDT, this.getDefaultDashboardDays() * -1);

					if(isPaginationEnabled()) {
						totalOrderCount = this.getLookupService().getTotalOrdersCount(this.getCurrentProfileId(pReq),
								loyaltyNumber, startDT, endDT);
						if (getStartIndex() > totalOrderCount) {
							pReq.setParameter("result", this.getResponsErrorWrapper(ORDER_LOOKUP_ORDER_START_INDEX_ERROR));
							pReq.serviceLocalParameter("error", pReq, pRes);
							return;
						}
					}
					List<DigitalOrderHistoryModel> orders = this.getLookupService().getOrdersFromDSWOrderHistoryForDashboard(
							this.getCurrentProfileId(pReq), loyaltyNumber, startDT, endDT, getStartIndex(),
							getMaxDashboardOrderDisplayCount());
					filterOrders = (List<DigitalOrderHistoryModel>) getFilter().filterCollection(orders, null, null);
					
					if (filterOrders != null) {
						Collections.sort(filterOrders, new Comparator<DigitalOrderHistoryModel>() {
							public int compare(DigitalOrderHistoryModel o1, DigitalOrderHistoryModel o2) {
								return (-1) * o1.getOrderPlacementDate().compareTo(o2.getOrderPlacementDate());
							}
						});

						if(!isPaginationEnabled()) {
							totalOrderCount = filterOrders.size();
						}
					}

				} else {
					if(isPaginationEnabled()) {
						if (!(startDT.compareTo(DateUtils.addMonths(new Date(), -6)) < 0)) {
							totalOrderCount = this.getLookupService().getTotalOrdersCount(this.getCurrentProfileId(pReq),
									loyaltyNumber, startDT, endDT);
						}

						if (getStartIndex() > totalOrderCount) {
							pReq.setParameter("result", this.getResponsErrorWrapper(ORDER_LOOKUP_ORDER_START_INDEX_ERROR));
							pReq.setParameter("totalOrderCount", totalOrderCount);
							pReq.serviceLocalParameter("error", pReq, pRes);
							return;
						}
					}
					filterOrders = this.getLookupService().getOrders(this.getCurrentProfileId(pReq), loyaltyNumber,
							startDT, endDT, getStartIndex(), getEndIndex());
					if (filterOrders != null && startDT.compareTo(DateUtils.addMonths(new Date(), -6)) < 0) {
						totalOrderCount = filterOrders.size();
					}
				}

				if (filterOrders == null || filterOrders.size() < 1) {
					pReq.setParameter("result", this.getResponsErrorWrapper(NO_ORDER_FOUND));
					pReq.setParameter("totalOrderCount", totalOrderCount);
					pReq.serviceLocalParameter("error", pReq, pRes);
					return;
				}

				pReq.setParameter("result", filterOrders);
				pReq.setParameter("totalOrderCount", totalOrderCount);
				pReq.serviceLocalParameter("output", pReq, pRes);
				return;
			} else {
				if (DigitalStringUtil.isBlank(orderIdpassed) || DigitalStringUtil.isBlank(email)) {
					pReq.setParameter("result", this.getResponsErrorWrapper(NO_INPUT));
					pReq.setParameter("totalOrderCount", totalOrderCount);
					pReq.serviceLocalParameter("error", pReq, pRes);
					return;
				}

				DigitalOrderHistoryModel detail = this.orderAndEmailMatch(orderIdpassed.toLowerCase(), email);

				if (detail == null) {
					pReq.setParameter("result", this.getResponsErrorWrapper(NO_ORDER_FOUND));
					pReq.serviceLocalParameter("error", pReq, pRes);
					return;
				}

				pReq.setParameter("result", detail);				
				pReq.serviceLocalParameter("output", pReq, pRes);

				return;

			}

		} catch (ParseException | FilterException | CommerceException ces) {
			logError("Exception in orderlookup service");
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/**
	 * 
	 * @param errorCode
	 * @return ResponseWrapper
	 */
	private ResponseWrapper getResponsErrorWrapper(String errorCode) {
		ResponseWrapper error = new ResponseWrapper();
		String errMsg = this.getMessageLocator().getMessageString(errorCode);
		ResponseError genError = new ResponseError(errorCode, errMsg);
		error.getGenericExceptions().add(genError);
		error.setFormError(false);
		error.setRequestSuccess(false);
		return error;
	}

	/**
	 * 
	 * @param orderIdpassed
	 * @param email
	 * @return DigitalOrderHistoryModel
	 */
	private DigitalOrderHistoryModel orderAndEmailMatch(String orderIdpassed, String email) {
		DigitalOrderHistoryModel result = null;

		if (atgDataSource){
			result = this.lookupService.validateHistoryEmailAddress(orderIdpassed, email);
		}

		if (result == null) {
			result = this.lookupService.validateYantraEmailAddress(orderIdpassed, email);
		}

		return result;
	}
	

	/**
	 * 
	 * @param pReq
	 */
	protected void initializeRequestParameters(DynamoHttpServletRequest pReq) {
		String orderIdpassed = (String) pReq.getLocalParameter(ORDERID);
		if (DigitalStringUtil.isNotBlank(orderIdpassed)) {
			setOrderId(orderIdpassed);
		}
		String email = (String) pReq.getLocalParameter(EMAIL);
		if (DigitalStringUtil.isNotBlank(email)) {
			setEmail(email);
		}
		String chain = (String) pReq.getLocalParameter(CHAIN);
		if (DigitalStringUtil.isNotBlank(chain)) {
			setChain(chain);
		}
		String source = (String) pReq.getLocalParameter(SOURCE);
		if (DigitalStringUtil.isNotBlank(source)) {
			setSource(source);
		}
		if (chain != null && chain.equals("history")) {
			String startDate = (String) pReq.getLocalParameter(STARTDATE);
			if (DigitalStringUtil.isNotBlank(startDate)) {
				setStartDate(startDate);
			}
			String endDate = (String) pReq.getLocalParameter(ENDDATE);
			if (DigitalStringUtil.isNotBlank(endDate)) {
				setEndDate(endDate);
			}
			String months = (String) pReq.getLocalParameter(MONTHS);
			if (DigitalStringUtil.isNotBlank(months)) {
				setMonths(months);
			}
			String startIndex = (String) pReq.getLocalParameter(START_INDEX);
			if (DigitalStringUtil.isNotBlank(startIndex)) {
				setStartIndex(Integer.valueOf(startIndex));
			}
			String endIndex = (String) pReq.getLocalParameter(END_INDEX);
			if (DigitalStringUtil.isNotBlank(endIndex)) {
				setEndIndex(Integer.valueOf(endIndex));
			}

			if (source != null && source.equals("dashboard")) {
				String numOrders = pReq.getParameter(NUMORDERS);
				if (DigitalStringUtil.isNotBlank(numOrders)) {
					setMaxDashboardOrderDisplayCount(Integer.valueOf(numOrders));
				}
			}
		}
	}

}
