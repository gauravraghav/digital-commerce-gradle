package com.digital.commerce.services.giftcard.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RemoveGiftCardResponse extends ResponseWrapper {
	private String gcStatus;
}
