
package com.digital.commerce.services.profile.rewards;

import static com.digital.commerce.services.profile.rewards.LoyaltyCertificate.ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE;
import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.pricing.PricingTools;
import atg.dtm.UserTransactionDemarcation;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.exception.DigitalRuntimeException;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalNumberUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.RequestHeaderAttributesConstant;
import com.digital.commerce.constants.OrderConstants.OrderPropertyManager;
import com.digital.commerce.constants.ProducCatalogConstants.ProducCatalogPropertyManager;
import com.digital.commerce.constants.RewardsCertificateConstants;
import com.digital.commerce.constants.RewardsCertificateConstants.OrderRewardCertificateStatus;
import com.digital.commerce.constants.RewardsCertificateConstants.ProfileRewardCertificateStatus;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.oms.yantra.domain.YantraLinkLoyaltyNumberToGuestOrder;
import com.digital.commerce.integration.oms.OrderManagementService;
import com.digital.commerce.integration.reward.RewardService;
import com.digital.commerce.integration.reward.domain.BirthdayGift;
import com.digital.commerce.integration.reward.domain.Certificate;
import com.digital.commerce.integration.reward.domain.Charity;
import com.digital.commerce.integration.reward.domain.Flags;
import com.digital.commerce.integration.reward.domain.Incentive;
import com.digital.commerce.integration.reward.domain.Person;
import com.digital.commerce.integration.reward.domain.Points;
import com.digital.commerce.integration.reward.domain.RewardServiceRequest;
import com.digital.commerce.integration.reward.domain.RewardServiceResponse;
import com.digital.commerce.integration.reward.domain.RewardsEnticementsResponse;
import com.digital.commerce.integration.reward.domain.Shopfor;
import com.digital.commerce.integration.reward.domain.api.RewardsSendOrderRequest;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.profile.DigitalBirthdayGiftTools;
import com.digital.commerce.services.profile.DigitalCommercePropertyManager;
import com.digital.commerce.services.profile.DigitalShopforTools;
import com.digital.commerce.services.rewards.domain.RewardsAddCustomerRequest;
import com.digital.commerce.services.rewards.domain.RewardsAddCustomerResponse;
import com.digital.commerce.services.rewards.domain.RewardsAddShopWithoutACardRequest;
import com.digital.commerce.services.rewards.domain.RewardsAddShopWithoutACardResponse;
import com.digital.commerce.services.rewards.domain.RewardsAddUpdateGiftResponse;
import com.digital.commerce.services.rewards.domain.RewardsAddUpdateShopforResponse;
import com.digital.commerce.services.rewards.domain.RewardsCharitiesResponse;
import com.digital.commerce.services.rewards.domain.RewardsCharityItem;
import com.digital.commerce.services.rewards.domain.RewardsDonateCertsRequest;
import com.digital.commerce.services.rewards.domain.RewardsDonateCertsResponse;
import com.digital.commerce.services.rewards.domain.RewardsGiftItem;
import com.digital.commerce.services.rewards.domain.RewardsIncentiveItem;
import com.digital.commerce.services.rewards.domain.RewardsIncentivesResponse;
import com.digital.commerce.services.rewards.domain.RewardsIssueCertsRequest;
import com.digital.commerce.services.rewards.domain.RewardsIssueCertsResponse;
import com.digital.commerce.services.rewards.domain.RewardsRetrieveBirthdayGiftsResponse;
import com.digital.commerce.services.rewards.domain.RewardsRetrievePointsOffersCertificatesRequest;
import com.digital.commerce.services.rewards.domain.RewardsRetrievePointsOffersCertificatesResponse;
import com.digital.commerce.services.rewards.domain.RewardsRetrieveRewardCertificatesResponse;
import com.digital.commerce.services.rewards.domain.RewardsRetrieveShopforResponse;
import com.digital.commerce.services.rewards.domain.RewardsSelectCustomerRequest;
import com.digital.commerce.services.rewards.domain.RewardsSelectCustomerResponse;
import com.digital.commerce.services.rewards.domain.RewardsShopforItem;
import com.digital.commerce.services.rewards.domain.RewardsSubscribeEmailFooterRequest;
import com.digital.commerce.services.rewards.domain.RewardsSubscribeEmailFooterResponse;
import com.digital.commerce.services.rewards.domain.RewardsUpdateCustomerRequest;
import com.digital.commerce.services.rewards.domain.RewardsUpdateCustomerResponse;
import com.digital.commerce.services.storelocator.DigitalStoreAddress;
import com.digital.commerce.services.utils.DigitalAddressUtil;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import com.digital.commerce.services.utils.FindRepositoryItemById;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** 
*/
@SuppressWarnings({"unchecked","rawtypes"})
@Getter
@Setter
public class DigitalRewardsManager extends ApplicationLoggingImpl {

	private static final String CLASSNAME = DigitalRewardsManager.class.getName();

	private static final String ERR_INVALID_EMAIL 					= "invalidEmailAddress";
	private static final String ERR_INVALID_EMAILSOURCE 			= "002";
	private static final String ERR_INVALID_MEMBERID_PROFILEID 		= "003";
	private static final String ERR_INVALID_FIRSTNAME 				= "invalidRecipientsFirstName";
	private static final String ER_INVALID_BIRTH_DAY_MONTH 			= "invalidDOB";
	private static final String ERR_MAX_BDAY_GIFTS_LIMIT 			= "exceededBirthDayGiftLimit";
	private static final String ERR_MAX_SHOPFOR_LIMIT = "exceededShopforLimit";
	private static final String ER_INVALID_BIRTH_DAY_SHOPFOR = "invalidDOBshopFor";
	private static final String ERR_INVALID_MEMBER 					= "invalidMember";
	private static final String ERR_DUPLICATE_BIRTHDAY_GIFT 		= "duplicateBirthDayGift";
	private static final String ERR_LOCKED_BIRTHDAY_GIFT 		= "lockedBirthDayGift";
	private static final String ERR_INVALID_SHOPFOR_NAME = "invalidShopforName";
	private static final String ERR_MISSING_SHOPFOR_SIZE = "missingShopforSize";
	private static final String ERR_MISSING_SHOPFOR_GENDER = "missingShopforGender";
	private static final String ERR_CUST_EMAIL_SAME_AS_BIRTHDAY_GIFT_EMAIL = "custEmailSameAsBDayGiftEmail";
	private static final String ERR_GENERIC_BIRTHDAY_GIFT 			= "rewardsBDayGiftServiceError";
	private static final String ERR_TIER_NOT_ELIGIBLE_BIRTHDAY_GIFT = "userNotLoggedInOrAuthorized";
	private static final String ERR_GENERIC_SHOPFOR					= "rewardsShopForServiceError";
	private static final String ERR_SHOPFOR_NOT_FOUND					= "rewardsShopForNotFoundError";
	private static final String ERR_GENERIC_ISSUE_CERT 			= "rewardsIssueCertServiceError";
	private static final String ERR_GENERIC_DONATE_CERT 			= "rewardsDonateCertServiceError";
	private static final String ERR_NO_REDEEMABLE_REWARD = "noRedeemableReward";
	private static final String ERR_NO_DONATABLE_REWARD = "noDonatableReward";
	private static final String ERR_INVALID_INPUT_FOR_ENTICEMENTS = "invalidInputEnticementRequest";

	private static final String REWARD_TYPE_BYVENDOR_OFFER 	= "OFFER";

	private static final String REWARD_TYPE_OFFER 					= "OFFERS";
	private static final String REWARD_TYPE_BIRTHDAY_OFFER 			= "BIRTHDAY_OFFER";
	private static final String REWARD_TYPE_DOUBLE_POINTS_OFFER 	= "DOUBLE_POINTS_DAYS";
	private static final String REWARD_TYPE_TRIPLE_POINTS_OFFER 	= "TRIPLE_POINTS_DAYS";

	private PricingTools				pricingTools;
	private DigitalServiceConstants			dswConstants;
	private DigitalCommercePropertyManager	propertyManager;

	private RewardService				rewardService;
	private OrderManager				orderManager;
	private Repository					orderRepository;
	private String[]					undiscountableProductTypes;
	private MessageLocator 				messageLocator;
	private DigitalAddressUtil 				addressUtil;	
	private long						maxBDayGiftsLimit = 2;
	private long						maxShopforLimit = 5;
	private boolean syncBDayGiftsPostAddFromRewards = false;
	private boolean syncBDayGiftsPostUpdateFromRewards = false;
	private boolean syncShopForsPostAddFromRewards = false;
	private boolean syncShopForsPostUpdateFromRewards = false;

	private final String vipRewardsDateFormat = "yyyy-MM-dd'T'HH:mm:ss";

	private DigitalBirthdayGiftTools birthdayGiftTools;
	private DigitalShopforTools shopforTools;

  private OrderManagementService orderManagementService;

	public DigitalRewardsManager() {
		super(DigitalRewardsManager.class.getName());
	}
	
	/** validated that the certificates provided for the ATG profile are indeed
	 * in EDW. If they are, then store the cert information and set the valid
	 * flag to true, other wise set valid flag to false.
	 * 
	 * @param pCertNumbers
	 *            string array of unique certificate numbers
	 * @param pProfileId
	 *            string value of the ATG profile id
	 * @param pMemberId
	 *            string value of the loyality member id
	 * @return List of LoyaltyCertificate if successfully, or empty if not
	 */
	public List<LoyaltyCertificate> validateCertificatesForRepricing( String[] pCertNumbers, String pProfileId, String pMemberId ) {
		if( isLoggingDebug()) {
			logDebug( "Enter: validateCertificatesForRepricing(String[] " + Arrays.toString(pCertNumbers) + " , String " + pProfileId + " , String + " + pMemberId + ")" );
		}

		List<String> certListToValidate = new ArrayList<>( Arrays.asList( pCertNumbers ) );
		List<LoyaltyCertificate> validatedCerts = new ArrayList<>();

		List<LoyaltyCertificate> certBeans = this.retrieveRewardsCertificates( pMemberId, pProfileId, null );

		if( null != certBeans && !certBeans.isEmpty()) {
			for (LoyaltyCertificate certBean : certBeans) {
				if (certListToValidate.contains(certBean.getId())) {
					validatedCerts.add(certBean);
					certListToValidate.remove(certBean.getId());
				}
			}
		}
		List<LoyaltyCertificate> list = new ArrayList<>();

		Iterables.addAll( list,  validatedCerts);

		Iterables.addAll( list, Iterables.transform( certListToValidate, new ToInvalidCertificateAdapter() ) );
		Collections.sort( list, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE );
		if( isLoggingDebug() ) {
			logDebug( "End: validateCertificatesForRepricing() : Number of valid certs :" + list.size() );
		}
		return list;
	}
	
	/**
	 * @param loyaltyCertificates
	 * @param codes - could be the certificate id of the certificate number. the id takes precendence.
	 * @param memberId
	 * @return
	 */
	public Set<LoyaltyCertificate> findValidatedCertificates( List<LoyaltyCertificate> loyaltyCertificates, Collection<String> codes, String memberId ) {
		Set<LoyaltyCertificate> retVal = null;
		if( codes != null && !codes.isEmpty() ) {
			retVal = new LinkedHashSet<>();
			if(loyaltyCertificates == null || loyaltyCertificates.isEmpty()){
				loyaltyCertificates = retrieveRewardsCertificates( memberId, this.getProfile().getRepositoryId(), null );
			}
			for( String code : codes ) {
				if(code.length() == getDswConstants().getRewardCertificateMarkdownLength()){
					Iterator<LoyaltyCertificate> iterator = Iterables
							.filter(loyaltyCertificates, new FindLoyaltyCertificateByMarkdownCodePredicate(code)).iterator();
					if (!iterator.hasNext()) {
						iterator = Iterables
								.filter(loyaltyCertificates, new FindLoyaltyCertificateByMarkdownCodePredicate(code))
								.iterator();
					}
					if (iterator.hasNext()) {
						retVal.addAll(Lists.newArrayList(iterator));
					} else {
						final LoyaltyCertificate loyaltyCertificate = new LoyaltyCertificate();
						loyaltyCertificate.setMarkdownCode(code);
						loyaltyCertificate.setValid(false);
						retVal.add(loyaltyCertificate);
					}
				} else {
					Iterator<LoyaltyCertificate> iterator = Iterables
							.filter(loyaltyCertificates, new FindLoyaltyCertificatePredicate(code)).iterator();
					if (!iterator.hasNext()) {
						iterator = Iterables
								.filter(loyaltyCertificates, new FindLoyaltyCertificateByClaimCodePredicate(code))
								.iterator();
					}
					if (iterator.hasNext()) {
						retVal.addAll(Lists.newArrayList(iterator));
					} else {
						final LoyaltyCertificate loyaltyCertificate = new LoyaltyCertificate();
						loyaltyCertificate.setCertificateNumber(code);
						loyaltyCertificate.setId(code);
						loyaltyCertificate.setValid(false);
						retVal.add(loyaltyCertificate);
					}
				}
			}
		}

		return retVal;
	}
	
	/** 
	 * Returns the validated certificates that are still available to be used.
	 *
	 * @param codes
	 * @param memberId
	 * @return */
	public Set<LoyaltyCertificate> findValidatedAvailableCertificates(Collection<String> codes, String memberId) {
		Set<LoyaltyCertificate> retVal = new LinkedHashSet<>();
		List<LoyaltyCertificate> loyaltyCertificates = new ArrayList<>();

		Set<LoyaltyCertificate> validatedCertificates = findValidatedCertificates(loyaltyCertificates, codes, memberId);
		if (validatedCertificates != null) {
			for (LoyaltyCertificate loyaltyCertificate : validatedCertificates) {
				if (!isAppliedRewardCertificate(getProfile(), loyaltyCertificate.getId())) {
					retVal.add(loyaltyCertificate);
				}
			}
		}
		return retVal;
	}
	
	/** 
	 * Returns the validated certificates that are still available to be used.
	 *
	 * @param codes
	 * @param memberId
	 * @return */
	public Set<LoyaltyCertificate> findValidatedAvailableCertificates(Set<LoyaltyCertificate> validatedCertificates,
			Collection<String> codes, String memberId) {
		Set<LoyaltyCertificate> retVal = new LinkedHashSet<>();
		if (validatedCertificates != null) {
			for (LoyaltyCertificate loyaltyCertificate : validatedCertificates) {
				if (!isAppliedRewardCertificate(getProfile(), loyaltyCertificate.getId())) {
					retVal.add(loyaltyCertificate);
				}
			}
		}
		return retVal;
	}
	
	/**
	 * Returns if the certificate is already applied on the profile.
	 *  
	 * @param validatedCertificates
	 * @param codes - will be either 3 digit Certificate Number or 12 digit Markdown code
	 * @param memberId
	 * @return true or false
	 */
	public boolean isRewardCertificateAlreadyApplied( Set<LoyaltyCertificate> validatedCertificates, Collection<String> codes, String memberId ) {
		boolean retVal = false;
		if( validatedCertificates != null ) {
			boolean isCertificateAvailable=false;
			for( LoyaltyCertificate loyaltyCertificate : validatedCertificates ) {
				if( loyaltyCertificate.isValid()){
					if(!isAppliedRewardCertificate( getProfile(), loyaltyCertificate.getId() ) ) {
						isCertificateAvailable = true;
						break;
					}
				}
			}

			retVal = !isCertificateAvailable;
		}
		return retVal;
	}
	
	/**
	 * 
	 * @param codes
	 * @param memberId
	 * @return Set of all valid certificates
	 */
	public Set<LoyaltyCertificate> getAllValidatedCertificates(Collection<String> codes, String memberId){
		List<LoyaltyCertificate> loyaltyCertificates = new ArrayList<>();
		return findValidatedCertificates( loyaltyCertificates, codes, memberId );
	}
	
	public boolean isAnyValidCertifcateFound(Set<LoyaltyCertificate> allCertificates){
		boolean foundValidCert = false;
		if(allCertificates != null && !allCertificates.isEmpty()){
			for(LoyaltyCertificate certificate : allCertificates){
				if(certificate.isValid()){
					foundValidCert = true;
				}
			}
		}
		return foundValidCert;
	}
	
	/** Returns whether or not this certificate is currently applied.
	 * 
	 * @param profile
	 * @param certificateId
	 * @return */
	public boolean isAppliedRewardCertificate( Profile profile, String certificateId ) {
		boolean retVal = false;
		MutableRepositoryItem certificateItem = getProfileCertificate( profile, certificateId );
		if( certificateItem != null ) {
			retVal = ProfileRewardCertificateStatus.APPLIED.getValue().equalsIgnoreCase( 
					(String)certificateItem.getPropertyValue( getPropertyManager().getCertificateStatusPropertyName()) );
		}
		return retVal;
	}

	/**
	 * @param order
	 */
	public void checkInvalidCertificates(Order order) {
		String profileId = this.getProfile().getRepositoryId();
		String memberId = (String) getProfile()
				.getPropertyValue(this.getPropertyManager().getLoyaltyNumberPropertyName());
		List<LoyaltyCertificate> loyaltyCertificates = this
				.retrieveRewardsCertificates(memberId, profileId, null);

		List<RepositoryItem> userCertificateList = this
				.getProfileCertificatesUsingRepository(getProfile());

		updateCertificatesStatus(order, loyaltyCertificates, userCertificateList);
	}

	/**
	 * @param order
	 * @param loyaltyCertificates
	 */
	public void checkInvalidCertificates(Order order, Set<LoyaltyCertificate> loyaltyCertificates) {

		if (null == loyaltyCertificates || loyaltyCertificates.isEmpty()) {
			return;
		}

		List<RepositoryItem> userCertificateList = this
				.getProfileCertificatesUsingRepository(getProfile());

		List<LoyaltyCertificate> loyaltyCertificateList = new ArrayList<>(loyaltyCertificates);

		updateCertificatesStatus(order, loyaltyCertificateList, userCertificateList);
	}

	/**
	 * This method checks if a particular certificate is a part of the order or not. In case it is
	 * not, it will be removed from the order.
	 *
	 * @param order
	 * @param loyaltyCertificates
	 * @param userCertificateList : certificate ids which needs to be checked.
	 */

	public void updateCertificatesStatus(Order order, List<LoyaltyCertificate> loyaltyCertificates,
			List<RepositoryItem> userCertificateList) {
		final Set<RepositoryItem> orderCertificates = getOrderCertificates(order);
		Set<String> certificateIds = new LinkedHashSet<>();
		if (orderCertificates != null) {
			for (RepositoryItem orderCertificate : orderCertificates) {
				if (orderCertificate != null) {
					certificateIds.add(orderCertificate.getRepositoryId());
				}
			}
		}
		if (!certificateIds.isEmpty()) {
			Map<String, RepositoryItem> userCertificates = new HashMap<>();
			if (userCertificateList == null || userCertificateList.isEmpty()) {
				userCertificateList = getProfileCertificatesUsingRepository(getProfile());
			}
			for (RepositoryItem userCertificate : userCertificateList) {
				String userCertId = (String) userCertificate
						.getPropertyValue(getPropertyManager().getCertificateIdPropertyName());
				userCertificates.put(userCertId, userCertificate);
			}
			String memberId = (String) getProfile()
					.getPropertyValue(this.getPropertyManager().getLoyaltyNumberPropertyName());

			final String METHOD_NAME = "updateCertificatesStatus";

			UserTransactionDemarcation td = null;
			try {
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

				boolean updateOrder = false;
				Set<LoyaltyCertificate> validatedCertificates = findValidatedCertificates(
						loyaltyCertificates, certificateIds, memberId);

				if (validatedCertificates != null) {
					for (LoyaltyCertificate validatedCertificate : validatedCertificates) {
						String certificateId = validatedCertificate.getId();
						boolean valid = validatedCertificate.isValid();
						if (valid) {
							MutableRepositoryItem profileCertificate = (MutableRepositoryItem) userCertificates
									.get(certificateId);
							if (profileCertificate != null) {
								profileCertificate
										.setPropertyValue(getPropertyManager().getCertificateStatusPropertyName(),
												ProfileRewardCertificateStatus.APPLIED.getValue());
							}
							// check the type of order certificate, if the type is null (legacy certificates
							// didn't have reward type persisted) need to update the type
							if (this.updateRewardType(order, certificateId, validatedCertificate.getType())) {
								updateOrder = true;
							}
						} else {
							try {
								removeCertificateFromOrder(order, certificateId);
								final MutableRepositoryItem certificateItem = (MutableRepositoryItem) userCertificates
										.get(certificateId);
								if (certificateItem != null) {
									certificateItem
											.setPropertyValue(getPropertyManager().getCertificateStatusPropertyName(),
													ProfileRewardCertificateStatus.INITIAL.getValue());
								}
								updateOrder = true;
							} catch (RepositoryException e) {
								if (isLoggingError()) {
									logError("checkInvalidCertificates::RepositoryException-->", e);
								}
								throw e;
							}
						}
					}
				}
				if (updateOrder) {
					try {
						getOrderManager().updateOrder(order);
					} catch (CommerceException e) {
						if (isLoggingError()) {
							logError("checkInvalidCertificates::updateOrder::CommerceException-->", e);
						}
						throw e;
					}
				}

			} catch (Exception e) {
				if (isLoggingError()) {
					logError("checkInvalidCertificates::Exception-->", e);
				}
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
			}
		}
	}

	/**
	 * Finds the certificate matching the given id
	 *
	 * @param profile
	 * @return
	 * @throws RepositoryException
	 */
	public List<LoyaltyCertificate> getProfileCertificates(final Profile profile) {
		List<LoyaltyCertificate> loyaltyCerts = new ArrayList<>();
		List<LoyaltyCertificate> rewardCerts = new ArrayList<>();
		List<RepositoryItem> certificates = getProfileCertificatesUsingRepository(profile);
		if (null != certificates && !certificates.isEmpty()) {
			Iterables.addAll(rewardCerts,
					Iterables.transform(certificates, new ToProfileCertificateAdapter()));
		}
		for (LoyaltyCertificate loyaltyCert : rewardCerts) {
			if (DigitalBaseConstants.REWARD_CERT_TYPE.equalsIgnoreCase(loyaltyCert.getType()) ||
					DigitalBaseConstants.REWARD_REWARD_TYPE.equalsIgnoreCase(loyaltyCert.getType())) {
				loyaltyCerts.add(loyaltyCert);
			}
		}
		Collections.sort(loyaltyCerts, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE);
		return loyaltyCerts;
	}

	/**
	 *
	 * @param profile
	 * @return
	 * @throws RepositoryException */
	public List<LoyaltyCertificate>  getProfileOffers( final Profile profile ) {
		List<LoyaltyCertificate> loyaltyCerts = new ArrayList<>();
		List<LoyaltyCertificate> rewardCerts = new ArrayList<>();
		List<RepositoryItem> certificates = getProfileCertificatesUsingRepository(profile);
		if( null != certificates && !certificates.isEmpty() ) {
			Iterables.addAll( rewardCerts, Iterables.transform( certificates , new ToProfileCertificateAdapter() ) );
		}
		for(LoyaltyCertificate loyaltyCert : rewardCerts){
			if(REWARD_TYPE_BYVENDOR_OFFER.equalsIgnoreCase(loyaltyCert.getType())){
				loyaltyCerts.add(loyaltyCert);
			}
		}
		Collections.sort( loyaltyCerts, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE );
		return loyaltyCerts;
	}

	/**
	 * Finds the Offers
	 * 
	 * @param profile
	 * @param rewardsResp
	 * @return List<LoyaltyCertificate>
	 * @throws RepositoryException
	 */
	public List<LoyaltyCertificate> getProfileOffers(final Profile profile,
			final RewardsRetrieveRewardCertificatesResponse rewardsResp) {
		List<LoyaltyCertificate> rewardOffers = new ArrayList<>();
		List<LoyaltyCertificate> rewardCerts = new ArrayList<>();
		List<RepositoryItem> certificates = loadProfileCertsAndOffersUsingLoyaltyService(profile, rewardsResp);

		if (null != certificates && !certificates.isEmpty()) {
			Iterables.addAll(rewardCerts, Iterables.transform(certificates, new ToProfileCertificateAdapter()));
		}
		for (LoyaltyCertificate loyaltyCert : rewardCerts) {
			if (REWARD_TYPE_BYVENDOR_OFFER.equalsIgnoreCase(loyaltyCert.getType())) {
				rewardOffers.add(loyaltyCert);				
			}
		}
		Collections.sort(rewardOffers, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE);
		return rewardOffers;
	}

	/**
	 * Finds the Offers
	 *
	 * @param profile
	 * @return List<LoyaltyCertificate>
	 * @throws RepositoryException
	 */
	public Map<String, List<LoyaltyCertificate>> getProfileOffersByType(Profile profile) {
		List<LoyaltyCertificate> rewardOffers = new ArrayList<>();
		List<LoyaltyCertificate> rewardBirthDayOffers = new ArrayList<>();
		List<LoyaltyCertificate> rewardDoublePointsOffers = new ArrayList<>();
		List<LoyaltyCertificate> rewardTriplePointsOffers = new ArrayList<>();
		List<LoyaltyCertificate> rewardCerts = new ArrayList<>();
		List<RepositoryItem> certificates;
		certificates = getProfileCertificatesUsingRepository(profile);
		if (null != certificates && !certificates.isEmpty()) {
			Iterables.addAll(rewardCerts, Iterables.transform(certificates, new ToProfileCertificateAdapter()));
		}
		for (LoyaltyCertificate loyaltyCert : rewardCerts) {
			if (REWARD_TYPE_BYVENDOR_OFFER.equalsIgnoreCase(loyaltyCert.getType())
					&& isBirthdayOffer(loyaltyCert.getCertificateNumber())) {
				rewardBirthDayOffers.add(loyaltyCert);
			}else if (REWARD_TYPE_BYVENDOR_OFFER.equalsIgnoreCase(loyaltyCert.getType())
					&& isDoublePointsOffer(loyaltyCert.getCertificateNumber())) {
				rewardDoublePointsOffers.add(loyaltyCert);
			}else if (REWARD_TYPE_BYVENDOR_OFFER.equalsIgnoreCase(loyaltyCert.getType())
					&& isTriplePointsOffer(loyaltyCert.getCertificateNumber())) {
				rewardTriplePointsOffers.add(loyaltyCert);
			}else if (REWARD_TYPE_BYVENDOR_OFFER.equalsIgnoreCase(loyaltyCert.getType())) {
				rewardOffers.add(loyaltyCert);
			}
		}
		Collections.sort(rewardOffers, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE);
		Collections.sort(rewardBirthDayOffers, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE);
		Collections.sort(rewardDoublePointsOffers, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE);
		Collections.sort(rewardTriplePointsOffers, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE);

		Map<String, List<LoyaltyCertificate>> rewardOffersByType = new HashMap<>();

		rewardOffersByType.put(REWARD_TYPE_OFFER, rewardOffers);
		rewardOffersByType.put(REWARD_TYPE_BIRTHDAY_OFFER, rewardBirthDayOffers);
		rewardOffersByType.put(REWARD_TYPE_DOUBLE_POINTS_OFFER, rewardDoublePointsOffers);
		rewardOffersByType.put(REWARD_TYPE_TRIPLE_POINTS_OFFER, rewardTriplePointsOffers);
		return rewardOffersByType;
	}

	/**
	 * Finds the Offers
	 *
	 * @param rewardsResp
	 * @param profile
	 * @return List<LoyaltyCertificate>
	 * @throws RepositoryException
	 */
	public Map<String, List<LoyaltyCertificate>> getProfileOffersByType(final RewardsRetrieveRewardCertificatesResponse rewardsResp, Profile profile) {
		List<LoyaltyCertificate> rewardOffers = new ArrayList<>();
		List<LoyaltyCertificate> rewardBirthDayOffers = new ArrayList<>();
		List<LoyaltyCertificate> rewardDoublePointsOffers = new ArrayList<>();
		List<LoyaltyCertificate> rewardTriplePointsOffers = new ArrayList<>();
		List<LoyaltyCertificate> rewardCerts = new ArrayList<>();
		List<RepositoryItem> certificates = loadProfileCertsAndOffersUsingLoyaltyService(profile, rewardsResp);

		if (null != certificates && !certificates.isEmpty()) {
			Iterables.addAll(rewardCerts, Iterables.transform(certificates, new ToProfileCertificateAdapter()));
		}
		for (LoyaltyCertificate loyaltyCert : rewardCerts) {
			if (REWARD_TYPE_BYVENDOR_OFFER.equalsIgnoreCase(loyaltyCert.getType())
					&& isBirthdayOffer(loyaltyCert.getCertificateNumber())) {
				rewardBirthDayOffers.add(loyaltyCert);
			}else if (REWARD_TYPE_BYVENDOR_OFFER.equalsIgnoreCase(loyaltyCert.getType())
					&& isDoublePointsOffer(loyaltyCert.getCertificateNumber())) {
				rewardDoublePointsOffers.add(loyaltyCert);
			}else if (REWARD_TYPE_BYVENDOR_OFFER.equalsIgnoreCase(loyaltyCert.getType())
					&& isTriplePointsOffer(loyaltyCert.getCertificateNumber())) {
				rewardTriplePointsOffers.add(loyaltyCert);
			}else if (REWARD_TYPE_BYVENDOR_OFFER.equalsIgnoreCase(loyaltyCert.getType())) {
				rewardOffers.add(loyaltyCert);
			}
		}
		Collections.sort(rewardOffers, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE);
		Collections.sort(rewardBirthDayOffers, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE);
		Collections.sort(rewardDoublePointsOffers, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE);
		Collections.sort(rewardTriplePointsOffers, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE);

		Map<String, List<LoyaltyCertificate>> rewardOffersByType = new HashMap<>();

		rewardOffersByType.put(REWARD_TYPE_OFFER, rewardOffers);
		rewardOffersByType.put(REWARD_TYPE_BIRTHDAY_OFFER, rewardBirthDayOffers);
		rewardOffersByType.put(REWARD_TYPE_DOUBLE_POINTS_OFFER, rewardDoublePointsOffers);
		rewardOffersByType.put(REWARD_TYPE_TRIPLE_POINTS_OFFER, rewardTriplePointsOffers);
		return rewardOffersByType;
	}

	public boolean isBirthdayOffer(String discountCode){
		boolean birthDayOffer = false;
		if(DigitalStringUtil.isNotBlank(discountCode)) {
			List<String> rewardsBirthdayClubDiscountCode = dswConstants.getRewardsBirthdayClubDiscountCode();
			List<String> rewardsBirthdayGoldDiscountCode = dswConstants.getRewardsBirthdayGoldDiscountCode();
			List<String> rewardsBirthdayEliteDiscountCode = dswConstants.getRewardsBirthdayEliteDiscountCode();

			if(rewardsBirthdayClubDiscountCode.contains(discountCode) ||
					rewardsBirthdayGoldDiscountCode.contains(discountCode) ||
					rewardsBirthdayEliteDiscountCode.contains(discountCode)){
				birthDayOffer = true;
			}
		}
		return birthDayOffer;
	}

	public boolean isDoublePointsOffer(String discountCode){
		boolean doublePointsOffer = false;
		if(DigitalStringUtil.isNotBlank(discountCode)) {
			List<String> rewardsDoublePointOneDiscountCode = dswConstants.getRewards2XPointsOneDiscountCode();
			List<String> rewardsDoublePointTwoDiscountCode = dswConstants.getRewards2XPointsTwoDiscountCode();

			if(rewardsDoublePointOneDiscountCode.contains(discountCode) ||
					rewardsDoublePointTwoDiscountCode.contains(discountCode)){
				doublePointsOffer = true;
			}
		}
		return doublePointsOffer;
	}

	public boolean isTriplePointsOffer(String discountCode){
		boolean triplePointsOffer = false;
		if(DigitalStringUtil.isNotBlank(discountCode)) {
			List<String> rewardsTriplePointOneDiscountCode = dswConstants.getRewards3XPointsDiscountCode();

			if(rewardsTriplePointOneDiscountCode.contains(discountCode)){
				triplePointsOffer = true;
			}
		}
		return triplePointsOffer;
	}

	protected List<RepositoryItem> getProfileCertificatesUsingRepository(final Profile profile){
		List<RepositoryItem>  userCertificates = new ArrayList<>();
		
		RepositoryItem profileItem = null;
		try {
			if(null != profile) {
				MutableRepository profileRepository = (MutableRepository) profile.getRepository();
				if (null != profileRepository) {
					profileItem = profileRepository.getItem(profile.getRepositoryId(), getPropertyManager().getUser());
				}
			}
		} catch( RepositoryException e ) {
			if( isLoggingError() ) {
				logError( String.format( "Unable to fetch certificates for %s", profile.getRepositoryId() ), e );
			}
		}
		
		if(profileItem == null){
			return userCertificates;
		}
		
		userCertificates = (List<RepositoryItem>)profileItem.getPropertyValue(getPropertyManager().getCertificatePropertyName());
		
		return userCertificates;
	}

	/**
	 *
	 * @param profile
	 */
	public int mergeShopForsUsingLoyaltyServiceToProfile(Profile profile) throws DigitalAppException{

		RewardsRetrieveShopforResponse shopForsResponse
				= this.retrieveRewardShopForItemsByProfile(profile.getRepositoryId());
		int shopforCount = 0;

		if(shopForsResponse != null && shopForsResponse.isRequestSuccess()){
			List<RewardsShopforItem> shopforItems = shopForsResponse.getItems();
			shopforCount = shopforItems.size();
			try {
				// sync the Rewards service response for Shopfor with ATG Profile.
				// Do this, irrespective of if shopForsResponse is blank or not.
				// This is required to ensure dormant items are removed from ATG Profile
				shopforTools.mergeShopForItemsToProfile(profile, shopforItems);
			} catch (DigitalAppException e) {
				if (isLoggingError()) {
					logError(String.format("Unable to fetch Shopfor items for %s", profile.getRepositoryId()), e);
				}
				throw new DigitalAppException(e.getMessage());
			}

			if( isLoggingDebug() ) {
				logDebug( "Shop for List is " + shopforItems );
			}
		}
		return shopforCount;
	}

	/**
	 *
	 * @param profile
	 */
	public int mergeBirthdayGiftsUsingLoyaltyServiceToProfile(Profile profile){

		int bDayGiftsCount = 0;

		RewardsRetrieveBirthdayGiftsResponse birthdayGiftsResponse
				= this.retrieveRewardBirthdayGiftsByProfile(profile.getRepositoryId());

		if(birthdayGiftsResponse != null && birthdayGiftsResponse.isRequestSuccess()){
			bDayGiftsCount = birthdayGiftsResponse.getUsedCount();
			List<RewardsGiftItem> birthdayGifts = birthdayGiftsResponse.getItems();
			try {
				// sync with rewards birthday gift data in order to handle combine scenarios
				// even if the list is empty so that any active one in ATG can be marked as
				// deleted
				birthdayGiftTools.mergeBirthDayGiftsToProfile(profile, birthdayGifts);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(String.format("Unable to fetch birth day gifts for %s", profile.getRepositoryId()), e);
				}
			}

			if( isLoggingDebug() ) {
				logDebug( "Birthday Gifts List is " + birthdayGifts );
			}
		}

		return bDayGiftsCount;
	}
	
	/**
	 * Loads the certs & offers by making Rewards WS call
	 * 
	 * @param profile
	 * @return
	 */
	public List<RepositoryItem> loadProfileCertsAndOffersUsingLoyaltyService(Profile profile) {
		
		List<RepositoryItem> userCertificateList = new ArrayList<>();
		String profileId = profile.getRepositoryId();
		String memberId = (String)getProfile().getPropertyValue( this.getPropertyManager().getLoyaltyNumberPropertyName() );
		
		List<LoyaltyCertificate> loyaltyCertificateList = this.retrieveRewardsCertificates(memberId, profileId, null);
		
		if( loyaltyCertificateList != null && !loyaltyCertificateList.isEmpty()) {
			MutableRepository profileRepository = (MutableRepository)profile.getRepository();
			for( LoyaltyCertificate loyaltyCertificate : loyaltyCertificateList ) {
				try {
					MutableRepositoryItem userCertificate = profileRepository.createItem( this.getPropertyManager().getLoyaltyCertificateItemDescriptorName() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateIdPropertyName(), loyaltyCertificate.getId() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateNumberPropertyName(), loyaltyCertificate.getCertificateNumber() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateValuePropertyName(), loyaltyCertificate.getValue());
					userCertificate.setPropertyValue( getPropertyManager().getExpirationDatePropertyName(), loyaltyCertificate.getExpirationDate() );
					userCertificate.setPropertyValue( getPropertyManager().getIssueDatePropertyName(), loyaltyCertificate.getIssueDate() );
					userCertificate.setPropertyValue( getPropertyManager().getMarkdownCodePropertyName(), loyaltyCertificate.getMarkdownCode() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateTypePropertyName(), loyaltyCertificate.getType() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateStatusPropertyName(), ProfileRewardCertificateStatus.INITIAL.getValue() );
					userCertificate.setPropertyValue( getPropertyManager().getOfferDisplayNamePropertyName(), loyaltyCertificate.getOfferDisplayName() );
					userCertificate.setPropertyValue( getPropertyManager().getOfferDetailsPageNamePropertyName(), loyaltyCertificate.getOfferDetailsPageName());
					userCertificateList.add( userCertificate );
				} catch( RepositoryException re ) {
					if( isLoggingError() ) {
						logError( "getProfileCertificatesUsingLoyaltyService - generate Repository Exception", re );
					}
				}
			}
			getProfile().setPropertyValue( getPropertyManager().getCertificatePropertyName(), userCertificateList );
			if( isLoggingDebug() ) {
				logDebug( "userCertificateList is " + userCertificateList );
			}
		}
		
		return userCertificateList;
	}
	
	/**
	 * Loads the certs & offers by using the response from previously made call
	 * 
	 * @param profile
	 * @param rewardsResp
	 * @return
	 */
	public List<RepositoryItem> loadProfileCertsAndOffersUsingLoyaltyService(Profile profile, RewardsRetrieveRewardCertificatesResponse rewardsResp) {
		
		List<RepositoryItem> userCertificateList = new ArrayList<>();
		String profileId = profile.getRepositoryId();
		String memberId = (String)getProfile().getPropertyValue( this.getPropertyManager().getLoyaltyNumberPropertyName() );
		
		List<LoyaltyCertificate> loyaltyCertificateList = this.retrieveRewardsCertificates(memberId, profileId, rewardsResp);
		
		if( loyaltyCertificateList != null && !loyaltyCertificateList.isEmpty()) {
			MutableRepository profileRepository = (MutableRepository)profile.getRepository();
			for( LoyaltyCertificate loyaltyCertificate : loyaltyCertificateList ) {
				try {
					MutableRepositoryItem userCertificate = profileRepository.createItem( this.getPropertyManager().getLoyaltyCertificateItemDescriptorName() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateIdPropertyName(), loyaltyCertificate.getId() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateNumberPropertyName(), loyaltyCertificate.getCertificateNumber() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateValuePropertyName(), loyaltyCertificate.getValue());
					userCertificate.setPropertyValue( getPropertyManager().getExpirationDatePropertyName(), loyaltyCertificate.getExpirationDate() );
					userCertificate.setPropertyValue( getPropertyManager().getIssueDatePropertyName(), loyaltyCertificate.getIssueDate() );
					userCertificate.setPropertyValue( getPropertyManager().getMarkdownCodePropertyName(), loyaltyCertificate.getMarkdownCode() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateTypePropertyName(), loyaltyCertificate.getType() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateStatusPropertyName(), ProfileRewardCertificateStatus.INITIAL.getValue() );
					userCertificate.setPropertyValue( getPropertyManager().getOfferDisplayNamePropertyName(), loyaltyCertificate.getOfferDisplayName() );
					userCertificate.setPropertyValue( getPropertyManager().getOfferDetailsPageNamePropertyName(), loyaltyCertificate.getOfferDetailsPageName());
					userCertificateList.add( userCertificate );
				} catch( RepositoryException re ) {
					if( isLoggingError() ) {
						logError( "getProfileCertificatesUsingLoyaltyService - generate Repository Exception", re );
					}
				}
			}
			getProfile().setPropertyValue( getPropertyManager().getCertificatePropertyName(), userCertificateList );
			if( isLoggingDebug() ) {
				logDebug( "userCertificateList is " + userCertificateList );
			}
		}
		
		return userCertificateList;
	}

	/**
	 * Loads the certs & offers by making Rewards WS call
	 * 
	 * @param profile
	 * @param order
	 * @return
	 */
	public List<RepositoryItem> loadProfileAndOrderCertsAndOffersUsingLoyaltyService(Profile profile, Order order, RewardsRetrieveRewardCertificatesResponse rewardsResp) {
		
		List<RepositoryItem> userCertificateList = new ArrayList<>();
		String profileId = profile.getRepositoryId();
		String memberId = (String)getProfile().getPropertyValue( this.getPropertyManager().getLoyaltyNumberPropertyName() );
		List<LoyaltyCertificate> loyaltyCertificateList = this.retrieveRewardsCertificates(memberId, profileId, rewardsResp);
		
		if( loyaltyCertificateList != null && !loyaltyCertificateList.isEmpty()) {
			MutableRepository profileRepository = (MutableRepository)profile.getRepository();
			for( LoyaltyCertificate loyaltyCertificate : loyaltyCertificateList ) {
				try {
					MutableRepositoryItem userCertificate = profileRepository.createItem( this.getPropertyManager().getLoyaltyCertificateItemDescriptorName() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateIdPropertyName(), loyaltyCertificate.getId() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateNumberPropertyName(), loyaltyCertificate.getCertificateNumber() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateValuePropertyName(), loyaltyCertificate.getValue());
					userCertificate.setPropertyValue( getPropertyManager().getExpirationDatePropertyName(), loyaltyCertificate.getExpirationDate() );
					userCertificate.setPropertyValue( getPropertyManager().getIssueDatePropertyName(), loyaltyCertificate.getIssueDate() );
					userCertificate.setPropertyValue( getPropertyManager().getMarkdownCodePropertyName(), loyaltyCertificate.getMarkdownCode() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateTypePropertyName(), loyaltyCertificate.getType() );
					userCertificate.setPropertyValue( getPropertyManager().getCertificateStatusPropertyName(), ProfileRewardCertificateStatus.INITIAL.getValue() );
					userCertificate.setPropertyValue( getPropertyManager().getOfferDisplayNamePropertyName(), loyaltyCertificate.getOfferDisplayName() );
					userCertificate.setPropertyValue( getPropertyManager().getOfferDetailsPageNamePropertyName(), loyaltyCertificate.getOfferDetailsPageName());
					userCertificateList.add( userCertificate );
				} catch( RepositoryException re ) {
					if( isLoggingError() ) {
						logError( "getProfileCertificatesUsingLoyaltyService - generate Repository Exception", re );
					}
				}
			}
			if( isLoggingDebug() ) {
				logDebug( "userCertificateList is " + userCertificateList );
			}
		}
		getProfile().setPropertyValue( getPropertyManager().getCertificatePropertyName(), userCertificateList );
		updateCertificatesStatus(order, loyaltyCertificateList, userCertificateList);
		
		return userCertificateList;
	}

	/** 
	 * Changes the certificate status for the given certificate (it is located off the profile).
	 * 
	 * @param certificateId
	 * @param status
	 * @throws RepositoryException */
	public void changeCertificateStatus( final Profile profile, String certificateId, String status ) {
		final MutableRepositoryItem certificateItem = getProfileCertificate( profile, certificateId );
		if( certificateItem != null ) {
			certificateItem.setPropertyValue( this.getPropertyManager().getCertificateStatusPropertyName(), status );
		}
	}

	/** 
	 * Finds the certificate matching the given id
	 * 
	 * @param certificateId
	 * @return
	 * @throws RepositoryException */
	public MutableRepositoryItem getProfileCertificate( final Profile profile, final String certificateId ) {

		RepositoryItem profileItem = null;
		try {
			MutableRepository profileRepository = (MutableRepository)profile.getRepository();
			profileItem = profileRepository.getItem( profile.getRepositoryId(), getPropertyManager().getUser() );
		} catch( RepositoryException e ) {
			if( isLoggingDebug() ) {
				logDebug( String.format( "Unable to fetch certificates %s for %s", certificateId, profile.getRepositoryId() ), e );
			}
		}
		MutableRepositoryItem retVal = null;
		if( profileItem != null ) {
			List<RepositoryItem> certificates = (List<RepositoryItem>)profileItem.getPropertyValue( 
					getPropertyManager().getCertificatePropertyName() );
			if( certificates != null ) {
				Iterator<RepositoryItem> filtered = Iterables.filter( certificates, new DigitalPredicate<RepositoryItem>() {

					@Override
					public boolean apply( RepositoryItem input ) {
						return certificateId.equals( input.getPropertyValue( 
								getPropertyManager().getCertificateIdPropertyName() ) );
					}

				} ).iterator();
				if( filtered.hasNext() ) {
					retVal = (MutableRepositoryItem)filtered.next();
				}
			}
		}
		return retVal;
	}
	
	/** Retrieve a list of available loyalty certificate value objects for the
	 * given member. The certificates are ordered by expiration date.
	 * 
	 * @param pMemberId
	 *            string value of member id
	 * @param pProfileId
	 *            String value of the ATG profile Id
	 * @return a list of loyalty certs objects or empty
	 */
	public List<LoyaltyCertificate> retrieveRewardsCertificates(String pMemberId, String pProfileId,
			RewardsRetrieveRewardCertificatesResponse rewardsResp) {
		if (this.isLoggingDebug()) {
			logDebug("Enter : retrieveRewardsCertificates(String " + pMemberId + ",String " + pProfileId + ")");
		}

		List<LoyaltyCertificate> rewardCerts = new ArrayList<>();
		try {

			List<Certificate> certs = null;

			if (rewardsResp != null) {
				certs = rewardsResp.getCertificates();
			} else {

				RewardServiceResponse selectCertificatesResponse = null;
				RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();

				if (DigitalStringUtil.isNotBlank(pProfileId) && rewardService.isServiceEnabled() && rewardService.isServiceMethodEnabled(
						RewardService.ServiceMethod.SELECT_CERTIFICATES_BY_PROFILEID.getServiceMethodName())) {
					rewardServiceRequest.getPerson().setProfileID(pProfileId);
					selectCertificatesResponse = rewardService
							.selectAvailableCertificatesByProfileID(rewardServiceRequest);
				} else if (DigitalStringUtil.isNotBlank(pMemberId) && rewardService.isServiceEnabled()
						&& rewardService.isServiceMethodEnabled(
								RewardService.ServiceMethod.SELECT_CERTIFICATES_BY_MEMBERID.getServiceMethodName())) {
					rewardServiceRequest.getPerson().setMemberID(pMemberId);
					selectCertificatesResponse = rewardService
							.selectAvailableCertificatesByMemberID(rewardServiceRequest);
				}

				if (selectCertificatesResponse != null) {
					certs = selectCertificatesResponse.getCertificates();
				}
			}

			if (null != certs && !certs.isEmpty()) {
				Iterables.addAll(rewardCerts, Iterables.transform(certs, new ToValidCertificateAdapter()));
			}
			Collections.sort(rewardCerts, ID_COMPARATOR_WITH_SAME_DATE_SAME_VALUE);
			if (isLoggingDebug()) {
				logDebug("End: retrieveRewardsCertificates : Number of certs : " + rewardCerts.size());
			}
			
			//TODO: Loop through rewards certs and if the certs displayName is empty then populate the display name from the configs based on the points type.
			updateOfferDisplayNameOnCerts(rewardCerts);

		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving selectAvailableCertificatesByMemberID", e);
		}
		return rewardCerts;
	}

	public void updateOfferDisplayNameOnCerts(
			List<LoyaltyCertificate> rewardCerts) {
		// TODO Auto-generated method stub
		if(null!=rewardCerts && !rewardCerts.isEmpty()){
			for(LoyaltyCertificate cert:rewardCerts){
				String certName=cert.getOfferDisplayName();
				if(DigitalStringUtil.isBlank(certName)){
					if(isDoublePointsOffer(cert.getCertificateNumber())){
						cert.setOfferDisplayName(dswConstants.getDoublePointsOfferDisplayName());
					}else if(isTriplePointsOffer(cert.getCertificateNumber())){
						cert.setOfferDisplayName(dswConstants.getTriplePointsOfferDisplayName());
					}

				}

			}

		}
	}

	/**
	 *
	 * @param profile
	 * @return
	 */
	public RewardsRetrieveRewardCertificatesResponse retrieveRewardCertificatesByProfile(Profile profile) {
		String METHOD_NAME = "RETRIEVE_REWARDS_CERTIFICATES_PROFILE";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsRetrieveRewardCertificatesResponse response = new RewardsRetrieveRewardCertificatesResponse();

		boolean isValid = true;

		try {

			if (DigitalStringUtil.isEmpty(profile.getRepositoryId())) {
				response.getGenericExceptions().add(new ResponseError(ERR_INVALID_MEMBERID_PROFILEID,
						"Either profileid or memberid must be populated"));
				isValid = false;
				response.setFormError(false);

			}
			RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();
			RewardServiceResponse selectCertificatesResponse = null;

			if (isValid) {
				if (profile.getRepositoryId()!= null) {
					rewardServiceRequest.getPerson()
							.setProfileID(profile.getRepositoryId());


					selectCertificatesResponse = rewardService.selectAvailableCertificatesByProfileID(rewardServiceRequest);
				}
			}
			if(null!=selectCertificatesResponse) {
				response.setCertificates(selectCertificatesResponse.getCertificates());
				response.setTotalCertValue(selectCertificatesResponse.getTotalCertValue());
			}
			response.setRequestSuccess(isValid);

		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving addShopWithoutACardRequest", e);

			String errorCode = "ServiceError";

			String causeMsg = e.getMessage();
			if (DigitalStringUtil.isNotBlank(causeMsg)) {
				if (causeMsg.contains("Customer not found")) {
					errorCode = "invalidMember";
					response.setValidLoyaltyMember(false);
				}
			}

			String msg = this.getMessageLocator().getMessageString(errorCode);

			response.getGenericExceptions().add(new ResponseError(errorCode, msg));
			response.setRequestSuccess(false);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}
		return response;
	}

	/**
	 * @param profileId
	 * @return RewardsRetrieveShopforResponse
	 * Get Shopfor items for the given Profile ID from Rewards System
	 */
	public RewardsRetrieveShopforResponse retrieveRewardShopForItemsByProfile(
			String profileId) {
		String METHOD_NAME = "RETRIEVE_REWARDS_SHOPFOR_PROFILE";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsRetrieveShopforResponse response = new RewardsRetrieveShopforResponse();

		boolean isValid = true;

		try {

			if (DigitalStringUtil.isEmpty(profileId)) {
				response.getGenericExceptions().add(new ResponseError(ERR_INVALID_MEMBERID_PROFILEID,
						"Either profileid must be populated"));
				isValid = false;
				response.setFormError(false);
			}
			RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();
			RewardServiceResponse rewardServiceResponse = null;

			if (isValid) {
				rewardServiceRequest.getPerson().setProfileID(profileId);
				rewardServiceResponse = rewardService.retrieveShopFors(rewardServiceRequest);
			}
			if (null != rewardServiceResponse) {
				List<Shopfor> shopforIntegrationDomainList = rewardServiceResponse.getShopfors();
				List<RewardsShopforItem> rewardsShopforItemsList = new ArrayList<>();
				if (shopforIntegrationDomainList != null) {
					for (Shopfor shopforIntegration : shopforIntegrationDomainList) {
						RewardsShopforItem shopforItem = new RewardsShopforItem();
						shopforItem.setRewardsShopforID(shopforIntegration.getRewardsShopforID());
						shopforItem.setProfileID(shopforIntegration.getProfileId());
						shopforItem.setShopforName(shopforIntegration.getShopforName());
						shopforItem.setRelationship(shopforIntegration.getRelationship());
						shopforItem.setBirthDay(shopforIntegration.getBirthDay());
						shopforItem.setActive(shopforIntegration.isActive());
						shopforItem.setBirthMonth(shopforIntegration.getBirthMonth());
						shopforItem.setWebType(shopforIntegration.getWebType());
						shopforItem.setGender(shopforIntegration.getGender());
						shopforItem.setSizeGroup(shopforIntegration.getSizeGroup());
						if (shopforIntegration.getSizes() != null)
							shopforItem.setSizes(shopforIntegration.getSizes().toArray(new String[0]));
						else
							shopforItem.setSizes(null);
						if (shopforIntegration.getWidths() != null)
							shopforItem.setWidths(shopforIntegration.getWidths().toArray(new String[0]));
						else
							shopforItem.setWidths(null);
						shopforItem.setDateCreated(shopforIntegration.getDateCreated());
						rewardsShopforItemsList.add(shopforItem);
					}
				}
				response.setItems(rewardsShopforItemsList);
			}
			response.setRequestSuccess(isValid);

		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving Shopfor Items from Rewards System", e);

			String errorCode = "ServiceError";

			String causeMsg = e.getMessage();
			if (DigitalStringUtil.isNotBlank(causeMsg)) {
				if (causeMsg.contains("Customer not found")) {
					errorCode = "invalidMember";
					response.setValidLoyaltyMember(false);
				}
			}

			String msg = this.getMessageLocator().getMessageString(errorCode);

			response.getGenericExceptions().add(new ResponseError(errorCode, msg));
			response.setRequestSuccess(false);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}
		return response;
	}

	/**
	 * @param profileId
	 * @return RewardsRetrieveBirthdayGiftsResponse
	 */
	public RewardsRetrieveBirthdayGiftsResponse retrieveRewardBirthdayGiftsByProfile(
			String profileId) {
		String METHOD_NAME = "RETRIEVE_REWARDS_BIRTHDAY_GIFTS_PROFILE";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsRetrieveBirthdayGiftsResponse response = new RewardsRetrieveBirthdayGiftsResponse();

		boolean isValid = true;

		try {

			if (DigitalStringUtil.isEmpty(profileId)) {
				response.getGenericExceptions().add(new ResponseError(ERR_INVALID_MEMBERID_PROFILEID,
						"Either profileid must be populated"));
				isValid = false;
				response.setFormError(false);
			}
			RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();
			RewardServiceResponse rewardServiceResponse = null;

			if (isValid) {
				rewardServiceRequest.getPerson().setProfileID(profileId);
				rewardServiceResponse = rewardService.retrieveBirthdayGifts(rewardServiceRequest);
			}
			if (null != rewardServiceResponse) {
				List<BirthdayGift> birthdayGiftList = rewardServiceResponse.getBirthdayGifts();
				List<RewardsGiftItem> giftItemList = new ArrayList<>();
				if (birthdayGiftList != null) {
					response.setUsedCount(birthdayGiftList.size());
					for (BirthdayGift gift : birthdayGiftList) {
						RewardsGiftItem giftItem = new RewardsGiftItem();
						giftItem.setId(gift.getBirthdayGiftId());
						giftItem.setEmail(gift.getEmail());
						giftItem.setFirstName(gift.getFriendName());
						giftItem.setStatus(gift.getLocked());
						giftItem.setGiftMessage(gift.getMessage());
						if (DigitalStringUtil.isNotBlank(gift.getDeliveryDate())) {
							try {
								Date deliveryDate = DigitalDateUtil
										.parseDate(gift.getDeliveryDate(), new String[]{"yyyy-MM-dd'T'HH:mm:ss"});
								giftItem.setGiftSendDate(deliveryDate);
								Calendar calendar = Calendar.getInstance();
								calendar.setTime(deliveryDate);
								giftItem.setBirthDay(Integer.toString(calendar.get(Calendar.DATE)));
								giftItem.setBirthMonth(Integer.toString(calendar.get(Calendar.MONTH) + 1));
							} catch (ParseException e) {
								logError("Error parsing birthday gift send date ", e);
							}
						} else {
							String dayOfBirth = gift.getDayOfBirth();
							String monthOfBirth = gift.getMonthOfBirth();

							giftItem.setBirthDay(dayOfBirth);
							giftItem.setBirthMonth(monthOfBirth);

							if (DigitalStringUtil.isNotBlank(dayOfBirth) && DigitalStringUtil.isNotBlank(monthOfBirth)) {
								// derive the date considering blackout days & leap year
								Calendar derivedSendCalendarDate = deriveBirthdayGiftSendDate(dayOfBirth,
										monthOfBirth);
								giftItem.setGiftSendDate(derivedSendCalendarDate.getTime());
								giftItem.setBirthDay(Integer.toString(derivedSendCalendarDate.get(Calendar.DATE)));
								giftItem.setBirthMonth(
										Integer.toString(derivedSendCalendarDate.get(Calendar.MONTH) + 1));
								giftItem.setGiftSendDate(derivedSendCalendarDate.getTime());
							}
						}
						giftItem.setProfileId(gift.getProfileId());
						giftItemList.add(giftItem);
					}
				}
				response.setItems(giftItemList);
			}
			response.setRequestSuccess(isValid);

		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving birthday gifts", e);

			String errorCode = "ServiceError";

			String causeMsg = e.getMessage();
			if (DigitalStringUtil.isNotBlank(causeMsg)) {
				if (causeMsg.contains("Customer not found")) {
					errorCode = "invalidMember";
					response.setValidLoyaltyMember(false);
				}
			}

			String msg = this.getMessageLocator().getMessageString(errorCode);

			response.getGenericExceptions().add(new ResponseError(errorCode, msg));
			response.setRequestSuccess(false);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}
		return response;
	}

	/**
	 *
	 * @param profileId
	 * @return RewardsCharitiesResponse
	 */
	public RewardsCharitiesResponse retrieveRewardCharities(String profileId) {
		String METHOD_NAME = "RETRIEVE_REWARDS_CHARITIES_PROFILE";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsCharitiesResponse response = new RewardsCharitiesResponse();

		boolean isValid = true;

		try {

			if (DigitalStringUtil.isEmpty(profileId)) {
				response.getGenericExceptions().add(new ResponseError(ERR_INVALID_MEMBERID_PROFILEID,
						"Either profileid must be populated"));
				isValid = false;
				response.setFormError(false);
			}
			RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();
			RewardServiceResponse rewardServiceResponse = null;

			if (isValid) {
				rewardServiceRequest.getPerson().setProfileID(profileId);
				rewardServiceResponse = rewardService.retrieveCharities(rewardServiceRequest);
			}
			if(null != rewardServiceResponse) {
				List<Charity> charityList = rewardServiceResponse.getCharities();
				List<RewardsCharityItem> rewardsCharityItems = new ArrayList<>();
				if(charityList != null) {
					response.setCount(charityList.size());
					for(Charity charity : charityList) {
						RewardsCharityItem rewardsCharityItem = new RewardsCharityItem();
						rewardsCharityItem.setCharityId(charity.getId());
						rewardsCharityItem.setCharityName(charity.getName());
						rewardsCharityItem.setCharityDescription(charity.getDescription());
						rewardsCharityItems.add(rewardsCharityItem);
					}
				}
				response.setItems(rewardsCharityItems);
			}
			response.setRequestSuccess(isValid);

		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving charities", e);

			String errorCode = "ServiceError";

			String causeMsg = e.getMessage();
			if (DigitalStringUtil.isNotBlank(causeMsg)) {
				if (causeMsg.contains("Customer not found")) {
					errorCode = "invalidMember";
				}
			}

			String msg = this.getMessageLocator().getMessageString(errorCode);

			response.getGenericExceptions().add(new ResponseError(errorCode, msg));
			response.setRequestSuccess(false);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}
		return response;
	}

	/**
	 *
	 * @param profileId
	 * @return RewardsIncentivesResponse
	 */
	public RewardsIncentivesResponse retrieveRewardIncentives(String profileId) {
		String METHOD_NAME = "RETRIEVE_REWARDS_INCENTIVES_PROFILE";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsIncentivesResponse response = new RewardsIncentivesResponse();

		boolean isValid = true;

		try {

			if (DigitalStringUtil.isEmpty(profileId)) {
				response.getGenericExceptions().add(new ResponseError(ERR_INVALID_MEMBERID_PROFILEID,
						"Either profileid must be populated"));
				isValid = false;
				response.setFormError(false);
			}
			RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();
			RewardServiceResponse rewardServiceResponse = null;

			if (isValid) {
				rewardServiceRequest.getPerson().setProfileID(profileId);
				rewardServiceResponse = rewardService.retrieveIncentives(rewardServiceRequest);
			}
			if(null != rewardServiceResponse) {
				List<Incentive> incentiveList = rewardServiceResponse.getIncentives();
				List<RewardsIncentiveItem> rewardsIncentiveItems = new ArrayList<>();
				if(incentiveList != null) {
					response.setCount(incentiveList.size());
					for(Incentive incentive : incentiveList) {
						RewardsIncentiveItem rewardsIncentiveItem = new RewardsIncentiveItem();
						rewardsIncentiveItem.setDenomination(incentive.getDenomination());
						rewardsIncentiveItem.setPointCost(incentive.getPointCost());
						rewardsIncentiveItem.setToBeRedeemed(incentive.isToBeRedeemed());
						rewardsIncentiveItems.add(rewardsIncentiveItem);
					}
				}
				response.setItems(rewardsIncentiveItems);
			}
			response.setRequestSuccess(isValid);

		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving incentives", e);

			String errorCode = "ServiceError";

			String causeMsg = e.getMessage();
			if (DigitalStringUtil.isNotBlank(causeMsg)) {
				if (causeMsg.contains("Customer not found")) {
					errorCode = "invalidMember";
				}
			}

			String msg = this.getMessageLocator().getMessageString(errorCode);

			response.getGenericExceptions().add(new ResponseError(errorCode, msg));
			response.setRequestSuccess(false);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}
		return response;
	}


	
	public RewardsEnticementsResponse retrieveEnticements(RewardsSendOrderRequest sendOrderRequest) {
		String METHOD_NAME = "RETRIEVE_ENTICEMENTS_POINTS_FOR_ORDER";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsEnticementsResponse response = new RewardsEnticementsResponse();

		boolean isValid = true;

		try {
			
			if (null==sendOrderRequest) {
				response.getGenericExceptions().add(new ResponseError(ERR_INVALID_INPUT_FOR_ENTICEMENTS,"Input for enticement must be populated"));
				isValid = false;
				response.setFormError(false);
			}
			RewardServiceResponse rewardsServiceResponse = null;
			if(isValid){
				rewardsServiceResponse = rewardService.retrieveEnticements(sendOrderRequest);
			}
			
			if(null != rewardsServiceResponse) {
				response.setRequestSuccess(isValid);
				response.setPoints(rewardsServiceResponse.getEnticementsPoints());
			}

		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving enticements", e);
			response.getGenericExceptions().add(new ResponseError(e.getErrorCode(), e.getLocalizedMessage()));
			response.setRequestSuccess(false);
			
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}
		return response;
	}

	/**
	 *
	 * @param rewardsIssueCertsRequest
	 * @return RewardsIssueCertsResponse
	 */
	public RewardsIssueCertsResponse issueRewardCertificate(RewardsIssueCertsRequest rewardsIssueCertsRequest) {
		String METHOD_NAME = "ISSUE_REWARDS_CERTIFICATE";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsIssueCertsResponse response = new RewardsIssueCertsResponse();

		boolean isValid = true;

		try {

			if (DigitalStringUtil.isEmpty(rewardsIssueCertsRequest.getProfileId())) {
				response.getGenericExceptions().add(new ResponseError(ERR_INVALID_MEMBERID_PROFILEID,
						"ProfileId must be populated"));
				isValid = false;
				response.setFormError(false);
			}

			RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();
			RewardServiceResponse rewardServiceResponse = new RewardServiceResponse();
			response.setRequestSuccess(false);

			if (isValid) {
				rewardServiceRequest.getPerson().setProfileID(rewardsIssueCertsRequest.getProfileId());
				rewardServiceRequest.setCertDenomination(rewardsIssueCertsRequest.getCertDenomination());
				rewardServiceResponse = rewardService.requestCertificate(rewardServiceRequest);
			}

			if(null != rewardServiceResponse) {
				if(rewardServiceResponse.isSuccess()) {
					response.setRequestSuccess(true);
				}
				else {
					List<ResponseError> errors = rewardServiceResponse.getGenericExceptions();
					if (errors.isEmpty()) {
						response.setRequestSuccess(false);
					} else {
						populateIssueCertsErrorResponse(errors.get(0).getErrorCode(),
								errors.get(0).getLocalizedMessage(), response);
					}
				}
			}

		} catch (DigitalIntegrationException e) {
			logError("Error while issue reward certificate", e);

			String errorCode = "ServiceError";

			String causeMsg = e.getMessage();
			if (DigitalStringUtil.isNotBlank(causeMsg)) {
				if (causeMsg.contains("Customer not found")) {
					errorCode = ERR_INVALID_MEMBER;
				}
			}

			String msg = this.getMessageLocator().getMessageString(errorCode);

			response.getGenericExceptions().add(new ResponseError(errorCode, msg));
			response.setRequestSuccess(false);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}
		return response;
	}

	/**
	 * @param errorCode
	 * @param causeMsg
	 * @param response
	 */
	private void populateIssueCertsErrorResponse(String errorCode, String causeMsg,
			RewardsIssueCertsResponse response) {
		if (DigitalStringUtil.isEmpty(errorCode)) {
			errorCode = ERR_GENERIC_ISSUE_CERT;
		}
		String msg = "";
		if (DigitalStringUtil.isNotBlank(causeMsg)) {
			if ("600".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer does not have a redeemable reward")) {
				errorCode = ERR_NO_REDEEMABLE_REWARD;
			}
			else if ("UNKNOWN".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer not found")) {
				errorCode = ERR_INVALID_MEMBER;
			}
		}
		if (DigitalStringUtil.isBlank(msg)) {
			if(errorCode.contains("HTTP")) {
				errorCode = ERR_GENERIC_ISSUE_CERT;
			}
			msg = this.getMessageLocator().getMessageString(errorCode);
		}
		response.setFormError(true);
		response.setRequestSuccess(false);
		response.getGenericExceptions().add(new ResponseError(errorCode, msg));
	}


	/**
	 * @param errorCode
	 * @param causeMsg
	 * @param response
	 */
	private void populateDonateCertsErrorResponse(String errorCode, String causeMsg,
      RewardsDonateCertsResponse response) {
		if (DigitalStringUtil.isEmpty(errorCode)) {
			errorCode = ERR_GENERIC_DONATE_CERT;
		}
		String msg = "";

		if (DigitalStringUtil.isNotBlank(causeMsg)) {
			if ("600".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer does not have a reward to donate")) {
				errorCode = ERR_NO_DONATABLE_REWARD;
			}
			else if ("UNKNOWN".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer not found")) {
				errorCode = ERR_INVALID_MEMBER;
			}
		}
		if (DigitalStringUtil.isBlank(msg)) {
			if(errorCode.contains("HTTP")) {
				errorCode = ERR_GENERIC_DONATE_CERT;
			}
			msg = this.getMessageLocator().getMessageString(errorCode);
		}
		response.setFormError(true);
		response.setRequestSuccess(false);
		response.getGenericExceptions().add(new ResponseError(errorCode, msg));
	}

	/**
	 * @param rewardsDonateCertsRequest
	 * @param order
	 * @return RewardsDonateCertsResponse
	 */
	public RewardsDonateCertsResponse donateCertificates(
			RewardsDonateCertsRequest rewardsDonateCertsRequest, DigitalOrderImpl order) {
		String METHOD_NAME = "DONATE_REWARDS_CERTIFICATES";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsDonateCertsResponse response = new RewardsDonateCertsResponse();

		try {

			RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();

			Charity charity = new Charity();
			charity.setId(rewardsDonateCertsRequest.getCharityId());

			rewardServiceRequest.setCharity(charity);

			Person person = new Person();
			person.setProfileID(rewardsDonateCertsRequest.getProfileId());
			person.setMemberID(rewardsDonateCertsRequest.getMemberId());
			rewardServiceRequest.setPerson(person);

			rewardServiceRequest.setDeviceName(getDeviceName());

			Collection<String> donationCertsList = rewardsDonateCertsRequest.getCertificateIdsList();

			String memberId = (String) getProfile()
					.getPropertyValue(this.getPropertyManager().getLoyaltyNumberPropertyName());

			Set<LoyaltyCertificate> validatedCertificates = findValidatedCertificates(
					null, donationCertsList, memberId);

			for (String donationCertId : donationCertsList) {
				boolean isDonationCertificateValid = false;
				for (LoyaltyCertificate userCertificate : validatedCertificates) {
					String userCertId = userCertificate.getId();

					String currentCertStatus = userCertificate.getStatus();

					if (userCertId.equalsIgnoreCase(donationCertId)
							&& (OrderRewardCertificateStatus.VALIDATED.getValue().equals(currentCertStatus) ||
							ProfileRewardCertificateStatus.APPLIED.getValue().equals(currentCertStatus) ||
							ProfileRewardCertificateStatus.INITIAL.getValue().equals(currentCertStatus))) {
						isDonationCertificateValid = true;
					}
				}
				if (!isDonationCertificateValid) {
					throw new DigitalIntegrationBusinessException(
							"Reward is no longer valid",
							ErrorCodes.ERROR_RESPONSE.getCode());
				}
			}

			rewardServiceRequest.setCertificates(rewardsDonateCertsRequest.getCertificateIdsList());

			RewardServiceResponse rewardServiceResponse = rewardService
					.donateCertificates(rewardServiceRequest);

      if(null != rewardServiceResponse) {
        if(rewardServiceResponse.isSuccess()) {
          response.setRequestSuccess(true);
        }
        else {
          List<ResponseError> errors = rewardServiceResponse.getGenericExceptions();
          if (errors.isEmpty()) {
            response.setRequestSuccess(false);
          } else {
            populateDonateCertsErrorResponse(errors.get(0).getErrorCode(),
                errors.get(0).getLocalizedMessage(), response);
          }
        }
      }

		} catch (DigitalIntegrationException e) {
			logError("Error while donating certificates", e);

			String errorCode = "ServiceError";

			String causeMsg = e.getMessage();
			if (DigitalStringUtil.isNotBlank(causeMsg)) {
				if (causeMsg.contains("Reward is no longer valid")) {
					errorCode = "invalidRewardCertificate";
				} else if (causeMsg.contains("Customer not found")) {
					errorCode = "invalidMember";
				}
			}

			String msg = this.getMessageLocator().getMessageString(errorCode);

			response.getGenericExceptions().add(new ResponseError(errorCode, msg));
			response.setRequestSuccess(false);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}
		return response;
	}

  /**
   * @return String
   */
  private String getDeviceName() {
    String deviceName = ServletUtil
        .getCurrentRequest()
        .getHeader(RequestHeaderAttributesConstant.AKAMAI_DEVICE_KEY.getValue());

    if (DigitalStringUtil.isEmpty(deviceName)) {
      deviceName = "desktop";
    }
    return deviceName;
  }

	/**
	 *
	 * @param profile
	 * @param synchRewardsBirthdayGifts
	 * @return RewardsRetrieveBirthdayGiftsResponse
	 */
	public RewardsRetrieveBirthdayGiftsResponse retrieveRewardsBirthDayGifts(Profile profile, boolean synchRewardsBirthdayGifts){
		RewardsRetrieveBirthdayGiftsResponse response = new RewardsRetrieveBirthdayGiftsResponse();
		Object currentLoyaltyTier = profile.getPropertyValue(getPropertyManager().getLoyaltyTierItemDescriptorName());
		if(currentLoyaltyTier != null && !DigitalBaseConstants.USER_TIER_CLUB.equalsIgnoreCase((String) currentLoyaltyTier)) {
			if (synchRewardsBirthdayGifts) {
				//NOTE: Need to call mergeBirthdayGiftsUsingLoyaltyServiceToProfile before reading Bday gift from profile
				mergeBirthdayGiftsUsingLoyaltyServiceToProfile(profile);
			}
			List<RewardsGiftItem> giftItems = birthdayGiftTools.retrieveBirthDayGiftsFromProfile(profile);
			if (giftItems != null && !giftItems.isEmpty()) {
				response.setItems(giftItems);
				response.setUsedCount(giftItems.size());
			}
			response.setAllowedCount(getMaxBDayGiftsLimit());
		}
		return response;
	}

	/**
	 *
	 * @param profile
	 * @param synchRewardsShopfor
	 * @return RewardsRetrieveBirthdayGiftsResponse
	 */
	public RewardsRetrieveShopforResponse retrieveRewardsShopforItems(Profile profile, boolean synchRewardsShopfor) throws DigitalAppException {
		RewardsRetrieveShopforResponse response = new RewardsRetrieveShopforResponse();
		// Who I shop for is for all Club, Gold and Elite members
		if (synchRewardsShopfor) {
			mergeShopForsUsingLoyaltyServiceToProfile(profile);
		}
		List<RewardsShopforItem> shopforItems = shopforTools.retrieveShopforItemsFromProfile(profile);
		if (shopforItems != null && !shopforItems.isEmpty()) {
			response.setItems(shopforItems);
		}
		return  response;
	}

	/**
	 *
	 * @param order
	 * @return true or false
	 * @throws CommerceException
	 * @throws DigitalIntegrationInactiveException
	 */
	public boolean reserveRewardCertificates( final DigitalOrderImpl order ) throws CommerceException, DigitalIntegrationInactiveException {
		boolean retVal = Boolean.TRUE;
		final Set<RepositoryItem> rewardCertificates = order.getRewardCertificates();
		if( rewardCertificates != null && !rewardCertificates.isEmpty()) {
			Iterator<RepositoryItem> i = rewardCertificates.iterator();
			Map<String, RepositoryItem> validCerts = new HashMap<>();
			List<String> emptyCerts = new ArrayList<>( rewardCertificates.size() );
			String amountUsedPName = OrderPropertyManager.REWARD_CERTIFICATE_AMT_USED.getValue();
			String certValuePName = OrderPropertyManager.REWARD_CERTIFICATE_AMT_AUTH.getValue();
			String validCertName = OrderRewardCertificateStatus.VALIDATED.getValue();
			while( i.hasNext() ) {
				RepositoryItem cert = i.next();
				if( validCertName.equals( cert.getPropertyValue( OrderPropertyManager.REWARD_CERTIFICATE_STATUS.getValue() ) ) ) {
					Double amountUsed = (Double)cert.getPropertyValue( amountUsedPName );
					Double certValue = (Double)cert.getPropertyValue( certValuePName );
					if((certValue > 0 && amountUsed > 0.0001 ) ||
							certValue == 0.0){
						validCerts.put( cert.getRepositoryId(), cert );
					} else {
						emptyCerts.add( cert.getRepositoryId() );
					}
				}
			}
			if( !emptyCerts.isEmpty()) {
				try {
					removeCertificatesFromOrder( order, emptyCerts.toArray( new String[emptyCerts.size()] ) );
				} catch( RepositoryException e ) {
					if( isLoggingError() ) {
						logError( "Attempted to remove un-used certificates but failed", e );
					}
				}
			}
			if( !validCerts.isEmpty()) {
				if( isLoggingDebug() ) {
					logDebug( "reserveRewardCertificates() - Valid Certs are present" ); 
				}

				try {
					List<String> validCertList = new ArrayList<>(validCerts.keySet());
					String[] validCertListArray = new String[validCertList.size()];
					validCertListArray = validCertList.toArray(validCertListArray);
					String profileId = order.getProfileId();
					String orderId = order.getId();
					String loyaltyNumber = (String)getProfile().getPropertyValue( getPropertyManager().getLoyaltyNumberPropertyName());

					Map<String, Map> validatedCertsOffersMap = this.validateCertsById( validCertListArray, profileId, loyaltyNumber );

					Map<Boolean, List<String>> validatedCertMap = validatedCertsOffersMap.get("REW");

					Map<Boolean, List<String>> validatedOfferMap = validatedCertsOffersMap.get("OFFER");

					RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();

					// reserve the list of valid certs
					if( !validatedCertMap.isEmpty() ) {
						rewardServiceRequest.setCertificates(validatedCertMap.get(Boolean.TRUE));
					}

					// reserve the list of valid offers
					if( !validatedOfferMap.isEmpty() ) {
						rewardServiceRequest.setOffers(validatedOfferMap.get(Boolean.TRUE));
					}

					if ((rewardServiceRequest.getCertificates() != null && !rewardServiceRequest
							.getCertificates().isEmpty())
							|| (rewardServiceRequest.getOffers() != null && !rewardServiceRequest.getOffers()
							.isEmpty())) {
						rewardServiceRequest.getPerson().setProfileID(profileId);
						rewardServiceRequest.setOrderNumber(orderId);
						rewardServiceRequest.getPerson().setMemberID(loyaltyNumber);
						rewardServiceRequest.setDeviceName(getDeviceName());

						rewardService.reserveLoyaltyCertificate(rewardServiceRequest);

						for (String vCertId : validCertList) {
							MutableRepositoryItem vCert = (MutableRepositoryItem) validCerts.get(vCertId);
							vCert.setPropertyValue(OrderPropertyManager.REWARD_CERTIFICATE_STATUS.getValue(),
									OrderRewardCertificateStatus.RESERVED.getValue());
							removeCertificateFromProfile(getProfile(), vCert.getRepositoryId());
						}
					}

				} catch(DigitalIntegrationInactiveException ex){
					if( isLoggingError() ) {
						logError( "reserveRewardCertificates service Inactive", ex);
					}
					throw ex;
				}catch( DigitalIntegrationException e ) {
					if( isLoggingError() ) {
						logError( "reserveRewardCertificates Integration exception", e);
					}
					retVal = Boolean.FALSE;
				}
			}
			getOrderManager().updateOrder( order );
		}
		return retVal;
	}

	/**
	 * @param order
	 */
	public void unReserveRewardCertificates(DigitalOrderImpl order) {
		if (order != null && order.getRewardCertificates() != null && !order.getRewardCertificates()
				.isEmpty()) {
			try {
				RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();
				rewardServiceRequest.getPerson().setProfileID(order.getProfileId());
				rewardServiceRequest.setOrderNumber(order.getId());
				rewardServiceRequest.setDeviceName(getDeviceName());

				Set<RepositoryItem> rewardCertificates = order.getRewardCertificates();
				if( rewardCertificates != null && !rewardCertificates.isEmpty()) {
					List<String> certificates = new ArrayList<>();
					List<String> offers = new ArrayList<>();
					for (RepositoryItem cert : rewardCertificates) {
						String certType = (String) cert.getPropertyValue("type");
						if ("CERT".equalsIgnoreCase(certType) || "REW".equalsIgnoreCase(certType)) {
							certificates.add(cert.getRepositoryId());
						} else {
							offers.add(cert.getRepositoryId());
						}
					}
					rewardServiceRequest.setCertificates(certificates);
					rewardServiceRequest.setOffers(offers);
				}

				rewardService.cancelLoyaltyCertificateReservation(rewardServiceRequest);
			} catch (DigitalIntegrationException e) {
				if (isLoggingError()) {
					logError("unReserveRewardCertificates", e);
				}
			}
		}
	}
	
	public double getQualifiedOrderAmount( Order order ) {

		double qualifiedAmount = 0.0;
		List<DigitalCommerceItem> cis = order.getCommerceItems();
		if( cis != null ) {
			for( DigitalCommerceItem ci : cis ) {
				RepositoryItem prod = (RepositoryItem)ci.getAuxiliaryData().getProductRef();
				String prodType = null;
				if( prod != null ) {
					prodType = (String)prod.getPropertyValue( ProducCatalogPropertyManager.PRODUCT_TYPE.getValue() );
				}
				if( isLoggingDebug() ) {
					logDebug( "product type: " + prodType );
				}
				if(prodType == null || getUndiscountableProductTypes() != null && getUndiscountableProductTypes().length > 0 && Arrays.binarySearch(getUndiscountableProductTypes(), prodType) < 0) {
					if( isLoggingDebug() ) {
						logDebug( "CI Price: " + ci.getPriceInfo().getAmount() );
					}
					qualifiedAmount += ( ci.getPriceInfo().getAmount() - ci.getPriceInfo().getOrderDiscountShare() );
				}
			}
			if( isLoggingDebug() ) {
				logDebug( "getQualifedOrderAmount:" + qualifiedAmount );
			}
			return getPricingTools().round( qualifiedAmount );
		} else {
			return 0.0;
		}
	}
	
	/** validate rewards certs by cert id, retrieve a list of certs for the given
	 * member, then test each cert id to determine if it is in the list passed
	 * in.
	 * 
	 * @param pCertId
	 *            string array of cert ids
	 * @param pProfileId
	 *            string value of ATG profile id
	 * @param pMemberId
	 *            string value of member id
	 * @return a map which contains array list of valid certs and invalid certs
	 */
	private Map<String, Map> validateCertsById( String[] pCertId, String pProfileId, String pMemberId ) {
		if( isLoggingDebug()) {
			logDebug( "Enter : validateCertsById(String[] " + Arrays.toString(pCertId) + " , String " + pProfileId + ", String " + pMemberId + ")" );
		}

		Map<String, Map> resultsMap = new HashMap<>();

		Map<Boolean, List<String>> certsResultsMap = new HashMap<>();
		Map<Boolean, List<String>> offersResultsMap = new HashMap<>();

		List<String> validCerts = new ArrayList<>();
		List<String> invalidCerts = new ArrayList<>();

		List<String> validOffers = new ArrayList<>();
		List<String> invalidOffers = new ArrayList<>();

		List<LoyaltyCertificate> certList = retrieveRewardsCertificates( pMemberId, pProfileId, null );
		List<String> certIdList = new ArrayList<>( Arrays.asList( pCertId ) );

		for (LoyaltyCertificate loyaltyCertVO : certList) {
			if (certIdList.contains(loyaltyCertVO.getId())) {
				if("REW".equalsIgnoreCase(loyaltyCertVO.getType())) {
					validCerts.add(loyaltyCertVO.getId());
				}else if("OFFER".equalsIgnoreCase(loyaltyCertVO.getType())) {
					validOffers.add(loyaltyCertVO.getId());
				}
			} else {
				if("REW".equalsIgnoreCase(loyaltyCertVO.getType())) {
					invalidCerts.add(loyaltyCertVO.getId());
				}else if("OFFER".equalsIgnoreCase(loyaltyCertVO.getType())) {
					invalidOffers.add(loyaltyCertVO.getId());
				}
			}
		}

		certsResultsMap.put(Boolean.TRUE, validCerts );
		certsResultsMap.put(Boolean.FALSE, invalidCerts );

		offersResultsMap.put(Boolean.TRUE, validOffers );
		offersResultsMap.put(Boolean.FALSE, invalidOffers );

		resultsMap.put("REW", certsResultsMap);
		resultsMap.put("OFFER", offersResultsMap);

		if( isLoggingDebug() ) {
			logDebug( "End : validateCertsById() : Valid certs size : " + certsResultsMap.size() );
		}
		return resultsMap;
	}
	
	public void removeCertificateFromProfile( Profile profile, String certificateId ) {
		if( profile != null && certificateId != null ) {
			List<RepositoryItem> certs = (List<RepositoryItem>)profile.getPropertyValue( getPropertyManager().getCertificatePropertyName() );
			for( int x = 0; x < certs.size(); x++ ) {
				RepositoryItem profileCert = certs.get( x );
				if( certificateId.equals( profileCert.getPropertyValue( getPropertyManager().getCertificateIdPropertyName() ) ) ) {
					certs.remove( x );
					return;
				}
			}
		}
	}

	public void removeCertificatesFromOrder( DigitalOrderImpl order, String[] certificateIds ) throws RepositoryException {
		Set<RepositoryItem> certs = order.getRewardCertificates();
		if( isLoggingDebug() ) {
			logDebug( "ORDER CURRENTLY HAS " + order.getRewardCertificates().size() + " REWARD CERTS" );
		}
		if( isLoggingDebug() ) {
			logDebug( "REMOVING " + certificateIds.length + " of them" );
		}
		if( certs != null && !certs.isEmpty() ) {
			for (String certificateId : certificateIds) {
				if (isLoggingDebug()) {
					logDebug("LOOKING FOR CERTIFICATE " + certificateId + " IN REPOSITORY");
				}
				RepositoryItem cert = getCertificate(certificateId);
				if (isLoggingDebug()) {
					logDebug("FOUND CERT...STATUS: " + cert.getPropertyValue(OrderPropertyManager.REWARD_CERTIFICATE_STATUS.getValue()));
				}
				if (!OrderRewardCertificateStatus.RESERVED.getValue().equals(cert.getPropertyValue(OrderPropertyManager.REWARD_CERTIFICATE_STATUS.getValue()))) {
					if (isLoggingDebug()) {
						logDebug("REMOVING CERT: " + cert.getRepositoryId());
					}
					certs.remove(cert);
				}

			}
		}
		order.setRewardCertificates( certs );
	}
	
	/** This method validate whether the given code appears to be for a reward certificate
	 * 
	 * @param code
	 * @return */
	public boolean isRewardCertificate( String code ) {
		code = DigitalStringUtil.trimToEmpty( code );
		boolean retVal = false;
		if( ( code.length() == this.getDswConstants().getRewardCertificateLength() || 
				code.length() == this.getDswConstants().getRewardCertificateMarkdownLength() ) 
				&& ( DigitalNumberUtil.isDigits( code ) ) ) retVal = true;
		return retVal;
	}
	
	/** This method finds certificated number based on markdown code
	 * 
	 * @param profile
	 * @param markdownCode
	 * @return */
	public String findCertificateNumberUsingMarkdownCode( Profile profile, String markdownCode ) {
		String retVal = null;
		String certficatePropertyName = this.getPropertyManager().getCertificatePropertyName();
		List<RepositoryItem> profileCertificateList = (List<RepositoryItem>)profile.getPropertyValue( certficatePropertyName );
		for (RepositoryItem rewardCertificate : profileCertificateList) {
			if (rewardCertificate != null) {
				String markdownCodePropertyName = this.getPropertyManager().getMarkdownCodePropertyName();
				if (markdownCode.equals(rewardCertificate.getPropertyValue(markdownCodePropertyName))) {
					String certificateNumberPropertyName = this.getPropertyManager().getCertificateNumberPropertyName();
					retVal = (String) rewardCertificate.getPropertyValue(certificateNumberPropertyName);
					break;
				}
			}
		}
		return retVal;
	}
	
	public boolean isMarkdownCodeAlreadyApplied( Profile profile, String markdownCode ) {
		boolean retVal = false;
		String certficatePropertyName = this.getPropertyManager().getCertificatePropertyName();
		List<RepositoryItem> profileCertificateList = (List<RepositoryItem>)profile.getPropertyValue( certficatePropertyName );
		for (RepositoryItem rewardCertificate : profileCertificateList) {
			if (rewardCertificate != null) {
				String markdownCodePropertyName = this.getPropertyManager().getMarkdownCodePropertyName();
				if (markdownCode.equals(rewardCertificate.getPropertyValue(markdownCodePropertyName))) {
					String certificateStatusPropertyName = this.getPropertyManager().getCertificateStatusPropertyName();
					retVal = ProfileRewardCertificateStatus.APPLIED.getValue().equalsIgnoreCase(
							(String) rewardCertificate.getPropertyValue(certificateStatusPropertyName));
					break;
				}
			}
		}
		return retVal;
	}
	
	public void addCertificateToOrder( Order order, LoyaltyCertificate certificate ) throws RepositoryException {
		DigitalOrderImpl dswOrder = (DigitalOrderImpl)order;
		if( isLoggingDebug() ) {
			logDebug( "Checking if " + certificate.getId() + " is in the order" );
		}
		if( !isCertificateAlreadyApplied( dswOrder, certificate.getId() ) ) {

			boolean isNew = false;
			Set certList = dswOrder.getRewardCertificates();
			MutableRepository rep = (MutableRepository)getOrderRepository();
			MutableRepositoryItem newCert;
			newCert = (MutableRepositoryItem)getCertificate( certificate.getId() );
			if( newCert == null ) {
				isNew = true;
				newCert = rep.createItem( certificate.getId(), OrderPropertyManager.REWARD_CERTIFICATE.getValue() );

			}
			newCert.setPropertyValue( OrderPropertyManager.REWARD_CERTIFICATE_NUMBER.getValue(), 
					certificate.getCertificateNumber() );
			newCert.setPropertyValue( OrderPropertyManager.REWARD_CERTIFICATE_AMT_AUTH.getValue(),
					certificate.getValue());
			newCert.setPropertyValue( OrderPropertyManager.REWARD_CERTIFICATE_STATUS.getValue(), 
					OrderRewardCertificateStatus.VALIDATED.getValue());
			newCert.setPropertyValue( OrderPropertyManager.REWARD_MARKDOWN_CODE.getValue(), 
					certificate.getMarkdownCode() );
			newCert.setPropertyValue( OrderPropertyManager.REWARD_TYPE.getValue(), 
					certificate.getType());
			if( certList == null ) {
				certList = new HashSet();
			}
			if( isNew ) {
				rep.addItem( newCert );
			} else {
				rep.updateItem( newCert );
			}
			certList.add( newCert );
			dswOrder.setRewardCertificates( certList );
		} else {
			updateRewardType(order, certificate.getId(), certificate.getType());
			if( isLoggingDebug() ) {
				logDebug( certificate.getId() + " Is already in the order" );
			}
		}
	}
	
	public void removeCertificateFromOrder( Order pOrder, String certificateId ) throws RepositoryException {
		DigitalOrderImpl dswOrder = (DigitalOrderImpl)pOrder;
		Set<RepositoryItem> certList = getOrderCertificates( pOrder );
		RepositoryItem cert = getCertificate( certificateId );
		if(cert != null && certList.contains(cert)){
			certList.remove( cert );
		}else{
			throw new DigitalRuntimeException(RewardsCertificateConstants.INVALID_CERTIFICATE);
		}
		dswOrder.setRewardCertificates( certList );
	}

	
	/** Returns true if the certificate is already applied
	 * 
	 * @param order
	 * @param certificateId
	 * @return */
	public boolean isCertificateAlreadyApplied( Order order, String certificateId ) {

		final Set<RepositoryItem> certificateSet = getOrderCertificates( order );
		return certificateSet != null && Iterables.filter( certificateSet, new FindRepositoryItemById( certificateId ) ).iterator().hasNext();

	}
	
	/** Returns true if the certificate is already applied
	 *
	 * @param order
	 * @param certificateId
	 * @param type
	 * @return */
	public boolean updateRewardType( Order order, String certificateId, String type ) {
		boolean ret = false;
		final Set<RepositoryItem> certificateSet = getOrderCertificates( order );
		if(certificateSet != null ){
			Iterator<RepositoryItem> iterator = Iterables.filter( certificateSet, new FindRepositoryItemById( certificateId ) ).iterator();
			if(iterator.hasNext()){
				MutableRepositoryItem certificateRepo = (MutableRepositoryItem)Iterables.filter( certificateSet, new FindRepositoryItemById( certificateId ) ).iterator().next();
				if(certificateRepo.getPropertyValue(OrderPropertyManager.REWARD_TYPE.getValue()) == null){
					certificateRepo.setPropertyValue( OrderPropertyManager.REWARD_TYPE.getValue(), type);
					ret = true;
				}
			}
		}

		return ret;
	}

	/**
	 *
	 * @param rewardsAddShopWithoutACardRequest
	 * @return
	 */
	public RewardsAddShopWithoutACardResponse addShopWithoutACardRequest(
			RewardsAddShopWithoutACardRequest rewardsAddShopWithoutACardRequest) {

		RewardsAddShopWithoutACardResponse response = new RewardsAddShopWithoutACardResponse();

		try {
			RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();

			if (DigitalStringUtil.isEmpty(rewardsAddShopWithoutACardRequest.getMemberId())) {
				response.getGenericExceptions().add(new ResponseError("invalidLoyaltyNumber",
						this.getMessageLocator().getMessageString("invalidLoyaltyNumber")));
			}

			if (DigitalStringUtil.isEmpty(rewardsAddShopWithoutACardRequest.getBarCode())) {
				response.getGenericExceptions().add(new ResponseError("emptyBarcodeNumber",
						this.getMessageLocator().getMessageString("emptyBarcodeNumber")));
			}

			if(!response.getGenericExceptions().isEmpty()){
				response.setRequestSuccess(false);
				return response;
			}

			rewardServiceRequest.getPerson().setMemberID(rewardsAddShopWithoutACardRequest.getMemberId());
			rewardServiceRequest.setBarcode(rewardsAddShopWithoutACardRequest.getBarCode());

			RewardServiceResponse rewardServiceResponse = rewardService
					.addShopWithoutACardRequest(rewardServiceRequest);

			if(rewardServiceResponse.isSuccess()) {
				response.setRewardServiceResponse(rewardServiceResponse);
				response.setRequestSuccess(true);
			}
			else{
				if(!rewardServiceResponse.getGenericExceptions().isEmpty()) {
					populateShopWithoutACardErrorResponse(
							rewardServiceResponse.getGenericExceptions().get(0).getErrorCode(),
							rewardServiceResponse.getGenericExceptions().get(0).getLocalizedMessage(), response);
				}
			}

		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving addShopWithoutACardRequest", e);
			String errorCode = "ServiceError";
			String msg = this.getMessageLocator().getMessageString(errorCode);
			response.getGenericExceptions().add(new ResponseError(errorCode, msg));
			response.setRequestSuccess(false);
		}

		return response;
	}

	/**
	 * @param causeMsg
	 * @param response
	 */
	private void populateShopWithoutACardErrorResponse(String errorCode, String causeMsg,
			RewardsAddShopWithoutACardResponse response) {
		if (DigitalStringUtil.isEmpty(errorCode)) {
			errorCode = "rewardsShopWithoutACardServiceError";
		}
		String msg = "";
		if (DigitalStringUtil.isNotBlank(causeMsg)) {
			if ("601".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer not found")) {
				errorCode = "invalidLoyaltyMemberAccount";
			} else if ("602".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer does not have a Loyalty account")) {
				errorCode = "invalidLoyaltyMemberAccount";
			} else if ("603".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Barcode is not the proper length")) {
				errorCode = "invalidBarcodeNumber";
			} else if ("604".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Demand transaction not found")) {
				errorCode = "transactionNotEligible";
			} else if ("605".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Demand transaction already processed for this Customer")) {
				errorCode = "duplicateBarcodeNumber";
			} else if ("606".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Demand transaction assigned to another Customer")) {
				errorCode = "transactionNotEligible";
			} else if ("607".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Demand transaction is older than the valid window")) {
				errorCode = "transactionNotEligible";
			} else if ("608".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer has already used SWAC twice today, try again tomorrow")) {
				errorCode = "pointAdjustmentLimit";
			} else if ("609".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer was not a Loyalty member when this Demand transaction occurred")) {
				errorCode = "transactionNotEligible";
			} else if ("610".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Alphanumeric characters present in Barcode")) {
				errorCode = "invalidBarcodeNumber";
			} else if ("611".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("is an invalid date")) {
				errorCode = "invalidBarcodeNumber";
			}
		}
		if (DigitalStringUtil.isBlank(msg)) {
			if(errorCode.contains("HTTP")) {
				errorCode = "rewardsShopWithoutACardServiceError";
			}
			msg = this.getMessageLocator().getMessageString(errorCode);
		}
		response.setFormError(true);
		response.setRequestSuccess(false);
		response.getGenericExceptions().add(new ResponseError(errorCode, msg));
	}

	/**
	 * @param rewardsUpdateCustomerRequest
	 * @param doSelect
	 * @return RewardsUpdateCustomerResponse
	 */
	public RewardsUpdateCustomerResponse updateCustomer(
			RewardsUpdateCustomerRequest rewardsUpdateCustomerRequest, boolean doSelect) {

		String METHOD_NAME = "UPDATE_CUSTOMER";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsUpdateCustomerResponse response = new RewardsUpdateCustomerResponse();

		try {
			RewardServiceRequest updateCustomerRequest = new RewardServiceRequest();

			if (doSelect) {
				RewardServiceRequest selectCustomerRequest = new RewardServiceRequest();
				selectCustomerRequest.getPerson().setProfileID(rewardsUpdateCustomerRequest.getProfileId());
				RewardServiceResponse selectCustomerResponse =
						rewardService.selectCustomerByProfileId(selectCustomerRequest);
				updateCustomerRequest = getCustomerBeanFromBtsRewardsServiceRequest(selectCustomerResponse);
			}
			updateRequestWithModifiedValues(updateCustomerRequest, rewardsUpdateCustomerRequest);
			RewardServiceResponse updateCustomerResponse = rewardService.updateCustomer(updateCustomerRequest);
			response.setRequestSuccess(updateCustomerResponse.isSuccess());
			if(!updateCustomerResponse.isSuccess()){
				response.setGenericExceptions(updateCustomerResponse.getGenericExceptions());
			}
		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving updateCustomer", e);

			response.getGenericExceptions().add(new ResponseError(e.getErrorCode(), e.getMessage()));
			response.setRequestSuccess(false);

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return response;
	}

	/**
	 * @param rewardsUpdateCustomerRequest
	 * @return
	 */
	public RewardsUpdateCustomerResponse updateCustomerByMemberId(
			RewardsUpdateCustomerRequest rewardsUpdateCustomerRequest, boolean doSelect) {

		String METHOD_NAME = "UPDATE_CUSTOMER_BY_MEMBER_ID";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsUpdateCustomerResponse response = new RewardsUpdateCustomerResponse();

		try {
			RewardServiceRequest updateCustomerRequest = new RewardServiceRequest();

			if (doSelect) {
				RewardServiceRequest selectCustomerRequest = new RewardServiceRequest();
				selectCustomerRequest.getPerson().setMemberID(rewardsUpdateCustomerRequest.getLoyaltyNumber());
				RewardServiceResponse selectCustomerResponse =
						rewardService.viewMemberData(selectCustomerRequest);
				updateCustomerRequest = getCustomerBeanFromBtsRewardsServiceRequest(selectCustomerResponse);
			}
			updateCustomerRequest = updateRequestWithModifiedValues(updateCustomerRequest,
					rewardsUpdateCustomerRequest);
			RewardServiceResponse updateCustomerResponse = rewardService
					.updateCustomerByMemberId(updateCustomerRequest);
			response.setRequestSuccess(updateCustomerResponse.isSuccess());
			if (!updateCustomerResponse.isSuccess()) {
				response.setGenericExceptions(updateCustomerResponse.getGenericExceptions());
			}
		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving updateCustomer", e);

			response.getGenericExceptions().add(new ResponseError(e.getErrorCode(), e.getMessage()));
			response.setRequestSuccess(false);

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return response;
	}

	/**
	 * @param rewardsAddCustomerRequest
	 * @return RewardsAddCustomerResponse
	 */
	public RewardsAddCustomerResponse addCustomer(
			RewardsAddCustomerRequest rewardsAddCustomerRequest) {

		String METHOD_NAME = "ADD_CUSTOMER";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsAddCustomerResponse response = new RewardsAddCustomerResponse();

		try {
			RewardServiceRequest addCustomerRequest = populateRewardServiceRequest(
					rewardsAddCustomerRequest);
			RewardServiceResponse addCustomerResponse = rewardService.addCustomer(addCustomerRequest);
			response.setRequestSuccess(addCustomerResponse.isSuccess());
			if (!addCustomerResponse.isSuccess()) {
				response.setGenericExceptions(addCustomerResponse.getGenericExceptions());
			}
		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving addCustomer", e);

			response.getGenericExceptions().add(new ResponseError(e.getErrorCode(), e.getMessage()));
			response.setRequestSuccess(false);

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return response;
	}



	/**
	 * @param rewardsAddCustomerRequest
	 * @return RewardsAddCustomerResponse
	 */
	public RewardsAddCustomerResponse retrieveMemberId(
			RewardsAddCustomerRequest rewardsAddCustomerRequest) {

		String METHOD_NAME = "RETRIEVE_MEMBERID";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsAddCustomerResponse response = new RewardsAddCustomerResponse();

		try {
			RewardServiceRequest addCustomerRequest = populateRewardServiceRequest(
					rewardsAddCustomerRequest);
			RewardServiceResponse addCustomerResponse = rewardService.addCustomer(addCustomerRequest);
			response.setRequestSuccess(addCustomerResponse.isSuccess());
			if (!addCustomerResponse.isSuccess()) {
				response.setGenericExceptions(addCustomerResponse.getGenericExceptions());
			}
		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving addCustomer", e);

			response.getGenericExceptions().add(new ResponseError(e.getErrorCode(), e.getMessage()));
			response.setRequestSuccess(false);

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return response;
	}

	/**
	 *
	 * @return
	 */
	private RewardServiceRequest populateRewardServiceRequest(RewardsAddCustomerRequest request) {

		RewardServiceRequest btsRewardServiceRequest = new RewardServiceRequest();
		btsRewardServiceRequest.getContact().setEmail(request.getEmail());
		btsRewardServiceRequest.getFlags().setEnrollAsMemberFlag("Y");
		btsRewardServiceRequest.getPerson().setFirstName(request.getFirstName());
		btsRewardServiceRequest.getPerson().setLastName(request.getLastName());

		String gender = request.getGender();
		if (DigitalStringUtil.isNotBlank(gender)) {
			gender = getGenderCode(gender.toUpperCase());
		}
		if (gender != null) {
			btsRewardServiceRequest.getPerson().setGender(Person.Gender.getGenderFromDescription(gender));
		}

		String mobilePhoneNumber = request.getMobilePhone();

		if (DigitalStringUtil.isNotBlank(mobilePhoneNumber)) {
			btsRewardServiceRequest.getContact().setMobilePhone(mobilePhoneNumber);
			btsRewardServiceRequest.getContact().setPhone1(mobilePhoneNumber);
		}

		String homePhoneNumber = request.getHomePhone();

		if (DigitalStringUtil.isNotBlank(homePhoneNumber)) {
			btsRewardServiceRequest.getContact().setPhone1(homePhoneNumber);
		}

		if (DigitalStringUtil.isNotBlank(request.getDayOfBirth())) {
			btsRewardServiceRequest.getPerson().setDayOfBirth(request.getDayOfBirth());
		}

		if (DigitalStringUtil.isNotBlank(request.getMonthOfBirth())) {
			btsRewardServiceRequest.getPerson().setMonthOfBirth(request.getMonthOfBirth());
		}

		String setNoEmail = "Y";
		String setNoPhone = "Y";
		String optOutMobile = "Y";
		if (DigitalStringUtil.isNotBlank(request.getOptOutMobile())
				&& "false".equalsIgnoreCase(request.getOptOutMobile())) {
			setNoEmail = "N";
		}


		if (DigitalStringUtil.isNotBlank(request.getOptOutEmail())
				&& "false".equalsIgnoreCase(request.getOptOutEmail())) {
			setNoPhone = "N";
			optOutMobile = "N";
		}

		btsRewardServiceRequest.getFlags().setNoEMail(setNoEmail);
		btsRewardServiceRequest.getFlags().setNoPhone(setNoPhone);
		btsRewardServiceRequest.getFlags().setOptOutMobile(optOutMobile);


		// Start code for setting the Address.
		String address1 = request.getAddress1();
		if (DigitalStringUtil.isNotBlank(address1) && DigitalStringUtil.isNotEmpty(address1)) {
			btsRewardServiceRequest.getAddress().setAddress1(address1);
		}
		String address2 = request.getAddress2();
		if (DigitalStringUtil.isNotBlank(address2) && DigitalStringUtil.isNotEmpty(address2)) {
			btsRewardServiceRequest.getAddress().setAddress2(address2);
		}
		String city = request.getCity();
		if (DigitalStringUtil.isNotBlank(city) && DigitalStringUtil.isNotEmpty(city)) {
			btsRewardServiceRequest.getAddress().setCity(city);
		}

		String state = request.getState();
		if (DigitalStringUtil.isNotBlank(state) && DigitalStringUtil.isNotEmpty(state)) {
			btsRewardServiceRequest.getAddress().setState(state);
		}
		btsRewardServiceRequest.getAddress().setCountry(request.getCountry());
		String postCode = request.getPostCode();
		if (!DigitalStringUtil.isEmpty(postCode) && postCode.contains("-")) {
			postCode = postCode.replace("-", "");
		}
		btsRewardServiceRequest.getAddress().setPostCode(postCode);

		btsRewardServiceRequest.getPerson().setProfileID(this.getProfile().getRepositoryId());

		return btsRewardServiceRequest;
	}


	/**
	 *
	 * @param gender
	 * @return String
	 */
	private String getGenderCode(String gender) {
		switch (gender) {
			case "MALE":
				return "M";
			case "FEMALE":
				return "F";
			case "UNKNOWN":
				return "U";
			default:
				return null;
		}
	}


	/**
	 *
	 * @param rewardsSelectCustomerRequest
	 * @return
	 */
	public RewardsSelectCustomerResponse findCustomer(RewardsSelectCustomerRequest rewardsSelectCustomerRequest) {

		String METHOD_NAME = "SELECT_CUSTOMER";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		RewardsSelectCustomerResponse response = new RewardsSelectCustomerResponse();

		try {

			RewardServiceRequest selectCustomerRequest = new RewardServiceRequest();
			selectCustomerRequest.getPerson().setProfileID(rewardsSelectCustomerRequest.getProfileId());
			RewardServiceResponse selectCustomerResponse = rewardService.selectCustomerByProfileId(selectCustomerRequest);
			response = populateSelectCustomerResponseValues(selectCustomerResponse);
			response.setRequestSuccess(selectCustomerResponse.isSuccess());
		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving updateCustomer", e);
			if(response.getGenericExceptions() != null){
				if(e.getCause() != null){
					response.getGenericExceptions().add(new ResponseError(e.getErrorCode() , e.getCause().getMessage()));
				}
				response.getGenericExceptions().add(new ResponseError(e.getErrorCode(), e.getMessage()));
			}
			response.setRequestSuccess(false);

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return response;
	}

	/**
	 * @param rewardsRetrievePointsOffersCertificatesRequest
	 * @return RewardsRetrievePointsOffersCertificatesResponse
	 */
	public RewardsRetrievePointsOffersCertificatesResponse retrieveCertificatePointsHistory(
			RewardsRetrievePointsOffersCertificatesRequest rewardsRetrievePointsOffersCertificatesRequest) {

		RewardsRetrievePointsOffersCertificatesResponse response = new RewardsRetrievePointsOffersCertificatesResponse();

		try {

			RewardServiceRequest request = new RewardServiceRequest();
			request.getPerson()
					.setProfileID(rewardsRetrievePointsOffersCertificatesRequest.getProfileId());
			request.setStartDate(rewardsRetrievePointsOffersCertificatesRequest.getStartDate());
			request.setEndDate(rewardsRetrievePointsOffersCertificatesRequest.getEndDate());

			RewardServiceResponse viewCertPointsResponse = rewardService.viewCertificateHistory(request);

			// Populate the mall plaza name in the points history
			if (null != viewCertPointsResponse && null != viewCertPointsResponse.getPoints()
					&& !viewCertPointsResponse.getPoints().isEmpty()) {
				for (Points point : viewCertPointsResponse.getPoints()) {
					if (!DigitalStringUtil.isBlank(point.getStoreID())) {
						DigitalStoreAddress storeAddress = addressUtil.fetchStoreAddress(point.getStoreID());
						point.setStoreMallPlazaName(storeAddress.getMallPlazaName());
					}
				}
			}

			response.setRewardsCertificatePointsHistory(viewCertPointsResponse);

		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving retrievePointsOffersCertificates", e);

			response.getGenericExceptions()
					.add(new ResponseError(e.getErrorCode(), e.getLocalizedMessage()));
			response.setRequestSuccess(false);

		}

		return response;

	}

	/**
	 *
	 * @param rewardsSubscribeEmailFooterRequest
	 * @return
	 */
	public RewardsSubscribeEmailFooterResponse anonymousSubscriptionFooter(RewardsSubscribeEmailFooterRequest rewardsSubscribeEmailFooterRequest) {
		boolean isValid = true;

		RewardsSubscribeEmailFooterResponse response = new RewardsSubscribeEmailFooterResponse();

		if (DigitalStringUtil.isBlank(rewardsSubscribeEmailFooterRequest.getEmail())) {
			response.getGenericExceptions().add(new ResponseError(ERR_INVALID_EMAIL, "Invalid email address"));
			isValid = false;
			response.setFormError(false);
			response.setRequestSuccess(false);
		}

		if (DigitalStringUtil.isBlank(rewardsSubscribeEmailFooterRequest.getEmailSource())) {
			response.getGenericExceptions().add(new ResponseError(ERR_INVALID_EMAILSOURCE, "Invalid email source"));
			isValid = false;
			response.setFormError(false);
			response.setRequestSuccess(false);
		}

		try {

			if (isValid) {
				RewardServiceRequest rewardServiceRequest = new RewardServiceRequest();

				Flags flags = new Flags();
				flags.setNoEMail("N");

				rewardServiceRequest.getContact().setEmail(rewardsSubscribeEmailFooterRequest.getEmail());
				rewardServiceRequest.setFlags(flags);
				rewardServiceRequest.setEmailSource(rewardsSubscribeEmailFooterRequest.getEmailSource());

				RewardServiceResponse anonymousSubscriptionResponse = rewardService.captureEmailFooter(rewardServiceRequest);

				response.setRewardServiceResponse(anonymousSubscriptionResponse);
				response.setRequestSuccess(anonymousSubscriptionResponse.isSuccess());
			} else {
				logWarning("anonymousSubscriptionFooter arguments failed data validation requirements");
				response.setRequestSuccess(false);
			}
		} catch (DigitalIntegrationException e) {
			logError("Error while retrieving anonymousSubscriptionFooter", e);

			response.getGenericExceptions().add(new ResponseError(e.getErrorCode(), e.getLocalizedMessage()));
		}

		return response;
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardsAddUpdateGiftResponse
	 */
	public RewardsAddUpdateShopforResponse createShopfor(RewardServiceRequest rewardServiceRequest) throws DigitalAppException {
		RewardsAddUpdateShopforResponse response = new RewardsAddUpdateShopforResponse();
		if (rewardServiceRequest != null) {
			try {
				RewardsShopforItem shopforItem = populateRewardsShopfor(rewardServiceRequest);

				RewardServiceResponse createShopforResponse = rewardService.addShopFor(rewardServiceRequest);

				if (createShopforResponse != null && createShopforResponse.isSuccess()) {
					response.setRequestSuccess(true);
					if (!createShopforResponse.getShopfors().isEmpty()) {
						shopforItem.setRewardsShopforID(createShopforResponse.getShopfors().get(0).getRewardsShopforID());
					}

					int shopForCount;
					if (isSyncShopForsPostAddFromRewards()) {
						shopForCount = mergeShopForsUsingLoyaltyServiceToProfile(getProfile());
					} else {
						shopforTools.addOrUpdateShopforForProfile(getProfile(), shopforItem);
						shopForCount = shopforTools.getCountofShopforItems(getProfile());
					}
					response.setCount(shopForCount);
					response.setItem(shopforItem);
				} else {
					populateShopForErrorResponse(
							createShopforResponse.getGenericExceptions().get(0).getErrorCode(),
							createShopforResponse.getGenericExceptions().get(0).getLocalizedMessage(),
							response);
				}

			} catch (DigitalIntegrationException e) {
				logError("Error while createShopfor in Rewards System", e);
				populateShopForErrorResponse(e.getErrorCode(), e.getMessage(), response);
				throw new DigitalAppException(e.getMessage());
			}
		} else {
			logWarning("createShopfor arguments failed data validation requirements");
			response.setFormError(true);
			response.setRequestSuccess(false);
		}

		return response;
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardsAddUpdateGiftResponse
	 */
	public RewardsAddUpdateShopforResponse updateShopfor(RewardServiceRequest rewardServiceRequest)
			throws DigitalAppException {
		RewardsAddUpdateShopforResponse response = new RewardsAddUpdateShopforResponse();
		if (rewardServiceRequest != null) {

			try {
				RewardsShopforItem shopforItem = populateRewardsShopfor(rewardServiceRequest);

				if (shopforTools.isShopforBelongsToProfileAndActive(getProfile(), shopforItem)) {

					RewardServiceResponse updateShopForResponse = rewardService
							.updateShopFor(rewardServiceRequest);

					if (updateShopForResponse != null && updateShopForResponse.isSuccess()) {
						response.setRequestSuccess(true);
						int shopForCount = 0;
						if (isSyncShopForsPostUpdateFromRewards()) {
							mergeShopForsUsingLoyaltyServiceToProfile(getProfile());
						} else {
							shopforTools.addOrUpdateShopforForProfile(getProfile(), shopforItem);
							shopForCount = shopforTools.getCountofShopforItems(getProfile());
						}
						response.setCount(shopForCount);
						response.setItem(shopforItem);
					} else {
						populateShopForErrorResponse(
								updateShopForResponse.getGenericExceptions().get(0).getErrorCode(),
								updateShopForResponse.getGenericExceptions().get(0).getLocalizedMessage(),
								response);
					}
				} else {
					logError("Error while updateShopfor as shopforID " + shopforItem.getShopforID()
							+ " doesn't belong to this customer " + shopforItem.getProfileID()
							+ " or already removed from the profile");
					response.setFormError(true);
					response.setRequestSuccess(false);
					String msg = this.getMessageLocator().getMessageString(ERR_SHOPFOR_NOT_FOUND);
					response.getGenericExceptions().add(new ResponseError(ERR_SHOPFOR_NOT_FOUND, msg));
				}

			} catch (DigitalIntegrationException e) {
				logError("Error while updateShopfor in Rewards System", e);
				populateShopForErrorResponse(e.getErrorCode(), e.getMessage(), response);
				throw new DigitalAppException(e.getMessage());
			}
		} else {
			logWarning("updateShopfor arguments failed data validation requirements");
			response.setFormError(true);
			response.setRequestSuccess(false);
		}

		return response;
	}


	/**
	 * @param rewardServiceRequest
	 * @return RewardsAddUpdateGiftResponse
	 */
	public RewardsAddUpdateShopforResponse deleteShopfor(RewardServiceRequest rewardServiceRequest)
			throws DigitalAppException {
		RewardsAddUpdateShopforResponse response = new RewardsAddUpdateShopforResponse();
		if (rewardServiceRequest != null) {

			try {
				RewardsShopforItem shopforItem = populateRewardsShopfor(rewardServiceRequest);

				if (shopforTools.isShopforBelongsToProfileAndActive(getProfile(), shopforItem)) {

					RewardServiceResponse deleteShopForResponse = rewardService
							.deleteShopFor(rewardServiceRequest);

					if (deleteShopForResponse.isSuccess()) {
						response.setRequestSuccess(true);
						int shopForCount = mergeShopForsUsingLoyaltyServiceToProfile(getProfile());
						response.setCount(shopForCount);
						response.setItem(shopforItem);
					} else {
						populateShopForErrorResponse(
								deleteShopForResponse.getGenericExceptions().get(0).getErrorCode(),
								deleteShopForResponse.getGenericExceptions().get(0).getLocalizedMessage(),
								response);
					}
				} else {
					logError("Error while deleteShopfor as shopforID " + shopforItem.getShopforID()
							+ " doesn't belong to this customer " + shopforItem.getProfileID()
							+ " or already removed from the profile");
					response.setFormError(true);
					response.setRequestSuccess(false);
					String msg = this.getMessageLocator().getMessageString(ERR_SHOPFOR_NOT_FOUND);
					response.getGenericExceptions().add(new ResponseError(ERR_SHOPFOR_NOT_FOUND, msg));
				}

			} catch (DigitalIntegrationException e) {
				logError("Error while deleteShopfor in Rewards System", e);
				populateShopForErrorResponse(e.getErrorCode(), e.getMessage(), response);
				throw new DigitalAppException(e.getMessage());
			}
		} else {
			logWarning("deleteShopfor arguments failed data validation requirements");
			response.setFormError(true);
			response.setRequestSuccess(false);
		}

		return response;
	}

  /**
   * @param rewardServiceRequest
   * @return RewardsAddUpdateGiftResponse
   */
  public RewardsAddUpdateGiftResponse createGift(RewardServiceRequest rewardServiceRequest) {
    RewardsAddUpdateGiftResponse response = new RewardsAddUpdateGiftResponse();
    if (rewardServiceRequest != null) {
      try {
        RewardsGiftItem giftItem = populateRewardsGiftItem(rewardServiceRequest);

        RewardServiceResponse createGiftResponse = rewardService
            .addBirthdayGift(rewardServiceRequest);

				if (createGiftResponse != null) {
					if (createGiftResponse.isSuccess()) {
						response.setRequestSuccess(true);
						giftItem.setStatus(DigitalBirthdayGiftTools.BIRTHDAY_GIFT_STATUS_NOT_SENT);
						if (!createGiftResponse.getBirthdayGifts().isEmpty()) {
							giftItem.setId(createGiftResponse.getBirthdayGifts().get(0).getBirthdayGiftId());
						}

						int bDayGiftsCount;
						if (isSyncBDayGiftsPostAddFromRewards()) {
							bDayGiftsCount = mergeBirthdayGiftsUsingLoyaltyServiceToProfile(getProfile());
						} else {
							birthdayGiftTools.addOrUpdateBDayGiftForProfile(getProfile(), giftItem);
							bDayGiftsCount = birthdayGiftTools.retrieveBirthDayGiftsFromProfile(getProfile())
									.size();
						}
						response.setCount(bDayGiftsCount);
						response.setItem(giftItem);
					} else {
						List<ResponseError> errors = createGiftResponse.getGenericExceptions();
						handleBDdayGiftErrors(response, createGiftResponse, errors);
					}
				}

      } catch (DigitalIntegrationException | RepositoryException e) {
        logError("Error while createGift", e);
        populateBDayGiftErrorResponse(null, e.getMessage(), response);
      }
    } else {
      logWarning("createGift arguments failed data validation requirements");
      response.setFormError(true);
      response.setRequestSuccess(false);
    }

    return response;
  }


	/**
	 * @param rewardServiceRequest
	 * @return RewardsAddUpdateGiftResponse
	 */
	public RewardsAddUpdateGiftResponse updateGift(RewardServiceRequest rewardServiceRequest) {
		RewardsAddUpdateGiftResponse response = new RewardsAddUpdateGiftResponse();
		if (rewardServiceRequest != null) {

			try {
				RewardsGiftItem giftItem = populateRewardsGiftItem(rewardServiceRequest);

				if (birthdayGiftTools.isBDayGiftBelongsToProfile(getProfile(), giftItem)) {

					RewardServiceResponse updateGiftResponse = rewardService
							.updateBirthdayGift(rewardServiceRequest);

					if (updateGiftResponse != null) {
						if (updateGiftResponse.isSuccess()) {
							response.setRequestSuccess(true);
							int bDayGiftsCount = 0;
							if (isSyncBDayGiftsPostUpdateFromRewards()) {
								mergeBirthdayGiftsUsingLoyaltyServiceToProfile(getProfile());
							} else {
								birthdayGiftTools.addOrUpdateBDayGiftForProfile(getProfile(), giftItem);
								bDayGiftsCount = birthdayGiftTools.retrieveBirthDayGiftsFromProfile(getProfile())
										.size();
							}
							response.setCount(bDayGiftsCount);
							response.setItem(giftItem);
						} else {
							List<ResponseError> errors = updateGiftResponse.getGenericExceptions();
							handleBDdayGiftErrors(response, updateGiftResponse, errors);
						}
					}
				} else {
					logError("Error while updateGift as giftId " + giftItem.getId()
							+ " doesn't belong to this customer " + giftItem.getProfileId());
					response.setFormError(true);
					response.setRequestSuccess(false);
					String msg = this.getMessageLocator().getMessageString(ERR_GENERIC_BIRTHDAY_GIFT);
					response.getGenericExceptions().add(new ResponseError(ERR_GENERIC_BIRTHDAY_GIFT, msg));
				}

			} catch (DigitalIntegrationException | RepositoryException e) {
				logError("Error while updateGift ", e);
				populateBDayGiftErrorResponse(null, e.getMessage(), response);
			}
		} else {
			logWarning("createGift arguments failed data validation requirements");
			response.setFormError(true);
			response.setRequestSuccess(false);
		}

		return response;
	}

	/**
	 * @param response
	 * @param updateGiftResponse
	 * @param errors
	 */
	private void handleBDdayGiftErrors(RewardsAddUpdateGiftResponse response,
			RewardServiceResponse updateGiftResponse, List<ResponseError> errors) {
		if (errors.isEmpty()) {
			response.setRequestSuccess(false);
			if (updateGiftResponse != null) {
				response.setGenericExceptions(updateGiftResponse.getGenericExceptions());
			}
		} else {
			populateBDayGiftErrorResponse(errors.get(0).getErrorCode(),
					errors.get(0).getLocalizedMessage(), response);
		}
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardsShopforItem
	 * Map Shopfor Domain from Integration project to Services project
	 */
	private RewardsShopforItem populateRewardsShopfor(RewardServiceRequest rewardServiceRequest) {

		RewardsShopforItem shopforItem = new RewardsShopforItem();

		if (rewardServiceRequest.getShopfor() != null) {
			Shopfor shopForDomain = rewardServiceRequest.getShopfor(); // This Domain is from Integration

			if (DigitalStringUtil.isNotBlank(shopForDomain.getShopforID())) {
				shopforItem.setShopforID(shopForDomain.getShopforID());
			}
			if (DigitalStringUtil.isNotBlank(shopForDomain.getProfileId())) {
				shopforItem.setProfileID(shopForDomain.getProfileId());
			}
			shopforItem.setActive(shopForDomain.isActive());
			if (DigitalStringUtil.isNotBlank(shopForDomain.getRewardsShopforID())) {
				shopforItem.setRewardsShopforID(shopForDomain.getRewardsShopforID());
			}
			if (DigitalStringUtil.isNotBlank(shopForDomain.getShopforName())) {
				shopforItem.setShopforName(shopForDomain.getShopforName());
			}
			if (DigitalStringUtil.isNotBlank(shopForDomain.getRelationship())) {
				shopforItem.setRelationship(shopForDomain.getRelationship());
			}
			if (DigitalStringUtil.isNotBlank(shopForDomain.getBirthDay())) {
				shopforItem.setBirthDay(shopForDomain.getBirthDay());
			}
			if (DigitalStringUtil.isNotBlank(shopForDomain.getBirthMonth())) {
				shopforItem.setBirthMonth(shopForDomain.getBirthMonth());
			}
			if (DigitalStringUtil.isNotBlank(shopForDomain.getWebType())) {
				shopforItem.setWebType(shopForDomain.getWebType());
			}
			if (DigitalStringUtil.isNotBlank(shopForDomain.getGender())) {
				shopforItem.setGender(shopForDomain.getGender());
			}
			if (DigitalStringUtil.isNotBlank(shopForDomain.getSizeGroup())) {
				shopforItem.setSizeGroup(shopForDomain.getSizeGroup());
			}
			if (shopForDomain.getSizes() != null) {
				shopforItem.setSizes(shopForDomain.getSizes().toArray(new String[0]));
			}
			if (shopForDomain.getWidths() != null) {
				shopforItem.setWidths(shopForDomain.getWidths().toArray(new String[0]));
			}
			if (shopForDomain.getDateCreated() != null) {
				shopforItem.setDateCreated(shopForDomain.getDateCreated());
			}
		}

		return shopforItem;
	}

	/**
	 * @param rewardServiceRequest
	 * @return RewardsGiftItem
	 */
	private RewardsGiftItem populateRewardsGiftItem(RewardServiceRequest rewardServiceRequest) {

		RewardsGiftItem giftItem = new RewardsGiftItem();

		if (rewardServiceRequest.getBirthdayGift() != null) {
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getBirthdayGift().getBirthdayGiftId())) {
				giftItem.setId(rewardServiceRequest.getBirthdayGift().getBirthdayGiftId());
			}

			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getBirthdayGift().getEmail())) {
				giftItem.setEmail(rewardServiceRequest.getBirthdayGift().getEmail());
			}
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getBirthdayGift().getFriendName())) {
				giftItem.setFirstName(rewardServiceRequest.getBirthdayGift().getFriendName());
			}
			if (DigitalStringUtil.isNotBlank(rewardServiceRequest.getBirthdayGift().getMessage())) {
				giftItem.setGiftMessage(rewardServiceRequest.getBirthdayGift().getMessage());
			}

			String dayOfBirth = rewardServiceRequest.getBirthdayGift().getDayOfBirth();
			String monthOfBirth = rewardServiceRequest.getBirthdayGift().getMonthOfBirth();

			if (DigitalStringUtil.isNotBlank(dayOfBirth) && DigitalStringUtil.isNotBlank(monthOfBirth)) {
				// derive the date considering blackout days & leap year
				Calendar derivedSendCalendarDate = deriveBirthdayGiftSendDate(dayOfBirth, monthOfBirth);
				giftItem.setGiftSendDate(derivedSendCalendarDate.getTime());
				giftItem.setBirthDay(Integer.toString(derivedSendCalendarDate.get(Calendar.DATE)));
				giftItem.setBirthMonth(Integer.toString(derivedSendCalendarDate.get(Calendar.MONTH) + 1));
				rewardServiceRequest.getBirthdayGift()
						.setDayOfBirth(Integer.toString(derivedSendCalendarDate.get(Calendar.DATE)));
				rewardServiceRequest.getBirthdayGift()
						.setMonthOfBirth(Integer.toString(derivedSendCalendarDate.get(Calendar.MONTH)));
				String deliveryDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
						.format(derivedSendCalendarDate.getTime());
				rewardServiceRequest.getBirthdayGift().setDeliveryDate(deliveryDate);
			}

			giftItem.setProfileId(rewardServiceRequest.getPerson().getProfileID());
		}

		return giftItem;
	}

	/**
	 * @param dayOfBirth
	 * @param monthOfBirth e.g. 01 - Jan, 02 - Feb ...... 12 - Dec
	 * @return Calendar
	 */
	private Calendar deriveBirthdayGiftSendDate(String dayOfBirth, String monthOfBirth) {
		Calendar baseSendCalendarDate = Calendar.getInstance();
		if (isLoggingDebug()) {
			logDebug("Base Gift Send date is " + baseSendCalendarDate.getTime());
		}

		int birthDayGiftBlackOutDays = (null == getDswConstants()) ? 0 :
				getDswConstants().getBirthDayGiftBlackOutDays();

		if (birthDayGiftBlackOutDays > 0) {
			baseSendCalendarDate.add(Calendar.DATE, birthDayGiftBlackOutDays);
			if (isLoggingDebug()) {
				logDebug("Updated Base Gift Send date is " + baseSendCalendarDate.getTime() +
						" after adding birthDayGiftBlackOutDays.");
			}
		}

		// the value used to set the <code>MONTH</code> calendar field in the calendar.
		// Month value is 0-based. e.g., 0 for January.
		int zeroBasedCalendarMonth = Integer.parseInt(monthOfBirth) - 1;

		Calendar requestedSendCalendarDate = Calendar.getInstance();
		requestedSendCalendarDate.set(requestedSendCalendarDate.get(Calendar.YEAR),
				zeroBasedCalendarMonth,
				Integer.parseInt(dayOfBirth), 0, 0);
		if (isLoggingDebug()) {
			logDebug("Gift Send date based on request is " + requestedSendCalendarDate.getTime());
		}

		Calendar derivedSendCalendarDate = Calendar.getInstance();

		// Request date is before today so increment year by 1
		if (requestedSendCalendarDate.getTime().compareTo(baseSendCalendarDate.getTime()) < 0) {
			int numberOfYearsToAdd = 1;
			derivedSendCalendarDate.add(Calendar.YEAR, numberOfYearsToAdd);
			if (isLoggingDebug()) {
				logDebug("Updated derived Gift Send date is " + derivedSendCalendarDate.getTime() +
						" after year increment as requested date was less than today.");
			}
		} else {
			if (isLoggingDebug()) {
				logDebug("Requested Gift Send date was greater than today so use the year as is.");
			}
		}

		// If derived month is Feb & day is > 28
		if (zeroBasedCalendarMonth == Calendar.FEBRUARY && Integer.parseInt(dayOfBirth) > 28) {

			if (!new GregorianCalendar().isLeapYear(derivedSendCalendarDate.get(Calendar.YEAR))) {
				// set the day to 28 as this is not a leap year
				derivedSendCalendarDate.set(derivedSendCalendarDate.get(Calendar.YEAR),
						zeroBasedCalendarMonth, 28, 0, 0);
				if (isLoggingDebug()) {
					logDebug(
							"Updated derived Gift Send date is " + derivedSendCalendarDate.getTime()
									+ " since it is not a leap year.");
				}
			}
			// if it is a leap year & day > 29
			else if (Integer.parseInt(dayOfBirth) > 29) {
				// set the day to 29 as this is a leap year
				derivedSendCalendarDate.set(derivedSendCalendarDate.get(Calendar.YEAR),
						zeroBasedCalendarMonth, 29, 0, 0);
				if (isLoggingDebug()) {
					logDebug(
							"Updated derived Gift Send date is " + derivedSendCalendarDate.getTime()
									+ " since it is a leap year max allowed day is 29.");
				}
			}
			// if it is a leap year & day < 29
			else {
				derivedSendCalendarDate.set(derivedSendCalendarDate.get(Calendar.YEAR),
						zeroBasedCalendarMonth,
						Integer.parseInt(dayOfBirth), 0, 0);
				if (isLoggingDebug()) {
					logDebug(
							"Updated derived Gift Send date is " + derivedSendCalendarDate.getTime()
									+ "since it is a leap year honor the requested day.");
				}
			}
		} else {
			derivedSendCalendarDate.set(derivedSendCalendarDate.get(Calendar.YEAR),
					zeroBasedCalendarMonth,
					Integer.parseInt(dayOfBirth), 0, 0);
		}

		if (isLoggingDebug()) {
			logDebug("Final derived Gift Send date is " + derivedSendCalendarDate.getTime());
		}

		return derivedSendCalendarDate;
	}

	/**
	 * @param errorCode
	 * @param causeMsg
	 * @param response
	 */
	private void populateShopForErrorResponse(String errorCode, String causeMsg,
			RewardsAddUpdateShopforResponse response) throws DigitalAppException {
		if (DigitalStringUtil.isEmpty(errorCode)) {
			errorCode = ERR_GENERIC_SHOPFOR;
		}
		String msg = "";
		if (DigitalStringUtil.isNotBlank(causeMsg)) {
			if ("600".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer not found")) {
				errorCode = ERR_INVALID_MEMBER;
			}
			else if ("601".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Shopfor not found")) {
				errorCode = ERR_SHOPFOR_NOT_FOUND;
				// Sync the Shop fors from rewards system
				int count = mergeShopForsUsingLoyaltyServiceToProfile(getProfile());
				Object[] params  = { count };
				msg = this.getMessageLocator().getMessageString(errorCode, params);
			}
		}
		if (DigitalStringUtil.isBlank(msg)) {
			if(errorCode.contains("HTTP")) {
				errorCode = ERR_GENERIC_SHOPFOR;
			}
			msg = this.getMessageLocator().getMessageString(errorCode);
		}
		response.setFormError(true);
		response.setRequestSuccess(false);
		response.getGenericExceptions().add(new ResponseError(errorCode, msg));
	}

	/**
	 * @param causeMsg
	 * @param response
	 */
	private void populateBDayGiftErrorResponse(String errorCode, String causeMsg,
			RewardsAddUpdateGiftResponse response) {
		if (DigitalStringUtil.isEmpty(errorCode)) {
			errorCode = ERR_GENERIC_BIRTHDAY_GIFT;
		}
		String msg = "";
		if (DigitalStringUtil.isNotBlank(causeMsg)) {
			if ("601".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Cannot edit a gift so near its processing date")) {
				errorCode = ERR_LOCKED_BIRTHDAY_GIFT;
			} else if ("602".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer doesn't have an ATG Account")) {
				errorCode = ERR_INVALID_MEMBER;
			} else if ("603".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer will be club on request date")) {
				errorCode = ERR_TIER_NOT_ELIGIBLE_BIRTHDAY_GIFT;
			} else if ("604".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("belongs to the customer requesting the birthday gift")) {
				errorCode = ERR_CUST_EMAIL_SAME_AS_BIRTHDAY_GIFT_EMAIL;
			} else if ("605".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("birthday gifts for that year")) {
				errorCode = ERR_MAX_BDAY_GIFTS_LIMIT;
				// Sync the BDay Gifts from rewards system
				int count = mergeBirthdayGiftsUsingLoyaltyServiceToProfile(getProfile());
				Object[] params = {count};
				msg = this.getMessageLocator().getMessageString(errorCode, params);
			} else if ("606".equalsIgnoreCase(errorCode) ||
					causeMsg.contains("Customer has already scheduled a birthday gift")) {
				errorCode = ERR_DUPLICATE_BIRTHDAY_GIFT;
			} else if (causeMsg.contains("Email is not valid")) {
				errorCode = ERR_INVALID_EMAIL;
			} else if (causeMsg.contains("Friend Name is required")) {
				errorCode = ERR_INVALID_FIRSTNAME;
			} else if (causeMsg.contains("Birthday is not valid")) {
				errorCode = ER_INVALID_BIRTH_DAY_MONTH;
			}
		}
		if (DigitalStringUtil.isBlank(msg)) {
			if(errorCode.contains("HTTP")) {
				errorCode = ERR_GENERIC_BIRTHDAY_GIFT;
			}
			msg = this.getMessageLocator().getMessageString(errorCode);
		}
		response.setFormError(true);
		response.setRequestSuccess(false);
		response.getGenericExceptions().add(new ResponseError(errorCode, msg));
	}

	/**
	 * @return true or false
	 */
	public boolean isMaxBDayGiftsLimitReached() {
		boolean maxBDayGiftsLimitReached = false;

		long maxBDayGiftsAllowed = getMaxBDayGiftsLimit();

		List<RewardsGiftItem> rewardsGiftItems = birthdayGiftTools
				.retrieveBirthDayGiftsFromProfile(getProfile());

		if (rewardsGiftItems.size() == maxBDayGiftsAllowed) {
			maxBDayGiftsLimitReached = true;
		}

		return maxBDayGiftsLimitReached;
	}

	/**
	 * @return true or false
	 */
	public boolean isMaxShopforLimitReached() throws DigitalAppException {
		boolean maxShopforLimitReached = false;

		long maxShopforAllowed = getMaxShopforLimit();

		Integer shopforSize = shopforTools.getCountofShopforItems(getProfile());

		if(shopforSize == maxShopforAllowed){
			maxShopforLimitReached = true;
		}

		return maxShopforLimitReached;
	}

	/**
	 * @param shopForRequest
	 * @return true or false
	 * @throws DigitalAppException
	 */
	public boolean isDimensionSupported(RewardServiceRequest shopForRequest) throws DigitalAppException {
		boolean dimSupportedFlag = true;

		Map<String, Object> shopForDimensions = shopforTools.getFiltersforShopfor();

		Shopfor shopfor = shopForRequest.getShopfor();

		String gender = shopfor.getGender();

		List<String> inputSizes = shopfor.getSizes();

		List<String> inputWidths = shopfor.getWidths();

		Map<String, Object> availableGenderSizes = (Map<String, Object>) shopForDimensions
				.get("genderSizes");

		if (availableGenderSizes.containsKey(gender)) {
			List<String> availableSizes = (List<String>) availableGenderSizes.get(gender);
			if (!availableSizes.containsAll(inputSizes)) {
				dimSupportedFlag = false;
			}
		} else {
			dimSupportedFlag = false;
		}

		List<String> availableWidths = (List<String>) shopForDimensions.get("widths");

		if (inputWidths != null && !availableWidths.containsAll(inputWidths)) {
			dimSupportedFlag = false;
		}

		return dimSupportedFlag;
	}

	/**
	 *
	 * @param selectCustomerResponse
	 * @return RewardsSelectCustomerResponse
	 */
	private RewardsSelectCustomerResponse populateSelectCustomerResponseValues(
			RewardServiceResponse selectCustomerResponse) {
		RewardsSelectCustomerResponse response = new RewardsSelectCustomerResponse();

		if (selectCustomerResponse.getPerson() != null) {
			response.setFirstName(selectCustomerResponse.getPerson().getFirstName());
			response.setLastName(selectCustomerResponse.getPerson().getLastName());
			if (selectCustomerResponse.getPerson().getGender() != null) {
				response.setGender(selectCustomerResponse.getPerson().getGender().getDescription());
			}
			response.setDayOfBirth(selectCustomerResponse.getPerson().getDayOfBirth());
			response.setMonthOfBirth(selectCustomerResponse.getPerson().getMonthOfBirth());
			response.setLoyaltyNumber(selectCustomerResponse.getPerson().getMemberID());
		}
		response.setFrequencyFashionEmail(selectCustomerResponse.getFrequencyFashionEmail());

		if (selectCustomerResponse.getContact() != null) {
			response.setEmail(selectCustomerResponse.getContact().getEmail());
			response.setMobilePhone(selectCustomerResponse.getContact().getMobilePhone());
			response.setHomePhone(selectCustomerResponse.getContact().getPhone1());
		}

		if (selectCustomerResponse.getFlags() != null) {
			response.setOptoutFashionEmail(selectCustomerResponse.getFlags().getOptOutFashionEmail());
			response.setOptoutTextToPhone(selectCustomerResponse.getFlags().getOptOutRewardsText());
			response.setNoEMail(selectCustomerResponse.getFlags().getNoEMail());
			response.setNoPhone(selectCustomerResponse.getFlags().getNoPhone());
			response.setOptOutMobile(selectCustomerResponse.getFlags().getOptOutMobile());
			response.setBadAddress(selectCustomerResponse.getFlags().getBadAddress());
			response.setBadEmail(selectCustomerResponse.getFlags().getBadEmail());
			response.setBadPhone(selectCustomerResponse.getFlags().getBadPhone());
		}

		if (selectCustomerResponse.getAddress() != null) {
			response.setAddress1(selectCustomerResponse.getAddress().getAddress1());
			response.setAddress2(selectCustomerResponse.getAddress().getAddress2());
			response.setCity(selectCustomerResponse.getAddress().getCity());
			response.setState(selectCustomerResponse.getAddress().getState());
			response.setPostCode(selectCustomerResponse.getAddress().getPostCode());
			response.setCountry(selectCustomerResponse.getAddress().getCountry());
		}
		response.setPreferredStore(selectCustomerResponse.getPreferredStore());
		response.setDoBankPoints(selectCustomerResponse.getPointsBanking());

		response.setCertDenomination(selectCustomerResponse.getCertDenomination());
		response.setPaperlessCert(selectCustomerResponse.getIsPaperlessCert());
		return response;
	}

	/**
	 *
	 * @param selectCustomerResponse
	 * @return RewardServiceRequest
	 */
	private RewardServiceRequest getCustomerBeanFromBtsRewardsServiceRequest(RewardServiceResponse selectCustomerResponse) {

		RewardServiceRequest updateCustomerRequest = new RewardServiceRequest();
		if (selectCustomerResponse.getAddress() != null) {
			updateCustomerRequest.setAddress(selectCustomerResponse.getAddress());
		}

		if (selectCustomerResponse.getPerson() != null) {
			updateCustomerRequest.setPerson(selectCustomerResponse.getPerson());
		}

		if (selectCustomerResponse.getContact() != null) {
			updateCustomerRequest.setContact(selectCustomerResponse.getContact());
		}

		if (selectCustomerResponse.getFlags() != null) {
			updateCustomerRequest.setFlags(selectCustomerResponse.getFlags());
		}

		updateCustomerRequest.setCertDenomination(selectCustomerResponse.getCertDenomination());
		updateCustomerRequest.setIsPaperlessCert(selectCustomerResponse.getIsPaperlessCert());

		updateCustomerRequest.setFraudCode(selectCustomerResponse.getFraudCode());
		updateCustomerRequest.setFraudCodeGroup(selectCustomerResponse.getFraudCodeGroup());
		updateCustomerRequest.setFrequencyFashionEmail(selectCustomerResponse.getFrequencyFashionEmail());
		updateCustomerRequest.setPreferredStore(selectCustomerResponse.getPreferredStore());
		updateCustomerRequest.setPointsBanking(selectCustomerResponse.getPointsBanking());

		return updateCustomerRequest;
	}

	/**
	 *
	 * @param updateCustomerRequest
	 * @param rewardsUpdateCustomerRequest
	 * @return
	 */
	private RewardServiceRequest updateRequestWithModifiedValues(RewardServiceRequest updateCustomerRequest,
																 RewardsUpdateCustomerRequest rewardsUpdateCustomerRequest) {

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getEmail())){
			updateCustomerRequest.getContact().setEmail(rewardsUpdateCustomerRequest.getEmail());
		}
		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getProfileId())) {
			updateCustomerRequest.getPerson().setProfileID(rewardsUpdateCustomerRequest.getProfileId());
		}
		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getFirstName())){
			updateCustomerRequest.getPerson().setFirstName(rewardsUpdateCustomerRequest.getFirstName());
		}
		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getLoyaltyNumber())) {
			updateCustomerRequest.getPerson().setMemberID(rewardsUpdateCustomerRequest.getLoyaltyNumber());
		}
		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getLastName())){
			updateCustomerRequest.getPerson().setLastName(rewardsUpdateCustomerRequest.getLastName());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getGender())){
			updateCustomerRequest.getPerson().setGender(Person.Gender.getGenderFromDescription(rewardsUpdateCustomerRequest.getGender()));
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getDayOfBirth())){
			updateCustomerRequest.getPerson().setDayOfBirth(rewardsUpdateCustomerRequest.getDayOfBirth());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getMonthOfBirth())){
			updateCustomerRequest.getPerson().setMonthOfBirth(rewardsUpdateCustomerRequest.getMonthOfBirth());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getFrequencyFashionEmail())){
			updateCustomerRequest.setFrequencyFashionEmail(rewardsUpdateCustomerRequest.getFrequencyFashionEmail());
		}

		if(null != rewardsUpdateCustomerRequest.getMobilePhone()){
			updateCustomerRequest.getContact().setMobilePhone(rewardsUpdateCustomerRequest.getMobilePhone());
		}

		if(null != rewardsUpdateCustomerRequest.getHomePhone()){
			updateCustomerRequest.getContact().setPhone1(rewardsUpdateCustomerRequest.getHomePhone());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getOptoutFashionEmail())){
			updateCustomerRequest.getFlags().setOptOutFashionEmail(rewardsUpdateCustomerRequest.getOptoutFashionEmail());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getOptoutTextToPhone())){
			updateCustomerRequest.getFlags().setOptOutRewardsText(rewardsUpdateCustomerRequest.getOptoutTextToPhone());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getNoEMail())){
			updateCustomerRequest.getFlags().setNoEMail(rewardsUpdateCustomerRequest.getNoEMail());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getNoPhone())){
			updateCustomerRequest.getFlags().setNoPhone(rewardsUpdateCustomerRequest.getNoPhone());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getOptOutMobile())){
			updateCustomerRequest.getFlags().setOptOutMobile(rewardsUpdateCustomerRequest.getOptOutMobile());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getAddress1())){
			updateCustomerRequest.getAddress().setAddress1(rewardsUpdateCustomerRequest.getAddress1());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getAddress2())){
			updateCustomerRequest.getAddress().setAddress2(rewardsUpdateCustomerRequest.getAddress2());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getCity())){
			updateCustomerRequest.getAddress().setCity(rewardsUpdateCustomerRequest.getCity());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getState())){
			updateCustomerRequest.getAddress().setState(rewardsUpdateCustomerRequest.getState());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getPostCode())){
			updateCustomerRequest.getAddress().setPostCode(rewardsUpdateCustomerRequest.getPostCode());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getCountry())){
			updateCustomerRequest.getAddress().setCountry(rewardsUpdateCustomerRequest.getCountry());
		}

		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getPreferredStore())){
			updateCustomerRequest.setPreferredStore(rewardsUpdateCustomerRequest.getPreferredStore());
		}
		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getBadAddress())) {
			updateCustomerRequest.getFlags().setBadAddress(rewardsUpdateCustomerRequest.getBadAddress());
		}
		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getBadEmail())) {
			updateCustomerRequest.getFlags().setBadEmail(rewardsUpdateCustomerRequest.getBadEmail());
		}
		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getBadPhone())) {
			updateCustomerRequest.getFlags().setBadPhone(rewardsUpdateCustomerRequest.getBadPhone());
		}
		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getDoBankPoints())) {
			updateCustomerRequest.setPointsBanking(rewardsUpdateCustomerRequest.getDoBankPoints());
		}
		
		if(DigitalStringUtil.isNotBlank(rewardsUpdateCustomerRequest.getCertDenomination())) {
			updateCustomerRequest.setCertDenomination(rewardsUpdateCustomerRequest.getCertDenomination());
		}

		String paperlessCertOptIn = rewardsUpdateCustomerRequest.getPaperlessCertOptIn();
    if(DigitalStringUtil.isNotBlank(paperlessCertOptIn)) {
      updateCustomerRequest
          .setIsPaperlessCert("Y".equalsIgnoreCase(paperlessCertOptIn));
    }

		return updateCustomerRequest;
	}

	private  class ToValidCertificateAdapter implements Function<Certificate, LoyaltyCertificate> {
		@Override
		public LoyaltyCertificate apply( Certificate from ) {
			final LoyaltyCertificate retVal = new LoyaltyCertificate();
			retVal.setId( from.getCertNbr() );
			retVal.setCertificateNumber( from.getDiscountCode() );
			retVal.setValue( ( DigitalStringUtil.isNotBlank( from.getCertValue() ) ) ? Double.valueOf(from.getCertValue()) : 0.0 );
			retVal.setMarkdownCode( from.getMarkdownCode() );
			retVal.setStatus(from.getCertStatus());
			retVal.setType( from.getCertType());
			try {
				retVal.setExpirationDate( DigitalStringUtil.isNotBlank( from.getExpDate() ) ? DigitalDateUtil.parseDate( from.getExpDate(), new String[] { vipRewardsDateFormat } ) : null );
				retVal.setIssueDate( DigitalStringUtil.isNotBlank( from.getIssueDate() ) ? DigitalDateUtil.parseDate( from.getIssueDate(), new String[] { vipRewardsDateFormat } ) : null );
			} catch( ParseException e ) {
				logWarning( String.format( "Unable to parse expiration date (%s) on certificate (%s)", from.getExpDate(), from.getCertNbr() ), e );
			}
			retVal.setValid( true );
			retVal.setOfferDisplayName(from.getOfferDisplayName());
			retVal.setOfferDetailsPageName(from.getOfferDetailsPageName());
			return retVal;
		}
	}


	private  class ToProfileCertificateAdapter implements Function<RepositoryItem, LoyaltyCertificate> {
		@Override
		public LoyaltyCertificate apply( RepositoryItem from ) {
			Date expDate;
			final LoyaltyCertificate retVal = new LoyaltyCertificate();
			retVal.setId( (String)from.getPropertyValue( getPropertyManager().getCertificateIdPropertyName()));
			retVal.setCertificateNumber( (String)from.getPropertyValue( getPropertyManager().getCertificateNumberPropertyName()));
			Double value = (Double)from.getPropertyValue( getPropertyManager().getCertificateValuePropertyName());
			retVal.setValue( value );
			retVal.setMarkdownCode( (String)from.getPropertyValue( getPropertyManager().getMarkdownCodePropertyName()));
			expDate = (Date)from.getPropertyValue( getPropertyManager().getExpirationDatePropertyName());
			retVal.setExpirationDate(expDate );
			retVal.setValid( true );
			retVal.setType((String)from.getPropertyValue( getPropertyManager().getCertificateTypePropertyName()));
			retVal.setStatus((String)from.getPropertyValue( getPropertyManager().getCertificateStatusPropertyName()));
			Date issueDate = (Date)from.getPropertyValue( getPropertyManager().getIssueDatePropertyName());
			retVal.setIssueDate(issueDate );
			retVal.setOfferDisplayName((String)from.getPropertyValue( getPropertyManager().getOfferDisplayNamePropertyName()));
			retVal.setOfferDetailsPageName((String)from.getPropertyValue( getPropertyManager().getOfferDetailsPageNamePropertyName()));
			return retVal;
		}
	}
	
	private  class ToOrderCertificateAdapter implements Function<RepositoryItem, LoyaltyCertificate> {
		@Override
		public LoyaltyCertificate apply( RepositoryItem from ) {
			
			final LoyaltyCertificate retVal = new LoyaltyCertificate();
			retVal.setId(from.getRepositoryId());
			retVal.setCertificateNumber( (String)from.getPropertyValue( OrderPropertyManager.REWARD_CERTIFICATE_NUMBER.getValue()));
			Double value = (Double)from.getPropertyValue( OrderPropertyManager.REWARD_CERTIFICATE_AMT_AUTH.getValue());
			retVal.setValue( value );
			retVal.setMarkdownCode( (String)from.getPropertyValue( OrderPropertyManager.REWARD_MARKDOWN_CODE.getValue()));
			retVal.setValid( true );
			retVal.setType((String)from.getPropertyValue( OrderPropertyManager.REWARD_TYPE.getValue()));
			retVal.setStatus((String)from.getPropertyValue( OrderPropertyManager.REWARD_CERTIFICATE_STATUS.getValue()));
			retVal.setOfferDisplayName((String)from.getPropertyValue( "offerDisplayName"));
			return retVal;
		}
	}
	
	private static class ToInvalidCertificateAdapter implements Function<String, LoyaltyCertificate> {
		@Override
		public LoyaltyCertificate apply( String from ) {
			final LoyaltyCertificate retVal = new LoyaltyCertificate();
			retVal.setId( from );
			retVal.setCertificateNumber( from );
			retVal.setMarkdownCode( from );
			retVal.setValue( 0.0 );
			retVal.setValid( false );
			return retVal;
		}
	}


	/**
	 *
	 * @param prodType
	 * @return true or false
	 */
	public boolean isProductDiscountable( String prodType ) {

		return prodType != null && (getUndiscountableProductTypes() == null || getUndiscountableProductTypes().length <= 0 || Arrays.binarySearch(getUndiscountableProductTypes(), prodType) >= 0);
	}
	
	public RepositoryItem getCertificate( String certificateId ) throws RepositoryException {
		return getOrderRepository().getItem( certificateId, OrderPropertyManager.REWARD_CERTIFICATE.getValue());
	}

	/** Returns the certificates used on the order
	 * 
	 * @param order
	 * @return */
	public Set<RepositoryItem> getOrderCertificates( Order order ) {
		Set<RepositoryItem> retVal = null;
		if( order instanceof DigitalOrderImpl ) {
			retVal = ( (DigitalOrderImpl)order ).getRewardCertificates();
		}
		if( retVal == null ) {
			retVal = new LinkedHashSet<>();
		}
		return retVal;
	}

	public List<LoyaltyCertificate> getAppliedORReservedOrderCertificates( DigitalOrderImpl order ) {
		List<LoyaltyCertificate> rewardCerts = new ArrayList<>();
		final Set<RepositoryItem> rewardCertificates = order.getRewardCertificates();
		if( rewardCertificates != null && !rewardCertificates.isEmpty()){
			Iterator<RepositoryItem> i = rewardCertificates.iterator();
			List<RepositoryItem> appliedCerts = new ArrayList<>();
			String VALIDATED_CERT = OrderRewardCertificateStatus.VALIDATED.getValue();
			String RESERVED_CERT = OrderRewardCertificateStatus.RESERVED.getValue();
			while( i.hasNext() ) {
				RepositoryItem cert = i.next();
				if( VALIDATED_CERT.equals( cert.getPropertyValue( OrderPropertyManager.REWARD_CERTIFICATE_STATUS.getValue() ) )
						|| RESERVED_CERT.equals( cert.getPropertyValue( OrderPropertyManager.REWARD_CERTIFICATE_STATUS.getValue() ) ) ) {
					appliedCerts.add(cert );
				}
			}
			if(!appliedCerts.isEmpty()) {
				Iterables.addAll( rewardCerts, Iterables.transform( appliedCerts , new ToOrderCertificateAdapter() ) );
			}
		}
		return rewardCerts;
	}
	
	public List<LoyaltyCertificate> getRedeemedOrderCertificates( DigitalOrderImpl order ) {
		List<LoyaltyCertificate> rewardCerts = new ArrayList<>();
		final Set<RepositoryItem> rewardCertificates = order.getRewardCertificates();
		if( rewardCertificates != null && !rewardCertificates.isEmpty()){
			Iterator<RepositoryItem> i = rewardCertificates.iterator();
			List<RepositoryItem> redeemedCerts = new ArrayList<>();
			String RESERVED_CERT = OrderRewardCertificateStatus.RESERVED.getValue();
			while( i.hasNext() ) {
				RepositoryItem cert = i.next();
				if( RESERVED_CERT.equals( cert.getPropertyValue( OrderPropertyManager.REWARD_CERTIFICATE_STATUS.getValue() ) ) ) {
					redeemedCerts.add(cert );
				}
			}
			if(!redeemedCerts.isEmpty()) {
				Iterables.addAll( rewardCerts, Iterables.transform( redeemedCerts , new ToOrderCertificateAdapter() ) );
			}
		}
		return rewardCerts;
	}

	/**
	 * @param prevOrder
	 */
	public void linkGuestOrderWithLoyaltyDetails(DigitalOrderImpl prevOrder) {
		try {
			YantraLinkLoyaltyNumberToGuestOrder toGuestOrder = new YantraLinkLoyaltyNumberToGuestOrder();
			toGuestOrder.setOrderId(prevOrder.getId());

			DigitalCommercePropertyManager pmr = getProfilePropertyManager();
			Object loyaltyNumber = getProfile().getPropertyValue(pmr.getLoyaltyNumberPropertyName());
			if(null != loyaltyNumber) {
				toGuestOrder.setLoyaltyNumber((String) loyaltyNumber);
			}

			Object loyaltyTier = getProfile().getPropertyValue(pmr.getLoyaltyTierItemDescriptorName());
			if(null != loyaltyTier) {
				toGuestOrder.setLoyaltyTier((String) loyaltyTier);
			}

			orderManagementService.linkOrderWithLoyalty(toGuestOrder);

		} catch (Exception ex) {
			logInfo("Exception while sending request to queue S211_LOCAL_QUEUE ", ex);
		}
	}

	public Profile getProfile() {
		return ComponentLookupUtil.lookupComponent( ComponentLookupUtil.PROFILE, Profile.class  );
	}

	public void setUndiscountableProductTypes( String[] undiscountableProductTypes ) {
		this.undiscountableProductTypes = undiscountableProductTypes;
		Arrays.sort( undiscountableProductTypes );
	}

	/**
	 *
	 * @return DigitalCommercePropertyManager
	 */
	public DigitalCommercePropertyManager getProfilePropertyManager(){
		return (DigitalCommercePropertyManager)this.getProfile().getProfileTools().getPropertyManager();
	}
}
