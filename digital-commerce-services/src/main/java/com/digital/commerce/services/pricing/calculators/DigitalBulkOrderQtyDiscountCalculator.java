package com.digital.commerce.services.pricing.calculators;


import static com.digital.commerce.common.util.ComponentLookupUtil.COUPON_MESSAGE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.pricing.BandedDiscountCalculatorHelper;
import atg.commerce.pricing.BulkOrderDiscountCalculator;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.QualifiedItem;
import atg.commerce.pricing.definition.DiscountStructure;
import atg.commerce.pricing.definition.MatchingObject;
import atg.repository.RepositoryItem;

import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.pricing.DigitalCouponMessage;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class DigitalBulkOrderQtyDiscountCalculator extends BulkOrderDiscountCalculator{
	public String getCalculatorType(){
		return "bulkQty";
	}
	
	  protected BandedDiscountCalculatorHelper bandedDiscountCalculatorHelper = new DigitalBandedOrderQtyDiscountCalculatorHelper();

	/**
	 * We are overriding this method simply to populate the custom
	 * orderPromoShare map on the commerceItem which is required by GIS and
	 * Yantra. There is no custom pricing logic here.
	 *
	 * @see #updateShippingItemsSubtotalMaps
	 *
	 * @param pPriceQuote
	 *            OrderPriceInfo representing the current price quote for the
	 *            order
	 * @param pOrder
	 *            The order to price
	 * @param pPricingModel
	 *            A RepositoryItems representing a PricingModel
	 * @param pProfile
	 *            The user's profile
	 * @param pExtraParameters
	 *            A Map of extra parameters to be used in the pricing, may be
	 *            null
	 */
	public void priceOrder(OrderPriceInfo pPriceQuote, Order pOrder,
						   RepositoryItem pPricingModel, Locale pLocale,
						   RepositoryItem pProfile,
						   Map pExtraParameters)
			throws PricingException {
		priceOrderBulkDiscount(pPriceQuote, pOrder, pPricingModel, pLocale, pProfile,
				pExtraParameters);
		String promoId = pPricingModel.getRepositoryId();
		/*
		 * We check for the discountStructure object to see if this is a
		 * promotion that is actually getting applied. There are lecagcy promos
		 * that are being priced but arent actually resulting in anything, so we
		 * use this to filter them out.
		 */
		DiscountStructure discountStruncture = (DiscountStructure) pExtraParameters
				.get("discountStructure");
		if (discountStruncture != null) {
			addOrderPromoShares(pOrder, promoId);
		}
	}

	public void priceOrderBulkDiscount(OrderPriceInfo pPriceQuote, Order pOrder,
									   RepositoryItem pPricingModel, Locale pLocale,
									   RepositoryItem pProfile, Map pExtraParameters)
			throws PricingException {
		if (isLoggingDebug()) {
			logDebug((new StringBuilder()).append("Pricing the Order : ")
					.append(pOrder.getId()).toString());
		}
		List commerceItems = pOrder.getCommerceItems();
		List priceQuotes = new ArrayList(commerceItems.size());
		CommerceItem item;
		DigitalCouponMessage couponMessage = (DigitalCouponMessage) ComponentLookupUtil.lookupComponent(COUPON_MESSAGE);
		for (Iterator itemIterator = commerceItems.iterator(); itemIterator
				.hasNext(); priceQuotes.add(item.getPriceInfo())) {
			item = (CommerceItem) itemIterator.next();
		}

		MatchingObject ret = null;
		ret = (MatchingObject) pExtraParameters.get("matchingObject");
		if (ret == null) {
			ret = findMatchingObject(pPriceQuote, priceQuotes, commerceItems, pPricingModel, pProfile, pLocale, pOrder, pExtraParameters);
			if (ret == null) {
				if (isLoggingDebug())
					logDebug((new StringBuilder()).append("order : ")
							.append(pOrder.getId()).append(" didn't qualify.")
							.toString());
				return;
			}
		}
		if (isLoggingDebug()) {
			logDebug((new StringBuilder()).append("discounting the order : ").append(pOrder.getId()).toString());
		}
		double adjuster = getAdjusterValue(pPricingModel, pExtraParameters);


		if (Double.isNaN(adjuster)) {
			//The below code is for closeness qualifier to work in tiered promotion scenarios
			pExtraParameters.put("didPromotionQualify", Boolean.FALSE);
			Collection promotionsNotApplied = (Set)pExtraParameters.get("promotionsNotApplied");
			if (promotionsNotApplied!=null){
				promotionsNotApplied.add(pPricingModel);
				pExtraParameters.put("promotionsNotApplied", promotionsNotApplied);
			}
			if (couponMessage.getPromoId()!=null && couponMessage.getPromoId().equals(pPricingModel.getRepositoryId())) {
				 couponMessage.setPromoApplied(false);
				 couponMessage.setErrorsCode("not_qualified_order_level");
			 }
			return;
		}
		String discountType = getDiscountType(pPricingModel, pExtraParameters);
		double oldOrderAmount = pPriceQuote.getAmount();
		if (isLoggingDebug()) {
			logDebug((new StringBuilder())
					.append("order's amount before discounting: ")
					.append(oldOrderAmount).toString());
		}
		double nonDiscountableTotal = 0.0D;
		nonDiscountableTotal = getNonDiscountableTotal(pOrder, pExtraParameters);
		if (isLoggingDebug()) {
			logDebug((new StringBuilder()).append("non discountable Total: ")
					.append(nonDiscountableTotal).toString());
		}
		double newAmount = getAmountToDiscount(pPriceQuote, pOrder, pPricingModel, pLocale, pProfile, pExtraParameters,
				nonDiscountableTotal);
		for (int i = 0; (long) i < ret.getQuantity(); i++) {
			newAmount = adjustOrderSubTotal(newAmount, adjuster, discountType, pOrder.getId(), pPricingModel, pExtraParameters);
		}

		newAmount = getPricingTools().round(newAmount) + nonDiscountableTotal;
		if (isLoggingDebug()) {
			logDebug((new StringBuilder())
					.append("order's amount after discounting: ")
					.append(newAmount).toString());
		}
		pPriceQuote.setAmount(newAmount);
		Map currentTaxableShares = createCurrentShareMap("orderDiscountShare",true, pPriceQuote, pOrder, pLocale, pProfile, pExtraParameters);
		Map currentNonTaxableShares = createCurrentShareMap("orderDiscountShare", false, pPriceQuote, pOrder, pLocale,pProfile, pExtraParameters);
		updateItemsDiscountShare(pOrder, oldOrderAmount, getPricingTools().round(oldOrderAmount - newAmount), adjuster, discountType,ret.getQuantity(), pExtraParameters);
		adjustShippingGroupSubtotalPriceInfos("orderDiscountShare",pPriceQuote, pOrder, pLocale, pProfile, pExtraParameters,currentTaxableShares, currentNonTaxableShares);

		pPriceQuote.setDiscounted(true);
		Integer pricingModelIndex = null;
		if (pExtraParameters != null) {
			pricingModelIndex = (Integer) pExtraParameters
					.get("pricingModelIndex");
		}
		double adjustAmount = pPriceQuote.getAmount() - oldOrderAmount;
		if (adjustAmount != 0.0D) {
			pPriceQuote
					.getAdjustments()
					.add(new PricingAdjustment(
							Constants.ORDER_DISCOUNT_PRICE_ADJUSTMENT_DESCRIPTION,
							pPricingModel, getPricingTools()
							.round(adjustAmount), 1L, pricingModelIndex));
		}
	}

	/**
	 * This method will get the items from the Order, iterate over them checking if the item is discountable
	 * or not. Based on this value it will update (or not) the non-discountable total.
	 *
	 * We need to override this method because OOTB exclusions to not apply to order-level promotions. We will pull any items that meet the exclusion criteria
	 * to make sure they are not factored into the discount.
	 *
	 * @param pOrder The order that contains the items to iterate over
	 * @param pExtraParametersMap A Map of extra parameters
	 * @return The combined total for the non-discountable items
	 */
	public double getNonDiscountableTotal(Order pOrder, Map pExtraParametersMap) {
		Double nonDiscountableTotal = super.getNonDiscountableTotal(pOrder, pExtraParametersMap);

		//Adding dsw exclusions to the non-discountable total
		List items = pOrder.getCommerceItems();
		if(items != null){
			Iterator itemsIter = items.iterator();
			while (itemsIter.hasNext()){
				DigitalCommerceItem item = (DigitalCommerceItem) itemsIter.next();
				if(item.isExcludedFromPromotion()){
					if(isLoggingDebug()){
						logDebug("DigitalBulkOrderQtyDiscountCalculator: subtracting amount " +item.getPriceInfo().getAmount());
					}
					nonDiscountableTotal += item.getPriceInfo().getAmount();
				}
			}
		}

		//NEW exclusion validation


		return nonDiscountableTotal;
	}

	/**
	 * Invoke customized discount calculator helper to get the adjuster value based on the list of items to receive the discount.
	 * @param pPricingModel
	 * @param pExtraParameters
	 * @return adjuster value
	 * @throws PricingException
	 */
	public double getAdjusterValue(RepositoryItem pPricingModel,
							  Map pExtraParameters) throws PricingException {
		double adjuster = 0.0D;
		Collection qualifiedItems = null,dswQualifiedItems=null;
		if (pExtraParameters != null) {
			qualifiedItems = (Collection) pExtraParameters.get("qualifiedItems");
			dswQualifiedItems =  (Collection)pExtraParameters.get("dswQualifiedTargetItems");

			if(qualifiedItems == null){
				qualifiedItems = dswQualifiedItems;
			}
		}
		adjuster = getBandedDiscountCalculatorHelper().getAdjuster(
				pPricingModel, qualifiedItems,
				pExtraParameters, getDefaultBandingProperty(),
				getDefaultBandingPropertyScope());

		return adjuster;
	}

	public double getAdjuster(RepositoryItem pPricingModel, Map pExtraParameters) throws PricingException {
		double adjuster = 0.0D;
		Collection qualifiedItems = null,dswQualifiedItems=null;
		if (pExtraParameters != null) {
			qualifiedItems = (Collection) pExtraParameters.get("qualifiedItems");
			dswQualifiedItems =  (Collection)pExtraParameters.get("dswQualifiedTargetItems");
			
			if(qualifiedItems == null){
				qualifiedItems = dswQualifiedItems;
			}
		}

		adjuster = getBandedDiscountCalculatorHelper().getAdjuster(pPricingModel, qualifiedItems, pExtraParameters,
							getDefaultBandingProperty(), getDefaultBandingPropertyScope());
		
		if (pExtraParameters != null) {
			if(pExtraParameters.containsKey("dswQualifiedTargetItems")){
				//remove after using it
				pExtraParameters.remove("dswQualifiedTargetItems");
			}
		}

		return adjuster;
	}

	/**
	 * This method populates the custom orderPromoShare object. The
	 * orderDiscountShare is already calulcated by the OOTB code by this point,
	 * so we use this along with the previous orderPromoShare entries to
	 * calculate the effective share of this promo.
	 *
	 * @param pOrder
	 * @param promoId
	 */
	public void addOrderPromoShares(Order pOrder, String promoId) {
		List<DigitalCommerceItem> items = pOrder.getCommerceItems();
		for (int i = 0; i < items.size(); i++) {
			DigitalCommerceItem item = items.get(i);
			ItemPriceInfo price = item.getPriceInfo();

			Map<String, Double> orderPromoShareMap = item.getOrderPromoShare();
			if (orderPromoShareMap == null) {
				orderPromoShareMap = new HashMap<>();
				item.setOrderPromoShare(orderPromoShareMap);

			}
			double orderDiscountShare = price.getOrderDiscountShare();
			double previousPromoShares = 0.0;
			for (String key : orderPromoShareMap.keySet()) {
				previousPromoShares += orderPromoShareMap.get(key);
			}
			double promoDiscountShare = orderDiscountShare
					- previousPromoShares;
			promoDiscountShare = getPricingTools().round(promoDiscountShare);
			if(promoDiscountShare!=0.0) {
				orderPromoShareMap.put(promoId, new Double(promoDiscountShare));
			}
		}

	}

	/*
	 *
	 *  @param pOrder
	 *  			 From which the list of commerce Items eligible for discount will be derived.
	 *  @return List of commerceItems eligible for discount.
	 */
	@Override
	protected List getItemsToReceiveDiscountShare(Order pOrder) {
		List items = pOrder.getCommerceItems();
		List itemsListModified = new ArrayList(items);
		if (items == null)
			return null;
		Iterator itemsListIterator = itemsListModified.iterator();
		do {
			if (!itemsListIterator.hasNext())
				break;
			DigitalCommerceItem item = (DigitalCommerceItem) itemsListIterator.next();

			ItemPriceInfo amountInfo = item.getPriceInfo();
			if (item.isExcludedFromPromotion()
					|| (Math.abs(amountInfo.getAmount()) <= 1.0000000000000001E-005D || !amountInfo
					.getDiscountable()))
				itemsListIterator.remove();
			// if((Math.abs(amountInfo.getAmount()) <= 1.0000000000000001E-005D
			// || !amountInfo.getDiscountable()))

		} while (true);
		return itemsListModified;
	}
}

