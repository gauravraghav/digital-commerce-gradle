/**
 * 
 */
package com.digital.commerce.services.storelocator;

import atg.commerce.catalog.CatalogTools;
import atg.commerce.inventory.InventoryException;
import atg.commerce.inventory.InventoryInfo;
import atg.commerce.inventory.InventoryManager;
import atg.commerce.inventory.RepositoryInventoryManager;
import atg.nucleus.GenericService;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author jvasired
 *
 */
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalStoreLocatorInventoryManager extends GenericService{
	
	private CatalogTools catalogTools;
	private DigitalGeoLocatorProvider geoLocatorProvider;
	private DigitalCoordinateManager coOrdinateManager;
	private InventoryManager inventoryManager;
	private Repository repository;
	private String rolledUpInventoryId;
	private DigitalGeoLocationUtil geoLocationUtil;
	
	private static final String ROLLED_UP_STOCK_LEVEL = "rolledUpStockLevel";
	private static final String STOCK_LEVEL = "stockLevel";
	private static final String SHIP_TO_STORE = "shipToStore";
	private static final String PICKUP_IN_STORE = "pickupInStore";
	private static final String SHIP_TO_STORE_BOSTS = "shipToStoreBOSTS";
	private static final String PICKUP_IN_STORE_BOPIS = "pickupInStoreBOPIS";
	private static final String PRODUCT_LEVEL_INVENTORY = "productLevelInventory";
	private static final String SKUS_RESULT = "skusResult";
	private static final String STORE_DETAILS = "storeDetails";
	private static final String BOPIS_ENABLED = "bopisEnabled";
	private static final String BOSTS_ENABLED = "bostsEnabled";
	
	private static final String    PERFORM_MONITOR_NAME    = "DigitalPDPInventoryDropet";
	private static final String PRODUCT_PARENTS = "parentProducts";
	private static final String	PROD_DEPT_PROPERTY			= "depId";
	private static final String	PROD_CLASS_PROPERTY			= "classId";
	private static final String	PROD_SUBCLASS_PROPERTY		= "subclassId";

	/**
	 * @return 
	 * 
	 */
	public ArrayList<RepositoryItem> getSkuInventoryForLocation(String skuId,ArrayList<RepositoryItem> result){
		
		String METHOD_NAME = "getSkuInventoryForStoreLocator";
		ArrayList<RepositoryItem> resultsList = null;
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			if(skuId!=null){
				resultsList = getInventory(skuId,result);
			}
		} catch (Exception e) {
			String msg = "Error in Getting Product/Sku Inventory";
			if (isLoggingError()) {
				logError(msg+" "+e.getMessage());
			}
		}
		finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		return resultsList;
	}
	
	/**
	 * This method returns inventory for the given sku id and location id.
	 * @param skuIds
	 * @param locationId
	 * @return
	 */
	public Map<String,Object> getInventoryForSkus(List<String> skuIds,String locationId){
		
		Map<String,Object> result = new HashMap<>();
		String METHOD_NAME = "getInventoryForSkus";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			if(skuIds!=null && !skuIds.isEmpty() && locationId!=null){
				result = getSKUInventoryForSKus(skuIds,null,locationId);
			}
		} catch (Exception e) {
			String msg = "Error in Getting Product/Sku Inventory";
			if (isLoggingError()) {
				logError(msg+" "+e.getMessage());
			}
		}
		finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		
		return result;
	}
	
	
	public Map<String,Object> getInventoryForSkusItems(List<RepositoryItem> skuItems,String locationId){
		
		Map<String,Object> result = new HashMap<>();
		String METHOD_NAME = "getInventoryForSkus";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			if(skuItems!=null && !skuItems.isEmpty() && locationId!=null){
				result = getSKUInventoryForSkuItems(skuItems,null,locationId);
			}
		} catch (Exception e) {
			String msg = "Error in Getting Product/Sku Inventory";
			if (isLoggingError()) {
				logError(msg+" "+e.getMessage());
			}
		}
		finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		
		return result;
	}

	/**
	 *
	 * @param skuId
	 * @param result
	 * @return ArrayList<RepositoryItem>
	 */
   private ArrayList<RepositoryItem> getInventory(String skuId,ArrayList<RepositoryItem> result){
	   String METHOD_NAME = "getInventory";
	   ArrayList<RepositoryItem> resultsList = new ArrayList<>();
	   try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			if (isLoggingDebug()) {
				logDebug("DigitalPDPInventoryDropet getInventory method");
			}
			if (result != null && !result.isEmpty()) {
				for (RepositoryItem storeItem : result) {
					Map<String,RepositoryItem> itemInStockMap = getSKUInventoryForStore(skuId, storeItem,storeItem.getRepositoryId());
					if(itemInStockMap!=null && itemInStockMap.get("InStock")!=null){
						resultsList.add(itemInStockMap.get("InStock"));
					}
				}
			}
		 }     
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
	   return resultsList;
   }
   
   /**
    * Returns Store level and rolled up inventory for the given Sku and Store.
    * 
    * @param skuId
    * @param locationId
    */
   private Map<String,RepositoryItem> getSKUInventoryForStore(String skuId,RepositoryItem storeItem,String locationId){
	   String METHOD_NAME = "getSKUInventoryForStore";
	   Map<String,RepositoryItem> statusItemMap = new HashMap<>();
	   try {
		   	   DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			   RepositoryItem skuItem = catalogTools.findSKU(skuId);
			   boolean isBOPIS=isBOPIS(skuItem);
			   boolean isBOSTS=isBOSTS(skuItem);
			   boolean isBOPISAndBOSTS=false;
			   boolean isBOSTSOnly=false;
			   boolean isBOPISOnly=false;
			   long rolledUpStock = 0;
			   long storeLevelStock = 0;
			   try {
				    DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
				    if(isLoggingDebug()){
						logDebug("DigitalPDPInventoryDropet getInventory method");
					}
				    //First check SKU level restriction
			    	if(skuItem!=null){
			    		if(isBOPIS && isBOSTS){
			    			isBOPISAndBOSTS=true;
			    		}else if(!isBOPIS && !isBOSTS){
			    			isBOPISAndBOSTS=false;
			    		}else if(!isBOPIS && isBOSTS){
			    			isBOSTSOnly=true;
			    		}else if(isBOPIS && !isBOSTS){
			    			isBOPISOnly=true;
			    		}
			    	}
			    	if(storeItem==null){
			    		storeItem = repository.getItem(locationId, "location");
			    	}
		    		//Second check Store[Location] level restriction
			    	if(storeItem!=null && storeItem instanceof MutableRepositoryItem){
			    	  try{
			    		   rolledUpStock = ((RepositoryInventoryManager)inventoryManager).queryStockLevel(skuId, getRolledUpInventoryId());			    		   
			    		   storeLevelStock = ((RepositoryInventoryManager)inventoryManager).queryStockLevel(skuId, locationId);			    		   
			    	   }catch(InventoryException ie){
			    		   if(DigitalStringUtil.isNotBlank(ie.getMessage()) && ie.getMessage().contains("No item with the unique catalog ref ID")) {
				    			if(isLoggingDebug()){
				    				logDebug(ie.getMessage() + " for the locationId " + locationId);
				    			}
			    		   } else {
			    			   logError("Inventory exception whole querying stock level " + ie.getMessage());
			    		   }
			    		}			  
			    	  
			    	  ((MutableRepositoryItem) storeItem).setPropertyValue(ROLLED_UP_STOCK_LEVEL, rolledUpStock);
			    	  
			    	  ((MutableRepositoryItem) storeItem).setPropertyValue(STOCK_LEVEL, storeLevelStock);
			    	  
				        if(isBOPISAndBOSTS){
				    		if(isShipToStore(storeItem)&&isPickupInStore(storeItem)){
				    			((MutableRepositoryItem) storeItem).setPropertyValue(SHIP_TO_STORE, "Y");
								((MutableRepositoryItem) storeItem).setPropertyValue(PICKUP_IN_STORE, "Y");
				    		}else if(isShipToStore(storeItem)&&!isPickupInStore(storeItem)){
				    			((MutableRepositoryItem) storeItem).setPropertyValue(SHIP_TO_STORE, "Y");
								((MutableRepositoryItem) storeItem).setPropertyValue(PICKUP_IN_STORE, "N");
				    		}else if(!isShipToStore(storeItem)&&isPickupInStore(storeItem)){
				    			((MutableRepositoryItem) storeItem).setPropertyValue(PICKUP_IN_STORE, "Y");
				    			 ((MutableRepositoryItem) storeItem).setPropertyValue(SHIP_TO_STORE, "N");
				    		}else if(!isShipToStore(storeItem)&& !isPickupInStore(storeItem)){
				    			((MutableRepositoryItem) storeItem).setPropertyValue(SHIP_TO_STORE, "N");
								  ((MutableRepositoryItem) storeItem).setPropertyValue(PICKUP_IN_STORE, "N");
				    		}
					      }else if(isBOSTSOnly && isShipToStore(storeItem)){
					    	  ((MutableRepositoryItem) storeItem).setPropertyValue(SHIP_TO_STORE, "Y");
							  ((MutableRepositoryItem) storeItem).setPropertyValue(PICKUP_IN_STORE, "N");
					      }else if(isBOPISOnly && isPickupInStore(storeItem)){
					    	  ((MutableRepositoryItem) storeItem).setPropertyValue(PICKUP_IN_STORE, "Y");
					    	  ((MutableRepositoryItem) storeItem).setPropertyValue(SHIP_TO_STORE, "N");
					      }else{
					    	  ((MutableRepositoryItem) storeItem).setPropertyValue(SHIP_TO_STORE, "N");
							  ((MutableRepositoryItem) storeItem).setPropertyValue(PICKUP_IN_STORE, "N");
					      }
				    }
			    	if(rolledUpStock>0){			    		
			    		statusItemMap.put("InStock", storeItem);			    		
			    	}
			   }catch (Exception ex) {
					String msg = "Error in getInventory";
					if (isLoggingError()) {
						logError(msg+" "+ex.getMessage());
					}
				} 
		   }catch (RepositoryException e) {
			   if (isLoggingError()) {
					logError(e.getMessage());
				}
			}
	       
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
	   return statusItemMap;
   }

	/**
	 * It gets Inventory for all the skus at a given store and produt level inventory and pickup
	 * options
	 *
	 * @param skuIds
	 * @param storeItem
	 * @param locationId
	 * @return Map<String, Object>
	 */
   private Map<String, Object> getSKUInventoryForSKus(List<String> skuIds,RepositoryItem storeItem,String locationId){
	   String METHOD_NAME = "getSKUInventoryForSKus";
	   List<InventoryInfo> infos = new ArrayList<>();
	   boolean isProductShipToStore=false;
	   boolean isProductPickUpInStore=false;
	   Map<String,Object> result = new HashMap<>();
	   Map<String,SkuInventoryDetailBean> mSKUsResult = new HashMap<>();
	   SkuInventoryDetailBean skuInventoryDetailBean = null;
	   try {
		   DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
		   for(String skuId : skuIds){
			   RepositoryItem skuItem = catalogTools.findSKU(skuId);
			   boolean isShipToStore=false;
			   boolean isPickUpInStore=false;
			   boolean isBOPIS=isBOPIS(skuItem);
			   boolean isBOSTS=isBOSTS(skuItem);
			   boolean isBOPISAndBOSTS=false;
			   boolean isBOSTSOnly=false;
			   boolean isBOPISOnly=false;
			   long storeLevelStock = 0;
		       long rolledUpStock = 0;
		       boolean isSKUNotAvailAtStore=false;
		       boolean isSKUNotAvailOnline=false;

		      
			   try {
				   if (locationId.equals(getRolledUpInventoryId())){
			    	   rolledUpStock = ((RepositoryInventoryManager)inventoryManager).queryStockLevel(skuId, getRolledUpInventoryId());
			    	   skuInventoryDetailBean = new SkuInventoryDetailBean();
					    skuInventoryDetailBean.setStoreLevelStock(storeLevelStock);
					    skuInventoryDetailBean.setRolledUpStock(rolledUpStock);
					    if (isGWPItem(skuItem))
					    {
					    	skuInventoryDetailBean.setShipToStore(false);
						    skuInventoryDetailBean.setPickUpInStore(false);
						    isProductShipToStore=false;
						    isProductPickUpInStore=false;
					    }else{
						    skuInventoryDetailBean.setShipToStore(isShipToStore);
						    skuInventoryDetailBean.setPickUpInStore(isPickUpInStore); 
						    if(isShipToStore){
							   isProductShipToStore=isShipToStore;
							   } 
						   if(isPickUpInStore){
							   isProductPickUpInStore=isPickUpInStore;
						   }
					    }
				    	mSKUsResult.put(skuId, skuInventoryDetailBean);
			       }
				   else 
				   {
						DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
					    if(isLoggingDebug()){
							logDebug("DigitalPDPInventoryDropet getInventory method");
						}
					    //First check SKU level restriction
				    	if(skuItem!=null){
				    		if(isBOPIS && isBOSTS){
				    			isBOPISAndBOSTS=true;
				    		}else if(!isBOPIS && !isBOSTS){
				    			isBOPISAndBOSTS=false;
				    		}else if(!isBOPIS && isBOSTS){
				    			isBOSTSOnly=true;
				    		}else if(isBOPIS && !isBOSTS){
				    			isBOPISOnly=true;
				    		}
				    	}
				    	if(storeItem==null){
				    		storeItem = repository.getItem(locationId, "location");
				    	}
				    	
			    		//Second check Store[Location] level restriction
				    	if(storeItem!=null && storeItem instanceof MutableRepositoryItem){
				    	  try{
					    		rolledUpStock = ((RepositoryInventoryManager)inventoryManager).queryStockLevel(skuId, getRolledUpInventoryId());	
					    		storeLevelStock = ((RepositoryInventoryManager)inventoryManager).queryStockLevel(skuId, locationId);							
					    	   }catch(Exception ie){
					    		   if(storeLevelStock==0){
					    			   isSKUNotAvailAtStore=true;
					    		   }if(rolledUpStock==0){
					    			   isSKUNotAvailOnline=true;
					    		   }
					    		   if(DigitalStringUtil.isNotBlank(ie.getMessage()) && ie.getMessage().contains("No item with the unique catalog ref ID")) {
						    			if(isLoggingDebug()){
						    				logDebug(ie.getMessage() + " for the locationId " + locationId);
						    			}
					    		   } else {
					    			   logError("Inventory exception whole querying stock level " + ie.getMessage());
					    		   }
					    		}
						   
				        if(isBOPISAndBOSTS){
				    		if(isShipToStore(storeItem)&&isPickupInStore(storeItem)){
								isShipToStore=true;
								isPickUpInStore=true;
				    		}else if(isShipToStore(storeItem)&&!isPickupInStore(storeItem)){
								isShipToStore=true;
				    		}else if(!isShipToStore(storeItem)&&isPickupInStore(storeItem)){
									isPickUpInStore=true;
				    		}
					      }else if(isBOSTSOnly && isShipToStore(storeItem)){
							  isShipToStore=true;
					      }else if(isBOPISOnly && isPickupInStore(storeItem)){
							  isPickUpInStore=true;
					      }
				        
				        //
				        if(isShipToStore(storeItem)){
				        	((MutableRepositoryItem) storeItem).setPropertyValue(SHIP_TO_STORE, "Y");
				        }else{
				        	((MutableRepositoryItem) storeItem).setPropertyValue(SHIP_TO_STORE, "N");
				        }
				        
				        if(isPickupInStore(storeItem)){
				        	((MutableRepositoryItem) storeItem).setPropertyValue(PICKUP_IN_STORE, "Y");
				        }else{
				        	((MutableRepositoryItem) storeItem).setPropertyValue(PICKUP_IN_STORE, "N");
				        }
				        
				    	if(!isSKUNotAvailAtStore || !isSKUNotAvailOnline){
				    		//Used default FULLSITE location as it is rolled-up stock at SKU level
				    		infos.add(new InventoryInfo(skuId, inventoryManager));
						    skuInventoryDetailBean = new SkuInventoryDetailBean();
						    skuInventoryDetailBean.setStoreLevelStock(storeLevelStock);
						    skuInventoryDetailBean.setRolledUpStock(rolledUpStock);
						    if (isGWPItem(skuItem))
						    {
						    	skuInventoryDetailBean.setShipToStore(false);
							    skuInventoryDetailBean.setPickUpInStore(false);
							    isProductShipToStore=false;
							    isProductPickUpInStore=false;
						    }else{
							    skuInventoryDetailBean.setShipToStore(isShipToStore);
							    skuInventoryDetailBean.setPickUpInStore(isPickUpInStore); 
							    if(isShipToStore){
								   isProductShipToStore=isShipToStore;
								   } 
							   if(isPickUpInStore){
								   isProductPickUpInStore=isPickUpInStore;
							   }
						    }
					    	mSKUsResult.put(skuId, skuInventoryDetailBean);
				    	}
				    	 }
				   	}
			   }catch (Exception ex) {
					String msg = "Error in getInventory";
					if (isLoggingError()) {
						logError(msg+" "+ex.getMessage());
					}
				}
			 /**  if(isShipToStore){
				   isProductShipToStore=isShipToStore;
				   } 
			   if(isPickUpInStore){
				   isProductPickUpInStore=isPickUpInStore;
			   }*/
			   } 
	    int productLevelStock=0;
		if(infos!=null && !infos.isEmpty()){
			for(InventoryInfo info : infos){
				if(info!=null){
					try{
						if(info.getStockLevel() != null){
							productLevelStock+=info.getStockLevel();
						}
					}catch(Exception e){
						if (isLoggingError()) {
								logError(e.getMessage());
						}
					}
				}
				
			}
		}
		
		ProductInventoryDetailBean productInventoryDetailBean = new ProductInventoryDetailBean();
		productInventoryDetailBean.setProductLevelStock(productLevelStock);
		productInventoryDetailBean.setPickUpInStore(isProductPickUpInStore);
		productInventoryDetailBean.setShipToStore(isProductShipToStore);
		result.put(STORE_DETAILS, getGeoLocationUtil().convertStoreLocator(storeItem));
		result.put(PRODUCT_LEVEL_INVENTORY, productInventoryDetailBean);
		result.put(SKUS_RESULT,mSKUsResult);
	   }catch (RepositoryException e) {
		   if (isLoggingError()) {
				logError(e.getMessage());
			}
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
	   return result;
   }
   
   
   private Map<String, Object> getSKUInventoryForSkuItems(List<RepositoryItem> skuItems,RepositoryItem storeItem,String locationId){
	   String METHOD_NAME = "getSKUInventoryForSkuItems";
	   List<InventoryInfo> infos = new ArrayList<>();
	   boolean isProductShipToStore=false;
	   boolean isProductPickUpInStore=false;
	   Map<String,Object> result = new HashMap<>();
	   Map<String,SkuInventoryDetailBean> mSKUsResult = new HashMap<>();
	   SkuInventoryDetailBean skuInventoryDetailBean = null;
	   try {
		   DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
		   for(RepositoryItem skuItem : skuItems){
			   String skuId = (String)skuItem.getRepositoryId();
			   boolean isShipToStore=false;
			   boolean isPickUpInStore=false;
			   boolean isBOPIS=isBOPIS(skuItem);
			   boolean isBOSTS=isBOSTS(skuItem);
			   boolean isBOPISAndBOSTS=false;
			   boolean isBOSTSOnly=false;
			   boolean isBOPISOnly=false;
			   long storeLevelStock = 0;
		       long rolledUpStock = 0;
		       boolean isSKUNotAvailAtStore=false;
		       boolean isSKUNotAvailOnline=false;

		      
			  // try {
				   if (locationId.equals(getRolledUpInventoryId())){
			    	   rolledUpStock = ((RepositoryInventoryManager)inventoryManager).queryStockLevel(skuId, getRolledUpInventoryId());
			    	   skuInventoryDetailBean = new SkuInventoryDetailBean();
					    skuInventoryDetailBean.setStoreLevelStock(storeLevelStock);
					    skuInventoryDetailBean.setRolledUpStock(rolledUpStock);
					    if (isGWPItem(skuItem))
					    {
					    	skuInventoryDetailBean.setShipToStore(false);
						    skuInventoryDetailBean.setPickUpInStore(false);
						    isProductShipToStore=false;
						    isProductPickUpInStore=false;
					    }else{
						    skuInventoryDetailBean.setShipToStore(isShipToStore);
						    skuInventoryDetailBean.setPickUpInStore(isPickUpInStore); 
						    if(isShipToStore){
							   isProductShipToStore=isShipToStore;
							   } 
						   if(isPickUpInStore){
							   isProductPickUpInStore=isPickUpInStore;
						   }
					    }
				    	mSKUsResult.put(skuId, skuInventoryDetailBean);
			       }
				   else 
				   {
						DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
					    if(isLoggingDebug()){
							logDebug("DigitalPDPInventoryDropet getInventory method");
						}
					    //First check SKU level restriction
				    	if(skuItem!=null){
				    		if(isBOPIS && isBOSTS){
				    			isBOPISAndBOSTS=true;
				    		}else if(!isBOPIS && !isBOSTS){
				    			isBOPISAndBOSTS=false;
				    		}else if(!isBOPIS && isBOSTS){
				    			isBOSTSOnly=true;
				    		}else if(isBOPIS && !isBOSTS){
				    			isBOPISOnly=true;
				    		}
				    	}
				    	if(storeItem==null){
				    		storeItem = repository.getItem(locationId, "location");
				    	}
				    	
			    		//Second check Store[Location] level restriction
				    	if(storeItem!=null && storeItem instanceof MutableRepositoryItem){
				    	  try{
					    		rolledUpStock = ((RepositoryInventoryManager)inventoryManager).queryStockLevel(skuId, getRolledUpInventoryId());	
					    		storeLevelStock = ((RepositoryInventoryManager)inventoryManager).queryStockLevel(skuId, locationId);							
					    	   }catch(Exception ie){
					    		   if(storeLevelStock==0){
					    			   isSKUNotAvailAtStore=true;
					    		   }if(rolledUpStock==0){
					    			   isSKUNotAvailOnline=true;
					    		   }
					    		   if(DigitalStringUtil.isNotBlank(ie.getMessage()) && ie.getMessage().contains("No item with the unique catalog ref ID")) {
						    			if(isLoggingDebug()){
						    				logDebug(ie.getMessage() + " for the locationId " + locationId);
						    			}
					    		   } else {
					    			   logError("Inventory exception whole querying stock level " + ie.getMessage());
					    		   }
					    		}
						   
				        if(isBOPISAndBOSTS){
				    		if(isShipToStore(storeItem)&&isPickupInStore(storeItem)){
								isShipToStore=true;
								isPickUpInStore=true;
				    		}else if(isShipToStore(storeItem)&&!isPickupInStore(storeItem)){
								isShipToStore=true;
				    		}else if(!isShipToStore(storeItem)&&isPickupInStore(storeItem)){
									isPickUpInStore=true;
				    		}
					      }else if(isBOSTSOnly && isShipToStore(storeItem)){
							  isShipToStore=true;
					      }else if(isBOPISOnly && isPickupInStore(storeItem)){
							  isPickUpInStore=true;
					      }
				        
				        //
				        if(isShipToStore(storeItem)){
				        	((MutableRepositoryItem) storeItem).setPropertyValue(SHIP_TO_STORE, "Y");
				        }else{
				        	((MutableRepositoryItem) storeItem).setPropertyValue(SHIP_TO_STORE, "N");
				        }
				        
				        if(isPickupInStore(storeItem)){
				        	((MutableRepositoryItem) storeItem).setPropertyValue(PICKUP_IN_STORE, "Y");
				        }else{
				        	((MutableRepositoryItem) storeItem).setPropertyValue(PICKUP_IN_STORE, "N");
				        }
				        
				    	if(!isSKUNotAvailAtStore || !isSKUNotAvailOnline){
				    		//Used default FULLSITE location as it is rolled-up stock at SKU level
				    		infos.add(new InventoryInfo(skuId, inventoryManager));
						    skuInventoryDetailBean = new SkuInventoryDetailBean();
						    skuInventoryDetailBean.setStoreLevelStock(storeLevelStock);
						    skuInventoryDetailBean.setRolledUpStock(rolledUpStock);
						    if (isGWPItem(skuItem))
						    {
						    	skuInventoryDetailBean.setShipToStore(false);
							    skuInventoryDetailBean.setPickUpInStore(false);
							    isProductShipToStore=false;
							    isProductPickUpInStore=false;
						    }else{
							    skuInventoryDetailBean.setShipToStore(isShipToStore);
							    skuInventoryDetailBean.setPickUpInStore(isPickUpInStore); 
							    if(isShipToStore){
								   isProductShipToStore=isShipToStore;
								   } 
							   if(isPickUpInStore){
								   isProductPickUpInStore=isPickUpInStore;
							   }
						    }
					    	mSKUsResult.put(skuId, skuInventoryDetailBean);
				    	}
				    	 }
				   	}
			   /*}catch (Exception ex) {
					String msg = "Error in getInventory";
					if (isLoggingError()) {
						logError(msg+" "+ex.getMessage());
					}
				}*/
			 /**  if(isShipToStore){
				   isProductShipToStore=isShipToStore;
				   } 
			   if(isPickUpInStore){
				   isProductPickUpInStore=isPickUpInStore;
			   }*/
			   } 
	    int productLevelStock=0;
		if(infos!=null && !infos.isEmpty()){
			for(InventoryInfo info : infos){
				if(info!=null){
					try{
						if(info.getStockLevel() != null){
							productLevelStock+=info.getStockLevel();
						}
					}catch(Exception e){
						if (isLoggingError()) {
								logError(e.getMessage());
						}
					}
				}
				
			}
		}
		
		ProductInventoryDetailBean productInventoryDetailBean = new ProductInventoryDetailBean();
		productInventoryDetailBean.setProductLevelStock(productLevelStock);
		productInventoryDetailBean.setPickUpInStore(isProductPickUpInStore);
		productInventoryDetailBean.setShipToStore(isProductShipToStore);
		result.put(STORE_DETAILS, getGeoLocationUtil().convertStoreLocator(storeItem));
		result.put(PRODUCT_LEVEL_INVENTORY, productInventoryDetailBean);
		result.put(SKUS_RESULT,mSKUsResult);
	   }catch (RepositoryException e) {
		   if (isLoggingError()) {
				logError(e.getMessage());
			}
		}catch (Exception ex) {
			String msg = "Error in getInventory";
			if (isLoggingError()) {
				logError(msg+" "+ex.getMessage());
			}
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
	   return result;
   }
   
   private boolean isBOPIS(RepositoryItem skuItem){
	   String sItem=null;
	   if(null!=skuItem){
		   sItem=(String)skuItem.getPropertyValue(BOPIS_ENABLED);
	   }
		if (null!=sItem){
			if ("1".equalsIgnoreCase(sItem) || "Y".equalsIgnoreCase(sItem)) {
				if(isLoggingDebug()){
					logDebug("SKU is BOPIS Eligible");
				}
				return true;
			}
		}
		return false;
   }
   
   private boolean isBOSTS(RepositoryItem skuItem){
	   String sItem=null;
	   if(null!=skuItem){
		   sItem=(String)skuItem.getPropertyValue(BOSTS_ENABLED);
	   }
	  if (null!=sItem){
		  if("1".equalsIgnoreCase(sItem) || "Y".equalsIgnoreCase(sItem)) {
			  if(isLoggingDebug()){
			  	logDebug("SKU is BOSTS Eligible");
			  }
			  return true;
			}
	  }
	  return false;
   }
  
	private boolean isShipToStore(RepositoryItem storeItem){
		String sItem=null;
		   if(null!=storeItem){
			   sItem=(String)storeItem.getPropertyValue(SHIP_TO_STORE_BOSTS);
		   }
		if (null!=sItem){
		    if("1".equalsIgnoreCase(sItem) || "Y".equalsIgnoreCase(sItem)) {
				if(isLoggingDebug()){
					logDebug("Store is Ship To Store Eligible");
				}
				return true;
			}
		}
		return false;
	}
	
	private boolean isPickupInStore(RepositoryItem storeItem){
		String sItem=null;
		   if(null!=storeItem){
			   sItem=(String)storeItem.getPropertyValue(PICKUP_IN_STORE_BOPIS);
		   }
		if (null!=sItem){
		    if("1".equalsIgnoreCase(sItem) || "Y".equalsIgnoreCase(sItem)) {
				if(isLoggingDebug()){
					logDebug("Store is Ship To Store Eligible");
				}
				return true;
			}
		}
		return false;
	 }
	//Determines whether the SKU is an GWP item
	public boolean isGWPItem(RepositoryItem skuItem){
		if (skuItem!=null)
		{
			@SuppressWarnings("unchecked")
			Set<RepositoryItem> parentProducts = (Set<RepositoryItem>)skuItem.getPropertyValue(PRODUCT_PARENTS);
			if (parentProducts!=null){
				Iterator<RepositoryItem> iter = parentProducts.iterator();
				while (iter.hasNext()){
					RepositoryItem productRef =iter.next();
					String productDepartment = (String)productRef.getPropertyValue(PROD_DEPT_PROPERTY);			
					String productClass =(String)productRef.getPropertyValue(PROD_CLASS_PROPERTY);
					String productSubClass = (String)productRef.getPropertyValue(PROD_SUBCLASS_PROPERTY);
					HashMap <String,String> gwpconfig = (HashMap<String, String>) MultiSiteUtil.getGWPConfiguration();
					if(productDepartment != null && productDepartment.equals(gwpconfig.get("gwpDepId")) 
							&& (productClass !=null && productClass.equals(gwpconfig.get("gwpClassId")))
							&& (productSubClass!=null && productSubClass.equals(gwpconfig.get("gwpSubClassId")) )) {					
						if( isLoggingDebug() ) {
							logDebug("GWP Item Found");
						}
						return true;		
					}
				}
			}		
		}
		return false;
	 }
}
