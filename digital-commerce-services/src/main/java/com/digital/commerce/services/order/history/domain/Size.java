
package com.digital.commerce.services.order.history.domain;

import javax.annotation.Generated;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
@Getter
@Setter
@ToString
public class Size {

    private String displayNameDefault;
    private String displayName;

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(displayNameDefault).append(displayName).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Size) == false) {
            return false;
        }
        Size rhs = ((Size) other);
        return new EqualsBuilder().append(displayNameDefault, rhs.displayNameDefault).append(displayName, rhs.displayName).isEquals();
    }

}
