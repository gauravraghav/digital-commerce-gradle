package com.digital.commerce.services.pricing;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.domain.ResponseWrapper;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.HttpServletUtil;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShippingGroupPropertyManager;
import com.digital.commerce.services.profile.DigitalProfileTools;

import atg.commerce.CommerceException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.OrderManager;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupPropertyContainer;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.dtm.UserTransactionDemarcation;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.nucleus.naming.ComponentName;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalUpdateShippingMethodComponent extends ApplicationLoggingImpl {

	static final String AVAILABLE_SHIPPING_METHODS = "availableShippingMethods";

	static final String CLASS_NAME = "DigitalUpdateShippingMethodComponent";

	private MessageLocator messageLocator;

	private DigitalProfileTools profileTools;

	private PricingTools pricingTools;

	private ComponentName mUserPricingModelsPath;

	public DigitalUpdateShippingMethodComponent() {
		super(DigitalUpdateShippingMethodComponent.class.getName());
	}

	public ResponseWrapper updateShippingMethod(String shippingMethod, String shippingGroupId)
			throws ServletException, IOException {
		final String METHOD_NAME = "updateShippingMethod";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
		final UserTransactionDemarcation td = TransactionUtils.startNewTransaction(CLASS_NAME, METHOD_NAME);
		ResponseWrapper response = new ResponseWrapper();
		response.setFormError(false);

		try {
			OrderHolder shoppingCart = (OrderHolder) ServletUtil.getCurrentRequest()
					.resolveName("/atg/commerce/ShoppingCart");
			Order order = shoppingCart.getCurrent();
			try {
				validateRequest(shippingMethod, shippingGroupId);
				ShippingGroup pWorkingShippingGroup = null;
				ShippingGroup ordersg = null;

				if (DigitalStringUtil.isNotBlank(shippingGroupId)) {
					pWorkingShippingGroup = order.getShippingGroup(shippingGroupId);
				} else {
					for (ShippingGroup sg : (List<ShippingGroup>) order.getShippingGroups()) {
						if(sg instanceof HardgoodShippingGroup){
							HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sg;
							String shipType = (String) hgSg
									.getPropertyValue(ShippingGroupPropertyManager.SHIP_TYPE.getValue());
							if (ShippingGroupConstants.ShipType.SHIP.getValue().equalsIgnoreCase(shipType)) {
								pWorkingShippingGroup = sg;
								break;
							}
						}
					}
				}
				ordersg = pWorkingShippingGroup;
				ordersg.setShippingMethod(shippingMethod);
				copyShippingGroupProperties(ordersg, pWorkingShippingGroup, ServletUtil.getCurrentRequest(),
						new DynamoHttpServletResponse());
				synchronized (order) {
					getOrderManager().updateOrder(order);
				}
				((DigitalPricingTools) getPricingTools()).priceOrderTotal(order,
						getPricingModelHolder(ServletUtil.getCurrentRequest()),
						HttpServletUtil.getUserLocale(ServletUtil.getCurrentRequest()), getProfile(), null);
			} catch (CommerceException | DigitalAppException de) {
				response.getGenericExceptions().add(new ResponseError("", de.getMessage()));
				response.setFormError(true);
				if (isLoggingError())
					logError(de);
			} catch (Exception e) {
				if (isLoggingError())
					logError(e.getMessage());
			}
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
			TransactionUtils.endTransaction(td, CLASS_NAME, METHOD_NAME);
		}
		return response;
	}

	private void validateRequest(String shippingMethod, String shippingGroupId) throws DigitalAppException {
		if (DigitalStringUtil.isBlank(shippingMethod)) {
			throw new DigitalAppException(getMessageLocator().getMessageString("template.content.key.notempty"));
		}
	}

	private void copyShippingGroupProperties(ShippingGroup pSrcShippingGroup, ShippingGroup pDestShippingGroup,
			DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		try {
			ShippingGroupPropertyContainer sm1 = (ShippingGroupPropertyContainer) pSrcShippingGroup;
			ShippingGroupPropertyContainer sm2 = (ShippingGroupPropertyContainer) pDestShippingGroup;
			sm2.copyProperties(sm1);
		} catch (CommerceException ce) {
			if (isLoggingDebug())
				logDebug(getMessageLocator().getMessageString("shipping.methods.copy.shippinggroup",
						pSrcShippingGroup.getShippingGroupClassType().toString()));
		}
	}

	private RepositoryItem getProfile() throws ServletException, IOException {
		final String METHOD_NAME = "getProfile";
		DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
		try {
			RepositoryItem profile = null;
			profile = (RepositoryItem) ServletUtil.getCurrentUserProfile();
			return profile;

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
		}
	}

	private OrderManager orderManager;

	private PricingModelHolder getPricingModelHolder(DynamoHttpServletRequest pRequest)
			throws ServletException, IOException {
		String perfName = "getPricingModels";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation("DigitalUpdateShippingMethodComponent", perfName);
		PricingModelHolder holder = null;
		try {
			Collection models = (Collection) pRequest
					.getObjectParameter(ParameterName.getParameterName("pricingModels"));
			if ((models == null) && (mUserPricingModelsPath != null)) {
				holder = (PricingModelHolder) pRequest.resolveName(mUserPricingModelsPath);
				if (holder != null)
					models = holder.getShippingPricingModels();
			}
			return holder;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation("DigitalUpdateShippingMethodComponent", perfName);
		}
	}

	public void setUserPricingModelsPath(String pUserPricingModelsPath) {
		if (pUserPricingModelsPath != null) {
			mUserPricingModelsPath = ComponentName.getComponentName(pUserPricingModelsPath);
		} else {
			mUserPricingModelsPath = null;
		}
	}

	public String getUserPricingModelsPath() {
		if (mUserPricingModelsPath != null) {
			return mUserPricingModelsPath.getName();
		}
		return null;
	}

}