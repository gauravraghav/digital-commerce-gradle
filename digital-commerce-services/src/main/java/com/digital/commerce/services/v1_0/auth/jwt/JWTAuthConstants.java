package com.digital.commerce.services.v1_0.auth.jwt;

public interface JWTAuthConstants {
    String GENERIC_ERROR = "GENERIC_ERROR";
    String AUTH_HEADER_MISSING = "AUTH_HEADER_MISSING";
    String AUTH_HEADER_FORMAT_ERROR = "AUTH_HEADER_FORMAT_ERROR";
    String ENCODING_ERROR = "ENCODING_ERROR";
    String TOKEN_EXPIRED = "TOKEN_EXPIRED";
    String VERIFICATION_ERROR = "VERIFICATION_ERROR";
    String BEARER_CLAIM="Bearer";
    String AUTHORIZATION_HEADER = "Authorization";
    String ACCESS_DENIED = "ACCESS_DENIED";
}
