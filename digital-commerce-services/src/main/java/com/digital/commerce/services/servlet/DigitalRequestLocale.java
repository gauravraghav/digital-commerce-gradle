package com.digital.commerce.services.servlet;

import java.util.List;
import java.util.Locale;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.multisite.Site;
import atg.multisite.SiteContext;
import atg.multisite.SiteContextManager;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.RequestLocale;
import atg.userprofiling.ProfileRequestLocale;

/**
 * The extensions to ootb RequestLocale.
 *
 * @author ATG
 * 
*/
@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalRequestLocale extends ProfileRequestLocale {

  

  //----------------------------------------------------------------------------
  // STATIC
  //----------------------------------------------------------------------------
  
  private static final String LANG_SELECTION_PARAMETER = "locale";
  private static final String PROFILE_LOCALE_UNSET_VALUE = "unset";
  private static final String LANGUAGES_ATTRIBUTE_NAME = "languages";
  
  /**
   * Site's default country attribute name .
   */
  private static final String DEFAULT_COUNTRY_ATTRIBUTE_NAME = "defaultCountry";
  
  /**
   * Site's default language attribute name .
   */
  private static final String DEFAULT_LANGUAGE_ATTRIBUTE_NAME = "defaultLanguage";

  private static final String USER_PREF_LANGUAGE_COOKIE_NAME = "userPrefLanguage";
  
  //----------------------------------------------------------------------------
  // PROPERTIES
  //----------------------------------------------------------------------------

  //-------------------------------
  // property: Logger
  private static ApplicationLogging mLogger =
    ClassLoggingFactory.getFactory().getLoggerForClass(DigitalRequestLocale.class);

  /**
   * @return ApplicationLogging object for logger.
   */
  private ApplicationLogging getLogger()  {
    return mLogger;
  }
  
  //----------------------------------------------------------------------------
  // METHODS
  //----------------------------------------------------------------------------

  /**
   * Obtains locale from http request.
   *
   * @param pRequest DynamoHttpServletRequest
   * @param pReqLocal Request locale
   * @return Locale object
   */
  public Locale discernRequestLocale(DynamoHttpServletRequest pRequest, RequestLocale pReqLocal) {
    if (getLogger().isLoggingDebug()){
      getLogger().logDebug("StoreRequestLocale:discernRequestLocale:Entry");
    }    

    Locale locale;

    if(getLogger().isLoggingDebug()){
      getLogger().logDebug("StoreRequestLocale:discernRequestLocale:initLocale from LangSelection");
    }

    locale = fillLocaleFromLangSelection(pRequest);
    
    if(locale==null) {
      if(getLogger().isLoggingDebug()){
        getLogger().logDebug("StoreRequestLocale:discernRequestLocale:initLocale from storeSelection");
      } 
      locale = fillLocaleFromStoreSelection(pRequest,pReqLocal);
    }
    
    if(locale==null) {
      if(getLogger().isLoggingDebug()){
         getLogger().logDebug("StoreRequestLocale:discernRequestLocale:initLocale from Profile");
      }  
       locale = fillLocaleFromProfile(pRequest,pReqLocal);
    }

    if(locale==null) {
      if(getLogger().isLoggingDebug()){
        getLogger().logDebug("StoreRequestLocale:discernRequestLocale:initLocale from UserPrefLang");
      }  
      locale = fillLocaleFromUserPrefLang(pRequest);
    }

    if(locale==null) {
      if(getLogger().isLoggingDebug()){
        getLogger().logDebug("StoreRequestLocale:discernRequestLocale:initLocale from super.discernRequestLocale(pRequest,pReqLocal");
      }  
      locale = super.discernRequestLocale(pRequest,pReqLocal);
    }

    if(getLogger().isLoggingDebug()) {
      getLogger().logDebug("StoreRequestLocale:discernRequestLocale:locale="+locale);
      getLogger().logDebug("StoreRequestLocale:discernRequestLocale:Exit");
    }

    return locale;
  }

  /**
   * @param pRequest DynamoHttpServletRequest object.
   * @return Locale based on language selection.
   */
  protected Locale fillLocaleFromLangSelection(DynamoHttpServletRequest pRequest) {

    if(getLogger().isLoggingDebug()){
      getLogger().logDebug("StoreRequestLocale:discernRequestLocale:fillLocaleFromLangSelection:Entry");
    }
    Locale locale = null;

    //langSelection parameter set by Affiliates and laguage dropdown
    String langSelection = pRequest.getParameter(LANG_SELECTION_PARAMETER);
    if(langSelection!=null && !langSelection.trim().equals("")){
      //init locale using langSelection
      //update/create persistent cookie "userPrefLang"
      //update profile.locale if different

      locale = RequestLocale.getCachedLocale(langSelection);
      /*
       *  Check if locale in request is applied for current site. 
       *  If not applied the method shouldn't return locale from
       *  request.
       */
      if (!langAppliedForSite(pRequest, locale)){
        locale =  null;
      }
    }
    if(getLogger().isLoggingDebug()) {
      getLogger().logDebug("StoreRequestLocale:fillLocaleFromLangSelection:locale="+locale);
      getLogger().logDebug("StoreRequestLocale:fillLocaleFromLangSelection:Exit");
    }    
    return locale;
  }

  /**
   * Check if language can be applied to current site
   * @param pRequest DynamoHttpServletRequest object
   * @param pLocal the request locale to  check
   * @return true if language can be applied to current site,
   *   false otherwise.
   */
  private boolean langAppliedForSite(DynamoHttpServletRequest pRequest,
      Locale pLocal) {    
    List<String> storeLanguages = null;
    Site site = null;
    SiteContext currentSiteContext = null;
    currentSiteContext = SiteContextManager.getCurrentSiteContext();
    boolean langAppliedForCurrentSite = false;
    
    if(currentSiteContext != null) {
      site = currentSiteContext.getSite();

      if(site != null) {
        // Get site languages
        storeLanguages = (List<String>)site.getPropertyValue(LANGUAGES_ATTRIBUTE_NAME);
        // Get language from locale
        String localeLang = pLocal.getLanguage();
        
        if(storeLanguages.contains(localeLang)){
          langAppliedForCurrentSite = true;
        }
      }      
    }
    return langAppliedForCurrentSite;
  }

  /**
   * @param pRequest DynamoHttpServletRequest object.
   * @param pReqLocal Request locale
   * @return Locale based on store selection.
   */
  protected Locale fillLocaleFromStoreSelection(DynamoHttpServletRequest pRequest
    ,RequestLocale pReqLocal) 
  {
    Locale locale = null;
    String  storeLocaleCode = getStoreLocaleCode(pRequest,pReqLocal);
    if(storeLocaleCode != null) {
      locale = RequestLocale.getCachedLocale(storeLocaleCode);
    }
    
    if(getLogger().isLoggingDebug()) {
      getLogger().logDebug("StoreRequestLocale:fillLocaleFromStoreSelection:locale="+locale);
    }
    return locale;
  }

  /**
   * Determines the locale code to use for the store. If the current user 
   * profile language is in the list of site languages then the locale code is 
   * constructed from the profile language & site default country code; 
   * otherwise if the user profile language is not in the list of site languages
   * then the locale code is constructed from the site default language & site 
   * default country code.
   *
   * @param pRequest DynamoHttpServletRequest object.
   * @param pReqLocal Request locale
   * @return Locale Code
   */
  private String getStoreLocaleCode(DynamoHttpServletRequest pRequest,
    RequestLocale pReqLocal) 
  {
    // Get the current SiteContext
    SiteContext currentSiteContext = SiteContextManager.getCurrentSiteContext();
    if(currentSiteContext != null) {
      
      // Get the current Site
      Site site = currentSiteContext.getSite();
      if(site != null) {
        
        // Default country from the Site
        String storeDefaultCountry = 
          (String)site.getPropertyValue(DEFAULT_COUNTRY_ATTRIBUTE_NAME);

        /*
         * Create the locale code from the profile language and the sites
         * default country code. If the selected profile language is supported
         * on this site.
         */
        Locale profileLocale = localeFromProfileAttribute(pRequest,pReqLocal);
        if(profileLocale != null) {
          // Languages supported by this store
          List storeLanguages = (List)site.getPropertyValue(LANGUAGES_ATTRIBUTE_NAME);
          String profileLanguage = profileLocale.getLanguage();          
          if (storeLanguages != null && storeLanguages.contains(profileLanguage)) {
            return profileLanguage + "_" + storeDefaultCountry;
          }
        }
        
        /*
         * Create the locale code from the sites default language and country 
         * code
         */
        String storeDefaultLanguage = (String)site.getPropertyValue(DEFAULT_LANGUAGE_ATTRIBUTE_NAME);
        if(!DigitalStringUtil.isEmpty(storeDefaultLanguage) 
        && !DigitalStringUtil.isEmpty(storeDefaultCountry)) 
        {
          return storeDefaultLanguage + "_" + storeDefaultCountry;         
        }    
      }
    }

    return null;
  }

  /**
   * @param pRequest DynamoHttpServletRequest object.
   * @return Locale based on user preffered language.
   */
  protected Locale fillLocaleFromUserPrefLang(DynamoHttpServletRequest pRequest) {

    if(getLogger().isLoggingDebug()) {
      getLogger().logDebug("StoreRequestLocale:discernRequestLocale:fillLocaleFromUserPrefLang:Entry");
    }
    Locale locale = null;

    String userPrefLang = pRequest.getCookieParameter(USER_PREF_LANGUAGE_COOKIE_NAME);
    if(userPrefLang!=null && !userPrefLang.trim().equals("")) {
      //init locale using userPrefLang
      //update profile.locale if different

      locale = RequestLocale.getCachedLocale(userPrefLang);
    }
    if(getLogger().isLoggingDebug()) {
      getLogger().logDebug("StoreRequestLocale:fillLocaleFromUserPrefLang:locale="+locale);
      getLogger().logDebug("StoreRequestLocale:fillLocaleFromUserPrefLang:Exit");
    }
    return locale;
  }

  /**
   * @param pRequest DynamoHttpServletRequest object.
   * @param pReqLocal RequestLocale object.
   * @return Locale based on profile.
   */
  protected Locale fillLocaleFromProfile(DynamoHttpServletRequest pRequest,RequestLocale pReqLocal) {

    if(getLogger().isLoggingDebug()){
      getLogger().logDebug("StoreRequestLocale:discernRequestLocale:fillLocaleFromProfile:Entry");
    }
    Locale locale = null;

    //Retrieve from Profile
    Locale profileLocale = localeFromProfileAttribute(pRequest,pReqLocal);
    if(profileLocale!=null && !PROFILE_LOCALE_UNSET_VALUE.equals(profileLocale.toString())) {
      locale = RequestLocale.getCachedLocale(profileLocale.toString());
    }

    if(getLogger().isLoggingDebug()) {
      getLogger().logDebug("StoreRequestLocale:fillLocaleFromProfile:locale="+locale);
      getLogger().logDebug("StoreRequestLocale:fillLocaleFromProfile:Exit");
    }
    return locale;
  }
}

