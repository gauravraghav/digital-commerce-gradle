package com.digital.commerce.services.filter.bean;

import java.util.Map;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;

/**
 * A Property customizer used to return a date property in a localized format. The local is obtained
 * from the current request. The dateFormatter component is used to format dates.
 *
 * @author
 */
public class DigitalPromotionTypePropertyCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {
	
	public DigitalPromotionTypePropertyCustomizer() {
		super(DigitalPromotionTypePropertyCustomizer.class.getName());
	}
	
	enum PromotionType { ITEM_DISCOUNT, ORDER_DISCOUNT, SHIPPING_DISCOUNT }

	  //---------------------------------------------------------------------------
	  // PROPERTIES
	  //---------------------------------------------------------------------------
	
	  //-----------------------------------
	  // property: dateFormatter
	  //-----------------------------------
	
	/*
	 * 
	 * <option value="Item Discount" code="0"/>
	    <option value="Item Discount - Percent Off" code="1"/>
	    <option value="Item Discount - Amount Off" code="2"/>
	    <option value="Item Discount - Fixed Price" code="3"/>
	    <option value="Shipping Discount" code="5"/>
	    <option value="Shipping Discount - Percent Off" code="6"/>
	    <option value="Shipping Discount - Amount Off" code="7"/>
	    <option value="Shipping Discount - Fixed Price" code="8"/>
	    <option value="Order Discount" code="9"/>
	    <option value="Order Discount - Percent Off" code="10"/>
	    <option value="Order Discount - Amount Off" code="11"/>
	    <option value="Order Discount - Fixed Price" code="12"/>
    *
    */
	
	
	private Object getPromotionTypeString(Integer type){
		if(type.intValue() == 0 || type.intValue() == 1 || type.intValue() == 2 || type.intValue() == 3){
			return PromotionType.ITEM_DISCOUNT;
		}

		if(type.intValue() == 5 || type.intValue() == 6 || type.intValue() == 7 || type.intValue() == 8){
			return PromotionType.SHIPPING_DISCOUNT;
		}		

		if(type.intValue() == 9 || type.intValue() == 10 || type.intValue() == 11 || type.intValue() == 12){
			return PromotionType.ORDER_DISCOUNT;
		}
		return type;		
	}
  

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Returns a Promotion Type.
   *
   * @param pTargetObject
   *   The object which the specified property is associated with.
   * @param pPropertyName
   *   The name of the property to return.
   * @param pAttributes
   *   The key/value pair attributes defined in the beanFilteringConfiguration.xml file
   *   for this property.
   *
   * @return
   *   A promotion type.
   *
   * @throws BeanFilterException
   */
  @Override
  public Object getPropertyValue(Object pTargetObject, String pPropertyName,
    Map<String,String> pAttributes) throws BeanFilterException {

    // Get property type code value that needs to be mapped.
    Object propValue = null;

    try {
      propValue = DynamicBeans.getPropertyValue(pTargetObject, pPropertyName);
    }
    catch (PropertyNotFoundException e) {
      throw new BeanFilterException(e);
    }

    // Make sure it's not null.
    if (propValue == null) {
      vlogDebug("Property {0} was not a valid promotion type: {1}", pPropertyName, propValue);
      return null;
    }
    
    return getPromotionTypeString((Integer) propValue);
  }
}
