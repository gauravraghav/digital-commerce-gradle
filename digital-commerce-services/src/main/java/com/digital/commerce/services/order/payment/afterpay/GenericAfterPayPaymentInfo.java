package com.digital.commerce.services.order.payment.afterpay;

import atg.commerce.order.Order;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenericAfterPayPaymentInfo implements AfterPayPaymentInfo {
    private static final long	serialVersionUID	= 1L;
    private Order order;
    private String token;
}
