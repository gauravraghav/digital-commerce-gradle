package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RewardsViewCertificatePointHistoryRequest {
	private String memberId;
	private String startDate;
	private String endDate;
}
