package com.digital.commerce.services.order.payment.paypal;

import java.io.Serializable;

import atg.commerce.order.Order;

/** Paypal payment info interface
 * 
 * */
public interface PaypalPaymentInfo extends Serializable {

	public abstract String getPaypalPayerId();

	public abstract String getPaypalEmail();

	public abstract String getCardinalOrderId();

	public abstract Order getOrder();

	public abstract double getAmount();

	public abstract boolean isNewUser();
	
	public abstract String getIpAddress();

}
