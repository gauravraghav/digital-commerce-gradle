package com.digital.commerce.services.order.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import atg.repository.RepositoryItem;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.services.order.DigitalOrderHistoryItem;
import com.digital.commerce.services.order.DigitalOrderHistoryModel;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.yantra.webservices.beans.order.omni.OmniOrderUpdate;
import com.yantra.webservices.beans.order.omni.OmniOrderUpdate.Order.OrderLines.OrderLine.OrderStatuses.OrderStatus;

public class DigitalOrderStatusUtil {
	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalOrderStatusUtil.class);
	
	public static String getProductId(String productStyle){
		return DigitalStringUtil.stripStart(productStyle,"0");
	}
	
	/**
	 * Transform Yantra response to common order object
	 * @param order
	 * @return
	 */
	public static DigitalOrderHistoryModel convertToOrderHistory(OrderStatusPayload osOrder, DigitalOrderImpl atgOrder, RepositoryItem profile) {
		DigitalOrderHistoryModel detail = new DigitalOrderHistoryModel();
		OmniOrderUpdate omniOrderUpdate = osOrder.getOmniOrderUpdate();
		if(omniOrderUpdate == null){
			logger.error( "Missing OmniOrderUpdate element in OrderStatus payload for CUSTOMER_PICKUP_CONFIRMED event :: " + osOrder.toString());
			return detail;
		}
		com.yantra.webservices.beans.order.omni.OmniOrderUpdate.Order order = osOrder.getOmniOrderUpdate().getOrder();
		
		if( order.getOrderDate() != null) {
			XMLGregorianCalendar oDate = order.getOrderDate();
			Date date = oDate.toGregorianCalendar().getTime();
			detail.setOrderPlacementDate(date);
		}

		String demandLocation = order.getSellerOrganizationCode();
		detail.setDemandLocation(demandLocation);
		
		//detail.setAddDate( Calendar.getInstance().getTime());
		detail.setOrderId(order.getOrderNo());
		detail.setEmailId(order.getCustomerEMailID());
		//detail.setOrderId(order.getOmniOrderUpdate().getOrder().getOrderNo());
		if (order.getExtn() != null && DigitalStringUtil.isNotBlank(order.getExtn().getExtnLoyaltyId())) {
			detail.setLoyalNumber(order.getExtn().getExtnLoyaltyId());
		}else{
			if(profile != null){
				if (DigitalStringUtil.isNotBlank((String)profile.getPropertyValue("loyaltyNumber"))) {
					detail.setLoyalNumber((String)profile.getPropertyValue("loyaltyNumber"));
				}
				detail.setProfileId(profile.getRepositoryId());
			}
		}
		
		if(DigitalStringUtil.isNotBlank(order.getOrderType())){
			detail.setSiteId(order.getOrderType());
		}

		if(order.getExtn() != null && DigitalStringUtil.isNotBlank(order.getExtn().getExtnLocale())){
			detail.setLocale(order.getExtn().getExtnLocale());
		}
		if(order.getOverallTotals() != null &&
				DigitalStringUtil.isNotBlank(order.getOverallTotals().getGrandTotal())){
			String total = order.getOverallTotals().getGrandTotal();
			detail.setTotal(Double.valueOf(total));
		}

		if(order.getOrderLines() != null) {
			List<DigitalOrderHistoryItem> items = new ArrayList<>(order.getOrderLines().getOrderLine().size());
			for(OmniOrderUpdate.Order.OrderLines.OrderLine line : order.getOrderLines().getOrderLine()){
				DigitalOrderHistoryItem item = new DigitalOrderHistoryItem();

				if(line.getOrderStatuses() != null && line.getOrderStatuses().getOrderStatus() != null ){
					OrderStatus orderStatus = line.getOrderStatuses().getOrderStatus();
					item.setStatus(orderStatus.getStatus());
					if( orderStatus.getStatusDate() != null ) {
						XMLGregorianCalendar oDate = orderStatus.getStatusDate();
						Date date = oDate.toGregorianCalendar().getTime();
						item.setStatusDate(date);
						detail.setStatusDate(date);
					}
					
					if(DigitalStringUtil.isNotBlank(orderStatus.getStatusQty())){
						item.setQuantity((int)Double.parseDouble(orderStatus.getStatusQty()));
					}
					if(DigitalStringUtil.isNotBlank(line.getFulfillmentType()) && 
							("BOSTS".equalsIgnoreCase(line.getFulfillmentType()) || "BOPIS".equalsIgnoreCase(line.getFulfillmentType()))){
						item.setPickupLocationId(orderStatus.getShipNode());
						item.setFulfillLocationId(orderStatus.getTOShipNode());
					}else{
						item.setFulfillLocationId(orderStatus.getShipNode());
					}
					
					if(DigitalStringUtil.isNotBlank(orderStatus.getTrackingNo())){
						item.setTrackingNumber(orderStatus.getTrackingNo());
					}
					
					if(DigitalStringUtil.isNotBlank(orderStatus.getSCAC())){
						item.setCarrier(orderStatus.getSCAC().trim());
					}
				}
				
				if(line.getLineOverallTotals() != null && DigitalStringUtil.isNotBlank(line.getLineOverallTotals().getUnitPrice())){
					item.setUnitPrice(Double.valueOf(line.getLineOverallTotals().getUnitPrice()));
				}
				item.setYantraLineItemId(line.getOrderLineKey());
				item.setSkuId(line.getItem().getItemID());
				if(null != line.getPersonInfoShipTo()){
					item.setShipToFirstName(line.getPersonInfoShipTo().getFirstName());
					item.setShipToLastName(line.getPersonInfoShipTo().getLastName());
				}				
				if(DigitalStringUtil.isNotEmpty(line.getItem().getCustomerItem())){
					item.setProductId(line.getItem().getCustomerItem());
				}
				else{
					item.setProductId(DigitalOrderStatusUtil.getProductId(line.getExtn().getExtnItemStyle()));
				}
				item.setProductDescription(line.getExtn().getExtnItemName());
				
				item.setOrderPlacementDate(detail.getOrderPlacementDate());
				item.setAtgCommerceId(line.getItem().getAlternateItemID());
				item.setGcFromEmailAddress(line.getExtn().getExtnGiftCardSenderEmail());
				item.setGcFromName(line.getExtn().getExtnGiftCardFrom());
				item.setGcToEmailAddress(line.getExtn().getExtnGiftCardRecipientEmail());
				item.setGcToName(line.getExtn().getExtnGiftCardTo());
				
			    items.add(item);
			}
			detail.setCommerceItems(items);
			if(atgOrder != null){
				if(DigitalStringUtil.isBlank(detail.getLocale())){
					detail.setLocale(atgOrder.getLocale());
				}
				if(DigitalStringUtil.isBlank(detail.getSiteId())){
					detail.setSiteId(atgOrder.getSiteId());
				}
			}
		}
		return detail;
	}
}
