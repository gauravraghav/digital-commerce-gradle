/**
 *
 */
package com.digital.commerce.services.order.payment;

import static com.digital.commerce.common.services.DigitalBaseConstants.GIFT_CARD_APPROVAL_RESPONSE_CODE;

import com.digital.commerce.constants.DigitalAfterPayConstants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.PaypalConstants.PaypalPaymentGroupPropertyManager;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.DigitalCommerceItemManager;
import com.digital.commerce.services.order.DigitalCreditCard;
import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.payment.creditcard.DigitalCreditCardServiceStatus;
import com.digital.commerce.services.order.payment.giftcard.GiftCardPaymentServices;
import com.digital.commerce.services.order.payment.giftcard.valueobject.DigitalGiftCardApproval;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.profile.DigitalCommercePropertyManager;
import com.digital.commerce.services.utils.DigitalAddressUtil;
import com.google.common.collect.Iterables;

import atg.commerce.CommerceException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupManager;
import atg.commerce.order.PaymentGroupRelationship;
import atg.commerce.order.RelationshipTypes;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"unchecked","rawtypes"})
@Getter
@Setter
public class DigitalPaymentGroupManager extends PaymentGroupManager {
	private OrderManager orderManager;
	private GiftCardPaymentServices paymentServices;

	public String getCreditCardPaymentGroupType() {
		return DigitalCreditCardServiceStatus.CREDIT_CARD;
	}

	public String getGiftCardPaymentGroupType() {
		return "giftCard";
	}

	public String getPaypalPaymentGroupType() {
		return PaypalPaymentGroupPropertyManager.PAYPAL_PAYMENT.getValue();
	}

	public String getAfterPayPaymentGroupType() {
		return DigitalAfterPayConstants.AFTERPAY_PAYMENT_TYPE;
	}

	/**
	 * Returns the first {@link CreditCard} on the order.
	 * 
	 * @param order
	 * @return
	 */
	public DigitalCreditCard findCreditCard(Order order) {
		DigitalCreditCard retVal = null;
		if (order != null) {
			final Iterator<DigitalCreditCard> filtered = Iterables.filter(order.getPaymentGroups(), DigitalCreditCard.class)
					.iterator();
			retVal = filtered.hasNext() ? filtered.next() : null;
		}
		return retVal;
	}
	
	/**
	 * Returns the {@link GiftCards} on the order.
	 * 
	 * @param order
	 * @return
	 */
	public List<DigitalGiftCard> findGiftCards(Order order) {
		List<DigitalGiftCard> retVal = new ArrayList<>();
		if (order != null) {
			final Iterator<DigitalGiftCard> filtered = Iterables.filter(order.getPaymentGroups(), DigitalGiftCard.class)
					.iterator();
			while (filtered.hasNext()) {
				retVal.add(filtered.next());
			}
		}
		return retVal;
	}

	/**
	 * Returns the first {@link PaypalPayment} on the order.
	 * 
	 * @param order
	 * @return
	 */
	public PaypalPayment findPaypalPayment(Order order) {
		PaypalPayment retVal = null;
		if (order != null) {
			final Iterator<PaypalPayment> filtered = Iterables.filter(order.getPaymentGroups(), PaypalPayment.class)
					.iterator();
			retVal = filtered.hasNext() ? filtered.next() : null;
		}
		return retVal;
	}

	public double getCreditCardPaymentAmount(Order order) {
		DigitalCreditCard creditCard = findCreditCard(order);
		return creditCard != null ? creditCard.getAmount() : 0.0;
	}

	public double getAmountRemaining(Order order, double startingTotal) {

		List pgrels = order.getPaymentGroupRelationships();
		for (int x = 0; x < pgrels.size(); x++) {
			PaymentGroupRelationship rel = (PaymentGroupRelationship) pgrels.get(x);

			if (rel.getRelationshipType() != RelationshipTypes.PAYMENTAMOUNTREMAINING
					&& !getCreditCardPaymentGroupType().equals(rel.getPaymentGroup().getPaymentGroupClassType())
					&& !getPaypalPaymentGroupType().equals(rel.getPaymentGroup().getPaymentGroupClassType())
					&& !getAfterPayPaymentGroupType().equals(rel.getPaymentGroup().getPaymentGroupClassType())) {
				if (startingTotal - rel.getAmount() > 0) {
					startingTotal -= rel.getAmount();
				} else {
					startingTotal = 0;
					break;
				}

			}
		}
		return startingTotal;
	}

	/**
	 * 
	 */
	public DigitalCreditCard createCreditCardFromProfile(Order order, Profile profile) {
		DigitalCreditCard creditCard = findCreditCard(order);
		if (creditCard == null) {
			try {
				creditCard = (DigitalCreditCard) this.createPaymentGroup();
				order.addPaymentGroup(creditCard);
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError("Exception occured while creating payment group from the profile", e);
				}
			}
		}
		if (creditCard != null) {
			RepositoryItem billingAddress = (RepositoryItem) profile
					.getPropertyValue(((DigitalCommercePropertyManager) profile.getProfileTools().getPropertyManager())
							.getBillingAddressPropertyName());
			if (billingAddress != null) {
				try {
					assignIndividualCreditCard(order, creditCard);
					OrderTools.copyAddress(billingAddress, creditCard.getBillingAddress());
				} catch (CommerceException cex) {
					if (isLoggingError()) {
						logError("Error occured while copyping profile billing address to credit card billing address",
								cex);
					}
				}
			}
		}
		return creditCard;
	}
	
	/**
	 * 
	 */
	public DigitalCreditCard createCreditCardFromProfileNoRelationship(Order order, Profile profile) {
		DigitalCreditCard creditCard = findCreditCard(order);
		if (creditCard == null) {
			try {
				creditCard = (DigitalCreditCard) this.createPaymentGroup();
				order.addPaymentGroup(creditCard);
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError("Exception occured while creating payment group from the profile", e);
				}
			}
		}
		if (creditCard != null) {
			RepositoryItem billingAddress = (RepositoryItem) profile
					.getPropertyValue(((DigitalCommercePropertyManager) profile.getProfileTools().getPropertyManager())
							.getBillingAddressPropertyName());
			if (billingAddress != null) {
				try {
					OrderTools.copyAddress(billingAddress, creditCard.getBillingAddress());
				} catch (CommerceException cex) {
					if (isLoggingError()) {
						logError("Error occured while copyping profile billing address to credit card billing address",
								cex);
					}
				}
			}
		}
		return creditCard;
	}

	/**
	 * @param order
	 * @return
	 */
	protected PaymentGroup getPaymentGroup(Order order) {
		PaymentGroup pg = null;
		if (order.getPaymentGroups() != null && order.getPaymentGroups().size() > 0) {
			pg = (PaymentGroup) order.getPaymentGroups().get(0);

		} else {
			try {
				order.addPaymentGroup(this.createPaymentGroup());
				pg = findCreditCard(order);
			} catch (CommerceException cex) {
				// TODO exception handling
				if (isLoggingError()) {
					logError("Exception occured while creating payment group", cex);
				}
				return null;
			}
		}
		return pg;
	}

	public List getNonCreditCardPaymentGroups(Order order) {
		List pgList = new LinkedList();
		List curPgs = order.getPaymentGroups();
		for (int x = 0; x < curPgs.size(); x++) {
			PaymentGroup pg = (PaymentGroup) curPgs.get(x);
			if (!getCreditCardPaymentGroupType().equals(pg.getPaymentGroupClassType())) {
				pgList.add(pg);
			}
		}
		return pgList;
	}

	public List getGiftCardPaymentGroups(Order order) {
		List pgList = new LinkedList();
		List curPgs = order.getPaymentGroups();
		for (int x = 0; x < curPgs.size(); x++) {
			PaymentGroup pg = (PaymentGroup) curPgs.get(x);
			if (getGiftCardPaymentGroupType().equals(pg.getPaymentGroupClassType())) {
				pgList.add(pg);
			}
		}
		return pgList;
	}

	/**
	 * Figures out if the gift card is already represented in this order.
	 * 
	 * @param order
	 * @param cardNumber
	 * @return
	 */
	public boolean isGiftCardInOrder(Order order, String cardNumber) {
		boolean retVal = false;
		for (DigitalGiftCard giftCard : Iterables.filter(order.getPaymentGroups(), DigitalGiftCard.class)) {
			if (giftCard.getCardNumber().equals(cardNumber)) {
				retVal = true;
			}
		}
		return retVal;
	}

	public DigitalGiftCardApproval getGiftCardBalance(String cardNumber, String cardPin, String currency)
			throws DigitalAppException {
		return getPaymentServices().getGiftCardBalance(cardNumber, cardPin, currency);
	}

	public DigitalGiftCard addGiftCardPaymentGroup(Order order, String giftCardNumber, String giftCardPin,
			DigitalGiftCardApproval cardApproval) throws CommerceException, DigitalAppException {
		if (cardApproval.isApprovalFlag() && cardApproval.getApprovalAmount() > 0) {
			double amountToAdd = cardApproval.getApprovalAmount();
			double amountRemaining = this.getAmountRemaining(order, order.getPriceInfo().getTotal());
			if (cardApproval.getApprovalAmount() > amountRemaining) {
				amountToAdd = amountRemaining;
			}
			if (amountToAdd > 0) {
				DigitalOrderTools orderTools = (DigitalOrderTools) getOrderManager().getOrderTools();
				PaymentGroup pg = orderTools.createPaymentGroup(orderTools.getGiftCardPaymentGroupName());
				if (pg instanceof DigitalGiftCard) {
					DigitalGiftCard giftCard = (DigitalGiftCard) pg;
					giftCard.setCardNumber(giftCardNumber);
					giftCard.setPinNumber(giftCardPin);
					giftCard.setAmount(amountToAdd);
					addPaymentGroupToOrder(order, giftCard);
					getOrderManager().addOrderAmountToPaymentGroup(order, giftCard.getId(), amountToAdd);
					return giftCard;
				} else {
					if (isLoggingError()) {
						logError("Unable to create giftcard payment group");
					}
					throw new CommerceException();
				}
			} else {
				throw new DigitalAppException("giftcard.nothing.toapply");
			}
		} else {
			throw new CommerceException();
		}
	}

	public void assignIndividualCreditCard(Order order, CreditCard creditCard) throws CommerceException {
		creditCard.setAmount(order.getPriceInfo().getTotal());
		removeAllPaymentGroupsFromOrder(order, getGiftCardPaymentGroups(order));
		addPaymentGroupToOrder(order, creditCard);
		getOrderManager().addRemainingOrderAmountToPaymentGroup(order, creditCard.getId());
	}

	public void removeGiftCardFromOrder(Order pOrder, String giftCardNumber,
			Boolean exists) throws RepositoryException, CommerceException,
			DigitalAppException {
		DigitalOrderImpl dswOrder = (DigitalOrderImpl) pOrder;
		List paymentGroupList = dswOrder.getPaymentGroups();
		List pgList = new LinkedList();
		if (paymentGroupList != null) {
			Iterator i = paymentGroupList.iterator();
			while (i.hasNext()) {
				PaymentGroup pg = (PaymentGroup) i.next();
				if (pg instanceof DigitalGiftCard) {
					DigitalGiftCard gc = (DigitalGiftCard) pg;	
					if(DigitalStringUtil.isNotEmpty(giftCardNumber)){
						if (!giftCardNumber.equals(gc.getCardNumber())) {
							pgList.add(pg);
						} else if (giftCardNumber.equals(gc.getCardNumber())
								&& true == exists) {
							// already exists scenario - if new gc already applied
							throw new DigitalAppException("giftcard.alreadyapplied");
						}
					}
				} else {
					pgList.add(pg);
				}

			}
			if(paymentGroupList.size() == pgList.size() && !exists){
				throw new DigitalAppException("giftcard.notapplied");
			}
			// Remove all payment group except from the pgList
			if (pgList != null) {
				removeAllPaymentGroupsFromOrder(
						dswOrder, pgList);
			}
		}
		

	}

	public void reapplyGiftCard(Order order){

		List<DigitalGiftCard> giftcards = findGiftCards(order);
		for (DigitalGiftCard giftcard : giftcards) {
			reapplyGiftcard(order, giftcard.getCardNumber(), giftcard.getPinNumber(), giftcard.getCurrencyCode());
		}
	}

	/**
	 *
	 * @param cardNumber
	 * @param pin
	 * @param currencyName
	 */
	public void reapplyGiftcard(Order order, String cardNumber, String pin, String currencyName) {
		DigitalGiftCardApproval approval = new DigitalGiftCardApproval();
		try {
			// form handlers
			synchronized (order) {
				removeGiftCardFromOrder(order, cardNumber, false);
				recalculatePaymentGroupAmounts(order);
				getOrderManager().updateOrder(order);
				try {
					approval = getPaymentServices().getGiftCardBalance(cardNumber, pin, currencyName);
				} catch (DigitalIntegrationException e) {
					logWarning("Error while retrieving SVS balance inquiry", e);
				}
				if (DigitalStringUtil.isNotBlank(approval.getResponseCode())
						&& GIFT_CARD_APPROVAL_RESPONSE_CODE == Integer.parseInt(approval.getResponseCode())) {

					DigitalGiftCard giftCardPaymentGroup = addGiftCardPaymentGroup(order, cardNumber, pin, approval);
					if (null != giftCardPaymentGroup) {
						recalculatePaymentGroupAmounts(order);
						// update first/last name of shipping group for
						// bopis/bosts
						// only orders from alternate pick-up
						((DigitalShippingGroupManager)this.getOrderManager().getShippingGroupManager()).updateShipAddrNameForBopisBostsOrder(order);

						getOrderManager().updateOrder(order);
					}
				} else {
					logWarning("SVS : "  + approval.getResponseCode());
				}
			}
		} catch (DigitalAppException e) {
			logError(e.getMessage(), e);
		} catch (RepositoryException | CommerceException e) {
			logError(e.getMessage(), e);
		}
	}

	/**
	 * This method is called from ProcValidatePaymentGroupsForCheckout in validateForCheckout pipeline, which will invoke validatePaymentGroup ( includes calling ValidateCreditCard )
	 * to validate credit card or any other payments if the below isPaymentGroupUsed method returns true. We return false here for order's with free shipping to tax free state and 
	 * rest of the order total being paid with reward certificate, essentially with zero dollar order.
	 */
	public boolean isPaymentGroupUsed(Order pOrder, PaymentGroup pPaymentGroup){
		DigitalCommerceItemManager cim = (DigitalCommerceItemManager)this.getOrderManager().getCommerceItemManager();
		if( !( pOrder.getPriceInfo().getTotal() > 0.00) 
				&& pOrder.getCommerceItemCount() > 0 
				&& !cim.hasGiftCartItem(pOrder)){
			return false;
		}else{
			return super.isPaymentGroupUsed(pOrder, pPaymentGroup);
		}
	}
	
	/**
	 * This method is called from ProcRemoveEmptyPaymentGroup from processOrder pipeline.
	 * We are overriding this method so that we remove the empty payment group for order 
	 * with free shipping to tax free state and applying reward certificate for the rest of the order amount.
	 * This will result in zero dollar amount to be paid or all the amount is paid with reward certificate.
	 * 
	 * The OOTB implementation doesn't remove the payment group is there is only one payment group. 
	 * This override will remove the payment group for reward certificate as tender resulting in zero dollar order.
	 */
	public void removeEmptyPaymentGroups(Order pOrder) throws CommerceException {
		DigitalCommerceItemManager cim = (DigitalCommerceItemManager)this.getOrderManager().getCommerceItemManager();
		if( !( pOrder.getPriceInfo().getTotal() > 0.00) 
				&& pOrder.getCommerceItemCount() > 0 
				&& !cim.hasGiftCartItem(pOrder) && pOrder.getPaymentGroupCount() > 0){
			try {
				/*Getting billing address containing email  and  phone number set by 
				 * addorUpdateOrderContactInfo  service earlier in the flow 
				*/
				DigitalContactInfo tmpOrderContactInfo = getBillingAddressFromOrder(pOrder);
				DigitalShippingGroupManager shm = (DigitalShippingGroupManager)this.getOrderManager().getShippingGroupManager();
				shm.setHgShippingAddress(tmpOrderContactInfo,pOrder);
			} catch (DigitalAppException e) {
				if( isLoggingError() ) {
					logError( "Exception while attempting to get billing address from order",e );
				}
			}			
			
			List paymentGroups = pOrder.getPaymentGroups();
			ArrayList removalList = new ArrayList(paymentGroups.size());
	
			Iterator iter = paymentGroups.iterator();
			while (iter.hasNext()) {
				PaymentGroup group = (PaymentGroup) iter.next();
				if ((group.getCommerceItemRelationshipCount() == 0)
						&& (group.getOrderRelationshipCount() == 0)
						&& (group.getShippingGroupRelationshipCount() == 0)) {
	
					removalList.add(group.getId());
				}
			}
			for (int i = 0; i < removalList.size(); i++) {
				removePaymentGroupFromOrder(pOrder, (String) removalList.get(i));
			}
			
			return;
		}else{
			super.removeEmptyPaymentGroups(pOrder);
		}
	}	
	
	public DigitalContactInfo getBillingAddressFromOrder(Order order)throws DigitalAppException{
		if(isLoggingDebug()){
			logDebug("getBillingAddress:: -Start");			
		}
		DigitalRepositoryContactInfo billingAddr = null;
		List<PaymentGroup> paymentGroupList = order.getPaymentGroups();
		if( paymentGroupList != null ) {
			for( PaymentGroup paymentGroup : paymentGroupList ) {
				if (paymentGroup instanceof CreditCard) {
					//get the payment group if its exists
					CreditCard creditCard = (CreditCard) paymentGroup;
					billingAddr = (DigitalRepositoryContactInfo)creditCard.getBillingAddress();
					
					
				}else if( paymentGroup instanceof PaypalPayment ) {
					PaypalPayment paypalPayment = (PaypalPayment)paymentGroup;
					billingAddr = (DigitalRepositoryContactInfo)paypalPayment.getBillingAddress();
				}	
			}
		}
		if(isLoggingDebug()){
			if(billingAddr != null){
				logDebug("getBillingAddress:: -billingAddr" +billingAddr);
			}else{
				logDebug("getBillingAddress:: -billingAddr is null");
			}
			logDebug("getBillingAddress:: -End");			
		}
		return DigitalAddressUtil.getDSWContactInfo(billingAddr);
	}


	/**
	 * Method to delete non gift card payment groups for afterPay payment
	 * @param pOrder
	 * @throws RepositoryException
	 * @throws CommerceException
	 * @throws DigitalAppException
	 */
		public void removeNonGiftCardPaymentGroups(Order pOrder) throws CommerceException {

			List<DigitalGiftCard> creditCards = findGiftCards(pOrder);
			if (null != creditCards) {
				// Remove all payment group except GiftCard
				this.removeAllPaymentGroupsFromOrder(pOrder, creditCards);
			}
		}

}
