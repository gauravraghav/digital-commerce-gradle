
package com.digital.commerce.services.order.history.domain;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
@Getter
@Setter
@ToString
public class OrderHistory {

    private List<Order> orders = new ArrayList<Order>();

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(orders).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof OrderHistory) == false) {
            return false;
        }
        OrderHistory rhs = ((OrderHistory) other);
        return new EqualsBuilder().append(orders, rhs.orders).isEquals();
    }

}
