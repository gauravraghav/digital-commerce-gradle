package com.digital.commerce.services.communication;

import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.integration.common.communication.CommunicationService;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.services.communication.domain.SendEmailRequest;
import com.digital.commerce.services.communication.domain.SendEmailResponse;

import atg.nucleus.logging.ApplicationLoggingImpl;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalEmailComponent extends ApplicationLoggingImpl {
	private static final String CLASSNAME = "DigitalEmailComponent";

	private CommunicationService emailService;
	
	public DigitalEmailComponent() {
		super(DigitalEmailComponent.class.getName());
	}
	
	public SendEmailResponse sendEmail(SendEmailRequest sendEmailRequest) {
		String METHOD_NAME = "SEND_EMAIL";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		SendEmailResponse response = new SendEmailResponse();

		try {
			response.setRequestSuccess(emailService.send(sendEmailRequest.getSender(), sendEmailRequest.getRecipients(),
					sendEmailRequest.getSubject(), sendEmailRequest.getSubjectDataElements(), sendEmailRequest.getTemplateKey(), sendEmailRequest.getDataElements(),
					sendEmailRequest.getContentType(), MultiSiteUtil.getSite().getId()));
			response.setRequestSuccess(true);
			response.setMessage("Email queued for sending");
			
		} catch (DigitalIntegrationException e) {
			logError("Error while sending email", e);

			response.getGenericExceptions().add(new ResponseError(e.getErrorCode(), e.getLocalizedMessage()));
			response.setRequestSuccess(false);

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return response;
	}
}
