package com.digital.commerce.services.order.purchase;

import static com.digital.commerce.constants.DigitalProfileConstants.MIL_ADDRESS_TYPE;
import static com.digital.commerce.constants.DigitalProfileConstants.USA_ADDRESS_TYPE;
import static com.digital.commerce.constants.DigitalProfileConstants.USPS_STATUS_ACCEPTED;
import static com.digital.commerce.constants.DigitalProfileConstants.USPS_STATUS_ERROR;
import static com.digital.commerce.constants.DigitalProfileConstants.USPS_STATUS_SUGGESTED;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.SystemException;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.HttpServletUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceResponse;
import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceResponse.AddressValidationDecision;
import com.digital.commerce.services.common.DigitalAddress;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.pricing.DigitalPricingTools;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.responses.ShippingAddressServiceResponse;
import com.digital.commerce.services.utils.DigitalAddressUtil;
import com.digital.commerce.services.validator.DigitalBasicAddressValidator;
import com.digital.commerce.services.validator.DigitalUSPSAddressValidator;

import atg.commerce.CommerceException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.RepositoryContactInfo;
import atg.commerce.order.purchase.CreateHardgoodShippingGroupFormHandler;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.commerce.profile.CommercePropertyManager;
import atg.core.util.Address;
import atg.droplet.DropletException;
import atg.nucleus.naming.ComponentName;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * Extension for HardGood Shipping
 * 
 * @author ad402865
 */
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalCreateHardgoodShippingGroupFormHandler extends
		CreateHardgoodShippingGroupFormHandler {

	private DigitalUSPSAddressValidator uspsAddressValidator;
	private String decision;
	private String customerDecision;
	private DigitalContactInfo address = new DigitalContactInfo();
	private DigitalContactInfo suggestedAddress;
	private MessageLocator messageLocator;
	private DigitalBasicAddressValidator dswBasicAddressValidator;
	private ShippingAddressServiceResponse shippingAddressServiceResponse;
	private HardgoodShippingGroup hardgoodShippingGroupTemp;
	private DigitalProfileTools dswProfileTools;
	private Boolean saveToProfile = false;
	private Boolean saveAsPrimary = false;
	private CommercePropertyManager propertyManager;
	private Boolean profilePage = false;
	private ComponentName userPricingModelsPath;
	private PricingTools pricingTools;
	private Integer shippingAddressMaxLimit;
	
	/**
	 * Validate Shipping Group
	 */
	public void validateShippingGroup() {
		final String METHOD_NAME = "validateShippingGroup";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			if (shippingAddressServiceResponse == null) {
				shippingAddressServiceResponse = new ShippingAddressServiceResponse();
			}
			/**
			 * Disabling OOB Validation to use DSW specific Basic Validation
			 **/
			// super.validateShippingGroup();
			RepositoryContactInfo shippingAddress = null;
			if (!profilePage) {
				shippingAddress = (DigitalRepositoryContactInfo) getHardgoodShippingGroup().getShippingAddress();
				initializeShippingAddress((DigitalRepositoryContactInfo) shippingAddress);
			} else {
				shippingAddressServiceResponse.setAddress(address);
			}
			Collection<String> dswShippingAddressValidationErrors = dswBasicAddressValidator
					.validateShippingAddress(address);
			if ((dswShippingAddressValidationErrors != null) && (!dswShippingAddressValidationErrors.isEmpty())) {
				try {
					if (isLoggingDebug()) {
						logDebug("dsw basic address validation failed with errors : "
								+ dswShippingAddressValidationErrors.toString());
					}
					for (Object entry : dswShippingAddressValidationErrors) {
						String errorCode = (String) entry;
						this.addFormException(
								new DropletException(this.getMessageLocator().getMessageString(errorCode), errorCode));
					}
				} catch (Exception exception) {
				}
				return;
			}
			Collection<String> dswValidationErrors = dswBasicAddressValidator.validateAddress(address);
			if ((dswValidationErrors != null) && (!dswValidationErrors.isEmpty())) {
				try {
					if (isLoggingDebug()) {
						logDebug("dsw basic address validation failed with errors : " + dswValidationErrors.toString());
					}
					for (Object entry : dswValidationErrors) {
						String errorCode = (String) entry;
						this.addFormException(
								new DropletException(this.getMessageLocator().getMessageString(errorCode), errorCode));
					}
				} catch (Exception exception) {
				}
				return;
			}
			if (DigitalStringUtil.isBlank(customerDecision)
					&& USA_ADDRESS_TYPE.equalsIgnoreCase(address.getAddressType())) {
				AddressValidationServiceResponse addressValidationServiceResponse = null;
				AddressValidationDecision addressValidationDecision = null;
				try {
					// USPS Service Call
					addressValidationServiceResponse = getUspsAddressValidator().validateAddress(address);
					addressValidationDecision = addressValidationServiceResponse.getAddressValidationDecision();
					if (addressValidationDecision != null) {
						decision = addressValidationDecision.getDescription();
						shippingAddressServiceResponse.setDecision(decision);
					}
				} catch (DigitalIntegrationException be) {
					decision = USPS_STATUS_ERROR;
					address.setAddressVerification(false);
					if (isLoggingDebug()) {
						logDebug("usps address validation failed with errors : " + be.toString());
					}
				}
				if (USPS_STATUS_ERROR.equalsIgnoreCase(decision)) {
					try {
						this.addFormException(
								new DropletException(this.getMessageLocator().getMessageString("USPSNoMatch"), "USPSNoMatch"));
						return;
					} catch (Exception exception) {
					}
					return;
				}
				if (USPS_STATUS_SUGGESTED.equalsIgnoreCase(decision)) {
					suggestedAddress = getSuggestedAddress(addressValidationServiceResponse);
					shippingAddressServiceResponse.setSuggestedAddress(suggestedAddress);
					try {
						this.addFormException(new DropletException(
								this.getMessageLocator().getMessageString("AcceptAddressMessage"), "AcceptAddressMessage"));
						return;
					} catch (Exception exception) {
					}
					return;
				}
				if (USPS_STATUS_ACCEPTED.equalsIgnoreCase(decision)) {
					address.setAddressVerification(false);
				}

				if (null != addressValidationDecision
						&& !DigitalStringUtil.isBlank(addressValidationDecision.getDescription())
						&& USPS_STATUS_ACCEPTED.equalsIgnoreCase(addressValidationDecision.getDescription())) {
					address.setAddressVerification(true);
				}
			}
			if (MIL_ADDRESS_TYPE.equalsIgnoreCase(address.getAddressType())) {
				String city = address.getCity();
				String rank = address.getRank();
				if (DigitalStringUtil.isBlank(city) || DigitalStringUtil.isEmpty(city)) {
					address.setCity(rank);
				} else {
					address.setCity(city);
				}

				if (!DigitalStringUtil.isBlank(rank) && !DigitalStringUtil.isEmpty(rank)) {
					address.setRank(rank);
				} else {
					address.setRank(city);
				}

				address.setRegion(address.getRegion());
				address.setState("");
				address.setAddressVerification(false);
			}
			if (USPS_STATUS_SUGGESTED.equalsIgnoreCase(customerDecision)) {
				address.setAddressVerification(true);
			}
			if (DigitalStringUtil.isBlank(address.getAddress2()) || DigitalStringUtil.isEmpty(address.getAddress2())
					|| "null".equalsIgnoreCase(address.getAddress2())) {
				address.setAddress2("");
			}
			shippingAddressServiceResponse.setAddress(address);
			if (!getProfilePage()) {
				getHardgoodShippingGroup().setShippingAddress((Address) address);
			}
		} finally {
			// Mark the transaction rollback if there are any form
			// validation errors
			if (this.getFormError()) {
				try {
					this.setTransactionToRollbackOnly();
				} catch (SystemException e) {
					logError("Transaction rollback exception :: ", e);
				}
			}
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/**
	 * 
	 * @param shippingAddress
	 */
	private void initializeShippingAddress(
			DigitalRepositoryContactInfo shippingAddress) {
		address.setFirstName(shippingAddress.getFirstName());
		address.setLastName(shippingAddress.getLastName());
		address.setAddress1(shippingAddress.getAddress1());
		address.setAddress2(shippingAddress.getAddress2());
		address.setCity(shippingAddress.getCity());
		address.setCountry(shippingAddress.getCountry());
		address.setCounty(shippingAddress.getCounty());
		if (DigitalStringUtil.isNotEmpty(shippingAddress.getEmail())) {
			address.setEmail(DigitalStringUtil.lowerCase(shippingAddress.getEmail()));
		}
		address.setFaxNumber(shippingAddress.getFaxNumber());
		address.setPhoneNumber(shippingAddress.getPhoneNumber());
		address.setState(shippingAddress.getState());
		address.setPostalCode(shippingAddress.getPostalCode());
		address.setAddressType(shippingAddress.getAddressType());
		address.setAddressVerification(shippingAddress.getAddressVerification());
		if (MIL_ADDRESS_TYPE.equalsIgnoreCase(shippingAddress.getAddressType())) {
			address.setRegion(shippingAddress.getRegion());
			address.setRank(shippingAddress.getCity());
			address.setState("");
		} else {
			address.setRegion("");
			address.setRank("");
			address.setState(shippingAddress.getState());
			address.setCity(shippingAddress.getCity());
		}
		address.setIsPoBox(shippingAddress.getIsPoBox());
		address.setOwnerId(shippingAddress.getOwnerId());
		shippingAddressServiceResponse.setAddress(address);
	}

	/**
	 * 
	 * @param addressValidationServiceResponse
	 * @return
	 */
	public DigitalContactInfo getSuggestedAddress(
			AddressValidationServiceResponse addressValidationServiceResponse) {
		DigitalContactInfo suggestedContactInfo = new DigitalContactInfo();
		suggestedContactInfo.setAddress1(addressValidationServiceResponse
				.getSuggestedAddressLine1());
		suggestedContactInfo.setAddress2(addressValidationServiceResponse
				.getSuggestedAddressLine2());
		suggestedContactInfo.setCity(addressValidationServiceResponse
				.getSuggestedCity());
		suggestedContactInfo.setState(addressValidationServiceResponse
				.getSuggestedState());
		StringBuilder zipcode = new StringBuilder();
		if (!DigitalStringUtil.isBlank(addressValidationServiceResponse
				.getSuggestedZip5())) {
			zipcode.append(addressValidationServiceResponse.getSuggestedZip5());
		}
		if (!DigitalStringUtil.isBlank(addressValidationServiceResponse
				.getSuggestedZip4())) {
			zipcode.append("-"
					+ addressValidationServiceResponse.getSuggestedZip4());
		}
		suggestedContactInfo.setPostalCode(zipcode.toString());
		suggestedContactInfo.setAddressVerification(true);
		return suggestedContactInfo;
	}
	
	/**
	 * get the HG Shipping Group
	 */
	public HardgoodShippingGroup getHardgoodShippingGroup(){
		if(null == this.getHardgoodShippingGroupTemp()){
			Order order = this.getOrder();
			if(null != order){
				HardgoodShippingGroup hsg = this.getShippingGroupManager().getFirstNonGiftHardgoodShippingGroup(order);
				this.setHardgoodShippingGroup(hsg);
				this.setHardgoodShippingGroupTemp(hsg);
			}
		}
		return super.getHardgoodShippingGroup();
	}

	/**
	 *
	 * @return the saveAsPrimary
	 */
	public Boolean isSaveAsPrimary() {
		return this.getSaveAsPrimary();
	}

	/**
	 * 
	 * @return the saveAsPrimary
	 */
	public boolean getSaveAsPrimaryAddress() {
		Boolean flag = false;
		RepositoryItem profile = getProfile();
		if(isSaveAsPrimary() && getDswProfileTools().isLoggedIn(profile)){
			flag = true;
		}
	     return flag;
	}

	public Boolean isSaveToProfile() {
		return this.getSaveToProfile();
	}

	@Override
	/**
	 * 
	 * @return true of false
	 */
	public boolean isAddToProfile()	{
		Boolean flag = false;
		RepositoryItem profile = getProfile();
		if(isSaveToProfile() && getDswProfileTools().isLoggedIn(profile)){
			flag = true;
		}
		if(!getProfilePage() && getSaveAsPrimaryAddress()) {
			flag = false;
		}
	     return flag;
	}
	
	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 */
	public void createHardgoodShippingGroup(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		try {
			boolean failed = false;
			if (isLoggingDebug()) {
				logDebug("Entering createHardgoodShippingGroup()");
			}
			RepositoryItem shippingHomeAddress = (RepositoryItem) getProfile()
					.getPropertyValue(propertyManager.getShippingAddressPropertyName());
			Map otherAddressMap = (Map) getProfile()
					.getPropertyValue(this.getPropertyManager().getSecondaryAddressPropertyName());
			if (otherAddressMap != null && shippingHomeAddress != null
					&& (otherAddressMap.size() >= shippingAddressMaxLimit - 1)) {
				this.addFormException(
						new DropletException(this.getMessageLocator().getMessageString("shippingAddressMaxLimitReached",
								shippingAddressMaxLimit.toString()), "shippingAddressMaxLimitReached"));
				if (getFormError()) {
					return;
				}
			}
			HardgoodShippingGroup shippingGroup = null;
			ShippingGroupMapContainer container = null;
			String name = null;
			container = getContainer();
			if (!profilePage) {
				shippingGroup = getHardgoodShippingGroup();
				name = getHardgoodShippingGroupName();
			}
			if (isValidateAddress()) {
				validateShippingGroup();
				if (getFormError()) {
					return;
				}
			}

			if (DigitalStringUtil.isBlank(name) && isGenerateNickname()) {
				if (isLoggingDebug()) {
					logDebug("Shipping Group Nickname is NULL. Thus getting a Unique Shipping Address Nickname.");
				}
				String addressName = "OtherAddress";
				Object pAddress = DigitalAddressUtil.copyToDSWAddress(new DigitalAddress(), address);
				RepositoryItem pProfile = getProfile();
				name = getDswProfileTools().getUniqueShippingAddressNickname(pAddress, pProfile, addressName);
			}

			if (!getProfilePage() && isAddToProfile()) {
				setHardgoodShippingGroupName(name);
			}

			if (isAddToContainer()) {
				if (isAssignNewShippingGroupAsDefault()) {
					container.setDefaultShippingGroupName(name);
				}
				container.addShippingGroup(name, shippingGroup);
			}

			if (!getProfilePage()) {
				if (isLoggingDebug()) {
					logDebug("Add to Profile flag value is ::" + isAddToProfile());
				}
				try {
					if (isAddToProfile()) {
						getOrderManager().getOrderTools().getProfileTools().createProfileRepositorySecondaryAddress(
								getProfile(), name, shippingGroup.getShippingAddress());
					}
					if (getSaveAsPrimaryAddress()) {
						getDswProfileTools().createProfileRepositoryPrimaryAddress(getProfile(),
								propertyManager.getShippingAddressPropertyName(),
								this.getHardgoodShippingGroupTemp().getShippingAddress());
					}
				} catch (RepositoryException repexec) {
					try {
						processException(repexec, "couldNotAddShippingAddressToProfile", pRequest, pResponse);
					} catch (Exception exception) {
						failed = true;
						if (isLoggingError()) {
							logError(exception);
						}
					}
				}
			} else {
				try {
					if (getSaveAsPrimaryAddress()) {
						// Create - Other Address - Start
						RepositoryItem ri = (RepositoryItem) getProfile()
								.getPropertyValue(propertyManager.getShippingAddressPropertyName());
						if (ri != null && DigitalStringUtil.isNotBlank(ri.getRepositoryId())) {
							Map mymap = (Map) getProfile()
									.getPropertyValue(getPropertyManager().getSecondaryAddressPropertyName());
							DigitalContactInfo oldAddress = new DigitalContactInfo();
							try {
								DigitalOrderTools.copyAddress(ri, oldAddress);
							} catch (CommerceException cEx) {
								if (isLoggingError()) {
									logError(cEx);
								}
							}
							mymap.put(name, ri);
						}
						// Create - Other Address - End

						getDswProfileTools().createProfileRepositoryPrimaryAddress(getProfile(),
								propertyManager.getShippingAddressPropertyName(), address);
					} else {
						getDswProfileTools().createProfileRepositorySecondaryAddress(getProfile(), name, address);
					}
				} catch (RepositoryException repexec) {
					try {
						processException(repexec, "couldNotAddShippingAddressToProfile", pRequest, pResponse);
					} catch (Exception exception) {
						failed = true;
						if (isLoggingError()) {
							logError(exception);
						}
					}
				}
			}
			if (!failed && address != null && DigitalStringUtil.isNotBlank(address.getFirstName())
					&& DigitalStringUtil.isNotBlank(address.getLastName())) {

				Order order = this.getOrder();
				List<HardgoodShippingGroup> hsgList = ((DigitalShippingGroupManager) this.getShippingGroupManager())
						.getNonGiftCardShippingGroups(order);
				for (HardgoodShippingGroup hsg : hsgList) {
					hsg.getShippingAddress().setFirstName(address.getFirstName());
					hsg.getShippingAddress().setLastName(address.getLastName());
				}
			}
		} finally {
			// Mark the transaction rollback if there are any form
			// validation errors
			if (this.getFormError()) {
				try {
					this.setTransactionToRollbackOnly();
				} catch (SystemException e) {
					logError("Transaction rollback exception :: ", e);
				}
			}
		}
	}

	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 */
	public void postCreateHardgoodShippingGroup(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		throws ServletException, IOException {
		OrderHolder shoppingCart = (OrderHolder)ServletUtil.getCurrentRequest().resolveName("/atg/commerce/ShoppingCart");
		Order order = shoppingCart.getCurrent();
		try{
		((DigitalPricingTools)getPricingTools()).priceOrderTotal( order, getPricingModelHolder(ServletUtil.getCurrentRequest()), HttpServletUtil.getUserLocale(ServletUtil.getCurrentRequest()), getProfile(), null );
		} catch (PricingException exc) {
			try {
		           processException(exc, "couldNotPriceOrderTotal", pRequest, pResponse);
		         }
		         catch (Exception exception) {
		           if (isLoggingError()) { logError(exception);
		           }
		         }
		}
	}
	
	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 */
	private PricingModelHolder getPricingModelHolder(DynamoHttpServletRequest pRequest)
			throws ServletException, IOException {
		String perfName = "getPricingModelHolder";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
				"DigitalCreateHardgoodShippingGroupFormHandler", perfName);
		PricingModelHolder holder = null;
		try {
			Collection models = (Collection) pRequest
					.getObjectParameter(ParameterName
							.getParameterName("pricingModels"));
			if ((models == null) && (userPricingModelsPath != null)) {
				holder = (PricingModelHolder) pRequest
						.resolveName(userPricingModelsPath);
				if (holder != null)
					models = holder.getShippingPricingModels();
			}
			return holder;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
					"DigitalUpdateShippingMethodComponent", perfName);
		}
	}

	/**
	 * 
	 * @param pUserPricingModelsPath
	 */
	public void setUserPricingModelsPath(String pUserPricingModelsPath) {
		if (pUserPricingModelsPath != null) {
			userPricingModelsPath = ComponentName
					.getComponentName(pUserPricingModelsPath);
		} else {
			userPricingModelsPath = null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String getUserPricingModelsPath() {
		if (userPricingModelsPath != null) {
			return userPricingModelsPath.getName();
		}
		return null;
	}

	/**
	 * Override the method to stop commit & post commit steps if there is any
	 * form exceptions during validation. Rollback the transaction.
	 * 
	 * @param pSuccessURL
	 * @param pFailureURL
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 */
	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if ((getSessionExpirationURL() != null) && (pRequest.getSession().isNew()) && (isFormSubmission(pRequest))) {
			RepositoryItem profile;

			if (((profile = getProfile()) == null) && ((profile = defaultUserProfile(true)) == null)
					&& (isLoggingError())) {
				logError("missingExpiredSessionProfile");
			}

			if ((profile != null) && (profile.isTransient())) {
				if (isLoggingDebug())
					logDebug("Session expired: redirecting to " + getSessionExpirationURL());
				redirectOrForward(pRequest, pResponse, getSessionExpirationURL());
				return false;
			}
		}

		if (isTransactionMarkedAsRollBack()) {
			if (isLoggingDebug()) {
				logDebug("Transaction Marked as Rollback - redirecting to: " + pFailureURL);
			}
			return false;
		}

		return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
	}

}