package com.digital.commerce.services.pricing.calculators;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.services.order.GiftCardCommerceItem;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.DetailedItemPriceInfo;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.ItemPricingCalculator;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingTools;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalGiftCardItemPricingCalculator extends GenericService implements ItemPricingCalculator {
	private static final List<String>	giftCardTypes		= new ArrayList<>();
	private static final String	PROD_TYPE_PROPERTY	= "productType";
	private PricingTools		pricingTools;

	static {
		giftCardTypes.add( "standardGiftCard" );
		giftCardTypes.add( "personalizedGiftCard" );
		giftCardTypes.add( "eGiftCard" );
		giftCardTypes.add( "reloadGiftCard" );
	}

	public void priceEachItem( List arg0, List arg1, RepositoryItem arg2, Locale arg3, RepositoryItem arg4, Map arg5 ) throws PricingException {
		if( isLoggingDebug() ) {
			logDebug( "Start priceEachItem" );
		}
		if( isLoggingDebug() ) {
			logDebug( "End priceEachItem" );
		}
	}

	public void priceItem( ItemPriceInfo arg0, CommerceItem arg1, RepositoryItem arg2, Locale arg3, RepositoryItem arg4, Map arg5 ) throws PricingException {
		if( isLoggingDebug() ) {
			logDebug( "Start priceItem" );
		}
		if( isLoggingDebug() ) {
			logDebug( "End priceItem" );
		}
	}

	public void priceItems( List pPriceQuotes, List pItems, RepositoryItem pPricingModel, java.util.Locale pLocale, RepositoryItem pProfile, Order pOrder, Map pExtraParameters ) throws PricingException {
		if( isLoggingDebug() ) {
			logDebug( "Start priceItems" );
		}
		if( pPriceQuotes != null ) {
			for( int i = 0; i < pPriceQuotes.size(); i++ ) {
				ItemPriceInfo ipInfo = (ItemPriceInfo)pPriceQuotes.get( i );
				CommerceItem ci = (CommerceItem)pItems.get( i );
				setPriceInfo( ci, ipInfo, pPricingModel, pLocale, pProfile, pOrder, pExtraParameters );
			}
		}
		if( isLoggingDebug() ) {
			logDebug( "End priceItems" );
		}
	}

	/** Updated priceinfo and its details
	 * 
	 * @param ci
	 * @param ipInfo
	 * @param pPricingModel
	 * @param pLocale
	 * @param pProfile
	 * @param pOrder
	 * @param pExtraParameters
	 * @throws PricingException */
	private void setPriceInfo( CommerceItem ci, ItemPriceInfo ipInfo, RepositoryItem pPricingModel, java.util.Locale pLocale, RepositoryItem pProfile, Order pOrder, Map pExtraParameters ) throws PricingException {
		if( isLoggingDebug() ) {
			logDebug( "Start setPriceInfo for ci: " + ci.getCatalogRefId() );
			logDebug( "ci is instance of " + ci.getClass().getName() );
		}

		if( ci != null && ipInfo != null && ci instanceof GiftCardCommerceItem ) {
			if( isLoggingDebug() ) {
				logDebug( "ci" + ci.getCatalogRefId() + " is gift card ci, reset priceInfo" );
			}

			RepositoryItem productRef = (RepositoryItem)ci.getAuxiliaryData().getProductRef();
			String productType = (String)productRef.getPropertyValue( PROD_TYPE_PROPERTY );
			if( isLoggingDebug() ) {
				logDebug( "ci" + ci.getCatalogRefId() + " product type:" + productType );
			}
			if( giftCardTypes.contains( productType ) ) { // additional check for gift
				// card, un-necessary
				GiftCardCommerceItem gcci = (GiftCardCommerceItem)ci;
				//As per new requirement (PLAT-1580) we need to take the DallarValue as list price
				BigDecimal listPrice = gcci.getCardPrice() == null ? new BigDecimal( "0" ).setScale( 2, RoundingMode.HALF_UP ) : new BigDecimal( gcci.getCardPrice().doubleValue() ).setScale( 2, RoundingMode.HALF_UP );
				listPrice = listPrice.add( ( gcci.getDollarValue() == null ) ? new BigDecimal( "0" ).setScale( 2, RoundingMode.HALF_UP ) : new BigDecimal( gcci.getDollarValue().doubleValue() ).setScale( 2, RoundingMode.HALF_UP ) );
				ipInfo.setListPrice( DigitalCommonUtil.round(listPrice.doubleValue()) );
				ipInfo.setRawTotalPrice( DigitalCommonUtil.round(gcci.getGiftCardPriceTotal() ));
				ipInfo.setAmount(DigitalCommonUtil.round(DigitalCommonUtil.round(gcci.getGiftCardPriceTotal() )));
				ipInfo.setAmountIsFinal( true );
				if( isLoggingDebug() ) {
					logDebug( "reset to [" + " List Price: " + ipInfo.getListPrice() + ", Raw Total Price: " + ipInfo.getRawTotalPrice() + ", Amount: " + ipInfo.getAmount() + ", isAmountFinal: " + ipInfo.isAmountIsFinal() + " ]" );
				}
				List<DetailedItemPriceInfo> detailsList = ipInfo.getCurrentPriceDetails();
				if( detailsList == null ) {
					detailsList = new ArrayList<>();
					List<DetailedItemPriceInfo> newDetails = getPricingTools().getDetailedItemPriceTools().createInitialDetailedItemPriceInfos( gcci.getGiftCardPriceTotal(), ipInfo, ci, pPricingModel, pLocale, pProfile, pExtraParameters,
							Constants.LIST_PRICE_ADJUSTMENT_DESCRIPTION );
					detailsList.addAll( newDetails );
				} else {
					DetailedItemPriceInfo details = (DetailedItemPriceInfo)detailsList.get( 0 );
					details.setQuantity( gcci.getQuantity() );
					details.setAmount( DigitalCommonUtil.round(gcci.getGiftCardPriceTotal()) );
				}
			}
		}
	}
}
