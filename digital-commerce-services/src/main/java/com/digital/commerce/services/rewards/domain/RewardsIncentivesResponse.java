package com.digital.commerce.services.rewards.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RewardsIncentivesResponse extends ResponseWrapper {

  int count;
  private List<RewardsIncentiveItem> items;
}
