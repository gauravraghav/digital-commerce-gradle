/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import com.digital.commerce.common.domain.ResponseWrapper;

/**
 * @author mmallipu
 *
 */
public class RewardsUpdateCustomerResponse extends ResponseWrapper {

	public RewardsUpdateCustomerResponse() {
	}
}

