package com.digital.commerce.services.order.listner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.order.processor.domain.payload.ReadyForPickupPayload;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderlineAttributesDTO;

public class DigitalReadyForPickupReminderListner extends DigitalOrderStatusListener {
	protected static final String CLASSNAME = DigitalReadyForPickupReminderListner.class.getName();

	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected HashMap<String, Object> handleEmailNotification(OrderStatusPayload payLoad, DigitalOrderImpl order,
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {
		HashMap<String, Object> extraAttributes = new HashMap<>();

		ReadyForPickupPayload readyForPickupReminderPayload = (ReadyForPickupPayload) payLoad;

		extraAttributes.put("pickUpByDate", readyForPickupReminderPayload.getPickUpByDate());
		
		String customerFirstName = readyForPickupReminderPayload.getPersonInfoBillTo().getFirstName();

		if (DigitalStringUtil.isEmpty(customerFirstName)) {
			if (DigitalStringUtil.isEmpty(customerFirstName)) {
				customerFirstName = readyForPickupReminderPayload.getPersonInfoShipTo().getFirstName();
				if (DigitalStringUtil.isEmpty(customerFirstName)) {
					customerFirstName = readyForPickupReminderPayload.getPersonInfoMarkFor().getFirstName();
				}
			}
		}
		
		extraAttributes.put("customerFirstName", customerFirstName);

		if (DigitalStringUtil.isNotEmpty(readyForPickupReminderPayload.getPersonInfoMarkFor().getEmailId())) {
			extraAttributes.put("altPickupEmail", readyForPickupReminderPayload.getPersonInfoMarkFor().getEmailId());
		}
		
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOPIS, itemsByFullfilmentType,
				extraAttributes);
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOSTS, itemsByFullfilmentType,
				extraAttributes);

		// get mall plaza name
		HashMap<String, OrderlineAttributesDTO> lines = (HashMap<String, OrderlineAttributesDTO>) extraAttributes
				.get("orderLines");

		if (lines != null) {
			Map.Entry<String, OrderlineAttributesDTO> entry = lines.entrySet().iterator().next();
			extraAttributes.put("mallPlazaName", (entry.getValue() != null) ? entry.getValue().getMallPlazaName() : "");
		}

		return extraAttributes;
	}

	@Override
	protected String getOrderNo(OrderStatusPayload payLoad) {
		if(payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null ){
			return payLoad.getOmniOrderUpdate().getOrder().getOrderNo();
		}
		return null;
	}

	@Override
	protected String getEmailTo(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return  ((ReadyForPickupPayload)payLoad).getOmniOrderUpdate().getOrder().getCustomerEMailID();
		//return ((ReadyForPickupPayload)payLoad).getPersonInfoShipTo().getEmailId();
	}

	@Override
	protected List<OrderLine> getOrderLines(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((ReadyForPickupPayload)payLoad).getOrderLines();
	}
	
}
