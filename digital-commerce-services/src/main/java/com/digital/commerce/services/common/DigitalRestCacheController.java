package com.digital.commerce.services.common;

import java.net.URLDecoder;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atg.nucleus.logging.ApplicationLoggingImpl;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;
import lombok.Setter;

/**`
 * @author HB391569
 */
@Getter
@Setter
public class DigitalRestCacheController extends ApplicationLoggingImpl {
	public DigitalRestCacheController() {
		super(DigitalRestCacheController.class.getName());
	}

	boolean enabled = false;

	private Map<String, String> serviceCacheExpirationMap = new HashMap<>();
	private static final String PERF_NAME = "addCacheHeaders";
	private static final String PERFORM_MONITOR_NAME = "DigitalRestCacheController";
	private String dailyCutOffTime;

	/**
	 * @param pRequest
	 * @param pResponse
	 */
	public void addCacheHeaders(HttpServletRequest pRequest,
			HttpServletResponse pResponse) {
		if (isLoggingDebug()) {
			logDebug("DigitalRestCacheController enabled: " + enabled);
		}
		if (enabled) {
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
						PERFORM_MONITOR_NAME, PERF_NAME);
				Date now = new Date();
				// get cache seconds based on URI
				int cacheSeconds = 0;
				cacheSeconds = this.getCacheSeconds(pRequest.getRequestURI(),
						pRequest.getQueryString(), pRequest.getMethod());
				// get seconds to cutoff time
				long secondsUntilCutoff = this.getSecondsUntilTime(
						dailyCutOffTime, now);
				if (isLoggingDebug()) {
					logDebug("Request URI: " + pRequest.getRequestURI());
					logDebug("Request query string: "
							+ pRequest.getQueryString());
					logDebug("Request method: " + pRequest.getMethod());
					logDebug("cache seconds: " + cacheSeconds);
					logDebug("Cutoff time: " + dailyCutOffTime);
					logDebug("seconds until cut off: " + secondsUntilCutoff);
				}
				// adjust cache seconds as needed
				if (cacheSeconds >= secondsUntilCutoff) {
					cacheSeconds = (int) secondsUntilCutoff;
				}
				// set the cache headers
				if (cacheSeconds > 0) {
					Date expirationDate = getExpirationDate(cacheSeconds,
							this.getDailyCutOffTime(), now);
					((HttpServletResponse) pResponse).setDateHeader("Expires",
							expirationDate.getTime());
					((HttpServletResponse) pResponse).setHeader(
							"Cache-Control", "max-age=" + cacheSeconds);
				} else {
					((HttpServletResponse) pResponse).setHeader(
							"Cache-Control", "max-age=0, no-cache, no-store");
					((HttpServletResponse) pResponse).setHeader("Pragma",
							"no-cache");
					((HttpServletResponse) pResponse).setHeader("Expires",
							"Thu, 01 Jan 1970 00:00:00 GMT");
				}
			} catch (Exception ex) {
				logError(
						"Unable to add cache headers, the data might be getting cached in browsers like IE11. ",
						ex);
			} finally {
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
						PERFORM_MONITOR_NAME, PERF_NAME);
			}
		} else {
			// add default headers as not sending headers is non-deterministic
			// and upto browser
			if (isLoggingDebug()) {
				logDebug("Adding default cache headers");
			}
			((HttpServletResponse) pResponse).setHeader(
					"Cache-Control", "max-age=0, no-cache, no-store");
			((HttpServletResponse) pResponse).setHeader("Pragma",
					"no-cache");
			((HttpServletResponse) pResponse).setHeader("Expires",
					"Thu, 01 Jan 1970 00:00:00 GMT");
		}
	}

	/**
	 * @param cacheSeconds
	 * @param dailyCutoffTime
	 * @return
	 */
	private Date getExpirationDate(long cacheSeconds, String dailyCutoffTime,
			Date now) {
		Date expirationDate = new Date(now.getTime() + cacheSeconds * 1000);
		Calendar cal = Calendar.getInstance();

		cal.setTime(now);

		if (dailyCutoffTime != null) {
			String[] splitTime = dailyCutoffTime.split(":");
			if (splitTime.length == 2) {
				try {
					cal.set(Calendar.HOUR_OF_DAY,
							Integer.parseInt(splitTime[0]));
					cal.set(Calendar.MINUTE, Integer.parseInt(splitTime[1]));
					cal.set(Calendar.SECOND, 0);
					if (cal.getTime().getTime() < now.getTime()) {
						cal.add(Calendar.DATE, 1);
					}
					if (expirationDate.getTime() > cal.getTime().getTime()) {
						return cal.getTime();
					}
				} catch (Exception ex) {
					logError(
							"Daily cut off time should be in hh:mm format (24 hour clock)",
							ex);
				}
			}
		}
		return expirationDate;
	}

	/**
	 * @param uri
	 * @param queryString
	 * @return
	 */
	// This method can be simplfied/optimized later
	private int getCacheSeconds(String uri, String queryString, String method) {
		int retVal = 0;
		// if GET method then only caching can be done
		if ("GET".equalsIgnoreCase(method) && serviceCacheExpirationMap != null
				&& !(serviceCacheExpirationMap.isEmpty())
				&& DigitalStringUtil.isNotBlank(uri)) {
			// compare cache key URI against incoming URI
			for (String key : serviceCacheExpirationMap.keySet()) {
				try {
					if (isLoggingDebug()) {
						logDebug("Cache key: " + key);
						logDebug("InComing URI: " + uri);
					}
					// check key contains URI with/wihtout QS
					Pattern pattern = Pattern.compile(key,
							Pattern.CASE_INSENSITIVE);
					Matcher matcher = pattern.matcher(uri);
					if (isLoggingDebug()) {
						logDebug("URI: " + uri + " matches with patten: " + key
								+ " " + matcher.matches());
					}
					if (matcher.matches()) {
						if (key.indexOf("?") > 0) {
							String[] splitPattern = key.split("\\?");
							if (splitPattern != null
									&& splitPattern.length > 1
									&& DigitalStringUtil
											.isNotBlank(splitPattern[1])) {
								if (DigitalStringUtil.isNotBlank(queryString)) {
									if (isLoggingDebug()) {
										logDebug("QS found in key and request, trying to match");
									}
									try {
										String qs = URLDecoder.decode(
												queryString, "UTF-8");
										String qsFromKey = URLDecoder.decode(
												splitPattern[1], "UTF-8");
										if (isLoggingDebug()) {
											logDebug("QS from cache key: "
													+ qsFromKey);
										}
										if (DigitalStringUtil.isNotBlank(qs)
												&& qs.contains(qsFromKey)) {
											if (isLoggingDebug()) {
												logDebug("QS from key exist in incoming QS");
											}
											retVal = new Integer(
													serviceCacheExpirationMap
															.get(key))
													.intValue();
											break;
										}
									} catch (Exception ex) {
										logError(
												"Error while decoding query strings for cache control",
												ex);
										// do nothing
									}
								}
							} else {
								if (isLoggingDebug()) {
									logDebug("No QS found in key, hence sending the cache seconds");
								}
								retVal = new Integer(
										serviceCacheExpirationMap.get(key))
										.intValue();
								break;
							}
						} else {
							if (isLoggingDebug()) {
								logDebug("Key and URI are same, hence sending the cache seconds");
							}
							retVal = new Integer(
									serviceCacheExpirationMap.get(key))
									.intValue();
							break;
						}
					}

				} catch (Exception ex) {
					logError(
							"Exception while finding out cache TTL, sending TTLs as zero..",
							ex);
				}
			}
		}
		return retVal;
	}

	/**
	 * @param time
	 * @return
	 */
	private long getSecondsUntilTime(String time, Date now) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(now);

		if (time != null) {
			String[] splitTime = time.split(":");
			if (splitTime.length == 2) {
				try {
					cal.set(Calendar.HOUR_OF_DAY,
							Integer.parseInt(splitTime[0]));
					cal.set(Calendar.MINUTE, Integer.parseInt(splitTime[1]));
					cal.set(Calendar.SECOND, 0);
					if (cal.getTime().getTime() < now.getTime()) {
						cal.add(Calendar.DATE, 1);
					}
					return (cal.getTime().getTime() - now.getTime()) / 1000;
				} catch (Exception ex) {
					logWarning(
							"Time should be in hh:mm format (24 hour clock)",
							ex);
				}
			}
		}
		return 0l;
	}

}
