/**
 * 
 */
package com.digital.commerce.services.scheduledrequest.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CurrentComponentPropertyResponse extends ResponseWrapper {

	private String currentValue="";
	private boolean doesHaveScheduledChange = false;
	private String scheduledChangeNewValue = "";
	private String scheduledTime = "";
}
