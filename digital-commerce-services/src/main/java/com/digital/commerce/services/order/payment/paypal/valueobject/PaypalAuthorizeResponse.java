package com.digital.commerce.services.order.payment.paypal.valueobject;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PaypalAuthorizeResponse extends PaypalResponse implements Serializable {

	private static final long	serialVersionUID	= 1240603138136425284L;
	private String				authorizationCode;
	private String				eligibleForProtection;

}
