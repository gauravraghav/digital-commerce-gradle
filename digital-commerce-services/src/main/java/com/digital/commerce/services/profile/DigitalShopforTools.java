package com.digital.commerce.services.profile;

import static com.digital.commerce.services.rewards.domain.RewardsShopforItem.CREATE_DATE_COMPARATOR;

import atg.adapter.gsa.ChangeAwareSet;
import atg.adapter.gsa.GSAItem;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.repository.SortDirective;
import atg.repository.SortDirectives;
import atg.userprofiling.Profile;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.rewards.domain.RewardsShopforItem;
import com.google.common.collect.Iterables;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author PR412029 Responsible for merging a new/updated set of Shopfor Items for a customer
 * profile with their existing Shopfor Items. Also, has methods to get count of Shopfor Items.
 */
public class DigitalShopforTools {

  private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalShopforTools.class);
  private static final String SHOPFOR_ID_PROPERTY = "id";
  private static final String SHOPFOR_PROFILEID_PROPERTY = "user";
  private static final String SHOPFOR_ISACTIVE_PROPERTY = "isActive";
  private static final String SHOPFOR_REWARDSSHOPFORID_PROPERTY = "rewardsShopforID";
  private static final String SHOPFOR_NAME_PROPERTY = "shopforName";
  private static final String SHOPFOR_RELATIONSHIP_PROPERTY = "relationship";
  private static final String SHOPFOR_BIRTHDAY_PROPERTY = "birthDay";
  private static final String SHOPFOR_BIRTHMONTH_PROPERTY = "birthMonth";
  private static final String SHOPFOR_WEBTYPE_PROPERTY = "webType";
  private static final String SHOPFOR_GENDER_PROPERTY = "gender";
  private static final String SHOPFOR_SIZEGROUP_PROPERTY = "sizeGroup";
  private static final String SHOPFOR_SIZES_PROPERTY = "sizes";
  private static final String SHOPFOR_WIDTHS_PROPERTY = "widths";
  private static final String SHOPFOR_DATECREATED_PROPERTY = "dateCreated";
  private static final String SHOPFOR_ITEM_DESCRIPTOR = "shopfor";
  private static final String SHOPFOR_SIZES_ITEM_DESCRIPTOR = "shopforSize";
  private static final String SHOPFOR_WIDTHS_ITEM_DESCRIPTOR = "shopforWidth";
  private static final String SHOPFOR_SIZE_SHOPFORSIZE_PROPERTY = "size";
  private static final String SHOPFOR_SIZE_SHOPFORGENDER_PROPERTY = "gender";
  private static final String SHOPFOR_SIZE_SHOPFORSIZECODE_PROPERTY = "sizeCode";
  private static final String SHOPFOR_WIDTH_SHOPFORWIDTH_PROPERTY = "width";
  private static final String SHOPFOR_WIDTH_SHOPFORWIDTHCODE_PROPERTY = "widthCode";

  public DigitalShopforTools() {
  }

  /**
   * @return Map
   * @throws DigitalAppException
   */
  public Map<String, Object> getFiltersforShopfor() throws DigitalAppException {

    Map<String, Object> result = new HashMap<>();

    try {
      Repository profileRepository = getProfile().getRepository();
      RepositoryItemDescriptor shopforSizeDescriptor = profileRepository
          .getItemDescriptor(SHOPFOR_SIZES_ITEM_DESCRIPTOR);
      RepositoryView shopforSizeView = shopforSizeDescriptor.getRepositoryView();
      QueryBuilder shopforSizeBuilder = shopforSizeView.getQueryBuilder();
      Query query = shopforSizeBuilder.createUnconstrainedQuery();
      SortDirectives sortDirectives = new SortDirectives();
      sortDirectives.addDirective(
          new SortDirective(SHOPFOR_SIZE_SHOPFORGENDER_PROPERTY, SortDirective.DIR_ASCENDING));
      sortDirectives.addDirective(
          new SortDirective(SHOPFOR_SIZE_SHOPFORSIZECODE_PROPERTY, SortDirective.DIR_ASCENDING));
      RepositoryItem[] shopforSizeItems = shopforSizeView.executeQuery(query, sortDirectives);

      //Each Gender will have list of Sizes
      Map<String, Object> genderSizes = new HashMap<>();
      ArrayList<String> sizes = new ArrayList<>();
      String gender = ""; //Start with blank
      for (RepositoryItem aShopforSizeItem : shopforSizeItems) {
        String currentGender = "";
        Object objectGender = aShopforSizeItem
            .getPropertyValue(SHOPFOR_SIZE_SHOPFORGENDER_PROPERTY);
        if (objectGender != null) {
          currentGender = objectGender.toString();
        }
        if (!gender.equalsIgnoreCase(currentGender)) {
          if (sizes.size() > 0) {
            genderSizes.put(gender, sizes.clone());
            sizes.clear(); //reset the sizes list and start over for next Gender
          }
        }
        gender = currentGender;
        Object size = aShopforSizeItem.getPropertyValue(SHOPFOR_SIZE_SHOPFORSIZE_PROPERTY);
        if (size != null) {
          sizes.add(size.toString());
        }
      }

      if (sizes.size() > 0) { //add the last Gender out of the above for loop
        genderSizes.put(gender, sizes);
      }

      //add genderSizes property to the Map of Maps
      result.put("genderSizes", genderSizes);

      // Get Widths and put it in "widths" object
      RepositoryItemDescriptor shopforWidthDescriptor = profileRepository
          .getItemDescriptor(SHOPFOR_WIDTHS_ITEM_DESCRIPTOR);
      RepositoryView shopforWidthView = shopforWidthDescriptor.getRepositoryView();
      QueryBuilder shopforWidthBuilder = shopforWidthView.getQueryBuilder();
      Query widthQuery = shopforWidthBuilder.createUnconstrainedQuery();
      SortDirectives sortWidthDirectives = new SortDirectives();
      sortWidthDirectives.addDirective(
          new SortDirective(SHOPFOR_WIDTH_SHOPFORWIDTHCODE_PROPERTY, SortDirective.DIR_ASCENDING));
      RepositoryItem[] shopforWidthItems = shopforWidthView
          .executeQuery(widthQuery, sortWidthDirectives);

      List<String> widths = new ArrayList<>();
      for (RepositoryItem aShopforWidthItem : shopforWidthItems) {
        Object objectWidth = aShopforWidthItem
            .getPropertyValue(SHOPFOR_WIDTH_SHOPFORWIDTH_PROPERTY);
        if (objectWidth != null) {
          widths.add(objectWidth.toString());
        }
      }
      result.put("widths", widths);

      return result;

    } catch (RepositoryException e) {
      logger.error(
          "Error occurred while getting Shopfor Lookup Filters from ATG Database - getFiltersforShopfor");
      throw new DigitalAppException(e.getMessage());
    }
  }

  /**
   * @param profile
   * @return Integer
   * @throws DigitalAppException
   */
  public final Integer getCountofShopforItems(final MutableRepositoryItem profile)
      throws DigitalAppException {

    try {
      // Get count of all Shopfor Items from the User Profile
      Repository shopforRepository = profile.getRepository();
      RepositoryItemDescriptor shopforDescriptor = shopforRepository
          .getItemDescriptor(SHOPFOR_ITEM_DESCRIPTOR);

      RepositoryView shopforView = shopforDescriptor.getRepositoryView();
      QueryBuilder shopforBuilder = shopforView.getQueryBuilder();
      QueryExpression profileFilter = shopforBuilder
          .createPropertyQueryExpression(SHOPFOR_PROFILEID_PROPERTY);
      QueryExpression constantFilter = shopforBuilder
          .createConstantQueryExpression(profile.getRepositoryId());
      Query query = shopforBuilder
          .createComparisonQuery(profileFilter, constantFilter, QueryBuilder.EQUALS);
      return shopforView.executeCountQuery(query);
    } catch (RepositoryException e) {
      logger.error("Unable to get Count of ShopforItems - getCountofShopforItems");
      throw new DigitalAppException(e.getMessage());
    }
  }

  /**
   * @param profile
   * @return List
   */
  public final List<RewardsShopforItem> retrieveShopforItemsFromProfile(
      final MutableRepositoryItem profile) {
    List<RewardsShopforItem> shopforItems = new ArrayList<>();
    final ChangeAwareSet shopforChangeAwareSet = ((ChangeAwareSet) profile
        .getPropertyValue(SHOPFOR_ITEM_DESCRIPTOR));

    if (shopforChangeAwareSet != null && !shopforChangeAwareSet.isEmpty()) {
      for (Object aShopforItem : shopforChangeAwareSet) {
        final GSAItem repositoryItem = (GSAItem) aShopforItem;
        RewardsShopforItem rewardsShopforItem = ShopForFactory.from(repositoryItem);
        if (rewardsShopforItem.isActive()) {
          shopforItems.add(rewardsShopforItem);
        }
      }
    }

    //Debug logs
    String profileId = profile.getRepositoryId();
    if (shopforItems.isEmpty() && logger.isDebugEnabled()) {
      logger.debug("There are NO Shopfor Items for this user - " + profileId);
    } else if (logger.isDebugEnabled()) {
      logger.debug("There are " + shopforItems.size()
          + " valid Shopfor Item(s) available for this profile - " + profileId);
    }

    Collections.sort( shopforItems, CREATE_DATE_COMPARATOR );

    return shopforItems;
  }

  /**
   * @param profile
   * @param shopforItem
   * @return true or false
   */
  public final boolean isShopforBelongsToProfileAndActive(final MutableRepositoryItem profile,
      final RewardsShopforItem shopforItem) {
    final String profileId = profile.getRepositoryId();
    List<RewardsShopforItem> currentShopforItems = new ArrayList<>();
    currentShopforItems.add(shopforItem);
    final ChangeAwareSet shopForChangeAwareSet =
        ((ChangeAwareSet) profile.getPropertyValue(SHOPFOR_ITEM_DESCRIPTOR));
    if (shopForChangeAwareSet != null && !shopForChangeAwareSet.isEmpty()) {
      for (Object aShopforChangeAwareSet : shopForChangeAwareSet) {
        final GSAItem repositoryItem = (GSAItem) aShopforChangeAwareSet;
        final RewardsShopforItem repositoryItemAsShopforItem = ShopForFactory.from(repositoryItem);
        final int existingRewardsShopforItemIndex =
            Iterables.indexOf(currentShopforItems,
                new ExistingShopForPredicate(repositoryItemAsShopforItem));
        if (existingRewardsShopforItemIndex != -1) {
          if (logger.isDebugEnabled()) {
            logger.debug("This Shop for Item Id " + repositoryItemAsShopforItem.getShopforID() +
                " belongs to the profile " + profileId);
          }
          if (repositoryItemAsShopforItem.isActive()) {
            return true;
          }
        }
      }
    } else {
      if (logger.isDebugEnabled()) {
        logger.debug(
            "This Shop for Item Id " + shopforItem.getShopforID()
                + " doesn't belong to the profile "
                + profileId);
      }
    }

    return false;
  }

  /**
   * @param profile
   * @param shopforItem
   * @throws DigitalAppException
   */
  public final void addOrUpdateShopforForProfile(MutableRepositoryItem profile,
      final RewardsShopforItem shopforItem) throws DigitalAppException {
    final MutableRepository profileRepository = (MutableRepository) profile.getRepository();
    final String profileId = profile.getRepositoryId();
    boolean updateFlag = false;
    Set<RepositoryItem> mergedShopforSet = new HashSet<>();
    List<RewardsShopforItem> currentShopfors = new ArrayList<>();
    currentShopfors.add(shopforItem);

    try {

      final Set<MutableRepositoryItem> shopForRepositoryItemSet = ((Set<MutableRepositoryItem>) profile
          .getPropertyValue(SHOPFOR_ITEM_DESCRIPTOR));
      if (shopForRepositoryItemSet != null && !shopForRepositoryItemSet.isEmpty()) {
        for (Object aShopForRepositoryItem : shopForRepositoryItemSet) {
          final MutableRepositoryItem repositoryItem = (MutableRepositoryItem) aShopForRepositoryItem;
          final RewardsShopforItem repositoryItemAsShopFor = ShopForFactory.from(repositoryItem);
          if (repositoryItemAsShopFor.isActive()) {
            final int existingRewardsShopForIndex =
                Iterables.indexOf(currentShopfors,
                    new ExistingShopForPredicate(repositoryItemAsShopFor));
            if (existingRewardsShopForIndex != -1) {
              if (logger.isDebugEnabled()) {
                logger.debug(
                    "This Shopfor Item Id " + repositoryItemAsShopFor.getShopforID() +
                        " belongs to the profile " + profileId);
              }
              populateExistingShopforItem(currentShopfors, repositoryItem, profileRepository,
                  existingRewardsShopForIndex);
              currentShopfors.remove(existingRewardsShopForIndex);
              updateFlag = true;
            }
            mergedShopforSet.add(repositoryItem);
          }
        }
      } else {
        if (logger.isDebugEnabled()) {
          logger.debug("This Shop For Item Id " + shopforItem.getShopforID()
              + " doesn't belong to the profile "
              + profileId);
        }
      }

      for (final RewardsShopforItem newShopforItem : currentShopfors) {
        if (logger.isDebugEnabled()) {
          logger.debug("This Shop for Item Id " + newShopforItem.getShopforID()
              + " is new for this customer so new record " +
              "is added for the profile " + profileId);
        }
        mergedShopforSet.add(ShopForFactory.to(profile, profileRepository, newShopforItem));
        updateFlag = true;
      }

      if (updateFlag) {
        if (logger.isDebugEnabled()) {
          logger.debug(
              "There are Shop for Items to be created or updated for this profile " + profileId);
        }
        MutableRepositoryItem profileItem = profileRepository.getItemForUpdate(profileId,
            profile.getItemDescriptor().getItemDescriptorName());
        profileItem.setPropertyValue(SHOPFOR_ITEM_DESCRIPTOR, mergedShopforSet);
        profileRepository.updateItem(profileItem);
      } else {
        if (logger.isDebugEnabled()) {
          logger.debug(
              "There are NO Shop for Items to be created or updated for this profile "
                  + profileId);
        }
      }

    } catch (RepositoryException e) {
      logger.error("RepositoryException in addOrUpdateShopforForProfile");
      throw new DigitalAppException(e.getMessage());
    }
  }

  /**
   * @param profile
   * @param currentShopfors
   * @throws DigitalAppException
   */
  public final void mergeShopForItemsToProfile(MutableRepositoryItem profile,
      List<RewardsShopforItem> currentShopfors) throws DigitalAppException {
    final MutableRepository profileRepository = (MutableRepository) profile.getRepository();
    final String profileId = profile.getRepositoryId();

    try {

      final Set<MutableRepositoryItem> shopForChangeAwareSet =
          ((Set<MutableRepositoryItem>) profile.getPropertyValue(SHOPFOR_ITEM_DESCRIPTOR));
      Set<RepositoryItem> mergedShopforSet = new HashSet<>();
      boolean updateFlag = false;
      if (shopForChangeAwareSet != null && !shopForChangeAwareSet.isEmpty()) {
        for (Object aShopForChangeAwareSet : shopForChangeAwareSet) {
          final MutableRepositoryItem repositoryItem = (MutableRepositoryItem) aShopForChangeAwareSet;
          final RewardsShopforItem repositoryItemAsShopFor = ShopForFactory.from(repositoryItem);
          int existingRewardsShopForIndex = -1;
          if (currentShopfors != null) {
            existingRewardsShopForIndex =
                Iterables.indexOf(currentShopfors,
                    new ExistingShopForPredicate(repositoryItemAsShopFor));
          }
          if (existingRewardsShopForIndex != -1) {
            if (logger.isDebugEnabled()) {
              logger.debug("This Birthday Gift Item Id " + repositoryItemAsShopFor.getShopforID() +
                  " is valid. Merging from Rewards to ATG Profile - "
                  + profileId);
            }

            Object isActive = repositoryItem.getPropertyValue(SHOPFOR_ISACTIVE_PROPERTY);
            if (isActive != null && (Integer) isActive == 0) {
              repositoryItem.setPropertyValue(SHOPFOR_ISACTIVE_PROPERTY, 1);
            }
            mergedShopforSet.add(repositoryItem);
            updateFlag = true;
            currentShopfors.remove(existingRewardsShopForIndex);
          } else {
            if (logger.isDebugEnabled()) {
              logger.debug("This Birthday Gift Item Id " + repositoryItemAsShopFor.getShopforID() +
                  " is no longer valid for the profile " + profileId);
            }
            Object isActive = repositoryItem.getPropertyValue(SHOPFOR_ISACTIVE_PROPERTY);
            if (isActive != null && (Integer) isActive != 0) {
              repositoryItem.setPropertyValue(SHOPFOR_ISACTIVE_PROPERTY, 0);
              mergedShopforSet.add(repositoryItem);
              updateFlag = true;
            }
          }
        }
      }

      if (null != currentShopfors) {
        for (final RewardsShopforItem shopforItem : currentShopfors) {
          if (logger.isDebugEnabled()) {
            logger.debug("This Shop for Rewards Item Id " + shopforItem.getRewardsShopforID()
                + " is new for this customer so new record is added for the profile " + profileId);
          }
          mergedShopforSet.add(ShopForFactory.to(profile, profileRepository, shopforItem));
          updateFlag = true;
        }
      }

      if (updateFlag) {
        if (logger.isDebugEnabled()) {
          logger.debug("There are Shop for Items to be synced up for the profile " + profileId);
        }
        MutableRepositoryItem profileItem = profileRepository.getItemForUpdate(profileId,
            profile.getItemDescriptor().getItemDescriptorName());
        profileItem.setPropertyValue(SHOPFOR_ITEM_DESCRIPTOR, mergedShopforSet);
        profileRepository.updateItem(profileItem);
      } else {
        if (logger.isDebugEnabled()) {
          logger.debug("There are NO Shop for Items to be synced up for the profile " + profileId);
        }
      }

    } catch (RepositoryException e) {
      logger.error("RepositoryException occured in mergeShopForItemsToProfile");
      throw new DigitalAppException(e.getMessage());
    }

  }

  private static class ShopForFactory {

    /**
     * Private Constructor as this class is not supposed to be initiated
     */
    private ShopForFactory() {

    }

    /**
     * @param repositoryItem
     * @param repository
     * @param rewardsShopforItem
     * @return MutableRepositoryItem
     * @throws DigitalAppException
     */
    static MutableRepositoryItem to(MutableRepositoryItem repositoryItem,
        MutableRepository repository,
        RewardsShopforItem rewardsShopforItem) throws DigitalAppException {
      MutableRepositoryItem retVal;
      try {
        retVal = repository.createItem(SHOPFOR_ITEM_DESCRIPTOR);
        String gender = null;
        //id
        if (repositoryItem instanceof Profile) {
          retVal.setPropertyValue(SHOPFOR_PROFILEID_PROPERTY,
              ((Profile) repositoryItem).getDataSource());
        } else {
          retVal.setPropertyValue(SHOPFOR_PROFILEID_PROPERTY, repositoryItem);
        }

        //is_active
        if (rewardsShopforItem.isActive()) {
          retVal.setPropertyValue(SHOPFOR_ISACTIVE_PROPERTY, 1);
        } else {
          retVal.setPropertyValue(SHOPFOR_ISACTIVE_PROPERTY, 0);
        }

        //rewards_shopfor_id
        if (DigitalStringUtil.isNotBlank(rewardsShopforItem.getRewardsShopforID())) {
          retVal.setPropertyValue(SHOPFOR_REWARDSSHOPFORID_PROPERTY,
              rewardsShopforItem.getRewardsShopforID());
        }

        //shopfor_name
        if (DigitalStringUtil.isNotBlank(rewardsShopforItem.getShopforName())) {
          retVal.setPropertyValue(SHOPFOR_NAME_PROPERTY, rewardsShopforItem.getShopforName());
        }

        //relationship
        if (DigitalStringUtil.isNotBlank(rewardsShopforItem.getRelationship())) {
          retVal.setPropertyValue(SHOPFOR_RELATIONSHIP_PROPERTY,
              rewardsShopforItem.getRelationship());
        }

        //birth_day
        if (DigitalStringUtil.isNotBlank(rewardsShopforItem.getBirthDay())) {
          retVal.setPropertyValue(SHOPFOR_BIRTHDAY_PROPERTY, rewardsShopforItem.getBirthDay());
        }

        //birth_month
        if (DigitalStringUtil.isNotBlank(rewardsShopforItem.getBirthMonth())) {
          retVal.setPropertyValue(SHOPFOR_BIRTHMONTH_PROPERTY, rewardsShopforItem.getBirthMonth());
        }

        //web_type
        if (DigitalStringUtil.isNotBlank(rewardsShopforItem.getWebType())) {
          retVal.setPropertyValue(SHOPFOR_WEBTYPE_PROPERTY, rewardsShopforItem.getWebType());
        }

        //gender
        if (DigitalStringUtil.isNotBlank(rewardsShopforItem.getGender())) {
          gender = rewardsShopforItem.getGender(); //Need this later
          retVal.setPropertyValue(SHOPFOR_GENDER_PROPERTY, gender);
        }

        //size_group
        if (DigitalStringUtil.isNotBlank(rewardsShopforItem.getSizeGroup())) {
          retVal.setPropertyValue(SHOPFOR_SIZEGROUP_PROPERTY, rewardsShopforItem.getSizeGroup());
        }

        //dsw_user_shopfor_size - dsw_shopfor_size
        if (rewardsShopforItem.getSizes() != null && rewardsShopforItem.getSizes().length > 0) {
          retVal.setPropertyValue(SHOPFOR_SIZES_PROPERTY,
              getSizeRepositoryItems(rewardsShopforItem.getSizes(), repository, gender));
        }

        //dsw_user_shopfor_width - dsw_shopfor_width
        if (rewardsShopforItem.getWidths() != null && rewardsShopforItem.getWidths().length > 0) {
          retVal.setPropertyValue(SHOPFOR_WIDTHS_PROPERTY,
              getWidthRepositoryItems(rewardsShopforItem.getWidths(), repository));
        }

      } catch (RepositoryException e) {
        logger.error(
            String.format("Unable to create shopForItem %s", rewardsShopforItem.getShopforID()),
            e);
        throw new DigitalAppException(e.getMessage());
      }
      return retVal;
    }

    /**
     * @param item
     * @return RewardsShopforItem
     */
    static RewardsShopforItem from(RepositoryItem item) {
      final RewardsShopforItem shopForsItem = new RewardsShopforItem();

      //shopfor_id
      final Object shopforID = item.getPropertyValue(SHOPFOR_ID_PROPERTY);
      if (null != shopforID) {
        shopForsItem.setShopforID((String) shopforID);
      }

      //id
      final Object profileObj = item.getPropertyValue(SHOPFOR_PROFILEID_PROPERTY);
      if (null != profileObj) {
        MutableRepositoryItem profileRepo = (MutableRepositoryItem) profileObj;
        shopForsItem.setProfileID(profileRepo.getRepositoryId());
      }

      //is_active
      final Object isActive = item.getPropertyValue(SHOPFOR_ISACTIVE_PROPERTY);
      if (null != isActive) {
        shopForsItem.setActive((Integer) isActive > 0);
      }

      //rewards_shopfor_id
      final Object rewardsShopforID = item.getPropertyValue(SHOPFOR_REWARDSSHOPFORID_PROPERTY);
      if (null != rewardsShopforID) {
        shopForsItem.setRewardsShopforID((String) rewardsShopforID);
      }

      //shopfor_name
      final Object shopforName = item.getPropertyValue(SHOPFOR_NAME_PROPERTY);
      if (null != shopforName) {
        shopForsItem.setShopforName((String) shopforName);
      }

      //relationship
      final Object relationship = item.getPropertyValue(SHOPFOR_RELATIONSHIP_PROPERTY);
      if (null != relationship) {
        shopForsItem.setRelationship((String) relationship);
      }

      //birth_day
      final Object birthDay = item.getPropertyValue(SHOPFOR_BIRTHDAY_PROPERTY);
      if (null != birthDay) {
        shopForsItem.setBirthDay((String) birthDay);
      }

      //birth_month
      final Object birthMonth = item.getPropertyValue(SHOPFOR_BIRTHMONTH_PROPERTY);
      if (null != birthMonth) {
        shopForsItem.setBirthMonth((String) birthMonth);
      }

      //web_type
      final Object webType = item.getPropertyValue(SHOPFOR_WEBTYPE_PROPERTY);
      if (null != webType) {
        shopForsItem.setWebType((String) webType);
      }

      //gender
      final Object gender = item.getPropertyValue(SHOPFOR_GENDER_PROPERTY);
      if (null != gender) {
        shopForsItem.setGender((String) gender);
      }

      //size_group
      final Object sizeGroup = item.getPropertyValue(SHOPFOR_SIZEGROUP_PROPERTY);
      if (null != sizeGroup) {
        shopForsItem.setSizeGroup((String) sizeGroup);
      }

      //date_created
      final Object dateCreated = item.getPropertyValue(SHOPFOR_DATECREATED_PROPERTY);
      if (null != dateCreated) {
        shopForsItem.setDateCreated((Date) dateCreated);
      }

      //dsw_user_shopfor_size - dsw_shopfor_size
      final Object sizes = item.getPropertyValue(SHOPFOR_SIZES_PROPERTY);
      if (null != sizes) {
        List<RepositoryItem> sizesRepItemList = (List<RepositoryItem>) sizes;
        List<String> sizesList = new ArrayList<>();
        for (RepositoryItem sizesRepItem : sizesRepItemList) {
          Object size = sizesRepItem.getPropertyValue(SHOPFOR_SIZE_SHOPFORSIZE_PROPERTY);
          if (size != null) {
            sizesList.add((String) size);
          }
        }
        shopForsItem.setSizes(sizesList.toArray(new String[0]));
      }

      //dsw_user_shopfor_width - dsw_shopfor_width
      final Object widths = item.getPropertyValue(SHOPFOR_WIDTHS_PROPERTY);
      if (null != widths) {
        List<RepositoryItem> widthsRepItemList = (List<RepositoryItem>) widths;
        List<String> widthsList = new ArrayList<>();
        for (RepositoryItem widthsRepItem : widthsRepItemList) {
          Object width = widthsRepItem.getPropertyValue(SHOPFOR_WIDTH_SHOPFORWIDTH_PROPERTY);
          if (width != null) {
            widthsList.add((String) width);
          }
        }
        shopForsItem.setWidths(widthsList.toArray(new String[0]));
      }

      return shopForsItem;


    }
  }

  private static class ExistingShopForPredicate implements DigitalPredicate<RewardsShopforItem> {

    private final RewardsShopforItem existingShopforItem;

    /**
     * @param existingShopforItem
     */
    ExistingShopForPredicate(RewardsShopforItem existingShopforItem) {
      this.existingShopforItem = existingShopforItem;
    }

    /**
     * @param input
     * @return true or false
     */
    @Override
    public final boolean apply(RewardsShopforItem input) {
      return DigitalStringUtil.equals(existingShopforItem.getProfileID(), input.getProfileID())
          && DigitalStringUtil
          .equals(existingShopforItem.getRewardsShopforID(), input.getRewardsShopforID());
    }
  }

  /**
   * @param currentShopforItems
   * @param repositoryItem
   * @param repository
   * @param existingRewardsShopforItemIndex
   */
  private void populateExistingShopforItem(final List<RewardsShopforItem> currentShopforItems,
      MutableRepositoryItem repositoryItem, MutableRepository repository,
      final int existingRewardsShopforItemIndex) throws DigitalAppException {
    final RewardsShopforItem rewardsShopforItem = currentShopforItems
        .get(existingRewardsShopforItemIndex);

    //shopfor_name
    repositoryItem.setPropertyValue(SHOPFOR_NAME_PROPERTY, rewardsShopforItem.getShopforName());
    //relationship
    repositoryItem
        .setPropertyValue(SHOPFOR_RELATIONSHIP_PROPERTY, rewardsShopforItem.getRelationship());
    //birth_day
    repositoryItem.setPropertyValue(SHOPFOR_BIRTHDAY_PROPERTY, rewardsShopforItem.getBirthDay());
    //birth_month
    repositoryItem
        .setPropertyValue(SHOPFOR_BIRTHMONTH_PROPERTY, rewardsShopforItem.getBirthMonth());
    //web_type
    repositoryItem.setPropertyValue(SHOPFOR_WEBTYPE_PROPERTY, rewardsShopforItem.getWebType());
    //gender
    repositoryItem.setPropertyValue(SHOPFOR_GENDER_PROPERTY, rewardsShopforItem.getGender());
    //size_group
    repositoryItem
        .setPropertyValue(SHOPFOR_SIZEGROUP_PROPERTY, rewardsShopforItem.getSizeGroup());
    //dsw_user_shopfor_size - dsw_shopfor_size
    if (rewardsShopforItem.getSizes() != null && rewardsShopforItem.getSizes().length > 0) {
      repositoryItem.setPropertyValue(SHOPFOR_SIZES_PROPERTY,
          getSizeRepositoryItems(rewardsShopforItem.getSizes(), repository,
              rewardsShopforItem.getGender()));
    }
    //dsw_user_shopfor_width - dsw_shopfor_width
    if (rewardsShopforItem.getWidths() != null && rewardsShopforItem.getWidths().length > 0) {
      repositoryItem.setPropertyValue(SHOPFOR_WIDTHS_PROPERTY,
          getWidthRepositoryItems(rewardsShopforItem.getWidths(), repository));
    } else {
      repositoryItem.setPropertyValue(SHOPFOR_WIDTHS_PROPERTY, null);
    }

  }

  /**
   * Get Size Repository Items from the lookup table and return
   *
   * @param sizes
   * @param profileRepository
   * @param gender
   * @return List
   * @throws DigitalAppException
   */
  private static List<RepositoryItem> getSizeRepositoryItems(String[] sizes,
      MutableRepository profileRepository, String gender) throws DigitalAppException {
    try {
      RepositoryItemDescriptor sizeItemDesc = profileRepository
          .getItemDescriptor(SHOPFOR_SIZES_ITEM_DESCRIPTOR);
      RepositoryView sizeView = sizeItemDesc.getRepositoryView();
      QueryBuilder sizeBuilder = sizeView.getQueryBuilder();
      QueryExpression sizeProperty = sizeBuilder
          .createPropertyQueryExpression(SHOPFOR_SIZE_SHOPFORSIZE_PROPERTY);
      QueryExpression genderProperty = sizeBuilder
          .createPropertyQueryExpression(SHOPFOR_SIZE_SHOPFORGENDER_PROPERTY);
      List<Query> queries = new ArrayList<>();
      List<RepositoryItem> finalItems = new ArrayList<>();

      for (String size : sizes) {
        //Size
        QueryExpression sizeValue = sizeBuilder.createConstantQueryExpression(size);
        Query sizeQuery = sizeBuilder
            .createComparisonQuery(sizeProperty, sizeValue, QueryBuilder.EQUALS);

        if (DigitalStringUtil.isBlank(gender)) { // This should not happen as Gender is required field
          logger.error("Gender is Null or Blank");
        }

        //Gender
        QueryExpression genderValue = sizeBuilder.createConstantQueryExpression(gender);
        Query genderQuery = sizeBuilder
            .createComparisonQuery(genderProperty, genderValue, QueryBuilder.EQUALS);

        //Size AND Gender
        Query query = sizeBuilder.createAndQuery(new Query[]{sizeQuery, genderQuery});
        queries.add(query);
      }

      executeQuery(sizeView, sizeBuilder, queries, finalItems);

      return finalItems;

    } catch (RepositoryException e) {
      logger
          .error("Error occured while getting Size items from Repository - getSizeRepositoryItems");
      throw new DigitalAppException(e.getMessage());
    }
  }

  /**
   * @param sizeView
   * @param sizeBuilder
   * @param queries
   * @param finalItems
   * @throws RepositoryException
   */
  private static void executeQuery(RepositoryView sizeView, QueryBuilder sizeBuilder,
      List<Query> queries, List<RepositoryItem> finalItems) throws RepositoryException {
    if (queries.size() > 0) {
      Query[] queriesArray = new Query[queries.size()];
      queriesArray = queries.toArray(queriesArray);
      Query finalQuery = sizeBuilder.createOrQuery(queriesArray);
      RepositoryItem[] queryRepositoryItems = sizeView.executeQuery(finalQuery);
      if (queryRepositoryItems != null) {
        Collections.addAll(finalItems, queryRepositoryItems);
      }
    }
  }

  /**
   * Get Width repository Items from the lookup table and return
   *
   * @param widths
   * @param profileRepository
   * @return List
   * @throws DigitalAppException
   */
  private static List<RepositoryItem> getWidthRepositoryItems(String[] widths,
      MutableRepository profileRepository)
      throws DigitalAppException {
    try {
      RepositoryItemDescriptor widthItemDesc = profileRepository
          .getItemDescriptor(SHOPFOR_WIDTHS_ITEM_DESCRIPTOR);
      RepositoryView widthView = widthItemDesc.getRepositoryView();
      QueryBuilder widthBuilder = widthView.getQueryBuilder();
      QueryExpression widthProperty = widthBuilder
          .createPropertyQueryExpression(SHOPFOR_WIDTH_SHOPFORWIDTH_PROPERTY);
      List<Query> queries = new ArrayList<>();
      List<RepositoryItem> finalItems = new ArrayList<>();

      for (String width : widths) {
        QueryExpression widthValue = widthBuilder.createConstantQueryExpression(width);
        Query query = widthBuilder
            .createComparisonQuery(widthProperty, widthValue, QueryBuilder.EQUALS);
        queries.add(query);
      }

      executeQuery(widthView, widthBuilder, queries, finalItems);

      return finalItems;

    } catch (RepositoryException e) {
      logger.error(
          "Repository Exception occurred while getting the Width repository items - getWidthRepositoryItems");
      throw new DigitalAppException(e.getMessage());

    }
  }

  /**
   * @return Profile
   */
  private Profile getProfile() {
    return ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PROFILE, Profile.class);
  }

}


