package com.digital.commerce.services.order.listner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.payload.GiftCardRefundShipmentPayload;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderlineAttributesDTO;

public class DigitalGiftCardRefundShipmentListner extends DigitalOrderStatusListener {
	protected static final String CLASSNAME = DigitalGiftCardRefundShipmentListner.class.getName();

	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

	@Override
	protected String getOrderNo(OrderStatusPayload payLoad) {
		if (payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null) {
			return payLoad.getOmniOrderUpdate().getOrder().getOrderNo();
		}
		return null;
	}

	@Override
	protected String getEmailTo(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((GiftCardRefundShipmentPayload) payLoad).getEmailId();
	}

	@Override
	protected List<OrderLine> getOrderLines(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((GiftCardRefundShipmentPayload) payLoad).getOrderLines();
	}

	@Override
	protected HashMap<String, Object> handleEmailNotification(OrderStatusPayload payLoad, DigitalOrderImpl order,
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {

		HashMap<String, Object> extraAttributes = new HashMap<>();

		GiftCardRefundShipmentPayload giftCardRefundShipmentPayload = (GiftCardRefundShipmentPayload) payLoad;

		String customerFirstName = giftCardRefundShipmentPayload.getPersonInfoBillTo().getFirstName();

		if (DigitalStringUtil.isEmpty(customerFirstName)) {
			customerFirstName = giftCardRefundShipmentPayload.getPersonInfoShipTo().getFirstName();
		}

		extraAttributes.put("customerFirstName", customerFirstName);

		HashMap<String, Object> shippingAddress = new HashMap<>();
		// get Shipping Address Details
		shippingAddress.put("firstName", giftCardRefundShipmentPayload.getPersonInfoShipTo().getFirstName());
		shippingAddress.put("company", giftCardRefundShipmentPayload.getPersonInfoShipTo().getCompany());
		shippingAddress.put("addressLine1", giftCardRefundShipmentPayload.getPersonInfoShipTo().getAddressLine1());
		shippingAddress.put("addressLine2", giftCardRefundShipmentPayload.getPersonInfoShipTo().getAddressLine2());
		shippingAddress.put("city", giftCardRefundShipmentPayload.getPersonInfoShipTo().getCity());
		shippingAddress.put("state", giftCardRefundShipmentPayload.getPersonInfoShipTo().getState());
		shippingAddress.put("zipCode", giftCardRefundShipmentPayload.getPersonInfoShipTo().getZipCode());
		shippingAddress.put("country", giftCardRefundShipmentPayload.getPersonInfoShipTo().getCountry());
		shippingAddress.put("dayPhone", giftCardRefundShipmentPayload.getPersonInfoShipTo().getDayPhone());
		shippingAddress.put("trackingNo", giftCardRefundShipmentPayload.getTrackingNo());

		extraAttributes.put("shippingAddress", shippingAddress);

		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOPIS, itemsByFullfilmentType, extraAttributes);
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOSTS, itemsByFullfilmentType, extraAttributes);

		// get mall plaza name
		HashMap<String, OrderlineAttributesDTO> lines = itemsByFullfilmentType.get("SHIP");

		if (lines != null) {
			Map.Entry<String, OrderlineAttributesDTO> entry = lines.entrySet().iterator().next();
			OrderlineAttributesDTO orderLine = entry.getValue();
			if (orderLine != null) {
				String carrierName = DigitalStringUtil.isNotBlank(giftCardRefundShipmentPayload.getScac())?
						giftCardRefundShipmentPayload.getScac().trim() : giftCardRefundShipmentPayload.getScac();
				orderLine.setNarwarUrl(getDigitalConstants().getNarwarUrl() + carrierName
						+ "?tracking_numbers=" + giftCardRefundShipmentPayload.getTrackingNo());
			}
		}
		return extraAttributes;

	}
}
