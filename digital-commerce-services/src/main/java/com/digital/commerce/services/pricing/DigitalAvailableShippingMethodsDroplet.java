package com.digital.commerce.services.pricing;
 
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.commerce.pricing.ShippingPricingEngine;
import atg.commerce.promotion.PromotionUpsellTools;
import atg.dtm.UserTransactionDemarcation;
import atg.nucleus.naming.ComponentName;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.HttpServletUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.inventory.YantraService.ServiceMethod;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShippingGroupPropertyManager;
import com.digital.commerce.services.utils.DigitalJSONUtil;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalAvailableShippingMethodsDroplet extends DynamoServlet {
 
	private static final String CLASSNAME = DigitalAvailableShippingMethodsDroplet.class.getName();

	static final ParameterName OUTPUT = ParameterName.getParameterName("output");

	static final ParameterName ERROR = ParameterName.getParameterName("error");

	static final String CLASS_NAME = "DigitalAvailableShippingMethodsDroplet";

	static final String AVAILABLE_SHIPPING_METHODS = "availableShippingMethods";

	static final String MY_RESOURCE_NAME = "atg.commerce.pricing.Resources";

	private ShippingPricingEngine shippingPricingEngine;

	private ComponentName profilePath;

	private ComponentName userPricingModelsPath;

	private DigitalShippingGroupManager shippingGroupManager;

	Locale defaultLocale;

	boolean mUseRequestLocale;

	private PromotionUpsellTools promotionUpsellTools;

	private PricingModelHolder userPricingModel;

	private PricingTools pricingTools;
	
	private TransactionUtils transactionUtils;

	private static final ParameterName FILTER = ParameterName.getParameterName("filterEmptyShippingMethods");
	private MessageLocator messageLocator;

	/**
	 * 
	 * @param pProfilePath
	 */
	public void setProfilePath(String pProfilePath) {
		if (pProfilePath != null) {
			this.profilePath = ComponentName.getComponentName(pProfilePath);
		} else {
			this.profilePath = null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String getProfilePath() {
		if (this.profilePath != null) {
			return this.profilePath.getName();
		}
		return null;
	}

	/**
	 * 
	 * @param pUserPricingModelsPath
	 */
	public void setUserPricingModelsPath(String pUserPricingModelsPath) {
		if (pUserPricingModelsPath != null) {
			this.userPricingModelsPath = ComponentName.getComponentName(pUserPricingModelsPath);
		} else {
			this.userPricingModelsPath = null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String getUserPricingModelsPath() {
		if (this.userPricingModelsPath != null) {
			return this.userPricingModelsPath.getName();
		}
		return null;
	}

    /**
     * 
     * @param pRequest
     * @param pResponse
     */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String METHOD_NAME = "service";
		final String SHIP_OPTIONS_UNAVAILABLE="SHIP_OPTIONS_UNAVAILABLE";
		Boolean filter = true;
		UserTransactionDemarcation td = null;
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
		try {
			td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);

			List availableMethods = null;
			OrderHolder shoppingCart = (OrderHolder) pRequest.resolveName("/atg/commerce/ShoppingCart");
			Order order = shoppingCart.getCurrent();
			AvailableShippingMethodsResponse availableShippingMethodsResponse = new AvailableShippingMethodsResponse();
			if (getShippingPricingEngine() != null) {
				Collection pricingModels = getPricingModels(pRequest);
				RepositoryItem profile = getProfile(pRequest);
				Locale locale = HttpServletUtil.getUserLocale(pRequest);
				try {
					for (ShippingGroup sg : (List<ShippingGroup>) order.getShippingGroups()) {
						if (sg instanceof HardgoodShippingGroup) {

							HardgoodShippingGroup hgSg = (HardgoodShippingGroup) sg;
							String shipType = (String) hgSg
									.getPropertyValue(ShippingGroupPropertyManager.SHIP_TYPE.getValue());

							if (ShippingGroupConstants.ShipType.SHIP.getValue().equalsIgnoreCase(shipType)
									&& order.getCommerceItemCount() > 0) {
								Map paramMap=new HashMap<>();
								paramMap.put("availableShippingMethodsResponse", availableShippingMethodsResponse);
								availableMethods = getShippingPricingEngine().getAvailableMethods(sg, pricingModels,
										locale, profile, paramMap);
							}
						}
					}
				} catch (PricingException exc) {
					if (isLoggingError()) {
						logError(exc);
					}
				}
			}
			
			if(availableShippingMethodsResponse.isShippingOptionsUnavailable()){
				List warningMessages=availableShippingMethodsResponse.getWarningMessages();
				warningMessages.add(messageLocator.getMessageString(SHIP_OPTIONS_UNAVAILABLE));
				availableShippingMethodsResponse.setWarningMessages(warningMessages);
			}
			Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> availableMethodsMap = new HashMap<>();
			if (null != availableMethods && availableMethods.size() > 0) {
				availableMethodsMap = (Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>>) availableMethods.get(0);
			}
			if (availableMethodsMap != null && !availableMethodsMap.isEmpty()) {
				availableMethodsMap = repriceShipping(availableMethodsMap, pRequest, order);
			}
			if (pRequest.getParameter(FILTER) != null
					&& "false".equalsIgnoreCase((String) pRequest.getParameter(FILTER))) {
				filter = false;
			}
			Map convertToJSONArrayFromMap = DigitalJSONUtil.convertToJSONArrayFromMap(availableMethodsMap, "shippingmethod",
					"itemList", "ShippingMethod", filter);

			List closenessQualifiers = promotionUpsellTools.compileShippingClosenessQualifiers(order);
			List<String> closenessQualifierMessages = new ArrayList<>();
			if (null != closenessQualifiers) {
				for (Object obj : closenessQualifiers) {
					String name = (String) ((RepositoryItem) obj).getPropertyValue("name");
					closenessQualifierMessages.add(name);
				}
			}
			availableShippingMethodsResponse.setClosenessQualifiers(closenessQualifierMessages);
			availableShippingMethodsResponse.setShippingMethodOptions(convertToJSONArrayFromMap);
			availableShippingMethodsResponse.setShippingMethodsInfos(availableMethodsMap);
			pRequest.setParameter("response", availableShippingMethodsResponse);
			pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
			return;
		} catch (Exception ex) {
			this.logError("Error DigitalAvailableShippingMethodsDroplet :: ", ex);
			TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
		} finally {
			
			TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
			TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
		}
	}
   
   /**
    * 
    * @param availableMethodsMap
    * @param pRequest
    * @param order
    * @return
    * @throws ServletException
    * @throws IOException
    * @throws CommerceException
    */
	private Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> repriceShipping(
			Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> availableMethodsMap,
			DynamoHttpServletRequest pRequest, Order order) throws ServletException, IOException, CommerceException {
		Iterator<Map.Entry<ShippingMethodInfo, List<CommerceItemInfoWrapper>>> entries = availableMethodsMap.entrySet()
				.iterator();
		Map<ShippingMethodInfo, List<CommerceItemInfoWrapper>> availableRepricedMethodsMap = new HashMap<>();
		List<ShippingMethodInfo> shippingMethodInfoList = new ArrayList<>();
		List<CommerceItemInfoWrapper> commerceItemDefaultInfoList = new ArrayList<>();
		List<CommerceItemInfoWrapper> commerceItemNDAInfoList = new ArrayList<>();
		List<CommerceItemInfoWrapper> commerceItemSDInfoList = new ArrayList<>();
		while (entries.hasNext()) {
			Map.Entry<ShippingMethodInfo, List<CommerceItemInfoWrapper>> entry = entries.next();
			ShippingMethodInfo shippingMethodInfo = entry.getKey();
			shippingMethodInfoList.add(shippingMethodInfo);
			if (ShippingGroupConstants.DEFAULT_SHIPPING_METHOD
					.equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
				commerceItemDefaultInfoList = entry.getValue();
			}
			if (ShippingGroupConstants.NEXT_DAY_SHIPPING_METHOD
					.equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
				commerceItemNDAInfoList = entry.getValue();
			}
			if (ShippingGroupConstants.SECOND_DAY_SHIPPING_METHOD
					.equalsIgnoreCase(shippingMethodInfo.getShippingMethod())) {
				commerceItemSDInfoList = entry.getValue();
			}
		}

		String selectedShippingMethod = shippingGroupManager.getShippingMethod(order);
		ShippingGroup sg = findFirstNonGiftCardShippingGroup(order);
		List<ShippingRate> shippingRateList = repriceShippingGroup(sg, order, HttpServletUtil.getUserLocale(pRequest),
				shippingMethodInfoList, selectedShippingMethod, pRequest);
		@SuppressWarnings("unused")
		ShippingMethodInfo grndSHMethod = null;
		for (ShippingRate shippingRate : shippingRateList) {
			ShippingMethodInfo shMethod = new ShippingMethodInfo();
			shMethod.setBaseRate(shippingRate.getBaseRate());
			shMethod.setShippingMethod(shippingRate.getBaseRateServiceName());
			List<CommerceItemInfoWrapper> value = new ArrayList<>();
			if (ShippingGroupConstants.DEFAULT_SHIPPING_METHOD.equalsIgnoreCase(shMethod.getShippingMethod())) {
				value = commerceItemDefaultInfoList;
				grndSHMethod = shMethod;
			}
			if (ShippingGroupConstants.NEXT_DAY_SHIPPING_METHOD.equalsIgnoreCase(shMethod.getShippingMethod())) {
				value = commerceItemNDAInfoList;
			}
			if (ShippingGroupConstants.SECOND_DAY_SHIPPING_METHOD.equalsIgnoreCase(shMethod.getShippingMethod())) {
				value = commerceItemSDInfoList;
			}
			availableRepricedMethodsMap.put(shMethod, value);
		}

		for (CommerceItemInfoWrapper ciInfoWrapper : commerceItemDefaultInfoList) {
			boolean isGRNOnly = true;
			for (CommerceItemInfoWrapper ciInfoNDAWrapper : commerceItemNDAInfoList) {
				if (ciInfoNDAWrapper.getId().equals(ciInfoWrapper.getId())) {
					isGRNOnly = false;
					break;
				}
			}
			if (isGRNOnly) {
				for (CommerceItemInfoWrapper ciInfoSDWrapper : commerceItemSDInfoList) {
					if (ciInfoSDWrapper.getId().equals(ciInfoWrapper.getId())) {
						isGRNOnly = false;
						break;
					}
				}
			}
			RepositoryItem profile = getProfile(pRequest);
			setGroundShippingFlag(isGRNOnly, ciInfoWrapper, order,profile);
		}

		return availableRepricedMethodsMap;
	}
  
	/**
	 * 
	 * @param grndShippingFlag
	 * @param ciInfoWrapper
	 * @param order
	 * @throws CommerceException
	 */
	private void setGroundShippingFlag(boolean grndShippingFlag, CommerceItemInfoWrapper ciInfoWrapper, Order order,RepositoryItem profile)
			throws CommerceException {
		List<DigitalCommerceItem> lineItems = order.getCommerceItems();
		boolean updateOrderFlag = false;
		for (DigitalCommerceItem lineItem : lineItems) {
			List<ShippingGroupCommerceItemRelationship> shippingGroupRelationships = (List<ShippingGroupCommerceItemRelationship>) lineItem
					.getShippingGroupRelationships();
			for (ShippingGroupCommerceItemRelationship sgr : shippingGroupRelationships) {
				ShippingGroup sg = sgr.getShippingGroup();
				if (sg instanceof HardgoodShippingGroup) {
					String shipType = (String) ((HardgoodShippingGroup) sg).getPropertyValue("shipType");
					if ((null == shipType || "ship".equalsIgnoreCase(shipType))
							&& lineItem.getCatalogRefId().equals(ciInfoWrapper.getId())) {
						if(grndShippingFlag != lineItem.getGroundShippingOnly()){
							lineItem.setGroundShippingOnly(grndShippingFlag);
							updateOrderFlag = true;
						}
					}
				}
			}
		}
		if (updateOrderFlag) {
			synchronized (order) {
				pricingTools.getOrderManager().updateOrder(order);
			}
		}
	}
   
	/** 
	 * Reprices the entire shipping group for all available shipping rates. If a
	 * shipping group has a current shipping method, make sure to reprice it last
	 * so that the order is left in its original state.
	 *
	 * @param sg
	 * @param order
	 * @param locale
	 * @param shippingMethods
	 * @param selectedShippingMethod
	 * @param request
	 * @return List
	 * @throws ServletException
	 * @throws IOException
	 */
	private List<ShippingRate> repriceShippingGroup( ShippingGroup sg, Order order, Locale locale, List shippingMethods, String selectedShippingMethod, DynamoHttpServletRequest request ) throws ServletException, IOException {
		List<ShippingRate> modifiedShippingOptions = new ArrayList<>();
		List sortedShippingMethods = sortShippingMethods( shippingMethods, selectedShippingMethod );
		for( Iterator iterator = sortedShippingMethods.iterator(); iterator.hasNext(); ) {
			String shippingMethod = (String)iterator.next();
			sg.setShippingMethod( shippingMethod ); 
			modifiedShippingOptions.add( createShippingGroupShippingRate( order, locale, request ) );
		}
		return modifiedShippingOptions;
	}

	/**
	 * 
	 * @param shippingMethods
	 * @param selectedShippingMethod
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	private List sortShippingMethods( List shippingMethods, final String selectedShippingMethod ) throws ServletException, IOException {
		List sortedShippingMethods = new ArrayList();
		ShippingMethodInfo shippingMethodInfo = null;
		String shippingMethod = null;
		for( Iterator iterator = shippingMethods.iterator(); iterator.hasNext(); ) {
			shippingMethodInfo = (ShippingMethodInfo)iterator.next();
			shippingMethod = shippingMethodInfo.getShippingMethod();
			if( selectedShippingMethod == null || !selectedShippingMethod.equalsIgnoreCase( shippingMethod ) ) {
				sortedShippingMethods.add( shippingMethod );
			}
		}
		if( null!=selectedShippingMethod && DigitalStringUtil.isNotBlank( selectedShippingMethod ) && selectedShippingMethod.length() == 3 ) {
			sortedShippingMethods.add( selectedShippingMethod );
		}
		return sortedShippingMethods;
	}

	/**
	 * 
	 * @param order
	 * @param locale
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	private ShippingRate createShippingGroupShippingRate( Order order, Locale locale, DynamoHttpServletRequest request ) throws ServletException, IOException {
		ShippingRate retVal = new ShippingRate();
		final ShippingGroup shippingGroup = findFirstNonGiftCardShippingGroup( order );
		if( shippingGroup != null ) {
			priceShipping( order, locale, request );
			if( shippingGroup.getPriceInfo() != null ) {
				retVal.setBaseRate( shippingGroup.getPriceInfo().getAmount() );
				retVal.setValid( true );
			} else {
				retVal.setValid( false );
			}
			retVal.setBaseRateServiceLevel( shippingGroup.getShippingMethod() );
			retVal.setBaseRateServiceName( shippingGroup.getShippingMethod() );
		}
		return retVal;
	}

	/** 
	 * Ignores {@link PricingException}s. Reprices the shipping only. It returns the total for all of the shipping,
	 * but the shipping group should be inspected to determine what the price really is. This should
	 * not affect tax, discounts, or order total.
	 * 
	 * @param order
	 * @param locale 
	 */
	private double priceShipping( Order order, Locale locale, DynamoHttpServletRequest request) throws ServletException, IOException {
		double shipping = 0.0;
		try {
			synchronized( order ) {
				//Triggering  priceShippingForOrderTotal to compute the shipping cost.
				shipping = pricingTools.priceShippingForOrderTotal(order, userPricingModel, locale, getProfile(request), null);
			}
		} catch (PricingException pex) {
		}
		return shipping;
	}
	
	/**
	 * 
	 * @param pRequest
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	private RepositoryItem getProfile(DynamoHttpServletRequest pRequest) throws ServletException, IOException {
		final String METHOD_NAME = "getProfile";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
		try {
			RepositoryItem profile = null;
			if (this.profilePath != null)
				profile = (RepositoryItem) pRequest.resolveName(this.profilePath);
			return profile;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
		}
	}

	/**
	 * 
	 * @param pRequest
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	private Collection getPricingModels(DynamoHttpServletRequest pRequest) throws ServletException, IOException {
		final String METHOD_NAME = "getPricingModels";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
		try {
			Collection models = null;
			if ((models == null) && (this.userPricingModelsPath != null)) {
				userPricingModel = (PricingModelHolder) pRequest.resolveName(this.userPricingModelsPath);
				if (userPricingModel != null)
					models = userPricingModel.getShippingPricingModels();
			}
			return models;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASS_NAME, METHOD_NAME);
		}
	}
   
	/**
	 * 
	 * @return the promotionUpsellTools
	 */
	public PromotionUpsellTools getPromotionUpsellTools() {
		return promotionUpsellTools;
	}
	
	/**
	 * 
	 * @param promotionUpsellTools the promotionUpsellTools to set
	 */
	public void setPromotionUpsellTools(PromotionUpsellTools promotionUpsellTools) {
		this.promotionUpsellTools = promotionUpsellTools;
	}
  
	/**
	 * 
	 * @param order
	 * @return
	 */
	public ShippingGroup findFirstNonGiftCardShippingGroup( Order order ) {
		ShippingGroup retVal = null;
		List shippingGroupList = order.getShippingGroups();
		if( shippingGroupList != null ) {
			for( int i = 0; retVal == null && i < shippingGroupList.size(); i++ ) {
				ShippingGroup sg = (ShippingGroup)shippingGroupList.get( i );
				if( sg instanceof HardgoodShippingGroup && !( shippingGroupManager.isGiftCardShippingGroup( sg ) ) ) {
					HardgoodShippingGroup hsg = (HardgoodShippingGroup)sg;
					String shipType = (String)hsg.getPropertyValue("shipType");
					if ( shipType == null || shipType.equalsIgnoreCase("ship")) {
						retVal = sg;
					}
				}
			}
		}
		return retVal;
	}
 }