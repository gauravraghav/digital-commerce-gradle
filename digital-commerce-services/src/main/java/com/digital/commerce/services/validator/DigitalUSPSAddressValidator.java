package com.digital.commerce.services.validator;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.addressvalidation.AddressValidationService;
import com.digital.commerce.integration.addressvalidation.AddressValidationService.ServiceMethod;
import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceRequest;
import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceResponse;
import com.digital.commerce.services.common.DigitalContactInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DigitalUSPSAddressValidator extends GenericService {

  private AddressValidationService addressValidationService;

  /**
   * @param address
   * @return AddressValidationServiceResponse
   * @throws DigitalIntegrationBusinessException
   */
  public AddressValidationServiceResponse validateAddress(DigitalContactInfo address)
      throws DigitalIntegrationBusinessException {
    final String METHOD_NAME = "validateAddress";
    AddressValidationServiceResponse addressValidationServiceResponse = new AddressValidationServiceResponse();
    try {
      DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
      if (addressValidationService != null && (addressValidationService).isServiceEnabled()
          && (addressValidationService)
          .isServiceMethodEnabled(ServiceMethod.VALIDATE.getServiceMethodName())) {
        addressValidationServiceResponse = addressValidationService
            .validate(getAddressValidationServiceRequest((DigitalContactInfo) address));
        if (this.isLoggingDebug()) {
          logDebug(
              "Address Line2 from Response" + addressValidationServiceResponse.getAddressLine2());
          logDebug(
              "Address Line1 from Response" + addressValidationServiceResponse.getAddressLine1());
          logDebug("City from Response" + addressValidationServiceResponse.getCity());
          logDebug("State from Response" + addressValidationServiceResponse.getState());
          logDebug("Zip from Response" + addressValidationServiceResponse.getZip5());
        }
        addressValidationServiceResponse.setSuccess(true);
      } else {
        //throw new DigitalAppException("AddressValidation.USPSDown.Address.Accepted");
      }
    } catch (DigitalIntegrationException e) {
      addressValidationServiceResponse.setSuccess(false);
      logError(e.getMessage(), e);
    } finally {
      DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
    }
    return addressValidationServiceResponse;
  }

  /**
   * @param address
   * @return AddressValidationServiceRequest
   */
  private AddressValidationServiceRequest getAddressValidationServiceRequest(
      DigitalContactInfo address) {
    AddressValidationServiceRequest addressValidationServiceRequest = new AddressValidationServiceRequest();
    if (!StringUtils.isBlank(address.getAddress1())) {
      addressValidationServiceRequest.setAddressLine1(DigitalStringUtil.trim(address
          .getAddress1()));
    }
    if (!StringUtils.isBlank(address.getAddress2())) {
      addressValidationServiceRequest.setAddressLine2(DigitalStringUtil.trim(address
          .getAddress2()));
    }
    if (!StringUtils.isBlank(address.getCity())) {
      addressValidationServiceRequest.setCity(DigitalStringUtil.trim(address.getCity()));
    }
    if (!StringUtils.isBlank(address.getState())) {
      addressValidationServiceRequest.setState(DigitalStringUtil.trim(address.getState()));
    }
    if (!StringUtils.isBlank(address.getPostalCode())) {
      if (address.getPostalCode().contains("-")) {
        String[] zipCodes = address.getPostalCode().split("-");
        if (null != zipCodes && zipCodes.length >= 2) {
          addressValidationServiceRequest.setZip5(DigitalStringUtil.trim(zipCodes[0]));
          addressValidationServiceRequest.setZip4(DigitalStringUtil.trim(zipCodes[1]));
        }
      }
      if (StringUtils.isBlank(addressValidationServiceRequest.getZip5())) {
        addressValidationServiceRequest.setZip5(DigitalStringUtil.trim(address.getPostalCode()));
      }
    }
    return addressValidationServiceRequest;
  }

}
