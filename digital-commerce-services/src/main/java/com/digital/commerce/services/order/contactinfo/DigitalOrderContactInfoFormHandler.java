package com.digital.commerce.services.order.contactinfo;

import java.util.List;

import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.purchase.PurchaseProcessConfiguration;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.ContactInfo;
import atg.droplet.DropletException;
import atg.dtm.UserTransactionDemarcation;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.Address;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.common.DigitalGenericFormHandler;
import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.payment.afterpay.AfterPayPayment;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.validator.DigitalBasicAddressValidator;
import lombok.Getter;
import lombok.Setter;

/**
 * @author SK402315
 *
 */
@Getter
@Setter
public class DigitalOrderContactInfoFormHandler extends DigitalGenericFormHandler {
	protected static final String CLASSNAME = DigitalOrderContactInfoFormHandler.class.getName();

	private DigitalBasicAddressValidator digitalBasicAddressValidator;
	private MessageLocator messageLocator;
	protected PurchaseProcessConfiguration configuration;
	protected Address contactAddress = new Address();
	protected ContactInfo dswContactInfo = new ContactInfo();
	private boolean receiveEmailOffers = false;
	private RepeatingRequestMonitor repeatingRequestMonitor;
	private DigitalShippingGroupManager digitalShippingGroupManager;
	protected static final String CREDITCARD_BILLING_ADDRESS_MISSING = "creditCardBillingAddressMissingError";
	protected static final String PAYPAL_BILLING_ADDRESS_MISSING = "paypalBillingAddressMissingError";
	protected static final String ADD_ORDERCONTACT_INFO_EXCEPTION = "addOrderContactInfoException";
	protected static final String SHIPPING_ADDRESS_MISSING = "shippingAddressMissing";
	private DigitalOrderContactInfoManager digitalOrderContactInfoManager;



	/**
	 * Method to add contactinfo to the billing address associated with the
	 * order
	 * 
	 * @param request
	 * @param response
	 * @throws DigitalAppException
	 */
	public void handleAddOrUpdateContactInfo(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws DigitalAppException {
		final String METHOD_NAME = "DigitalOrderContactInfoFormHandler.handleAddOrUpdateContactInfo";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(METHOD_NAME))) {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			// Process the request and populate the request in BillingContact
			DigitalContactInfo dswContactInfo = processRequest();

			List<String> lstValidationErrors = getDigitalBasicAddressValidator().validateContactInfo(dswContactInfo);

			logInfo("Basic Validation Errors for contactinfo" + lstValidationErrors);

			if (lstValidationErrors.size() > 0) {
				for (String errorMessage : lstValidationErrors) {
					addFormException(new DropletException(this.getMessageLocator().getMessageString(errorMessage),
							errorMessage));
				}
			} else {
				UserTransactionDemarcation td = null;
				try {
					td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
					ContactInfo billingAddr = null;
					// These rest services are stateful. So we can get the
					// current Order
					DigitalOrderImpl order = (DigitalOrderImpl) getOrder();

					// set the optIn flag into the transient variable.
					// This is used in DigitalCommitOrderFormHandler to call Rewards
					// for Anonymous opt-in/opt-out
					if(null!=order){
					order.setEmailOptIn(isReceiveEmailOffers());
					}
					// After Successful USPS call set the ContactInfo to the
					// Order Object
					@SuppressWarnings("unchecked")
					List<PaymentGroup> listPaymentGroups = order.getPaymentGroups();

					if (listPaymentGroups != null) {
						
						if(listPaymentGroups.isEmpty()){

							//TODO: Add default payment group; i.e CreditCard on order
							PaymentGroup pg=getConfiguration().getPaymentGroupManager().createPaymentGroup();
							getConfiguration().getPaymentGroupManager().addPaymentGroupToOrder(getOrder(),pg);
							listPaymentGroups = order.getPaymentGroups();

							if(listPaymentGroups.isEmpty()) {
							addFormException(new DropletException(
									this.getMessageLocator()
												.getMessageString("addOrderContactInfoException"), "PAYMENT_GROUP_MISSING"));
							}
						}
						
						for (PaymentGroup paymentGroup : listPaymentGroups) {

							if (paymentGroup instanceof CreditCard) {
								// get the payment group if its exists
								CreditCard creditCard = (CreditCard) paymentGroup;
								billingAddr = (ContactInfo) creditCard.getBillingAddress();
								if (billingAddr != null) {
									billingAddr.setPhoneNumber(dswContactInfo.getPhoneNumber());
									if (DigitalStringUtil.isNotEmpty(dswContactInfo.getEmail())) {
										billingAddr.setEmail(DigitalStringUtil.lowerCase(dswContactInfo.getEmail()));
									}
									((CreditCard) paymentGroup).setBillingAddress(billingAddr);
								} else {
									addFormException(new DropletException(
											this.getMessageLocator()
													.getMessageString(CREDITCARD_BILLING_ADDRESS_MISSING),
											CREDITCARD_BILLING_ADDRESS_MISSING));
								}
							} else if (paymentGroup instanceof PaypalPayment) {
								PaypalPayment paypalPayment = (PaypalPayment) paymentGroup;
								billingAddr = (ContactInfo) paypalPayment.getBillingAddress();
								if (billingAddr != null) {
									billingAddr.setPhoneNumber(dswContactInfo.getPhoneNumber());
									if (DigitalStringUtil.isNotEmpty(dswContactInfo.getEmail())) {
										billingAddr.setEmail(DigitalStringUtil.lowerCase(dswContactInfo.getEmail()));
									}
									((PaypalPayment) paymentGroup).setBillingAddress(billingAddr);
								} else {
									addFormException(new DropletException(
											this.getMessageLocator().getMessageString(PAYPAL_BILLING_ADDRESS_MISSING),
											PAYPAL_BILLING_ADDRESS_MISSING));
								}
							}
							else if (paymentGroup instanceof AfterPayPayment) {
								setShippingAddress(dswContactInfo, order);
							}
							else if (paymentGroup instanceof DigitalGiftCard) {
								// set contact into to all available
								// hardgoodshipping address
								setShippingAddress(dswContactInfo, order);
								// set contact into to all available gcshipping
								// address
								setGCShippingAddress(dswContactInfo, order);
							}

						}

						if (!this.getFormError()) {
							this.setDswContactInfo((ContactInfo) billingAddr);
						}

					}
				} catch (Exception ex) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
					logError(ex);
				} finally {
					TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
					if(rrm != null){
						rrm.removeRequestEntry(METHOD_NAME);
					}
				}
			}
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/**
	 * Method used to get all hardgood shipping groups and set the billing
	 * address to it.
	 * 
	 * @param contactInfo
	 * @param order
	 */
	private void setShippingAddress(ContactInfo contactInfo, Order order) {

		@SuppressWarnings("unchecked")
		List<HardgoodShippingGroup> hgShippingGroups = getDigitalShippingGroupManager().getHardgoodShippingGroups(order);
		ContactInfo shippingAddr = null;
		if (hgShippingGroups != null) {
			for (HardgoodShippingGroup hgShippingGroup : hgShippingGroups) {
				shippingAddr = (ContactInfo) hgShippingGroup.getShippingAddress();
				shippingAddr.setPhoneNumber(contactInfo.getPhoneNumber());
				if (DigitalStringUtil.isNotEmpty(contactInfo.getEmail())) {
					shippingAddr.setEmail(DigitalStringUtil.lowerCase(contactInfo.getEmail()));
				}
				hgShippingGroup.setShippingAddress(shippingAddr);
			}
		}

	}

	/**
	 * Method used to get all the gift card shipping groups and set the billing
	 * address to it.
	 * 
	 * @param contactInfo
	 * @param order
	 */
	private void setGCShippingAddress(ContactInfo contactInfo, Order order) {
		List<HardgoodShippingGroup> gcShippingGroups = getDigitalShippingGroupManager().getGiftCardShippingGroups(order);
		ContactInfo gcShippingAddr = null;
		if (gcShippingGroups != null) {
			for (HardgoodShippingGroup gcShippingGroup : gcShippingGroups) {
				gcShippingAddr = (ContactInfo) gcShippingGroup.getShippingAddress();
				gcShippingAddr.setPhoneNumber(dswContactInfo.getPhoneNumber());
				if (DigitalStringUtil.isNotEmpty(dswContactInfo.getEmail())) {
					gcShippingAddr.setEmail(DigitalStringUtil.lowerCase(dswContactInfo.getEmail()));
				}
				gcShippingGroup.setShippingAddress(gcShippingAddr);
			}
		}

	}

	/**
	 * 
	 * @return DSWContactInfo
	 */
	public DigitalContactInfo processRequest() {
		DigitalContactInfo dswContactInfo = new DigitalContactInfo();
		if (DigitalStringUtil.isNotEmpty(this.getContactAddress().getEmail())) {
			dswContactInfo.setEmail(DigitalStringUtil.lowerCase(this.getContactAddress().getEmail()));
		}
		dswContactInfo.setPhoneNumber(this.getContactAddress().getPhoneNumber());
		return dswContactInfo;
	}


	/**
	 * 
	 * @return Order
	 * @throws DigitalAppException
	 */
	private Order getOrder() throws DigitalAppException {
		return getConfiguration().getShoppingCart() != null ? getConfiguration().getShoppingCart().getCurrent() : null;
	}

}
