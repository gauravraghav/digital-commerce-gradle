package com.digital.commerce.services.order.payment.afterpay;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.core.util.ContactInfo;
import atg.core.util.Null;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.service.cache.Cache;
import atg.userprofiling.Profile;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationInactiveException;
import com.digital.commerce.integration.payment.PaymentService;
import com.digital.commerce.integration.payment.AfterPayPaymentService;
import com.digital.commerce.integration.payment.afterpay.domain.*;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.DigitalHardgoodShippingGroup;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.GiftCardCommerceItem;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.afterpay.valueobject.AfterPayConfigurationResponse;
import com.digital.commerce.services.order.payment.afterpay.valueobject.AfterPayCreateOrderRequest;
import com.digital.commerce.services.order.payment.afterpay.valueobject.AfterPayOrderResponse;
import com.digital.commerce.services.order.purchase.valueobject.DigitalOrderRequest;
import com.digital.commerce.services.profile.DigitalProfileTools;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.transaction.TransactionManager;
import lombok.Getter;
import lombok.Setter;

import javax.transaction.TransactionManager;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AfterPayCheckoutManager extends GenericService {

  static final String CLASSNAME = AfterPayCheckoutManager.class.getName();
  private static final String ORDER_UNSUCCESSFUL = "UNSUCCESSFUL";
  private static final String ORDER_SUCCESSFUL = "SUCCESSFUL";

  private static final String AFTER_PAY_LOOKUP_CONTACT_INFO_REQUIRED = "afterPayLookupContactInfoInvalid";
  private static final String ORDER_NOT_ELIGIBLE = "orderNotEligibleForAfterPay";
  private static final String AFTER_PAY_SHIPPING_INFO_REQUIRED = "afterPayLookupShippingInfoInvalid";
  private static final String AFTER_PAY_SERVICE_UNAVAILABLE = "afterPayServiceUnavailable";

  private String minimumAmount;
  private String maximumAmount;
  private String currency;
  private String numberOfPayments;
  private TransactionManager transactionManager;
  private DigitalProfileTools profileTools;
  private AfterPayPaymentService afterPayService;
  private OrderManager orderManager;
  private DigitalShippingGroupManager shippingGroupManager;
  private DigitalPaymentGroupManager paymentGroupManager;
  private Cache afterPayConfigCache;
  private DigitalAfterPayTools afterPayTools;

  /**
   * @param pOrder
   * @param confirmURL
   * @param cancelURL
   * @return DigitalAppException
   * @throws DigitalAppException
   */
  public AfterPayOrderResponse order(Order pOrder, String confirmURL, String cancelURL)
      throws DigitalAppException {
    final String METHOD_NAME = "order";
    AfterPayOrderResponse orderResponse = new AfterPayOrderResponse();
    try {
      //DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(getName(), METHOD_NAME);

      if (this.isLoggingDebug()) {
        logDebug("AfterPayCheckoutManager: order(): - using web services");
      }
      if (!getAfterPayService().isServiceEnabled() ||
          !getAfterPayService()
              .isServiceMethodEnabled(PaymentService.ServiceMethod.ORDER.getServiceMethodName())) {
        orderResponse.setSuccess(false);
        orderResponse.setStatusCode(ORDER_UNSUCCESSFUL);
        logInfo("AfterPay Order service is inactive at Platform Service");
        return orderResponse;
      }

      CreateOrderRequest afterPayOrderReq = this
          .convertOrderToRequest((DigitalOrderImpl) pOrder, confirmURL, cancelURL);
      CreateOrderResponse afterPayOrderRes = this.getAfterPayService()
          .createOrder(afterPayOrderReq);

      //Set the response
      orderResponse.setExpirationDate(afterPayOrderRes.getExpires());
      orderResponse.setStatusCode(ORDER_SUCCESSFUL);
      orderResponse.setSuccess(true);
      orderResponse.setToken(afterPayOrderRes.getToken());
    } catch (DigitalIntegrationInactiveException ex) {
      orderResponse.setSuccess(false);
      orderResponse.setStatusCode(ORDER_UNSUCCESSFUL);
      logInfo("AfterPay Order service is inactive at Platform Service", ex);
      return orderResponse;
    } catch (DigitalIntegrationException e) {
      this.logError(e);
      throw e;
    } finally {
      //DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(getName(), METHOD_NAME);
    }
    return orderResponse;
  }

  // TODO: Complete this mapping


  public AfterPayOrderResponse capturePayment(DigitalOrderImpl order,String token)
          throws DigitalAppException, CommerceException {
    final String METHOD_NAME = "createPayment";
    AfterPayOrderResponse orderResponse = new AfterPayOrderResponse();
    try {

      if (this.isLoggingDebug()) {
        logDebug("AfterPayCheckoutManager: capturePayment(): - using web services");
      }
      if (!getAfterPayService().isServiceEnabled() || !getAfterPayService().isServiceMethodEnabled(AfterPayPaymentService.ServiceMethod.CREATE_PAYMENT.getServiceMethodName())) {
        orderResponse.setSuccess(false);
        orderResponse.setStatusCode(ORDER_UNSUCCESSFUL);
        logInfo("AfterPay Order service is inactive at Platform Service");
        return orderResponse;
      }

      CapturePaymentRequest capturePaymentRequest = new CapturePaymentRequest();
      capturePaymentRequest.setMerchantReference(order.getId());
      capturePaymentRequest.setToken(token);

      Payment capturePaymentResponse = this.getAfterPayService().createPayment(capturePaymentRequest);
      //Set the response
      if(null!=capturePaymentResponse && "APPROVED".equalsIgnoreCase(capturePaymentResponse.getStatus())) {
        orderResponse.setId(capturePaymentResponse.getId());
        orderResponse.setStatusCode(capturePaymentResponse.getStatus());
        orderResponse.setSuccess(true);
        orderResponse.setToken(capturePaymentResponse.getToken());
        //TODO: Update the date as per the format we need.(capturePaymentResponse.getCreated())
        orderResponse.setCreatedDate(new Date());
        getAfterPayTools().updateOrderData(order, orderResponse, orderResponse.getToken());
      }else{
        orderResponse.setSuccess(false);
      }
    } catch (DigitalIntegrationInactiveException ex) {
      orderResponse.setSuccess(false);
      orderResponse.setStatusCode(ORDER_UNSUCCESSFUL);
      logInfo("AfterPay Capture service is inactive at Platform Service", ex);
      return orderResponse;
    } catch (DigitalIntegrationException e) {
      this.logError("capturePayment :: Exception from AfterPay service " +e.getMessage());
      throw e;
    } catch (CommerceException e) {
      this.logError(e);
      throw e;
    } catch (Exception e) {
      this.logError(e);
      orderResponse.setSuccess(false);
      orderResponse.setFormError(true);
      throw e;
    }finally {
      DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(getName(), METHOD_NAME);
    }
    return orderResponse;
  }


  /**
   * @param order
   * @param confirmURL
   * @param cancelURL
   * @return CreateOrderRequest
   * @throws DigitalAppException
   */
  private CreateOrderRequest convertOrderToRequest(DigitalOrderImpl order, String confirmURL,
      String cancelURL) throws DigitalAppException {
    CreateOrderRequest afterPayOrderReq = new CreateOrderRequest();
    afterPayOrderReq.setDescription(order.getDescription());
    afterPayOrderReq.setMerchantReference(order.getId());
    Money totalAmount = new Money();
    double amountOwe = DigitalCommonUtil.roundHalfUp(this.getAmountOwe(order));
    totalAmount.setAmount(Double.toString(amountOwe));
    totalAmount.setCurrency(order.getPriceInfo().getCurrencyCode());
    afterPayOrderReq.setTotalAmount(totalAmount);
    if (order.getTaxPriceInfo() != null) {
      Money taxAmount = new Money();
      taxAmount.setAmount(Double.toString(order.getPriceInfo().getTax()));
      taxAmount.setCurrency(order.getTaxPriceInfo().getCurrencyCode());
      afterPayOrderReq.setTaxAmount(taxAmount);
    }
    DigitalHardgoodShippingGroup dswHardgoodShippingGroup = this.getShippingInfo(order);
    if(dswHardgoodShippingGroup != null) {
      Contact shipping = new Contact();
      shipping.setName(dswHardgoodShippingGroup.getShippingAddress().getFirstName() + " "
              + dswHardgoodShippingGroup.getShippingAddress().getLastName());
      shipping.setLine1(dswHardgoodShippingGroup.getShippingAddress().getAddress1());
      shipping.setLine2(dswHardgoodShippingGroup.getShippingAddress().getAddress2());
      shipping.setSuburb(dswHardgoodShippingGroup.getShippingAddress().getCity());
      shipping.setState(dswHardgoodShippingGroup.getShippingAddress().getState());
      shipping.setPostcode(dswHardgoodShippingGroup.getShippingAddress().getPostalCode());
      shipping.setCountryCode(
              convertCountryCode(dswHardgoodShippingGroup.getShippingAddress().getCountry()));
      shipping.setPhoneNumber(
              ((ContactInfo) dswHardgoodShippingGroup.getShippingAddress()).getPhoneNumber());
      afterPayOrderReq.setShipping(shipping);
    }
    Money shippingAmount = new Money();
    shippingAmount.setAmount(Double.toString(dswHardgoodShippingGroup.getPriceInfo().getAmount()));
    shippingAmount.setCurrency(dswHardgoodShippingGroup.getPriceInfo().getCurrencyCode());
    afterPayOrderReq.setShippingAmount(shippingAmount);
    afterPayOrderReq.setItems(this.getItems(order));
    afterPayOrderReq.setBilling(this.getBillingAddressFromOrder(order));

    afterPayOrderReq.setConsumer(this.getConsumer(order, dswHardgoodShippingGroup));

    Merchant merchant = new Merchant();
    merchant.setRedirectConfirmUrl(confirmURL);
    merchant.setRedirectCancelUrl(cancelURL);
    afterPayOrderReq.setMerchant(merchant);

    return afterPayOrderReq;
  }

  /**
   * This method attempts to find the first non bopis/bosts shipping group (aka ship to home).
   * If the ship to home group has items, it is returned.  Otherwise look at the bopis/bosts
   * shipping groups.  For each group, calculate the total dollar amount of each item.  Return
   * the group that has the highest dollar amount.
   * @param order
   * @return DSWHardgoodShippingGroup
   */
  private DigitalHardgoodShippingGroup getShippingInfo(DigitalOrderImpl order) {
	  DigitalHardgoodShippingGroup dswHardgoodShippingGroup = null;
    //Find first ship to home shipping group
    ShippingGroup shippingGroup = this.getShippingGroupManager()
        .getFirstNonBopisBostsHardgoodShippingGroup(order);
    if (shippingGroup instanceof DigitalHardgoodShippingGroup ) {
      dswHardgoodShippingGroup = (DigitalHardgoodShippingGroup) shippingGroup;
    }
    //If ship to home shipping group wasn't found or had no items to ship, find a bopis/bosts shipping group
    if (dswHardgoodShippingGroup == null || shippingGroup.getCommerceItemRelationshipCount() == 0) {
      List<DigitalHardgoodShippingGroup> sgs = this.getShippingGroupManager().getHardgoodShippingGroups(order);
      double maxAmount = 0.0d;
      //for each shipping group in order...
      for(DigitalHardgoodShippingGroup sg : sgs) {
        List<ShippingGroupCommerceItemRelationship> shippingGroupCommerceItemRelationships = sg.getCommerceItemRelationships();
        double shippingGroupItemTotal = 0.0d;
        //calculate the total amount across all items
        for(ShippingGroupCommerceItemRelationship commerceItemRelationship : shippingGroupCommerceItemRelationships) {
          shippingGroupItemTotal += commerceItemRelationship.getCommerceItem().getPriceInfo().getAmount();
        }
        //keep shipping group with the highest total
        if(maxAmount < shippingGroupItemTotal) {
          maxAmount = shippingGroupItemTotal;
          dswHardgoodShippingGroup = sg;
        }
      }
    }
    return dswHardgoodShippingGroup;
  }

  /**
   * @param order
   * @return List<Item>
   */
  private List<Item> getItems(Order order) {
    List<Item> items = new ArrayList<>();
    for (Object o : order.getCommerceItems()) {
      CommerceItem commerceItem = (CommerceItem) o;
      Item item = new Item();
      item.setSku(commerceItem.getCatalogRefId());
      item.setName((String)((RepositoryItem)commerceItem.getAuxiliaryData().getProductRef()).getPropertyValue("productTitle"));
      Money price = new Money();
      price.setAmount(Double.toString(commerceItem.getPriceInfo().getAmount()));
      price.setCurrency(commerceItem.getPriceInfo().getCurrencyCode());
      item.setPrice(price);
      item.setQuantity(commerceItem.getQuantity());
      items.add(item);
    }
    return items;
  }

  /**
   * @param order
   * @return Contact
   */
  private Contact getBillingAddressFromOrder(Order order) {
    Contact billing = null;
    ContactInfo addr = null;
    //Billing address
    List<PaymentGroup> paymentGroupList = order.getPaymentGroups();
    if (paymentGroupList != null) {
      for (PaymentGroup pg : paymentGroupList) {
        if (pg instanceof AfterPayPayment) {
          AfterPayPayment afterPayPayment = (AfterPayPayment) pg;
          addr = afterPayPayment.getBillingAddress();
        }
      }
    }
    if (addr != null) {
      billing = new Contact();
      billing.setName(addr.getFirstName() + " " + addr.getLastName());
      billing.setLine1(addr.getAddress1());
      billing.setLine2(addr.getAddress2());
      billing.setSuburb(addr.getCity());
      billing.setState(addr.getState());
      billing.setPostcode(addr.getPostalCode());
      billing.setCountryCode(addr.getCountry());
      billing.setPhoneNumber(addr.getPhoneNumber());
    }
    return billing;

  }

  /**
   * @param order
   * @return true or false
   */
  public boolean isEligibleForCart(DigitalOrderImpl order) {
    boolean isEligible = false;
    if (order != null && order.getPriceInfo() != null && !order.isTaxOffline() && this.getAfterPayService().isServiceEnabled()) {
      if (!this.isGiftCardPresentInOrder(order) && this.isAmountOwedWithinLimits(order)) {
          isEligible = true;
      }
    }
    return isEligible;
  }

  /**
   * @param order
   * @return double
   */
  public double getInstallmentAmountForCart(DigitalOrderImpl order) {
    double amountOwe = this.getAmountOwe(order);
    if (this.isEligibleForCart(order)) {
      AfterPayConfigurationResponse config = this.getConfiguration();
      return DigitalCommonUtil.roundHalfUp(amountOwe / Integer.parseInt(config.getNumberOfPayments()));
    }
    return 0.0d;
  }


  /**
   * @return AfterPayConfigurationResponse
   */
  public AfterPayConfigurationResponse getConfiguration() {
    AfterPayConfigurationResponse response = new AfterPayConfigurationResponse();
    //Set default values from properties file here
    response.setCurrency(this.getCurrency());
    response.setMaximumAmount(this.getMaximumAmount());
    response.setMinimumAmount(this.getMinimumAmount());
    //This comes from properties file only, afterpay doesn't provide this...yet.
    response.setNumberOfPayments(this.getNumberOfPayments());

    //Try to override defaults with service config
    try {
      final String cacheKey = "afterpay-config";
      final AfterPayConfiguration[] configuration =
          (AfterPayConfiguration[]) afterPayConfigCache.get(cacheKey);

      if (configuration != null && configuration.length > 0) {
        /**
         Even though this is an array, AfterPay says they're only returning one.
         We'll take type == PAY_BY_INSTALLMENT
         **/
        for (AfterPayConfiguration c : configuration) {
          if ("PAY_BY_INSTALLMENT".equalsIgnoreCase(c.getType())) {
            response.setCurrency(c.getMinimumAmount().getCurrency());
            response.setMinimumAmount(c.getMinimumAmount().getAmount());
            response.setMaximumAmount(c.getMaximumAmount().getAmount());
            break;
          }
        }
      }
    } catch (DigitalIntegrationException e) {
      logError(
          "AfterPay configuration service call failed. Using default values from "
              + "properties file. ", e);
    } catch (Exception e) {
      logError("AfterPay configuration service not able to lookup from cache ", e);
    }
    return response;
  }

  /**
   * @param order
   * @param orderRequest
   * @return AfterPayOrderResponse
   * @throws DigitalAppException
   */
  public AfterPayOrderResponse createOrder(DigitalOrderImpl order,
      AfterPayCreateOrderRequest orderRequest) throws DigitalAppException {
    AfterPayOrderResponse afterPayOrderResponse = new AfterPayOrderResponse();
    if (this.isOrderEligible(order)) {
      CreateOrderRequest orderRequestForService = this
          .convertOrderToRequest(order, orderRequest.getConfirmURL(), orderRequest.getCancelURL());
      if(orderRequestForService.getConsumer() == null
        || DigitalStringUtil.isEmpty(orderRequestForService.getConsumer().getEmail())
        || DigitalStringUtil.isEmpty(orderRequestForService.getConsumer().getPhoneNumber())
        || DigitalStringUtil.isEmpty(orderRequestForService.getConsumer().getGivenNames())
        || DigitalStringUtil.isEmpty(orderRequestForService.getConsumer().getSurname())
      ) {
        throw this.createDigitalAppException("Contact info is incomplete",AFTER_PAY_LOOKUP_CONTACT_INFO_REQUIRED);
      }
      if(orderRequestForService.getShipping() == null) {
        throw this.createDigitalAppException("Shipping info is missing",AFTER_PAY_SHIPPING_INFO_REQUIRED);
      }
      CreateOrderResponse response;
      try {
         response = this.getAfterPayService().createOrder(orderRequestForService);
      } catch (DigitalIntegrationBusinessException e) {
        throw this.createDigitalAppException("AfterPay createOrder error",AFTER_PAY_SERVICE_UNAVAILABLE);
      }
      if (response != null) {
        afterPayOrderResponse.setToken(response.getToken());
        afterPayOrderResponse.setSuccess(true);
      }
    } else {
      throw this.createDigitalAppException("Order is not eligible for AfterPay",ORDER_NOT_ELIGIBLE);
    }
    return afterPayOrderResponse;
  }

  private DigitalAppException createDigitalAppException(String msg, String errorCode) {
    DigitalAppException ex = new DigitalAppException(msg);
    ex.setErrorCode(errorCode);
    return ex;
  }

  /**
   * @param order
   * @param orderRequest
   * @return true or false
   */
  public boolean isOrderEligible(DigitalOrderImpl order) {
    boolean isEligible = false;
    if (order != null
        && this.isEligibleForCart(order)) {
      isEligible = true;
    }
    return isEligible;
  }

  /**
   * @param order
   * @return true or false
   */
  public boolean isGiftCardPresentInOrder(DigitalOrderImpl order) {
    List<CommerceItem> ciList = order.getCommerceItems();
    for (CommerceItem ci : ciList) {
      if ((ci instanceof GiftCardCommerceItem)) {
        return true;
      }
    }
    return false;
  }

  public boolean isAmountOwedWithinLimits(DigitalOrderImpl order) {
      double amountOwe = this.getAmountOwe(order);
      AfterPayConfigurationResponse config = this.getConfiguration();
      if(Double.parseDouble(config.getMinimumAmount()) <= amountOwe && Double.parseDouble(config.getMaximumAmount()) >= amountOwe) {
          return true;
      } else {
          return false;
      }
  }

  /**
   * @param order
   * @return double
   */
  private double getAmountOwe(DigitalOrderImpl order) {
    //using roundHalfUp method to eliminate any double issues
    //issue occurred with 40.74.  ATG getTotal was returning 40.739999999

    double amountOwe = 0.0d;
    try {
      amountOwe = DigitalCommonUtil.roundHalfUp(
              DigitalCommonUtil.roundHalfUp(order.getPriceInfo().getTotal()) -
                      DigitalCommonUtil.roundHalfUp(((DigitalOrderTools) (this.getOrderManager().getOrderTools())).getGiftCardsTotal(order))
      );
    } catch(NullPointerException e) {
      final String methodName = "AfterPayCheckoutManager.getAmountOwe";
      logError("AfterPay calculation for installment amount failed. ", e);
      if(order == null) {
        logError( "Order object is null in " + methodName );
      } else if(order.getPriceInfo() == null) {
        logError( "Order.getPriceInfo object is null in " + methodName  );
      }

      if (this.getOrderManager() == null) {
        logError( "this.getOrderManager() object is null in " + methodName  );
      } else if (this.getOrderManager().getOrderTools() == null) {
        logError( "this.getOrderManager().getOrderTools() object is null in " + methodName  );
      }
    }
    if (amountOwe < 0.0d) {
      amountOwe = 0.0d;
    }
    return amountOwe;
  }

  /**
   * @param order
   * @param shippingGroup
   * @return Consumer
   * @throws DigitalAppException
   */
  private Consumer getConsumer(DigitalOrderImpl order, DigitalHardgoodShippingGroup shippingGroup)
      throws DigitalAppException {
    Consumer consumer = new Consumer();
    Profile profile = getProfile();
    if ((null != profile) && (profileTools.isLoggedIn(profile) || profileTools.isCookied(profile))) {
        consumer.setGivenNames((String) profile.getPropertyValue("firstName"));
        consumer.setSurname((String) profile.getPropertyValue("lastName"));
    }
    // Sometimes a profile can have null/empty/whitespace name.  Use shipping in this case...or anonymous case
    if(DigitalStringUtil.isBlank(consumer.getGivenNames()) || DigitalStringUtil.isBlank(consumer.getSurname())) {
      consumer.setGivenNames(shippingGroup.getShippingAddress().getFirstName());
      consumer.setSurname(shippingGroup.getShippingAddress().getLastName());
    }
    DigitalContactInfo contactInfo = this.getPaymentGroupManager().getBillingAddressFromOrder(order);
    consumer.setEmail(contactInfo.getEmail());
    consumer.setPhoneNumber(contactInfo.getPhoneNumber());
    return consumer;
  }

  private Profile getProfile() {
    return ComponentLookupUtil.lookupComponent( ComponentLookupUtil.PROFILE, Profile.class  );
  }

  /**
   * @param country
   * @return String
   */
  private String convertCountryCode(String country) {
    if (country != null && country.length() > 2) {
      return country.substring(0, 2);
    } else {
      return country;
    }
  }
  
  public double getAmountOwedWithinLimits(double price) {
   AfterPayConfigurationResponse config = this.getConfiguration();
    if(Double.parseDouble(config.getMinimumAmount()) <= price && Double.parseDouble(config.getMaximumAmount()) >= price) {
      return DigitalCommonUtil.roundHalfUp(price/Integer.parseInt(config.getNumberOfPayments()));
    } else {
      return 0.0d;
    }
  }

}
