package com.digital.commerce.services.common;

import com.google.common.base.Objects;

import atg.core.util.ContactInfo;
import lombok.Getter;
import lombok.Setter;

/** 
 * 
 * This class holds the repository mapping with the custom user fields.
 * 
 */
@Getter
@Setter
public class DigitalContactInfo extends ContactInfo {

	/**
     *
     */
	private static final long	serialVersionUID		= -4661166814510786799L;

	private String				rank;

	private String				addressType				= AddressType.USA.getValue();

	private String				region;

	private Boolean				addressVerification		= Boolean.FALSE;

	private Boolean				isPoBox					= Boolean.FALSE;
	private String				pobox;

	private String				businessName;

	private boolean				giftCardAddress			= false;
	private String				emailConfirm;

	private boolean				doAddressVerification	= false;


	public void setIsPoBox( boolean isPoBox ) {
		this.isPoBox = new Boolean( isPoBox );
	}

	public void setRegion( String region ) {
		this.region = region;
		if(null!=region){
			setState(region);
		}
	}
}
