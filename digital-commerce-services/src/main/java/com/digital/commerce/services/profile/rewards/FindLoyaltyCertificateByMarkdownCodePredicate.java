package com.digital.commerce.services.profile.rewards;

import com.digital.commerce.common.util.DigitalPredicate;


public class FindLoyaltyCertificateByMarkdownCodePredicate implements DigitalPredicate<LoyaltyCertificate> {

	private final String	markdownCode;

	public FindLoyaltyCertificateByMarkdownCodePredicate( String markdownCode ) {
		this.markdownCode = markdownCode;
	}

	@Override
	public boolean apply( LoyaltyCertificate input ) {
		return markdownCode.equalsIgnoreCase( input.getMarkdownCode() );
	}

}
