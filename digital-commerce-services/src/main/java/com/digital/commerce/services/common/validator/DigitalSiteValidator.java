package com.digital.commerce.services.common.validator;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.multisite.SiteGroupManager;
import atg.multisite.SiteManager;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.service.collections.validator.CollectionObjectValidator;
import lombok.Getter;
import lombok.Setter;

/**
 * DigitalSiteValidator validates an item based on its sites/siteGroup/siteIds
 * properties.
 * 
 * siteGroup and sites properties could be configured for promotional items, and
 * siteIds property could be configured for products.
 * 
 * <p>
 * A Set of the (repository) items site ids and siteGroups site ids is
 * constructed and compared to the site ids of the current sites
 * shareableTypeId siteGroup site ids. If any sites from the items Set match
 * then the item will be returned.
 * </p>
 * 
 * 
 */
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalSiteValidator extends ApplicationLoggingImpl implements
		CollectionObjectValidator {

	public DigitalSiteValidator() {
		super(DigitalSiteValidator.class.getName());
	}
	
	/**
	 * property: shareableTypeId
	 */
	protected String shareableTypeId;

	/**
	 * property: SiteIdsPropertyName
	 */
	protected String siteIdsPropertyName;

	/**
	 * property: sitesPropertyName
	 */
	protected String sitesPropertyName;

	/**
	 * property: siteGroupPropertyName
	 */
	protected String siteGroupPropertyName;

	/**
	 * property: siteGroupManager
	 */
	protected SiteGroupManager siteGroupManager;

	/**
	 * property: siteManager
	 */
	protected SiteManager siteManager;

	/**
	 * property: includeDisabledSites
	 */
	protected boolean includeDisabledSites;

	/**
	 * property: includeInactiveSites
	 */
	protected boolean includeInactiveSites;
	/**
	 * This method validates the passed in object (repository items) based on
	 * whether or not any of its items sites/siteGroups.sites exist in the
	 * current shareableTypeId siteGroup.
	 * 
	 * @param object
	 *            to validate
	 * @return true if the object passes validation or if no validation was
	 *         performed.
	 */
	@Override
	public boolean validateObject(Object pObject) {
		if (!(pObject instanceof RepositoryItem)) {
			return false;
		}

		// Get current sharing sites. Items form these sites will be included in
		// result collection.
		Collection<String> sharingSiteIds = getSiteGroupManager()
				.getSharingSiteIds(getShareableTypeId());

		// sharingSiteIds includes the current site. It's null if there is no
		// current site or if
		// the configured shareable type isn't registered (e.g., the customer
		// has unregistered the
		// ShoppingCart shareable type to say that all sites can share the
		// cart). In either case,
		// we return true (no validation)
		if (sharingSiteIds == null) {
			return true;
		}

		// If we're supposed to filter out items that exist only on disabled
		// sites or only on inactive sites,
		// then remove the sites from consideration before we start looking at
		// the items.
		if (!isIncludeDisabledSites() || !isIncludeInactiveSites()) {
			String[] siteArray = sharingSiteIds.toArray(new String[0]);
			if (!isIncludeDisabledSites()) {
				siteArray = getSiteManager().filterDisabledSites(siteArray);
			}
			if (!isIncludeInactiveSites()) {
				siteArray = getSiteManager().filterInactiveSites(siteArray);
			}

			sharingSiteIds = Arrays.asList(siteArray);
		}

		// Perform the filtering
		RepositoryItem item = (RepositoryItem) pObject;

		// Merged siteIds set
		Set combinedSiteIds = new HashSet();

		// The items siteIds from getSitesPropertyName() property
		Set itemSiteIds = getSiteIds(item);
		if (itemSiteIds != null && itemSiteIds.size() > 0) {
			combinedSiteIds.addAll(itemSiteIds);
		}

		// The siteIds from getSiteIdsPropertyName() property
		if (getSiteIdsPropertyName() != null) {
			Set sites = (Set<String>) item
					.getPropertyValue(getSiteIdsPropertyName());
			if (sites != null && sites.size() > 0) {
				combinedSiteIds.addAll(sites);
			}
		}

		// The items siteGroup siteIds
		Set itemSiteGroupSiteIds = getSiteGroupSiteIds(item);
		if (itemSiteGroupSiteIds != null && itemSiteGroupSiteIds.size() > 0) {
			combinedSiteIds.addAll(itemSiteGroupSiteIds);
		}

		if (combinedSiteIds.size() > 0) {
			// Now see if this items sites exists in the sharingSiteIds
			if (!Collections.disjoint(combinedSiteIds, sharingSiteIds)) {
				return true;
			}
		}
		// This items has no sites specified. It is available on all sites.
		else {
			return true;
		}

		// The item doesn't pass validation
		return false;
	}

	/**
	 * Gets the siteIds property from the passed in RepositoryItem and returns
	 * them in a Set.
	 * 
	 * @param pItem
	 *            A RepositoryItem
	 * @return A Set of pItems siteIds
	 */
	protected Set<String> getSiteIds(RepositoryItem pItem) {
		if (pItem == null || DigitalStringUtil.isEmpty(getSitesPropertyName())) {
			return null;
		}

		// A collection of Site RepositoryItems
		Collection sites = null;

		try {
			sites = (Collection) pItem.getPropertyValue(getSitesPropertyName());
		} catch (Exception e) {
			if (isLoggingError()) {
				logError("Could not get property " + getSitesPropertyName()
						+ " from repository item " + pItem, e);
			}
		}

		if (sites == null) {
			return null;
		}

		// Create our result set from the collection of sites
		Set<String> siteIds = new HashSet<>(sites.size());
		for (Object site : sites) {
			if (site instanceof RepositoryItem) {
				String siteId = (String) ((RepositoryItem) site)
						.getRepositoryId();

				if (siteId == null) {
					continue;
				}
				siteIds.add(siteId);
			} else {
				// check whether sites collection consists of site IDs
				if (site instanceof String) {
					siteIds.add((String) site);
				}
			}
		}
		return siteIds;
	}

	/**
	 * Gets the siteIds from all pItems siteGroups and returns them in a Set.
	 * 
	 * @param pItem
	 *            A RepositoryItem
	 * @return A Set of siteIds constructed from the siteGroups of pItem
	 */
	protected Set<String> getSiteGroupSiteIds(RepositoryItem pItem) {
		if (pItem == null || DigitalStringUtil.isEmpty(getSiteGroupPropertyName())) {
			return null;
		}

		// A collection of SiteGroup repository Items
		Collection siteGroups = null;

		try {
			siteGroups = (Collection) pItem
					.getPropertyValue(getSiteGroupPropertyName());
		} catch (Exception e) {
			if (isLoggingError()) {
				logError("Could not get property " + getSiteGroupPropertyName()
						+ " from repository item " + pItem, e);
			}
		}

		if (siteGroups == null) {
			return null;
		}

		// Create our result set from the collection of siteGroups
		Set<String> siteIds = new HashSet();
		for (Object site : siteGroups) {
			if (site instanceof RepositoryItem) {
				Set<String> currentSiteId = getSiteIds((RepositoryItem) site);
				if (currentSiteId != null) {
					siteIds.addAll(currentSiteId);
				}
			}
		}
		return siteIds;
	}

}
