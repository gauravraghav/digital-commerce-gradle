package com.digital.commerce.services.pricing.reprice;

/*  */

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExchangeItem extends Item {

	private String	type	= "Exchange";
	private String	lineItemId;
	private String	orderLineKey;
	private String	returnedLineItemId;
	private int		quantity;
	private String	shippingGroupId;
	private boolean	evenExchange;
}
