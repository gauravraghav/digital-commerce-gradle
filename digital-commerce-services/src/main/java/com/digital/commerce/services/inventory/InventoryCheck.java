package com.digital.commerce.services.inventory;

import java.util.Arrays;
import java.util.Set;

import com.digital.commerce.services.order.GiftCardCommerceItem;

import atg.commerce.order.CommerceItem;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

//TODO MK:REDESIGN import com.digital.edt.order.GiftCardCommerceItem;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class InventoryCheck extends GenericService {
	private Repository			productCatalog;
	private String[]			skippedProductTypes;
	private static final String	SKU_DESCRIPTOR				= "sku";
	private static final String	PARENT_PRODUCTS_PROPERTY	= "parentProducts";
	private static final String	PRODUCT_TYPE_PROPERTY		= "productType";

	/** Returns true if product related to SKU has productType which is not
	 * accounted towards inventory
	 * 
	 * @param skuId
	 * @return */
	public boolean isUnaccountable( String skuId ) {
		boolean isUnaccountable = false;
		if( isLoggingDebug() ) {
			logDebug( "[Start]" );
		}
		if( getProductCatalog() != null && skuId != null ) {
			try {
				RepositoryItem sku = (RepositoryItem)getProductCatalog().getItem( skuId, SKU_DESCRIPTOR );
				if( sku != null ) {
					Set parentProducts = (Set)sku.getPropertyValue( PARENT_PRODUCTS_PROPERTY );
					// SKUs like gift card etc should have only one parent
					if( parentProducts != null && parentProducts.size() == 1 ) {
						RepositoryItem product = (RepositoryItem)parentProducts.iterator().next();
						String productType = (String)product.getPropertyValue( PRODUCT_TYPE_PROPERTY );
						isUnaccountable = containsProductType( productType );
						if( isUnaccountable ) {
							if( isLoggingDebug() ) {
								logDebug( "[Found skippedProductType " + productType + " for product " + product.getRepositoryId() + ", " + product.getItemDisplayName() + ", this items shouldnot be accounted towards inventory]" );
							}
						}
					}
				}
			} catch( RepositoryException e ) {
				if( isLoggingError() ) {
					logError( e );
				}
			}
		}
		if( isLoggingDebug() ) {
			logDebug( "[End]" );
		}
		return isUnaccountable;
	}

	/** Returns true if commerceItem is of type GiftCardCommerceItem which is not
	 * accounted towards inventory
	 * 
	 * @param ci
	 * @return */
	  public boolean isUnaccountable( CommerceItem ci ) {
		boolean isUnaccountable = false;
		
 		if( isLoggingDebug() ) {
			logDebug( "[Start]" );
		}
		isUnaccountable = ( ci instanceof GiftCardCommerceItem );
		if( isUnaccountable ) {
			if( isLoggingDebug() ) {
				logDebug( "[Found commerce item of type GiftCardCommerceItem for SKU: " + ci.getCatalogRefId() + ", this items shouldnot be accounted towards inventory]" );
			}
		}
		if( isLoggingDebug() ) {
			logDebug( "[End]" );
		}
		return isUnaccountable;
	}

	/** Searches key in the skippedProductType array
	 * 
	 * @param productType
	 * @return */
	private boolean containsProductType( String productType ) {
		if( isLoggingDebug() ) {
			logDebug( "[Start]" );
		}
		boolean containsProductType = false;
		if( getSkippedProductTypes() != null && productType != null ) {
			int searchIdx = Arrays.binarySearch( getSkippedProductTypes(), productType );
			if( searchIdx >= 0 ) {
				containsProductType = true;
			}
		}
		if( isLoggingDebug() ) {
			logDebug( "[End]" );
		}
		return containsProductType;
	}
}
