/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RewardsRetrievePointsOffersCertificatesRequest {
	private String profileId;
	private Date startDate;
	private Date endDate;
}
