package com.digital.commerce.services.digitalvisa;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.dtm.UserTransactionDemarcation;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdsAuthFormHandler extends GenericFormHandler {

	private AdsAuthRequest adsAuthRequest;
	private AdsManager adsManager;
	private RepeatingRequestMonitor repeatingRequestMonitor;


	/**
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 *             Entry point for getAdsPostData REST service
	 */
	public boolean handleGetAdsPostData(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String METHOD_NAME = "AdsAuthFormHandler.handleGetAdsPostData";
		final String CLASSNAME = this.getClass().getName();
		UserTransactionDemarcation td = null;
		if ((repeatingRequestMonitor == null) || (repeatingRequestMonitor.isUniqueRequestEntry(METHOD_NAME))) {
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				adsAuthRequest = adsManager.getAdsPostData();
			} catch (Exception e) {
				logError("Error while getting the adsAuthPostData", e);
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				addFormException(getAdsAuthErrorMessage(e));
				return false;
			} finally {
				if (repeatingRequestMonitor != null) {
					repeatingRequestMonitor.removeRequestEntry(METHOD_NAME);
				}
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			}
		}

		return true;
	}

	/**
	 * @param e
	 * @return Based on DigitalAppException data create a DropletException
	 */
	private DropletException getAdsAuthErrorMessage(Exception e) {
		String errorMessage = e.getMessage();
		DropletException dex = null;
		if (e instanceof DigitalAppException) {
			DigitalAppException appEx = (DigitalAppException) e;
			if (!DigitalStringUtil.isBlank(appEx.getErrorCode())) {
				dex = new DropletException(errorMessage, appEx.getErrorCode());
			} else {
				dex = new DropletException(errorMessage);
			}
		} else {
			dex = new DropletException(errorMessage);
		}
		return dex;
	}
}
