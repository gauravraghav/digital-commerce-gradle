package com.digital.commerce.services.profile;

public interface DigitalWebOfferConstants {
	static final String	UNDELIVERABLE_EMAIL_PROPERTY		= "undeliverableEmailAddress";
	static final String	UNDELIVERABLE_MAILING_PROPERTY		= "undeliverableMailingAddress";
	static final String	LOYALTY_SIGNUP_DATE_PROPERTY		= "loyaltySignupDate";
	static final String	LOYALTY_SIGNUP_STORE_PROPERTY		= "loyaltySignupStoreNumber";
	static final String	CLUSTOMER_GROUP_PROPERTY			= "clustomerGroup";
	static final String	MEMBER_STATUS_PROPERTY				= "memberStatus";
	static final String	SHOPPING_CHANNEL_PROPERTY			= "shoppingChannel";
	static final String	OFFERS_PROPERTY						= "offers";
	static final String	OFFER_ITEM_DESCRIPTOR				= "offer";
	static final String	CAMPAIGN_PROPERTY					= "campaignId";
	static final String	PROMOTION_PROPERTY					= "promotionId";
	static final String	SEGMENT_PROPERTY					= "segmentId";
	static final String	COLLATERAL_PROPERTY					= "collateralId";
	static final String	START_DATE_PROPERTY					= "startDate";
	static final String	END_DATE_PROPERTY					= "endDate";
	static final String	LOYALTY_TIER_START_DATE_PROPERTY	= "loyaltyTierStartDate";
	static final String	CURRENT_POINTS_BALANCE_PROPERTY		= "currentPointsBalance";
	static final String	POINTS_BANKING_ENABLED_PROPERTY		= "pointsBankingEnabled";
}
