package com.digital.commerce.services.filter.bean;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import atg.commerce.locations.CoordinateManager;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalCommerceItemStoreCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {
    private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalCommerceItemStoreCustomizer.class);

    private String[] returnFields;

    private CoordinateManager storeManager;

    public DigitalCommerceItemStoreCustomizer() {
        super(DigitalCommerceItemStoreCustomizer.class.getName());
    }

    /**
     * @param pTargetObject
     * @param pPropertyName
     * @param pAttributes
     * @return Object
     * @throws BeanFilterException
     */
    @Override
    public Object getPropertyValue(Object pTargetObject, String pPropertyName, Map<String, String> pAttributes) throws BeanFilterException {
        if (pTargetObject == null) {
            return "";
        }

        try {
            Map<String, String> result = new HashMap<>();
            RepositoryItem item = null;
            if (pTargetObject instanceof RepositoryItem) {
                String value = (String) ((RepositoryItem) pTargetObject).getPropertyValue(pPropertyName);
                if (value.startsWith("s")) {
                    item = getStoreManager().getRepository().getItem(value, this.getStoreManager().getDefaultItemType());
                } else {
                    item = this.getStoreByNumber(value);
                }
                result.put(pPropertyName, value);
            } else {
                Method method = pTargetObject.getClass().getMethod("get"
                        + pPropertyName.substring(0, 1).toUpperCase() + pPropertyName.substring(1));
                Object propertyValue = method.invoke(pTargetObject);
                if (propertyValue != null) {
                    String keyValue = propertyValue.toString();
                    if (keyValue.startsWith("s")) {
                        item = getStoreManager().getRepository().
                                getItem(keyValue, this.getStoreManager().getDefaultItemType());
                    } else {
                        item = this.getStoreByNumber(keyValue);
                    }
                    result.put(pPropertyName, keyValue);
                }
            }

            if (item != null) {
                for (String field : this.returnFields) {
                    int subAttribute = field.indexOf(".");
                    if (subAttribute > -1) {
                        Object keyValue = item.getPropertyValue(field.substring(0, subAttribute));
                        if (keyValue != null) {
                            String value = ((RepositoryItem) keyValue).
                                    getPropertyValue(field.substring(subAttribute + 1)).toString();
                            result.put(field, value);
                        }
                        continue;
                    }

                    result.put(field, item.getPropertyValue(field).toString());
                }
            }

            return result;
        } catch (RepositoryException | NoSuchMethodException | SecurityException
                | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            return null;
        }
    }

    /**
     * @param storeId
     * @return RepositoryItem
     */
    private RepositoryItem getStoreByNumber(String storeId) {

        if (!MultiSiteUtil.getWebsiteLocationId().equalsIgnoreCase(storeId)) {
            return null;
        }

        RepositoryItem[] items;
        try {
            // Store Numbers from Yantra will have 29_ in the beginning
            if (storeId.contains("_")) {
                storeId = storeId.substring(storeId.indexOf("_") + 1);
            }
            RepositoryView view = getStoreManager().getRepository().
                                            getView(this.getStoreManager().getDefaultItemType());
            RqlStatement statement = RqlStatement.parseRqlStatement("storeNumber = ?0");
            Object params[] = new Object[1];
            params[0] = storeId;
            items = statement.executeQuery(view, params);
        } catch (RepositoryException e) {
            logger.error(e);
            return null;
        }

        if (items != null) {
            return items[0];
        }

        return null;
    }

}
