package com.digital.commerce.services.profile.rewards;

import com.digital.commerce.common.util.DigitalPredicate;


public class FindLoyaltyCertificateByClaimCodePredicate implements DigitalPredicate<LoyaltyCertificate> {

	private final String	claimCode;

	public FindLoyaltyCertificateByClaimCodePredicate( String claimCode ) {
		this.claimCode = claimCode;
	}

	@Override
	public boolean apply( LoyaltyCertificate input ) {
		return claimCode.equalsIgnoreCase( input.getCertificateNumber() );
	}

}
