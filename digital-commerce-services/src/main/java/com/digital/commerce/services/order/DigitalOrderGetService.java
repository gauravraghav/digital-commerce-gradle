package com.digital.commerce.services.order;

import java.io.IOException;
import java.io.Writer;

import com.digital.commerce.common.util.DigitalCommonUtil;

import atg.commerce.order.OrderGetService;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryPropertyDescriptor;
import atg.repository.databinding.ItemMappingElement;

public class DigitalOrderGetService extends OrderGetService {

	@Override
	protected void handleSimpleType(RepositoryItem pItem,
			RepositoryPropertyDescriptor pPropertyDesc, Writer pInstanceWriter,
			String pNSPrefix, ItemMappingElement pItemMappingElement,
			String pGlobalNSPrefix) throws IOException {
		if( isLoggingDebug() ) {

			logDebug( "In Homebreweed: GetService.handleSimpleType() for propertyDescriptor: " + pPropertyDesc.getName() + " whose type is: " + pPropertyDesc.getPropertyType().getName() );
		}

		String elemName = getXMLPropertyElementName( pPropertyDesc, pItemMappingElement, pNSPrefix, pGlobalNSPrefix );

		pInstanceWriter.write( "<" + elemName + " " + getXMLTypeAttributes( pPropertyDesc ) + ">" );

		Object propertyValue = pItem.getPropertyValue( pPropertyDesc.getName() );
		if( pPropertyDesc.getPropertyType() == java.sql.Timestamp.class ) {

			String timeStamp = propertyValue.toString();
			timeStamp = timeStamp.replace( ' ', 'T' );
			pInstanceWriter.write( timeStamp );
		} else if( pPropertyDesc.getPropertyType() == String.class ) {

			propertyValue = getAdjustedValue( pPropertyDesc, propertyValue.toString() );

			pInstanceWriter.write( "<![CDATA[" );
			pInstanceWriter.write( propertyValue.toString() );
			pInstanceWriter.write( "]]>" );
		} else if( pPropertyDesc.getPropertyType() == Double.class ) {
			Double propertyDoubleValue = (Double)pItem.getPropertyValue(pPropertyDesc.getName());	
			propertyValue = DigitalCommonUtil.round(propertyDoubleValue);
			
			pInstanceWriter.write( propertyValue.toString() );
		} else {
			pInstanceWriter.write( propertyValue.toString() );
		}

		pInstanceWriter.write( "</" + elemName + ">" );
		
	}

}
