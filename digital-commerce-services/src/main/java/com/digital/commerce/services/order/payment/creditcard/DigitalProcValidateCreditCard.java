/**
 *
 */
package com.digital.commerce.services.order.payment.creditcard;

import java.util.ResourceBundle;

import atg.commerce.order.CreditCard;
import atg.commerce.order.processor.BillingAddrValidator;
import atg.commerce.order.processor.ProcValidateCreditCard;
import atg.service.pipeline.PipelineResult;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.DigitalProfileConstants;
import com.digital.commerce.services.common.validator.DigitalStartEndDateValidator;
import com.digital.commerce.services.order.DigitalCreditCard;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import com.digital.commerce.services.validator.DigitalBasicAddressValidator;
import lombok.Getter;
import lombok.Setter;

/**
 * TODO: this uses different validation than the formhandlers. Why?
 *
 * @author wibrahim
 */
@Getter
@Setter
public class DigitalProcValidateCreditCard extends ProcValidateCreditCard {
	
	private MessageLocator messageLocator;
	private DigitalServiceConstants dswServiceConstants;
	private DigitalBasicAddressValidator dswBasicAddressValidator;
	private DigitalStartEndDateValidator startEndDateValidator;


	/**
	 * Validate all Credit Card fields .
	 * <p/>
	 * Note that the super class is not called because it generates error messages that are not desirable.
	 * <p/>
	 * Credit Card type is deliberately not validated. Some validations are conditional.
	 *
	 * @param CreditCard
	 * @param PipelineResult
	 * @param ResourceBundle
	 */
	protected void validateCreditCardFields( CreditCard cc, PipelineResult pResult, ResourceBundle pResourceBundle ) {
		if( isLoggingDebug() ) {
			logDebug( "DigitalProcValidateCreditCard.validateCreditCardFields(CreditCard, PipelineResult, ResourceBundle) - start" );
		}
		if( DigitalStringUtil.isBlank( cc.getExpirationMonth() ) || !DigitalStringUtil.isNumeric( cc.getExpirationMonth() ) ) {
			addHashedError( pResult, DigitalProfileConstants.MSG_INVALID_CREDIT_CARD_EXPIRATION_MONTH, cc.getId(), getMessageLocator().getMessageString( DigitalProfileConstants.MSG_INVALID_CREDIT_CARD_EXPIRATION_MONTH ) );
		}else if( DigitalStringUtil.isBlank( cc.getExpirationYear() ) || !DigitalStringUtil.isNumeric( cc.getExpirationYear() ) ) {
			addHashedError( pResult, DigitalProfileConstants.MSG_INVALID_CREDIT_CARD_EXPIRATION_YEAR, cc.getId(), getMessageLocator().getMessageString( DigitalProfileConstants.MSG_INVALID_CREDIT_CARD_EXPIRATION_YEAR ) );
		}else if(getStartEndDateValidator().isCreditCardExpired(cc)){
			addHashedError( pResult, DigitalProfileConstants.MSG_CARD_EXPIRED, cc.getId(), getMessageLocator().getMessageString( DigitalProfileConstants.MSG_CARD_EXPIRED ) );
		}

		if( DigitalStringUtil.isBlank( cc.getCreditCardType() ) ) {
			addHashedError( pResult, DigitalProfileConstants.MSG_INVALID_CARD_TYPE, cc.getId(), getMessageLocator().getMessageString( DigitalProfileConstants.MSG_INVALID_CARD_TYPE ) );
		}

		if(isLoggingDebug()) {
			logDebug("CC Billing address "  + cc.getBillingAddress());
		}
		if(!getDswBasicAddressValidator().isValidBillingAddress(cc.getBillingAddress())){
			logError("Billing address has validation errors  "  + cc.getBillingAddress());
			addHashedError( pResult, DigitalProfileConstants.MSG_MISSIGN_BILLING_ADDRESS, cc.getId(), getMessageLocator().getMessageString( DigitalProfileConstants.MSG_MISSIGN_BILLING_ADDRESS ) );
		}else{
			BillingAddrValidator bav = getBillingAddressValidator();
			bav.validateBillingAddress( cc.getBillingAddress(), cc.getId(), pResult, pResourceBundle );
		}

		//converting from CreditCard to DigitalCreditCard type to get access to getNameOnCard() method
		DigitalCreditCard creditCard = (DigitalCreditCard)cc;
	
		// VALIDATE NAME ON CARD EXISTS
		if( DigitalStringUtil.isBlank( creditCard.getNameOnCard() ) ) {
			addHashedError( pResult, DigitalProfileConstants.MISSINGNAMEONCARD, cc.getId(), getMessageLocator().getMessageString( DigitalProfileConstants.MISSINGNAMEONCARD ) );
		}
		// VALIDATE IF PAYPAGEREGISTARTION ID OR TOKENVALUE EXISTS
		if( DigitalStringUtil.isBlank( creditCard.getPaypageRegistrationId())
				&& DigitalStringUtil.isBlank( creditCard.getTokenValue())) {
			addHashedError( pResult, DigitalProfileConstants.MISSING_LOWVALUE_OR_HIGHVALUE_TOKEN, cc.getId(), getMessageLocator().getMessageString( DigitalProfileConstants.MISSING_LOWVALUE_OR_HIGHVALUE_TOKEN ) );
		}
	
		if( isLoggingDebug() ) {
			logDebug( "DigitalProcValidateCreditCard.validateCreditCardFields(CreditCard, PipelineResult, ResourceBundle) - end" );
		}
	}
}
