package com.digital.commerce.services.profile;

import atg.adapter.gsa.ChangeAwareSet;
import atg.adapter.gsa.GSAItem;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.RequestHeaderAttributesConstant;
import com.digital.commerce.services.rewards.domain.RewardsGiftItem;
import com.digital.commerce.services.rewards.domain.RewardsShopforItem;
import com.google.common.collect.Iterables;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Responsible for merging a new/updated set of birthday gifts for a customer profile with their
 * existing birthday gift set.
 */
public class DigitalBirthdayGiftTools {

  private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalBirthdayGiftTools.class);

  private static final String USER_PROPERTY = "user";
  private static final String BIRTHDAY_GIFT_STATUS_PROPERTY = "status";
  private static final String BIRTHDAY_GIFT_ID_PROPERTY = "giftId";
  private static final String BIRTHDAY_GIFT_EMAIL_PROPERTY = "email";
  private static final String BIRTHDAY_GIFT_FIRST_NAME_PROPERTY = "firstName";
  private static final String BIRTHDAY_GIFT_SEND_DATE_PROPERTY = "giftSendDate";
  private static final String BIRTHDAY_GIFT_CUSTOMER_MESSAGE_PROPERTY = "customerMessage";
  private static final String BIRTHDAY_GIFTS_ITEM_DESCRIPTOR = "birthdayGifts";
  private static final String BIRTHDAY_GIFT_ITEM_DESCRIPTOR = "birthdayGift";
  private static final String BIRTHDAY_GIFT_ADD_DATE_PROPERTY = "addDate";
  private static final String BIRTHDAY_GIFT_UPDATE_DATE_PROPERTY = "updateDate";
  private static final String BIRTHDAY_GIFT_ADD_USERNAME_PROPERTY = "addUsername";
  private static final String BIRTHDAY_GIFT_UPDATE_USERNAME_PROPERTY = "updateUsername";
  public static final String BIRTHDAY_GIFT_STATUS_NOT_SENT = "NOT_SENT";
  private static final String BIRTHDAY_GIFT_STATUS_LOCKED = "LOCKED";
  private static final String BIRTHDAY_GIFT_STATUS_SENT = "SENT";
  private static final String BIRTHDAY_GIFT_STATUS_DELETED = "DELETED";
  private static final String BIRTHDAY_GIFT_DEFAULT_USERNAME = "WEB";

  /**
   * Empty constructor
   */
  public DigitalBirthdayGiftTools() {

  }

  /**
   * @param profile
   * @param currentBirthdayGifts
   * @throws RepositoryException
   */
  public final void mergeBirthDayGiftsToProfile(MutableRepositoryItem profile,
                                                List<RewardsGiftItem> currentBirthdayGifts) throws RepositoryException {
    final MutableRepository profileRepository = (MutableRepository) profile.getRepository();
    final String profileId = profile.getRepositoryId();
    final ChangeAwareSet bDayGiftChangeAwareSet =
            ((ChangeAwareSet) profile.getPropertyValue(BIRTHDAY_GIFTS_ITEM_DESCRIPTOR));
    Set<RepositoryItem> mergedBDayGiftSet = new HashSet<>();
    Set<GSAItem> dormantBDayGiftSet = new HashSet<>();
    boolean updateFlag = false;
    if (bDayGiftChangeAwareSet != null && !bDayGiftChangeAwareSet.isEmpty()) {
      for (Object aBDayGiftChangeAwareSet : bDayGiftChangeAwareSet) {
        final GSAItem repositoryItem = (GSAItem) aBDayGiftChangeAwareSet;
        final RewardsGiftItem repositoryItemAsBDayGift = BDayGiftFactory.from(repositoryItem);
        int existingRewardsGiftItemIndex = -1;
        if (currentBirthdayGifts != null) {
          existingRewardsGiftItemIndex =
                  Iterables.indexOf(currentBirthdayGifts,
                          new ExistingBirthdayGiftPredicate(repositoryItemAsBDayGift));
        }
        if (existingRewardsGiftItemIndex != -1) {
          if (logger.isDebugEnabled()) {
            logger.debug("This Birthday Gift Item Id " + repositoryItemAsBDayGift.getId() +
                    " is valid so check to see if record needs to be updated for the profile "
                    + profileId);
          }
          boolean tmpUpdateFlag =
                  populateExistingGiftItem(currentBirthdayGifts, repositoryItem,
                          existingRewardsGiftItemIndex);

          if (tmpUpdateFlag) {
            updateFlag = true;
          }
          mergedBDayGiftSet.add(repositoryItem);
          currentBirthdayGifts.remove(existingRewardsGiftItemIndex);
        } else {
          if (logger.isDebugEnabled()) {
            logger.debug("This Birthday Gift Item Id " + repositoryItemAsBDayGift.getId() +
                    " is no longer valid for the profile " + profileId);
          }
          Object status = repositoryItem.getPropertyValue(BIRTHDAY_GIFT_STATUS_PROPERTY);
          if (status != null && !BIRTHDAY_GIFT_STATUS_DELETED.equalsIgnoreCase((String) status)) {
            dormantBDayGiftSet.add(repositoryItem);
          }
        }
      }
    }

    if (null != currentBirthdayGifts) {
      for (final RewardsGiftItem giftItem : currentBirthdayGifts) {
        if (logger.isDebugEnabled()) {
          logger.debug("This Birthday Gift Item Id " + giftItem.getId()
                  + " is new for this customer so new record is added for the profile " + profileId);
        }
        mergedBDayGiftSet.add(BDayGiftFactory.to(profile, profileRepository, giftItem));
        updateFlag = true;
      }
    }

    for (GSAItem dormantGiftItem : dormantBDayGiftSet) {
      if (logger.isDebugEnabled()) {
        logger.debug("This Birthday Gift Item Id " + dormantGiftItem.getRepositoryId()
                + " is no longer valid " +
                "so updating to DELETED status for the profile " + profileId);
      }
      dormantGiftItem.setPropertyValue(BIRTHDAY_GIFT_STATUS_PROPERTY, BIRTHDAY_GIFT_STATUS_DELETED);
      dormantGiftItem.setPropertyValue(BIRTHDAY_GIFT_UPDATE_DATE_PROPERTY, new Date());
      updateFlag = true;
    }

    if (updateFlag) {
      if (logger.isDebugEnabled()) {
        logger.debug("There are Birthday Gift Items to be synced up for the profile " + profileId);
      }
      MutableRepositoryItem profileItem = profileRepository.getItemForUpdate(profileId,
              profile.getItemDescriptor().getItemDescriptorName());
      profileItem.setPropertyValue(BIRTHDAY_GIFTS_ITEM_DESCRIPTOR, mergedBDayGiftSet);
      profileRepository.updateItem(profileItem);
    } else {
      if (logger.isDebugEnabled()) {
        logger
                .debug("There are NO Birthday Gift Items to be synced up for the profile " + profileId);
      }
    }
  }

  /**
   * @param profile
   * @return List
   */
  public final List<RewardsGiftItem> retrieveBirthDayGiftsFromProfile(
          final MutableRepositoryItem profile) {
    List<RewardsGiftItem> rewardsGiftItems = new ArrayList<>();
    final ChangeAwareSet bDayGiftsChangeAwareSet =
            ((ChangeAwareSet) profile.getPropertyValue(BIRTHDAY_GIFTS_ITEM_DESCRIPTOR));

    if (bDayGiftsChangeAwareSet != null && !bDayGiftsChangeAwareSet.isEmpty()) {
      for (Object aBDayGiftsChangeAwareSet : bDayGiftsChangeAwareSet) {
        final GSAItem repositoryItem = (GSAItem) aBDayGiftsChangeAwareSet;
        final RewardsGiftItem repositoryItemAsRewardsGiftItem = BDayGiftFactory
                .from(repositoryItem);
        if (!BIRTHDAY_GIFT_STATUS_DELETED
                .equalsIgnoreCase(repositoryItemAsRewardsGiftItem.getStatus())) {
          rewardsGiftItems.add(repositoryItemAsRewardsGiftItem);
        }
      }
    }

    String profileId = profile.getRepositoryId();
    if (rewardsGiftItems.isEmpty()) {
      if (logger.isDebugEnabled()) {
        logger.debug("There are NO valid Birthday Gift Item Ids for the profile " + profileId);
      }
    } else {
      if (logger.isDebugEnabled()) {
        logger.debug(
                "There is " + rewardsGiftItems.size() + " valid Birthday Gift Item(s) used for the " +
                        "profile " + profileId);
      }
    }

    return rewardsGiftItems;
  }

  /**
   * @param profile
   * @param giftItem
   * @return true or false
   */
  public final boolean isBDayGiftBelongsToProfile(final MutableRepositoryItem profile,
                                                  final RewardsGiftItem giftItem) {
    final String profileId = profile.getRepositoryId();
    List<RewardsGiftItem> currentBirthdayGifts = new ArrayList<>();
    currentBirthdayGifts.add(giftItem);
    final ChangeAwareSet bDayGiftChangeAwareSet =
            ((ChangeAwareSet) profile.getPropertyValue(BIRTHDAY_GIFTS_ITEM_DESCRIPTOR));
    if (bDayGiftChangeAwareSet != null && !bDayGiftChangeAwareSet.isEmpty()) {
      for (Object aBDayGiftChangeAwareSet : bDayGiftChangeAwareSet) {
        final GSAItem repositoryItem = (GSAItem) aBDayGiftChangeAwareSet;
        final RewardsGiftItem repositoryItemAsBDayGift = BDayGiftFactory.from(repositoryItem);
        final int existingRewardsGiftItemIndex =
                Iterables.indexOf(currentBirthdayGifts,
                        new ExistingBirthdayGiftPredicate(repositoryItemAsBDayGift));
        if (existingRewardsGiftItemIndex != -1) {
          if (logger.isDebugEnabled()) {
            logger.debug("This Birthday Gift Item Id " + repositoryItemAsBDayGift.getId() +
                    " belongs to the profile " + profileId);
          }
          return true;
        }
      }
    } else {
      if (logger.isDebugEnabled()) {
        logger.debug(
                "This Birthday Gift Item Id " + giftItem.getId() + " doesn't belong to the profile "
                        + profileId);
      }
    }

    return false;
  }

  /**
   * @param profile
   * @param giftItem
   * @throws RepositoryException
   */
  public final void addOrUpdateBDayGiftForProfile(MutableRepositoryItem profile,
                                                  final RewardsGiftItem giftItem) throws RepositoryException {
    final MutableRepository profileRepository = (MutableRepository) profile.getRepository();
    final String profileId = profile.getRepositoryId();
    boolean updateFlag = false;
    Set<RepositoryItem> mergedBDayGiftSet = new HashSet<>();
    List<RewardsGiftItem> currentBirthdayGifts = new ArrayList<>();
    currentBirthdayGifts.add(giftItem);
    final ChangeAwareSet bDayGiftChangeAwareSet =
            ((ChangeAwareSet) profile.getPropertyValue(BIRTHDAY_GIFTS_ITEM_DESCRIPTOR));
    if (bDayGiftChangeAwareSet != null && !bDayGiftChangeAwareSet.isEmpty()) {
      for (Object aBDayGiftChangeAwareSet : bDayGiftChangeAwareSet) {
        final GSAItem repositoryItem = (GSAItem) aBDayGiftChangeAwareSet;
        final RewardsGiftItem repositoryItemAsBDayGift = BDayGiftFactory
                .from(repositoryItem);
        if (!BIRTHDAY_GIFT_STATUS_DELETED
                .equalsIgnoreCase(repositoryItemAsBDayGift.getStatus())) {
          final int existingRewardsGiftItemIndex =
                  Iterables.indexOf(currentBirthdayGifts,
                          new ExistingBirthdayGiftPredicate(repositoryItemAsBDayGift));
          if (existingRewardsGiftItemIndex != -1) {
            if (logger.isDebugEnabled()) {
              logger.debug(
                      "This Birthday Gift Item Id " + repositoryItemAsBDayGift.getId() +
                              " belongs to the profile " + profileId);
            }
            populateExistingGiftItem(currentBirthdayGifts, repositoryItem,
                    existingRewardsGiftItemIndex);
            currentBirthdayGifts.remove(existingRewardsGiftItemIndex);
            updateFlag = true;
          }
          mergedBDayGiftSet.add(repositoryItem);
        }
      }
    } else {
      if (logger.isDebugEnabled()) {
        logger.debug("This Birthday Gift Item Id " + giftItem.getId()
                + " doesn't belong to the profile "
                + profileId);
      }
    }

    for (final RewardsGiftItem newGiftItem : currentBirthdayGifts) {
      if (logger.isDebugEnabled()) {
        logger.debug("This Birthday Gift Item Id " + newGiftItem.getId()
                + " is new for this customer so new record " +
                "is added for the profile " + profileId);
      }
      mergedBDayGiftSet.add(BDayGiftFactory.to(profile, profileRepository, newGiftItem));
      updateFlag = true;
    }

    if (updateFlag) {
      if (logger.isDebugEnabled()) {
        logger.debug(
                "There are Birthday Gift Items to be created or updated for this profile " + profileId);
      }
      MutableRepositoryItem profileItem = profileRepository.getItemForUpdate(profileId,
              profile.getItemDescriptor().getItemDescriptorName());
      profileItem.setPropertyValue(BIRTHDAY_GIFTS_ITEM_DESCRIPTOR, mergedBDayGiftSet);
      profileRepository.updateItem(profileItem);
    } else {
      if (logger.isDebugEnabled()) {
        logger.debug(
                "There are NO Birthday Gift Items to be created or updated for this profile "
                        + profileId);
      }
    }
  }


  private static class BDayGiftFactory {

    /**
     * Private constructor so that this class with static methods should not be instantiated
     */
    private BDayGiftFactory() {

    }

    /**
     * @param item
     * @return RewardsGiftItem
     */
    static RewardsGiftItem from(GSAItem item) {
      final RewardsGiftItem bDayGiftItem = new RewardsGiftItem();

      final Object giftId = item.getPropertyValue(BIRTHDAY_GIFT_ID_PROPERTY);
      if (null != giftId) {
        bDayGiftItem.setId((String) giftId);
      }

      final Object firstName = item.getPropertyValue(BIRTHDAY_GIFT_FIRST_NAME_PROPERTY);
      if (null != firstName) {
        bDayGiftItem.setFirstName((String) firstName);
      }

      final Object msg = item.getPropertyValue(BIRTHDAY_GIFT_CUSTOMER_MESSAGE_PROPERTY);
      if (null != msg) {
        bDayGiftItem.setGiftMessage((String) msg);
      }

      final Object email = item.getPropertyValue(BIRTHDAY_GIFT_EMAIL_PROPERTY);
      if (null != email) {
        bDayGiftItem.setEmail((String) email);
      }

      final Object giftSendDate = item.getPropertyValue(BIRTHDAY_GIFT_SEND_DATE_PROPERTY);
      if (null != giftSendDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime((Date) giftSendDate);
        bDayGiftItem.setBirthMonth(Integer.toString(cal.get(Calendar.MONTH) + 1));
        bDayGiftItem.setBirthDay(Integer.toString(cal.get(Calendar.DATE)));
        bDayGiftItem.setGiftSendDate((Date) giftSendDate);
      }

      final Object status = item.getPropertyValue(BIRTHDAY_GIFT_STATUS_PROPERTY);
      if (null != status) {
        bDayGiftItem.setStatus((String) status);
      }

      bDayGiftItem.setProfileId(
              ((MutableRepositoryItem) item.getPropertyValue(USER_PROPERTY)).getRepositoryId());

      final Object addDate = item.getPropertyValue(BIRTHDAY_GIFT_ADD_DATE_PROPERTY);
      if (addDate != null) {
        bDayGiftItem.setAddDate((Date) addDate);
      }

      final Object updateDate = item.getPropertyValue(BIRTHDAY_GIFT_UPDATE_DATE_PROPERTY);
      if (updateDate != null) {
        bDayGiftItem.setUpdateDate((Date) updateDate);
      }

      final Object addUsername = item.getPropertyValue(BIRTHDAY_GIFT_ADD_USERNAME_PROPERTY);
      if (addUsername != null) {
        bDayGiftItem.setAddUsername((String) addUsername);
      }

      final Object updateUsername = item.getPropertyValue(BIRTHDAY_GIFT_UPDATE_USERNAME_PROPERTY);
      if (updateUsername != null) {
        bDayGiftItem.setUpdateUsername((String) updateUsername);
      }
      return bDayGiftItem;
    }

    /**
     * @param repositoryItem
     * @param repository
     * @param rewardsGiftItem
     * @return MutableRepositoryItem
     */
    static MutableRepositoryItem to(MutableRepositoryItem repositoryItem,
                                    MutableRepository repository,
                                    RewardsGiftItem rewardsGiftItem) {
      MutableRepositoryItem retVal = null;
      try {
        retVal = repository.createItem(BIRTHDAY_GIFT_ITEM_DESCRIPTOR);
        if (repositoryItem instanceof Profile) {
          retVal.setPropertyValue(USER_PROPERTY, ((Profile) repositoryItem).getDataSource());
        } else {
          retVal.setPropertyValue(USER_PROPERTY, repositoryItem);
        }
        if (DigitalStringUtil.isNotBlank(rewardsGiftItem.getFirstName())) {
          retVal
                  .setPropertyValue(BIRTHDAY_GIFT_FIRST_NAME_PROPERTY, rewardsGiftItem.getFirstName());
        }
        if (DigitalStringUtil.isNotBlank(rewardsGiftItem.getGiftMessage())) {
          retVal.setPropertyValue(BIRTHDAY_GIFT_CUSTOMER_MESSAGE_PROPERTY,
                  rewardsGiftItem.getGiftMessage());
        }
        if (DigitalStringUtil.isNotBlank(rewardsGiftItem.getEmail())) {
          retVal.setPropertyValue(BIRTHDAY_GIFT_EMAIL_PROPERTY, rewardsGiftItem.getEmail());
        }
        if (rewardsGiftItem.getGiftSendDate() != null) {
          retVal.setPropertyValue(BIRTHDAY_GIFT_SEND_DATE_PROPERTY,
                  rewardsGiftItem.getGiftSendDate());
        }
        if (DigitalStringUtil.isNotBlank(rewardsGiftItem.getStatus())) {
          retVal.setPropertyValue(BIRTHDAY_GIFT_STATUS_PROPERTY,
                  BDayGiftStatusFactory.from(rewardsGiftItem.getStatus()));
        }
        retVal.setPropertyValue(BIRTHDAY_GIFT_ID_PROPERTY, rewardsGiftItem.getId());
        retVal.setPropertyValue(BIRTHDAY_GIFT_ADD_DATE_PROPERTY, new Date());
        retVal.setPropertyValue(BIRTHDAY_GIFT_UPDATE_DATE_PROPERTY, new Date());
        String username = "";
        if (null != ServletUtil.getCurrentRequest()) {
          username = ServletUtil.getCurrentRequest().getHeader(
                  RequestHeaderAttributesConstant.AKAMAI_DEVICE_KEY.getValue());
        }
        retVal.setPropertyValue(BIRTHDAY_GIFT_ADD_USERNAME_PROPERTY,
                (DigitalStringUtil.isBlank(username) ? BIRTHDAY_GIFT_DEFAULT_USERNAME : username));
        retVal.setPropertyValue(BIRTHDAY_GIFT_UPDATE_USERNAME_PROPERTY,
                (DigitalStringUtil.isBlank(username) ? BIRTHDAY_GIFT_DEFAULT_USERNAME : username));
      } catch (RepositoryException e) {
        logger.error(String.format("Unable to create rewardsGiftItem %s", rewardsGiftItem.getId()),
                e);
      }
      return retVal;
    }
  }

  private static class BDayGiftStatusFactory {

    /**
     * Private constructor so that this class with static methods should not be instantiated
     */
    private BDayGiftStatusFactory() {

    }

    /**
     * @param input
     * @return String
     */
    static String from(String input) {
      String value;
      switch (input) {
        case "Y":
          value = BIRTHDAY_GIFT_STATUS_LOCKED;
          break;
        case "N":
          value = BIRTHDAY_GIFT_STATUS_NOT_SENT;
          break;
        default:
          value = BIRTHDAY_GIFT_STATUS_NOT_SENT;
      }
      return value;
    }

    /**
     * @param input
     * @return String
     */
    static String to(String input) {
      String value;
      switch (input) {
        case BIRTHDAY_GIFT_STATUS_LOCKED:
          value = "Y";
          break;
        case BIRTHDAY_GIFT_STATUS_DELETED:
          value = "Y";
          break;
        case BIRTHDAY_GIFT_STATUS_NOT_SENT:
          value = "N";
          break;
        case BIRTHDAY_GIFT_STATUS_SENT:
          value = "Y";
          break;
        default:
          value = "Y";
      }
      return value;
    }
  }

  private static class ExistingBirthdayGiftPredicate implements DigitalPredicate<RewardsGiftItem> {

    private final RewardsGiftItem existingGiftItem;

    /**
     * @param existingGiftItem
     */
    ExistingBirthdayGiftPredicate(RewardsGiftItem existingGiftItem) {
      this.existingGiftItem = existingGiftItem;
    }

    /**
     * @param input
     * @return true or false
     */
    @Override
    public final boolean apply(RewardsGiftItem input) {
      return DigitalStringUtil.equals(existingGiftItem.getProfileId(), input.getProfileId())
              && DigitalStringUtil.equals(existingGiftItem.getId(), input.getId());
    }
  }


  /**
   * @param currentBirthdayGifts
   * @param repositoryItem
   * @param existingRewardsGiftItemIndex
   * @return true or false
   */
  private boolean populateExistingGiftItem(final List<RewardsGiftItem> currentBirthdayGifts,
                                           GSAItem repositoryItem, final int existingRewardsGiftItemIndex) {
    final RewardsGiftItem rewardsGiftItem = currentBirthdayGifts.get(existingRewardsGiftItemIndex);
    String rewardsBDayGiftNewStatus = BIRTHDAY_GIFT_STATUS_NOT_SENT;
    if (DigitalStringUtil.isNotBlank(rewardsGiftItem.getStatus())) {
      rewardsBDayGiftNewStatus = BDayGiftStatusFactory.from(rewardsGiftItem.getStatus());
    }
    final String currentSystemStatus = (String) repositoryItem
            .getPropertyValue(BIRTHDAY_GIFT_STATUS_PROPERTY);
    if (BIRTHDAY_GIFT_STATUS_NOT_SENT.equalsIgnoreCase(currentSystemStatus) ||
            BIRTHDAY_GIFT_STATUS_NOT_SENT.equalsIgnoreCase(rewardsBDayGiftNewStatus)) {
      if (logger.isDebugEnabled()) {
        logger.debug(
                "This Birthday Gift Item Id " + rewardsGiftItem.getId() + " current system status " +
                        "is " + BIRTHDAY_GIFT_STATUS_NOT_SENT
                        + " so eligible for record update to new status " +
                        rewardsBDayGiftNewStatus);
      }
      repositoryItem.setPropertyValue(BIRTHDAY_GIFT_STATUS_PROPERTY, rewardsBDayGiftNewStatus);
      if (null != rewardsGiftItem.getGiftSendDate()) {
        repositoryItem.setPropertyValue(BIRTHDAY_GIFT_SEND_DATE_PROPERTY,
                rewardsGiftItem.getGiftSendDate());
      }
      repositoryItem.setPropertyValue(BIRTHDAY_GIFT_CUSTOMER_MESSAGE_PROPERTY,
              rewardsGiftItem.getGiftMessage());
      repositoryItem.setPropertyValue(BIRTHDAY_GIFT_FIRST_NAME_PROPERTY,
              rewardsGiftItem.getFirstName());
      repositoryItem.setPropertyValue(BIRTHDAY_GIFT_EMAIL_PROPERTY,
              rewardsGiftItem.getEmail());
      repositoryItem.setPropertyValue(BIRTHDAY_GIFT_UPDATE_DATE_PROPERTY, new Date());
      String username = "";
      if (null != ServletUtil.getCurrentRequest()) {
        username = ServletUtil.getCurrentRequest().getHeader(
                RequestHeaderAttributesConstant.AKAMAI_DEVICE_KEY.getValue());
      }
      repositoryItem.setPropertyValue(BIRTHDAY_GIFT_UPDATE_USERNAME_PROPERTY,
              (DigitalStringUtil.isBlank(username) ? BIRTHDAY_GIFT_DEFAULT_USERNAME : username));
      return true;
    } else {
      if (logger.isDebugEnabled()) {
        logger.debug(
                "This Birthday Gift Item Id " + rewardsGiftItem.getId() + " current system status " +
                        "is " + BIRTHDAY_GIFT_STATUS_NOT_SENT
                        + " so NOT eligible for record update to new status " +
                        rewardsBDayGiftNewStatus + " from Rewards");
      }
    }
    return false;
  }
}




