package com.digital.commerce.services.order.processor;

import java.util.HashMap;

import com.digital.commerce.services.profile.DigitalProfileTools;

import atg.commerce.order.Order;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class DigitalProcShippingGroupUpdate implements PipelineProcessor{ 
	
	private DigitalProfileTools profileTools;

	public int runProcess(Object paramObject, PipelineResult paramPipelineResult)
		    throws Exception{
		HashMap map = (HashMap)paramObject;
	    Order order = (Order)map.get("Order");
	    RepositoryItem profile = (RepositoryItem)map.get("Profile");
	    
	    if(profile != null)
	    	this.profileTools.replaceEmptyShippingGroupWithDefault(order, profile);	    
	    
	    return 1;
	    
	}
		  
    public int[] getRetCodes(){
      return new int[]{1};
    }

}
