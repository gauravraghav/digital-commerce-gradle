package com.digital.commerce.services.promotions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.promotions.collections.validator.DigitalGlobalPromotionValidator;

import atg.nucleus.naming.ParameterName;
import atg.service.collections.filter.FilterException;
import atg.service.collections.filter.ValidatorFilter;
import atg.service.collections.validator.CollectionObjectValidator;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class DigitalPromotionFilterDroplet extends DynamoServlet {

	private static final ParameterName OUTPUT = ParameterName
			.getParameterName("output");
	private static final ParameterName EMPTY = ParameterName
			.getParameterName("empty");
	public static final ParameterName ERROR_OPARAM = ParameterName
			.getParameterName("error");
	private static final String FILTERED_PROMOTIONS = "filteredPromotions";
	
	private static final String    PERFORM_MONITOR_NAME    = "DigitalPromotionFilterDroplet";
	
	private DigitalPromotionService promotionService;

	/**
	 * property: filter
	 */
	private ValidatorFilter filter;

	/**
	 * This method will call existing initalizeAndFilterPromotions method.
	 * Render oparam output with parameter filteredPromotions.
	 */
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		String METHOD_NAME = "service";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			
			Collection filteredPromotions = initalizeSiteGroupPromotions(pRequest,
					pResponse);
	
			if (filteredPromotions.isEmpty()) {
				pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			}
			else{
				if (isLoggingDebug()) {
					logDebug("No promotions available");
					logDebug("Returning promotions = " + filteredPromotions);
				}
				pRequest.setParameter(FILTERED_PROMOTIONS, filteredPromotions);
				pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);			
			}
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
	}

	/**
	 * Populates the mSiteGroupPromotions Collection
	 * 
	 * @throws IOException
	 * @throws ServletException
	 */
	protected Collection initalizeSiteGroupPromotions(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		String METHOD_NAME = "initalizeSiteGroupPromotions";
		
		// Result collection
		Collection filteredPromotions = new ArrayList();
		
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			
			CollectionObjectValidator[] validators = getFilter().getValidators();
			String globalPromoFlag= pRequest.getParameter("loadOnlyGlobalPromo");
			boolean loadOnlyGlobalPromo = Boolean.parseBoolean(globalPromoFlag);

			for (CollectionObjectValidator validator : validators)
		     {
		      if (validator instanceof DigitalGlobalPromotionValidator) {
		    	  ((DigitalGlobalPromotionValidator)validator).setLoadOnlyGlobalPromotions(loadOnlyGlobalPromo);
		      }
		     }
			
			// All promotions
			Collection unfilteredCollection = getPromotionService().findAllPromotions();

			// Check that there are promotions to filter
			if (unfilteredCollection == null || unfilteredCollection.size() == 0) {
				return filteredPromotions;
			}			
			  
			// Filter all promotions using filter parameter
			filteredPromotions = getFilter().filterCollection(
					unfilteredCollection, null, null);
		} catch (FilterException e) {			
			String msg = "Item does not have a sites property";
			if (isLoggingError()) {
				logError(msg);
			}
			pRequest.setParameter("errorMsg", msg);
			pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		return filteredPromotions;
	}

}
