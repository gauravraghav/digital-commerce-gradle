package com.digital.commerce.services.pricing;


import java.util.Arrays;
import java.util.Iterator;

import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.google.common.collect.Iterables;

import atg.repository.RepositoryItem;
import lombok.Getter;

/** The promotion restriction types supported by the system.
 * 
 * @author <a href="mailto:knaas@dswinc.com">knaas</a> */
@Getter
public enum PromotionRestrictionType {
	None("None"), CSC("CSC"), APP("APP");

	private final String	value;

	private PromotionRestrictionType( String value ) {
		this.value = value;
	}

	/** Looks up the promotion restriction type for the given value.
	 * 
	 * @param value
	 * @return */
	public static PromotionRestrictionType valueFor( final String value ) {
		final String trimmedValue = DigitalStringUtil.trimToEmpty( value );
		final Iterator<PromotionRestrictionType> filtered = Iterables.filter( Arrays.asList( values() ), new DigitalPredicate<PromotionRestrictionType>() {

			@Override
			public boolean apply( PromotionRestrictionType input ) {
				return DigitalStringUtil.equalsIgnoreCase( DigitalStringUtil.trimToEmpty( input.getValue() ), trimmedValue );
			}

		} ).iterator();

		return filtered.hasNext() ? filtered.next() : PromotionRestrictionType.None;
	}

	/** Looks up the promotion restriction type for the given value.
	 * 
	 * @param value
	 * @return */
	public static PromotionRestrictionType valueFor( RepositoryItem promotion ) {
		return valueFor( (String)promotion.getPropertyValue( "restrictionType" ) );
	}

}
