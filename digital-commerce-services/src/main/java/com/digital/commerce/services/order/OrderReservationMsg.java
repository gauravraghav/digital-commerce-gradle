package com.digital.commerce.services.order;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderReservationMsg implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String productId;
	private String productName;
	private String brandName;
	private String errorCode;
	private String errorMsg;
}
