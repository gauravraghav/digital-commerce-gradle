package com.digital.commerce.services.pricing.reprice;

import lombok.Getter;
import lombok.Setter;

/*  */
@Getter
@Setter
public class Item {
	private long	timestamp;
	private String	type;
}
