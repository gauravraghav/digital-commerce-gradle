package com.digital.commerce.services.scheduledrequest;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.ldap.DigitalLdapTools;
import com.digital.commerce.common.scheduledrequest.ScheduledRequestService;
import com.digital.commerce.common.scheduledrequest.ScheduledRequestScheduledJob;
import com.digital.commerce.common.scheduledrequest.ScheduledRequest;
import com.digital.commerce.common.scheduledrequest.ScheduledRequestManager;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.scheduledrequest.domain.AddScheduledJobRequest;
import com.digital.commerce.services.scheduledrequest.domain.AddScheduledJobResponse;
import com.digital.commerce.services.scheduledrequest.domain.CurrentComponentPropertyRequest;
import com.digital.commerce.services.scheduledrequest.domain.CurrentComponentPropertyResponse;
import com.digital.commerce.services.utils.DigitalServiceConstants;

import atg.beans.DynamicBeans;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.scheduler.Schedulable;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked","deprecation"})
@Getter
@Setter
public class DigitalScheduledJobComponent extends ApplicationLoggingImpl {

	private static final String CLASSNAME = "DigitalScheduledJobComponent";

	private ScheduledRequestManager scheduledRequestManager;

	private MessageLocator messageLocator;
	
	private DigitalServiceConstants dswConstants;
	
	private DigitalLdapTools ldapTools;

	public DigitalScheduledJobComponent() {
		super(DigitalScheduledJobComponent.class.getName());
	}

	/**
	 * 
	 * @return
	 */
	public AddScheduledJobResponse addScheduledJobRequest(AddScheduledJobRequest addScheduledJobRequest) {
		String METHOD_NAME = "ADD_SHCEDULED_JOB_REQUEST";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		AddScheduledJobResponse response = new AddScheduledJobResponse();

		try {
			/*
			 * check creds against LDAP
			 */
			ScheduledRequest scheduledRequest = new ScheduledRequest();

			scheduledRequest.setCreationDate(new Timestamp((new Date()).getTime()));

			if (DigitalStringUtil.isBlank(addScheduledJobRequest.getComponentName())
					|| (DigitalStringUtil.isBlank(addScheduledJobRequest.getComponentProperty())
							&& DigitalStringUtil.isBlank(addScheduledJobRequest.getComponentMethod()))) {
				response.setRequestSuccess(false);
				response.getGenericExceptions().add(new ResponseError("-1",
						"Scheduled job requires a component name and either a component property or compononet method"));
			} else {
				scheduledRequest.setComponentName(addScheduledJobRequest.getComponentName());
				scheduledRequest.setComponentProperty(addScheduledJobRequest.getComponentProperty());
				scheduledRequest.setComponentMethod(addScheduledJobRequest.getComponentMethod());
				scheduledRequest.setNewValue(addScheduledJobRequest.getNewValue());
				final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy HH:mm");
				Date schedule = DATE_FORMAT.parse(addScheduledJobRequest.getStringifiedSchedule());

				if (schedule.before(new Date())) {
					response.setRequestSuccess(false);
					response.getGenericExceptions().add(new ResponseError("-1", "Scheduled date is in the past"));
				} else {

					if (DigitalStringUtil.isEmpty(addScheduledJobRequest.getComponentName())
							|| ComponentLookupUtil.lookupComponent(addScheduledJobRequest.getComponentName()) == null) {
						response.setRequestSuccess(false);
						response.getGenericExceptions()
								.add(new ResponseError("-1", String.format("Scheduled component (%s) does not exist",
										addScheduledJobRequest.getComponentName())));
					} else {
						// check login credentials
						if (DigitalStringUtil.isBlank(addScheduledJobRequest.getLogin()) || DigitalStringUtil.isBlank(addScheduledJobRequest.getPassword())) {
							// login and password must be populated
							response.getGenericExceptions()
							.add(new ResponseError("-1", String.format("Login and Password are required",
									addScheduledJobRequest.getComponentName())));
						} else if (!verifyDashboardLoginRules(addScheduledJobRequest.getLogin(), addScheduledJobRequest.getPassword())) {
							// login did not pass
							response.getGenericExceptions()
							.add(new ResponseError("-1", String.format("User is not authenticated for this service",
									addScheduledJobRequest.getComponentName())));
						} else {
							// login passed
							scheduledRequest.setSchedule(schedule);

							scheduledRequest.setStringifiedSchedule(addScheduledJobRequest.getStringifiedSchedule());
							scheduledRequest.setServerNames(new HashSet(addScheduledJobRequest.getServerNames()));

							getScheduledRequestManager().createAndBroadcast(scheduledRequest);

							response.setRequestSuccess(true);								
						}
					}
				}
			}

		} catch (Exception e) {
			logError("Error while retrieving addShopWithoutACardRequest", e);

			response.getGenericExceptions().add(new ResponseError("-1", e.getMessage()));
			response.setRequestSuccess(false);

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return response;
	}

	private boolean verifyDashboardLoginRules(String pLogin, String pPassword) {
		if (DigitalStringUtil.isNotBlank(pLogin)
				&& DigitalStringUtil.isNotBlank(pPassword)) {
			try {
				return getLdapTools().doUserAuthetication(pLogin, pPassword,
						null,
						Arrays.asList(getDswConstants().getDashboardRole()));
			} catch (DigitalAppException de) {
				logError("Exeption while authenticating user: " + pLogin);
				return false;
			}
		} else {
			logError("Login and Password must be populated ");
			return false;
		}
	}
	
	public CurrentComponentPropertyResponse currentComponentPropertyRequest(
			CurrentComponentPropertyRequest currentComponentPropertyRequest) {
		String METHOD_NAME = "CURRENT_COMPONENT_PROPERTY_REQUEST";

		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);

		CurrentComponentPropertyResponse response = new CurrentComponentPropertyResponse();

		try {
			/*
			 * check creds against LDAP
			 */
			Object component = ComponentLookupUtil.lookupComponent(currentComponentPropertyRequest.getComponentName());

			if (DigitalStringUtil.isEmpty(currentComponentPropertyRequest.getComponentName()) || component == null) {
				response.setRequestSuccess(false);
				response.getGenericExceptions()
						.add(new ResponseError("-1", String.format("Scheduled component (%s) does not exist",
								currentComponentPropertyRequest.getComponentName())));
			} else {

				String currentValue = DynamicBeans.getPropertyValueAsString(component,
						currentComponentPropertyRequest.getComponentProperty());

				Scheduler scheduler = getScheduledRequestManager().getScheduler();
				ScheduledJob theScheduledJob = null;
				Schedulable theSchedulable = null;
				int scheduledJobCount = scheduler.getScheduledJobCount();
				for (int i = 0; i <= scheduledJobCount; i++) {
					theScheduledJob = scheduler.findScheduledJob(i);

					if (theScheduledJob != null) {
						theSchedulable = theScheduledJob.getSchedulable();
						if (theSchedulable instanceof ScheduledRequestService) {

							ScheduledRequestScheduledJob scheduledRequestScheduledJob = (ScheduledRequestScheduledJob) theScheduledJob;
							String theComponent = scheduledRequestScheduledJob.getScheduledRequest().getComponentName();
							String theProperty = scheduledRequestScheduledJob.getScheduledRequest().getComponentProperty();

							if (DigitalStringUtil.equalsIgnoreCase(currentComponentPropertyRequest.getComponentName(),
									theComponent)
									&& DigitalStringUtil.equals(currentComponentPropertyRequest.getComponentProperty(),
											theProperty)) {

								response.setScheduledTime(
										scheduledRequestScheduledJob.getScheduledRequest().getSchedule().toLocaleString());
								response.setDoesHaveScheduledChange(true);
								response.setScheduledChangeNewValue(
										scheduledRequestScheduledJob.getScheduledRequest().getNewValue());
							}
						}
					}
				}

				response.setCurrentValue(currentValue);
				response.setRequestSuccess(true);
			}

		} catch (Exception e) {
			logError("Error while retrieving addShopWithoutACardRequest", e);

			response.getGenericExceptions().add(new ResponseError("-1", e.getMessage()));
			response.setRequestSuccess(false);

		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(CLASSNAME, METHOD_NAME);
		}

		return response;
	}
}
