/**
 * 
 */
package com.digital.commerce.services.pricing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.promotion.DigitalPromotionTools;
import com.digital.commerce.services.promotion.DigitalGWPManager;

import atg.commerce.CommerceException;
import atg.commerce.inventory.InventoryException;
import atg.commerce.inventory.InventoryManager;
import atg.commerce.order.ChangedProperties;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.ItemPricingEngineImpl;
import atg.commerce.pricing.PricingCommerceItem;
import atg.commerce.pricing.PricingContext;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.QualifiedItem;
import atg.commerce.pricing.definition.DiscountDetail;
import atg.commerce.promotion.GiftWithPurchaseSelectionChoice;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;
import atg.web.messaging.UserMessage;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalItemPricingEngine extends ItemPricingEngineImpl {

	private boolean checkForPriceChanges;

	private PricingModelHolder pricingModelHolder;

//	private DigitalPromotionTools dswPromotionTools;

	private Profile profile;

//	private DigitalGWPManager dswGwpManager;

	private InventoryManager inventoryManager;
	
	private DigitalGWPManager dswGwpManager;
	
	private DigitalPromotionTools dswPromotionTools;

	private MessageLocator messageLocator;

	/**
	 * Whether the a check for price changes should occur
	 * 
	 * @param checkForPriceChanges
	 */
	public void setCheckForPriceChanges(boolean checkForPriceChanges) {
		this.checkForPriceChanges = checkForPriceChanges;
	}

	/*
	 * Extends the standard pricing services to check for a price change, Item
	 * pricing is unchange otherwise, this method simply call's the superclass's
	 * pricing method
	 * 
	 * @see
	 * atg.commerce.pricing.ItemPricingEngineImpl#priceEachItem(java.util.List,
	 * java.util.Collection, java.util.Locale, atg.repository.RepositoryItem,
	 * java.util.Map)
	 */
	public List priceEachItem(List pItems, Collection pPricingModels,
			Locale pLocale, RepositoryItem pProfile, Map pExtraParameters)
			throws PricingException {
		final String METHOD_NAME="priceEachItem";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		
			List<ItemPriceInfo> priceInfos = super.priceEachItem(pItems, pPricingModels, pLocale,
					pProfile, pExtraParameters);
			if (isCheckForPriceChanges() && priceInfos != null) {
				int x=0;
				for (ItemPriceInfo itemPriceInfo : priceInfos) {
					checkPriceChange(itemPriceInfo,(CommerceItem) pItems.get(x));
					x++;
				}
			}
			return priceInfos;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/*
	 * Extends the standard pricing services to check for a price change, Item
	 * pricing is unchange otherwise, this method simply call's the superclass's
	 * pricing method
	 * 
	 * @see
	 * atg.commerce.pricing.ItemPricingEngineImpl#priceItem(atg.commerce.order
	 * .CommerceItem, java.util.Collection, java.util.Locale,
	 * atg.repository.RepositoryItem, java.util.Map)
	 */
	public ItemPriceInfo priceItem(CommerceItem pItem,
			Collection pPricingModels, Locale pLocale, RepositoryItem pProfile,
			Map pExtraParameters) throws PricingException {
		final String METHOD_NAME="priceItem";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				
			ItemPriceInfo priceInfo = super.priceItem(pItem, pPricingModels,
					pLocale, pProfile, pExtraParameters);
			if (isCheckForPriceChanges() && priceInfo != null) {
				checkPriceChange(priceInfo, pItem);
			}
			return priceInfo;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/**
	 * Checks if a price change has occured on this item based on the unit
	 * prices (listPrice and salePrice)
	 * 
	 * @param priceInfo
	 * @param item
	 */
	protected void checkPriceChange(ItemPriceInfo priceInfo, CommerceItem item) {
		final String PRICE_CHANGED_PROP="priceChanged";
		final String PRE_PRICE_PROP="prevPrice";
		Double prevPrice = new Double(0.0);
		if(!(item instanceof PricingCommerceItem)){// GWP may use PricingCommerceItem - v11 update
			prevPrice = (Double) ((ChangedProperties) item).getPropertyValue("prevPrice");
		}else{
			return;
		}
		double newPrice;
		if (priceInfo.isOnSale()) {
			newPrice = priceInfo.getSalePrice();
		} else {
			newPrice = priceInfo.getListPrice();
		}

		if (prevPrice.doubleValue() == 0.0) {
			((ChangedProperties) item).setPropertyValue(PRICE_CHANGED_PROP,
					new Boolean(false));
		} else if (prevPrice.doubleValue() != newPrice) {

			((ChangedProperties) item).setPropertyValue(PRICE_CHANGED_PROP,
					new Boolean(true));
		} else {
			((ChangedProperties) item).setPropertyValue(PRICE_CHANGED_PROP,
					new Boolean(false));
		}
		((ChangedProperties) item).setPropertyValue(PRE_PRICE_PROP, new Double(
				newPrice));
	}

	/*
	 * Extends the standard pricing services to check for a price change, Item
	 * pricing is unchange otherwise, this method simply call's the superclass's
	 * pricing method
	 * 
	 * @see
	 * atg.commerce.pricing.ItemPricingEngineImpl#priceItems(java.util.List,
	 * java.util.Collection, java.util.Locale, atg.repository.RepositoryItem,
	 * atg.commerce.order.Order, java.util.Map)
	 */
	public List priceItems(List pItems, Collection pPricingModels,
			Locale pLocale, RepositoryItem pProfile, Order pOrder,
			Map pExtraParameters) throws PricingException {
		
		final String METHOD_NAME="priceItems";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			List<ItemPriceInfo> priceInfos = super.priceItems(pItems, pPricingModels, pLocale,
					pProfile, pOrder, pExtraParameters);
			if (isCheckForPriceChanges() && priceInfos != null) {
				int x=0;
				for (ItemPriceInfo priceInfo: priceInfos) {
					checkPriceChange(priceInfo,(CommerceItem) pItems.get(x));
					x++;
				}
			}
			return priceInfos;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	@Override
	protected Collection<RepositoryItem> applyPromotions(
			Collection pPricingModels, int pPricingMethod,
			List pItemPriceQuotes, List pItems, PricingContext pPricingContext,
			Map pExtraParameters) throws PricingException {

		try {
			if ((pPricingModels != null) && (pPricingModels.size() >= 0)) {
				if (isLoggingDebug()) {
					logDebug("# of promotions before vetoing: "
							+ pPricingModels.size());
				}
				Collection<RepositoryItem> pricingModels = vetoPromotionsForEvaluation(                                                         
						pPricingContext, pExtraParameters, pPricingModels);

				if (pricingModels != null) {
					if (isLoggingDebug()) {
						logDebug("# of promotions after vetoing: "
								+ pricingModels.size());
					}

					Collection removeModel = new ArrayList();

					for (RepositoryItem model : pricingModels) {
						pPricingContext.setPricingModel(model);

						Collection qualifiedItemsToProcess = getQualifierService(
								model, pExtraParameters).findQualifyingItems(
								pPricingContext, pExtraParameters);

						Collection qualifiedItems = new ArrayList();
						 if (qualifiedItemsToProcess!=null && qualifiedItemsToProcess.size()>0) {
							for (Object qualifiedObject : qualifiedItemsToProcess) {
								QualifiedItem qualifiedItem = (QualifiedItem) qualifiedObject;
								if (qualifiedItem != null && qualifiedItem.getDiscount() != null 
										&& "gwp".equals(qualifiedItem.getDiscount().getCalculatorType())) 
								{
									List<DiscountDetail> discountDetails = qualifiedItem
											.getDiscount().getDiscountDetails();
	
									for (DiscountDetail discountDetail : discountDetails)
									{
	
										String pGiftType = (String) discountDetail
												.getAttributes().get("giftType");
										String pGiftDetail = (String) discountDetail
												.getAttributes().get("giftDetail");
										@SuppressWarnings("unused")
										String giftDetails = (String) discountDetail
												.getAttributes().get("giftDetail");
	
										final GiftWithPurchaseSelectionChoice choice = getDswGwpManager()
												.getGWPGiftItem(pGiftType,
														pGiftDetail,
														pExtraParameters);
										if (choice != null
												&& choice.getSkuCount() > 0) {
											final RepositoryItem sku = choice
													.getSkus().iterator().next();
											final int invStatus = getInventoryManager()
													.queryAvailabilityStatus(
															sku.getRepositoryId());
	
											if (invStatus != InventoryManager.AVAILABILITY_STATUS_IN_STOCK) {
												if (isLoggingDebug()) {
													logDebug("SKU "
															+ sku.getRepositoryId()
															+ " is not in stock. Promotion will not be applied.");
												}
												//pExtraParameters.get("pricingModelHolder");
												setPricingModelHolder((PricingModelHolder)pExtraParameters.get("pricingModelHolder"));
												PricingContext pricingContextItem = (PricingContext)pExtraParameters.get("pricingContext");
												setProfile((Profile)pricingContextItem.getProfile());
												getDswPromotionTools()
														.revokePromotion(
																getProfile(),
																model.getRepositoryId(),
																true,
																getPricingModelHolder());

												Set parentProducts = (Set)sku.getPropertyValue( "parentProducts" );
												String productName = "";
												String brandName = "";
												if( parentProducts != null && !parentProducts.isEmpty() ) {
													RepositoryItem product = (RepositoryItem)parentProducts.iterator().next();
													RepositoryItem dswBrand = (RepositoryItem)product.getPropertyValue( "dswBrand" );
													productName = (String)product.getPropertyValue( "displayName" );
													if (dswBrand!=null) {brandName= (String)(dswBrand).getPropertyValue("displayName");}
												}
												 UserMessage message = new UserMessage("GWPOutOfInventory", "GWPOutOfInventory", brandName + " "+productName +" is no longer available due to high demand.", -10, "information", null);
												 getPricingTools().sendUserMessage(message, null, null);											
												removeModel.add(model);											
											}
										}
									}
								}
							}
							if (isLoggingDebug()) {
								logDebug("qualifiedItems: " + qualifiedItems);
							}
						}
					}
					if (removeModel != null && removeModel.size() > 0) {
						pPricingModels.removeAll(removeModel);
					}
				}
			}
		}

		catch (InventoryException e) {
			throw new PricingException(e.getMessage());			
		} catch (CommerceException e) {
			throw new PricingException(e.getMessage());			
		}
		
		return super.applyPromotions(pPricingModels, pPricingMethod,
				pItemPriceQuotes, pItems, pPricingContext, pExtraParameters);
	}

}
