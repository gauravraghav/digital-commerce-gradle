package com.digital.commerce.services.pricing.calculators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.Order;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderDiscountCalculator;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.definition.DiscountStructure;
import atg.repository.RepositoryItem;

import com.digital.commerce.services.order.DigitalCommerceItem;

@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalOrderDiscountCalculator extends OrderDiscountCalculator {

	/** We are overriding this method simply to populate the custom orderPromoShare map on the commerceItem which is
     * required by  Yantra. There is no custom pricing logic here.
	 * 
	 * @see #updateShippingItemsSubtotalMaps
	 * 
	 * @param pPriceQuote
	 *            OrderPriceInfo representing the current price quote for the
	 *            order
	 * @param pOrder
	 *            The order to price
	 * @param pPricingModel
	 *            A RepositoryItems representing a PricingModel
	 * @param pProfile
	 *            The user's profile
	 * @param pExtraParameters
	 *            A Map of extra parameters to be used in the pricing, may be null */
	public void priceOrder( OrderPriceInfo pPriceQuote, Order pOrder, RepositoryItem pPricingModel, Locale pLocale, RepositoryItem pProfile, Map pExtraParameters ) throws PricingException {
        Object messageOnly = pPricingModel.getPropertyValue("messageOnly");
        if (messageOnly != null && (Boolean) messageOnly) {
            if (isLoggingDebug()) {
                logDebug("Skipping this Promotion as it is only Display Only");
            }
        } else {
            super.priceOrder(pPriceQuote,pOrder,pPricingModel,pLocale,pProfile,pExtraParameters );
            String promoId = pPricingModel.getRepositoryId();
            /*
            We check for the discountStructure object to see if this is a promotion that is actually getting applied. There are lecagcy promos that are being priced
            but arent actually resulting in anything, so we use this to filter them out.
             */
            DiscountStructure discountStruncture = (DiscountStructure) pExtraParameters.get("discountStructure");
            if(discountStruncture!=null) {
                addOrderPromoShares(pOrder, promoId);
            }
        }

	}


    /**
     * This method populates the custom orderPromoShare object. The orderDiscountShare is already calculated by the OOTB
     * code by this point, so we use this along with the previous orderPromoShare entries to calculate the effective
     * share of this promo.
     * @param pOrder
     * @param promoId
     */
    public void addOrderPromoShares(Order pOrder, String promoId){
        List<DigitalCommerceItem> items = pOrder.getCommerceItems();
        for( int i = 0; i < items.size(); i++ ) {
            DigitalCommerceItem item = items.get( i );
            ItemPriceInfo price = item.getPriceInfo();


            Map<String, Double> orderPromoShareMap = item.getOrderPromoShare();
            if( orderPromoShareMap == null ) {
                orderPromoShareMap = new HashMap<>();
                item.setOrderPromoShare(orderPromoShareMap);

            }
            double orderDiscountShare = price.getOrderDiscountShare();
            double previousPromoShares = 0.0;
            for(String key : orderPromoShareMap.keySet()){
               previousPromoShares+=orderPromoShareMap.get(key);
            }
            double promoDiscountShare = orderDiscountShare - previousPromoShares;
            promoDiscountShare = getPricingTools().round(promoDiscountShare);
            if(promoDiscountShare!=0.0) {
            	orderPromoShareMap.put( promoId, new Double( promoDiscountShare ) );
            }
        }

    }
    
    
    /**
     * This method will get the items from the Order, iterate over them checking if the item is discountable 
     * or not. Based on this value it will update (or not) the non-discountable total. 
     * 
     * We need to override this method because OOTB exclusions to not apply to order-level promotions. We will pull any items that meet the exclusion criteria
     * to make sure they are not factored into the discount.
     * 
     * @param pOrder The order that contains the items to iterate over
     * @param pExtraParametersMap A Map of extra parameters
     * @return The combined total for the non-discountable items
     */
    public double getNonDiscountableTotal(Order pOrder, Map pExtraParametersMap) {
        Double nonDiscountableTotal = super.getNonDiscountableTotal(pOrder, pExtraParametersMap);
        
        //Adding dsw exclusions to the non-discountable total
        List items = pOrder.getCommerceItems();
        if(items != null){
        	Iterator itemsIter = items.iterator();
        	while (itemsIter.hasNext()){
        		DigitalCommerceItem item = (DigitalCommerceItem) itemsIter.next();
        		if(item.isExcludedFromPromotion()){
        			if(isLoggingDebug()){
        				logDebug("DigitalBulkOrderDiscountCalculator: subtracting amount " +item.getPriceInfo().getAmount());
        			}
        			nonDiscountableTotal += item.getPriceInfo().getAmount();
        		}
        	}
        }
        
        //NEW exclusion validation
        
        
        return nonDiscountableTotal;
      }

    protected List getItemsToReceiveDiscountShare(Order pOrder)
    {
        List items = pOrder.getCommerceItems();
        List itemsListModified = new ArrayList(items);
        if(items == null)
            return null;
        Iterator itemsListIterator = itemsListModified.iterator();
        do
        {
            if(!itemsListIterator.hasNext())
                break;
            DigitalCommerceItem item = (DigitalCommerceItem)itemsListIterator.next();
            ItemPriceInfo amountInfo = item.getPriceInfo();
            if(item.isExcludedFromPromotion() || (Math.abs(amountInfo.getAmount()) <= 1.0000000000000001E-005D || !amountInfo.getDiscountable())) 
            	itemsListIterator.remove();
           // if((Math.abs(amountInfo.getAmount()) <= 1.0000000000000001E-005D || !amountInfo.getDiscountable()))
                
        } while(true);
        return itemsListModified;
    }


}
