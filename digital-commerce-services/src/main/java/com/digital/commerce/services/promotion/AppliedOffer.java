package com.digital.commerce.services.promotion;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Getter
@Setter
public class AppliedOffer implements Serializable {
	private static final long serialVersionUID = 1L;
	private String couponCode;
	private Set<AppliedPromotion> appliedPromotions = new HashSet<>();

	@Override
	public boolean equals( Object obj ) {
		return EqualsBuilder.reflectionEquals( this, obj );
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode( this );
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString( this );
	}

	@Getter
	@Setter
	@ToString
	public class AppliedPromotion implements Serializable{
		private static final long serialVersionUID = 1L;
		private String	description;
		private String	promotionId;
		private String offerDetailsPageName;
		private String userFriendlyName;
		private String userFriendlyDescription;
		private String shopNowLink;
		private String discountIconType;
		private boolean targetedOffer;	

		@Override
		public boolean equals( Object obj ) {
			return EqualsBuilder.reflectionEquals( this, obj );
		}
	
		@Override
		public int hashCode() {
			return HashCodeBuilder.reflectionHashCode( this );
		}
	}
}
