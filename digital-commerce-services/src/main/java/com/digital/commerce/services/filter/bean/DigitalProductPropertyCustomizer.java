package com.digital.commerce.services.filter.bean;

import atg.commerce.catalog.custom.CustomCatalogTools;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;
import com.digital.commerce.common.logger.DigitalLogger;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class DigitalProductPropertyCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {
	private CustomCatalogTools	tools;
	private String[]			returnFields;
	private boolean				skuLevel	= false;
	DigitalLogger logger = DigitalLogger.getLogger(DigitalProductPropertyCustomizer.class.getName());
	public DigitalProductPropertyCustomizer() {
		super( DigitalProductPropertyCustomizer.class.getName() );
	}

	/*
	 * (non-Javadoc)
	 * @see atg.service.filter.bean.PropertyCustomizer#getPropertyValue(java.lang.Object, java.lang.String, java.util.Map)
	 *
	 * @param pTargetObject
	 * @param pPropertyName
	 * @param pAttributes
	 * @return Object
	 * @throws BeanFilterException
	 */
	@Override
	public Object getPropertyValue(Object pTargetObject, String pPropertyName, Map<String, String> pAttributes)
			throws BeanFilterException {
		if (pTargetObject == null) {
			return "";
		}

		try {
			Map<String, String> result = new HashMap<>();
			RepositoryItem item = null;
			if (pTargetObject instanceof RepositoryItem) {
				String keyValue = (String) ((RepositoryItem) pTargetObject).getPropertyValue(pPropertyName);
				if (this.isSkuLevel()) {
					item = tools.findSKU(keyValue);
				} else {
					item = tools.findProduct(keyValue);
				}
				result.put(pPropertyName, keyValue);
			} else {
				Method method = pTargetObject.getClass()
						.getMethod("get" + pPropertyName.substring(0, 1).toUpperCase() + pPropertyName.substring(1));
				Object propertyValue = method.invoke(pTargetObject);
				if (propertyValue != null) {
					String keyValue = propertyValue.toString();
					if (this.isSkuLevel()) {
						item = tools.findSKU(keyValue);
					} else {
						item = tools.findProduct(keyValue);
					}

					result.put(pPropertyName, keyValue);
				}

			}

			if (item != null) {
				for (String field : this.returnFields) {
					try{
						int subAttribute = field.indexOf(".");
						if (subAttribute > -1) {
							Object keyValue = item.getPropertyValue(field.substring(0, subAttribute));
							if (keyValue != null) {
								result.put(field, ((RepositoryItem) keyValue)
										.getPropertyValue(field.substring(subAttribute + 1)).toString());
							}
							continue;
						}
						Object keyValue = item.getPropertyValue(field);
						if (null != keyValue) {
							result.put(field, keyValue.toString());
						} else {
							result.put(field, "");
						}
					}catch (Exception e){
						logger.error("Unable to load the property " + field + ". ", e);
						result.put(field, "");
					}
				}
			}

			return result;
		} catch (RepositoryException | NoSuchMethodException | SecurityException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

}
