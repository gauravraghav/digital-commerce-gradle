/**
 * 
 */
package com.digital.commerce.services.rewards.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author mmallipu
 *
 */
@Getter
@Setter
public class RewardsViewPointHistoryRequest {
	private String memberId;
	private String startDate;
	private String endDate;
}
