package com.digital.commerce.services.profile;

import com.digital.commerce.common.util.DigitalPredicate;
import com.digital.commerce.services.order.DigitalCreditCard;

import atg.commerce.order.PaymentGroupRelationship;

public class FindPaymentGroupRelationshipWithCreditCard implements DigitalPredicate<Object> {
	@Override
	public boolean apply( Object input ) {
		boolean retVal = false;
		if( input instanceof PaymentGroupRelationship ) {
			retVal = ( (PaymentGroupRelationship)input ).getPaymentGroup() instanceof DigitalCreditCard;
		}
		return retVal;
	}
}
