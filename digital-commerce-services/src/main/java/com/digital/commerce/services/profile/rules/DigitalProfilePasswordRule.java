package com.digital.commerce.services.profile.rules;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.DigitalProfileConstants;

import atg.nucleus.ServiceException;
import atg.repository.RepositoryItem;
import atg.security.PasswordRuleImpl;
import atg.servlet.ServletUtil;
import atg.userprofiling.ProfileUserMessage;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class DigitalProfilePasswordRule extends PasswordRuleImpl {

	private DigitalBaseConstants constants;

	private Pattern PASSWORD_ALPHABETIC_PATTERN;
	private Pattern PASSWORD_UPPER_ALPHABETIC_PATTERN;
	private Pattern PASSWORD_LOWER_ALPHABETIC_PATTERN;
	private Pattern PASSWORD_NUMERIC_PATTERN;
	private Pattern PASSWORD_SPECIAL_CHARACTER_PATTERN;
	private int STRONG_PASSWORD_SCORE = 2;
	/**
	 * 
	 * Checks the given password against a rule
	 * 
	 * @param password
	 * @return true if password passes the rule
	 */
	public boolean checkRule(String password, Map map) {
		String source = ServletUtil.getCurrentRequest().getParameter("source");
		RepositoryItem currentProfile=(RepositoryItem)ServletUtil.getCurrentUserProfile();
		boolean generatedPassword=false;
		if(null!=currentProfile){
			generatedPassword=(Boolean) currentProfile.getPropertyValue("generatedPassword");
		}
		if (!DigitalStringUtil.isBlank(source) && (source.equalsIgnoreCase("stores")) || generatedPassword) {
			return true;
		}
		boolean passed = false;
		passed = this.validatePassword(password);
		return passed;
	}

	/**
	 * Returns the rule description as a message for use in a droplet exception
	 * for display to user
	 * 
	 * @return String
	 */
	public String getRuleResource() {

		return ProfileUserMessage.format("INVALID_PASSWORD_FORMAT", ServletUtil.getUserLocale());
	}

	/**
	 * 
	 * @param password
	 * @return true or false
	 */
	public boolean validatePassword(String password) {

		if (DigitalStringUtil.isEmpty(password)) {
			return false;
		}

		if (password.trim().length() < DigitalProfileConstants.MIN_PASSWORD_LENGTH
				|| password.trim().length() > DigitalProfileConstants.MAX_PASSWORD_LENGTH) {
			return false;
		}

		// Start logic for Password Rules:

		// Password must be at least 8 characters long and include at least 2 of
		// the following: a number, an uppercase/lowercase letter, one of these
		// special characters ! # $ & @ ? % ...
		AtomicLong passScore = new AtomicLong(0);

		// If Password has numeric
		if (PASSWORD_NUMERIC_PATTERN.matcher((CharSequence) password.trim()).find()) {
			passScore.incrementAndGet();
		}
		// If Password has combination of alphabets with Upper or Lower case
		if (PASSWORD_ALPHABETIC_PATTERN.matcher((CharSequence) password.trim()).find()
				&& (PASSWORD_UPPER_ALPHABETIC_PATTERN.matcher((CharSequence) password.trim()).find()
						|| PASSWORD_LOWER_ALPHABETIC_PATTERN.matcher((CharSequence) password.trim()).find())) {
			passScore.incrementAndGet();
		}

		// If Password has special characters
		if (PASSWORD_SPECIAL_CHARACTER_PATTERN.matcher((CharSequence) password.trim()).find()) {
			passScore.incrementAndGet();
		}

		return isStrongPassword(passScore);
	}

	/**
	 * 
	 * @param passScore
	 * @return true or false
	 */
	private boolean isStrongPassword(AtomicLong passScore) {
		return passScore.get() >= STRONG_PASSWORD_SCORE;
	}

	/**
	 * 
	 * @throws ServiceException
	 */
	public void doStartService() throws ServiceException {
		PASSWORD_ALPHABETIC_PATTERN = Pattern.compile(this.getConstants().getPasswordAlphabeticChars());
		PASSWORD_UPPER_ALPHABETIC_PATTERN = Pattern.compile(this.getConstants().getPasswordUpperAlphabeticChars());
		PASSWORD_LOWER_ALPHABETIC_PATTERN = Pattern.compile(this.getConstants().getPasswordLowerAlphabeticChars());
		PASSWORD_NUMERIC_PATTERN = Pattern.compile(this.getConstants().getPasswordNumericChars());
		PASSWORD_SPECIAL_CHARACTER_PATTERN = Pattern.compile(this.getConstants().getPasswordSpecialCharacters());
		STRONG_PASSWORD_SCORE = this.getConstants().getStrongPasswordScore();
	}

}
