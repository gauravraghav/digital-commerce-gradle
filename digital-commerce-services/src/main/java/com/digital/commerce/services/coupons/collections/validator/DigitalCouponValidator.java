package com.digital.commerce.services.coupons.collections.validator;

import java.util.Iterator;

import java.util.Set;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.utils.DigitalServiceConstants;

import atg.commerce.claimable.ClaimableException;
import atg.commerce.claimable.ClaimableManager;
import atg.commerce.claimable.ClaimableTools;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.promotion.PromotionTools;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.collections.validator.CollectionObjectValidator;
import atg.service.util.CurrentDate;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import com.digital.commerce.common.util.ComponentLookupUtil;
import static com.digital.commerce.common.util.ComponentLookupUtil.DSW_BASE_CONSTANTS;
import com.digital.commerce.common.services.DigitalBaseConstants;
import lombok.Getter;
import lombok.Setter;

/**
 * DigitalCouponValidator validates an item based on its
 * properties.
 * 
 */
@Getter
@Setter
public class DigitalCouponValidator extends ApplicationLoggingImpl implements
		CollectionObjectValidator {
	
	private static final String    PERFORM_MONITOR_NAME    = "DigitalCouponValidator";
		
	private ClaimableTools claimableTools;
	private ClaimableManager claimableManager;
	private CurrentDate currentDate;
	private Profile profile;
	private PromotionTools promotionTools;
	private String[] clientRestrictionTypes;
	private String[] promotionRestrictionTypes;
	protected String restrictionTypePropertyName;
	private DigitalServiceConstants	dswConstants;

	public DigitalServiceConstants getDswConstants() {
		if(null==dswConstants){
			dswConstants = (DigitalServiceConstants) ComponentLookupUtil.lookupComponent(DSW_BASE_CONSTANTS);
		}
		return dswConstants;
	}

	/**
	 * Default constructor
	 */
	public DigitalCouponValidator() {
		super(DigitalCouponValidator.class.getName());
	}

	/**
	 * This method validates the passed in object (repository items) based on
	 * whether or not any of its items exist in the
	 * current restriction type.
	 * 
	 * @param object
	 *            to validate
	 * @return true if the object passes validation or if no validation was
	 *         performed.
	 */
	@Override
	public boolean validateObject(Object pObject) {
		String METHOD_NAME = "validateObject";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			if (!(pObject instanceof RepositoryItem)) {
				return false;
			}
	
			// Perform the filtering
			RepositoryItem item = (RepositoryItem) pObject;
	    	
			String pProfileId = getProfile().getRepositoryId();		 
		
			RepositoryItem profile = getPromotionTools().getProfileRepository().getItemForUpdate(pProfileId , getPromotionTools().getProfileItemType());

			String promotionsName = getClaimableTools().getPromotionsPropertyName();
			Set<?> multiplePromotions = (Set<?>)item.getPropertyValue(promotionsName);
			boolean validPromotion = isValidPromotion(multiplePromotions);
			boolean validCoupon = isValidCoupon(item, profile);
			if(validCoupon && validPromotion){	            		
				return true;
			}
		} catch (RepositoryException e) {
			return false;
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
       

		// The item doesn't pass validation
		return false;
	}
	

	
	/**
	 * Validate the coupon usage has not exceeded the max limit
	 * 
	 * @param coupon
	 * @param profile
	 * @return true or false
	 * @throws RepositoryException
	 */
	private boolean isValidCoupon(RepositoryItem coupon, RepositoryItem profile) throws RepositoryException{

		String METHOD_NAME = "isValidCoupon";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);		
			
			if (!getClaimableManager().canClaimCoupon(profile.getRepositoryId(), coupon.getRepositoryId(), true)) {
				return false;
			}
	
			Integer value = (Integer)coupon.getPropertyValue(getClaimableTools().getUsesPropertyName());
			int uses = value != null ? value.intValue() : 0;
			value = (Integer)coupon.getPropertyValue(getClaimableTools().getMaxUsesPropertyName());
			int maxUses = value != null ? value.intValue() : -1;
	
			if (maxUses > -1) {
			  if (uses >= maxUses)
			  {
				  return false;
			  }
			}
			
		} catch (ClaimableException e) {
			logError("Error occurred when checking if coupon is already claimed", e);
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
				
		return true;
	}
	
	/**
	 * Validate if the promotion is enabled & not expired
	 * 
	 * @param multiplePromotions
	 * @return true or false
	 */
	private boolean isValidPromotion(Set<?> multiplePromotions){

		String METHOD_NAME = "isValidPromotion";
		
		boolean validPromotion = true;
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			if ((multiplePromotions == null) || (multiplePromotions.size() == 0)) {
				validPromotion = false;
			}
			else
			{
				validPromotion = isPromoClientRestricted(multiplePromotions);
				
				//If Promotion is not client restricted then check for other restrictions
				if(validPromotion){
					validPromotion = isPromoRestricted(multiplePromotions);
				}
				
				//If Promotion is not restricted then check for validity
				if(validPromotion){
					validPromotion = isPromoExpired(multiplePromotions);
				}
			}
			
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		
		return validPromotion;
	}
	
	private boolean isAppOnlyPromotion(Set<?> multiplePromotions) {
		boolean appOnlyPromotion = false;
		Iterator<?> promotions = multiplePromotions.iterator();
		while(promotions.hasNext()){			
			RepositoryItem promotion = (RepositoryItem) promotions.next();
			String restrictionClientName = (String) promotion.getPropertyValue(getRestrictionTypePropertyName().trim());
				if (restrictionClientName.trim().equalsIgnoreCase("APP")) {
					appOnlyPromotion=true;
					}
			}
		return appOnlyPromotion;		
	}

	/**
	 * 
	 * @param multiplePromotions
	 * @return
	 */
	private boolean isPromoExpired(Set<?> multiplePromotions){
		boolean validPromotion = true;
		Iterator<?> promotions = multiplePromotions.iterator();
		while(promotions.hasNext()){
			PromotionTools promotionTools = getPromotionTools();
			RepositoryItem promotion = (RepositoryItem) promotions.next();				
			if (promotionTools.checkPromotionExpiration(promotion, getCurrentDate().getTimeAsDate())){
				validPromotion = false;
			}
			else{
				// Minimum one promotion is valid (aka enabled, not expired etc) so return true
				return true;
			}
		}		
		return validPromotion;		
	}
	
	/**
	 * True if targeted offers should be filtered out. GetAvailableSavings
	 * service should not return coupons tied to targeted promotions in discount
	 * sections as such coupons are returned under web auth offers section ONLY
	 * for rewards members.
	 * 
	 * @param multiplePromotions
	 * @return true or false
	 */
	private boolean isPromoRestricted(Set<?> multiplePromotions) {
		boolean validPromotion = true;
		if (getPromotionRestrictionTypes() != null) {
			Iterator<?> promotions = multiplePromotions.iterator();
			while (promotions.hasNext()) {
				RepositoryItem promotion = (RepositoryItem) promotions.next();

				for (String retrictionType : getPromotionRestrictionTypes()) {
					// If the client falls in the targeted offer restriction
					if ("TARGETED_OFFER".equalsIgnoreCase(retrictionType)) {
						if (Boolean.TRUE.equals(promotion.getPropertyValue("targetedOffer"))) {
							validPromotion = false;
						}
					}
				}
			}
		}
		return validPromotion;
	}
	
	/**
	 * 
	 * @param multiplePromotions
	 * @return
	 */
	private boolean isPromoClientRestricted(Set<?> multiplePromotions){
		boolean validPromotion = true;
		Iterator<?> promotions = multiplePromotions.iterator();
		while(promotions.hasNext()){			
			RepositoryItem promotion = (RepositoryItem) promotions.next();
			String restrictionClientName = (String) promotion.getPropertyValue(getRestrictionTypePropertyName().trim());
			for(String client : getClientRestrictionTypes()){
				// If the client falls in the restriction
				if (client.trim().equalsIgnoreCase(restrictionClientName)) {
					validPromotion=false;
					//TODO: Validate if the source of origin is APP or CSC
					//TODO: Get order and get the origin of Order value; if the value is androidapp or iosapp; its not restricted
					Order order = ((OrderHolder)ServletUtil.getCurrentRequest().resolveName("/atg/commerce/ShoppingCart")).getCurrent();
					String originOfOrder=order.getOriginOfOrder();
					if(originOfOrder!=null && client.trim().equalsIgnoreCase("APP") && ("androidapp".equalsIgnoreCase(originOfOrder) || "iosapp".equalsIgnoreCase(originOfOrder) || getDswConstants().isPaTool())){
							validPromotion=true;
					}
				}
			}
		}
		return validPromotion;		
	}

}
