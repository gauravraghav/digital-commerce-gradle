package com.digital.commerce.services.order.purchase.valueobject;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import javax.annotation.Generated;
import lombok.Getter;
import lombok.Setter;

@Generated("com.robohorse.robopojogenerator")
@JsonRootName(value = "order")
@Getter
@Setter
public class DigitalOrderRequest {

  @JsonProperty("afterPay")
  private AfterPay afterPay;

  @JsonProperty("creditCard")
  private CreditCard creditCard;

  @JsonProperty("exchange")
  private boolean exchange;

  @JsonProperty("paymentType")
  private String paymentType;
}