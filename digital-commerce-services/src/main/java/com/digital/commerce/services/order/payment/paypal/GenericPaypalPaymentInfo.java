package com.digital.commerce.services.order.payment.paypal;

import atg.commerce.order.Order;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/** 
 * PaypalpaymentInfo used to do authorize operations
*/
@Getter
@Setter
@ToString
public class GenericPaypalPaymentInfo implements PaypalPaymentInfo {
	private static final long	serialVersionUID	= 1L;
	private String				paypalPayerId;
	private String				paypalEmail;
	private String				cardinalOrderId;
	private Order				order;
	private double				amount;
	private boolean				newUser;
	private String ipAddress;
}
