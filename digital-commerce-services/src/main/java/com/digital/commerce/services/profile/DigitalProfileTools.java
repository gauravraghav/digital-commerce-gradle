package com.digital.commerce.services.profile;

import static com.digital.commerce.constants.DigitalProfileConstants.PO_BOX_PATTERN;
import static com.digital.commerce.constants.DigitalProfileConstants.USER_STATUS_ANONYMOUS;
import static com.digital.commerce.constants.DigitalProfileConstants.USER_STATUS_ANONYMOUS_COOKIED;
import static com.digital.commerce.constants.DigitalProfileConstants.USER_STATUS_COOKIED;
import static com.digital.commerce.constants.DigitalProfileConstants.USER_STATUS_LOGGED_IN;
import static com.digital.commerce.constants.DigitalProfileConstants.USPS_STATUS_ACCEPTED;
import static com.digital.commerce.constants.DigitalProfileConstants.USPS_STATUS_SUGGESTED;

import atg.adapter.gsa.ChangeAwareSet;
import atg.adapter.gsa.GSAItem;
import atg.adapter.gsa.GSAPropertyDescriptor;
import atg.commerce.CommerceException;
import atg.commerce.locations.Coordinate;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupRelationship;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.profile.CommerceProfileTools;
import atg.commerce.profile.CommercePropertyManager;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.dtm.UserTransactionDemarcation;
import atg.multisite.Site;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryUtils;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.security.IdentityManager;
import atg.security.PasswordHasher;
import atg.service.lockmanager.DeadlockException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileRequestTools;
import atg.userprofiling.PropertyManager;
import atg.userprofiling.address.AddressTools;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.services.inventory.InventoryHelper;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.HTTPUtils;
import com.digital.commerce.constants.DigitalProfileConstants;
import com.digital.commerce.constants.PaypalConstants.PaypalPaymentGroupPropertyManager;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceResponse;
import com.digital.commerce.integration.reward.bts.BtsRewardService;
import com.digital.commerce.integration.reward.domain.Contact;
import com.digital.commerce.integration.reward.domain.Flags;
import com.digital.commerce.integration.reward.domain.Person;
import com.digital.commerce.integration.reward.domain.Person.Gender;
import com.digital.commerce.integration.reward.domain.Points;
import com.digital.commerce.integration.reward.domain.RewardServiceRequest;
import com.digital.commerce.integration.reward.domain.RewardServiceResponse;
import com.digital.commerce.services.common.AddressType;
import com.digital.commerce.services.common.CountryCode;
import com.digital.commerce.services.common.DigitalAddress;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.common.validator.DigitalStartEndDateValidator;
import com.digital.commerce.services.i18n.DigitalCustomDateFormatter;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalCommerceItemManager;
import com.digital.commerce.services.order.DigitalCreditCard;
import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.DigitalRepositoryContactInfo;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.order.payment.creditcard.DigitalCreditCardServiceStatus;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.profile.vo.Offer;
import com.digital.commerce.services.promotion.DigitalPromotionTools;
import com.digital.commerce.services.region.RegionProfile;
import com.digital.commerce.services.rewards.domain.RewardsUpdateCustomerRequest;
import com.digital.commerce.services.rewards.domain.RewardsUpdateCustomerResponse;
import com.digital.commerce.services.storelocator.DigitalCoordinateManager;
import com.digital.commerce.services.storelocator.DigitalGeoLocationUtil;
import com.digital.commerce.services.storelocator.DigitalGeoLocatorProvider;
import com.digital.commerce.services.storelocator.DigitalStoreLocatorModel;
import com.digital.commerce.services.utils.DigitalAddressUtil;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import com.digital.commerce.services.validator.DigitalBasicAddressValidator;
import com.digital.commerce.services.validator.DigitalUSPSAddressValidator;
import com.google.common.collect.Iterables;
import lombok.Getter;
import lombok.Setter;
import java.beans.IntrospectionException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

@SuppressWarnings({"unchecked","rawtypes","unused"})
@Getter
@Setter
public class DigitalProfileTools extends CommerceProfileTools {
	
	private static final DigitalLogger	logger	= DigitalLogger.getLogger( DigitalProfileTools.class );

	private static final String GIFTCARD_ITEM_TYPE = "giftCard";

	private static final String CLASSNAME = DigitalProfileTools.class.getName();

	private DigitalRewardsManager rewardCertificateManager;

	private DigitalWebOfferTools webOfferTools;

	private DigitalAddressUtil addressUtil;

	private ProfileRequestTools profileRequestTools;

	private InventoryHelper inventoryTools;
	private DigitalServiceConstants dswConstants;

	private BtsRewardService rewardService;
	private Repository locationRepository;
	private DigitalGeoLocatorProvider geoLocatorProvider;
	private DigitalCoordinateManager locationManager;
	private DigitalStartEndDateValidator startEndDateValidator;
	private DigitalUSPSAddressValidator addressValidator;
	private MessageLocator messageLocator;
	private DigitalBasicAddressValidator dswBasicAddressValidator;
	private DigitalShippingGroupManager dswShippingGroupManager;
	private DigitalCustomDateFormatter 		customDateFormatter;

	//NC419910 : KTLO1-174
	private Map<String, String> rewardsToPlatformAlertsMapping;

	private static final String FAIL = "Failed";

	private static final String SUCCESS = "Success";

	private static final String FAVOTIRE_STORES = "favoriteStores";

	private static final String LOCATION = "location";

	private static final String USER = "user";
	private static final String DEFAULT_CREDIT_CARD = "defaultCreditCard";
	private static final String CREDIT_CARDS = "creditCards";
	private static final String SHIPPING_ADDRESS = "shippingAddress";
	private static final String ADDRESS_TYPE_USA = "USA";
	private static final String ADDRESS_TYPE_MILITARY = "MIL";
	private static final String STATUS = "Status";

	private static final String REMOVE_ADDRESS_SUCCESS = "remove_address_success";
	private static final String REMOVE_ADDRESS_NOT_EXIST = "remove_address_not_exists";
	private static final String REMOVE_ADDRESS_NOT_ALLOWED = "remove_address_not_allowed";

	private static final String REMOVE_CARD_SUCCESS = "remove_card_success";
	private static final String REMOVE_CARD_NOT_EXIST = "remove_card_not_exists";
	private static final String MAKE_DEFAULT_CARD_ERROR = "make_default_card_error";
	
	private String 						storeDetailsQuery;
	private static final String SHIP_TO_STORE = "shipToStore";
	private static final String PICKUP_IN_STORE = "pickupInStore";
	private static final String SHIP_TO_STORE_BOSTS = "shipToStoreBOSTS";
	private static final String PICKUP_IN_STORE_BOPIS = "pickupInStoreBOPIS";
	private static final String IS_PAPERLESS_CERT = "isPaperlessCert";
	private static final String CERT_DENOMINATION = "certDenomination";
  private static final String TIER_COOKIE_NAME = "TIER";
	
	private DigitalGeoLocationUtil geoLocationUtil;

	private boolean enableTransactionLock;

	private static final String PERFORM_MONITOR_NAME    		= "DigitalProfileTools";

	/**
	 * 
	 * @param order
	 * @return
	 * @throws RepositoryException
	 */
	public String getLoyaltyNumber(Order order) throws RepositoryException {		
		if (DigitalStringUtil.isBlank(order.getProfileId()) || DigitalStringUtil.isBlank((String) getProfileForOrder(order)
				.getPropertyValue(getCommercePropertyManager().getLoyaltyNumberPropertyName()))) {
			return "NA";
		} else {
			return (String) getProfileForOrder(order)
					.getPropertyValue(getCommercePropertyManager().getLoyaltyNumberPropertyName());
		}
	}
	
	/**
	 * 
	 * @param order
	 * @return
	 */
	public boolean isContactInfoPresent(Order order){
		List<PaymentGroup> listPaymentGroups = order.getPaymentGroups();
		boolean isInfoPresent = false;
		ContactInfo billingAddr = null;
		if(listPaymentGroups!=null){
			for (PaymentGroup paymentGroup : listPaymentGroups){
				
				if (paymentGroup instanceof CreditCard) {
					//get the payment group if its exists
					CreditCard creditCard = (CreditCard) paymentGroup;
					billingAddr = (ContactInfo)creditCard.getBillingAddress();
					if(billingAddr != null && DigitalStringUtil.isNotEmpty(billingAddr.getEmail()) && DigitalStringUtil.isNotEmpty(billingAddr.getPhoneNumber())){
							isInfoPresent=true;
							break;
					}
				}else if( paymentGroup instanceof PaypalPayment ) {
					PaypalPayment paypalPayment = (PaypalPayment)paymentGroup;
					billingAddr = (ContactInfo)paypalPayment.getBillingAddress();
					if(billingAddr != null && DigitalStringUtil.isNotEmpty(billingAddr.getEmail()) && DigitalStringUtil.isNotEmpty(billingAddr.getPhoneNumber())){
							isInfoPresent=true;
							break;
					}
				}else{
					List<HardgoodShippingGroup> hgShippingGroups = getDswShippingGroupManager().getHardgoodShippingGroups(order);
					ContactInfo shippingAddr = null;
					if(hgShippingGroups!=null){
						for (HardgoodShippingGroup hgShippingGroup : hgShippingGroups){
							shippingAddr= (ContactInfo)hgShippingGroup.getShippingAddress();
							if(shippingAddr != null && DigitalStringUtil.isNotEmpty(shippingAddr.getEmail())&& DigitalStringUtil.isNotEmpty(shippingAddr.getPhoneNumber())){
									isInfoPresent=true;
									break;
							}
						}
					}
				}
			}
			if(listPaymentGroups.isEmpty() && !( order.getPriceInfo().getTotal() > 0.00) 
					&& order.getCommerceItemCount() > 0 
					&& ! ((DigitalCommerceItemManager)getOrderManager().getCommerceItemManager()).hasGiftCartItem(order)){
				
				//Check for contact info details for payment groups removed scenario when user wants to use only rewards as tender.
				List<HardgoodShippingGroup> hgShippingGroups = getDswShippingGroupManager().getHardgoodShippingGroups(order);
				ContactInfo shippingAddr = null;
					if(hgShippingGroups!=null){
						for (HardgoodShippingGroup hgShippingGroup : hgShippingGroups){
							shippingAddr= (ContactInfo)hgShippingGroup.getShippingAddress();
							if(shippingAddr != null && DigitalStringUtil.isNotEmpty(shippingAddr.getEmail())&& DigitalStringUtil.isNotEmpty(shippingAddr.getPhoneNumber())){
									isInfoPresent=true;
						}
					}
				}
				
			}
			
		}
		return  isInfoPresent;
	}
	

	/**
	 * 
	 * @param profile
	 * @return
	 * @throws RepositoryException
	 */
	public String getLoyaltyNumber(Profile profile) throws RepositoryException {
		return (String) profile.getPropertyValue(getCommercePropertyManager().getLoyaltyNumberPropertyName());
	}

	/**
	 * Checks if user is migrated from legacy loyalty system or not
	 * 
	 * @param login
	 * @return
	 */
	public boolean isUserMigrated(String login) {
		if (this.isLoggingDebug()) {
			logDebug("Checking if user with login:" + login + " is a migrated user...");
		}

		RqlStatement statement;
		try {
			DigitalCommercePropertyManager pmgr = getCommercePropertyManager();
			RepositoryItemDescriptor usersDesc = getProfileRepository().getItemDescriptor("user");
			RepositoryView userRepView = usersDesc.getRepositoryView();

			statement = RqlStatement.parseRqlStatement("login = ?0");
			Object params[] = new Object[1];
			params[0] = new String(login);

			RepositoryItem[] userItems = statement.executeQuery(userRepView, params);
			if (userItems == null)
				return false; /* no user with that login id found */

      return ((Boolean) userItems[0].getPropertyValue(pmgr.getNewUserFlagPropertyName()))
          .booleanValue();
		} catch (RepositoryException e) {
			if (this.isLoggingDebug()) {
				logDebug("An exception occurred in prelogin phase while determining if the user is a migrated user", e);
			}
			return false;
		}
	}

	/**
	 * Checks if user is migrated from legacy loyalty system or not
	 * 
	 * @param loyaltyNumber
	 * @return
	 */
	public String findProfileIdByLoyaltyNumber(String loyaltyNumber) {
		String profileId = null;
		if (this.isLoggingDebug()) {
			logDebug("findProfileByLoyaltyNumber : " + loyaltyNumber );
		}
		if(DigitalStringUtil.isBlank(loyaltyNumber)){
			return null;
		}
		RqlStatement statement;
		try {
			RepositoryItemDescriptor usersDesc = getProfileRepository().getItemDescriptor("user");
			RepositoryView userRepView = usersDesc.getRepositoryView();

			statement = RqlStatement.parseRqlStatement("loyaltyNumber = ?0");
			Object params[] = new Object[1];
			params[0] = new String(loyaltyNumber);

			RepositoryItem[] userItems = statement.executeQuery(userRepView, params);
			if (userItems != null){
				profileId = userItems[0].getRepositoryId();
			}
			return profileId;
		} catch (RepositoryException e) {
			logError("An exception occurred in findProfileIdByLoyaltyNumber :: ", e);
			return profileId;
		}
	}

	/**
	 * Get Login based on the given ATG Profile ID
	 *
	 * @param profileID
	 * @return
	 */
	public String findLoginByProfileID(String profileID) {
		String login = null;
		if (this.isLoggingDebug()) {
			logDebug("findLoginByProfileID : " + profileID );
		}
		if(DigitalStringUtil.isBlank(profileID)){
			return null;
		}
		RqlStatement statement;
		try {
			RepositoryItemDescriptor usersDesc = getProfileRepository().getItemDescriptor("user");
			RepositoryView userRepView = usersDesc.getRepositoryView();

			statement = RqlStatement.parseRqlStatement("id = ?0");
			Object params[] = new Object[1];
			params[0] = new String(profileID);

			RepositoryItem[] userItems = statement.executeQuery(userRepView, params);
			if (userItems != null){
				Object loginNameobj = userItems[0].getPropertyValue(this.getPropertyManager().getLoginPropertyName());
				if (loginNameobj != null)
					login = loginNameobj.toString();
			}
			return login;
		} catch (RepositoryException e) {
			logError("An exception occurred in findLoginByProfileID :: ", e);
			return login;
		}
	}
	
	/**
	 * Validate if given credit card exists in profile
	 * 
	 * @param creditCard
	 * @param profile
	 * @return true/false
	 * 
	 */
	public boolean isCreditCardPresentInProfile(Profile profile, CreditCard creditCard) {
		DigitalCreditCard dswCreditCard = new DigitalCreditCard();
		CommercePropertyManager cpm = getCommercePropertyManager();
		if (null != creditCard && creditCard instanceof DigitalCreditCard) {
			dswCreditCard = (DigitalCreditCard) creditCard;
		}
		Map usersCreditCardMap = getUsersCreditCardMap(profile);
		if (usersCreditCardMap.keySet().iterator().hasNext()) {
			Iterator iter = usersCreditCardMap.keySet().iterator();
			while (iter.hasNext()) {
				String key = (String) iter.next();
				RepositoryItem ccRepItem = (RepositoryItem) usersCreditCardMap.get(key);
				String tokenValue=(String)ccRepItem.getPropertyValue(DigitalCreditCardServiceStatus.TOKEN_VALUE);
				String expirationMonth=(String)ccRepItem.getPropertyValue(cpm.getCreditCardExpirationMonthPropertyName());
				String expirationYear=(String) ccRepItem.getPropertyValue(cpm.getCreditCardExpirationYearPropertyName());
				if(DigitalStringUtil.isNotEmpty(tokenValue) && DigitalStringUtil.isNotEmpty(expirationMonth) && DigitalStringUtil.isNotEmpty(expirationYear)){
					if(tokenValue.equalsIgnoreCase(dswCreditCard.getTokenValue()) && expirationMonth.equals(dswCreditCard.getExpirationMonth()) && expirationYear.equals(dswCreditCard.getExpirationYear())){
						if (this.isLoggingDebug()) {
							logDebug("Card :: " + key + " with TokenValue: "+ tokenValue+ "is duplicate  marking flag not to save card to profile ");
						}
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Check if the profile has saved credit card
	 *
	 * @param profile
	 * @return true/false
	 *
	 */
	public boolean isSavedCreditCardExistInProfile(Profile profile) {
		DigitalCreditCard dswCreditCard = new DigitalCreditCard();
		CommercePropertyManager cpm = getCommercePropertyManager();
		Map usersCreditCardMap = getUsersCreditCardMap(profile);
		if (usersCreditCardMap.size() > 0 ) {
			return true;
		}
		return false;
	}


	/**
	 * Validate if user can save more cards to his profile
	 * 
	 * @param profile
	 * @return true/false
	 * 
	 */
	public boolean isCreditCardMaxLimitExceeded(Profile profile) {
		Map usersCreditCardMap = getUsersCreditCardMap(profile);
		if(usersCreditCardMap.size() >= MultiSiteUtil.getCreditCardMaxLimit()){
			return true;
		}
		return false;
	}
	
	
	/**
	 * Copies the credit card to the profile and makes it the new default credit
	 * card on the profile. A unique nickname is automatically generated.
	 * 
	 * @param creditCard
	 * @param profile
	 */
	public void copyCreditCardToProfile(Profile profile, CreditCard creditCard) {
		DigitalCreditCard cc = new DigitalCreditCard();
		if (null != creditCard && creditCard instanceof DigitalCreditCard) {
			cc = (DigitalCreditCard) creditCard;
			if (null != cc && null != cc.getTokenValue()) {
				cc.setCreditCardNumber(cc.getTokenValue());

			}
		}
		String name = getUniqueCreditCardNickname(creditCard, profile, null);
		copyCreditCardToProfile(creditCard, profile, name);
		Map usersCreditCardMap = getUsersCreditCardMap(profile);
		// check if we have to make the card as default credit card
		if (usersCreditCardMap != null && usersCreditCardMap.containsKey(name) && cc.isDefaultCreditCard()) {
			profile.setPropertyValue(
					((DigitalCommercePropertyManager) getPropertyManager()).getDefaultCreditCardPropertyName(),
					usersCreditCardMap.get(name));
			if (this.isLoggingDebug()) {
				logDebug("Set Card  :" + name + "as default Card");
			}
		}
	}

	/**
	 * Copy credit card to Profile
	 * 
	 * @param pCreditCard
	 * @param pProfile
	 * @param pNickName
	 */
	public void copyCreditCardToProfile(CreditCard pCreditCard, RepositoryItem pProfile, String pNickName) {
		MutableRepository repository = (MutableRepository) pProfile.getRepository();
		if (isTokenNotEmpty(pCreditCard)) {
			if (isLoggingDebug())
				logDebug("adding credit card to users profile");
			try {
				MutableRepositoryItem creditCardRepositoryItem = createCreditCardItem(pProfile);
				copyCreditCard(pCreditCard, creditCardRepositoryItem);
				repository.updateItem(creditCardRepositoryItem);
				addCreditCardToUsersMap(pProfile, creditCardRepositoryItem, pNickName);
				if (this.isLoggingDebug()) {
					logDebug("Successfully copied Card  :" + pNickName + "to profile: ");
				}
			} catch (RepositoryException re) {
				if (isLoggingError())
					logError(re);
			}
		}
	}

	/**
	 * Get the credit card type to be stored in database, uses the logic
	 * mentioned below in the method
	 * 
	 * @param dswCreditCard
	 * @return String cardType
	 */
	public String getCreditCardType(DigitalCreditCard dswCreditCard) {

		if (isLoggingDebug()) {
			logDebug("getCreditCardType  : Start");
			logDebug("getCreditCardType  : dswCreditCard.getCreditCardType() : " + dswCreditCard.getCreditCardType());
			logDebug("getCreditCardType  : dswCreditCard.getVantivFirstSix() : " + dswCreditCard.getVantivFirstSix());
		}
		if (dswCreditCard.getCreditCardType().trim().equalsIgnoreCase(getDswConstants().getCreditCardTypeVI().trim())) {
			if (isCardDSWVisaCard(dswCreditCard)) {
				if (isLoggingDebug()) {
					logDebug("getCreditCardType  : cardType returned is :: "
							+ getDswConstants().getCreditCardTypeDSWVisa());
				}
				return getDswConstants().getCreditCardTypeDSWVisa();
			} else {
				if (isLoggingDebug()) {
					logDebug("getCreditCardType  : cardType returned is :: "
							+ getDswConstants().getCreditCardTypeVisa());
				}

				return getDswConstants().getCreditCardTypeVisa();
			}
		}

		if (isCardChinaUnionPayCard(dswCreditCard)) {
			// if vantiv first six matches reg expression ^(62|88)
			// then card type is CUP
			if (isLoggingDebug()) {
				logDebug("getCreditCardType  : cardType returned is :: " + getDswConstants().getCreditCardTypeJcb());
			}
			return getDswConstants().getCreditCardTypeCup();
		}else if (isCardDiscoverCard(dswCreditCard)) {
			// if vantiv first six matches reg expression
			// /(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/
			// then card type is DISCOVER
			if (isLoggingDebug()) {
				logDebug("getCreditCardType  : cardType returned is :: "
						+ getDswConstants().getCreditCardTypeDiscover());
			}
			return getDswConstants().getCreditCardTypeDiscover();
		} else if (isCardDinersCard(dswCreditCard)) {
			// if vantiv first six matches reg expression /36"/ OR /^30[0-5]/
			// then card type is DINERS
			if (isLoggingDebug()) {
				logDebug("getCreditCardType  : cardType returned is :: " + getDswConstants().getCreditCardTypeDiners());
			}
			return getDswConstants().getCreditCardTypeDiners();
		} else if (isCardJCBCard(dswCreditCard)) {
			// if vantiv first six matches reg expression ^35(2[89]|[3-8][0-9])
			// then card type is JCB
			if (isLoggingDebug()) {
				logDebug("getCreditCardType  : cardType returned is :: " + getDswConstants().getCreditCardTypeJcb());
			}
			return getDswConstants().getCreditCardTypeJcb();
		} else if (isCardMasterCard(dswCreditCard)) {
			if (isLoggingDebug()) {
				logDebug("getCreditCardType  : cardType returned is :: " + getDswConstants().getCreditCardTypeMaster());
			}
			return getDswConstants().getCreditCardTypeMaster();
		} else if (isCardAmexCard(dswCreditCard)) {

			if (isLoggingDebug()) {
				logDebug("getCreditCardType  : cardType returned is :: " + getDswConstants().getCreditCardTypeAmex());
			}
			return getDswConstants().getCreditCardTypeAmex();
		} else {
			// else return vantiv returned credit card type
			if (isLoggingInfo()) {
				logInfo("Unmapped CreditCardType : returning Vantiv returned CreditCardType :: " + dswCreditCard.getCreditCardType());
			}
			return dswCreditCard.getCreditCardType();
		}
	}

	/**
	 * Returns true if card type supplied is Diners card else false
	 * 
	 * @param dswCreditCard
	 * @return boolean true|false
	 */
	public boolean isCardDinersCard(DigitalCreditCard dswCreditCard) {

		Pattern dinnersPatternOne = Pattern.compile(getDswConstants().getDinnersCardPatternOne());
		Pattern dinnersPatternTwo = Pattern.compile(getDswConstants().getDinnersCardPatternTwo());
		Matcher dinnersMatcherOne = dinnersPatternOne.matcher(dswCreditCard.getVantivFirstSix());
		Matcher dinnersMatcherTwo = dinnersPatternTwo.matcher(dswCreditCard.getVantivFirstSix());
		if (dinnersMatcherOne.find() || dinnersMatcherTwo.find()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns true if card type supplied is Visa card else false
	 * 
	 * @param dswCreditCard
	 * @return boolean true|false
	 */
	public boolean isCardDSWVisaCard(DigitalCreditCard dswCreditCard) {
		if (dswCreditCard.getVantivFirstSix().equals(getDswConstants().getDswVisaFirstSix())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns true if card type supplied is JCB card else false
	 * 
	 * @param dswCreditCard
	 * @return boolean true|false
	 */
	public boolean isCardJCBCard(DigitalCreditCard dswCreditCard) {

		Pattern jcbPatternOne = Pattern.compile(getDswConstants().getJcbCardPattern());
		Matcher jcbMatcherOne = jcbPatternOne.matcher(dswCreditCard.getVantivFirstSix());
		if (jcbMatcherOne.find()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns true if card type supplied is Discover card else false
	 * 
	 * @param dswCreditCard
	 * @return boolean true|false
	 */
	public boolean isCardDiscoverCard(DigitalCreditCard dswCreditCard) {
		Pattern dicoverPatternOne = Pattern.compile(getDswConstants().getDiscoverCardPattern());
		Matcher discoverMatcher = dicoverPatternOne.matcher(dswCreditCard.getVantivFirstSix());
		if (discoverMatcher.find()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns true if card type supplied is Discover card else false
	 * 
	 * @param dswCreditCard
	 * @return boolean true|false
	 */
	public boolean isCardChinaUnionPayCard(DigitalCreditCard dswCreditCard) {
		Pattern chinaUnionPayPatternOne = Pattern.compile(getDswConstants().getChinaUnionPayCardPattern());
		Matcher chinaUnionPayMatcher = chinaUnionPayPatternOne.matcher(dswCreditCard.getVantivFirstSix());
		if (chinaUnionPayMatcher.find()) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Returns true if card type supplied is Master card else false
	 * 
	 * @param dswCreditCard
	 * @return boolean true|false
	 */
	public boolean isCardMasterCard(DigitalCreditCard dswCreditCard) {

		if (dswCreditCard.getCreditCardType().equalsIgnoreCase(getDswConstants().getCreditCardTypeMc())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns true if card type supplied is Amex card else false
	 * 
	 * @param dswCreditCard
	 * @return boolean true|false
	 */
	public boolean isCardAmexCard(DigitalCreditCard dswCreditCard) {
		if (dswCreditCard.getCreditCardType().equalsIgnoreCase(getDswConstants().getCreditCardTypeAX())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if credit card has token value
	 * 
	 * @param pCreditCard
	 * @return isTokenNotEmpty
	 */
	public boolean isTokenNotEmpty(CreditCard pCreditCard) {
		boolean isTokenNotEmpty = false;
		if (null != pCreditCard && pCreditCard instanceof DigitalCreditCard) {
			DigitalCreditCard cc = (DigitalCreditCard) pCreditCard;
			if (null != cc && null != cc.getTokenValue()) {
				cc.setCreditCardNumber(null);
				isTokenNotEmpty = true;
			}
		}
		return isTokenNotEmpty;
	}

	/**
	 * Save PayPal Payment information to Profile
	 * 
	 * @param profile
	 * @param paypal
	 */
	public void savePaypalToProfile(Profile profile, PaypalPayment paypal) {

		MutableRepository profileRepository = (MutableRepository) profile.getRepository();
		try {
			MutableRepositoryItem paypalRepositoryItem = profileRepository
					.createItem(PaypalPaymentGroupPropertyManager.PAYPAL_PAYMENT.getValue());
			paypalRepositoryItem.setPropertyValue(PaypalPaymentGroupPropertyManager.PAYPAL_PAYER_ID.getValue(),
					paypal.getPaypalPayerId());
			paypalRepositoryItem.setPropertyValue(PaypalPaymentGroupPropertyManager.PAYPAL_EMAIL.getValue(),
					paypal.getPaypalEmail());
			paypalRepositoryItem.setPropertyValue(PaypalPaymentGroupPropertyManager.CARDINAL_ORDER_ID.getValue(),
					paypal.getCardinalOrderId());
			profileRepository.addItem(paypalRepositoryItem);

			profile.setPropertyValue("paypalPayment", paypalRepositoryItem);
		} catch (RepositoryException re) {
			if (isLoggingError())
				logError(re);
		}
	}

	/**
	 * This loads the loyalty member into profile. it makes the calls to back
	 * end systems using account and loyalty services.
	 * 
	 * Because remote services are cached, it is okay to call them over and over
	 * again now !
	 * 
	 * @param profile
	 *            profile in session
	 * @return true or false
	 */
	public boolean loadLoyaltyMemberIntoProfile(Profile profile, boolean isValidLoyaltyMember) {
		boolean retVal = false;
		String METHOD_NAME = "loadLoyaltyMemberIntoProfile";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			MutableRepository rep = getProfileRepository();
			MutableRepositoryItem profileItem = rep.getItemForUpdate(profile.getRepositoryId(),
					profile.getItemDescriptor().getItemDescriptorName());
			if (profileItem != null) {
				RewardServiceResponse selectProfileByIdResponse = loadBasicLoyaltyInfoIntoProfile(profileItem, isValidLoyaltyMember);
				if (null != selectProfileByIdResponse && isLoggingDebug()) {
					logDebug("The response from select profile by id is null");
			}
				if (profileItem.getPropertyValue(getCommercePropertyManager().getLoyaltyNumberPropertyName()) != null
						&& null != selectProfileByIdResponse) {
					loadContactAndAlertPreferencesInfoIntoProfile(profileItem, selectProfileByIdResponse);
				}
				rep.updateItem(profileItem);
				retVal = true;
			} else {
				logError("cannot find profile item to update and return");
			}
		} catch (RepositoryException e) {
			logError(String.format("Unable to load loyalty info into profile for %s", profile.getRepositoryId()), e);
		}
		finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
		}
		return retVal;
	}

	/**
	 * 
	 * @param profile
	 */
	public void loadLoyaltyCertificatesAndOffersIntoProfile(Profile profile) {
		getRewardCertificateManager().loadProfileCertsAndOffersUsingLoyaltyService(profile);
	}

	/**
	 * 
	 * @param profileItem
	 */
	public RewardServiceResponse loadBasicLoyaltyInfoIntoProfile(MutableRepositoryItem profileItem, boolean isValidLoyaltyMember) {
		RewardServiceResponse btsRewardServiceResponse = null;
		try {
			RewardServiceRequest btsRewardServiceRequest = new RewardServiceRequest();
			btsRewardServiceRequest.getPerson().setProfileID(profileItem.getRepositoryId());
			if(isValidLoyaltyMember){
				btsRewardServiceResponse = this.getRewardService().selectCustomerByProfileId(btsRewardServiceRequest);
			}

			if (btsRewardServiceResponse != null && btsRewardServiceResponse.isSuccess()) {
				Person person = btsRewardServiceResponse.getPerson();
				String memberId = person.getMemberID();
				String birthDay = person.getDayOfBirth();
				String birthMonth = person.getMonthOfBirth();
				String birthYear = DigitalProfileConstants.CUSTOMER_DEFAULT_BIRTH_YEAR;
				Gender gender = person.getGender();

				String firstName = person.getFirstName();
				String lastName = person.getLastName();
				
				String certDenomination = btsRewardServiceResponse.getCertDenomination();
				if (memberId != null) {
					btsRewardServiceRequest.getPerson().setMemberID(memberId);
					// memberDataResponse = this.getRewardService().viewMemberData(btsRewardServiceRequest);
					if (btsRewardServiceResponse != null) {
						List<Points> pointsList = btsRewardServiceResponse.getPoints();
						Points points;
						if (!pointsList.isEmpty()) {
							points = pointsList.get(0);
							String loyaltyTier = points.getTier();
							Long pointsBalance = Long.valueOf(points.getLoyaltyPoints());
							Long pointsNextReward = Long.valueOf(points.getPointsUntilNextReward());
							Long pointsNextTier = Long.valueOf(points.getPointsToNextStatus());
							Date loyaltySignUpDate = points.getLoyaltySignupDate();
							String loyaltySignUpStoreNumber = points.getLoyaltySignupStoreNumber();
							Date loyaltyStartDate = points.getLoyaltyTierStartDate();
							Date loyaltyExpirationDate = points.getLoyaltyTierExpirationDate();
							String loyaltyNumber = btsRewardServiceResponse.getPerson().getMemberID();
							if(DigitalStringUtil.isNotEmpty(loyaltyNumber) && !"0".equalsIgnoreCase(loyaltyNumber)) {
								profileItem.setPropertyValue("loyaltyNumber", loyaltyNumber);
							}
							if(DigitalStringUtil.isNotEmpty(loyaltyTier)) {
								profileItem.setPropertyValue("loyaltyTier", loyaltyTier);
							}
							profileItem.setPropertyValue("pointsBalance", pointsBalance);
							profileItem.setPropertyValue("loyaltySignupDate", loyaltySignUpDate);
							profileItem.setPropertyValue("loyaltySignupStoreNumber", loyaltySignUpStoreNumber);
							profileItem.setPropertyValue("loyaltyTierStartDate", loyaltyStartDate);
							profileItem.setPropertyValue("loyaltyTierExpirationDate", loyaltyExpirationDate);
							profileItem.setPropertyValue("pointsNextTier", pointsNextTier);
							profileItem.setPropertyValue("pointsNextReward", pointsNextReward);
							// New fields added as part of making rewards data
							// real time in Oct-2016
							profileItem.setPropertyValue("dollarsNextReward", points.getDollarsNextReward());
							profileItem.setPropertyValue("dollarsFutureReward", points.getDollarsFutureReward());
							profileItem.setPropertyValue("dollarsNextTier", points.getDollarsNextTier());
							profileItem.setPropertyValue("dollarsPendingCert", points.getDollarsPendingCertIssue());

						}
					}
					if(DigitalStringUtil.isNotEmpty(memberId) && !"0".equalsIgnoreCase(memberId)) {
						profileItem
								.setPropertyValue(getCommercePropertyManager().getLoyaltyNumberPropertyName(),
										memberId);
					}
					if (DigitalStringUtil.isNotBlank(firstName) && DigitalStringUtil.isNotEmpty(firstName)) {
						profileItem.setPropertyValue(getCommercePropertyManager().getFirstNamePropertyName(),
								firstName.trim());
					}
					if (DigitalStringUtil.isNotBlank(lastName) && DigitalStringUtil.isNotEmpty(lastName)) {
						profileItem.setPropertyValue(getCommercePropertyManager().getLastNamePropertyName(),
								lastName.trim());
					}
					if (gender != null) {
						profileItem.setPropertyValue(getCommercePropertyManager().getGenderPropertyName(),
								gender.name().toLowerCase());
					}
					if (birthDay != null && birthMonth != null && birthYear != null) {
						profileItem.setPropertyValue("dateOfBirth",
								DigitalDateUtil.calculteDOB(birthDay, birthMonth, birthYear));
					} else {
						profileItem.setPropertyValue("dateOfBirth", null);
					}

					int currentPointsBalance = btsRewardServiceResponse.getCurrentBalancePoint();
					String pointsBankingEnabled = btsRewardServiceResponse.getPointsBanking();
					profileItem.setPropertyValue("currentPointsBalance", new Long(currentPointsBalance));
					if (pointsBankingEnabled != null) {
						switch (pointsBankingEnabled) {
						case "N":
							pointsBankingEnabled = "false";
							break;
						case "Y":
							pointsBankingEnabled = "true";
							break;

						default:
							break;
						}
						profileItem.setPropertyValue("pointBankerFlag", pointsBankingEnabled);

					}

					Flags flags = btsRewardServiceResponse.getFlags();
					if (flags != null) {
						String invalidLoyaltyMemberFlag = flags.getInvalidLoyaltyMemberFlag();
						if (DigitalStringUtil.isNotBlank(invalidLoyaltyMemberFlag)
								&& DigitalStringUtil.isNotEmpty(invalidLoyaltyMemberFlag)) {
							profileItem.setPropertyValue("validLoyaltyMember",
									"Y".equals(invalidLoyaltyMemberFlag.trim()));
						}
						String ccFlag = btsRewardServiceResponse.getFlags().getIsCardHolder();
						if (DigitalStringUtil.isNotBlank(ccFlag) && DigitalStringUtil.isNotEmpty(ccFlag)) {
							profileItem.setPropertyValue("ccFlag", "Y".equals(ccFlag.trim()));
						}
					}

					profileItem.setPropertyValue("mobilePhoneNumber", btsRewardServiceResponse.getContact().getMobilePhone());

					if (DigitalStringUtil.isNotEmpty(btsRewardServiceResponse.getContact().getPhone1())
							&& profileItem.getPropertyValue("homeAddress") != null) {
						((MutableRepositoryItem) profileItem.getPropertyValue("homeAddress"))
								.setPropertyValue("phoneNumber", btsRewardServiceResponse.getContact().getPhone1());
					}
					
					if(!DigitalStringUtil.isBlank(certDenomination)){
						profileItem.setPropertyValue("certDenomination", Long.parseLong(certDenomination));
					}
					profileItem.setPropertyValue("isPaperlessCert",btsRewardServiceResponse.getIsPaperlessCert());
				}
			}

		} catch (DigitalIntegrationException de) {
				logError("Error in loading basic Loyalty Data into Profile :: "+de);
		}
		return btsRewardServiceResponse;
	}
	

	public Date getDateOfBirth(Profile profile){

    return (Date)profile.getPropertyValue("dateOfBirth");
	}

	public String getGender(Profile profile){
		Object gender =  profile.getPropertyValue("gender");
		if(gender != null){
			return (String) gender;
		}
		return null;
	}

	public String getProfileInitials(Profile profile){
		String profileInitials = null;
		if(profile != null) {
			String firstName = (String) profile.getPropertyValue("firstName");
			String lastName = (String) profile.getPropertyValue("lastName");

			if (DigitalStringUtil.isNotBlank(firstName) && DigitalStringUtil.isNotBlank(lastName)) {
				profileInitials = firstName.substring(0, 1) + lastName.substring(0, 1);
				profileInitials = profileInitials.toUpperCase();
			}
		}

		return profileInitials;
	}

	public boolean isProfileComplete(Profile profile, boolean syncProfileWithRewards){

		if (syncProfileWithRewards) {
			if(isEnableTransactionLock()) {
				UserTransactionDemarcation td = null;
				String METHOD_NAME = "isProfileComplete";
				try {
					TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
					td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
					this.loadBasicLoyaltyInfoIntoProfile(profile, true);
				} finally {
					TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
					TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				}
			}
			else{
				this.loadBasicLoyaltyInfoIntoProfile(profile, true);
			}
		}


		try {
			Date dob = getDateOfBirth(profile);
			if (dob == null) {
				return false;
			}

			long birthDay = DigitalDateUtil.getDayOfMonth(dob);
			if (birthDay <= 0) {
				return false;
			}

			long birthMonth = DigitalDateUtil.getMonth(dob);
			if (birthMonth < 0) {
				return false;
			}

			String firstName = (String) profile.getPropertyValue("firstName");
			if(DigitalStringUtil.isBlank(firstName)){
				return false;
			}

			String lastName = (String) profile.getPropertyValue("lastName");
			if(DigitalStringUtil.isBlank(lastName)){
				return false;
			}

			String gender = (String) profile.getPropertyValue("gender");
			if(DigitalStringUtil.isBlank(gender)){
				return false;
			}

			String email = (String) profile.getPropertyValue("email");
			if(DigitalStringUtil.isBlank(email)){
				return false;
			}

			String mobilePhoneNumber = (String) profile.getPropertyValue("mobilePhoneNumber");
			if(DigitalStringUtil.isBlank(mobilePhoneNumber)){
				return false;
			}

			if(!isProfileMailingAddressExists(profile)){
				return false;
			}

			if(!isProfilePrimaryShippingAddressExists(profile)){
				return false;
			}

			if(!isSavedCreditCardExistInProfile(profile)){
				return false;
			}

			return true;
		}catch(Exception ex){
			logError("Error getting profile details", ex);
			return false;
		}
	}

	/**
	 * Loads the shipping, billing, home addresses as well as first name, last
	 * name.
	 * 
	 * @param profile
	 * @param btsRewardServiceResponse
	 */
	private void loadContactAndAlertPreferencesInfoIntoProfile(MutableRepositoryItem profile,
			RewardServiceResponse btsRewardServiceResponse) {
		try {

			MutableRepository rep = getProfileRepository();

			MutableRepositoryItem primaryContact = (MutableRepositoryItem) profile
					.getPropertyValue(getCommercePropertyManager().getDefaultHomeAddrPropertyName());

			if (primaryContact == null) {
				primaryContact = rep.createItem(getCommercePropertyManager().getContactInfoItemDescriptorName());
				rep.addItem(primaryContact);
			}
			if (primaryContact != null) {
				com.digital.commerce.integration.reward.domain.Address address = btsRewardServiceResponse
						.getAddress();
				Person person = btsRewardServiceResponse.getPerson();
				// Update email in the DSW system from Rewards.
				Contact contact = btsRewardServiceResponse.getContact();
				if (contact != null) {
					String email = contact.getEmail();
					if (!DigitalStringUtil.isBlank(email)) {
						profile.setPropertyValue(this.getPropertyManager().getEmailAddressPropertyName(),
								email.toLowerCase().trim());
					}
					if (!DigitalStringUtil.isBlank(contact.getMobilePhone())) {
						profile.setPropertyValue(((DigitalCommercePropertyManager) this.getPropertyManager())
								.getMobilePhoneNumberPorpertyName(), contact.getMobilePhone().trim());
					}
				}

				String firstName = person.getFirstName();
				if (!DigitalStringUtil.isBlank(firstName)) {
					String tmpFName = trimFieldToRepositoryLength(primaryContact,
							getCommercePropertyManager().getAddressFirstNamePropertyName(), firstName.trim());
					primaryContact.setPropertyValue(getCommercePropertyManager().getAddressFirstNamePropertyName(),
							tmpFName);
				}

				String lastName = person.getLastName();
				if (!DigitalStringUtil.isBlank(lastName)) {
					String tmpLName = trimFieldToRepositoryLength(primaryContact,
							getCommercePropertyManager().getAddressLastNamePropertyName(), lastName.trim());
					primaryContact.setPropertyValue(getCommercePropertyManager().getAddressLastNamePropertyName(),
							tmpLName);
				}

				String address1 = address.getAddress1();
				if (!DigitalStringUtil.isBlank(address1)) {
					String tmpAddress1 = trimFieldToRepositoryLength(primaryContact,
							getCommercePropertyManager().getAddressLineOnePropertyName(), address1.trim());
					primaryContact.setPropertyValue(getCommercePropertyManager().getAddressLineOnePropertyName(),
							tmpAddress1);
				}

				String address2 = address.getAddress2();
				if (!DigitalStringUtil.isBlank(address2)) {
					String tmpAddress2 = trimFieldToRepositoryLength(primaryContact,
							getCommercePropertyManager().getAddressLineTwoPropertyName(), address2.trim());
					primaryContact.setPropertyValue(getCommercePropertyManager().getAddressLineTwoPropertyName(),
							tmpAddress2);
				}

				String address3 = address.getAddress3();
				if (!DigitalStringUtil.isBlank(address3)) {
					String tmpAddress3 = trimFieldToRepositoryLength(primaryContact,
							getCommercePropertyManager().getAddressLineThreePropertyName(), address3.trim());
					primaryContact.setPropertyValue(getCommercePropertyManager().getAddressLineThreePropertyName(),
							tmpAddress3);
				}

				String city = address.getCity();
				if (!DigitalStringUtil.isBlank(city)) {
					String tmpCity = trimFieldToRepositoryLength(primaryContact,
							getCommercePropertyManager().getAddressCityPropertyName(), city.trim());
					primaryContact.setPropertyValue(getCommercePropertyManager().getAddressCityPropertyName(), tmpCity);
				}
				String state = address.getState();
				String country = address.getCountry();
				AddressType addressType = AddressType.valueFor(address.getAddressType(), country, state);
				// Address Type has been commented as, there is no Address Type
				// coming from Rewards Integration Response.
				if (!DigitalStringUtil.isBlank(state)) {
					if (AddressType.MILITARY.equals(addressType)) {
						primaryContact.setPropertyValue("region", state.trim());
						primaryContact.setPropertyValue(getCommercePropertyManager().getAddressStatePropertyName(), "");
					} else {
						primaryContact.setPropertyValue(getCommercePropertyManager().getAddressStatePropertyName(),
								state.trim());
						primaryContact.setPropertyValue("region", "");
					}

				}

				if (country != null) {
					primaryContact.setPropertyValue(getCommercePropertyManager().getAddressCountryPropertyName(),
							country.trim());
				}
				String postalCode = address.getPostCode();
				CountryCode countryCode = CountryCode.valueFor(country);

				if (postalCode != null) {
					String tmpPostCode = postalCode.trim();
					// TODO RS Need to check why this logic is needed
					if (CountryCode.USA.equals(countryCode) && tmpPostCode.length() > 5 && tmpPostCode.length() < 10) {
						tmpPostCode = tmpPostCode.substring(0, 5) + "-" + tmpPostCode.substring(5);
					} else if (CountryCode.USA.equals(countryCode) && postalCode.length() == 6) {
						tmpPostCode = tmpPostCode.substring(0, 3) + " " + tmpPostCode.substring(3);
					}

					primaryContact.setPropertyValue("postalCode", tmpPostCode);
				}

				// FORMAT AND OVERRIDE PHONE NUMBER ACCORDING TO ADDRESS
				// TYPE STORED FOR HOME ADDRESS IF UNITED STATES OR CANADA
				if (CountryCode.USA.equals(countryCode) || CountryCode.CANADA.equals(countryCode)) {
					String phone = btsRewardServiceResponse.getContact().getPhone1();
					if (!DigitalStringUtil.isBlank(phone)) {
						primaryContact.setPropertyValue(
								getCommercePropertyManager().getAddressPhoneNumberPropertyName(),
								DigitalStringUtil.stripNonDigit(phone.trim()));
					}
				}

				rep.updateItem(primaryContact);
			}

			// add this home address to the users default home address
			// property and update repository
			updateProperty(getCommercePropertyManager().getDefaultHomeAddrPropertyName(), primaryContact, profile);
			// Update preferredStore from Rewards system
			String storeId = btsRewardServiceResponse.getPreferredStore();
			List<RepositoryItem> favStores = (List<RepositoryItem>) profile.getPropertyValue(FAVOTIRE_STORES);
			List<RepositoryItem> favStoresList = new ArrayList<>();
			if (favStores != null && favStores.size() > 0) {
				RepositoryItem storeItem = favStores.get(0);
				if (storeItem != null) {
					if (storeItem.getPropertyValue("storeNumber") != null && storeId != null) {
						storeId = storeId.trim();
						if (!((String) storeItem.getPropertyValue("storeNumber")).equalsIgnoreCase(storeId)) {
							String storeDetailsQueryStat = getStoreDetailsQuery() + storeId + "'";
							RepositoryItem[] item = getGeoLocationUtil().findBySQL(storeDetailsQueryStat, "location");
							if (item != null && item.length > 0) {
								favStoresList.add(item[0]);
								if (favStoresList != null && favStoresList.size() > 0) {
									profile.setPropertyValue(FAVOTIRE_STORES, favStoresList);
								}
							}
						}
					}
				}
			} else if (storeId != null) {
				String storeDetailsQueryStat = getStoreDetailsQuery() + storeId + "'";
				RepositoryItem[] item = getGeoLocationUtil().findBySQL(storeDetailsQueryStat, "location");
				if (item != null && item.length > 0) {
					favStoresList.add(item[0]);
					if (favStoresList != null && favStoresList.size() > 0) {
						profile.setPropertyValue(FAVOTIRE_STORES, favStoresList);
					}
				}
			}
			// Update Alert Preferences from
			loadAlertPreferencesIntoProfile(profile, btsRewardServiceResponse);

		} catch (Exception e) {
			if (isLoggingError()) {
				logError("Error while 'updating' primary contact in profile repository  ", e);
			}
		}
	}

	/**
	 * 
	 * @param profile
	 * @param btsRewardServiceResponse
	 */
	private void loadAlertPreferencesIntoProfile(final MutableRepositoryItem profile,
			RewardServiceResponse btsRewardServiceResponse) {

		try {
			if (isLoggingDebug()) {
				logDebug("in loadAlertPreferencesIntoProfile");
			}
			final MutableRepository profileRepository = (MutableRepository) profile.getRepository();
			final ChangeAwareSet alertPreferencesChangeAwareSet = ((ChangeAwareSet) profile
					.getPropertyValue("alertPreferences"));
			String emailAlertFrequency = null;
			Flags flags = null;
			String noEmail = null;
			String noPhone = null;
			if (btsRewardServiceResponse != null) {
				emailAlertFrequency = btsRewardServiceResponse.getFrequencyFashionEmail();
				
				//NC419910: KTLO1-174 : User alert preferences are not being copied correctly into ATG
				//TODO: get these mappings from the property file
				if(emailAlertFrequency!=null && getRewardsToPlatformAlertsMapping()!=null && !getRewardsToPlatformAlertsMapping().isEmpty()){
					emailAlertFrequency=getRewardsToPlatformAlertsMapping().get(emailAlertFrequency);
				}
				
				flags = btsRewardServiceResponse.getFlags();
				if (flags != null) {
					noEmail = flags.getNoEMail();
					noPhone = flags.getNoPhone();
					if (noPhone != null && noPhone.equalsIgnoreCase("Y")) {
						noPhone = "true";
					}else{
						noPhone = "false";
					}
					
					if (noEmail != null && noEmail.equalsIgnoreCase("Y")) {
						noEmail = "true";
					}else{
						noEmail = "false";
					}
						
					}
			}
			if (alertPreferencesChangeAwareSet == null || alertPreferencesChangeAwareSet.isEmpty()) {
				Set alertPreferenceSet = new HashSet();
				MutableRepositoryItem alertItem = profileRepository.createItem("alertPreference");
				alertItem.setPropertyValue("alertType", "DSW_REWARDS");
				alertItem.setPropertyValue("user", profile);

				if (emailAlertFrequency != null) {
					alertItem.setPropertyValue("emailAlertFrequency", emailAlertFrequency);
				}

				if (!DigitalStringUtil.isEmpty(noPhone)) {
					alertItem.setPropertyValue("alertMethodSMS", !Boolean.parseBoolean(noPhone));
				}

				if (!DigitalStringUtil.isEmpty(noEmail)) {
					alertItem.setPropertyValue("alertMethodEmail", !Boolean.parseBoolean(noEmail));
				}

				alertPreferenceSet.add(alertItem);
				profile.setPropertyValue("alertPreferences", alertPreferenceSet);

			} else {
				final Iterator iterator = alertPreferencesChangeAwareSet.iterator();

				final GSAItem repositoryItem = (GSAItem) iterator.next();
				if (emailAlertFrequency != null) {
					repositoryItem.setPropertyValue("emailAlertFrequency", emailAlertFrequency);
				}

				if (!DigitalStringUtil.isEmpty(noPhone)) {
					repositoryItem.setPropertyValue("alertMethodSMS", !Boolean.parseBoolean(noPhone));
				}

				if (!DigitalStringUtil.isEmpty(noEmail)) {
					repositoryItem.setPropertyValue("alertMethodEmail", !Boolean.parseBoolean(noEmail));
				}

			}

		} catch (Exception e) {
			logError("Error while loading preferences into profile.." + e.getMessage());
		}
	}

	/**
	 * This gets called after user is created/logged in but just before the
	 * register/log in event is fired.
	 * 
	 * @see atg.userprofiling.ProfileServices#postLoginUser()
	 * @see atg.userprofiling.ProfileServices#postCreateUser()
	 * @see atg.userprofiling.ProfileFormHandler#postCreateUser()
	 * @see atg.userprofiling.ProfileFormHandler#postLoginUser()
	 */
	@Override
	public boolean assumeSecurityIdentity(Profile profile, IdentityManager identityManager) {

		boolean retVal = super.assumeSecurityIdentity(profile, identityManager);
		String source = ServletUtil.getCurrentRequest().getParameter("source");
		if (isLoggingDebug()) {
			logDebug("Source is " + source);
		}
		if (DigitalStringUtil.isBlank(source) || !(source.equalsIgnoreCase("stores"))) {
			try {
				/*
				 * Temp work around for rewards calls caching for 2017-holidays. This is used by
				 * BtsRewardsService calls
				 */
				if (this.getDswConstants().isRewardsCacheEnabled()) {
					DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
					if (null != request) {
						request.setParameter("cacheRewardsCalls", "Y");
					}
				}

				loadLoyaltyMemberIntoProfile(profile, true);

				if ("orderconfirmation".equalsIgnoreCase(source)) {
					String lastOrderId = ServletUtil.getCurrentRequest().getParameter("orderID");
					Object loyaltyNumber = profile
							.getPropertyValue(getCommercePropertyManager().getLoyaltyNumberPropertyName());
					if (null == loyaltyNumber) {
						logInfo(
								"Loyalty Number is not available for the profile " + profile.getRepositoryId());
						return retVal;
					}
					if (DigitalStringUtil.isBlank(lastOrderId)) {
						logInfo(
								"Input request does not have the previous order id so S211 message is not sent for the profile "
										+ profile.getRepositoryId());
						return retVal;
					}

					rewardCertificateManager.linkGuestOrderWithLoyaltyDetails(
							(DigitalOrderImpl) getOrderManager().loadOrder(lastOrderId));
				}
			} catch (Exception e) {
				logError("Error in loading Loyalty Member to Profile." + profile.getItemDisplayName(), e);
			}
		}
		return retVal;
	}

	/**
	 * 
	 * @param profileItem
	 * 
	 * @param lockAfterFailedLogin
	 * 
	 * @return
	 */
	public Map<Object,Object> markFailedLoginAttempt(MutableRepositoryItem profileItem, boolean lockAfterFailedLogin) {
		
		Map<Object, Object> lockedStatus = new HashMap<>();
		lockedStatus.put("isLocked", Boolean.FALSE);
		lockedStatus.put("attempts", 0);
		try {
			if (profileItem != null) {

				MutableRepository repository = (MutableRepository) profileItem.getRepository();
				Integer attempts = (Integer) profileItem.getPropertyValue("nbrForgotPasswordTry");
				attempts = (attempts != null) ? attempts + 1 : new Integer(1);
				profileItem.setPropertyValue("nbrForgotPasswordTry", attempts);
				lockedStatus.put("isLocked", Boolean.FALSE);
				lockedStatus.put("attempts", attempts);
				if (lockAfterFailedLogin && attempts != null && attempts == MultiSiteUtil.getMaxForgotPasswordRetry()) {
					profileItem.setPropertyValue("accountLockedTime",new Timestamp(System.currentTimeMillis()));
					if (isLoggingDebug()) {
						logDebug("################# ACCOUNT LOCKED ######### ::: WITH Profile_ID :: "+profileItem.getRepositoryId());
					}
					
					lockedStatus.put("isLocked", Boolean.TRUE);
					lockedStatus.put("attempts", attempts);
				}
				repository.updateItem(profileItem);
			}
		} catch (RepositoryException re) {
			if (isLoggingError())
				logError("failure trying to increment failed login attempts ", re);
		}
		return lockedStatus;

	}
	
	/** clear out the coupon based promo, and grantedcoupon, so the user has a
	 * fresh start  
	 * 
	 * @param profile */
	public void cleanPromotions( RepositoryItem profile ) {
		final DigitalPromotionTools promTools = (DigitalPromotionTools)getPromotionTools();

		final Collection<RepositoryItem> activePromotions = (Collection<RepositoryItem>)profile.getPropertyValue( promTools.getActivePromotionsProperty() );
		final Collection<RepositoryItem> appliedCoupons = (Collection<RepositoryItem>)profile.getPropertyValue( promTools.getCurrentCouponsProperty() );

		boolean hasPromotions = ( activePromotions != null && !activePromotions.isEmpty() ) || ( appliedCoupons != null && !appliedCoupons.isEmpty() );

		if( activePromotions != null ) {
			activePromotions.clear();
		}
		if( appliedCoupons != null ) {
			appliedCoupons.clear();
		}

		if( hasPromotions ) {
			try {
				getPromotionTools().initializePricingModels();
			} catch( Exception exc ) {
				if( isLoggingError() ) logger.error( "Unable to reinitialize pricing models", exc );
			}
		}
	}


	/**
	 * clear out the CSR coupons & promotions
	 * 
	 * @param profile
	 */
	public void cleanCSRPromotions(RepositoryItem profile) {
		final DigitalPromotionTools promTools = (DigitalPromotionTools) getPromotionTools();

		final Collection<RepositoryItem> activePromotions = (Collection<RepositoryItem>) profile
				.getPropertyValue(promTools.getCurrentActivePromotionsProperty());
		final Collection<RepositoryItem> appliedCoupons = (Collection<RepositoryItem>) profile
				.getPropertyValue(promTools.getCurrentCouponsProperty());

		final Collection<RepositoryItem> removePromotions = new HashSet<>();
		final Collection<RepositoryItem> removeCoupons = new HashSet<>();

		boolean hasPromotions = (activePromotions != null && !activePromotions.isEmpty())
				|| (appliedCoupons != null && !appliedCoupons.isEmpty());

		for (RepositoryItem activePromotion : activePromotions) {
			RepositoryItem promo = (RepositoryItem) activePromotion.getPropertyValue("promotion");
			if (promTools.isCSRPromotionRestricted(promo)) {
				if (this.isLoggingDebug()) {
					logDebug("User profile has active CSR promotion-->" + promo.getRepositoryId());
				}
				removePromotions.add(activePromotion);
			}
		}

		for (RepositoryItem appliedCoupon : appliedCoupons) {
			if (promTools.isCSRCouponRestricted(appliedCoupon.getRepositoryId())) {
				if (this.isLoggingDebug()) {
					logDebug("User profile has active CSR coupon-->" + appliedCoupon.getRepositoryId());
				}
				removeCoupons.add(appliedCoupon);
			}
		}

		if (activePromotions != null && removePromotions != null & !removePromotions.isEmpty()) {
			if (this.isLoggingDebug()) {
				logDebug("Removing CSR promotions from user's active promotion");
			}
			activePromotions.removeAll(removePromotions);
		}
		if (appliedCoupons != null && removeCoupons != null & !removeCoupons.isEmpty()) {
			if (this.isLoggingDebug()) {
				logDebug("Removing CSR coupons from user's active promotion");
			}
			appliedCoupons.removeAll(removeCoupons);
		}

		if (hasPromotions) {
			try {
				getPromotionTools().initializePricingModels();
			} catch (Exception exc) {
				if (isLoggingError())
					logError("Unable to reinitialize pricing models", exc);
			}
		}
	}

	/**
	 * 
	 * @param profile
	 * @return
	 */
	public boolean isLoggedIn(RepositoryItem profile) {
		boolean retVal = false;
		if (profile != null && !profile.isTransient()) {
			retVal = getSecurityStatusAsInt(profile) >= getPropertyManager().getSecurityStatusLogin();
		}
		return retVal;
	}

	/**
	 * 
	 * @param profile
	 * @return
	 */
	public boolean isKnown(RepositoryItem profile) {
		boolean ret = false;
		if (profile != null) {
			if (profile.isTransient()
					|| getSecurityStatusAsInt(profile) == getPropertyManager().getSecurityStatusAnonymous()
					|| isPersistentAnonymousUser(profile)) {
				ret = false;
			} else {
				ret = true;
			}
		}
		return ret;
	}

	/**
	 * 
	 * @param profile
	 * @return
	 */
	public boolean isDSWAnanymousUser(RepositoryItem profile) {
		return !(this.isKnown(profile));
	}

	/**
	 * 
	 * @param profile
	 * @return
	 */
	public boolean isPersistentAnonymousUser(RepositoryItem profile) {
		boolean retVal = false;
		if (profile != null && !(profile.isTransient())) {
			String login = (String) profile.getPropertyValue("login");
			String repId = profile.getRepositoryId();
			retVal = DigitalStringUtil.equals(repId, login);
		}
		return retVal;
	}

	/**
	 * 
	 * @param profile
	 * @return
	 */
	private int getSecurityStatusAsInt(RepositoryItem profile) {
		int retVal = getPropertyManager().getSecurityStatusAnonymous();
		String profileId = null; boolean isTransient = false;
		if (profile != null) {
			final Object securityStatusAsObject = profile.getPropertyValue("securityStatus");
			profileId = profile.getRepositoryId();
			isTransient = profile.isTransient();
			if (securityStatusAsObject != null) {
				retVal = ((Integer) securityStatusAsObject).intValue();
			} else {
				logError(String.format("Null security status for profile %1$s, continuing as anonymous",
						profile.getRepositoryId()));
			}
		}
		if(isLoggingDebug()) {
			logDebug("User Id: " +  profileId + " security status: " + retVal + "  transient flag " + isTransient);
		}
		return retVal;
	}

	/**
	 * 
	 * @param paypalPayment
	 * @return
	 * @throws CommerceException
	 */
	public PaypalPayment createPaypalPayment(PaypalPayment paypalPayment) throws CommerceException {
		PaypalPayment paypalGroup = (PaypalPayment) getPaymentGroupManager()
				.createPaymentGroup(PaypalPaymentGroupPropertyManager.PAYPAL_PAYMENT.getValue());
		paypalGroup.setAmount(paypalPayment.getAmount());
		paypalGroup.setCardinalOrderId(paypalPayment.getCardinalOrderId());
		paypalGroup.setPaypalPayerId(paypalPayment.getPaypalPayerId());
		paypalGroup.setPaypalEmail(paypalPayment.getPaypalEmail());

		if (paypalPayment.getBillingAddress() != null) {
			paypalGroup.setBillingAddress(createBillingAddress(paypalPayment.getBillingAddress()));
		}
		return paypalGroup;

	}

	/**
	 * 
	 * @param pBillingAddress
	 * @return
	 */
	public ContactInfo createBillingAddress(ContactInfo pBillingAddress) {
		ContactInfo billingAddress = new ContactInfo();
		billingAddress.setFirstName(pBillingAddress.getFirstName());
		billingAddress.setLastName(pBillingAddress.getLastName());
		billingAddress.setAddress1(pBillingAddress.getAddress1());
		billingAddress.setAddress2(pBillingAddress.getAddress2());
		billingAddress.setCity(pBillingAddress.getCity());
		billingAddress.setState(pBillingAddress.getState());
		billingAddress.setCountry(pBillingAddress.getCountry().trim());
		billingAddress.setPostalCode(pBillingAddress.getPostalCode());
		billingAddress.setPhoneNumber(pBillingAddress.getPhoneNumber());
		billingAddress.setEmail(pBillingAddress.getEmail());
		return billingAddress;

	}

	/**
	 * private DigitalShippingGroupManager getShippingGroupManager() { return
	 * (DigitalShippingGroupManager)getOrderManager().getShippingGroupManager(); }
	 */
	private DigitalPaymentGroupManager getPaymentGroupManager() {
		return (DigitalPaymentGroupManager) getOrderManager().getPaymentGroupManager();
	}

	/**
	 * Copies all of the address fields from one to the other
	 * 
	 * @param address
	 * @param otherAddress
	 */
	public static <RA> RA copyAddress(Object address, RA otherAddress) throws CommerceException {
		if (address instanceof Address && otherAddress instanceof Address) {
			OrderTools.copyAddress((Address) address, (Address) otherAddress);
		} else if (address instanceof Address && otherAddress instanceof RepositoryItem) {
			OrderTools.copyAddress((Address) address, (RepositoryItem) otherAddress);
		} else if (address instanceof RepositoryItem && otherAddress instanceof Address) {
			OrderTools.copyAddress((RepositoryItem) address, (Address) otherAddress);
		}
		return otherAddress;
	}

	/**
	 * Anonymous (or transient) profiles as well as their orders will only be
	 * persisted if they contain items. Persisting Anonymous profiles can also
	 * be achieved by setting persistentAnonymousProfiles=true in
	 * ProfileRequestservlet. However, it will persist the profile even if the
	 * user just visits the site (while getting the session confirmation). Hence
	 * the override.
	 * 
	 * @param shoppingCart
	 */
	public void persistShoppingCart(OrderHolder shoppingCart) {
		final boolean orderEmpty = shoppingCart.isCurrentEmpty();
		if (orderEmpty) {
			return;
		}
		RepositoryItem profile = shoppingCart.getProfile();
		
		if(!this.getDswConstants().isPaTool()){
			synchronized (profile) {
	
				if (profile instanceof Profile && profile.isTransient()) {
					try {
						final MutableRepository repository = getProfileRepository();
						final MutableRepositoryItem mutableProfile;
						if (((Profile) profile).getDataSource() instanceof MutableRepositoryItem) {
							mutableProfile = (MutableRepositoryItem) ((Profile) profile).getDataSource();
						} else {
							mutableProfile = repository.getItemForUpdate(profile.getRepositoryId(),
									repository.getDefaultViewName());
						}
						getProfileRequestTools().initAnonymousProfile(mutableProfile, this);
						((Profile) profile).setDataSource(repository.addItem(mutableProfile));
					} catch (RepositoryException e) {
						if (isLoggingError()) {
							logError(String.format("Unable to make the anonymous profile %s persistent",
									profile.getRepositoryId()), e);
						}
					}
				}
			}
		}

		/**
		 * NOTE: added the below code to persist the shopping cart for anonymous
		 * user and set persistOrdersForAnonymousUsers=false in
		 * ShoppingCart.properties as it is persisting orders without any items
		 * in the bag
		 */
		try {
			persistShoppingCarts(profile, shoppingCart);
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError(String.format("Unable to make the shopping cart persistent for profile %s",
						profile.getRepositoryId()), e);
			}
		}
	}

	/**
	 * Returns the default operational first name to use for the order. It looks
	 * at the credit card billing billing address first name, then the profile
	 * billing address first name, then the profile first name.
	 * 
	 * @param profile
	 * @param order
	 * @return
	 */
	public String getDefaultOperationalFirstName(RepositoryItem profile, DigitalOrderImpl order) {
		String retVal = null;
		try {
			if (DigitalStringUtil.isBlank(retVal) && order != null) {
				final int creditCardIndex = Iterables.indexOf(order.getPaymentGroupRelationships(),
						new FindPaymentGroupRelationshipWithCreditCard());
				if (creditCardIndex != -1) {
					final PaymentGroupRelationship paymentGroupRelationship = (PaymentGroupRelationship) Iterables
							.get(order.getPaymentGroupRelationships(), creditCardIndex);
					final DigitalCreditCard creditCard = (DigitalCreditCard) paymentGroupRelationship.getPaymentGroup();
					retVal = ((DigitalRepositoryContactInfo) creditCard.getBillingAddress()).getFirstName();
				}

				if (DigitalStringUtil.isBlank(retVal) || DigitalStringUtil.isEmpty(retVal)) {
					final int payPalIndex = Iterables.indexOf(order.getPaymentGroupRelationships(),
							new FindPaymentGroupRelationshipWithPayPal());
					if (payPalIndex != -1) {
						List paymentGroupList = order.getPaymentGroups();
						if (paymentGroupList != null) {
							Iterator i = paymentGroupList.iterator();
							while (i.hasNext()) {
								PaymentGroup pg = (PaymentGroup) i.next();
								if (pg instanceof PaypalPayment) {
									PaypalPayment paypalPayment = (PaypalPayment) pg;
									retVal = paypalPayment.getBillingAddress().getFirstName();
								}
							}
						}
					}
				}

				if (DigitalStringUtil.isBlank(retVal) || DigitalStringUtil.isEmpty(retVal)) {
					HardgoodShippingGroup hgSg = null;
					Collection shippingGroups = order.getShippingGroups();
					Iterator shippingGrouperator = shippingGroups.iterator();
					ShippingGroup sg = null;
					while (shippingGrouperator.hasNext()) {
						sg = (ShippingGroup) shippingGrouperator.next();
						if (sg instanceof HardgoodShippingGroup) {
							hgSg = (HardgoodShippingGroup) sg;
							if (hgSg != null) {
								DigitalRepositoryContactInfo shAddress = (DigitalRepositoryContactInfo) hgSg
										.getShippingAddress();
								retVal = shAddress.getFirstName();
								if (retVal != null)
									break;
							}
						}
					}
				}
				
				if (DigitalStringUtil.isBlank(retVal) || DigitalStringUtil.isEmpty(retVal)) {
					retVal = order.getAltPickupFirstName();
				}
			}

			if (DigitalStringUtil.isBlank(retVal) || DigitalStringUtil.isEmpty(retVal)) {
				ContactInfo billingAddress = this.getProfileBillingAddress(profile);
				if (null != billingAddress) {
					retVal = billingAddress.getFirstName();
				}
			}

			if (DigitalStringUtil.isBlank(retVal) || DigitalStringUtil.isEmpty(retVal)) {
				retVal = (String) profile.getPropertyValue(getPropertyManager().getFirstNamePropertyName());
			}
		} catch (CommerceException ex) {
			logError("Error getting defaultOperationalFirstName :: ", ex);
		}
		return retVal;
	}

	/**
	 * Returns the operational first name to use for the order else lookup from
	 * profile
	 * 
	 * @param profile
	 * @param order
	 * @return
	 * @see #getDefaultOperationalFirstName(Profile, Order)
	 */
	public String getOperationalFirstName(RepositoryItem profile, DigitalOrderImpl order) {
		String retVal = getDefaultOperationalFirstName(profile, order);
		if (DigitalStringUtil.isBlank(retVal)) {
			retVal = (String) profile
					.getPropertyValue(getCommercePropertyManager().getOperationalFirstNamePropertyName());
		}
		return retVal;
	}

	/**
	 * Gets the default operational email address to use for the order. It looks
	 * at the credit card billing address email, then the profile billing
	 * address, then the profile email address.
	 * 
	 * @param profile
	 * @param order
	 * @return
	 */
	public String getDefaultOperationalEmailAddress(RepositoryItem profile, Order order) {
		String retVal = null;
		if (order != null) {
				try {
					final int creditCardIndex = Iterables.indexOf(order.getPaymentGroupRelationships(),
							new FindPaymentGroupRelationshipWithCreditCard());
					if (creditCardIndex != -1) {
						final PaymentGroupRelationship paymentGroupRelationship = (PaymentGroupRelationship) Iterables
								.get(order.getPaymentGroupRelationships(), creditCardIndex);
						final DigitalCreditCard creditCard = (DigitalCreditCard) paymentGroupRelationship.getPaymentGroup();
						retVal = ((DigitalRepositoryContactInfo) creditCard.getBillingAddress()).getEmail();
					}

					if (DigitalStringUtil.isBlank(retVal) || DigitalStringUtil.isEmpty(retVal)) {
						final int payPalIndex = Iterables.indexOf(order.getPaymentGroupRelationships(),
								new FindPaymentGroupRelationshipWithPayPal());
						if (payPalIndex != -1) {
							List paymentGroupList = order.getPaymentGroups();
							if (paymentGroupList != null) {
								Iterator i = paymentGroupList.iterator();
								while (i.hasNext()) {
									PaymentGroup pg = (PaymentGroup) i.next();
									if (pg instanceof PaypalPayment) {
										PaypalPayment paypalPayment = (PaypalPayment) pg;
										retVal = paypalPayment.getBillingAddress().getEmail();
									}
								}
							}
						}
					}

					if (DigitalStringUtil.isBlank(retVal) || DigitalStringUtil.isEmpty(retVal)) {
						HardgoodShippingGroup hgSg = null;
						Collection shippingGroups = order.getShippingGroups();
						Iterator shippingGrouperator = shippingGroups.iterator();
						ShippingGroup sg = null;
						while (shippingGrouperator.hasNext()) {
							sg = (ShippingGroup) shippingGrouperator.next();
							if (sg instanceof HardgoodShippingGroup) {
								hgSg = (HardgoodShippingGroup) sg;
								if (hgSg != null) {
									DigitalRepositoryContactInfo shAddress = (DigitalRepositoryContactInfo) hgSg
											.getShippingAddress();
									retVal = shAddress.getEmail();
									if (retVal != null)
										break;
								}
							}
						}
					}
				} catch (Exception e) {
					if (isLoggingWarning()) {
						logWarning(String.format("Unable to load the profile %s for order %s", profile.getRepositoryId(),order.getId()), e);
					}
				}

		}

		if (DigitalStringUtil.isBlank(retVal) || DigitalStringUtil.isEmpty(retVal)) {
			RepositoryItem billingAddress = (RepositoryItem) profile
					.getPropertyValue(getCommercePropertyManager().getBillingAddressPropertyName());
			if (billingAddress != null) {
				retVal = (String) billingAddress.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());
			}
		}

		if (DigitalStringUtil.isBlank(retVal) || DigitalStringUtil.isEmpty(retVal)) {
			retVal = (String) profile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());
		}

		return retVal;
	}

	/**
	 * Returns the operational email address to use for the order. If not found
	 * return from profile
	 * 
	 * @param profile
	 * @param order
	 * @return
	 * @see #getDefaultOperationalEmailAddress(Profile, Order)
	 */
	public String getOperationalEmailAddress(RepositoryItem profile, Order order) {
		String retVal = getDefaultOperationalEmailAddress(profile, order);
		if (DigitalStringUtil.isBlank(retVal)) {
			retVal = (String) profile
					.getPropertyValue(getCommercePropertyManager().getOperationalEmailAddressPropertyName());
		}
		return retVal;
	}

	/**
	 * 
	 * @return
	 */
	private DigitalCommercePropertyManager getCommercePropertyManager() {
		return (DigitalCommercePropertyManager) getPropertyManager();
	}


	/**
	 * With ATG, the profile can have - default home address, default shipping
	 * address, default billing address and default credit card (with billing
	 * address) With redesign, we are not storing default billing address at
	 * profile level. We are storing default shipping and default credit card
	 * with billing address, when user adds default credit card. The below
	 * method will return billing address from default credit card.
	 * 
	 * @param profile
	 * @return
	 * @throws CommerceException
	 */
	public ContactInfo getProfileBillingAddress(RepositoryItem profile) throws CommerceException {
		ContactInfo billingAddr = null;
		// Set Billing Address from profile
		RepositoryItem defaultCreditCard = this.getDefaultCreditCard(profile);
		if (null != defaultCreditCard) {
			DigitalCommercePropertyManager pm = (DigitalCommercePropertyManager) this.getPropertyManager();
			RepositoryItem defaultBillingAddress = (RepositoryItem) defaultCreditCard
					.getPropertyValue(pm.getCreditCardBillingAddressPropertyName());

			if (defaultBillingAddress != null) {
				billingAddr = new ContactInfo();
				try {
					DigitalOrderTools.copyAddress(defaultBillingAddress, billingAddr);
				} catch (CommerceException cEx) {
					if (isLoggingError()) {
						logError(cEx);
					}
					throw cEx;
				}
				if (getAddressUtil().isEmptyAddress(billingAddr)) {
					billingAddr = null;
				}
			}
		}
		return billingAddr;
	}

	/**
	 * Returns Billing address for a given credit card key
	 * 
	 * @param creditCardKey
	 * @return
	 * @throws CommerceException
	 */
	public ContactInfo getCreditCardBillingAddress(String creditCardKey) throws CommerceException {
		ContactInfo billingAddr = null;
		// Get credit card repo item using the credit card key
		RepositoryItem ccRepItem = getCreditCardById(creditCardKey);

		if (null != ccRepItem) {
			DigitalCommercePropertyManager pm = (DigitalCommercePropertyManager) this.getPropertyManager();
			RepositoryItem defaultBillingAddress = (RepositoryItem) ccRepItem
					.getPropertyValue(pm.getCreditCardBillingAddressPropertyName());

			if (defaultBillingAddress != null) {
				billingAddr = new ContactInfo();
				try {
					DigitalOrderTools.copyAddress(defaultBillingAddress, billingAddr);
				} catch (CommerceException cEx) {
					if (isLoggingError()) {
						logError(cEx);
					}
					throw cEx;
				}
				if (getAddressUtil().isEmptyAddress(billingAddr)) {
					billingAddr = null;
				}
			}
		}
		return billingAddr;
	}

	/**
	 * Override OOTB method to handle cart merge
	 * 
	 */
	@Override
	protected void postLoginUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
			RepositoryItem pProfile, OrderHolder pShoppingCart, PricingModelHolder pPricingModelHolder)
			throws ServletException {
		try {
			Order currentCart = pShoppingCart.getCurrent();
			List<DigitalCommerceItem> preItems = currentCart.getCommerceItems();
			HashMap<String, Long> copyPreItems = new HashMap<>(preItems.size());
			for (DigitalCommerceItem pItem : preItems) {
				// add shipping type to the key
				copyPreItems
						.put(pItem.getCatalogRefId() + (pItem.getShipType() == null ? "" : pItem.getShipType())
								+ (pItem.getStoreId() == null ? "" : pItem.getStoreId()), pItem.getQuantity());
			}

			if (pRequest.getParameter("checkout") != null && pRequest.getParameter("checkout")
					.equalsIgnoreCase("true")) {
				((DigitalOrderImpl) currentCart).setDropPrevCartFlag(true);
				if (currentCart instanceof OrderImpl) {
					// Reload the order as removeInvalidRewardCertificate in
					// loadOrder pipleline might have updated the order, else we
					// will get order version mismatch exception
					((OrderImpl) currentCart).invalidateOrder();
				}
				super.postLoginUser(pRequest, pResponse, pProfile, pShoppingCart, pPricingModelHolder);
			} else {

				if (currentCart instanceof OrderImpl) {
					// Reload the order as removeInvalidRewardCertificate in
					// loadOrder pipleline might have updated the order, else we
					// will get order version mismatch exception
					((OrderImpl) currentCart).invalidateOrder();
				}
				super.postLoginUser(pRequest, pResponse, pProfile, pShoppingCart, pPricingModelHolder);
				Order loadedCart = pShoppingCart.getCurrent();
				// compare and merge cart
				if (currentCart.getId().equals(loadedCart.getId())) {
					// nothing to be merged, leave this placeholder for
					// futureimplmentation

				} else {
					List<DigitalCommerceItem> items = loadedCart.getCommerceItems();
					List<CommerceItem> existItems = new ArrayList<>();
					for (DigitalCommerceItem item : items) {
						if (copyPreItems
								.containsKey(
										item.getCatalogRefId() + (item.getShipType() == null ? "" : item.getShipType())
												+ (item.getStoreId() == null ? "" : item.getStoreId()))) {
							item.setMergedQuantity(item.getQuantity() - copyPreItems
									.get(item.getCatalogRefId() + (item.getShipType() == null ? ""
											: item.getShipType())
											+ (item.getStoreId() == null ? "" : item.getStoreId())));
						} else {
							item.setMergedQuantity(item.getQuantity());
						}
					}
				}
			}

			this.replaceEmptyShippingGroupWithDefault(pShoppingCart.getCurrent(), pProfile);
		}catch (Exception e){
			logError("Error post login but user has logged-in successfully");
		}
	}

	/**
	 * Use default shipping group after login if order shipping group is empty
	 * 
	 * @param order
	 * @param pProfile
	 */
	public void replaceEmptyShippingGroupWithDefault(Order order, RepositoryItem pProfile) {
		final String PERFORM_MONITOR_NAME = "DigitalProfileTools_replaceEmptyShippingGroupWithDefault";
		final String METHOD_NAME = "replaceEmptyShippingGroupWithDefault";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
		final UserTransactionDemarcation td = TransactionUtils
				.startNewTransaction(CLASSNAME, METHOD_NAME);
		TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
		try {
			List<ShippingGroup> existingShippingGroups = order.getShippingGroups();
			RepositoryItem defaultShippingAddress = this.getDefaultShippingAddress(pProfile);

			if (order == null || defaultShippingAddress == null) {
				return;
			}

			for (ShippingGroup sgp : existingShippingGroups) {
				if (sgp instanceof HardgoodShippingGroup) {
					if (DigitalAddressUtil
							.isOnlyAddressEmpty(((HardgoodShippingGroup) sgp).getShippingAddress())) {
						try {
							AddressTools.copyAddress(defaultShippingAddress,
									((HardgoodShippingGroup) sgp).getShippingAddress());
						} catch (IntrospectionException e) {
							logError(e);
						}
					}
				}
			}

		} catch (Exception e) {
			logError(e);
		} finally {
			try {
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			} catch (Throwable th) {
				logError(th);
			}
		}
	}

	/**
	 * 
	 * @param pProfile
	 * @param pShoppingCart
	 * @param pUserPricingModels
	 * @param pLocale
	 */
	public void repriceShoppingCartForLogin(RepositoryItem pProfile, OrderHolder pShoppingCart,
			PricingModelHolder pUserPricingModels, Locale pLocale) {
		try {
			logDebug("Acquiring transaction lock within loadUserShoppingCartForLogin");
			acquireTransactionLock();
		} catch (DeadlockException de) {
			logError(de);
		}
		try {
			repriceShoppingCarts(pProfile, pShoppingCart, pUserPricingModels, pLocale, getRepriceOrderPricingOp());
			persistShoppingCarts(pProfile, pShoppingCart);
		} catch (CommerceException ce) {
			logError(ce);
		} finally {
			logDebug("Releasing transaction lock within loadUserShoppingCartForLogin");
			releaseTransactionLock();
		}
	}

	/**
	 * 
	 * @param pProfile
	 * @throws DigitalIntegrationException
	 */
	public void removeCustomerRewardAddress(RepositoryItem pProfile) throws DigitalIntegrationException {

		RewardServiceRequest btsRewardServiceRequest = new RewardServiceRequest();
		btsRewardServiceRequest.getAddress().setAddress1("");
		btsRewardServiceRequest.getAddress().setCity("");
		btsRewardServiceRequest.getAddress().setCountry("");
		btsRewardServiceRequest.getAddress().setPostCode("");
		btsRewardServiceRequest.getAddress().setState("");
		btsRewardServiceRequest.getPerson().setProfileID(pProfile.getRepositoryId());
		this.rewardService.updateCustomer(btsRewardServiceRequest);
	}

	/**
	 * 
	 * @param pProfile
	 * @param pCreditCardName
	 * @return
	 * @throws RepositoryException
	 */
	public String removeCreditCard(RepositoryItem pProfile, String pCreditCardName) throws RepositoryException {
		RepositoryItem card = getCreditCardByNickname(pCreditCardName, pProfile);
		if (card != null) {
			this.removeProfileCreditCard(pProfile, pCreditCardName);
			return getMessageLocator().getMessageString(REMOVE_CARD_SUCCESS);
		} else
			return getMessageLocator().getMessageString(REMOVE_CARD_NOT_EXIST);
	}

	/**
	 * 
	 * @param pProfile
	 * @param id
	 * @return
	 * @throws RepositoryException
	 * @throws DigitalIntegrationException
	 */
	public String removeProfileRepositoryAddressById(RepositoryItem pProfile, String id)
			throws RepositoryException, DigitalIntegrationException {

		boolean addressRemoved = false;

		if (pProfile == null || DigitalStringUtil.isBlank(id))
			return getMessageLocator().getMessageString(REMOVE_ADDRESS_NOT_EXIST);

		try {
			logDebug("Acquiring transaction lock within loadUserShoppingCartForLogin");
			acquireTransactionLock();
		} catch (DeadlockException de) {
			logError(de);
		}

		final UserTransactionDemarcation td = TransactionUtils.startNewTransaction(CLASSNAME,
				"removeProfileRepositoryAddressById");

		try {

			DigitalCommercePropertyManager cpmgr = (DigitalCommercePropertyManager) getPropertyManager();
			Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) pProfile
					.getPropertyValue(cpmgr.getSecondaryAddressPropertyName());
			RepositoryItem defaultShippingAddress = this.getDefaultShippingAddress(pProfile);
			RepositoryItem homeAddress = (RepositoryItem) pProfile.getPropertyValue(cpmgr.getHomeAddressPropertyName());

			/*	NC419910:KTLO1-102 Unable to delete a shipping address from profile. Dotcom and IOS App.
			 * If User requests to delete an address on his profile. ShippingAddress, Other Address delete will take precedence over delete Home address check. Delete if matches
			 * If one of the first two checks is successfull then don't even bother to check home address delete check
			 * If none of the above(two)was successful; check if user is trying to delete his home address(ideally not possible through frontend); throw an error back to user that he cannot delete his home address.
			 */
			if (defaultShippingAddress != null && defaultShippingAddress.getRepositoryId().equals(id)) {
				updateProperty(cpmgr.getShippingAddressPropertyName(), null, pProfile);
				addressRemoved = true;
			}
			
			
			if (secondaryAddresses != null) {
				for (Map.Entry<String, RepositoryItem> entry : secondaryAddresses.entrySet()) {
					if (entry.getValue().getRepositoryId().equals(id)) {
						secondaryAddresses.remove(entry.getKey());
						break;
					}
				}
				updateProperty(cpmgr.getSecondaryAddressPropertyName(), secondaryAddresses, pProfile);
				addressRemoved = true;
			}

			if (!addressRemoved && homeAddress != null && homeAddress.getRepositoryId().equals(id)) {
				// disable remove home address function according new requirment
				return getMessageLocator().getMessageString(REMOVE_ADDRESS_NOT_ALLOWED);
			}
			
		} catch (RepositoryException re) {
			TransactionUtils.rollBackTransaction(td, CLASSNAME, "removeProfileRepositoryAddressById");
			throw re;
		} finally {
			TransactionUtils.endTransaction(td, CLASSNAME, "removeProfileRepositoryAddressById");
			releaseTransactionLock();
		}

		if (!addressRemoved)
			return getMessageLocator().getMessageString(REMOVE_ADDRESS_NOT_EXIST);
		else
			return getMessageLocator().getMessageString(REMOVE_ADDRESS_SUCCESS);

	}

	/**
	 * @param pProfile
	 * @param favoriteStoreId
	 * @return String
	 * @throws RepositoryException
	 */
	public String updateFavoriteStore(RepositoryItem pProfile, String favoriteStoreId)
			throws RepositoryException {

		if (pProfile != null && !DigitalStringUtil.isBlank(favoriteStoreId)) {
			try {
				MutableRepositoryItem profleToUpdate = getProfileRepository()
						.getItemForUpdate(pProfile.getRepositoryId(), USER);
				List<RepositoryItem> favStores = new ArrayList<>();
				RepositoryItem locationItem = locationRepository.getItem(favoriteStoreId, LOCATION);
				favStores.add(locationItem);
				profleToUpdate.setPropertyValue(FAVOTIRE_STORES, favStores);
				getProfileRepository().updateItem(profleToUpdate);
				Object memberId = pProfile
						.getPropertyValue(getCommercePropertyManager().getLoyaltyNumberPropertyName());
				if (memberId != null && DigitalStringUtil.isNotBlank((String) memberId)) {
					RewardsUpdateCustomerRequest updateCustomerRequest = new RewardsUpdateCustomerRequest();
					updateCustomerRequest.setProfileId(pProfile.getRepositoryId());
					updateCustomerRequest
							.setPreferredStore((String) locationItem.getPropertyValue("storeNumber"));
					RewardsUpdateCustomerResponse response = this.getRewardCertificateManager()
							.updateCustomer(updateCustomerRequest, false);
					if (!response.isRequestSuccess()) {
						if (response.getGenericExceptions() != null && !response.getGenericExceptions()
								.isEmpty()) {
							logError(
									"Unable to Update FavoriteStore in the Rewards." + response.getGenericExceptions()
											.get(0).getLocalizedMessage());
						}
						return this.getMessageLocator()
								.getMessageString(DigitalProfileConstants.UPDATE_FAVSTORE_FAILURE);
					} else {
						return SUCCESS;
					}
				} else {
					return SUCCESS;
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError("updateFavoriteStore " + e);
				}
			}
		}
		return FAIL;
	}

	/**
	 *
	 * @param pProfile
	 * @param pLatitude
	 * @param pLongitude
	 * @param regionProfile
	 * @return
	 * @throws RepositoryException
	 */
	public DigitalStoreLocatorModel getUserFavoriteStore(RepositoryItem pProfile, Double pLatitude, Double pLongitude,
											   RegionProfile regionProfile) throws RepositoryException {
		if (pProfile != null) {
			Coordinate cordinate = null;
			Object postalCode = null;
			if(this.isLoggedIn(pProfile) || this.isCookied(pProfile)) {
				List<RepositoryItem> favStores = (List<RepositoryItem>) pProfile.getPropertyValue(FAVOTIRE_STORES);
				if (favStores != null && favStores.size() > 0) {
					RepositoryItem storeItem = favStores.get(0);
					if (storeItem != null) {
						updateStoreTransientProperties(storeItem);
						return  getGeoLocationUtil().convertStoreLocator(storeItem);
					}
				}
				if (pProfile.getPropertyValue("homeAddress") != null) {
					// get fav store co-ordinate from home address
					RepositoryItem homeAddressContactInfoItem = (RepositoryItem) pProfile.getPropertyValue("homeAddress");
					if (homeAddressContactInfoItem != null
							&& homeAddressContactInfoItem.getPropertyValue("addressType") != null) {
						if (((String) homeAddressContactInfoItem.getPropertyValue("addressType")).equalsIgnoreCase("USA")) {
							postalCode = homeAddressContactInfoItem.getPropertyValue("postalCode");
						}
					}
				}
			}

			if (postalCode != null) {
				String zipCode = postalCode.toString();
				if(DigitalStringUtil.length(DigitalStringUtil.trim(postalCode.toString())) > 5){
					zipCode = DigitalStringUtil.substringBefore(zipCode, "-");
				}
				cordinate = geoLocatorProvider.getGeoLocation(zipCode, null);
			}

			if (cordinate == null && pLatitude != null && pLongitude != null && (pLatitude != 0.0 && pLongitude != 0.0)) {
				// If Location services turned on get fav store from latitude &
				// longitude, resolved through UI.
				cordinate = new Coordinate(pLatitude, pLongitude);
			}

			if(cordinate == null){
				// get fav store based on Akami headers latitude and longitude
				// values OR city & state
				Double latitude = regionProfile.getLatitude();
				Double longitude = regionProfile.getLongitude();
				if (latitude != 0.0 && longitude != 0.0) {
					cordinate = new Coordinate(latitude, longitude);
				}
			}
			if (cordinate != null) {
				RepositoryItem[] items = null;
				Site site = MultiSiteUtil.getSite();
				List<String> siteIds = new ArrayList<>();
				if (null != site) {
					siteIds.add(site.getId());
				}
				items = (RepositoryItem[]) locationManager.getNearest(cordinate, 0.0, siteIds);
				if (items != null && items.length > 0) {
					Collections.sort(Arrays.asList(items), new Comparator<RepositoryItem>() {
						public int compare(RepositoryItem o1, RepositoryItem o2) {
							return ((Double) o1.getPropertyValue("distance"))
									.compareTo((Double) o2.getPropertyValue("distance"));
						}
					});
					updateStoreTransientProperties((RepositoryItem) items[0]);
					return getGeoLocationUtil().convertStoreLocator(((RepositoryItem) items[0]));
				}
			}
		}
		return null;
	}
	
   
	private void updateStoreTransientProperties(RepositoryItem item) {
		// TODO Auto-generated method stub
		if(null!=item){
		       if(isShipToStore(item)){
		        	((MutableRepositoryItem) item).setPropertyValue(SHIP_TO_STORE, "Y");
		        }else{
		        	((MutableRepositoryItem) item).setPropertyValue(SHIP_TO_STORE, "N");
		        }
		        
		        if(isPickupInStore(item)){
		        	((MutableRepositoryItem) item).setPropertyValue(PICKUP_IN_STORE, "Y");
		        }else{
		        	((MutableRepositoryItem) item).setPropertyValue(PICKUP_IN_STORE, "N");
		        }
		}
		
	}
	
	private boolean isShipToStore(RepositoryItem storeItem){
		String shipToStoreBOSTS =(String) storeItem.getPropertyValue(SHIP_TO_STORE_BOSTS);
		if (shipToStoreBOSTS != null && ("1".equalsIgnoreCase(shipToStoreBOSTS) || "Y".equalsIgnoreCase(shipToStoreBOSTS))) {
					if(isLoggingDebug()){
						logDebug("Store is Ship To Store Eligible");
					}
				return true;
		}
		return false;
	}
	
	private boolean isPickupInStore(RepositoryItem storeItem){
		String pickupInStoreBOPIS=(String) storeItem.getPropertyValue(PICKUP_IN_STORE_BOPIS);
		if (pickupInStoreBOPIS != null && ("1".equalsIgnoreCase(pickupInStoreBOPIS) || "Y".equalsIgnoreCase(pickupInStoreBOPIS))) {
				if(isLoggingDebug()){
					logDebug("Store is PickUp In Store Eligible");
				}
				return true;
			}
		return false;
	 }

	/**
	 * 
	 * @param prifileItem
	 * @return
	 */
	public Map<String, RepositoryItem> geDefaultCreditCardsDetails(RepositoryItem prifileItem) {
		Map<String, RepositoryItem> activeCreditCardsMap = new HashMap<>();
		if (prifileItem != null) {
			if (!startEndDateValidator
					.validateObject((RepositoryItem) prifileItem.getPropertyValue(DEFAULT_CREDIT_CARD))) {
				activeCreditCardsMap.put(DEFAULT_CREDIT_CARD,
						(RepositoryItem) prifileItem.getPropertyValue(DEFAULT_CREDIT_CARD));
				return activeCreditCardsMap;
			}
		}
		return null;

	}

	/**
	 * 
	 * @param pProfile
	 * @return
	 */
	public Map<String, RepositoryItem> getActiveCreditCardsDetails(RepositoryItem pProfile) {
		if (pProfile != null) {
			Map<String, RepositoryItem> activeCreditCardsMap = new HashMap<>();
			Map<String, RepositoryItem> creditCardsMap = (Map<String, RepositoryItem>) pProfile
					.getPropertyValue(CREDIT_CARDS);
			if (creditCardsMap != null && creditCardsMap.size() > 0) {
        for (Iterator<Map.Entry<String, RepositoryItem>> it = creditCardsMap.entrySet().iterator(); it.hasNext();) {
					Map.Entry<String, RepositoryItem> entry = it.next();
					// NC::KTLO1-139:: Commenting the above line.
					if (!startEndDateValidator.validateObject(entry.getValue())) {
						activeCreditCardsMap.put(entry.getKey(), entry.getValue());
					}
				}
				return activeCreditCardsMap;
			}

		}
		return null;

	}

	/**
	 * Copies the credit card to the profile and makes it the new default credit
	 * card on the profile. A unique nickname is automatically generated.
	 * 
	 * @param creditCard
	 * @param profile
	 */
	public void saveCreditCardAsDefault(Profile profile, CreditCard creditCard, String pNickName) {
		// check if we have to make the card as default credit card
		Map usersCreditCardMap = getUsersCreditCardMap(profile);
		profile.setPropertyValue(((DigitalCommercePropertyManager) getPropertyManager()).getDefaultCreditCardPropertyName(),
				usersCreditCardMap.get(pNickName));
		if (isLoggingDebug()) {
			logDebug("Set Card  :" + pNickName + "as default Card");
		}
	}

	/**
	 * 
	 * @param pProfile
	 * @param pAddress
	 * @param addressRepositryId
	 * @param isPrimaryShipAddr
	 * @param suggestedAddress
	 * @return
	 * @throws RepositoryException
	 * @throws IntrospectionException
	 */
	public Map<Object, Object> updateProfileRepositoryAddress(
			RepositoryItem pProfile, DigitalAddress pAddress,
			String addressRepositryId, String isPrimaryShipAddr,String suggestedAddress)
			throws RepositoryException, IntrospectionException {
		Map<Object, Object> responseMap = null;
		DigitalContactInfo dswContactInfo = null;
		String addressName = "OtherAddress";
		addressName = getUniqueShippingAddressNickname(pAddress, pProfile,addressName);
		
		//KTLO1-874 : SECURICON PEN TEST - Shipping Address can be changed by unathorized user
		RepositoryItem pRepositoryAddress = null;
		if(addressRepositryId!= null){
			//Validate that this addressRepositryId belongs to the same user; before we start updating the address
			responseMap=validateCustomersProfileAddressId(addressRepositryId, pProfile);
			if (responseMap != null && responseMap.size() > 0) {
				return responseMap;
			}
			
		}
		if (pAddress != null) {
			dswContactInfo = populateContactInfo(pAddress);
			if(DigitalStringUtil.isBlank(suggestedAddress)){
				responseMap = validateAddress(dswContactInfo);
			}
			if (responseMap != null && responseMap.size() > 0) {
				return responseMap;
			}
		}
		if (addressRepositryId!= null && super.getProfileAddressById(addressRepositryId) != null) {
			// Update existing address.
			pRepositoryAddress = super.getProfileAddressById(addressRepositryId);
			super.updateProfileRepositoryAddress(pRepositoryAddress, pAddress);
			if (null!=dswContactInfo && Boolean.valueOf(isPrimaryShipAddr)
					&& ADDRESS_TYPE_USA.equalsIgnoreCase(dswContactInfo.getAddressType())
					|| ADDRESS_TYPE_MILITARY.equalsIgnoreCase(dswContactInfo.getAddressType())) {
				makeItPrimary(pProfile,addressName,pRepositoryAddress,pAddress);
			}
		} 
		if (responseMap != null && responseMap.isEmpty()) {
			responseMap.put("formError", "false");
			responseMap.put(STATUS, SUCCESS);
		}
		return responseMap;
	}
	
	//KTLO1-874 : SECURICON PEN TEST - Shipping Address can be changed by unathorized user
	private Map<Object, Object> validateCustomersProfileAddressId(String addressRepositryId, RepositoryItem pProfile) {
		// TODO Auto-generated method stub
		boolean addressIdFound = false;
		Map<Object, Object> responseMap = new HashMap<>();
		Map<String, RepositoryItem> profileOtherAddresses = (Map<String, RepositoryItem>) pProfile.getPropertyValue("secondaryAddresses");
		if (null != profileOtherAddresses && profileOtherAddresses.size() > 0) {
			for (RepositoryItem item : profileOtherAddresses.values()) {
				String repositoryId = item.getRepositoryId();
				if (repositoryId.equalsIgnoreCase(addressRepositryId)) {
					addressIdFound = true;
					break;
				}
			}
		}
		if(!addressIdFound){
		Map<Object, Object> errorMessagMap = null;
		List<Object> li = new ArrayList<>();
		errorMessagMap = new HashMap<>();
		errorMessagMap.put("localizedMessage", this.getMessageLocator().getMessageString("updateAddress.user.unauthorized"));
		errorMessagMap.put("errorCode", "updateAddress.user.unauthorized");
		li.add(errorMessagMap);
		responseMap = new HashMap<>();
		responseMap.put("formError", "true");
		responseMap.put("formExceptions", li);
		}
		
		return responseMap;

	}

	/**
	 * Copies the current primary shipping address to other addresses
	 * 
	 * @param pProfile
	 * @param pAddressName
	 * @param primaryShippingAddress
	 * @throws RepositoryException 
	 * @throws IntrospectionException 
	 */
	private boolean copyPrimaryShippingAddressToOtherAddresses(RepositoryItem pProfile,String pAddressName, RepositoryItem primaryShippingAddress) 
			throws RepositoryException, IntrospectionException {
		MutableRepositoryItem item = getProfileRepository().getItemForUpdate(primaryShippingAddress.getRepositoryId(),"contactInfo");
		DigitalAddress dswAddress = new DigitalAddress();
		boolean flag=false;
		dswAddress = (DigitalAddress)AddressTools.copyAddress(primaryShippingAddress, dswAddress);
		if (dswAddress != null) {
			if(AddressTools.copyAddress(dswAddress, item) != null)
				flag=true;
			item.setPropertyValue("ownerId", pProfile.getPropertyValue("id"));
		}
		getProfileRepository().updateItem(item);
		Map mymap = (Map) pProfile.getPropertyValue("secondaryAddresses");
		mymap.put(pAddressName, item);
	  return flag;
	}

	/**
	 * 
	 * @param pProfile
	 * @param adressName
	 * @param pRepositoryAddress
	 * @param pAddress
	 * @throws RepositoryException 
	 * @throws IntrospectionException
	 */
	private void makeItPrimary(RepositoryItem pProfile, String adressName,RepositoryItem pRepositoryAddress, DigitalAddress pAddress) 
			throws RepositoryException, IntrospectionException {
		if(pProfile.getPropertyValue(SHIPPING_ADDRESS) != null){
			RepositoryItem prevShipAddr = (RepositoryItem)pProfile.getPropertyValue(SHIPPING_ADDRESS);
			if(copyPrimaryShippingAddressToOtherAddresses(pProfile,adressName,prevShipAddr)){
				MutableRepositoryItem item = getProfileRepository().getItemForUpdate(pRepositoryAddress.getRepositoryId(),"contactInfo");
				AddressTools.copyAddress(pAddress, item);
				item.setPropertyValue("ownerId", pProfile.getPropertyValue("id"));
				getProfileRepository().updateItem(item);
				MutableRepositoryItem profile = RepositoryUtils.getMutableRepositoryItem(pProfile);
				profile.setPropertyValue(SHIPPING_ADDRESS, item);
				removeProfileRepositoryAddress(pProfile, getProfileAddressName(pProfile,pRepositoryAddress));
			}
		}else{
			super.createProfileRepositoryPrimaryAddress(pProfile,SHIPPING_ADDRESS, pAddress);
			removeProfileRepositoryAddress(pProfile, getProfileAddressName(pProfile,pRepositoryAddress));
		}
	}

	/**
	 * 
	 * @param pAddress
	 * @return
	 */
	private DigitalContactInfo populateContactInfo(DigitalAddress pAddress) {

		DigitalContactInfo dswContactInfo = new DigitalContactInfo();
		String addressType = pAddress.getAddressType();
		String address1 = pAddress.getAddress1();
		String address2 = pAddress.getAddress2();
		if(DigitalStringUtil.isNotBlank(address2) && "null".equalsIgnoreCase(address2)){
				address2 = "";
				pAddress.setAddress2(address2);
		}
		String city = pAddress.getCity();
		String state = pAddress.getState();
		String postalCode = pAddress.getPostalCode();
		String firstName = pAddress.getFirstName();
		String lastName = pAddress.getLastName();
		String country = pAddress.getCountry();
		if (!DigitalStringUtil.isBlank(addressType) && ADDRESS_TYPE_MILITARY.equalsIgnoreCase(addressType)) {
			String region = pAddress.getRegion();
			String pobox = pAddress.getPobox();
			String rank = pAddress.getRank();
			dswContactInfo.setRegion(region);
			dswContactInfo.setPobox(pobox);
			dswContactInfo
					.setIsPoBox(this.isPOBOX(pAddress.getAddress1(), pAddress.getAddress2(), pAddress.getAddress3()));
			dswContactInfo.setRank(rank);
		}
		dswContactInfo.setAddress1(address1);
		dswContactInfo.setAddress2(address2);
		dswContactInfo.setCity(city);
		dswContactInfo.setCountry(country);
		dswContactInfo.setAddressType(addressType);
		dswContactInfo.setState(state);
		dswContactInfo.setPostalCode(postalCode);
		dswContactInfo.setFirstName(firstName);
		dswContactInfo.setLastName(lastName);

		return dswContactInfo;

	}

	/**
	 * Validate Address with USPS system
	 * 
	 * @param dswContactInfo
	 * @return
	 */
	private Map<Object, Object> validateAddress(DigitalContactInfo dswContactInfo) {
		//Mimic the error/success response with map variables to make it in synch with create account, 
		//so that UI can use the same contract of create account for validation messages
		Map<Object, Object> responseMap = new HashMap<>();
		Map<String, String> suggestedAddressMap = null;
		Map<Object, Object> errorMessagMap = null;
		List<Object> li = new ArrayList<>();		

		Collection dswValidationErrors = dswBasicAddressValidator.validateAddress(dswContactInfo);
		if ((dswValidationErrors != null) && (!dswValidationErrors.isEmpty())) {

					if (isLoggingDebug()) {
						logDebug("dsw basic address validation failed with errors : "+ dswValidationErrors.toString());
					}
					for (Object entry : dswValidationErrors) {
						errorMessagMap = new HashMap();
						errorMessagMap.put("localizedMessage",getMessageLocator().getMessageString((String) entry));
						errorMessagMap.put("errorCode","atg.droplet.DropletException");
						li.add(errorMessagMap);						}
					responseMap.put("formExceptions", li);
					responseMap.put("formError", "true");
			return responseMap;
		}

		if (DigitalProfileConstants.USA_ADDRESS_TYPE.equals(dswContactInfo
				.getAddressType())) {
			AddressValidationServiceResponse addressValidationServiceResponse = null;			
			try {
				// USPS Service Call
				addressValidationServiceResponse = getAddressValidator().validateAddress(dswContactInfo);
			} catch (DigitalIntegrationBusinessException be) {				
				dswContactInfo.setAddressVerification(false);
				logError("usps address validation failed with errors : " + be);
				
			}
			
			if (addressValidationServiceResponse != null && !addressValidationServiceResponse.isSuccess()) {
				dswContactInfo.setAddressVerification(false);
				logError("Unable to Validate USPS Address.");
			} 
			else if (addressValidationServiceResponse != null && addressValidationServiceResponse.isSuccess()
					&& USPS_STATUS_SUGGESTED.equalsIgnoreCase(addressValidationServiceResponse
									.getAddressValidationDecision().getDescription())) {
				errorMessagMap = new HashMap<>();
				errorMessagMap.put("localizedMessage", this.getMessageLocator().getMessageString("USPSSuggested"));
				errorMessagMap.put("errorCode", "atg.droplet.DropletException");
				li.add(errorMessagMap);
				responseMap.put("formError", "true");
				responseMap.put("formExceptions", li);
				responseMap.put("decision", "suggested");
				suggestedAddressMap = new HashMap<>();
				suggestedAddressMap.put("suggestedAddress1",addressValidationServiceResponse.getSuggestedAddressLine1());
				suggestedAddressMap.put("suggestedAddress2",addressValidationServiceResponse.getSuggestedAddressLine2());
				suggestedAddressMap.put("suggestedCity",addressValidationServiceResponse.getSuggestedCity());
				suggestedAddressMap.put("suggestedState",addressValidationServiceResponse.getSuggestedState());
				suggestedAddressMap.put("suggestedZip5",addressValidationServiceResponse.getSuggestedZip5());
				suggestedAddressMap.put("suggestedZip4",addressValidationServiceResponse.getSuggestedZip4());
				responseMap.put("suggestedAddressMap", suggestedAddressMap);

			} else if (addressValidationServiceResponse != null
					&& addressValidationServiceResponse.isSuccess() && !USPS_STATUS_ACCEPTED.equalsIgnoreCase(
							addressValidationServiceResponse.getAddressValidationDecision().getDescription())
					&& !USPS_STATUS_SUGGESTED.equalsIgnoreCase(
							addressValidationServiceResponse.getAddressValidationDecision().getDescription())) {
				errorMessagMap = new HashMap<>();
				errorMessagMap.put("localizedMessage", this.getMessageLocator().getMessageString("USPSNoMatch"));
				errorMessagMap.put("errorCode", "atg.droplet.DropletException");
				li.add(errorMessagMap);
				responseMap.put("formError", "true");
				responseMap.put("formExceptions", li);
				logError("Unable to Validate USPS Address.");
			}
		}
		return responseMap;

	}
	
	/**
	 * 
	 * @param profileItem
	 */
	private void loadLoyaltyWebOffersInfoProfile(MutableRepositoryItem profileItem, boolean isValidLoyaltyMember) {
		this.getWebOfferTools().mergeOffersToProfile(profileItem, isValidLoyaltyMember);
	}

	/**
	 * 
	 * @param profile
	 * @return
	 */
	public List<Offer> retrieveWebOffersFromProfile(Profile profile, boolean isValidLoyaltyMember) {
		List<Offer> offers = new ArrayList<>();

		try {
			MutableRepository rep = (MutableRepository) getProfileRepository();
			MutableRepositoryItem profileItem = rep.getItemForUpdate(profile.getRepositoryId(),
					profile.getItemDescriptor().getItemDescriptorName());
			if (profileItem != null) {
				loadLoyaltyWebOffersInfoProfile(profileItem, isValidLoyaltyMember);
				rep.updateItem(profileItem);

				offers = getWebOfferTools().retrieveWebOffersFromProfile(profileItem);
			} else {
				if (this.isLoggingError()) {
					logError("cannot find profile item to update and return");
				}
			}

		} catch (Exception e) {
			logError(String.format("Unable to load loyalty info into profile for %s", profile.getRepositoryId()), e);
		}

		return offers;
	}

	/**
	 * 
	 * @param orderId
	 * @return
	 */
	public DigitalContactInfo populateContactInfoForGuestCheckout(String orderId) {
		if (orderId != null) {
			Order order;
			try {
				order = getOrderManager().loadOrder(orderId);
				if (order != null) {
					List paymentGroups = order.getPaymentGroups();
					Iterator paymentGrouperator = paymentGroups.iterator();
					Address billingAddress = null;
					while (paymentGrouperator.hasNext()) {
						PaymentGroup pg = (PaymentGroup) paymentGrouperator.next();
						if (pg instanceof DigitalCreditCard) {
							DigitalCreditCard ccGroup = (DigitalCreditCard) pg;
							billingAddress = ccGroup.getBillingAddress();
						} else if (pg instanceof PaypalPayment) {
							PaypalPayment paypalGroup = (PaypalPayment) pg;
							billingAddress = paypalGroup.getBillingAddress();
						} else {
							return null;
						}
						break;
					}
					if (billingAddress != null) {
						DigitalContactInfo dswContactInfo = new DigitalContactInfo();
						dswContactInfo.setEmail(((DigitalRepositoryContactInfo) billingAddress).getEmail());
						dswContactInfo.setFirstName(billingAddress.getFirstName());
						dswContactInfo.setLastName(billingAddress.getLastName());
						return dswContactInfo;
					}
				}
			} catch (CommerceException e) {
				logError(e);
			}
		}
		return null;

	}

	/**
	 * 
	 * @param pProfile
	 * @param cardId
	 * @param cardName
	 * @return
	 * @throws RepositoryException
	 */
	public String makeItAsDefaultCrditCard(RepositoryItem pProfile, String cardId, String cardName)
			throws RepositoryException {
		RepositoryItem creditCardItem = null;

		if (!DigitalStringUtil.isBlank(cardId)) {
			creditCardItem = getCreditCardById(cardId);
		} else if (!DigitalStringUtil.isBlank(cardName) && null != pProfile) {
			creditCardItem = getCreditCardByNickname(cardName, pProfile);
		}
		if (null != creditCardItem && null != pProfile) {
			if (!DigitalStringUtil.isBlank(cardName)) {
				if (setDefaultCreditCard(pProfile, cardName))
					return "Success";
			} else if (setDefaultCreditCard(pProfile, getCreditCardNickname(pProfile, creditCardItem)))
				return "Success";

		}
		return getMessageLocator().getMessageString(MAKE_DEFAULT_CARD_ERROR);
	}

	public Map<String, Object> getCertDenominationDetails(Profile profile){

		Map<String, Object> certDenominationDetails = new HashMap<>();
		try {
			Long currentCertDenominationLong = (Long)profile.getPropertyValue(CERT_DENOMINATION);
			Boolean isPaperlessCert = (Boolean)profile.getPropertyValue(IS_PAPERLESS_CERT);

			if(currentCertDenominationLong == null){
				currentCertDenominationLong = getDswConstants().getDefaultCertDenomination();
			}

			certDenominationDetails.put("currentDenomination", currentCertDenominationLong);
			certDenominationDetails.put("paperlessIndicator", isPaperlessCert);

			Map<String,Integer> allowedDenominationsLong = new LinkedHashMap<>();
			Map<String,String> allowedDenominations = getDswConstants().getRewardDenominations();
			for(Map.Entry<String,String> entry: allowedDenominations.entrySet()){
				String key = entry.getKey();
				String value = entry.getValue();
				allowedDenominationsLong.put(key, Integer.parseInt(value));
			}

			certDenominationDetails.put("allowedDenominations", allowedDenominationsLong);


		}catch(Exception ex){
			logError("Error parsing DOB ", ex);
			certDenominationDetails.put("currentDenomination", null);
			certDenominationDetails.put("paperlessIndicator", null);
		}

		return certDenominationDetails;
	}

	/**
	 * 
	 * @param address1
	 * @param address2
	 * @param address3
	 * @return
	 */
	public Boolean isPOBOX(String address1, String address2, String address3) {
		return (address1 != null && PO_BOX_PATTERN.matcher(address1).find())
				|| (address2 != null && PO_BOX_PATTERN.matcher(address2).find())
				|| (address3 != null && PO_BOX_PATTERN.matcher(address3).find());

	}
	
	@Override
    public String generateNewPasswordForProfile(RepositoryItem pProfile)
            throws RepositoryException
        {
            String generatedPassword = null;
            if(getIllegalPasswordChecker() != null)
            {
                int i = 0;
                do
                {
                    if(i >= getPasswordGenerationTriesLimit())
                        break;
                    generatedPassword = dswConstants.generateRandomPassword();
                    //KTLO1-875:: SECURICON PEN TEST - Temp password predictable
                    //if(!getIllegalPasswordChecker().matches(generatedPassword))
                    //break;
                    
                    if(getPasswordRuleChecker() != null && getPasswordRuleChecker().isEnabled()){
                    	boolean passedRules = getPasswordRuleChecker().checkRules(generatedPassword, null);
                    	if (passedRules) {
                    		break;
                    	}
                    }
                    i++;
                } while(true);
            } else {
                generatedPassword = dswConstants.generateRandomPassword();
            }
            
            PropertyManager pmgr = getPropertyManager();
            String loginPropertyName = pmgr.getLoginPropertyName();
            String passwordPropertyName = pmgr.getPasswordPropertyName();
            String generatedPwdPropertyName = pmgr.getGeneratedPasswordPropertyName();
            String passwordHasherKeyPropertyName = pmgr.getUserPasswordHasherPropertyName();
            
            MutableRepository repository = (MutableRepository)pProfile.getRepository();
            MutableRepositoryItem mutItem = repository.getItemForUpdate(pProfile.getRepositoryId(), pProfile.getItemDescriptor().getItemDescriptorName());
            String login = (String)pProfile.getPropertyValue(loginPropertyName);
            String salt = passwordSaltForLogin(login);
            if(salt == null)
                salt = login;
            mutItem.setPropertyValue(passwordPropertyName, pmgr.generatePassword(salt, generatedPassword));
            
            //defaulting to current passwordHasher's Key (PBKDF2PasswordHasher-10000) in cryptoAgilityPasswordHashers
            String passwordHasherKey = pmgr.getCryptoAgilityPasswordHasherCode("/atg/dynamo/security/PBKDF2PasswordHasher-10000");
            
            // The below logic will get the passwordHasherKey associated with the currentPasswordHaser from cryptoAgilityPasswordHashers
            // This is done so that in future if the current passwordHasher is changed, then we don't have change the code to update the passwordHasherKey from 5 to new key 
            PasswordHasher currentPasswordHasher = pmgr.getPasswordHasher(); 
            String tempPasswordHasherKey = pmgr.getCryptoAgilityPasswordHasherCode(currentPasswordHasher.getPwdHasherComponentPath());
            if(DigitalStringUtil.isNotBlank(tempPasswordHasherKey)){
            	passwordHasherKey = tempPasswordHasherKey;
            }
            
            mutItem.setPropertyValue(passwordHasherKeyPropertyName, passwordHasherKey);
            mutItem.setPropertyValue(generatedPwdPropertyName, new Boolean(true));
            repository.updateItem(mutItem);
            return generatedPassword;
        }
	
	/**
	 * @param pProfile
	 * @return
	 * @throws RepositoryException
	 */
	public Boolean getCCFlag(RepositoryItem pProfile)
            throws RepositoryException{
		if(pProfile.getPropertyValue("ccFlag")!=null){
			return (Boolean) pProfile.getPropertyValue("ccFlag");
		}else{
			return false;
		}
	}
	/**
	 * @param pProfile
	 * @return
	 * @throws RepositoryException
	 */
	public String getLoyaltyId(RepositoryItem pProfile)
            throws RepositoryException{
		if(pProfile.getPropertyValue("loyaltyNumber")!=null){
			return (String) pProfile.getPropertyValue("loyaltyNumber");
		}else{
			return "";
		}
	}
	/**
	 * @param pProfile
	 * @return
	 * @throws RepositoryException
	 */
	public String getLoyaltyTier(RepositoryItem pProfile) {
		if(pProfile.getPropertyValue("loyaltyTier")!=null){
			return (String) pProfile.getPropertyValue("loyaltyTier");
		}else{
			return "";
		}
	}

	/**
	 * @param pProfile
	 * @return
	 * @throws RepositoryException
	 */
	public Date getLoyaltyExpirationDate(RepositoryItem pProfile) {
		Object obj = pProfile.getPropertyValue( this.getCommercePropertyManager().getLoyaltyTierExpirationDatePropertyName());
		if(obj !=null){
			return (Date) obj;
		}else{
			return null;
		}
	}

	public boolean isGoldOrEliteTier(RepositoryItem pProfile)
			throws RepositoryException{
		boolean ret = false;
		String tier = getLoyaltyTier(pProfile);

		if(DigitalStringUtil.isNotBlank(tier) &&
				(dswConstants.USER_TIER_ELITE.equalsIgnoreCase(tier) ||
						dswConstants.USER_TIER_GOLD.equalsIgnoreCase(tier))){
			ret = true;
		}

		return ret;
	}

	/**
	 * Default value of wishlist count to return for Anonymous user
	 * 
	 * @return
	 */
	public Integer getAnonWishListCount(){
		return 0;
	}	

    /**
     * 
     * @param pItem
     * @param pPropertyName
     * @param pOrigField
     * @return
     */
	public String trimFieldToRepositoryLength( RepositoryItem pItem, String pPropertyName, String pOrigField ) {
		String trimField;
		trimField = pOrigField.trim();
		if( pItem == null || DigitalStringUtil.isEmpty( pPropertyName ) || DigitalStringUtil.isEmpty( pOrigField ) ) { return trimField; }
		try {
			GSAPropertyDescriptor propDescrip = (GSAPropertyDescriptor)pItem.getItemDescriptor().getPropertyDescriptor( pPropertyName );
			if( propDescrip == null || propDescrip.getJDBCColumnLengths() == null || propDescrip.getJDBCColumnLengths().length == 0 ) return trimField;

			int propertyLength = propDescrip.getJDBCColumnLengths()[0];
			if( trimField.length() > propertyLength ) trimField = trimField.substring( 0, propertyLength );

		} catch( RepositoryException e ) {
			if( isLoggingError() ) {
				logger.error( "Repository exception when retrieving item descriptor: ", e );
			}
		}
		return trimField;
	}
	
	/**
	 * @param pProfile
	 * @return
	 * @throws RepositoryException
	 */
	public String getUserStatus(RepositoryItem pProfile)
            throws RepositoryException{
		String userStatus = "";
		try {
			if(this.isLoggedIn(pProfile)) {
				userStatus = USER_STATUS_LOGGED_IN;
			} else if(this.isCookied(pProfile)) {
				userStatus = USER_STATUS_COOKIED;
			} else if(this.isAnonymous(pProfile)) {
				userStatus = USER_STATUS_ANONYMOUS;
			} else if(this.isAnonymousCookied(pProfile)) {
				userStatus = USER_STATUS_ANONYMOUS_COOKIED;
			}
		}catch(Exception ex) {
			logError("Exception while trying to get getUserStatus", ex);
		}
		return userStatus;
	}

	/**
	 * @param profile
	 * @return
	 */
	public boolean isCookied(RepositoryItem profile) {
		boolean retVal = false;
		retVal = !this.isDSWAnanymousUser(profile)
				&& (getSecurityStatusAsInt(profile) == getPropertyManager()
						.getSecurityStatusCookie());
		return retVal;
	}

	/**
	 * @param profile
	 * @return
	 */
	public boolean isAnonymous(RepositoryItem profile) {
		boolean retVal = false;
		retVal = this.isDSWAnanymousUser(profile)
				&& (getSecurityStatusAsInt(profile) == getPropertyManager()
						.getSecurityStatusAnonymous());
		return retVal;
	}

	/**
	 * @param profile
	 * @return
	 */
	public boolean isAnonymousCookied(RepositoryItem profile) {
		boolean retVal = false;
		retVal = this.isDSWAnanymousUser(profile)
				&& (getSecurityStatusAsInt(profile) == getPropertyManager()
						.getSecurityStatusCookie());
		return retVal;
	}
	
	public RepositoryItem getMailingAddress(RepositoryItem pProfile) {
		 return (RepositoryItem)pProfile.getPropertyValue("homeAddress");
	}

	public Boolean isProfileMailingAddressExists(RepositoryItem pProfile) {
		RepositoryItem profileItem = (RepositoryItem) pProfile.getPropertyValue("homeAddress");
		if(profileItem != null && !DigitalStringUtil.isBlank((String) profileItem.getPropertyValue("address1")) && 
				!DigitalStringUtil.isBlank((String)profileItem.getPropertyValue("city"))
				&& !DigitalStringUtil.isBlank((String)profileItem.getPropertyValue("state")) && !DigitalStringUtil.isBlank((String)profileItem.getPropertyValue("postalCode"))){
			return true;
		}
		 return false;
	}

	public Boolean isProfilePrimaryShippingAddressExists(RepositoryItem pProfile) {
		RepositoryItem profileItem = (RepositoryItem) pProfile.getPropertyValue("shippingAddress");
		if(profileItem != null && !DigitalStringUtil.isBlank((String) profileItem.getPropertyValue("address1")) &&
				!DigitalStringUtil.isBlank((String)profileItem.getPropertyValue("city"))
				&& !DigitalStringUtil.isBlank((String)profileItem.getPropertyValue("state")) && !DigitalStringUtil.isBlank((String)profileItem.getPropertyValue("postalCode"))){
			return true;
		}
		return false;
	}

	
	/**
	 * @param loginId
	 * @return
	 */
	public Map<String, Object> lookupUserByLogin(String loginId) {
		final String METHOD_NAME = "lookupUserByEmail";
		Map<String, Object> rtn = new HashMap<>();
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			if (DigitalStringUtil.isNotBlank(loginId)) {
				RepositoryItemDescriptor usersDesc = this.getProfileRepository().getItemDescriptor("user");
				RepositoryView userRepView = usersDesc.getRepositoryView();
				RqlStatement statement = RqlStatement.parseRqlStatement("login = ?0");
				Object params[] = new Object[1];
				params[0] = new String(loginId);
				RepositoryItem[] userItems = statement.executeQuery(userRepView, params);
				if (userItems == null || userItems.length == 0) {
					rtn.put("existingUser", false);
					rtn.put("userID", "");
					rtn.put("loyaltyNumber", "");
					rtn.put("generatedPassword", false);
				} else {
					RepositoryItem item = userItems[0];
					DigitalCommercePropertyManager pmgr = getCommercePropertyManager();
					rtn.put("existingUser", true);
					rtn.put("userID", item.getPropertyValue("id"));
					rtn.put("loyaltyNumber", item.getPropertyValue(pmgr.getLoyaltyNumberPropertyName()));
					rtn.put("generatedPassword", item.getPropertyValue("generatedPassword"));
					rtn.put("userItem", item);
				}
			} else {
				rtn.put("existingUser", false);
				rtn.put("userID", "");
				rtn.put("loyaltyNumber", "");
				rtn.put("generatedPassword", false);
			}
		} catch (RepositoryException e) {
			logError("An exception occurred while determining if the user is a migrated user" + e.getMessage());
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
		return rtn;
	}

	/**
	 * Removes previous session PayPal and GC payment groups from the order if
	 * they are present.
	 *
	 * @param order
	 * @param profile
	 * @throws Exception 
	 */
	public void removePreviousPaymentsFromCurrentOrder(DigitalOrderImpl order, Profile profile) throws Exception {
		final String methodName = "removePreviousPaymentsFromCurrentOrder";

		final UserTransactionDemarcation td = TransactionUtils.startNewTransaction(CLASSNAME, methodName);

		try {

			final PaypalPayment paypalPayment = getPaymentGroupManager().findPaypalPayment(order);

			final List<DigitalGiftCard> gcPaymentList = getPaymentGroupManager().findGiftCards(order);

			boolean updateOrderFlag = false;
			
			String profileId = (String) profile.getPropertyValue("id");

			// Check if PayPal tender is associated with this order, If so clean
			// up
			if (paypalPayment != null) {
				getPaymentGroupManager().removePaymentGroupFromOrder(order, paypalPayment.getId());
				
				// Create default payment group if not present as paypal payment group is removed
				getPaymentGroupManager().createCreditCardFromProfile(order, profile);
				
				updateOrderFlag = true;
				if (isLoggingDebug()) {
					logDebug("Clean-up of previously set PayPal payment group = " + paypalPayment.getId()
							+ " is successful for the order = " + order.getId() + " tied to profile = " + profileId);
				}
			} else {
				if (isLoggingDebug()) {
					logDebug("There are is no PayPal payment group with the order = " + order.getId()
							+ " tied to profile = " + profileId);
				}
			}

			// Check if Gift Card tender is associated with this order, If so
			// clean up
			if (gcPaymentList != null && !gcPaymentList.isEmpty()) {
				for (DigitalGiftCard gcPayment : gcPaymentList) {
					getPaymentGroupManager().removePaymentGroupFromOrder(order, gcPayment.getId());
					updateOrderFlag = true;
					if (isLoggingDebug()) {
						logDebug("Clean-up of previously set Gift Card payment group = " + gcPayment.getId()
								+ " is successful for the order = " + order.getId() + " tied to profile = "
								+ profileId);
					}
				}
			} else {
				if (isLoggingDebug()) {
					logDebug("There are is no Gift card payment groups with the order = " + order.getId()
							+ " tied to profile = " + profileId);
				}
			}

			// Update the order if there are any payment groups are removed
			if (updateOrderFlag) {
				if (DigitalStringUtil.isEmpty(profileId)) {
					profileId = getProfileForOrder(order).getRepositoryId();
				}

				// Transaction handling block begins by acquiring the lock on
				// profile
				try {
					acquireTransactionLock();
				} catch (DeadlockException de) {
					if (isLoggingError()) {
						logError(de);
					}
				}

				try {
					synchronized (order) {
						// perform order updates
						getOrderManager().updateOrder(order);
						logInfo("Clean-up of previously set payment groups is successful for the order = "
								+ order.getId() + " tied to profile = " + profileId);
					}
				} catch (Exception e) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
					throw e;
				} finally {
					try {
						releaseTransactionLock();
						TransactionUtils.endTransaction(td, CLASSNAME, methodName);
					} catch (Throwable th) {
						logError(th);
					}
				}
			} else {
				if (isLoggingDebug()) {
					logDebug("There was no clean up done as there are no payment groups associated with the order = "
							+ order.getId() + " tied to profile = " + profileId);
				}
			}

		} catch (Exception cEx) {
			TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
			throw cEx;
		} finally {
			TransactionUtils.endTransaction(td, CLASSNAME, methodName);
		}
	}
	
	/**
	 * This method will account for Insole User;s ONLY skipping
	 * checkUserPwdHasherMatchesPMGRPwdHasher validation when still KDF-4 (aka
	 * MD5) is on user's profile due to legacy who have not yet converted to
	 * KDF-5. This check has to be skipped as from Insole tool for impersonation
	 * of any given user ONLY username is passed whereas password is not.
	 * 
	 * @param user
	 * @param pLogin
	 * @param pPassword
	 * @return true or false
	 */
	public boolean upgradePassword(RepositoryItem user, String pLogin, String pPassword) {
		if (!dswConstants.isPaTool()) {
			return super.upgradePassword(user, pLogin, pPassword);
		}

		return false;
	}

	public Map<String, Object> getRewardsSummary(Profile profile, boolean syncReward) {
		Map rewardsSummary = new HashMap<>();
		if(profile == null){
			logError(":: Profile is null ::");
			return rewardsSummary;
		}

		if (syncReward) {
			loadBasicLoyaltyInfoIntoProfile(profile, true);
		}

		DigitalCommercePropertyManager dswCpm = getDSWPropertyManager();

		Object dollarsFutureRewardObj = profile.getPropertyValue(dswCpm.getDollarsFutureRewardPropertyName());
		rewardsSummary.put("dollarsFutureReward", dollarsFutureRewardObj);

		Object dollarNextRewardObj = profile.getPropertyValue(dswCpm.getDollarsNextRewardPropertyName());
		rewardsSummary.put("dollarsNextReward", dollarNextRewardObj);

		Object dollarNextTierObj = profile.getPropertyValue(dswCpm.getDollarsNextTierPropertyName());
		rewardsSummary.put("dollarsNextTier", dollarNextTierObj);

		long certOMeter = 0;
		if (dollarNextRewardObj != null && dollarsFutureRewardObj != null) {
			long dollarNextReward = (long) dollarNextRewardObj;
			long dollarsFutureReward = (long) dollarsFutureRewardObj;

			if(dollarsFutureReward > dollarNextReward) {
				certOMeter = ((dollarsFutureReward - dollarNextReward) * 100) / dollarsFutureReward;
				if (certOMeter < 0) {
					certOMeter = 0;
				}
			}
		}
		rewardsSummary.put("certOMeter", certOMeter);

		String currentLoyaltyTier = (String)profile.getPropertyValue(dswCpm.getLoyaltyTierItemDescriptorName());
		rewardsSummary.put("loyaltyTier", currentLoyaltyTier);


		String loyaltyTierExpirationDate = null;
		int currentYear = DigitalDateUtil.getCurrentYear();
		int expirationYear = DigitalDateUtil.getCurrentYear();
		try {
			//tier expiration date for basic or club member will always be null. Only for Gold/Elite members tier expiration date is set.
			Object expDate = profile.getPropertyValue(dswCpm.getLoyaltyTierExpirationDatePropertyName());
			if(expDate != null && expDate instanceof Date) {
				loyaltyTierExpirationDate = getCustomDateFormatter().getLocalizedDateString((Date)expDate);
				expirationYear = DigitalDateUtil.getYear((Date)expDate);
			}else{
				logInfo("loyaltyTierExpirationDate is invalid :: " + expDate + " :: for tier:: " + currentLoyaltyTier);
			}
		}catch(Exception ex){
			logError("Exception converting loyaltyTierExpirationDate", ex);
		}
		rewardsSummary.put("loyaltyTierExpirationDate", loyaltyTierExpirationDate);
		rewardsSummary.put("loyaltyNumber",profile.getPropertyValue(dswCpm.getLoyaltyNumberPropertyName()));
		rewardsSummary.put("id",profile.getPropertyValue("id"));

		rewardsSummary.put("pointsNextReward",profile.getPropertyValue(dswCpm.getPointsNextRewardPropertyName()));

		String nextTier =  null;
		long dollarsNextTierMinimum = 0;
		long tierOMeter = 0;

		long dollarNextTier = 0;
		if(dollarNextTierObj != null && dollarNextTierObj instanceof Long){
			dollarNextTier = (long)dollarNextTierObj;
		}else if(DigitalBaseConstants.USER_TIER_GOLD.equalsIgnoreCase(currentLoyaltyTier)){
			dollarNextTier = dswConstants.getRewardsGoldTierQualifierDollar();
		}else if(DigitalBaseConstants.USER_TIER_ELITE.equalsIgnoreCase(currentLoyaltyTier)){
			dollarNextTier = dswConstants.getRewardsEliteTierQualifierDollar();
		}

		if(DigitalBaseConstants.USER_TIER_CLUB.equalsIgnoreCase(currentLoyaltyTier)){
			nextTier = DigitalBaseConstants.USER_TIER_GOLD;
			dollarsNextTierMinimum = dswConstants.getRewardsGoldTierQualifierDollar();
			tierOMeter =((dollarsNextTierMinimum - dollarNextTier)*100)/dollarsNextTierMinimum;
		}else if(DigitalBaseConstants.USER_TIER_GOLD.equalsIgnoreCase(currentLoyaltyTier)){
			if(expirationYear == currentYear){
				dollarsNextTierMinimum = dswConstants.getRewardsGoldTierQualifierDollar();
				nextTier = String.valueOf(currentYear);
			}else{
				dollarsNextTierMinimum = dswConstants.getRewardsEliteTierQualifierDollar();
				nextTier = DigitalBaseConstants.USER_TIER_ELITE;
			}
			tierOMeter =((dollarsNextTierMinimum - dollarNextTier)*100)/dollarsNextTierMinimum;
		}else if(DigitalBaseConstants.USER_TIER_ELITE.equalsIgnoreCase(currentLoyaltyTier)){
			if(expirationYear == currentYear){
				dollarsNextTierMinimum = dswConstants.getRewardsEliteTierQualifierDollar();
				nextTier = String.valueOf(currentYear);
				tierOMeter =((dollarsNextTierMinimum - dollarNextTier)*100)/dollarsNextTierMinimum;
			}else{
				tierOMeter = 100;
				nextTier = null;
			}
		}

		if(tierOMeter < 0){
			tierOMeter = 0;
		}

		rewardsSummary.put("tierOMeter", tierOMeter);
		rewardsSummary.put("nextTier", nextTier);
		rewardsSummary.put("dollarsNextTierMinimum",dollarsNextTierMinimum);

		return rewardsSummary;
	}

	public DigitalCommercePropertyManager getDSWPropertyManager(){
		return (DigitalCommercePropertyManager)getPropertyManager();
	}

	/**
	 *
	 * @param pProfile
	 * @param pRequest
	 * @param pResponse
	 */
	public void createOrUpdateTierCookie(Profile pProfile, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		try {
			String profileLoyaltyTier = getLoyaltyTier(pProfile);
			String loyaltyTierValue =
          DigitalStringUtil.isNotBlank(profileLoyaltyTier)? profileLoyaltyTier : "GUEST";
			boolean tierCookieNotPresentFlag = true;
			Cookie[] cookies = pRequest.getCookies();
			if (null != cookies) {
				for (Cookie cookie : cookies) {
					String cookieName = cookie.getName();
					if (TIER_COOKIE_NAME.equalsIgnoreCase(cookieName)) {
						if(!loyaltyTierValue.equalsIgnoreCase(cookie.getValue())) {
							cookie.setValue(loyaltyTierValue);
							cookie.setPath("/");
							cookie.setHttpOnly(true);
							cookie.setSecure(true);
							pResponse.addCookie(cookie);
						}
						tierCookieNotPresentFlag = false;
						break;
					}
				}
			}

			if(tierCookieNotPresentFlag) {
				HTTPUtils.addSessionCookie(TIER_COOKIE_NAME, loyaltyTierValue, pResponse, true,  true);
			}
		} catch (Exception e) {
			logError("TIER Cookie creation failed. " + e.getMessage());
		}
	}
}

