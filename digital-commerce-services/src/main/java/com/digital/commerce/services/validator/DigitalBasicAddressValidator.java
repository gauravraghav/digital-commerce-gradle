package com.digital.commerce.services.validator;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.DigitalProfileConstants;
import com.digital.commerce.services.common.AddressType;
import com.digital.commerce.services.common.CountryCode;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.profile.DigitalProfileTools;

import atg.commerce.CommerceException;
import atg.core.util.Address;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalBasicAddressValidator extends GenericService{

	String			nameCharsRegEx;
	Pattern			NAME_PATTERN;
	String			poboxCharsRegEx;
	String			poboxChars4suggRegEx;
	String			poboxCharsReplaceRegEx;
	Pattern			POBOX_PATTERN;
	Pattern			POBOX_PATTERN_4SUGG;

	String			emailCharsRegEx;
	Pattern			EMAIL_PATTERN;

	String			phoneCharsRegEx;
	Pattern			PHONE_PATTERN;
	private String	emailConfirmKey;

	String			digitCharsRegEx	;
	Pattern			DIGIT_PATTERN;

	String			subtotalCharsRegEx;
	Pattern			SUBTOTAL_PATTERN;

	String			zipTypeUSCharsRegEx;
	Pattern			ZIP_TYPE_US_PATTERN;

	String			zipTypeCanadaCharsRegEx;
	Pattern			ZIP_TYPE_CANADA_PATTERN;

	String			zipTypeJapanCharsRegEx;
	Pattern			ZIP_TYPE_JAPAN_PATTERN;
	
	@Override
	public void doStartService() throws ServiceException {
		super.doStartService();
		NAME_PATTERN			= Pattern.compile( nameCharsRegEx );
		POBOX_PATTERN			= Pattern.compile( poboxCharsRegEx );
		POBOX_PATTERN_4SUGG		= Pattern.compile( poboxChars4suggRegEx );
		EMAIL_PATTERN			= Pattern.compile( emailCharsRegEx );
		PHONE_PATTERN			= Pattern.compile( phoneCharsRegEx );
		DIGIT_PATTERN			= Pattern.compile( digitCharsRegEx );
		SUBTOTAL_PATTERN		= Pattern.compile( subtotalCharsRegEx );
		ZIP_TYPE_US_PATTERN		= Pattern.compile( zipTypeUSCharsRegEx );
		ZIP_TYPE_CANADA_PATTERN	= Pattern.compile( zipTypeCanadaCharsRegEx );
		ZIP_TYPE_JAPAN_PATTERN	= Pattern.compile( zipTypeJapanCharsRegEx );
	}


	public List<String> validateAddress(DigitalContactInfo address ) {
		return validateAddress( address, null );
	}
	
	public List<String> validateContactInfo( DigitalContactInfo address) {
		List<String> fields = new LinkedList<>();

		//Check for null or blank email address and do basic validation only if email is not null or blank
		if(DigitalStringUtil.isBlank(address.getEmail())){
			fields.add( DigitalProfileConstants.MSG_EMAIL_MISSING );
		}else if(!validateEmailAddress( address.getEmail()) ){ 
			fields.add( DigitalProfileConstants.MSG_INVALID_EMAIL );
		}
		if(DigitalStringUtil.isBlank(address.getPhoneNumber())){
			fields.add( DigitalProfileConstants.MSG_INVALID_PHONE_NUMBER );
		}else if(!validatePhoneNbr( address.getPhoneNumber(),null,null,null) ){ 
			fields.add( DigitalProfileConstants.MSG_INVALID_PHONE );
		}
		return fields;
	}
	
	public List<String> validateShippingAddress( DigitalContactInfo address ) {
		List<String> fields = new LinkedList<>();
		if( address == null ) return fields;
		AddressType addressType = AddressType.valueFor( address );
		CountryCode countryCode = CountryCode.valueFor( address.getCountry() );
		if( !validateFirstName( address.getFirstName() ) ) fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_FIRSTNAME );
		if( !validateLastName( address.getLastName() ) ) fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_LASTNAME );
		if( !validateStreetAddress( address.getAddress1() ) ) fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_ADDRESS1 );
		if( !validateCityName( address.getCity() ) ) {
			if( !AddressType.MILITARY.equals( addressType ) ) {
				fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_CITY );
			} else {
				fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_SELECTED_CITY );
			}
		}

		if( !AddressType.INTERNATIONAL.equals( addressType ) ) {
			if( !( validatePostalCode( address.getPostalCode() ) ) ) {
				fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_ZIP_CODE_USA );
			} else if( AddressType.USA.equals( addressType ) ) {
				if( !( ZIP_TYPE_US_PATTERN.matcher( address.getPostalCode() ).matches() ) ) {
					fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_ZIP_CODE_USA );
				}
			}
		} else if( AddressType.INTERNATIONAL.equals( addressType ) ) {
			if( CountryCode.JAPAN.equals( countryCode ) ) {
				if( !validatePostalCode( address.getPostalCode() ) ) {
					fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_ZIP_CODE_INTERNATIONAL );
				} else if( !ZIP_TYPE_JAPAN_PATTERN.matcher( address.getPostalCode() ).matches() ) {
					fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_ZIP_CODE_INTERNATIONAL );
				}
			} else if( CountryCode.CANADA.equals( countryCode ) ) {
				if( !validatePostalCode( address.getPostalCode() ) ) {
					fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_ZIP_CODE_INTERNATIONAL );
				} else if( !ZIP_TYPE_CANADA_PATTERN.matcher( address.getPostalCode() ).matches() ) {
					fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_ZIP_CODE_INTERNATIONAL );
				}
			} else {
				if( !validateInternationalPostalCode( address.getPostalCode() ) ) {
					fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_ZIP_CODE_INTERNATIONAL );
				}
			}
		}

		if( AddressType.MILITARY.equals( addressType ) ) {
			if( !validateRegion( address.getRegion() ) ) {
				fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_REGION );
			}
			if( DigitalStringUtil.isNotBlank( address.getRank() ) ) {
				if( !validateRank( address.getRank() ) ) {
					fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_RANK );
				}
			}
		} else {
			if( !validateStateName( address.getState() ) ) {
				if( AddressType.INTERNATIONAL.equals( addressType ) ) {
					fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_PROVINCE_TERRITORY );
				} else {
					fields.add( DigitalProfileConstants.MSG_INVALID_SHIPPING_STATE );
				}
			}
		}
		return fields;
	}
	
	
	public boolean isValidBillingAddress(Address address) {

		if (address == null) {
			return false;
		}
		DigitalContactInfo contactInfo = null;

		try {
			contactInfo = DigitalProfileTools.copyAddress(address, new DigitalContactInfo());
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError("Error copying address ", e);
			}
		}

		if (isLoggingDebug()) {
			logDebug("The copied billing address " + contactInfo);
		}
		
		List<String> errors = validateAddress(contactInfo, null, false);
		
		if (errors != null && errors.size() > 0) {
			logError("Invalid billing address " + contactInfo);
			return false;
		} else {
			return true;
		}
	}

	public List<String> validateAddress( DigitalContactInfo address, String emailConfirm) {
		return validateAddress(address, emailConfirm, true);
	}
	
	public List<String> validateAddress( DigitalContactInfo address, String emailConfirm, boolean validateEmail ) {
		List<String> fields = new LinkedList<>();

		if( address == null ) { return fields; }
		AddressType addressType = AddressType.valueFor( address );
		CountryCode countryCode = CountryCode.valueFor( address.getCountry() );

		if(isLoggingDebug()) {
			logDebug("Incoming address " + address);
		}
		if( !validateFirstName( address.getFirstName() ) ) {
			logError("First name validation failed");
			fields.add( DigitalProfileConstants.MSG_INVALID_FIRSTNAME );
		}
		if( !validateLastName( address.getLastName() ) ) {
			logError("Last name validation failed");
			fields.add( DigitalProfileConstants.MSG_INVALID_LASTNAME );
		}

		if(isLoggingDebug()) {
			logDebug("Address.getPobox " + address.getPobox());
		}
		
		if( !DigitalStringUtil.isBlank( address.getPobox()) ) {
			if( !validatePOBox( address.getPobox() ) ) {
				logError("PO Box validation failed");
				fields.add( setPOBoxSuggetion( address ) );
			}

			if( !validatePOBox( address.getPobox(), address.getAddress1() ) ) {
				logError("PO Box Address1 validation failed");
				fields.add( DigitalProfileConstants.MSG_INVALID_ADDRESS );
			}

			if( !validatePOBox( address.getPobox(), address.getAddress2() ) ) {
				logError("PO Box Address2 validation failed");
				fields.add( DigitalProfileConstants.MSG_INVALID_ADDRESS );
			}
		} else if( !DigitalStringUtil.isBlank( address.getAddress1() ) ) {
			if( !validateStreetAddress( address.getAddress1() ) ) {
				logError("Addressline1 invalid");
				fields.add( DigitalProfileConstants.MSG_INVALID_ADDRESS );
			}
		} else {
			logError("Addressline1 is blank");
			fields.add( DigitalProfileConstants.MSG_INVALID_ADDRESS );
		}

		if( !validateSecondaryAddress( address.getAddress2() ) ) {
			logError("Addressline2 invalid");
			fields.add( DigitalProfileConstants.MSG_INVALID_ADDRESS2 );
		}

		if( !validateSecondaryAddress( address.getAddress3() ) ) {
			logError("Addressline3 invalid");
			fields.add( DigitalProfileConstants.MSG_INVALID_ADDRESS3 );
		}
	   
		if( AddressType.MILITARY.equals( addressType ) ) {
    		if(address.getCity()!=null){
				String city=address.getCity();
				if(!city.equalsIgnoreCase(DigitalProfileConstants.CITY_APO) && !city.equalsIgnoreCase(DigitalProfileConstants.CITY_FPO)){
					logError("Military Rank is invalid");
					fields.add( DigitalProfileConstants.MSG_INVALID_SELECTED_PO);
				}
			}
       }else if( !validateCityName( address.getCity() ) ) {
    	   	logError("City is invalid");
			fields.add( DigitalProfileConstants.MSG_INVALID_CITY );
	   }
		// VALIDATE ZIP (POSTAL) CODE
		/* Modification for defect 390 - Starts Also corrected the logic to
		 * display form error when zip code is left blank in US/Canada and
		 * Military addresses. Previously there has been no check for Zip code
		 * to be mandatory for Military and US/Canada Address. */
		if( !AddressType.INTERNATIONAL.equals( addressType ) ) {			
			if(DigitalStringUtil.isBlank( address.getPostalCode())){
				logError("Postal Code is blank");
				fields.add( DigitalProfileConstants.MSG_INVALID_ZIP_CODE_USA );
			}
			if( !validatePostalCode( address.getPostalCode() ) ) {
				logError("Postal Code is invalid");
				fields.add( DigitalProfileConstants.MSG_INVALID_ZIP_CODE_USA );
			} else if( AddressType.USA.equals( addressType ) ) {
				if( !ZIP_TYPE_US_PATTERN.matcher( address.getPostalCode() ).matches() ) {
					logError("USA Postal Code is invalid");
					fields.add( DigitalProfileConstants.MSG_INVALID_ZIP_CODE_USA );
				}
			}
		}

		else if( AddressType.INTERNATIONAL.equals( addressType ) ) {
			if(DigitalStringUtil.isBlank( address.getPostalCode())){
				logError("INTERNATIONAL Postal Code is blank");
				fields.add( DigitalProfileConstants.MSG_INVALID_ZIP_CODE_INTERNATIONAL );
			}			
			if( CountryCode.JAPAN.equals( countryCode ) ) {
				if( !validatePostalCode( address.getPostalCode() ) ) {
					logError("JAPAN Postal Code is invalid");
					fields.add( DigitalProfileConstants.MSG_INVALID_ZIP_CODE_INTERNATIONAL );
				} else if( !( ZIP_TYPE_JAPAN_PATTERN.matcher( address.getPostalCode() ).matches() ) ) {
					logError("JAPAN Postal Code is invalid for the pattern match");
					fields.add( DigitalProfileConstants.MSG_INVALID_ZIP_CODE_INTERNATIONAL );
				}
			} else if( CountryCode.CANADA.equals( countryCode ) ) {
				if( !validatePostalCode( address.getPostalCode() ) ) {
					logError("CANADA Postal Code is invalid");
					fields.add( DigitalProfileConstants.MSG_INVALID_ZIP_CODE_INTERNATIONAL );
				} else if( !( ZIP_TYPE_CANADA_PATTERN.matcher( address.getPostalCode() ).matches() ) ) {
					logError("CANADA Postal Code is invalid for the pattern match");
					fields.add( DigitalProfileConstants.MSG_INVALID_ZIP_CODE_INTERNATIONAL );
				}
			} else {
				if( !validateInternationalPostalCode( address.getPostalCode() ) ) {
					logError("INTERNATIONAL Postal Code is invalid");
					fields.add( DigitalProfileConstants.MSG_INVALID_ZIP_CODE_INTERNATIONAL );
				}
			}
		}
			//validate only if we get some data or else  do not 
			if(DigitalStringUtil.isNotBlank( address.getPhoneNumber()) && !validatePhoneNbr( address.getPhoneNumber(), address.getAddressType(), address.getCountry(), address.getState() ) ) {
				logError("Phone number is invalid");
				fields.add( DigitalProfileConstants.MSG_INVALID_PHONE );
			}
		
			
			if(DigitalStringUtil.isNotBlank( address.getEmail()) && !validateEmailAddress( address.getEmail()) ) {
				logError("Email is invalid");
				fields.add( DigitalProfileConstants.MSG_INVALID_EMAIL );
			}
		
		if( validateEmail && DigitalStringUtil.isNotBlank( emailConfirm ) ) {
			if( !confirmEmail( address.getEmailConfirm(), emailConfirm ) ) {
				fields.add( DigitalProfileConstants.MSG_INVALID_CONFIRM_EMAIL );
			}
		}
		if( AddressType.INTERNATIONAL.equals( addressType ) && !validateCountry( address.getCountry() ) ) {
			logError("INTERNATIONAL Country is invalid");
			fields.add( DigitalProfileConstants.MSG_INVALID_COUNTRY );
		}
		if( AddressType.MILITARY.equals( addressType ) ) {
			if(address.getRegion()==null && address.getState()!=null){
				String state=address.getState();
				if(state.equalsIgnoreCase(DigitalProfileConstants.REGION_AA)||state.equalsIgnoreCase(DigitalProfileConstants.REGION_AE)||
						state.equalsIgnoreCase(DigitalProfileConstants.REGION_AP)){
					address.setRegion(state);
				}
			}			
			if(address.getRegion()!=null){
				String region=address.getRegion();
				if(!region.equalsIgnoreCase(DigitalProfileConstants.REGION_AA) && !region.equalsIgnoreCase(DigitalProfileConstants.REGION_AE) &&
							!region.equalsIgnoreCase(DigitalProfileConstants.REGION_AP)){
					logError("Military Region is invalid");
					fields.add( DigitalProfileConstants.MSG_INVALID_REGION );
				}
			}
		} else {
			if( !validateStateName( address.getState() ) ) {
				if( AddressType.INTERNATIONAL.equals( addressType ) ) {
					logError("Internation province is invalid");
					fields.add( DigitalProfileConstants.MSG_INVALID_PROVINCE_TERRITORY );
				} else {
					logError("Internation state is invalid");
					fields.add( DigitalProfileConstants.MSG_INVALID_STATE );
				}
			}
		}
		return fields;
	}
	
	public boolean validateFirstName( String pFName ) {
		return !DigitalStringUtil.isEmpty( pFName ) && pFName.trim().length() >= DigitalProfileConstants.MIN_FNAME_LENGTH && pFName.trim().length() <= DigitalProfileConstants.MAX_FNAME_LENGTH && NAME_PATTERN.matcher( pFName ).matches();
	}

	public boolean validateCompanyName( String pCName ) {
		if( pCName != null ) {
			return !( pCName.trim().length() > DigitalProfileConstants.MAX_COMPANY_LENGTH );
		} else {
			return true;
		}
	}

	public boolean validateLastName( String pLName ) {
		return !DigitalStringUtil.isEmpty( pLName ) && pLName.trim().length() >= DigitalProfileConstants.MIN_LNAME_LENGTH && pLName.trim().length() <= DigitalProfileConstants.MAX_LNAME_LENGTH && NAME_PATTERN.matcher( pLName ).matches();
	}

	public boolean validateCityName( String pCName ) {
		return !DigitalStringUtil.isEmpty( pCName ) && pCName.length() >= DigitalProfileConstants.MIN_CITY_LENGTH && pCName.length() <= DigitalProfileConstants.MAX_CITY_LENGTH;
	}

	public boolean validateStateName( String pSName ) {
		return !DigitalStringUtil.isEmpty( pSName ) && pSName.length() >= DigitalProfileConstants.MIN_STATE_LENGTH && pSName.length() <= DigitalProfileConstants.MAX_STATE_LENGTH;
	}

	public boolean validatePostalCode( String pPCode ) {
		return DigitalStringUtil.isNotBlank( pPCode ) && pPCode.trim().length() >= DigitalProfileConstants.MIN_US_POSTAL_CODE_LENGTH && pPCode.trim().length() <= DigitalProfileConstants.MAX_US_POSTAL_CODE_LENGTH;
	}

	public boolean validateInternationalPostalCode( String postalCode ) {
		return postalCode == null || ( DigitalStringUtil.trimToEmpty( postalCode ).length() >= DigitalProfileConstants.MIN_POSTAL_CODE_LENGTH && DigitalStringUtil.trimToEmpty( postalCode ).length() <= DigitalProfileConstants.MAX_POSTAL_CODE_LENGTH );
	}

	public boolean validateCountry( String pCountry ) {
		return !DigitalStringUtil.isEmpty( pCountry ) && pCountry.length() >= DigitalProfileConstants.MIN_COUNTRY_LENGTH && pCountry.length() <= DigitalProfileConstants.MAX_COUNTRY_LENGTH;
	}

	public boolean validateStreetAddress( String pAddress ) {
		// if the street address is parsed, then its assumed that numbers are entered
		boolean validAddress = false;
		try {
			Double.parseDouble( DigitalStringUtil.trimToEmpty( pAddress ) );
		} catch( NumberFormatException e ) {
			validAddress = true;
		}

		return DigitalStringUtil.isNotEmpty( pAddress ) && pAddress.length() >= DigitalProfileConstants.MIN_ADDR_LENGTH && pAddress.length() <= DigitalProfileConstants.MAX_ADDR_LENGTH && validAddress;
	}

	public boolean validatePOBox( String pPOBox ) {
		// if the street address is parsed, then its assumed that numbers are entered
		try {
			Double.parseDouble( DigitalStringUtil.trimToEmpty( pPOBox ) );
		} catch( NumberFormatException e ) {
		}

		return !DigitalStringUtil.isEmpty( pPOBox ) && pPOBox.length() >= DigitalProfileConstants.MIN_ADDR_LENGTH && pPOBox.length() <= DigitalProfileConstants.MAX_ADDR_LENGTH && POBOX_PATTERN.matcher( pPOBox ).matches();

	}

	private String setPOBoxSuggetion( DigitalContactInfo address ) {
		String tPOBox = address.getPobox();
		String sPOBox = "";

		try {
			Integer.parseInt( tPOBox );
			address.setPobox( "PO Box " + tPOBox );
			return DigitalProfileConstants.MSG_SUGGEST_POBOX;
		} catch( NumberFormatException e ) {
			//logger.info( "Non-numeric PO Box received: " + tPOBox );
		}

		try {
			if( POBOX_PATTERN_4SUGG.matcher( tPOBox ).matches() ) {
				sPOBox = tPOBox.replaceFirst( poboxCharsReplaceRegEx, "PO Box" );
				address.setPobox( sPOBox );
				return DigitalProfileConstants.MSG_SUGGEST_POBOX;
			} else {
				return DigitalProfileConstants.MSG_INVALID_POBOX;
			}
		} catch( Exception e ) {
			return DigitalProfileConstants.MSG_INVALID_POBOX;
		}
	}

	private boolean validatePOBox( String pobox, String address1 ) {
		String tPobox = null;
		if( pobox != null && pobox.trim().length() != 0 ) tPobox = pobox;
		String tAddress1 = null;
		if( address1 != null && address1.trim().length() != 0 ) tAddress1 = address1;

		if( tPobox != null && tAddress1 != null ) return false;
		return true;
	}

	public boolean validateSecondaryAddress( String pAddress ) {
		if( pAddress != null ) { return !( pAddress.length() > DigitalProfileConstants.MAX_ADDR_LENGTH ); }
		return true;
	}

	public boolean validateOtherAddress( String pAddress ) {
		// if the address is parsed, then its assumed that numbers are entered
		boolean validAddress = false;
		try {
			Double.parseDouble( DigitalStringUtil.trimToEmpty( pAddress ) );
		} catch( NumberFormatException e ) {
			validAddress = true;
		}

		return !DigitalStringUtil.isEmpty( (String)pAddress ) && pAddress.length() <= DigitalProfileConstants.MAX_ADDR_LENGTH && validAddress;

	}

	public boolean validatePhoneNbr( String pPhNbr, String type, String country, String state ) {
		String cleanPhone = DigitalStringUtil.trimToEmpty( pPhNbr );
		AddressType addressType = AddressType.valueFor( type, country, state );
		CountryCode countryCode = CountryCode.valueFor( country );
		if( AddressType.USA.equals( addressType ) || CountryCode.CANADA.equals( countryCode ) ) {
			if( cleanPhone.length() > 0 && cleanPhone.charAt( 0 ) == '1' ) {
				cleanPhone = cleanPhone.substring( 1 );
			}
			return !DigitalStringUtil.isEmpty( cleanPhone ) && cleanPhone.length() >= DigitalProfileConstants.MIN_PHONE_LENGTH
					&& cleanPhone.length() <= DigitalProfileConstants.MAX_PHONE_US_LENGTH
					&& PHONE_PATTERN.matcher( cleanPhone ).matches();
		} else {
			return !DigitalStringUtil.isEmpty( cleanPhone ) && cleanPhone.length() >= DigitalProfileConstants.MIN_PHONE_LENGTH
					&& cleanPhone.length() <= DigitalProfileConstants.MAX_PHONE_LENGTH
					&& PHONE_PATTERN.matcher( cleanPhone ).matches();
		}
	}
	
	public boolean validateRegion( String pRegion ) {
		return !DigitalStringUtil.isEmpty( pRegion ) && pRegion.trim().length() >= DigitalProfileConstants.MIN_REGION_LENGTH;
	}
	
	/** This function validates the Rank
	 * 
	 * @param pRank as string
	 * @return true if rank is validated, false otherwise. */
	public boolean validateRank( String pRank ) {
		if( ( null != pRank ) && ( pRank.trim().length() > DigitalProfileConstants.MAX_RANK_LENGTH ) ) { return false; }
		return true;
	}

	public boolean validateEmailAddress( String pEmail ) {
		// if there's no email passed, there was no input field and we don't validate it
		if( pEmail == null ) {
			return true;
		} // Set the email pattern string
		if( pEmail.trim().length() < DigitalProfileConstants.MIN_EMAIL_LENGTH
				|| pEmail.trim().length() > DigitalProfileConstants.MAX_EMAIL_LENGTH
				|| !EMAIL_PATTERN.matcher( pEmail.trim() ).matches() ) {
			return false;
		}
		return true;
	}
	
	public boolean confirmEmail( String pEmail, String pConfirm ) {
		return pConfirm.trim().equalsIgnoreCase( pEmail );
	}
}
