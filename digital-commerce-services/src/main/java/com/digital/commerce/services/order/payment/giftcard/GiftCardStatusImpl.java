/**
 * 
 */
package com.digital.commerce.services.order.payment.giftcard;

import java.sql.Timestamp;
import java.util.Date;

import atg.payment.PaymentStatusImpl;
import lombok.Getter;
import lombok.Setter;

/** This class defines a gift card transaction status. It encapsulates basic
 * transaction information returned from a gift card payment system
 * 
 * @author djboyd */
@Getter
@Setter
public class GiftCardStatusImpl extends PaymentStatusImpl implements GiftCardStatus {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -4125674646446849938L;
	private Timestamp			authorizationExpiration;
	private String				authorizationCode;

	/**
	 * 
	 */
	public GiftCardStatusImpl() {}

	/** @param pTransactionId
	 * @param pAmount
	 * @param pTransactionSuccess
	 * @param pErrorMessage
	 * @param pTransactionTimestamp */
	public GiftCardStatusImpl( String pTransactionId, double pAmount, boolean pTransactionSuccess, String pErrorMessage, Date pTransactionTimestamp ) {
		super( pTransactionId, pAmount, pTransactionSuccess, pErrorMessage, pTransactionTimestamp );
	}

	/* (non-Javadoc)
	 * @see com.digital.edt.payment.giftcard.GiftCardStatus#getApprovalFlag() */
	public boolean getApprovalFlag() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.digital.edt.payment.giftcard.GiftCardStatus#getApprovedAmount() */
	public double getApprovedAmount() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.digital.edt.payment.giftcard.GiftCardStatus#getSRC() */
	public String getSRC() {
		// TODO Auto-generated method stub
		return null;
	}

}
