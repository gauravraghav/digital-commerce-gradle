package com.digital.commerce.services.pricing.reprice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.digital.commerce.common.util.DigitalStringUtil;

/*  */

@SuppressWarnings({"rawtypes","unchecked"})
public class ReturnOrdersHandler extends DefaultHandler {
	private final String	RETURNORDER				= "ReturnOrder";

	private final String	PREVIOUSRETURN			= "PreviousReturn";

	private final String	RETURNORDERID			= "returnOrderID";

	private final String	LINEITEM				= "lineItem";

	private final String	TIMESTAMP				= "timestamp";

	private final String	LINEITEMID				= "lineItemId";

	private final String	CUSTOMERREQUESTEXCHANGE	= "CustomerRequestedExchange";

	private final String	QUANTITY				= "quantity";

	private final String	SHIPPINGGROUPID			= "shippingGroupID";

	private final String	CUSTOMERREASONCODE		= "customerReasonCode";

	private final String	DISPOSITIONCODE			= "dispositionCode";

	private final String	ENTRYTYPE				= "entryType";

	private final String	ORDERLINEKEY			= "DerivedFromOrderLineKey";

	private List			returnOrders			= new ArrayList( 10 );

	private String			returnOrder				= "";

	private String			returnOrderId			= "";

	private String			lineItem				= "";

	private String			lineItemId				= "";

	private String			customerRequestExchange	= "";

	private String			quantity				= "";

	private String			shippingGroupId			= "";

	private String			customerReasonCode		= "";

	private String			dispositionCode			= "";

	private String			entryType				= "";

	private String			timestamp				= "";

	private String			orderLineKey			= "";

	private StringBuffer	characterBuf;

	private Calendar		calendar				= Calendar.getInstance();

	ReturnItem				ri						= null;

	String					returnOrderIdValue		= null;

	private String			errMsg					= null;

	public String getErrorMessage() {
		return errMsg;
	}

	public List getReturnOrders() {
		return returnOrders;
	}

	/** Receive notification of the start of an element.
	 * 
	 * @param namespaceURI -
	 *            The Namespace URI, or the empty string if the element has no
	 *            Namespace URI or if Namespace processing is not being
	 *            performed.
	 * @param localName -
	 *            The local name (without prefix), or the empty string if
	 *            Namespace processing is not being performed.
	 * @param qName -
	 *            The qualified name (with prefix), or the empty string if
	 *            qualified names are not available.
	 * @param atts -
	 *            The attributes attached to the element. If there are no
	 *            attributes, it shall be an empty Attributes object.
	 * @throws SAXException -
	 *             Any SAX exception, possibly wrapping another exception. */
	public void startElement( String namespaceURI, String localName, String qName, Attributes atts ) {
		characterBuf = new StringBuffer();
		try {
			if( RETURNORDER.equals( localName ) || PREVIOUSRETURN.equals( localName ) ) {
				returnOrder = RETURNORDER;
			}

			if( DigitalStringUtil.isNotBlank(returnOrder)) {
				if( RETURNORDERID.equals( localName ) ) {
					returnOrderId = RETURNORDERID;
				}
				if( LINEITEM.equals( localName ) ) {
					lineItem = LINEITEM;
				}
			}

			if( DigitalStringUtil.isNotBlank(returnOrder) && DigitalStringUtil.isNotBlank(lineItem)) {
				if( ri == null ) ri = new ReturnItem();

				if( LINEITEMID.equals( localName ) ) lineItemId = LINEITEMID;
				if( CUSTOMERREQUESTEXCHANGE.equals( localName ) ) {
					customerRequestExchange = CUSTOMERREQUESTEXCHANGE;
				}
				if( TIMESTAMP.equals( localName ) ) {
					timestamp = TIMESTAMP;
				}

				if( QUANTITY.equals( localName ) ) {
					quantity = QUANTITY;
				}

				if( SHIPPINGGROUPID.equals( localName ) ) {
					shippingGroupId = SHIPPINGGROUPID;
				}

				if( CUSTOMERREASONCODE.equals( localName ) ) {
					customerReasonCode = CUSTOMERREASONCODE;
				}

				if( DISPOSITIONCODE.equals( localName ) ) {
					dispositionCode = DISPOSITIONCODE;
				}

				if( ENTRYTYPE.equals( localName ) ) {
					entryType = ENTRYTYPE;
				}

				if( ORDERLINEKEY.equals( localName ) ) {
					orderLineKey = ORDERLINEKEY;
				}
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}

	/** Receive notification of character data inside an element.
	 * 
	 * @param ch -
	 *            The characters.
	 * @param start -
	 *            The start position in the character array.
	 * @param length -
	 *            The number of characters to use from the character array.
	 * @throws SAXException -
	 *             Any SAX exception, possibly wrapping another exception. */
	public void characters( char[] ch, int start, int length ) {
		characterBuf.append( ch, start, length );
	}

	private long format( String value ) {
		int year = Integer.parseInt( value.substring( 0, 4 ) );
		int month = Integer.parseInt( value.substring( 5, 7 ) );
		int date = Integer.parseInt( value.substring( 8, 10 ) );
		int hour = Integer.parseInt( value.substring( 11, 13 ) );
		int minute = Integer.parseInt( value.substring( 14, 16 ) );
		int second = Integer.parseInt( value.substring( 17 ) );

		calendar.set( year, month, date, hour, minute, second );
		return calendar.getTimeInMillis();
	}

	/** Receive notification of the end of an element.
	 * 
	 * @param namespaceURI -
	 *            The Namespace URI, or the empty string if the element has no
	 *            Namespace URI or if Namespace processing is not being
	 *            performed.
	 * @param localName -
	 *            The local name (without prefix), or the empty string if
	 *            Namespace processing is not being performed.
	 * @param qName -
	 *            The qualified name (with prefix), or the empty string if
	 *            qualified names are not available.
	 * @throws SAXException -
	 *             Any SAX exception, possibly wrapping another exception. */
	public void endElement( String namespaceURI, String localName, String qName ) {
		populateValue();
		try {
			if( DigitalStringUtil.isNotBlank(returnOrder) && DigitalStringUtil.isNotBlank(lineItem) && LINEITEM.equals( localName ) ) {
				if( ri != null ) {
					ri.setType( "Return" );
					returnOrders.add( ri );
				}

				lineItem = "";
				lineItemId = "";
				customerRequestExchange = "";
				timestamp = "";
				quantity = "";
				shippingGroupId = "";
				customerReasonCode = "";
				dispositionCode = "";
				entryType = "";
				orderLineKey = "";
				ri = null;
			}

			if( RETURNORDER.equals( localName ) || PREVIOUSRETURN.equals( localName ) ) {
				returnOrder = "";
				returnOrderId = "";
				returnOrderIdValue = null;
			}

		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}

	private void populateValue() {
		try {
			String value = characterBuf.toString();

			if( DigitalStringUtil.isNotBlank(returnOrder) && DigitalStringUtil.isNotBlank(returnOrderId) ) {
				returnOrderIdValue = value;
				returnOrderId = "";
			}

			if( DigitalStringUtil.isNotBlank(returnOrder) && DigitalStringUtil.isNotBlank(lineItem) ) {
				if( DigitalStringUtil.isNotBlank(lineItemId) ) {
					ri.setLineItemId( value );
					lineItemId = "";
				}
				if( DigitalStringUtil.isNotBlank(timestamp) ) {
					ri.setTimestamp( format( value ) );
					timestamp = "";
				}
				if( DigitalStringUtil.isNotBlank(customerRequestExchange) ) {
					ri.setCustomerRequestExchange( value );
					customerRequestExchange = "";
				}
				if( DigitalStringUtil.isNotBlank(quantity) ) {
					ri.setQuantity( Integer.parseInt( value ) );
					quantity = "";
				}
				if( DigitalStringUtil.isNotBlank(shippingGroupId) ) {
					ri.setShippingGroupId( value );
					shippingGroupId = "";
				}
				if( DigitalStringUtil.isNotBlank(customerReasonCode) ) {
					ri.setCustomerReasonCode( value );
					customerReasonCode = "";
				}
				if( DigitalStringUtil.isNotBlank(dispositionCode) ) {
					ri.setDispositionCode( value );
					dispositionCode = "";
				}
				if( DigitalStringUtil.isNotBlank(entryType) ) {
					ri.setEntryType( value );
					entryType = "";
				}

				if( DigitalStringUtil.isNotBlank(orderLineKey) ) {
					ri.setOrderLineKey( value );
					orderLineKey = "";
				}
			}
		} catch( Exception e ) {
			errMsg = e.getMessage();
		}
	}
}
