
package com.digital.commerce.services.order.contactinfo;

import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.reward.RewardService;
import com.digital.commerce.integration.reward.bts.BtsRewardService;
import com.digital.commerce.integration.reward.domain.RewardServiceRequest;
import com.digital.commerce.integration.reward.domain.RewardServiceResponse;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.utils.DigitalServiceConstants;

import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sk402315
 *
 */
@Getter
@Setter
public class DigitalOrderContactInfoManager extends ApplicationLoggingImpl {
	
	private BtsRewardService rewardsService;
	private DigitalProfileTools digitalProfileTools;
	private DigitalServiceConstants digitalConstants;
	
	/**
	 * Method used to update opt-in flag for promotional emails
	 * 
	 * @param receiveEmailOffers
	 * @param digitalContactInfo
	 * @param profile
	 * @return true if rewards service is updated successfully else false
	 */
	public boolean updateEmailOptinOptout(boolean receiveEmailOffers, DigitalContactInfo digitalContactInfo, Profile profile) {
		if (getDigitalProfileTools().isLoggedIn(profile) || getDigitalProfileTools().isCookied(profile)) {
			if (isLoggingInfo()) {
				logInfo("Customer " + profile.getRepositoryId()
						+ " is either logged-in/cookied user, hence not doing anything.");
			}
			// return updateCustomer(receiveEmailOffers, dswContactInfo,
			// profile);
			return true;
		} else {
			return addOrUpdateEmailOffers(receiveEmailOffers, digitalContactInfo, profile);
		}
	}

	/**
	 * Method used to update opt-in flag for promotional emails
	 * 
	 * @param receiveEmailOffers
	 * @param digitalContactInfo
	 * @param profile
	 * @return
	 */
	public boolean updateCustomer(boolean receiveEmailOffers, DigitalContactInfo digitalContactInfo, Profile profile){
		
		RewardServiceRequest btsRewardServiceRequest = processRewardsRequest(getRewardsDetails(profile));
		btsRewardServiceRequest.getContact().setEmail(digitalContactInfo.getEmail());
		String reciveEmailflag = getDigitalConstants().getEmailUpdateNo();
		if(!receiveEmailOffers){
			reciveEmailflag =getDigitalConstants().getEmailUpdateYes();
		}
		btsRewardServiceRequest.getFlags().setNoEMail(reciveEmailflag);
		btsRewardServiceRequest.getFlags().setOptOutFashionEmail(reciveEmailflag);
		btsRewardServiceRequest.setEmailSource( getDigitalConstants().getRewardsEmailSource());	
		RewardServiceResponse btsRewardServiceResponse = new RewardServiceResponse();
		try {
			if(getRewardsService().isServiceEnabled() && getRewardsService().isServiceMethodEnabled(RewardService.ServiceMethod.UPDATE_EMAIL_SUBSCRIBER.getServiceMethodName())){
					btsRewardServiceResponse = getRewardsService().updateCustomer(btsRewardServiceRequest);
			}
		} catch (DigitalIntegrationException e) {
			btsRewardServiceResponse.setSuccess(false);
			if(isLoggingDebug() ) {
				logDebug( "Exception in updating rewards details for profile id :  " +profile.getRepositoryId() ,e );
			}
		}
	
		return btsRewardServiceResponse.isSuccess();
	}
		
	
	
	

	/**
	 * Method used to update opt-in flag for promotional emails
	 * 
	 * @param receiveEmailOffers
	 * @param digitalContactInfo
	 * @param profile
	 * @return
	 */
	public boolean addOrUpdateEmailOffers(boolean receiveEmailOffers, DigitalContactInfo digitalContactInfo, Profile profile){
		
		//get the rewards details using profile
		RewardServiceRequest btsRewardServiceRequest = new RewardServiceRequest(); 
		btsRewardServiceRequest.getContact().setEmail(digitalContactInfo.getEmail());
		String reciveEmailflag = getDigitalConstants().getEmailUpdateNo();
		if(!receiveEmailOffers){
			reciveEmailflag =getDigitalConstants().getEmailUpdateYes();
		}
		btsRewardServiceRequest.getFlags().setNoEMail(reciveEmailflag);
		btsRewardServiceRequest.getContact().setEmail(digitalContactInfo.getEmail());
		btsRewardServiceRequest.getFlags().setOptOutFashionEmail(reciveEmailflag);
		btsRewardServiceRequest.setEmailSource( getDigitalConstants().getRewardsEmailSource());
		btsRewardServiceRequest.setFrequencyFashionEmail(getDigitalConstants().getFashionFrequencyEmailFlag());
			
		RewardServiceResponse btsRewardServiceResponse = new RewardServiceResponse();
		try {
			if(getRewardsService().isServiceEnabled() && getRewardsService().isServiceMethodEnabled(RewardService.ServiceMethod.UPDATE_EMAIL_SUBSCRIBER.getServiceMethodName())){
					btsRewardServiceResponse = getRewardsService().updateEmailSubscriber(btsRewardServiceRequest);
			}
		} catch (DigitalIntegrationException e) {
			btsRewardServiceResponse.setSuccess(false);
			if(isLoggingDebug() ) {
				logDebug( "Exception in updating rewards details for profile id :  " +profile.getRepositoryId() ,e );
			}
		}
		return btsRewardServiceResponse.isSuccess();
		
	}
	
	/**
	 * Method returns rewards details for the logged in user
	 * @param profile
	 * @return btsRewardServiceResponse
	 */
	public RewardServiceResponse getRewardsDetails(Profile profile){
		
		RewardServiceRequest btsRewardServiceRequest = new RewardServiceRequest();
		btsRewardServiceRequest.getPerson().setProfileID(profile.getRepositoryId());

		RewardServiceResponse btsRewardServiceResponse = new RewardServiceResponse();
		try {
			btsRewardServiceResponse = getRewardsService().selectCustomerByProfileId(btsRewardServiceRequest);
		} catch (DigitalIntegrationException e) {
			if(isLoggingDebug() ) {
				logDebug( "Exception in retriveing rewards details for profile id :  " +profile.getRepositoryId(),e );
			}
		}
		return btsRewardServiceResponse;
	}
	
	/**
	 * This method is used to prepare rewards request from rewards system 
	 * 
	 * @param rewardsResponse
	 * @return btsRewardServiceRequest
	 */
	public RewardServiceRequest processRewardsRequest(RewardServiceResponse rewardsResponse){
		
		RewardServiceRequest btsRewardServiceRequest = new RewardServiceRequest();
		
		btsRewardServiceRequest.setAddress(rewardsResponse.getAddress());
		btsRewardServiceRequest.setContact(rewardsResponse.getContact());
		btsRewardServiceRequest.setFlags(rewardsResponse.getFlags());
		btsRewardServiceRequest.setPerson(rewardsResponse.getPerson());
		btsRewardServiceRequest.setFraudCode(rewardsResponse.getFraudCode());
		btsRewardServiceRequest.setFraudCodeGroup(rewardsResponse.getFraudCodeGroup());
		btsRewardServiceRequest.setFrequencyFashionEmail(rewardsResponse.getFrequencyFashionEmail());
		btsRewardServiceRequest.setPointsBanking(rewardsResponse.getPointsBanking());
		btsRewardServiceRequest.setPreferredStore(rewardsResponse.getPreferredStore());
		btsRewardServiceRequest.setSignUpDate(rewardsResponse.getSignUpDate());
		btsRewardServiceRequest.setSignUpStore(rewardsResponse.getSignUpStore());
		btsRewardServiceRequest.setFrequencyFashionEmail(rewardsResponse.getFrequencyFashionEmail());
		
		return btsRewardServiceRequest;
		
	}
}
