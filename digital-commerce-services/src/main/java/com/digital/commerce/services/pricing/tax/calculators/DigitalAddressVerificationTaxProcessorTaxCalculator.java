package com.digital.commerce.services.pricing.tax.calculators;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.pricing.DigitalTaxableItemImpl;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.RelationshipTypes;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.pricing.AddressVerificationTaxProcessorTaxCalculator;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.TaxPriceInfo;
import atg.core.util.Address;
import atg.payment.tax.ShippingDestination;
import atg.payment.tax.ShippingDestinationImpl;
import atg.payment.tax.TaxRequestInfoImpl;
import atg.payment.tax.TaxableItem;
import atg.payment.tax.TaxableItemImpl;
import atg.repository.RepositoryItem;

@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalAddressVerificationTaxProcessorTaxCalculator extends AddressVerificationTaxProcessorTaxCalculator{
	
	 private static final String CLASSNAME="DigitalAddressVerificationTaxProcessorTaxCalculator";
	 
	/**
	   * Sets all amount properties of the input TaxPriceInfo to zero
	   * @param pTaxPriceInfo the TaxPriceInfo to clear
	   */
	  private void clearTaxPriceInfo(TaxPriceInfo pPriceQuote) {
	    pPriceQuote.setAmount(0.0);
	    pPriceQuote.setCityTax(0.0);
	    pPriceQuote.setCountyTax(0.0);
	    pPriceQuote.setStateTax(0.0);
	    pPriceQuote.setDistrictTax(0.0);
	    pPriceQuote.setCountryTax(0.0);
	  }

	
	
	//-------------------------------------
	  /**
	   * TaxPricingCalculator implementation
	   */

	  //-------------------------------------
	  /**
	   * Tax an order within a context.  This implementation consults the configured
	   * TaxProcessor for the tax.  The steps taken here are:
	   * <ul>
	   *  <li>create and configure a TaxRequestInfo
	   *  <li>pass the TaxRequestInfo to the TaxProcessor
	   *  <li>interpret the TaxStatus on return
	   *  <li>configure the output TaxPriceInfo with information gleaned from the TaxStatus
	   * </ul>
	   *
	   * NOTE:
	   *  The algorithm used by this method to determine the tax of the order finds
	   *  an average price for each item and uses that price in determining the tax
	   *  on every unit of that item.  The reason for this is that there is currently no
	   *  way to specify which individual units of a CommerceItem belong in which shipping
	   *  relationship.  Additionally, units as small as one of a CommerceItem
	   *  can be priced differently from other units in the same CommerceItem.  These two
	   *  situations mean that when taxing a CommerceItem, there is no way to know
	   *  explicitly what the total price of a shipping relationship is.  Therefore, an
	   *  average price is computed for all items, and that price is multiplied by the
	   *  quantity of the CommerceItem that reside in the current relationship to find the
	   *  total price of the relationship.
	   *  <p>
	   *  Ideally, we should change a shipping relationship to include an exact specification
	   *  of which units of a CommerceItem belong to that relationship.
	   *
	   *
	   * @param pPriceQuote TaxPriceInfo representing the tax quote for the order
	   * @param pOrder The order to tax
	   * @param pPricingModels A Collection of RepositoryItems representing PricingModels
	   * @param pProfile The user's profile
	   * @param pExtraParameters A Map of extra parameters to be used in the pricing, may be null
	   */	  
	public void priceTax(TaxPriceInfo pPriceQuote, Order pOrder, RepositoryItem pPricingModel, Locale pLocale, RepositoryItem pProfile, Map pExtraParameters)
	    throws PricingException 
	  {
	    String METHOD_NAME = "priceTax";
	    try {
	    	DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
					CLASSNAME, METHOD_NAME);
	      // got to have an order
	      if (pOrder == null) {
	        throw new PricingException(MessageFormat.format(Constants.PARAMETER_NOT_SET,
	                                                        new Object[] {
	                                                          "pOrder",
	                                                          "priceTax"
	                                                        }));
	      }

	      if (pPriceQuote == null) {
	        throw new PricingException(MessageFormat.format(Constants.PARAMETER_NOT_SET,
	                                                        new Object[] {"pPriceQuote", "priceTax"}));
	      }

	      // make sure that there's a price and that it's not zero
	      if (pOrder.getPriceInfo() == null || pOrder.getPriceInfo().getAmount() == 0) {
	        // return no tax
	        clearTaxPriceInfo(pPriceQuote);
	        return;
	      }

	      // configure the TaxRequestInfo
	      TaxRequestInfoImpl tri = new TaxRequestInfoImpl();

	      // set the order id
	      tri.setOrderId(pOrder.getId());

	      // set the order
	      tri.setOrder(pOrder);
	      
	      /**
	       * calculate billing address for all shipping groups ahead of time.
	       * billing address may not be available.  Try to find the first PaymentGroup
	       * of the order that has an address associated with it and use that as this
	       * tax calculation's billing address.  If no payment group has an address,
	       * there's no billing address.
	       */
	      /*List paymentGroups = pOrder.getPaymentGroups();
	      if (paymentGroups != null) {
	        Iterator paymentIterator = paymentGroups.iterator();
	        while (paymentIterator.hasNext()) {
	          PaymentGroup pg = (PaymentGroup) paymentIterator.next();
	          try {
	            if (DynamicBeans.getBeanInfo(pg).hasProperty(getBillingAddressPropertyName())) {
	              Address billingAddress = (Address) DynamicBeans.getPropertyValue(pg, getBillingAddressPropertyName());
	              if (billingAddress != null) {
	                tri.setBillingAddress(billingAddress);
	                break;
	              }
	            }
	          }
	          catch (IntrospectionException e) {
	            // do nothing, just skip to the next one
	          }
	          catch (PropertyNotFoundException e) {
	            // won't happen because we check for it first
	          }
	        }
	      }*/

	      // if, after all that, we still don't have a billing address, bail

	      List shippingDestinations = new LinkedList();

	      List shippingGroups = pOrder.getShippingGroups();
	      Iterator groupIterator = shippingGroups.iterator();

	      // remember how many of each item has been taxed.  If we're about to tax the
	      // last of an item, add the remainder to it.
	      Map taxedItems = new HashMap();
	      // remember how much of each item is taxable.
	      Map taxedItemAmounts = new HashMap();

	      while (groupIterator.hasNext()) {
	        ShippingGroup group = (ShippingGroup) groupIterator.next();

	        ShippingDestinationImpl dest = new ShippingDestinationImpl();

	        // currency code comes from the order
	        dest.setCurrencyCode(pOrder.getPriceInfo().getCurrencyCode());
	        if( null != group.getPriceInfo()) {
	        	dest.setShippingAmount(group.getPriceInfo().getAmount());
	        } else {
	        	dest.setShippingAmount(0.0d);
	        }
	        Address shippingAddress = determineShippingAddress(group);
	        dest.setShippingAddress(shippingAddress);


	        // create a TaxableItem for all the items to be associated with this group
	        List taxableItems = new LinkedList();

	        // when creating taxableitems, if there are no relationships in this group,
	        // use the order's items to create the taxableitems
	        if (group.getCommerceItemRelationships() == null ||
	            group.getCommerceItemRelationships().isEmpty() ) {

	          List commerceItems = pOrder.getCommerceItems();
	          Iterator commerceItemIterator = commerceItems.iterator();
	          while (commerceItemIterator.hasNext()) {
	        	  
	        	//Fetched DigitalCommerceItem to obtain shipType and storeId
	        	DigitalCommerceItem item = (DigitalCommerceItem)commerceItemIterator.next();

	            //Referring to extended TaxableItemImpl class
	            DigitalTaxableItemImpl ti = new DigitalTaxableItemImpl();

	            // just in case, we should round the taxableItem's amount.
	            // this price should already be rounded
	            if (isLoggingDebug()) {
	              logDebug("rounding item: " + item.getId()+ " amount for taxing: " + item.getPriceInfo().getAmount());
	            }

	            //double roundedAmount = getPricingTools().round(item.getPriceInfo().getAmount());
	            // get the taxable amount instead of just the amount
	            double taxableAmount = getPricingTools().calculateTaxableAmount(item, pOrder, pLocale, 
	                                                                            pProfile, pExtraParameters);
	            double roundedAmount = getPricingTools().round(taxableAmount);
	            
	            if (isLoggingDebug()) {
	              logDebug("rounded item price for taxing to : " + roundedAmount);
	            }

	            ti.setAmount(roundedAmount);
	            ti.setCatalogRefId(item.getCatalogRefId());
	            ti.setProductId(item.getAuxiliaryData().getProductId());
	            
	            ti.setShipType(item.getShipType());
	            ti.setStoreId(item.getStoreId());

	            ti.setQuantity(item.getQuantity());

		    if (getTaxStatusProperty() != null) {
	              Object catalogRef = item.getAuxiliaryData().getCatalogRef();
		      if (catalogRef != null) {
	                try {
	                  Object taxStatus = DynamicBeans.getPropertyValue(catalogRef, getTaxStatusProperty());
			  
			  if (taxStatus instanceof String) {
			    if (isLoggingDebug())
	                      logDebug("Setting taxStatus to " + (String)taxStatus);
			    ti.setTaxStatus((String)taxStatus);
			  }
			  else {
			    if (isLoggingDebug())
			      logDebug("Setting taxStatus to " + String.valueOf(taxStatus));
			    ti.setTaxStatus(String.valueOf(taxStatus));
			  }
	                }
	                catch (PropertyNotFoundException exc) {
	                  if (isLoggingDebug())
	                    logDebug("Could not find taxStatus property for " + catalogRef, exc);
	                }
	              }
	            }

	            taxableItems.add(ti);
	            taxedItems.put(item, Long.valueOf(item.getQuantity()));
	            taxedItemAmounts.put(item, Double.valueOf(roundedAmount));
	          } // end for each item
	        } // end if no relationships

	        // else there is at least one relationship - create taxable items from relationships
	        else {

	          /**
	           * create a TaxableItem for each relationship.  Since no unit of an item can exist in
	           * two separate relationships, this should cover all items to be taxed.
	           */

	          Iterator relationshipIterator = group.getCommerceItemRelationships().iterator();
	          while (relationshipIterator.hasNext()) {
	            ShippingGroupCommerceItemRelationship rel = (ShippingGroupCommerceItemRelationship) relationshipIterator.next();
	            TaxableItemImpl ti;
	            boolean useAverage = false;
	            double totalTaxAmount = 0.0;

	            if((getPricingTools().isShippingSubtotalUsesAverageItemPrice()) ||
	               (rel.getRange() == null))
	              useAverage = true;

	            if(useAverage)
	              ti = (TaxableItemImpl) createTaxableItemForRelationshipByAverage(rel, tri);
	            else
	              ti = (TaxableItemImpl) createTaxableItemForRelationship(rel, tri,
	                                                                      pOrder, pLocale,
	                                                                      pProfile, pExtraParameters);

	            if (ti == null) {
	              // Nothing to do for the current relationship
	              continue;
	            }

	            long quantityTaxed = 0;
	            totalTaxAmount += ti.getAmount();

	            if(useAverage)
	              quantityTaxed = checkTaxableItemByAverage(ti, rel, taxedItems, taxedItemAmounts,
	                                                        pOrder, pLocale, pProfile,pExtraParameters);
	            else
	              quantityTaxed = checkTaxableItem(ti, rel, taxedItems, taxedItemAmounts, totalTaxAmount,
	                                               pOrder, pLocale, pProfile, pExtraParameters);

	            DigitalTaxableItemImpl dswTaxableItem = new DigitalTaxableItemImpl(ti);
	             dswTaxableItem.setShipType(((DigitalCommerceItem)rel.getCommerceItem()).getShipType());
	             dswTaxableItem.setStoreId(((DigitalCommerceItem)rel.getCommerceItem()).getStoreId());
	              
	            if(quantityTaxed > 0) {
	              taxableItems.add(dswTaxableItem);            
	              taxedItems.put(rel.getCommerceItem(), Long.valueOf(quantityTaxed));
	              Double currentTaxedAmount=(Double)taxedItemAmounts.get(rel.getCommerceItem());
	              double currentax=0.0;
	              if(currentTaxedAmount != null)
	                currentax= currentTaxedAmount.doubleValue();
	              taxedItemAmounts.put(rel.getCommerceItem(), Double.valueOf(currentax + ti.getAmount()));
	            }

	          } // end for each relationship

	        } // end if there are relationships

	        // the shipping group subtotal is the total amount of the taxable items (from the
	        // group subtotal price info)
	        double shippingGroupSubtotal = 0.0;

	        if (group.getId() == null) {
	          if (isLoggingDebug()) {
	            logDebug("DANGER: group ID is null");
	          }
	        } else {

	          OrderPriceInfo pi = (OrderPriceInfo) pOrder.getPriceInfo().getTaxableShippingItemsSubtotalPriceInfos().get(group.getId());

	          if (pi == null) {
	            if (isLoggingDebug()) {
	              logDebug("WARNING: no entry in order's group taxable subtotal map found for group ID: " + group.getId());
	            }
	          } else {
	            shippingGroupSubtotal = pi.getAmount();
	          }
	        } // end else group ID not null

	        if (isLoggingDebug()) {
	          logDebug("shipping group subtotal for taxing: " + shippingGroupSubtotal);
	        }

	        // set taxable item amount to be group's taxable subtotal
	        dest.setTaxableItemAmount(shippingGroupSubtotal);

	        // set the taxableitems
	        TaxableItem [] taxableItemArray = new TaxableItem [taxableItems.size()];
	        taxableItemArray = (TaxableItem []) taxableItems.toArray(taxableItemArray);
	        dest.setTaxableItems(taxableItemArray);

	        // if the destination's taxable amount is zero, don't even add it
	        if (dest.getTaxableItemAmount() > 0) {
	          shippingDestinations.add(dest);
	        }

	      } // end for each group

	      ShippingDestination [] shippingDestinationArray = new ShippingDestination[shippingDestinations.size()];
	      shippingDestinationArray = (ShippingDestination [])shippingDestinations.toArray(shippingDestinationArray);
	      tri.setShippingDestinations(shippingDestinationArray);

	      // if there are no destinations, don't even bother calculating tax
	      if (shippingDestinations.size() > 0) {

	        if (getCalculateTaxByShipping()) {
	          calculateTaxByShipping(tri, pPriceQuote, pOrder, pPricingModel, pLocale, pProfile, pExtraParameters);
	        } else {
	          calculateTax(tri, pPriceQuote, pOrder, pPricingModel, pLocale, pProfile, pExtraParameters);
	        }
	      }

	    }
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
					CLASSNAME, METHOD_NAME);	
	    }

	  } // end priceTax

		
 
	 /**
	   * This method verifies the amount for each relationship.  It assumes the amounts were set
	   * by average the unit price times the quantity.  It takes care of leftovers that this rounding
	   * may have missed.
	   *
	   * @param pTaxableItem The taxable item as returned by createTaxableItemForRelationship
	   * @param pRelationship The relationship that the taxable item was created from
	   * @param pTaxedItems This is a map from an item to the amount of the item that will be taxed
	   *                    This map is used to determine any leftovers remaining due to rounding
	   * @param pTaxedItemAmountss This is a map from an item to the amount of the item that will be taxable
	   *                    This map is used to determine any leftovers remaining due to rounding.  This
	   *                    parameter is included for consistency and is not used.  We use the average unit price
	   *                    times the quantity instead.
	   * @param pTaxableItems This is the list of taxable items.  Once all verification is finished, 
	   *                      pTaxableItem is added to this list
	   * @return the quantity of the relationship that will be taxed
	   **/
	  private long checkTaxableItemByAverage(TaxableItemImpl pTaxableItem, 
	                                         ShippingGroupCommerceItemRelationship pRelationship, 
	                                         Map pTaxedItems,
	                                         Map pTaxedItemAmounts,
	                                         Order pOrder,
	                                         Locale pLocale,
	                                         RepositoryItem pProfile,
	                                         Map pExtraParameters)
	    throws PricingException
	  {
	    if (pTaxableItem != null) {
	      CommerceItem item = pRelationship.getCommerceItem();

	      long relationshipQuantity = 0;
            switch (pRelationship.getRelationshipType()) {
                case RelationshipTypes.SHIPPINGQUANTITY:
                    relationshipQuantity = pRelationship.getQuantity();
                    break;
                case RelationshipTypes.SHIPPINGQUANTITYREMAINING:
                    relationshipQuantity = getOrderManager().getShippingGroupManager().getRemainingQuantityForShippingGroup(item);
                    break;
                default:
                    throw new PricingException(MessageFormat.format(Constants.BAD_SHIPPING_GROUP_TYPE,
                            new Object[]{
                                    Integer.valueOf(pRelationship.getRelationshipType())
                            }));
            }

	      // if there's no quantity in this rel, skip it
	      if (relationshipQuantity == 0) {
	        if (isLoggingDebug()) {
	          logDebug("no quantity of the item: " + item.getId() + " in this relationship to tax, continuing.");
	        }
	        return 0;
	      }

	      if (isLoggingDebug()) {
	        logDebug("rounding the price of " + relationshipQuantity + " units of the item: " + item.getId() + " : " + pTaxableItem.getAmount());
	      }

	      // round the taxable item's price.
	      // If these are the last units being priced, round and add the leftovers.
	      double roundedAmount = getPricingTools().roundDown(pTaxableItem.getAmount());

	      if (isLoggingDebug()) {
	        logDebug("rounded the price of " + relationshipQuantity + " units of the item to: " + roundedAmount);
	      }

	      pTaxableItem.setAmount(roundedAmount);

	      long quantityAlreadyTaxed = 0;
	      Long quantityTaxed = (Long) pTaxedItems.get(item);
	      if (quantityTaxed != null) {
	        quantityAlreadyTaxed = quantityTaxed.longValue();
	      }

	      if (isLoggingDebug()) {
	        logDebug("quantity of the item: " + item.getId() + " already taxed: " + quantityAlreadyTaxed);
	      }

	      if (isLoggingDebug()) {
	        logDebug("quantity in this relationship: " + relationshipQuantity);
	      }

	      if (isLoggingDebug()) {
	        logDebug("total quantity of the item: " + item.getQuantity());
	      }

	      // if we're about to tax the last of an item, add the leftovers to its amount
	      if (quantityAlreadyTaxed + relationshipQuantity == item.getQuantity()) {

	        if (isLoggingDebug()) {
	          logDebug("calculating leftovers from rounding unit prices down");
	        }

	        // the leftovers are the remainders from rounding every unit of the CommerceItem
	        double averageUnitPrice =
	          item.getPriceInfo().getAmount() / item.getQuantity();

	        // if we divided by zero... use zero instead
	        if(Double.isNaN(averageUnitPrice) || Double.isInfinite(averageUnitPrice)) {
	          if(isLoggingDebug()) {
	            logDebug(MessageFormat.format(Constants.QUOTIENT_IS_NAN, "checkTaxableItemByAverage",
	                Double.toString(item.getPriceInfo().getAmount()), 
	                Double.toString(item.getQuantity()) ));
	          }
	          averageUnitPrice = 0.0;
	        }

	        if (isLoggingDebug()) {
	          logDebug("average unit price: " + averageUnitPrice);
	        }

	        double roundedAveragePrice = getPricingTools().roundDown(averageUnitPrice);

	        if (isLoggingDebug()) {
	          logDebug("rounded average price: " + roundedAveragePrice);
	        }

	        if (isLoggingDebug()) {
	          logDebug("total item amount: " + item.getPriceInfo().getAmount());
	        }

	        if (isLoggingDebug()) {
	          logDebug("rounded unit price times quantity of: " + item.getQuantity() + " : " + ( roundedAveragePrice * item.getQuantity()));
	        }


	        double leftovers = item.getPriceInfo().getAmount() - (roundedAveragePrice * item.getQuantity());

	        if (isLoggingDebug()) {
	          logDebug("adding leftovers: " + leftovers);
	        }

	        double withLeftovers = pTaxableItem.getAmount() + leftovers;

	        if (isLoggingDebug()) {
	          logDebug("rounding taxable item amount with leftovers");
	        }

	        double rounded = getPricingTools().roundDown(withLeftovers);

	        if (isLoggingDebug()) {
	          logDebug("rounded with leftovers to: " + rounded);
	        }

	        pTaxableItem.setAmount(rounded);

	      } // end if we need to add leftovers

	      return quantityAlreadyTaxed + relationshipQuantity;
	    } // end if taxableItem is not null
	    else {
	      return 0;
	    }
	  }
 
	
	
	  /**
	   * This method verifies the amount for each relationship.  It assumes the amounts
	   * were set by summing the detailedItemPriceInfos.
	   *
	   * @param pTaxableItem The taxable item as returned by createTaxableItemForRelationship
	   * @param pRelationship The relationship that the taxable item was created from
	   * @param pTaxedItems This is a map from an item to the amount of the item that will be taxed
	   *                    This map is used to determine any leftovers remaining due to rounding
	   * @param pTaxedItemAmounts This is a map from an item to the amount of the item that will be taxable
	   *                    This map is used to determine any leftovers remaining due to rounding
	   * @param pTaxableItems This is the list of taxable items.  Once all verification is finished, 
	   *                      pTaxableItem is added to this list
	   * @param pTotalTaxAmount The total tax amount so far.  This is used for getting the leftovers
	   * @return the quantity of the relationship that will be taxed
	   **/
	  private long checkTaxableItem(TaxableItemImpl pTaxableItem, 
	                                ShippingGroupCommerceItemRelationship pRelationship, 
	                                Map pTaxedItems,
	                                Map pTaxedItemAmounts,
	                                double pTotalTaxAmount,
	                                Order pOrder,
	                                Locale pLocale,
	                                RepositoryItem pProfile,
	                                Map pExtraParameters)
	    throws PricingException
	  {
	    if (pTaxableItem != null) {
	    
	      // if this relationship doesn't have a range, go ahead and use the old method
	      // since we could't get a reliable subtotal for this relationship without it
	      if(pRelationship.getRange() == null)
	        return checkTaxableItemByAverage(pTaxableItem, pRelationship, pTaxedItems, pTaxedItemAmounts,
	                                         pOrder, pLocale, pProfile, pExtraParameters);
	      
	      CommerceItem item = pRelationship.getCommerceItem();

	      long relationshipQuantity = 0;
            switch (pRelationship.getRelationshipType()) {
                case RelationshipTypes.SHIPPINGQUANTITY:
                    relationshipQuantity = pRelationship.getQuantity();
                    break;
                case RelationshipTypes.SHIPPINGQUANTITYREMAINING:
                    relationshipQuantity = getOrderManager().getShippingGroupManager().getRemainingQuantityForShippingGroup(item);
                    break;
                default:
                    throw new PricingException(MessageFormat.format(Constants.BAD_SHIPPING_GROUP_TYPE,
                            new Object[]{
                                    Integer.valueOf(pRelationship.getRelationshipType())
                            }));
            }

	      // if there's no quantity in this rel, skip it
	      if (relationshipQuantity == 0) {
	        if (isLoggingDebug()) {
	          logDebug("no quantity of the item: " + item.getId() + " in this relationship to tax, continuing.");
	        }
	        return 0;
	      }

	      // when checking by average we reset the price to a rounded amount.  Now
	      // we just sum the details so this isn't necessary anymore.

	      long quantityAlreadyTaxed = 0;
	      Long quantityTaxed = (Long) pTaxedItems.get(item);
	      if (quantityTaxed != null) {
	        quantityAlreadyTaxed = quantityTaxed.longValue();
	      }

	      if (isLoggingDebug()) {
	        logDebug("quantity of the item: " + item.getId() + " already taxed: " + quantityAlreadyTaxed);
	        logDebug("quantity in this relationship: " + relationshipQuantity);
	        logDebug("total quantity of the item: " + item.getQuantity());
	      }

		double totalTax = getPricingTools().calculateTaxableAmount(item, pOrder, pLocale,
				pProfile, pExtraParameters);
	      // the average method had to worry about leftovers.  Now we just sum the details.
	      // if we're about to tax the last of an item, add the leftovers to its amount
	      if (quantityAlreadyTaxed + relationshipQuantity == item.getQuantity()) {
	        ItemPriceInfo info = item.getPriceInfo();

	        Double amountTaxableSoFar = (Double) pTaxedItemAmounts.get(item);
	        double taxableSoFar = 0.0;
	        if(amountTaxableSoFar != null)
	          taxableSoFar = amountTaxableSoFar.doubleValue();
	        double roundedTotal = getPricingTools().round(totalTax);

	        double leftovers = roundedTotal - pTotalTaxAmount - taxableSoFar;

	        if(isLoggingDebug())
	          logDebug("Calculate " + leftovers + " for the taxable amount.");

	        double withLeftovers = pTaxableItem.getAmount() + leftovers;

	        pTaxableItem.setAmount(getPricingTools().round(withLeftovers));

	        if (isLoggingDebug()) {
	          logDebug("calculating leftovers from rounding unit prices down");
	        }
	      } else {
			  pTaxableItem.setAmount(getPricingTools().round(totalTax));
		  }
	      return quantityAlreadyTaxed + relationshipQuantity;
	    } // end if taxableItem is not null
	    else {
	      return 0;
	    }
	  }
}