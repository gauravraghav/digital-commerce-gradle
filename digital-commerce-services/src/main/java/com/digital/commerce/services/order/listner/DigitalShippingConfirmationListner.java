package com.digital.commerce.services.order.listner;

import java.util.HashMap;
import java.util.List;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.integration.order.processor.domain.payload.ShipmentConfirmedPayload;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderlineAttributesDTO;

public class DigitalShippingConfirmationListner extends DigitalOrderStatusListener {
	protected static final String CLASSNAME = DigitalShippingConfirmationListner.class.getName();

	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

	@Override
	protected String getOrderNo(OrderStatusPayload payLoad) {
		if(payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null ){
			return payLoad.getOmniOrderUpdate().getOrder().getOrderNo();
		}
		return null;
	}

	@Override
	protected String getEmailTo(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((ShipmentConfirmedPayload)payLoad).getPersonInfoBillTo().getEmailId();
	}

	@Override
	protected List<OrderLine> getOrderLines(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((ShipmentConfirmedPayload)payLoad).getOrderLines();
	}

	@Override
	protected HashMap<String, Object> handleEmailNotification(
			OrderStatusPayload payLoad,
			DigitalOrderImpl order,
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {
		
		HashMap<String, Object> extraAttributes = new HashMap<>();
		
		ShipmentConfirmedPayload epayLoad = (ShipmentConfirmedPayload)payLoad;
	    
	    extraAttributes.put("pickupPerson",epayLoad.getPersonInfoShipTo().getFirstName()+
				" "+epayLoad.getPersonInfoShipTo().getLastName());

		String customerFirstName = epayLoad.getPersonInfoBillTo().getFirstName();

		if (DigitalStringUtil.isEmpty(customerFirstName)) {
			customerFirstName = epayLoad.getPersonInfoShipTo().getFirstName();
		}
		
	    extraAttributes.put("customerFirstName", customerFirstName);
	    
	    HashMap<String, Object> shippingAddress = new HashMap<>();
		// get Shipping Address Details
	    if(epayLoad.getOrderLines() != null && !epayLoad.getOrderLines().isEmpty()){
			shippingAddress.put("firstName", epayLoad.getOrderLines().get(0).getPersonInfoShipTo().getFirstName());
			shippingAddress.put("company", epayLoad.getOrderLines().get(0).getPersonInfoShipTo().getCompany());
			shippingAddress.put("addressLine1", epayLoad.getOrderLines().get(0).getPersonInfoShipTo().getAddressLine1());
			shippingAddress.put("addressLine2", epayLoad.getOrderLines().get(0).getPersonInfoShipTo().getAddressLine2());
			shippingAddress.put("city", epayLoad.getOrderLines().get(0).getPersonInfoShipTo().getCity());
			shippingAddress.put("state", epayLoad.getOrderLines().get(0).getPersonInfoShipTo().getState());
			shippingAddress.put("zipCode", epayLoad.getOrderLines().get(0).getPersonInfoShipTo().getZipCode());
			shippingAddress.put("country", epayLoad.getOrderLines().get(0).getPersonInfoShipTo().getCountry());
			shippingAddress.put("dayPhone", epayLoad.getOrderLines().get(0).getPersonInfoShipTo().getDayPhone());
	    }
		
		extraAttributes.put("shippingAddress", shippingAddress);
		
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.SHIP, itemsByFullfilmentType, extraAttributes);
		
		return extraAttributes;
	}
}
