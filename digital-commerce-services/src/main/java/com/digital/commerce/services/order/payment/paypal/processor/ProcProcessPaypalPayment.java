package com.digital.commerce.services.order.payment.paypal.processor;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.order.payment.paypal.PaypalPaymentInfo;

import atg.commerce.CommerceException;
import atg.commerce.payment.PaymentManager;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.commerce.payment.processor.ProcProcessPaymentGroup;
import atg.payment.PaymentStatus;
import lombok.Getter;
import lombok.Setter;

/**
 * Processor class to process the requests for authorization operation for
 * PayPal payment.
 * 
 */
@Getter
@Setter
public class ProcProcessPaypalPayment extends ProcProcessPaymentGroup {

	private PaypalPaymentProcessor paypalPaymentProcessor;
	private PaymentManager paymentManager;


	/**
	 * Authorizes the payment group. Calls underlying processor's authorize
	 * method.
	 * 
	 * @param pParams
	 * @return
	 * @throws CommerceException
	 */

	public PaymentStatus authorizePaymentGroup(
			PaymentManagerPipelineArgs pParams) throws CommerceException {
		final String METHOD_NAME = "authorizePaymentGroup";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			PaypalPaymentInfo ppi = null;
	
			try {
				ppi = (PaypalPaymentInfo) pParams.getPaymentInfo();
			} catch (ClassCastException cce) {
				if (isLoggingError()){
					logError("Expecting class of type PaypalPaymentInfo but got: " + pParams.getPaymentInfo().getClass().getName());
				}
				throw cce;
			}
			if (isLoggingDebug()){
				logDebug("paypalpayment info is :" + ppi.toString());
			}
			return getPaypalPaymentProcessor().authorize(ppi);
			
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}
	
	 /**
     * Credits Payment group.
     * NOTE: No requirement to do debit from storefront for DSW.
     * @param pParams
     * @return
     * @throws CommerceException
     */

    public PaymentStatus debitPaymentGroup(PaymentManagerPipelineArgs  pParams) throws CommerceException {
    	if (isLoggingInfo()){
			logInfo("No requirement to do debit from storefront for DSW");
		}
        return null;
    }

    /**
     * Credits Payment group.
     * NOTE: No requirement to do credit from storefront for DSW.
     * @param pParams
     * @return
     * @throws CommerceException
     */

    public PaymentStatus creditPaymentGroup(PaymentManagerPipelineArgs  pParams) throws CommerceException {
    	if (isLoggingInfo()){
			logInfo("No requirement to do credit from storefront for DSW");
		}
        return null;
    }

	/**
	 * Return the possible return values for this processor.
	 */

	public int[] getRetCodes() {
    return new int[]{ 0 };
	}
}
