package com.digital.commerce.services.pricing.reprice;


import lombok.Getter;
import lombok.Setter;

/* */
@Getter
@Setter
public class RepricingConstants {

	private String	shippingServiceName;
	private String	repricingUser;
	private String	repricingBasicUser;
	private String	repricingPremierUser;
	private String	repricingPriceList;
	private String	repricingSalePriceList;
	private String	defaultShipMethod;
	private String	repricingClubUser;
	private String	repricingGoldUser;
	private String	repricingEliteUser;
}
