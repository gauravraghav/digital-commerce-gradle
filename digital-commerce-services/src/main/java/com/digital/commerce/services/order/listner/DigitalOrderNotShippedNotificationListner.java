package com.digital.commerce.services.order.listner;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.PaymentMethod;
import com.digital.commerce.integration.order.processor.domain.payload.OrderConfirmedPayload;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderlineAttributesDTO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DigitalOrderNotShippedNotificationListner extends DigitalOrderStatusListener {
	protected static final String CLASSNAME = DigitalOrderNotShippedNotificationListner.class.getName();

	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected HashMap<String, Object> handleEmailNotification(OrderStatusPayload payLoad, DigitalOrderImpl order,
															  HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {
		HashMap<String, Object> extraAttributes = new HashMap<>();

		OrderConfirmedPayload orderConfirmedPayload = (OrderConfirmedPayload) payLoad;

		String customerFirstName = orderConfirmedPayload.getPersonInfoBillTo().getFirstName();

		if (DigitalStringUtil.isEmpty(customerFirstName)) {
			customerFirstName = orderConfirmedPayload.getPersonInfoShipTo().getFirstName();

		}

		extraAttributes.put("customerFirstName", customerFirstName);

		return extraAttributes;
	}

	@Override
	protected String getOrderNo(OrderStatusPayload payLoad) {
		if (payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null) {
			return payLoad.getOmniOrderUpdate().getOrder().getOrderNo();
		}
		return null;
	}

	@Override
	protected String getEmailTo(OrderStatusPayload payLoad) {

		return ((OrderConfirmedPayload) payLoad).getPersonInfoBillTo().getEmailId();
	}

	@Override
	protected List<OrderLine> getOrderLines(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((OrderConfirmedPayload) payLoad).getOrderLines();
	}

	/**
	 * @param payLoad
	 * @param params
	 */
	public void putOrderDetail(OrderStatusPayload payLoad, HashMap params) {
		super.putOrderDetail(payLoad, params);
		if (payLoad.getTotals() != null) {
			if (payLoad.getTotals().getHdrCharges() > 0.0) {
				params.put("shipping",
						(payLoad.getTotals().getHdrCharges() - payLoad.getTotals().getHdrDiscount()));
			} else {
				params.put("shipping", "0.0");
			}
		}
	}

}
