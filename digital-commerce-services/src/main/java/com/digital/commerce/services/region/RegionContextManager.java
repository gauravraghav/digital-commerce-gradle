package com.digital.commerce.services.region;

import atg.nucleus.GenericService;

public class RegionContextManager extends GenericService {
	private static ThreadLocal<String> region = new ThreadLocal<>();
	
	public void setCurrentRegion(String para){
		region.set(para);
	}
	
	public static String getCurrentRegion(){
		return (String)region.get();
	}

}
