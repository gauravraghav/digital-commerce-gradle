package com.digital.commerce.services.order.purchase;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import lombok.Getter;
import lombok.Setter;
import org.xml.sax.InputSource;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.digital.commerce.common.domain.Address;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.util.JAXBContextCache;
import com.digital.commerce.services.order.GiftCardDTO;
import com.digital.edt.reviews.updateshoppingbasket.Card;
import com.digital.edt.reviews.updateshoppingbasket.Transaction;
import com.digital.edt.reviews.updateshoppingbasket.WebCardCreationResponse;

/**
 * Adds gift cards from Arroweye, SVS to the shopping cart.
 */
@Getter
@Setter
public class GiftCardResponseDroplet extends DynamoServlet {
	protected static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	protected static final String	ASI_RESPONSE			= "com.digital.edt.reviews.updateshoppingbasket";
	protected static final String	ASI_CALL_BACK_PARAM		= "xml";
	
	private JAXBContextCache jaxbContextCache;
	
	protected String giftCardFreeShipMethodTxt;
	
	public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
		try{
			String encodedResponseXml = req.getParameter(ASI_CALL_BACK_PARAM);
			
			if (DigitalStringUtil.isNotBlank(encodedResponseXml)) {
				
					String responseXml = getDecodedString(encodedResponseXml);
					
					WebCardCreationResponse response = (WebCardCreationResponse) getUpdateShoppingBagXml(responseXml);
					List<GiftCardDTO> cardList = getGiftCardDTOList(response);

					if(!cardList.isEmpty()){
						req.setParameter("response",  cardList.toArray(new GiftCardDTO[cardList.size()]));
						req.setParameter("addItemCount",  cardList.size());
					}
					req.serviceLocalParameter(OUTPUT, req, res); 
			}
		}catch(Exception ex){
			if (isLoggingError()) {
				logError("Unable to update the order", ex);
			}
			req.setParameter( "errorMsg", ex.getMessage() );
			req.serviceLocalParameter( "error", req, res );
		}		
	}

	/** Returns object graph for updateShoppingBasketXml
	 * 
	 * @param updateShoppingBasketXml
	 * @return
	 * @throws DigitalAppException */
	public WebCardCreationResponse getUpdateShoppingBagXml( String updateShoppingBasketXml ) throws DigitalAppException {
		WebCardCreationResponse o = null;
		try {
			if( updateShoppingBasketXml != null ) {
				JAXBContext	context = jaxbContextCache.newJAXBContext(ASI_RESPONSE);
				Unmarshaller unMarshaller = context.createUnmarshaller();				
				StringReader reader = new StringReader( updateShoppingBasketXml );
				InputSource is = new InputSource( reader );
				o = (WebCardCreationResponse)unMarshaller.unmarshal( is );
			}
		} catch( JAXBException e ) {
			throw new DigitalAppException( e );
		}
		return o;
	}
	
	/**
	 * @param impl
	 * @return
	 */
	private List<GiftCardDTO> getGiftCardDTOList(WebCardCreationResponse impl) {
		if (isLoggingDebug()) {
			logDebug("[Start]");
		}
		List<GiftCardDTO> dtoListObj = new ArrayList<>();
		HashMap<String, Address> addressMap = new HashMap<>();
		if (impl != null) {
			Transaction txImpl = (Transaction) impl.getTransaction();
			List<Card> cards = txImpl.getCard();
			if (cards != null) {
				for (int i = 0; i < cards.size(); i++) {
					GiftCardDTO dto = new GiftCardDTO();
					updateDTOWithTxAttributes(txImpl, dto);
					Card aCard = cards.get(i);
					updateDTOWithCardAttributes(aCard, dto);
					dtoListObj.add(dto);
				}
			}
			// add addresses in map for associating them with gift card
			List<com.digital.edt.reviews.updateshoppingbasket.Address> addresses = (txImpl.getAddresses() != null && txImpl
					.getAddresses().getAddress() != null) ? txImpl
					.getAddresses().getAddress() : null;
			if (addresses != null) {
				for (int i = 0; i < addresses.size(); i++) {
					Address dto = new Address();
					com.digital.edt.reviews.updateshoppingbasket.Address address = addresses.get(i);
					updateDTOWithAddressAttributes(address, dto);
					addressMap.put(new String(address.getId()), dto);
				}
			}
			// associate address with card
			for (int i = 0; i < dtoListObj.size(); i++) {
				GiftCardDTO dto = dtoListObj.get(i);
				if(dto.getAddressId() != null){
					dto.setAddress(addressMap.get(dto.getAddressId().toString()));
				}
				
				if(dto.getReturnAddressId() != null){
					dto.setReturnAddress(addressMap.get(dto.getReturnAddressId().toString()));
				}
				if (isLoggingDebug()) {
					logDebug("[GiftCardDTO Object \n" + dto + "]");
					if (isLoggingDebug()) {
						logDebug("[GiftCardDTO Object \n" + dto + "]");
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("[End]");
		}
		return dtoListObj;
	}
	
	/**
	 * Updates transacation attributes on DTO
	 * 
	 * @param txImpl
	 * @param dto
	 */
	private void updateDTOWithTxAttributes(Transaction txImpl, GiftCardDTO dto) {
		if (isLoggingDebug()) {
			logDebug("[Start]");
		}
		dto.setDswAffiliateId(txImpl.getCLTAffiliateID().longValue());
		dto.setDswOrderNumber(txImpl.getClientOrderNumber());
		dto.setVendorOrderNumber(txImpl.getASIOrderNumber());
		dto.setVendorInvoiceAmount(txImpl.getASIInvoiceAmount().doubleValue());
		dto.setVendorCardPrice(txImpl.getASICardPrice().doubleValue());
		dto.setVendorPostage(txImpl.getASIPostage().doubleValue());
		if (isLoggingDebug()) {
			logDebug("[End]");
		}
	}

	/**
	 * Updates card attributes on DTO
	 * 
	 * @param card
	 * @param dto
	 */
	private void updateDTOWithCardAttributes(Card card, GiftCardDTO dto) {
		if (isLoggingDebug()) {
			logDebug("[Start]");
		}
		dto.setProductId(card.getProductId());
		dto.setCardPrice(card.getCardPrice().doubleValue());		
		dto.setDollarValue(card.getDollarValue().longValue());
		dto.setPostage(card.getPostage().doubleValue());
		dto.setBarCode(card.getBarCode());
		dto.setVendorId(card.getASIID());
		dto.setCardQty(card.getCardQty().longValue());
		dto.setAddressId(card.getAddressId().longValue());
		dto.setReturnAddressId(card.getReturnAddressId().longValue());
		if(DigitalStringUtil.isNotBlank(card.getShipMethod()) && 
				this.getGiftCardFreeShipMethodTxt().equalsIgnoreCase(card.getShipMethod())){
			dto.setPostage(0.00);
		}else{
			dto.setPostage(card.getPostage().doubleValue());
		}
		if (isLoggingDebug()) {
			logDebug("[End]");
		}
	}

	/**
	 * Update Address atrributes on dto
	 * 
	 * @param address
	 * @param dto
	 */
	private void updateDTOWithAddressAttributes(com.digital.edt.reviews.updateshoppingbasket.Address address, Address dto) {
		if (isLoggingDebug()) {
			logDebug("[Start]");
		}
		dto.setFirstName(address.getFirst());
		dto.setMiddleName(address.getMiddle());
		dto.setLastName(address.getLast());
		dto.setAddress1(address.getStreet1());
		dto.setAddress2(address.getStreet2());
		dto.setZip(address.getZip());
		dto.setCity(address.getCity());
		dto.setState(address.getState());
		dto.setCountryCode(address.getCountryCode().toString());
		dto.setPhoneNumber(address.getTelephone());
		dto.setEmail(address.getEmail());
		if (isLoggingDebug()) {
			logDebug("[End]");
		}
	}
	
	/** Returns URLDecoded string
	 * 
	 * @param param
	 * @return
	 * @throws GiftCardException */
	public static String getDecodedString( String param ) throws DigitalAppException {
		String decodedStr = "";
		try {
			decodedStr = URLDecoder.decode( param, "UTF-8" );
			return decodedStr;
		} catch( UnsupportedEncodingException e ) {
			throw new DigitalAppException( e );
		}
	}
}