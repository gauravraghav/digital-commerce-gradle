package com.digital.commerce.services.inventory;

import java.util.List;

import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.services.inventory.InventoryHelper;
import com.digital.commerce.common.util.DigitalPredicate;
import com.google.common.collect.Iterables;

import atg.commerce.inventory.InventoryException;
import atg.commerce.inventory.InventoryManager;
import atg.commerce.inventory.MissingInventoryItemException;
import atg.commerce.inventory.RepositoryInventoryManager;
import atg.commerce.order.CommerceItem;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InventoryTools extends GenericService implements InventoryHelper {
	private InventoryCheck		inventoryCheck;

	private RepositoryInventoryManager	inventoryManager;
	
	private DigitalBaseConstants digitalConstants;

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#getStockLevelInfinite()
	 */
	@Override
	public int getStockLevelInfinite() {
		return STOCK_LEVEL_INFINITE;
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isAvailable(atg.commerce.inventory.InventoryManager, java.lang.String)
	 */
	@Override
	public boolean isAvailable( InventoryManager inventoryManager, String sku ) {
		return isAvailable( getInventoryManager(), sku, 1 );
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isAnyAvailable(java.lang.String, long)
	 */
	@Override
	public boolean isAnyAvailable( String sku, long quantity ) {
		return isAvailable( getInventoryManager(), sku, quantity );
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isAnyAvailable(java.util.List)
	 */
	@Override
	public boolean isAnyAvailable( List<String> skus ) {
		boolean retVal = false;
		if( skus != null && !skus.isEmpty() ) {
			retVal = Iterables.any( skus, new DigitalPredicate<String>() {

				@Override
				public boolean apply( String input ) {
					return isAvailable( getInventoryManager(), input );
				}

			} );
		}
		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isAvailable(java.lang.String, long)
	 */
	@Override
	public boolean isAvailable( String sku, long quantity ) {
		return isAvailable( getInventoryManager(), sku, quantity );
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isAvailable(java.lang.String)
	 */
	@Override
	public boolean isAvailable( String sku ) {
		return isAvailable( getInventoryManager(), sku );
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isAvailable(atg.commerce.order.CommerceItem, long)
	 */
	@Override
	public boolean isAvailable( CommerceItem commerceItem, long quantity ) {
		return isAvailable( getInventoryManager(), commerceItem, quantity );
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isAvailable(atg.commerce.order.CommerceItem)
	 */
	@Override
	public boolean isAvailable( CommerceItem commerceItem ) {
		return isAvailable( getInventoryManager(), commerceItem, 1 );
	}

	private void logInventoryException( String skuId, InventoryException e ) {
		if( isLoggingDebug() ) {
			logDebug( "Could not find inventory record for SKU: " + skuId, e );
		} else {
			if( e instanceof MissingInventoryItemException ) {
				if( isLoggingInfo() ) {
					logInfo( "Could not find inventory record for SKU: " + skuId );
				}
			} else {
				if( isLoggingWarning() ) {
					logWarning( "Could not find inventory record for SKU: " + skuId );
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isAvailable(atg.commerce.inventory.InventoryManager, atg.commerce.order.CommerceItem, long)
	 */
	@Override
	public boolean isAvailable( InventoryManager inventoryManager, CommerceItem commerceItem, long quantity ) {
		boolean retVal = isUnaccountable( commerceItem );

		if( !retVal ) { 
			String skuId = ( (RepositoryItem)commerceItem.getAuxiliaryData().getCatalogRef() ).getRepositoryId();
			try {
				long stockLevel = inventoryManager.queryStockLevel( skuId );
				// if it's not in stock, check back order
				if(null!=digitalConstants && digitalConstants.isBackOrderEnabled()){
					retVal = stockLevel <= InventoryHelper.STOCK_LEVEL_INFINITE || stockLevel >= quantity || stockLevel + inventoryManager.queryBackorderLevel( skuId ) >= quantity;
				}else{
					retVal = stockLevel <= InventoryHelper.STOCK_LEVEL_INFINITE || stockLevel >= quantity;
				}
			} catch( InventoryException e ) {
				logInventoryException( skuId, e );
			}
		}

		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isAvailable(atg.commerce.inventory.InventoryManager, java.lang.String, long)
	 */
	@Override
	public boolean isAvailable( InventoryManager inventoryManager, String skuId, long quantity ) {
		boolean retVal = isUnaccountable( skuId );

		if( !retVal ) {
			try {
				long stockLevel = inventoryManager.queryStockLevel( skuId );
				// if it's not in stock, check back order
				if(digitalConstants.isBackOrderEnabled()){
					retVal = stockLevel <= InventoryHelper.STOCK_LEVEL_INFINITE || stockLevel >= quantity || stockLevel + inventoryManager.queryBackorderLevel( skuId ) >= quantity;
				}else{
					retVal = stockLevel <= InventoryHelper.STOCK_LEVEL_INFINITE || stockLevel >= quantity; 
				}
			} catch( InventoryException e ) {
				logInventoryException( skuId, e );
			}
		}

		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isAvailableWithCheckforExists(java.lang.String)
	 */
	@Override
	public int isAvailableWithCheckforExists(String skuId) {
		
		long quantity = 1;
		boolean retVal = isUnaccountable(skuId);

		if (!retVal && inventoryManager != null) {
			try {
				long stockLevel = inventoryManager.queryStockLevel(skuId);
				boolean backOrderEnabled=false;
				if(digitalConstants.isBackOrderEnabled()){
					backOrderEnabled=true;
				}
				// if it's not in stock, check back order
				if (stockLevel <= InventoryHelper.STOCK_LEVEL_INFINITE
						|| stockLevel >= quantity){
					return InventoryConstants.INSTOCK;
				}else if(backOrderEnabled && stockLevel+ inventoryManager.queryBackorderLevel(skuId) >= quantity ){
					return InventoryConstants.INSTOCK;
			    }
			} catch (InventoryException e) {
				logInventoryException(skuId, e);
				if (e instanceof MissingInventoryItemException) {
					return InventoryConstants.NO_INVENTORY_RECORD;
				}
			}
		}

		return InventoryConstants.OUTOFSTOCK;
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isUnaccountable(java.lang.String)
	 */
	@Override
	public boolean isUnaccountable( String skuId ) {
		return getInventoryCheck().isUnaccountable( skuId );
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isUnaccountable(atg.commerce.order.CommerceItem)
	 */
	@Override
	public boolean isUnaccountable( CommerceItem commerceItem ) {
		return getInventoryCheck().isUnaccountable( commerceItem );
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#getMaxRequestableInventory(java.lang.String, long)
	 */
	@Override
	public long getMaxRequestableInventory( String skuId, long quantity ) {
		return getMaxRequestableInventory( getInventoryManager(), skuId, quantity );
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#getMaxRequestableInventory(atg.commerce.inventory.InventoryManager, java.lang.String, long)
	 */
	@Override
	public long getMaxRequestableInventory( InventoryManager inventoryManager, String skuId, long quantity ) {
		long retVal = quantity;
		try {
			long inStockLevel = inventoryManager.queryStockLevel( skuId );
			if( inStockLevel > InventoryHelper.STOCK_LEVEL_INFINITE && inStockLevel < quantity ) {
				retVal = inStockLevel;
			}
		} catch( InventoryException e ) {
			// Swallow exception for giftCards.
		}
		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#getInventoryStatus(java.lang.String)
	 */
	@Override
	public int getInventoryStatus( String skuId ) {
		return getInventoryStatus( getInventoryManager(), skuId, 1 );
	}	


	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#getInventoryStatus(java.lang.String)
	 */
	@Override
	public int getInventoryStatus( String skuId, String pLocationId ) {
		return getInventoryStatus( getInventoryManager(), skuId, 1, pLocationId );
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#getInventoryStatus(java.lang.String, long)
	 */
	@Override
	public int getInventoryStatus( String skuId, long quantity ) {
		return getInventoryStatus( getInventoryManager(), skuId, quantity );
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#getInventoryStatus(atg.commerce.inventory.InventoryManager, java.lang.String, long)
	 */
	@Override
	public int getInventoryStatus( InventoryManager inventoryManager, String skuId, long quantity ) {
		int retVal = InventoryManager.AVAILABILITY_STATUS_OUT_OF_STOCK;
		try {
			final long inStockLevel = inventoryManager.queryStockLevel( skuId );
			if( inStockLevel <= InventoryHelper.STOCK_LEVEL_INFINITE || inStockLevel >= quantity ) {
				retVal = InventoryManager.AVAILABILITY_STATUS_IN_STOCK;
			} else if(digitalConstants.isBackOrderEnabled()){
				final long backorderLevel = inventoryManager.queryBackorderLevel( skuId );
				if( ( inStockLevel + backorderLevel ) >= quantity ) {
					retVal = InventoryManager.AVAILABILITY_STATUS_BACKORDERABLE;
				}
			}
		} catch( InventoryException ex ) {
			// it's not in the inventory records, might be a giftcard or
			// something - we'll consider it in stock
			retVal = InventoryManager.AVAILABILITY_STATUS_IN_STOCK;
		}
		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#getInventoryStatus(atg.commerce.inventory.RepositoryInventoryManager, java.lang.String, long, pLocationId)
	 */
	@Override
	public int getInventoryStatus( RepositoryInventoryManager inventoryManager, String skuId, long quantity, String pLocationId ) {
		int retVal = InventoryManager.AVAILABILITY_STATUS_OUT_OF_STOCK;
		try {
			final long inStockLevel = inventoryManager.queryStockLevel( skuId, pLocationId );
			if( inStockLevel <= InventoryHelper.STOCK_LEVEL_INFINITE || inStockLevel >= quantity ) {
				retVal = InventoryManager.AVAILABILITY_STATUS_IN_STOCK;
			} else if(digitalConstants.isBackOrderEnabled()){
				final long backorderLevel = inventoryManager.queryBackorderLevel( skuId, pLocationId );
				if( ( inStockLevel + backorderLevel ) >= quantity ) {
					retVal = InventoryManager.AVAILABILITY_STATUS_BACKORDERABLE;
				}
			}
		} catch( InventoryException ex ) {
			// it's not in the inventory records, might be a giftcard or
			// something - we'll consider it in stock
			retVal = InventoryManager.INVENTORY_STATUS_ITEM_NOT_FOUND;
		}
		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#guessQtyReserveAtStoreHub(java.lang.String, int)
	 */
	@Override
	public int guessQtyReserveAtStoreHub( String skuId, int quantity ) {
		int retVal = 0;
		int remainingFCStock = 0;
		int remainingStoreStock = 0;
		@SuppressWarnings("unused")
		int remainingDropShipStock = 0;
		DigitalInventoryManager im = (DigitalInventoryManager)getInventoryManager();
		int storeStock = im.findStockLevelForNode( skuId, "storeStockLevel" );
		int dropShipStock = im.findStockLevelForNode( skuId, "dropShipStockLevel" );
		int remainingStock = im.findStockLevelForNode( skuId, "stockLevel" );
		int nonFCStock = storeStock + dropShipStock;
		remainingFCStock = remainingStock - nonFCStock;
		if( remainingFCStock < 0 ) remainingFCStock = 0;
		remainingStoreStock = remainingStock - remainingFCStock - dropShipStock;
		if( remainingStoreStock < 0 ) remainingStoreStock = 0;
		if( remainingStock >= dropShipStock )
			remainingDropShipStock = dropShipStock;
		else
			remainingDropShipStock = remainingStock;

		if( storeStock > 0 && quantity > remainingFCStock && remainingStock > dropShipStock ) {
			retVal = quantity - remainingFCStock;
			if( retVal > remainingStoreStock ) retVal = remainingStoreStock;
		}

		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#guessQtyReserveAtDropship(java.lang.String, int)
	 */
	@Override
	public int guessQtyReserveAtDropship( String skuId, int quantity ) {
		int retVal = 0;
		int remainingFCStock = 0;
		int remainingStoreStock = 0;
		int remainingDropShipStock = 0;
		int remainingNonDropShipStock = 0;
		DigitalInventoryManager im = (DigitalInventoryManager)getInventoryManager();
		int storeStock = im.findStockLevelForNode( skuId, "storeStockLevel" );
		int dropShipStock = im.findStockLevelForNode( skuId, "dropShipStockLevel" );
		int remainingStock = im.findStockLevelForNode( skuId, "stockLevel" );
		int nonFCStock = storeStock + dropShipStock;

		remainingFCStock = remainingStock - nonFCStock;
		if( remainingFCStock < 0 ) remainingFCStock = 0;
		remainingStoreStock = remainingStock - remainingFCStock - dropShipStock;
		if( remainingStoreStock < 0 ) remainingStoreStock = 0;
		if( remainingStock >= dropShipStock )
			remainingDropShipStock = dropShipStock;
		else
			remainingDropShipStock = remainingStock;

		remainingNonDropShipStock = remainingFCStock + remainingStoreStock;

		if( dropShipStock > 0 && quantity > remainingNonDropShipStock ) {
			retVal = quantity - remainingNonDropShipStock;
			if( retVal > remainingDropShipStock ) retVal = remainingStoreStock;
		}

		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.digital.commerce.services.inventory.InventoryHelper#isInventoryRecordExists(java.lang.String)
	 */
	@Override
	public boolean isInventoryRecordExists( String skuId ) {
		return ( (DigitalInventoryManager)getInventoryManager() ).inventoryRecordExists( skuId );
	}
}
