package com.digital.commerce.services.catalog;

import atg.adapter.gsa.ChangeAwareList;
import atg.commerce.endeca.cache.DimensionValueCache;
import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.commerce.pricing.PricingTools;
import atg.core.util.StringList;
import atg.json.JSONException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.gifts.DigitalGiftlistManager;
import com.digital.commerce.services.order.payment.afterpay.AfterPayCheckoutManager;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.ArrayUtils;

/** @author PR412029 */
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalCatalogTools {

	private static final String COLOR_CODE = "colorCode";
	private static final String COLOR = "color";
	private static final String SKU_STOCK_LEVEL = "skuStockLevel";
	private static final String DEFAULT_COLOR_CODE = "defaultColorCode";
	protected static final String CHILD_SKUS = "childSKUs";
	private static final String HEADER_PROD_INVENTORY = "X-Cacheble-Product-Inventory";
	private DimensionValueCacheTools	dimensionValueCacheTools;
	private Map<String, StringList>		wishlistPage;
	private Map<String, StringList>		pdpPage;
	private StringList					priceInCartPrices;
	private static final String			ENDECA_BROWSE_URL			= "/browse";
	private 	final DigitalLogger		logger						= DigitalLogger.getLogger( getClass().getName() );
	protected static final String			PRODUCT_ITEM_DESCRIPTOR		= "product";
	protected static final String			PRODUCT_ATTR_PRICEINCART	= "priceInCart";
	private DigitalServiceConstants			dswConstants;
	private static final String			EMPTY_SPACE					= "";
	private DigitalGiftlistManager giftListManager;
	private AfterPayCheckoutManager afterPayCheckoutManager;

	public void setWishlistPage( Map<String, StringList> pWishlistPage ) {

		if( pWishlistPage == null ) {
			this.wishlistPage = null;
		} else {
			Map<String, StringList> mapNew = new HashMap<>();
			for( Map.Entry entryCur : pWishlistPage.entrySet() ) {
				mapNew.put( entryCur.getKey().toString(), new StringList( entryCur.getValue().toString().split( "," ) ) );
			}
			this.wishlistPage = mapNew;
		}

	}

	public Map<String, StringList> getPdpPage() {
		return this.wishlistPage;
	}

	public void setPdpPage( Map<String, StringList> pPdpPage ) {
		if( pPdpPage == null ) {
			this.pdpPage = null;
		} else {
			Map<String, StringList> mapNew = new HashMap<>();
			for( Map.Entry entryCur : pPdpPage.entrySet() ) {
				mapNew.put( entryCur.getKey().toString(), new StringList( entryCur.getValue().toString().split( "," ) ) );
			}
			this.pdpPage = mapNew;
		}

	}

	public DigitalCatalogTools() {

	}

	/** @param dimensionName - Dimension Name in Endeca like brand, product.category etc.,
	 * @param dimensionValueName - Dimension Value Name in Endeca like Nike, Converse etc.,
	 * @return Navigation URL for the current Dimension Value like /N-1z1gfs/ */
	public String getURLforEndecaDimensionValue( String dimensionName, String dimensionValueName ) {

		final String METHOD_NAME = "getURLforEndecaDimensionValue";

		try {

			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );

			// Get DimensionValueCache
			DimensionValueCache dimValCache = getDimensionValueCacheTools().getCache();

			// if Cache is not empty then proceed. Otherwise, by default we will return NULL
			if( dimValCache.getNumCacheEntries() > 0 ) {

				// Attempt to get the Dimension Value Cache object from DimensionValueCache for the given Dimension name and Dimension Value
				// name
				// Logic to add these to cache is in DigitalDimensionValueCacheRefreshHandler
				DimensionValueCacheObject dimValCacheObject = dimValCache.getCachedObjectForDimval( dimensionName + "-" + dimensionValueName );

				// Let's make sure we have this Dim Val in Cache. If nont, by default we are returning NULL
				if( dimValCacheObject != null ) {
					String navURL = dimValCacheObject.getUrl();
					// Return the Navigation URL for this Dimension Value
					if( !DigitalStringUtil.isEmpty( navURL ) ) {
						if( navURL.startsWith( ENDECA_BROWSE_URL ) ) { return navURL.substring( ENDECA_BROWSE_URL.length() ); }
					}
				}

			} else {
				// getDimensionValueCacheTools().getCache() will automatically fill the Cache if empty.
				// If still, Cache count is 0 then something is wrong. Raise an exception.
				logger.error( "DimensionValueCache is empty even after force refreshing the cache. May be MDEX engine is down. Hide Filters and Clear all filters won't function properly" );
				throw new Exception( "DimensionValueCache is empty while trying to lookup the Dimension Value URL in DigitalProductTools" );
			}

		} catch( Exception ex ) {
			logger.error( "Exception thrown in DigitalProductTools.getURLforEndecaDimensionValue", ex );
			return null;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}
		return null;
	}

	/** This code is copied from CurrencyCodeDroplet.service() method
	 * 
	 * @param locale - Locale object from /atg/userprofiling/Profile.priceList.locale
	 * @return A list with 2 Strings in it. First one is Currency code and second one is Currency Symbol */
	public String getCurrencyCode( Object locale ) {

		String currencyCode = null;
		final String METHOD_NAME = "getCurrencyCode";

		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );

			if( locale != null ) {

				if( locale instanceof Locale ) {
					currencyCode = PricingTools.getInternationalCurrencySymbol((Locale)locale);
				} else {
					currencyCode = PricingTools.getInternationalCurrencySymbol( (String)locale );
				}

				if( logger.isDebugEnabled() ) {
					logger.debug( " Currency Code is " + currencyCode + " for locale " + locale );
				}

				/* -----------Currency Symbol is not used---------------
				 * if( locale instanceof Locale ) {
				 * currencySymbol = PricingTools.getCurrencySymbol( (Locale)(Locale)locale );
				 * } else {
				 * currencySymbol = PricingTools.getCurrencySymbol( (String)(String)locale );
				 * }
				 * if( isLoggingDebug() ) {
				 * logDebug( " Currency Symbol is " + currencySymbol + " for locale " + locale );
				 * } */

				return currencyCode;

			} else {
				logger.error( "Locale passed is NULL" );
				return null;
			}

		} catch( Exception ex ) {
			logger.error( "Exception thrown in getCurrencyCode() method", ex );
			return null;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}

	}

	/** @param product - Product repository item from ProductLookupDroplet
	 * @param locale - Locale object
	 * @param isAnanymousUser - Boolean which tells if the current user is anonymous or not
	 * @return - Map of Maps with what needs to be sent to the UI 
	 * @throws Exception */
	public Map<String, Object> transformOutputforPDPpage( RepositoryItem product, Object locale, Boolean isAnanymousUser, String giftlistID ) throws Exception {

		final String METHOD_NAME = "transformOutputforPDPpage";

		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );

			// Find Currency Code
			String currencyCode = getCurrencyCode( locale );

			// Create Product map with all required attributes
			Map<String, Object> productSKUMap = new HashMap<>();
			productSKUMap = BuildRepositoryMap( product, this.pdpPage );

			// add Currency Code to the ProductSKUMap
			productSKUMap.put( "currencyCode", currencyCode );

			// add Endeca Brand Name CTA to the dswBrand Map
			if( productSKUMap.containsKey( "dswBrand" ) && productSKUMap.get( "dswBrand" ) instanceof Map<?, ?> ) {
				Map<String, Object> dswBrand = (Map<String, Object>)productSKUMap.get( "dswBrand" );

				if( dswBrand.containsKey( "displayNameDefault" ) ) {
					String navStringURL = getURLforEndecaDimensionValue( "brand", dswBrand.get( "displayNameDefault" ).toString() );
					if( navStringURL != null ) dswBrand.put( "navStringURL", navStringURL );
				}
			}
			
			// Calculate Product Stock Level from Sku Stock Level
			// Also, Calculate the SKU with lowest Inventory and add that to response headers for Akamai caching 
			Long productStockLevel = 0L;
			Long lowestInventory = 0L;
			if (productSKUMap.containsKey("childSKUs") && productSKUMap.get("childSKUs") instanceof ArrayList<?>) {
				
				List<Object> childSKUs = new ArrayList<>();
				childSKUs = (List<Object>)productSKUMap.get("childSKUs");
				if (childSKUs.size() > 0 && childSKUs.get(0) instanceof Map<?,?>) {					
					for (Object objsku : childSKUs) {
						Long skuStockLevel, oldSkuStockLevel = 86400L;						
						Map<String, Object> sku = (Map<String, Object>) objsku;
						if (sku.get("skuStockLevel") instanceof Long) {
							skuStockLevel = Long.parseLong(String.valueOf(sku.get("skuStockLevel"))) ;
							lowestInventory = (skuStockLevel > 0 && skuStockLevel < oldSkuStockLevel) ? skuStockLevel : lowestInventory;
							oldSkuStockLevel = skuStockLevel;
							productStockLevel = productStockLevel + skuStockLevel;						
						}
					}
											
				}
			}
			productSKUMap.put("productStockLevel", productStockLevel); //Sum of all SkuStockLevel
			
			/*if (productStockLevel > 0) //Product in stock 
				ServletUtil.getCurrentResponse().setHeader(HEADER_PROD_INVENTORY, lowestInventory.toString());
			else //Product out of stock
				ServletUtil.getCurrentResponse().setHeader(HEADER_PROD_INVENTORY, "0"); */

			// Process bullets
			if( productSKUMap.containsKey( "bullets" ) && productSKUMap.get( "bullets" ) instanceof String ) {
				String[] list = productSKUMap.get( "bullets" ).toString().split( getDswConstants().getListItemEndTag() );
				List<String> arrList = new ArrayList<>();
				for( String lists : list ) {
					String bullList = lists.replace( getDswConstants().getListItemTag(), EMPTY_SPACE );
					// get rid of the <ul> tags if any in the string
					bullList = bullList.replace( getDswConstants().getUlStartTag(), EMPTY_SPACE );
					// get rid of the </ul> tags if any in the string
					bullList = bullList.replace( getDswConstants().getUlEndTag(), EMPTY_SPACE );
					// finally get rid of any html special characters if there are any in the string
					bullList = bullList.replaceAll( "\\<(/?[^\\>]+)\\>", EMPTY_SPACE );
					// Trim empty spaces before we add it to the collection.
					arrList.add( bullList.replace( getDswConstants().getListItemEndTag(), EMPTY_SPACE ).trim() );
				}
				productSKUMap.put( "bullets", arrList );
			}

			// Check if the current Product is in user's favorite list. Don't do this for anonymous user
			if( !isAnanymousUser && DigitalStringUtil.isNotBlank(giftlistID) ) { // This will be NULL if user didn't login or cookied
				Boolean isProductInFavorites = false;
				Object pID = product.getPropertyValue("ID");
				if (pID != null) {					
					isProductInFavorites = giftListManager.isProductinWishlist(giftlistID, pID.toString());
				}				
				
				productSKUMap.put( "isProductInFavorites", isProductInFavorites );
			}

			double afterPayInstallmentPrice = 0.0d;
			boolean priceInCart = false;

			if (productSKUMap.get("priceInCart") != null && productSKUMap
					.get("priceInCart") instanceof Boolean) {
				priceInCart = (Boolean) productSKUMap.get("priceInCart");
			}

			if (!priceInCart) {
				double productPrice = 0d;
				Object nonMembMinPrice = productSKUMap.get("nonMemberMinPrice");
				if (productSKUMap.containsKey("defaultSKU") && productSKUMap
						.get("defaultSKU") instanceof Map) {
					Map<String, Object> defaultSKU = (Map<String, Object>) (productSKUMap.get("defaultSKU"));
					if (defaultSKU != null && defaultSKU.get("skuStockLevel") != null && defaultSKU
							.get("skuStockLevel") instanceof Long) {
						long skuStockLevel = (Long) defaultSKU.get("skuStockLevel");
						Object nonMembPrice = defaultSKU.get("nonMemberPrice");
						if (null != nonMembPrice && null != nonMembMinPrice) {
							productPrice = (skuStockLevel > 0) ? (Double) nonMembPrice : (Double) nonMembMinPrice;
						} else if (null != nonMembMinPrice) {
							productPrice = (Double) nonMembMinPrice;
						}
					}
				} else {
					if (null != nonMembMinPrice) {
						productPrice = (Double) nonMembMinPrice;
					}
				}
				afterPayInstallmentPrice = afterPayCheckoutManager
						.getAmountOwedWithinLimits(productPrice);
			}

			productSKUMap.put("afterPayInstallmentPrice", afterPayInstallmentPrice);

			return productSKUMap;

		} catch( Exception ex ) {
			logger.error( "Exception thrown in transformOutputforPDPpage() method", ex );
			throw ex;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}
	}

	/** Returns the Map of Maps required to send to Wish List Page
	 * 
	 * @param product - Product repository item from ProductLookup
	 * @param wishItem - Wish List Repository Item. We will call this method for every WishList item
	 * @param locale - Locale object to calculate Currency code (multi-site support)
	 * @return Returns Map of Maps which will then be converted to JSON and sent to UI. 
	 * @throws Exception */
	public Map<String, Object> transformOutputforWishListpage( RepositoryItem product, RepositoryItem wishItem, Object locale, Integer version ) throws Exception {

		final String METHOD_NAME = "transformOutputforWishListpage";
		Map<String, Object> output = new HashMap<>();
		String skuID = null;
		String quantityDesired = "0";
		String wishListID = "";		

		// Setup fields at Wishlist Level
		Object wishItemCatRefIdObj = wishItem.getPropertyValue( "catalogRefId" );
		if( wishItemCatRefIdObj != null ) {
			skuID = wishItemCatRefIdObj.toString();
		}
		Object quantityDesiredObj = wishItem.getPropertyValue( "quantityDesired" );
		if( quantityDesiredObj != null ) { 
			quantityDesired = quantityDesiredObj.toString();
		}
		Object wishItemIdObj = wishItem.getPropertyValue( "id" );
		if( wishItemIdObj != null ) {
			wishListID = wishItemIdObj.toString();
		}
		
		// long startTime = System.currentTimeMillis();

		// Find Currency Code
		String currencyCode = getCurrencyCode( locale );

		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );

			if( !DigitalStringUtil.isEmpty( skuID ) ) { // this is a SKU
				// Loop through all childSKUs and process only that Child SKU that is matching with the WishList SKU
				// Check for Product isActive is not required as we are using DSW_WISHLIST_ACTIVE_ITEMS_V VIEW to remove Inactive products now - KTLO1-1116-2 branch
				Object childSkusObj = product.getPropertyValue( CHILD_SKUS );
				if( childSkusObj != null && childSkusObj instanceof List<?> ) {
					List<RepositoryItem> childSKUItems = (List<RepositoryItem>)childSkusObj;
					for( RepositoryItem skuItem : childSKUItems ) {
						if( skuItem.getPropertyValue( "Id" ).toString().equals( skuID ) ) { // Is this the SKU that user have in wishlist?
							// Create SKU map first for the selected SKU
							Map<String, Object> skuMap = new HashMap<>();
							skuMap = BuildRepositoryMap( skuItem, this.wishlistPage );
							// Create Product map now with Product level attributes
							Map<String, Object> productSKUMap = new HashMap<>();
							productSKUMap = BuildRepositoryMap( product, this.wishlistPage );
							// add Quantity to the ProductSKUMap
							productSKUMap.put( "Quantity", quantityDesired );
							// add Currency Code to the ProductSKUMap
							productSKUMap.put( "currencyCode", currencyCode );
							// Put skuMap inside productMap
							productSKUMap.put( "sku", skuMap );
							// Now, create the final output
							output.put( "sku", productSKUMap );
						}
					}
				}
			} else { // this is a product, No need to check for isActive for this Product as we are using DSW_WISHLIST_ACTIVE_ITEMS_V VIEW which already excludes Inactive products
				// This innerOutput hashmap is required as JSON schema requires product node inside product node
				Map<String, Object> innerOutput = new HashMap<>();
				// Create Product map now with Product level attributes
				Map<String, Object> productSKUMap = new HashMap<>();

				// Here, childSKUs and defaultSKU are sent as additional attributes to include in the result for V2. For V3, we won't be sending any Child SKUs
				if (version < 3) {
					productSKUMap = BuildRepositoryMap( product, this.wishlistPage, CHILD_SKUS, "defaultSKU");
					//add selectedColorCode to the productSkuMap only for V2
					productSKUMap.put("selectedColorCode", getDisplayedColorCode(product));
				} else {
					//added FaceoutColor and Color Swatches at Product level for V3
					productSKUMap = BuildRepositoryMap( product, this.wishlistPage, "defaultSKU", "colorCodes", "faceoutNonClearance");
					//Calculate Product level Inventory from SKU level Inventory. But, skip querying skuStockLevel if we got Inventory greater than 5
					//This avoids unncessary skuStockLevel query to Database. Anyway, UI will show "Add to Bag" button if InventoryCount > 0
					//If InventoryCount <=5 then they will show "Only 4 left" etc.,
					Long InventoryCount = 0L;
					Object childSKUs = product.getPropertyValue( CHILD_SKUS );
					if( childSKUs != null && childSKUs instanceof List<?> ) {
						List<RepositoryItem> childSKUItems = (List<RepositoryItem>)childSKUs;
						long invThreshold = 5L;
						if (getDswConstants().getStockLevelThreshold() != 0L)
							invThreshold = getDswConstants().getStockLevelThreshold();
						for( RepositoryItem skuItem : childSKUItems ) {
							Object skuStockLevel = skuItem.getPropertyValue( "skuStockLevel" );
							if (skuStockLevel != null && skuStockLevel instanceof Long)
								InventoryCount = InventoryCount + Long.parseLong(skuStockLevel.toString()); 
							if (InventoryCount > invThreshold) { 
								//This may look stupid, but I don't care as long as we have Inventory more than 5. 
								//Then, send some big number to UI to avoid querying each and every SKU. Querying skuStockLevel is very expensive. Nobody cares about exact Inventory count
								InventoryCount = 9999L;
								break;
							}
								
						}
					}
					//Let's add InventoryCount now
					productSKUMap.put("InventoryCount", InventoryCount);
					
				}
				
				// add Quantity to the ProductSKUMap
				productSKUMap.put( "Quantity", quantityDesired );
				// add Currency Code to the ProductSKUMap
				productSKUMap.put( "currencyCode", currencyCode );
				innerOutput.put( "product", productSKUMap );
				// now, put innerOutput inside the output hashmap
				output.put( "product", innerOutput );
			}

			// output will be empty if Product-SKU mapping is wrong in Wishlist table or the product is not Active
			if( output == null || output.isEmpty() ) {
				logger.warn( "Product-SKU mapping is wrong for WishListID " + wishListID );
				return null;
			}

			return output;

		} catch( Exception ex ) {
			logger.error( "Exception thrown in transformOutputforWishListpage() method", ex );
			throw ex;
		} finally {
			// long stopTime = System.currentTimeMillis();
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
			// logger.info( "Time taken for " + (String)product.getPropertyValue( "id" ) + " - " + ( stopTime - startTime ) + "ms" );
		}
	}

	/** Check if the given wishlist item is valid or not. Ensure product is Active and ensure Product-SKU mapping is correct.
	 * If both condition satisfies then increment the given wishlist Count. Otherwise, return the same wishCount
	 * This method is used in the FOR LOOP inside giftlistLookupActor (getWishListCount) *
	 * ---------------This method is No longer used -----------------------------------------------------------------
	 * @param product -- Underlying product in the wishlist
	 * @param wishItem -- Current Wishlist Item
	 * @param wishCount -- Current Wishlist count
	 * @return Final wishlist count after validating the given Wishlist item */
	public Integer isValidWishListItem( RepositoryItem product, RepositoryItem wishItem, Integer wishCount ) {

		final String METHOD_NAME = "getWishListCount";
		Boolean isActive = false;
		String skuID = null;

		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
			Object productIsActiveObj = product.getPropertyValue( "isActive" );
			if( productIsActiveObj != null )  {
				isActive = (Boolean)productIsActiveObj;
			}
			Object wishItemCatRefIdObj = wishItem.getPropertyValue( "catalogRefId" );
			if( wishItemCatRefIdObj != null ) { 
				skuID = wishItemCatRefIdObj.toString();
			}

			// If the product in Wishlist is not active then don't increment the counter
			if( isActive ) {

				// Now, check if the Product-SKU mapping is correct or not
				if( !DigitalStringUtil.isEmpty( skuID ) ) { // We need to process only the SKU
					// Loop through all childSKUs and look for only that Child SKU that is matching with the WishList SKU
					Object productChildSkusObj = product.getPropertyValue( CHILD_SKUS );
					if( productChildSkusObj != null && productChildSkusObj instanceof List<?> ) {
						List<RepositoryItem> childSKUItems = (List<RepositoryItem>)productChildSkusObj;
						for( RepositoryItem skuItem : childSKUItems ) {
							if( skuItem.getPropertyValue( "Id" ).toString().equals( skuID ) ) { // Is this the SKU that user have in
																								// wishlist?
								return wishCount + 1; // Increment WishList count and return
							}
						}
					}
				} else { // Product is in wishlist and it is active
					return wishCount + 1;
				}

			}

			return wishCount;

		} catch( Exception ex ) {
			logger.error( "Exception thrown in getWishListCount() method", ex );
			return wishCount;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}

	}

	/** This is a Private method used by many other methods in this class. This method gets a Repository item (any Repository item) and
	 * converts it to Map of Maps. It has recursive calls to handle Repository Item inside Repository Item. It also accepts a filter
	 * variable and sends only those filtered properties in the HashMap
	 * 
	 * @param repItem - Any Repository Item. It can be product or sku or material or anything
	 * @param filter - Filter variable which lists out only required properties from each Item descriptor. Please note that this filter is
	 *            for each Item descriptor and not at Repository level.
	 * @param addAttributes - Add any new attribute that are missing in the filter. Useful in WishList page where childSKU should not be
	 *            sent for products in wishlist (NOTE: This accepts only new attributes for Product item descriptor)
	 * @return - Returns Map of Maps with required properties
	 * @throws JSONException
	 * @throws RepositoryException */
	public Map<String, Object> BuildRepositoryMap( RepositoryItem repItem, Map<String, StringList> filter, String... addAttributes ) throws JSONException, RepositoryException, Exception {

		Map<String, Object> output = new HashMap<>();

		String itemDescriptor = repItem.getItemDescriptor().getItemDescriptorName();
		Boolean isPriceinCart = false;
		
		// Ugly check to ensure this Item descriptor's parent is "product" (shoe, handbag etc.,)
		if( repItem.getItemDescriptor().hasProperty( CHILD_SKUS ) ) {
			itemDescriptor = PRODUCT_ITEM_DESCRIPTOR;
			// Determine if this product has "priceInCart" enabled
			Object priceinCartValue = repItem.getPropertyValue( PRODUCT_ATTR_PRICEINCART );
			if( priceinCartValue != null && (Boolean)priceinCartValue ) {
				isPriceinCart = true;
			}
		} else if( addAttributes != null && addAttributes.length == 1 && addAttributes[0]
				.equals( PRODUCT_ATTR_PRICEINCART + ":" + "true" ) ) {
			// set priceIncart if this condition satisifies
			isPriceinCart = true;
			Arrays.fill( addAttributes, "" ); // empty this array now
		}

		if( filter != null && filter.containsKey( itemDescriptor ) ) {
			// This item descriptor has a filter set. Let's loop through only these attributes

			// Let's merge optional filter Array (addAttributes) with mandatory filter array (filter) to form the final filter Array
			String[] filterArray = (String[])ArrayUtils.addAll( filter.get( itemDescriptor ).getStrings(), addAttributes );
			// Now, loop through merged filterArray
			for( String propName : filterArray ) {
				if( repItem.getItemDescriptor().hasProperty( propName ) ) {
					Object propValue = repItem.getPropertyValue( propName );
					
					if( propValue instanceof RepositoryItem ) {
						// recursive call this same method if type is repository Item
						if( isPriceinCart ) // If priceinCart is true then we need to send this to the recursive call. This is required to
											// handle price logic at SKU level
							output.put( propName, BuildRepositoryMap( (RepositoryItem)propValue, filter, PRODUCT_ATTR_PRICEINCART + ":" + isPriceinCart.toString() ) );
						else
							output.put( propName, BuildRepositoryMap( (RepositoryItem)propValue, filter ) );
					} else if( propValue instanceof ChangeAwareList ) {
						// ChangeAwareList is usually a List of Repository Items
						ChangeAwareList repList = (ChangeAwareList)propValue;
						List<Object> innerOutput = new ArrayList<>();
						for( int i = 0; i < repList.size(); i++ ) {
							Object repListitem = repList.get( i );
							if( repListitem instanceof RepositoryItem ) {
								if( isPriceinCart ) // If priceinCart is true then we need to send this to the recursive call. This is
													// required to handle price logic at SKU level
									innerOutput.add( BuildRepositoryMap( (RepositoryItem)repListitem, filter, PRODUCT_ATTR_PRICEINCART + ":" + isPriceinCart.toString() ) );
								else
									innerOutput.add( BuildRepositoryMap( (RepositoryItem)repListitem, filter ) );
							} else {
								innerOutput.add( repListitem );
							}
						}
						output.put( propName, innerOutput );
					} else if( propValue != null ) {
						// If "priceInCart=true then set all Prices to 0. Do this only for "product" item descriptor.
						if( isPriceinCart && this.priceInCartPrices != null && this.priceInCartPrices.size() > 0 && Arrays.asList( priceInCartPrices.stringAt( 0 ).split( "," ) ).contains( propName ) ) {
							output.put( propName, "0.0" );
						} else {
							if( propValue instanceof String ) // Should trim only Strings
								output.put( propName, propValue.toString().trim() );
							else
								output.put( propName, propValue );
						}
					}
				}
			}

		} else {
			// This item descriptor does not have a filter set. Let's loop through all attributes
			// PriceinCart logic not added here as we assume SKU and Product will always have Filter set
			for( String propName : repItem.getItemDescriptor().getPropertyNames() ) {

				Object propValue = repItem.getPropertyValue( propName );
				if( propValue instanceof RepositoryItem ) {
					// recursive call this same method if type is repository Item
					output.put( propName, BuildRepositoryMap( (RepositoryItem)propValue, filter ) );
				} else if( propValue instanceof ChangeAwareList ) {
					// ChangeAwareList is usually a List of Repository Items
					ChangeAwareList repList = (ChangeAwareList)propValue;
					Map<String, Object> innerOutput = new HashMap<>();
					for( int i = 0; i < repList.size(); i++ ) {
						Object repListitem = repList.get( i );
						if( repListitem instanceof RepositoryItem ) {
							innerOutput.put( Integer.toString( i ), BuildRepositoryMap( (RepositoryItem)repListitem, filter ) );
						} else {
							innerOutput.put( Integer.toString( i ), repListitem );
						}
					}
					output.put( propName, innerOutput );
				} else if( propValue != null ) {
					// If "priceInCart=true then set all Prices to 0. Do this only for "product" item descriptor.
					if( isPriceinCart && this.priceInCartPrices != null && this.priceInCartPrices.size() > 0 && Arrays.asList( priceInCartPrices.stringAt( 0 ).split( "," ) ).contains( propName ) ) {
						output.put( propName, "0.0" );
					} else {
						if( propValue instanceof String ) // Should trim only Strings
							output.put( propName, propValue.toString().trim() );
						else
							output.put( propName, propValue );
					}
				}
			}
		}

		return output;
	}

	private String getDisplayedColorCode(RepositoryItem repItem) {
		Object childSkusObj = repItem.getPropertyValue( CHILD_SKUS );
		if (null == childSkusObj) {
			return null; 
		}
		List<RepositoryItem> childSKUItems = (List<RepositoryItem>)childSkusObj; 
		long maxStockLevel=0;
		long stockLevel=0;
		String defaultColorCode="";
		Object defColorCodeObj = repItem.getPropertyValue(DEFAULT_COLOR_CODE);
		if(null != defColorCodeObj) {
			defaultColorCode = (String) defColorCodeObj;
		}
		String displayColorCode = defaultColorCode; // If there is no sku with color code available and all the skus dont have inventory, the defaultColor is selected color code. 
		for(RepositoryItem childSkuItem : childSKUItems){
			Object skuStockLevelObj = childSkuItem.getPropertyValue(SKU_STOCK_LEVEL);
			if(null != skuStockLevelObj) {
				stockLevel = (Long) skuStockLevelObj;
			}
			if(stockLevel == 0) {
				continue;
			}
			//Below code will be executed only for skus with stock available
			RepositoryItem skuColorItem = null;
			String currentSkuColorCode = "";
			Object colorObj = childSkuItem.getPropertyValue(COLOR);
			if(null != colorObj ){
				skuColorItem = (RepositoryItem) colorObj;
				Object colorCodeObj = skuColorItem.getPropertyValue(COLOR_CODE);
				if (null != colorCodeObj) {
					currentSkuColorCode = (String) colorCodeObj;
				}
			}
			
			if(DigitalStringUtil.equals(defaultColorCode, currentSkuColorCode)){ // Requiremement says  "If the default color has inventory, the default color will face out."
																			 // so current default color code is kept as the color code. 	
				displayColorCode = defaultColorCode;
				break;
			}			
			if(stockLevel > maxStockLevel){
				displayColorCode = currentSkuColorCode; 
				maxStockLevel = stockLevel;
			}
		}
		return displayColorCode;
	}

}
