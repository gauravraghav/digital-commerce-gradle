package com.digital.commerce.services.filter.bean;

import java.util.List;
import java.util.Map;

import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.DigitalHardgoodShippingGroup;

import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupRelationship;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;
@SuppressWarnings({"unchecked"})
public class DigitalCommerceItemShippingInfoCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {

	public DigitalCommerceItemShippingInfoCustomizer() {
		super(DigitalCommerceItemShippingInfoCustomizer.class.getName());
	}
	
	@Override
	 public Object getPropertyValue(Object pTargetObject, String pPropertyName, Map<String, String> pAttributes) throws BeanFilterException {
		if(pTargetObject == null)
			return "";
		
		List<ShippingGroupRelationship> shippingGroupRelationships = ((DigitalCommerceItem)pTargetObject).getShippingGroupRelationships();
		
		if(shippingGroupRelationships.size() == 0)
			return "";
		
		ShippingGroupRelationship sgr = shippingGroupRelationships.get(0);
		
		ShippingGroup sg = sgr.getShippingGroup();
		
		if(sg instanceof DigitalHardgoodShippingGroup){
			return ((DigitalHardgoodShippingGroup) sg).getPropertyValue(pPropertyName);
		}
		
		return null;
		
	}

}
