package com.digital.commerce.services.pricing;

import static com.digital.commerce.common.services.DigitalBaseConstants.SHIPPING_METHODS;
import static com.digital.commerce.common.services.DigitalBaseConstants.STATIC_SHIPPING_RATES;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.multisite.MultiSiteUtil;

import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import lombok.Getter;
import lombok.Setter;

/** 
 *         This component is a mapping between shipping LOS codes and their
 *         corresponding human readable names */
@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public class ShippingHandlingInfo {
	
	private Boolean loggingDebug;
	private static final DigitalLogger logger = DigitalLogger.getLogger(ShippingHandlingInfo.class);


	/** This method creates a <code>ShippingRateTable</code> object with static
	 * data.
	 * 
	 * @return <code>ShippingRateTable</code> */
	public ShippingRateTable getStaticShippingRateTable( Order order ) {
		ShippingRateTable srTable = new ShippingRateTable();

		if( isLoggingDebug() ) {
			logger.debug( "Inside ShippingHandlingInfo:getStaticShippingRateTable(Order , String ) ..." );
		}
		String siteId = order.getSiteId();
		Properties shippingRates = ((Properties)(MultiSiteUtil.getShippingRates(siteId).get(STATIC_SHIPPING_RATES)));
		
		Iterator i = ((ArrayList)(MultiSiteUtil.getShippingRates(siteId).get(SHIPPING_METHODS))).iterator();

		while( i.hasNext() ) {
			String method = (String)i.next();

			ShippingRate sr = new ShippingRate();
			sr.setBaseRateServiceLevel( method );
			sr.setBaseRatesServiceLevelDescription( method );
			sr.setBaseRateServiceName( method );
			sr.setBaseRate( Double.valueOf( shippingRates.getProperty( method ) ).doubleValue() );
			for( int x = 0; x < order.getShippingGroups().size(); x++ ) {
				ShippingGroup sg = (ShippingGroup)order.getShippingGroups().get( x );
				String sgId = sg.getId();
				srTable.addShippingRate( sgId, sr );
			}
		}
		return srTable;
	}

	/**
	 * @return the loggingDebug
	 */
	public Boolean isLoggingDebug() {
		return this.getLoggingDebug();
	}
}
