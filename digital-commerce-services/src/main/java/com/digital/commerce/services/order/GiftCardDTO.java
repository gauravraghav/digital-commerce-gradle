package com.digital.commerce.services.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.digital.commerce.common.domain.Address;
import lombok.Getter;
import lombok.Setter;

/** DTO to hold gift card and its supporing information like from address, ship
 * address, order details etc
 * 
 * @author Admin */

@Getter
@Setter
public class GiftCardDTO implements Serializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 6666098497629472828L;
	private Address				address = new Address();
	private Long				addressId;
	private Double				vendorCardPrice;
	private String				vendorId;
	private Double				vendorInvoiceAmount;
	private String				vendorOrderNumber;
	private Double				vendorPostage;
	private String				barCode;
	private Double				cardPrice;
	private Long				cardQty;
	private String				dswOrderNumber;
	private Long				dswAffiliateId;
	private Long				dollarValue;
	private Double				postage;
	private String				productId;
	private Address				returnAddress = new Address();
	private Long				returnAddressId;
	private String				pin;
	private String				userEmail;
	private String				giftCardNumber;

	public Double getGiftCardPriceTotal() {
		BigDecimal amount = (BigDecimal) (getCardPrice() == 0.0d ? new BigDecimal( "0" ).setScale( 2, RoundingMode.HALF_UP ) : getCardPrice());
		amount = amount.add( ( getDollarValue() == null ? new BigDecimal( "0" ).setScale( 2, RoundingMode.HALF_UP ) : new BigDecimal( getDollarValue() ).setScale( 2, RoundingMode.HALF_UP ) ) );
		amount = amount.multiply( getCardQty() == null ? new BigDecimal( "1" ).setScale( 2, RoundingMode.HALF_UP ) : new BigDecimal( getCardQty() ).setScale( 2, RoundingMode.HALF_UP ) );
		amount = amount.add( (BigDecimal) ( getPostage() == 0.0d ? new BigDecimal( "0" ).setScale( 2, RoundingMode.HALF_UP ) : getPostage() ));
		return amount.doubleValue();
	}

	/** Cost of plastic + Dollar Value
	 * 
	 * @return */
	public Double getGiftCardPrice() {
		BigDecimal price = (BigDecimal) (getCardPrice() == 0.0d ? new BigDecimal( "0" ).setScale( 2, RoundingMode.HALF_UP ) : getCardPrice());
		price = price.add( ( getDollarValue() == null ? new BigDecimal( "0" ).setScale( 2, RoundingMode.HALF_UP ) : new BigDecimal( getDollarValue() ).setScale( 2, RoundingMode.HALF_UP ) ) );
		return price.doubleValue();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder( 1000 );
		sb.append( "[GiftCardDTO: \n" );
		sb.append( " | cltAffiliateId= " );
		sb.append( getDswAffiliateId() );
		sb.append( " | clientOrderNumber= " );
		sb.append( getDswOrderNumber() );
		sb.append( " | VendorOrderNumber= " );
		sb.append( getVendorOrderNumber() );
		sb.append( "VendorInvoiceAmount= " );
		sb.append( getVendorInvoiceAmount() );
		sb.append( " | VendorCardPrice= " );
		sb.append( getVendorCardPrice() );
		sb.append( " | VendorPostage= " );
		sb.append( getVendorPostage() );
		sb.append( " | productId= " );
		sb.append( getProductId() );
		sb.append( " | cardPrice= " );
		sb.append( getCardPrice() );
		sb.append( " | dollarValue= " );
		sb.append( getDollarValue() );
		sb.append( " | postage= " );
		sb.append( getPostage() );
		sb.append( " | barCode= " );
		sb.append( getBarCode() );
		sb.append( " | VendorId= " );
		sb.append( getVendorId() );
		sb.append( " | addressId= " );
		sb.append( getAddressId() );
		sb.append( " | returnAddressId= " );
		sb.append( getReturnAddressId() );
		sb.append( " | cardQty= " );
		sb.append( getCardQty() );
		sb.append( " | address= " );
		sb.append( ( address == null ) ? "Not Found" : address.toString() );
		sb.append( " | returnAddress= " );
		sb.append( ( returnAddress == null ) ? "Not Found" : returnAddress.toString() );
		sb.append( " | userEmail= " );
		sb.append( userEmail );
		sb.append( " | giftCardNumber= " );
		sb.append( giftCardNumber );
		sb.append( " | pin= " );
		sb.append( pin );
		sb.append( "]" );
		return sb.toString();
	}
}
