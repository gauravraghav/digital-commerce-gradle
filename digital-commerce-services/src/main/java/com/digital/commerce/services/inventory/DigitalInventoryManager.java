package com.digital.commerce.services.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.configuration.ErrorCodes;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.inventory.InventoryReservationService;
import com.digital.commerce.integration.inventory.YantraService;
import com.digital.commerce.integration.inventory.storenet.domain.InventoryReservationServiceRequest;
import com.digital.commerce.integration.inventory.storenet.domain.InventoryReservationServiceResponse;
import com.digital.commerce.integration.inventory.storenet.domain.ItemInventory;
import com.digital.commerce.integration.inventory.yantra.domain.Item;
import com.digital.commerce.integration.inventory.yantra.domain.YantraInventoryReservationServiceRequest;
import com.digital.commerce.integration.inventory.yantra.domain.YantraInventoryReservationServiceResponse;
import com.digital.commerce.integration.inventory.yantra.domain.YantraReservation;
import com.digital.commerce.integration.inventory.processor.InventoryDecrementMessageSource;
import com.digital.commerce.services.order.DigitalCommerceItem;
import com.digital.commerce.services.order.GiftCardCommerceItem;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;

import atg.commerce.catalog.CatalogTools;
import atg.commerce.inventory.InventoryException;
import atg.commerce.inventory.RepositoryInventoryManager;
import atg.commerce.locations.CoordinateManager;
import atg.commerce.order.Order;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * This code no longer directly decrements inventory but now posts a message
 * allowing inventory to be decremented asynchronously one sku at a time. This
 * change was made to reduce contention for inventory records when there is a
 * hot sku (i.e. GWP items). This will also eliminate one scenario in which a
 * customer's credit card can be auth'ed multiple times.
 * 
 * Because ATG is not the system of record for inventory, there is no need for
 * the reduction in stock level to be part of the same transaction in which the
 * order is placed.
 * 
 * @author thanna
 * @author wibrahim
 */
@Getter
@Setter
public class DigitalInventoryManager extends RepositoryInventoryManager {
	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalInventoryManager.class);

	private YantraService yantraService;
	private InventoryReservationService storeNetService;

	private CatalogTools catalogTools;
	private CoordinateManager storeManager;
	private DigitalBaseConstants dswConstants;


	private InventoryDecrementMessageSource inventoryDecrementMessageSender;

	@Getter
	public enum ReservationStatus {
		UNEVALUATED(-1), SUCCESS(2), UNMATCHED(3), EVALUATED(1), ERROR(0);
		private final int value;

		private ReservationStatus(int value) {
			this.value = value;
		}

	}

    private String shipNodePrefix = "29_";

	/*
	 * TODO MK:REDESIGN need to revisit during integration with
	 * dsw-commerce-integration project private InventoryDecrementMessageSender
	 * inventoryDecrementMessageSender; // Injected by Spring - see
	 */
	@Override
	protected RepositoryItem getInventoryItem(String pId, String pLocationId)
			throws RepositoryException {
		String key = null;
		if (pLocationId == null) {
			key = pId;
		} else {
			key = pId + ":" + pLocationId;
		}

		RepositoryItem item = null;
		InventoryCache requestCache = getRequestInventoryCache();
		if (requestCache != null) {
			item = requestCache.get(key);
			if (item != null)
				return item;
			item = super.getInventoryItem(pId, pLocationId);
			if (item != null)
				requestCache.put(key, item);
		} else {
			item = super.getInventoryItem(pId, pLocationId);
		}
		return item;
	}

	/**
	 * 
	 * @return
	 */
	private InventoryCache getRequestInventoryCache() {
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		if (request == null)
			return null;
		return ComponentLookupUtil.lookupComponent(
				"/com/digital/commerce/services/inventory/InventoryCache",
				InventoryCache.class);
	}
	
    /**
     * 
     * @param skuId
     * @return
     */
	public boolean inventoryRecordExists(String skuId) {
		boolean retVal = false;

		try {
			RepositoryItem inventoryItem = (RepositoryItem) getInventoryItem(skuId);
			if (inventoryItem != null)
				retVal = true;
			else
				retVal = false;
		} catch (RepositoryException e) {
			if (isLoggingDebug()) {
				logError("Digital Inventory Manager - No such repository " + e);
			}
		}
		return retVal;

	}

	/**
	 * 
	 * @param skuId
	 * @param property
	 * @return
	 */
	public int findStockLevelForNode(String skuId, String property) {
		int retVal = 0;
		try {
			RepositoryItem inventoryItem = (RepositoryItem) getInventoryItem(skuId);
			if (inventoryItem != null) {
				Long inventoryLevel = (Long) inventoryItem.getPropertyValue(property);
				if (inventoryLevel != null) {
					retVal = inventoryLevel.intValue();
				}
			}
		} catch (RepositoryException e) {
			if (isLoggingDebug()) {
				logError("Digital Inventory Manager - No such repository " + e);
			}
		}
		return retVal;

	}

	/**
	 * 
	 * @param id
	 * @param bagItem
	 * @param serviceCall
	 * @return
	 */
    private ReservationStatus storeNetReservationService(String id,
			DigitalCommerceItem bagItem, boolean serviceCall) {

		boolean matched = true;

		boolean serviceCallFailure = false;

		if (serviceCall) {
			
			//storenet call
			InventoryReservationServiceRequest storenetInventoryServiceRequest = new InventoryReservationServiceRequest();
			storenetInventoryServiceRequest.setExternalOrderNumber(id);
			storenetInventoryServiceRequest.setItemHoldQuantity((short) bagItem
					.getQuantity());
			storenetInventoryServiceRequest.setVendorNumber(bagItem
					.getStoreId());
			storenetInventoryServiceRequest.setSku(bagItem.getCatalogRefId());
		    InventoryReservationServiceResponse storeNetInventoryReservationServiceResponse = new InventoryReservationServiceResponse();
			try {
				storeNetInventoryReservationServiceResponse = this.storeNetService
						.addHold(storenetInventoryServiceRequest);

				if (storeNetInventoryReservationServiceResponse == null){					
					bagItem.setItemReserved(true);
					bagItem.setItemQuantityReserved(bagItem.getQuantity());
					throw new DigitalIntegrationBusinessException("Storenet Reservation Error", ErrorCodes.COMM_ERROR.getCode(),
							new Exception("Empty Response"));
				}
				
				double reservedQty = 0.00;
				
				ItemInventory itemInventory = storeNetInventoryReservationServiceResponse.getItemInventory();
				
				// As StoreNet accepts inventory reservation beyond the available inventory, DSW has to calculate on how many got reserved during this request. # PLAT-1452
				if(itemInventory != null){
					if(itemInventory.getItemQuantityAllocated().doubleValue() > itemInventory.getItemQuantityOnHold().doubleValue() ){
						reservedQty = bagItem.getQuantity()  + (itemInventory.getItemQuantityOnHand().doubleValue() - itemInventory.getItemQuantityAllocated().doubleValue());
					}
					else{
						reservedQty = itemInventory.getItemQuantityOnHand().doubleValue() - itemInventory.getItemQuantityAllocated().doubleValue();
					}
				}
				
				if(reservedQty < 1){				
					bagItem.setItemReserved(false);
					reservedQty = 0.00;
				} else {				
					bagItem.setItemReserved(true);
				}
				
				bagItem.setItemQuantityReserved(reservedQty);
				
				if ((reservedQty == 0.00) || (bagItem.getQuantity() > reservedQty)) {
					matched = false;
					bagItem.setInventoryEvaluationResult(ReservationStatus.UNMATCHED.value);
				} else {
					bagItem.setInventoryEvaluationResult(ReservationStatus.SUCCESS.value);
					
					// Notify Yantra service as Store Net call reservation went through
					reserveBOPISItemInYantra(id, bagItem, serviceCall);
					
				}

			} catch (DigitalIntegrationException e) {
				logger.error(e);
				serviceCallFailure = true;
				// Notify Yantra service as Store Net call reservation went through
				reserveBOPISItemInYantra(id, bagItem, serviceCall);
			}
		}


		if (!serviceCall) {
			matched = this.reposityReservationService(bagItem);
			
		}
		
		if (serviceCall && serviceCallFailure) {
			bagItem.setItemReserved(true);
			bagItem.setInventoryEvaluationResult(ReservationStatus.SUCCESS.value);
			bagItem.setItemQuantityReserved(bagItem.getQuantity());
			matched = true;
		}

		if (matched){
			return ReservationStatus.SUCCESS;
		}
		else{
			return ReservationStatus.UNMATCHED;
		}

	}
    
    /**
     * To ensure overall supply is maintained in Yantra Inventory master 
     * so that demand is not over promised to the user than the actual
     * 
     * @param id
     * @param bagItem
     * @param serviceCall
     */
    private void reserveBOPISItemInYantra(String id,
			DigitalCommerceItem bagItem, boolean serviceCall){
    	
    	if(serviceCall){
			Item yantraItem = new Item();
			yantraItem.setItemID(bagItem.getCatalogRefId());
			yantraItem.setRequiredQty((double) bagItem.getQuantity());
			yantraItem.setProductClass("GOOD");
			yantraItem.setUnitOfMeasure("EACH");
			yantraItem.setShipNode(getShipNodePrefix() + bagItem.getStoreId());
			yantraItem.setFulfillmentType(Item.FulfillmentType.BOPIS);
			List<Item> yantraItems = new ArrayList<>(1);
			yantraItems.add(yantraItem);
			this.updateYantraReservationService(id, yantraItems);
    	}
    }
    
    /**
     * 
     * @param id
     * @param items
     */
	private void updateYantraReservationService(String id,
			List<Item> items) {
		
		YantraInventoryReservationServiceRequest yantraServiceRequest = new YantraInventoryReservationServiceRequest();
		yantraServiceRequest.setReservationId(id);
		yantraServiceRequest.setYantraItems(items);
		
		try {
				this.getYantraService().reservation(
					yantraServiceRequest);
		} catch (DigitalIntegrationException e) {
			logger.error(e);
		}

	}

	/**
	 * 
	 * @param id
	 * @param items
	 * @param bagItems
	 * @param serviceCall
	 * @return
	 */
	private ReservationStatus yantraReservationService(String id,
			List<Item> items, List<DigitalCommerceItem> bagItems,
			boolean serviceCall) {
		boolean matched = true;
		boolean serviceFailed = false;
		YantraInventoryReservationServiceResponse yantraServiceResponse = null;

		if (serviceCall) {
			YantraInventoryReservationServiceRequest yantraServiceRequest = new YantraInventoryReservationServiceRequest();
			yantraServiceRequest.setReservationId(id);
			yantraServiceRequest.setYantraItems(items);
			yantraServiceResponse = new YantraInventoryReservationServiceResponse();
			try {
				yantraServiceResponse = this.getYantraService().reservation(
						yantraServiceRequest);
				//yantraServiceResponse = null;
				if (yantraServiceResponse == null)
					serviceFailed = true;

			} catch (DigitalIntegrationException e) {
				logger.error(e);
				// support hot-hot situation
				serviceFailed = true;
			}
		}

		if (!serviceCall) {
		    serviceFailed = true;
			boolean intermMatched = true;
			for (Item yItem : items) {
				for (DigitalCommerceItem cItem : bagItems) {
					if (cItem.getCatalogRefId().equals(yItem.getItemID())) {
						intermMatched = this.reposityReservationService(cItem);
						if (!intermMatched)
							matched = false;
					}
				}
			}
		}
		
		if (serviceCall && serviceFailed) {
			for (Item yItem : items) {
				for (DigitalCommerceItem cItem : bagItems) {
					if (cItem.getCatalogRefId().equals(yItem.getItemID())
							&& (DigitalStringUtil.isEmpty(cItem.getShipType())
									|| DigitalStringUtil.isEmpty(cItem.getStoreId()) || (cItem
									.getShipType()
									.equals(ShippingGroupConstants.ShipType.BOSTS
											.getValue())
									&& yItem.getShipNode().equals(
											getShipNodePrefix()
													+ cItem.getStoreId())))) {
						cItem.setInventoryEvaluationResult(ReservationStatus.SUCCESS.value);
						cItem.setItemReserved(true);
						cItem.setItemQuantityReserved(cItem.getQuantity());						
					}
				}
			}
			matched = true;
		}


		if (!serviceFailed && yantraServiceResponse != null) {
			Map<String, Double> flowQtyMap = new HashMap<>();
			List<YantraReservation> reservations = yantraServiceResponse.getYantraReservations();
			if (reservations != null) {
				for (YantraReservation reservation : reservations) {
					for (DigitalCommerceItem item : bagItems) {
						if (item.getItemQuantityReserved() != item.getQuantity()
								&& item.getCatalogRefId().equalsIgnoreCase(reservation.getItem().getItemID())) {
							// Check if this reservation is for Ship To Home
							// Line Item
							if (item.getShipType().equalsIgnoreCase(ShippingGroupConstants.ShipType.SHIP.getValue())
									&& DigitalStringUtil.isEmpty(item.getStoreId())
									&& DigitalStringUtil.isEmpty(reservation.getProcureFromNode())
									&& (DigitalStringUtil.isEmpty(reservation.getFullfillmentType())
											|| ShippingGroupConstants.ShipType.SHIP.getValue()
													.equalsIgnoreCase(reservation.getFullfillmentType()))) {
								item.setItemReserved(true);
								double omsReservedQty = 0.00;
								double itemReservedQty = 0.00;
								if (flowQtyMap.containsKey(item.getId())) {
									itemReservedQty = flowQtyMap.get(item.getId()) == null ? omsReservedQty
											: flowQtyMap.get(item.getId());
									omsReservedQty = (reservation.getReservedQty() == item.getQuantity())
											? reservation.getReservedQty()
											: (itemReservedQty + reservation.getReservedQty());
								} else {
									omsReservedQty = reservation.getReservedQty();
								}
								flowQtyMap.put(item.getId(), omsReservedQty);
								item.setItemQuantityReserved(omsReservedQty);
								break;
							}
							
							// Check if this reservation is for Ship To Store
							// Line Item (aka BOSTS)
							if (!DigitalStringUtil.isEmpty(item.getStoreId()) && item.getShipType()
									.equalsIgnoreCase(ShippingGroupConstants.ShipType.BOSTS.getValue())
									&& reservation.getShipNode().equals(getShipNodePrefix() + item.getStoreId())
									&& ShippingGroupConstants.ShipType.BOSTS.getValue()
											.equalsIgnoreCase(reservation.getFullfillmentType())) {
								item.setItemReserved(true);
								double omsReservedQty = 0.00;
								double itemReservedQty = 0.00;
								if (flowQtyMap.containsKey(item.getId())) {
									itemReservedQty = flowQtyMap.get(item.getId()) == null ? omsReservedQty
											: flowQtyMap.get(item.getId());
									omsReservedQty = (reservation.getReservedQty() == item.getQuantity())
											? reservation.getReservedQty()
											: (itemReservedQty + reservation.getReservedQty());
								} else {
									omsReservedQty = reservation.getReservedQty();
								}
								flowQtyMap.put(item.getId(), omsReservedQty);
								item.setItemQuantityReserved(omsReservedQty);
								break;
							}
						}
					}
				}

				for (DigitalCommerceItem item : bagItems) {
					if (item.isItemReserved() && item.getQuantity() > item.getItemQuantityReserved()) {
						matched = false;
					} else if (!item.isItemReserved()) {
						item.setItemQuantityReserved(0.00);
					}
				}

			} else {
				matched = false;
			}
		}

		// Mark reserve items
		for (DigitalCommerceItem item : bagItems) {
			if (!item.isItemReserved()) {
				item.setInventoryEvaluationResult(ReservationStatus.UNMATCHED.value);
				matched = false;
			}
		}

		if (matched){
			return ReservationStatus.SUCCESS;
		}
		else {
			return ReservationStatus.UNMATCHED;
		}
	}

	/**
	 * 
	 * @param order
	 * @return 0 : error; 1: All items has been evaluated; 2: Item is being
	 *         evaluated
	 */
	public ReservationStatus orderReservation(Order order, boolean yantraCall,
			boolean storeNetCall) throws RepositoryException {
		boolean evaluated = true;
		boolean matched = true;
		if (order == null)
			return ReservationStatus.ERROR;
		else {
			@SuppressWarnings("unchecked")
			List<DigitalCommerceItem> items = order.getCommerceItems();
			List<Item> yantraItems = new ArrayList<>();
			List<DigitalCommerceItem> referenceItems = new ArrayList<>();
			
			for (DigitalCommerceItem item : items) {
				if(item instanceof GiftCardCommerceItem){
					item.setItemReserved(true);
					item.setItemQuantityReserved(item.getQuantity());
					item.setInventoryEvaluationResult(ReservationStatus.SUCCESS.value);
					continue;
				}
				
				// If getItemQuantityReserved == -1 then we didn't reserve anything. So, force Yantra reservation
				if (item.getInventoryEvaluationResult() == ReservationStatus.UNEVALUATED.getValue() && -1 == item.getItemQuantityReserved() ) {
					evaluated = false;
				} else {
					if (item.getInventoryEvaluationResult() == ReservationStatus.SUCCESS.getValue()) {
						if (item.getQuantity() != (long) item.getItemQuantityReserved()) {
							item.setItemReserved(false);
							item.setItemQuantityReserved(0.00);
							item.setInventoryEvaluationResult(ReservationStatus.UNEVALUATED.getValue());
							evaluated = false;
						}
					}
					if (item.getInventoryEvaluationResult() == ReservationStatus.UNMATCHED.getValue()) {
						if (item.getQuantity() > (long) item.getItemQuantityReserved()) {
							item.setItemReserved(false);
							item.setItemQuantityReserved(0.00);
							item.setInventoryEvaluationResult(ReservationStatus.UNEVALUATED.getValue());
							evaluated = false;
						}
					}

				}

				if (!evaluated) {
					if (item.getShipType() != null
							&& item.getShipType().equals(
									ShippingGroupConstants.ShipType.BOPIS
											.getValue())) 
					{
						RepositoryItem sku = this.getCatalogTools().findSKU(
								item.getCatalogRefId());
						RepositoryItem store = this.getStoreByNumber(item
								.getStoreId());
						if (isSkuEligibileForBOPISFulfillment(sku) && isStoreEligibleForPickUpInStoreFulfillment(store)) {	
						
								ReservationStatus storeStatus = this
										.storeNetReservationService(order.getId(),
												item, storeNetCall);
								if (storeStatus == ReservationStatus.ERROR){
										return ReservationStatus.ERROR;
								}
								else {
										item.setInventoryEvaluationResult(storeStatus.value);
										if (storeStatus == ReservationStatus.SUCCESS){
											item.setItemQuantityReserved(item.getQuantity());
										}
										else {
											matched = false;
										}
								}
						} else {
							item.setStorePickAvailable(false);
							item.setInventoryEvaluationResult(ReservationStatus.UNMATCHED.value);
							matched = false;
						}
					} else {
						boolean addToYantrafg = false;
						if (item.getShipType() != null
								&& item.getShipType().equals(
										ShippingGroupConstants.ShipType.BOSTS
												.getValue())) {
							RepositoryItem sku = this.getCatalogTools()
									.findSKU(item.getCatalogRefId());
							RepositoryItem store = this.getStoreByNumber(item
									.getStoreId());
							if (isSkuEligibileForBOSTSFulfillment(sku) && isStoreEligibleForShipToStoreFulfillment(store)) {
								addToYantrafg = true;
							} else {
								item.setInventoryEvaluationResult(ReservationStatus.UNMATCHED.value);
								item.setStorePickAvailable(false);
								matched = false;
							}
						} else {
							addToYantrafg = true;
						}
						
						if (addToYantrafg) {
							Item yantraItem = new Item();
							yantraItem.setItemID(item.getCatalogRefId());
							yantraItem.setRequiredQty((double) item.getQuantity());
							yantraItem.setProductClass("GOOD");
							yantraItem.setUnitOfMeasure("EACH");
							if (item.getShipType() != null
									&& item.getShipType()
											.equals(ShippingGroupConstants.ShipType.BOSTS
													.getValue())) {
								yantraItem.setShipNode(getShipNodePrefix() + item.getStoreId());
								yantraItem.setFulfillmentType(Item.FulfillmentType.BOSTS);
							} else {
								yantraItem.setFulfillmentType(Item.FulfillmentType.REGULAR);
							}
							
							yantraItems.add(yantraItem);
							referenceItems.add(item);
						}
					}
				}
				
				RepositoryItem sku = this.getCatalogTools().findSKU(item.getCatalogRefId());
				RepositoryItem store = this.getStoreByNumber(item.getStoreId());
				if (isSkuEligibileForBOSTSFulfillment(sku) && isStoreEligibleForShipToStoreFulfillment(store)) {
					item.setShipToStoreAvailable(true);
				} else {
					item.setShipToStoreAvailable(false);
				}
				
				if (isSkuEligibileForBOPISFulfillment(sku) && isStoreEligibleForPickUpInStoreFulfillment(store)) {					
					item.setStorePickAvailable(true);
				} else {
					item.setStorePickAvailable(false);
				}
			}

			if (yantraItems != null && yantraItems.size() > 0) {
				ReservationStatus yantraStatus = this.yantraReservationService(
						order.getId(), yantraItems, items, yantraCall);
				if (yantraStatus == ReservationStatus.UNMATCHED){
					matched = false;
				}
				if (yantraStatus == ReservationStatus.ERROR){
					return ReservationStatus.ERROR;
				}
			}
		}

		if (evaluated){
			return ReservationStatus.EVALUATED;
		}

		if (matched){
			return ReservationStatus.SUCCESS;
		}
		else {
			return ReservationStatus.UNMATCHED;
		}

	}

	/**
	 * This method to support hot-hot situation
	 * 
	 * @param cItem
	 * @return
	 * @throws InventoryException
	 */
	public boolean reposityReservationService(DigitalCommerceItem cItem) {
		int availableQty = 0;
		boolean matched = true;
		try {
			if (cItem.getShipType() != null
					&& cItem.getShipType().equals(
							ShippingGroupConstants.ShipType.BOPIS.getValue())) {
				String sid = cItem.getStoreId();
				availableQty = this.queryAvailabilityStatus(
						cItem.getCatalogRefId(), sid);
			} else {
				availableQty = this.queryAvailabilityStatus(
						cItem.getCatalogRefId(),
						MultiSiteUtil.getWebsiteLocationId());
			}
		} catch (InventoryException e) {
			logger.error(e);
			availableQty = 0;
		}

		if (availableQty < cItem.getQuantity()) {
			cItem.setInventoryEvaluationResult(ReservationStatus.UNMATCHED.value);
			matched = false;
		} else {
			cItem.setInventoryEvaluationResult(ReservationStatus.SUCCESS.value);
			availableQty = (int) cItem.getQuantity();
		}
		cItem.setItemQuantityReserved(availableQty);
		cItem.setItemReserved(true);
		return matched;
	}

	@Override
	public int queryAvailabilityStatus(String pId) throws InventoryException {
		return this.queryAvailabilityStatus(pId,
				MultiSiteUtil.getWebsiteLocationId());
	}

	@Override
	public int purchase(String pId, long pHowMany) throws InventoryException {
		return this.purchase(pId, pHowMany,
				MultiSiteUtil.getWebsiteLocationId());
	}

	@Override
	public int decreaseStockLevel(String pId, long pNumber)
			throws InventoryException {
		return this.decreaseStockLevel(pId, pNumber,
				MultiSiteUtil.getWebsiteLocationId());
	}

	@Override
	public int purchase(String pId, long pNumber, String pLocationId) throws InventoryException {
		if (MultiSiteUtil.getWebsiteLocationId().equals(pLocationId)) {
			return super.purchase(pId, pNumber, pLocationId);
		} else {
			int result = super.purchase(pId, pNumber, pLocationId);
			super.purchase(pId, pNumber, MultiSiteUtil.getWebsiteLocationId());
			return result;
		}
	}

	@Override
	public int decreaseStockLevel(String pId, long pNumber, String pLocationId) throws InventoryException {
		if (MultiSiteUtil.getWebsiteLocationId().equals(pLocationId)) {
			return super.decreaseStockLevel(pId, pNumber, pLocationId);
		} else {
			int result = super.decreaseStockLevel(pId, pNumber, pLocationId);
			super.decreaseStockLevel(pId, pNumber,
					MultiSiteUtil.getWebsiteLocationId());
			return result;
		}
	}

	@Override
	public int purchaseOffBackorder(String pId, long pHowMany)
			throws InventoryException {
		return purchaseOffBackorder(pId, pHowMany,
				MultiSiteUtil.getWebsiteLocationId());
	}

	@Override
	public long queryStockLevel(String pId) throws InventoryException {
		return queryStockLevel(pId, MultiSiteUtil.getWebsiteLocationId());
	}

	@Override
	public long queryBackorderLevel(String pId) throws InventoryException {
		return queryBackorderLevel(pId, MultiSiteUtil.getWebsiteLocationId());
	}

	/**
	 * 
	 * @param skuId
	 * @param property
	 * @param locationId
	 * @return
	 */
	public int findStockLevelForNodeWithLocationId(String skuId,
			String property, String locationId) {
		int retVal = 0;
		try {
			RepositoryItem inventoryItem = (RepositoryItem) getInventoryItem(
					skuId, locationId);
			if (inventoryItem != null) {
				Long inventoryLevel = (Long) inventoryItem
						.getPropertyValue(property);
			if (inventoryLevel != null) {
					retVal = inventoryLevel.intValue();
				}
			}
		} catch (RepositoryException e) {
			if (isLoggingDebug()) {
				logError("Digital Inventory Manager - No such repository " + e);
			}
		}
		return retVal;
	}

	/**
	 * 
	 * @param storeId
	 * @return
	 */
	private RepositoryItem getStoreByNumber(String storeId) {

		RepositoryItem[] items = null;
		try {
			RepositoryView view = getStoreManager().getRepository().getView(
					this.getStoreManager().getDefaultItemType());
			RqlStatement statement = RqlStatement
					.parseRqlStatement("storeNumber = ?0");
		Object params[] = new Object[1];
			params[0] = storeId;
			items = statement.executeQuery(view, params);
		} catch (RepositoryException e) {
			logger.error(e);
			return null;
		}

		if (items != null) {
			return items[0];
		}

		return null;
	}
	
	private static final String SHIP_TO_STORE = "shipToStoreBOSTS";
	private static final String PICKUP_IN_STORE = "pickupInStoreBOPIS";
	private static final String BOPIS_ENABLED = "bopisEnabled";
	private static final String BOSTS_ENABLED = "bostsEnabled";
	
	/**
	 * 
	 * @param sku
	 * @return
	 */
	private boolean isSkuEligibileForBOSTSFulfillment(RepositoryItem sku){
		if(sku == null){
			return false;
		}
		Object bostsEnabled = sku.getPropertyValue(BOSTS_ENABLED);
		if (bostsEnabled != null && !(DigitalStringUtil.isEmpty((String) bostsEnabled))) {
			if ("1".equalsIgnoreCase((String) bostsEnabled) || "Y".equalsIgnoreCase((String) bostsEnabled)){
				if(isLoggingDebug()){
					logDebug("SKU is Eligibile For BOSTS Fulfillment");
				}
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param store
	 * @return
	 */
	private boolean isStoreEligibleForShipToStoreFulfillment(RepositoryItem store){
		if(store == null){
			return false;
		}
		Object shipToStore = store.getPropertyValue(SHIP_TO_STORE);
		if(shipToStore != null && !(DigitalStringUtil.isEmpty((String) shipToStore))) {
			if("1".equalsIgnoreCase((String) shipToStore) || "Y".equalsIgnoreCase((String) shipToStore)){
				if(isLoggingDebug()){
					logDebug("Store is Eligibile For Ship To Store Fulfillment");
				}
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param sku
	 * @return
	 */
	private boolean isSkuEligibileForBOPISFulfillment(RepositoryItem sku){
		if(sku == null){
			return false;
		}
		Object bopisEnabled = sku.getPropertyValue(BOPIS_ENABLED);
		if (bopisEnabled != null && !(DigitalStringUtil.isEmpty((String) bopisEnabled))) {
			if ("1".equalsIgnoreCase((String) bopisEnabled) || "Y".equalsIgnoreCase((String) bopisEnabled)){
				if(isLoggingDebug()){
					logDebug("SKU is Eligibile For BOPIS Fulfillment");
				}
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param store
	 * @return
	 */
	private boolean isStoreEligibleForPickUpInStoreFulfillment(RepositoryItem store){
		if(store == null){
			return false;
		}
		Object pickUpInStore = store.getPropertyValue(PICKUP_IN_STORE);
		if(pickUpInStore != null && !(DigitalStringUtil.isEmpty((String) pickUpInStore))) {
			if("1".equalsIgnoreCase((String) pickUpInStore) || "Y".equalsIgnoreCase((String) pickUpInStore)){
				if(isLoggingDebug()){
					logDebug("Store is Eligibile For PickUp In Store Fulfillment");
				}
				return true;
			}
		}
		
		return false;
	}

}
