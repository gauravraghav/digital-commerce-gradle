package com.digital.commerce.services.storelocator;

import com.digital.commerce.services.order.DigitalShippingGroupManager;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalStoreTools {

	DigitalShippingGroupManager dswshipgroupmgr;
	public DigitalStoreAddress fetchStoreAddress( String storeNumber) {
    	return dswshipgroupmgr.fetchStoreAddress(storeNumber);
	}
}