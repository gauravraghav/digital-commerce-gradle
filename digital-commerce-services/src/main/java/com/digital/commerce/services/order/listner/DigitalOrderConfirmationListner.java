package com.digital.commerce.services.order.listner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.order.processor.domain.OrderLine;
import com.digital.commerce.integration.order.processor.domain.PaymentMethod;
import com.digital.commerce.integration.order.processor.domain.payload.OrderConfirmedPayload;
import com.digital.commerce.integration.order.processor.domain.payload.OrderStatusPayload;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.OrderlineAttributesDTO;

public class DigitalOrderConfirmationListner extends DigitalOrderStatusListener {
	protected static final String CLASSNAME = DigitalOrderConfirmationListner.class.getName();

	@Override
	public String getListenerName() {
		return this.getClass().getName();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected HashMap<String, Object> handleEmailNotification(OrderStatusPayload payLoad, DigitalOrderImpl order,
			HashMap<String, HashMap<String, OrderlineAttributesDTO>> itemsByFullfilmentType) {
		HashMap<String, Object> extraAttributes = new HashMap<>();

		OrderConfirmedPayload orderConfirmedPayload = (OrderConfirmedPayload) payLoad;

		String customerFirstName = orderConfirmedPayload.getPersonInfoBillTo().getFirstName();

		if (DigitalStringUtil.isEmpty(customerFirstName)) {
			customerFirstName = orderConfirmedPayload.getPersonInfoShipTo().getFirstName();

		}

		extraAttributes.put("customerFirstName", customerFirstName);
		
		HashMap<String, Object> shippingAddress = new HashMap<>();
		// get Shipping Address Details
		shippingAddress.put("firstName", orderConfirmedPayload.getPersonInfoShipTo().getFirstName());
		shippingAddress.put("company", orderConfirmedPayload.getPersonInfoShipTo().getCompany());
		shippingAddress.put("addressLine1", orderConfirmedPayload.getPersonInfoShipTo().getAddressLine1());
		shippingAddress.put("addressLine2", orderConfirmedPayload.getPersonInfoShipTo().getAddressLine2());
		shippingAddress.put("city", orderConfirmedPayload.getPersonInfoShipTo().getCity());
		shippingAddress.put("state", orderConfirmedPayload.getPersonInfoShipTo().getState());
		shippingAddress.put("zipCode", orderConfirmedPayload.getPersonInfoShipTo().getZipCode());
		shippingAddress.put("country", orderConfirmedPayload.getPersonInfoShipTo().getCountry());
		shippingAddress.put("dayPhone", orderConfirmedPayload.getPersonInfoShipTo().getDayPhone());
		
		extraAttributes.put("shippingAddress", shippingAddress);

		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOPIS, itemsByFullfilmentType, extraAttributes);
		consolidateOrderSummaryAndPayload(payLoad, DigitalOrderStatusListener.BOSTS, itemsByFullfilmentType, extraAttributes);

		
		// get mall plaza name
		HashMap<String, OrderlineAttributesDTO> lines = (HashMap<String, OrderlineAttributesDTO>) extraAttributes
				.get("orderLines");

		if (lines != null) {
			Map.Entry<String, OrderlineAttributesDTO> entry = lines.entrySet().iterator().next();
			extraAttributes.put("mallPlazaName", entry.getValue().getMallPlazaName());
		}
		putPaymentMethodsDetails(payLoad, extraAttributes);
		return extraAttributes;
	}

	private void putPaymentMethodsDetails(OrderStatusPayload payLoad, HashMap<String, Object> params) {
		OrderConfirmedPayload orderConfirmedPayload = (OrderConfirmedPayload) payLoad;
		Map<String, String> paymentMethodMap = null;
		List<Map<String, String>> paymentMethodsList = new ArrayList<>();
		if (orderConfirmedPayload.getPaymentMethods() != null && orderConfirmedPayload.getPaymentMethods().size() > 0) {
			for (PaymentMethod pm : orderConfirmedPayload.getPaymentMethods()) {
				paymentMethodMap = new HashMap<>();
				paymentMethodMap.put("paymentTypeGroup", pm.getPaymentTypeGroup());
				paymentMethodMap.put("creditCardType", pm.getCreditCardType());
				paymentMethodMap.put("paymentMethodType", pm.getPaymentMethodType());
				paymentMethodMap.put("displayCreditCardNo", pm.getDisplayCreditCardNo());
				paymentMethodMap.put("totalAuthorized", pm.getTotalAuthorized());
				paymentMethodMap.put("displaySvcNo", pm.getDisplaySvcNo());
				paymentMethodsList.add(paymentMethodMap);
			}
			params.put("paymentMethods", paymentMethodsList);

		}

	}

	@Override
	protected String getOrderNo(OrderStatusPayload payLoad) {
		if (payLoad.getOmniOrderUpdate() != null && payLoad.getOmniOrderUpdate().getOrder() != null) {
			return payLoad.getOmniOrderUpdate().getOrder().getOrderNo();
		}
		return null;
	}

	@Override
	protected String getEmailTo(OrderStatusPayload payLoad) {

		return ((OrderConfirmedPayload) payLoad).getPersonInfoBillTo().getEmailId();
	}

	@Override
	protected List<OrderLine> getOrderLines(OrderStatusPayload payLoad) {
		// TODO Auto-generated method stub
		return ((OrderConfirmedPayload) payLoad).getOrderLines();
	}

}
