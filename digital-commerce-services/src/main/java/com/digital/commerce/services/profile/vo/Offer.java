package com.digital.commerce.services.profile.vo;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/** Represents a customized deal/offer based on various facets of the customer along with shopping habits.
 * 
 */
public class Offer implements Serializable {
	private static final long	serialVersionUID	= 2670020779766443067L;
	
	/* This below are coming from rewards services */
	private String				campaignId;
	private String				promotionId;
	private String				segmentId;
	private String				collateralId;
	private Date				startDate;
	private Date				endDate;
	
	/* The below are coming from promotions repository*/
	private String				promoDescription; 
	private String				promoCode;
	private String				promoStatus; // ACTIVE / INIT
	private String 				userFriendlyDescription;
	private String 				userFriendlyName;
	private String 				couponCode;
	private boolean				targetedOffer;
	private String 				discountIconType;
	private String 				shopNowLink;
	private String 				offerDetailsPageName;
	private Integer 			displayOrder; //REW-1257

	public boolean isTargetedOffer() {
		return targetedOffer;
	}

	public void setTargetedOffer(boolean targetedOffer) {
		this.targetedOffer = targetedOffer;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId( String campaignId ) {
		this.campaignId = campaignId;
	}

	public String getPromotionId() {
		return promotionId;
	}

	public void setPromotionId( String promotionId ) {
		this.promotionId = promotionId;
	}

	public String getSegmentId() {
		return segmentId;
	}

	public void setSegmentId( String segmentId ) {
		this.segmentId = segmentId;
	}

	public String getCollateralId() {
		return collateralId;
	}

	public void setCollateralId( String collateralId ) {
		this.collateralId = collateralId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate( Date startDate ) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate( Date endDate ) {
		this.endDate = endDate;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString( this );
	}

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode( this );
	}

	public boolean equals( Object object ) {
		return EqualsBuilder.reflectionEquals( this, object );
	}

	public String getPromoDescription() {
		return promoDescription;
	}

	public void setPromoDescription(String promoDescription) {
		this.promoDescription = promoDescription;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getPromoStatus() {
		return promoStatus;
	}

	public void setPromoStatus(String promoStatus) {
		this.promoStatus = promoStatus;
	}

	public String getUserFriendlyDescription() {
		return userFriendlyDescription;
	}

	public void setUserFriendlyDescription(String userFriendlyDescription) {
		this.userFriendlyDescription = userFriendlyDescription;
	}

	public String getUserFriendlyName() {
		return userFriendlyName;
	}

	public void setUserFriendlyName(String userFriendlyName) {
		this.userFriendlyName = userFriendlyName;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getDiscountIconType() {
		return discountIconType;
	}

	public void setDiscountIconType(String discountIconType) {
		this.discountIconType = discountIconType;
	}

	public String getShopNowLink() {
		return shopNowLink;
	}

	public void setShopNowLink(String shopNowLink) {
		this.shopNowLink = shopNowLink;
	}

	public String getOfferDetailsPageName() {
		return offerDetailsPageName;
	}

	public void setOfferDetailsPageName(String offerDetailsPageName) {
		this.offerDetailsPageName = offerDetailsPageName;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
}
