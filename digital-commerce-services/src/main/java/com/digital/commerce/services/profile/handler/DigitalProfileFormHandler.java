package com.digital.commerce.services.profile.handler;

import static com.digital.commerce.common.util.ComponentLookupUtil.ORDER_TOOLS;
import static com.digital.commerce.constants.DigitalProfileConstants.USPS_STATUS_ACCEPTED;
import static com.digital.commerce.constants.DigitalProfileConstants.USPS_STATUS_SUGGESTED;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.profile.CommerceProfileFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.core.util.Base64;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.dtm.UserTransactionDemarcation;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.PropertyManager;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.domain.ResponseError;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalAESEncryptionUtil;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.RequestHeaderAttributesConstant;
import com.digital.commerce.common.util.RequestParametersConstant;
import com.digital.commerce.constants.DigitalProfileConstants;
import com.digital.commerce.integration.common.communication.CommunicationService;
import com.digital.commerce.integration.common.communication.domain.Recipient;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent.ContentType;
import com.digital.commerce.integration.common.exception.DigitalIntegrationBusinessException;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.integration.addressvalidation.domain.AddressValidationServiceResponse;
import com.digital.commerce.integration.reward.bts.BtsRewardService;
import com.digital.commerce.integration.reward.domain.Person;
import com.digital.commerce.integration.reward.domain.RewardServiceRequest;
import com.digital.commerce.integration.reward.domain.RewardServiceResponse;
import com.digital.commerce.services.common.AddressType;
import com.digital.commerce.services.common.DigitalAddress;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderTools;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.payment.DigitalPaymentGroupManager;
import com.digital.commerce.services.profile.DigitalCommercePropertyManager;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.rewards.DigitalRewardsManager;
import com.digital.commerce.services.rewards.domain.RewardsUpdateCustomerRequest;
import com.digital.commerce.services.rewards.domain.RewardsUpdateCustomerResponse;
import com.digital.commerce.services.utils.DigitalAddressUtil;
import com.digital.commerce.services.validator.DigitalBasicAddressValidator;
import com.digital.commerce.services.validator.DigitalFormValidationService;
import com.digital.commerce.services.validator.DigitalUSPSAddressValidator;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.transaction.TransactionManager;

/**
 * 
 * @author yprakash
 * 
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
@Getter
@Setter
public class DigitalProfileFormHandler extends CommerceProfileFormHandler {

    private DigitalFormValidationService formValidationServices;
    private DigitalUSPSAddressValidator addressValidator;
    private MessageLocator messageLocator;
    private DigitalBasicAddressValidator dswBasicAddressValidator;

		private DigitalRewardsManager rewardsManager;

    private String alertType;
    private String alertMethodEmail;
    private String alertMethodSMS;

    private String successMessage;
    private String source;
    private boolean sendEmailOnCreateAccount;
    private String loyaltyNumber;
    private String birthMonth;
    private String birthDay;
    private String birthYear;
    private String daytimeTelephoneNumber;
    
    private String makeLoginSameAsEmail;
    private String makeEmailSameAsLogin;

    private CommunicationService communicationService;
    private Order  lastOrder;

		private String oldUserName;
    private RepeatingRequestMonitor repeatingRequestMonitor;
    private String transactionId;
  
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String OLD_USER_NAME = "oldUserName";
    private static final String LOYALTY_NUMBER = "loyaltyNumber";
    private static final String EMAIL = "email";
    private static final String HOME_ADDRESS = "homeAddress";
    private static final String ORDER_ID = "orderID";

    private static final String INVALID_EMAIL_FORMAT = "INVALID_EMAIL_FORMAT";
    private static final String INVALID_LOGIN_FORMAT = "INVALID_LOGIN_FORMAT";
    private static final String INVALID_GENDER_OPTION = "INVALID_GENDER_OPTION";
    private static final String INVALID_MOBILE_FORMAT = "INVALID_MOBILE_FORMAT";
    private static final String UPDATE_EMAIL_EXISTS = "UPDATE_EMAIL_EXISTS";
    private static final String UPDATE_LOGIN_EXISTS = "UPDATE_LOGIN_EXISTS";
    private static final String DUPLICATE_EMAIL = "DUPLICATE_EMAIL";
    private static final String INVALID_DAYTIME_PHONENUMBER = "INVALID_DAYTIME_PHONENUMBER";
    private static final String PASSWORD_EQUALS_OLDPASSWORD = "passwordEqualsOldPassword";//NOSONAR
    private static final String INVALID_LOYALTY_NUMBER = "invalidLoyaltyNumber";
    private static final String INVALID_BIRTH_DAY_MONTH = "INVALID_BIRTH_DAY_MONTH";
    private static final String ACCOUNT_ALREADY_LOCKED = "account.is.already.locked";
    private static final String ACCOUNT_IS_BEING_LOCKED = "account.is.being.locked";
    private static final String ACCOUNT_LOCK_WARNING="accountLockWarning";
    private static final String DUPILCATE_REQUEST = "duplicateRequest";
    private static final String EMPTY_OLD_PASSWORD = "emptyOldPassword";//NOSONAR
    private static final String EMPTY_PASSWORD = "emptyPassword";//NOSONAR
    private static final String PASSWORD_SAME_AS_OLD_PASSWORD = "passwordEqualsOldPassword";//NOSONAR
    private static final String GENERATED_PASSWORD="generatedPassword";//NOSONAR
    private static final String EXPIRED_TEMPORARY_PASSWORD="expiredTempPassword";//NOSONAR
    private static final String TEMP_PASSWORD_EXPIRY_DATE="tempPasswordExpiryDate";//NOSONAR
    private static final String LOGIN_PROPERTY="login";
    private static final String FIRST_NAME_PROPERTY ="firstName";
    private static final String LAST_NAME_PROPERTY="lastName";
    private static final String EMAIL_PROPERTY="email";
    private static final String USER_PROPERTY="user";
    private static final String PASSWORD_PROPERTY="password";//NOSONAR
    private static final String EMPTY_LOGIN_HASH_OR_LOGIN = "emptyLoginHashOrLogin";
    private static final String IS_PAPERLESS_CERT = "isPaperlessCert";
    private static final String DOB = "dateOfBirth";
   	private static final String CERT_DENOMINATION = "certDenomination";

    private Map<String, String> suggestedAddressMap = null;
    private String suggestedAddress;
    private boolean passwordChanged = false;
    private boolean forceUpdatePassword = false;

		private static final String ADDRESS_TYPE_INTERNATIONAL = "INTL";
    private static final String ADDRESS_TYPE_MILITARY = "MIL";
    private static final String TRACKING_DATE = "trackingDate";
    private static final String SITE_URL = "siteUrl";
    private static final String FIND_STORE_URL = "findStoreUrl";
    private static final String LOGIN_URL = "loginUrl";
    private double version=1.0;
    private String loginHash = null;
    private static final String ACCOUNT_CREATION_ERROR = "account.create.error";

		private static final String DYN_SESS_CONF = "_dynSessConf";
		private static final String JSESSION_ID = "JSESSIONID";
		private DigitalShippingGroupManager dswShippingGroupManager;
	
	/* For rewards Denomination - Start*/
    private String certDenomination;
    private boolean paperlessCert;
    /* For rewards Denomination - End*/
    private static final String PERFORM_MONITOR_NAME= "DigitalProfileFormHandler_updateSourceOfOrder";
    private static final String PERF_NAME = "method";
    static final String CLASSNAME = DigitalProfileFormHandler.class.getName();

	/**
	 * Allowed services to do session cookie cleanup
	 */
	private String[] allowedServices;

	private DigitalAESEncryptionUtil encryptUtil;
	
	/**
	 * @param pRequest
	 * @param pResponse
	 */
	protected void preLoginUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String METHOD_NAME = "preLoginUser";
		try {

			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			if (isLoggingDebug()) {
				logDebug("preLoginUser(DynamoHttpServletRequest pRequest=" + pRequest
						+ ", DynamoHttpServletResponse pResponse=" + pResponse + ") - start");
			}
			DigitalProfileTools profileTools = ((DigitalProfileTools) getProfileTools());
			PropertyManager pmgr = profileTools.getPropertyManager();
			String loginPropertyName = pmgr.getLoginPropertyName();
			String login = null;
			login = getStringValueProperty(loginPropertyName);
			// KTLO1-652
			login=DigitalStringUtil.lowerCase(login);
			Map<String, Object> returnMap = profileTools.lookupUserByLogin(login);
			boolean tempPasswordFlag = false;
			if (Boolean.TRUE.equals(returnMap.get(GENERATED_PASSWORD))) {
				tempPasswordFlag = true;
			}
			
			String oldPassword = getStringValueProperty("OLDPASSWORD");
			// Check if user is being accessed from Insole or via forgot password flow any other flow stop user being logged-in
			if (tempPasswordFlag && !profileTools.getDswConstants().isPaTool() && DigitalStringUtil.isEmpty(oldPassword) && isForceUpdatePassword()) {
				addFormException(new DropletException(this.getMessageLocator().getMessageString(GENERATED_PASSWORD),GENERATED_PASSWORD));
			} else {
				Profile p = getProfile();
				if (profileTools.isKnown(p)) {
					super.preLoginUser(pRequest, pResponse);
				}
			}
			
			int status = checkFormError(getLoginErrorURL(), pRequest, pResponse);
			if (status != 0 && !this.getFormExceptions().isEmpty()) {
				for (int i = 0; i < this.getFormExceptions().size(); i++) {
					if (((DropletException) this.getFormExceptions().get(i)).getMessage()
							.contains(DigitalProfileConstants.INVALID_LOGIN_DIFFERENT_USER)){
						resetFormExceptions();
					}
				}
			}
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
		}

	}

    /**
     * @param pRequest
     * @param pResponse
     */
	protected void postLoginUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		try {
		getProfile().setPropertyValue("nbrForgotPasswordTry", 0);
		getProfile().setPropertyValue("accountLockedTime", null);
		DigitalProfileTools profileTools = (DigitalProfileTools) this.getProfileTools();
		// Added the code for cleaning CSRPromotions Post Login.
		if (profileTools.getDswConstants().isCleanUpCSRPromotionsPostLogin()) {
			profileTools.cleanCSRPromotions(this.getProfile().getDataSource());
		}
		boolean passwordExpired = false;
		if ((getExpiredPasswordService() != null) && (getExpiredPasswordService().isEnabled())) {
			passwordExpired = getExpiredPasswordService().isPasswordExpired(getProfile());
		}
		if (passwordExpired) {
				addFormException(
						new DropletException(this.getMessageLocator().getMessageString("expiredPassword"),
					"expiredPassword"));
			if (isLoggingInfo()) {
				logInfo("Password expired for User :: " + getProfile().getPropertyValue(LOGIN_PROPERTY));
			}
		}
		super.postLoginUser(pRequest, pResponse);
		Order order = this.getShoppingCart().getCurrent();
			//TODO: update the sourceOfOrder here from the akamai headers.
		updateSourceOfOrderOnCurrentOrder();
		
			((DigitalPaymentGroupManager) this.getOrderManager().getPaymentGroupManager())
					.reapplyGiftCard(order);
		// Update the TIER cookie value based on the current loyalty tier
		profileTools.createOrUpdateTierCookie(getProfile(), pRequest, pResponse);
		}
		catch (Exception e){
			logError("Failed in post login processing ", e);
		}

		if (isLoggingInfo()) {
			logInfo("User " + getProfile().getPropertyValue(LOGIN_PROPERTY) + " successfully logged in.");
		}
	}
	
	private void updateSourceOfOrderOnCurrentOrder() {
		// TODO Auto-generated method stub
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, PERF_NAME);
		final String methodName = "originOfOrderUpdateEnabled";
		final UserTransactionDemarcation td = TransactionUtils.startNewTransaction(CLASSNAME, methodName);
		try {

			DigitalOrderImpl currentOrder = (DigitalOrderImpl) getShoppingCart().getCurrent();
			DigitalOrderTools ot = (DigitalOrderTools) ComponentLookupUtil.lookupComponent(ORDER_TOOLS, DigitalOrderTools.class);
			if (currentOrder!=null && ot !=null) {

				try {
					synchronized (currentOrder) {
						ot.determineOriginOfOrder(currentOrder);
						this.getOrderManager().updateOrder(currentOrder);
						logInfo("UPDATE_ORIGIN_OF_ORDER_POST_LOGIN ::: DONE ::: For Order ### :: "+currentOrder.getId()+ " With SourceOfOrigin Value ::: "+ currentOrder.getOriginOfOrder() );
					}
				} catch (Exception e) {
					TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
					throw e;
				} finally {
					try {
						TransactionUtils.releaseTransactionLock(CLASSNAME, methodName);
						TransactionUtils.endTransaction(td, CLASSNAME, methodName);
					} catch (Throwable th) {
						logError(th);
					}
				}
			}
		}catch (Exception e) {
			logError("Clean-up of origin of order with the order failed");
			TransactionUtils.rollBackTransaction(td, CLASSNAME, methodName);
		} finally {
			TransactionUtils.endTransaction(td, CLASSNAME, methodName);
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, PERF_NAME);
		}

	}

	/**
	 * @param pRequest
	 * @param pResponse
	 */
	@Override
	public void postLogoutUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		/*
		 * Add the locale and pushSite back to URL to get around
		 * MultiXRequestCheckerServlet
		 */
		pRequest.addQueryParameter(RequestParametersConstant.PUSH_SITE.getValue(),
				pRequest.getParameter(RequestParametersConstant.PUSH_SITE.getValue()));
		pRequest.addQueryParameter(RequestParametersConstant.LOCALE.getValue(),
				pRequest.getParameter(RequestParametersConstant.LOCALE.getValue()));

    super.postLogoutUser(pRequest, pResponse);

		String serviceURL = pRequest.getRequestURL().toString();

		boolean cleanUpOfCookiesAllowed = isCleanUpOfCookiesAllowed(serviceURL);
		if (!cleanUpOfCookiesAllowed) {
			if (isLoggingDebug()) {
				logDebug("Clean-up of session cookies is not allowed for the service: " + serviceURL);
			}
		} else {
			Cookie[] cookies = pRequest.getCookies();
			if (null != cookies) {
				for (Cookie cookie : cookies) {
					String cookieName = cookie.getName();
					if (DYN_SESS_CONF.equalsIgnoreCase(cookieName)) {
						cookie.setMaxAge(0);
						cookie.setValue(null);
						cookie.setPath("/");
						pResponse.addCookie(cookie);
					}
					if (JSESSION_ID.equalsIgnoreCase(cookieName)) {
						cookie.setMaxAge(0);
						cookie.setValue(null);
						cookie.setPath("/");
						pResponse.addCookie(cookie);
					}
				}
			}
		}

		// Update the TIER cookie value based on the current loyalty tier
		((DigitalProfileTools)getProfileTools()).createOrUpdateTierCookie(getProfile(), pRequest, pResponse);
		if (isLoggingInfo()) {
			logInfo("User " + getProfile().getPropertyValue(LOGIN_PROPERTY) + " successfully logged out.");
		}
	}

	
	private void validateBirth(DynamoHttpServletRequest pRequest)
	{
		boolean blankBirthDay = DigitalStringUtil.isBlank(birthDay);
		boolean blankBirthMonth = DigitalStringUtil.isBlank(birthMonth);

		if ((blankBirthDay && !blankBirthMonth) || (!blankBirthDay && blankBirthMonth) || 
				(!DigitalDateUtil.validateDOB(this.getBirthDay(),this.getBirthMonth(),this.getBirthYear()) 
				&& (!blankBirthDay && !blankBirthMonth)) ) {
			// Either birthDay or birthMonth is blank/empty but not both
			addFormException(new DropletException(this.getMessageLocator().getMessageString(INVALID_BIRTH_DAY_MONTH),
					INVALID_BIRTH_DAY_MONTH));
		} else {
			// Both birthDay and birthMonth are either populated with valid date or blank/empty
			// (valid case)
			setDateOfBirth(pRequest);
		}
	}
	
    /**
     * @param pRequest
     * @param pResponse
     */
	protected void preUpdateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
    	passwordChanged = false;
		if (((DigitalProfileTools)this.getProfileTools()).getDswConstants().isRewardsCacheEnabled() && null != pRequest) {
			pRequest.setParameter("cacheRewardsCalls", "Y");
		}
    	validateBirth(pRequest);

		String email = (String) this.getValue().get("email");
		String login = (String) this.getValue().get(LOGIN_PROPERTY);
		String password = (String) this.getValue().get(PASSWORD_PROPERTY);
		//KTLO1-652
		email=DigitalStringUtil.lowerCase(email);
		//KTLO1-652		
		login=DigitalStringUtil.lowerCase(login);


		if (email != null) {
			if (!this.getFormValidationServices().validateEmailAddress(email)) {
				if (isLoggingInfo()) {
					logInfo("Email id " + email + " supplied by user : " + getProfile().getPropertyValue(LOGIN_PROPERTY)
							+ " is invalid. ");
				}
				addFormException(new DropletException(this.getMessageLocator().getMessageString(INVALID_EMAIL_FORMAT),
						INVALID_EMAIL_FORMAT));

			}
		}
		// Validate Login
		// Is there any validation rules for login?
		else if (login != null) {
			if (!this.getFormValidationServices().validateEmailAddress(login)) {
				if (isLoggingInfo()) {
					logInfo("login id " + email + " supplied  by user : " + getProfile().getPropertyValue(LOGIN_PROPERTY)
							+ " is invalid. ");
				}
				addFormException(new DropletException(this.getMessageLocator().getMessageString(INVALID_LOGIN_FORMAT),
						INVALID_LOGIN_FORMAT));
			}
		}

		if (password != null) {

			// Get the hashed Password from UI.

			String passwordSalt = this.getProfileTools().passwordSaltForLogin(login);
			if (DigitalStringUtil.isBlank(passwordSalt)) {
				passwordSalt = (String) this.getProfile().getPropertyValue(LOGIN_PROPERTY);
				if (isLoggingDebug()) {
					logDebug("dsw basic password validation - passwordSalt from Login : " + passwordSalt);
				}
			}
			String currentPassword = (String) this.getProfile().getPropertyValue(PASSWORD_PROPERTY);
			String hashedPassword = null;
			if (passwordSalt != null) {
				hashedPassword = this.getProfileTools().getPropertyManager().generatePassword(passwordSalt, password);
			}

			// If the new Password from UI is different from currentPassword
			// then only do the following.
			if (hashedPassword != null && !hashedPassword.equalsIgnoreCase(currentPassword)) {
				if (this.getProfileTools().getPasswordRuleChecker() != null
						&& this.getProfileTools().getPasswordRuleChecker().isEnabled()) {
					boolean passedRules = this.getProfileTools().getPasswordRuleChecker().checkRules(password, null);
					if (!passedRules) {
						if (isLoggingInfo()) {
							logInfo("Password :" + password + " supplied by user "
									+ getProfile().getPropertyValue(LOGIN_PROPERTY) + "is invalid. ");
						}
						String lastRuleCheckedErrorCode = this.getProfileTools().getPasswordRuleChecker()
								.getLastRuleCheckedDescription();
						addFormException(new DropletException(lastRuleCheckedErrorCode, lastRuleCheckedErrorCode));
					} else if (!getFormError()) { // Set the currentPassword to
						// the password History.
						try {
							String generatedPwdPropertyName = getProfileTools().getPropertyManager()
									.getGeneratedPasswordPropertyName();
							boolean generatedPasswordFlg = (boolean) this.getProfile()
									.getPropertyValue(generatedPwdPropertyName);
							MutableRepository pProfile = getProfileTools().getProfileRepository();
							MutableRepositoryItem mutItem = null;

							if (generatedPasswordFlg) {
								try {
									mutItem = pProfile.getItemForUpdate(getProfile().getRepositoryId(),
											getProfile().getItemDescriptor().getItemDescriptorName());
									mutItem.setPropertyValue(generatedPwdPropertyName, Boolean.FALSE);
									this.getValue().put(GENERATED_PASSWORD, false);
								} catch (RepositoryException e) {
									this.logError("error setting generated flag", e);
								}
							}
							this.getValue().put("lastPasswordUpdate", Calendar.getInstance().getTime());
							this.getValue().put("lastActivity", Calendar.getInstance().getTime());

							this.getProfileTools().getPreviousNPasswordManager().updatePreviousPasswordsProperty(
									(MutableRepositoryItem) this.getProfileItem(), currentPassword);
							passwordChanged = true;

						} catch (Exception e) {
							if (isLoggingError()) {
								logError("Unable to update PreviousPasswordsProperty" +e);
							}
						}

					}

				}

			} else {
				if (isLoggingInfo()) {
					logInfo("Password :" + password + " supplied by user " + getProfile().getPropertyValue(LOGIN_PROPERTY)
							+ "is same as old password. ");
				}
				addFormException(
						new DropletException(this.getMessageLocator().getMessageString(PASSWORD_EQUALS_OLDPASSWORD),
								PASSWORD_EQUALS_OLDPASSWORD));
			}

		}

		String gender = (String) this.getValue().get("gender");
		if (!DigitalStringUtil.isEmpty(gender)
				&& (!"male".equalsIgnoreCase(gender) && !"female".equalsIgnoreCase(gender) && !"unknown".equalsIgnoreCase(gender))) {
			if (isLoggingInfo()) {
				logInfo("User " + getProfile().getPropertyValue(LOGIN_PROPERTY) + "entered blank gender. ");
			}
			addFormException(new DropletException(this.getMessageLocator().getMessageString(INVALID_GENDER_OPTION),
					INVALID_GENDER_OPTION));
		}

		Object obj = this.getValueProperty("homeAddress");
		if (obj != null) {
			DigitalContactInfo contactInfo = this.populateContactInfo("homeAddress");
			// Don't do USPS address validation in case of suggested address
			// being selected.
			if (DigitalStringUtil.isBlank(suggestedAddress)) {
				validateAddress(contactInfo);
			}
		}

		Object shippingObj = this.getValueProperty("shippingAddress");
		if (shippingObj != null) {
			DigitalContactInfo contactInfo = this.populateContactInfo("shippingAddress");
			// Don't do USPS address validation in case of suggested address
			// being selected.
			if (DigitalStringUtil.isBlank(suggestedAddress)) {
				validateAddress(contactInfo);
			}
		}

		String mobilePhoneNumber = (String) this.getValue().get("mobilePhoneNumber");
		if (mobilePhoneNumber != null) {
			if (!this.getFormValidationServices().validateMobilePhoneNumber(mobilePhoneNumber)) {
				if (isLoggingInfo()) {
					logInfo("User " + getProfile().getPropertyValue(LOGIN_PROPERTY)
							+ "provided invalid mobile phone number :. " + mobilePhoneNumber);
				}
				addFormException(new DropletException(this.getMessageLocator().getMessageString(INVALID_MOBILE_FORMAT),
						INVALID_MOBILE_FORMAT));
			}

			if ("".equalsIgnoreCase(mobilePhoneNumber)) {
				MutableRepositoryItem profileItem = (MutableRepositoryItem) getProfileItem();
				profileItem.setPropertyValue("mobilePhoneNumber", "");
			}
		}

		String daytimePhoneNumber = (String) this.getValue().get("daytimeTelephoneNumber");
		if (daytimePhoneNumber != null) {
			if (!this.getFormValidationServices().validateMobilePhoneNumber(daytimePhoneNumber)) {
				addFormException(
						new DropletException(this.getMessageLocator().getMessageString(INVALID_DAYTIME_PHONENUMBER),
								INVALID_DAYTIME_PHONENUMBER));
			}

			MutableRepositoryItem profileHomeAddressItem = (MutableRepositoryItem) getProfileItem()
					.getPropertyValue("homeAddress");

			if ("".equalsIgnoreCase(daytimePhoneNumber)) {
				profileHomeAddressItem.setPropertyValue("phoneNumber", "");
			} else {
				profileHomeAddressItem.setPropertyValue("phoneNumber", daytimePhoneNumber);
				if (isLoggingInfo()) {
					logInfo(" Phone number :. " + daytimePhoneNumber + "updated for User "
							+ getProfile().getPropertyValue(LOGIN_PROPERTY));
				}
			}
		}

		// The corresponding flag will passed when the user tries to update the
		// contactInfo section having email

		if (this.getMakeLoginSameAsEmail() != null && "true".equalsIgnoreCase(this.getMakeLoginSameAsEmail())) {
			this.getValue().put(LOGIN_PROPERTY, email);
			login = email;
		}
		
		if (this.getMakeEmailSameAsLogin() != null && "true".equalsIgnoreCase(this.getMakeEmailSameAsLogin())) {
			this.getValue().put("email", login);
			email = login;
		}

		// Check for duplicate email Existing:
		if (email != null && !email.equalsIgnoreCase((String) this.getProfile().getPropertyValue("email"))) {
			RepositoryItem profileItem = this.getProfileTools().getItemFromEmail(email);
			if (profileItem != null) {
				if (isLoggingInfo()) {
					logInfo("User " + getProfile().getPropertyValue(LOGIN_PROPERTY) + "provided existing email : " + email);
				}
				addFormException(new DropletException(this.getMessageLocator().getMessageString(UPDATE_EMAIL_EXISTS),
						UPDATE_EMAIL_EXISTS));
			}
		}

		// Check for duplicate login Existing:
		if (login != null && !login.equalsIgnoreCase((String) this.getProfile().getPropertyValue(LOGIN_PROPERTY))) {
			Profile profile = new Profile();
			if (this.getProfileTools().locateUserFromLogin(login, profile, USER_PROPERTY)) {
				if (isLoggingInfo()) {
					logInfo("User " + getProfile().getPropertyValue(LOGIN_PROPERTY) + "provided existing login : " + login);
				}
				addFormException(new DropletException(this.getMessageLocator().getMessageString(UPDATE_LOGIN_EXISTS),
						UPDATE_LOGIN_EXISTS));
			}
			// If login has changed set the oldUserName to login or email. Email
			// for migrated users not having a valid login(Without @)
			else {
				String oldUName = (String) this.getProfile().getPropertyValue(LOGIN_PROPERTY);
				if (this.getFormValidationServices().validateEmailAddress(oldUName)) {
					this.setOldUserName(oldUName);
				} else {
					this.setOldUserName((String) this.getProfile().getPropertyValue("email"));
				}
			}
		}
		if (!this.getFormError()) {
			try {
				if (DigitalStringUtil
						.isNotBlank(((DigitalProfileTools) getProfileTools()).getLoyaltyNumber(getProfile()))) {
					updateRewardMember(pRequest);
				}
			} catch (RepositoryException e) {
				logError("Error while retrieving User Details", e);
			}
			super.preUpdateUser(pRequest, pResponse);
		}

	}
    
    /** 
     * Operation to handle the change password functionality.
	 * 
	 * @param pRequest
	 *            the servlet's request
	 * @param pResponse
	 *            the servlet's response
	 * @exception ServletException
	 *                if there was an error while executing the code
	 * @exception IOException
	 *                if there was an error with servlet io 
	 */
	protected void preChangePassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		ProfileTools ptools = getProfileTools();
		PropertyManager pmgr = ptools.getPropertyManager();
		String loginPropertyName = pmgr.getLoginPropertyName();
		String passwordPropertyName = pmgr.getPasswordPropertyName();
		MutableRepositoryItem userItem=null;

		try {
			boolean changePassword = true;
			String password = getStringValueProperty(passwordPropertyName);
			String oldPassword = getStringValueProperty("OLDPASSWORD");

			if (DigitalStringUtil.isBlank(oldPassword)) {
				oldPassword = null;
				if (isConfirmOldPassword()) {
					changePassword = false;
					if (isLoggingDebug()) {
						logDebug("handleChangePassword: missing old password");
					}
					addFormException(new DropletException(this.getMessageLocator().getMessageString(EMPTY_OLD_PASSWORD),
							EMPTY_OLD_PASSWORD));
				}
			}
			
			if (DigitalStringUtil.isBlank(password)) {
				changePassword = false;
				if (isLoggingDebug()) {
					logDebug("handleChangePassword: missing password");
				}
				addFormException(new DropletException(this.getMessageLocator().getMessageString(EMPTY_PASSWORD),
						EMPTY_PASSWORD));
			}
			
			// START KTLO1-903 : Check for emailHash in the request
			String login = null;
			login = getStringValueProperty(loginPropertyName);
			if(version > 1.0 && (DigitalStringUtil.isBlank(loginHash)  && DigitalStringUtil.isBlank(login))) {
				changePassword = false;
				if (isLoggingDebug()) {
					logDebug("handleChangePassword: missing loginHash OR login");
				}
				addFormException(new DropletException(this.getMessageLocator().getMessageString(EMPTY_LOGIN_HASH_OR_LOGIN),EMPTY_LOGIN_HASH_OR_LOGIN));
			}

			// END KTLO1-903
			if ((changePassword) && (isConfirmPassword())) {
				String confirmPassword = getStringValueProperty("CONFIRMPASSWORD");
				if ((confirmPassword == null) || ("".equals(confirmPassword)) || (!password.equals(confirmPassword))) {
					changePassword = false;
					String msg = formatUserMessage("passwordsDoNotMatch", pRequest);
					addFormException(new DropletException(msg, "passwordsDoNotMatch"));
				}
			}

			if ((changePassword) && (oldPassword != null) && (password != null) && (password.equals(oldPassword))) {
				changePassword = false;
				addFormException(
						new DropletException(this.getMessageLocator().getMessageString(PASSWORD_SAME_AS_OLD_PASSWORD),
								PASSWORD_SAME_AS_OLD_PASSWORD));
			}

			if ((changePassword) && (ptools.getPasswordRuleChecker() != null)
					&& (ptools.getPasswordRuleChecker().isEnabled())) {
				MutableRepository repository = ptools.getProfileRepository();
				MutableRepositoryItem mutableItem = repository.getItemForUpdate(getRepositoryId(),
						getCreateProfileType());

				// START KTLO1-903 : Replace plain text email in the URL with encrypted value
				// while user trying to follow through forgot password flow
				if(version > 1.0) {
					String loginDecrypt = null;
					if (null != loginHash) {
						try {
							loginDecrypt = (String) getEncryptUtil().decrypt(loginHash);
							if (null != loginDecrypt) {
								login = loginDecrypt;
								setValueProperty(loginPropertyName, login);
							}
						} catch (DigitalAppException e) {
							// TODO Auto-generated catch block
							if (isLoggingError()) {
								logError("handleChangePassword: Exception while decrypting the EMAIL for customer "
										+ e.getMessage());
							}
						}
					}
				}
				
				// END KTLO1-903 : Replace plain text email in the URL with encrypted value
				// while user trying to follow through forgot password flow
				
				if(DigitalStringUtil.isEmpty(login)){
					try {
						login = (String) DynamicBeans.getSubPropertyValue(mutableItem, loginPropertyName);
					} catch (PropertyNotFoundException exc) {
						if (isLoggingError()) {
							logError(exc);
						}
					}					
				}

				Map map = null;
				if (login != null) {
					map = new Hashtable();
					map.put(loginPropertyName, login);
				}

				boolean passedRules = ptools.getPasswordRuleChecker().checkRules(password, map);
				
				//KTLO1-1508 Forgot password rules
				if(null!=login){
					userItem = (MutableRepositoryItem) ptools.getItem(login, null, USER_PROPERTY);
				}
				if(null!=userItem){
					boolean tempPasswordFlag=(Boolean) userItem.getPropertyValue(GENERATED_PASSWORD);
					Date tempPasswordExpirayDate=(Date)userItem.getPropertyValue(TEMP_PASSWORD_EXPIRY_DATE);
					if(tempPasswordFlag && null!=tempPasswordExpirayDate){
						Date now=Calendar.getInstance().getTime();
						long difference=tempPasswordExpirayDate.getTime()-now.getTime();
						if((difference)<0){
							addFormException(new DropletException(this.getMessageLocator().getMessageString(EXPIRED_TEMPORARY_PASSWORD),EXPIRED_TEMPORARY_PASSWORD));
						}
					}
				}

				if (!passedRules) {
					changePassword = false;

					addFormException(new DropletException(
							ptools.getPasswordRuleChecker().getLastRuleCheckedDescription(), "passwordRuleCheckError"));
				}
			}

			if (changePassword && !getFormError()) {
				if(null!=userItem){
					userItem.setPropertyValue(TEMP_PASSWORD_EXPIRY_DATE, null);
				}
				DigitalProfileTools profileTools = (DigitalProfileTools) getProfileTools();
				final boolean loggedIn = profileTools.isLoggedIn(getProfile());
				
				if(null!=userItem){
					//reset the account lock values as soon as you found that customer triggered forgot password flow
					userItem.setPropertyValue("nbrForgotPasswordTry", 0);
					userItem.setPropertyValue("accountLockedTime", null);
				}

				if (!loggedIn) {
					this.setValueProperty(PASSWORD_PROPERTY, oldPassword);
					this.handleLogin(pRequest, pResponse);
					this.setValueProperty( "oldPassword", oldPassword );
					this.setValueProperty( PASSWORD_PROPERTY, password );
				}

				super.preChangePassword(pRequest, pResponse);
			}
		} catch (RepositoryException exc) {
			String msg = formatUserMessage("errorUpdatingProfile", pRequest);
			addFormException(new DropletException(msg, exc, "errorUpdatingProfile"));
			if (isLoggingError()) {
				logError(exc);
			}
		}
	}
    
	/** 
     * Operation to handle the change password functionality.
	 * 
	 * @param request
	 *            the servlet's request
	 * @param response
	 *            the servlet's response
	 * @exception ServletException
	 *                if there was an error while executing the code
	 * @exception IOException
	 *                if there was an error with servlet io 
	 */
	@Override
	public boolean handleChangePassword(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws ServletException, IOException {
		final String METHOD_NAME = "DigitalProfileFormHandler.handleChangePassword";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
		boolean retVal = false;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		try {
			if ((rrm == null) || (rrm.isUniqueRequestEntry(METHOD_NAME))) {
				retVal = super.handleChangePassword(request, response);				
			} else {
				addFormException(new DropletException(this.getMessageLocator().getMessageString(DUPILCATE_REQUEST),
						DUPILCATE_REQUEST));
			}
		} finally {
			if (rrm != null) {
				rrm.removeRequestEntry(METHOD_NAME);
			}
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
		}
		return retVal;
	}

	/**
	 * @param pRequest
	 * @param memberId
	 * @throws RepositoryException
	 */
	private void updateRewardMemberByLoyaltyNumber(DynamoHttpServletRequest pRequest, String memberId)
			throws RepositoryException {

		if (DigitalStringUtil.isNotEmpty(memberId)
				&& DigitalStringUtil.isNotEmpty((String) this.getValue().get(FIRST_NAME))
				&& DigitalStringUtil.isNotEmpty((String) this.getValue().get(LAST_NAME))
				|| DigitalStringUtil.isNotEmpty((String) this.getValue().get(EMAIL))
				|| (null != this.getValue().get(HOME_ADDRESS))
				|| (null != this.getValue().get(CERT_DENOMINATION))
				|| (null != this.getValue().get(IS_PAPERLESS_CERT))
				|| (null != this.getValue().get(DOB))) {

			RewardsUpdateCustomerRequest rewardsUpdateCustomerRequest = new RewardsUpdateCustomerRequest();
			rewardsUpdateCustomerRequest.setLoyaltyNumber(memberId);
			this.populateRewardsUpdateCustomerRequest(pRequest, rewardsUpdateCustomerRequest);

			RewardsUpdateCustomerResponse response = this.getRewardsManager()
					.updateCustomerByMemberId(rewardsUpdateCustomerRequest, false);
			if (!response.isRequestSuccess()) {
				handleUpdateCustomerErrorResponse(response);
			} else {
				this.setSuccessMessage(
						this.getMessageLocator().getMessageString(DigitalProfileConstants.UPDATE_REWARD_SUCCESS));
			}
		}
	}

	/**
	 * @param response
	 */
	private void handleUpdateCustomerErrorResponse(RewardsUpdateCustomerResponse response) {
		List<ResponseError> exceptionList = response.getGenericExceptions();
		for (ResponseError error : exceptionList) {
			String errorMessage = error.getLocalizedMessage();
			if ("600".equalsIgnoreCase(error.getErrorCode()) ||
					errorMessage.contains("No Customer Found")) {
				// Do nothing to handle non members allow ATG profile update
			} else if ("602".equalsIgnoreCase(error.getErrorCode()) ||
					errorMessage.contains("Email is already in use")) {
				addFormException(new DropletException(
						this.getMessageLocator()
								.getMessageString(
										DigitalProfileConstants.UPDATE_REWARD_FAILURE_EXISTING_EMAIL_ID),
						DigitalProfileConstants.UPDATE_REWARD_FAILURE_EXISTING_EMAIL_ID));
			} else {
				addFormException(new DropletException(
						this.getMessageLocator()
								.getMessageString(
										DigitalProfileConstants.UPDATE_REWARD_USER_FAILURE),
						DigitalProfileConstants.UPDATE_REWARD_USER_FAILURE));
			}
			logError("Unable to Update the Rewards Customer :- " + errorMessage);
		}
	}

	/**
	 * @param pRequest
	 * @throws RepositoryException
	 */
	private void updateRewardMember(DynamoHttpServletRequest pRequest)
			throws RepositoryException {

		if (!DigitalStringUtil.isEmpty((String) this.getValue().get(FIRST_NAME))
				&& !DigitalStringUtil.isEmpty((String) this.getValue().get(LAST_NAME))
				|| !DigitalStringUtil.isEmpty((String) this.getValue().get(EMAIL))
				|| (null != this.getValue().get(HOME_ADDRESS))
				|| (null != this.getValue().get(CERT_DENOMINATION))
				|| (null != this.getValue().get(IS_PAPERLESS_CERT))
				|| (null != this.getValue().get(DOB))) {

			RewardsUpdateCustomerRequest rewardsUpdateCustomerRequest = new RewardsUpdateCustomerRequest();

			this.populateRewardsUpdateCustomerRequest(pRequest, rewardsUpdateCustomerRequest);

			RewardsUpdateCustomerResponse response = this.getRewardsManager()
					.updateCustomer(rewardsUpdateCustomerRequest, false);
			if (!response.isRequestSuccess()) {
				handleUpdateCustomerErrorResponse(response);
			} else {
				this.setSuccessMessage(
						this.getMessageLocator().getMessageString(DigitalProfileConstants.UPDATE_REWARD_SUCCESS));
				Profile profile = getProfile();
				if ((null != profile)) {
					((DigitalProfileTools) getProfileTools())
							.loadLoyaltyMemberIntoProfile(profile, true);
				}
			}
		}
	}
    
	/**
	 * Operation to handle the change password functionality.
	 * 
	 * @param pRequest
	 *            the servlet's request
	 * @param pResponse
	 *            the servlet's response
	 * @exception ServletException
	 *                if there was an error while executing the code
	 * @exception IOException
	 *                if there was an error with servlet io
	 */
	@Override
	protected void postChangePassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		super.postChangePassword(pRequest, pResponse);
		logInfo("Temp Password changed to permanent for customer");
		sendChangePasswordConfirmationEmail();
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see atg.userprofiling.ProfileFormHandler#postUpdateUser(atg.servlet.DynamoHttpServletRequest,
	 *      atg.servlet.DynamoHttpServletResponse)
	 * 
	 */
	@Override
	protected void postUpdateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		super.postUpdateUser(pRequest, pResponse);
		if (!DigitalStringUtil.isEmpty(oldUserName)) {
			try {
				String newUserName = (String) getProfile().getPropertyValue(LOGIN_PROPERTY);
				logInfo("Customer login changed from " + oldUserName + " to " + newUserName);
				List<Recipient> recipients = new ArrayList<>();
				Recipient recipient = new Recipient();
				recipient.setAddress(oldUserName);
				recipient.setRecipientType(Recipient.RecipientType.TO);
				recipients.add(recipient);
				Map dataTemplate = new HashMap();
				dataTemplate.put(TRACKING_DATE, DigitalDateUtil.getOperationEmailsTrackingDate());
				dataTemplate.put(FIRST_NAME, this.getProfile().getPropertyValue(FIRST_NAME));
				dataTemplate.put(OLD_USER_NAME, oldUserName);
				dataTemplate.put("newUserName", newUserName);
				dataTemplate.put(SITE_URL, MultiSiteUtil.getSiteURL());
				dataTemplate.put(FIND_STORE_URL, MultiSiteUtil.getSiteURL() + MultiSiteUtil.getStoreLocatorURI());
				dataTemplate.put(LOGIN_URL, MultiSiteUtil.getSiteURL() + MultiSiteUtil.getSignInURI());
				Site site = MultiSiteUtil.getSite();
				String siteId = null;
				if (site != null) {
					siteId = site.getId();
				}
				communicationService.send(recipients, "UsernameChange", dataTemplate, ContentType.HTML, siteId);
				if ((getFormExceptions().isEmpty()) && isLoggingInfo()) {
					if (null != this.getValueProperty(LOGIN_PROPERTY)) {
						logInfo("UserName for User " + getProfile().getPropertyValue(LOGIN_PROPERTY)
								+ " has been successfully updated");
					}
					if (null != this.getValueProperty(PASSWORD_PROPERTY)) {
						logInfo("Password for User " + getProfile().getPropertyValue(LOGIN_PROPERTY)
								+ " has been successfully updated");
					}
					if (null != this.getValueProperty("email")) {
						logInfo("Email for User " + getProfile().getPropertyValue(LOGIN_PROPERTY)
								+ " has been successfully updated");
					}
				}
			} catch (DigitalIntegrationException de) {
				logError("DigitalIntegrationException occured during post update username change send email ", de);
			}
		}
		
		if (passwordChanged) {
			logInfo("Password changed for customer from profile");
			sendChangePasswordConfirmationEmail();
		}
	}
	
	/**
	 * Send confirmation email when temp password is changed by user to permanent
	 * 
	 */
	private void sendChangePasswordConfirmationEmail() {
		// Build data for operational email for PasswordChanged
		String email = (String) this.getProfile().getPropertyValue(EMAIL);
		logInfo("Password changed for customer " + getProfile().getPropertyValue(LOGIN_PROPERTY));
		if (!DigitalStringUtil.isEmpty(email) && !DigitalStringUtil.isBlank(email)) {
			try {
				// Get recipient
				List<Recipient> recipients = new ArrayList<>();
				Recipient recipient = new Recipient();
				recipient.setAddress(email);
				recipient.setRecipientType(Recipient.RecipientType.TO);
				recipients.add(recipient);

				// Get site id
				Site site = MultiSiteUtil.getSite();
				String siteId = null;
				if (site != null) {
					siteId = site.getId();
				} else {
					siteId = SiteContextManager.getCurrentSiteId();
				}

				// Add data to template
				Map dataTemplate = new HashMap();
				dataTemplate.put(TRACKING_DATE, DigitalDateUtil.getOperationEmailsTrackingDate());
				dataTemplate.put(EMAIL, email);
				dataTemplate.put(FIRST_NAME, this.getProfile().getPropertyValue(FIRST_NAME));
				dataTemplate.put(SITE_URL, MultiSiteUtil.getSiteURL());
				dataTemplate.put(FIND_STORE_URL, MultiSiteUtil.getSiteURL());
				dataTemplate.put(LOGIN_URL, MultiSiteUtil.getSiteURL() + MultiSiteUtil.getSignInURI());

				if (isLoggingDebug()) {
					logDebug("Sending PasswordChanged email to " + recipient.getAddress());
				}
				// Send email to the customer that changed their password
				// successfully
				communicationService.send(recipients, "PasswordChanged", dataTemplate, ContentType.HTML, siteId);
			} catch (DigitalIntegrationException de) {
				logError("DigitalIntegrationException occured during post change password change send email ", de);
			}
		}

	}

	/**
	 * @param pRequest
	 * @param pResponse
	 */
	@Override
	public boolean handleUpdate(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "DigitalProfileFormHandler.handleUpdate";
		boolean flag = true;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			TransactionManager tm = super.getTransactionManager();
			TransactionDemarcation td = super.getTransactionDemarcation();
			boolean rollBack = false;
			final String METHOD_NAME = "handleUpdate";
			try {

				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
				TransactionUtils.acquireTransactionLock(CLASSNAME, METHOD_NAME);
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}

				Object obj = this.getValueProperty("homeAddress");

				Dictionary dictionary = this.getValue();
				Dictionary homeAddress = (Dictionary) dictionary.get("homeAddress");
				// START KTLO1-652: Change login and email to lowercase
				Object emailObj = dictionary.get("email");
				if(null != emailObj && DigitalStringUtil.isNotBlank(emailObj.toString()))  {
					dictionary.put("email", DigitalStringUtil.lowerCase(emailObj.toString()));
				}
				Object loginObj = dictionary.get("login");
				if(null != loginObj && DigitalStringUtil.isNotBlank(loginObj.toString())) {
					dictionary.put("login", DigitalStringUtil.lowerCase(loginObj.toString()));
				}
				// END KTLO1-652
				
				if (obj != null && null != homeAddress) {
					String address1 = (String) this.getValueProperty("homeAddress.address1");
					String address2 = (String) this.getValueProperty("homeAddress.address2");
					String address3 = (String) this.getValueProperty("homeAddress.address3");
					Boolean isPOBox = ((DigitalProfileTools) this.getProfileTools()).isPOBOX(address1, address2, address3);
					if (isPOBox != null) {
						homeAddress.put("isPoBox", isPOBox);
					}
				}

				Object shippingObj = this.getValueProperty("shippingAddress");
				if (shippingObj != null) {
					String address1 = (String) this.getValueProperty("shippingAddress.address1");
					String address2 = (String) this.getValueProperty("shippingAddress.address2");
					String address3 = (String) this.getValueProperty("shippingAddress.address3");
					Boolean isPOBox = ((DigitalProfileTools) this.getProfileTools()).isPOBOX(address1, address2, address3);
					if (isPOBox != null) {
						Dictionary shippingAddress = (Dictionary) dictionary.get("shippingAddress");
						shippingAddress.put("isPoBox", isPOBox);

					}
				}

				if (!DigitalStringUtil.isEmpty(this.getAlertMethodEmail())
						&& !DigitalStringUtil.isEmpty(this.getAlertMethodSMS())) {
					updateAlertPreference(pRequest);
				}

				flag = super.handleUpdate(pRequest, pResponse);

			} catch (IOException ie) {
				logError("IOException occured during update ", ie);
				rollBack = true;
			} catch (TransactionDemarcationException e) {
				throw new ServletException(e);
			} finally {
				try {
					if (tm != null) {
						td.end(rollBack);
					}
				} catch (TransactionDemarcationException e) {
					logError("TransactionDemarcationException occured during update ", e);
				}
				TransactionUtils.releaseTransactionLock(CLASSNAME, METHOD_NAME);
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			}
		}
		return flag;
	}

	/**
	 * @param pRequest
	 * @param pResponse
	 */
	@Override
	protected void preCreateUser(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		//check whether cookied or logged in customer is trying to register
		if (!((DigitalProfileTools) getProfileTools()).isDSWAnanymousUser(this.getProfile())) {
			if (isLoggingDebug()) {
				logDebug("Trying to create profile for already existing user id# " + this.getProfile()
						.getRepositoryId());
			}
			addFormException(new DropletException(
					this.getMessageLocator().getMessageString(ACCOUNT_CREATION_ERROR),
					ACCOUNT_CREATION_ERROR));
		} else {
			validateBirth(pRequest);

			String loginUsername = pRequest.getParameter(LOGIN_PROPERTY);
			if(DigitalStringUtil.isNotEmpty(loginUsername)) {
				this.getValue()
						.put(LOGIN_PROPERTY, loginUsername.trim().toLowerCase());
			}

			String email = (String) this.getValue().get(EMAIL_PROPERTY);

			if (DigitalStringUtil.isEmpty(email) && DigitalStringUtil.isNotEmpty(source)
					&& "stores".equalsIgnoreCase(source)) {
				String firstName = pRequest.getParameter(FIRST_NAME_PROPERTY);
				if(DigitalStringUtil.isNotEmpty(firstName)) {
					this.getValue()
							.put(FIRST_NAME_PROPERTY, firstName.trim());
				}
				
				String lastName = pRequest.getParameter(LAST_NAME_PROPERTY);
				if(DigitalStringUtil.isNotEmpty(lastName)) {
					this.getValue().put(LAST_NAME_PROPERTY, lastName.trim());
				}

				if(null != this.getValue().get(LOGIN_PROPERTY)) {
					email = (String) this.getValue().get(LOGIN_PROPERTY);
					this.getValue().put(EMAIL_PROPERTY, email);
				}

				if (DigitalStringUtil.isEmpty(getLoyaltyNumber())) {
					addFormException(new DropletException(
							this.getMessageLocator()
									.getMessageString(DigitalProfileConstants.MSG_INVALID_LOYALTY_NUMBER),
							DigitalProfileConstants.MSG_INVALID_LOYALTY_NUMBER));
				}
			}

			if (!this.getFormError() && !this.getFormValidationServices().validateEmailAddress(email)) {
				addFormException(
						new DropletException(((DigitalProfileTools) this.getProfileTools()).getMessageLocator()
								.getMessageString(INVALID_EMAIL_FORMAT), INVALID_EMAIL_FORMAT));
			}

			if (!this.getFormError()) {
				email = StringUtils.toLowerCase(email);
			}

			// To do validate the firstName
			String firstName = (String) this.getValue().get(FIRST_NAME);
		if (DigitalStringUtil.isEmpty(firstName) && !getSource().equalsIgnoreCase("orderconfirmation")) {
				addFormException(new DropletException(
						this.getMessageLocator().getMessageString(DigitalProfileConstants.FIRST_NAME_REQUIRED),
						DigitalProfileConstants.FIRST_NAME_REQUIRED));
			}
			String lastName = (String) this.getValue().get(LAST_NAME);
		if (DigitalStringUtil.isEmpty(lastName) && !getSource().equalsIgnoreCase("orderconfirmation")) {
				addFormException(new DropletException(
						this.getMessageLocator().getMessageString(DigitalProfileConstants.LAST_NAME_REQUIRED),
						DigitalProfileConstants.LAST_NAME_REQUIRED));
			}

			String mobilePhoneNumber = (String) this.getValue().get("mobilePhoneNumber");
			if (mobilePhoneNumber != null
					&& !this.getFormValidationServices().validateMobilePhoneNumber(mobilePhoneNumber)) {
				addFormException(
						new DropletException(this.getMessageLocator().getMessageString(INVALID_MOBILE_FORMAT),
								INVALID_MOBILE_FORMAT));
			}
			String gender = (String) this.getValue().get("gender");
			if (!DigitalStringUtil.isEmpty(gender)
					&& (!"male".equalsIgnoreCase(gender) && !"female".equalsIgnoreCase(gender) && !"unknown"
					.equalsIgnoreCase(gender))) {
				addFormException(
						new DropletException(this.getMessageLocator().getMessageString(INVALID_GENDER_OPTION),
								INVALID_GENDER_OPTION));
			}

			Object obj = this.getValueProperty("homeAddress");
			if (obj != null) {
				DigitalContactInfo contactInfo = this.populateContactInfo("homeAddress");

				// Don't do USPS address validation in case of suggested address
				// being selected.
				if (DigitalStringUtil.isBlank(suggestedAddress)) {
					validateAddress(contactInfo);
				}
			}

			// Check for duplicate email Existing:
			RepositoryItem profileItem = this.getProfileTools().getItemFromEmail(email);
			if (profileItem != null) {
				addFormException(
						new DropletException(this.getMessageLocator().getMessageString(DUPLICATE_EMAIL),
								DUPLICATE_EMAIL));
			}

			if (source != null && "stores".equalsIgnoreCase(source)
					&& this.getValue().get(LOGIN_PROPERTY) != null) {
				DigitalProfileTools profileTools = ((DigitalProfileTools) getProfileTools());
				String randomPassword = profileTools.getDswConstants().generateRandomPassword();
				this.getValue().put(PASSWORD_PROPERTY, randomPassword);
				this.getValue().put(GENERATED_PASSWORD, true);
				if (getLoyaltyNumber() != null) {
					this.getValue().put("loyaltyNumber", getLoyaltyNumber());
				} else {
					this.logError("Unable to Add or Update Customer as loyaltyNumber is Empty");
					addFormException(new DropletException(
							this.getMessageLocator().getMessageString(INVALID_LOYALTY_NUMBER),
							INVALID_LOYALTY_NUMBER));
				}

				setTransactionId(Base64.encodeToString(randomPassword));
			}
		}
		if (!this.getFormError()) {
			super.preCreateUser(pRequest, pResponse);
		}
	}

	/**
	 * @param pRequest
	 * @param pResponse
	 */
	@Override
	protected void postCreateUser(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		// Only if the request is not coming from instore-sign up call rewards
		if (!("stores".equalsIgnoreCase(source))) {
			if (!saveCustomer(pRequest)) {
				this.setSuccessMessage(
						this.getMessageLocator().getMessageString(DigitalProfileConstants.CREATE_REWARD_FAILURE));
				this.logError(
						"Unable to Add or Update Customer in Rewards System although User is Created.");
			} else {
				this.setSuccessMessage(
						this.getMessageLocator().getMessageString(DigitalProfileConstants.CREATE_REWARD_SUCCESS));
			}
		}
		super.postCreateUser(pRequest, pResponse);
		// Update the TIER cookie value based on the current loyalty tier
		((DigitalProfileTools) getProfileTools())
				.createOrUpdateTierCookie(getProfile(), pRequest, pResponse);

		// Start the Logic for Sending email.
		if (isSendEmailOnCreateAccount() || (MultiSiteUtil.isSendEmailOnCreateAccount() && DigitalStringUtil
				.isBlank(source))) {
			try {

				List<Recipient> recipients = new ArrayList<>();

				Recipient recipient = new Recipient();
				recipient.setAddress((String) this.getValue().get("email"));
				recipient.setRecipientType(Recipient.RecipientType.TO);
				recipients.add(recipient);
				Map dataTemplate = new HashMap();
				dataTemplate.put(FIRST_NAME, this.getProfile().getPropertyValue(FIRST_NAME));
				dataTemplate.put(LOYALTY_NUMBER, this.getProfile().getPropertyValue(LOYALTY_NUMBER));
				Site site = MultiSiteUtil.getSite();
				String siteId = null;
				if (site != null) {
					siteId = site.getId();
				}
				communicationService
						.send(recipients, "CreateAccount", dataTemplate, ContentType.HTML, siteId);
			} catch (Exception de) {
				logError("Error occured in Sending email post user creation " + de);
			}
		}
	}

	/**
	 * @param pRequest
	 * @param pResponse
	 */
	@Override
	public boolean handleCreate(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "DigitalProfileFormHandler.handleCreate";
		boolean flag = true;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			TransactionManager tm = super.getTransactionManager();
			TransactionDemarcation td = super.getTransactionDemarcation();
			boolean rollBack = false;
			final String METHOD_NAME = "handleCreate";
			try {

				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);

				if (isLoggingDebug()) {
					logDebug("Trying to create profile for new user id# " + this.getProfile().getRepositoryId());
				}

				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}

				// Handle Create account flow from orderconfirmation page
				// Copy the Shipping address from the Last Order to the Home address of this user's profile
				if (getSource().equalsIgnoreCase("orderconfirmation")) {
					Order lastOrder = getLastOrder();
					Boolean orderError = false;
					if (this.getValue().get(ORDER_ID) != null && (lastOrder == null || ( lastOrder != null && !lastOrder.getId().equalsIgnoreCase(this.getValue().get(ORDER_ID).toString())))) {
						try {
							lastOrder = getOrderManager().loadOrder(this.getValue().get(ORDER_ID).toString());
							this.setLastOrder(lastOrder);
						} catch (CommerceException ce) {
							orderError = true;
							logError("Error while retrieving Last submitted Order to create account in Order Confirmation page", ce);
						}
					} else if (this.getValue().get(ORDER_ID) == null) {
						orderError = true;
					}
					this.getValue().remove(ORDER_ID); // We donn't want to persist the Order ID
					if (!orderError) { //Order_ID exist in the input and the lastOrder is guaranteed to be the correct Order for this user
						// Use Shipping address ONLY if NOT BOSTS/BOPIS/Giftcard shipping address
						HardgoodShippingGroup shipGroup = (HardgoodShippingGroup) this.getDswShippingGroupManager().getFirstNonBopisBostsHardgoodShippingGroup(lastOrder);

						if (shipGroup != null && !DigitalAddressUtil.isAddressEmpty(shipGroup.getShippingAddress())) {
							RepositoryItem shipRepositoryItem = shipGroup.getRepositoryItem();
							Object addressTypeObject = shipRepositoryItem.getPropertyValue("addressType");
							Object rankObject = shipRepositoryItem.getPropertyValue("rank");
							Object regionObject = shipRepositoryItem.getPropertyValue("region");
							Address addr = shipGroup.getShippingAddress();
							this.setValueProperty(FIRST_NAME, addr.getFirstName());
							this.setValueProperty(LAST_NAME, addr.getLastName());

							Dictionary homeAddress = new Hashtable();
							// Set Homeaddress here. Shipping address will be automatically set in createuser()
							homeAddress.put("address1", addr.getAddress1() == null ? NULL_SENTINEL : addr.getAddress1());
							homeAddress.put("address2", addr.getAddress2() == null ? NULL_SENTINEL : addr.getAddress2());
							homeAddress.put("address3", addr.getAddress3() == null ? NULL_SENTINEL : addr.getAddress3());
							homeAddress.put("city", addr.getCity() == null ? NULL_SENTINEL : addr.getCity());
							homeAddress.put("country", addr.getCountry() == null ? NULL_SENTINEL : addr.getCountry());
							homeAddress.put("county", addr.getCounty() == null ? NULL_SENTINEL : addr.getCounty() );
							homeAddress.put("postalCode", addr.getPostalCode() == null ? NULL_SENTINEL : addr.getPostalCode());
							homeAddress.put("firstName", addr.getFirstName() == null ? NULL_SENTINEL : addr.getFirstName());
							homeAddress.put("lastName", addr.getLastName() == null ? NULL_SENTINEL : addr.getLastName());
							homeAddress.put("middleName", addr.getMiddleName() == null ? NULL_SENTINEL : addr.getMiddleName());
							homeAddress.put("prefix", addr.getPrefix() == null ? NULL_SENTINEL : addr.getPrefix());
							homeAddress.put("state", addr.getState() == null ? NULL_SENTINEL : addr.getState());
							homeAddress.put("suffix", addr.getSuffix() == null ? NULL_SENTINEL : addr.getSuffix());
							if (addressTypeObject != null) {
								homeAddress.put("addressType", addressTypeObject.toString());
							}
							if (regionObject != null) {
								homeAddress.put("region", regionObject.toString());
							}
							if (rankObject != null) {
								homeAddress.put("rank", rankObject.toString());
							}
							this.setValueProperty("homeAddress", homeAddress);
						}
					}
				}


				Object obj = this.getValueProperty("homeAddress");
				String homeMobilePhoneNumber = null;
				Dictionary dictionary = this.getValue();
				Object emailObj = dictionary.get("email");
				if (null != emailObj && DigitalStringUtil.isNotBlank(emailObj.toString())) {
					dictionary.put("email", DigitalStringUtil.lowerCase(emailObj.toString()));
				}
				Object loginObj = dictionary.get("login");
				if (null != loginObj && DigitalStringUtil.isNotBlank(loginObj.toString())) {
					dictionary.put("login", DigitalStringUtil.lowerCase(loginObj.toString()));
				}
				Dictionary homeAddress = (Dictionary) dictionary.get("homeAddress");
				if (null != (String) this.getValueProperty("mobilePhoneNumber")) {
					homeMobilePhoneNumber = (String) this.getValueProperty("mobilePhoneNumber");
				}
				if (obj != null && null != homeAddress) {
					String address1 = (String) this.getValueProperty("homeAddress.address1");
					String address2 = (String) this.getValueProperty("homeAddress.address2");
					String address3 = (String) this.getValueProperty("homeAddress.address3");
					Boolean isPOBox = ((DigitalProfileTools) this.getProfileTools()).isPOBOX(address1, address2, address3);
					if (isPOBox != null) {
						homeAddress.put("isPoBox", isPOBox);
					}
					if (null != homeMobilePhoneNumber) {
						// Set the phone number tied to the home address to the
						// mobile phone # entered on the login screen
						homeAddress.put("phoneNumber", homeMobilePhoneNumber);
					}
					// If address type is not International, set the home
					// address to the shipping address and make it
					// default/primary
					String addressType = (String) this.getValueProperty("homeAddress.addressType");

					if (!ADDRESS_TYPE_INTERNATIONAL.equalsIgnoreCase(addressType)) {
						this.setValueProperty("shippingAddress", this.getValueProperty("homeAddress"));

						DigitalAddress shippingAddress = new DigitalAddress();
						// Populate DigitalAddress object for PrimaryAddress call
						shippingAddress.setAddressType(addressType);
						shippingAddress.setAddress1(address1);
						shippingAddress.setAddress2(address2);
						shippingAddress.setAddress3(address3);
						shippingAddress.setCity((String) this.getValueProperty("shippingAddress.city"));
						shippingAddress.setState((String) this.getValueProperty("shippingAddress.state"));
						shippingAddress.setPostalCode((String) this.getValueProperty("shippingAddress.postalCode"));
						shippingAddress.setCountry((String) this.getValueProperty("shippingAddress.Country"));
						if (isPOBox != null) {
							shippingAddress.setIsPoBox(isPOBox);
						}
						shippingAddress.setFirstName((String) this.getValueProperty("shippingAddress.firstName"));
						shippingAddress.setLastName((String) this.getValueProperty("shippingAddress.lastName"));

						if (!DigitalStringUtil.isBlank(addressType) && ADDRESS_TYPE_MILITARY.equalsIgnoreCase(addressType)) {
							shippingAddress.setRegion((String) this.getValueProperty("shippingAddress.region"));
							shippingAddress.setPobox((String) this.getValueProperty("shippingAddress.pobox"));
							shippingAddress.setRank((String) this.getValueProperty("shippingAddress.rank"));
						}

						if (DigitalStringUtil.isBlank(suggestedAddress) || Boolean.parseBoolean(suggestedAddress) == true) {
							shippingAddress.setAddressVerification(true);
							homeAddress.put("addressVerification", true);
						}

						DigitalProfileTools profileTools = ((DigitalProfileTools) getProfileTools());

						profileTools.createProfileRepositoryPrimaryAddress(this.getProfile(), "shippingAddress",
								shippingAddress);

					}
				}

				if (!DigitalStringUtil.isEmpty(this.getAlertMethodEmail())) {
					setAlertPreferences(pRequest);
				}

				String loyaltyTierItemDescriptorName = ((DigitalCommercePropertyManager) getProfileTools().
						getPropertyManager()).getLoyaltyTierItemDescriptorName();

				if (getProfile().getPropertyValue(loyaltyTierItemDescriptorName) == null) {
					getProfile().setPropertyValue(loyaltyTierItemDescriptorName, DigitalBaseConstants.USER_TIER_DEFAULT);
				}
				
				Profile profile=getProfile();
				//ktlo1-1508: Forgot password flow changes
				boolean generatedPassword=false;
				if(Boolean.TRUE.equals(this.getValue().get(GENERATED_PASSWORD))){
					generatedPassword=true;
				}
				DigitalProfileTools profileTools = ((DigitalProfileTools) getProfileTools());
				if(generatedPassword && null!=profile){
					Calendar dateNow=Calendar.getInstance();
					profile.setPropertyValue("lastPasswordUpdate", dateNow.getTime());
					String generatedPwdPropertyName = getProfileTools().getPropertyManager().getGeneratedPasswordPropertyName();
					Boolean isPasswordGenerated=(Boolean)profile.getPropertyValue(generatedPwdPropertyName);
					if(isPasswordGenerated){
						dateNow.add(Calendar.HOUR, profileTools.getDswConstants().getTemporaryPasswordResetExpiryHours());
						profile.setPropertyValue("tempPasswordExpiryDate",dateNow.getTime());
					}
					
				}
				flag = super.handleCreate(pRequest, pResponse);

			} catch (RepositoryException e) {
				if (isLoggingInfo()) {
					logInfo("Unable to save shipping address to default for profileId: "
							+ this.getProfile().getRepositoryId());
				}
				logError("RepositoryException occured during account creation ", e);
				rollBack = true;
			} catch (IOException ie) {
				logError("IOException occured during account creation ", ie);
				rollBack = true;
			} catch (TransactionDemarcationException e) {
				logError("TransactionDemarcationException occured during account creation ", e);
				throw new ServletException(e);
			} catch (Exception ie) {
				logError("Exception occured during account creation ", ie);
				rollBack = true;
			} finally {
				try {
					if (this.getFormError()) {
						rollBack = true;
					}
					if (tm != null) {
						td.end(rollBack);
					}
				} catch (TransactionDemarcationException e) {
					logError("TransactionDemarcationException occured during account creation rollback ", e);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			}
		}

		return flag;
	}

	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 */
	@Override
	public boolean handleLogin(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "DigitalProfileFormHandler.handleLogin";
		final String METHOD_NAME = "handleLogin";
		boolean flag = true;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			try {

				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
				if (checkForAccountLocked()) {
					if (isLoggingInfo()) {
						logInfo("Account for User " + getProfile().getPropertyValue(LOGIN_PROPERTY) + " is already locked.");
					}
					resetFormExceptions();
					String msg = getMessageLocator().getMessageString(ACCOUNT_ALREADY_LOCKED,Long.toString(((DigitalProfileTools) this.getProfileTools()).getDswConstants().getAccountLockTime()));
					addFormException(new DropletException(msg, ACCOUNT_ALREADY_LOCKED));
					return true;
				}
				flag = super.handleLogin(pRequest, pResponse);
				

				if (getFormError() && ("false".equals(pRequest.getParameter("HANDLE_LOGIN")))) {
					MutableRepositoryItem profileItem = null;
					if (!((DigitalProfileTools) getProfileTools()).isKnown(getProfile())) {
						String loginPropertyName = getProfileTools().getPropertyManager().getLoginPropertyName();
						String login = getStringValueProperty(loginPropertyName);
						profileItem = (MutableRepositoryItem) getProfileTools().getItem(login, null);
					} else {
						profileItem = (MutableRepositoryItem) getProfile().getDataSource();
					}
					
					for (Object dEx : this.getFormExceptions()) {
						String oldPassword = getStringValueProperty("OLDPASSWORD"); 
						if (DigitalStringUtil.isNotEmpty(oldPassword) && "invalidPassword".equals(((DropletException) dEx).getErrorCode())){
							resetFormExceptions();
							addFormException(new DropletException(this.getMessageLocator().getMessageString("invalidTempPassword"),
									"invalidTempPassword"));
							break;
						}
					}
					
					DigitalProfileTools profileTools = (DigitalProfileTools) getProfileTools();
					Map<Object, Object> lockedStatus = new HashMap<>();
					lockedStatus=profileTools.markFailedLoginAttempt(profileItem, true);
					if ((Boolean)lockedStatus.get("isLocked")) {
						if (isLoggingInfo()) {
							logInfo("Account for User " + getProfile().getPropertyValue(LOGIN_PROPERTY)
									+ " is marked for failed login Attempt due to account being locked.");
						}
						resetFormExceptions();
						String msg = getMessageLocator().getMessageString(ACCOUNT_IS_BEING_LOCKED,Long.toString(((DigitalProfileTools) this.getProfileTools()).getDswConstants().getAccountLockTime()));
						addFormException(new DropletException(msg,ACCOUNT_IS_BEING_LOCKED));
					}else if((!(Boolean)lockedStatus.get("isLocked")) && ((Integer)lockedStatus.get("attempts"))==(MultiSiteUtil.getMaxForgotPasswordRetry()-1)){
						resetFormExceptions();
						addFormException(new DropletException(this.getMessageLocator().getMessageString(ACCOUNT_LOCK_WARNING),ACCOUNT_LOCK_WARNING));
					}
				}				
			} catch (ServletException se) {
				logError("ServletException occured during login ", se);
				throw se;
			} catch (IOException io) {
				logError("IOException occured during login ", io);
				throw io;
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			}
		}
		return flag;
	}

	/**
	 * 
	 * @return true or false
	 */
	private boolean checkForAccountLocked() {
		boolean locked = false;
		boolean resetLock=false;
		MutableRepositoryItem profileItem = null;
		String loginPropertyName = getProfileTools().getPropertyManager().getLoginPropertyName();
		String login = getStringValueProperty(loginPropertyName);
		profileItem = (MutableRepositoryItem) getProfileTools().getItem(login, null);
		if (profileItem == null) {
			return locked;
		}

		Object userAccountLockedTimeObj = profileItem.getPropertyValue("accountLockedTime");
		if (userAccountLockedTimeObj != null) {
			long accountLockTime = ((DigitalProfileTools) this.getProfileTools()).getDswConstants().getAccountLockTime();
			long accountLockedTime = ((Date) userAccountLockedTimeObj).getTime();
			long currentTime = System.currentTimeMillis();
			long diff = currentTime - accountLockedTime;
			long diffMinutes = diff / (60 * 1000);
			if (diffMinutes < accountLockTime) {
				return true;
			}else{
				resetLock=true;
			}
		} 

		Integer nbr = (Integer) profileItem.getPropertyValue("nbrForgotPasswordTry");
		if (nbr != null && nbr <= MultiSiteUtil.getMaxForgotPasswordRetry() && resetLock) {
			MutableRepository repository = (MutableRepository) profileItem.getRepository();
			profileItem.setPropertyValue("nbrForgotPasswordTry",0);
			profileItem.setPropertyValue("accountLockedTime",null);
			try {
				repository.updateItem(profileItem);
			} catch (RepositoryException e) {
				if (isLoggingError()){
					logError("failure trying to increment failed login attempts ", e);
				}
			}
		}

		if (nbr != null && nbr >= MultiSiteUtil.getMaxForgotPasswordRetry() && !resetLock) {
			return true;
		}


		return locked;
	}

	/**
	 * 
	 * @param dswContactInfo
	 */
	private void validateAddress(DigitalContactInfo dswContactInfo) {

		Collection dswShippingValidationErrors = dswBasicAddressValidator.validateShippingAddress(dswContactInfo);
		if ((dswShippingValidationErrors != null) && (!dswShippingValidationErrors.isEmpty())) {
			try {
				logError("dsw basic shipping address validation failed with errors : "
						+ dswShippingValidationErrors.toString());
				for (Object entry : dswShippingValidationErrors)
					this.addFormException(new DropletException(
							this.getMessageLocator().getMessageString((String) entry), (String) entry));
			} catch (Exception exception) {
				logError("Exception occured when validating shipping address ", exception);
			}
			return;
		}

		Collection dswValidationErrors = dswBasicAddressValidator.validateAddress(dswContactInfo);
		if ((dswValidationErrors != null) && (!dswValidationErrors.isEmpty())) {
			try {
				logError("dsw basic address validation failed with errors : " + dswValidationErrors.toString());
				for (Object entry : dswValidationErrors){
					this.addFormException(new DropletException(
							this.getMessageLocator().getMessageString((String) entry), (String) entry));
				}
			} catch (Exception exception) {
				logError("Exception occured when validating address ", exception);
			}
			return;
		}

		if (DigitalProfileConstants.USA_ADDRESS_TYPE.equals(dswContactInfo.getAddressType())) {
			AddressValidationServiceResponse addressValidationServiceResponse = null;
			try {
				// USPS Service Call
				addressValidationServiceResponse = getAddressValidator().validateAddress(dswContactInfo);
				if (addressValidationServiceResponse != null && !addressValidationServiceResponse.isSuccess()) {
					dswContactInfo.setAddressVerification(false);
					logError("Unable to Validate USPS Address.");
				} else if (addressValidationServiceResponse != null && addressValidationServiceResponse.isSuccess()
						&& USPS_STATUS_SUGGESTED.toString().equalsIgnoreCase(
								addressValidationServiceResponse.getAddressValidationDecision().getDescription())) {
					this.addFormException(new DropletException(
							this.getMessageLocator().getMessageString("USPSSuggested"), "USPSSuggested"));
					suggestedAddressMap = new HashMap<>();
					suggestedAddressMap.put("suggestedAddress1",
							addressValidationServiceResponse.getSuggestedAddressLine1());
					suggestedAddressMap.put("suggestedAddress2",
							addressValidationServiceResponse.getSuggestedAddressLine2());
					suggestedAddressMap.put("suggestedCity", addressValidationServiceResponse.getSuggestedCity());
					suggestedAddressMap.put("suggestedState", addressValidationServiceResponse.getSuggestedState());
					suggestedAddressMap.put("suggestedZip5", addressValidationServiceResponse.getSuggestedZip5());
					suggestedAddressMap.put("suggestedZip4", addressValidationServiceResponse.getSuggestedZip4());
					setSuggestedAddressMap(suggestedAddressMap);
				} else if (addressValidationServiceResponse != null && addressValidationServiceResponse.isSuccess()
						&& !USPS_STATUS_ACCEPTED.equalsIgnoreCase(addressValidationServiceResponse.getAddressValidationDecision()
										.getDescription())
						&& !USPS_STATUS_SUGGESTED.equalsIgnoreCase(
								addressValidationServiceResponse.getAddressValidationDecision().getDescription())) {
					logError("Unable to Validate USPS Address.");
					this.addFormException(new DropletException(this.getMessageLocator().getMessageString("USPSNoMatch"),
							"USPSNoMatch"));
				}
			} catch (DigitalIntegrationBusinessException be) {
					logError("usps address validation failed with errors : " + be.toString());
			}
		}
	}

	/**
	 * 
	 * @param type
	 * @return DigitalContactInfo
	 */
	private DigitalContactInfo populateContactInfo(String type) {

		String addressType = null;
		String address1 = null;
		String address2 = null;
		String city = null;		
		String state = null;		
		String postalCode = null;
		String firstName = null;
		String lastName = null;
		String country = null;
		String region = null;
		String rank = null;

		DigitalContactInfo dswContactInfo = new DigitalContactInfo();		

		if ("homeAddress".equalsIgnoreCase(type)) {

			addressType = (String) this.getValueProperty("homeAddress.addressType");
			address1 = (String) this.getValueProperty("homeAddress.address1");
			address2 = (String) this.getValueProperty("homeAddress.address2");

			city = (String) this.getValueProperty("homeAddress.city");
			state = (String) this.getValueProperty("homeAddress.state");
			postalCode = (String) this.getValueProperty("homeAddress.postalCode");
			firstName = (String) this.getValueProperty("homeAddress.firstName");

			if (AddressType.MILITARY.getValue().equals(addressType)) {
				region = (String) this.getValueProperty("homeAddress.region");
				rank = (String) this.getValueProperty("homeAddress.rank");

				if (DigitalStringUtil.isNotEmpty(rank)) {
					city = rank;
				}

				if (DigitalStringUtil.isNotEmpty(region)) {
					state = region;
				}
			}

			if (DigitalStringUtil.isBlank(firstName) || DigitalStringUtil.isEmpty(firstName)) {
				firstName = (String) this.getValue().get(FIRST_NAME);
			}
			lastName = (String) this.getValueProperty("homeAddress.lastName");
			if (DigitalStringUtil.isBlank(lastName) || DigitalStringUtil.isEmpty(lastName)) {
				lastName = (String) this.getValue().get(LAST_NAME);
			}

			country = (String) this.getValueProperty("homeAddress.country");

		}

		else if ("shippingAddress".equalsIgnoreCase(type)) {

			addressType = (String) this.getValueProperty("shippingAddress.addressType");
			address1 = (String) this.getValueProperty("shippingAddress.address1");
			address2 = (String) this.getValueProperty("shippingAddress.address2");
			city = (String) this.getValueProperty("shippingAddress.city");
			state = (String) this.getValueProperty("shippingAddress.state");
			postalCode = (String) this.getValueProperty("shippingAddress.postalCode");
			firstName = (String) this.getValueProperty("shippingAddress.firstName");
			lastName = (String) this.getValueProperty("shippingAddress.lastName");
			country = (String) this.getValueProperty("shippingAddress.country");

			if (AddressType.MILITARY==AddressType.valueFor(addressType)) {
				region = (String) this.getValueProperty("shippingAddress.region");
				rank = (String) this.getValueProperty("shippingAddress.rank");

				if (DigitalStringUtil.isNotEmpty(rank)) {
					city = rank;
				}

				if (DigitalStringUtil.isNotEmpty(region)) {
					state = region;
				}
			}

		}

		dswContactInfo.setAddress1(address1);
		dswContactInfo.setAddress2(address2);
		dswContactInfo.setCity(city);
		dswContactInfo.setRegion(region);
		dswContactInfo.setRank(rank);
		dswContactInfo.setCountry(country);
		dswContactInfo.setAddressType(addressType);
		dswContactInfo.setState(state);
		dswContactInfo.setPostalCode(postalCode);
		dswContactInfo.setFirstName(firstName);
		dswContactInfo.setLastName(lastName);

		return dswContactInfo;

	}

	/**
	 * @param pRequest
	 * @return true or false
	 */
	private boolean saveCustomer(DynamoHttpServletRequest pRequest) {
		boolean flag = false;
		DigitalProfileTools tools = (DigitalProfileTools) this.getProfileTools();
		BtsRewardService rewardsService = tools.getRewardService();
		RewardServiceRequest serviceRequest = populateRewardServiceRequest(pRequest);
		RewardServiceResponse retrieveMemberIdResponse;
		RewardServiceResponse saveCustomerResponse = null;
		String memberId = null;
		try {
			retrieveMemberIdResponse = rewardsService.retrieveMemberID(serviceRequest);
			if (retrieveMemberIdResponse != null && retrieveMemberIdResponse.isSuccess()) {
				memberId = retrieveMemberIdResponse.getPerson().getMemberID();
				updateRewardMemberByLoyaltyNumber(pRequest, memberId);
				flag = true;
			}
		} catch (Exception e) {
			logError("Add Customer  Member Does not exist in Rewards system..." + e);
		}
		try {
			if (DigitalStringUtil.isEmpty(memberId)) {
				saveCustomerResponse = rewardsService.addCustomer(serviceRequest);
			}
		} catch (Exception e) {
			logError(e);
		}
		if (saveCustomerResponse != null && saveCustomerResponse.isSuccess()) {
			flag = true;
		}
		return flag;
	}

	/**
	 * @return RewardServiceRequest
	 */
	private RewardServiceRequest populateRewardServiceRequest(DynamoHttpServletRequest pRequest) {

		RewardServiceRequest btsRewardServiceRequest = new RewardServiceRequest();

		// Set the SignupDate = (Last Order Submit date - 1 second) for Signups made from Order Confirmation page
		if (getSource().equalsIgnoreCase("orderconfirmation")) {
			Order lastOrder = getLastOrder();
			SimpleDateFormat vipRewardsFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
			Calendar calender = Calendar.getInstance();
			calender.setTime(lastOrder.getSubmittedDate());
			calender.add(Calendar.SECOND, -1);
			btsRewardServiceRequest.setSignUpDate(vipRewardsFormatter.format(calender.getTime()));
		}

		String email = (String) this.getValue().get("email");
		if (!DigitalStringUtil.isBlank(email)) {
			btsRewardServiceRequest.getContact().setEmail(email);
			btsRewardServiceRequest.getFlags().setBadEmail("N");
		}
		btsRewardServiceRequest.getFlags().setEnrollAsMemberFlag("Y");
		btsRewardServiceRequest.getPerson().setFirstName((String) this.getValue().get("firstName"));
		btsRewardServiceRequest.getPerson().setLastName((String) this.getValue().get("lastName"));

		String gender = (String) this.getValue().get("gender");
		if (!DigitalStringUtil.isBlank(gender)) {
			gender = getGenderCode(gender.toUpperCase());
		}
		if (gender != null) {
			btsRewardServiceRequest.getPerson().setGender(Person.Gender.getGenderFromDescription(gender));
		}

		String mobilePhoneNumber = (String) this.getValue().get("mobilePhoneNumber");

		if (!DigitalStringUtil.isEmpty(mobilePhoneNumber)) {
			btsRewardServiceRequest.getContact().setMobilePhone(mobilePhoneNumber);
			btsRewardServiceRequest.getContact().setPhone1(mobilePhoneNumber);
			btsRewardServiceRequest.getFlags().setBadPhone("N");
		}

		if (null != getBirthDay()) {
			btsRewardServiceRequest.getPerson().setDayOfBirth(getBirthDay());
		}

		if (null != getBirthMonth()) {
			btsRewardServiceRequest.getPerson().setMonthOfBirth(getBirthMonth());
		}

		String setNoEmail = "Y";
		String setNoPhone = "Y";
		String optOutMobile = "Y";
		if (this.getAlertMethodEmail() != null && this.getAlertMethodEmail().equalsIgnoreCase("true")) {
			setNoEmail = "N";
		}

		if (this.getAlertMethodSMS() != null && this.getAlertMethodSMS().equalsIgnoreCase("true")) {
			setNoPhone = "N";
			optOutMobile = "N";
		}

		btsRewardServiceRequest.getFlags().setNoEMail(setNoEmail);
		btsRewardServiceRequest.getFlags().setNoPhone(setNoPhone);
		btsRewardServiceRequest.getFlags().setOptOutMobile(optOutMobile);

		btsRewardServiceRequest
				.setFrequencyFashionEmail(DigitalProfileConstants.REWARDS_EMAIL_ALERT_FREQUENCY);

		// Start code for setting the Address.
		if (this.getValueProperty("homeAddress") != null) {
			String address1 = (String) this.getValueProperty("homeAddress.address1");
			if (DigitalStringUtil.isNotBlank(address1) && DigitalStringUtil.isNotEmpty(address1)) {
				btsRewardServiceRequest.getAddress().setAddress1(address1);
			}
			String address2 = (String) this.getValueProperty("homeAddress.address2");
			if (DigitalStringUtil.isNotBlank(address2) && DigitalStringUtil.isNotEmpty(address2)) {
				btsRewardServiceRequest.getAddress().setAddress2(address2);
			}
			String city = (String) this.getValueProperty("homeAddress.city");
			if (DigitalStringUtil.isNotBlank(city) && DigitalStringUtil.isNotEmpty(city)) {
				btsRewardServiceRequest.getAddress().setCity(city);
			} else {
				btsRewardServiceRequest.getAddress()
						.setCity((String) this.getValueProperty("homeAddress.rank"));
			}
			String state = (String) this.getValueProperty("homeAddress.state");
			if (DigitalStringUtil.isNotBlank(state) && DigitalStringUtil.isNotEmpty(state)) {
				btsRewardServiceRequest.getAddress().setState(state);
			} else {
				btsRewardServiceRequest.getAddress()
						.setState((String) this.getValueProperty("homeAddress.region"));
			}
			btsRewardServiceRequest.getAddress()
					.setCountry((String) this.getValueProperty("homeAddress.country"));
			String postCode = (String) this.getValueProperty("homeAddress.postalCode");
			if (!DigitalStringUtil.isEmpty(postCode) && postCode.contains("-")) {
				postCode = postCode.replace("-", "");
			}
			btsRewardServiceRequest.getAddress().setPostCode(postCode);
		}

		Long defaultCertDen = ((DigitalProfileTools) this.getProfileTools()).getDswConstants()
				.getDefaultCertDenomination();
		btsRewardServiceRequest.setCertDenomination(String.valueOf(defaultCertDen));
		btsRewardServiceRequest.setIsPaperlessCert(Boolean.FALSE);
		btsRewardServiceRequest.getPerson().setProfileID(this.getProfile().getRepositoryId());

		String deviceName = ServletUtil
				.getCurrentRequest()
				.getHeader(RequestHeaderAttributesConstant.AKAMAI_DEVICE_KEY.getValue());

		if (DigitalStringUtil.isEmpty(deviceName)) {
			deviceName = "desktop";
		}
		btsRewardServiceRequest.setDeviceName(deviceName);

		return btsRewardServiceRequest;
	}

	/**
	 * @param pRequest
	 * @param updateCustomerRequest
	 * @throws RepositoryException
	 */
	private void populateRewardsUpdateCustomerRequest(DynamoHttpServletRequest pRequest,
			RewardsUpdateCustomerRequest updateCustomerRequest) throws RepositoryException {

		String email = (String) this.getValue().get("email");
		String firstName = (String) this.getValue().get("firstName");
		String lastName = (String) this.getValue().get("lastName");
		String gender = (String) this.getValue().get("gender");
		if (!DigitalStringUtil.isBlank(gender)) {
			gender = getGenderCode(gender.toUpperCase());
		}
		String birthMonth = pRequest.getParameter("birthMonth");
		String birthDay = pRequest.getParameter("birthDay");
		String alertMethodEmail = this.getAlertMethodEmail();
		String alertMethodSMS = this.getAlertMethodSMS();
		String setNoEmail = null;
		String setNoPhone = null;
		String optOutMobile = null;
		String mobilePhoneNumber = (String) this.getValue().get("mobilePhoneNumber");
		String homePhoneNumber = (String) this.getValue().get("daytimeTelephoneNumber");
		String certDenomination = (String) this.getValue().get(CERT_DENOMINATION);

		if (!DigitalStringUtil.isEmpty(email)) {
			// Check if email is being updated or adding new one if so reset bad email flag
			if(!email.equalsIgnoreCase(updateCustomerRequest.getEmail())){
				updateCustomerRequest.setBadEmail("N");
			}
			updateCustomerRequest.setEmail(email);
		}


		if (!DigitalStringUtil.isEmpty(firstName)) {
			updateCustomerRequest.setFirstName(firstName);
		}

		if (!DigitalStringUtil.isEmpty(lastName)) {
			updateCustomerRequest.setLastName(lastName);
		}

		if (!DigitalStringUtil.isEmpty(gender)) {
			updateCustomerRequest.setGender(gender);
		}

		/*
		 * If birthDay is null do not do anything - DOB from selectCustomer used
		 * If birthDay is not null && "" - Client wants birthDay to be
		 * nullified, set as "0" in the update request If birthDay is not null
		 * && has some value - UI wants DOB to be changed. Send the one from
		 * request
		 */
		if (null != birthDay) {
			if ("".equals(birthDay.trim())) {
				updateCustomerRequest.setDayOfBirth("0");
			} else {
				updateCustomerRequest.setDayOfBirth(birthDay);
			}
		}

		/*
		 * If birthMonth is null do not do anything - DOB from selectCustomer
		 * used If birthMonth is not null && "" - Client wants birthMonth to be
		 * nullified, set as "0" in the update request If birthMonth is not null
		 * && has some value - UI wants DOB to be changed. Send the one from
		 * request
		 */
		if (null != birthMonth) {
			if ("".equals(birthMonth.trim())) {
				updateCustomerRequest.setMonthOfBirth("0");
			} else {
				updateCustomerRequest.setMonthOfBirth(birthMonth);
			}
		}
		if (alertMethodEmail != null) {
			if (alertMethodEmail.equalsIgnoreCase("true")) {
				setNoEmail = "N";
			} else if (alertMethodEmail.equalsIgnoreCase("false")) {
				setNoEmail = "Y";
			}
		}

		if (alertMethodSMS != null && alertMethodSMS.equalsIgnoreCase("true")) {
			setNoPhone = "N";
			optOutMobile = "N";
		} else if (alertMethodSMS != null && alertMethodSMS.equalsIgnoreCase("false")) {
			setNoPhone = "Y";
			optOutMobile = "Y";
		}

		if (!DigitalStringUtil.isEmpty(setNoEmail)) {
			updateCustomerRequest.setNoEMail(setNoEmail);
		}

		if (!DigitalStringUtil.isEmpty(setNoPhone)) {
			updateCustomerRequest.setNoPhone(setNoPhone);
			updateCustomerRequest.setOptOutMobile(optOutMobile);
		}

		if (mobilePhoneNumber != null) {
			updateCustomerRequest.setMobilePhone(mobilePhoneNumber);
		}

		if (homePhoneNumber != null) {
			updateCustomerRequest.setHomePhone(homePhoneNumber);
		}

		// Start code for setting the Address.
		if (this.getValueProperty("homeAddress") != null) {
			updateCustomerRequest.setAddress1((String) this.getValueProperty("homeAddress.address1"));
			updateCustomerRequest.setAddress2((String) this.getValueProperty("homeAddress.address2"));
			String city = (String) this.getValueProperty("homeAddress.city");
			if (DigitalStringUtil.isNotBlank(city) && DigitalStringUtil.isNotEmpty(city)) {
				updateCustomerRequest.setCity(city);
			} else {
				updateCustomerRequest.setCity((String) this.getValueProperty("homeAddress.rank"));
			}
			String state = (String) this.getValueProperty("homeAddress.state");
			if (DigitalStringUtil.isNotBlank(state) && DigitalStringUtil.isNotEmpty(state)) {
				updateCustomerRequest.setState(state);
			} else {
				updateCustomerRequest.setState((String) this.getValueProperty("homeAddress.region"));

			}
			String postCode = (String) this.getValueProperty("homeAddress.postalCode");
			if (!DigitalStringUtil.isEmpty(postCode) && postCode.contains("-")) {
				postCode = postCode.replace("-", "");
			}
			updateCustomerRequest.setPostCode(postCode);

			updateCustomerRequest.setCountry((String) this.getValueProperty("homeAddress.country"));

		}

		if (getProfileItem().getItemDescriptor().hasProperty("pointBankerFlag")
				&& getProfileItem().getPropertyValue("pointBankerFlag") instanceof String) {
			String pointsBanking = getProfileItem().getPropertyValue("pointBankerFlag").toString();
			if (null != pointsBanking && pointsBanking.equalsIgnoreCase("true")) {
				updateCustomerRequest.setDoBankPoints("Y");
			} else {
				updateCustomerRequest.setDoBankPoints("N");
			}
		} else {
			updateCustomerRequest.setDoBankPoints("N");
		}

		updateCustomerRequest.setProfileId(this.getProfile().getRepositoryId());
		if (null != certDenomination) {
			updateCustomerRequest.setCertDenomination(certDenomination);
		}

    if((this.getValue().get(IS_PAPERLESS_CERT) != null)) {
			boolean isPaperlessCert = (Boolean) this.getValue().get(IS_PAPERLESS_CERT);
			updateCustomerRequest.setPaperlessCertOptIn(isPaperlessCert? "Y" : "N");
		}
	}

	/**
	 * 
	 * @param gender
	 * @return String
	 */
	private String getGenderCode(String gender) {
		switch (gender) {
		case "MALE":
			return "M";
		case "FEMALE":
			return "F";
		case "UNKNOWN":
			return "U";
		default:
			return null;
		}
	}

	/**
	 * 
	 * @param pRequest
	 */
	private void setAlertPreferences(DynamoHttpServletRequest pRequest) {
		try {
			MutableRepository repository = (MutableRepository) this.getProfile().getRepository();
			MutableRepositoryItem item = repository.createItem("alertPreference");

			String alertType = this.getAlertType();
			
			if (DigitalStringUtil.isEmpty(alertType)) {
				alertType = "DSW_REWARDS";
			}

			item.setPropertyValue("emailAlertFrequency", DigitalProfileConstants.EMAIL_ALERT_FREQUENCY);
			if (!DigitalStringUtil.isEmpty(this.getAlertMethodEmail())) {
				item.setPropertyValue("alertMethodEmail", Boolean.parseBoolean(this.getAlertMethodEmail()));

			}

			if (!DigitalStringUtil.isEmpty(this.getAlertMethodSMS())) {
				item.setPropertyValue("alertMethodSMS", Boolean.parseBoolean(this.getAlertMethodSMS()));

			}

			item.setPropertyValue("alertType", alertType);
			item.setPropertyValue(USER_PROPERTY, this.getProfile().getDataSource());

			Set alertPreferences = new HashSet();
			alertPreferences.add(item);
			this.getProfile().setPropertyValue("alertPreferences", alertPreferences);
		}

		catch (Exception e) {
			if (isLoggingError()) {
				logError("Unable to set Alert Preferences", e);
			}
		}

	}

	/**
	 * 
	 * @param pRequest
	 */
	private void updateAlertPreference(DynamoHttpServletRequest pRequest) {

		try {

			MutableRepository repository = (MutableRepository) this.getProfile().getRepository();

			String alertType = this.getAlertType();
			if (DigitalStringUtil.isEmpty(alertType)) {
				alertType = "DSW_REWARDS";
			}

			Set alertPreferences = (Set) this.getProfile().getPropertyValue("alertPreferences");
			if (alertPreferences != null && !alertPreferences.isEmpty()) {
				Iterator it = alertPreferences.iterator();
				MutableRepositoryItem alertItem = (MutableRepositoryItem) it.next();

				if (this.getAlertMethodEmail() != null) {
					alertItem.setPropertyValue("alertMethodEmail", Boolean.parseBoolean(this.getAlertMethodEmail()));

				}

				if (this.getAlertMethodSMS() != null) {
					alertItem.setPropertyValue("alertMethodSMS", Boolean.parseBoolean(this.getAlertMethodSMS()));

				}

				repository.updateItem(alertItem);

			}

			else {
				setAlertPreferences(pRequest);
			}

		}

		catch (RepositoryException re) {
			logError("RepositoryException occured while updateing the alert preference ::"+re);
		}

	}

	private void preUpdateCertsDenomination(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		String CERT_AMOUNT = "amount";
		String PAPERLESS = "paperless";
		boolean isCertDenomEmpty = true;
		boolean isPaperlessFlagEmpty = true;

		if (null != pRequest.getParameter(CERT_AMOUNT)) {
			this.getValue().put(CERT_DENOMINATION, pRequest.getParameter(CERT_AMOUNT));
			isCertDenomEmpty = false;
		}


		if (null != pRequest.getParameter(PAPERLESS)) {
			this.getValue().put(IS_PAPERLESS_CERT, Boolean.parseBoolean(pRequest.getParameter(PAPERLESS)));
			isPaperlessFlagEmpty = false;
		}

		if (isCertDenomEmpty && isPaperlessFlagEmpty) {
			if (isCertDenomEmpty) {
				addFormException(new DropletException(
						this.getMessageLocator().getMessageString(DigitalProfileConstants.CERT_AMOUNT_REQUIRED),
						DigitalProfileConstants.CERT_AMOUNT_REQUIRED));
			}
			if (isPaperlessFlagEmpty) {
				addFormException(new DropletException(
						this.getMessageLocator().getMessageString(DigitalProfileConstants.PAPERLESS_FLAG_REQUIRED),
						DigitalProfileConstants.PAPERLESS_FLAG_REQUIRED));
			}
		}

	}

	/**
	 *
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 */
	public boolean handleUpdateCertsDenomination(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		String METHOD_NAME = "DigitalProfileFormHandler.handleUpdateCertsDenomination";
		boolean retVal = true;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(METHOD_NAME))) {
			TransactionManager tm = super.getTransactionManager();
			TransactionDemarcation td = super.getTransactionDemarcation();
			boolean rollBack = false;
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}

				preUpdateCertsDenomination(pRequest, pResponse);

				if (!getFormError()) {
					// This will update the 'certDenomination' and paperless flag
					updateRewardMember(pRequest);
				}
			} catch (Exception ex) {
				retVal = false;
				addFormException(new DropletException(
						this.getMessageLocator().getMessageString(DigitalProfileConstants.PROFILE_UPDATE_ERROR),
						DigitalProfileConstants.PROFILE_UPDATE_ERROR));
				if (isLoggingError()) {
					logError(ex);
				}
				rollBack = true;
			} finally {
				try {
					if (tm != null) {
						td.end(rollBack);
					}
				} catch (TransactionDemarcationException e) {
					logError("TransactionDemarcationException occurred during update ", e);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			}
		}
		return retVal;
	}

	/**
	 * @param serviceURL
	 * @return true or false
	 */
	private boolean isCleanUpOfCookiesAllowed(String serviceURL) {
		if (DigitalStringUtil.isEmpty(serviceURL) || null == allowedServices) {
			return false;
		}
		for (String allowedServiceName : allowedServices) {
			if (serviceURL.contains(allowedServiceName)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @return the source
	 */
	public String getSource() {
		return DigitalStringUtil.isEmpty(source) ? "" : source;
	}

	/**
	 * 
	 * @param pRequest
	 */
	private void setDateOfBirth(DynamoHttpServletRequest pRequest) {
		if (birthDay != null && birthMonth != null && birthYear != null) {
			if (DigitalStringUtil.isEmpty(birthDay) && DigitalStringUtil.isEmpty(birthMonth)) {
				this.getValue().put("dateOfBirth", "");
			} else {
				Date dob = DigitalDateUtil.calculteDOB(getBirthDay(), getBirthMonth(), getBirthYear());
				this.getValue().put("dateOfBirth", dob);
			}
		}
	}
}