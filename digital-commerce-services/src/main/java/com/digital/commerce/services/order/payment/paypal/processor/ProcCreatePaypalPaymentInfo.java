package com.digital.commerce.services.order.payment.paypal.processor;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.services.order.payment.paypal.GenericPaypalPaymentInfo;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.order.payment.paypal.PaypalPaymentInfo;

import atg.commerce.order.Order;
import atg.commerce.payment.PaymentManagerPipelineArgs;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import lombok.Getter;
import lombok.Setter;

/**
 * Creates the PayPal payment info object using pipeline arguments
 * 
 */
@Getter
@Setter
public class ProcCreatePaypalPaymentInfo extends ApplicationLoggingImpl implements
		PipelineProcessor {
	private static final int SUCCESS = 1;
	private static final String SERVICE_NAME = "CreatePaypalPaymentInfo";
	private String paypalPaymentInfoClass;

	public ProcCreatePaypalPaymentInfo() {
		super(ProcCreatePaypalPaymentInfo.class.getName());
	}


	protected PaypalPaymentInfo getPaypalPaymentInfo() throws Exception {
		if (isLoggingDebug())
			logDebug("Making a new instance of type: "
					+ getPaypalPaymentInfoClass());

    return (GenericPaypalPaymentInfo) Class
        .forName(getPaypalPaymentInfoClass()).newInstance();
	}

	public int runProcess(Object pParam, PipelineResult pResult) throws Exception {
		final String METHOD_NAME = "runProcess";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
			
			PaymentManagerPipelineArgs params = (PaymentManagerPipelineArgs) pParam;
			Order order = params.getOrder();
			PaypalPayment paypalPayment = (PaypalPayment) params.getPaymentGroup();
			double amount = params.getAmount();
	
			// create and populate paypal payment info class
			GenericPaypalPaymentInfo ppi = (GenericPaypalPaymentInfo) getPaypalPaymentInfo();
			addDataToPaypalPaymentInfo(order, paypalPayment, amount, params, ppi);
			params.setPaymentInfo(ppi);
			if (isLoggingDebug())
				logDebug("paypalpayment info is :" + ppi.toString());
			return SUCCESS;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(SERVICE_NAME, METHOD_NAME);
		}
	}

	private void addDataToPaypalPaymentInfo(Order pOrder,
			PaypalPayment pPaymentGroup, double pAmount,
			PaymentManagerPipelineArgs pParams,
			GenericPaypalPaymentInfo pPaypalPaymentInfo) {
		pPaypalPaymentInfo.setPaypalPayerId(pPaymentGroup.getPaypalPayerId());
		pPaypalPaymentInfo.setPaypalEmail(pPaymentGroup.getPaypalEmail());
		pPaypalPaymentInfo.setCardinalOrderId(pPaymentGroup.getCardinalOrderId());
		pPaypalPaymentInfo.setNewUser(pPaymentGroup.isNewUser());
		pPaypalPaymentInfo.setOrder(pOrder);
		pPaypalPaymentInfo.setAmount(pAmount);

	}

	/**
	 * } Return the possible return values for this processor. This processor
	 * always returns a success code.
	 **/

	public int[] getRetCodes() {
    return new int[]{ SUCCESS };
	}

}
