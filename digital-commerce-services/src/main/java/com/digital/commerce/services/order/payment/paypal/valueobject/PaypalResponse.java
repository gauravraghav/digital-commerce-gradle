package com.digital.commerce.services.order.payment.paypal.valueobject;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PaypalResponse implements Serializable {

	private static final long	serialVersionUID	= -402964880647532498L;
	private String				statusCode;
	String						cardinalOrderId;
	String						transactionId;
	String						errorNo;
	String						errorDesc;
	String						reasonCode;
	String						reasonDesc;
	String						merchantData;
	boolean success=false;
}
