/**
 * 
 */
package com.digital.commerce.services.pricing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.payment.BillingShippingServices;
import com.digital.commerce.services.promotion.DigitalPromotionTools;

import atg.commerce.order.Order;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes","deprecation","unchecked"})
@Getter
@Setter
public class DigitalPricingTools extends PricingTools {

	private BillingShippingServices billingShippingServices;

	private ShippingHandlingInfo shippingHandlingInfo;

	private DigitalPromotionTools promotionTools;

	/**
	 * Gift cards only orders do not calculate shipping quotes.
	 */

	protected double priceShippingForOrderTotal(Order pOrder, Collection pPricingModels, Locale pLocale, RepositoryItem pProfile, Map pExtraParameters, boolean pGenerateOrderRanges) throws PricingException {
		List shippingPromoMatches = new ArrayList();

		DigitalOrderImpl order = (DigitalOrderImpl) pOrder;
		// if order has giftcard only, then we dont' need to get any shipping quote
		if (!order.getIsGiftCardOrderOnly()) {
			OrderShippingQuote orderQuote = getBillingShippingServices().getOrderShippingQuote(pOrder, "sales", shippingPromoMatches, getShippingHandlingInfo(), pExtraParameters);

			if (pExtraParameters == null) {
				pExtraParameters = new HashMap();
			}
			pExtraParameters.put(ShippingGroupConstants.SHIPPING_PRICE_MAP, orderQuote.getShippingDestinationRates());
		}
		return super.priceShippingForOrderTotal(pOrder, pPricingModels, pLocale, pProfile, pExtraParameters, pGenerateOrderRanges);
	}

	public void performPricingOperation(String pPricingOperation, Order pOrder,
										PricingModelHolder pPricingModels, Locale pLocale,
										RepositoryItem pProfile, Map pExtraParameters)
			throws PricingException {
		if (pPricingModels != null) {
			Collection<RepositoryItem> pAllPromotions = pPricingModels.getAllPromotions();
			if (isLoggingDebug()) {
				logDebug("Inside performPricingOperation in DigitalPricingTools. Size of pAllPromotions= " + pAllPromotions.size() + " " + pAllPromotions.toString());
			}
			pAllPromotions = getPromotionTools().validateImplicitTargetedPromotions(pAllPromotions, pProfile);
			pPricingModels.setAllPromotions(pAllPromotions);

			Collection<RepositoryItem> pAllItemPricingModels = pPricingModels.getItemPricingModels();
			if (isLoggingDebug()) {
				logDebug("Inside performPricingOperation in DigitalPricingTools. Size of pAllItemPricingModels= " + pAllItemPricingModels.size() + " " + pAllItemPricingModels.toString());
			}
			pAllItemPricingModels = getPromotionTools().validateImplicitTargetedPromotions(pAllItemPricingModels, pProfile);
			pPricingModels.setItemPricingModels(pAllItemPricingModels);

			Collection<RepositoryItem> pAllOrderPricingModels = pPricingModels.getOrderPricingModels();
			if (isLoggingDebug()) {
				logDebug("Inside performPricingOperation in DigitalPricingTools. Size of pAllOrderPricingModels= " + pAllOrderPricingModels.size() + " " + pAllOrderPricingModels.toString());
			}
			pAllOrderPricingModels = getPromotionTools().validateImplicitTargetedPromotions(pAllOrderPricingModels, pProfile);
			pPricingModels.setOrderPricingModels(pAllOrderPricingModels);

			Collection<RepositoryItem> pAllShipPricingModels = pPricingModels.getShippingPricingModels();
			if (isLoggingDebug()) {
				logDebug("Inside performPricingOperation in DigitalPricingTools. Size of pAllShipPricingModels= " + pAllShipPricingModels.size() + " " + pAllShipPricingModels.toString());

			}
			pAllShipPricingModels = getPromotionTools().validateImplicitTargetedPromotions(pAllShipPricingModels, pProfile);
			pPricingModels.setShippingPricingModels(pAllShipPricingModels);
		}

		super.performPricingOperation(pPricingOperation, pOrder,
				pPricingModels, pLocale, pProfile, pExtraParameters);
	}
}
