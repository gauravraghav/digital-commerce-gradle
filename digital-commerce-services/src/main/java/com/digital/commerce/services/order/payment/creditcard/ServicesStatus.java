/**
 * 
 */
package com.digital.commerce.services.order.payment.creditcard;

import java.io.Serializable;

import org.apache.commons.lang.builder.HashCodeBuilder;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/** @author wibrahim */
@Getter
@Setter
@ToString
public abstract class ServicesStatus implements Serializable {

	private static final long	serialVersionUID	= 3648176100755514828L;
	private String				statusCode;
	private String				statusMessage;

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode( this );
	}
}
