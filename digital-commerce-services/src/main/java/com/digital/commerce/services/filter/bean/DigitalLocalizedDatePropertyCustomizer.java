package com.digital.commerce.services.filter.bean;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;
import atg.servlet.ServletUtil;

import com.digital.commerce.services.i18n.DigitalCustomDateFormatter;
import lombok.Getter;
import lombok.Setter;

/**
 * A Property customizer used to return a date property in a localized format.
 * The local is obtained from the current request. The dateFormatter component
 * is used to format dates.
 *
 * @author
 */
@Getter
@Setter
public class DigitalLocalizedDatePropertyCustomizer extends ApplicationLoggingImpl implements PropertyCustomizer {

	public DigitalLocalizedDatePropertyCustomizer() {
		super(DigitalLocalizedDatePropertyCustomizer.class.getName());
	}

	// ---------------------------------------------------------------------------
	// PROPERTIES
	// ---------------------------------------------------------------------------

	// -----------------------------------
	// property: dateFormatter
	// -----------------------------------
	private DigitalCustomDateFormatter dateFormatter = null;


	// -----------------------------------
	// property: customDateFormats
	// -----------------------------------
	private Map<String, String> customDateFormats = null;


	// ---------------------------------------------------------------------------
	// METHODS
	// ---------------------------------------------------------------------------

	/**
	 * Returns a date String in a locale specific format. A format parameter can
	 * be passed in the pAttributes map which controls the date pattern, e.g
	 * MM/dd/yyyy. The returned date String will be formatted in a locale
	 * specific way using the pattern.
	 *
	 * @param pTargetObject
	 *            The object which the specified property is associated with.
	 * @param pPropertyName
	 *            The name of the property to return.
	 * @param pAttributes
	 *            The key/value pair attributes defined in the
	 *            beanFilteringConfiguration.xml file for this property.
	 *
	 * @return A date String in a locale specific format.
	 *
	 * @throws BeanFilterException
	 */
	@Override
	public Object getPropertyValue(Object pTargetObject, String pPropertyName, Map<String, String> pAttributes)
			throws BeanFilterException {

		// Get date value that needs formatted.
		Object propValue = null;

		try {
			propValue = DynamicBeans.getPropertyValue(pTargetObject, pPropertyName);
		} catch (PropertyNotFoundException e) {
			throw new BeanFilterException(e);
		}
		
		if(propValue instanceof Long){
		   Calendar cal = Calendar.getInstance();
		   cal.setTimeInMillis((long)propValue);
		   propValue = cal.getTime();   
		 }else if (!(propValue instanceof Date)) {
		  vlogDebug("Property {0} was not a date: {1}", pPropertyName, propValue);
		   return null;
		 }

		// Get the locale and pattern to use for formatting.
		Locale locale = ServletUtil.getUserLocale();
		DigitalCustomDateFormatter dswCustomDateFormatter = getDateFormatter();
		if(getCustomDateFormats() != null && !getCustomDateFormats().isEmpty()){
			dswCustomDateFormatter.setCustomDateFormats(getCustomDateFormats());
		}
		return dswCustomDateFormatter.getLocalizedDateString((Date) propValue, locale);
	}
}
