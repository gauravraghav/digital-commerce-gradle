package com.digital.commerce.services.communication.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SendEmailResponse extends ResponseWrapper {
	private String message;
}
