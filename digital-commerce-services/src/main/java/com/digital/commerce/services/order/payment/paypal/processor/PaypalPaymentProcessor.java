package com.digital.commerce.services.order.payment.paypal.processor;

import com.digital.commerce.services.order.payment.paypal.PaypalPaymentInfo;

/**
 * Interface for paypal payment processor
 * 
 */
public interface PaypalPaymentProcessor {

    public PaypalPaymentStatus authorize(PaypalPaymentInfo paypalPaymentInfo);
}
