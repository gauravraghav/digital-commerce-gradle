package com.digital.commerce.services.order.processor;

import static com.digital.commerce.constants.DigitalProfileConstants.GIFTCARD_INSUFFICIENT_FUNDS;

import atg.commerce.order.PaymentGroup;
import atg.commerce.order.processor.ProcAuthorizePayment;
import atg.service.pipeline.PipelineResult;
import com.digital.commerce.common.config.CCAuthFraudMessageLocator;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.constants.PaypalConstants;
import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import lombok.Getter;
import lombok.Setter;

import java.text.MessageFormat;
import java.util.ResourceBundle;

@Getter
@Setter
public class DigitalProcAuthorizePayment extends ProcAuthorizePayment{
	
	private CCAuthFraudMessageLocator	authFraudLocator;
	private MessageLocator				messageLocator;
	
	private static final String			FAILED_CREDITCARD_AUTH	    = "failedCreditCardAuthorization";
	
	 /**
	   * This method handles adding authorization error messages to the pipeline result
	   * object.  It creates an errorMessage and errorKey and adds these to the ResultObject.
	   *
	   * @param pFailedPaymentGroup the payment group that failed to authorize
	   * @param pStatusMessage message indicating why the payment group failed to authorize
	   * @param pResult the pipeline result object.
	   * @param pBundle resource bundle specific to users locale
	   */
	  @Override
		protected void addPaymentGroupError(PaymentGroup pFailedPaymentGroup, String pStatusMessage,
				PipelineResult pResult, ResourceBundle pBundle) {
			if (pFailedPaymentGroup instanceof PaypalPayment) {
				String errorMessage;
				String errorKey;
				String key = "FailedPaypalPaymentGroupAuthorization";
				if (DigitalStringUtil.isNotBlank(pStatusMessage) && pStatusMessage
						.contains(PaypalConstants.ERROR_MSG_UNAVAILABLE)) {
					key = "FailedPaypalPaymentGroupAuthorizationUnavailable";
				}
				errorMessage = MessageFormat.format(pBundle.getString(key),
						new Object[]{
								pFailedPaymentGroup.getId(),
								pStatusMessage
						});
				errorKey = key + " : " + pFailedPaymentGroup.getId();
				pResult.addError(errorKey, errorMessage);
				if (this.isLoggingError()) {
					this.logError(key + " :: " + pStatusMessage);
				}
			} else if (pFailedPaymentGroup instanceof DigitalGiftCard) {
				String errorMessage;
				String errorKey;
				String key = "FailedGiftCardPaymentGroupAuthorization";
				if (DigitalStringUtil.isNotBlank(pStatusMessage) && pStatusMessage
						.contains(DigitalBaseConstants.GIFTCARD_UNAVAILABLE)) {
					key = "FailedGiftCardPaymentGroupAuthorizationUnavailable";
					pStatusMessage = "";
				} else if (DigitalStringUtil.isNotBlank(pStatusMessage) && pStatusMessage
						.equalsIgnoreCase(GIFTCARD_INSUFFICIENT_FUNDS)) {
					key = GIFTCARD_INSUFFICIENT_FUNDS;
					pStatusMessage = "";
				}
				errorMessage = MessageFormat.format(pBundle.getString(key),
						new Object[]{
								pFailedPaymentGroup.getId(),
								pStatusMessage
						});
				errorKey = key;
				pResult.addError(errorKey, errorMessage);
				if (this.isLoggingError()) {
					this.logError(key + " :: " + pStatusMessage);
				}
			} else {
				super.addPaymentGroupError(pFailedPaymentGroup, pStatusMessage, pResult, pBundle);
			}
		}
	  
	/** This method handles adding authorization error messages to the pipeline
	 * result object. It creates an errorMessage and errorKey and adds these to
	 * the ResultObject.
	 * 
	 * @param pFailedPaymentGroup
	 *            the payment group that failed to authorize
	 * @param pStatusMessage
	 *            message indicating why the payment group failed to authorize
	 * @param pResult
	 *            the pipeline result object.
	 * @param pBundle
	 *            resource bundle specific to users locale */
	protected void addCreditCardError( PaymentGroup pFailedPaymentGroup, String pStatusMessage, PipelineResult pResult, ResourceBundle pBundle ) {

		String errorMessage;
		String authFraudMessageKey;
		String errorKey;

		if( pStatusMessage == null ) {
			errorMessage = getMessageLocator().getMessageString(FAILED_CREDITCARD_AUTH);
		} else {
			if( isLoggingDebug() ) {
				logDebug( "\n\n\n\n\n\naddCreditCardError : pStatusMessage : " + pStatusMessage );
			}
			authFraudMessageKey = getAuthFraudLocator().getReasonCodeMap().getProperty( pStatusMessage );

			if( isLoggingDebug() ) {
				logDebug( "\n\n\naddCreditCardError :  authFraudMessageKey : " + authFraudMessageKey );
			}

			if( authFraudMessageKey == null ) {
				
				errorMessage = getMessageLocator().getMessageString(FAILED_CREDITCARD_AUTH);
			} else {
				errorMessage = getMessageLocator().getMessageString( authFraudMessageKey );
			}

			if( isLoggingDebug() ) {
				logDebug( "\n\n\naddCreditCardError : errorMessage :" + errorMessage );
			}
		}
		errorKey = "FailedCreditCardAuth:" + pFailedPaymentGroup.getId();
		pResult.addError( errorKey, errorMessage );
	}
}
