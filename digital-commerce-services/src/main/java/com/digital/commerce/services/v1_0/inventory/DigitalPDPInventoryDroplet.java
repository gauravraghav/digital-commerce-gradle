/**
 * 
 */
package com.digital.commerce.services.v1_0.inventory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.region.RegionProfile;
import com.digital.commerce.services.storelocator.DigitalCoordinateManager;
import com.digital.commerce.services.storelocator.DigitalGeoLocatorProvider;
import com.digital.commerce.services.storelocator.DigitalStoreLocatorInventoryManager;

import atg.commerce.catalog.CatalogTools;
import atg.commerce.inventory.InventoryDroplet;
import atg.commerce.inventory.InventoryException;
import atg.commerce.inventory.InventoryInfo;
import atg.commerce.inventory.InventoryManager;
import atg.commerce.locations.Coordinate;
import atg.dtm.UserTransactionDemarcation;
import atg.multisite.Site;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jvasired
 *
 */
@SuppressWarnings("unchecked")
@Getter
@Setter
public class DigitalPDPInventoryDroplet extends InventoryDroplet{
	
	private CatalogTools catalogTools;
	private DigitalGeoLocatorProvider geoLocatorProvider;
	private DigitalCoordinateManager coOrdinateManager;
	private InventoryManager inventoryManager;
	private String rolledUpInventoryId;
	private DigitalStoreLocatorInventoryManager storeLocatorInventoryManager;
	private MessageLocator messageLocator;
	private RegionProfile regionProfile;
	private DigitalBaseConstants dswConstants;
	
	
	private static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	private static final String LOCATION_ID = "locationId";
	private static final String POSTAL_CODE = "postalCode";
	private static final String FILTEROUTOF_STOCK = "filterOutOfStock";
	private static final String PRODUCT_ID = "productId";
	private static final String SKU_ID = "skuId";
	private static final String ERROR = "error";
	private static final String LATITUDE = "latitude";
	private static final String LONGITUDE = "longitude";
	private static final String CITY = "city";
	private static final String STATE = "state";
	private static final String CHILDSKUS = "childSKUs";
	private static final String STORELEVEL_INVENTORY = "storeLevelInventory";
	private static final String ROLLEDUP_INVENTORY = "rolledUpInventory";
	private static final String INVENTORY_INFO = "inventoryInfo";
	private static final String LOCATE_INVENTORY = "locateItemsInventory";
	private static final String SKUANDPRODUCT_INVENTORY_OF_STORE = "SKUAndProductInventoryOfStore";
	
	private static final String    PERFORM_MONITOR_NAME    = "DigitalPDPInventoryDropet";
	private static final String    PRODUCT_SKU_ID_EMPTY = "PRODUCT_SKU_ID_EMPTY";
	private static final String    ERROR_INVENTORY = "ERROR_INVENTORY";
	private static final String    ERROR_GETTING_SKU = "ERROR_GETTING_SKU";
	
	/**
	 * 
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			     throws ServletException, IOException{
		
		StringBuffer error = new StringBuffer();
		String METHOD_NAME = "service";
		UserTransactionDemarcation td = null;
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			td = TransactionUtils.startNewTransaction(this.getName(), METHOD_NAME);
			if (!getDswConstants().isBccPreviewEnabled()) {			
				String locationId = pRequest.getParameter(LOCATION_ID);
				if (DigitalStringUtil.isEmpty(locationId)) {
					locationId = getLocationId(pRequest,error);
				}
				String availabilityFilterString = pRequest.getParameter(FILTEROUTOF_STOCK);
				boolean availabilityFilter = false;
				if (availabilityFilterString != null) {
					availabilityFilter = Boolean.parseBoolean(availabilityFilterString);
				}
				if (!DigitalStringUtil.isEmpty(pRequest.getParameter(PRODUCT_ID))) {
					//get inventory data for PDP page and checkout pages
					getProductInventoryDetails(pRequest,locationId,availabilityFilter);
				} else if (!DigitalStringUtil.isEmpty(pRequest.getParameter(SKU_ID))) {
					final String skuId = pRequest.getParameter(SKU_ID);
					@SuppressWarnings("serial")
					List<String> skuList = new ArrayList<String>() {{add(skuId);}};
					getInventory(skuList, locationId,availabilityFilter, pRequest);
				} else {
					error.append(this.getMessageLocator().getMessageString(PRODUCT_SKU_ID_EMPTY));
					pRequest.setParameter(ERROR, error.toString());
				}
			}
			pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
		} catch (Exception e) {
			String msg = this.getMessageLocator().getMessageString(ERROR_INVENTORY);
			if (isLoggingError()) {
				logError(msg+" "+e.getMessage());
				pRequest.setParameter(ERROR, msg);
			}
			TransactionUtils.rollBackTransaction(td, this.getName(), METHOD_NAME);
		}
		finally {
			TransactionUtils.endTransaction(td, this.getName(), METHOD_NAME);
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
	}
	
	/**
	 * Returns Product Level Inventory details.
	 * @param pRequest
	 * @param respMsg
	 * @return
	 */
	private void getProductInventoryDetails(DynamoHttpServletRequest pRequest,String locationId,
			boolean availabilityFilter){
		String METHOD_NAME = "getLocationId";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			if (!DigitalStringUtil.isEmpty(pRequest.getParameter(PRODUCT_ID))) {
				String productId = pRequest.getParameter(PRODUCT_ID);
				String locateItemsInventory = pRequest.getParameter(LOCATE_INVENTORY);
				List<RepositoryItem> skuItems=null;
				if(null!=productId){
					skuItems = getCatalogRefItems(productId);
				}
				if(locateItemsInventory!=null && Boolean.valueOf(locateItemsInventory) && locationId!=null && productId!=null){
					
					if(null!=skuItems){
						Map<String,Object> productStoreInventory = storeLocatorInventoryManager.getInventoryForSkusItems(skuItems,locationId);
						pRequest.setParameter(SKUANDPRODUCT_INVENTORY_OF_STORE, productStoreInventory);
					}
				}else{
					getInventory(getCatalogRefIds(productId), locationId,availabilityFilter, pRequest);
				}
			}
		}finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }	
	}
	
	/**
	 * Gets the locationId for postalCode/Latitude/Longitude/City&State
	 * @param pRequest
	 * @return
	 * @throws RepositoryException
	 */
	private String getLocationId(DynamoHttpServletRequest pRequest,StringBuffer respMsg) throws RepositoryException {
		String METHOD_NAME = "getLocationId";
		Coordinate coordinate = null;
		String postalCode = pRequest.getParameter(POSTAL_CODE);
		String locationId = null;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			if(!DigitalStringUtil.isEmpty(postalCode)){
				coordinate = geoLocatorProvider.getGeoLocation(postalCode, null);
			}else if(!DigitalStringUtil.isEmpty(pRequest.getParameter(LATITUDE)) && !DigitalStringUtil.isEmpty(pRequest.getParameter(LONGITUDE))){
				coordinate = new Coordinate(Double.valueOf(pRequest.getParameter(LATITUDE)),Double.valueOf(pRequest.getParameter(LONGITUDE)));
			}else if(!DigitalStringUtil.isEmpty(pRequest.getParameter(CITY)) && !DigitalStringUtil.isEmpty(pRequest.getParameter(STATE))){
				coordinate = geoLocatorProvider.getGeoLocation(pRequest.getParameter(CITY),pRequest.getParameter(STATE),new Locale("en"));
			}else if(regionProfile.getLatitude()!=0.0 && regionProfile.getLongitude()!=0.0){
				//Read the latitude and longitude from Akamai Headers
				coordinate = new Coordinate(regionProfile.getLatitude(),regionProfile.getLongitude());
			}
			if(coordinate!=null){
				Site site = MultiSiteUtil.getSite();
				List<String> siteIds = new ArrayList<>();
				if(null!=site){
					siteIds.add(site.getId());
				}
				RepositoryItem[] items = coOrdinateManager.getNearest(coordinate, 0.0, siteIds);
				if(items!=null && items.length>0){
					Collections.sort(Arrays.asList(items),new Comparator<RepositoryItem>() {
							public int compare(RepositoryItem o1, RepositoryItem o2) {
								return ((Double)o1.getPropertyValue("distance")).compareTo((Double)o2.getPropertyValue("distance"));
							}
						});
					 if(items!=null && items.length>0){
							return ((String)((RepositoryItem)items[0]).getRepositoryId());
					  }
				}
				if(isLoggingDebug()){
					logDebug("DigitalPDPInventoryDropet Location Id is "+locationId);
				}
			}else{
				if(isLoggingDebug()){
					logDebug("DigitalPDPInventoryDropet Co-odinates are Null");
				}
			}
		}finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		return locationId;
	}

	/**
	 * Helper method to get catalogRefIds for product
	 * 
	 * @param productId
	 * @return
	 */
   private List<String> getCatalogRefIds(String productId) {
	    String METHOD_NAME = "getCatalogRefIds";
		List<String> skuIdsList = null;
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			RepositoryItem productItem = catalogTools.findProduct(productId);
			if (productItem != null) {
				List<RepositoryItem> skusList = (List<RepositoryItem>) productItem
						.getPropertyValue(CHILDSKUS);
				skuIdsList = new ArrayList<>();
				for (RepositoryItem skuItem : skusList) {
					skuIdsList.add((String) skuItem.getRepositoryId());
				}
			}
			if(isLoggingDebug()){
				for(String skuId : skuIdsList){
					logDebug("DigitalPDPInventoryDropet SkuIds Id is "+skuId);
				}
			}
		} catch (RepositoryException e) {
			String msg = this.getMessageLocator().getMessageString(ERROR_GETTING_SKU);
			if (isLoggingError()) {
				logError(msg+" "+e.getMessage());
			}
		}
		finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		return skuIdsList;
	}
   
   
   private List<RepositoryItem> getCatalogRefItems(String productId) {
	    String METHOD_NAME = "getCatalogRefItems";
		List<RepositoryItem> skuItems=null;
		
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
			RepositoryItem productItem = catalogTools.findProduct(productId);
			if (productItem != null) {
				skuItems = (List<RepositoryItem>) productItem.getPropertyValue(CHILDSKUS);
			}
		} catch (RepositoryException e) {
			String msg = this.getMessageLocator().getMessageString(ERROR_GETTING_SKU);
			if (isLoggingError()) {
				logError(msg+" "+e.getMessage());
			}
		}
		finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
		return skuItems;
	}
   
   /**
    * 
    * @param skuIdsList
    * @param locationId
    * @param availabilityFilter
    * @param pRequest
 * @return 
    */
   private Collection<InventoryInfo> getInventory(List<String> skuIdsList,String locationId,boolean availabilityFilter, DynamoHttpServletRequest pRequest){
	   String METHOD_NAME = "getInventory";
	   Collection<InventoryInfo> inventoryInfos = null;
	   try {
		    DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
		    if(isLoggingDebug()){
				logDebug("DigitalPDPInventoryDropet getInventory method");
			}
			inventoryInfos = super.getBatchInfo(skuIdsList, locationId, inventoryManager,
					availabilityFilter);
			Collection<InventoryInfo> rolledUpinventoryInfos = super.getBatchInfo(skuIdsList, rolledUpInventoryId,
					inventoryManager, availabilityFilter);
			Map<String, Collection<InventoryInfo>> inventoryInfosMap = new HashMap<>();
			inventoryInfosMap.put(STORELEVEL_INVENTORY, inventoryInfos);
			inventoryInfosMap.put(ROLLEDUP_INVENTORY,rolledUpinventoryInfos);
			pRequest.setParameter(INVENTORY_INFO, inventoryInfosMap);
		} catch (InventoryException ex) {
			String msg = this.getMessageLocator().getMessageString(ERROR_INVENTORY);
			if (isLoggingError()) {
				logError(msg+" "+ex.getMessage());
			}
		}
	    finally {
	    	DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(PERFORM_MONITOR_NAME, METHOD_NAME);
	    }
	   return inventoryInfos;
   }
}
