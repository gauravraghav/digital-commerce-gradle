package com.digital.commerce.services.order;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class DigitalOrderHistoryItem implements Serializable, Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String	shipToFirstName;
	private String	productDescription;
	private String	pickupLocationId;
	private String	returnLocationId;
	private double	unitPrice;
	private String	fulfillLocationId;
	private String	productId;
	private String	skuId;
	private String	shipToLastName;
	private int		quantity;
	private Date	addDate;
	private Date	updateDate;
	private Date	orderPlacementDate;
	private String	trackingNumber;
	private String	carrier;
	private String	status;
	private String	statusDescription; //added to preserve Yantra status description if we need in future
	private Date	statusDate;
	private String	yantraLineItemId;
	private String	atgCommerceId;
	private String	gcFromName;
	private String	gcToName;
	private String	gcFromEmailAddress;
	private String	gcToEmailAddress;
	private String fulfillmentType;
	private boolean shipsToStore;
	private String shipToAddress;
}
