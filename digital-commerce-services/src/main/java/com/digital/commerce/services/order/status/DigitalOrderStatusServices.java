package com.digital.commerce.services.order.status;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringEscapeUtils;

import atg.nucleus.logging.ApplicationLoggingImpl;

import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.integration.order.processor.OrderStatusProcessor;

@Getter
@Setter
public class DigitalOrderStatusServices extends ApplicationLoggingImpl{
	OrderStatusProcessor orderStatusProcessor;
	
	public void handleOrderStatusEvent(String xml){
		String pOrderXML=xml;
		try {
			pOrderXML=StringEscapeUtils.escapeXml(pOrderXML);
			pOrderXML = DigitalCommonUtil.getDecodedString(xml);
			this.getOrderStatusProcessor().processOrderStatus(pOrderXML);
		}catch(Exception ex){
			logError("Error DigitalOrderStatusServices :: ",ex);
		}			
	}
}
