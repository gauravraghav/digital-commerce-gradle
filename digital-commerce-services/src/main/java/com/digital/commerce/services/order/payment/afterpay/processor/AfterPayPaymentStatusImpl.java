package com.digital.commerce.services.order.payment.afterpay.processor;

import atg.payment.PaymentStatusImpl;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class AfterPayPaymentStatusImpl extends PaymentStatusImpl implements AfterPayPaymentStatus {

    private static final long serialVersionUID = 1L;

    private String paymentId;
    private String status;
    private Date createdDate;
    private String merchantReference;

    public AfterPayPaymentStatusImpl(){

    }

    public AfterPayPaymentStatusImpl(String pTransactionId, double pAmount, boolean pTransactionSuccess, String pErrorMessage, Date pTransactionTimestamp
                        , String paymentId, String status, Date createdDate, String merchantReference) {
        super(pTransactionId,pAmount,pTransactionSuccess,pErrorMessage,pTransactionTimestamp);
        this.paymentId = paymentId;
        this.status = status;
        this.createdDate = createdDate;
        this.merchantReference = merchantReference;
    }
}
