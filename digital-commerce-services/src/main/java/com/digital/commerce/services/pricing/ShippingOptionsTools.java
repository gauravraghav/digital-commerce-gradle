package com.digital.commerce.services.pricing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;

import com.digital.commerce.common.util.DigitalStringUtil;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.shipping.ShippingMethod;
import com.digital.commerce.services.payment.BillingShippingServices;

import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.commerce.pricing.ShippingPricingEngine;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;


/** An attempt to extract functionality out of DigitalShippingOptionsDroplet to leverage the logic in mobile.
 * 
 */
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class ShippingOptionsTools {
	private static final DigitalLogger						logger						= DigitalLogger.getLogger( ShippingOptionsTools.class );
	private static final String						SHIPPING_OPTIONS_ERROR		= "shippingOptionsError";
	private static final ShippingMethodComparator	SHIPPING_METHOD_COMPARATOR	= new ShippingMethodComparator();
	private PricingTools							pricingTools;
	private DigitalShippingGroupManager					shippingGroupManager;
	private ShippingHandlingInfo					shippingHandlingInfo;
	private Profile									profile;
	private PricingModelHolder						userPricingModel;
	private MessageLocator							messageLocator;
	private ShippingPricingEngine					shippingPricingEngine;
	private BillingShippingServices billingShippingServices;

	public ShippingGroup findFirstNonGiftCardShippingGroup( Order order ) {
		ShippingGroup retVal = null;
		List shippingGroupList = order.getShippingGroups();
		if( shippingGroupList != null ) {
			for( int i = 0; retVal == null && i < shippingGroupList.size(); i++ ) {
				ShippingGroup sg = (ShippingGroup)shippingGroupList.get( i );
				if( sg instanceof HardgoodShippingGroup && !( shippingGroupManager.isGiftCardShippingGroup( sg ) ) ) {
					HardgoodShippingGroup hsg = (HardgoodShippingGroup)sg;
					String shipType = (String)hsg.getPropertyValue("shipType");
					if ( shipType == null || shipType.equalsIgnoreCase("ship")) {
						retVal = sg;
					}
				}
			}
		}
		return retVal;
	}
	

	public List loadAvailableShippingOptions( Order order, Locale locale ) {
		DigitalShippingGroupManager sgm = (DigitalShippingGroupManager)shippingGroupManager;
		List availableShippingOptions = new ArrayList();
		if( order.getShippingGroups() != null && !order.getShippingGroups().isEmpty() && ( !( order instanceof DigitalOrderImpl ) || !( (DigitalOrderImpl)order ).getIsGiftCardOrderOnly() ) ) {
			ShippingRateTable shippingRateTable = getShippingRateTable( order );
			for( Iterator shippingGroupIterator = order.getShippingGroups().iterator(); shippingGroupIterator.hasNext(); ) {
				ShippingGroup sg = (ShippingGroup)shippingGroupIterator.next();
				String shippingMethod = sg.getShippingMethod();
				if( shippingMethod != null && sg instanceof HardgoodShippingGroup && !sgm.isGiftCardShippingGroup( sg ) && sg.getCommerceItemRelationshipCount() != 0) {
					List shippingOptions = shippingRateTable.getRatesList( sg.getId() );
					if( shippingOptions != null && shippingOptions.size() > 0 ) {
						for( Iterator shippingOptionsIterator = shippingOptions.iterator(); shippingOptionsIterator.hasNext(); ) {
							ShippingRate shippingOption = (ShippingRate)shippingOptionsIterator.next();
							shippingOption.setValid( true );
							availableShippingOptions.add( shippingOption );
						}
						if( findShippingRate( availableShippingOptions, shippingMethod ) == null ) {
							ShippingRate shippingRate = new ShippingRate();
							shippingRate.setBaseRateServiceLevel( shippingMethod );
							shippingRate.setBaseRateServiceName( shippingMethod );
							shippingRate.setValid( false );
							/*Object[] params = { messageLocator.getMessageString( ShippingUtils.getNameMessageLocatorKey( shippingMethod ) ) };

							String messageKey = "errInvalidLOS";
							if( ( (DSWOrderImpl)order ).isStoreOnlyShipping() && !( ShippingMethod.Ground.getMappedValue().equals( shippingMethod ) ) ) {
								messageKey = "invalidStoreShippingMethod";
							}
							shippingRate.setErrorMessage( messageLocator.getMessageString( messageKey, params ) );*/
							availableShippingOptions.add( shippingRate );
						}
					} else {
						ShippingRate shippingRate = new ShippingRate();
						shippingRate.setBaseRateServiceLevel( shippingMethod );
						shippingRate.setBaseRateServiceName( shippingMethod );
						shippingRate.setValid( false );
						shippingRate.setErrorMessage( messageLocator.getMessageString( "errHazMatShippingNextDay" ) );
						availableShippingOptions.add( shippingRate );
					}
				}
			}
		}
		return availableShippingOptions;
	}

	public ShippingRate findShippingRate( List shippingOptions, String shippingMethod ) {
		ShippingRate retVal = null;
		if( shippingMethod != null && shippingOptions != null ) {
			for( Iterator iterator = shippingOptions.iterator(); retVal == null && iterator.hasNext(); ) {
				ShippingRate shippingRate = (ShippingRate)iterator.next();
				if( shippingMethod.equalsIgnoreCase( shippingRate.getBaseRateServiceName() ) ) {
					retVal = shippingRate;
				}
			}
		}
		return retVal;
	}

	/** Gets the shipping rate table. If the shipping rate table is unavailable
	 * (e.g., web service is down), the {@link DigitalOrderImpl#setShippingAvailable(boolean)} is called to indicate
	 * that shipping is dead. A static table is then returned.
	 * 
	 * 
	 * @return */
	private ShippingRateTable getShippingRateTable( Order order ) {
		ShippingRateTable shippingRateTable = null;
		try {
			shippingRateTable = getBillingShippingServices().getShippingRateTable( order, "Sales" );
			if( order instanceof DigitalOrderImpl ) {
				( (DigitalOrderImpl)order ).setShippingAvailable( true );
			}
		} catch (DigitalIntegrationException e) {
			if( order instanceof DigitalOrderImpl ) {
				( (DigitalOrderImpl)order ).setShippingAvailable( false );
			}
			shippingRateTable = shippingHandlingInfo.getStaticShippingRateTable( order );
			
			logger.error(e.getMessage());
			} catch ( DigitalAppException e) {
			if( order instanceof DigitalOrderImpl ) {
				( (DigitalOrderImpl)order ).setShippingAvailable( false );
			}
			shippingRateTable = shippingHandlingInfo.getStaticShippingRateTable( order );
			ServletUtil.getCurrentRequest().setParameter( SHIPPING_OPTIONS_ERROR, messageLocator.getMessageString( "invalidShippingAddress" ) );
			}
		return shippingRateTable;
	}

	/** Gets the selected shipping method for the order */
	public String getSelectedShippingMethod( Order order ) {
		return shippingGroupManager.getShippingMethod( order ); 
	}

	/** Returns the possible shipping methods for the shipping group.
	 * Store specific shipping methods are not in the returned list.
	 * 
	 * @param request
	 * @param response
	 * @param order
	 * @param sg
	 * @param locale
	 * @return
	 * @throws ServletException
	 * @throws IOException */
	public List getPossibleMethods( Order order, ShippingGroup sg, Locale locale ) throws ServletException, IOException {
		ShippingRateTable rateTable = shippingHandlingInfo.getStaticShippingRateTable( order );
		Map extraParams = new HashMap();
		extraParams.put( "RATE_TABLE", rateTable );
		List<String> possibleMethods = null;
		try {
			possibleMethods = shippingPricingEngine.getAvailableMethods( sg, userPricingModel.getShippingPricingModels(), locale, profile, extraParams );
			for( Iterator<String> iterator = possibleMethods.iterator(); iterator.hasNext(); ) {
				String shippingMethod = iterator.next();
				if( DigitalStringUtil.isBlank( shippingMethod ) || shippingMethod.length() != 3 || shippingMethod.startsWith( "S" ) ) {
					iterator.remove();
				}
			}
		} catch( PricingException exc ) {
			logger.error( "Unable to get Possible Shipping methods ", exc );
		}
		return possibleMethods;
	}

	/** Returns the best available shipping method. It assumes that the options are
	 * in ascending order.
	 * 
	 * @param shippingOptions
	 * @param selectedShippingMethod
	 * @return */
	public String getBestPossibleShippingMethod( List availableShippingOptions, List possibleMethods, String selectedShippingMethod ) {
		String shippingMethod = selectedShippingMethod;
		ShippingRate selectedShippingRate = findShippingRate( availableShippingOptions, selectedShippingMethod );
		if( availableShippingOptions != null ) {
			Collections.sort( availableShippingOptions, SHIPPING_METHOD_COMPARATOR );
		}
		if( selectedShippingRate == null && availableShippingOptions != null && !availableShippingOptions.isEmpty() ) {
			selectedShippingRate = (ShippingRate)availableShippingOptions.get( 0 );
		}
		if( selectedShippingRate != null && !selectedShippingRate.isValid() ) {
			shippingMethod = null;
			if( availableShippingOptions != null ) {
				for( int i = availableShippingOptions.indexOf( selectedShippingRate ); shippingMethod == null && i >= 0; i-- ) {
					ShippingRate shippingRate = (ShippingRate)availableShippingOptions.get( i );
					if( shippingRate.isValid() ) {
						shippingMethod = ( (ShippingRate)shippingRate ).getBaseRateServiceName();
					}
				}
			} else if( possibleMethods != null && !possibleMethods.isEmpty() ) {
				Collections.sort( possibleMethods, SHIPPING_METHOD_COMPARATOR );
				shippingMethod = (String)possibleMethods.get( 0 );
			}
		}
		return shippingMethod;
	}

	/** Loads the static list of shipping rates.
	 * 
	 * @param order
	 * @param sg
	 * @param locale
	 * @param availableMethods
	 * @return */
	public List loadShippingOptions( Order order, ShippingGroup sg, Locale locale, final List availableMethods, String selectedShippingMethod ) {
		List shippingRateAndOptions = null;
		if( availableMethods != null && availableMethods.size() > 0 ) {
			shippingRateAndOptions = repriceShippingGroup( sg, order, locale, availableMethods, selectedShippingMethod );
		}
		return shippingRateAndOptions;
	}

	/** Reprices the entire shipping group for all available shipping rates. If a
	 * shipping group has a current shipping method, make sure to reprice it last
	 * so that the order is left in its original state.
	 * 
	 * @param order
	 * @param locale
	 * @param shippingOptions
	 * @param sg */
	private List repriceShippingGroup( ShippingGroup sg, Order order, Locale locale, List shippingMethods, String selectedShippingMethod ) {
		List modifiedShippingOptions = new ArrayList();
		List sortedShippingMethods = sortShippingMethods( shippingMethods, selectedShippingMethod );
		for( Iterator iterator = sortedShippingMethods.iterator(); iterator.hasNext(); ) {
			String shippingMethod = (String)iterator.next();
			sg.setShippingMethod( shippingMethod );
			modifiedShippingOptions.add( createShippingGroupShippingRate( order, locale ) );
		}
		return modifiedShippingOptions;
	}

	private List sortShippingMethods( List shippingMethods, final String selectedShippingMethod ) {
		List sortedShippingMethods = new ArrayList();
		for( Iterator iterator = shippingMethods.iterator(); iterator.hasNext(); ) {
			String shippingMethod = (String)iterator.next();
			if( selectedShippingMethod == null || !selectedShippingMethod.equalsIgnoreCase( shippingMethod ) ) {
				sortedShippingMethods.add( shippingMethod );
			}
		}
		if( null!=selectedShippingMethod && DigitalStringUtil.isNotBlank( selectedShippingMethod ) && selectedShippingMethod.length() == 3 ) {
			sortedShippingMethods.add( selectedShippingMethod );
		}
		return sortedShippingMethods;
	}

	protected ShippingRate createShippingGroupShippingRate( Order order, Locale locale ) {
		ShippingRate retVal = new ShippingRate();
		final ShippingGroup shippingGroup = findFirstNonGiftCardShippingGroup( order );
		if( shippingGroup != null ) {
			priceShipping( order, locale );
			if( shippingGroup.getPriceInfo() != null ) {
				retVal.setBaseRate( shippingGroup.getPriceInfo().getAmount() );
				retVal.setValid( true );
			} else {
				retVal.setValid( false );
			}
			retVal.setBaseRateServiceLevel( shippingGroup.getShippingMethod() );
			retVal.setBaseRateServiceName( shippingGroup.getShippingMethod() );
		}
		return retVal;
	}

	/** Ignores {@link PricingException}s. Reprices the shipping only. It returns the total for all of the shipping,
	 * but the shipping group should be inspected to determine what the price really is. This should
	 * not affect tax, discounts, or order total.
	 * 
	 * @param order
	 * @param locale */
	protected double priceShipping( Order order, Locale locale ) {
		double shipping = 0.0;
		try {
			synchronized( order ) {
				shipping = pricingTools.priceShippingForOrderTotal( order, userPricingModel, locale, profile, null );
			}
		} catch( PricingException pex ) {
		}
		return shipping;
	}

	public Comparator<Object> getShippingMethodComparator() {
		return SHIPPING_METHOD_COMPARATOR;
	}


	public void setDefaultLocale( Locale defaultLocale ) {
	}

	protected static class ShippingMethodComparator implements Comparator<Object> {
		public int compare( Object o1, Object o2 ) {
			Integer o1Value = getNumericValue( o1 );
			Integer o2Value = getNumericValue( o2 );
			return o1Value.compareTo( o2Value );
		}

		private Integer getNumericValue( Object o ) {
			String shippingMethod = null;
			if( o instanceof ShippingRate ) {
				shippingMethod = ( (ShippingRate)o ).getBaseRateServiceName();
			} else if( o instanceof String ) {
				shippingMethod = o.toString();
			}
			if( ShippingMethod.Ground.equals( shippingMethod ) ) {
				return Integer.valueOf( 1 );
			} else if( ShippingMethod.SecondDay.equals( shippingMethod ) ) {
				return Integer.valueOf( 2 );
			} else if( ShippingMethod.NextDay.equals( shippingMethod ) ) { return Integer.valueOf( 3 ); }
			return Integer.valueOf( 4 );
		}
	}

	/**
	 * @return the billingShippingServices
	 */
	public BillingShippingServices getBillingShippingServices() {
		return billingShippingServices;
	}


	/**
	 * @param billingShippingServices the billingShippingServices to set
	 */
	public void setBillingShippingServices(
			BillingShippingServices billingShippingServices) {
		this.billingShippingServices = billingShippingServices;
	}

}
