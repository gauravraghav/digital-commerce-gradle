/**
 *
 */
package com.digital.commerce.services.order.payment.giftcard.processor;

import static com.digital.commerce.constants.DigitalProfileConstants.GIFTCARD_ERROR_CODES;
import static com.digital.commerce.constants.DigitalProfileConstants.GIFTCARD_INSUFFICIENT_FUNDS;
import static com.digital.commerce.constants.DigitalProfileConstants.GIFTCARD_INTEGRATION_DEFAULT_ERROR_MESSAGE;

import atg.servlet.ServletUtil;
import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.payment.giftcard.GiftCardAuthHolder;
import com.digital.commerce.services.order.payment.giftcard.GiftCardAuthInfo;
import com.digital.commerce.services.order.payment.giftcard.GiftCardInfo;
import com.digital.commerce.services.order.payment.giftcard.GiftCardPaymentServices;
import com.digital.commerce.services.order.payment.giftcard.GiftCardStatus;
import com.digital.commerce.services.order.payment.giftcard.GiftCardStatusImpl;
import com.digital.commerce.services.order.payment.giftcard.valueobject.DigitalGiftCardApproval;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;


/** This class is a gift card processor.
 * 
 * @author djboyd */
@Getter
@Setter
public class GiftCardProcessorImpl implements GiftCardProcessor {

	private GiftCardPaymentServices			paymentServices;
	private String					giftCardAuthHolderPath;
	private MessageLocator messageLocator;
	private Boolean loggingDebug;
	private Boolean loggingError;

	private static final DigitalLogger logger = DigitalLogger.getLogger(GiftCardProcessorImpl.class);
	
	public GiftCardProcessorImpl() {}

	/* (non-Javadoc)
	 * @see com.digital.edt.payment.giftcard.GiftCardProcessor#authorize(com.digital.edt.payment.giftcard.GiftCardInfo) */
	/** used to pre authorize a gift card, that is to determine if the gift card
	 * is valid and if there is money to cover all or partially the cost of the
	 * cart. */
	public GiftCardStatus authorize( GiftCardInfo pGiftCardInfo ) {
		/** Check to see if there has already been a successful Auth operation
		 * for this giftcard paymentgroup In this session. */
		GiftCardStatusImpl status = new GiftCardStatusImpl();
		GiftCardAuthInfo storedGiftCardAuthInfo = getStoredAuthInfo( pGiftCardInfo.getPaymentId() );
		if( storedGiftCardAuthInfo != null ) {
			populateStatusFromAuthInfo( storedGiftCardAuthInfo, status );
		} else {
			// TODO current spec says preauth/auth are the only available
			// methods,
			// I'm passing preauth here and auth in the debit section. This will
			// change once the official spec is in
			try {
				DigitalGiftCardApproval giftCardApproval = getPaymentServices().authorizeGiftCardPayment( pGiftCardInfo.getGiftCardNumber(), pGiftCardInfo.getPinNumber(), pGiftCardInfo.getAmount() );
				status.setAmount( giftCardApproval.getApprovalAmount() );
				status.setAuthorizationCode( giftCardApproval.getAuthorizationCode() );
				status.setTransactionId( giftCardApproval.getTransactionId() );
				status.setErrorMessage( getMessageLocator().getErrorMessageFromErrorCodeMap( GIFTCARD_ERROR_CODES, giftCardApproval.getStatusMessage() ) );
				status.setTransactionSuccess( giftCardApproval.isApprovalFlag() );
				status.setTransactionTimestamp( new Date() );
				Calendar c = giftCardApproval.getAuthTimeStamp();

				if( c != null ) {
					status.setAuthorizationExpiration( new Timestamp( c.getTimeInMillis() ) );				
				}
				GiftCardAuthInfo giftCardAuthInfo = new GiftCardAuthInfo();
				if (giftCardApproval.isApprovalFlag()) {
					if(pGiftCardInfo.getAmount() != giftCardApproval.getApprovalAmount()){
						logger.error("There is not enough GiftCard balance left");
						String error = GIFTCARD_INSUFFICIENT_FUNDS;
						status = new GiftCardStatusImpl(giftCardApproval.getTransactionId(),
								giftCardApproval.getApprovalAmount(), false, error, new Date());
						status.setAuthorizationCode(giftCardApproval.getAuthorizationCode());
					} else {
						giftCardAuthInfo.setAmount(giftCardApproval.getApprovalAmount());
						giftCardAuthInfo.setTransactionId(giftCardApproval.getTransactionId());
						giftCardAuthInfo.setErrorMessage(getMessageLocator()
								.getErrorMessageFromErrorCodeMap(GIFTCARD_ERROR_CODES,
										giftCardApproval.getResponseCode()));
						giftCardAuthInfo.setTransactionSuccess(giftCardApproval.isApprovalFlag());
						giftCardAuthInfo.setTransactionTimestamp(new Date());
						if (c != null) {
							giftCardAuthInfo.setAuthorizationExpiration(new Timestamp(c.getTimeInMillis()));
						}
						giftCardAuthInfo.setAuthorizationCode(giftCardApproval.getAuthorizationCode());
						storeAuthInfo(pGiftCardInfo.getPaymentId(), giftCardAuthInfo);
					}
				} else {
					logger.error("Authorizing a GiftCard Failed");
					String error = getMessageLocator().getErrorMessageFromErrorCodeMap(GIFTCARD_ERROR_CODES,
							giftCardApproval.getResponseCode());
					status = new GiftCardStatusImpl(giftCardApproval.getTransactionId(),
							giftCardApproval.getApprovalAmount(), false, error, new Date());
					status.setAuthorizationCode(giftCardApproval.getAuthorizationCode());
				}
			} catch( DigitalAppException e ) {
					logger.error(e.getMessage() + "authorize()" + pGiftCardInfo.getOrderNumber() );
					logger.error("DigitalAppException while authorizing a GiftCard  ::  "+e);
				status.setTransactionSuccess( false );
				status.setErrorMessage( getMessageLocator().getMessageString(GIFTCARD_INTEGRATION_DEFAULT_ERROR_MESSAGE) );
				
			}
		}

		return status;
	}

	/** cancel the preAuth on the gift card, by passing $0.0 to the service call.
	 * if the call is success, the storedAuthInfo will be removed. If the call
	 * result is failure, then nothing is to done.
	 * 
	 * @param DigitalGiftCard
	 * @param Order
	 * @return GiftCardStatus */
	public GiftCardStatus expireAuthorization( DigitalGiftCard pPaymentGroup, String preAuthId ) {
		if( isLoggingDebug() ) {
			logger.debug( "expire authorization on gift card " + pPaymentGroup.getCardNumber() );
		}
		GiftCardStatusImpl status = new GiftCardStatusImpl();
		try {
			DigitalGiftCardApproval giftCardApproval = getPaymentServices().expireAuthorizeGiftCardPayment( pPaymentGroup.getCardNumber(), pPaymentGroup.getPinNumber(), preAuthId, pPaymentGroup.getAmount() );
			status.setAmount( giftCardApproval.getApprovalAmount() );
			status.setAuthorizationCode( giftCardApproval.getAuthorizationCode() );
			status.setTransactionId( giftCardApproval.getTransactionId() );
			status.setErrorMessage( giftCardApproval.getStatusMessage() );
			status.setTransactionSuccess( giftCardApproval.isApprovalFlag() );
			status.setTransactionTimestamp( new Date() );

			if( giftCardApproval.isApprovalFlag() ) {
				removAuthInfo( pPaymentGroup.getId() );
			}
		} catch( DigitalIntegrationException e ) {
			logger.error("Exception: expire authorize() on gift card", e);
			status.setTransactionSuccess( false );
			status.setErrorMessage( getMessageLocator().getErrorMessageFromErrorCodeMap( GIFTCARD_ERROR_CODES, GIFTCARD_INTEGRATION_DEFAULT_ERROR_MESSAGE ) );
			if( isLoggingError() ) {
			 logger.error(e.getMessage() + "expire authorize()" );
			}
		} catch( DigitalAppException e ) {
			if( isLoggingError() ) {
				logger.error( e );
			}
		} 
		return status;
	}

	public GiftCardStatus debit( GiftCardInfo pGiftCardInfo, GiftCardStatus pStatus ) {
		return new GiftCardStatusImpl();
	}

	public GiftCardStatus credit( GiftCardInfo pGiftCardInfo, GiftCardStatus pStatus ) {
		return new GiftCardStatusImpl();
	}

	public GiftCardStatus credit( GiftCardInfo pGiftCardInfo ) {
		return new GiftCardStatusImpl();
	}

	private GiftCardAuthInfo getStoredAuthInfo( String paymentId ) {
		GiftCardAuthHolder giftCardAuthHolder = resolveAuthHolder();
		if(null!=giftCardAuthHolder) {
		return giftCardAuthHolder.findGiftCardAuthInfoForPaymentGroupId( paymentId );
		}
		else return null;
	}

	private void storeAuthInfo( String paymentId, GiftCardAuthInfo giftCardAuthInfo ) {
		GiftCardAuthHolder giftCardAuthHolder = resolveAuthHolder();
		if(null!=giftCardAuthHolder) {
		giftCardAuthHolder.addGiftCardAuthInfo( paymentId, giftCardAuthInfo );
		}
	}

	private void removAuthInfo( String paymentId ) {
		GiftCardAuthHolder giftCardAuthHolder = resolveAuthHolder();
		if(null!=giftCardAuthHolder) {
		giftCardAuthHolder.removeGiftCardAuthInfo( paymentId );
		}
	}

	private GiftCardAuthHolder resolveAuthHolder() {
		if( !DigitalStringUtil.isEmpty( giftCardAuthHolderPath ) ) {
			GiftCardAuthHolder giftCardAuthHolder = (GiftCardAuthHolder)ServletUtil.getCurrentRequest().resolveName( giftCardAuthHolderPath );
			if( isLoggingDebug() ) logger.debug( "resolved giftCardAuthHolder: " + giftCardAuthHolder );
			return giftCardAuthHolder;
		} else {
			if( isLoggingDebug() ) logger.debug( "giftCardAuthHolderPath is empty, so can not resolve GiftCardAuthInfo component" );
			return null;
		}
	}

	private void populateStatusFromAuthInfo( GiftCardAuthInfo giftCardAuthInfo, GiftCardStatusImpl status ) {
		status.setAmount( giftCardAuthInfo.getAmount() );

		status.setTransactionId( giftCardAuthInfo.getTransactionId() );
		status.setErrorMessage( giftCardAuthInfo.getErrorMessage() );
		status.setTransactionSuccess( giftCardAuthInfo.isTransactionSuccess() );
		status.setTransactionTimestamp( giftCardAuthInfo.getTransactionTimestamp() );
		status.setAuthorizationExpiration( giftCardAuthInfo.getAuthorizationExpiration() );
		status.setAuthorizationCode( giftCardAuthInfo.getAuthorizationCode() );
	}


	/**
	 * @return the loggingDebug
	 */
	public Boolean isLoggingDebug() {
		return this.getLoggingDebug();
	}

	/**
	 * @return the loggingError
	 */
	public Boolean isLoggingError() {
		return this.getLoggingError();
	}

}
