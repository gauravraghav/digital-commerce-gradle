package com.digital.commerce.services.common.session;

import atg.nucleus.naming.ParameterName;
import atg.rest.SessionConfirmation;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.HTTPUtils;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

/**
 * @author HB391569 DSW custom sessionConfirmation component to not only return the _dynSessConf,
 * but also send the same as httpOnly cookie. The intention is to avoid inefficient _dynSessConf
 * handling in DSW UI code.
 */
@Getter
@Setter
public class DigitalSessionConfirmationDroplet extends DynamoServlet {

  private String cookieName = "_dynSessConf";
  private static final ParameterName OUTPUT = ParameterName.getParameterName("output");

  private SessionConfirmation sessionConfirmation;
  private boolean secure;

  /*
   * (non-Javadoc)
   *
   * @see
   * atg.servlet.DynamoServlet#service(atg.servlet.DynamoHttpServletRequest,
   * atg.servlet.DynamoHttpServletResponse)
   */
  @Override
  public void service(DynamoHttpServletRequest req,
      DynamoHttpServletResponse res) throws ServletException, IOException {
    try {
      long _dynSessConf = getSessionConfirmation().getSessionConfirmationNumber();
      req.setParameter("sessionConfirmationNumber", _dynSessConf);
      Cookie cookie = HTTPUtils
          .createCookie(getCookieName(), Long.toString(_dynSessConf), isSecure(), "/");
      res.addCookie(cookie);
      req.serviceLocalParameter(OUTPUT, req, res);
    } catch (IllegalStateException ex) {
      String msg = ex.toString();
      if (DigitalStringUtil.isNotBlank(msg) &&
          msg.contains(
              "Session confirmation number can only be retrieved once per session at login time")) {
        res.sendError(430, ex.toString());
      }
      throw ex;
    } catch (Exception ex) {
      logWarning("Error while lookup of session confirmation number", ex);
      throw ex;
    }
  }
}