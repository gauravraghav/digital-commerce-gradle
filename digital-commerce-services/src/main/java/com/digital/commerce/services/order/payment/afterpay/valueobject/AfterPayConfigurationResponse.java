package com.digital.commerce.services.order.payment.afterpay.valueobject;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AfterPayConfigurationResponse implements Serializable {

    private static final long serialVersionUID = 3922163727471467056L;
    private String				minimumAmount;
    private String				maximumAmount;
    private String				currency;
    private String 				numberOfPayments;
}
