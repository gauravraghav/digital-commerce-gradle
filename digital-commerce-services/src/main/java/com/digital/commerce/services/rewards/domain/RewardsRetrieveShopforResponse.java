package com.digital.commerce.services.rewards.domain;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RewardsRetrieveShopforResponse extends ResponseWrapper {
	private List<RewardsShopforItem> items;
	private boolean isValidLoyaltyMember = true;
}
