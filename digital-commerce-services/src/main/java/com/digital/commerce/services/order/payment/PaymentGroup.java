package com.digital.commerce.services.order.payment;

import java.io.Serializable;

import com.digital.commerce.services.common.AddressTypeDetails;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/** Class is responsible for handling payment details. */
@Getter
@Setter
@ToString
public class PaymentGroup implements Serializable {
	// --------------------------------
	private static final long	serialVersionUID	= 5711682353549086124L;
	// property:Type
	private String				type;
	// property:BillingAddress
	private AddressTypeDetails	billingAddress;
	// property:CreditCardNumber
	private String				creditCardNumber;
	// property:CreditCardType
	private String				creditCardType;
	// property:GiftCertificateNumber
	private String				giftCertificateNumber;
	// property:PaymentAmount
	private double				paymentAmount;
}
