package com.digital.commerce.services.order.purchase.valueobject;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
import lombok.Getter;
import lombok.Setter;

@Generated("com.robohorse.robopojogenerator")
@Getter
@Setter
public class NewCard {

	@JsonProperty("expirationYear")
	private String expirationYear;

	@JsonProperty("billingSameAsShippingAddress")
	private boolean billingSameAsShippingAddress;

	@JsonProperty("vantivLastFour")
	private String vantivLastFour;

	@JsonProperty("vantivFirstSix")
	private String vantivFirstSix;

	@JsonProperty("expirationMonth")
	private String expirationMonth;

	@JsonProperty("creditCardType")
	private String creditCardType;

	@JsonProperty("vantivResponse")
	private String vantivResponse;

	@JsonProperty("copyCreditCardToProfile")
	private boolean copyCreditCardToProfile;

	@JsonProperty("defaultCreditCard")
	private boolean defaultCreditCard;

	@JsonProperty("nameOnCard")
	private String nameOnCard;

	@JsonProperty("vantivPaypageRegistrationId")
	private String vantivPaypageRegistrationId;

	@JsonProperty("vantivLitleTxnId")
	private String vantivLitleTxnId;
}