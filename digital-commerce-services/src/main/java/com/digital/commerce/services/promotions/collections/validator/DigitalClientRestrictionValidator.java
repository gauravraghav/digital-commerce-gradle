package com.digital.commerce.services.promotions.collections.validator;

import com.digital.commerce.services.utils.DigitalServiceConstants;

import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.repository.RepositoryItem;
import atg.service.collections.validator.CollectionObjectValidator;
import atg.servlet.ServletUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * DigitalClientRestrictionValidator validates an item based on its RestrictionType
 * properties.
 * 
 */
@Getter
@Setter
public class DigitalClientRestrictionValidator implements
		CollectionObjectValidator {
	
	/**
	 * property: RestrictionTypePropertyName
	 */
	protected String restrictionTypePropertyName;
	
	private String[] clientRestrictionTypes;
	
	private DigitalServiceConstants	dswConstants;
	/**
	 * This method validates the passed in object (repository items) based on
	 * whether or not any of its items exist in the
	 * current restriction type.
	 * 
	 * @param object
	 *            to validate
	 * @return true if the object passes validation or if no validation was
	 *         performed.
	 */
	@Override
	public boolean validateObject(Object pObject) {
		
		if (!(pObject instanceof RepositoryItem)) {
			return false;
		}

		// Perform the filtering
		RepositoryItem item = (RepositoryItem) pObject;

		String restrictionClientName = (String) item.getPropertyValue(getRestrictionTypePropertyName().trim());
		
		for(String client : getClientRestrictionTypes()){
			// If the client falls in the restriction
			if (client.trim().equalsIgnoreCase(restrictionClientName)) {
				//TODO: Validate if the source of origin is APP or CSC
				//TODO: Get order and get the origin of Order value; if the value is androidapp or iosapp; its not restricted
				Order order = ((OrderHolder)ServletUtil.getCurrentRequest().resolveName("/atg/commerce/ShoppingCart")).getCurrent();
				String originOfOrder=order.getOriginOfOrder();
				if(originOfOrder!=null && client.trim().equalsIgnoreCase("APP") && ("androidapp".equalsIgnoreCase(originOfOrder) || "iosapp".equalsIgnoreCase(originOfOrder) || getDswConstants().isPaTool())){
						return true;
				}
				return false;
			}
		}

		// The item doesn't pass validation
		return true;
	}

}
