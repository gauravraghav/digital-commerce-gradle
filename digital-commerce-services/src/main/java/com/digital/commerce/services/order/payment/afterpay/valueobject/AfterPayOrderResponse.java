package com.digital.commerce.services.order.payment.afterpay.valueobject;

import com.digital.commerce.common.domain.ResponseWrapper;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class AfterPayOrderResponse extends ResponseWrapper implements Serializable {

    private static final long serialVersionUID = -5273249813389887206L;

    private String statusCode;
    private boolean success;
    private String expirationDate;
    private String token;
    private String id;
    private Date createdDate;
}
