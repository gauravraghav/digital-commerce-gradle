package com.digital.commerce.services.order;

import lombok.Getter;

@Getter
public enum ShipNode {
	Multi("Multi"), Store("StoreHub"), FulfillmentCenter("3PLDC"), GiftCard("GiftCard"), DropShip("DropShip");

	private final String	mappedValue;

	private ShipNode( String mappedValue ) {
		this.mappedValue = mappedValue;
	}

	public String getMappedValue() {
		return mappedValue;
	}

	public static ShipNode fromMappedValue( String value ) {
		ShipNode retVal = null;
		for( ShipNode node : ShipNode.values() ) {
			if( node.getMappedValue().equalsIgnoreCase( value ) ) {
				retVal = node;
			}
		}
		return retVal;
	}
}
