/**
 * 
 */
package com.digital.commerce.services.order.payment.giftcard;

import atg.payment.PaymentStatus;

/** This class defines a gift card transaction status. It encapsulates basic transaction
 * information returned from a gift card payment system
 * 
 * @author djboyd */
public interface GiftCardStatus extends PaymentStatus {
	/** @return True = payment is approved (i.e. balance request was allowed because account number/PIN match), False if not approved */
	public abstract boolean getApprovalFlag();

	/** @return The amount actually redeemed by this card */
	public abstract double getApprovedAmount();

	/** Gift card service response codes
	 * 
	 * @return */
	public abstract String getSRC();
}
