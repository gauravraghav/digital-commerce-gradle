/**
 *
 */
package com.digital.commerce.services.order;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingAdjustment;
import atg.repository.RepositoryItem;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalMutableInt;
import com.digital.commerce.common.util.HttpServletUtil;
import com.digital.commerce.constants.OrderConstants.OrderPropertyManager;
import com.digital.commerce.services.order.payment.afterpay.AfterPayPayment;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShipType;
import com.digital.commerce.services.order.shipping.ShippingGroupConstants.ShippingGroupPropertyManager;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalOrderImpl extends OrderImpl {

	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalOrderImpl.class);

	/** added this property only for the purpose of pmdl rule editing, the same
	 * value is set at the DigitalOrderPriceInfo object. */
	private double					qualifiedOrderAmount;
	private String					lastOrderId;
	private String					lastFour;
	private String                  threatMetrixId;
	private long					lastDisplayCount;
	private boolean					isElectronicOrderOnly;
	private boolean					isGiftCardOrderOnly;
	private boolean					isPickupOrderOnly;
	private boolean					shippingAvailable;
	private boolean					copyCreditCardToProfile;
	private String 					locale;
	private boolean 				emailOptIn;
	private boolean pointsCalculated;
	private String sourceOfOrder;

	public String getSourceOfOrder() {
		if (this.sourceOfOrder ==null){
			sourceOfOrder = (String)getPropertyValue("originOfOrder");
		}
		return sourceOfOrder; 
	}
	public void setPointsCalculated(boolean pointsCalculated) {
		setPropertyValue(POINTS_CALCULATED, pointsCalculated );
	}

	private static final String[]	ELECTRIC_GIFT_CARD_TYPES		= { "eGiftCard", "reloadGiftCard" };

	private static final String[]	GIFT_CARD_TYPES					= { "eGiftCard", "reloadGiftCard", "personalizedGiftCard", "standardGiftCard" };

	private static final String		PROPERTY_REWARD_CERTIFICATES	= "rewardCertificates";

	private static final String		PROPERTY_PRODUCT_TYPE			= "type";

	private static final String		PRODUCT_TYPE_GIFTWRAP			= "giftWrap";
	private static final String 	PURCHASE_LIMIT_EXCEEDEDMSG 		= "purchaseLimitExceededMsg";
	private static final String 	CARTLINE_LIMIT_EXCEEDEDMSG 		= "cartLineLimitExceededMsg";
	
	private static final int			GIFT_CARD_SHIPPING_GROUP_STATE	= 8;
	
	private static final String 		ALTERNATE_PICKUP_FIRST_NAME = "altPickupFirstName";
	private static final String 		ALTERNATE_PICKUP_LAST_NAME = "altPickupLastName";
	private static final String 		ALTERNATE_PICKUP_EMAIL = "altPickupEmail";
	private static final String 		POINTS_CALCULATED="pointsCalculated";
	
	private List<DigitalCommerceItem> exclusionItem;

	public String getAltPickupFirstName() {
		return (String)getPropertyValue(ALTERNATE_PICKUP_FIRST_NAME);
	}

	public void setAltPickupFirstName(String alternatePickUpFirstName) {
		setPropertyValue(ALTERNATE_PICKUP_FIRST_NAME, alternatePickUpFirstName );
	}

	public String getAltPickupLastName() {
		return (String)getPropertyValue(ALTERNATE_PICKUP_LAST_NAME);
	}

	public void setAltPickupLastName(String alternatePickUpLastName) {
		setPropertyValue(ALTERNATE_PICKUP_LAST_NAME, alternatePickUpLastName );
	}

	public String getAltPickupEmail() {
		return (String)getPropertyValue(ALTERNATE_PICKUP_EMAIL);
	}
	
	private List<OrderReservationMsg> inventoryProcessResult;

	public void setAltPickupEmail(String alternatePickUpEmail) {
		setPropertyValue(ALTERNATE_PICKUP_EMAIL, alternatePickUpEmail );
	}

	public boolean isDisplayCountChanged() {
		boolean ret = true;

		if( lastDisplayCount == getTotalCommerceItemCount() && lastOrderId != null && lastOrderId.equals( this.getId() ) ) {
			ret = false;
		}

		lastDisplayCount = getTotalCommerceItemCount();
		lastOrderId = this.getId();

		return ret;
	}
	

	/**
	 * Paypal Consideration:
	 * PayPal can accept non-shipping orders for authorization.  
	 * If the cart is made up entirely of BOPIS & BOSTS items, return 1, else return 0.
	 *
	 * TODO: delete this method and use getOrderShipType 
	 * @return 
	 */
	public String isNonShippingOrder() {
		int orderShipType = this.getOrderShipType();
		if (orderShipType == 1) {
			return ShippingGroupConstants.NO_SHIPPING_YES;
		} 
		
		return ShippingGroupConstants.NO_SHIPPING_NO;
	}
	

	
	/**
	 * currently this method will return the shippinggroup who is valued the most.
	 * ShippingGroup.priceInfo.amount. 
	 * 
	 * TODO - implement isBostis flag to handle a mixed bag scenario, in a mixed 
	 * bag scenario, the highest value of the bostis shipping group should be returned
	 * and passed to the paypal call.
	 * 
	 * This method hasn't been tested yet.  
	 * 
	 * @param isBostis
	 * @return
	 */
	public ShippingGroup getHighestValueShippingGroup(boolean isBostis) {
		
		List <ShippingGroup> shippingGroupList  = getShippingGroups();
		ShippingGroup shippingGroup = null;
		
		if (shippingGroupList.size() == 1) {
			shippingGroup = shippingGroupList.get(0);
			return shippingGroup;
		}
		

		ShippingGroup []  sgList = (ShippingGroup []) shippingGroupList.toArray();
		
		ShippingGroup highestShippingGroup = null;
		
	    for (int a=1; a<sgList.length; a++) {
	        for(int b=0; b<sgList.length - a; b++) {
	            if (sgList[b].getPriceInfo().getAmount() > sgList[b+1].getPriceInfo().getAmount()) {
	            	highestShippingGroup = sgList[b];
	            	sgList[b] = sgList[b+1];
	            	sgList[b+1] = highestShippingGroup;
	            }
	        
	            
	        }

	    }
	    
	    return sgList[0];
	    
	}

	
	/**
	 * 
	 * @return 0 - Regular Shipping 
	 * 		   1 - Bopis/sts 
	 *         2 - Mixed
	 */
	public int getOrderShipType () {
		List<ShippingGroup> sgs = getShippingGroups();
		HardgoodShippingGroup hsg = null;
		boolean isRegularSg = false;
		boolean isBopisBostSg = false;
		
		for (ShippingGroup sg: sgs) {
			
			if (sg instanceof HardgoodShippingGroup) {
				hsg = (HardgoodShippingGroup) sg;
				
				String shipType = (String) hsg.getPropertyValue(ShippingGroupPropertyManager.SHIP_TYPE.getValue());
				
				if (hsg.getCommerceItemRelationshipCount() > 0) {
					if (shipType == null || shipType.equalsIgnoreCase(ShipType.SHIP.getValue())) {
						isRegularSg = true;
						
					} else {
						isBopisBostSg = true; 
					}
				}
			}
		}
		
		if (isRegularSg && isBopisBostSg) {
			return 2;
		} else if (isBopisBostSg) {
			return 1;
		} else {
			return 0;
		}
	}

	
	/** @return Returns the qualifiedOrderAmount. */
	public double getQualifiedOrderAmount() {
	
		Double rewardAmount = 0.0;
		Set<RepositoryItem> rewardCerts = this.getRewardCertificates();
		if(rewardCerts != null && !rewardCerts.isEmpty()){
			
			for(RepositoryItem rewardCert : rewardCerts){
				Double value = (Double)rewardCert.getPropertyValue( OrderPropertyManager.REWARD_CERTIFICATE_AMT_USED.getValue());
				if(value != null){
					rewardAmount+=value;
				}
			}
		}
		
		qualifiedOrderAmount = BigDecimal.valueOf(this.getPriceInfo().getAmount()).setScale(2, RoundingMode.HALF_UP ).doubleValue() + rewardAmount;
	
		if (getExclusionItem()!=null && !getExclusionItem().isEmpty()){
				for(DigitalCommerceItem filteredItem : getExclusionItem()){
					qualifiedOrderAmount = qualifiedOrderAmount - filteredItem.getPriceInfo().getAmount();
				}
			
			
		}
		return qualifiedOrderAmount;
	}


	/** @return Returns the loyaltyId. */
	public String getLoyaltyId() {
		return (String)getPropertyValue( "loyaltyId" );
	}

	/** @param loyaltyId
	 *            The loyaltyId to set. */
	public void setLoyaltyId( String loyaltyId ) {
		setPropertyValue( "loyaltyId", loyaltyId );
	}

	/** @return Returns the loyaltyTier. */
	public String getLoyaltyTier() {
		return (String)getPropertyValue( "loyaltyTier" );
	}

	/** @param loyaltyTier
	 *            The loyaltyTier to set. */
	public void setLoyaltyTier( String loyaltyTier ) {
		setPropertyValue( "loyaltyTier", loyaltyTier );
	}

	/** @param mktgChannel the mktgChannel to set */
	public void setMktgChannel( String mktgChannel ) {
		setPropertyValue( "mktgChannel", mktgChannel );
	}

	/** @return the mktgChannel */
	public String getMktgChannel() {
		return (String)getPropertyValue( "mktgChannel" );
	}

	/** @param mktgChannelID the mktgChannelID to set */
	public void setMktgChannelID( String mktgChannelID ) {
		setPropertyValue( "mktgChannelID", mktgChannelID );
	}

	/** @return the mktgChannelID */
	public String getMktgChannelID() {
		return (String)getPropertyValue( "mktgChannelID" );
	}

	/** @param csrRepID the csrRepID to set */
	public void setCsrRepID( String CsrRepID ) {
		setPropertyValue( "CsrRepID", CsrRepID );
	}

	/** @return the csrRepID */
	public String getCsrRepID() {
		return (String)getPropertyValue( "CsrRepID" );
	}

	/**
	 *
	 */
	private static final long	serialVersionUID	= 3811259803622852426L;

	/** return true if order contains only card commerce items
	 * 
	 * @return */
	public boolean getIsGiftCardOrderOnly() {
		boolean result = true;
		List<CommerceItem> commerceItems = getCommerceItems();
		for( int x = 0; x < commerceItems.size(); x++ ) {
			CommerceItem ci = commerceItems.get( x );
			RepositoryItem product = (RepositoryItem)ci.getAuxiliaryData().getProductRef();

			if( product != null ) {
				String type = (String)product.getPropertyValue( PROPERTY_PRODUCT_TYPE );
				if( !isGiftCardType( type ) ) {
					result = false;
					break;
				}

			}
		}
		isGiftCardOrderOnly = result;

		return isGiftCardOrderOnly;
	}

	/** return true if order contains only electronic items - eCard, and
	 * reloadCard
	 * 
	 * @return */
	public boolean getIsElectronicOrderOnly() {
		boolean result = true;
		List<CommerceItem> commerceItems = getCommerceItems();
		for( int x = 0; x < commerceItems.size(); x++ ) {
			CommerceItem ci = commerceItems.get( x );
			RepositoryItem product = (RepositoryItem)ci.getAuxiliaryData().getProductRef();

			if( product != null && !isElectronicType( (String)product.getPropertyValue( PROPERTY_PRODUCT_TYPE ) ) ) {
				result = false;
				break;
			}
		}
		isElectronicOrderOnly = result;
		return isElectronicOrderOnly;
	}

	/** return true if the order's hardgood shipping groups are only pick up in store types.
	 * (There are no items being delivered in hardgood shipping groups, so we are ignoring gift cards)
	 * 
	 * @return */
	public boolean getIsPickupOrderOnly() {
		boolean result = true;
	
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for(ShippingGroup sg : shippingGroups){
			// if the sg is a hardgoodShippingGroup and has at least 1 commerce item, check if it is of type "ship"/null or bopis or bosts
			if(sg != null && sg instanceof HardgoodShippingGroup && sg.getCommerceItemRelationshipCount() > 0){
				HardgoodShippingGroup HGshippingGroup = (HardgoodShippingGroup)sg;
				String hgShipType = (String)HGshippingGroup.getPropertyValue("shipType");
				if(hgShipType == null || "".equals(hgShipType) || "ship".equalsIgnoreCase(hgShipType)){
					result = false;
					break;
				}
			}
		}

		isPickupOrderOnly = result;

		return isPickupOrderOnly;
	}
	
	/** 
	 * 
	 * @return */
	public boolean isNonGCShipToHomeOrder() {
		boolean result = false;
		
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for(ShippingGroup sg : shippingGroups){
			// if the sg is a hardgoodShippingGroup and has at least 1 commerce item, check if it is of type "ship"/null or bopis or bosts
			if(sg != null && sg instanceof HardgoodShippingGroup && sg.getCommerceItemRelationshipCount() > 0){
				HardgoodShippingGroup HGshippingGroup = (HardgoodShippingGroup)sg;
				int state = sg.getState();
				String hgShipType = (String)HGshippingGroup.getPropertyValue("shipType");
				if((hgShipType == null || "".equals(hgShipType) || "ship".equalsIgnoreCase(hgShipType)) 
						&& GIFT_CARD_SHIPPING_GROUP_STATE != state){
					result = true;
					break;
				}
			}
		}

		return result;
	}
	
	private static boolean isElectronicType( String type ) {
		boolean result = false;
		if( type == null ) { return result; }
		for( int i = 0; i < ELECTRIC_GIFT_CARD_TYPES.length; i++ ) {
			if( ELECTRIC_GIFT_CARD_TYPES[i].equals( type ) ) {
				result = true;
				break;
			}
		}
		return result;
	}

	private static boolean isGiftCardType( String type ) {
		boolean result = false;
		if( type == null ) { return result; }
		for( int i = 0; i < GIFT_CARD_TYPES.length; i++ ) {
			if( GIFT_CARD_TYPES[i].equals( type ) ) {
				result = true;
				break;
			}
		}
		return result;
	}

	/** return the total amount of commerce items, which is modifable and
	 * shippable by hardgood shipping group, it is used on address.jsp page to
	 * show/hide "add address option". */
	public long getTotalShippableCommerceItemCount() {
		ensureContainers();
		long result = 0;
		List<CommerceItem> commerceItems = getCommerceItems();
		for( int x = 0; x < commerceItems.size(); x++ ) {
			CommerceItem ci = commerceItems.get( x );
			RepositoryItem product = (RepositoryItem)ci.getAuxiliaryData().getProductRef();
			if( product != null ) {
				String productType = (String)product.getPropertyValue( PROPERTY_PRODUCT_TYPE );
				// if item is not card, and not giftwrappable
				if( !isGiftCardType( productType ) && !PRODUCT_TYPE_GIFTWRAP.equals( productType ) ) {
					result = result + ci.getQuantity();
				}
			}
		}
		return result;

	}

	/**
	 * override this method, so the count does not take in the giftwrap item count
	 *
	 * @return long
	 */
	public long getTotalCommerceItemCount() {
		long result = 0;
		try {
			ensureContainers();
			List<CommerceItem> commerceItems = getCommerceItems();
			if (commerceItems.toString().contains("removed")) {
				this.invalidateOrder();
				commerceItems = getCommerceItems();
			}
			for (int x = 0; x < commerceItems.size(); x++) {
				CommerceItem ci = commerceItems.get(x);
				RepositoryItem product = (RepositoryItem) ci.getAuxiliaryData().getProductRef();
				if (product != null) {
					String productType = (String) product.getPropertyValue(PROPERTY_PRODUCT_TYPE);
					if (productType != null && !productType.equals(PRODUCT_TYPE_GIFTWRAP)) {
						result = result + ci.getQuantity();
					}
				}
			}
		}catch (Exception e){
			logger.error("Exception occured when calculating item count ", e);
		}
		return result;
	}

	public long getVisibleItemCount() {
		long totalItems = 0;
		List<CommerceItem> commerceItems = getCommerceItems();
		if( commerceItems.toString().contains( "removed" ) ) {
			this.invalidateOrder();
			commerceItems = getCommerceItems();
		}
		for( int x = 0; x < commerceItems.size(); x++ ) {
			CommerceItem ci = commerceItems.get( x );
			if( !ci.toString().contains( "removed" ) ) {
				RepositoryItem product = (RepositoryItem)ci.getAuxiliaryData().getProductRef();
				if( product != null && !Boolean.FALSE.equals( product.getPropertyValue( "isVisible" ) ) ) {
					totalItems += ci.getQuantity();
				}
			} else {
				this.invalidateOrder();
			}
		}
		return totalItems;
	}

	public Set<RepositoryItem> getRewardCertificates() {
		return (Set<RepositoryItem>)getPropertyValue( PROPERTY_REWARD_CERTIFICATES );
	}

	public void setRewardCertificates( Set<RepositoryItem> certs ) {
		setPropertyValue( PROPERTY_REWARD_CERTIFICATES, certs );
	}

	public List getOrderRewards() {
		return (List)getPropertyValue( "orderRewards" );
	}

	public void setOrderRewards( List<?> rewards ) {
		setPropertyValue( "orderRewards", rewards );
	}

	//
	// BEGIN: CS13/CS14 helper methods
	//

	public double getTotalShippingCost() {
		double total = 0;
		Iterator<ShippingGroup> shippingGrouperator = this.getShippingGroups().iterator();
		while( shippingGrouperator.hasNext() ) {
			ShippingGroup sGroup = shippingGrouperator.next();
			if( sGroup.getPriceInfo() != null && ( sGroup instanceof HardgoodShippingGroup || sGroup.getPriceInfo().getAmount() != 0.0 ) ) {
				total += sGroup.getPriceInfo().getAmount();
			}
		}

		return total;
	}

	public double getTotalGCShippingCost() {
		double total = 0;

		Iterator shippingGrouperator = this.getShippingGroups().iterator();
		while( shippingGrouperator.hasNext() ) {
			ShippingGroup sGroup = (ShippingGroup)shippingGrouperator.next();
			if( sGroup.getPriceInfo() != null && ( sGroup instanceof HardgoodShippingGroup || sGroup.getPriceInfo().getAmount() != 0.0 ) ) {
				if( sGroup.getState() == GIFT_CARD_SHIPPING_GROUP_STATE ){
					total += sGroup.getPriceInfo().getAmount();
				}
			}
		}

		return total;
	}

	public double getTotalNonGCShippingCost() {
		double total = 0;
		Iterator shippingGrouperator = this.getShippingGroups().iterator();
		while( shippingGrouperator.hasNext() ) {
			ShippingGroup sGroup = (ShippingGroup)shippingGrouperator.next();
			if( sGroup.getPriceInfo() != null && ( sGroup instanceof HardgoodShippingGroup || sGroup.getPriceInfo().getAmount() != 0.0 ) ) {
				if( sGroup.getState() != GIFT_CARD_SHIPPING_GROUP_STATE ) {
					total += sGroup.getPriceInfo().getAmount();
				}
			}
		}

		return total;
	}


	public CreditCard getFirstCreditCardPaymentGroup() {
		Iterator<PaymentGroup> paymentGrouperator = this.getPaymentGroups().iterator();
		while( paymentGrouperator.hasNext() ) {
			PaymentGroup pGroup = paymentGrouperator.next();
			if( pGroup instanceof CreditCard ) { return (CreditCard)pGroup; }
		}
		return null;
	}

	public DigitalGiftCard getFirstGiftCardPaymentGroup() {
		Iterator<PaymentGroup> paymentGrouperator = this.getPaymentGroups().iterator();
		while( paymentGrouperator.hasNext() ) {
			PaymentGroup pGroup = paymentGrouperator.next();
			if( pGroup instanceof DigitalGiftCard ) { return (DigitalGiftCard)pGroup; }
		}
		return null;
	}

	public PaypalPayment getFirstPayPalPaymentGroup() {
		Iterator<PaymentGroup> paymentGrouperator = this.getPaymentGroups().iterator();
		while( paymentGrouperator.hasNext() ) {
			PaymentGroup pGroup = paymentGrouperator.next();
			if( pGroup instanceof PaypalPayment ) { return (PaypalPayment)pGroup; }
		}
		return null;
	}

	public AfterPayPayment getFirstAfterPayPaymentGroup() {
		Iterator<PaymentGroup> paymentGrouperator = this.getPaymentGroups().iterator();
		while( paymentGrouperator.hasNext() ) {
			PaymentGroup pGroup = paymentGrouperator.next();
			if( pGroup instanceof AfterPayPayment ) { return (AfterPayPayment)pGroup; }
		}
		return null;
	}

	public List<DigitalGiftCard> getGiftCardPaymentGroups() {
		final List<DigitalGiftCard> retVal = new ArrayList<>();
		if( getPaymentGroups() != null && !getPaymentGroups().isEmpty() ) {
			for( Object object : getPaymentGroups() ) {
				final PaymentGroup paymentGroup = (PaymentGroup)object;
				if( paymentGroup instanceof DigitalGiftCard ) {
					retVal.add( (DigitalGiftCard)paymentGroup );
				}
			}
		}
		return retVal;
	}

	public double getCreditCardAmount() {
		CreditCard ccPayGroup = getFirstCreditCardPaymentGroup();
		if( ccPayGroup != null ) { return ccPayGroup.getAmount(); }
		return 0;
	}

	public double getGiftCardAmount() {
		
		DigitalGiftCard gcPayGroup = getFirstGiftCardPaymentGroup();
		if( gcPayGroup != null ) { return gcPayGroup.getAmount(); }
		
		return 0;
	}

	public double getPayPalAmount() {
		PaypalPayment ppPayGroup = getFirstPayPalPaymentGroup();
		if( ppPayGroup != null ) { return ppPayGroup.getAmount(); }
		return 0;
	}

	public double getAfterPayAmount() {
		AfterPayPayment apPayGroup = getFirstAfterPayPaymentGroup();
		if( apPayGroup != null ) { return apPayGroup.getAmount(); }
		return 0.0d;
	}

	public List<String> listItemPromoDescriptions( CommerceItem ci ) {
		List<String> iPromoDescriptions = new ArrayList<>();
		if( ci.getPriceInfo() != null ) {
			Iterator<PricingAdjustment> itemAdjusterator = ci.getPriceInfo().getAdjustments().iterator();
			while( itemAdjusterator.hasNext() ) {
				PricingAdjustment iAdjust = itemAdjusterator.next();
				if( iAdjust != null ) {
					RepositoryItem iPromo = iAdjust.getPricingModel();
					if( iPromo != null ) {
						iPromoDescriptions.add( (String)iPromo.getPropertyValue( "description" ) );
					}
				}
			}
		}
		return iPromoDescriptions;
	}
	
	public Map<String,String> listItemPromoDescriptionsMap( CommerceItem ci ) {
		Map<String,String> iPromoDescriptions = new HashMap<>();
		if( ci.getPriceInfo() != null ) {
			Iterator<PricingAdjustment> itemAdjusterator = ci.getPriceInfo().getAdjustments().iterator();
			while( itemAdjusterator.hasNext() ) {
				PricingAdjustment iAdjust = itemAdjusterator.next();
				if( iAdjust != null ) {
					RepositoryItem iPromo = iAdjust.getPricingModel();
					if( iPromo != null ) {
						iPromoDescriptions.put( (String)iPromo.getPropertyValue( "id" ),(String)iPromo.getPropertyValue( "description" ) );
					}
				}
			}
		}
		return iPromoDescriptions;
	}

	public List<DigitalOrderPromoVars> getOrderPromoNames() {
		List<DigitalOrderPromoVars> oPromoNames = new ArrayList<>();
		DigitalOrderPromoVars DSWPromo = new DigitalOrderPromoVars();
		if( this.getPriceInfo() != null ) {
			Iterator<PricingAdjustment> orderAdjusterator = this.getPriceInfo().getAdjustments().iterator();
			while( orderAdjusterator.hasNext() ) {
				PricingAdjustment oAdjust = orderAdjusterator.next();
				if( oAdjust != null ) {
					RepositoryItem oPromo = oAdjust.getPricingModel();
					if( oPromo != null ) {
						DSWPromo = new DigitalOrderPromoVars();
						DSWPromo.setPromoAmount( new Double( oAdjust.getTotalAdjustment() ) );
						DSWPromo.setPromoDescription( (String)oPromo.getPropertyValue( "description" ) );
						DSWPromo.setPromoName( (String)oPromo.getPropertyValue( "displayName" ) );
						oPromoNames.add( DSWPromo );
					}
				}
			}
		}
		return oPromoNames;
	}

	//
	// END: CS13/CS14 helper methods
	//

	public boolean isTaxOffline() {
		return getPropertyValue( "taxOffline" ) != null ? (Boolean)getPropertyValue( "taxOffline" ) : false;
	}

	public void setTaxOffline( boolean taxOffline ) {
		setPropertyValue( "taxOffline", taxOffline );
	}

	public boolean isOrderContainsLuxuryItem() {
		return !filterLineItemsBySkuFlag( "isLuxury", Boolean.TRUE ).isEmpty();
	}

	public boolean isOrderContainsWhiteGloveItem() {
		return !filterLineItemsBySkuFlag( "isWhiteGloveEligible", Boolean.TRUE ).isEmpty();
	}

	public boolean isOrderContainsSignatureRequiredItem() {
		return !filterLineItemsBySkuFlag( "isSignatureRequired", Boolean.TRUE ).isEmpty();
	}

	public boolean isOrderContainsUnreturnableToStoreItem() {
		return !filterLineItemsBySkuFlag( "isReturnableToStore", Boolean.FALSE ).isEmpty();
	}

	public List<CommerceItem> filterLineItemsBySkuFlag( String propertyName, Boolean compareValue ) {
		List<CommerceItem> commerceItems = this.getCommerceItems();
		List<CommerceItem> filteredCommerceItems = new ArrayList<>();

		if( commerceItems != null ) {
			for( CommerceItem item : commerceItems ) {
				if( item != null ) {
					if( item.getAuxiliaryData() != null ) {
						RepositoryItem sku = (RepositoryItem)item.getAuxiliaryData().getCatalogRef();
						if( compareValue.equals( sku.getPropertyValue( propertyName ) ) ) {
							filteredCommerceItems.add( item );
						}
					}
				}
			}
		}

		return filteredCommerceItems;
	}

	public BigDecimal getOrderTotal() {
		BigDecimal retVal = BigDecimal.ZERO;
		if( this.getPriceInfo() != null ) {
			retVal = BigDecimal.valueOf(this.getPriceInfo().getAmount()).setScale( 2, RoundingMode.HALF_UP ).add( BigDecimal.valueOf(this.getPriceInfo().getShipping() ).setScale( 2, RoundingMode.HALF_UP ) )
					.add( BigDecimal.valueOf(this.getPriceInfo().getTax() ).setScale( 2, RoundingMode.HALF_UP ) );
		}
		return retVal;
	}

	/**
	 * Returns whether or not this order will be shipped from multiple fulfillment centers.
	 *
	 * @return
	 */
	public boolean isOrderContainsMultipleShipNodes() {
		boolean ret = false;
		Set<String> shipNodes = Sets.newHashSet();
		List<DigitalCommerceItem> commerceItems = this.getCommerceItems();
		for( DigitalCommerceItem ci : commerceItems ) {
			String shipType = ci != null ? ci.getShipType() : null;
			if(ci!=null){
				RepositoryItem product = (RepositoryItem)ci.getAuxiliaryData().getProductRef();
				if( product != null && !isBOPISorBOSTS( shipType ) && !isElectronicType( (String)product.getPropertyValue( PROPERTY_PRODUCT_TYPE ) ) ) {
					if( ci instanceof GiftCardCommerceItem ) {
						shipNodes.add( ShipNode.GiftCard.getMappedValue() );
					} else {
						shipNodes.add( ci.getShipNode() );
					}
				}
			}
		}
		if( shipNodes.size() > 1 || shipNodes.contains( ShipNode.Multi.getMappedValue() ) ) {
			ret = true;
		}

		return ret;
	}

	protected boolean isBOPISorBOSTS( String shipType ) {
		return shipType != null && ( shipType.matches( "BOPIS" ) || shipType.matches( "BOSTS" ) );
	}

	/** Returns whether or not this order will be shipped from multiple fulfillment centers.
	 * 
	 * @return */
	public boolean isStoreOnlyShipping() {
		boolean ret = true;
		List<DigitalCommerceItem> commerceItems = this.getCommerceItems();
		for( DigitalCommerceItem ci : commerceItems ) {
			if( !( ci instanceof GiftCardCommerceItem ) ) {
				if( ci.getStoreHubQty() == null || ci.getStoreHubQty() <= 0 ) {
					ret = false;
				}
			}
		}
		return ret;
	}

	public boolean isWhiteGloveOrder() {
		return (Boolean)getPropertyValue( "whiteGloveOrder" );
	}

	public void setWhiteGloveOrder( boolean whiteGloveOrder ) {
		setPropertyValue( "whiteGloveOrder", whiteGloveOrder );
	}

	public boolean isMultipleShipmentOrder() {
		return (Boolean)getPropertyValue( "multipleShipmentOrder" );
	}

	public void setMultipleShipmentOrder( boolean multipleShipmentOrder ) {
		setPropertyValue( "multipleShipmentOrder", multipleShipmentOrder );
	}
	
	public List<ShippingGroup> getNonBopisShippingGroups(){
		List<ShippingGroup> shippingGroups = getShippingGroups();
		List<ShippingGroup> returnShippingGroups = new ArrayList<>();
		for(ShippingGroup sg : shippingGroups){
			if(sg instanceof HardgoodShippingGroup){
				String shipType = (String)((HardgoodShippingGroup)sg).getPropertyValue("shipType");
				if(null == shipType || "ship".equals(shipType)){
					returnShippingGroups.add(sg);
				}			
			}else{
				returnShippingGroups.add(sg);
			}
		}
		return returnShippingGroups;
	}
	
	/**
	 * Returns true if the order has at least one item for pickup at a store
	 * @return
	 */
	public boolean isPickupItems(){
		boolean retVal = false;
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for(ShippingGroup sg : shippingGroups){
			if(sg instanceof HardgoodShippingGroup){
				String shipType = (String)((HardgoodShippingGroup)sg).getPropertyValue("shipType");
				if("BOPIS".equalsIgnoreCase(shipType) || "BOSTS".equalsIgnoreCase(shipType)){
					if(sg.getCommerceItemRelationshipCount() > 0) {
						return true;
					}
				}			
			}
		}
		return retVal;
	}
	
	/**
	 * Returns true if the order has at least one item for pickup at a store
	 * @return
	 */
	public boolean isPickupOnlyOrder(){
		boolean retVal = true;
		List<ShippingGroup> shippingGroups = getShippingGroups();
		for(ShippingGroup sg : shippingGroups){
			if(sg instanceof HardgoodShippingGroup){
				String shipType = (String)((HardgoodShippingGroup)sg).getPropertyValue("shipType");
				if(null == shipType || "ship".equals(shipType)){
					if(sg.getCommerceItemRelationshipCount() > 0) {
						return false;
					}
				}			
			}
		}
		return retVal;
	}
	/**
	 * Counts the number of BOPIS / BOSTS / Ship to Home items in the order for
	 * logging.
	 * 
	 * @return String
	 */
	public String getShipTypeCounts() {
		ShipTypeCounter shipTypeCounter = new ShipTypeCounter();
		for (ShippingGroup sg : (List<ShippingGroup>) getShippingGroups()) {
			if (sg instanceof HardgoodShippingGroup) {
				String shipType = (String) ((HardgoodShippingGroup) sg)
						.getPropertyValue("shipType");
				if (DigitalServiceConstants.SHIP_TYPE_BOPIS.equalsIgnoreCase(shipType)) {
					shipTypeCounter.increment(DigitalServiceConstants.SHIP_TYPE_BOPIS);
				} else if (DigitalServiceConstants.SHIP_TYPE_BOSTS
						.equalsIgnoreCase(shipType)) {
					shipTypeCounter.increment(DigitalServiceConstants.SHIP_TYPE_BOSTS);
				} else {
					shipTypeCounter.increment(DigitalServiceConstants.SHIP_TYPE_SHIP);
				}
			}
		}
		return shipTypeCounter.toString();
	}

	private class ShipTypeCounter {

		HashMap<String, DigitalMutableInt> data;

		public ShipTypeCounter() {
			data = new HashMap<>();
			data.put(DigitalServiceConstants.SHIP_TYPE_BOPIS, new DigitalMutableInt());
			data.put(DigitalServiceConstants.SHIP_TYPE_BOSTS, new DigitalMutableInt());
			data.put(DigitalServiceConstants.SHIP_TYPE_SHIP, new DigitalMutableInt());
		}

		public void increment(String key) {
			data.get(key).increment();
		}

		public String toString() {
			String outVal = "";
			for (Map.Entry<String, DigitalMutableInt> entry : data.entrySet()) {
				outVal += (entry.getKey() + " = " + entry.getValue() + ", ");
			}
			
			if (outVal.length() > 1) {
				outVal = outVal.substring(0, outVal.length() - 2);
			}
			
			return outVal;
		}
	}

	public void setLocale(){
		
		String localeSelection = HttpServletUtil.getLocaleInfo();
		if(null != localeSelection){
			this.locale = localeSelection;
			setPropertyValue( OrderPropertyManager.LOCALE.getValue(), this.locale );
		}
	}
	
	public String getLocale(){
		if(null == this.locale){
			this.setLocale();
		}
		return this.locale;
	}
	
	/** @return Returns the loyaltyId. */
	public boolean isDropPrevCartFlag() {
		return (getPropertyValue("dropPrevCartFlag") == null ? false : (Boolean)getPropertyValue( "dropPrevCartFlag" ));
	}

	/** @param loyaltyId
	 *            The loyaltyId to set. */
	public void setDropPrevCartFlag( boolean dropPrevCartFlag ) {
		setPropertyValue( "dropPrevCartFlag", dropPrevCartFlag );
	}

	public void setPurchaseLimitExceededMsg(String message) {
		setPropertyValue( PURCHASE_LIMIT_EXCEEDEDMSG, message );
	}
	
	public void setCartLineLimitExceededMsg(String message) {
		setPropertyValue( CARTLINE_LIMIT_EXCEEDEDMSG, message );
	}
	
	public String getPurchaseLimitExceededMsg() {
		return (String)getPropertyValue( PURCHASE_LIMIT_EXCEEDEDMSG);
	}
	
	public String getCartLineLimitExceededMsg() {
		return (String)getPropertyValue( CARTLINE_LIMIT_EXCEEDEDMSG );
	}
	
}
