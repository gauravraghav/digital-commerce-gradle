package com.digital.commerce.constants;

import lombok.Getter;

public class RewardsCertificateConstants {

	@Getter
	public enum ProfileRewardCertificateStatus{
		INITIAL("INITIAL"),
		APPLIED("APPLIED");
		
		private final String value;
		
		private ProfileRewardCertificateStatus( String value ) {
			this.value = value;
		}
	}

	@Getter
	public enum OrderRewardCertificateStatus{
		INITIAL("INITIAL"),
		VALIDATED("VALIDATED"),
		RESERVED("RESERVED"),
		DONATED("DONATED");
		
		private final String value;
		
		private OrderRewardCertificateStatus( String value ) {
			this.value = value;
		}

	}

	@Getter
	public enum WebOfferStatus{
		INITIAL("INITIAL"),
		APPLIED("APPLIED");
		
		private final String value;
		
		private WebOfferStatus( String value ) {
			this.value = value;
		}

	}
	public static final String	INVALID_CERTIFICATE =		"INVALID_CERTIFICATE";
}
