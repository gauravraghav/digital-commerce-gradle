package com.digital.commerce.constants;

import lombok.Getter;

/** Property constants for Paypal
 */

public final class PaypalConstants {
	@Getter
	public enum PaypalPaymentGroupPropertyManager{
		PAYPAL_PAYMENT("paypalPayment"),
		PAYPAL_PAYER_ID("paypalPayerId"),
		PAYPAL_EMAIL("paypalEmail"),
		CARDINAL_ORDER_ID("cardinalOrderId");
		
		private final String value;
		
		private PaypalPaymentGroupPropertyManager( String value ) {
			this.value = value;
		}
	}
	
	
	public static final String	PAYPAL_PAYMENT_TYPE	= "paypalPayment";
	
	// Lookup Call
	public static final String	PAYPAL_LOOKUP_MESSAGE_TYPE			= "cmpi_lookup";
	public static final String	PAYPAL_USD_CURRENCY_CODE			= "840";
	public static final String	PAYPAL_TRANSACTION_TYPE				= "PP";
	public static final String	PAYPAL_TRANSACTION_MOBILE_MODE		= "P";
	public static final String	PAYPAL_TRANSACTION_FULL_SITE_MODE	= "S";
	public static final String	FORCE_ADDRESS_YES					= "Y";
	public static final String	FORCE_ADDRESS_NO					= "N";
	public static final String	OVERRIDE_ADDRESS_YES 				= "Y";
	public static final String	OVERRIDE_ADDRESS_NO					= "N";
	public static final String	PAYPAL_PAYMENT_AVAILABLE			= "Y";
	public static final String	PAYPAL_PAYMENT_UNAVAILABLE			= "U";

	// Authentication Call
	public static final String	PAYPAL_AUTHENTICATE_MESSAGE_TYPE	= "cmpi_authenticate";
	public static final String	AUTHENTICATE_SUCCESS_STATUS			= "SUCCESS";//"Y";
	public static final String	AUTHENTICATE_CANCEL_STATUS			= "CANCELED";//"X";
	public static final String	AUTHENTICATE_ERROR_STATUS			= "ERROR";//"E";
	public static final String	AUTHENTICATE_UNAVAILABLE_STATUS		= "UNAVAILABLE";//"U";
	public static final String	AUTHENTICATE_ADDR_ERROR_STATUS		= "INVALID_SHIPPING_ADDRESS";
	public static final String	AUTHENTICATE_INT_ADDR_ERROR_STATUS	= "INTERNATION_SHIPPING_ADDRESS_ERROR";
	
	public static final String	INTERNATIONAL_ADDR_IND 				= "TSTINTL";
	
	// Authorization Call
	public static final String	PAYPAL_AUTHORIZE_MESSAGE_TYPE		= "cmpi_authorize";
	public static final String	AUTHORIZE_APPROVED_STATUS			= "Y";
	public static final String	AUTHORIZE_PENDING_STATUS			= "P";
	public static final String	AUTHORIZE_ERROR_STATUS				= "E";
	public static final String	AUTHORIZE_UNAVAILABLE_STATUS		= "U";
	
	//Order Call
	public static final String	PAYPAL_ORDER_MESSAGE_TYPE			= "cmpi_order";
	
	//PayPal Region Constants
	public static final String	PAYPAL_REGION_AE					= "AE";
	public static final String	PAYPAL_REGION_AP					= "AP";
	public static final String	PAYPAL_REGION_AA					= "AA";
	
	//PayPal error number
	
	public static final String VENDOR_PAYMENT_PROCESSOR_ERR_CODE	= "1018";
	
	public static final String PAYPAL_CHARGED_ZERO 					= "10525";

	public static final String SELECTED_PAYMENT_PAYPAL				= "PayPal";
	
	public static final String 		ERROR_MSG_UNAVAILABLE			= "Unavailable";
	public static final String 		ERROR_MSG_PENDING				= "Pending";
	public static final String 		ERROR_MSG_ERROR					= "Error";
}
