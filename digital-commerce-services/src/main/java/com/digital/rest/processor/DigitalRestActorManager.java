package com.digital.rest.processor;

import atg.dtm.UserTransactionDemarcation;
import atg.rest.RestActorManager;
import atg.rest.RestException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.digital.commerce.common.transaction.TransactionUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalRestActorManager extends RestActorManager{
    private static final String CLASSNAME = DigitalRestActorManager.class.getName();
    boolean enabled;

    @Override
    public void processActor(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws RestException {
        if(!this.isEnabled()){
            super.processActor(pRequest, pResponse);
            return;
        }

        final String METHOD_NAME="processActor";

        UserTransactionDemarcation td = null;
        try {
            td = TransactionUtils.startNewTransaction(this.getName(), METHOD_NAME);
            if (this.isLoggingDebug()){
                logDebug("Transaction Started in RestActormanager");
            }
            super.processActor(pRequest, pResponse);
        }catch(Throwable ex){
            TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
            if(this.isLoggingError()){
                logError(ex);
            }
        }finally{
            TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
            if (this.isLoggingDebug()) {
                logDebug("Transaction Ended in RestActormanager");
            }
        }

    }
}
