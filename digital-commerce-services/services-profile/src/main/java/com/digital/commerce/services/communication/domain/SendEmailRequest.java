package com.digital.commerce.services.communication.domain;

import java.util.List;
import java.util.Map;

import com.digital.commerce.integration.common.communication.domain.Recipient;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent.ContentType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SendEmailRequest {
	private String sender;
	private List<Recipient> recipients; 
	private String subject;
	private List<String> subjectDataElements;
	private String templateKey;
	private Map<String, String> dataElements;
	private ContentType contentType;

}
