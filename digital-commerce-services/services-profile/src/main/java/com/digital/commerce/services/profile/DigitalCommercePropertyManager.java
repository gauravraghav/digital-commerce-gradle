
package com.digital.commerce.services.profile;

import java.util.Locale;

import atg.commerce.profile.CommerceProfileUserMessage;
import atg.commerce.profile.CommercePropertyManager;
import lombok.Getter;
import lombok.Setter;

/** DSW-specific extenstions with names of extended profile properties
 **/
@Getter
@Setter
public class DigitalCommercePropertyManager extends CommercePropertyManager {

	private String	middleNamePropertyName					= "middleName";

	private String	confirmEmailPropertyName				= "confirmEmail";

	private String	genderPropertyName						= "gender";

	private String	birthDatePropertyName					= "dateOfBirth";

	// Loyalty number property
	private String	loyaltyNumberPropertyName				= "loyaltyNumber";

	// Loyalty Certificate Item Descriptor Name
	private String	loyaltyCertificateItemDescriptorName	= "loyaltyCertificate";

	// Loyalty Certificate Item Descriptor Name
	private String	loyaltyTierItemDescriptorName			= "loyaltyTier";

	// Certificate Property Name
	private String	certificatePropertyName				= "certificates";

	// Business Property Name
	private String	businessNamePropertyName				= "businessName";

	// New User property name
	private String	newUserFlagPropertyName				= "newUserFlag";

	private String	wishListPorpertyName					= "wishlist";

	//Profile reward cert associated properties
	private String	certificateIdPropertyName				= "certificateId";
	private String	certificateNumberPropertyName			= "certificateNumber";
	private String	markdownCodePropertyName				= "markdownCode";
	private String	certificateStatusPropertyName			= "status";
	private String	certificateValuePropertyName			= "certificateValue";
	private String  expirationDatePropertyName				= "expirationDate";
	private String  issueDatePropertyName					= "issueDate";
	private String	certificateTypePropertyName				= "type";
	private String  user 									= "user";
	private String  promotionDescPropertyName				= "description";
	private String  promotionPropertyName					= "promotion";
	private String  promotionsPropertyName					= "promotions";

	private String  homeAddressPropertyName                = "homeAddress";

	private String  promotionShopNowLinkPropertyName		= "shopNowLink";
	private String  promotionUserFriendlyDescPropertyName	= "userFriendlyDescription";
	private String  promotionDiscountTypePropertyName		= "discountIconType";
	private String  promotionDiscountIconTypePropertyName	= "discountIconType";
	private String  promotionOfferDetailsPageNamePropertyName	= "offerDetailsPageName";
	private String  promotionUserFriendlyNamePropertyName	= "userFriendlyName";
	private String  promotionBeginUsablePropertyName		= "beginUsable";
	private String  promotionEndUsablePropertyName			= "endUsable";
	private String  promotionStartDatePropertyName			= "startDate";
	private String  promotionExpirationDatePropertyName	= "expirationDate";
	private String  promotionHasPromotionsPropertyName		= "hasPromotions";


	private String  offerDisplayNamePropertyName			= "offerDisplayName";
	private String  offerDetailsPageNamePropertyName		= "OfferDetailsPageName";
	private String	mobilePhoneNumberPorpertyName			= "mobilePhoneNumber";
	private String	homePhoneNumberPorpertyName			= "phoneNumber";
	private String  promotionTypePropertyName				= "type";

    private String rewardsTotalCertificateValuePropertyName = "rewardsTotalCertificateValue";
    private String dollarsNextTierPropertyName = "dollarsNextTier";
    private String dollarsNextRewardPropertyName = "dollarsNextReward";
    private String loyaltyTierExpirationDatePropertyName = "loyaltyTierExpirationDate";
    private String pointsNextRewardPropertyName = "pointsNextReward";
	private String dollarsFutureRewardPropertyName = "dollarsFutureReward";
	private String birthMonthPropertyName;
	private String birthYearPropertyName;

	public String getDefaultHomeAddrName( Locale pLocale ) {
		return CommerceProfileUserMessage.format( "defaultHomeAddress", pLocale );
	}

	// Jitesh - added this property
	public String getDefaultHomeAddrPropertyName() {
		return "homeAddress";
	}

	// Jitesh - added this property
	public String getAddressTypePropertyName() {
		return "addressType";
	}

	public String getOperationalEmailAddressPropertyName() {
		return "operationalEmailAddress";
	}

	public String getOperationalFirstNamePropertyName() {
		return "operationalEmailFirstName";
	}
}
