package com.digital.commerce.services.profile.handler;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;

import atg.beans.PropertyNotFoundException;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.dtm.UserTransactionDemarcation;
import atg.multisite.SiteContextManager;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.ForgotPasswordHandler;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.PropertyManager;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.csc.ContactCenterUser;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalAESEncryptionUtil;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.communication.CommunicationService;
import com.digital.commerce.integration.common.communication.domain.Recipient;
import com.digital.commerce.integration.common.communication.domain.smtp.DigitalEmailEvent.ContentType;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import com.digital.commerce.services.validator.DigitalFormValidationService;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"unchecked","rawtypes"})
@Getter
@Setter
public class DigitalForgotPasswordHandler extends ForgotPasswordHandler {

	private CommunicationService communicationService;
	
	private MessageLocator messageLocator;

	private DigitalServiceConstants dswConstants;
	
	private RepeatingRequestMonitor repeatingRequestMonitor;
	
	private DigitalAESEncryptionUtil encryptUtil;
	
	 private DigitalFormValidationService formValidationServices;
	
	private double version = 1.0;
	
	/* (non-Javadoc)
	 * @see atg.userprofiling.ForgotPasswordHandler#generateNewPasswordTemplateParams(atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse, atg.repository.RepositoryItem, java.lang.String)
	 */
	@Override
	protected Map generateNewPasswordTemplateParams(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse, RepositoryItem pProfile,
			java.lang.String pNewPassword) {
		ProfileTools pt = getProfileTools();

		try {

			MutableRepository repository = pt.getProfileRepository();
			MutableRepositoryItem profile = repository.getItemForUpdate(pProfile.getRepositoryId(), "user");
			Calendar dateNow=Calendar.getInstance();
			profile.setPropertyValue("lastPasswordUpdate", dateNow.getTime());
			String generatedPwdPropertyName = getProfileTools().getPropertyManager().getGeneratedPasswordPropertyName();
			Boolean isPasswordGenerated=(Boolean)profile.getPropertyValue(generatedPwdPropertyName);
			if(isPasswordGenerated){
				dateNow.add(Calendar.HOUR, dswConstants.getTemporaryPasswordResetExpiryHours());
				profile.setPropertyValue("tempPasswordExpiryDate",dateNow.getTime());
			}

			if(dswConstants.isPaTool()) {
				profile.setPropertyValue("nbrForgotPasswordTry",0);
				profile.setPropertyValue("newUserFlag", false);
			}
		} catch (Exception ex) {
			logError("Failed to update lastPasswordUpdate ", ex);
		}

		return super.generateNewPasswordTemplateParams(pRequest, pResponse,
				pProfile, pNewPassword);
	}

	
	/* (non-Javadoc)
	 * @see atg.userprofiling.ForgotPasswordHandler#handleForgotPassword(atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	@Override
	public boolean handleForgotPassword(DynamoHttpServletRequest request, //NOSONAR
			DynamoHttpServletResponse response) throws ServletException,
			IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		setValueProperty("email", null); // Done as in our case login itself is an email so we don't need to use the email property
		String myHandleMethod = "DigitalForgotPasswordHandler.handleForgotPassword";
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))){
			String emailAddress = (String) getValue().get("login");

            if(DigitalStringUtil.isBlank(emailAddress)) {
                emailAddress = request.getParameter("email");
            }
			
			if(DigitalStringUtil.isBlank(emailAddress)) {
				addFormException(new DropletException(
						messageLocator
								.getMessageString("forgotPasswordEmailEmpty"),
						"forgotPasswordEmailEmpty"));
				return false;
			}

			if (emailAddress != null) {
				//KTLO1-1600: Check if this valid email address otherwise assume its an loginHash and decrypt it
				if(!getFormValidationServices().validateEmailAddress(emailAddress)){
					String loginDecrypt;
					try {
						loginDecrypt = (String) getEncryptUtil().decrypt(emailAddress);
						if (null != loginDecrypt) {
							emailAddress = loginDecrypt;
							setValueProperty("login", emailAddress);
						}
					} catch (DigitalAppException e) {
						// TODO Auto-generated catch block
						if (isLoggingError()) {
							logError("handleChangePassword: Exception while decrypting the EMAIL for customer "
									+ e.getMessage());
						}
					}
					
				} 
				emailAddress = emailAddress.toLowerCase();
				setValueProperty("login", emailAddress);

			}

			
			if (checkForAccountLocked() && !dswConstants.isPaTool()) {
				addFormException(
						new DropletException(this.getMessageLocator().getMessageString("account.is.already.locked"),
								"accountIsAlreadyLocked"));
				return false;
			}
			
			final UserTransactionDemarcation td = TransactionUtils
					.startNewTransaction(this.getClass().getName(), "handleForgotPassword");
	
			MutableRepositoryItem[] users;
			String loginPropertyName = getProfileTools().getPropertyManager().getLoginPropertyName();
			String login = getStringValueProperty(loginPropertyName);
			Boolean emptySite = getEmptySite();
			String siteId = getSiteId();
	
			if (emptySite != null) {
				if (emptySite) {
					setSiteId(null);
				} else {
					if (siteId != null) {
						setSiteId(siteId);
					} else {
						setSiteId(SiteContextManager.getCurrentSiteId());
					}
				}
			} else {
				if (siteId != null) {
					setSiteId(siteId);
				} else {
					setSiteId(SiteContextManager.getCurrentSiteId());
				}
			}
	
			List<Recipient> recipients = new ArrayList<>();
	
			Recipient recipient = new Recipient();
	
			recipient.setAddress(login);
			recipient.setRecipientType(Recipient.RecipientType.TO);
			recipients.add(recipient);
	
			try {
				if (isLoggingInfo() && dswConstants.isPaTool()) {
					ContactCenterUser contactCenterUser = ComponentLookupUtil
							.lookupComponent(ComponentLookupUtil.INSOLE_AGENT_BEAN,
									ContactCenterUser.class);
					try {
						StringBuilder message = new StringBuilder();
						message.append("Insole agent ");
						if (null != contactCenterUser) {
							message.append(contactCenterUser.getUserId());
						}
						message.append(" attempted resetting password for customer "
								+ login);
						logInfo(message.toString());
					} catch (Exception e1) {
						logError("Error logging InSole Application Data");
					}
				}	
				users = lookupUsers(login, null);// Sending email as null because in our case login itself is an email so we don't need to use the email property
				Map dataTemplate = null;
				String templateKey = "ForgotPassword";
				if (users == null || users.length == 0) {
					logInfo("User does not exist with login " + emailAddress);
					if(dswConstants.isPaTool()) {
						addFormException(new DropletException(this.getMessageLocator()
								.getMessageString("insoleCustomerLoginDoesNotExist", "insoleCustomerLoginDoesNotExist")));
						return false;
					}
					else{
						templateKey = "ForgotPasswordNoAccount";
					}
				}
				dataTemplate = this.createDataTemplate(login,users,request,response);
				communicationService.send(recipients, templateKey,
						dataTemplate, ContentType.HTML, getSiteId());
			} catch (Exception e) {
				String error = String.format(
						"There was an error in generating the forgotPasswordEmail: emailAddress - %s",
						login);
				logError(error, e);
				addFormException(new DropletException(error, "GenericeForgotPasswordEmailError"));
				TransactionUtils.rollBackTransaction(td, this.getClass().getName(),
						"removeProfileRepositoryAddressById");
			} finally {				
				if(!dswConstants.isPaTool()) {
					// this is seperately logged for InSole
					logInfo("Forgot password action performed for customer with email " + login);
				}
				TransactionUtils.endTransaction(td, this.getClass().getName(),
						"handleForgotPassword");
				if (rrm != null) {
				      rrm.removeRequestEntry(myHandleMethod);
				}	
			}
		}
			return true;
	}

	private Map createDataTemplate(String login, MutableRepositoryItem[] users, DynamoHttpServletRequest request,
								   DynamoHttpServletResponse response) throws PropertyNotFoundException, RepositoryException, DigitalAppException {
		Map dataTemplate = null;
		if(users == null || users.length == 0) {
			dataTemplate = new HashMap();
			dataTemplate.put("createAccountUrl", MultiSiteUtil.getSiteURL()
					+ MultiSiteUtil.getCreateAccountURI()
					+ login);
		} else {
			String generatedPwdPropertyName = this.getProfileTools().getPropertyManager().getGeneratedPasswordPropertyName();
			boolean generatedPasswordFlg = (boolean)users[0].getPropertyValue(generatedPwdPropertyName);
			if(!generatedPasswordFlg){
				String currentPassword = (String)users[0].getPropertyValue("password");
				this.getProfileTools().getPreviousNPasswordManager().updatePreviousPasswordsProperty(
						(MutableRepositoryItem) users[0],currentPassword);
			}

			String newPassword = this.getProfileTools().generateNewPasswordForProfile(users[0]);
			dataTemplate = generateNewPasswordTemplateParams(request,
					response, users[0], newPassword);
			dataTemplate.put("firstName",
					users[0].getPropertyValue("firstName"));
			/*
			 * KTLO1-903: Avoid plain text email in forgotpassword URL
			 * Send the AES encrypted value of the login in email QS, if fails send as plain text.
			 * The changePassword method should be intelligent enough to decrypt or use plain text
			 *
			 */
			if(version > 1.0) {
				String loginHash =  encryptUtil.encrypt(login);
				if(null==loginHash){
					logInfo("ForgotPassword:: ENCRYPTION Failed for customer with login :: " + login);
				}
				dataTemplate.put("loginUrl", MultiSiteUtil.getSiteURL()
						+ MultiSiteUtil.getForgotPasswordURI()
						+ "&loginHash=" + loginHash);
			}
			else {
				dataTemplate.put("loginUrl", MultiSiteUtil.getSiteURL()
						+ MultiSiteUtil.getForgotPasswordURI()
						+ "&email=" + login);
			}

		}
		dataTemplate.put("trackingDate", DigitalDateUtil.getOperationEmailsTrackingDate());
		dataTemplate.put("siteUrl",MultiSiteUtil.getSiteURL());
		dataTemplate.put("findStoreUrl",MultiSiteUtil.getSiteURL()+ MultiSiteUtil.getStoreLocatorURI());

		return dataTemplate;
	}

	/**
	 * 
	 * @return
	 */
	private boolean checkForAccountLocked() {
		boolean locked = false;
		MutableRepositoryItem profileItem = null;
		String loginPropertyName = getProfileTools().getPropertyManager()
				.getLoginPropertyName();
		String login = getStringValueProperty(loginPropertyName);
		profileItem = (MutableRepositoryItem) getProfileTools().getItem(login,
				null);
		if (profileItem == null) {
			return locked;
		}
		Integer nbr = (Integer) profileItem
				.getPropertyValue("nbrForgotPasswordTry");
		if (nbr != null && nbr > MultiSiteUtil.getMaxForgotPasswordRetry()) {
			locked = true;
		}

		return locked;
	}
}