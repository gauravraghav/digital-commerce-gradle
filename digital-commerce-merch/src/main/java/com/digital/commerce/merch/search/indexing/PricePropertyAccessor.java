/**
 * 
 */
package com.digital.commerce.merch.search.indexing;

import java.util.Collection;
import java.util.Iterator;

import com.digital.commerce.common.logger.DigitalLogger;



import atg.commerce.pricing.priceLists.PriceListException;
import atg.commerce.pricing.priceLists.PriceListManager;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

@SuppressWarnings({"rawtypes"})
public class PricePropertyAccessor extends PropertyAccessorImpl {
	protected static String		LIST_PRICE_PROPERTY	= "listPrice";

	protected static String		CHILD_SKUS_PROPERTY	= "childSKUs";

	private PriceListManager	priceListManager;

	private boolean				mLoggingDebug		= false;

	private boolean				mLoggingError		= true;

	private static final DigitalLogger	logger				= DigitalLogger.getLogger( PricePropertyAccessor.class );

	protected Object getTextOrMetaPropertyValue( Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType ) {
		// RepositoryItem priceList = (RepositoryItem)
		// pContext.getAttribute("PRICE_LIST");
		RepositoryItem priceList = null;
		try {
			priceList = this.getPriceListManager().getPriceList( pPropertyName );
		} catch( PriceListException e ) {
			if( this.isLoggingError() ) {
				logger.error( "Exception: PriceListException", e );
			}
		}

		if( priceList != null ) {
			Double price = getPrice( pContext, pItem, priceList );

			return price;
		} else {
			return pItem.getPropertyValue( LIST_PRICE_PROPERTY );
		}
	}

	protected Double getPrice( Context pContext, RepositoryItem pItem, RepositoryItem pPriceList ) {
		if( pItem == null ) { return null; }
		// RepositoryItem sku = getSkuToPrice(pContext, pItem);
		RepositoryItem price = null;
		// if (sku != null)
		// {
		try {
			price = (RepositoryItem)this.getPriceListManager().getPrice( pPriceList, null, pItem );
			if( price != null ) {
				Double listPrice = (Double)price.getPropertyValue( LIST_PRICE_PROPERTY );

				return listPrice;
			} else {
				return null;
			}
		} catch( PriceListException e ) {
			if( isLoggingError() || pContext.getIndexingOutputConfig().isLoggingDebug() ) {
				pContext.getIndexingOutputConfig().logDebug( "Unable to price item " + pItem.getRepositoryId(), e );
			}
			return null;
		}
		// }
		// return null;
	}

	protected RepositoryItem getSkuToPrice( Context pContext, RepositoryItem pItem ) {
		Collection skus = (Collection)pItem.getPropertyValue( CHILD_SKUS_PROPERTY );
		Iterator i = skus.iterator();
		if( i.hasNext() ) { return (RepositoryItem)i.next(); }
		return null;
	}

	public PriceListManager getPriceListManager() {
		return priceListManager;
	}

	public void setPriceListManager( PriceListManager priceListManager ) {
		this.priceListManager = priceListManager;
	}

	public boolean isLoggingDebug() {
		return mLoggingDebug;
	}

	public boolean isLoggingError() {
		return mLoggingError;
	}

	public void setLoggingDebug( boolean pLoggingDebug ) {
		mLoggingDebug = pLoggingDebug;
	}

	public void setLoggingError( boolean pLoggingError ) {
		mLoggingError = pLoggingError;
	}
}
