package com.digital.commerce.merch.integration.catimport;

import atg.repository.RepositoryException;

/** contains import step, and failure reason
 * 
 * @author AndyShapiro */
public class CatalogImportException extends RepositoryException {

	// -------------------------------------
	// Properties
	// -------------------------------------

	/**
	 * 
	 */
	private static final long serialVersionUID = -6473574761880572929L;
	// -------------------------------------
	// property: ImportStep
	private String	mImportStep;

	public String getImportStep() {
		return mImportStep;
	}

	public void setImportStep( String pImportStep ) {
		mImportStep = pImportStep;
	}

	// -------------------------------------
	// property: FailureReason
	private String	mFailureReason;

	public String getFailureReason() {
		if( null == mFailureReason ) return getMessage();
		return mFailureReason;
	}

	public void setFailureReason( String pFailureReason ) {
		mFailureReason = pFailureReason;
	}

	// -------------------------------------
	// property: Description

	/** concatenates failure reason with source exception message */
	public String getDescription() {
		StringBuilder description = new StringBuilder( getFailureReason() );
		if( getSourceException() != null ) {
			description.append( ": originating exception:" );
			description.append( getSourceException().getMessage() );
		}
		return description.toString();
	}

	// -------------------------------------
	// Constructors

	public CatalogImportException( Throwable pSourceException ) {
		super( pSourceException );
		setSourceException( pSourceException );
	}

	public CatalogImportException( String pMsg ) {
		super( pMsg );
	}

	public CatalogImportException( String pImportStep, String pFailureReason ) {
		super();
		setImportStep( pImportStep );
		setFailureReason( pFailureReason );
	}

	public CatalogImportException( Exception pException, String pImportStep, String pFailureReason ) {
		super( pException.getMessage() );
		setSourceException( pException );
		setImportStep( pImportStep );
		setFailureReason( pFailureReason );
	}
}
