package com.digital.commerce.merch.integration.catimport.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import atg.core.util.StringUtils;

@SuppressWarnings({"rawtypes","unchecked"})
public class StringUtil extends StringUtils {
	private static final String	TOKEN_COMMA			= ",";
	private static final String	DEFAULT_DELIMITER	= TOKEN_COMMA;

	/** Convert string to ArrayList tokens by specified delimiter, using {@link java.util.regex.Pattern#split(CharSequence, int)} where limit
	 * is
	 * default to -1 here.
	 * 
	 * @param pString data string to be tokenized.
	 * @param pDel delimiter used when tokenize the string
	 * @param pTrim whether to trim the tokens of leading and trailing whitespcace
	 * @return list of tokens of the string using pDel as delimiter.
	 *         return empty set, if either pString or pDel is null. */
	public static Set tokenize( String pString, String pDel, boolean pTrim ) {
		HashSet tokens = new HashSet();
		if( pString != null && pDel != null ) {
			String[] arr = Pattern.compile( pDel ).split( pString, -1 );
			if( arr == null ) return null;
			for( int i = 0, len = arr.length; i < len; i++ ) {
				if( pTrim )
					tokens.add( arr[i].trim() );
				else
					tokens.add( arr[i] );
			}
		}
		return tokens;
	}

	/** Convert string to ArrayList tokens by default "," delimiter and trimming
	 * the tokens of leading and trailing whitespace
	 * 
	 * @param pString data string to be tokenized.
	 * @return list of tokens of the string using pDel as delimiter.
	 *         return null, if either pString or pDel is null. */
	public static Set tokenize( String pString ) {

		return tokenize( pString, DEFAULT_DELIMITER, true );
	}

	/** make a delimitter separated string version of a List of strings
	 * 
	 * @param propertyNames
	 * @param string
	 * @return */
	public static String makeList( List pPropertyNames, String pSeparator ) {
		if( null == pPropertyNames ) return "NULL LIST";
		return StringUtil.makeList( (String[])pPropertyNames.toArray( new String[pPropertyNames.size()] ), pSeparator );
	}

	/** Check if the string parameter is set
	 * 
	 * @param pValue
	 *            the string to check
	 * @return true if the string is set */
	public static boolean isSet( String pValue ) {

		if( pValue == null || pValue.length() == 0 ) return false;

		return true;

	}

	public static boolean isNotSet( String pValue ) {
		return !isSet( pValue );
	}
}
