package com.digital.commerce.merch.epub.workflow.automated;

import java.util.ArrayList;
import java.util.List;

public class DigitalBirthdayCouponsResponseData {
	private String			couponBatchId;
	private List<String>			couponCodes			= new ArrayList<>();
	private boolean couponsGenerated; 
	
	public boolean isCouponsGenerated() {
		return couponsGenerated;
	}
	public void setCouponsGenerated(boolean couponsGenerated) {
		this.couponsGenerated = couponsGenerated;
	}
	public List<String> getCouponCodes() {
		return couponCodes;
	}
	public void setCouponCodes(List<String> couponCodes) {
		this.couponCodes = couponCodes;
	}
	public String getCouponBatchId() {
		return couponBatchId;
	}
	public void setCouponBatchId(String couponBatchId) {
		this.couponBatchId = couponBatchId;
	}

}
