package com.digital.commerce.merch.assetmanager.action;

import java.util.Collection;
import java.util.Iterator;

import atg.droplet.DropletException;
import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryUtils;
import atg.service.asset.AssetResolverException;
import atg.service.asset.AssetWrapper;
import atg.servlet.ServletUtil;
import atg.web.assetmanager.AssetManagerResources;
import atg.web.assetmanager.Utils;
import atg.web.assetmanager.action.ActionResults;
import atg.web.assetmanager.action.ListActionFormHandler;
import atg.web.tree.CannotDeleteNonEmptyContainerException;
import atg.web.tree.LocalizedTreeException;
import atg.web.tree.MutableTreeDefinition;
import atg.web.tree.NoParentToRemoveFromException;
import atg.web.tree.TreeException;
import atg.web.tree.UnlinkNotAllowedException;
@SuppressWarnings({"rawtypes","deprecation"})
public class DigitalListActionFormHandler extends ListActionFormHandler {

	public ActionResults doNonDestinationOperation( String pOperationName ) throws AssetResolverException, TreeException {
		ActionResults results = new ActionResults();
		Collection checkedItemURIs = getCheckedURIs();
		Iterator iter = checkedItemURIs.iterator();
		while( iter.hasNext() ) {
			String uri = (String)iter.next();
			AssetWrapper item = Utils.getAsset( uri );
			MutableTreeDefinition treedef = (MutableTreeDefinition)getTreeDefinition( uri );
			if( pOperationName.equals( "duplicate" ) ) {
				treedef = null;
			}

			if( treedef != null ) {
				try {
					if( pOperationName.equals( "delete" ) ) treedef.deleteAsset( item );
					if( pOperationName.equals( "unlink" ) ) throw new RuntimeException( "Not implemented" );
					if( pOperationName.equals( "unlinkFromAll" ) ) treedef.unlinkAssetFromAll( item );
					results.addSuccessItem( item );
				} catch( NoParentToRemoveFromException e ) {
					results.addNoOpItem( item, "NoParentToRemoveFromException" );
				} catch( UnlinkNotAllowedException e ) {
					results.addNoOpItem( item, "UnlinkNotAllowedException" );
				} catch( CannotDeleteNonEmptyContainerException e ) {
					results.addNoOpItem( item, "CannotDeleteNonEmptyContainerException" );
				} catch( LocalizedTreeException e ) {
					throw e;
				} catch( TreeException e ) {
					results.addNoOpItem( item, "TreeException" );
				}
			} else {
				if( pOperationName.equals( "delete" ) ) {
					if( item.getAsset() instanceof RepositoryItem ) {
						try {
							RepositoryItem repitem = (RepositoryItem)item.getAsset();
							MutableRepository rep = (MutableRepository)repitem.getRepository();
							RepositoryUtils.removeReferencesToItem( repitem );
							rep.removeItem( repitem.getRepositoryId(), repitem.getItemDescriptor().getItemDescriptorName() );
							results.addSuccessItem( repitem );
						} catch( RepositoryException e ) {
							addFormException( new DropletException( AssetManagerResources.format( "bafh.delete.error", ServletUtil.getCurrentRequest() ) ) );
							if( isLoggingError() ) logError( e );
						}
					} else
						throw new RuntimeException( "Not implemented" );
				}
				if( pOperationName.equals( "unlink" ) ) { throw new RuntimeException( "Not implemented" ); }
				if( pOperationName.equals( "unlinkFromAll" ) ) { throw new RuntimeException( "Not implemented" ); }
				if( pOperationName.equals( "duplicate" ) ) {
					if( item.getAsset() instanceof RepositoryItem ) {
						try {
							RepositoryItem repitem = (RepositoryItem)item.getAsset();
							getCloneHelper().cloneItem( repitem );
							results.addSuccessItem( repitem );
						} catch( RepositoryException e ) {
							addFormException( new DropletException( AssetManagerResources.format( "bafh.duplicate.error", ServletUtil.getCurrentRequest() ) ) );
							if( isLoggingError() ) logError( e );
						}
					} else
						throw new RuntimeException( "Not implemented" );
				}
			}
		}
		if( !( getFormError() ) ) clearCheckedAssets();
		return results;
	}

}
