package com.digital.commerce.merch.integration.catimport.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
/** This class contains utility methods that operate on collections
 * 
 * @author ashapiro
 * @version $Id: CollectionUtil.java,v 1.1 2008/09/19 14:55:01 apowers Exp $ */

import com.digital.commerce.merch.integration.catimport.DigitalCatalogImportService;

/** @author ashapiro */
@SuppressWarnings({"rawtypes","unchecked"})
public class CollectionUtil extends GenericService {
	private static ApplicationLogging	sLogger	= ClassLoggingFactory.getFactory().getLoggerForClass( CollectionUtil.class );

	/** will add two maps together according to the following logic.
	 * If a Map.Entry only exists in one map, it will be added as is to the result
	 * map. If it exists in both, and if the corresponding values are Collections,
	 * the collections will be added to become the value of the Map.Entry for the
	 * result Map
	 * 
	 * @param pMap1 the first map
	 * @param pMap2 the second map
	 * @return the Map representing the addition of both maps */
	public static Map AddMaps( Map pMap1, Map pMap2 ) {
		HashMap retMap = new HashMap();
		if( sLogger.isLoggingDebug() ) sLogger.logDebug( "DEBUG: initial:" );
		DigitalCatalogImportService.debugExclusionMap( retMap );
		retMap.putAll( pMap1 );
		Iterator keyIter = pMap2.keySet().iterator();
		while( keyIter.hasNext() ) {
			// see if map 1 already has this key
			Object key = keyIter.next();
			Object map1Value = pMap1.get( key );
			Object map2Value = pMap2.get( key );
			if( map1Value == null ) { // does not, so add the object for this key to the new map
				retMap.put( key, pMap2.get( key ) );
			} else {  // if these are both collections we can add to the collection
				if( map1Value instanceof Collection && map2Value instanceof Collection ) {
					( (Collection)map1Value ).addAll( (Collection)map2Value );
					retMap.remove( key );
					retMap.put( key, map1Value );
				}
			}
		}
		return retMap;

	}
}
