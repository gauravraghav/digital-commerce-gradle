package com.digital.commerce.merch.mime;

import atg.remote.assetmanager.common.service.PreviewImageServiceImpl;
import atg.remote.assetmanager.common.service.ServiceUtils;
import atg.repository.RepositoryException;
import atg.service.asset.AssetWrapper;
import atg.web.viewmapping.ItemMappingException;
import atg.web.viewmapping.MappedItem;

import java.util.Map;

/**
 * Created by ihartney on 12/22/2014.
 */
@SuppressWarnings({"rawtypes","unused"})
public class Scene7PreviewImageServiceImpl extends PreviewImageServiceImpl{
    @Override
    public String getPreviewImageURL(AssetWrapper pAssetWrapper, MappedItem pMappedItem) {

        try {
            if (pMappedItem != null) {
                Map attributes = pMappedItem.getAttributes();
                String previewImagePropertyName = getPreviewImagePropertyName(pMappedItem);
                if(previewImagePropertyName!=null) {
                    Object previewImageURL = ServiceUtils.getPropertyValue(ServiceUtils.getAssetRepositoryItem(pAssetWrapper), previewImagePropertyName);
                    if (previewImageURL instanceof String) {
                        String imageURL = ((String) previewImageURL).toLowerCase();
                        if(imageURL.contains("scene7")){
                            return (String)previewImageURL;
                        }
                    }
                }

            }
        }catch(ItemMappingException imex){
            if(isLoggingError()){
                logError("Error getting attributes from mapped type.",imex);
            }
        }catch(RepositoryException rex){
            if(isLoggingError()){
                logError("Error getting previewImageUrl from assetWrapper",rex);
            }
        }
        return super.getPreviewImageURL(pAssetWrapper, pMappedItem);
    }


}
