package com.digital.commerce.merch.media.descriptor;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.nucleus.Nucleus;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

/** Property Descriptor for returning the Image URL for a Product
 * Get the SKU media mapper, default image server path and color codes
 * 
 * @author Professional Access */
public class BuildColorSwatchURLPropertyDescriptor extends RepositoryPropertyDescriptor {

	private transient DigitalLogger logger = DigitalLogger.getLogger(BuildColorSwatchesListPropertyDescriptor.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** This is a constant parameter name for the 'mediaNameMapper' component
	 * associated in the Repository definition file. */
	protected static final String	PATH_NAME_PROPERTY		= "mediaNameMapper";

	/** This is a constant parameter name for the 'id'.
	 * This constant is used to refer the id property of the Product repository item. */
	private static final String		ID_PROPERTY				= "id";

	/** Property to hold 'mMediaNameMapper'component. */
	protected String				mMediaNameMapper		= null;
	
	protected MutableRepository mediaRepository = null;

	/** @return Returns the SkuMediaNameMapper. */
	public String getSkuMediaNameMapper() {
		return mMediaNameMapper;
	}

	/** @param pMediaNameMapper The ProductFirstSkuColorCodeMapper to set. */
	public void setSkuMediaNameMapper( String pMediaNameMapper ) {
		mMediaNameMapper = pMediaNameMapper;
	}

	/** This method is used to construct product image url.
	 * This method fetches the Product default SKU color code and also image server path
	 * and constructs the URL, using the mediaNameMapper component associated.
	 * 
	 * 
	 * @param pItem parameter to hold RepositoryItem.
	 * @param pValue parameter to hold object.
	 * @return Object. */
	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		String colorCode = null;
		String productId = null;
		String defaultImageServer = null;
		StringBuilder finalURL = new StringBuilder();
		if( pItem != null ) {
			productId = (String)pItem.getPropertyValue( ID_PROPERTY );
			// altText = (String)pItem.getPropertyValue(DISPLAY_NAME_PROPERTY);
			colorCode = getSkuMediaMapper().getSKUColorCode( pItem );
		}
		if( getSkuMediaMapper().getDefaultImageServerMediaDir() != null ) {
			defaultImageServer = getSkuMediaMapper().getDefaultImageServerMediaDir();
		}
//		finalURL.append( "<img src='" );
		finalURL.append( defaultImageServer );
		finalURL.append( productId );
		finalURL.append( "_" );
		finalURL.append( colorCode );
		finalURL.append( "_ss_sw?$swatches$" );
//		finalURL.append( "_ss_sw?$swatches$' alt=\"" );
//		finalURL.append( altText );
//		finalURL.append( "\" width=\"41\" height=\"41\" class=\"mediaImage\" />" );
		
		try{
			
		
//		MutableRepositoryItem mediaItem = getMediaRepository().createItem("media-external");
		Nucleus nucleus = Nucleus.getGlobalNucleus();
		MutableRepository mediaRepository = (MutableRepository)nucleus.resolveName( "/atg/content/media/MediaRepository");
		
		MutableRepositoryItem mediaItem = mediaRepository.createItem("media-external");
		mediaItem.setPropertyValue("url", finalURL.toString());
		mediaItem.setPropertyValue("name", "Color Swatch");
		mediaItem.setPropertyValue("path", "/colorFolder");
		
		
		return mediaItem;
		
		}
		catch (Exception ex){
			logger.error("Error creating media: ", ex);
		}
		
		return null;
//		return finalURL.toString();

	}

	public MutableRepository getMediaRepository() {
		return mediaRepository;
	}

	public void setMediaRepository(MutableRepository mediaRepository) {
		this.mediaRepository = mediaRepository;
	}

	/* @see atg.repository.RepositoryPropertyDescriptor#setValue(java.lang.String, java.lang.Object) */
	public void setValue( String pAttributeName, Object pValue ) {
		super.setValue( pAttributeName, pValue );
		if( ( pValue == null ) || ( pAttributeName == null ) ) { return; }
		if( pAttributeName.equalsIgnoreCase( PATH_NAME_PROPERTY ) ) {
			setSkuMediaNameMapper( (String)pValue );
		}
	}

	/** @return Returns the ProductFirstSkuColorCodeMapper by
	 *         resolving it into Nucleus component */
	public ProductFirstSkuColorCodeMapper getSkuMediaMapper() {
		if( getSkuMediaNameMapper() == null ) {
			return null;
		} else {
			Nucleus nucleus = Nucleus.getGlobalNucleus();
			if( nucleus != null ) {
				return (ProductFirstSkuColorCodeMapper)nucleus.resolveName( getSkuMediaNameMapper() );
			} else {
				return null;
			}
		}
	}

}
