package com.digital.commerce.merch.services.targetting;

import java.util.*;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.multisite.RepositorySiteGroupStorageAdapter;
import atg.multisite.SiteGroup;
import atg.multisite.SiteGroupManager;
import atg.multisite.SiteGroupRepositoryImpl;
import atg.multisite.SiteManager;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemGroup;
import atg.repository.RepositoryView;
import atg.repository.nucleus.RepositoryGroupContainerService;

public class DigitalRepositoryGroupContainer extends RepositoryGroupContainerService {
	
	private SiteManager siteManager;
	private SiteGroupManager siteGroupManager;
	private Repository siteRepository;
	private String siteGroupItemType;
	private String siteGroupSitesProperty;
	private String siteConfigurationItemType;
	private RepositorySiteGroupStorageAdapter repositorySiteGroupStorageAdapter;
	private String userSegmentDescription;
	private Map<String, String> regionNameToDescriptionMap;
	
	private String sitePrefix;
	private String regionPrefix;
	private String siteGroupPrefix;
	private String prefixDelimiter;
	
	
	 
	/**
	 * It returns user segments and site groups
	 * 
	 * @param pRepository
	 * @param pItemDescViewName
	 * @return
	 * @throws RepositoryException
	 */
	public List<UserGroupsBean> getUserGroupsByTypeAndSubtypes(Repository pRepository, String pItemDescViewName)
			throws RepositoryException {
		List<RepositoryItemGroup> itemGroupsList = super
				.getGroupsByTypeAndSubtypes(pRepository, pItemDescViewName);
		if(isLoggingDebug()) {
			logDebug("itemGroupsList: "
					+ itemGroupsList);
		}
		List<UserGroupsBean> itemGroups = new ArrayList<>();
		RepositoryItem[] allSites = getAllSites();
		if(isLoggingDebug()) {
			logDebug("allSites: "
					+ Arrays.toString(allSites));
		}
		
		UserGroupsBean userGroupsBean = null;
		
		String delimiter = getPrefixDelimiter();
		
		if (allSites != null) {
			for (RepositoryItem siteItem : allSites) {
				StringBuilder siteBuffer = new StringBuilder();
				if (getSitePrefix() != null) {
					siteBuffer.append(getSitePrefix());
					if (delimiter != null) {
						siteBuffer.append(delimiter);
					}
				}
				siteBuffer.append(siteItem.getRepositoryId());
				userGroupsBean = new UserGroupsBean();
				userGroupsBean.setName(siteBuffer.toString());
				userGroupsBean.setDescription((String) siteItem
						.getPropertyValue("name"));
				itemGroups.add(userGroupsBean);
			}
		}
		for (RepositoryItemGroup repositoryItemGroup : itemGroupsList) {
			userGroupsBean = new UserGroupsBean();
			if (isLoggingDebug()) {
				logDebug("User Group Name: "
						+ repositoryItemGroup.getGroupName());
			}
			userGroupsBean.setName(repositoryItemGroup.getGroupName());
			userGroupsBean.setDescription(getUserSegmentDescription() + " "
					+ repositoryItemGroup.getGroupName());
			itemGroups.add(userGroupsBean);
		}
        
		Map<String, String> regions = getRegionGroups();
		if (null != regions) {
			
			for (Map.Entry<String, String> entry : regions.entrySet()) {
				userGroupsBean = new UserGroupsBean();
				StringBuilder regionBuffer = new StringBuilder();
				if (getRegionPrefix() != null) {
					regionBuffer.append(getRegionPrefix());
					if (delimiter != null) {
						regionBuffer.append(delimiter);
					}
				}
				regionBuffer.append(entry.getKey());
				userGroupsBean.setName(regionBuffer.toString());
				userGroupsBean.setDescription(entry.getValue());
				itemGroups.add(userGroupsBean);
				
			}
			
		}
		return itemGroups;
	}
	
	/**
	 * 
	 * Returns all sites from Version repository
	 * 
	 * @return
	 * @throws RepositoryException
	 */
	public RepositoryItem[] getAllSites() throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Inside getAllSites()");
		}
		return getAllSites(getSiteRepository());
	}
	
	/**
	 * 
	 * @param pSiteRepository
	 * @return
	 * @throws RepositoryException
	 */
	@SuppressWarnings("UnnecessaryBoxing")
	public RepositoryItem[] getAllSites(Repository pSiteRepository)
			throws RepositoryException {
	
		if (isLoggingDebug()) {
			logDebug("Querying all sites");
		}

		RepositoryView view = pSiteRepository.getView("siteConfiguration");

		QueryBuilder builder = view.getQueryBuilder();
		QueryExpression siteEnabled = builder.createPropertyQueryExpression("enabled");
        QueryExpression siteEnabledValue = builder.createConstantQueryExpression(new Integer(1));
        Query siteEnabledQuery = builder.createComparisonQuery(siteEnabled, siteEnabledValue, QueryBuilder.EQUALS);
		
        return view.executeQuery(siteEnabledQuery);
    }

	
	/**
	 * Helper method to get siteGroups by siteId
	 * 
	 * @param pSiteId
	 * @return
	 */
	public Collection<SiteGroup> getSiteGroupsBySiteId(String pSiteId) {
		if (isLoggingDebug()) {
			logDebug("This is ....getSiteGroupsBySiteId pSiteId "+pSiteId);
		}
		
		if (DigitalStringUtil.isBlank(pSiteId)) {
			vlogDebug("SiteId is null, returning null", new Object[0]);
			return null;
		}
		RepositoryItem[] siteGroups = null;
		try {
			RepositoryView siteGroupView = getSiteRepository()
					.getView(getSiteGroupItemType());

			QueryBuilder siteGroupBuilder = siteGroupView.getQueryBuilder();

			QueryExpression sitePropQE = siteGroupBuilder
					.createPropertyQueryExpression(getSiteGroupSitesProperty());

			RepositoryView siteView = getSiteRepository().getView(
					getSiteConfigurationItemType());

			QueryBuilder siteBuilder = siteView.getQueryBuilder();
			Query siteIdQuery = siteBuilder
					.createIdMatchingQuery(new String[] { pSiteId });

			Query siteQuery = siteGroupBuilder.createIncludesItemQuery(
					sitePropQE, siteIdQuery);

			siteGroups = siteGroupView.executeQuery(siteQuery);
		} catch (RepositoryException re) {
			vlogError(re, "Error getting SiteGroup repository items",
					new Object[0]);
		}
		if (siteGroups == null) {
			vlogDebug("No SiteGroups contain site {0}.",
					new Object[] { pSiteId });
			return null;
		}
		return wrapSiteGroupRepoItems(siteGroups);
	}
	
	/**
	 * 
	 * @param repoItems
	 * @return
	 */
	protected Collection<SiteGroup> wrapSiteGroupRepoItems(
			RepositoryItem[] repoItems) {
		if (isLoggingDebug()) {
			logDebug("This is  wrapSiteGroupRepoItems");
		}
		if ((repoItems == null) || (repoItems.length <= 0)) {
			return null;
		}
		Collection<SiteGroup> siteGroups = new HashSet<>();
		for (RepositoryItem item : repoItems) {
			SiteGroupRepositoryImpl siteGroup = repositorySiteGroupStorageAdapter
					.getSiteGroupRepositoryImpl(item);
			siteGroups.add(siteGroup);
		}
		return siteGroups;
	}
	
	/**
	 * Gets site groups and formats to fit into response
	 * 
	 * @return
	 */
	private Map<String,String> getRegionGroups() {
		Map<String,String> regionMap = new HashMap<>();
		if(regionNameToDescriptionMap == null) {
			return null;
		}
		for (Map.Entry<String, String> entry : regionNameToDescriptionMap.entrySet())
		{
		    if (isLoggingDebug()) {
				logDebug("Region Groups: "+entry.getKey()+ entry.getValue());
			}
		    regionMap.put(entry.getKey(), entry.getValue());
		}
		return regionMap;
		
	}

	public SiteManager getSiteManager() {
		return siteManager;
	}

	public void setSiteManager(SiteManager siteManager) {
		this.siteManager = siteManager;
	}

	public SiteGroupManager getSiteGroupManager() {
		return siteGroupManager;
	}

	public void setSiteGroupManager(SiteGroupManager siteGroupManager) {
		this.siteGroupManager = siteGroupManager;
	}

	public Repository getSiteRepository() {
		return siteRepository;
	}

	public void setSiteRepository(
			Repository siteRepository) {
		this.siteRepository = siteRepository;
	}

	public String getSiteGroupItemType() {
		return siteGroupItemType;
	}

	public void setSiteGroupItemType(String siteGroupItemType) {
		this.siteGroupItemType = siteGroupItemType;
	}

	public String getSiteGroupSitesProperty() {
		return siteGroupSitesProperty;
	}

	public void setSiteGroupSitesProperty(String siteGroupSitesProperty) {
		this.siteGroupSitesProperty = siteGroupSitesProperty;
	}

	public String getSiteConfigurationItemType() {
		return siteConfigurationItemType;
	}

	public void setSiteConfigurationItemType(String siteConfigurationItemType) {
		this.siteConfigurationItemType = siteConfigurationItemType;
	}

	public RepositorySiteGroupStorageAdapter getRepositorySiteGroupStorageAdapter() {
		return repositorySiteGroupStorageAdapter;
	}

	public void setRepositorySiteGroupStorageAdapter(
			RepositorySiteGroupStorageAdapter repositorySiteGroupStorageAdapter) {
		this.repositorySiteGroupStorageAdapter = repositorySiteGroupStorageAdapter;
	}

	public String getUserSegmentDescription() {
		return userSegmentDescription;
	}

	public void setUserSegmentDescription(String userSegmentDescription) {
		this.userSegmentDescription = userSegmentDescription;
	}

	public Map<String, String> getRegionNameToDescriptionMap() {
		return regionNameToDescriptionMap;
	}

	public void setRegionNameToDescriptionMap(
			Map<String, String> regionNameToDescriptionMap) {
		this.regionNameToDescriptionMap = regionNameToDescriptionMap;
	}

	public String getSitePrefix() {
		return sitePrefix;
	}

	public void setSitePrefix(String sitePrefix) {
		this.sitePrefix = sitePrefix;
	}

	public String getSiteGroupPrefix() {
		return siteGroupPrefix;
	}

	public void setSiteGroupPrefix(String siteGroupPrefix) {
		this.siteGroupPrefix = siteGroupPrefix;
	}

	public String getPrefixDelimiter() {
		return prefixDelimiter;
	}

	public void setPrefixDelimiter(String prefixDelimiter) {
		this.prefixDelimiter = prefixDelimiter;
	}

	public String getRegionPrefix() {
		return regionPrefix;
	}

	public void setRegionPrefix(String regionPrefix) {
		this.regionPrefix = regionPrefix;
	}






}