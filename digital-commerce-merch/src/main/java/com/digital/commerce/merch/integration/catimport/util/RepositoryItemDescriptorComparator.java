package com.digital.commerce.merch.integration.catimport.util;

import java.util.Comparator;

import com.digital.commerce.merch.integration.catimport.repository.RepositoryUtils;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.repository.RepositoryItemDescriptor;

/** Comparator for item descriptors. This provides a comparator for use with Arrays.sort. It implements
 * an import sort order based on the "importorder" attribute specified at the item descriptor leve in
 * the ATG catalog repository definition. If an item descriptor is defined as follows:
 * <item-descriptor name="product" ... >
 * ...
 * <attribute name="importorder" value="100"/>
 * ...
 * </item-descriptor>
 * then the value used for ordering the import the product catalog is 100. Item types with values less
 * 100 will be have their new items imported into the CA project before product. Item types with values
 * greater than 100 will have their items import into the CA project after product items. If a value
 * is not specified, it defaults to 0 (zero).
 * 
 * Note: From empirical results, it is import to import product items after sku items. If not, all the
 * product child skus will not be properly linked up.
 * 
 * @author taiken */
public class RepositoryItemDescriptorComparator implements Comparator<RepositoryItemDescriptor> {

	private static final String			IMPORT_ORDER_ATTRIBUTE_NAME	= "importorder";
	private static final int				DEFAULT_ORDER_PRIORITY		= 0;

	private static ApplicationLogging	sLogger						= ClassLoggingFactory.getFactory().getLoggerForClass( RepositoryUtils.class );

	public int compare( RepositoryItemDescriptor r1, RepositoryItemDescriptor r2 ) {

		String r1ImportOrder = (String)r1.getBeanDescriptor().getValue( IMPORT_ORDER_ATTRIBUTE_NAME );
		String r2ImportOrder = (String)r2.getBeanDescriptor().getValue( IMPORT_ORDER_ATTRIBUTE_NAME );

		// get the import attribute for the first item descriptor and convert it to an int.
		int r1Order = DEFAULT_ORDER_PRIORITY;

		if( r1ImportOrder != null ) {
			sLogger.logDebug( "RepositoryItemDescriptorComparator: " + r1.getItemDescriptorName() + " import Order is " + r1ImportOrder );

			try {
				r1Order = new Integer( r1ImportOrder ).intValue();
				sLogger.logDebug( "RepositoryItemDescriptorComparator: " + r1Order + " (int)" );
			} catch( NumberFormatException nfe ) {
				// do nothing, will default to the DEFAULT_ORDER_PRIORTY
				sLogger.logDebug( "RepositoryItemDescriptorComparator: " + r1.getItemDescriptorName() + " threw NumberFormatException exception." );
			}
		} else {
			sLogger.logDebug( "RepositoryItemDescriptorComparator: " + r1.getItemDescriptorName() + " import Order is null." );
		}

		// get the import attribute for the second item descriptor and convert it to an int.
		int r2Order = DEFAULT_ORDER_PRIORITY;

		if( r2ImportOrder != null ) {
			sLogger.logDebug( "RepositoryItemDescriptorComparator: " + r2.getItemDescriptorName() + " import Order is " + r2ImportOrder );

			try {
				r2Order = new Integer( r2ImportOrder ).intValue();
				sLogger.logDebug( "RepositoryItemDescriptorComparator: " + r2Order + " (int)" );
			} catch( NumberFormatException nfe ) {
				// do nothing, will default to the DEFAULT_ORDER_PRIORTY
				sLogger.logDebug( "RepositoryItemDescriptorComparator: " + r2.getItemDescriptorName() + " threw NumberFormatException exception." );
			}
		} else {
			sLogger.logDebug( "RepositoryItemDescriptorComparator: " + r2.getItemDescriptorName() + " import Order is null." );
		}

		sLogger.logDebug( "RepositoryItemDescriptorComparator: " + r1.getItemDescriptorName() + " has import order " + r1Order );
		sLogger.logDebug( "RepositoryItemDescriptorComparator: " + r2.getItemDescriptorName() + " has import order " + r2Order );
		sLogger.logDebug( "RepositoryItemDescriptorComparator: returning " + ( r1Order - r2Order ) );
		sLogger.logDebug( "---------------------------" );
		return r1Order - r2Order;
	}

}
