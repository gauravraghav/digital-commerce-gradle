package com.digital.commerce.merch.multisite.validation;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.remote.assetmanager.editor.model.PropertyUpdate;
import atg.remote.assetmanager.editor.service.AssetEditorInfo;
import atg.remote.assetmanager.editor.service.RepositoryAssetPropertyServiceImpl;
import atg.repository.RepositoryItem;
import atg.service.dynamo.LangLicense;
import atg.servlet.ServletUtil;
@SuppressWarnings({"unchecked"})
public class DigitalLocalePropertyValidationService extends
		RepositoryAssetPropertyServiceImpl {

	// --------------------------------------------------------------------------
	// CONSTANTS
	// --------------------------------------------------------------------------

	/** Resource name */
	private static final String RESOURCE_NAME = "com.digital.commerce.common.i18n.DigitalTranslationValitionResources";

	// --------------------------------------------------------------------------
	// METHODS
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------
	/**
	 * @return The resource bundle to be used in this class.
	 */
	public ResourceBundle getResourceBundle() {

		// Get the locale from the logged in user. If the user's locale can't be
		// found,
		// get the browser locale. If either of these can't be found, use the
		// server locale.
		Locale locale = ServletUtil.getUserLocale();

		if (locale != null) {
			// Get the resource bundle associated with the current locale.
			try{
				return LayeredResourceBundle.getBundle(RESOURCE_NAME, locale);
			}catch(Exception ex){
				logWarning("Error retrieving locale based resourcebundle for locale - " + locale + ex.getMessage());
			}
		}

		// Couldn't retrieve the locale from user's profile, browser or server
		// so just get the default locale.
		return LayeredResourceBundle.getBundle(RESOURCE_NAME,
				LangLicense.getLicensedDefault());
	}

	// --------------------------------------------------------------------
	/**
	 * Validate the repository item with the given property update.
	 * 
	 * @param pEditorInfo
	 *            pEditorInfo Information object for the current editor.
	 * @param pItem
	 *            The current updated asset.
	 * @param pUpdates
	 *            URL property update.
	 */
	@Override
	protected void validateItemPropertyUpdate(AssetEditorInfo pEditorInfo,
			RepositoryItem pItem, PropertyUpdate pUpdate) {
		super.validateItemPropertyUpdate(pEditorInfo, pItem, pUpdate);

		// Run 'translations' property key validation.
		if (pUpdate.getPropertyName().equals("defaultCountry")) {// property name from siteRepository.xml
			validateSiteDefaultCountryKeyValue(pEditorInfo, pItem, pUpdate);
		}
	}

	// --------------------------------------------------------------------
	/**
	 * <p>
	 * Validate the translations property keys to ensure they are valid language
	 * codes. An invalid language code returns an error to the user.
	 * </p>
	 * <p>
	 * This method also checks each site in the category/product/sku site list
	 * to see if an updated translation property key is supported by those
	 * sites. If the language is not found in any of these sites 'languages'
	 * property lists, a warning is returned to the user.
	 * </p>
	 * 
	 * @param pEditorInfo
	 *            pEditorInfo Information object for the current editor.
	 * @param pItem
	 *            The current updated asset.
	 * @param pUpdates
	 *            URL property update.
	 */
	public void validateSiteDefaultCountryKeyValue(
			AssetEditorInfo pEditorInfo, RepositoryItem pItem,
			PropertyUpdate pUpdate) {

		// Get the translations property updates.
		Map<String, RepositoryItem> localesStr = (Map<String, RepositoryItem>) super
				.getCollectionPropertyUpdateValue(pEditorInfo, pItem, pUpdate);

		if (localesStr != null) {
			
			// Retrieve the updated language code values.
			Set<String> localesCodes = localesStr.keySet();

			for (String localeCode : localesCodes) {

				// Add an error if an updated language code doesn't exist in the ISO language list.
				if (!isValidLocale(localeCode)) {

					if (isLoggingDebug()) {
						logDebug("The translations lcoale code key ("
								+ localeCode
								+ ") does not conform to the Java ISO locale standard.");
					}

					Object[] errorParams = { localeCode };

					pEditorInfo.getAssetService().addError(
						ResourceUtils.getUserMsgResource("catalog.error.translationLanguageKeyInvalid",
													RESOURCE_NAME, getResourceBundle(), errorParams));
				}
			}
		}
	}
	
	boolean isValidLocale(String value) {
		boolean ret = false;
		try {
			org.apache.commons.lang.LocaleUtils.toLocale(value);
			ret = true;
		} catch (Exception ex) {
			logError("Invalid locale value = " + value, ex);
			ret = false;
		}
		return ret;
	}
}
