package com.digital.commerce.merch.epub.workflow.automated;

import atg.epub.workflow.process.action.PublishingActionConfiguration;
import atg.repository.Repository;

public class DigitalPublishingActionConfiguration extends PublishingActionConfiguration{
	private Repository	auditRepository;
	private Repository externalProfileRepository;
	private String sqlBirthdayGiftStatusItemsQuery;

	/**
	   * Default Constructor
	   */
	  public DigitalPublishingActionConfiguration(){
	    super();
	  }

	public Repository getAuditRepository() {
		return auditRepository;
	}

	public void setAuditRepository(Repository auditRepository) {
		this.auditRepository = auditRepository;
	}

	public Repository getExternalProfileRepository() {
		return externalProfileRepository;
	}

	public void setExternalProfileRepository(Repository externalProfileRepository) {
		this.externalProfileRepository = externalProfileRepository;
	}

	public String getSqlBirthdayGiftStatusItemsQuery() {
		return sqlBirthdayGiftStatusItemsQuery;
	}

	public void setSqlBirthdayGiftStatusItemsQuery(
			String sqlBirthdayGiftStatusItemsQuery) {
		this.sqlBirthdayGiftStatusItemsQuery = sqlBirthdayGiftStatusItemsQuery;
	}

}
