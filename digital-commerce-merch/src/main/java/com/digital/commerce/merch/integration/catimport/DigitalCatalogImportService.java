package com.digital.commerce.merch.integration.catimport;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.transaction.TransactionManager;

import com.digital.commerce.merch.integration.catimport.repository.CloneInfo;
import com.digital.commerce.merch.integration.catimport.repository.RepositoryUtils;
import com.digital.commerce.merch.integration.catimport.util.DateUtils;
import com.digital.commerce.merch.integration.catimport.util.RepositoryItemDescriptorComparator;

import atg.adapter.gsa.GSARepository;
import atg.beans.DynamicPropertyDescriptor;
import atg.core.util.ResourceUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.ServiceException;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.repository.DuplicateIdException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.jdbc.FakeXADataSource;
import atg.service.jdbc.MonitoredDataSource;
import atg.service.util.CurrentDate;

/** Class that imports repository items. The import should be set up as follows:
 * (1) The repository definition files for the repositories whose new items are to
 * be input should contain attributes that specify which item types to import,
 * which properties of those item types to import, and what order to import
 * them in.
 * (2) The method doNewImport is considered the entry point to this class and
 * should be called with a map of source to destination repositories.
 * 
 * Basically it is assumed that the source repository is a mirror of the destination
 * repository. In addition, the repository for the import audit records must be
 * specified (property auditRepository).
 * 
 * @author Brenda Faulkner, Andy Powers, Tim Aiken
 * 
 * 
 * @version $Id: DigitalCatalogImportService.java,v 1.9 2010/10/01 15:17:25 apowers Exp $ */
@SuppressWarnings({"rawtypes"})
public class DigitalCatalogImportService extends com.digital.commerce.merch.integration.catimport.util.GenericService {

	private static ApplicationLogging	sLogger						= ClassLoggingFactory.getFactory().getLoggerForClass( DigitalCatalogImportService.class );

	// RQL Constants for retrieving items to import
	// Object Array for RQL Query
	private static final Object[]		PARAM_ARRAY					= new Object[1];
	private static final String			BASE_RQL					= "all";

	private static final String			NEW_ITEM_RQL				= "feedIsNew=true";
	
	private static final String			UPDATED_ITEM_RQL				= "feedModified=true";

	// RQL to retrieve last successful import record
	private static final String			AUDIT_RQL					= "status=\"exported\" and importType = ?0 ORDER BY importDate DESC";
	
	// RQL to retrieve last successful import record
		private static final String			AUDIT_UPDATE_RQL					= "status=\"exported\" and importType = ?0 and jobType = ?1 ORDER BY importDate DESC";

	private static final String			IMPORT_DATE_PROPERTY		= "importDate";
	// Constants for Error Handling
	private static final String			ERROR_WRITING_AUDIT_FAILURE	= "was not able to write audit record";

	private static final String			AUDIT_IMPORT_SUCCESS		= "successful import";
	private static final String			AUDIT_IMPORT_FAILURE		= "failed import";

	private static final String			ERROR_REPOSITORY_EXCEPTION	= "import encountered a repository exception";

	/** resources file */
	// ResourceBundle Name
	protected static final String		RESOURCE_BUNDLE_NAME		= "com.digital.commerce.merch.integration.catimport.CatalogImportResources";

	// ResourceBundle
	protected static ResourceBundle		sResourceBundle				= ResourceBundle.getBundle( RESOURCE_BUNDLE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault() );

	private static final String			IMPORT_EXCEPTION_KEY		= "IMPORT_EXCEPTION";

	private final static String		IMPORT_EXCEPTION_STRING		= ResourceUtils.getUserMsgResource( IMPORT_EXCEPTION_KEY, RESOURCE_BUNDLE_NAME, sResourceBundle );

	private boolean						defaultUpdateFlag;

	public boolean isDefaultUpdateFlag() {
		return defaultUpdateFlag;
	}

	public boolean getDefaultUpdateFlag() {
		return defaultUpdateFlag;
	}

	public void setDefaultUpdateFlag( boolean updateflag ) {
		defaultUpdateFlag = updateflag;
	}

	// class constants for import via XML item descriptor and property import definition feature

	// for any item type, we assume to that the following RQL is all we need to determine if the item is new
	private static final String			IMPORT_NEW_RQL												= "feedIsNew=true";
	private static RqlStatement			IMPORT_RQL_STATEMENT										= null;

	static {
		try {
			IMPORT_RQL_STATEMENT = RqlStatement.parseRqlStatement( IMPORT_NEW_RQL );
		} catch( Exception e ) {/* should never happen */
			sLogger.logInfo("Generic exception while reading the auto import SQl");
		}
	}

	/* There are several attributes to specify what to copy and an addition attribute to specify the order
	 * 1. At the item type (item descriptor level). Here you specify one of:
	 * <attribute name="importNewItemsViaCIS" value="all"/>,
	 * <attribute name="importNewItemsViaCIS" value="allwritable"/>,
	 * <attribute name="importNewItemsViaCIS" value="allrequred"/>, or
	 * <attribute name="importNewItemsViaCIS" value="perpropertyattributes"/>
	 * The values are case insensitive. */
	private static final String			ITEM_TYPE_CIS_IMPORT_ATTRIBUTE								= "importNewItemsViaCIS";

	private static final String			ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_ALL					= "all";
	private static final String			ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_ALLWRITABLE			= "allwritable";
	private static final String			ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_ALLREQUIRED			= "allrequired";
	private static final String			ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_PERPROPERTYATTRIBUTE	= "perpropertyattributes";
	private static final String			ITEM_TYPE_CIS_UPDATE_ATTRIBUTE_VALUE_PERPROPERTYATTRIBUTE	= "perpropertyUpdatebleattributes";
	private static final String			ITEM_PROPERTY_UPDATABLE_VIA_CIS	= "itemPropertyUpdatableViaCIS";
	/* 2. At the property level. Here you specify one of:
	 * <attribute name="copyNewItemPropertyViaCIS" value="include" />
	 * <attribute name="copyNewItemPropertyViaCIS" value="exclude" />
	 * The values are case insensitive here as well.
	 * You can mix the item type level and property level specifications inside the same item type to
	 * include and exclude extra properties. Please be aware that "perpropertyattributes" is the default
	 * and therefore just an explicit way to show the import spec. */
	private static final String			PROPERTY_TYPE_COPY_NEW_ITEMS_VIA_CIS						= "copyNewItemPropertyViaCIS";
	private static final String			PROPERTY_TYPE_COPY_NEW_ITEMS_VIA_CIS_INCLUDED				= "include";
	private static final String			PROPERTY_TYPE_COPY_NEW_ITEMS_VIA_CIS_EXCLUDED				= "exclude";
	

	// cISImportItemTypeAttributeValidValues contains the four attribute values for import
	// specification at the item type level, i.e {"all", "allwritable", "allrequired", and "perpropertyattributes"
	private static final Set<String>	cISImportItemTypeAttributeValidValues						= new HashSet<>();

	static {

		cISImportItemTypeAttributeValidValues.add( ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_ALL );
		cISImportItemTypeAttributeValidValues.add( ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_ALLWRITABLE );
		cISImportItemTypeAttributeValidValues.add( ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_ALLREQUIRED );
		cISImportItemTypeAttributeValidValues.add( ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_PERPROPERTYATTRIBUTE );
	}
	
	private static final String			ITEM_TYPE_IMPORT_USING_UPDATE								= "cisImportUsingUpdateIfExists";

	private static final String			ITEM_TYPE_CIS_UPDATE_ATTRIBUTE								= "updateItemsViaCIS";
	private static final String			TRUE_STRING													= "true";

	/* The following interface and static initialization code sets up a mapping of
	 * <item type attribute value> - function to all for that attribute value.
	 * However because of the limitations of Java to treat functions/methods as first
	 * class objects, we have to wrap the function in an anonymous class defined by the
	 * Callable Interface (The "Command" pattern), which is declared above.
	 * Perhaps this is a bit of overkill, but we are trying to make this as easily extensible
	 * as possible. */
	protected interface Callable<I, O> {
		public O call( I input );
	}

	private static Map<String, Callable<RepositoryItemDescriptor, Set<String>>>	repositoryPropertyMappingMethods	= new HashMap<>();

	static {
		repositoryPropertyMappingMethods.put( ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_ALL, new Callable<RepositoryItemDescriptor, Set<String>>() {
			public Set<String> call( RepositoryItemDescriptor r ) {
				return getAllProperties( r );
			}
		} );

		repositoryPropertyMappingMethods.put( ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_ALLREQUIRED, new Callable<RepositoryItemDescriptor, Set<String>>() {
			public Set<String> call( RepositoryItemDescriptor r ) {
				return getRequiredProperties( r );
			}
		} );

		repositoryPropertyMappingMethods.put( ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_ALLWRITABLE, new Callable<RepositoryItemDescriptor, Set<String>>() {
			public Set<String> call( RepositoryItemDescriptor r ) {
				return getWritableProperties( r );
			}
		} );

		repositoryPropertyMappingMethods.put( ITEM_TYPE_CIS_IMPORT_ATTRIBUTE_VALUE_PERPROPERTYATTRIBUTE, new Callable<RepositoryItemDescriptor, Set<String>>() {
			public Set<String> call( RepositoryItemDescriptor r ) {
				return getPerPropertyAttributeProperties( r );
			}
		} );
	
	}

	// This variable contains a map of the {item descriptor name} + "." + {attribute value} ==> property set
	// where propertySet contains the properties that correspond to that attribute value.
	private Map<RepositoryItemDescriptor, Set<String>>							importItemDescriptorPropertyMap		= new HashMap<>();

	public Map<RepositoryItemDescriptor, Set<String>> getImportItemDescriptorPropertyMap() {
		return importItemDescriptorPropertyMap;
	}

	public void setImportItemDescriptorPropertyMap( Map<RepositoryItemDescriptor, Set<String>> importPropertyMap ) {
		this.importItemDescriptorPropertyMap = importPropertyMap;
	}

	// Initialization states for the import property map
	public enum InitImportPropertyMapResultType {
		PROPERTYMAPUNINITIALIZED, PROPERTYMAPINITIALIZESUCCESS, PROPERTYMAPINITIALIZEERROR
	}

    // Data to hold the initial state
	private static InitImportPropertyMapResultType	initializedImportPropertyMapStatus	= InitImportPropertyMapResultType.PROPERTYMAPUNINITIALIZED;

	public static InitImportPropertyMapResultType getInitializedImportPropertyMap() {
		return initializedImportPropertyMapStatus;
	}

	public static void setInitializedImportPropertyMap( InitImportPropertyMapResultType initializedImportPropertyMapStat ) {
		DigitalCatalogImportService.initializedImportPropertyMapStatus = initializedImportPropertyMapStat;
	}
	
	// here this is just feedModified="true" used to look for updated items

		String	mUpdatedItemRqlString	= UPDATED_ITEM_RQL;

		public String getUpdatedItemRqlString() {
			return mUpdatedItemRqlString;
		}

		public void setUpdatedItemRqlString( String pUpdatedItemRqlString ) {
			mUpdatedItemRqlString = pUpdatedItemRqlString;
		}

		private RqlStatement	mUpdatedItemRql;

		public RqlStatement getUpdatedItemRql() {
			return mUpdatedItemRql;
		}

	// here this is just feedIsNew="true" used to look for new items

	String	mNewItemRqlString	= NEW_ITEM_RQL;

	public String getNewItemRqlString() {
		return mNewItemRqlString;
	}

	public void setNewItemRqlString( String pNewItemRqlString ) {
		mNewItemRqlString = pNewItemRqlString;
	}

	private RqlStatement	mNewItemRql;

	public RqlStatement getNewItemRql() {
		return mNewItemRql;
	}

	private Repository	auditRepository;

	public Repository getAuditRepository() {
		return auditRepository;
	}

	public void setAuditRepository( Repository auditRepository ) {
		this.auditRepository = auditRepository;
	}

	// variables for import audit state

	private boolean	lastImportResult;

	public boolean getLastImportResult() {
		return lastImportResult;
	}

	public void setLastImportResult( boolean lastImportResult ) {
		this.lastImportResult = lastImportResult;
	}

	private RepositoryItem	lastImportAuditRecord;

	public RepositoryItem getLastImportAuditRecord() {
		return lastImportAuditRecord;
	}

	public void setLastImportAuditRecord( RepositoryItem lastImportAuditRecord ) {
		this.lastImportAuditRecord = lastImportAuditRecord;
	}

	private CatalogImportException	lastImportException;

	public CatalogImportException getLastImportException() {
		return lastImportException;
	}

	public void setLastImportException( CatalogImportException lastImportException ) {
		this.lastImportException = lastImportException;
	}


	// -------------------------------------
	// Member Variables
	// ---

	private RqlStatement			mAuditRql;
	
	private RqlStatement			mAuditUpdateRql;

	private CatalogImportException	mException			= null;

	// -------------------------------------
	// Member Variables
	// -------------------------------------

	// -------------------------------------
	// Properties
	// -------------------------------------

	String							mDefaultRqlString	= BASE_RQL;

	public String getDefaultRqlString() {
		return mDefaultRqlString;
	}

	public void setDefaultRqlString( String pSkuLinkRqlString ) {

		mDefaultRqlString = pSkuLinkRqlString;
	}

	// temporary properties for testing purposes
	// reset the following to cause this to return failure

	/** Property NewItems A map indicating the new repository items that are being
	 * added for the import */
	private Map<String, List<String>>	mNewItems	= new HashMap<>();

	public Map<String, List<String>> getNewItems() {
		return mNewItems;
	}

	public void setNewItems( Map<String, List<String>> pNewItems ) {
		mNewItems = pNewItems;
	}

	/** isNewWorkToDo - see if there are any importable items". If so, set change the audit record to
	 * reflect the importDate.
	 * 
	 * @param pFeedRepository
	 *            the repository containing the items to be imported
	 * @param pImportType
	 *            the type of import (autodeploy or manual are two examples)
	 * @return true if items need to be imported, false otherwise
	 * @throws CatalogImportException */
	public boolean isNewWorkToDo( List<Repository> pSourceRepositories, String pImportType ) throws CatalogImportException {

		// TAA
		if( isLoggingDebug() ) {
			logDebug( "-------> DigitalCatalogImportService: isNewWorkToDo()" );
		}

		// make sure importItemDescriptorPropertyMap is initialized
		switch( getInitializedImportPropertyMap() ) {
		case PROPERTYMAPUNINITIALIZED:
			initializeImportItemDescriptorPropertyMap( pSourceRepositories );
			break;
		case PROPERTYMAPINITIALIZEERROR:
			return false;
		case PROPERTYMAPINITIALIZESUCCESS:
			break;
		}

		boolean ret = false;
		RepositoryItem importRecord = null;
		// set up date parameter that indicates which items have been modified since
		// the last import
		try {
			importRecord = getImportRecord( pImportType );

			if( importRecord != null ) {
				ret = isNewImportableItem();
				// set up the param array for the rql queries
			}
			if( importRecord != null && ret == false ) {
				CatalogImportException ex = new CatalogImportException( "An Import Audit record was set up but there were no items to import " );
				Timestamp importDate = (Timestamp)importRecord.getPropertyValue( IMPORT_DATE_PROPERTY );
				writeAudit( ret, ex, importRecord.getRepositoryId(), importDate );
			}
		} catch( RepositoryException e ) {
			throw new CatalogImportException( e );
		}

		if( isLoggingDebug() ) {
			if( ret )
				logDebug( "New work to do." );
			else
				logDebug( "No new work to do." );
		}

		return ret;
	}
	
	/** isNewUpdateToDo - see if there are any importable items". If so, set change the audit record to
	 * reflect the importDate.
	 * 
	 * @param pFeedRepository
	 *            the repository containing the items to be imported
	 * @param pImportType
	 *            the type of import (autodeploy or manual are two examples)
	 * @return true if items need to be imported, false otherwise
	 * @throws CatalogImportException */
	public boolean isNewUpdateToDo( List<Repository> pSourceRepositories, String pImportType, String pJobType ) throws CatalogImportException {

		// TAA
		if( isLoggingDebug() ) {
			logDebug( "-------> DigitalCatalogImportService: isNewWorkToDo()" );
		}

		// make sure importItemDescriptorPropertyMap is initialized
		switch( getInitializedImportPropertyMap() ) {
		case PROPERTYMAPUNINITIALIZED:
			initializeImportItemDescriptorPropertyMap( pSourceRepositories );
			break;
		case PROPERTYMAPINITIALIZEERROR:
			return false;
		case PROPERTYMAPINITIALIZESUCCESS:
			break;
		}

		boolean ret = false;
		RepositoryItem importRecord = null;
		// set up date parameter that indicates which items have been modified since
		// the last import
		try {
			importRecord = getUpdateRecord(pImportType, pJobType);

			if( importRecord != null ) {
				ret = isNewUpdatableItem();
				// set up the param array for the rql queries
			}
			if( importRecord != null && ret == false ) {
				CatalogImportException ex = new CatalogImportException( "An Import Audit record was set up but there were no items to import " );
				Timestamp importDate = (Timestamp)importRecord.getPropertyValue( IMPORT_DATE_PROPERTY );
				//writeAudit( ret, ex, importRecord.getRepositoryId(), importDate );
			}
		} catch( RepositoryException e ) {
			throw new CatalogImportException( e );
		}

		if( isLoggingDebug() ) {
			if( ret )
				logDebug( "New work to do." );
			else
				logDebug( "No new work to do." );
		}

		return ret;
	}

	/** isNewImportableItem - check and see if there are any items to import
	 * 
	 * @param pFeedRepository
	 * @return - true if there is something to import, false otherwise */
	private boolean isNewImportableItem() {

		String[] params = {};

		for( RepositoryItemDescriptor itemDescriptor : importItemDescriptorPropertyMap.keySet() ) {
			logDebug( "Checking for importable items for Item Descriptor " + itemDescriptor );
			Repository itemDescriptorRepository = itemDescriptor.getRepository();
			Set<String> propSet = importItemDescriptorPropertyMap.get( itemDescriptor );
			if( isLoggingDebug() ) {

				logDebug( "itemDescriptor is " + itemDescriptor.getItemDescriptorName() );
				if( itemDescriptorRepository == null ){
					logDebug( "isNewImportableItem: itemDescriptorRepository is NULL." );
				return false;
				}
				else{
					logDebug( "isNewImportableItem: itemDescriptorRepository is " + itemDescriptorRepository.getRepositoryName() );
				}
				if( propSet == null || propSet.isEmpty())
					logDebug( "isNewImportableItem: propSet is null or 0" );
				else
					logDebug( "isNewImportableItem: propSet is " + propSet.toString() );
			}

			if( propSet != null && !propSet.isEmpty()) {
				int count = 0;
				RepositoryView view = null;
				if(null!=itemDescriptorRepository){
					try {
						view = itemDescriptorRepository.getView( itemDescriptor );
						count = getNewItemRql().executeCountQuery( view, params );
					}catch( RepositoryException ex ) {
						logError( "Error while checking for Importable: " + ex.toString() );
						return false;
					}
				}
				if( isLoggingDebug() ) {
					logDebug( "Found " + count + " " + itemDescriptor.getItemDescriptorName() + " items to import." );
				}

				if( count > 0 ) { return true; // there is stuff to import
				}
			}
		}

		// Exhausted our item types. No importable items found
		return false;
	}
	
	/** isNewupdatableItem - check and see if there are any items to import
	 * 
	 * @param pFeedRepository
	 * @return - true if there is something to import, false otherwise */
	private boolean isNewUpdatableItem() {
		String[] params = {};

		for( RepositoryItemDescriptor itemDescriptor : importItemDescriptorPropertyMap.keySet() ) {
			logDebug( "Checking for importable items for Item Descriptor " + itemDescriptor );
			Repository itemDescriptorRepository = itemDescriptor.getRepository();
			Set<String> propSet = importItemDescriptorPropertyMap.get( itemDescriptor );
			if( isLoggingDebug() ) {
				vlogInfo("itemDescriptor is " + itemDescriptor.getItemDescriptorName());
				logDebug( "itemDescriptor is " + itemDescriptor.getItemDescriptorName() );
				if( itemDescriptorRepository == null ){
					logDebug( "isNewImportableItem: itemDescriptorRepository is NULL." );
				return false;
				}
				else{
					logDebug( "isNewImportableItem: itemDescriptorRepository is " + itemDescriptorRepository.getRepositoryName() );
				}
				if( propSet == null || propSet.isEmpty())
					logDebug( "isNewImportableItem: propSet is null or 0" );
				else
					logDebug( "isNewImportableItem: propSet is " + propSet.toString() );
			}

			if( propSet != null && !propSet.isEmpty()) {
				int count = 0;
				RepositoryView view = null;
				if(null!=itemDescriptorRepository){
					try {
						vlogInfo("executing rql query for updatable items : "+getUpdatedItemRql().toString());
						view = itemDescriptorRepository.getView( itemDescriptor );
						count = getUpdatedItemRql().executeCountQuery( view, params );
					}catch( RepositoryException ex ) {
						logError( "Error while checking for Importable: " + ex.toString() );
						return false;
					}
				}
				vlogInfo( "Found " + count + " " + itemDescriptor.getItemDescriptorName() + " items to import." );

				if( count > 0 ) { 
					vlogInfo("found count of updatable items: "+count);
;					return true; // there is stuff to import
				}
			}
		}
		
		return false;
	}

	/** This method retrieves the import record from the importAudit view. The
	 * record will contain the last modified date, which is the key that will be
	 * used to retrieve the importable records from the feed repository
	 * 
	 * @param pFeedRepository
	 * @param pImportType
	 * @return
	 * @throws RepositoryException
	 * @throws CatalogImportException */
	private RepositoryItem getImportRecord( String pImportType ) throws RepositoryException, CatalogImportException {
		RepositoryView auditView;

		Object[] params = new Object[1];
		params[0] = pImportType;

		if( getAuditRepository() == null ) throw new CatalogImportException( "Audit Repository is NULL. Cannot proceed." );

		auditView = getAuditRepository().getView( "importAudit" );
		if( isLoggingDebug() ) {
			logDebug( "getImportRecord: pfeedRepository name is " + getAuditRepository().getRepositoryName() );
			if( auditView == null )
				logDebug( "getImportRecord: auditView is null" );
			else
				logDebug( "getImportRecord: auditView is not null" );
		}

		RepositoryItem importRecord = getImportAudit( auditView, mAuditRql, params );
		if( null == importRecord ) {
			if( isLoggingDebug() ) {
				logDebug( "getImportRecord: Import was not able to be run because no valid audit record was found." );
			}
		}
		return importRecord;
	}
	
	/** This method retrieves the import record from the importAudit view. The
	 * record will contain the last modified date, which is the key that will be
	 * used to retrieve the importable records from the feed repository
	 * 
	 * @param pFeedRepository
	 * @param pImportType
	 * @return
	 * @throws RepositoryException
	 * @throws CatalogImportException */
	private RepositoryItem getUpdateRecord( String pImportType, String pJobType ) throws RepositoryException, CatalogImportException {
		RepositoryView auditView;

		Object[] params = new Object[2];
		params[0] = pImportType;
		params[1] = pJobType;

		if( getAuditRepository() == null ) throw new CatalogImportException( "Audit Repository is NULL. Cannot proceed." );

		auditView = getAuditRepository().getView( "importAudit" );
		vlogInfo("getImportRecord: pfeedRepository name is " + getAuditRepository().getRepositoryName());
		if( isLoggingDebug() ) {
			logDebug( "getImportRecord: pfeedRepository name is " + getAuditRepository().getRepositoryName() );
			if( auditView == null )
				logDebug( "getImportRecord: auditView is null" );
			else
				logDebug( "getImportRecord: auditView is not null" );
		}

		RepositoryItem importRecord = getImportAudit( auditView, mAuditUpdateRql, params );
		if( null == importRecord ) {
			if( isLoggingDebug() ) {
				logDebug( "getImportRecord: Import was not able to be run because no valid audit record was found." );
			}
		}
		return importRecord;
	}

	/** This method adds a new item to the map that keeps track of all new items
	 * being added for the feed
	 * 
	 * This is to get around the fact that
	 * 
	 * @param pItemType
	 * @param pItem */
	private void addNewItem( String pItemType, RepositoryItem pItem ) {
		// if there are no new items yet, then initialize the list
		if( getNewItems() == null ) {
			setNewItems( new HashMap<String, List<String>>() );
		}

		// lists are stored by item type
		List<String> newItemList = (List<String>)getNewItems().get( pItemType );

		if( newItemList == null ) {
			newItemList = new ArrayList<>();
		}

		newItemList.add( pItem.getRepositoryId() );

		getNewItems().put( pItemType, newItemList );
	}

	/** This method determines if the item being added is a new item
	 * 
	 * @param pItemType
	 * @param pRepositoryId
	 * @return */
	public boolean isItemNew( String pItemType, String pRepositoryId ) {
		boolean isNew = false;

		if( getNewItems() != null ) {
			List<String> newItemList = (List<String>)getNewItems().get( pItemType );

			if( newItemList != null  && newItemList.contains( pRepositoryId )) {
				isNew = true;
			}
		}
		return isNew;
	}

	/** return the requested items for import based on the supplied view and query
	 * 
	 * @param pSkuView
	 * @param pSkuQuery
	 * @param pStartingIndex
	 * @param pHowmany
	 * @return
	 * @throws RepositoryException */
	public RepositoryItem[] getItemsToImport( RepositoryView pView, Query pQuery, int pStartingIndex, int pHowmany ) throws RepositoryException {

		return pView.executeQuery( pQuery, pStartingIndex, pStartingIndex + pHowmany );
	}

	public RepositoryItem[] getItemsFromSource( RepositoryView pView, RqlStatement pQuery, Object[] pParams, int pStartingIndex, int pHowmany ) throws RepositoryException {

		if( isLoggingDebug() ) {
			logDebug( "getItemsFromSource: pQuery is " + pQuery.toString() );
		}
		return pQuery.executeQuery( pView, pParams );
	}

	/** Gets the name of the itemType
	 * 
	 * @param pItemDescriptor
	 * @return */
	public void debugItem( String pIndent, RepositoryItem pItem ) {
		String thisIndent = pIndent + "|";
		String nextIndent = pIndent + "   ";
		if( null != pItem ) {
			try {
				if( isLoggingDebug() ) {
					logDebug( "" + thisIndent + "==========================" + pItem.getItemDescriptor().getItemDescriptorName() + "item:" + pItem.getRepositoryId() + "========================================" );
				}
			} catch( RepositoryException e1 ) {
				logError("RepositoryException: ", e1);
			}
			try {
				DynamicPropertyDescriptor[] props = pItem.getItemDescriptor().getPropertyDescriptors();

				for( int i = 0; i < props.length; i++ ) {
					String propName = props[i].getName();
					Object value = pItem.getPropertyValue( props[i].getName() );
					if( isLoggingDebug() ) {
						logDebug( "" + thisIndent + propName + ":=" + value );
					}
					if( value instanceof RepositoryItem ) {
						debugItem( nextIndent, (RepositoryItem)value );
					}
				}
			} catch( RepositoryException e ) {
				logError("RepositoryException: ", e);
			}
			if( isLoggingDebug() ) {
				logDebug( "" + thisIndent + "- - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - -" );
			}
		}

	}

	public void debugItem( RepositoryItem pItem ) {
		debugItem( "", pItem );
	}

	public static void debugExclusionMap( Map pExclusionMap ) {

		sLogger.logDebug( "item - property exclusions:" );

		if( pExclusionMap.keySet().iterator().hasNext() ) {

			Iterator iter = pExclusionMap.keySet().iterator();
			while( iter.hasNext() ) {
				String key = (String)iter.next();
				sLogger.logDebug( "exclusions for item type: " + key );
				sLogger.logDebug( "===============================" );
				Collection exclusions = (Collection)pExclusionMap.get( key );
				debugCollection( exclusions );
			}

		} else {
			sLogger.logDebug( "is empty" );
		}
	}

	private static void debugCollection( Collection exclusions ) {
		Iterator keyIter = exclusions.iterator();
		while( keyIter.hasNext() ) {
			String newkey = (String)keyIter.next();
			sLogger.logDebug( "excluding property:" + newkey );
		}
	}

	/** perform operations on startup */
	@Override
	public void doStartService() throws ServiceException {
		super.doStartService();

		try {

			// parse audit RQL
			mAuditRql = RqlStatement.parseRqlStatement( AUDIT_RQL );
			mAuditUpdateRql = RqlStatement.parseRqlStatement( AUDIT_UPDATE_RQL );
			mNewItemRql = RqlStatement.parseRqlStatement( getNewItemRqlString() );
			mUpdatedItemRql = RqlStatement.parseRqlStatement( getUpdatedItemRqlString() );
		} catch( RepositoryException e ) {
			throw new ServiceException( e );
		}
		if( isLoggingInfo() ) {
			logInfo( "!!!!!!!!!!!!!!!!!!!!!!! starting catalog Import service. . . . . . " );
		}

		// set up transforms
		if( isLoggingDebug() ) {
			logDebug( "adding sku, product and category transforms " );
		}

	}

	/** Retrieves the import record
	 * 
	 * @param pRepository
	 *            the repository that contains the audit records
	 * @param pAuditView
	 *            the view that will return the records
	 * @param pAuditRql
	 *            the rql that will return the records
	 * @param pParams
	 *            the object array used with the RQL
	 * @return
	 * @throws CatalogImportException */
	public RepositoryItem getImportAudit(RepositoryView pAuditView, RqlStatement pAuditRql, Object[] pParams ) throws CatalogImportException {

		try {
			if( isLoggingDebug() ) {
				logDebug( "Executing the query: " + pAuditRql.toString() );
				if( pAuditView == null )
					logDebug( "auditView is null" );
				else
					logDebug( "auditView is not null" );

				// TSC - add index 0 to pParams
				logDebug( "With params: " + pParams[0].toString() );
			}
			RepositoryItem[] auditRecs = pAuditRql.executeQuery( pAuditView, pParams );
			if( auditRecs == null || auditRecs.length == 0 ) { return null; }
			return auditRecs[0];
		} catch( RepositoryException e ) {
			// can't retrieve audit record
			throw new CatalogImportException( e, "Get Import Audit", ERROR_REPOSITORY_EXCEPTION );
		}
	}

	/** log the exception in a specified format: CATALOG IMPORT EXCEPTION occurred
	 * on: {0} during step: {1}. Failure reason= {2}. Source Exception= {3}.
	 * 
	 * @param pException */
	public void handleException( CatalogImportException pException ) {
		String importStep = pException.getImportStep();
		String failureReason = pException.getFailureReason();
		String sourceException = ( pException.getSourceException() == null ) ? "none" : pException.getSourceException().toString();
		Object[] arguments = { DateUtils.formatDate(), importStep, failureReason, sourceException };
		String result = MessageFormat.format( IMPORT_EXCEPTION_STRING, arguments );
		if( isLoggingError() ) {
			logError( result, pException);
		}
	}

	/** write audit record to the import repository.
	 * 
	 * @param pImportRepository
	 * @param pSuccess
	 * @param pException
	 * @return */
	public boolean writeAudit( boolean pSuccess, CatalogImportException pException, String pImportRecordId, Timestamp pImportDate ) {

		if( getAuditRepository() == null ) {
			logError( "Audit Repository is Null. Cannot write Audit record." );
			return false;
		}

		TransactionManager tm = ( (GSARepository)getAuditRepository() ).getTransactionManager();
		String description = ( pSuccess == true ) ? AUDIT_IMPORT_SUCCESS + " " + new CurrentDate().getTimeAsTimestamp() : pException.getDescription();
		String status = ( pSuccess == true ) ? AUDIT_IMPORT_SUCCESS : AUDIT_IMPORT_FAILURE;

		if( isLoggingDebug() ) {
			logDebug( "writing audit record with status:" + status + ", import date:" + pImportDate.toString() + ", description:" + description );
			if( pSuccess != true ) {
				logDebug( "exception description:" + pException.getDescription() );
			}
		}

		TransactionDemarcation td = new TransactionDemarcation();
		try {
			// start a new transaction
			td.begin( tm, TransactionDemarcation.REQUIRES_NEW );
			MutableRepositoryItem auditRec = ( (MutableRepository)getAuditRepository() ).getItemForUpdate( pImportRecordId, "importAudit" );
			auditRec.setPropertyValue( "status", status );
			auditRec.setPropertyValue( "description", description );
			auditRec.setPropertyValue( "jobType", "productCatalogImport" );
			( (MutableRepository)getAuditRepository() ).updateItem( auditRec );
		} catch( TransactionDemarcationException e ) {
			logError("TransactionDemarcationException: ", e);
		} catch( Exception e ) {
			// log error
			if( isLoggingError() ) {
				logError( ERROR_WRITING_AUDIT_FAILURE );
				logError( "exception:" + e );
			}
			mException = new CatalogImportException( e, "Write Audit", e.getMessage() );
			handleException( mException );
			return false;
		} finally {
			// commit the transaction
			try {
				td.end();
			} catch( TransactionDemarcationException e ) {
				logError("TransactionDemarcationException: ", e);
			}
		}
		return true;
	}

	public boolean validateRepositories( Repository pFeedRepository, Repository pImportRepository ) {
		boolean isValid = true;
		GSARepository exprep = (GSARepository)pImportRepository;
		GSARepository imprep = (GSARepository)pFeedRepository;
		MonitoredDataSource ds = (MonitoredDataSource)imprep.getDataSource();
		FakeXADataSource xads = (FakeXADataSource)ds.getDataSource();
		if( isLoggingDebug() ) {
			logDebug( "validating " + pImportRepository.getRepositoryName() + " of class:" + pImportRepository.getClass() + " database:" + exprep.getDatabaseName() );
			logDebug( "toString:" + exprep.toString() );

			logDebug( "validating " + pFeedRepository.getRepositoryName() + " of class:" + pFeedRepository.getClass() + " database:" + imprep.getDatabaseName() );
			logDebug( "data source:" + xads + " url:" + xads.getURL() + " user:" + xads.getUser() + " password:" + xads.getPassword() );
		}

		return isValid;
	}

	/* ============New routines for extensible import of new items (TAA)=================== */

	/** doNewImport - import new items into a CA project
	 * 
	 * This is the method that should be called to run the catalog import. It is
	 * assumed the caller sets up the context in which this import will be run.
	 * Category items, skus, products, and all their dependent objects will be
	 * imported into the supplied import repository This method clones the objects
	 * to be imported and copies the configured properties. A List property for
	 * each item type is configured which identifies which properties to copy and
	 * which properties to exclude from update. (e.g. getProdPropsToCopy and
	 * getExcludedProdPropsForUpdate). An example of why certain properties need
	 * to be excluded from update operations is: a product's ChildSkus might be
	 * changed by Merchandisers, so the import should not over-write this
	 * property. The childSKUs property of the product is excluded from copying
	 * for the update operation.
	 * 
	 * Any other failure will be considered critical and the import will fail. It
	 * is the job of the calling procedure to roll back when the the import fails.
	 * 
	 * @param pSrcRepository - repository importing from
	 * @param pDstRepository - repository importing to
	 * @param pImportType - manual (0) or autodeployment(1)
	 * @return true if import was successful, false otherwise */
	public boolean doNewImport( HashMap<Repository, MutableRepository> srcDstRepositoryMapping, String pImportType ) {

		boolean ret = true;

		if( isLoggingInfo() ) {
			logInfo( "Processing catalog import for New Items on:" + DateUtils.formatDate() );
		}

		if( srcDstRepositoryMapping == null || srcDstRepositoryMapping.size() <= 0 ) {
			ret = false;
			mException = new CatalogImportException( "Import was not able to be run because source and destination repositories were not supplied" );
			handleException( mException );
			return ret;
		}

		RepositoryItem importRecord = null;
		Timestamp importDate = null;
		try {
			importRecord = getImportRecord( pImportType );
			// set up the param array for the rql queries
			if( importRecord != null ) {
				importDate = (Timestamp)importRecord.getPropertyValue( IMPORT_DATE_PROPERTY );
				String importType = (String)importRecord.getPropertyValue( "importType" );
				PARAM_ARRAY[0] = importDate;

				if( isLoggingInfo() ) {
					logInfo( "Import date:" + importDate + " with type " + importType );
				}

				if( isLoggingInfo() ) logInfo( "Performing import of repositories" );

				importNewItems( srcDstRepositoryMapping );

			}
		} catch( CatalogImportException e ) {
			handleException( e );
			ret = false;
		} catch( Exception e2 ) {
			// there is a bug in ATG code such that it doesn't properly generate
			// RepositoryException,
			// e.g. MutableRepositoryItem.setPropertyValue causes a
			// java.lang.IllegalArgumentException
			// when the property is marked as non-writable, so we need to catch
			// Exception
			logError("Exception: ", e2);
			mException = new CatalogImportException( e2, "New Import Step", ERROR_REPOSITORY_EXCEPTION );
			handleException( mException );
			ret = false;
		} finally {
			if( mException == null ) {
				mException = new CatalogImportException( "UNKNOWN ERROR" );
			}

			// write audit record
			if( importRecord == null ) {
				if( isLoggingInfo() ) {
					logInfo( "There were no catalog imports to run on: " + DateUtils.formatDate() + " for type " + pImportType );
				}
			} else {
				// update audit recode after transaction is closed, save data that we will need
				setLastImportResult( ret );
				setLastImportAuditRecord( importRecord );
				setLastImportException( mException );

				if( isLoggingInfo() ) {
					logInfo( "catalog import completed on:" + DateUtils.formatDate() + " with type " + pImportType );
				}
			}
			setNewItems( null );
		}
		return ret;
	}

	/** doNewUpdate - update existing items into a CA project
	 * 
	 * This is the method that should be called to run the catalog update. It is
	 * assumed the caller sets up the context in which this import will be run.
	 * Category items, skus, products, and all their dependent objects will be
	 * imported into the supplied import repository This method updates the objects
	 * to be updated and copies the configured properties. A List property for
	 * each item type is configured which identifies which properties to copy and
	 * which properties to exclude from update. (e.g. getProdPropsToCopy and
	 * getExcludedProdPropsForUpdate). An example of why certain properties need
	 * to be excluded from update operations is: a product's ChildSkus might be
	 * changed by Merchandisers, so the import should not over-write this
	 * property. The childSKUs property of the product is excluded from copying
	 * for the update operation.
	 * 
	 * Any other failure will be considered critical and the import will fail. It
	 * is the job of the calling procedure to roll back when the the import fails.
	 * 
	 * @param pSrcRepository - repository importing from
	 * @param pDstRepository - repository importing to
	 * @param pImportType - manual (0) or autodeployment(1)
	 * @return true if import was successful, false otherwise */
	public boolean doNewUpdate( HashMap<Repository, MutableRepository> srcDstRepositoryMapping, String pImportType, String pJobType ) {

		boolean ret = true;

		if( isLoggingInfo() ) {
			logInfo( "Processing catalog import for New Items on:" + DateUtils.formatDate() );
		}

		if( srcDstRepositoryMapping == null || srcDstRepositoryMapping.size() <= 0 ) {
			ret = false;
			mException = new CatalogImportException( "Import was not able to be run because source and destination repositories were not supplied" );
			handleException( mException );
			return ret;
		}

		RepositoryItem importRecord = null;
		Timestamp importDate = null;
		try {
			importRecord = getUpdateRecord(pImportType, pJobType);
			// set up the param array for the rql queries
			if( importRecord != null ) {
				importDate = (Timestamp)importRecord.getPropertyValue( IMPORT_DATE_PROPERTY );
				String importType = (String)importRecord.getPropertyValue( "importType" );
				PARAM_ARRAY[0] = importDate;

				if( isLoggingInfo() ) {
					logInfo( "Update date:" + importDate + " with type " + importType );
				}

				if( isLoggingInfo() ) logInfo( "Performing import of repositories" );

				updatetems(srcDstRepositoryMapping);

			}
		} catch( CatalogImportException e ) {
			handleException( e );
			ret = false;
		} catch( Exception e2 ) {
			// there is a bug in ATG code such that it doesn't properly generate
			// RepositoryException,
			// e.g. MutableRepositoryItem.setPropertyValue causes a
			// java.lang.IllegalArgumentException
			// when the property is marked as non-writable, so we need to catch
			// Exception
			logError("Exception: ", e2);
			mException = new CatalogImportException( e2, "New Import Step", ERROR_REPOSITORY_EXCEPTION );
			handleException( mException );
			ret = false;
		} finally {
			if( mException == null ) {
				mException = new CatalogImportException( "UNKNOWN ERROR" );
			}

			// write audit record
			if( importRecord == null ) {
				if( isLoggingInfo() ) {
					logInfo( "There were no catalog imports to run on: " + DateUtils.formatDate() + " for type " + pImportType );
				}
			} else {
				// update audit recode after transaction is closed, save data that we will need
				setLastImportResult( ret );
				setLastImportAuditRecord( importRecord );
				setLastImportException( mException );

				if( isLoggingInfo() ) {
					logInfo( "catalog update completed on:" + DateUtils.formatDate() + " with type " + pImportType );
				}
			}
			setNewItems( null );
		}
		return ret;
	}
	
	/** importNewItems - imports new items. loops through the importItemDescriptorPropertyMap
	 * which contains an item descriptor to property set mapping, and calls
	 * importNewItemsFromItemType to do the work for each item descriptor.
	 * 
	 * @param pSrcRepository
	 * @param pDstRepository
	 * @throws DuplicateIdException
	 * @throws RepositoryException */
	public void importNewItems( HashMap<Repository, MutableRepository> srcDstRepositoryMapping ) throws DuplicateIdException, RepositoryException {

		if( isLoggingInfo() ) {
			logInfo( "================================== Importing new items" + " as per attributes in XML definition. ==================================" );
		}

		RepositoryItemDescriptor[] repItemDescArray = {};
		repItemDescArray = getImportItemDescriptorPropertyMap().keySet().toArray( repItemDescArray );

		// Using the "importorder" item descriptor attribute, sort the item descriptors into the order
		// in which we wish to import their new items
		Arrays.sort( repItemDescArray, new RepositoryItemDescriptorComparator() );
		if( isLoggingDebug() ) {
			for( int arind = 0; arind < repItemDescArray.length; arind++ ) {
				if( repItemDescArray[arind] != null )
					logDebug( "importNewItems: Repository Item Descriptor " + repItemDescArray[arind].getItemDescriptorName() );
				else
					logDebug( "importNewItems: Repository Item Descriptor at position " + arind + " is null" );
			}
		}

		for( int ridInd = 0; ridInd < repItemDescArray.length; ridInd++ ) {
			RepositoryItemDescriptor itemDescriptor = repItemDescArray[ridInd];
			logDebug( "Checking Item Descriptor: " + itemDescriptor );
			Set<String> propSet = importItemDescriptorPropertyMap.get( itemDescriptor );
			if( propSet == null || propSet.size() == 0 ) {
				logWarning( "Property set to copy for item Descriptor " + itemDescriptor + " is null or empty" );
				continue;
			}
			importNewItemsFromItemType( itemDescriptor.getRepository(), srcDstRepositoryMapping.get( itemDescriptor.getRepository() ), itemDescriptor.getItemDescriptorName(), propSet );
		}
	}
	
	/** updateNewItems - update items. loops through the importItemDescriptorPropertyMap
	 * which contains an item descriptor to property set mapping, and calls
	 * importNewItemsFromItemType to do the work for each item descriptor.
	 * 
	 * @param pSrcRepository
	 * @param pDstRepository
	 * @throws DuplicateIdException
	 * @throws RepositoryException */
	public void updatetems( HashMap<Repository, MutableRepository> srcDstRepositoryMapping ) throws RepositoryException {

		if( isLoggingInfo() ) {
			logInfo( "================================== Importing new items" + " as per attributes in XML definition. ==================================" );
		}

		RepositoryItemDescriptor[] repItemDescArray = {};
		repItemDescArray = getImportItemDescriptorPropertyMap().keySet().toArray( repItemDescArray );

		// Using the "importorder" item descriptor attribute, sort the item descriptors into the order
		// in which we wish to import their new items
		Arrays.sort( repItemDescArray, new RepositoryItemDescriptorComparator() );
		if( isLoggingInfo() ) {
			for( int arind = 0; arind < repItemDescArray.length; arind++ ) {
				if( repItemDescArray[arind] != null )
					logInfo( "updateItems: Repository Item Descriptor " + repItemDescArray[arind].getItemDescriptorName() );
				else
					logInfo( "updateItems: Repository Item Descriptor at position " + arind + " is null" );
			}
		}
		
		for( int ridInd = 0; ridInd < repItemDescArray.length; ridInd++ ) {
			RepositoryItemDescriptor itemDescriptor = repItemDescArray[ridInd];
			vlogInfo( "Checking Item Descriptor: " + itemDescriptor );
			Set<String> propSet = importItemDescriptorPropertyMap.get( itemDescriptor );
			if( propSet == null || propSet.size() == 0 ) {
				logWarning( "Property set to copy for item Descriptor " + itemDescriptor + " is null or empty" );
				continue;
			}
			updateItemsFromItemType( itemDescriptor.getRepository(), srcDstRepositoryMapping.get( itemDescriptor.getRepository() ), itemDescriptor.getItemDescriptorName(), propSet );
		}
	}
	

	/** importNewItemsFromItemType - import new items found in the item descriptor parameter of the
	 * source repository parameter to the destination repository
	 * (pDstRepository) copying pPropertySet properties from source
	 * to destination.
	 * 
	 * @param pSrcRepository - repository to import from
	 * @param pDstRepository - repository to import to
	 * @param pItemDescriptorName - item descriptor containing 0 or more items to import
	 * @param pPropertySet - properties to clone/copy
	 * @throws DuplicateIdException
	 * @throws RepositoryException
	 *             * */
	public void importNewItemsFromItemType( Repository pSrcRepository, MutableRepository pDstRepository, String pItemDescriptorName, Set<String> pPropertySet ) throws DuplicateIdException, RepositoryException, CatalogImportException {
		if( isLoggingInfo() ) {
			logInfo( "================================== Importing:" + pItemDescriptorName + " items as per attributes in XML definition. ==================================" );
		}

		// check imput parameters
		if( pSrcRepository == null ) throw new CatalogImportException( "Source repository parameter is null." );

		if( pDstRepository == null ) throw new CatalogImportException( "Destination repository parameter is null." );

		RepositoryView itemView = pSrcRepository.getView( pItemDescriptorName );
		if( isLoggingDebug() ) {
			logDebug( "importNewItemsFromItemType: importing items via query: " + IMPORT_NEW_RQL );
		}
		RepositoryItem items[];
		items = getItemsFromSource( itemView, IMPORT_RQL_STATEMENT, /* pParams */null, 0, -1 );

		if( isLoggingInfo() ) {
			if( items == null )
				logInfo( "Found no items to import from item descriptor " + pItemDescriptorName );
			else
				logInfo( "Found " + items.length + " items to import from item descriptor " + pItemDescriptorName );
		}

		// Compute Repository attribute for updating items that already exist
		String updateAttributeValue = (String)( ( pSrcRepository.getItemDescriptor( pItemDescriptorName ) ).getBeanDescriptor().getValue( ITEM_TYPE_IMPORT_USING_UPDATE ) );
		if( isLoggingDebug() ) {
			if( updateAttributeValue == null )
				logDebug( "importNewItemsFromItemType: Did not find " + ITEM_TYPE_IMPORT_USING_UPDATE + " for " + pItemDescriptorName );
			else
				logDebug( "importNewItemsFromItemType: " + ITEM_TYPE_IMPORT_USING_UPDATE + " = " + updateAttributeValue );
		}

		boolean updateFlag = getDefaultUpdateFlag();
		if( updateAttributeValue != null ) {
			if( updateAttributeValue.compareToIgnoreCase( TRUE_STRING ) == 0 )
				updateFlag = true;
			else
				updateFlag = false;
		}
		if( isLoggingDebug() ) logDebug( "importNewItemsFromItemType: updateFlag = " + ( updateFlag ? "true" : "false" ) );

		cloneNewItems( items, pDstRepository, pPropertySet, updateFlag );

		if( isLoggingInfo() ) {
			logInfo( "================================== import of" + pItemDescriptorName + " finished." );
		}
	}
	
	/** importNewItemsFromItemType - import new items found in the item descriptor parameter of the
	 * source repository parameter to the destination repository
	 * (pDstRepository) copying pPropertySet properties from source
	 * to destination.
	 * 
	 * @param pSrcRepository - repository to import from
	 * @param pDstRepository - repository to import to
	 * @param pItemDescriptorName - item descriptor containing 0 or more items to update
	 * @param pPropertySet - properties to update
	 * @throws RepositoryException
	 *             * */
	public void updateItemsFromItemType( Repository pSrcRepository, MutableRepository pDstRepository, String pItemDescriptorName, Set<String> pPropertySet ) throws DuplicateIdException, RepositoryException, CatalogImportException {
		if( isLoggingInfo() ) {
			logInfo( "================================== Importing:" + pItemDescriptorName + " items as per attributes in XML definition. ==================================" );
		}

		// check imput parameters
		if( pSrcRepository == null ) throw new CatalogImportException( "Source repository parameter is null." );

		if( pDstRepository == null ) throw new CatalogImportException( "Destination repository parameter is null." );

		RepositoryView itemView = pSrcRepository.getView( pItemDescriptorName );
		if( isLoggingInfo() ) {
			logInfo( "updateItemsFromItemType: importing items via query: " + UPDATED_ITEM_RQL );
		}
		RepositoryItem items[];
		items = getItemsFromSource( itemView, mUpdatedItemRql, /* pParams */null, 0, -1 );

		if( isLoggingInfo() ) {
			if( items == null )
				logInfo( "Found no items to import from item descriptor " + pItemDescriptorName );
			else
				logInfo( "Found " + items.length + " items to import from item descriptor " + pItemDescriptorName );
		}

		// Compute Repository attribute for updating items that already exist
		

		boolean updateFlag = getDefaultUpdateFlag();
		
		if( isLoggingInfo() ) logInfo( "importNewItemsFromItemType: updateFlag = " + ( updateFlag ? "true" : "false" ) );
		if(updateFlag) {
			cloneUpdatedItems( items, pDstRepository, pPropertySet, true );
		} else {
			vlogInfo("default update flag is false, not updating");
		}

		if( isLoggingInfo() ) {
			logInfo( "================================== import of" + pItemDescriptorName + " finished." );
		}
	}

	/** cloneNewItems - clone the repository items passed in to the destination repository by copying the
	 * properties specified in the parameter
	 * 
	 * @param pItems - repository items to clone into the destination repository
	 * @param pDstRepository - destination repository
	 * @param pPropertySet - set of properties to copy
	 * @throws RepositoryException
	 * @throws DuplicateIdException */
	public void cloneNewItems( RepositoryItem[] pItems, MutableRepository pDstRepository, Set<String> pPropertySet, boolean updateFlag ) throws RepositoryException, DuplicateIdException {

		if( null == pItems ) return; // nothing to do

		for( int i = 0; i < pItems.length; i++ ) {
			CloneInfo cloneInfo = new CloneInfo( pItems[i], pDstRepository );
			if( isLoggingDebug() ) {
				cloneInfo.setLoggingDebug( true );

				logDebug( "Original item type is " + pItems[i].getItemDescriptor().getItemDescriptorName() );
				logDebug( cloneInfo.toString() );

			}
			String itemType = cloneInfo.getItemType();

			if( isLoggingInfo() ) {
				logInfo( "Cloning item " + ( i + 1 ) + " of " + pItems.length + " with id:" + pItems[i].getRepositoryId() + "   itemtype:" + itemType );
			}
			if( isLoggingDebug() ) {
				logDebug( "Clone info for " + pItems[i].getRepositoryId() + " is " + cloneInfo.toString() );
			}

			MutableRepositoryItem exportItem = null;

			// New 5/20/2013 TAA - check to see if Item is present. in destination repository
			// If so and updateFlag is false, skip item (don't getItemForUpdate)
			RepositoryItem existingItem = pDstRepository.getItem( pItems[i].getRepositoryId(), itemType );
			if( !updateFlag && existingItem != null ) {
				if( isLoggingDebug() ) logDebug( "CloneNewItems: updateFlag is false and item already exists  in " + pDstRepository.getRepositoryName() + ". Ignoring item." + itemType + " : " + pItems[i].getRepositoryId() );
				continue; // skip this item
			}

			cloneInfo = RepositoryUtils.getItemForExport( cloneInfo );  // should really be called "getItemForClone"

			exportItem = cloneInfo.getClone();
			if( isLoggingDebug() ) {
				logDebug( "CloneNewItems: export Item(" + cloneInfo.getItemType() + "):" + cloneInfo.getClone() + ":" + pItems[i].getRepositoryId() );
				// + "is update?" + isUpdate);
			}

			if( pPropertySet == null || pPropertySet.isEmpty() ) { throw new RepositoryException( "missing configuration for properties to copy for:" + cloneInfo.getItemType() ); }

			if( isLoggingDebug() ) {
				logDebug( "CloneNewItems: keeping track of new item." );
			}

			addNewItem( itemType, cloneInfo.getSourceItem() );

			if( isLoggingDebug() ) {
				logDebug( "CloneNewItems: copying item properties" );

				for( Iterator<String> e = pPropertySet.iterator(); e.hasNext(); ) {
					String member = e.next();
					logDebug( "CloneNewItems: Property set Member: " + member );
				}
			}

			String[] tempString = {}; // initial size is zero. toArray(t) below will resize
			String[] propertySetArray = pPropertySet.toArray( tempString );
			if( isLoggingDebug() ) {
				for( int indx = 0; indx < propertySetArray.length; indx++ ) {
					String o = propertySetArray[indx];
					logDebug( "CloneNewItems: classname for " + o.toString() + " is " + o.getClass().getName() );
				}
			}

			RepositoryUtils.copyItemProperties( cloneInfo.getSourceItem(), cloneInfo.getClone(), propertySetArray, isLoggingDebug(), getImportItemDescriptorPropertyMap() );

			if( isLoggingDebug() ) {
				logDebug( "CloneNewItems: Doing addItem" );
			}

			// New 5/20/2013 TAA - check to see if Item is present. If so
			if( isLoggingDebug() ) logDebug( "CloneNewItems: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv" );
			if( updateFlag ) {
				if( isLoggingDebug() )
					logDebug( "CloneNewItems: updateFlag is true. checking " + pDstRepository.getRepositoryName() + " for " + exportItem.getRepositoryId() + " in Item Descriptor " + exportItem.getItemDescriptor().getItemDescriptorName() );
				if( existingItem != null ) {
					if( isLoggingDebug() ) {
						logDebug( "CloneNewItems: Item with id " + exportItem.getRepositoryId() + " in " + pDstRepository.getRepositoryName() + ":" + exportItem.getItemDescriptor() + "repository is an Existing Item. Doing update" );
						logDebug( "CloneNewItems: Item Found: " + existingItem.toString() );
					}
					pDstRepository.updateItem( exportItem );
				} else {
					if( isLoggingDebug() )
						logDebug( "CloneNewItems: updateFlag is true, but doing addItem since " + exportItem.getItemDescriptor().getItemDescriptorName() + " : " + exportItem.getRepositoryId() + " does not exist in "
								+ pDstRepository.getRepositoryName() );
					pDstRepository.addItem( exportItem );
				}
			} else {
				if( isLoggingDebug() ) logDebug( "CloneNewItems: updateFlag is false. Adding Item " + exportItem.getItemDescriptor().getItemDescriptorName() + " : " + exportItem.getRepositoryId() );
				pDstRepository.addItem( exportItem );
			}

			if( isLoggingDebug() ) {
				logDebug( "CloneNewItems: addItem or updateItem Done" );
				logDebug( "CloneNewItems: ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" );
			}
		}
	}
	
	/** cloneUpdatedItems - clone the repository items passed in to the destination repository by copying the
	 * properties specified in the parameter
	 * 
	 * @param pItems - repository items to clone into the destination repository
	 * @param pDstRepository - destination repository
	 * @param pPropertySet - set of properties to copy
	 * @throws RepositoryException
	 * @throws DuplicateIdException */
	public void cloneUpdatedItems( RepositoryItem[] pItems, MutableRepository pDstRepository, Set<String> pPropertySet, boolean updateFlag ) {

		if( null == pItems ) return; // nothing to do

		for( int i = 0; i < pItems.length; i++ ) {
			////CloneInfo cloneInfo = new CloneInfo( pItems[i], pDstRepository );
			/*if( isLoggingDebug() ) {
				cloneInfo.setLoggingDebug( true );

				logDebug( "Original item type is " + pItems[i].getItemDescriptor().getItemDescriptorName() );
				logDebug( cloneInfo.toString() );

			}*/
			String itemType = null;
			try {
				itemType = pItems[i].getItemDescriptor().getItemDescriptorName() == null ? null : pItems[i].getItemDescriptor().getItemDescriptorName();
			} catch (RepositoryException e2) {
				vlogError("updateItems: error getting repositoryitem type " + pDstRepository.getRepositoryName() + ". Ignoring item." + itemType + " : " + pItems[i].getRepositoryId()+e2);
				continue;
			}

			if( isLoggingInfo() ) {
				logInfo( "Cloning item " + ( i + 1 ) + " of " + pItems.length + " with id:" + pItems[i].getRepositoryId() + "   itemtype:" + itemType );
			}
			if( isLoggingDebug() ) {
				logDebug( "Cloning item " + ( i + 1 ) + " of " + pItems.length + " with id:" + pItems[i].getRepositoryId() + "   itemtype:" + itemType );
			}

			//MutableRepositoryItem exportItem = null;

			// New 5/20/2013 TAA - check to see if Item is present. in destination repository
			// If so and updateFlag is false, skip item (don't getItemForUpdate)
			MutableRepositoryItem existingItem;
			try {
				existingItem = pDstRepository.getItemForUpdate( pItems[i].getRepositoryId(), itemType );
			} catch (RepositoryException e1) {
				vlogError("updateItems: updateFlag is true and item does not exist  in " + pDstRepository.getRepositoryName() + ". Ignoring item." + itemType + " : " + pItems[i].getRepositoryId()+e1);
				continue;
			}
			

			//cloneInfo = RepositoryUtils.getItemForExport( cloneInfo );  // should really be called "getItemForClone"

			//exportItem = existingItem;
			if( isLoggingDebug() ) {
				logDebug( "updateItems: export Item(" + itemType + "):" + ":" + pItems[i].getRepositoryId() );
				// + "is update?" + isUpdate);
			}

			if( pPropertySet == null || pPropertySet.isEmpty() ) { 
				vlogError("updateItems: updateFlag is true and item does not have valid properties to update " + pDstRepository.getRepositoryName() + ". Ignoring item." + itemType + " : " + pItems[i].getRepositoryId());
				continue; 
				}

			//addNewItem( itemType, pItems[i] );

			if( isLoggingInfo() ) {
				logInfo( "UpdateItems: copying item properties" );

				for( Iterator<String> e = pPropertySet.iterator(); e.hasNext(); ) {
					String member = e.next();
					vlogInfo( "updateItems: Property set Member: " + member );
				}
			}

			String[] tempString = {}; // initial size is zero. toArray(t) below will resize
			String[] propertySetArray = pPropertySet.toArray( tempString );
			/*if( isLoggingDebug() ) {
				for( int indx = 0; indx < propertySetArray.length; indx++ ) {
					String o = propertySetArray[indx];
					logDebug( "CloneNewItems: classname for " + o + " is " + o.getClass().getName() );
				}
			}*/

			RepositoryUtils.copyItemProperties( pItems[i], existingItem, propertySetArray, isLoggingDebug(), getImportItemDescriptorPropertyMap() );
			try {
				if( existingItem != null ) {
					if( isLoggingDebug() ) {
						logDebug( "CloneNewItems: Item with id " + existingItem.getRepositoryId() + " in " + pDstRepository.getRepositoryName() + ":" + existingItem.getItemDescriptor() + "repository is an Existing Item. Doing update" );
						logDebug( "CloneNewItems: Item Found: " + existingItem.toString() );
					}
					
						pDstRepository.updateItem( existingItem );
					
				} 
			} catch (RepositoryException e) {
				vlogError("Error updating item");
			}
			if( isLoggingDebug() ) {
				logDebug( "CloneNewItems: addItem or updateItem Done" );
				logDebug( "CloneNewItems: ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" );
			}
		}
	}

	/* Initialization routines for getting import properties from the repository definition */
	/** initializeImportItemDescriptorPropertyMap - set the <item-type, property-set-to-copy> entries in
	 * importItemDescriptorPropertyMap
	 * 
	 * @param pSrcRepository - source repository to look for import attributes
	 * @return - true is successful, false if error */
	public boolean initializeImportItemDescriptorPropertyMap( List<Repository> pSourceRepositories ) {

		vlogInfo( "In initializeImportItemDescriptorPropertyMap " );

		for( int repoInd = 0; repoInd < pSourceRepositories.size(); repoInd++ ) {
			Repository srcRepository = (Repository)pSourceRepositories.get( repoInd );

			vlogInfo( "initializeImportItemDescriptorPropertyMap: working with repository " + srcRepository.getRepositoryName() );

			String[] itemDescNames = srcRepository.getItemDescriptorNames();

			// go through all item descriptors in the source repository
			for( int n = 0; n < itemDescNames.length; n++ ) {
				RepositoryItemDescriptor itemDesc = null;

				// if (isLoggingDebug())
				// logDebug("initializeImportItemDescriptorPropertyMap: working on item descriptor: " + itemDescNames[n]);

				try {
					itemDesc = srcRepository.getItemDescriptor( itemDescNames[n] );
				} catch( RepositoryException re ) {
					logError( "Can't initialize import properties from " + srcRepository.getRepositoryName() + ". Can't get Item Descriptor." );
					setInitializedImportPropertyMap( InitImportPropertyMapResultType.PROPERTYMAPINITIALIZEERROR );
					return false;
				}

				// get the item-descriptor-level import attribute
				String cisAttribute = (String)itemDesc.getBeanDescriptor().getValue( ITEM_TYPE_CIS_UPDATE_ATTRIBUTE );
				if( cisAttribute == null ) {
					vlogInfo( "No " + ITEM_TYPE_CIS_UPDATE_ATTRIBUTE + " attribute found for " + srcRepository.getRepositoryName() + ":" + itemDescNames[n] );
				} else {
					vlogInfo( "Found " + ITEM_TYPE_CIS_UPDATE_ATTRIBUTE +" : "+cisAttribute+ " attribute found for " + srcRepository.getRepositoryName() + ":" + itemDescNames[n] + " (" + itemDesc.getItemDescriptorName() + ")" );
					// check to see if value is valid cISImportItemTypeAttributeValidValues
					cisAttribute = cisAttribute.toLowerCase();
					if( cISImportItemTypeAttributeValidValues.contains( cisAttribute ) ) {
						// get properties designated by the attribute
						Set<String> propSet = repositoryPropertyMappingMethods.get( cisAttribute ).call( itemDesc );
						Map<String, Set<String>> includeExcludePropertySets = getCISIncludeAndExcludePropertiesFromItemDescriptor( itemDesc );
						Set<String> includeSet = includeExcludePropertySets.get( "includeProperties" );
						Set<String> excludeSet = includeExcludePropertySets.get( "excludeProperties" );
						propSet.addAll( includeSet );    // propSet = propSet Union includeSet
						propSet.removeAll( excludeSet ); // propSet = propSet difference excludeSet
						importItemDescriptorPropertyMap.put( itemDesc, propSet );

						if( isLoggingDebug() ) {
							logDebug( "initializeImportItemDescriptorPropertyMap: Added property set for " + itemDesc.getItemDescriptorName() + " to importItemDescriptorPropertyMap" );
						}
					} else if(cisAttribute.equalsIgnoreCase(ITEM_TYPE_CIS_UPDATE_ATTRIBUTE_VALUE_PERPROPERTYATTRIBUTE)) {
						vlogInfo( "Found " + ITEM_TYPE_CIS_UPDATE_ATTRIBUTE +" : "+cisAttribute+ " attribute found for " + srcRepository.getRepositoryName() + ":" + itemDescNames[n] + " (" + itemDesc.getItemDescriptorName() + ")" );
						Set<String> propSet = getPerPropertyUpdateAttributeProperties(itemDesc);
						importItemDescriptorPropertyMap.put( itemDesc, propSet );
					} else {
						logError( "Can't initialize update properties from " + srcRepository.getRepositoryName() + ":" + itemDescNames[n] + ". Value for Repository attribute " + ITEM_TYPE_CIS_UPDATE_ATTRIBUTE + " is invalid" );
						setInitializedImportPropertyMap( InitImportPropertyMapResultType.PROPERTYMAPINITIALIZEERROR );
						return false;
					}
				}

			}
		}
		setInitializedImportPropertyMap( InitImportPropertyMapResultType.PROPERTYMAPINITIALIZESUCCESS );
		return true;
	}

	/** getCISIncludeAndExcludePropertiesFromItemDescriptor - gets a map containing the include and exclude
	 * properties for the supplied item descriptor
	 * 
	 * @param itemDesc - the item descriptor to check for include and exclude properties.
	 * @return - a map containing entries for includeProperties and excludeProperties */
	public Map<String, Set<String>> getCISIncludeAndExcludePropertiesFromItemDescriptor( RepositoryItemDescriptor itemDesc ) {

		if( isLoggingDebug() ) logDebug( "in getCISIncludeAndExcludePropertiesFromItemDescriptor for " + itemDesc.getItemDescriptorName() );
		String[] propertyNames = itemDesc.getPropertyNames();
		Set<String> includeProperties = new HashSet<>();
		Set<String> excludeProperties = new HashSet<>();

		for( int pn = 0; pn < propertyNames.length; pn++ ) {
			DynamicPropertyDescriptor propertyDesc = itemDesc.getPropertyDescriptor( propertyNames[pn] );
			String attributeValue = (String)propertyDesc.getValue( PROPERTY_TYPE_COPY_NEW_ITEMS_VIA_CIS );
			if( attributeValue == null ) continue;
			if( PROPERTY_TYPE_COPY_NEW_ITEMS_VIA_CIS_INCLUDED.compareTo( attributeValue.toLowerCase() ) == 0 )
				includeProperties.add( propertyNames[pn] );
			else if( PROPERTY_TYPE_COPY_NEW_ITEMS_VIA_CIS_EXCLUDED.compareTo( attributeValue.toLowerCase() ) == 0 )
				excludeProperties.add( propertyNames[pn] );
			else
				logWarning( "Invalid Value for " + PROPERTY_TYPE_COPY_NEW_ITEMS_VIA_CIS_INCLUDED + " attribute for property " + propertyNames[pn] + " --> " + attributeValue.toLowerCase() + ". Attribute ignored." );
		}

		if( isLoggingDebug() ) {
			logDebug( "Found " + includeProperties.size() + " include properties in " + itemDesc.getItemDescriptorName() );
			logDebug( " These are " + includeProperties.toString() );
			logDebug( "Found " + excludeProperties.size() + " exclude properties in " + itemDesc.getItemDescriptorName() );
			logDebug( " These are " + excludeProperties.toString() );

		}

		Map<String, Set<String>> retVal = new HashMap<>();
		retVal.put( "includeProperties", includeProperties );
		retVal.put( "excludeProperties", excludeProperties );
		return retVal;
	}

	/** getRequiredProperties - get the properties designated as "required" for the supplied repository item descriptor
	 * 
	 * @param pSrcRepositoryItemDescriptor - repository item descriptor for which to get "required" properties.
	 * @return - the set of property names that are marked "required." */
	public static Set<String> getRequiredProperties( RepositoryItemDescriptor pSrcRepositoryItemDescriptor ) {
		if( pSrcRepositoryItemDescriptor == null ) {
			sLogger.logWarning( "getRequiredProperties (Item Descriptor null)" );
			return null;
		} else {
			sLogger.logDebug( "In getRequiredProperties for " + pSrcRepositoryItemDescriptor.getItemDescriptorName() );
		}

		String[] propertyNames = pSrcRepositoryItemDescriptor.getPropertyNames();
		Set<String> props = new HashSet<>();

		for( int pn = 0; pn < propertyNames.length; pn++ ) {
			DynamicPropertyDescriptor propertyDesc = pSrcRepositoryItemDescriptor.getPropertyDescriptor( propertyNames[pn] );
			if( propertyDesc.isRequired() ) props.add( propertyDesc.getName() );
		}

		return props;
	}

	/** getWritableProperties - return the properties designated as "writable" for the supplied repository item descriptor
	 * 
	 * @param pSrcRepositoryItemDescriptor - repository item descriptor for which to get "writable" properties.
	 * @return - the set of property names that are marked "writable." */
	public static Set<String> getWritableProperties( RepositoryItemDescriptor pSrcRepositoryItemDescriptor ) {
		if( pSrcRepositoryItemDescriptor == null ) {
			sLogger.logWarning( "getWritableProperties (Item Descriptor null)" );
			return null;
		} else {
			sLogger.logDebug( "In getWritableProperties for " + pSrcRepositoryItemDescriptor.getItemDescriptorName() );
		}

		String[] propertyNames = pSrcRepositoryItemDescriptor.getPropertyNames();
		Set<String> props = new HashSet<>();

		for( int pn = 0; pn < propertyNames.length; pn++ ) {
			DynamicPropertyDescriptor propertyDesc = pSrcRepositoryItemDescriptor.getPropertyDescriptor( propertyNames[pn] );
			if( propertyDesc.isWritable() ) props.add( propertyDesc.getName() );
		}

		return props;
	}

	/** getAllProperties - return all properties for the supplied repository item descriptor
	 * 
	 * @param pSrcRepositoryItemDescriptor - repository item descriptor for which to get all properties.
	 * @return - the set of property names that are marked "writable." */
	public static Set<String> getAllProperties( RepositoryItemDescriptor pSrcRepositoryItemDescriptor ) {
		if( pSrcRepositoryItemDescriptor == null ) {
			sLogger.logWarning( "getAllProperties (Item Descriptor null)" );
			return null;
		} else {
			sLogger.logDebug( "In getAllProperties for " + pSrcRepositoryItemDescriptor.getItemDescriptorName() );
		}

		String[] propertyNames = pSrcRepositoryItemDescriptor.getPropertyNames();
		Set<String> props = new HashSet<>();

		for( int pn = 0; pn < propertyNames.length; pn++ ) {
			props.add( propertyNames[pn] );
		}
		return props;
	}

	/** getPerPropertyAttributeProperties - return all properties initialized as perPropertyAttribute for the supplied
	 * repository item descriptor. Note, this is just a null set. All properties will
	 * be specified in property attributes themselves, not at the Item Type level.
	 * This is really just a place holder.
	 * 
	 * @param pSrcRepositoryItemDescriptor - repository item descriptor for which to get perPropertyAttribute properties.
	 * @return - the set of property names that are marked "writable." */
	public static Set<String> getPerPropertyAttributeProperties( RepositoryItemDescriptor pSrcRepositoryItemDescriptor ) {
		if( pSrcRepositoryItemDescriptor == null ) {
			sLogger.logWarning( "getPerPropertyAttributeProperties (Item Descriptor null)" );
			return null;
		} else {
			sLogger.logDebug( "In getPerPropertyAttributeProperties for " + pSrcRepositoryItemDescriptor.getItemDescriptorName() );
		}

		return new HashSet<>();
	}
	
	/** getPerPropertyAttributeProperties - return all properties initialized as perPropertyAttribute for the supplied
	 * repository item descriptor. Note, this is just a null set. All properties will
	 * be specified in property attributes themselves, not at the Item Type level.
	 * This is really just a place holder.
	 * 
	 * @param pSrcRepositoryItemDescriptor - repository item descriptor for which to get perPropertyAttribute properties.
	 * @return - the set of property names that are marked "writable." */
	public Set<String> getPerPropertyUpdateAttributeProperties( RepositoryItemDescriptor pSrcRepositoryItemDescriptor ) {
		if( pSrcRepositoryItemDescriptor == null ) {
			sLogger.logWarning( "getPerPropertyAttributeProperties (Item Descriptor null)" );
			return null;
		} else {
			sLogger.logDebug( "In getPerPropertyAttributeProperties for " + pSrcRepositoryItemDescriptor.getItemDescriptorName() );
		}

		String[] propertyNames = pSrcRepositoryItemDescriptor.getPropertyNames();
		Set<String> props = new HashSet<>();

		for( int pn = 0; pn < propertyNames.length; pn++ ) {
			DynamicPropertyDescriptor propertyDesc = pSrcRepositoryItemDescriptor.getPropertyDescriptor( propertyNames[pn] );
			String propertyUpdatableVIACIS = (String)propertyDesc.getValue(ITEM_PROPERTY_UPDATABLE_VIA_CIS);
			if( "Y".equalsIgnoreCase(propertyUpdatableVIACIS) && propertyDesc.isWritable()) {
				vlogInfo("property: " + propertyDesc.getName() + " found for update");
				props.add(propertyDesc.getName());
			}
		}

		return props;
	}

	// ---------------for debugging-------------------
	public void displayImportItemDescriptorPropertyMap() {
		if( isLoggingDebug() ) {
			for( RepositoryItemDescriptor itemDescriptor : importItemDescriptorPropertyMap.keySet() ) {
				logDebug( "Item Descriptor: " + itemDescriptor.getItemDescriptorName() );
				Set<String> propSet = importItemDescriptorPropertyMap.get( itemDescriptor );
				for( Iterator<String> e = propSet.iterator(); e.hasNext(); ) {
					String member = e.next();
					logDebug( "___Member: " + member );
				}
			}
		}
	}

}
