package com.digital.commerce.merch.deployment.file;

import atg.deployment.file.DeploymentConfiguration;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DigitalDeploymentConfiguration extends DeploymentConfiguration {

	private boolean enableFullDeployment;

}
