package com.digital.commerce.merch.assetmanager.editor.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.ResourceBundle;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.remote.assetmanager.editor.model.PropertyEditorAssetViewUpdate;
import atg.remote.assetmanager.editor.model.PropertyUpdate;
import atg.remote.assetmanager.editor.service.AssetEditorInfo;
import atg.remote.assetmanager.editor.service.CouponAssetServiceImpl;
import atg.repository.RepositoryItem;
import atg.service.asset.AssetWrapper;
import atg.servlet.ServletUtil;

/**
 * Logic to check the display date range between usage start and end date.
 * 
 * @author MS398446
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalCouponAssetServiceImpl extends CouponAssetServiceImpl {
	
	private static final String DISPLAY_START_DATE_PROPERTY = "displayStartDate";
	private static final String DISPLAY_END_DATE_PROPERTY = "displayEndDate";
	private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS",
			Locale.US);

	private static ResourceBundle mResourceBundle = LayeredResourceBundle
			.getBundle(
					"com.digital.commerce.common.catalog.DigitalPricingModelsTemplateResources",
					ServletUtil.getUserLocale());
	
	@Override
	public void preValidateNewAsset(AssetEditorInfo pEditorInfo,
			Collection pUpdates) {
		super.preValidateNewAsset(pEditorInfo, pUpdates);
		validateCouponID(pEditorInfo, pUpdates);
		validateDisplayDateProperties(pEditorInfo, pUpdates);
	}

	private void validateCouponID(AssetEditorInfo pEditorInfo,
			Collection pUpdates) {
		 AssetWrapper wrapper = pEditorInfo.getAssetWrapper();
	     RepositoryItem item = (RepositoryItem)wrapper.getAsset();
	     boolean isCouponCodeModified=false;
	     boolean hasLowercase=false;
	        String couponId=(String) item.getPropertyValue("id");
		        for(Iterator iter = pUpdates.iterator(); iter.hasNext();)
		        {
		            Object obj = iter.next();
		            if(obj instanceof PropertyEditorAssetViewUpdate)
		            {
		                Collection updates = ((PropertyEditorAssetViewUpdate)obj).getPropertyUpdates();
		                Iterator propIter = updates.iterator();
		                while(propIter.hasNext()) 
		                {
		                    PropertyUpdate update = (PropertyUpdate)propIter.next();
		                    if("$id".equals(update.getPropertyName()))
		                    {
		                    	isCouponCodeModified=true;
		                    	
		                        String id = (String)update.getPropertyValue();
		                		if(null!=id){
		                		hasLowercase = !id.equalsIgnoreCase(id);
		                		}
		                    }
		                }
		            }
		        }
		    
		    if(!isCouponCodeModified){
		    	hasLowercase = !couponId.equalsIgnoreCase(couponId);
		    }
		
    		if(hasLowercase){
    			String validationMessage = ResourceUtils
    					.getUserMsgResource(
    							"couponValidator.invalidCouponValue",
    							"com.digital.commerce.common.catalog.DigitalPricingModelsTemplateResources",
    							mResourceBundle);
    			pEditorInfo.getAssetService().addError("Coupon Code",validationMessage);
    		}
	}

	@Override
	public void preValidateUpdateAsset(AssetEditorInfo pEditorInfo,
			Collection pUpdates) {
		super.preValidateUpdateAsset(pEditorInfo, pUpdates);
		validateDisplayDateProperties(pEditorInfo, pUpdates);
	}

	private void validateDisplayDateProperties(AssetEditorInfo pEditorInfo,
			Collection<PropertyEditorAssetViewUpdate> pUpdates) {
		if (isLoggingDebug()) {
			logDebug("inside DigitalCouponAssetServiceImpl validateDisplayDateProperties");
		}

		RepositoryItem item = (RepositoryItem) pEditorInfo.getAssetWrapper()
				.getAsset();
		
		Date displayStartDate = (Date) item
				.getPropertyValue(DISPLAY_START_DATE_PROPERTY);
		Date displayEndDate = (Date) item.getPropertyValue(DISPLAY_END_DATE_PROPERTY);

		if (isLoggingDebug()) {
			logDebug("Before update**************");
			logDebug("displayStartDate -->" + displayStartDate);
			logDebug("displayEndDate -->" + displayEndDate);
		
		}

		for (PropertyEditorAssetViewUpdate viewUpdate : pUpdates) {
			for (PropertyUpdate propertyUpdate : viewUpdate
					.getPropertyUpdates()) {
				String propertyName = propertyUpdate.getPropertyName();
				if ((DISPLAY_START_DATE_PROPERTY.equalsIgnoreCase(propertyName))
						|| (DISPLAY_END_DATE_PROPERTY.equalsIgnoreCase(propertyName))) {
					Object propertyUpdateObj = propertyUpdate.getPropertyValue(); 
					if (isLoggingDebug()) {
						logDebug("propertyUpdate -->"
								+ propertyUpdateObj);
					}
					
					if (propertyUpdateObj != null) {
						Date parsedDate = null;
						String dateValue = (String) propertyUpdateObj;
						if (DigitalStringUtil.isNotEmpty(dateValue.trim())) {
							try {
								parsedDate = df.parse(dateValue);
							} catch (ParseException e) {
								logError("Unable to parse date");
							}
						}
						if (DISPLAY_START_DATE_PROPERTY.equalsIgnoreCase(propertyName)) {
							displayStartDate = parsedDate;
						}
						if (DISPLAY_END_DATE_PROPERTY.equalsIgnoreCase(propertyName)) {
							displayEndDate = parsedDate;
						}
					}
					
				}
			}
		}

		if (isLoggingDebug()) {
			logDebug("After DigitalCouponAssetServiceImpl update**************");
			logDebug("displayStartDate -->" + displayStartDate);
			logDebug("displayEndDate -->" + displayEndDate);		
		}

	
			if (displayStartDate == null && displayEndDate != null) {
				String validationMessage = ResourceUtils
						.getUserMsgResource(
								"displayDateValidator.invalidDisplayDateEntry",
								"com.digital.commerce.common.catalog.DigitalPricingModelsTemplateResources",
								mResourceBundle);
				pEditorInfo.getAssetService().addError(DISPLAY_START_DATE_PROPERTY,
						validationMessage);
			}
			if (displayStartDate != null && displayEndDate == null) {
				String validationMessage = ResourceUtils
						.getUserMsgResource(
								"displayDateValidator.invalidDisplayDateEntry",
								"com.digital.commerce.common.catalog.DigitalPricingModelsTemplateResources",
								mResourceBundle);
				pEditorInfo.getAssetService().addError(DISPLAY_END_DATE_PROPERTY,
						validationMessage);
			}

				
			if (displayStartDate!=null && displayEndDate!=null){
				if (displayStartDate.after(displayEndDate)) {
					String validationMessage = ResourceUtils
							.getUserMsgResource(
									"displayDateValidator.invalidDisplayDateRangeEntry",
									"com.digital.commerce.common.catalog.DigitalPricingModelsTemplateResources",
									mResourceBundle);
					pEditorInfo.getAssetService().addError(DISPLAY_END_DATE_PROPERTY,
							validationMessage);
				}
			}

	}
}