package com.digital.commerce.merch.integration.catimport.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import atg.core.util.DateDoodads;

/** extensions to ATG Date utilities
 * 
 * @author ashapiro */

public class DateUtils extends DateDoodads {
	// -------------------------------------
	// Constants
	// -------------------------------------

	private static String	DEFAULT_DATE_FORMAT	= "yyyy-MM-dd";

	/** Creates a date string for the current date according to the specified format
	 * 
	 * @param pFormat date according to the specified format
	 * @return the date string */
	public static String formatDate( String pFormat ) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat( pFormat, Locale.US );
		Calendar date = new GregorianCalendar();
		return dateFormatter.format( date.getTime() );
	}

	public static String formatDate() {
		return formatDate( DEFAULT_DATE_FORMAT );
	}

}
