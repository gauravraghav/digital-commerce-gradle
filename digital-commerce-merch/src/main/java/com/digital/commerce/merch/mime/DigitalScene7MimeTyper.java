package com.digital.commerce.merch.mime;

import atg.servlet.ExtensionMimeTyper;

/**
 * Created by ihartney on 12/18/2014.
 *
 * This class is needed by the BCC when using Scene7 Images. Scene7 images do not have filename extenstions, and so
 * the BCC in  certain places will not automatically load the images. This class extends the mimeTyper to look for
 * "scene7" in the URL, and set the mimeType to "image/jpeg"  this should be enough to trigger the BCC into displaying
 * the image... even if it isnt a jpeg.
 */

public class DigitalScene7MimeTyper extends ExtensionMimeTyper {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7678126705603629468L;

	@Override
    public String getMimeType(String pFileName) {

        if(pFileName.toLowerCase().contains("scene7")){
            return "image/jpeg";
        }else {
            return super.getMimeType(pFileName);
        }
    }
}
