package com.digital.commerce.merch.pricing.calculator;

import atg.commerce.pricing.BandedDiscountCalculatorHelper;
import atg.commerce.pricing.BulkOrderDiscountCalculator;

public class BulkOrderQtyDiscountCalculator extends BulkOrderDiscountCalculator{
	public String getCalculatorType(){
		return "bulkQty";
	}
	
	  protected BandedDiscountCalculatorHelper mBandedDiscountCalculatorHelper = new DigitalBandedOrderQtyDiscountCalculatorHelper();
	  
	  public BandedDiscountCalculatorHelper getBandedDiscountCalculatorHelper() {
	    return mBandedDiscountCalculatorHelper;
	  }
	  
	  public void setBandedDiscountCalculatorHelper(BandedDiscountCalculatorHelper pBandedDiscountCalculatorHelper) {
	    mBandedDiscountCalculatorHelper = pBandedDiscountCalculatorHelper;
	  }
}
