package com.digital.commerce.merch.integration.catimport;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.jms.JMSException;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;

import com.digital.commerce.merch.integration.catimport.util.SingletonScheduledService;

import atg.deployment.common.DeploymentException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.epub.PublishingException;
import atg.epub.project.CreateProject;
import atg.epub.project.Process;
import atg.epub.project.ProcessEnumStatus;
import atg.epub.project.ProcessHome;
import atg.epub.project.Project;
import atg.epub.project.ProjectConstants;
import atg.epub.project.ProjectException;
import atg.epub.project.ProjectHome;
import atg.nucleus.Nucleus;
import atg.nucleus.ServiceException;
import atg.repository.MutableRepository;
import atg.repository.Repository;
import atg.repository.RepositoryItemDescriptor;
import atg.security.Persona;
import atg.security.ThreadSecurityManager;
import atg.security.User;
import atg.service.scheduler.ScheduledJob;
import atg.userdirectory.UserDirectoryUserAuthority;
import atg.versionmanager.Branch;
import atg.versionmanager.VersionManager;
import atg.versionmanager.WorkingContext;
import atg.versionmanager.Workspace;
import atg.versionmanager.exceptions.VersionException;

public class UpdateCatalogInWorkspace extends SingletonScheduledService {

	// -------------------------------------
	// Constants
	// -------------------------------------
	// private static final int MAX_ACTIVE_PROJECTS = 999;
	private static final String	JOB_NAME				= "CATALOG Update";
	private static final String	JOB_DESCRIPTION			= "Update Catalog Attributes into catalog-import project";
	private static final String	IMPORT_DATE_PROPERTY	= "importDate";
	// -------------------------------------
	// Properties
	// -------------------------------------

	String						mImportType;

	public String getImportType() {
		return mImportType;
	}

	public void setImportType( String pImportType ) {
		mImportType = pImportType;
	}

	String						mJobType;

	public String getJobType() {
		return mJobType;
	}

	public void setJobType( String pJobType ) {
		mJobType = pJobType;
	}
	
	// ---------- Property: username ----------
	/** the username to use when importing data. The default is
	 * "importRepository". **/
	// String mUsername = "publishing";
	// TSC - should move this to config
	String	mUsername	= "admin";

	/** Set property <code>username</code>. This is
	 * the username to use when importing data. The default is "importRepository".
	 * 
	 * @param pUsername new value to set **/
	public void setUsername( String pUsername ) {
		mUsername = pUsername;
	}

	String	mProcessId;

	public void setProcessId( String pProcessId ) {
		mProcessId = pProcessId;
	}

	public String getProcessId() {
		return mProcessId;
	}

	/** Get property <code>username</code>. This is
	 * the username to use when importing data. The default is "importRepository".
	 * 
	 * @beaninfo description: the username to use when importing
	 *           data. The default is "importRepository".
	 * @return <code>username</code> **/
	public String getUsername() {
		return mUsername;
	}

	// -------------------------------------
	// property: DigitalCatalogImportService

	DigitalCatalogImportService	mCatImportService;

	public DigitalCatalogImportService getCatalogImportService() {
		return mCatImportService;
	}

	public void setCatalogImportService( DigitalCatalogImportService pCatImportService ) {
		mCatImportService = pCatImportService;
	}

	// -------------------------------------
	// property: transactionManager
	TransactionManager	mTransactionManager;

	/** Sets property transactionManager */
	public void setTransactionManager( TransactionManager pTransactionManager ) {
		mTransactionManager = pTransactionManager;
	}

	/** Returns property transactionManager */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	// -------------------------------------
	// property: Project
	Project	mProject;

	/** Sets property Project */
	public void setProject( Project pProject ) {
		mProject = pProject;
	}

	/** Gets property Project */
	public Project getProject() {
		return mProject;
	}

	// ---------- Property: versionManager ----------
	/** the version manager to use for imports. */
	VersionManager	mVersionManager	= null;

	/** Set property <code>versionManager</code>. This is the version manager to
	 * use for imports.
	 * 
	 * @param pVersionManager
	 *            new value to set */
	public void setVersionManager( VersionManager pVersionManager ) {
		mVersionManager = pVersionManager;
	}

	/** Get property <code>versionManager</code>. This is the version manager to
	 * use for imports.
	 * 
	 * @beaninfo description: the version manager to use for imports.
	 * @return <code>versionManager</code> */
	public VersionManager getVersionManager() {
		return mVersionManager;
	}

	// ---------- Property: branchName ----------
	/***************************************************************************** name of branch to use if creating a new workspace. The default is "main". ****************************************************************************/
	String	mBranchName	= "main";

	/** Set property <code>branchName</code>. This is the name of branch to use
	 * if creating a new workspace. The default is "main".
	 * 
	 * @param pBranchName
	 *            new value to set */
	public void setBranchName( String pBranchName ) {
		mBranchName = pBranchName;
	}

	/** Get property <code>branchName</code>. This is the name of branch to use
	 * if creating a new workspace. The default is "main".
	 * 
	 * @beaninfo description: name of branch to use if creating a new workspace.
	 *           The default is "main".
	 * @return <code>branchName</code> */
	public String getBranchName() {
		return mBranchName;
	}

	// ---------- Property: processName ----------
	/** the name of a process to create to use for import. A value of <code>null</code> means do the import without creating a process. **/
	String	mProcessName;

	/** Set property <code>processName</code>. This is the name of a process
	 * to use for import. A value of <code>null</code> means do the import
	 * without creating a process. The default is <code>null</code>.
	 * 
	 * @param pProcessName new value to set **/
	public void setProcessName( String pProcessName ) {
		mProcessName = pProcessName;
	}

	/** Get property <code>processName</code>. This is the name of a
	 * process to use for import. A value of <code>null</code> means
	 * do the import without creating a process. The default is <code>null</code>.
	 * 
	 * @beaninfo description: the name of a process to use for import. A value of <code>null</code> means do the import without creating a
	 *           process. The
	 *           default is <code>null</code>.
	 * @return <code>processName</code> **/
	public String getProcessName() {
		return mProcessName;
	}

	// ---------- Property: WorkspaceName ----------
	/***************************************************************************** name of process to use when creating a new workspace. ****************************************************************************/
	String	mWorkspaceName	= "catalogImport";

	/** Set property <code>WorkspaceName</code>. This is the name of process to use
	 * when creating a new workspace.
	 * 
	 * @param pBranchName
	 *            new value to set */
	public void setWorkspaceName( String pWorkspaceName ) {
		mWorkspaceName = pWorkspaceName;
	}

	/** Get property <code>WorkspaceName</code>. This is the name of process to use
	 * when creating a new workspace.
	 * 
	 * @beaninfo description: name of process to use when creating a new workspace.
	 * 
	 * @return <code>process</code> */
	public String getWorkspaceName() {
		return mWorkspaceName;
	}

	// -------------------------
	// property: userAuthority

	private UserDirectoryUserAuthority	mUserAuthority;

	/** Sets the UserAuthority to use */
	public void setUserAuthority( UserDirectoryUserAuthority pUserAuthority ) {
		mUserAuthority = pUserAuthority;
	}

	/** Returns UserAuthority to use */
	public UserDirectoryUserAuthority getUserAuthority() {
		return mUserAuthority;
	}

	/** Property ImportRepository */
	// MutableRepository mImportRepository;

	/** @return Returns the import repository. */
	/* public MutableRepository getImportRepository() {
	 * return mImportRepository;
	 * } */
	/** @param pImportRepository
	 *            The repository to set. */
	/* public void setImportRepository(MutableRepository pImportRepository) {
	 * mImportRepository = pImportRepository;
	 * } */
	List<String>	mSourceRepositoryNames;

	public List<String> getSourceRepositoryNames() {
		return mSourceRepositoryNames;
	}

	public void setSourceRepositoryNames( List<String> mSourceRepositoryNames ) {
		this.mSourceRepositoryNames = mSourceRepositoryNames;
	}

	List<String>	mDestinationRepositoryNames;

	public List<String> getDestinationRepositoryNames() {
		return mDestinationRepositoryNames;
	}

	public void setDestinationRepositoryNames( List<String> mDestinationRepositoryNames ) {
		this.mDestinationRepositoryNames = mDestinationRepositoryNames;
	}

	List<Repository>		sourceRepositories		= new ArrayList<>(2);
	List<MutableRepository>	destinationRepositories	= new ArrayList<>(2);

	public List<atg.repository.Repository> getSourceRepositories() {
		return sourceRepositories;
	}

	public void setSourceRepositories( List<atg.repository.Repository> sourceRepositories ) {
		this.sourceRepositories = sourceRepositories;
	}

	public List<atg.repository.MutableRepository> getDestinationRepositories() {
		return destinationRepositories;
	}

	public void setDestinationRepositories( List<atg.repository.MutableRepository> destinationRepositories ) {
		this.destinationRepositories = destinationRepositories;
	}

	// maps import source repositories to destination repositories
	private HashMap<Repository, MutableRepository>	srcDestRepositoryMapping	= new HashMap<>();

	public HashMap<Repository, MutableRepository> getSrcDestRepositoryMapping() {
		return srcDestRepositoryMapping;
	}

	public void setSrcDestRepositoryMapping( HashMap<Repository, MutableRepository> srcDestRepositoryMapping ) {
		this.srcDestRepositoryMapping = srcDestRepositoryMapping;
	}

	// -------------------------------------
	// property: ProjectName

	String	mProjectName;

	public String getProjectName() {
		return mProjectName;
	}

	public void setProjectName( String pProjectName ) {
		mProjectName = pProjectName;
	}

	// -------------------------------------
	// property: WorfklowName

	String	mWorkflowName	= "/Content Administration/import.wdl";

	public String getWorkflowName() {
		return mWorkflowName;
	}

	public void setWorkflowName( String pWorkflowName ) {
		mWorkflowName = pWorkflowName;
	}

	// -------------------------------------
	// Member variables

	/** workspace to use for import */
	Workspace	mWorkspace	= null;

	private Workspace getWorkspace() {
		return mWorkspace;
	}

	private void setWorkspace( Workspace pWorkspace ) {
		mWorkspace = pWorkspace;
	}

	/** flag specifying if process has been created */
	boolean								mProcessCreated			= false;

	private HashSet<ProcessEnumStatus>	mInactiveProcessStates	= new HashSet<>();

	private User						mCurrentUser			= null;

	public User getCurrentUser() {
		return mCurrentUser;
	}

	public void initRepositoryLists() {
		if( sourceRepositories.size() > 0 ) return;
		for( int i = 0; i < getSourceRepositoryNames().size(); i++ ) {
			String srcRepositoryName = (String)getSourceRepositoryNames().get( i );
			Repository srcRepository = (Repository)Nucleus.getGlobalNucleus().resolveName( srcRepositoryName );
			sourceRepositories.add( srcRepository );

			String dstRepositoryName = (String)getDestinationRepositoryNames().get( i );
			MutableRepository dstRepository = (MutableRepository)Nucleus.getGlobalNucleus().resolveName( dstRepositoryName );
			destinationRepositories.add( dstRepository );
			srcDestRepositoryMapping.put( srcRepository, dstRepository );
		}

	}

	/** Entry point for performing the update service task. It performs the following actions:
	 * 1. Checks to see if there is anything to do (isNewWorkToDo)
	 * 2. Sets CA import context */
	protected void performTask() {

		// TSC
		if( isLoggingDebug() ) {
			logDebug( "-------> ImportCatalogInWorkspace: performTask()" );
		}

		boolean workToDo = false;
		setProcessId( null );

		if( isLoggingInfo() ) {
			logInfo( getProjectName() + " performing " + getImportType() + " import on:" + getDateString() );
		}

		// initialize lists of repositories
		// initRepositoryLists();

		// see if there's anything to do
		try {
			workToDo = getCatalogImportService().isNewUpdateToDo( getSourceRepositories(), getImportType(), getJobType() );
		} catch( CatalogImportException e1 ) {
			logError("CatalogImportException:", e1);
		}

		if( workToDo ) {
			if( isLoggingInfo() ) logInfo( getProjectName() + " service found items to import for " + getImportType() + " import" );
		} else {
			if( isLoggingInfo() ) logInfo( getProjectName() + " service found no work to do for " + getImportType() + " import" );
			return;
		}

		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollback = false;
		boolean caContextSet = false;

		try {
			td.begin( getTransactionManager() );

			if( isLoggingDebug() ) logDebug( getProjectName() + " setting context for " + getImportType() + " import. . ." );

			caContextSet = setImportContext();

			if( caContextSet ) {

				vlogInfo( getProjectName() + " set context for " + getImportType() + " import successfully" );
				boolean successfulImport = true;

				// import repositories within this context

				successfulImport = getCatalogImportService().doNewUpdate( getSrcDestRepositoryMapping(), getImportType(), getJobType() );

				if( isLoggingInfo() ) logInfo( "Return from catalog " + getImportType() + " import result is " + successfulImport );

				if( successfulImport ) {
					rollback = false;
				} else {
					rollback = true;
				}
			}
		} catch( VersionException e ) {

			if( isLoggingError() ) {
				logError( "Unable to execute " + getImportType() + " import " + e );
			}
			rollback = true;

		} catch( TransactionDemarcationException e ) {

			if( isLoggingError() ) {
				logError( "Unable to execute " + getImportType() + " import - tde " + e );
			}
			logError("TransactionDemarcationException:", e);
			rollback = true;

		} finally {

			try {
				try {
					if( isLoggingDebug() && getProject() != null ) logDebug( "Project is " + getProject().getDisplayName() );
					if( getProject().getAssets() != null ) logDebug( "Number of assets in project = " + getProject().getAssets().size() );

					if( rollback ) {
						if( isLoggingDebug() ) logDebug( "Rollback true." );

						setProcessId( null );
						if( getTransactionManager().getStatus() != javax.transaction.Status.STATUS_MARKED_ROLLBACK ) {
							if( isLoggingError() ) logError( "Deleting project after failure." );
							if( getProject() != null ) getProject().delete( getUsername() );
						}
					}

					if( caContextSet ) {
						clearImportContext();
					}

				} catch( SystemException e ) {
					logError("SystemException:", e);
				}
			} catch( VersionException | JMSException | PublishingException | RemoveException | EJBException | TransactionDemarcationException | DeploymentException e ) {
				if( isLoggingError() ) logError( "Could not delete " + getImportType() + " import project " + e );
			}
      Object importDatePorperty = null;
			try {
				td.end();
				importDatePorperty = getCatalogImportService().getLastImportAuditRecord().getPropertyValue( IMPORT_DATE_PROPERTY );
				getCatalogImportService().writeAudit( getCatalogImportService().getLastImportResult(), getCatalogImportService().getLastImportException(), getCatalogImportService().getLastImportAuditRecord().getRepositoryId(),
						(Timestamp)importDatePorperty );
			} catch( TransactionDemarcationException e ) {
				if( isLoggingError() ) logError( e );
				getCatalogImportService().writeAudit( false, new CatalogImportException( e.toString() ), getCatalogImportService().getLastImportAuditRecord().getRepositoryId(),
						(Timestamp)importDatePorperty );
			}

		}

	}

	/** setImportContext sets up the CA project context to which the import will occur
	 * 
	 * @return true - if CA process successfully created
	 *         false - if not
	 * @throws VersionException
	 * 
	 *             This method performs the following actions
	 *             1. Assumes a user id for the process
	 *             2. Prepares the context **/
	private boolean setImportContext() throws VersionException {
		// the process name will contain the date
		String projectName = getProjectName();
		if( isLoggingDebug() ) {
			logDebug( projectName + " getting process home to look for existing import process. . ." );
		}
		boolean processCreated = false;

		try {
			assumeUserIdentity( getUsername() );
			processCreated = preparePublishingContext();
			if( processCreated ) {
				updateProcessId();
			}
		} catch( ProjectException e ) {
			logError( "ProjectException ", e);
		}
		return processCreated;

	}

	/** see if there is an active import process.
	 * An import process is considered actived if it's in any state other than deployed.
	 * 
	 * @param pProcsColl
	 * @return */
	/*private boolean findActiveImportProcess(Collection pProcsColl) {
	 * boolean found=false;
	 * if (pProcsColl==null)
	 * return false;
	 * Iterator procIter=pProcsColl.iterator();
	 * while (procIter.hasNext())
	 * {
	 * Process process=(Process)procIter.next();
	 * if (isLoggingDebug())
	 * {
	 * logDebug("found process with status:"+process.getStatus());
	 * logDebug("found process with name:"+process.getDisplayName());
	 * logDebug("activity:"+process.getActivity());
	 * logDebug("project name:"+process.getProject().getDisplayName());
	 * //logDebug("item id:"+process.getProcessData().getRepositoryId());
	 * }
	 * if (mInactiveProcessStates.contains(process.getStatus()))
	 * continue;
	 * else
	 * {
	 * found=true;
	 * if (isLoggingDebug())
	 * {
	 * logDebug("found active import project with process id::"+process.getId());
	 * }
	 * break;
	 * }
	 * }
	 * return found;
	 * } */

	private void clearImportContext() {
		WorkingContext.popDevelopmentLine();
		setProject( null );
	}

	private String getDateString() {
		DateFormat dateFormat = new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss" );
		Date date = new Date();
		return dateFormat.format( date );
	}

	/** Set up mInactiveProcessStates which will be used to test if any active import processes are running
	 * deployed or completed processes are considered inactive */
	public void doStartService() throws ServiceException {
		super.doStartService();

		// TSC
		if( isLoggingDebug() ) {
			logDebug( "-------> ImportCatalogInWorkspace: doStartService" );
		}

		if( isLoggingInfo() ) {
			logInfo( getProjectName() + " scheduled service starting . . ." );
		}

		initRepositoryLists();

		// set up list of inactive process states
		mInactiveProcessStates.add( ProcessEnumStatus.getDeployed() );
		mInactiveProcessStates.add( ProcessEnumStatus.getCompleted() );

		// Register ourselves as a scheduled service
		if( isLoggingInfo() ) {
			logInfo( "scheduling job: " + JOB_NAME );
		}

		ScheduledJob job = new ScheduledJob( JOB_NAME, JOB_DESCRIPTION, getAbsoluteName(), getSchedule(), this, ScheduledJob.SCHEDULER_THREAD );

		if( job != null ) mJobId = getScheduler().addScheduledJob( job );

		if( isLoggingInfo() ) {
			logInfo( getProjectName() + " scheduled service started successfully" );
		}

	}

	// Stop method
	public void doStopService() throws ServiceException {
		getScheduler().removeScheduledJob( mJobId );
	}

	// -------------------------------------
	/** Prepare the publishing context, creating a project as needed
	 * 
	 * @exception VersionException if there is version manager trouble
	 * @exception ProjectException if there is project trouble **/
	private boolean preparePublishingContext() throws VersionException, ProjectException {
		// if the user is creating a project, then create only once at the start
		if( getProcessName() != null ) {
			String processName = getProcessName() + " " + getDateString();
			// Previous version had the line below. with autodeploy as false
			// mWorkspaceName = CreateProject.createProcess(processName, getWorkflowName(), getUsername(), false);
			mWorkspaceName = CreateProject.createProcess( processName, getWorkflowName(), getUsername() );
			mProcessCreated = true;
			if( isLoggingDebug() ) logDebug( "preparePublishingContext() created project: " + getProcessName() );
		}

		// if the workspace name is null then no workspace is used
		if( mWorkspaceName == null ) {
			if( isLoggingDebug() ) logDebug( "preparePublishingContext() nothing to do" );
			mProcessCreated = false;
			return false;
		}

		// get the named workspace
		setWorkspace( mVersionManager.getWorkspaceByName( mWorkspaceName ) );

		// create workspace if needed
		if( getWorkspace() == null ) {
			// our workspace is based on the specified branch
			Branch branch = mVersionManager.getBranchByName( mBranchName );
			setWorkspace( mVersionManager.createWorkspace( mWorkspaceName, branch ) );

			if( isLoggingDebug() ) {
				logDebug( "preparePublishingContext() created: " + getWorkspace() );
			}
		} else {
			if( isLoggingDebug() ) logDebug( "preparePublishingContext() found: " + getWorkspace() );
		}

		// tell the version manager to use the workspace
		WorkingContext.pushDevelopmentLine( getWorkspace() );

		if( isLoggingDebug() ) logDebug( "preparePublishingContext() prepared: " + getWorkspace() );

		return true;
	}

	// -------------------------------------
	/** Assume identity of configured user. We will assume a valid user.
	 * This method should only be called by the scheduled service because it replaces the current user identity */
	private void assumeUserIdentity( String pUser ) throws ProjectException {
		if( pUser == null ) throw new ProjectException( "null user name" );

		if( mUserAuthority == null ) mUserAuthority = (UserDirectoryUserAuthority)Nucleus.getGlobalNucleus().resolveName( "/atg/dynamo/security/UserAuthority" );

		mCurrentUser = new User();
		Persona adminPersona = mUserAuthority.getPersona( "Profile$login$" + pUser );
		if( adminPersona == null ) throw new ProjectException( "unable to get personna for:" + pUser );

		// create a temporary User object for the admin identity
		mCurrentUser.addPersona( adminPersona );

		// replace the current User object
		ThreadSecurityManager.setThreadUser( mCurrentUser );
	}

	/** This method updates the mProject field with the current project
	 * once the process is created. */
	private void updateProcessId() {
		try {
			if( getWorkspace() == null ) {
				if( isLoggingInfo() ) {
					logInfo( "Unable to set process information because there is not a workspace" );
				}
			} else {
				// first get the project
				ProjectHome projectHomeHandle = ProjectConstants.getPersistentHomes().getProjectHome();
				Project currentProject = projectHomeHandle.findProjectByWorkspaceId( getWorkspace().getID() );
				setProject( currentProject );
				if( currentProject == null ) {
					if( isLoggingInfo() ) {
						logInfo( "Unable to set process information.  There is no project for workspace " + getWorkspace().getID() );
					}
					return;
				}
				// then use the project to get the corresponding process
				ProcessHome processHomeHandle = ProjectConstants.getPersistentHomes().getProcessHome();
				Process currentProcess = processHomeHandle.findProcessByProjectId( currentProject.getId() );

				if( isLoggingDebug() ) {
					logDebug( "Setting process information with " );
					logDebug( "Project id: " + currentProject.getId() );
					logDebug( "Workspace; " + getWorkspace().getID() );
				}
				if( currentProcess == null ) {
					if( isLoggingInfo() ) {
						logInfo( "Unable to set process information.  There is no process for project with id " + currentProject.getId() );
					}
					return;
				}
				setProcessId( currentProcess.getId() );
			}
		} catch( FinderException findEx ) {
			if( isLoggingError() ) {
				logError( "Unable to set process information.  Exception thrown during find: ", findEx );
			}
		} catch( VersionException verEx ) {
			if( isLoggingError() ) logError( "Unable to set process information.  Version exception: ", verEx );
		}
	}

	public void testPerformTask() {
		if( isLoggingDebug() ) {
			logDebug( "----- Starting testPeformTask() -----" );
		}
		DateFormat dateFormat = new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss" );
		Calendar cal = Calendar.getInstance();
		String dateTime = dateFormat.format( cal.getTime() );

		//Monitor mon = com.jamonapi.MonitorFactory.start( "CatalogImportService." + dateTime );
		performTask();
		//mon.stop();
		if( isLoggingDebug() ) {
			//logDebug( mon.toString() );
		}
		if( isLoggingWarning() ) {
			//logWarning( mon.toString() );
		}
	}

	public void testLog4j() {
		if( isLoggingError() ) {
			logError( "******************* Logging ERROR on *******************" );
		}
		if( isLoggingWarning() ) {
			logWarning( "******************* Logging WARNING on *******************" );
		}
		if( isLoggingDebug() ) {
			logDebug( "******************* Logging DEBUG on *******************" );
		}
		if( isLoggingInfo() ) {
			logInfo( "******************* Logging INFO on *******************" );
		}
	}

	// test entry points
	public void testInitRepositoryImports() {
		try {
			getCatalogImportService().initializeImportItemDescriptorPropertyMap( getSourceRepositories() );
			getCatalogImportService().displayImportItemDescriptorPropertyMap();
		} catch( Exception e ) {
			logError( "testInitRepositoryImports() threw and exception " + e );
		}
	}

	public void testChkForNewItems() {
		try {
			if( getCatalogImportService().isNewWorkToDo( getSourceRepositories(), getImportType() ) ) logDebug( "!!!!!!!!!!!!! testChkForNewItems: Found import work to do !!!!!!!!." );
		} catch( Exception ex ) {
			logError( "testChkForNewItems() threw an exception: " + ex );

		}
	}

	public void testGetImportAttributes() {
		String skuString = "sku";
		String productString = "product";
		RepositoryItemDescriptor itmdesc;
		try {

			Repository importRepository = (Repository)getNucleus().resolveName( "/atg/commerce/catalog/ProductCatalogFeed" );
			itmdesc = importRepository.getItemDescriptor( skuString );
			String attr = (String)itmdesc.getBeanDescriptor().getValue( "importorder" );
			if( isLoggingDebug() ) {
				logDebug( "sku item descriptor, importorder property = " + ( attr == null ? "Null" : attr ) );
			}

			attr = (String)itmdesc.getBeanDescriptor().getValue( "importNewItemsViaCIS" );
			if( isLoggingDebug() ) {
				logDebug( "sku item descriptor, importNewItemsViaCIS property = " + ( attr == null ? "Null" : attr ) );
			}

			itmdesc = importRepository.getItemDescriptor( productString );
			attr = (String)itmdesc.getBeanDescriptor().getValue( "importorder" );
			if( isLoggingDebug() ) {
				logDebug( "product item descriptor, importorder property = " + ( attr == null ? "Null" : attr ) );
			}

			attr = (String)itmdesc.getBeanDescriptor().getValue( "importNewItemsViaCIS" );
			if( isLoggingDebug() ) {
				logDebug( "product item descriptor, importNewItemsViaCIS property = " + ( attr == null ? "Null" : attr ) );
			}
		} catch( Exception ex ) {
			logError( ex.toString() );
		}

	}

	// check source and destination repositories
	public void testSourceAndDestRepositories() {
		if( isLoggingInfo() ) {
			for( int i = 0; i < mSourceRepositoryNames.size(); i++ ) {
				logInfo( "Source Repository: " + mSourceRepositoryNames.get( i ) );
				if( getNucleus().resolveName( mSourceRepositoryNames.get( i ), null, false ) == null )
					logInfo( "Error: Can't find " + mSourceRepositoryNames.get( i ) + " in Nucleus." );
				else
					logInfo( "Found " + mSourceRepositoryNames.get( i ) + " in Nucleus." );

			}
			for( int j = 0; j < mDestinationRepositoryNames.size(); j++ ) {
				logInfo( "Destination Repository: " + mDestinationRepositoryNames.get( j ) );
				if( getNucleus().resolveName( mDestinationRepositoryNames.get( j ), null, false ) == null )
					logInfo( "Error: Can't find " + mDestinationRepositoryNames.get( j ) + " in Nucleus." );
				else
					logInfo( "Found " + mDestinationRepositoryNames.get( j ) + " in Nucleus." );
			}
		}
	}

}
