package com.digital.commerce.merch.assetmanager.editor.service;

import java.util.Collection;
import java.util.Iterator;
import java.util.ResourceBundle;

import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.remote.assetmanager.editor.model.PropertyEditorAssetViewUpdate;
import atg.remote.assetmanager.editor.model.PropertyUpdate;
import atg.remote.assetmanager.editor.service.AssetEditorInfo;
import atg.remote.assetmanager.editor.service.CouponBatchAssetServiceImpl;
import atg.repository.RepositoryItem;
import atg.service.asset.AssetWrapper;
import atg.servlet.ServletUtil;

/**
 * Validate prefix for couponbatch.
 * 
 * @author NC419910
 */
@SuppressWarnings({"rawtypes"})
public class DigitalCouponBatchAssetServiceImpl extends CouponBatchAssetServiceImpl {

	private static ResourceBundle mResourceBundle = LayeredResourceBundle
			.getBundle(
					"com.digital.commerce.common.catalog.DigitalPricingModelsTemplateResources",
					ServletUtil.getUserLocale());

	public void preValidateNewAsset(AssetEditorInfo pEditorInfo,
			Collection pUpdates) {
		super.preValidateNewAsset(pEditorInfo, pUpdates);
		validateCouponPrefix(pEditorInfo, pUpdates);
	}

	private void validateCouponPrefix(AssetEditorInfo pEditorInfo,
			Collection pUpdates) {
		// TODO Auto-generated method stub
		 AssetWrapper wrapper = pEditorInfo.getAssetWrapper();
	        @SuppressWarnings("unused")
			RepositoryItem item = (RepositoryItem)wrapper.getAsset();
	        for(Iterator iter = pUpdates.iterator(); iter.hasNext();)
	        {
	            Object obj = iter.next();
	            if(obj instanceof PropertyEditorAssetViewUpdate)
	            {
	                Collection updates = ((PropertyEditorAssetViewUpdate)obj).getPropertyUpdates();
	                Iterator propIter = updates.iterator();
	                while(propIter.hasNext()) 
	                {
	                    PropertyUpdate update = (PropertyUpdate)propIter.next();
	                    if("prefix".equals(update.getPropertyName()))
	                    {
	                        String prefix = (String)update.getPropertyValue();
	                    	boolean hasLowercase=false;
	                		if(null!=prefix){
	                		hasLowercase = !prefix.equals(prefix.toUpperCase());
	                		}
	                		if(hasLowercase){
	                			String validationMessage = ResourceUtils
	                					.getUserMsgResource(
	                							"couponBatchValidator.invalidCouponPrefixValue",
	                							"com.digital.commerce.common.catalog.DigitalPricingModelsTemplateResources",
	                							mResourceBundle);
	                			addError("prefix", validationMessage);
	                		}
	                    }
	                }
	            }
	        }
	}

}