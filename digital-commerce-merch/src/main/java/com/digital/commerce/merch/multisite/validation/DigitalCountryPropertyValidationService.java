package com.digital.commerce.merch.multisite.validation;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.nucleus.GenericService;
import atg.remote.assetmanager.editor.model.PropertyUpdate;
import atg.remote.assetmanager.editor.service.AssetEditorInfo;
import atg.remote.multisite.service.SiteAssetService;
import atg.remote.multisite.service.validation.SiteAssetServiceValidator;
import atg.service.dynamo.LangLicense;
import atg.servlet.ServletUtil;
@SuppressWarnings({"rawtypes"})
public class DigitalCountryPropertyValidationService extends GenericService implements SiteAssetServiceValidator {

	// --------------------------------------------------------------------------
	// CONSTANTS
	// --------------------------------------------------------------------------

	/** Resource name */
	private static final String RESOURCE_NAME = "com.digital.commerce.common.i18n.DigitalTranslationValitionResources";

	// --------------------------------------------------------------------------
	// METHODS
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------
	/**
	 * @return The resource bundle to be used in this class.
	 */
	public ResourceBundle getResourceBundle() {

		// Get the locale from the logged in user. If the user's locale can't be
		// found,
		// get the browser locale. If either of these can't be found, use the
		// server locale.
		Locale locale = ServletUtil.getUserLocale();

		if (locale != null) {
			// Get the resource bundle associated with the current locale.
			try{
				return LayeredResourceBundle.getBundle(RESOURCE_NAME, locale);
			}catch(Exception ex){
				logWarning("Error retrieving locale based resourcebundle for locale - " + locale + ex.getMessage());
			}
		}

		// Couldn't retrieve the locale from user's profile, browser or server
		// so just get the default locale.
		return LayeredResourceBundle.getBundle(RESOURCE_NAME,
				LangLicense.getLicensedDefault());
	}


	  //--------------------------------------------------------------------
	  /**
	   * This method will run validation on site property updates. If validation
	   * fails, an error or warning will be created to be displayed to the user.
	   * 
	   * @param pEditorInfo Information object for the current editor.
	   * @param pUpdates Collection of PropertyEditorAssetViewUpdate objects.
	   */
	  @Override
	  public void validate(AssetEditorInfo pEditorInfo, Collection pUpdates) {

		    SiteAssetService sas = new SiteAssetService();
		    
		    // Get all current property updates.
		    List<PropertyUpdate> updates = sas.getPropertyUpdates(pUpdates);
		    
		if (updates != null) {
			 // Iterate through all URL property updates.
		      for (PropertyUpdate propUpdate : updates) {
		        // Get updated property name
		        String name = propUpdate.getPropertyName();
		        
		        if (name.equals("defaultCountry")) {
		 
		          String countryCodeValue = (String) propUpdate.getPropertyValue();
		          
		          if (!DigitalStringUtil.isEmpty(countryCodeValue)) {
		            List<String> countryCodes = Arrays.asList(Locale.getISOCountries());
		  
		            if (countryCodeValue != null && !countryCodes.contains(countryCodeValue)) {
		              
		              pEditorInfo.getAssetService().addError(
		                ResourceUtils.getUserMsgResource("siteLocale.error.defaultCountryCodeInvalid",
		                                                 RESOURCE_NAME,
		                                                 getResourceBundle()));
		            }
		          }
		        }
		      }
		}
	}
}
