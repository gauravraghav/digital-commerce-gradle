package com.digital.commerce.merch.services.targetting;

public class UserGroupsBean {
	private String name;
	
	private String description;

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String groupName) {
		name = groupName;
		
	}
}