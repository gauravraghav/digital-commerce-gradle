package com.digital.commerce.merch.assetmanager.editor.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import atg.flex.service.RemoteOperationException;
import atg.remote.assetmanager.editor.model.AssetEditorReturnState;
import atg.remote.assetmanager.editor.service.AssetEditorService;
import atg.remote.assetmanager.transfer.model.AssetExportPropertyInfo;
import atg.remote.assetmanager.transfer.service.CouponBatchPreviewEngine;
import atg.remote.assetmanager.transfer.service.ExportException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.asset.AssetResolverException;
import atg.service.asset.AssetUtils;
import atg.service.asset.AssetWrapper;

/**
 * Extended AssetEditorService to support Product repositoryItem consisting of url property of "media-external" item-type.
 * 
 * @author MS398446
 */


public class DigitalAssetEditorService extends AssetEditorService
{
	
	private CouponBatchPreviewEngine couponBatchPreviewEngine;
	 public CouponBatchPreviewEngine getCouponBatchPreviewEngine() {
		return couponBatchPreviewEngine;
	}


	public void setCouponBatchPreviewEngine(
			CouponBatchPreviewEngine couponBatchPreviewEngine) {
		this.couponBatchPreviewEngine = couponBatchPreviewEngine;
	}


	public DigitalAssetEditorService(){
		 super();
	 }
   
  
	 /* 
	 * Fetches the url String property value from the RepositoryItem
	 */
	@Override
    public String getMediaUrlProperty(String pAssetURI)
        throws RemoteOperationException
    {
        if(pAssetURI == null)
            return null;
        try
        {
            AssetWrapper wrap = AssetUtils.getAssetResolver().resolveAsset(pAssetURI);
            if(wrap != null)
            {
                Object asset = wrap.getAsset();
                if(asset != null && (asset instanceof RepositoryItem))
                {
                    RepositoryItem item = (RepositoryItem)asset;
                    //Check whether the RepositoryItem contains url property
                    if(item.getItemDescriptor().hasProperty("url")){
                    	
                    	if (isLoggingDebug()) {logDebug("ItemDescriptorName :::: "+item.getItemDescriptor().getItemDescriptorName());}
                    	
                    	//Check whether the url property is of type RepositoryItem
                    	Object urlObj = item.getPropertyValue("url");
                    	if (urlObj instanceof RepositoryItem){
                    		if (isLoggingDebug()) {logDebug(urlObj +" :::: instanceof RepositoryItem");}   
                    		
                    		RepositoryItem repoItem = (RepositoryItem)urlObj;                    		
                    		if (isLoggingDebug()) {logDebug("Has url property :::: "+ repoItem.getItemDescriptor().hasProperty("url"));}
                    		//Fetch the String url property
                    		if(repoItem.getItemDescriptor().hasProperty("url")){
                    			 return (String)repoItem.getPropertyValue("url");
                    		}
	                	}
	                	else{
	                		// String url property
	                		if (isLoggingDebug()) {logDebug(urlObj +" :::: is a String");}
	                		return (String)urlObj;
	                	}
                	}
                 }
            }
        }
        catch(AssetResolverException | RepositoryException e)
        {
            if(isLoggingError()){logError(e);}
            throw new RemoteOperationException(e);
        }
			return null;
    }   
	
	/**
	 * Added this method to persist batch coupons if the batch is created for birthday flow
	 * @author NC419910
	 */
	@Override
	public AssetEditorReturnState addAsset(String pUID, Collection pUpdates)
			throws RemoteOperationException
			{
		AssetEditorReturnState returnState;
		returnState = super.addAsset(pUID, pUpdates);

		if(returnState!=null && returnState.getSuccessState()!=null ){
			AssetWrapper assetWrapper = getEditorInfo(pUID).getAssetWrapper();
			RepositoryItem item = (RepositoryItem)getEditorInfo(pUID).getAssetWrapper().getAsset();
			try {
				String itemDescriptorName=(String)item.getItemDescriptor().getItemDescriptorName();
				if("CouponBatch".equalsIgnoreCase(itemDescriptorName)){
					boolean isBirthdayCoupon=(Boolean) item.getPropertyValue("birthdayCouponBatch");
					if(isBirthdayCoupon){
						String assetURI = assetWrapper.geturi();
						String assetType = "/atg/commerce/claimable/SecureClaimableRepository:CouponBatch";
						List assetUris = new ArrayList();
						assetUris.add(assetURI);
						AssetExportPropertyInfo assetExportPropertyInfo = new AssetExportPropertyInfo();
						assetExportPropertyInfo.setUseVerticalFormat(false);
						List pAssetExportProperties = new ArrayList();
						assetExportPropertyInfo.setAssetExportProperties(pAssetExportProperties);

						try {
							getCouponBatchPreviewEngine().performPreview(assetType, assetUris,assetExportPropertyInfo);
						} catch (ExportException e) {
							if (isLoggingDebug()) {logDebug("ExportException occured while performing previewEngine :::: ");}
						}
					}
				}
			} catch (RepositoryException e) {
				if (isLoggingDebug()) {logDebug("RepositoryException occured while creating previewEngine :::: ");}
			}
		}
		return returnState;

			}

}


