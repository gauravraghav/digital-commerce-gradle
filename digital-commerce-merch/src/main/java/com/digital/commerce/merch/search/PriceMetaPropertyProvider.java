package com.digital.commerce.merch.search;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import atg.beans.DynamicPropertyDescriptor;
import atg.commerce.pricing.priceLists.PriceListException;
import atg.commerce.pricing.priceLists.PriceListManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.repository.search.MetaProperty;
import atg.repository.search.MetaPropertyProvider;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

@SuppressWarnings({"rawtypes","unchecked"})
public class PriceMetaPropertyProvider extends GenericService implements MetaPropertyProvider {
	private PriceListManager	priceListManager;

	public PriceListManager getPriceListManager() {
		return priceListManager;
	}

	public void setPriceListManager( PriceListManager priceListManager ) {
		this.priceListManager = priceListManager;
	}

	public Set getProperties( Set pAcceptableTypes ) {

		Set priceProperties = new HashSet();

		Collection plists;
		try {
			plists = this.getPriceListManager().getPriceLists();

			// Build price DynamicPropertyDescriptor
			Iterator i = plists.iterator();
			RepositoryItem plist = null;
			while( i.hasNext() ) {
				DynamicPropertyDescriptor propDesc = new DynamicPropertyDescriptor();
				plist = (RepositoryItem)i.next();
				propDesc.setName( "childSKUs." + plist.getRepositoryId() ); // In configuration

				propDesc.setPropertyType( Double.class );

				propDesc.setDisplayName( plist.getRepositoryId() );

				MetaProperty priceProperty = new MetaProperty( propDesc, PropertyTypeEnum.FLOAT.toString() );

				priceProperties.add( priceProperty );
			}
		} catch( PriceListException e ) {
			if( this.isLoggingError() ) {
				logError( "Couldn't lookup pricelists" );
			}
		}

		return priceProperties;

	}

}
