package com.digital.commerce.merch.integration.catimport.util;

import atg.nucleus.ServiceException;

public class GenericService extends atg.nucleus.GenericService {
	private String[]	mRequiredConfigurations	= null;

	/** @return Returns the requiredConfigurations. */
	public String[] getRequiredConfigurations() {
		return mRequiredConfigurations;
	}

	/** @param pRequiredConfigurations The requiredConfigurations to set. */
	public void setRequiredConfigurations( String[] pRequiredConfigurations ) {
		mRequiredConfigurations = pRequiredConfigurations;
	}

	private String	mAbsoluteName	= null;

	/** a utility method to check for proper configuration. Just wraps the helpers static method */
	public void assertConfigNotNull( final Object pProperty, final String pPropertyName ) throws ServiceException {
		ServiceHelper.assertConfigNotNull( pProperty, pPropertyName );
	}

	/** a utility method to check for proper non zero configuration. Just wraps the helpers static method */
	public void assertConfigNotZero( final int pProperty, final String pPropertyName ) throws ServiceException {
		ServiceHelper.assertConfigNotZero( pProperty, pPropertyName );
	}

	/** @return Returns the mAbsoluteName. */
	public String getAbsoluteName() {
		return ( ( null == super.getAbsoluteName() ) ? mAbsoluteName : super.getAbsoluteName() );
	}

	/** @param pAbsoluteName The mAbsoluteName to set. */
	public void setAbsoluteName( String pAbsoluteName ) {
		mAbsoluteName = pAbsoluteName;
	}

	public void assertConfigsNotNull() throws ServiceException {
		ServiceHelper.assertConfigsNotNull( this, getRequiredConfigurations() );
	}

	/** if the RequiredConfiurations property is set, it will assert that
	 * these are configured properly
	 * 
	 * @see atg.nucleus.GenericService#doStartService() */
	public void doStartService() throws ServiceException {
		super.doStartService();
		if( null != getRequiredConfigurations() ) {
			assertConfigsNotNull();
		}
	}

}
