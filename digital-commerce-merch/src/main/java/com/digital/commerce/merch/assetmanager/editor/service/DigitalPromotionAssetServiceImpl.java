package com.digital.commerce.merch.assetmanager.editor.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.remote.assetmanager.editor.model.PropertyEditorAssetViewUpdate;
import atg.remote.assetmanager.editor.model.PropertyUpdate;
import atg.remote.assetmanager.editor.service.AssetEditorInfo;
import atg.remote.assetmanager.editor.service.PromotionAssetServiceImpl;
import atg.repository.RepositoryItem;
import atg.servlet.ServletUtil;

/**
 * Logic to check the display date range between usage start and end date.
 * 
 * @author MS398446
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalPromotionAssetServiceImpl extends PromotionAssetServiceImpl {

	private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS",
			Locale.US);

	private static ResourceBundle mResourceBundle = LayeredResourceBundle
			.getBundle(
					"com.digital.commerce.common.catalog.DigitalPricingModelsTemplateResources",
					ServletUtil.getUserLocale());

	public void preValidateNewAsset(AssetEditorInfo pEditorInfo,
			Collection pUpdates) {
		super.preValidateNewAsset(pEditorInfo, pUpdates);
		validateDisplayDateProperties(pEditorInfo, pUpdates);
	}

	public void preValidateUpdateAsset(AssetEditorInfo pEditorInfo,
			Collection pUpdates) {
		super.preValidateUpdateAsset(pEditorInfo, pUpdates);
		validateDisplayDateProperties(pEditorInfo, pUpdates);
	}

	private void validateDisplayDateProperties(AssetEditorInfo pEditorInfo,
			Collection<PropertyEditorAssetViewUpdate> pUpdates) {
		if (isLoggingDebug()) {
			logDebug("inside validateDisplayDateProperties");
		}

		RepositoryItem item = (RepositoryItem) pEditorInfo.getAssetWrapper()
				.getAsset();
		
		Date displayStartDate = (Date) item
				.getPropertyValue("displayStartDate");
		Date displayEndDate = (Date) item.getPropertyValue("displayEndDate");

		if (isLoggingDebug()) {
			logDebug("Before update**************");
			logDebug("displayStartDate -->" + displayStartDate);
			logDebug("displayEndDate -->" + displayEndDate);			
		}

		for (PropertyEditorAssetViewUpdate viewUpdate : pUpdates) {
			for (PropertyUpdate propertyUpdate : viewUpdate
					.getPropertyUpdates()) {
				String propertyName = propertyUpdate.getPropertyName();
				if (("displayStartDate".equalsIgnoreCase(propertyName))
						|| ("displayEndDate".equalsIgnoreCase(propertyName))) {
					Object proertyValueUpdateObj = propertyUpdate.getPropertyValue();
					if (isLoggingDebug()) {
						logDebug("propertyUpdate -->"
								+ proertyValueUpdateObj);
					}
					
					if (proertyValueUpdateObj != null) {
						Date parsedDate = null;
						String dateValue = (String) proertyValueUpdateObj;
						if (DigitalStringUtil.isNotEmpty(dateValue.trim())) {
							try {
								parsedDate = df.parse(dateValue);
							} catch (ParseException e) {
								logError("Unable to parse date");
							}
						}
						if ("displayStartDate"
								.equalsIgnoreCase(propertyName)) {
							displayStartDate = parsedDate;
						}
						if ("displayEndDate".equalsIgnoreCase(propertyName)) {
							displayEndDate = parsedDate;
						}
					}
				}
			}
		}

		if (isLoggingDebug()) {
			logDebug("After update**************");
			logDebug("displayStartDate -->" + displayStartDate);
			logDebug("displayEndDate -->" + displayEndDate);
		}

	
			if (displayStartDate == null && displayEndDate != null) {
				String validationMessage = ResourceUtils
						.getUserMsgResource(
								"displayDateValidator.invalidDisplayDateEntry",
								"com.digital.commerce.common.catalog.DigitalPricingModelsTemplateResources",
								mResourceBundle);
				pEditorInfo.getAssetService().addError("displayStartDate",
						validationMessage);
			}
			if (displayStartDate != null && displayEndDate == null) {
				String validationMessage = ResourceUtils
						.getUserMsgResource(
								"displayDateValidator.invalidDisplayDateEntry",
								"com.digital.commerce.common.catalog.DigitalPricingModelsTemplateResources",
								mResourceBundle);
				pEditorInfo.getAssetService().addError("displayEndDate",
						validationMessage);
			}			
	
			if (displayStartDate!=null && displayEndDate!=null ){
				if (displayStartDate.after(displayEndDate)) {
					String validationMessage = ResourceUtils
							.getUserMsgResource(
									"displayDateValidator.invalidDisplayDateRangeEntry",
									"com.digital.commerce.common.catalog.DigitalPricingModelsTemplateResources",
									mResourceBundle);
					pEditorInfo.getAssetService().addError("displayEndDate",
							validationMessage);
				}
			}
		

	}
}