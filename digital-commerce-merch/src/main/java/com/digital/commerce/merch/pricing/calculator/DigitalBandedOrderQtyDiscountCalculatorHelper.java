package com.digital.commerce.merch.pricing.calculator;

import atg.commerce.pricing.BandedDiscountCalculatorHelper;
import atg.commerce.pricing.CalculatorInfo;
import atg.commerce.pricing.DiscountAttributeInfo;

/**
 * 
 *
 */
public class DigitalBandedOrderQtyDiscountCalculatorHelper extends
		BandedDiscountCalculatorHelper {

	public CalculatorInfo getCalculatorInfo(String pCalculatorType) {
		if (isLoggingDebug()) {
			logDebug("Entered getCalculatorInfo");
			logDebug("pCalculatorType: " + pCalculatorType);
		}

		CalculatorInfo info = new CalculatorInfo(pCalculatorType);

		DiscountAttributeInfo[] structAtts = new DiscountAttributeInfo[2];
		structAtts[0] = new DiscountAttributeInfo("bandingProperty");
		structAtts[0].setDataType(String.class);
		structAtts[0].setRequired(false);

		structAtts[1] = new DiscountAttributeInfo("bandingPropertyScope");
		structAtts[1].setDataType(String.class);
		structAtts[1].setEnumeratedValues(new String[] { "QualifiedItem", "DetailedItemPriceInfo" });

		structAtts[1].setRequired(false);

		info.setDiscountStructureAttributeInfos(structAtts);

		DiscountAttributeInfo[] atts = new DiscountAttributeInfo[2];
		atts[0] = new DiscountAttributeInfo("quantity");
		atts[0].setDataType(Number.class);

		atts[1] = new DiscountAttributeInfo("adjuster");
		atts[1].setDataType(Double.class);

		info.setDiscountDetailAttributeInfos(atts);

		CalculatorInfo.DiscountDetailInfo[] detailsInfos = new CalculatorInfo.DiscountDetailInfo[1];
		detailsInfos[0] = info.createDiscountDetailInfo();
		detailsInfos[0].setDetailAttributes(atts);

		if (isLoggingDebug()) {
			logDebug("Leaving with info: " + info);
		}

		return info;
	}

}
