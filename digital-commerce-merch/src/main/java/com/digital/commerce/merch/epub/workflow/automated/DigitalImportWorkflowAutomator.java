package com.digital.commerce.merch.epub.workflow.automated;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringEscapeUtils;

import com.digital.commerce.claimable.DigitalCouponBatchTools;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.DigitalDateUtil;
import com.digital.commerce.common.util.DigitalStringUtil;

import atg.adapter.gsa.query.Builder;
import atg.commerce.claimable.ClaimableException;
import atg.commerce.claimable.ClaimableTools;
import atg.epub.ImportWorkflowAutomator;
import atg.epub.PublishingSession;
import atg.epub.PublishingWorkflowException;
import atg.multisite.Site;
import atg.remote.assetmanager.transfer.model.AssetExportPropertyInfo;
import atg.remote.assetmanager.transfer.service.CouponBatchPreviewEngine;
import atg.remote.assetmanager.transfer.service.ExportException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.security.ThreadSecurityManager;
import atg.userprofiling.ProfileTools;


/**
 * This will trigger the automatic workflow to create a couponBatch with "X" no of couponCodes
 * CouponBatchId will be the prefix for the couponCodes
 * No of coupon codes to generate depends on the birthdayGift item and the sql query: sqlBirthdayGiftQuery
 * Each coupon Code generated will be stored against the birthdayGift item
 * 
 * @author NC419910
 *
 */

@SuppressWarnings("unchecked")
public class DigitalImportWorkflowAutomator extends ImportWorkflowAutomator {
	private static final String CHAR_HYPHEN = "-";
	private static final String DATE_PATTERN = "MM/dd/yyyy";
	private static final String ASSET_URI="atgasset:/Claimable/CouponBatch/";
	private static final String ASSET_TYPE="/atg/commerce/claimable/SecureClaimableRepository:CouponBatch";
	private static final String BIRTHDAY_COUPON_BATCH_PROPERTY="birthdayCouponBatch";
	private static final String COUPON_PREFIX_PROPERTY="prefix";
	private static final String NUMBER_OF_COUPONS_PROPERTY="numberOfCoupons";
	private static final String START_DATE_PROPERTY="startDate";
	private static final String EXPIRATION_PROPERTY="expirationDate";
	private static final String PROMOTIONS_PROPERTY="promotions";
	private static final String DTFORMAT_4_IDGENERATION = "MM-dd-yyyy-hh-mm-ss-SSS";
	private static final String BIRTHDAY_GIFT_ITEM_DESCRIPTOR_NAME="birthdayGift";
	private static final String BIRTHDAY_GIFT_COUPON_BATCH_ID_PROPERTY="couponBatchId";
	private static final String BIRTHDAY_GIFT_COUPON_BATCH_CODE_PROPERTY="couponBatchCode";
	private static final String BIRTHDAY_COUPONS_PROPERTY="birthdayCoupons";
	private static final String BIRTHDAY_GIFT_ID_PROPERTY_NAME="giftId";
	private static final String BIRTHDAY_GIFT_UPDATE_DATE_PROPERTY_NAME="updateDate";
	private static final String BIRTHDAY_GIFT_UPDATE_USER_NAME_PROPERTY_NAME="updateUsername";
	private static final String BATCH_COUPON_MAX_USERS_PROPERTY="maxUses";

	private String startDate;
	private Repository claimableRepository;
	private Repository promotionRepository;
	private Repository externalProfileRepository;
	private DigitalCouponBatchTools couponBatchTools;
	private CouponBatchPreviewEngine couponBatchPreviewEngine;
	private String promotionItemDescriptorName;
	private String couponBatchItemDescriptorName;
	private ClaimableTools claimableTools;
	private ProfileTools profileTools;
	private String sqlBirthdayGiftQuery;
	private String sqlBirthdayGiftItemsQuery;
	private boolean enableBirthdayCoupons;
	private int noOfCouponsPerCouponBatch;
	private String updateUserName;
	private String defaultSiteId;
	private int maxUses;

	public int getMaxUses() {
		return maxUses;
	}

	public void setMaxUses(int maxUses) {
		this.maxUses = maxUses;
	}

	public String getDefaultSiteId() {
		return defaultSiteId;
	}

	public void setDefaultSiteId(String defaultSiteId) {
		this.defaultSiteId = defaultSiteId;
	}

	public DigitalImportWorkflowAutomator(){
		// Do nothing
	}

	public void triggerCouponBatchGeneration(){
		if(isEnableBirthdayCoupons()){
			try{
				getGiftListIds();
			}catch(Exception e){
				logError(e.getMessage());
			}
		}else{
			if(isLoggingInfo()){
				logInfo(getClass().getSimpleName()+"BirthdayCouponBatchProcess : COUPON BATCH PROCESS IS DISABLED : isCouponBatchProcessEnabled() :::" +isEnableBirthdayCoupons() );
			}
		}
	}

	private void getGiftListIds() {
		//Trigger the ATG Profile to get the list of unique giftIds for the given startDate
		int noOfCouponsToGenerate=0;
		RepositoryItem[] userItems=null;
		DigitalBirthdayCouponsResponseData response= new DigitalBirthdayCouponsResponseData();
		userItems=getBirthdayProfiles();
		Set<String> uniqueGiftIds;
		if(null!=userItems && userItems.length >0){
			uniqueGiftIds=getUniqueValues(userItems);
			if(getNoOfCouponsPerCouponBatch()<1){
				setNoOfCouponsPerCouponBatch(uniqueGiftIds.size());//set the size of the unique gift ids list as the NoofCouponsPerCouponBatch if its not defined
			}
			List<Set<String>> uniqueGiftIdSets = (List<Set<String>>)splitUniqueGiftIdsToSubGiftIds(uniqueGiftIds,getNoOfCouponsPerCouponBatch());
			for(Set<String> uniqueGiftIdSet:uniqueGiftIdSets){
				if(!uniqueGiftIdSet.isEmpty()){
					noOfCouponsToGenerate=uniqueGiftIdSet.size();
				}
				if(noOfCouponsToGenerate>0){
					try {
						response=publishAssetData(noOfCouponsToGenerate);
					} catch (Exception e) {
						logError(getClass().getSimpleName()+" Exception occured while creating batch coupons"+e.getMessage());
					}
				}

				//get the response and the userItems to update the birthdayGiftItems
				if(response!=null && response.isCouponsGenerated()){
					MutableRepository profileRepository = (MutableRepository) getExternalProfileRepository();
					RepositoryView profileView=null;
					MutableRepositoryItem[] birthdayGiftItems=null;
					try {
						profileView = profileRepository.getView(BIRTHDAY_GIFT_ITEM_DESCRIPTOR_NAME);
						Builder builder = (Builder)profileView.getQueryBuilder();
						int i=0;
						for (String giftId:uniqueGiftIdSet) {
							Object params[] = new Object[1];
							params[0] = giftId;
							birthdayGiftItems =(MutableRepositoryItem[])profileView.executeQuery (builder.createSqlPassthroughQuery(getSqlBirthdayGiftItemsQuery(), params));
							if(null!=birthdayGiftItems && birthdayGiftItems.length>0){
								for(MutableRepositoryItem birthdayGiftItem:birthdayGiftItems){
									birthdayGiftItem.setPropertyValue(BIRTHDAY_GIFT_COUPON_BATCH_ID_PROPERTY, response.getCouponBatchId());
									birthdayGiftItem.setPropertyValue(BIRTHDAY_GIFT_COUPON_BATCH_CODE_PROPERTY, response.getCouponCodes().get(i));
									birthdayGiftItem.setPropertyValue(BIRTHDAY_GIFT_UPDATE_DATE_PROPERTY_NAME, new Date());
									birthdayGiftItem.setPropertyValue(BIRTHDAY_GIFT_UPDATE_USER_NAME_PROPERTY_NAME, getUpdateUserName());
									profileRepository.updateItem(birthdayGiftItem);
								}
								i++;
							}
						} 
					}catch (RepositoryException e) {
						logError(getClass().getSimpleName()+ "RepositoryException while updating the CouponBath and CouponCode back on birthdayGift Items "+e.getMessage());
					}
				}else{
					if(isLoggingInfo()){
						logInfo(getClass().getSimpleName()+"BirthdayCouponBatchProcess : Reponse is either null or coupons not generated ::: ");
					}
				}
			}
		}else{
			if(isLoggingInfo()){
				logInfo(getClass().getSimpleName()+"BirthdayCouponBatchProcess : NO PROFILES TO TRIGGER THE BIRTHDAY COUPON BATCH PROCESS for GIVEN COUPON START DATE :::" +getCouponStartDate() );
			}
		}

	}
	/**
	 * This method will return the list into sublist
	 * @param uniqueGiftLists
	 * @param noOfCouponsPerCouponBatch
	 * @return
	 */
	
    private  List<Set<String>> splitUniqueGiftIdsToSubGiftIds(Set<String> uniqueGiftLists, int noOfCouponsPerCouponBatch) {
        List<Set<String>> giftLists = new ArrayList<>();
        Set<String> gList = new HashSet<>();
        int count = 0;
        if (uniqueGiftLists != null) {
            for (String str : uniqueGiftLists) {
                if (count < noOfCouponsPerCouponBatch) {
                    count = count + 1;
                    gList.add(str);
                } else {
                	giftLists.add(gList);
                	gList = new HashSet<>();
                	gList.add(str);
                    count = 1;
                }
 
            }
            if (gList.size() <= noOfCouponsPerCouponBatch) {
            	giftLists.add(gList);
            }
        }
        return giftLists;
    }
    
    /**
	 * This method will return the unique giftIds
	 * @param userItems
	 * @return
	 */
	private Set<String> getUniqueValues(RepositoryItem[] userItems) {
		Set<String> uniqueGiftIds=new HashSet<>();
		if(null!=userItems && userItems.length>0){
			for(RepositoryItem item:userItems){
				String giftId=(String) item.getPropertyValue(BIRTHDAY_GIFT_ID_PROPERTY_NAME);
				if(!uniqueGiftIds.contains(giftId)){
					uniqueGiftIds.add(giftId);
				}
			}
		}
		return uniqueGiftIds;
	}

	public DigitalBirthdayCouponsResponseData publishAssetData(int noOfCouponsToGenerate) throws DigitalAppException{

		DigitalBirthdayCouponsResponseData response=new DigitalBirthdayCouponsResponseData();
		logInfo(getClass().getSimpleName()+" ::: publishAssetData :: publish Asset Data  ::");
		String sessionId = (new StringBuilder(getProjectNameStub()).append(CHAR_HYPHEN)
				.append(Calendar.getInstance().getTime())).toString();

		try{
			if(isLoggingInfo()){
				logInfo(getClass().getSimpleName()+" ::: publishAssetData :: about to startWorkflowSession");
			}
			PublishingSession pubSession = startWorkflowSession(sessionId);

			if(isLoggingInfo()){
				logInfo("publishAssetData :: createData");
			}
			response=populateBatchCouponAssetData(noOfCouponsToGenerate);

			if(isLoggingInfo()){
				logInfo("publishAssetData :: endWorkflowSession");
			}
			endWorkflowSession(pubSession);

		}catch(PublishingWorkflowException pwe){
			if(isLoggingError()){ 
				logError("PublishingWorkflowException:: Error in publishAssetData   ::  " +pwe.getMessage());
			}
			throw new DigitalAppException(pwe);
		}catch(Exception e){
			if(isLoggingError()){
				logError("Error in publishAssetData  ::  " +e.getMessage());
			}
			throw new DigitalAppException(e);
		}
		return response;
	}

	private RepositoryItem[] getBirthdayProfiles() {
		if(isLoggingInfo()) {
			logInfo(getClass().getSimpleName()+ " getProfiles():START");
		}

		MutableRepository profileRepository = (MutableRepository) getExternalProfileRepository();
		RepositoryView profileView=null;
		RepositoryItem[] userGiftItems=null;
		try {
			profileView = profileRepository.getView(BIRTHDAY_GIFT_ITEM_DESCRIPTOR_NAME);
			Builder builder = (Builder)profileView.getQueryBuilder();
			userGiftItems =profileView.executeQuery (builder.createSqlPassthroughQuery(StringEscapeUtils.unescapeJava(getSqlBirthdayGiftQuery()),null));
		} catch (RepositoryException e) {
			logError(getClass().getSimpleName()+ "RepositoryException while getting the GiftItems List "+e.getMessage());
		}

		if (userGiftItems == null){
			if(isLoggingInfo()) {
				logInfo(getClass().getSimpleName()+ "NO GIFT ITEMS TO TRIGGER THE COUPON BATCH PROCESS :: Triggered Date "+new Date());
			}
			return null;
		}

		if(isLoggingInfo()) {
			logInfo(getClass().getSimpleName()+ " getProfiles():END");
		}
		return userGiftItems;

	}

	private DigitalBirthdayCouponsResponseData populateBatchCouponAssetData(int numberOfCouponsToGenerate) throws DigitalAppException, ClaimableException, RepositoryException{
		DigitalBirthdayCouponsResponseData response=new DigitalBirthdayCouponsResponseData();
		if(isLoggingInfo()){
			logInfo(getClass().getSimpleName()+" ::: populateBatchCouponAssetData :: START POPULATING COUPONBATCH DATA ::");
		}

		MutableRepositoryItem assetRepItem=null;
		assetRepItem=(MutableRepositoryItem) getClaimableTools().createClaimableItem(null,getCouponBatchItemDescriptorName(),true);
		MutableRepository mutPromotionRepository = (MutableRepository) getPromotionRepository();
		RepositoryItem promotionItem = null;
		Set<RepositoryItem> promoItems=new HashSet<>();
		Set<String> promotionIds=getPromotions();
		if(null!=promotionIds && !promotionIds.isEmpty()){
			for(String promotionId:promotionIds){
				promotionItem=mutPromotionRepository.getItem(promotionId, getPromotionItemDescriptorName());
				if(null!=promotionItem){
					promoItems.add(promotionItem);
				}
			}
			if(!promoItems.isEmpty()){
				assetRepItem.setPropertyValue(PROMOTIONS_PROPERTY, promoItems);
			}
		}
		assetRepItem.setPropertyValue(BIRTHDAY_COUPON_BATCH_PROPERTY, Boolean.TRUE);
		assetRepItem.setPropertyValue(COUPON_PREFIX_PROPERTY,assetRepItem.getRepositoryId().toUpperCase());
		assetRepItem.setPropertyValue(NUMBER_OF_COUPONS_PROPERTY, numberOfCouponsToGenerate);
		assetRepItem.setPropertyValue(START_DATE_PROPERTY, getCouponStartDate());
		assetRepItem.setPropertyValue(EXPIRATION_PROPERTY, getCouponExpirationDate());
		assetRepItem.setPropertyValue(BATCH_COUPON_MAX_USERS_PROPERTY, getMaxUses());
	
		getClaimableTools().persistTransientClaimableItem(assetRepItem);

		String batchId=assetRepItem.getRepositoryId();
		List assetUris = new ArrayList();
		assetUris.add(ASSET_URI+batchId);
		AssetExportPropertyInfo assetExportPropertyInfo = new AssetExportPropertyInfo();
		assetExportPropertyInfo.setUseVerticalFormat(false);
		List pAssetExportProperties = new ArrayList();
		assetExportPropertyInfo.setAssetExportProperties(pAssetExportProperties);

		try {
			getCouponBatchPreviewEngine().performPreview(ASSET_TYPE, assetUris,assetExportPropertyInfo);
		} catch (ExportException e) {
			logError(getClass().getSimpleName()+":: Exception occured during preview process :::  "+e.getMessage());
		}

		response.setCouponBatchId(batchId);
		//get the coupons from the batch and set on the resposne
		List<String> coupons;
		coupons=(List<String>) assetRepItem.getPropertyValue(BIRTHDAY_COUPONS_PROPERTY);
		if(null!=coupons && !coupons.isEmpty()){
			response.setCouponCodes(coupons);
			response.setCouponsGenerated(true);
		}

		if(isLoggingInfo()){
			logInfo(getClass().getSimpleName()+" ::: populateBatchCouponAssetData :: START POPULATING COUPONBATCH DATA ::");
		}

		return response;
	}


	@Override
	protected String getProjectName(String pSessionId){
		String prjName;
		prjName = createProjectName(getProjectNameStub());
		if(isLoggingInfo()){
			logInfo("getProjectName :: prjName :: "+prjName);
		}
		return prjName;
	}

	private String createProjectName(String prjPrefix){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat(DTFORMAT_4_IDGENERATION);
		String dtString = dateFormat.format(cal.getTime());
		return (new StringBuilder(prjPrefix).append(CHAR_HYPHEN).append(dtString)).toString();
	}

	private Date getCouponStartDate() {
		SimpleDateFormat df = new SimpleDateFormat(DATE_PATTERN);
		Date startDt = null;
		try {
			if (DigitalStringUtil.isNotBlank(getStartDate())) {
				startDt = df.parse(getStartDate());
			} else {
				startDt = new Date();
				startDt = (df.parse(df.format(startDt)));
				startDt = DigitalDateUtil.addDays(startDt, getBufferDays());
			}
		} catch (ParseException e1) {
			if (isLoggingError()) {
				logError("Error while parsing START DATE ::" + e1.getMessage());
			}
		}
		return startDt;
	}

	private Date getCouponExpirationDate(){

		return (DigitalDateUtil.addDays(getCouponStartDate(), getNoOfDaysToExpire()));
	}

	/**
	 * This method unsets the security context on the current thread.
	 */
	protected void releaseUserIdentity() {
		ThreadSecurityManager.setThreadUser(null);
	}

	private String mProjectNamePrefix;
	public String getProjectNamePrefix(){
		return mProjectNamePrefix;
	}
	public void setProjectNamePrefix(String val){
		mProjectNamePrefix = val;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public int getBufferDays() {
		
		return (Integer)getSite().getPropertyValue("bufferDays");
	}

	public int getNoOfDaysToExpire() {
		return (Integer)getSite().getPropertyValue("noOfDaysToExpire");
	}

	public DigitalCouponBatchTools getCouponBatchTools() {
		return couponBatchTools;
	}

	public void setCouponBatchTools(DigitalCouponBatchTools couponBatchTools) {
		this.couponBatchTools = couponBatchTools;
	}

	public CouponBatchPreviewEngine getCouponBatchPreviewEngine() {
		return couponBatchPreviewEngine;
	}


	public void setCouponBatchPreviewEngine(
			CouponBatchPreviewEngine couponBatchPreviewEngine) {
		this.couponBatchPreviewEngine = couponBatchPreviewEngine;
	}

	public Repository getPromotionRepository() {
		return promotionRepository;
	}

	public void setPromotionRepository(Repository promotionRepository) {
		this.promotionRepository = promotionRepository;
	}

	public String getPromotionItemDescriptorName() {
		return promotionItemDescriptorName;
	}

	public void setPromotionItemDescriptorName(String promotionItemDescriptorName) {
		this.promotionItemDescriptorName = promotionItemDescriptorName;
	}

	public String getCouponBatchItemDescriptorName() {
		return couponBatchItemDescriptorName;
	}

	public void setCouponBatchItemDescriptorName(
			String couponBatchItemDescriptorName) {
		this.couponBatchItemDescriptorName = couponBatchItemDescriptorName;
	}

	public ClaimableTools getClaimableTools() {
		return claimableTools;
	}

	public void setClaimableTools(ClaimableTools claimableTools) {
		this.claimableTools = claimableTools;
	}

	public ProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(ProfileTools profileTools) {
		this.profileTools = profileTools;
	}
	public Repository getExternalProfileRepository() {
		return externalProfileRepository;
	}

	public void setExternalProfileRepository(Repository externalProfileRepository) {
		this.externalProfileRepository = externalProfileRepository;
	}
	public String getSqlBirthdayGiftQuery() {
		return sqlBirthdayGiftQuery;
	}

	public void setSqlBirthdayGiftQuery(String sqlBirthdayGiftQuery) {
		this.sqlBirthdayGiftQuery = sqlBirthdayGiftQuery;
	}

	public Repository getClaimableRepository() {
		return claimableRepository;
	}

	public void setClaimableRepository(Repository claimableRepository) {
		this.claimableRepository = claimableRepository;
	}

	public String getSqlBirthdayGiftItemsQuery() {
		return sqlBirthdayGiftItemsQuery;
	}

	public void setSqlBirthdayGiftItemsQuery(String sqlBirthdayGiftItemsQuery) {
		this.sqlBirthdayGiftItemsQuery = sqlBirthdayGiftItemsQuery;
	}
	public Set<String> getPromotions() {
		List<String> promos=null;
		promos=(List<String>) getSite().getPropertyValue("promotions");
		if(null!=promos){
			return new HashSet<>(promos);
		}else{
			return null;
		}
	}

	public String getUpdateUserName() {
		return updateUserName;
	}

	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}

	public boolean isEnableBirthdayCoupons() {
		return enableBirthdayCoupons;
	}

	public void setEnableBirthdayCoupons(boolean enableBirthdayCoupons) {
		this.enableBirthdayCoupons = enableBirthdayCoupons;
	}
	
	public Site getSite() {
		// Get  site for SITE ID---DSW
		return MultiSiteUtil.getSite(getDefaultSiteId());
	}

	public int getNoOfCouponsPerCouponBatch() {
		return noOfCouponsPerCouponBatch;
	}

	public void setNoOfCouponsPerCouponBatch(int noOfCouponsPerCouponBatch) {
		this.noOfCouponsPerCouponBatch = noOfCouponsPerCouponBatch;
	}

}
