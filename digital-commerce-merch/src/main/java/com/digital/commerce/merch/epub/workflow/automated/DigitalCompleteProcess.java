package com.digital.commerce.merch.epub.workflow.automated;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Set;

import javax.transaction.TransactionManager;

import org.apache.commons.lang.StringEscapeUtils;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.merch.integration.catimport.CatalogImportException;
import com.digital.commerce.merch.integration.catimport.util.DateUtils;

import atg.adapter.gsa.GSARepository;
import atg.adapter.gsa.query.Builder;
import atg.core.util.ResourceUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.epub.project.Project;
import atg.epub.workflow.process.action.CompleteProcess;
import atg.process.ProcessException;
import atg.process.ProcessExecutionContext;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.service.util.CurrentDate;
import atg.versionmanager.AssetVersion;
import atg.versionmanager.exceptions.VersionException;

public class DigitalCompleteProcess extends CompleteProcess {
	private static final String			AUDIT_IMPORT_FAILURE		= "failed import";
	private static final String			COUPON_BATCH_CHECKIN_FINISHED="finished";
	private static final String			ERROR_WRITING_AUDIT_FAILURE	= "was not able to write audit record";
	protected static final String		RESOURCE_BUNDLE_NAME		= "com.digital.commerce.merch.integration.catimport.CatalogImportResources";
	protected static ResourceBundle		sResourceBundle				= ResourceBundle.getBundle( RESOURCE_BUNDLE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault() );
	private static final String			IMPORT_EXCEPTION_KEY		= "IMPORT_EXCEPTION";
	protected final static String		IMPORT_EXCEPTION_STRING		= ResourceUtils.getUserMsgResource( IMPORT_EXCEPTION_KEY, RESOURCE_BUNDLE_NAME, sResourceBundle );
	private static final String BIRTHDAY_GIFT_ITEM_DESCRIPTOR_NAME="birthdayGift";
	private static final String BIRTHDAY_GIFT_ITEM_STATUS_PROPERTY_NAME="status";
	private static final String BIRTHDAY_GIFT_ITEM_STATUS_VALUE="SENT";
	private static final String COUPON_BATCH_PROJECT_NAME="DigitalCouponBatchImport:";
	private static final String COUPON_BATCH_ITEM_DESCRIPTOR_NAME="CouponBatch";
	private CatalogImportException	mException			= null;
	private static transient DigitalLogger logger = DigitalLogger.getLogger(DigitalCompleteProcess.class);

	@Override
	protected void executeAction(ProcessExecutionContext pContext)
			throws ProcessException {

		Project prj = null;
		try {
			prj = getProject(pContext);
			super.executeAction(pContext);
			if (prj.getDisplayName().startsWith(COUPON_BATCH_PROJECT_NAME)) {
				//Get the Audit repository and update the status to be complete
				writeAudit(prj.getDisplayName(),true, null);
				//Update birthday coupon batch status
				updatestatus(prj);
			} 
		} catch (Exception ee) {
			logger.info(getClass().getName()+" Exception occured while trying to checkin the project  ::  ", ee);
		}
	}

	/**
	 * Update the birthday coupons status field to "SENT"; when the project is checked in
	 * Loop through all assets and get the couponBatch id
	 * @param prj
	 */
	private void updatestatus(Project prj) {
		Set assets = null;
		assets=prj.getCheckedInAssets();
		if(assets!=null && !assets.isEmpty()){
			Iterator<AssetVersion> assetsIterator=(Iterator<AssetVersion>) assets.iterator();
			if(assetsIterator!=null){
				while(assetsIterator.hasNext()){
					AssetVersion assetVersion=assetsIterator.next();
					if(assetVersion!=null){
						try {
							RepositoryItem item=assetVersion.getRepositoryItem();
							if(null!=item && COUPON_BATCH_ITEM_DESCRIPTOR_NAME.equalsIgnoreCase(item.getItemDescriptor().getItemDescriptorName())){
								String couponBatchId=item.getRepositoryId();
								MutableRepository profileRepository = (MutableRepository) ((DigitalPublishingActionConfiguration) getActionConfiguration()).getExternalProfileRepository();
								RepositoryView profileView=null;
								MutableRepositoryItem[] birthdayGiftItems=null;
								profileView = profileRepository.getView(BIRTHDAY_GIFT_ITEM_DESCRIPTOR_NAME);
								Builder builder = (Builder)profileView.getQueryBuilder();
											Object[] params = new Object[1];
											params[0] = couponBatchId;
											birthdayGiftItems =(MutableRepositoryItem[])profileView.executeQuery (builder.createSqlPassthroughQuery(StringEscapeUtils.unescapeJava(((DigitalPublishingActionConfiguration) getActionConfiguration()).getSqlBirthdayGiftStatusItemsQuery()), params));
												if(null!=birthdayGiftItems && birthdayGiftItems.length>0){
												for(MutableRepositoryItem birthdayGiftItem:birthdayGiftItems){
													birthdayGiftItem.setPropertyValue(BIRTHDAY_GIFT_ITEM_STATUS_PROPERTY_NAME, BIRTHDAY_GIFT_ITEM_STATUS_VALUE);
													profileRepository.updateItem(birthdayGiftItem);
												}
											}
							}
						} catch (RepositoryException e) {
							logger.info(getClass().getSimpleName()+ "RepositoryException while updating the CouponBath and CouponCode back on birthdayGift Items ", e);
						} catch (VersionException e) {
							logger.info(getClass().getSimpleName()+ "VersionException while updating the CouponBath and CouponCode back on birthdayGift Items ",e);
						}
						
					}
					
				}
				
			}
			
		}
		
	}

	public boolean writeAudit(String prjName, boolean pSuccess, CatalogImportException pException) {

		MutableRepository auditRespo=(MutableRepository)((DigitalPublishingActionConfiguration) getActionConfiguration()).getAuditRepository();

		if(auditRespo==null) {
			logger.info( "Audit Repository is Null. Cannot write Audit record." );
			return false;
		}
		
		TransactionManager tm = ( (GSARepository)auditRespo ).getTransactionManager();
		String description = pSuccess ? "CouponBatchProcess with Project Name :: "+prjName +"   " + COUPON_BATCH_CHECKIN_FINISHED + " deployment at  :: " + new CurrentDate().getTimeAsTimestamp() : pException.getDescription();
		String status = pSuccess? COUPON_BATCH_CHECKIN_FINISHED : AUDIT_IMPORT_FAILURE;

		if( logger.isInfoEnabled()) {
			logger.info( "writing audit record with status:" + status + ", import date:");
			if( !pSuccess ) {
				logger.info( "exception description:" + pException.getDescription() );
			}
		}
		
		TransactionDemarcation td = new TransactionDemarcation();
		try {
			// start a new transaction
			td.begin( tm, TransactionDemarcation.REQUIRES_NEW );
			MutableRepositoryItem auditRec = (auditRespo ).createItem( "importAudit" );
			auditRec.setPropertyValue( BIRTHDAY_GIFT_ITEM_STATUS_PROPERTY_NAME, status );
			auditRec.setPropertyValue( "description", description );
			auditRec.setPropertyValue( "importDate", new Date() );
			auditRec.setPropertyValue( "importType", "autodeploy" );
			auditRec.setPropertyValue( "jobType", "couponBatchImport" );
			(auditRespo ).addItem( auditRec );
		} catch( TransactionDemarcationException e ) {
			logger.info("TransactionDemarcationException: ", e);
		} catch( Exception e ) {
			if( logger.isInfoEnabled() ) {
				logger.info( ERROR_WRITING_AUDIT_FAILURE );
				logger.info( "exception:" + e );
			}
			mException = new CatalogImportException( e, "Write Audit", e.getMessage() );
			handleException( mException );
			return false;
		} finally {
			try {
				td.end();
			} catch( TransactionDemarcationException e ) {
				logger.info("TransactionDemarcationException: ", e);
			}
		}
		return true;
	}

	public void handleException( CatalogImportException pException ) {
		String importStep = pException.getImportStep();
		String failureReason = pException.getFailureReason();
		String sourceException = ( pException.getSourceException() == null ) ? "none" : pException.getSourceException().toString();
		Object[] arguments = { DateUtils.formatDate(), importStep, failureReason, sourceException };
		String result = MessageFormat.format( IMPORT_EXCEPTION_STRING, arguments );
		if(logger.isInfoEnabled() ) {
			logger.info( result, pException);
		}
	}
}
