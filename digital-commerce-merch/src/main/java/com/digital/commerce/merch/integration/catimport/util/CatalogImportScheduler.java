package com.digital.commerce.merch.integration.catimport.util;

import atg.nucleus.*;
import atg.service.scheduler.*;

public class CatalogImportScheduler extends GenericService implements Schedulable {
	public CatalogImportScheduler() {}

	// Scheduler property
	Scheduler	scheduler;

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler( Scheduler pScheduler ) {
		this.scheduler = pScheduler;
	}

	// Schedule property
	Schedule	schedule;

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule( Schedule pSchedule ) {
		this.schedule = pSchedule;
	}

	// Schedulable method
	public void performScheduledTask( Scheduler scheduler, ScheduledJob job ) {
		if( isLoggingDebug() ) logDebug( "DEBUG: Hello!" );
	}

	// Start method
	int	jobId;

	public void doStartService() throws ServiceException {
		ScheduledJob job = new ScheduledJob( "hello", "Prints Hello!", getAbsoluteName(), getSchedule(), this, ScheduledJob.SCHEDULER_THREAD );
		jobId = getScheduler().addScheduledJob( job );
	}

	// Stop method
	public void doStopService() throws ServiceException {
		getScheduler().removeScheduledJob( jobId );
	}
}
