/* Created on September 15, 2008
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates */
package com.digital.commerce.merch.integration.catimport;

import com.digital.commerce.merch.integration.catimport.util.StringUtil;

import atg.nucleus.GenericService;
import atg.process.action.ActionException;
import atg.security.ThreadSecurityManager;
import atg.workflow.ActorAccessException;
import atg.workflow.MissingWorkflowDescriptionException;
import atg.workflow.OutcomeDescriptor;
import atg.workflow.TaskAccessRights;
import atg.workflow.TaskInfo;
import atg.workflow.WorkflowDescriptor;
import atg.workflow.WorkflowException;
import atg.workflow.WorkflowManager;
import atg.workflow.WorkflowView;

import java.util.Collection;
import java.util.Iterator;
import javax.ejb.EJBException;
import javax.servlet.ServletException;

/** @author Brenda Faulkner
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates */
@SuppressWarnings({"rawtypes"})
public class WorkflowOutcome extends GenericService {

	String			mSubject;

	WorkflowManager	mWorkflowManager;

	String			mOutcomeId;

	WorkflowView	mWorkflowView	= null;

	public WorkflowView getWorkflowView() {
		if( mWorkflowView == null ) {
			if( getWorkflowManager() != null ) {
				mWorkflowView = getWorkflowManager().getWorkflowView( ThreadSecurityManager.currentUser() );
			}
		}
		return mWorkflowView;
	}

	public void fireWorkflowOutcome( String pProcessId, String pOutcomeName ) throws ServletException {
		if( StringUtil.isNotSet( pProcessId ) ) { throw new ServletException( "ProcessId is not set" ); }
		if( StringUtil.isNotSet( pOutcomeName ) ) { throw new ServletException( "OutcomeName is not set" ); }
		TaskInfo ti = getTaskInfo( pProcessId, pOutcomeName );
		if( ti == null ) throw new ServletException( "Unable to find resource for:" + pProcessId );
		WorkflowDescriptor wd = ti.getTaskDescriptor().getWorkflow();

		try {
			getWorkflowView().fireTaskOutcome( wd.getProcessName(), wd.getSegmentName(), getSubject(), getOutcomeId(), 0 );
		} catch( ActionException e ) {
			if( isLoggingError() ) {
				logError( e );
			}
		} catch( ActorAccessException | EJBException | MissingWorkflowDescriptionException e ) {
			if( isLoggingError() ) {
				logError( e );
			}
			return;
		}
  }

	private TaskInfo getTaskInfo( String pProcessId, String pOutcomeName ) {
		TaskInfo info = null;
		Collection tasks = null;
		try {
			tasks = getWorkflowView().getActiveTasks( pProcessId, null, TaskAccessRights.EXECUTE, 0 );
		} catch( WorkflowException we ) {
			if( isLoggingDebug() ) logDebug( "DEBUG: " + we.getMessage() );
			return null;
		}
		Iterator iter = tasks.iterator();
		if( iter.hasNext() ) {
			info = (TaskInfo)iter.next();
			OutcomeDescriptor outcome = info.getTaskDescriptor().getOutcomeByName( pOutcomeName );
			String subject = info.getSubjectId();
			if( outcome != null ) {
				setOutcomeId( outcome.getOutcomeElementId() );
				setSubject( subject );
			}
			return info;
		} else {
			return null;
		}
	}

	private void setSubject( String pSubject ) {
		mSubject = pSubject;
	}

	private String getSubject() {
		return mSubject;
	}

	public void setWorkflowManager( WorkflowManager pWorkflowManager ) {
		mWorkflowManager = pWorkflowManager;
	}

	public WorkflowManager getWorkflowManager() {
		return mWorkflowManager;
	}

	private void setOutcomeId( String pOutcomeId ) {
		mOutcomeId = pOutcomeId;
	}

	private String getOutcomeId() {
		return mOutcomeId;
	}
}
