/******************************************************************************************************************************************************** Author : PA
 * Version : 1.0
 * Date Created :
 * 
 * Change history :
 * 
 * Task/Bug Date(mm-dd-yyyy) Author Change Description ***********************************************************************************************************************************************************/

package com.digital.commerce.merch.media.descriptor;

import java.io.Serializable;

import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;

/** This implementation get the products default sku
 * for the default SKU gets the color code and returns that value
 * In case of no color code default returns the BLACK color code
 * 
 * @author Professional Access 
 */
public class ProductFirstSkuColorCodeMapper extends ApplicationLoggingImpl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3937485186023452393L;

	/** This is a constant parameter name for the 'defaultSKU'.
	 * This constant is used to refer the defaultSKU property of the Product repository item. */
	private static final String	DEFAULT_SKU_PROPERTY		= "defaultSKU";

	/** This is a constant parameter name for the 'color'.
	 * This constant is used to refer the color property of the SKU repository item. */
	private static final String	COLOR_PROPERTY				= "color";

	/** This is a constant parameter name for the 'colorCode'.
	 * This constant is used to refer the colorCode property of the colorCode repository item. */
	private static final String	COLOR_CODE_PROPERTY			= "colorCode";

	private static final String	DISPLAY_NAME_PROPERTY		= "displayName";
	/** This is a constant parameter name for default color code , i.e '001'.
	 * This will be used to send the color code of BLACK in case of color Code is Null */
	private static final String	DEFAULT_COLOR_CODE			= "001";

	/** This is a constant parameter name for default color code , i.e '001'.
	 * This will be used to send the color code of BLACK in case of color Code is Null */
	private static final String	DEFAULT_COLOR_DISPLAY_NAME	= "NA";

	/** Property to hold mDefaultImageServerMediaDir. */
	private String				mDefaultImageServerMediaDir	= null;

	
	public ProductFirstSkuColorCodeMapper() {
		super(ProductFirstSkuColorCodeMapper.class.getName());
	}
	
	/** @return Returns the Default Image Server Media Directory path. */
	public String getDefaultImageServerMediaDir() {
		return mDefaultImageServerMediaDir;
	}

	/** @param pMediaNameMapper The Default Image Server Media Directory path to set. */
	public void setDefaultImageServerMediaDir( String pDefaultImageServerMediaDir ) {
		mDefaultImageServerMediaDir = pDefaultImageServerMediaDir;
	}

	/** Returns the color code of the Products default SKU
	 * 
	 * @param pProduct
	 * @return color code */
	public String getSKUColorCode( RepositoryItem pProduct ) {
		if( isLoggingDebug() ) {
			logDebug( "Begin ProductFirstSkuColorCodeMapper.getSKUColorCode() method ... " );
			logDebug( "Product: " + pProduct );
		}
		RepositoryItem skuItem = null;
		RepositoryItem colorItem = null;
		String colorCode = null;

		if( pProduct != null ) skuItem = (RepositoryItem)pProduct.getPropertyValue( DEFAULT_SKU_PROPERTY );
		if( skuItem != null ) colorItem = (RepositoryItem)skuItem.getPropertyValue( COLOR_PROPERTY );
		if( colorItem != null ) colorCode = (String)colorItem.getPropertyValue( COLOR_CODE_PROPERTY );
		if( isLoggingDebug() ) logDebug( "End of ProductFirstSkuColorCodeMapper.getSKUColorCode() method ... " );
		if( colorCode != null ) {
			return colorCode;
		} else {
			// Always return Black as default
			return DEFAULT_COLOR_CODE;
		}
	}

	public String getColorDisplayName( RepositoryItem pProduct ) {
		if( isLoggingDebug() ) {
			logDebug( "Begin ProductFirstSkuColorCodeMapper.getColorDisplayName() method ... " );
			logDebug( "Product: " + pProduct );
		}
		RepositoryItem skuItem = null;
		RepositoryItem colorItem = null;
		String colorDisplayName = null;

		if( pProduct != null ) skuItem = (RepositoryItem)pProduct.getPropertyValue( DEFAULT_SKU_PROPERTY );
		if( skuItem != null ) colorItem = (RepositoryItem)skuItem.getPropertyValue( COLOR_PROPERTY );
		if( colorItem != null ) colorDisplayName = (String)colorItem.getPropertyValue( DISPLAY_NAME_PROPERTY );
		if( isLoggingDebug() ) logDebug( "End of ProductFirstSkuColorCodeMapper.getColorDisplayName() method ... " );
		if( colorDisplayName != null ) {
			return colorDisplayName;
		} else {
			// Always return Black as default
			return DEFAULT_COLOR_DISPLAY_NAME;
		}
	}
}
