package com.digital.commerce.merch.integration.catimport.repository;

/**
 * Extensions to ATG RepositoryUtils
 */

import atg.adapter.gsa.ChangeAwareList;
import atg.adapter.gsa.ChangeAwareValue;
import atg.adapter.gsa.GSAItem;
import atg.adapter.gsa.GSAItemDescriptor;
import atg.adapter.version.CurrentVersionItem;
import atg.beans.DynamicPropertyDescriptor;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryPropertyDescriptor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"rawtypes", "unchecked"})
public class RepositoryUtils extends atg.repository.RepositoryUtils {

  private static ApplicationLogging sLogger = ClassLoggingFactory.getFactory()
      .getLoggerForClass(RepositoryUtils.class);

  // -------------------------------------
  // Methods
  // -------------------------------------

  /**
   * Gets an repository item for the cloning operation
   *
   * @param pCloneInfo the info supporting the cloning
   * @return the updated cloneInfo
   * @throws RepositoryException
   */
  public static CloneInfo getItemForExport(CloneInfo pCloneInfo) throws RepositoryException {
    if (null == pCloneInfo) {
      if (sLogger.isLoggingError()) {
        sLogger.logError("ERROR:null cloneInfo. ");
      }
      return null;
    }
    pCloneInfo.setLoggingDebug(true);
    MutableRepositoryItem itemForExport = null;
    if (sLogger.isLoggingDebug()) {
      sLogger.logDebug(
          "getDestinationRepository(" + pCloneInfo.getDestinationRepository().getRepositoryName()
              + ").getItemForUpdate(" + pCloneInfo.getItemId() + "," + pCloneInfo.getItemType()
              + ")");
    }
    try {

      // see if this item exists in destination. If so , update the item
      RepositoryItem item = pCloneInfo.getDestinationRepository()
          .getItem(pCloneInfo.getItemId(), pCloneInfo.getItemType());
      if (item != null) {
        if (sLogger.isLoggingDebug()) {
          sLogger
              .logDebug("item already exists in export repository. id:" + item.getRepositoryId());
        }
        itemForExport = pCloneInfo.getDestinationRepository()
            .getItemForUpdate(pCloneInfo.getItemId(), pCloneInfo.getItemType());
        if (sLogger.isLoggingDebug()) {
          if (itemForExport == null) {
            sLogger.logDebug(pCloneInfo.getDestinationRepository().getRepositoryName());
            sLogger
                .logDebug("Could not find the item in the repository id: " + pCloneInfo.getItemId()
                    + " with type " + pCloneInfo.getItemType());
          } else {
            sLogger.logDebug(
                "Found the item for export in repository: " + pCloneInfo.getDestinationRepository()
                    .getRepositoryName());
          }
        }
      }
    }
    // versioned repository will throw exception on getItemForUpdate if the item doesn't exist.
    // non-versioned repository will not.
    // so catch exception and create a new item
    catch (RepositoryException e) {
      if (sLogger.isLoggingError()) {
        sLogger.logError("!!!!!!!!! generated exception:" + e);
      }
      throw e;
    }

    if (itemForExport == null) {
      // it doesn't exist so is a new item
      if (pCloneInfo.isLoggingDebug()) {
        sLogger.logDebug(
            "item does not already exist in export repository. Creating one with id:" +
                pCloneInfo.getItemId());
      }
      try {
        itemForExport = pCloneInfo.getDestinationRepository()
            .createItem(pCloneInfo.getItemId(), pCloneInfo.getItemType());
        if (pCloneInfo.isLoggingDebug()) {
          if (null == itemForExport) {
            sLogger.logDebug("create returned null");
          } else {
            sLogger.logDebug("create returned:" + itemForExport.getRepositoryId());
          }
        }
      }
      // versioned repository will throw exception on getItemForUpdate if the item doesn't exist.
      // non-versioned repository will not.
      // so catch exception and create a new item
      catch (RepositoryException e) {
        if (sLogger.isLoggingError()) {
          sLogger.logError("!!!!!!!!!!!!!!!!!!!!create item generated exception:" + e);
        }
        throw e;
      }
      pCloneInfo.setUpdate(false);
      if (sLogger.isLoggingDebug()) {
        sLogger.logDebug(
            "cloneItem set pUpdate in CloneInfo for " + pCloneInfo.getItemType() + " : "
                + pCloneInfo.getItemId() + " to false (TAA).");
      }
    } else { // it exists so is an update
      if (sLogger.isLoggingDebug()) {
        // taa sLogger.logDebug("setting update to true");
        sLogger.logDebug(
            "cloneItem set pUpdate in CloneInfo for " + pCloneInfo.getItemType() + " : "
                + pCloneInfo.getItemId() + " to true (TAA).");
      }
      pCloneInfo.setUpdate(true);
    }

    pCloneInfo.setClone(itemForExport);
    return pCloneInfo;
  }

  /**
   * Copies flat (non object reference) properties from source to destination
   *
   * @param pSourceItem
   * @param pDestinationItem
   * @param pPropertyNames
   * @param isLoggingDebug
   * @param itemDescriptorSetMap
   */
  public static void copyItemProperties(RepositoryItem pSourceItem,
      MutableRepositoryItem pDestinationItem, String[] pPropertyNames, boolean isLoggingDebug,
      Map<RepositoryItemDescriptor, Set<String>> itemDescriptorSetMap) {
    for (String pPropertyName : pPropertyNames) {
      if (isLoggingDebug) {
        sLogger.logDebug("Copying property with name: " + pPropertyName);
      }
      Object sourcePropertyValue = pSourceItem.getPropertyValue(pPropertyName);

      if (sourcePropertyValue != null) {
        if (isLoggingDebug) {
          sLogger.logDebug("Source property type  is " + sourcePropertyValue.getClass().getName());
          sLogger.logDebug("Source property value is " + sourcePropertyValue.toString());
        }

        /* This section is for debugging the bug in ATG where it is not
         * setting the property even though the isAssignableFrom is true.
         * This should not be run in production. */
        RepositoryItemDescriptor rid;
        try {
          rid = pDestinationItem.getItemDescriptor();
          DynamicPropertyDescriptor pd = rid.getPropertyDescriptor(pPropertyName);
          if (isLoggingDebug) {
            // uncomment below to print out property values
            sLogger.logDebug("Source value: " + sourcePropertyValue.toString());
            sLogger.logDebug("Source class: " + sourcePropertyValue.getClass());
            sLogger.logDebug("Dest class: " + pd.getPropertyType() + ",");
            sLogger.logDebug(
                "Dest display-name: " + pd.getDisplayName() + "," + pd.getDisplayNameResource());
            sLogger.logDebug(
                "isAssign: " + pd.getPropertyType()
                    .isAssignableFrom(sourcePropertyValue.getClass())
                    + ",");
            sLogger.logDebug("instanceof: " + (pd instanceof RepositoryPropertyDescriptor) + ",");
            sLogger.logDebug("\"" + sourcePropertyValue.getClass().toString() + "\" xx");
          }

        } catch (RepositoryException e) {
          if (sLogger.isLoggingError()) {
            sLogger.logError("RepositoryException-->", e);
          }
        }
      }

      boolean isAssignable = (sourcePropertyValue != null && GSAItem.class
          .isAssignableFrom(sourcePropertyValue.getClass()));

      // check if it is a repository item or a regular data type
      if (isAssignable) {
        // for repository item type, get the current version item
        if (isLoggingDebug) {
          sLogger.logDebug("Casting sourcePropertyValue to atg.adapter.gsa.GSAItem");
        }
        RepositoryItem item = (RepositoryItem) sourcePropertyValue;
        MutableRepository pRepository = (MutableRepository) pDestinationItem.getRepository();
        CurrentVersionItem cvi;
        try {
          // get the current version item
          cvi = (CurrentVersionItem) pRepository
              .getItem(item.getRepositoryId(), item.getItemDescriptor().getItemDescriptorName());
          // set the current version item as the reference
          pDestinationItem.setPropertyValue(pPropertyName, cvi);
        } catch (RepositoryException e) {
          if (sLogger.isLoggingError()) {
            sLogger.logError("RepositoryException------>", e);
          }
        }
      } else {
        if (isLoggingDebug) {
          // for regular data type, set the value directly
          sLogger.logDebug(
              "Regular data type, Attempting to set the value directly for " + pPropertyName);
        }
        try {
          if (sourcePropertyValue != null && isCollectionOrMap(sourcePropertyValue.getClass())) {
            ChangeAwareValue sourceItemValue = (ChangeAwareValue) sourcePropertyValue;
            if (sourceItemValue instanceof ChangeAwareList) {
              ChangeAwareList sourceItemList = (ChangeAwareList) sourceItemValue;
              if (sourceItemList.size() > 0) {
                RepositoryItem sourceItem = (RepositoryItem) sourceItemList.get(0);

                GSAItemDescriptor repDescName = (GSAItemDescriptor) sourceItem.getItemDescriptor();
                List<RepositoryItem> destItemList = null;
                if (null != itemDescriptorSetMap && itemDescriptorSetMap.containsKey(repDescName)) {
                  destItemList = copyListItemProperties(sourceItemValue,
                      (MutableRepository) pDestinationItem.getRepository());
                } else {
                  if (isLoggingDebug) {
                    sLogger.logDebug("Could not get the value for the list properties");
                  }
                }
                pDestinationItem.setPropertyValue(pPropertyName, destItemList);
              }
            } else {
              // for regular data type, set the value directly
              //Take care of Map
              if (isLoggingDebug) {
                sLogger.logDebug("Regular data type, Attempting to set the value directly for "
                    + pPropertyName);
              }
              pDestinationItem.setPropertyValue(pPropertyName, sourcePropertyValue);
            }
          } else {
            // for regular data type, set the value directly
            if (isLoggingDebug) {
              sLogger.logDebug(
                  "Regular data type, Attempting to set the value directly for "
                      + pPropertyName);
            }
            pDestinationItem.setPropertyValue(pPropertyName, sourcePropertyValue);
          }
        } catch (RepositoryException re) {
          if (sLogger.isLoggingError()) {
            sLogger.logError("Repository Exception: " + pPropertyName, re);
          }
        } catch (IllegalArgumentException iae) {
          if (sLogger.isLoggingError()) {
            sLogger.logError("Error setting value: " + pPropertyName, iae);
          }
        }
      }
    }

  }

  /**
   * @param pSourceItem
   * @param destRepo
   * @return List
   */
  private static List<RepositoryItem> copyListItemProperties(Object pSourceItem,
      MutableRepository destRepo) {
    List<RepositoryItem> destChangeAwareList = new ArrayList();
    List<RepositoryItem> sourceItemList = (ChangeAwareList) pSourceItem;
    for (RepositoryItem sourceItem : sourceItemList) {
      CloneInfo cloneInfo = new CloneInfo(sourceItem, destRepo);
      MutableRepositoryItem exportItem;
      try {
        cloneInfo = RepositoryUtils.getItemForExport(cloneInfo);
      } catch (RepositoryException e) {
        if (sLogger.isLoggingError()) {
          sLogger.logError("Exception occurred while copying list item properties ", e);
        }
      }  // should really be called "getItemForClone"
      if (null != cloneInfo) {
        exportItem = cloneInfo.getClone();
        destChangeAwareList.add(exportItem);
      }
    }
    return destChangeAwareList;
  }

  /**
   * @param pPropertyType
   * @return true or false
   */
  private static boolean isCollectionOrMap(Class pPropertyType) {
    if (pPropertyType != null) {
      if (List.class.isAssignableFrom(pPropertyType)) {
        return true;
      } else if (Set.class.isAssignableFrom(pPropertyType)) {
        return true;
      } else {
        return Map.class.isAssignableFrom(pPropertyType);
      }
    }
    return false;
  }

}
