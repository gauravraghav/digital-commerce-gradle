/**
 *
 */
package com.digital.commerce.merch.media.descriptor;

import atg.adapter.gsa.ChangeAwareMap;
import atg.nucleus.Nucleus;
import atg.repository.*;

import java.util.*;

import com.digital.commerce.common.logger.DigitalLogger;

@SuppressWarnings({"rawtypes","unchecked"})
public class BuildColorSwatchesListPropertyDescriptor extends RepositoryPropertyDescriptor

{
	private transient DigitalLogger logger = DigitalLogger.getLogger(BuildColorSwatchesListPropertyDescriptor.class);
    private static final long		serialVersionUID	= -8795816015069305785L;

    protected static final String	TYPE_NAME			= "buildColorSwatchesListPropertyDescriptor";

    protected static final String	COLLECTION			= "collection";

    protected static final String	PROPERTY			= "property";

    protected static final String	SUB_PROPERTY		= "colorCode";

    /** This is a constant parameter name for the 'mediaNameMapper' component
     * associated in the Repository definition file. */
    protected static final String	PATH_NAME_PROPERTY	= "mediaNameMapper";

    /** Property to hold 'mMediaNameMapper'component. */
    protected String				mMediaNameMapper	= null;

    /** @return Returns the SkuMediaNameMapper. */
    public String getSkuMediaNameMapper() {
        return mMediaNameMapper;
    }

    /** @param pMediaNameMapper The ProductFirstSkuColorCodeMapper to set. */
    public void setSkuMediaNameMapper( String pMediaNameMapper ) {
        mMediaNameMapper = pMediaNameMapper;
    }

    static {
        RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, BuildColorSwatchesListPropertyDescriptor.class );
    }

    public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
        // if (pValue != null && pValue instanceof Integer)
        // {
        // return pValue;
        // }
        String productId = (String)pItem.getPropertyValue( "id" );
        String property = (String)getValue( COLLECTION );
        String subProperty = (String)getValue( PROPERTY );
        Set subPropSet = new HashSet();
        String defaultImageServer = null;

        if( getSkuMediaMapper().getDefaultImageServerMediaDir() != null ) {
            defaultImageServer = getSkuMediaMapper().getDefaultImageServerMediaDir();
        } else {
            defaultImageServer = "https://dsw.scene7.com/is/image/DSWShoes/";
        }

        Object list = pItem.getPropertyValue( property );

        HashMap<String,MutableRepositoryItem> swatchMap = new HashMap<>();

        if( list instanceof Collection ) {
            Collection propList = (Collection)list;
            if( subProperty == null || subProperty.trim().length() == 0 ) {
                return new ChangeAwareMap(new HashMap(),pItem,"colorSwatchesString");
            } else {

                Iterator i = propList.iterator();
                while( i.hasNext() ) {
                    RepositoryItem prop = (RepositoryItem)i.next();
                    Object subPropVal = prop.getPropertyValue( subProperty );
                    subPropSet.add( subPropVal );
                }

                if( subPropSet != null && subPropSet.size() > 0 ) {

                    Iterator colors = subPropSet.iterator();

                    while( colors.hasNext() ) {
                        RepositoryItem color = (RepositoryItem)colors.next();
                        if( color != null ) {




                            String colorId = (String)color.getPropertyValue( "id" );
                            String colorCode = (String)color.getPropertyValue( "colorCode" );
                            String displayName = (String)color.getPropertyValue( "displayName" );
                            String altText = colorId + ": " + displayName + ", " + colorCode;
                            StringBuilder finalURL = new StringBuilder();
                            //finalURL.append( "<img src='" );
                            finalURL.append( defaultImageServer );
                            finalURL.append( productId );
                            finalURL.append( "_" );
                            finalURL.append( colorCode );
                            finalURL.append( "_ss_sw?$swatches$" );
                            //finalURL.append( "_ss_sw?$swatches$' alt=\"" );
                            //finalURL.append( altText );
                            //finalURL.append( "\"" );
                            //finalURL.append( " title=\"" );
                            //finalURL.append( altText );
                            //finalURL.append( "\" width=\"41\" height=\"41\" class=\"mediaImage\" />" );
                            //String colorSwatch = finalURL.toString();
                            //colorSwatchesSet = colorSwatchesSet + colorSwatch + "&nbsp;";

                            try{
                                Nucleus nucleus = Nucleus.getGlobalNucleus();
                                //MutableRepository mediaRepository = (MutableRepository)nucleus.resolveName("/atg/commerce/catalog/ProductCatalog");
                                MutableRepository mediaRepository = (MutableRepository)nucleus.resolveName("/atg/content/media/MediaRepository");

                                MutableRepositoryItem mediaItem = mediaRepository.createItem("media-external");
                                mediaItem.setPropertyValue("url", finalURL.toString());
                                mediaItem.setPropertyValue("name", finalURL.toString());
                                mediaItem.setPropertyValue("path", "/colorFolder");

                                swatchMap.put(altText,mediaItem);

                            }
                            catch (Exception ex){
                            	logger.error("Error creating media: ",ex);
                            }
                        }
                    }

                }
                // pItem.setPropertyValueInCache(this, colorSwatchesSet);
                ChangeAwareMap value = new ChangeAwareMap(Collections.unmodifiableMap(swatchMap),pItem,getName());
                if(logger.isDebugEnabled()) {
                	logger.debug("Outputting Unmodifiable, NonVersioned and NonPersistent ChangeAwareMap");
                }
                return value;
            }
        } else {
            throw new IllegalArgumentException( "collection must be an instance of java.util.Collection" );
        }
    }

    public String getTypeName() {
        return TYPE_NAME;
    }

    /* @see atg.repository.RepositoryPropertyDescriptor#setValue(java.lang.String, java.lang.Object) */
    public void setValue( String pAttributeName, Object pValue ) {
        super.setValue( pAttributeName, pValue );
        if( ( pValue == null ) || ( pAttributeName == null ) ) { return; }
        if( pAttributeName.equalsIgnoreCase( PATH_NAME_PROPERTY ) ) {
            setSkuMediaNameMapper( (String)pValue );
        }
    }

    /** @return Returns the ProductFirstSkuColorCodeMapper by
     *         resolving it into Nucleus component */
    public ProductFirstSkuColorCodeMapper getSkuMediaMapper() {
        if( getSkuMediaNameMapper() == null ) {
            return null;
        } else {
            Nucleus nucleus = Nucleus.getGlobalNucleus();
            if( nucleus != null ) {
                return (ProductFirstSkuColorCodeMapper)nucleus.resolveName( getSkuMediaNameMapper() );
            } else {
                return null;
            }
        }
    }

    @Override
    public boolean isVersionable() {
        return false;
    }

    @Override
    public boolean isPersistent() {
        return false;
    }

    @Override
    public boolean isDerived() {
        return true;
    }
}
