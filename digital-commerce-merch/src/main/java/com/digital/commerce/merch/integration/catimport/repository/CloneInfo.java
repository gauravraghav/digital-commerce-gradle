package com.digital.commerce.merch.integration.catimport.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;

/** Bean containing properties supporting clone operation
 * 
 * @author AndyShapiro */
@SuppressWarnings({"rawtypes"})
public class CloneInfo {
	private static transient DigitalLogger  logger = DigitalLogger.getLogger(CloneInfo.class);
	// -------------------------------------
	// Properties
	// -------------------------------------

	// -------------------------------------
	// property: SourceItem
	RepositoryItem	mSourceItem;

	/** @return the SourceItem */
	public RepositoryItem getSourceItem() {
		return mSourceItem;
	}

	/** set the SourceItem
	 * 
	 * @param pSource */
	public void setSourceItem( RepositoryItem pSource ) {
		mSourceItem = pSource;
	}

	// -------------------------------------
	// property: DeepCopyExceptions
	// Map where key is item type and value are properties within that item type to exclude from deepCopy
	// if value is null, indicates just copy the reference to the item, not the item itself

	Map	mDeepCopyExceptions;

	/** @return DeepCopyExceptions */
	public Map getDeepCopyExceptions() {
		return mDeepCopyExceptions;
	}

	/** set DeepCopyExceptions
	 * 
	 * @param pPropExceptions */
	public void setPropExceptions( Map pPropExceptions ) {
		mDeepCopyExceptions = pPropExceptions;
	}

	// -------------------------------------
	// property: ExcludedProperties
	// Map where key is item type and value are properties within that item type to exclude from cloning
	Map	mExcludedProperties;

	/** @return ExcludedProperties */
	public Map getExcludedProperties() {
		return mExcludedProperties;
	}

	/** set ExcludedProperties
	 * 
	 * @param pExcludedProperties */
	public void setExcludedProperties( Map pExcludedProperties ) {
		mExcludedProperties = pExcludedProperties;
	}

	// -------------------------------------
	// property: ExcludedPropertiesIfUpdate
	// Map where key is item type and value are properties within that item type to exclude from cloning if this operation is an update
	// as opposed to a create
	Map	mExcludedPropertiesIfUpdate;

	/** @return ExcludedPropertiesIfUpdate */
	public Map getExcludedPropertiesIfUpdate() {
		return mExcludedPropertiesIfUpdate;
	}

	/** set ExcludedPropertiesIfUpdate
	 * 
	 * @param pExcludedPropertiesIfUpdate */
	public void setExcludedPropertiesIfUpdate( Map pExcludedPropertiesIfUpdate ) {
		mExcludedPropertiesIfUpdate = pExcludedPropertiesIfUpdate;
	}

	// -------------------------------------
	// property: DestinationRepository - the repository receiving the clone

	MutableRepository	mDestRepository;

	public MutableRepository getDestinationRepository() {
		return mDestRepository;
	}

	public void setDestinationRepository( MutableRepository pDestRepository ) {
		mDestRepository = pDestRepository;
	}

	// -------------------------------------
	// property: DeepCopy - flag indicating if is deep copy (true) or shallow copy (false)
	boolean	mDeepCopy;

	public boolean isDeepCopy() {
		return mDeepCopy;
	}

	public void setDeepCopy( boolean pDeepCopy ) {
		mDeepCopy = pDeepCopy;
	}

	/** will this cloning result in an update or create */
	// -------------------------------------
	// property: Update - flag indicating if update (true) or create (false)
	private boolean	mUpdate;

	public boolean isUpdate() {
		return mUpdate;
	}

	public void setUpdate( boolean pUpdate ) {
		mUpdate = pUpdate;
	}

	// -------------------------------------
	// property: ClonedItem - the clone
	MutableRepositoryItem	mClone;

	public MutableRepositoryItem getClone() {
		return mClone;
	}

	public void setClone( MutableRepositoryItem pClonedItem ) {
		mClone = pClonedItem;
	}

	// -------------------------------------
	// property: ContainedItemTypesToClone - which referenced item types within this cloning operation to clone
	List	mContainedItemTypesToClone	= new ArrayList();

	public List getContainedItemTypesToClone() {
		return mContainedItemTypesToClone;
	}

	public void setContainedItemTypesToClone( List pContainedItemTypesToClone ) {
		mContainedItemTypesToClone = pContainedItemTypesToClone;
	}

	// -------------------------------------
	// property: LoggingDebug - flag indicating whether cloning is being debugged
	boolean	mLoggingDebug;

	public boolean isLoggingDebug() {
		return mLoggingDebug;
	}

	public void setLoggingDebug( boolean pLoggingDebug ) {
		mLoggingDebug = pLoggingDebug;
	}

	// -------------------------------------
	// Constructors

	public CloneInfo( RepositoryItem pSource, boolean pDeepCopy, Map pPropExceptions, Map pExcludedProperties, Map pExcludedUpdateProperties, MutableRepository pDestRepository, String pItemId, List pImportContainedTypes ) {
		super();
		setSourceItem( pSource );
		setDeepCopy( pDeepCopy );
		setPropExceptions( pPropExceptions );
		setExcludedProperties( pExcludedProperties );
		setExcludedPropertiesIfUpdate( pExcludedUpdateProperties );
		setDestinationRepository( pDestRepository );
		setContainedItemTypesToClone( pImportContainedTypes );
	}

	public CloneInfo( RepositoryItem pSource, MutableRepository pDestRepository ) {
		super();
		setSourceItem( pSource );
		setDeepCopy( false );
		setPropExceptions( null );
		setExcludedProperties( null );
		setExcludedPropertiesIfUpdate( null );
		setDestinationRepository( pDestRepository );
		setContainedItemTypesToClone( null );
	}

	// -------------------------------------
	// Methods

	public String getItemId() {
		return ( null == getSourceItem() ) ? null : getSourceItem().getRepositoryId();
	}

	/** @return itemDescriptor */
	public RepositoryItemDescriptor getItemDescriptor() {
		try {
			return ( null == getSourceItem() ) ? null : getSourceItem().getItemDescriptor();
		} catch( RepositoryException e ) {
			// TODO Auto-generated catch block
			logger.error("RepositoryException: ", e);
			return null;
		}
	}

	/** gets the base ItemType. If this is a subType, the root superType will be returned
	 * 
	 * @return the base ItemType */
	public String getItemType() {
		return ( null == getItemDescriptor() ) ? null : getItemDescriptor().getItemDescriptorName(); // TAA

		// return (null==getItemDescriptor())?null:getDescriptorName(getItemDescriptor());
	}

	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append( "cloneInfo for item type:" ).append( getItemType() );
		buf.append( " source item id:" ).append( getSourceItem().getRepositoryId() );
		buf.append( " cloning to repository:" ).append( getDestinationRepository().getRepositoryName() );
		if( null != getClone() ) buf.append( " clone:" ).append( getClone().getRepositoryId() );
		if( isUpdate() )
			buf.append( " is update" );
		else
			buf.append( " is create" );
		return buf.toString();
	}

}
