package com.digital.commerce.merch.deployment.event;

import java.util.Arrays;
import java.util.Date;

import org.apache.commons.lang.ArrayUtils;
import com.digital.commerce.common.logger.DigitalLogger;
import atg.deployment.DeploymentConstants;
import atg.deployment.common.event.DeploymentEmailer;
import atg.deployment.common.event.DeploymentEvent;
import atg.service.email.SMTPEmailSender;

public class DigitalDeploymentEmailer extends DeploymentEmailer {

	String mToOncallAddress;
	String[] errorTargetNames;
	String errorStateName;
	boolean enableOncallPager;
	private static final DigitalLogger	logger	= DigitalLogger.getLogger( DigitalDeploymentEmailer.class );

	public String getErrorStateName() {
		return errorStateName;
	}

	public void setErrorStateName(String errorStateName) {
		this.errorStateName = errorStateName;
	}

	public String[] getErrorTargetNames() {
		return errorTargetNames;
	}

	public void setErrorTargetNames(String[] errorTargetNames) {
		this.errorTargetNames = errorTargetNames;
	}

	public void setToOncallAddress(String pToOncallAddress) {
		mToOncallAddress = pToOncallAddress;
	}

	public String getToOncallAddress() {
		return mToOncallAddress;
	}

	public boolean isEnableOncallPager() {
		return enableOncallPager;
	}

	public void setEnableOncallPager(boolean enableOncallPager) {
		this.enableOncallPager = enableOncallPager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * atg.deployment.common.event.DeploymentEmailer#deploymentEvent(atg.deployment
	 * .common.event.DeploymentEvent) Overriden the method to send alert to
	 * oncall pager for PRODUCTION deployment failures so that things can be
	 * fixed sooner than waiting for the morning as we do not do BCC deployment
	 * during day time
	 */
	@Override
	public void deploymentEvent(DeploymentEvent pEvent) {
		StringBuilder sb = new StringBuilder();
		sb.append("enableOncallPager:").append(enableOncallPager)
				.append(",errorStateName:").append(errorStateName)
				.append(",errorTargetNames:").append(Arrays.toString(errorTargetNames))
				.append(",mToOncallAddress:").append(mToOncallAddress)
				.append(",mFromAddress:").append(getFromAddress())
				.append(",mToAddress:").append(getToAddress())
				.append(",active:").append(isActive());
		logger.info("BCC email data:" + sb.toString());
		if (isEnableOncallPager()) {
			SMTPEmailSender emailSender = getSMTPEmailSender();
			String fromAddress = getFromAddress();
			String onCallAddress = getToOncallAddress();
			logger.info("On-call pager enabled, attempting to send pager to "
					+ onCallAddress);
			try {
				if (emailSender != null
						&& fromAddress != null
						&& onCallAddress != null
						&& ArrayUtils.contains(errorTargetNames,
								pEvent.getTarget())
						&& isActive()
						&& (DeploymentEvent.stateToString(pEvent.getNewState())
								.startsWith(errorStateName))) {
					logger.info("Error during BCC deployment and all information for sending out pager available");
					String subject = DeploymentEvent.stateToString(pEvent
							.getNewState());
					StringBuilder body = new StringBuilder();

					body.append("target : ")
							.append(pEvent.getTarget())
							.append("\n")
							.append("new state : ")
							.append(DeploymentEvent.stateToString(pEvent
									.getNewState()))
							.append("\n")
							.append("")
							.append((pEvent.getErrorMessage() == null ? ""
									: "error message : "
											+ pEvent.getErrorMessage() + "\n"))
							.append("deployment ID : ")
							.append(pEvent.getDeploymentID())
							.append("\n")
							.append("deployment begin timestamp : ")
							.append(new Date(pEvent
									.getDeploymentBeginTimestamp()))
							.append("\n")
							.append("deployment project IDs : ")
							.append(this.stringArrToString(pEvent
									.getDeploymentProjectIDs()))
							.append("\n")
							.append("deployment type : ")
							.append(((pEvent.getDeploymentType() == DeploymentConstants.TYPE_FULL ? "TYPE_FULL"
									: "TYPE_INCREMENTAL")))
							.append("\n")
							.append("deployment mode : ")
							.append((pEvent.getDeploymentMode() == DeploymentConstants.MODE_ONLINE ? "MODE_ONLINE"
									: "MODE_SWITCH"))
							.append("\n")
							.append("deployment server : ")
							.append(pEvent.getDeploymentServer())
							.append("\n")
							.append("deployment scheduled : ")
							.append(pEvent.isDeploymentScheduled())
							.append("\n")
							.append("deployment create initiator : ")
							.append(pEvent.getDeploymentCreateInitiator())
							.append("\n")
							.append("")
							.append((pEvent.getDeploymentStopInitiator() == null ? ""
									: "deployment stop initiator : "
											+ pEvent.getDeploymentStopInitiator()
											+ "\n")).append("");

					emailSender.sendEmailMessage(fromAddress, onCallAddress,
							subject, body.toString());
				} else {
					logger.info("Information for sending out pager not available, just sending standard email");
					super.deploymentEvent(pEvent);
				}
			} catch (Exception ee) {
				logger.info("Exception while attempting to send pager, just sending standard email");
			
				if (null!=emailSender && emailSender.isLoggingError())
					emailSender.logError(ee);
				super.deploymentEvent(pEvent);
				
			}
		} else {
			logger.info("On-call pager not enabled, just sending standard email");
			super.deploymentEvent(pEvent);
		}
	}

	/**
	 * Prints the elements of the array into a single String.
	 **/
	private String stringArrToString(String[] pArray) {
		if (pArray == null)
			return null;

		StringBuilder sb = new StringBuilder("[ ");
		for (int i = 0; i < pArray.length; i++) {
			if (i > 0)
				sb.append(", ");
			sb.append(pArray[i]);
		}
		sb.append(" ]");
		return sb.toString();
	}
}
