package com.digital.commerce.merch.integration.catimport.util;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.ServiceException;

/** contains useful static methods for MNSGenricService to use
 * 
 * @author ashapiro */

public class ServiceHelper {
	public static final void assertConfigNotNull( final Object pProperty, final String pPropertyName ) throws ServiceException {
		if( null == pProperty ) { throw new ServiceException( "Cannot start component. Invalid null property: " + pPropertyName ); }
	}

	/** a utility method to check for proper non zero configuration */
	public static final void assertConfigNotZero( final int pProperty, final String pPropertyName ) throws ServiceException {
		if( 0 == pProperty ) { throw new ServiceException( "Cannot start component. Invalid Zero property: " + pPropertyName ); }
	}

	public static final void assertConfigsNotNull( Object pObjectToCheck, String[] pPropertiesToCheck ) throws ServiceException {
		if( null != pPropertiesToCheck ) {
			// check for required configurations
			for( int i = pPropertiesToCheck.length - 1; i > -1; i-- ) {
				try {
					Object value = DynamicBeans.getPropertyValue( pObjectToCheck, pPropertiesToCheck[i] );
					ServiceHelper.assertConfigNotNull( value, pPropertiesToCheck[i] );
				} catch( PropertyNotFoundException e ) {
					String msg = pPropertiesToCheck[i] + " is not a valid property name. Please check RequiredConfigurations.";
					throw new ServiceException( msg, e );
				}
			}
		}

	}

}
