/**
 * 
 */
package com.digital.commerce.merch.media.descriptor;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import atg.nucleus.Nucleus;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

@SuppressWarnings({"rawtypes","unchecked"})
public class BuildColorSwatchesStringPropertyDescriptor extends RepositoryPropertyDescriptor

{

	private static final long		serialVersionUID	= -8795816015069305786L;

	protected static final String	TYPE_NAME			= "buildColorSwatchesStringPropertyDescriptor";

	protected static final String	COLLECTION			= "collection";

	protected static final String	PROPERTY			= "property";

	protected static final String	SUB_PROPERTY		= "colorCode";

	/** This is a constant parameter name for the 'mediaNameMapper' component
	 * associated in the Repository definition file. */
	protected static final String	PATH_NAME_PROPERTY	= "mediaNameMapper";

	/** Property to hold 'mMediaNameMapper'component. */
	protected String				mMediaNameMapper	= null;

	/** @return Returns the SkuMediaNameMapper. */
	public String getSkuMediaNameMapper() {
		return mMediaNameMapper;
	}

	/** @param pMediaNameMapper The ProductFirstSkuColorCodeMapper to set. */
	public void setSkuMediaNameMapper( String pMediaNameMapper ) {
		mMediaNameMapper = pMediaNameMapper;
	}

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, BuildColorSwatchesStringPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		// if (pValue != null && pValue instanceof Integer)
		// {
		// return pValue;
		// }
		String productId = (String)pItem.getPropertyValue( "id" );
		String property = (String)getValue( COLLECTION );
		String subProperty = (String)getValue( PROPERTY );
		Set subPropSet = new HashSet();
		String colorSwatchesSet = "";
		String defaultImageServer = null;

		if( getSkuMediaMapper().getDefaultImageServerMediaDir() != null ) {
			defaultImageServer = getSkuMediaMapper().getDefaultImageServerMediaDir();
		} else {
			defaultImageServer = "https://a248.e.akamai.net/f/248/9086/10h/origin-d2.scene7.com/is/image/DSWShoes/";
		}

		Object list = pItem.getPropertyValue( property );
		if( list instanceof Collection ) {
			Collection propList = (Collection)list;
			if( subProperty == null || subProperty.trim().length() == 0 ) {
				return colorSwatchesSet;
			} else {

				Iterator i = propList.iterator();
				while( i.hasNext() ) {
					RepositoryItem prop = (RepositoryItem)i.next();
					Object subPropVal = prop.getPropertyValue( subProperty );
					subPropSet.add( subPropVal );
				}

				if( subPropSet != null && subPropSet.size() > 0 ) {

					Iterator colors = subPropSet.iterator();

					while( colors.hasNext() ) {
						RepositoryItem color = (RepositoryItem)colors.next();
						if( color != null ) {
							String colorId = (String)color.getPropertyValue( "id" );
							String colorCode = (String)color.getPropertyValue( "colorCode" );
							String displayName = (String)color.getPropertyValue( "displayName" );
							String altText = colorId + ": " + displayName + ", " + colorCode;
							StringBuilder finalURL = new StringBuilder();
							finalURL.append( "<img src='" );
							finalURL.append( defaultImageServer );
							finalURL.append( productId );
							finalURL.append( "_" );
							finalURL.append( colorCode );
							finalURL.append( "_ss_sw?$swatches$' alt=\"" );
							finalURL.append( altText );
							finalURL.append( "\"" );
							finalURL.append( " title=\"" );
							finalURL.append( altText );
							finalURL.append( "\" width=\"41\" height=\"41\" class=\"mediaImage\" />" );
							String colorSwatch = finalURL.toString();
							colorSwatchesSet = colorSwatchesSet + colorSwatch + "&nbsp;";
						}
					}

				}
				// pItem.setPropertyValueInCache(this, colorSwatchesSet);
				return colorSwatchesSet;
			}
		} else {
			throw new IllegalArgumentException( "collection must be an instance of java.util.Collection" );
		}
	}

	public String getTypeName() {
		return TYPE_NAME;
	}

	/* @see atg.repository.RepositoryPropertyDescriptor#setValue(java.lang.String, java.lang.Object) */
	public void setValue( String pAttributeName, Object pValue ) {
		super.setValue( pAttributeName, pValue );
		if( ( pValue == null ) || ( pAttributeName == null ) ) { return; }
		if( pAttributeName.equalsIgnoreCase( PATH_NAME_PROPERTY ) ) {
			setSkuMediaNameMapper( (String)pValue );
		}
	}

	/** @return Returns the ProductFirstSkuColorCodeMapper by
	 *         resolving it into Nucleus component */
	public ProductFirstSkuColorCodeMapper getSkuMediaMapper() {
		if( getSkuMediaNameMapper() == null ) {
			return null;
		} else {
			Nucleus nucleus = Nucleus.getGlobalNucleus();
			if( nucleus != null ) {
				return (ProductFirstSkuColorCodeMapper)nucleus.resolveName( getSkuMediaNameMapper() );
			} else {
				return null;
			}
		}
	}

}
