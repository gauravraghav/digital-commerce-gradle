package com.digital.commerce.merch.multisite.validation;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.nucleus.GenericService;
import atg.remote.assetmanager.editor.model.PropertyUpdate;
import atg.remote.assetmanager.editor.service.AssetEditorInfo;
import atg.remote.multisite.service.SiteAdminAssetPropertyService;
import atg.remote.multisite.service.SiteAssetService;
import atg.remote.multisite.service.validation.SiteAssetServiceValidator;
import atg.repository.RepositoryItem;
import atg.service.asset.AssetResolverException;
import atg.service.dynamo.LangLicense;
import atg.servlet.ServletUtil;
import atg.web.util.AssetUtil;
@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalLanguagePropertyValidationService extends GenericService
		implements SiteAssetServiceValidator {

	// --------------------------------------------------------------------------
	// CONSTANTS
	// --------------------------------------------------------------------------

	/** Resource name */
	private static final String RESOURCE_NAME = "com.digital.commerce.common.i18n.DigitalTranslationValitionResources";

	// --------------------------------------------------------------------------
	// METHODS
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------
	/**
	 * @return The resource bundle to be used in this class.
	 */
	public ResourceBundle getResourceBundle() {

		// Get the locale from the logged in user. If the user's locale can't be
		// found,
		// get the browser locale. If either of these can't be found, use the
		// server locale.
		Locale locale = ServletUtil.getUserLocale();

		if (locale != null) {
			// Get the resource bundle associated with the current locale.
			try{
				return LayeredResourceBundle.getBundle(RESOURCE_NAME, locale);
			}catch(Exception ex){
				logWarning("Error retrieving locale based resourcebundle for locale - " + locale + ex.getMessage());
			}
		}

		// Couldn't retrieve the locale from user's profile, browser or server
		// so just get the default locale.
		return LayeredResourceBundle.getBundle(RESOURCE_NAME,
				LangLicense.getLicensedDefault());
	}

	// --------------------------------------------------------------------
	/**
	 * 
	 * 
	 * @param pEditorInfo
	 *            Information object for the current editor.
	 * @param pUpdates
	 *            Collection of PropertyEditorAssetViewUpdate objects.
	 */
	@Override
	public void validate(AssetEditorInfo pEditorInfo, Collection pUpdates) {
		SiteAssetService sas = new SiteAssetService();

		// Get all current property updates.
		List<PropertyUpdate> updates = sas.getPropertyUpdates(pUpdates);

		if (updates != null) {
			RepositoryItem item = null;

			// The language code to be validated.
			String defaultLanguageCode = null;

			List<String> siteLanguageCodes = null;

			// Iterate through all URL property updates.
			for (PropertyUpdate propUpdate : updates) {
				// Get updated property name
				String name = propUpdate.getPropertyName();

				if (name.equals("defaultLanguage")) {
					// Get the value of defaultLanguage.
					defaultLanguageCode = (String) propUpdate
							.getPropertyValue();

					if (!DigitalStringUtil.isEmpty(defaultLanguageCode)) {

						// validate language code to ensure it conforms to the
						// latest ISO language standard.
						validateDefaultLanguageCodeValue(pEditorInfo,defaultLanguageCode);
					}
				} else if (name.equals("languages")) {

					try {
						item = AssetUtil.getRepositoryItem(pEditorInfo
								.getAssetWrapper().geturi());
						SiteAdminAssetPropertyService service = new SiteAdminAssetPropertyService();

						siteLanguageCodes = (List<String>) service.getGeneralCollectionPropertyUpdateValue(
													pEditorInfo, item, propUpdate);
						
					} catch (AssetResolverException are) {
						if (isLoggingError()) {
							String repositoryId = null;
							if (item != null) {
								repositoryId = item.getRepositoryId();
							}

							logDebug("Exception getting current site from repository using id: "
									+ repositoryId);
						}
					}

					if (siteLanguageCodes != null && siteLanguageCodes.size() > 0) {
						validateSiteLanguages(pEditorInfo, siteLanguageCodes,propUpdate.getPropertyName());
					}
				}
			}

		}
	}

	// --------------------------------------------------------------------------
	/**
	 * Ensure the defaultLanguage is a valid language code.
	 * 
	 * @param pEditorInfo
	 *            Information object for the current editor.
	 * @param pLanguageCode
	 *            The language code to be validated.
	 */
	public void validateDefaultLanguageCodeValue(AssetEditorInfo pEditorInfo,
			String pLanguageCode) {

		if (!DigitalStringUtil.isEmpty(pLanguageCode)) {

			List<String> languageCodes = Arrays
					.asList(Locale.getISOLanguages());

			if (pLanguageCode != null && !languageCodes.contains(pLanguageCode)) {

				if (isLoggingDebug()) {
					logDebug("The defaultLanguage code ("
							+ pLanguageCode
							+ ") does not conform to the current Java ISO locale standard.");
				}

				// Add error message.
				pEditorInfo.getAssetService().addError(
						ResourceUtils.getUserMsgResource(
								"siteLocale.error.defaultLanguageCodeInvalid",
								RESOURCE_NAME, getResourceBundle()));
			}
			if (isLoggingDebug()) {
				logDebug("The defaultLanguage code is valid, conforming to the current Java ISO locale standard.");
			}
		}
	}

	// --------------------------------------------------------------------------
	/**
	 * Ensure the language defined in the site's languages property are valid
	 * language codes.
	 * 
	 * @param pEditorInfo
	 *            Information object for the current editor.
	 * @param pLanguageCode
	 *            The language codes to be validated.
	 * @param pPropName
	 *            The name of the site 'languages' property.
	 */
	public void validateSiteLanguages(AssetEditorInfo pEditorInfo,
			List<String> pLanguageCodes, String pPropName) {

		List<String> languageCodes = Arrays.asList(Locale.getISOLanguages());

		if (pLanguageCodes != null && !DigitalStringUtil.isEmpty(pPropName)) {

			for (String languageCode : pLanguageCodes) {

				if (!languageCodes.contains(languageCode)) {

					if (isLoggingDebug()) {
						logDebug("The site '"
								+ pPropName
								+ "' property contains an invalid language code ("
								+ languageCode + ").");
					}

					Object[] errorParams = { languageCode };

					// Add error message.
					pEditorInfo
							.getAssetService()
							.addError(
									ResourceUtils
											.getUserMsgResource(
													"siteLocale.error.languagesLanguageCodeInvalid",
													RESOURCE_NAME,
													getResourceBundle(),
													errorParams));
				}
			}
		}
	}
}
