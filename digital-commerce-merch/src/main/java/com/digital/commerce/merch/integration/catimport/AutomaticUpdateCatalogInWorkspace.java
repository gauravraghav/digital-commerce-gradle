package com.digital.commerce.merch.integration.catimport;

import javax.servlet.ServletException;
import atg.versionmanager.exceptions.VersionException;
import atg.deployment.common.DeploymentException;
import atg.epub.PublishingException;
import atg.dtm.TransactionDemarcationException;
import javax.ejb.RemoveException;

import javax.jms.JMSException; 

public class AutomaticUpdateCatalogInWorkspace extends UpdateCatalogInWorkspace {

	String	mOutcomeName;

	public void setOutcomeName( String pOutcomeName ) {
		mOutcomeName = pOutcomeName;
	}

	public String getOutcomeName() {
		return mOutcomeName;
	}

	WorkflowOutcome	mWorkflowOutcome;

	public void setWorkflowOutcome( WorkflowOutcome pWorkflowOutcome ) {
		mWorkflowOutcome = pWorkflowOutcome;
	}

	public WorkflowOutcome getWorkflowOutcome() {
		return mWorkflowOutcome;
	}

	protected void performTask() {

		// TSC
		if( isLoggingDebug() ) {
			logDebug( "DEBUG: -------> AutomaticUpdateCatalogInWorkspace: performTask()" );
		}
		super.performTask();
		// now, fire the task events to move the workflow to the next task
		fireOutcome();
		setProcessId( null );
	}

	/** Fires an event for the given task, thus advancing the workflow to the next step
	 * 
	 * @param pTaskId Id of the task
	 * @param pOutcomeId Id of the outcome to advance to
	 * @param pProcessId Id of process associated with this workflow */
	void fireOutcome() {
		try {
			if( getProcessId() != null ) getWorkflowOutcome().fireWorkflowOutcome( getProcessId(), getOutcomeName() );
		} catch( ServletException servEx ) {
			if( isLoggingError() ) {
				logError( "Unable to advance workflow " + getWorkflowName(), servEx );
			}
			try {
				if( getProject() != null ) getProject().delete( getUsername() );
			} catch( RemoveException | JMSException | TransactionDemarcationException | PublishingException | DeploymentException | VersionException remEx ) {
				if( isLoggingError() ) {
					logError( "Unable to delete project with name " + getUsername() + " " + remEx );
				}
			}
		}

	}
}
