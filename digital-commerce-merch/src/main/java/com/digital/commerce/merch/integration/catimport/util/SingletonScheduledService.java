package com.digital.commerce.merch.integration.catimport.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import atg.nucleus.ServiceException;
import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

/** This is basic scheduled service super class.
 * All scheduled services should extend this.
 * Note: this service reuses the current thread for job execution.
 * 
 * @author ashapiro */
public abstract class SingletonScheduledService extends SingletonSchedulableService {

	/** get runtime for debugging purposes
	 * 
	 * @return the curret date. */

	public Date getCurrentDate() {
		Calendar c = new GregorianCalendar();
		return c.getTime();
	}

	/** The enable scheduler flag
	 * By default it is false */
	private boolean	mSchedulerEnabled	= false;

	/** Get the enable scheduler flag
	 * 
	 * @return The enable scheduler flag */
	public boolean isSchedulerEnabled() {
		return mSchedulerEnabled;
	}

	/** Set the enable scheduler flag
	 * 
	 * @param pSchedulerEnabled The enable scheduler flag */
	public void setSchedulerEnabled( boolean pSchedulerEnabled ) {
		mSchedulerEnabled = pSchedulerEnabled;
	}

	/** The operation name for performance monitor */
	private String	mOpName;

	/** Get the operation name for performance monitor
	 * 
	 * @return The operation name for performance monitor */
	protected String getOpName() {
		return mOpName;
	}

	/** Set the operation name for performance monitor
	 * 
	 * @param pOpName The operation name for performance monitor */
	protected void setOpName( String pOpName ) {
		mOpName = pOpName;
	}

	/** This service starts by parsing all the files
	 * that need to be parsed (new files or modified ones)
	 * 
	 * @exception atg.nucleus.ServiceException */
	public void doStartService() throws ServiceException {
		// Only if the scheduler is enabled
		if( isSchedulerEnabled() ) {
			if( isLoggingDebug() ) logDebug( "DEBUG: Scheduler Enabled" );
			super.doStartService();
		} else {
			if( isLoggingDebug() ) logDebug( "DEBUG: Scheduler Disabled" );
		}
	}

	/** When this service stops we should deregister it from
	 * th scheduler and the file system data manager
	 * 
	 * @exception ServiceException */
	public void doStopService() throws ServiceException {
		// Only if the scheduler is enabled
		if( isSchedulerEnabled() ) super.doStopService();
	}

	/** This method performs the scheduled task. It calls the
	 * perform task method that should be implemented in any subclasses.
	 * 
	 * @param pScheduler The scheduler
	 * @param pScheduledJob The scheduled job */
	public void doScheduledTask( Scheduler pScheduler, ScheduledJob pScheduledJob ) {
		if( isLoggingDebug() ) {
			logDebug( "DEBUG: doScheduledTask (): START : " + getJobName() );
			logDebug( "DEBUG: START TIME : " + getCurrentDate() );
		}

		String parameter = "doScheduledTask ()";
		boolean failed = false;
		PerformanceMonitor.startOperation( getOpName(), parameter );

		// Peform the task
		performTask();

		if( !failed ) {
			try {
				PerformanceMonitor.endOperation( getOpName(), parameter );
			} catch( PerfStackMismatchException psme ) {
				logError("PerfStackMismatchException:" , psme);
			}
		}

		if( isLoggingDebug() ) logDebug( "DEBUG: doScheduledTask (): END" );
	}

	/** This method forces the scheduled task to be called without
	 * interfering with the schedule. It calls the perform task
	 * method that should be implemented in any subclasses. */
	public void forceScheduledTask() {
		if( isLoggingDebug() ) logDebug( "DEBUG: forceScheduledTask (): START" );

		String parameter = "forceScheduledTask	 ()";
		boolean failed = false;
		PerformanceMonitor.startOperation( getOpName(), parameter );

		// Peform the task
		performTask();

		if( !failed ) {
			try {
				PerformanceMonitor.endOperation( getOpName(), parameter );
			} catch( PerfStackMismatchException psme ) {
				logError( "PerfStackMismatchException ", psme);
			}
		}

		if( isLoggingDebug() ) logDebug( "DEBUG: forceScheduledTask (): END" );
	}

	// ------------------<HelperMethods>------------------

	private String	mAbsoluteName	= null;

	/** a utility method to check for proper configuration
	 * 
	 * @param pProperty property value to test
	 * @param pPropertyName property name being tested
	 * @throws ServiceException if the property is null */
	public void assertConfigNotNull( final Object pProperty, final String pPropertyName ) throws ServiceException {
		if( null == pProperty ) throw new ServiceException( "Cannot start component. Invalid null property: " + pPropertyName );
	}

	/** a utility method to check for proper non zero configuration
	 * 
	 * @param pProperty property value to test
	 * @param pPropertyName property name being tested
	 * @throws ServiceException if the property is 0 */
	public void assertConfigNotZero( final int pProperty, final String pPropertyName ) throws ServiceException {
		if( 0 == pProperty ) throw new ServiceException( "Cannot start component. Invalid Zero property: " + pPropertyName );
	}

	/** @return Returns the mAbsoluteName. */
	public String getAbsoluteName() {
		return ( ( null == super.getAbsoluteName() ) ? mAbsoluteName : super.getAbsoluteName() );
	}

	/** @param pAbsoluteName The mAbsoluteName to set. */
	public void setAbsoluteName( String pAbsoluteName ) {
		mAbsoluteName = pAbsoluteName;
	}

	// ------------------</HelperMethods>------------------

	/** This method performs the scheduled task. It is an
	 * abstract method and must be implemented by any subclasses. */
	abstract protected void performTask();

}
