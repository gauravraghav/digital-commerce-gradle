package com.digital.commerce.merch.international.translation.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.beans.DynamicPropertyDescriptor;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.multisite.SiteManager;
import atg.remote.assetmanager.editor.model.PropertyUpdate;
import atg.remote.assetmanager.editor.service.AssetEditorInfo;
import atg.remote.assetmanager.editor.service.CollectionPropertyServiceInfo;
import atg.remote.assetmanager.editor.service.RepositoryAssetPropertyServiceImpl;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.dynamo.LangLicense;
import atg.servlet.ServletUtil;

@SuppressWarnings({"unchecked","rawtypes"})
public class InternationalDigitalCatalogAssetPropertyValidation extends
		RepositoryAssetPropertyServiceImpl {

	// --------------------------------------------------------------------------
	// CONSTANTS
	// --------------------------------------------------------------------------

	/** Resource name */
	private static final String RESOURCE_NAME = "com.digital.commerce.common.i18n.DigitalTranslationValitionResources";

	/** siteIds property name */
	private static final String SITE_IDS = "siteIds";

	// --------------------------------------------------------------------------
	// METHODS
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------
	/**
	 * @return The resource bundle to be used in this class.
	 */
	public ResourceBundle getResourceBundle() {

		// Get the locale from the logged in user. If the user's locale can't be
		// found,
		// get the browser locale. If either of these can't be found, use the
		// server locale.
		Locale locale = ServletUtil.getUserLocale();

		if (locale != null) {
			// Get the resource bundle associated with the current locale.
			return LayeredResourceBundle.getBundle(RESOURCE_NAME, locale);
		}

		// Couldn't retrieve the locale from user's profile, browser or server
		// so just get the default locale.
		return LayeredResourceBundle.getBundle(RESOURCE_NAME,
				LangLicense.getLicensedDefault());
	}

	// --------------------------------------------------------------------
	/**
	 * Validate the repository item with the given property update.
	 * 
	 * @param pEditorInfo
	 *            pEditorInfo Information object for the current editor.
	 * @param pItem
	 *            The current updated asset.
	 * @param pUpdates
	 *            URL property update.
	 */
	@Override
	protected void validateItemPropertyUpdate(AssetEditorInfo pEditorInfo,
			RepositoryItem pItem, PropertyUpdate pUpdate) {
		super.validateItemPropertyUpdate(pEditorInfo, pItem, pUpdate);

		// Run 'translations' property key validation.
		if (pUpdate.getPropertyName().equals("translations")) {
			validateTranslationsLanguageKeyValue(pEditorInfo, pItem, pUpdate);
		}else if(pUpdate.getPropertyName().equalsIgnoreCase("userFriendlyNameDefault")){
			String value = (String)pUpdate.getPropertyValue();
			int maxLength = Integer.parseInt(ResourceUtils.getUserMsgResource("inputMaxLength",
							RESOURCE_NAME,
							getResourceBundle(),
							null));
			if(DigitalStringUtil.isNotBlank(value) && value.length() > maxLength){

				pEditorInfo
						.getAssetService()
						.addError(
								ResourceUtils
										.getUserMsgResource(
												"promotion.error.exceedLengthForUserFriendlyName",
												RESOURCE_NAME,
												getResourceBundle(),
												null));				
			}
		}
	}

	// --------------------------------------------------------------------
	/**
	 * <p>
	 * Validate the translations property keys to ensure they are valid language
	 * codes. An invalid language code returns an error to the user.
	 * </p>
	 * <p>
	 * This method also checks each site in the category/product/sku site list
	 * to see if an updated translation property key is supported by those
	 * sites. If the language is not found in any of these sites 'languages'
	 * property lists, a warning is returned to the user.
	 * </p>
	 * 
	 * @param pEditorInfo
	 *            pEditorInfo Information object for the current editor.
	 * @param pItem
	 *            The current updated asset.
	 * @param pUpdates
	 *            URL property update.
	 */
	public void validateTranslationsLanguageKeyValue(
			AssetEditorInfo pEditorInfo, RepositoryItem pItem,
			PropertyUpdate pUpdate) {

		// Get the translations property updates.
		Map<String, RepositoryItem> translations = (Map<String, RepositoryItem>) super.getCollectionPropertyUpdateValue(pEditorInfo, pItem, pUpdate);

		if (translations != null) {

			// List of languages to detail in invalid language warning message.
			List<String> languagesNotInSiteList = new ArrayList<>();

			DynamicPropertyDescriptor desc = null;
			CollectionPropertyServiceInfo info = null;

			// Sites that contain the current category/product/sku.
			List<Map> sites = null;

			try {
				desc = pItem.getItemDescriptor()
						.getPropertyDescriptor(SITE_IDS);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(
							"There was a problem retrieving the itemDescriptor for "
									+ pItem, e);
				}
			}

			if (desc != null) {
				info = (CollectionPropertyServiceInfo) pEditorInfo
						.getAssetPropertyServiceData().get(desc.getName());

				if (info == null || info.propertyValue == null
						|| info.collectionDescriptorInfo == null) {
					if (isLoggingDebug()) {
						logDebug("Collection property " + SITE_IDS
								+ " does not have a cached value.");
					}
				} else {
					sites = super.getCollectionPropertyValues(pEditorInfo,
							SITE_IDS, info, 0, 999);
				}
			}

			// Retrieve the updated language code values.
			Set<String> localeCodes = translations.keySet();

			for (String localeCode : localeCodes) {

				// Add an error if an updated language code doesn't exist in the
				// ISO language list.
				// if (!ISOlanguageCodes.contains(localeCode)) {
				if (!isValidLocale(localeCode)) {
					if (isLoggingDebug()) {
						logDebug("The translations language code key ("
								+ localeCode
								+ ") does not conform to the Java ISO locale standard.");
					}

					Object[] errorParams = { localeCode };

					pEditorInfo
							.getAssetService()
							.addError(
									ResourceUtils
											.getUserMsgResource(
													"catalog.error.translationLanguageKeyInvalid",
													RESOURCE_NAME,
													getResourceBundle(),
													errorParams));
				} else if (sites != null) {
					// Check that the sites in the category/product/sku site
					// list supports the updated translation key.

					List<String> siteList = new ArrayList<>();

					for (int i = 0; i < sites.size(); i++) {
						Map<String, String> siteNames = sites.get(i);
						Collection<String> siteColValue = siteNames.values();
						Iterator it = siteColValue.iterator();

						while (it.hasNext()) {
							siteList.add((String) (it.next()));
						}
					}

					RepositoryItem[] items = null;

					try {
						items = SiteManager.getSiteManager().getAllSites();
					} catch (RepositoryException e) {
						if (isLoggingError()) {
							logError(
									"There was a problem retrieving all sites in system",
									e);
						}
					}

					if (items != null) {
						String langSource = getLangFromLocale(localeCode);
						siteLoop: for (String name : siteList) {

							// Get the RepositoryItem objects for sites in
							// siteIds property.
							for (RepositoryItem item : items) {

								if (item.getItemDisplayName().equals(name)) {

									// Check if a site's languages list contains
									// the updated language code.
									List<String> siteLanguages = (List<String>) item
											.getPropertyValue("languages");

									if (siteLanguages.contains(langSource)) {
										if (languagesNotInSiteList
												.contains(langSource)) {
											languagesNotInSiteList
													.remove(langSource);
										}

										if (isLoggingDebug()) {
											logDebug("The language code ("
													+ langSource
													+ ") is valid as it has been set as a language code in site "
													+ name);
										}

										break siteLoop;
									} else {
										if (!languagesNotInSiteList
												.contains(langSource)) {
											// Language code not set in current
											// site's 'languages' list.
											languagesNotInSiteList
													.add(langSource);
										}
									}

									break;
								}
							}
						}
					}
				}
			}

			if (languagesNotInSiteList.size() > 0) {

				if (isLoggingDebug()) {
					logDebug("The language codes " + languagesNotInSiteList
							+ " are not available in any sites referenced by "
							+ pItem.getItemDisplayName() + ", adding warning.");
				}

				// The updated language code(s) are not in the site's language
				// list, add warning.
				Object[] errorParams = { languagesNotInSiteList,
						pItem.getItemDisplayName() };
				String errorResourceKey = "";

				if (languagesNotInSiteList.size() == 1) {
					errorResourceKey = "catalog.error.translationLanguageKeyNotAvailable";
				} else {
					errorResourceKey = "catalog.error.translationLanguageKeysNotAvailable";
				}

				pEditorInfo.getAssetService().addWarning(
						ResourceUtils
								.getUserMsgResource(errorResourceKey,
										RESOURCE_NAME, getResourceBundle(),
										errorParams));
			}
		}
	}

	boolean isValidLocale(String value) {
		boolean ret = false;
		try {
			org.apache.commons.lang.LocaleUtils.toLocale(value);
			ret = true;
		} catch (Exception ex) {
			logError("Invalid locale value = " + value, ex);
			ret = false;
		}
		return ret;
	}

	String getLangFromLocale(String value) {
		String lang = value;
		try {
			Locale locale = org.apache.commons.lang.LocaleUtils.toLocale(value);
			lang = locale.getLanguage();
		} catch (Exception ex) {
			logError("Error converting to locale for value=" + value, ex);
		}
		return lang;
	}

}
