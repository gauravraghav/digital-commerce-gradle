package com.digital.commerce.userprofiling;

import atg.userprofiling.ProfileTools;

/**
 * 
 */
public class DigitalProfileTools extends ProfileTools{
	/**
	 * Property to hold user AccountLockTime
	 */
	private long mAccountLockTime;

	/**
	 * @return the mAccountLockTime
	 */
	public long getAccountLockTime() {
		return mAccountLockTime;
	}

	/**
	 * @param pAccountLockTime the mAccountLockTime to set
	 */
	public void setAccountLockTime(long pAccountLockTime) {
		this.mAccountLockTime = pAccountLockTime;
	}
	
	/**
	 * Property to hold max Access Attempts
	 */
	private int mMaxAccessAttempts;

		
	/**
	 * @return the mMaxAccessAttempts
	 */
	public int getMaxAccessAttempts() {
		return mMaxAccessAttempts;
	}

	/**
	 * @param pMaxAccessAttempts the mMaxAccessAttempts to set
	 */
	public void setMaxAccessAttempts(int pMaxAccessAttempts) {
		this.mMaxAccessAttempts = pMaxAccessAttempts;
	}

	/**
	 * Property to hold user AccountLockedMsg
	 */
	private String mAccountLockedMsg;

	/**
	 * @return the mAccountLockedMsg
	 */
	public String getAccountLockedMsg() {
		return mAccountLockedMsg;
	}

	/**
	 * @param pAccountLockedMsg the mAccountLockedMsg to set
	 */
	public void setAccountLockedMsg(String pAccountLockedMsg) {
		this.mAccountLockedMsg = pAccountLockedMsg;
	}
	
}
