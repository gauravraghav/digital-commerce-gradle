package com.digital.commerce.userprofiling;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.ServletException;

import atg.droplet.DropletException;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;
import atg.scenario.userprofiling.ScenarioProfileFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.PropertyManager;

/**
 * @author cheedun
 * This class extends ScenarioProfileFormHandler to override findUser method.
 * findUser(): This method locks the user account for X minutes if the entered password is wrong for X times.
 * 
 */
@SuppressWarnings({"rawtypes"})
public class DigitalProfileFormHandler extends ScenarioProfileFormHandler {
	
	private static final String	USER_FAILED_ACCESS_ATTEMPS = "failedAccessAttempts";
	private static final String	USER_INVALID_PASSWORD = "invalidPassword";  //NOSONAR
	private static final String	USER_ACCOUNT_LOCKED_TIME = "accountLockedTime";

	
	
	 /**
	   * Checks the supplied login and password and attempts to find a profile which matches the supplied values.
	   *     
	   * If the password is invalid ,updates failed access attempts to make sure that the user account is locked for 
	   * configured time and the form error will be added.
	   * 
	   * @return the user profile or null if the user could not be found
	   * @param pRequest the servlet's request
	   * @param pResponse the servlet's response
	   * @exception ServletException if there was an error while executing the code
	   * @exception IOException if there was an error with servlet io
	   */
	 protected RepositoryItem findUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException
 {
		RepositoryItem item = super.findUser(pRequest, pResponse);
		ProfileTools ptools = getProfileTools();
		PropertyManager pmgr = ptools.getPropertyManager();
		String loginPropertyName = pmgr.getLoginPropertyName();
		String login = getStringValueProperty(loginPropertyName);

		if ((login != null) && (isTrimProperty(loginPropertyName))) {
			login = login.trim();
		}
		MutableRepositoryItem profile = (MutableRepositoryItem) ptools.getItem(
				login, null, getLoginProfileType());
		if (profile != null) {
			logInfo("################# VALIDATE LOGIN ATTEMPTS for USER with USERNAME :::  "+profile.getPropertyValue("login"));
			DigitalProfileTools profileTools = (DigitalProfileTools) getProfileTools();
			if (item == null) {
				Iterator itr = getFormExceptions().iterator();
				Object obj = null;
				while (itr.hasNext()) {
					obj = itr.next();
					if (obj instanceof DropletException) {
						if (((DropletException) obj).getErrorCode()
								.equalsIgnoreCase(USER_INVALID_PASSWORD)) {
							// update failed access attempts
							if (profile
									.getPropertyValue(USER_FAILED_ACCESS_ATTEMPS) == null) {
								profile.setPropertyValue(
										USER_FAILED_ACCESS_ATTEMPS, 1);
							} else {
								int accessAttempts = (Integer) profile
										.getPropertyValue(USER_FAILED_ACCESS_ATTEMPS);
								profile.setPropertyValue(
										USER_FAILED_ACCESS_ATTEMPS,
										accessAttempts + 1);
							}
						}
					}
				}
			} else {
				if (profile.getPropertyValue(USER_FAILED_ACCESS_ATTEMPS) != null
						&& (Integer) profile
								.getPropertyValue(USER_FAILED_ACCESS_ATTEMPS) > 0) {
					profile.setPropertyValue(USER_FAILED_ACCESS_ATTEMPS, 0);
				}
			}
			// check if the failed access attempts exceeds max attempts
			if (profile.getPropertyValue(USER_FAILED_ACCESS_ATTEMPS) != null) {
				if ((Integer) profile
						.getPropertyValue(USER_FAILED_ACCESS_ATTEMPS) > profileTools
						.getMaxAccessAttempts()) {
					profile.setPropertyValue(USER_ACCOUNT_LOCKED_TIME,
							new Timestamp(System.currentTimeMillis()));
						profile.setPropertyValue(USER_FAILED_ACCESS_ATTEMPS, 0);
				}
			}

			// check if the profile has got accountLockedTime set
				if (profile.getPropertyValue(USER_ACCOUNT_LOCKED_TIME) != null) {
					long accountLockTime = ((DigitalProfileTools) getProfileTools())
							.getAccountLockTime();
					long accountLockedTime = ((Date) profile
							.getPropertyValue(USER_ACCOUNT_LOCKED_TIME))
							.getTime();
					long currentTime = System.currentTimeMillis();
					long diff = currentTime - accountLockedTime;
					long diffMinutes = diff / (60 * 1000);
					if (diffMinutes < accountLockTime) {
						profile.setPropertyValue(USER_FAILED_ACCESS_ATTEMPS, 0);
						logInfo("################# ACCOUNT LOCKED ######### for USER with USERNAME :::  "+profile.getPropertyValue("login")+":::: AT:::"+profile.getPropertyValue(USER_ACCOUNT_LOCKED_TIME));
						addFormException(new DropletException(profileTools.getAccountLockedMsg()));
						return null;
					} else {
						logInfo("################# ACCOUNT UNLOCKED ######### for USER with USERNAME :::  "+profile.getPropertyValue("login")+":::: AT:::"+profile.getPropertyValue(USER_ACCOUNT_LOCKED_TIME));
						profile.setPropertyValue(USER_ACCOUNT_LOCKED_TIME, null);
					}
				} 
				return item;
		}
		return item;
	}
}
