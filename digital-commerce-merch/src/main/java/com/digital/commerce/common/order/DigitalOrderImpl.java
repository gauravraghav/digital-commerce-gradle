package com.digital.commerce.common.order;

import atg.commerce.order.OrderImpl;

public class DigitalOrderImpl extends OrderImpl {

	/** added this property only for the purpose of pmdl rule editing, the same
	 * value is set at the DigitalOrderPriceInfo object. */
	private double	qualifiedOrderAmount;

	/** @return Returns the qualifiedOrderAmount. */
	public double getQualifiedOrderAmount() {
		return qualifiedOrderAmount;
	}

	/** @param qualifiedOrderAmount
	 *            The qualifiedOrderAmount to set. */
	public void setQualifiedOrderAmount( double qualifiedOrderAmount ) {
		this.qualifiedOrderAmount = qualifiedOrderAmount;
	}

	private String sourceOfOrder;

	public String getSourceOfOrder() {
		if (this.sourceOfOrder ==null){
			sourceOfOrder = (String)getPropertyValue("originOfOrder");
		}
		return sourceOfOrder; 
	}

	public void setSourceOfOrder(String sourceOfOrder) {
		this.sourceOfOrder = sourceOfOrder;
	}

	private static final long	serialVersionUID	= 3811259803622852426L;

}
