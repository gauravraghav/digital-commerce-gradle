package com.digital.commerce.claimable;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.claimable.ClaimableException;
import atg.commerce.claimable.CouponBatchTools;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.digital.commerce.common.logger.DigitalLogger;

import java.util.List;

public class DigitalCouponBatchTools extends CouponBatchTools {
	
	private static final DigitalLogger LOGGER = DigitalLogger.getLogger(DigitalCouponBatchTools.class);
    private boolean enableBirthdayCoupons;
    
	public boolean isEnableBirthdayCoupons() {
		return enableBirthdayCoupons;
	}

	public void setEnableBirthdayCoupons(boolean enableBirthdayCoupons) {
		this.enableBirthdayCoupons = enableBirthdayCoupons;
	}

	@Override
	public List<String> generateCouponBatchCodes(RepositoryItem pCouponBatch)
            throws ClaimableException
        {
    	List<String> codes =super.generateCouponBatchCodes(pCouponBatch);
    	//Get this codes and update in new itemDescriptor 
    	//Mark codes as available if the coupon batch is deployed
    	boolean isBirthdayCoupon=(Boolean) pCouponBatch.getPropertyValue("birthdayCouponBatch");
    	if(isEnableBirthdayCoupons() && isBirthdayCoupon){
    		//populateBirthdayCoupons
    		populateBirthdayCoupons(pCouponBatch,codes);
    	}
    	return codes;
        }
	

	private void populateBirthdayCoupons(RepositoryItem pCouponBatch, List codes) {
		try {
			DynamicBeans.setPropertyValue(pCouponBatch, "birthdayCoupons", codes);
			((MutableRepository)getClaimableRepository()).updateItem((MutableRepositoryItem)pCouponBatch);
		} catch (RepositoryException | PropertyNotFoundException e) {
			LOGGER.info(getClass().getSimpleName() +" :: Exception :: " +e.getMessage());
		}
	}
}
