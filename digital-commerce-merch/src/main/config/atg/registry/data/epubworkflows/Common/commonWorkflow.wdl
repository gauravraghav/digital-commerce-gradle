<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE process SYSTEM "dynamosystemresource:/atg/dtds/pdl/pdl_1.0.dtd">
<process author="admin" creation-time="1208186632423" enabled="true" last-modified-by="admin" modification-time="1389907194248">
  <segment migrate-from="1330021944271,1333474490343,1389907026178,1389907039109,1389907102398" migrate-subjects="true">
    <segment-name>main</segment-name>
    <!--================================-->
    <!--== startWorkflow  -->
    <!--================================-->
    <event id="1">
      <event-name>atg.workflow.StartWorkflow</event-name>
      <filter operator="eq">
        <event-property>
          <property-name>processName</property-name>
        </event-property>
        <constant>/Common/commonWorkflow.wdl</constant>
      </filter>
      <filter operator="eq">
        <event-property>
          <property-name>segmentName</property-name>
        </event-property>
        <constant>main</constant>
      </filter>
      <attributes>
        <attribute name="atg.workflow.elementType">
          <constant>startWorkflow</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$role$epubUser:execute;Admin$role$administrators-group:execute;Profile$role$epubSuperAdmin:execute;Profile$role$epubManager:execute;Admin$role$managers-group:execute;Profile$role$epubAdmin:execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>A basic workflow which focuses on creating and editing commerce assets. Deploys to production target.</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Content Administration Project</constant>
        </attribute>
        <attribute name="atg.workflow.resourceBundle">
          <constant>atg.registry.ContentAdministrationResources</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>startWorkflow.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>startworkflow.description</constant>
        </attribute>
      </attributes>
    </event>
    <!--================================-->
    <!--== Create project without a workflow and process' project name  -->
    <!--================================-->
    <action id="2">
      <action-name>createProjectForProcess</action-name>
    </action>
    <!--================================-->
    <!--== author  -->
    <!--================================-->
    <label id="3">
      <attributes>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">true</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$role$epubManager:write,execute;Profile$role$epubUser:write,execute;Admin$role$administrators-group:write,execute;Admin$role$managers-group:write,execute;Profile$role$epubSuperAdmin:write,execute;Profile$role$epubAdmin:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Create and modify assets for eventual deployment</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>author</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Author</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>author.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>author.description</constant>
        </attribute>
      </attributes>
    </label>
    <fork exclusive="true" id="4">
      <branch id="4.1">
        <!--================================-->
        <!--== review  -->
        <!--================================-->
        <event id="4.1.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Common/commonWorkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>4.1.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Assets are ready for approval</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>review</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Ready for Review</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>author.review.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>author.review.description</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Change Project's Current project's Editable to false  -->
        <!--================================-->
        <action id="4.1.2">
          <action-name construct="modify-action">modify</action-name>
          <action-param name="modified">
            <subject-property>
              <property-name>project</property-name>
              <property-name>editable</property-name>
            </subject-property>
          </action-param>
          <action-param name="operator">
            <constant>assign</constant>
          </action-param>
          <action-param name="modifier">
            <constant type="java.lang.Boolean">false</constant>
          </action-param>
        </action>
        <!--================================-->
        <!--== Check assets are up to date  -->
        <!--================================-->
        <action id="4.1.3">
          <action-name>assetsUpToDate</action-name>
        </action>
      </branch>
      <branch id="4.2">
        <!--================================-->
        <!--== delete  -->
        <!--================================-->
        <event id="4.2.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Common/commonWorkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>4.2.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Delete the project</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>delete</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Delete Project</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>author.delete.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>author.delete.description</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Delete project  -->
        <!--================================-->
        <action id="4.2.2">
          <action-name>deleteProject</action-name>
        </action>
      </branch>
    </fork>
    <!--================================-->
    <!--== contentReview  -->
    <!--================================-->
    <label id="5">
      <attributes>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">true</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$role$epubManager:write,execute;Profile$role$epubSuperAdmin:write,execute;Admin$role$managers-group:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Review and approve changes to project assets</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>contentReview</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Content Review</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>contentReview.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>contentReview.description</constant>
        </attribute>
      </attributes>
    </label>
    <fork exclusive="true" id="6">
      <branch id="6.1">
        <!--================================-->
        <!--== approve  -->
        <!--================================-->
        <event id="6.1.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Common/commonWorkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>6.1.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>The content has been reviewed and appears good to proceed</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>approve</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Approve Content</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>contentReview.approve.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>contentReview.approve.description</constant>
            </attribute>
          </attributes>
        </event>
      </branch>
      <branch id="6.2">
        <!--================================-->
        <!--== reject  -->
        <!--================================-->
        <event id="6.2.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Common/commonWorkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>6.2.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Reject the project</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>reject</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Reject</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>contentReview.reject.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>contentReview.reject.description</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Reopen project  -->
        <!--================================-->
        <action id="6.2.2">
          <action-name>reopenProject</action-name>
        </action>
        <jump id="6.2.3" target="3"/>
      </branch>
      <branch id="6.3">
        <!--================================-->
        <!--== delete  -->
        <!--================================-->
        <event id="6.3.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Common/commonWorkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>6.3.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Delete the project</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>delete</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Delete Project</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>contentReview.delete.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>contentReview.delete.description</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Delete project  -->
        <!--================================-->
        <action id="6.3.2">
          <action-name>deleteProject</action-name>
        </action>
      </branch>
    </fork>
    <fork id="7">
      <branch id="7.1">
        <!--================================-->
        <!--== Between 7:00AM and 9:00 PM  -->
        <!--================================-->
        <time id="7.1.1">
          <time-range>
            <time-of-day>
              <hours>7</hours>
              <minutes>00</minutes>
              <meridian>AM</meridian>
            </time-of-day>
            <time-of-day>
              <hours>9</hours>
              <minutes>00</minutes>
              <meridian>PM</meridian>
            </time-of-day>
          </time-range>
        </time>
      </branch>
      <branch id="7.2">
        <!--================================-->
        <!--== Between 9:00PM and 7:00 AM  -->
        <!--================================-->
        <time id="7.2.1">
          <time-range>
            <time-of-day>
              <hours>9</hours>
              <minutes>00</minutes>
              <meridian>PM</meridian>
            </time-of-day>
            <time-of-day>
              <hours>7</hours>
              <minutes>00</minutes>
              <meridian>AM</meridian>
            </time-of-day>
          </time-range>
        </time>
        <!--================================-->
        <!--== onCallReview  -->
        <!--================================-->
        <label id="7.2.2">
          <attributes>
            <attribute name="atg.workflow.assignable">
              <constant type="java.lang.Boolean">true</constant>
            </attribute>
            <attribute name="atg.workflow.elementType">
              <constant>task</constant>
            </attribute>
            <attribute name="atg.workflow.acl">
              <constant>Profile$role$epubSuperAdmin:write,execute</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>task for holding projects advanced during the catalog feed processing window.</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>onCallReview</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>On-Call Review</constant>
            </attribute>
          </attributes>
        </label>
        <fork exclusive="true" id="7.2.3">
          <branch id="7.2.3.1">
            <!--================================-->
            <!--== approve  -->
            <!--================================-->
            <event id="7.2.3.1.1">
              <event-name>atg.workflow.TaskOutcome</event-name>
              <filter operator="eq">
                <event-property>
                  <property-name>processName</property-name>
                </event-property>
                <constant>/Common/commonWorkflow.wdl</constant>
              </filter>
              <filter operator="eq">
                <event-property>
                  <property-name>segmentName</property-name>
                </event-property>
                <constant>main</constant>
              </filter>
              <filter operator="eq">
                <event-property>
                  <property-name>outcomeElementId</property-name>
                </event-property>
                <constant>7.2.3.1.1</constant>
              </filter>
              <attributes>
                <attribute name="atg.workflow.elementType">
                  <constant>outcome</constant>
                </attribute>
                <attribute name="atg.workflow.description">
                  <constant>The content has been reviewed and appears good to proceed</constant>
                </attribute>
                <attribute name="atg.workflow.name">
                  <constant>approve</constant>
                </attribute>
                <attribute name="atg.workflow.displayName">
                  <constant>Approve Content</constant>
                </attribute>
              </attributes>
            </event>
          </branch>
          <branch id="7.2.3.2">
            <!--================================-->
            <!--== reject  -->
            <!--================================-->
            <event id="7.2.3.2.1">
              <event-name>atg.workflow.TaskOutcome</event-name>
              <filter operator="eq">
                <event-property>
                  <property-name>processName</property-name>
                </event-property>
                <constant>/Common/commonWorkflow.wdl</constant>
              </filter>
              <filter operator="eq">
                <event-property>
                  <property-name>segmentName</property-name>
                </event-property>
                <constant>main</constant>
              </filter>
              <filter operator="eq">
                <event-property>
                  <property-name>outcomeElementId</property-name>
                </event-property>
                <constant>7.2.3.2.1</constant>
              </filter>
              <attributes>
                <attribute name="atg.workflow.elementType">
                  <constant>outcome</constant>
                </attribute>
                <attribute name="atg.workflow.description">
                  <constant>Reject the project</constant>
                </attribute>
                <attribute name="atg.workflow.name">
                  <constant>reject</constant>
                </attribute>
                <attribute name="atg.workflow.displayName">
                  <constant>Reject</constant>
                </attribute>
              </attributes>
            </event>
            <!--================================-->
            <!--== Reopen project  -->
            <!--================================-->
            <action id="7.2.3.2.2">
              <action-name>reopenProject</action-name>
            </action>
            <jump id="7.2.3.2.3" target="3"/>
          </branch>
        </fork>
      </branch>
    </fork>
    <!--================================-->
    <!--== productionApproval  -->
    <!--================================-->
    <label id="8">
      <attributes>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">true</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$role$epubManager:write,execute;Admin$role$managers-group:write,execute;Profile$role$epubSuperAdmin:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Approve for deployment to Production target</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>productionApproval</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Approve for Production Deployment</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>productApproval.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>productApproval.description</constant>
        </attribute>
      </attributes>
    </label>
    <fork exclusive="true" id="9">
      <branch id="9.1">
        <!--================================-->
        <!--== approveAndDeploy  -->
        <!--================================-->
        <event id="9.1.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Common/commonWorkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>9.1.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Approve for immediate deployment to Production target</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>approveAndDeploy</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Approve and Deploy to Production</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>productApproval.approveAndDeploy.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>productApproval.approveAndDeploy.description</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Approve and deploy project to target TEST-4150 DSW.com  -->
        <!--================================-->
        <action id="9.1.2">
          <action-name>approveAndDeployProject</action-name>
          <action-param name="target">
            <constant>Production</constant>
          </action-param>
        </action>
      </branch>
      <branch id="9.2">
        <!--================================-->
        <!--== approve  -->
        <!--================================-->
        <event id="9.2.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Common/commonWorkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>9.2.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Approve project for deployment to Production target</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>approve</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Approve for Production Deployment</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>productApproval.approve.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>productApproval.approve.description</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Approve project for target TEST-4150 DSW.com  -->
        <!--================================-->
        <action id="9.2.2">
          <action-name>approveProject</action-name>
          <action-param name="target">
            <constant>Production</constant>
          </action-param>
        </action>
      </branch>
      <branch id="9.3">
        <!--================================-->
        <!--== reject  -->
        <!--================================-->
        <event id="9.3.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Common/commonWorkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>9.3.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Reject project for deployment to Production target</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>reject</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Reject Production Deployment</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>productApproval.reject.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>productApproval.reject.description</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Reopen project  -->
        <!--================================-->
        <action id="9.3.2">
          <action-name>reopenProject</action-name>
        </action>
        <!--================================-->
        <!--== Release asset locks  -->
        <!--================================-->
        <action id="9.3.3">
          <action-name>releaseAssetLocks</action-name>
        </action>
        <jump id="9.3.4" target="3"/>
      </branch>
    </fork>
    <!--================================-->
    <!--== waitForDeploymentToComplete  -->
    <!--================================-->
    <label id="10">
      <attributes>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">false</constant>
        </attribute>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$role$epubAdmin:write,execute;Profile$role$epubSuperAdmin:write,execute;Admin$role$administrators-group:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Wait for deployment to complete on Production target</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>waitForDeploymentToComplete</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Wait for Production Deployment Completion</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>waitForDeploymentToComplete.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>waitForDeploymentToComplete.description</constant>
        </attribute>
      </attributes>
    </label>
    <fork exclusive="true" id="11">
      <branch id="11.1">
        <!--================================-->
        <!--== Wait for deployment to complete on target TEST-4150 DSW.com  -->
        <!--================================-->
        <event id="11.1.1">
          <event-name>atg.deployment.DeploymentStatus</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>targetId</property-name>
            </event-property>
            <constant>Production</constant>
          </filter>
        </event>
        <fork id="11.1.2">
          <branch id="11.1.2.1">
            <!--================================-->
            <!--== Deployment completed event status is success on target TEST-4150 DSW.com  -->
            <!--================================-->
            <condition id="11.1.2.1.1">
              <filter operator="deploymentCompleted">
                <constant>1</constant>
                <constant>Production</constant>
              </filter>
            </condition>
          </branch>
          <branch id="11.1.2.2">
            <!--================================-->
            <!--== Deployment completed event status is failure on target TEST-4150 DSW.com  -->
            <!--================================-->
            <condition id="11.1.2.2.1">
              <filter operator="deploymentCompleted">
                <constant>0</constant>
                <constant>Production</constant>
              </filter>
            </condition>
            <!--================================-->
            <!--== Release asset locks  -->
            <!--================================-->
            <action id="11.1.2.2.2">
              <action-name>releaseAssetLocks</action-name>
            </action>
            <jump id="11.1.2.2.3" target="8"/>
          </branch>
        </fork>
      </branch>
    </fork>
    <!--================================-->
    <!--== verifyProduction  -->
    <!--================================-->
    <label id="12">
      <attributes>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">true</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$role$epubUser:write,execute;Profile$role$epubManager:write,execute;Admin$role$managers-group:write,execute;Profile$role$epubSuperAdmin:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Verify that the deployment to Production was successful and that all deployed assets look and function appropriately</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>verifyProduction</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Verify Production Deployment</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>verifyProduction.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>verifyProduction.description</constant>
        </attribute>
      </attributes>
    </label>
        <!--================================-->
        <!--== Validate project is deployed on target TEST-4150 DSW.com  -->
        <!--================================-->
        <action id="13">
          <action-name>validateProjectDeployed</action-name>
          <action-param name="target">
            <constant>Production</constant>
          </action-param>
        </action>
        <!--================================-->
        <!--== Check in project's workspace  -->
        <!--================================-->
        <action id="14">
          <action-name>checkInProject</action-name>
        </action>
        <!--================================-->
        <!--== Complete project  -->
        <!--================================-->
        <action id="15">
          <action-name>completeProject</action-name>
        </action>
        <!--================================-->
        <!--== Complete process  -->
        <!--================================-->
        <action id="16">
          <action-name>completeProcess</action-name>
        </action>
  </segment>
  <segment migrate-from="1334151358932,1334155563763" migrate-subjects="true">
    <segment-name>express</segment-name>
    <!--================================-->
    <!--== startWorkflow  -->
    <!--================================-->
    <event id="1">
      <event-name>atg.workflow.StartWorkflow</event-name>
      <filter operator="eq">
        <event-property>
          <property-name>processName</property-name>
        </event-property>
        <constant>/Common/commonWorkflow.wdl</constant>
      </filter>
      <filter operator="eq">
        <event-property>
          <property-name>segmentName</property-name>
        </event-property>
        <constant>express</constant>
      </filter>
      <attributes>
        <attribute name="atg.workflow.elementType">
          <constant>startWorkflow</constant>
        </attribute>
        <attribute name="atg.workflow.resourceBundle">
          <constant>atg.registry.ContentAdministrationResources</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$accessRight$emergencyWorkflow:execute;Admin$role$administrators-group:execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>An express workflow which focuses on creating and editing arbitrary assets. Deploys to production target.</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>startworkflow.description</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>startWorkflow.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Emergency Project</constant>
        </attribute>
      </attributes>
    </event>
    <!--================================-->
    <!--== Create project without a workflow and process' project name  -->
    <!--================================-->
    <action id="2">
      <action-name>createProjectForProcess</action-name>
    </action>
    <!--================================-->
    <!--== author  -->
    <!--================================-->
    <label id="3">
      <attributes>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">true</constant>
        </attribute>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$accessRight$emergencyWorkflow:write,execute;Admin$role$administrators-group:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Create and modify assets for eventual deployment</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>author</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>author.description</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>author.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Author</constant>
        </attribute>
      </attributes>
    </label>
    <fork exclusive="true" id="4">
      <branch id="4.1">
        <!--================================-->
        <!--== expressDeploy  -->
        <!--================================-->
        <event id="4.1.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Common/commonWorkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>express</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>4.1.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Deploy immediately</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>expressDeploy</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>author.expressDeploy.description</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>author.expressDeploy.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Deploy to Production</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== releaseConflictingAssetLocks.displayName  -->
        <!--================================-->
        <action id="4.1.2">
          <action-name>releaseConflictingAssetLocks</action-name>
        </action>
        <!--================================-->
        <!--== Change Project's Current project : Editable to false  -->
        <!--================================-->
        <action id="4.1.3">
          <action-name construct="modify-action">modify</action-name>
          <action-param name="modified">
            <subject-property>
              <property-name>project</property-name>
              <property-name>editable</property-name>
            </subject-property>
          </action-param>
          <action-param name="operator">
            <constant>assign</constant>
          </action-param>
          <action-param name="modifier">
            <constant type="java.lang.Boolean">false</constant>
          </action-param>
        </action>
        <!--================================-->
        <!--== Check assets are up to date  -->
        <!--================================-->
        <action id="4.1.4">
          <action-name>assetsUpToDate</action-name>
        </action>
        <!--================================-->
        <!--== Approve and deploy project to target Production  -->
        <!--================================-->
        <action id="4.1.5">
          <action-name>approveAndDeployProject</action-name>
          <action-param name="target">
            <constant>Production</constant>
          </action-param>
        </action>
        <!--================================-->
        <!--== Wait for deployment to complete on target Production  -->
        <!--================================-->
        <event id="4.1.6">
          <event-name>atg.deployment.DeploymentStatus</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>targetId</property-name>
            </event-property>
            <constant>Production</constant>
          </filter>
        </event>
        <fork id="4.1.7">
          <branch id="4.1.7.1">
            <!--================================-->
            <!--== Deployment completed event status is success on target Production  -->
            <!--================================-->
            <condition id="4.1.7.1.1">
              <filter operator="deploymentCompleted">
                <constant>1</constant>
                <constant>Production</constant>
              </filter>
            </condition>
            <!--================================-->
            <!--== Check in project's workspace  -->
            <!--================================-->
            <action id="4.1.7.1.2">
              <action-name>checkInProject</action-name>
            </action>
            <!--================================-->
            <!--== Complete project  -->
            <!--================================-->
            <action id="4.1.7.1.3">
              <action-name>completeProject</action-name>
            </action>
            <!--================================-->
            <!--== Complete process  -->
            <!--================================-->
            <action id="4.1.7.1.4">
              <action-name>completeProcess</action-name>
            </action>
          </branch>
          <branch id="4.1.7.2">
            <!--================================-->
            <!--== Deployment completed event status is failure on target Production  -->
            <!--================================-->
            <condition id="4.1.7.2.1">
              <filter operator="deploymentCompleted">
                <constant>0</constant>
                <constant>Production</constant>
              </filter>
            </condition>
            <!--================================-->
            <!--== Release asset locks  -->
            <!--================================-->
            <action id="4.1.7.2.2">
              <action-name>releaseAssetLocks</action-name>
            </action>
            <jump id="4.1.7.2.3" target="3"/>
          </branch>
        </fork>
      </branch>
      <branch id="4.2">
        <!--================================-->
        <!--== delete  -->
        <!--================================-->
        <event id="4.2.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Common/commonWorkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>express</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>4.2.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Delete the project</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>delete</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>author.delete.description</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>author.delete.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Delete Project</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Delete project  -->
        <!--================================-->
        <action id="4.2.2">
          <action-name>deleteProject</action-name>
        </action>
      </branch>
    </fork>
  </segment>
</process>
