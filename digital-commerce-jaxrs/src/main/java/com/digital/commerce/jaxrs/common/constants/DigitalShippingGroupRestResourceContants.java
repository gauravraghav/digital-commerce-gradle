package com.digital.commerce.jaxrs.common.constants;

/**
 * DigitalShippingGroupRestResourceContants
 *
 * @author TAIS
 *
 */
public class DigitalShippingGroupRestResourceContants extends DigitalJaxRsResourceCommonConstants {

	public static final String GENERATE_NICK_NAME = "generateNickname";

	public static final String PROFILE_PAGE = "profilePage";

	public static final String TRUE = "true";

	public static final String SAVE_AS_PRIMARY = "saveAsPrimary";

	public static final String HOME_ADDRESS = "homeAddress";

	public static final String ADDRESS_FIRSTNAME = "address.firstName";

	public static final String ADDRESS_LASTNAME = "address.lastName";

	public static final String ADDRESS_ADDRESS1 = "address.address1";

	public static final String ADDRESS_MIDDLENAME = "address.middleName";

	public static final String ADDRESS_ADDRESS2 = "address.address2";

	public static final String ADDRESS_CITY = "address.city";

	public static final String ADDRESS_STATE = "address.state";

	public static final String ADDRESS_COUNTRY = "address.country";

	public static final String ADDRESS_POSTAL_CODE = "address.postalCode";

	public static final String ADDRESS_PHONENUMBER = "address.phoneNumber";

	public static final String ADDRESS_RANK = "address.rank";

	public static final String ADDRESS_ADDRESSTYPE = "address.addressType";

	public static final String ADDRESS_REGION = "address.region";

	public static final String ADDRESS_POBOX = "address.isPoBox";

	public static final String ADDRESS_VARIFICATION = "address.addressVerification";

	public static final String CUSTOMER_DECISION = "customerDecision";

	public static final String SHIPPING_SERVICE_RESPONSE = "ShippingServiceResponse";

	public static final String MAILLING_ADDRESS_EXIST = "mailingAddressExists";

	public static final String HARDGOOR_SHIPPINGGROUP_NAME = "hardgoodShippingGroupName";

	public static final String HARDGOOR_SHIPPINGGROUP_TYPE = "hardgoodShippingGroupType";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_RANK = "hardgoodShippingGroup.shippingAddress.rank";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_PHONENUMBER = "hardgoodShippingGroup.shippingAddress.phoneNumber";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_VARIFICATION = "hardgoodShippingGroup.shippingAddress.addressVerification";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_POBOX = "hardgoodShippingGroup.shippingAddress.isPoBox";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_REGION = "hardgoodShippingGroup.shippingAddress.region";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_ADDRESSTYPE = "hardgoodShippingGroup.shippingAddress.addressType";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_POSTAL_CODE = "hardgoodShippingGroup.shippingAddress.postalCode";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_COUNTRY = "hardgoodShippingGroup.shippingAddress.country";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_STATE = "hardgoodShippingGroup.shippingAddress.state";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_CITY = "hardgoodShippingGroup.shippingAddress.city";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_ADDRESS2 = "hardgoodShippingGroup.shippingAddress.address2";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_ADDRESS1 = "hardgoodShippingGroup.shippingAddress.address1";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_MIDDLENAME = "hardgoodShippingGroup.shippingAddress.middleName";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_FIRSTNAME = "hardgoodShippingGroup.shippingAddress.firstName";

	public static final String SHIPTYPE = "shipType";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_SHIPTYPE = "hardgoodShippingGroup.shippingAddress.shipType";

	public static final String STORE_ID = "storeId";

	public static final String HARDGOOD_SHIPPING_GROUP_ADDRESS_STOREID = "hardgoodShippingGroup.shippingAddress.storeId";

}
