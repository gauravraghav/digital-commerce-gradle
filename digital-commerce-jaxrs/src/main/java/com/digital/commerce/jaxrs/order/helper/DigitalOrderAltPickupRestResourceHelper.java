package com.digital.commerce.jaxrs.order.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;

import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalOrderAltPickupRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalOrderLookupRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalProfileRestResourceContants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.purchase.DigitalAltOrderPickupFormHandler;
import com.digital.commerce.services.profile.rewards.LoyaltyCertificate;
import com.digital.commerce.services.promotion.AppliedOffer;
import com.digital.commerce.services.promotion.AppliedOffer.AppliedPromotion;
import com.digital.commerce.services.promotion.DigitalCouponFormHandler;
import com.digital.commerce.services.utils.DigitalServiceConstants;

import atg.commerce.order.OrderHolder;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.ShoppingCartFormHandler;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.service.jaxrs.DropletInvoker;
import atg.service.jaxrs.OParam;
import atg.service.jaxrs.Output;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

/**
 * This class provides OrderAltPickupActor's chain ids implementations.
 * 
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalOrderAltPickupRestResourceHelper extends GenericService {
	
	
	/**
	 * mCommonHelper - Common Helper
	 */
	private DigitalCommonRestResourceHelper commonHelper;
	

	/**
	 * Provide add alt pickup person details
	 * 
	 * @return lResponseMap
	 * 
	 */
	public Map<String, Object> addAltPickupPersonDetails(Map<String, Object> pRequestPayload)throws JSONException, Exception {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addAltPickupPersonDetails");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		FormHandlerExecutor executor = new FormHandlerExecutor(DigitalOrderAltPickupRestResourceConstants.ALT_ORDER_PICKUP_FORMHANDLER_COMPONENT_NAME,
				DigitalOrderAltPickupRestResourceConstants.ADD_ALT_ORDER_PICKUP);
		executor.addInput(DigitalOrderAltPickupRestResourceConstants.ALT_PICKUP_FIRST_NAME, pRequestPayload.get(DigitalOrderAltPickupRestResourceConstants.FNAME));
		executor.addInput(DigitalOrderAltPickupRestResourceConstants.ALT_PICKUP_LAST_NAME, pRequestPayload.get(DigitalOrderAltPickupRestResourceConstants.LNAME));
		executor.addInput(DigitalOrderAltPickupRestResourceConstants.ALT_PICKUP_EMAIL, pRequestPayload.get(DigitalOrderAltPickupRestResourceConstants.EMAIL));

		FormHandlerInvocationResult lResult = executor.execute();
		if (lResult.isFormError()) {
			lResponseMap = getCommonHelper().createGenericErrorResponse(lResult);
		} else {
			DigitalAltOrderPickupFormHandler lFormResult = (DigitalAltOrderPickupFormHandler) lResult.getFormHandler();
			DigitalOrderImpl lShoppongCart = (DigitalOrderImpl) lFormResult.getShoppingCart().getCurrent();
			lResponseMap.put(DigitalOrderAltPickupRestResourceConstants.ALT_PICKUP_FIRST_NAME,lShoppongCart.getAltPickupFirstName());
			lResponseMap.put(DigitalOrderAltPickupRestResourceConstants.ALT_PICKUP_LAST_NAME,lShoppongCart.getAltPickupLastName());
			lResponseMap.put(DigitalOrderAltPickupRestResourceConstants.ALT_PICKUP_EMAIL,lShoppongCart.getAltPickupEmail());
		}
		
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addAltPickupPersonDetails");
		}
		return lResponseMap;
	}
	/**
	 * Provide remove alt pickup person details
	 * 
	 * @return lResponseMap
	 * 
	 */
	public Map<String, Object> removeAltPickupPersonDetails()throws JSONException, Exception {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method removeAltPickupPersonDetails");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		FormHandlerExecutor executor = new FormHandlerExecutor(DigitalOrderAltPickupRestResourceConstants.ALT_ORDER_PICKUP_FORMHANDLER_COMPONENT_NAME,
				DigitalOrderAltPickupRestResourceConstants.REMOVE_ALT_ORDER_PICKUP);

		FormHandlerInvocationResult lResult = executor.execute();
		if (lResult.isFormError()) {
			lResponseMap = getCommonHelper().createGenericErrorResponse(lResult);
		} else {
			DigitalAltOrderPickupFormHandler lFormResult = (DigitalAltOrderPickupFormHandler) lResult.getFormHandler();
			DigitalOrderImpl lShoppongCart = (DigitalOrderImpl) lFormResult.getShoppingCart().getCurrent();
			lResponseMap.put(DigitalOrderAltPickupRestResourceConstants.ALT_PICKUP_FIRST_NAME,lShoppongCart.getAltPickupFirstName());
			lResponseMap.put(DigitalOrderAltPickupRestResourceConstants.ALT_PICKUP_LAST_NAME,lShoppongCart.getAltPickupLastName());
			lResponseMap.put(DigitalOrderAltPickupRestResourceConstants.ALT_PICKUP_EMAIL,lShoppongCart.getAltPickupEmail());
		}
		
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method removeAltPickupPersonDetails");
		}
		return lResponseMap;
	}
}
