package com.digital.commerce.jaxrs.catalog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import com.digital.commerce.services.catalog.DigitalCatalogTools;

import atg.adapter.gsa.ChangeAwareList;
import atg.adapter.gsa.ChangeAwareSet;
import atg.core.util.StringList;
import atg.json.JSONException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

/**
 * Extended DigitalCatalogTools class to create Jax-rs services response
 *
 * @author TAIS
 *
 */
public class DigitalCatalogToolsJaxRs extends DigitalCatalogTools {

	/**
	 * This method gets a Repository item (any Repository item) and converts it to Map of Maps
	 *
	 * @param repItem
	 *            - Any Repository Item. It can be product or sku or material or anything
	 * @param filter
	 *            - Filter variable which lists out only required properties from each Item descriptor. Please note that
	 *            this filter is for each Item descriptor and not at Repository level.
	 * @param addAttributes
	 *            - Add any new attribute that are missing in the filter. Useful in WishList page where childSKU should not
	 *            be sent for products in wishlist (NOTE: This accepts only new attributes for Product item descriptor)
	 * @return - Returns Map of Maps with required properties
	 * @throws JSONException
	 * @throws RepositoryException
	 */
	@Override
	public Map<String, Object> BuildRepositoryMap(RepositoryItem repItem, Map<String, StringList> filter, String... addAttributes)
			throws JSONException, RepositoryException, Exception {

		Map<String, Object> output = new HashMap<>();

		String itemDescriptor = repItem.getItemDescriptor().getItemDescriptorName();
		Boolean isPriceinCart = false;

		// Ugly check to ensure this Item descriptor's parent is "product" (shoe, handbag etc.,)
		if (repItem.getItemDescriptor().hasProperty(CHILD_SKUS)) {
			itemDescriptor = PRODUCT_ITEM_DESCRIPTOR;
			// Determine if this product has "priceInCart" enabled
			Object priceinCartValue = repItem.getPropertyValue(PRODUCT_ATTR_PRICEINCART);
			if (priceinCartValue != null && (Boolean) priceinCartValue) {
				isPriceinCart = true;
			}
		} else if (addAttributes != null && addAttributes.length == 1
				&& addAttributes[0].toString().equals(PRODUCT_ATTR_PRICEINCART + ":" + "true")) {
			// set priceIncart if this condition satisifies
			isPriceinCart = true;
			Arrays.fill(addAttributes, ""); // empty this array now
		}

		if (filter != null && filter.containsKey(itemDescriptor)) {
			// This item descriptor has a filter set. Let's loop through only these attributes

			// Let's merge optional filter Array (addAttributes) with mandatory filter array (filter) to form the final filter Array
			String[] filterArray = (String[]) ArrayUtils.addAll(filter.get(itemDescriptor).getStrings(), addAttributes);
			// Now, loop through merged filterArray
			for (String propName : filterArray) {
				if (repItem.getItemDescriptor().hasProperty(propName)) {
					Object propValue = repItem.getPropertyValue(propName);

					if (propValue instanceof RepositoryItem) {
						// recursive call this same method if type is repository Item
						if (isPriceinCart) // If priceinCart is true then we need to send this to the recursive call. This is required to
							// handle price logic at SKU level
							output.put(propName, BuildRepositoryMap((RepositoryItem) propValue, filter,
									PRODUCT_ATTR_PRICEINCART + ":" + isPriceinCart.toString()));
						else
							output.put(propName, BuildRepositoryMap((RepositoryItem) propValue, filter));
					} else if (propValue instanceof ChangeAwareList) {
						// ChangeAwareList is usually a List of Repository Items
						ChangeAwareList repList = (ChangeAwareList) propValue;
						List<Object> innerOutput = new ArrayList<>();
						for (int i = 0; i < repList.size(); i++) {
							Object repListitem = repList.get(i);
							if (repListitem instanceof RepositoryItem) {
								if (isPriceinCart) // If priceinCart is true then we need to send this to the recursive call. This is
									// required to handle price logic at SKU level
									innerOutput.add(BuildRepositoryMap((RepositoryItem) repListitem, filter,
											PRODUCT_ATTR_PRICEINCART + ":" + isPriceinCart.toString()));
								else
									innerOutput.add(BuildRepositoryMap((RepositoryItem) repListitem, filter));
							} else {
								innerOutput.add(repListitem);
							}
						}
						output.put(propName, innerOutput);
					} else if (propValue instanceof ChangeAwareSet) {
						// ChangeAwareList is usually a List of Repository Items
						ChangeAwareSet repList = (ChangeAwareSet) propValue;
						List<Object> innerOutput = new ArrayList<>();
						for (Object repListitem : repList) {
							if (repListitem instanceof RepositoryItem) {
								if (isPriceinCart) // If priceinCart is true then we need to send this to the recursive call. This is
									// required to handle price logic at SKU level
									innerOutput.add(BuildRepositoryMap((RepositoryItem) repListitem, filter,
											PRODUCT_ATTR_PRICEINCART + ":" + isPriceinCart.toString()));
								else
									innerOutput.add(BuildRepositoryMap((RepositoryItem) repListitem, filter));
							} else {
								innerOutput.add(repListitem);
							}
						}
						output.put(propName, innerOutput);
					} else if (propValue != null) {
						// If "priceInCart=true then set all Prices to 0. Do this only for "product" item descriptor.
						if (isPriceinCart && getPriceInCartPrices() != null && getPriceInCartPrices().size() > 0
								&& Arrays.asList(getPriceInCartPrices().stringAt(0).split(",")).contains(propName)) {
							output.put(propName, "0.0");
						} else {
							if (propValue instanceof String) // Should trim only Strings
								output.put(propName, propValue.toString().trim());
							else
								output.put(propName, propValue);
						}
					}
				}
			}

		} else {
			// This item descriptor does not have a filter set. Let's loop through all attributes
			// PriceinCart logic not added here as we assume SKU and Product will always have Filter set
			for (String propName : repItem.getItemDescriptor().getPropertyNames()) {

				Object propValue = repItem.getPropertyValue(propName);
				if (propValue instanceof RepositoryItem) {
					// recursive call this same method if type is repository Item
					output.put(propName, BuildRepositoryMap((RepositoryItem) propValue, filter));
				} else if (propValue instanceof ChangeAwareList) {
					// ChangeAwareList is usually a List of Repository Items
					ChangeAwareList repList = (ChangeAwareList) propValue;
					Map<String, Object> innerOutput = new HashMap<>();
					for (int i = 0; i < repList.size(); i++) {
						Object repListitem = repList.get(i);
						if (repListitem instanceof RepositoryItem) {
							innerOutput.put(Integer.toString(i), BuildRepositoryMap((RepositoryItem) repListitem, filter));
						} else {
							innerOutput.put(Integer.toString(i), repListitem);
						}
					}
					output.put(propName, innerOutput);
				} else if (propValue != null) {
					// If "priceInCart=true then set all Prices to 0. Do this only for "product" item descriptor.
					if (isPriceinCart && this.getPriceInCartPrices() != null && this.getPriceInCartPrices().size() > 0
							&& Arrays.asList(getPriceInCartPrices().stringAt(0).split(",")).contains(propName)) {
						output.put(propName, "0.0");
					} else {
						if (propValue instanceof String) // Should trim only Strings
							output.put(propName, propValue.toString().trim());
						else
							output.put(propName, propValue);
					}
				}
			}
		}

		return output;
	}

}
