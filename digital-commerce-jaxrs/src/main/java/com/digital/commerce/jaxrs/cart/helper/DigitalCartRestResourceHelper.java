package com.digital.commerce.jaxrs.cart.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.jaxrs.common.constants.DigitalCartResourceConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.services.order.purchase.DigitalCartModifierFormHandler;

import atg.commerce.order.Order;
import atg.commerce.order.purchase.CartModifierFormHandler;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.droplet.TagConverterManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.service.jaxrs.DropletInvoker;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

/**
 * Helper class for Cart Resource
 *
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalCartRestResourceHelper extends GenericService {

	/**
	 * commonHelper - Common Helper
	 */
	private DigitalCommonRestResourceHelper commonHelper;

	/**
	 * Helper method to build error response for cart services
	 *
	 * @param pResult
	 *            Execution Result
	 * @return lErrorMap
	 */
	public Map<String, Object> populateErrorResponse(FormHandlerInvocationResult pResult) {
		Map<String, Object> lErrorMap = new HashMap<>();
		lErrorMap.put(DigitalCartResourceConstants.FORM_ERROR, true);
		lErrorMap.put(DigitalCartResourceConstants.FORM_EXCEPTION, getCommonHelper().createFormExceptionResponse(pResult.getFormExceptions()));
		CartModifierFormHandler lFH = (CartModifierFormHandler) pResult.getFormHandler();
		lErrorMap.put(DigitalCartResourceConstants.CONCURRENT_UPDATE, lFH.isConcurrentUpdate());
		return lErrorMap;
	}

	/**
	 * Helper method to add form input for add item service
	 *
	 * @param pExecutor
	 *            Form Handler
	 * @param pRequestMap
	 *            Request Payload
	 * @throws ServletException
	 */
	public void addInputForAddItem(FormHandlerExecutor pExecutor, Map<String, Object> pRequest) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method addInputForAddItem()");
		}

		pExecutor.addInput(DigitalCartResourceConstants.CATALOG_REF_IDS, pRequest.get(DigitalCartResourceConstants.CATALOG_REF_IDS),
				TagConverterManager.getTagConverterByName("array"), new Properties());
		pExecutor.addInput(DigitalCartResourceConstants.PRODUCT_ID, pRequest.get(DigitalCartResourceConstants.PRODUCT_ID));
		pExecutor.addInput(DigitalCartResourceConstants.QUANTITY, Integer.parseInt((String) pRequest.get(DigitalCartResourceConstants.QUANTITY)));
		pExecutor.addInput(DigitalCartResourceConstants.LOCATION_ID, pRequest.get(DigitalCartResourceConstants.LOCATION_ID));
		pExecutor.addInput(DigitalCartResourceConstants.SITE_ID, pRequest.get(DigitalCartResourceConstants.SITE_ID));
		pExecutor.addInput(DigitalCartResourceConstants.SHIP_TYPE, pRequest.get(DigitalCartResourceConstants.SHIP_TYPE));
		pExecutor.addInput(DigitalCartResourceConstants.STORE_ID, pRequest.get(DigitalCartResourceConstants.STORE_ID));
		pExecutor.addInput(DigitalCartResourceConstants.STORE_LOCATION_ID, pRequest.get(DigitalCartResourceConstants.STORE_LOCATION_ID));
		pExecutor.addInput(DigitalCartResourceConstants.GITT_RECEIPT, pRequest.get(DigitalCartResourceConstants.GITT_RECEIPT));
		pExecutor.addInput(DigitalCartResourceConstants.GIFT_RECEIPT_MESSAGE, pRequest.get(DigitalCartResourceConstants.GIFT_RECEIPT_MESSAGE));
		pExecutor.addInput(DigitalCartResourceConstants.GIFT_LIST_ITEM_ID, pRequest.get(DigitalCartResourceConstants.GIFT_LIST_ITEM_ID));

		if (DigitalStringUtil.isNotBlank((String) pRequest.get(DigitalCartResourceConstants.ADD_TO_WISH_LIST))) {
			Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
			RepositoryItem lWishlist = (RepositoryItem) lProfile.getPropertyValue(DigitalCartResourceConstants.WISHLIST);
			if (lWishlist != null) {
				pExecutor.addInput(DigitalCartResourceConstants.GIFT_LIST_ID, lWishlist.getRepositoryId());
			} else {
				pExecutor.addInput(DigitalCartResourceConstants.GIFT_LIST_ID, pRequest.get(DigitalCartResourceConstants.GIFT_LIST_ID));
			}
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method addInputForAddItem()");
		}

	}

	/**
	 * This method will populate add item success response.
	 *
	 * @param pOrder
	 *            Order
	 * @param pAddedFromWishlist
	 * @return lResponseMap
	 */
	public Map<String, Object> populateAddItemSuccessResponse(Order pOrder, boolean pAddedFromWishlist) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method populateAddItemSuccessResponse()");
		}
		Map<String, Object> lResponseMap = new HashMap<>();
		lResponseMap.put(DigitalCartResourceConstants.TOTAL_BAG_COUNT, pOrder.getTotalCommerceItemCount());
		lResponseMap.put(DigitalCartResourceConstants.ORDER_TOTAL, pOrder.getPriceInfo().getTotal());
		if (!pAddedFromWishlist) {
			lResponseMap.put(DigitalCartResourceConstants.SUB_TOTAL, pOrder.getPriceInfo().getRawSubtotal());
			lResponseMap.put(DigitalCartResourceConstants.COMMERCE_ITEMS, pOrder.getCommerceItems());
		}
		lResponseMap.put(DigitalCartResourceConstants.CLOSENESS_QUALIFIERS, getCommonHelper().populateClosenessQualifiers(pOrder, null));

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method populateAddItemSuccessResponse()");
		}
		return lResponseMap;
	}

	/**
	 * Helper method to add form input for update item service
	 *
	 * @param pExecutor
	 *            Form Handler
	 * @param pRequestMap
	 *            Request Payload
	 * @throws ServletException
	 */
	public void addInputForUpdateItem(FormHandlerExecutor pExecutor, Map<String, Object> pRequest) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method addInputForUpdateItem()");
		}

		pExecutor.addInput(DigitalCartResourceConstants.CATALOG_REF_IDS, pRequest.get(DigitalCartResourceConstants.CATALOG_REF_IDS),
				TagConverterManager.getTagConverterByName("array"), new Properties());
		pExecutor.addInput(DigitalCartResourceConstants.PRODUCT_ID, pRequest.get(DigitalCartResourceConstants.PRODUCT_ID));
		pExecutor.addInput(DigitalCartResourceConstants.ITEM, pRequest.get(DigitalCartResourceConstants.ITEM));
		pExecutor.addInput(DigitalCartResourceConstants.QUANTITY, Integer.parseInt((String) pRequest.get(DigitalCartResourceConstants.QUANTITY)));
		pExecutor.addInput(DigitalCartResourceConstants.SHIP_TYPE, pRequest.get(DigitalCartResourceConstants.SHIP_TYPE));
		pExecutor.addInput(DigitalCartResourceConstants.STORE_ID, pRequest.get(DigitalCartResourceConstants.STORE_ID));
		pExecutor.addInput(DigitalCartResourceConstants.STORE_LOCATION_ID, pRequest.get(DigitalCartResourceConstants.STORE_LOCATION_ID));

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method addInputForUpdateItem()");
		}

	}

	/**
	 * This method will populate update item success response.
	 *
	 * @param pOrder
	 *            Order
	 * @return lResponseMap
	 */
	public Map<String, Object> populateUpdateItemSuccessResponse(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method populateUpdateItemSuccessResponse()");
		}

		DropletInvoker lRepriceOrder = new DropletInvoker(DigitalCartResourceConstants.REPRICE_ORDER_DROPLET_PATH);
		lRepriceOrder.addInput(DigitalCartResourceConstants.PRICING_OP, DigitalCartResourceConstants.ORDER_TOTAL_PRICING_OP);
		lRepriceOrder.invoke();
		Map<String, Object> lResponseMap = new HashMap<>();
		lResponseMap.put(DigitalCartResourceConstants.TOTAL_BAG_COUNT, pOrder.getTotalCommerceItemCount());
		lResponseMap.put(DigitalCartResourceConstants.ORDER_TOTAL, pOrder.getPriceInfo().getTotal());
		lResponseMap.put(DigitalCartResourceConstants.CLOSENESS_QUALIFIERS, getCommonHelper().populateClosenessQualifiers(pOrder, null));

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method populateUpdateItemSuccessResponse()");
		}
		return lResponseMap;
	}

	/**
	 * Helper method to build paypal lookup success response
	 *
	 * @param pResult
	 *            Execution Result
	 * @return lResponse
	 */
	public Map<String, Object> populatePaypalLookupResponse(FormHandlerInvocationResult pResult) {
		Map<String, Object> lResponse = new HashMap<>();
		DigitalCartModifierFormHandler lFH = (DigitalCartModifierFormHandler) pResult.getFormHandler();
		lResponse.put(DigitalCartResourceConstants.ASC_URL, lFH.getAcsURL());
		lResponse.put(DigitalCartResourceConstants.PP_PAYLOAD, lFH.getPpPayload());
		lResponse.put(DigitalCartResourceConstants.CARDINAL_ORDER_ID, lFH.getCardinalOrderId());
		return lResponse;
	}
}
