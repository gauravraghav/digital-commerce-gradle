package com.digital.commerce.jaxrs.common.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.digital.commerce.common.services.DigitalBaseConstants;

import atg.nucleus.GenericService;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * @author TAIS
 *
 */
@RestResource(id = "com.digital.v1.utils.DigitalServiceUtil")
@Path("/utils")
@Api("Utility Service")
@Getter
@Setter
public class DigitalServiceUtil extends GenericService {
	
	
	/**
	 * constantService - Constant Service Component
	 */
	private DigitalBaseConstants constantService;
	
	//private static ResourceBundle lBundle = LayeredResourceBundle.getBundle("com.digital.commerce.common.rest.ServicesResource");
	
	/**
	 * Service to return All Constants
	 * 
	 * @see - /com/digital/commerce/services/v1_0/utils/DigitalConstantsActor/getAllConstants
	 * 
	 * @return Response
	 */
	@GET
	@Path("/getAllConstants")
	@Endpoint(id = "/utils/getAllConstants#GET", isSingular = true, filterId = "dsw-constants-response")
	public Response getAllConstants() {


		Map<String, Object> lResponse = new HashMap<String, Object>();

		Map<String, Object> lConstants = constantService.getAllConstants();
		
		if(!lConstants.isEmpty()){
			lResponse.put("Response", lConstants);
		}else{
			//TODO Need to change to resource bundle 
			lResponse.put("Error", "Constants Empty.");
		}
		return Response.ok(lResponse).build();
	}
}