package com.digital.commerce.jaxrs.order.helper;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;

import com.digital.commerce.jaxrs.common.constants.DigitalOrderCommonRestResourceConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.services.common.DigitalContactInfo;
import com.digital.commerce.services.order.contactinfo.DigitalOrderContactInfoFormHandler;

import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.GenericService;
import lombok.Getter;
import lombok.Setter;

/**
 * This class provides OrderAltPickupActor's chain ids implementations.
 * 
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalOrderContactInfoRestResourceHelper extends GenericService {
	
	
	/**
	 * commonHelper - Common Helper
	 */
	private DigitalCommonRestResourceHelper commonHelper;
	

	/**
	 * This method provide details of add and update of contact info of order.
	 * 
	 * @param pRequestPayload
	 * @return lResponseMap
	 */
	public Map<String, Object> addOrUpdateContactInfoDetails(Map<String, Object> pRequestPayload) throws JSONException, Exception {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addOrUpdateContactInfoDetails");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		FormHandlerExecutor executor = new FormHandlerExecutor(DigitalOrderCommonRestResourceConstants.ORDER_CONTACTINFO_FORMHANDLER_COMPONENT_NAME,
				DigitalOrderCommonRestResourceConstants.ADD_UPDATE_CONTACTINFO);
		executor.addInput(DigitalOrderCommonRestResourceConstants.CONTACT_ADDRESS_PHONE_NUMBER, pRequestPayload.get(DigitalOrderCommonRestResourceConstants.PHONENUMBER));
		executor.addInput(DigitalOrderCommonRestResourceConstants.CONTACT_ADDRESS_EMAIL, pRequestPayload.get(DigitalOrderCommonRestResourceConstants.EMAIL));
		executor.addInput(DigitalOrderCommonRestResourceConstants.RECEIVE_EMAIL_OFFERS, pRequestPayload.get(DigitalOrderCommonRestResourceConstants.RECEIVE_EMAIL_OFFERS));

		FormHandlerInvocationResult lResult = executor.execute();
		DigitalOrderContactInfoFormHandler lResultHandler = (DigitalOrderContactInfoFormHandler) lResult.getFormHandler();
		DigitalContactInfo lContactInfo = lResultHandler.processRequest();
		if (lResult.isFormError()) {
			lResponseMap = getCommonHelper().createGenericErrorResponse(lResult);
		}else{
			lResponseMap.put(DigitalOrderCommonRestResourceConstants.CONTACT_INFO, lContactInfo);
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addOrUpdateContactInfoDetails");
		}
		return lResponseMap;
	}
}
