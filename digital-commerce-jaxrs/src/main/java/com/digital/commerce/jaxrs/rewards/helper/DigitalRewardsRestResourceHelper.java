package com.digital.commerce.jaxrs.rewards.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;

import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalRewardResourceConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.services.rewards.DigitalRewardsComponent;
import com.digital.commerce.services.rewards.DigitalRewardsFormHandler;
import com.digital.commerce.services.rewards.domain.RewardsAddUpdateGiftResponse;
import com.digital.commerce.services.rewards.domain.RewardsAddUpdateShopforResponse;

import atg.commerce.order.Order;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.droplet.TagConverterManager;
import atg.nucleus.GenericService;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

/**
 * This class will provide DigitalRewards Actor responses and helper for rest
 * resources.
 * 
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalRewardsRestResourceHelper extends GenericService {

	/**
	 * mRewardsComponent - DigitalRewardsComponent Component
	 */
	public DigitalRewardsComponent rewardsComponent;

	/**
	 * mCommonHelper - Common Helper
	 */
	private DigitalCommonRestResourceHelper commonHelper;

	/**
	 * Populate RewardsCerts
	 * 
	 * @return lResponseMap
	 */
	public Map<String, Object> populateRewardsCerts() {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateRewardsCerts");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();

		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		if (lProfile != null) {
			lResponseMap.put(DigitalJaxRsResourceCommonConstants.REWARDS_CERTIFICATES,
					getRewardsComponent().retrieveRewardCertificatesByProfile(lProfile));
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateRewardsCerts");
		}
		return lResponseMap;
	}

	/**
	 * Populate anonymous email subscription response from formhandler
	 * invocation result
	 *
	 * @param result
	 *            the formhandler invocation result
	 * @return the map
	 */
	public Map<String, Object> populateAnonymousEmailSubscriptionResponse(FormHandlerInvocationResult result) {
		vlogDebug("Start -> {0} Method populateAnonymousEmailSubscriptionResponse. result {1}",
				getClass().getCanonicalName(), result);
		Map<String, Object> lFinalResponse = new HashMap<>();
		if (result != null) {
			DigitalRewardsFormHandler rewardFormHandler = (DigitalRewardsFormHandler) result.getFormHandler();
			if (rewardFormHandler != null) {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
						rewardFormHandler.getRewardsSubscribeEmailFooterResponse());
				if (isLoggingDebug()) {
					logDebug("populateAnonymousEmailSubscriptionResponse lFinalResponse=" + lFinalResponse);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateAnonymousEmailSubscriptionResponse");
		}
		return lFinalResponse;
	}

	/**
	 * Populate missing points result from formhandler and add to FinalResponse
	 * Map.
	 *
	 * @param result
	 *            the formhandler invocation result
	 * @return the map
	 */
	public Map<String, Object> populateAddMissingPointsResponse(FormHandlerInvocationResult result) {
		vlogDebug("Start -> {0} Method populateAddMissingPointsResponse. result {1}", getClass().getCanonicalName(),
				result);
		Map<String, Object> lFinalResponse = new HashMap<>();
		if (result != null) {
			DigitalRewardsFormHandler rewardFormHandler = (DigitalRewardsFormHandler) result.getFormHandler();
			if (rewardFormHandler != null) {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
						rewardFormHandler.getRewardsAddShopWithoutACardResponse());
				if (isLoggingDebug()) {
					logDebug("populateAddMissingPointsResponse lFinalResponse=" + lFinalResponse);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateAddMissingPointsResponse");
		}
		return lFinalResponse;
	}

	/**
	 * Populate Rewards Details Response from Formhandler and add to
	 * FinalResponse Map.
	 *
	 * @param result
	 *            the formhandler invocation result
	 * @return the map
	 */
	public Map<String, Object> populateRewardDetailsResponse(FormHandlerInvocationResult result) {
		vlogDebug("Start -> {0} Method populateRewardDetailsResponse. result {1}", getClass().getCanonicalName(),
				result);
		Map<String, Object> lFinalResponse = new HashMap<>();
		if (result != null) {
			DigitalRewardsFormHandler rewardFormHandler = (DigitalRewardsFormHandler) result.getFormHandler();
			if (rewardFormHandler != null) {
				if (rewardFormHandler.getRetrieveRewardDetailsResponse().getFormError()) {

					lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
							rewardFormHandler.getRetrieveRewardDetailsResponse().getDetails());
				} else {
					lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
							rewardFormHandler.getRetrieveRewardDetailsResponse().getDetails());
				}

				if (isLoggingDebug()) {
					logDebug("populateRewardDetailsResponse lFinalResponse=" + lFinalResponse);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateRewardDetailsResponse");
		}
		return lFinalResponse;
	}
	
	/**
	 * Populate Rewards Charities Response from FormHandler and add to FinalResponse Map.
	 *  
	 * @param result
	 * @return Map
	 */
	public Map<String, Object> populateRewardsCharitiesResponse(FormHandlerInvocationResult result) {
		vlogDebug("Start -> {0} Method populateRewardsCharitiesResponse. result {1}",
				getClass().getCanonicalName(), result);
		Map<String, Object> lFinalResponse = new HashMap<>();
		if (result != null) {
			DigitalRewardsFormHandler rewardFormHandler = (DigitalRewardsFormHandler) result.getFormHandler();
			if (rewardFormHandler != null) {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
						rewardFormHandler.getRewardsCharitiesResponse());
				if (isLoggingDebug()) {
					logDebug("populateRewardsCharitiesResponse lFinalResponse=" + lFinalResponse);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateRewardsCharitiesResponse");
		}
		return lFinalResponse;
	}

	/**
	 * Gets the shop for filters response map.
	 *
	 * @return responseMap the shop for filters response map
	 * @throws DigitalAppException
	 */
	public Map<String, Object> getShopForFiltersResponseMap() throws DigitalAppException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getShopForFiltersResponseMap");
		}
		Map<String, Object> filtersShopForMap = getRewardsComponent().getRewardCertificateManager().getShopforTools()
				.getFiltersforShopfor();
		if (isLoggingDebug()) {
			logDebug("getShopForFiltersResponseMap filtersShopForMap=" + filtersShopForMap);
		}
		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE, filtersShopForMap);
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getShopForFiltersResponseMap");
		}
		return responseMap;
	}

	/**
	 * Adds the input for create shop for item from request map
	 *
	 * @param pExecutor
	 *            the form handler executor
	 * @param pRequestMap
	 *            the request map
	 * @throws ServletException
	 *             the servlet exception
	 */
	public void addInputForCreateShopForItem(FormHandlerExecutor pExecutor, Map<String, Object> pRequestMap)
			throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addInputForCreateShopForItem");
			logDebug("addInputForCreateShopForItem pRequestMap=" + pRequestMap);
		}
		if (pExecutor != null && pRequestMap != null && !pRequestMap.isEmpty()) {
			pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_NAME_KEY,
					pRequestMap.get(DigitalRewardResourceConstants.SHOP_FOR_NAME));
			pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_RELATION_KEY,
					pRequestMap.get(DigitalRewardResourceConstants.RELATION_SHIP));
			pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_BIRTHDAY_KEY,
					String.valueOf(pRequestMap.get(DigitalRewardResourceConstants.BIRTH_DAY)));
			pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_BIRTH_MONTH_KEY,
					String.valueOf(pRequestMap.get(DigitalRewardResourceConstants.BIRTH_MONTH)));
			pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_GENDER_KEY,
					pRequestMap.get(DigitalRewardResourceConstants.GENDER));
			pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_WEB_TYPE,
					pRequestMap.get(DigitalRewardResourceConstants.WEB_TYPE));
			pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_SIZE_GROUP_KEY,
					pRequestMap.get(DigitalRewardResourceConstants.SIZE_GROUP));
			Object sizes = pRequestMap.get(DigitalRewardResourceConstants.SIZES);
			String[] sizesArray = DigitalJaxRsUtility.checkAndGetStringArray(sizes);
			if (sizesArray != null) {
				pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_SIZES_KEY, sizesArray,
						TagConverterManager.getTagConverterByName(DigitalRewardResourceConstants.ARRAY), new Properties());
			} else {
				pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_SIZES_KEY, sizes,
						TagConverterManager.getTagConverterByName(DigitalRewardResourceConstants.ARRAY), new Properties());
			}
			Object widths = pRequestMap.get(DigitalRewardResourceConstants.WIDTHS);
			String[] widthsArray = DigitalJaxRsUtility.checkAndGetStringArray(widths);
			if (widthsArray != null) {
				pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_WIDTHS_KEY, widthsArray,
						TagConverterManager.getTagConverterByName(DigitalRewardResourceConstants.ARRAY), new Properties());
			} else {
				pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_WIDTHS_KEY, widths,
						TagConverterManager.getTagConverterByName(DigitalRewardResourceConstants.ARRAY), new Properties());
			}
			vlogDebug("addInputForCreateShopForItem sizes {0}, widths {1}", sizes, widths);
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addInputForCreateShopForItem");
		}
	}

	/**
	 * Adds the input for create gift item from request map
	 *
	 * @param pExecutor
	 *            the form handler executor
	 * @param pRequestMap
	 *            the request map
	 * @throws ServletException
	 *             the servlet exception
	 */
	public void addInputForCreateGiftItem(FormHandlerExecutor pExecutor, Map<String, Object> pRequestMap)
			throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addInputForCreateGiftItem");
			logDebug("addInputForCreateGiftItem pRequestMap=" + pRequestMap);
		}
		if (pExecutor != null && pRequestMap != null && !pRequestMap.isEmpty()) {
			pExecutor.addInput(DigitalRewardResourceConstants.GIFT_FIRST_NAME_KEY,
					pRequestMap.get(DigitalRewardResourceConstants.FIRSTNAME));
			pExecutor.addInput(DigitalRewardResourceConstants.GIFT_LAST_NAME_KEY,
					pRequestMap.get(DigitalRewardResourceConstants.LASTNAME));
			pExecutor.addInput(DigitalRewardResourceConstants.GIFT_BIRTHDAY_KEY,
					String.valueOf(pRequestMap.get(DigitalRewardResourceConstants.BIRTH_DAY)));
			pExecutor.addInput(DigitalRewardResourceConstants.GIFT_BIRTH_MONTH_KEY,
					String.valueOf(pRequestMap.get(DigitalRewardResourceConstants.BIRTH_MONTH)));
			pExecutor.addInput(DigitalRewardResourceConstants.GIFT_EMAIL_KEY,
					pRequestMap.get(DigitalRewardResourceConstants.EMAIL));
			pExecutor.addInput(DigitalRewardResourceConstants.GIFT_MESSAGE_KEY,
					pRequestMap.get(DigitalRewardResourceConstants.GIFT_MESSAGE));
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addInputForCreateGiftItem");
		}
	}

	/**
	 * Adds the input for update gift item from request map
	 *
	 * @param pExecutor
	 *            the form handler executor
	 * @param pRequestMap
	 *            the request map
	 * @throws ServletException
	 *             the servlet exception
	 */
	public void addInputForUpdateGiftItem(FormHandlerExecutor pExecutor, Map<String, Object> pRequestMap)
			throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addInputForUpdateGiftItem");
			logDebug("addInputForUpdateGiftItem pRequestMap=" + pRequestMap);
		}
		if (pExecutor != null && pRequestMap != null && !pRequestMap.isEmpty()) {
			pExecutor.addInput(DigitalRewardResourceConstants.GIFT_MESSAGE_ID_KEY,
					pRequestMap.get(DigitalRewardResourceConstants.ID));
			addInputForCreateGiftItem(pExecutor, pRequestMap);
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addInputForUpdateGiftItem");
		}
	}

	/**
	 * Adds the input for update shop for item from request map
	 *
	 * @param pExecutor
	 *            the form handler executor
	 * @param pRequestMap
	 *            the request map
	 * @throws ServletException
	 *             the servlet exception
	 */
	public void addInputForUpdateShopForItem(FormHandlerExecutor pExecutor, Map<String, Object> pRequestMap)
			throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addInputForUpdateShopForItem");
			logDebug("addInputForUpdateShopForItem pRequestMap=" + pRequestMap);
		}
		if (pExecutor != null && pRequestMap != null && !pRequestMap.isEmpty()) {
			populateShopForIdDetails(pExecutor, pRequestMap);
			addInputForCreateShopForItem(pExecutor, pRequestMap);
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addInputForUpdateShopForItem");
		}
	}

	/**
	 * Utility method for populating shop for id details from request map
	 *
	 * @param pExecutor
	 *            the executor
	 * @param pRequestMap
	 *            the request map
	 * @throws ServletException
	 *             the servlet exception
	 */
	private void populateShopForIdDetails(FormHandlerExecutor pExecutor, Map<String, Object> pRequestMap)
			throws ServletException {
		pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_ID_KEY,
				pRequestMap.get(DigitalRewardResourceConstants.SHOP_FOR_ID));
		pExecutor.addInput(DigitalRewardResourceConstants.SHOP_FOR_REWARDS_ID_KEY,
				pRequestMap.get(DigitalRewardResourceConstants.SHOP_FOR_REWARDS_ID));
	}

	/**
	 * Helper method to build error response for reward services
	 *
	 * @param pResult
	 *            Execution Result
	 * @return lErrorMap
	 */
	public Map<String, Object> populateErrorResponse(FormHandlerInvocationResult pResult) {
		vlogDebug("Start -> {0} Method populateErrorResponse. result {1}", getClass().getCanonicalName(), pResult);
		Map<String, Object> lErrorMap = new HashMap<>();
		if (pResult != null) {
			lErrorMap.put(DigitalRewardResourceConstants.FORM_ERROR, true);
			lErrorMap.put(DigitalRewardResourceConstants.FORM_EXCEPTION,
					getCommonHelper().createFormExceptionResponse(pResult.getFormExceptions()));
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateErrorResponse");
		}
		return lErrorMap;
	}

	/**
	 * Helper method to build success response for shop for item
	 * create/update/delete services
	 *
	 * @param pResult
	 *            Execution Result
	 * @return responseMap
	 */
	public Map<String, Object> populateShopForItemSuccessResponse(FormHandlerInvocationResult pResult) {
		vlogDebug("Start -> {0} Method populateShopForItemSuccessResponse. result {1}", getClass().getCanonicalName(),
				pResult);
		Map<String, Object> responseMap = new HashMap<>();
		if (pResult != null && pResult.getFormHandler() != null) {
			DigitalRewardsFormHandler rewardFormHandler = (DigitalRewardsFormHandler) pResult.getFormHandler();
			RewardsAddUpdateShopforResponse addUpdateShopForResponse = rewardFormHandler
					.getRewardsAddUpdateShopforResponse();
			if (addUpdateShopForResponse != null) {
				responseMap.put(DigitalJaxRsResourceCommonConstants.STATUS_CODE, DigitalJaxRsResourceCommonConstants.SUCCESS);
				responseMap.put(DigitalJaxRsResourceCommonConstants.COUNT, addUpdateShopForResponse.getCount());
				if (addUpdateShopForResponse.getItem() != null) {
					responseMap.put(DigitalJaxRsResourceCommonConstants.ID,
							addUpdateShopForResponse.getItem().getShopforID());
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("populateShopForItemSuccessResponse responseMap=" + responseMap);
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateShopForItemSuccessResponse");
		}
		return responseMap;
	}

	/**
	 * Helper method to build success response for gift item create/update
	 * services
	 *
	 * @param pResult
	 *            Execution Result
	 * @return responseMap
	 */
	public Map<String, Object> populateGiftItemSuccessResponse(FormHandlerInvocationResult pResult) {
		vlogDebug("Start -> {0} Method populateGiftItemSuccessResponse. result {1}", getClass().getCanonicalName(),
				pResult);
		Map<String, Object> responseMap = new HashMap<>();
		if (pResult != null && pResult.getFormHandler() != null) {
			DigitalRewardsFormHandler rewardFormHandler = (DigitalRewardsFormHandler) pResult.getFormHandler();
			RewardsAddUpdateGiftResponse addUpdatGiftResponse = rewardFormHandler.getRewardsAddUpdateGiftResponse();
			if (addUpdatGiftResponse != null) {
				responseMap.put(DigitalJaxRsResourceCommonConstants.STATUS_CODE, DigitalJaxRsResourceCommonConstants.SUCCESS);
				responseMap.put(DigitalJaxRsResourceCommonConstants.COUNT, addUpdatGiftResponse.getCount());
				if (addUpdatGiftResponse.getItem() != null) {
					responseMap.put(DigitalJaxRsResourceCommonConstants.ID, addUpdatGiftResponse.getItem().getId());
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("populateGiftItemSuccessResponse responseMap=" + responseMap);
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateGiftItemSuccessResponse");
		}
		return responseMap;
	}

	/**
	 * Helper method to build success response for donate and issue cert
	 *
	 * @return the map
	 */
	public Map<String, Object> populateCertSuccessResponse() {
		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put(DigitalJaxRsResourceCommonConstants.STATUS_CODE, DigitalJaxRsResourceCommonConstants.SUCCESS);
		return responseMap;
	}

	/**
	 * Perform reprice order.
	 *
	 * @param pResult
	 *            the result
	 */
	public void performRepriceOrder(FormHandlerInvocationResult pResult) {
		vlogDebug("Start -> {0} Method performRepriceOrder. result {1}", getClass().getCanonicalName(), pResult);
		if (pResult != null && pResult.getFormHandler() != null) {
			DigitalRewardsFormHandler rewardFormHandler = (DigitalRewardsFormHandler) pResult.getFormHandler();
			Order pOrder = rewardFormHandler.getOrder();
			if (pOrder != null) {
				getCommonHelper().invokeRepriceOrderDroplet(pOrder,
						DigitalJaxRsResourceCommonConstants.ORDER_TOTAL_PRICING_OP);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method performRepriceOrder");
		}
	}

	/**
	 * Adds the input for donate certs
	 *
	 * @param pExecutor
	 *            the form handler executor
	 * @param pRequestMap
	 *            the request map
	 * @throws ServletException
	 *             the servlet exception
	 */
	public void addInputForDonateCerts(FormHandlerExecutor pExecutor, Map<String, Object> pRequestMap)
			throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addInputForDonateCerts");
			logDebug("addInputForDonateCerts pRequestMap=" + pRequestMap);
		}
		if (pExecutor != null && pRequestMap != null && !pRequestMap.isEmpty()) {
			pExecutor.addInput(DigitalRewardResourceConstants.CHARITY_ID_KEY,
					String.valueOf(pRequestMap.get(DigitalRewardResourceConstants.CHARITY_ID)));
			Object certIds = pRequestMap.get(DigitalRewardResourceConstants.CERT_IDS);
			String certsString = certIds != null ? certIds.toString() : null;
			pExecutor.addInput(DigitalRewardResourceConstants.CERT_IDS_KEY, certsString);
			vlogDebug("addInputForDonateCerts certsString=" + certsString);
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addInputForDonateCerts");
		}
	}
}
