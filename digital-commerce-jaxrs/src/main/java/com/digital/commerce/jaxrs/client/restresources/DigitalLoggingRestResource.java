package com.digital.commerce.jaxrs.client.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.jaxrs.common.constants.DigitalAdsActorConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalCommitOrderConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalLoggingActorConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.services.client.ClientLogger;
import com.digital.commerce.services.order.DigitalOrderImpl;

import atg.commerce.order.OrderHolder;
import atg.droplet.DropletException;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class will provide RestResources for logMessage Services.
 * 
 * @author TAIS
 *
 */
@RestResource(id = "com.digital.commerce.jaxrs.client.restresources.DigitalLoggingRestResource")
@Path("/client")
@Api("logMessage Service")
@Getter
@Setter
public class DigitalLoggingRestResource extends DigitalGenericService {

	/**
	 * commonHelper - Helper class.
	 */
	private DigitalCommonRestResourceHelper commonHelper;

	/**
	 * Service for logMessage Service
	 * 
	 * @see /rest/model/com/digital/commerce/services/v1_0/client/LoggingActor/logMessage
	 * @param pRequestBody
	 * @return Response
	 */
	@POST
	@Path("/logMessage")
	@Endpoint(id = "/client/logMessage#POST", isSingular = true, filterId = "common-response-schema")
	public Response logMessage() {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method logMessage()");
		}
		setRequestedTime();
		Map<String, Object> lHelperMap = new HashMap<>();
		Map<String, Object> lFinalResponse = new HashMap<>();

		try {

			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalLoggingActorConstants.CLIENT_LOGGER_PATH,
					DigitalLoggingActorConstants.LOGMESSAGE_METHOD);

			FormHandlerInvocationResult lResult = executor.execute();

			if (lResult.isFormError()) {
				lHelperMap = getCommonHelper().createGenericErrorResponse(lResult);
			}

		} catch (ServletException e) {
			vlogError(e, "CommerceException");
		}
		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lHelperMap);
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method logMessage()");
		}
		return Response.ok(lFinalResponse).build();

	}

}
