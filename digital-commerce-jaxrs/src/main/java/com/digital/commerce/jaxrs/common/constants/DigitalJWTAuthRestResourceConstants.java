package com.digital.commerce.jaxrs.common.constants;

/**
 * DigitalJWTAuthRestResource Constants.
 *
 * @author TAIS
 *
 */
public class DigitalJWTAuthRestResourceConstants extends DigitalJaxRsResourceCommonConstants {

	public static final String JWT_AUTH_HANDLER_COMPONENT_NAME = "/com/digital/commerce/services/v1_0/auth/jwt/JWTAuthFormHandler";

	public static final String VALIDATE_JWT_TOKEN = "validateJWTToken";

	public static final String GENERATE_JWT_TOKEN = "generateJWTToken";

	public static final String USER_NAME = "userName";

	public static final String PASSWORD = "password";

	public static final String SOURCE = "source";

	public static final String CLIENT_SECRET = "clientId";

	public static final String CLIENT_ID = "clientSecret";

	public static final String AUDIENCE = "audience";


}
