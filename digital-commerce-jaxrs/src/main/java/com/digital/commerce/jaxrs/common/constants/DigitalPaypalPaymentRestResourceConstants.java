package com.digital.commerce.jaxrs.common.constants;

/**
 * @author TAIS
 *
 */
public class DigitalPaypalPaymentRestResourceConstants extends DigitalJaxRsResourceCommonConstants {

	public static final String PAYPAL_PAYMENT_FORMHANDLER_COMPONENT_NAME = "/com/digital/commerce/services/order/payment/paypal/PaypalAuthenticationFormHandler";

	public static final String PAYPAL_AUTHENTICATION = "paypalAuthentication";

	public static final String PAYPAL_EXPRESS_CHECKOUT = "paypalExpressCheckout";

	public static final String GUEST_CHECKOUT = "guestCheckout";
	
	public static final String REPRICE_ORDER_DROPLET = "/atg/commerce/order/purchase/RepriceOrderDroplet";

	public static final Object IS_ORDER_RESP_REQUIRED_FLAG = "isOrderRespRequiredFlag";

	

	


}
