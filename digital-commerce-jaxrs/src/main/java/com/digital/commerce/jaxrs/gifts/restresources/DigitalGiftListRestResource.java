package com.digital.commerce.jaxrs.gifts.restresources;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalGiftListRestResourceConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.jaxrs.gifts.helper.DigitalGiftListRestResourceHelper;

import atg.commerce.CommerceException;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.droplet.TagConverterManager;
import atg.nucleus.annotation.Hidden;
import atg.repository.RepositoryItem;
import atg.service.jaxrs.RestException;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * DigitalGiftListRestResource - Service class for Gift List Services
 *
 * @author TAIS
 */
@RestResource(id = "com.digital.commerce.jaxrs.gifts.restresources.DigitalGiftListRestResource")
@Path("/giftLists")
@Api("Gift List Services")
@Getter
@Setter
public class DigitalGiftListRestResource extends DigitalGenericService {

	/**
	 * giftListHelper - Helper class for Gift List
	 */
	private DigitalGiftListRestResourceHelper giftListHelper;

	@Hidden
	@GET
	@Endpoint(id = "/giftLists/#GET", isSingular = true, filterId = "common-response-schema")
	public Response getGiftList() throws RestException, CommerceException {
		return Response.ok(null).build();
	}

	/**
	 * Service to remove item from gift list
	 *
	 * @see -/atg/commerce/gifts/GiftlistActor/removeItemFromWishlist
	 *
	 * @param pGiftItemId
	 *            Gift Item Id To Remove
	 * @return Response
	 */
	@DELETE
	@Path("/removeItemFromWishlist")
	@Endpoint(id = "/giftLists/removeItemFromWishlist#DELETE", isSingular = true, filterId = "common-response-schema")
	public Response removeItemFromWishList(@QueryParam("removeGiftitemIds") String pGiftItemId) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method removeItemFromWishList()");
		}
		setRequestedTime();
		Map<String, Object> lResponseMap = new HashMap<>();

		try {

			FormHandlerInvocationResult lResult = getGiftListHelper().removeItemFromGiftList(pGiftItemId);

			if (lResult.isFormError()) {
				lResponseMap.put(DigitalGiftListRestResourceConstants.RESPONSE,
						getGiftListHelper().getCommonHelper().createFormExceptionResponse(lResult.getFormExceptions()));
			} else {
				lResponseMap.put(DigitalGiftListRestResourceConstants.RESPONSE, DigitalGiftListRestResourceConstants.SUCCESS);
			}

		} catch (ServletException e) {
			vlogError(e, "ServletException");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method removeItemFromWishList()");
		}

		return Response.ok(lResponseMap).build();

	}

	/**
	 * Service for adding item in wishlist
	 *
	 * @see -/atg/commerce/gifts/GiftlistActor/dswAddItemToWishlist
	 *
	 * @param pRequestBody
	 *            Request Body
	 * @return Response
	 */
	@POST
	@Path("/dswAddItemToWishlist")
	@Endpoint(id = "/giftLists/dswAddItemToWishlist#POST", isSingular = true, filterId = "common-response-schema")
	public Response dswAddItemToWishlist(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method dswAddItemToWishlist()");
		}
		setRequestedTime();
		Object lResponseMap = null;
		Map<String, Object> lFinalMap = new HashMap<>();

		try {
			Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
			RepositoryItem lProfileWishlist = (RepositoryItem) lProfile.getPropertyValue(DigitalGiftListRestResourceConstants.WISHLIST);

			if (lProfileWishlist == null || lProfileWishlist.isTransient()) {
				lResponseMap = createWishList();
			}

			lProfileWishlist = (RepositoryItem) lProfile.getPropertyValue(DigitalGiftListRestResourceConstants.WISHLIST);
			if (lProfileWishlist != null) {
				String lItemList = pRequestBody.get("itemList").toString();
				FormHandlerExecutor executor = new FormHandlerExecutor(DigitalGiftListRestResourceConstants.GIFT_LIST_FORM_HANDLER,
						DigitalGiftListRestResourceConstants.HANDLE_ADD_ITEM_LIST);
				executor.addInput(DigitalGiftListRestResourceConstants.ITEM_LIST_STR, lItemList);
				executor.addInput(DigitalGiftListRestResourceConstants.GIFT_LIST_ID, lProfileWishlist.getRepositoryId());
				FormHandlerInvocationResult lAddItemResult = executor.execute();
				if (lAddItemResult.isFormError()) {
					lResponseMap = getGiftListHelper().getCommonHelper().createGenericErrorResponse(lAddItemResult);
				} else {
					lResponseMap = DigitalGiftListRestResourceConstants.SUCCESS;
				}
			}

			lFinalMap.put(DigitalGiftListRestResourceConstants.RESPONSE, lResponseMap);

		} catch (ServletException e) {
			vlogError(e, "ServletException");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method dswAddItemToWishlist()");
		}

		return Response.ok(lFinalMap).build();

	}

	/**
	 * Service for adding item in wishlist
	 *
	 * @see -/atg/commerce/gifts/GiftlistActor/dswAddItemToWishlist
	 *
	 * @param pRequestBody
	 *            Request Body
	 * @return Response
	 */
	@POST
	@Path("/dswMoveItemsFromCartToWishlist")
	@Endpoint(id = "/giftLists/dswMoveItemsFromCartToWishlist#POST", isSingular = true, filterId = "dsw-move-cart-to-wishlist-schema")
	public Response dswMoveItemsFromCartToWishlist(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method dswMoveItemsFromCartToWishlist()");
		}
		setRequestedTime();
		Map<String, Object> lFinalMap = new HashMap<>();

		try {
			Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
			RepositoryItem lProfileWishlist = (RepositoryItem) lProfile.getPropertyValue(DigitalGiftListRestResourceConstants.WISHLIST);

			if (lProfileWishlist == null || lProfileWishlist.isTransient()) {
				lFinalMap.put(DigitalGiftListRestResourceConstants.RESPONSE, createWishList());
			}
			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
			lProfileWishlist = (RepositoryItem) lProfile.getPropertyValue(DigitalGiftListRestResourceConstants.WISHLIST);
			if (lProfileWishlist != null) {
				lFinalMap = moveItemsFromCart(lRequestMap, lProfileWishlist.getRepositoryId());
			}

			lFinalMap.put(DigitalGiftListRestResourceConstants.ITEM_IDS, lRequestMap.get(DigitalGiftListRestResourceConstants.ITEM_IDS));

		} catch (ServletException e) {
			vlogError(e, "ServletException");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method dswMoveItemsFromCartToWishlist()");
		}

		return Response.ok(lFinalMap).build();

	}

	/**
	 * This method will invoke move items to cart handle method.
	 *
	 * @param pRequestMap
	 *            Request Body
	 * @param pGiftListId
	 *            Profile Giftlist id
	 * @return lResponseMap Response
	 * @throws ServletException
	 */
	private Map<String, Object> moveItemsFromCart(Map<String, Object> pRequestMap, String pGiftListId) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method moveItemsFromCart()");
		}
		Map<String, Object> lResponseMap = new HashMap<>();
		Map<String, Object> lResult = new HashMap<>();
		FormHandlerExecutor executor = new FormHandlerExecutor(DigitalGiftListRestResourceConstants.GIFT_LIST_FORM_HANDLER,
				DigitalGiftListRestResourceConstants.HANDLE_MOVE_ITEMS_FROM_CART);
		executor.addInput(DigitalGiftListRestResourceConstants.ITEM_IDS, pRequestMap.get("itemIds"),
				TagConverterManager.getTagConverterByName("array"), new Properties());
		executor.addInput(DigitalGiftListRestResourceConstants.GIFT_LIST_ID, pGiftListId);
		executor.addInput(DigitalGiftListRestResourceConstants.SEND_ITEM_REMOVE_MSG, false);
		executor.addInput(DigitalGiftListRestResourceConstants.QUANTITY, pRequestMap.get("quantity"));

		boolean lMoveFromPreviousCart = pRequestMap.get("moveFromPreviouseCart") == null ? false
				: (boolean) pRequestMap.get("moveFromPreviouseCart");
		executor.addInput("moveFromPreviouseCart", lMoveFromPreviousCart);

		FormHandlerInvocationResult lMoveItemResult = executor.execute();
		if (lMoveItemResult.isFormError()) {
			lResponseMap = getGiftListHelper().getCommonHelper().createGenericErrorResponse(lMoveItemResult);
			lResult.put(DigitalGiftListRestResourceConstants.RESPONSE, lResponseMap);
		} else {
			lResult.put(DigitalGiftListRestResourceConstants.RESPONSE, DigitalGiftListRestResourceConstants.SUCCESS);
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method moveItemsFromCart()");
		}
		return lResult;
	}

	/**
	 * This method will create wishlist for current profile
	 *
	 * @return
	 * @throws ServletException
	 */
	private Map<String, Object> createWishList() throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method createWishList()");
		}
		FormHandlerExecutor executor = new FormHandlerExecutor(DigitalGiftListRestResourceConstants.GIFT_LIST_FORM_HANDLER,
				DigitalGiftListRestResourceConstants.HANDLE_SAVE_GIFT_LIST);
		executor.addInput(DigitalGiftListRestResourceConstants.WISHLIST, true);
		FormHandlerInvocationResult lCreateResult = executor.execute();
		if (lCreateResult.isFormError()) {
			return getGiftListHelper().getCommonHelper().createGenericErrorResponse(lCreateResult);
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method createWishList()");
		}
		return null;
	}

	/**
	 * Service for sharing the wishlist
	 *
	 * @see -/atg/commerce/gifts/GiftlistActor/dswShareWishlist
	 *
	 * @param pRequestBody
	 *            Request Body
	 * @return lFinalMap
	 */
	@POST
	@Path("/dswShareWishlist")
	@Endpoint(id = "/giftLists/dswShareWishlist#POST", isSingular = true, filterId = "common-response-schema")
	public Response dswShareWishlist(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method dswShareWishlist()");
		}
		setRequestedTime();
		Map<String, Object> lFinalMap = new HashMap<>();
		Map<String, Object> lResponseMap = new HashMap<>();
		try {
			Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
			RepositoryItem lProfileWishlist = (RepositoryItem) lProfile.getPropertyValue(DigitalGiftListRestResourceConstants.WISHLIST);

			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalGiftListRestResourceConstants.SHARE_GIFT_LIST_FORM_HANDLER,
					DigitalGiftListRestResourceConstants.HANDLE_SHARE_WISHLIST);

			executor.addInput(DigitalGiftListRestResourceConstants.EMAILS, lRequestMap.get(DigitalGiftListRestResourceConstants.EMAILS),
					TagConverterManager.getTagConverterByName("array"), new Properties());
			executor.addInput(DigitalGiftListRestResourceConstants.INCLUDE_OWNER,
					lRequestMap.get(DigitalGiftListRestResourceConstants.INCLUDE_OWNER));
			executor.addInput(DigitalGiftListRestResourceConstants.SHARED_MESSAGE,
					lRequestMap.get(DigitalGiftListRestResourceConstants.SHARED_MESSAGE));
			executor.addInput(DigitalGiftListRestResourceConstants.GIFT_LIST_ID, lProfileWishlist.getRepositoryId());
			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lResponseMap = getGiftListHelper().getCommonHelper().createGenericErrorResponse(lResult);
			}
		} catch (ServletException e) {
			vlogError(e, "ServletException");
		}

		lFinalMap.put(DigitalGiftListRestResourceConstants.RESPONSE, lResponseMap);

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method dswShareWishlist()");
		}

		return Response.ok(lFinalMap).build();

	}
}
