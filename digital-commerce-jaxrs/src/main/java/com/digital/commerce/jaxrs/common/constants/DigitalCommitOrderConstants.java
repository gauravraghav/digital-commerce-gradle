package com.digital.commerce.jaxrs.common.constants;

/**
 * Constant for Commit Order Service
 * 
 * @author TAIS
 *
 */
public class DigitalCommitOrderConstants extends DigitalJaxRsResourceCommonConstants {
	
	public static final String COMMIT_ORDER_FORM_HANDLER_PATH = "/atg/commerce/order/purchase/CommitOrderFormHandler";
	
	public static final String HANDLE_COMMIT_ORDER = "commitOrder";
	
	public static final String CREDIT_CRD_TYPE = "creditCardType";
	
	public static final String NAME_ON_CARD = "nameOnCard";
	
	public static final String EXPIRATION_YEAR = "expirationYear";
	
	public static final String EXPIRATION_MONTH = "expirationMonth";
	
	public static final String NEW_CREDIT_CRD_PAYMENT = "newCreditCardPayment";
	
	public static final String VANTIV_BIN = "vantivBin";
	
	public static final String VANTIV_PAY_PAGE_REGISTRATION_ID = "vantivPaypageRegistrationId";
	
	public static final String VANTIV_CODE = "vantivCode";
	
	public static final String VANTIV_MESSAGE = "vantivMessage";
	
	public static final String VANTIV_TIME = "vantivTime";
	
	public static final String VANTIV_TYPE = "vantivType";
	
	public static final String VANTIV_LITLE_TXN_ID = "vantivLitleTxnId";
	
	public static final String VANTIV_FIRST_SIX = "vantivFirstSix";
	
	public static final String VANTIV_LAST_FOUR = "vantivLastFour";
	
	public static final String CREDIT_CARD_KEY = "creditCardKey";
	
	public static final String COPY_CREDIT_CARD_TO_PROFILE = "copyCreditCardToProfile";
	
	public static final String PAYMENT_TYPE = "paymentType";
	
	public static final String MAKE_AS_PRIMARY = "makeAsPrimary";
	
	public static final String BILLING_SAME_AS_SHIPPING_ADDRESS = "billingSameAsShippingAddress";
	
	public static final String CVV_NUMBER = "cvvNumber";
	
	public static final String CVV = "cvv";
	
	public static final String EXCHANGE = "exchange";
	
	public static final String IS_EXCHANGE = "isExchange";
	
	public static final String PREV_ORD_EMAIL = "prevOrder.email";

    

}
