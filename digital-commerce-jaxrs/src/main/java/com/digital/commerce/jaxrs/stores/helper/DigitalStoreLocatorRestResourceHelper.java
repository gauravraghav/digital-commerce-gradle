package com.digital.commerce.jaxrs.stores.helper;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.digital.commerce.jaxrs.common.constants.DigitalStoreLocatorRestResourceConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.services.storelocator.DigitalStoreLocatorFormHandler;
import com.digital.commerce.services.storelocator.DigitalStoreLocatorModel;

import atg.commerce.inventory.InventoryInfo;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.GenericService;
import atg.service.jaxrs.DropletInvoker;
import atg.service.jaxrs.OParam;
import atg.service.jaxrs.Output;
import lombok.Getter;
import lombok.Setter;

/**
 * This class will provide DigitalStoreLocator Actor responses and helper for rest
 * resources. 
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalStoreLocatorRestResourceHelper extends GenericService{
	
	/**
	 * mCommonHelper - Common Helper
	 */
	private DigitalCommonRestResourceHelper commonHelper;
	
	/**
	 * Populate Store Locator results from Formhandlers .
	 * @param result
	 * @return
	 */
	public Map<String, Object> populateStoreLocatorResponse(FormHandlerInvocationResult result) {
		vlogDebug("Start -> {0} Method populateStoreLocatorResponse. result {1}", getClass().getCanonicalName(),result);
		Map<String, Object> lFinalResponseMap = new HashMap<>();
		Map<String, DigitalStoreLocatorModel> lStoreLocatorModelMap = new HashMap<>();
		if (result != null) {
			DigitalStoreLocatorFormHandler storeLocatorFormHandler = (DigitalStoreLocatorFormHandler)result.getFormHandler();
			
			if (storeLocatorFormHandler != null && !storeLocatorFormHandler.getStoreLocatorResults().isEmpty()) {
				
				lFinalResponseMap.put(DigitalStoreLocatorRestResourceConstants.RESULTS_ARE_FROM_SEARC,
						storeLocatorFormHandler.isResultsFromSearch());
				lFinalResponseMap.put(DigitalStoreLocatorRestResourceConstants.TOTAL_COUNT, storeLocatorFormHandler.getResultSetSize());
				List<DigitalStoreLocatorModel> storeLocatorList = (List<DigitalStoreLocatorModel>) storeLocatorFormHandler.getStoreLocatorResults();
				
				for(DigitalStoreLocatorModel storeLocatorModel:storeLocatorList){
					lStoreLocatorModelMap.put(storeLocatorModel.getRepositoryId(),storeLocatorModel);
				}
				lFinalResponseMap.put(DigitalStoreLocatorRestResourceConstants.STORE_LOCATOR_RESULTS_MAP, lStoreLocatorModelMap);
				
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateStoreLocatorResponse");
		}
		return lFinalResponseMap;
	}
	
	/**
	 * Method to fetch the store inventory response from DigitalPDPInventoryLookup droplet and return the inventory details to
     * DigitalStoreLocatorRestResource
     * 
	 * @param pStoreLocatorResultsMap
	 * @param pSKUID
	 * @param pFilterOutOfStock
	 */
	public void storeLocatorInventoryValidation(
			Map<String, DigitalStoreLocatorModel> pStoreLocatorResultsMap, String pSKUID, String pFilterOutOfStock) {

		if (isLoggingDebug()) {
		    logDebug("Entering into method DigitalStoreLocatorRestResourceHelper.storeLocatorInventoryValidation Map: "
			    + pStoreLocatorResultsMap);
		}
	
		for(DigitalStoreLocatorModel storeLocatorModel : pStoreLocatorResultsMap.values()){
			Map<String, Object> inventoryResponse = invokeInventoryDroplet(storeLocatorModel.getLocationId(),pSKUID,pFilterOutOfStock);
			if (inventoryResponse != null && inventoryResponse.containsKey(DigitalStoreLocatorRestResourceConstants.INVENTORY_DROPLET_PARAM)) {
				Map inventoryInfosMap = (HashMap)inventoryResponse.get(DigitalStoreLocatorRestResourceConstants.INVENTORY_DROPLET_PARAM);
				if(inventoryInfosMap != null && inventoryInfosMap.containsKey(DigitalStoreLocatorRestResourceConstants.STORE_LEVEL_INVENTORY)){
					Collection<InventoryInfo> inventoryInfosCollection = (Collection<InventoryInfo>)inventoryInfosMap.get(DigitalStoreLocatorRestResourceConstants.STORE_LEVEL_INVENTORY);
					if(inventoryInfosCollection != null && !inventoryInfosCollection.isEmpty()){
						Iterator<InventoryInfo> inventoryInfos = inventoryInfosCollection.iterator();
						
						if(inventoryInfos.hasNext()){
							InventoryInfo inventoryInfo = inventoryInfos.next();
							
							if(inventoryInfo.getStockLevel() > 0){
								storeLocatorModel.setStockLevel(inventoryInfo.getStockLevel());
							}
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
    	    logDebug("Exiting method DigitalStoreLocatorRestResourceHelper.storeLocatorInventoryValidation : pSKUID : " + pSKUID);
    	}
    }
    
    /**
     * Invoking Inventory Droplet and getting InventoryInfos Map.
     * @param pSkuId
     * @param pLocationId
     * @param pFilterOutOfStock
     * @return
     */
	private Map<String, Object> invokeInventoryDroplet(String pLocationId, String pSkuId, String pFilterOutOfStock){
    	
    	Map<String, Object> response = new HashMap<String, Object>();
    	
    	DropletInvoker dropletInvoker = new DropletInvoker(DigitalStoreLocatorRestResourceConstants.DSW_INVENTORY_LOOKUP_DROPLET);

    	dropletInvoker.addInput(DigitalStoreLocatorRestResourceConstants.LOCATION_ID, pLocationId);
    	dropletInvoker.addInput(DigitalStoreLocatorRestResourceConstants.SKU_ID, pSkuId);
    	dropletInvoker.addInput(DigitalStoreLocatorRestResourceConstants.FILTER_OUT_OF_STOCK, pFilterOutOfStock);

    	OParam oParamOutput = dropletInvoker.addOParam(DigitalStoreLocatorRestResourceConstants.OUTPUT);
    	Output inventoryInfo = oParamOutput.addOutput(DigitalStoreLocatorRestResourceConstants.INVENTORY_INFO, DigitalStoreLocatorRestResourceConstants.INVENTORY_INFO);

    	dropletInvoker.invoke();
    	
    	if(inventoryInfo.getObject() != null){
    		response.put(DigitalStoreLocatorRestResourceConstants.INVENTORY_DROPLET_PARAM, inventoryInfo.getObject());
    	}

    	if (isLoggingDebug()) {
    	    logDebug("Exiting method DigitalStoreLocatorRestResourceHelper.invokeInventoryDroplet : Response returned : " + response);
    	}

    	return response;
    	
    }
	/**
	 * This method will do a sort a Map by using distance attribute.
	 * @param data
	 * @param attribute
	 * @return
	 */
	public Map sortMapData(Map<String,DigitalStoreLocatorModel> data, String attribute) {
		Map result = new HashMap();
		if(data != null){
			Map<String,DigitalStoreLocatorModel> sortedMap = new TreeMap(new StoreLocatorComparator(data, attribute));
			sortedMap.putAll(data);
			LinkedHashMap<String, DigitalStoreLocatorModel> lLinkedHashMap = new LinkedHashMap<String, DigitalStoreLocatorModel>();
			lLinkedHashMap.putAll(sortedMap);
			result.put(DigitalStoreLocatorRestResourceConstants.SORTED_LIST, lLinkedHashMap);
			if(isLoggingDebug()){
				vlogDebug("print sorted lLinkedHashMap >>>>>>>>>>>>>>>>>>>>>>>> :{0}", lLinkedHashMap);
			}
			
		}
		
        return result;
	}
	/**
	 * 
	 * @author TAIS
	 *
	 */
	class StoreLocatorComparator implements Comparator<String> {
        String attribute;
        Map map;

        /**
         * @param map
         * @param attribute
         */
        public StoreLocatorComparator(Map map, String attribute) {
            this.attribute = attribute;
            this.map = map;
        }

        /**
         * @param id1
         * @param id2
         * @return int
         */
        public int compare(String id1, String id2) {
        	int results = 0;
            if (this.map.get(id1) instanceof DigitalStoreLocatorModel
                    && this.map.get(id2) instanceof DigitalStoreLocatorModel) {
            	
            	DigitalStoreLocatorModel v1 = (DigitalStoreLocatorModel) this.map.get(id1);
            	DigitalStoreLocatorModel v2 = (DigitalStoreLocatorModel) this.map.get(id2);
            	
            	if(this.attribute.equals(DigitalStoreLocatorRestResourceConstants.DISTANCE)){
            		results = v1.getDistance().compareTo(v2.getDistance());
               	}
            	
            }
            return results;
        }
    }
}
