package com.digital.commerce.jaxrs.purchase.helper;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.jaxrs.common.constants.DigitalCommitOrderConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalOrderLookupService;
import com.digital.commerce.services.order.purchase.DigitalCommitOrderFormHandler;
import com.digital.commerce.services.utils.DigitalServiceConstants;

import atg.commerce.order.OrderHolder;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.GenericService;
/**
 * Helper class for Commit Order Service.
 * 
 * @author TAIS
 *
 */
public class DigitalCommitOrderRestResourceHelper extends GenericService {
	
	/**
	 * mCommonHelper - Common Helper
	 */
	private DigitalCommonRestResourceHelper mCommonHelper;
	
	/**
	 * mOrderLookupService - Order Service.
	 */
	private DigitalOrderLookupService mOrderLookupService;
	
	
	/**
	 * Helper method to add form input for commit order service
	 * 
	 * @param pExecutor
	 * @param pRequestMap
	 * @throws ServletException
	 */
	public void addInputForCommitOrder(FormHandlerExecutor pExecutor, Map<String, Object> pRequestMap) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method addInputForCommitOrder()");
		}

		pExecutor.addInput(DigitalCommitOrderConstants.CREDIT_CRD_TYPE, pRequestMap.get(DigitalCommitOrderConstants.CREDIT_CRD_TYPE));
		pExecutor.addInput(DigitalCommitOrderConstants.NAME_ON_CARD, pRequestMap.get(DigitalCommitOrderConstants.NAME_ON_CARD));
		pExecutor.addInput(DigitalCommitOrderConstants.EXPIRATION_YEAR, pRequestMap.get(DigitalCommitOrderConstants.EXPIRATION_YEAR));
		pExecutor.addInput(DigitalCommitOrderConstants.EXPIRATION_MONTH, pRequestMap.get(DigitalCommitOrderConstants.EXPIRATION_MONTH));
		pExecutor.addInput(DigitalCommitOrderConstants.NEW_CREDIT_CRD_PAYMENT, pRequestMap.get(DigitalCommitOrderConstants.NEW_CREDIT_CRD_PAYMENT));
		pExecutor.addInput(DigitalCommitOrderConstants.VANTIV_BIN, pRequestMap.get(DigitalCommitOrderConstants.VANTIV_BIN));
		pExecutor.addInput(DigitalCommitOrderConstants.VANTIV_PAY_PAGE_REGISTRATION_ID, pRequestMap.get(DigitalCommitOrderConstants.VANTIV_PAY_PAGE_REGISTRATION_ID));
		pExecutor.addInput(DigitalCommitOrderConstants.VANTIV_CODE, pRequestMap.get(DigitalCommitOrderConstants.VANTIV_CODE));
		pExecutor.addInput(DigitalCommitOrderConstants.VANTIV_MESSAGE, pRequestMap.get(DigitalCommitOrderConstants.VANTIV_MESSAGE));
		pExecutor.addInput(DigitalCommitOrderConstants.VANTIV_TIME, pRequestMap.get(DigitalCommitOrderConstants.VANTIV_TIME));
		pExecutor.addInput(DigitalCommitOrderConstants.VANTIV_TYPE, pRequestMap.get(DigitalCommitOrderConstants.VANTIV_TYPE));
		pExecutor.addInput(DigitalCommitOrderConstants.VANTIV_LITLE_TXN_ID, pRequestMap.get(DigitalCommitOrderConstants.VANTIV_LITLE_TXN_ID));
		pExecutor.addInput(DigitalCommitOrderConstants.VANTIV_FIRST_SIX, pRequestMap.get(DigitalCommitOrderConstants.VANTIV_FIRST_SIX));
		pExecutor.addInput(DigitalCommitOrderConstants.VANTIV_LAST_FOUR, pRequestMap.get(DigitalCommitOrderConstants.VANTIV_LAST_FOUR));
		pExecutor.addInput(DigitalCommitOrderConstants.CREDIT_CARD_KEY, pRequestMap.get(DigitalCommitOrderConstants.CREDIT_CARD_KEY));
		pExecutor.addInput(DigitalCommitOrderConstants.COPY_CREDIT_CARD_TO_PROFILE, pRequestMap.get(DigitalCommitOrderConstants.COPY_CREDIT_CARD_TO_PROFILE));
		pExecutor.addInput(DigitalCommitOrderConstants.PAYMENT_TYPE, pRequestMap.get(DigitalCommitOrderConstants.PAYMENT_TYPE));
		pExecutor.addInput(DigitalCommitOrderConstants.MAKE_AS_PRIMARY, pRequestMap.get(DigitalCommitOrderConstants.MAKE_AS_PRIMARY));
		pExecutor.addInput(DigitalCommitOrderConstants.BILLING_SAME_AS_SHIPPING_ADDRESS, pRequestMap.get(DigitalCommitOrderConstants.BILLING_SAME_AS_SHIPPING_ADDRESS));
		pExecutor.addInput(DigitalCommitOrderConstants.CVV_NUMBER, pRequestMap.get(DigitalCommitOrderConstants.CVV));
		pExecutor.addInput(DigitalCommitOrderConstants.EXCHANGE, pRequestMap.get(DigitalCommitOrderConstants.IS_EXCHANGE));
		
		
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method addInputForCommitOrder()");
		}

	}
	/**
	 *  This Method Populate Commit Order Response.
	 * @param pResult
	 * @return Map
	 */
	public Map<String,Object> populateCommitOrderResponse(FormHandlerInvocationResult pResult){
		
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateCommitOrderSuccessDetails");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		
		if(pResult != null){
			DigitalCommitOrderFormHandler commitOrdFormHandler = (DigitalCommitOrderFormHandler) pResult.getFormHandler();
			
			DigitalOrderImpl lOrder = (DigitalOrderImpl)commitOrdFormHandler.getOrder();
			lResponseMap.put(DigitalCommitOrderConstants.ORDER, lOrder);
			
			OrderHolder lshoppingCard = (OrderHolder)ComponentLookupUtil.lookupComponent(ComponentLookupUtil.SHOPPING_CART);
			DigitalOrderImpl lLastOrder = (DigitalOrderImpl) lshoppingCard.getLast();
			if(getCommonHelper().getDswConstants().isPlaceOrderBeanFilterEnabled()){
				String lEmail = getOrderLookupService().lookupEmailFromOrder(lLastOrder);
				lResponseMap.put(DigitalCommitOrderConstants.LAST_ORDER, lLastOrder);
				lResponseMap.put(DigitalCommitOrderConstants.PREV_ORD_EMAIL, lEmail);
				
			}else{
				lResponseMap.put(DigitalCommitOrderConstants.LAST_ORDER, lLastOrder);
			}
		}
		
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateCommitOrderSuccessDetails");
		}
		return lResponseMap;
	}

	/**
	 * @return mCommonHelper
	 */
	public DigitalCommonRestResourceHelper getCommonHelper() {
		return mCommonHelper;
	}

	/**
	 * @param mCommonHelper
	 */
	public void setCommonHelper(DigitalCommonRestResourceHelper mCommonHelper) {
		this.mCommonHelper = mCommonHelper;
	}

	/**
	 * @return mOrderLookupService
	 */
	public DigitalOrderLookupService getOrderLookupService() {
		return mOrderLookupService;
	}

	/**
	 * @param mOrderLookupService
	 */
	public void setOrderLookupService(DigitalOrderLookupService mOrderLookupService) {
		this.mOrderLookupService = mOrderLookupService;
	}
	

}