package com.digital.commerce.jaxrs.order.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalOrderLookupRestResourceConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.jaxrs.order.helper.DigitalOrderAltPickupRestResourceHelper;
import com.digital.commerce.jaxrs.order.helper.DigitalOrderLookupRestResourceHelper;

import atg.droplet.DropletException;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class provides rest resources  for OrderAltPickupActor.
 * 
 * @author TAIS
 *
 */

@RestResource(id = "com.digital.commerce.jaxrs.order.restresources.DigitalOrderAltPickupRestResource")
@Path("/orderPickup")
@Api("orderPickup Service")
@Getter
@Setter
public class DigitalOrderAltPickupRestResource extends DigitalGenericService {
	/**
	 * orderPickupHelper - DigitalOrderAltPickupRestResourceHelper Component
	 */
	private DigitalOrderAltPickupRestResourceHelper orderPickupHelper;

	/**
	 * Service to getOrderInfo.
	 * 
	 * @see - Actor Chain - /com/digital/commerce/services/v1_0/order/purchase/OrderAltPickupActor/getOrderInfo
	 * 
	 * @return Response
	 */
	@GET
	@Path("/getorderPickupInfo")
	@Endpoint(id = "/orderPickup/getorderPickupInfo#GET", isSingular = true, filterId = "dsw-constants-response")
	public Response getorderPickupInfo() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getorderPickupInfo");
		}

		Map<String, Object> lResponse = new HashMap<String, Object>();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getorderPickupInfo");
		}
		return Response.ok(lResponse).build();
	}


	/**
	 * Service for add alt pickup person to order.
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/order/purchase/OrderAltPickupActor/addAltPickupPerson
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/addAltPickupPerson")
	@Endpoint(id = "/orderPickup/addAltPickupPerson#POST", isSingular = true, filterId = "common-response-schema")
	public Response addAltPickupPerson(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addAltPickupPerson");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Map<String, Object> lResultMap = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequestBody);
		if (isLoggingDebug()) {
			logDebug("Request for addAltPickupPerson " + lRequestPayload);
		}
		try {
			lResultMap = getOrderPickupHelper().addAltPickupPersonDetails(lRequestPayload);
			lFinalResponse.put(DigitalOrderLookupRestResourceConstants.RESPONSE, lResultMap);
		} catch (Exception e) {
			vlogError(e, "JSONException | ServletException");

		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addAltPickupPerson");
		}
		return Response.ok(lFinalResponse).build();
	}
	/**
	 * Service to remove alt pickup person to order.
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/order/purchase/OrderAltPickupActor/removeAltPickupPerson
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@DELETE
	@Path("/removeAltPickupPerson")
	@Endpoint(id = "/orderPickup/removeAltPickupPerson#DELETE", isSingular = true, filterId = "common-response-schema")
	public Response removeAltPickupPerson() {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method removeAltPickupPerson");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Map<String, Object> lResultMap = new HashMap<String, Object>();
		try {
			lResultMap = getOrderPickupHelper().removeAltPickupPersonDetails();
			lFinalResponse.put(DigitalOrderLookupRestResourceConstants.RESPONSE, lResultMap);
		} catch (Exception e) {
			vlogError(e, "JSONException | ServletException");

		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method removeAltPickupPerson");
		}
		return Response.ok(lFinalResponse).build();
	}
}
