package com.digital.commerce.jaxrs.order.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.jaxrs.common.constants.DigitalOrderLookupRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalProfileRestResourceContants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.services.profile.rewards.LoyaltyCertificate;
import com.digital.commerce.services.promotion.AppliedOffer;
import com.digital.commerce.services.promotion.AppliedOffer.AppliedPromotion;
import com.digital.commerce.services.promotion.DigitalCouponFormHandler;
import com.digital.commerce.services.utils.DigitalServiceConstants;

import atg.nucleus.GenericService;
import atg.service.jaxrs.DropletInvoker;
import atg.service.jaxrs.OParam;
import atg.service.jaxrs.Output;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

/**
 * This class provides orderLookupActor's chain ids implementations.
 * 
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalOrderLookupRestResourceHelper extends GenericService {
	
	
	/**
	 * mCommonHelper - Common Helper
	 */
	private DigitalCommonRestResourceHelper commonHelper;
	
	private boolean enableSecurity;
	
	private DigitalServiceConstants dswConstants;
	

	/**
	 * Provide order history in order dashboard
	 * 
	 * @return lResponseMap
	 * 
	 */
	public Map<String, Object> populateOrderDashboard() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateOrderDashboard");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();

		DropletInvoker dropletInvoker = new DropletInvoker(DigitalOrderLookupRestResourceConstants.ORDER_HISTORY_DROPLET);

		dropletInvoker.addInput(DigitalProfileRestResourceContants.USER_ID, lProfile.getRepositoryId());
		dropletInvoker.addInput(DigitalProfileRestResourceContants.SOURCE_ORIGINATED, DigitalOrderLookupRestResourceConstants.DASHBOARD);
		dropletInvoker.addInput(DigitalProfileRestResourceContants.CHAIN, DigitalOrderLookupRestResourceConstants.HISTORY);

		OParam oParamOutput = dropletInvoker.addOParam(DigitalOrderLookupRestResourceConstants.OUTPUT);
		Output lSuccessResultOutput = oParamOutput.addOutput(DigitalOrderLookupRestResourceConstants.RESULT_SMALL,
				DigitalOrderLookupRestResourceConstants.RESULT_SMALL);
		Output lSuccessCountOutput = oParamOutput.addOutput(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT,
				DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT);

		OParam oErrorParam = dropletInvoker.addOParam(DigitalOrderLookupRestResourceConstants.ERROR_SMALL);
		Output lErrorResultOutput = oErrorParam.addOutput(DigitalOrderLookupRestResourceConstants.RESULT_SMALL,
				DigitalOrderLookupRestResourceConstants.RESULT_SMALL);
		Output lErrorCountOutput = oErrorParam.addOutput(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT,
				DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT);

		dropletInvoker.invoke();

		if (lSuccessResultOutput.getObject() != null) {
			lResponseMap.put(DigitalOrderLookupRestResourceConstants.MY_ORDERS, lSuccessResultOutput.getObject());
			lResponseMap.put(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT, lSuccessCountOutput.getObject());
		} else {
			lResponseMap.put(DigitalOrderLookupRestResourceConstants.ERROR, lErrorResultOutput.getObject());
			lResponseMap.put(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT, lErrorCountOutput.getObject());
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateOrderDashboard");
		}
		return lResponseMap;
	}
	
	/**
	 * This Method invoke DigitalOrderHistory Droplet and get Order History Details.
	 * @param pRequestMap
	 * @return lResponseMap
	 */
	public Map<String,Object> populateOrderHistoryDetails(Map<String,Object> pRequestMap){
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateOrderHistoryDetails");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		if(pRequestMap != null && !pRequestMap.isEmpty()){
			
			DropletInvoker dropletInvoker = new DropletInvoker(DigitalOrderLookupRestResourceConstants.ORDER_HISTORY_DROPLET);
			Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.USER_ID, lProfile.getRepositoryId());
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.SORT_BY, pRequestMap.get(DigitalOrderLookupRestResourceConstants.SUBMITTED_DATE));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.SOURCE_ORIGINATE, pRequestMap.get(DigitalOrderLookupRestResourceConstants.SOURCE));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.NUM_OF_ORDERS, pRequestMap.get(DigitalOrderLookupRestResourceConstants.HOW_MANY));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.START_DATE, pRequestMap.get(DigitalOrderLookupRestResourceConstants.START_DATE));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.END_DATE, pRequestMap.get(DigitalOrderLookupRestResourceConstants.END_DATE));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.MONTHS, pRequestMap.get(DigitalOrderLookupRestResourceConstants.MONTHS));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.START_INDEX, pRequestMap.get(DigitalOrderLookupRestResourceConstants.START_INDEX));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.END_INDEX, pRequestMap.get(DigitalOrderLookupRestResourceConstants.END_INDEX));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.CHAIN,DigitalOrderLookupRestResourceConstants.HISTORY);
			
			OParam oParamOutput = dropletInvoker.addOParam(DigitalOrderLookupRestResourceConstants.OUTPUT);
			Output successResults = oParamOutput.addOutput(DigitalOrderLookupRestResourceConstants.RESULT_SMALL,DigitalOrderLookupRestResourceConstants.RESULT_SMALL);
			Output successTotalOrderCount = oParamOutput.addOutput(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT,DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT);
			
			OParam oParamError = dropletInvoker.addOParam(DigitalOrderLookupRestResourceConstants.ERROR_SMALL);
			Output errorResults = oParamError.addOutput(DigitalOrderLookupRestResourceConstants.RESULT_SMALL,DigitalOrderLookupRestResourceConstants.RESULT_SMALL);
			Output errorTotalOrderCount = oParamError.addOutput(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT,DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT);
			
			dropletInvoker.invoke();
			
			if(successResults.getObject() != null){
				lResponseMap.put(DigitalOrderLookupRestResourceConstants.MY_ORDERS, successResults.getObject());
				lResponseMap.put(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT, successTotalOrderCount.getObject());
			}else if(errorResults.getObject() != null){
				lResponseMap.put(DigitalOrderLookupRestResourceConstants.ERROR, errorResults.getObject());
				lResponseMap.put(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT, errorTotalOrderCount.getObject());
			}
			
		}else{
			lResponseMap.put(DigitalOrderLookupRestResourceConstants.ERROR_SMALL, getCommonHelper().SERVICE_RESOURCE_BUNDLE.getString(DigitalOrderLookupRestResourceConstants.INVALID_DATA_PASSED));
		}
		
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateOrderHistoryDetails");
		}
		return lResponseMap;
	}
	
	/**
	 * This Method invoke DigitalOrderHistory Droplet and get Order History Details.
	 * @param pRequestMap
	 * @return lResponseMap
	 */
	public Map<String,Object> populateDSWOrderLookupDetails(Map<String,Object> pRequestMap){
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateDSWOrderLookupDetails");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		if(pRequestMap != null && !pRequestMap.isEmpty()){
			
			DropletInvoker dropletInvoker = new DropletInvoker(DigitalOrderLookupRestResourceConstants.ORDER_HISTORY_DROPLET);
			Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.USER_ID, lProfile.getRepositoryId());
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.ORDER_ID, pRequestMap.get(DigitalOrderLookupRestResourceConstants.ORDER_ID));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.EMAIL, pRequestMap.get(DigitalOrderLookupRestResourceConstants.EMAIL));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.SORT_BY, DigitalOrderLookupRestResourceConstants.SUBMITTED_DATE);
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.SOURCE_ORIGINATE, pRequestMap.get(DigitalOrderLookupRestResourceConstants.SOURCE));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.NUM_OF_ORDERS, pRequestMap.get(DigitalOrderLookupRestResourceConstants.HOW_MANY));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.START_DATE, pRequestMap.get(DigitalOrderLookupRestResourceConstants.START_DATE));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.END_DATE, pRequestMap.get(DigitalOrderLookupRestResourceConstants.END_DATE));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.MONTHS, pRequestMap.get(DigitalOrderLookupRestResourceConstants.MONTHS));
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.CHAIN,DigitalOrderLookupRestResourceConstants.STATUS);
			
			OParam oParamOutput = dropletInvoker.addOParam(DigitalOrderLookupRestResourceConstants.OUTPUT);
			Output successResults = oParamOutput.addOutput(DigitalOrderLookupRestResourceConstants.RESULT_SMALL,DigitalOrderLookupRestResourceConstants.RESULT_SMALL);
			Output successTotalOrderCount = oParamOutput.addOutput(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT,DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT);
			
			OParam oParamError = dropletInvoker.addOParam(DigitalOrderLookupRestResourceConstants.ERROR_SMALL);
			Output errorResults = oParamError.addOutput(DigitalOrderLookupRestResourceConstants.RESULT_SMALL,DigitalOrderLookupRestResourceConstants.RESULT_SMALL);
			Output errorTotalOrderCount = oParamError.addOutput(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT,DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT);
			
			dropletInvoker.invoke();
			
			if(successResults.getObject() != null){
				lResponseMap.put(DigitalOrderLookupRestResourceConstants.MY_ORDERS, successResults.getObject());
				lResponseMap.put(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT, successTotalOrderCount.getObject());
			}else if(errorResults.getObject() != null){
				lResponseMap.put(DigitalOrderLookupRestResourceConstants.ERROR, errorResults.getObject());
				lResponseMap.put(DigitalOrderLookupRestResourceConstants.TOTAL_ORDER_COUNT, errorTotalOrderCount.getObject());
			}
			
		}else{
			lResponseMap.put(DigitalOrderLookupRestResourceConstants.ERROR_SMALL, getCommonHelper().SERVICE_RESOURCE_BUNDLE.getString(DigitalOrderLookupRestResourceConstants.INVALID_DATA_PASSED));
		}
		
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateDSWOrderLookupDetails");
		}
		return lResponseMap;
	}
	
	/**
	 * This Method invoke DigitalOrderLookup Droplet and get Order Details.
	 * @param pRequestMap
	 * @return lResponseMap
	 */
	public Map<String,Object> populateOrderLookupDetail(String pOrderId,String pEmail,String pRepriceOrderFlag){
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateOrderLookupDetail");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		lResponseMap = invokeOrderLookup(pOrderId,pEmail,pRepriceOrderFlag);
		
		if(!isEnableSecurity()){
			
			Map<String, Object> couponResultMap =  fetchAppliedOffersAndCertificates(pOrderId);
			lResponseMap.putAll(couponResultMap);
			
			Map<String,Object> rtnMap = getCommonHelper().getProfileTools().lookupUserByLogin(pEmail);
			
			rtnMap.put(DigitalOrderLookupRestResourceConstants.EXISTING_USER, rtnMap.get(DigitalOrderLookupRestResourceConstants.EXISTING_USER));
			rtnMap.put(DigitalOrderLookupRestResourceConstants.ATG_PROFILE_ID, rtnMap.get(DigitalOrderLookupRestResourceConstants.USERID));
			rtnMap.put(DigitalOrderLookupRestResourceConstants.LOYALTY_NUMBER, rtnMap.get(DigitalOrderLookupRestResourceConstants.LOYALTY_NUMBER));
			
			if(rtnMap.get(DigitalOrderLookupRestResourceConstants.EXISTING_USER) != null && ((Boolean)rtnMap.get(DigitalOrderLookupRestResourceConstants.EXISTING_USER)) == true){
				lResponseMap.put(DigitalOrderLookupRestResourceConstants.NEW_USER_ELIGIBILITY, false);
			}else{
				lResponseMap.put(DigitalOrderLookupRestResourceConstants.NEW_USER_ELIGIBILITY, true);
			}
			
			lResponseMap.putAll(rtnMap);
		}
		
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateOrderLookupDetail");
		}
		return lResponseMap;
	}
	
	/**
	 * This method invoke order Lookup droplet and provide DigitalDetailedOrder results
	 * @param pOrderId
	 * @param pEmail
	 * @param pRepriceOrderFlag
	 * @return
	 */
	private Map<String,Object> invokeOrderLookup(String pOrderId,String pEmail,String pRepriceOrderFlag){
		

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method invokeOrderLookup");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
			
		DropletInvoker dropletInvoker = new DropletInvoker(DigitalOrderLookupRestResourceConstants.ORDER_LOOKUP_DROPLET);
		
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		
		dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.ORDER_ID, pOrderId);
		dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.EMAIL, pEmail);
		dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.REPRICE_ORDER_FLAG, pRepriceOrderFlag);
		if(isEnableSecurity()){
			dropletInvoker.addInput(DigitalOrderLookupRestResourceConstants.USER_ID, lProfile);
		}
		
		OParam oParamOutput = dropletInvoker.addOParam(DigitalOrderLookupRestResourceConstants.OUTPUT);
		Output successResults = oParamOutput.addOutput(DigitalOrderLookupRestResourceConstants.RESULT_SMALL,DigitalOrderLookupRestResourceConstants.RESULT_SMALL);
		Output orderSummary = oParamOutput.addOutput(DigitalOrderLookupRestResourceConstants.ORDER_SUMMARY,DigitalOrderLookupRestResourceConstants.ORDER_SUMMARY);
		Output warningMessages = oParamOutput.addOutput(DigitalOrderLookupRestResourceConstants.WARNING_MESSAGES,DigitalOrderLookupRestResourceConstants.WARNING_MESSAGES);
		
		
		OParam oParamError = dropletInvoker.addOParam(DigitalOrderLookupRestResourceConstants.ERROR_SMALL);
		Output errorResults = oParamError.addOutput(DigitalOrderLookupRestResourceConstants.RESULT_SMALL,DigitalOrderLookupRestResourceConstants.RESULT_SMALL);
		
		dropletInvoker.invoke();
		
		if(successResults.getObject() != null){
			lResponseMap.put(DigitalOrderLookupRestResourceConstants.RESULT_SMALL, successResults.getObject());
			lResponseMap.put(DigitalOrderLookupRestResourceConstants.ORDER_SUMMARY, orderSummary.getObject());
			lResponseMap.put(DigitalOrderLookupRestResourceConstants.WARNING_MESSAGES, warningMessages.getObject());
		}else if(errorResults.getObject() != null){
			lResponseMap.put(DigitalOrderLookupRestResourceConstants.ERROR_SMALL, errorResults.getObject());
		}
			
		
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateOrderHistoryDetails");
		}
		return lResponseMap;
		
	}
	/**
	 * Fetch User AppliedOffers and Redeemed Order Certificates.
	 * @param pOrderId
	 * @return
	 */
	private Map<String,Object> fetchAppliedOffersAndCertificates(String pOrderId){
		
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method fetchAppliedOffersAndCertificates : OrderId "+pOrderId);
		}
		
		Map<String, Object> lResponse = new HashMap<>();
		DigitalCouponFormHandler couponFormHandler = ComponentLookupUtil.lookupComponent(DigitalOrderLookupRestResourceConstants.COUPON_FORM_HANDLER_PATH,DigitalCouponFormHandler.class);
		couponFormHandler.setOrderId(pOrderId);
		Set<AppliedOffer> lAppliedOffersSet = couponFormHandler.getAppliedCoupons();
		List<LoyaltyCertificate> lLoyaltyCertificateList = couponFormHandler.getRedeemedOrderCertificates();
		
		lResponse.put("appliedOffers", lAppliedOffersSet);
		lResponse.put("RewardCertificates", lLoyaltyCertificateList);
		
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method fetchAppliedOffersAndCertificates");
		}

		
		return lResponse;
	}
}
