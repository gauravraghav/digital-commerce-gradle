package com.digital.commerce.jaxrs.common.constants;

/**
 * ProfileRestResourceContants
 *
 * @author TAIS
 *
 */
public class DigitalProfileRestResourceContants extends DigitalJaxRsResourceCommonConstants {

	public static final String PROFILE = "/atg/userprofiling/Profile";
	
	public static final String PROFILE_FORMHANDLER_COMPONENT_NAME = "/atg/userprofiling/ProfileFormHandler";
	
	public static final String FORGOT_PASSWORD_HANDLER_COMPONENT_NAME = "/atg/userprofiling/ForgotPasswordHandler";
	
	public static final String CREATE_CREDIT_CRD_FORM_HANDLER = "/atg/commerce/order/purchase/CreateCreditCardFormHandler";
	
	public static final String REGION_PROFILE = "/com/digital/commerce/services/region/RegionProfile";
	
	public static final String SHOPPING_CART = "/atg/commerce/ShoppingCart";
	
	public static final String INSTORE_DEVICE_DROPLET = "/com/digital/commerce/common/util/DigitalInStoreDeviceDroplet";
	
	public static final String HANDLE_ADD_NEW_CREDIT_CRD = "addNewCreditCard";
	
	public static final String LOGIN = "login";

	public static final String LOGIN_VALUE = "value.login";

	public static final String PASSWORD = "password";

	public static final String PASSWORD_VALUE = "value.password";
	public static final String FORCE_UPDATE_PASSWORD = "forceUpdatePassword";
	public static final String LOYALTY_NUMBER = "loyaltyNumber";

	public static final String LOYALTY_TIER = "loyaltyTier";

	public static final String POINTS_BALANCE = "pointsBalance";

	public static final String POINTS_NEXT_REWARD = "pointsNextReward";

	public static final String DOLLER_PENDING_CERT = "dollarsPendingCert";

	public static final String POINTS_NEXT_TIER = "pointsNextTier";

	public static final String CURRENT_POINT_BALANCE = "currentPointsBalance";

	public static final String POINTS_EXPIRATION_NEXT_MONTH = "pointsExpiringNextMonth";

	public static final String FAVORITE_STORE_DETAILS = "favoriteStoreDetails";

	public static final String USER_STATUS = "userStatus";

	public static final String SECURITY_STATUS = "securityStatus";

	public static final String SKIP_CERTS = "skipCerts";

	public static final String REWARD_AMOUNT = "rewardsAmount";

	public static final String SKIP_ORDERS = "skipOrders";

	public static final String SKIP_FEV_STORE = "skipFavStore";

	public static final String SKIP_REWARD_DEATIL = "skipRewardDetails";

	public static final String SYNC_REWARD = "syncReward";

	public static final String DSW_USER_INFO = "dswUserInfo";

	public static final String SKIP_SAVING = "skipSaving";

	public static final String IS_INSTORE_DEVICE = "isDSWInStoreDevice";

	public static final String LONGNITUDE = "longitude";

	public static final String RETURN_WISHLIST_IDS = "returnWishlistIds";

	public static final String AVAILABLE_SAVINGS = "availableSavings";

	public static final String REWARD_DETAILS = "rewardDetails";

	public static final String CCFLAG = "ccFlag";

	public static final String COOKIED_USER = "cookiedUser";

	public static final String ID = "id";

	public static final String GIFT_LIST_ID = "giftlistId";

	public static final String WISHLIST_DOT_COUNT = "wishList.wishListCount";

	public static final String CHAIN = "chain";

	public static final String SOURCE_ORIGINATED = "sourceOriginate";

	public static final String USER_ID = "userId";

	public static final String CREATE = "create";

	public static final String VALUE_MOBILE = "value.mobilePhoneNumber";

	public static final String BIRTH_MOUNTH = "birthMonth";

	public static final String BIRTHDAY = "birthDay";

	public static final String BIRTH_YEAR = "birthYear";

	public static final String VALUE_RECEIVEPROMO = "value.receivePromo";

	public static final String VALUE_PHONEPROMO = "value.phonePromo";

	public static final String ALERT_METHOD_EMAIL = "alertMethodEmail";

	public static final String ALERT_METHOD_SMS = "alertMethodSMS";

	public static final String HOMEADDRESS_HOMETYPE = "value.homeAddress.addressType";

	public static final String SOURCE = "source";

	public static final String SUGGESTED_ADDRESS = "suggestedAddress";

	public static final String HOMEADDRESS_REGION = "value.homeAddress.region";

	public static final String HOMEADDRESS_RANK = "value.homeAddress.rank";

	public static final String VALUE_AUTOLOGIN = "value.autoLogin";

	public static final String VALUE_REALMID = "value.realmId";

	public static final String VALUE_CONFIRM_PASSWORD = "value.confirmPassword";

	public static final String VALUE_DOB = "value.dateOfBirth";

	public static final String VALUE_FIRSTNAME = "value.firstName";

	public static final String VALUE_LOGIN = "value.login";

	public static final String VALUE_MIDDLENAME = "value.middleName";

	public static final String VALUE_PASSWORD = "value.password";

	public static final String VALUE_ORDERID = "value.orderID";

	public static final String VALUE_HOMEADDRESS_ADDRESS1 = "value.homeAddress.address1";

	public static final String VALUE_HOMEADDRESS_DAYTIME_PHONENUMBER = "value.daytimeTelephoneNumber";

	public static final String VALUE_HOMEADDRESS_ADDRESS2 = "value.homeAddress.address2";

	public static final String VALUE_HOMEADDRESS_ADDRESS3 = "value.homeAddress.address3";

	public static final String VALUE_HOMEADDRESS_CITY = "value.homeAddress.city";

	public static final String VALUE_HOMEADDRESS_STATE = "value.homeAddress.state";

	public static final String VALUE_HOMEADDRESS_COMPANYNAME = "value.homeAddress.companyName";

	public static final String VALUE_HOMEADDRESS_COUNTRY = "value.homeAddress.country";

	public static final String VALUE_HOMEADDRESS_COUNTY = "value.homeAddress.county";

	public static final String VALUE_HOMEADDRESS_JOBTITLE = "value.homeAddress.jobTitle";

	public static final String VALUE_HOMEADDRESS_POSTAL_CODE = "value.homeAddress.postalCode";

	public static final String VALUE_HOMEADDRESS_FAXNUMBER = "value.homeAddress.faxNumber";

	public static final String VALUE_HOMEADDRESS_FIRSTNAME = "value.homeAddress.firstName";

	public static final String VALUE_HOMEADDRESS_LASTNAME = "value.homeAddress.lastName";

	public static final String VALUE_HOMEADDRESS_MIDDLENAME = "value.homeAddress.middleName";

	public static final String VALUE_HOMEADDRESS_PHONENUMBER = "value.homeAddress.phoneNumber";

	public static final String VALUE_HOMEADDRESS_PREFIX = "value.homeAddress.prefix";

	public static final String VALUE_HOMEADDRESS_SUFFIX = "value.homeAddress.suffix";

	public static final String VALUE_LASTNAME = "value.lastName";

	public static final String VALUE_EMAIL = "value.email";

	public static final String VALUE_GENDER = "value.gender";

	public static final String VALUE_MEMBER = "value.member";

	public static final String CREATE_SUCCESS = "createSuccess";

	public static final String HOME_ADDRESS = "homeAddress";

	public static final String ATG_REST_VALUES = "atg-rest-values";

	public static final String LOGOUT = "logout";

	public static final String DPSLOGOUT = "DPSLogout";

	public static final String LOGOUT_ERROR_URL = "logoutErrorURL";

	public static final String LOGOUT_SUCCESS_URL = "logoutSuccessURL";

	public static final String FIRST_NAME_PROFILE = "firstName";

	public static final String MIDDLE_NAME = "middleName";

	public static final String GENDER = "gender";

	public static final String RECEIVE_PROMO = "receivePromo";

	public static final String PHONE_PROMO = "phonePromo";

	public static final String MOBILE_PHONE_NUMBER = "mobilePhoneNumber";

	public static final String DAYTIME_PHONENUMBER = "daytimeTelephoneNumber";

	public static final String ALERT_PREFERENCES = "alertPreferences";

	public static final String PHONE_NUMBER = "phoneNumber";

	public static final String PROFILE_RESPONSE = "profile";

	public static final String DATE_OF_BIRTH = "dateOfBirth";

	public static final String SHOPFOR_ITEM_DESCRIPTOR = "shopfor";

	public static final String POPULATE_CONTACTINFO = "populateContactInfo";

	public static final String FAVORITE_STORE_ID = "favoriteStoreId";

	public static final String CARD_ID = "cardId";

	public static final String CARD_NAME = "cardName";

	public static final String UPDATE = "update";

	public static final String MAKE_LOGIN_AS_SAME_EMAIL = "makeLoginSameAsEmail";

	public static final String MAKE_LOGIN_AS_SAME_LOGIN = "makeEmailSameAsLogin";

	public static final String SHIPPING_ADDRESS = "shippingAddress";

	public static final String VALUE_SHIPPING_ADDRESS_ADDRESSTYPE = "value.shippingAddress.addressType";

	public static final String VALUE_SHIPPING_ADDRESS_REGION = "value.shippingAddress.region";

	public static final String VALUE_SHIPPING_ADDRESS_RANK = "value.shippingAddress.rank";

	public static final String VALUE_SHIPPING_ADDRESS_ADDRESS1 = "value.shippingAddress.address1";

	public static final String VALUE_SHIPPING_ADDRESS_ADDRESS2 = "value.shippingAddress.address2";

	public static final String VALUE_SHIPPING_ADDRESS_CITY = "value.shippingAddress.city";

	public static final String VALUE_SHIPPING_ADDRESS_STATE = "value.shippingAddress.state";

	public static final String VALUE_SHIPPING_ADDRESS_POSTALCODE = "value.shippingAddress.postalCode";

	public static final String VALUE_SHIPPING_ADDRESS_FIRST_NAME = "value.shippingAddress.firstName";

	public static final String VALUE_SHIPPING_ADDRESS_MIDDLE_NAME = "value.shippingAddress.middleName";

	public static final String VALUE_SHIPPING_ADDRESS_LAST_NAME = "value.shippingAddress.lastName";

	public static final String BIRTH_YEAR_VALUE = "2004";

	public static final String UPDATE_CERT_DENOMINATIONS = "updateCertsDenomination";

	public static final String PAPER_LESS_CART = "paperlessCert";

	public static final String CERT_DENOMINATIONS = "certDenomination";

	public static final String EXCLUDE_EXPIRED_CARDS = "excludeExpiredCards";

	public static final String PRIMARY_CREDIT_CARD = "primaryCreditCard";

	public static final String ACTIVE_CREDIT_CARD = "activeCreditCards";

	public static final String OTHER_CREDIT_CARD = "otherCreditCards";

	public static final String ADDRESS_ID = "addressId";

	public static final String CHANGE_PASSWORD = "changePassword";

	public static final String LOGIN_HASH = "loginHash";

	public static final String OLD_PASSWORD_VALUE = "value.oldPassword";

	public static final String OLD_PASSWORD = "oldPassword";

	public static final String VERSION = "version";

	public static final String VERSION_VALUE = "2.0";

	public static final String OTHER_ADDRESS = "otherAddress";

	public static final String ADDRESS_REPOSITORY_ID = "addressRepositryId";

	public static final String MAKE_PRIMARY_SHIPPING_ADDRESS = "makePrimaryShippingAddress";

	public static final String UPDATE_OTHER_ADDRESS_STATUS = "updateOtherAddrStatus";

	public static final String DSWADDRESS_PATH = "com.digital.commerce.services.common.DigitalAddress";

	public static final String WISHLIST_COUNT = "wishListCount";
	
	public static final String FORGOT_PASSWORD = "forgotPassword";

	public static final String TWO = "2.0";

	public static final String CLIENT_SECRET = "clientSecret";

	public static final String SEND_EMAIL_CREATE_ACCOUNT = "sendEmailOnCreateAccount";

	public static final String MAILLING_ADDRESS_EXISTS = "mailingAddressExists";

	public static final String FAVORITE_STORES = "favoriteStores";

	public static final String CONTACT_INFO = "contactInfo";

	public static final String SECONDARY_ADDRESSES = "secondaryAddresses";

	public static final String ALL_SECONDARY_ADDRESS = "allSecondaryAddresses";

	public static final String DSW_VISA_MEMBER = "dswVisaMember";
	
	public static final String BILLING_SAME_AS_SHIPPING_ADDRESS = "billingSameAsShippingAddress";
	
	public static final String BILLING_SAME_AS_MAILING_ADDRESS = "billingSameAsMailingAddress";
	
	public static final String CRDT_CRD_PAY_PAGE_REGISTRATION_ID = "creditCard.paypageRegistrationId";
	
	public static final String PAY_PAGE_REGISTRATION_ID = "paypageRegistrationId";
	
	public static final String CRDT_CRD_VANTIV_FIRST_SIX = "creditCard.vantivFirstSix";
	
	public static final String VANTIV_FIRST_SIX = "vantivFirstSix";
	
	public static final String CRDT_CRD_VANTIV_FIRST_FOUR = "creditCard.vantivLastFour";
	
	public static final String VANTIV_FIRST_FOUR = "vantivLastFour";
	
	public static final String CRDT_CRD_DEFAULT_CREDIT_CRD = "creditCard.defaultCreditCard";
	
	public static final String IS_DEFAULT_CREDIT_CRD = "isDefaultCreditCard";
	
	public static final String CRDT_CRD_CREDIT_CRD_TYPE = "creditCard.creditCardType";
	
	public static final String CREDIT_CRD_TYPE = "creditCardType";
	
	public static final String CRDT_CRD_NAME_ON_CRD = "creditCard.nameOnCard";
	
	public static final String NAME_ON_CRD = "nameOnCard";
	
	public static final String CRDT_CRD_CREDIT_CRD_NUMBER = "creditCard.creditCardNumber";
	
	public static final String CREDIT_CRD_NUMBER = "creditCardNumber";
	
	public static final String CRDT_CRD_EXPIRATION_MONTH = "creditCard.expirationMonth";
	
	public static final String EXPIRATION_MONTH = "expirationMonth";
	
	public static final String CRDT_CRD_EXPIRATION_YEAR = "creditCard.expirationYear";
	
	public static final String EXPIRATION_YEAR = "expirationYear";
	
	public static final String CRDT_CRD_BILL_FIRST_NAME = "creditCard.billingAddress.firstName";
	
	public static final String CRDT_CRD_BILL_MIDDLE_NAME = "creditCard.billingAddress.middleName";
	
	public static final String CRDT_CRD_BILL_LAST_NAME = "creditCard.billingAddress.lastName";
	
	public static final String CRDT_CRD_BILL_ADDRESS_ONE = "creditCard.billingAddress.address1";
	
	public static final String CRDT_CRD_BILL_ADDRESS_TWO = "creditCard.billingAddress.address2";
	
	public static final String CRDT_CRD_BILL_CITY = "creditCard.billingAddress.city";
	
	public static final String CRDT_CRD_BILL_STATE = "creditCard.billingAddress.state";
	
	public static final String CRDT_CRD_BILL_COUNTRY = "creditCard.billingAddress.country";
	
	public static final String CRDT_CRD_BILL_POSTAL_CODE = "creditCard.billingAddress.postalCode";
	
	public static final String DEFAULT_CREDIT_CRD_ID = "defaultCreditCardId";
	

}
