package com.digital.commerce.jaxrs.common.constants;

public class DigitalAdsActorConstants extends DigitalJaxRsResourceCommonConstants {

	public static final String ADS_AUTH_FORM_HANDLER_PATH =  "/com/digital/commerce/services/digitalvisa/AdsAuthFormHandler";

	public static final String HANDLE_ADS_POSTDATA = "getAdsPostData";

}
