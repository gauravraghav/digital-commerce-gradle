package com.digital.commerce.jaxrs.gifts.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;

import com.digital.commerce.jaxrs.common.constants.DigitalCartResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalGiftListRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.services.catalog.DigitalCatalogTools;
import com.digital.commerce.services.gifts.DigitalGiftlistManager;

import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.droplet.TagConverterManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.service.jaxrs.DropletInvoker;
import atg.service.jaxrs.OParam;
import atg.service.jaxrs.Output;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

/**
 * Helper class for Gift List Resource
 *
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalGiftListRestResourceHelper extends GenericService {

	/**
	 * mGiftlistManager - Gift List manager
	 */
	private DigitalGiftlistManager giftlistManager;

	/**
	 * catalogTools - CatalogTools
	 */
	private DigitalCatalogTools catalogTools;

	/**
	 * commonHelper - Common Helper
	 */
	private DigitalCommonRestResourceHelper commonHelper;

	/**
	 * This method will populate the user's wishlist details based on request parameters
	 *
	 * @param pReturnProdIdsOnly
	 * @param pStartIndex
	 * @param pEndIndex
	 * @param pGiftLisId
	 *
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked") // OK To Do
	public Map<String, Object> populateWishListDetailsV3(Boolean pReturnProdIdsOnly, Integer pStartIndex, Integer pEndIndex,
			String pGiftLisId) throws Exception {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method populateWishListDetailsV3()");
		}

		Map<String, Object> lResult = new HashMap<String, Object>();
		Map<String, Object> lGiftlisItemResponse = new HashMap<>();

		boolean lReturnProdIdsOnly = pReturnProdIdsOnly == null ? false : pReturnProdIdsOnly;
		String lWishlistId = pGiftLisId;
		int lStartIndex = pStartIndex == null ? 0 : pStartIndex - 1;
		int lEndIndex = pEndIndex == null ? 30 : pEndIndex;

		if (lWishlistId == null) {
			Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
			RepositoryItem lProfileWishlist = (RepositoryItem) lProfile.getPropertyValue(DigitalGiftListRestResourceConstants.WISHLIST);
			if (lProfileWishlist != null) {
				lWishlistId = lProfileWishlist.getRepositoryId();
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("ReturnProdIdsOnly - {0}, lWishlistId - {1}, lStartIndex - {2}, lEndIndex - {3}", lReturnProdIdsOnly, lWishlistId,
					lStartIndex, lEndIndex);
		}

		RepositoryItem lGiftlist = getGiftlistManager().getGiftlist(lWishlistId);

		if (lGiftlist == null) {
			vlogDebug("Wishlist id is not there in repository.");
			return lResult;
		}

		DropletInvoker dropletInvoker = new DropletInvoker(DigitalGiftListRestResourceConstants.GIFTT_LIST_SITE_FILTER_DROPLET);

		// TODO Change DigitalGiftListRestResourceConstants.GIFT_LIST_ITEMS to
		// DigitalGiftListRestResourceConstants.GIFT_LIST_ITEMS_FROM_VIEW as in local, view
		// "DSW_WISHLIST_ACTIVE_ITEMS_V" is not working properly.
		dropletInvoker.addInput(DigitalGiftListRestResourceConstants.COLLECTION,
				lGiftlist.getPropertyValue(DigitalGiftListRestResourceConstants.GIFT_LIST_ITEMS));

		OParam oParamOutput = dropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
		Output lSuccessOutput = oParamOutput.addOutput(DigitalGiftListRestResourceConstants.FILTERED_COLLECTION,
				DigitalGiftListRestResourceConstants.FILTERED_COLLECTION);

		dropletInvoker.invoke();

		List<RepositoryItem> lFilteredCollection = (List<RepositoryItem>) lSuccessOutput.getObject();
		if (lFilteredCollection == null) {
			vlogDebug("No filtered collection for wishlist.");
			return lResult;
		}

		lResult.put(DigitalGiftListRestResourceConstants.WISH_LIST_ID, lWishlistId);

		if (!lReturnProdIdsOnly) {
			lResult.put(DigitalGiftListRestResourceConstants.WISH_LIST_COUNT, lFilteredCollection.size());
			// Make it same as list size if request number is greater than list
			// size
			lEndIndex = lEndIndex > lFilteredCollection.size() ? lFilteredCollection.size() : lEndIndex;
			lFilteredCollection = lFilteredCollection.subList(lStartIndex, lEndIndex);

			lResult.put(DigitalGiftListRestResourceConstants.WISH_ITEMS,
					populateWishlistItemResponse(lGiftlisItemResponse, lFilteredCollection));
			return lResult;
		} else {
			for (RepositoryItem lGistlistItem : lFilteredCollection) {
				Map<String, Object> lGiftlistData = new HashMap<>();
				if (lGistlistItem.getPropertyValue(DigitalGiftListRestResourceConstants.CATALOG_REF_ID) != null) {
					lGiftlistData.put(DigitalGiftListRestResourceConstants.SKU,
							lGistlistItem.getPropertyValue(DigitalGiftListRestResourceConstants.CATALOG_REF_ID));
				} else {
					lGiftlistData.put(DigitalGiftListRestResourceConstants.PRODUCT,
							lGistlistItem.getPropertyValue(DigitalGiftListRestResourceConstants.PRODUCT_ID));
				}
				lGiftlisItemResponse.put(lGistlistItem.getRepositoryId(), lGiftlistData);
			}
			lGiftlisItemResponse.put(DigitalGiftListRestResourceConstants.WISH_LIST_COUNT, lFilteredCollection.size());
			return lGiftlisItemResponse;
		}

	}

	/**
	 * This method will populate wishlist item details with inner product/sku details
	 *
	 * @param pGiftlisItemResponse
	 * @param pFilteredCollection
	 * @return
	 * @throws Exception
	 */
	private Map<String, Object> populateWishlistItemResponse(Map<String, Object> pGiftlisItemResponse,
			List<RepositoryItem> pFilteredCollection) throws Exception {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method populateWishlistItemResponse()");
		}
		for (RepositoryItem lGistlistItem : pFilteredCollection) {
			DropletInvoker lProductLookupDropletInvoker = new DropletInvoker(DigitalGiftListRestResourceConstants.PRODUCT_LOOKUP_DROPLET);
			lProductLookupDropletInvoker.addInput(DigitalGiftListRestResourceConstants.REPOSITORY_KEY, ServletUtil.getUserLocale());
			lProductLookupDropletInvoker.addInput(DigitalGiftListRestResourceConstants.ID,
					lGistlistItem.getPropertyValue(DigitalGiftListRestResourceConstants.PRODUCT_ID));
			lProductLookupDropletInvoker.addInput(DigitalGiftListRestResourceConstants.FILTER_BY_SITE, true);
			lProductLookupDropletInvoker.addInput(DigitalGiftListRestResourceConstants.FILTER_BY_CATALOG, false);

			OParam oParamProductOutput = lProductLookupDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
			Output lProductOutput = oParamProductOutput.addOutput(DigitalGiftListRestResourceConstants.ELEMENT,
					DigitalGiftListRestResourceConstants.ELEMENT);

			lProductLookupDropletInvoker.invoke();

			if (lProductOutput.getObject() != null) {
				Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
				RepositoryItem lPriceList = (RepositoryItem) lProfile.getPropertyValue(DigitalGiftListRestResourceConstants.PRICE_LIST);
				Map<String, Object> lFormattedOuput = getCatalogTools().transformOutputforWishListpage(
						(RepositoryItem) lProductOutput.getObject(), lGistlistItem,
						lPriceList.getPropertyValue(DigitalGiftListRestResourceConstants.LOCALE), 2);

				if (lFormattedOuput != null) {
					pGiftlisItemResponse.put(lGistlistItem.getRepositoryId(), lFormattedOuput);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method populateWishlistItemResponse()");
		}
		return pGiftlisItemResponse;
	}

	/**
	 * Method to remove gift item from gift list
	 *
	 * @param pGiftItemId
	 *            Gift Item To Remove
	 * @return FormHandlerInvocationResult Result
	 * @throws ServletException
	 */
	public FormHandlerInvocationResult removeItemFromGiftList(String pGiftItemId) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method removeItemFromGiftList()");
		}
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		RepositoryItem lProfileWishlist = (RepositoryItem) lProfile.getPropertyValue(DigitalGiftListRestResourceConstants.WISHLIST);
		String lGiftlistId = lProfileWishlist.getRepositoryId();

		if (isLoggingDebug()) {
			vlogDebug("Profile id is {0}, Gift List Id is {1}", lProfile.getRepositoryId(), lGiftlistId);
		}

		FormHandlerExecutor executor = new FormHandlerExecutor(DigitalGiftListRestResourceConstants.GIFT_LIST_FORM_HANDLER,
				DigitalGiftListRestResourceConstants.UPDATE_GIFT_LIST_ITEMS);

		executor.addInput(DigitalCartResourceConstants.GIFT_LIST_ID, lGiftlistId);
		executor.addInput("removeAll", false);
		executor.addInput(DigitalGiftListRestResourceConstants.REMOVE_GIFT_IDS, pGiftItemId, TagConverterManager.getTagConverterByName("array"),
				new Properties());
		FormHandlerInvocationResult lResult = executor.execute();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method removeItemFromGiftList()");
		}
		return lResult;
	}
}
