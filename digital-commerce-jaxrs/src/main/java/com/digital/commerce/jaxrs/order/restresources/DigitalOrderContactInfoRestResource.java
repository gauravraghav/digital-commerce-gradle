package com.digital.commerce.jaxrs.order.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalOrderCommonRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalOrderLookupRestResourceConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.jaxrs.order.helper.DigitalOrderAltPickupRestResourceHelper;
import com.digital.commerce.jaxrs.order.helper.DigitalOrderContactInfoRestResourceHelper;
import com.digital.commerce.jaxrs.order.helper.DigitalOrderLookupRestResourceHelper;

import atg.droplet.DropletException;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class provides rest resources  for DigitalOrderContactInfoActor.
 * 
 * @author TAIS
 *
 */

@RestResource(id = "com.digital.commerce.jaxrs.order.restresources.DigitalOrderContactInfoRestResource")
@Path("/orderContactInfo")
@Api("orderContactInfo Service")
@Getter
@Setter
public class DigitalOrderContactInfoRestResource extends DigitalGenericService {
	/**
	 * contactInfoHelper - DigitalOrderContactInfoRestResourceHelper Component
	 */
	private DigitalOrderContactInfoRestResourceHelper contactInfoHelper;

	/**
	 * Service to getOrderInfo.
	 * 
	 * @see - Actor Chain - /com/digital/commerce/services/v1_0/order/contactinfo/DigitalOrderContactInfoActor/getOrderInfo
	 * 
	 * @return Response
	 */
	@GET
	@Path("/orderContactInfo")
	@Endpoint(id = "/orderContactInfo/getOrderInfo#GET", isSingular = true, filterId = "digital-constants-response")
	public Response getContactInfo() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getContactInfo");
		}

		Map<String, Object> lResponse = new HashMap<String, Object>();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getContactInfo");
		}
		return Response.ok(lResponse).build();
	}


	/**
	 * Service for add or update contactInfo.
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/order/contactinfo/DigitalOrderContactInfoActor/addOrUpdateContactInfo
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/addOrUpdateContactInfo")
	@Endpoint(id = "/orderContactInfo/addOrUpdateContactInfo#POST", isSingular = true, filterId = "order-contactInfo-schema")
	public Response addOrUpdateContactInfo(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addOrUpdateContactInfo");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Map<String, Object> lResultMap = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequestBody);
		if (isLoggingDebug()) {
			logDebug("Request for addOrUpdateContactInfo " + lRequestPayload);
		}
		try {
			lResultMap = getContactInfoHelper().addOrUpdateContactInfoDetails(lRequestPayload);
			lFinalResponse.put(DigitalOrderCommonRestResourceConstants.RESPONSE, lResultMap);
		} catch (Exception e) {
			vlogError(e, "JSONException | ServletException");

		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addOrUpdateContactInfo");
		}
		return Response.ok(lFinalResponse).build();
	}
}
