package com.digital.commerce.jaxrs.common.constants;

/**
 * Constant file for reward resource
 *
 * @author TAIS
 *
 */
public class DigitalRewardResourceConstants extends DigitalJaxRsResourceCommonConstants {

	public static final String REWARD_FORM_HANDLER_PATH = "/com/digital/commerce/services/v1_0/rewards/DigitalRewardsFormHandler";

	public static final String HANDLE_ANONYMOUS_SUBSCRIPTION = "anonymousSubscriptionFooter";

	public static final String HANDLE_CREATE_SHOP_FOR = "createShopfor";

	public static final String HANDLE_UPDATE_SHOP_FOR = "UpdateShopfor";

	public static final String HANDLE_DELETE_SHOP_FOR = "DeleteShopfor";

	public static final String HANDLE_ADD_SHOP_WITHOUT_A_CARD_REQUEST = "addShopWithoutACardRequest";

	public static final String HANDLE_CREATE_GIFT = "createGift";

	public static final String HANDLE_UPDATE_GIFT = "updateGift";
	
	public static final String HANDLE_RETRIEVE_REWARD_DETAILS = "retrieveRewardDetails";
	
	public static final String HANDLE_ISSUE_CERT = "issueCertificate";
	
	public static final String HANDLE_DONATE_CERT = "donateCertificates";
	
	public static final String HANDLE_RETRIEVE_CHARITIES = "retrieveCharities";

	public static final String EMAIL_KEY = "rewardsSubscribeEmailFooterRequest.email";

	public static final String EMAIL_SOURCE_KEY = "rewardsSubscribeEmailFooterRequest.emailSource";

	public static final String EMAIL_SOURCE = "emailSource";

	public static final String SHOP_FOR_NAME_KEY = "rewardsShopforItemRequest.shopforName";

	public static final String SHOP_FOR_NAME = "shopforName";

	public static final String SHOP_FOR_RELATION_KEY = "rewardsShopforItemRequest.relationship";

	public static final String RELATION_SHIP = "relationship";

	public static final String SHOP_FOR_BIRTHDAY_KEY = "rewardsShopforItemRequest.birthDay";

	public static final String SHOP_FOR_BIRTH_MONTH_KEY = "rewardsShopforItemRequest.birthMonth";

	public static final String SHOP_FOR_WEB_TYPE = "rewardsShopforItemRequest.webType";

	public static final String SHOP_FOR_GENDER_KEY = "rewardsShopforItemRequest.gender";

	public static final String SHOP_FOR_SIZE_GROUP_KEY = "rewardsShopforItemRequest.sizeGroup";

	public static final String SHOP_FOR_SIZES_KEY = "rewardsShopforItemRequest.sizes";

	public static final String SHOP_FOR_WIDTHS_KEY = "rewardsShopforItemRequest.widths";

	public static final String SHOP_FOR_ID_KEY = "rewardsShopforItemRequest.shopforID";

	public static final String SHOP_FOR_ID = "shopforID";

	public static final String SHOP_FOR_REWARDS_ID_KEY = "rewardsShopforItemRequest.rewardsShopforID";

	public static final String SHOP_FOR_REWARDS_ID = "rewardsShopforID";

	public static final String GIFT_FIRST_NAME_KEY = "rewardsGiftItemRequest.firstName";

	public static final String GIFT_LAST_NAME_KEY = "rewardsGiftItemRequest.lastName";

	public static final String GIFT_BIRTHDAY_KEY = "rewardsGiftItemRequest.birthDay";

	public static final String GIFT_BIRTH_MONTH_KEY = "rewardsGiftItemRequest.birthMonth";

	public static final String GIFT_MESSAGE_KEY = "rewardsGiftItemRequest.giftMessage";
	
	public static final String GIFT_MESSAGE_ID_KEY = "rewardsGiftItemRequest.id";

	public static final String GIFT_ID_KEY = "rewardsGiftItemRequest.id";

	public static final String GIFT_EMAIL_KEY = "rewardsGiftItemRequest.email";

	public static final String MEMBER_ID_KEY = "rewardsAddShopWithoutACardRequest.memberId";

	public static final String BARCODE_KEY = "rewardsAddShopWithoutACardRequest.barCode";
	
    public static final String FILTERS_KEY = "filters";
    
    public static final String SYNC_REWARDS_KEY = "syncRewards";
    
    public static final String COMBINE_BIRTHDAY_OFFERS_KEY = "combineBirthdayOffers";
    
    public static final String CHARITY_ID_KEY= "rewardsDonateCertsRequest.charityId";
    
    public static final String CERT_IDS_KEY = "rewardsDonateCertsRequest.certificateIds";
    
    public static final String CERT_DENOMINATION_KEY="rewardsIssueCertsRequest.certDenomination";
    
    public static final String CERT_DENOMINATION = "certDenomination";
    
    public static final String CHARITY_ID = "charityId";
    
    public static final String CERT_IDS = "certificateIds";

}
