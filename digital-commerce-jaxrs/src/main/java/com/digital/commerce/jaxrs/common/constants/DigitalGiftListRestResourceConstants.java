package com.digital.commerce.jaxrs.common.constants;

/**
 * Constant file for Gift List Services
 *
 * @author TAIS
 *
 */
public class DigitalGiftListRestResourceConstants extends DigitalJaxRsResourceCommonConstants {

	public static final String GIFTT_LIST_SITE_FILTER_DROPLET = "/atg/commerce/collections/filter/droplet/GiftlistSiteFilterDroplet";

	public static final String WISH_LIST_ID = "wishlistId";

	public static final String COLLECTION = "collection";

	public static final String GIFT_LIST_ITEMS = "giftlistItems";

	public static final String FILTERED_COLLECTION = "filteredCollection";

	public static final String WISH_LIST_COUNT = "wishListCount";

	public static final String PRODUCT_LOOKUP_DROPLET = "/atg/commerce/catalog/ProductLookup";

	public static final String FILTER_BY_SITE = "filterBySite";

	public static final String FILTER_BY_CATALOG = "filterByCatalog";

	public static final String WISH_ITEMS = "wishItems";

	public static final String PRICE_LIST = "priceList";

	public static final String GIFT_LIST_ITEMS_FROM_VIEW = "giftlistItemsfromVIEW";

	public static final String GIFT_LIST_FORM_HANDLER = "/atg/commerce/gifts/GiftlistFormHandler";

	public static final String UPDATE_GIFT_LIST_ITEMS = "updateGiftlistItems";

	public static final String REMOVE_GIFT_IDS = "removeGiftitemIds";

	public static final String HANDLE_SAVE_GIFT_LIST = "saveGiftlist";

	public static final String HANDLE_ADD_ITEM_LIST = "addItemToGiftlist";

	public static final String HANDLE_MOVE_ITEMS_FROM_CART = "moveItemsFromCart";

	public static final String GIFT_LIST_ID = "giftlistId";

	public static final String ITEM_LIST_STR = "itemListStr";

	public static final String ITEM_IDS = "itemIds";

	public static final String SEND_ITEM_REMOVE_MSG = "sendItemRemovedMessages";

	public static final String HANDLE_SHARE_WISHLIST = "emailShare";

	public static final String EMAILS = "emails";

	public static final String INCLUDE_OWNER = "includeOwner";

	public static final String SHARED_MESSAGE = "sharedMessage";

	public static final String SHARE_GIFT_LIST_FORM_HANDLER = "/com/digital/commerce/services/gifts/DigitalShareWishlistHandler";

}
