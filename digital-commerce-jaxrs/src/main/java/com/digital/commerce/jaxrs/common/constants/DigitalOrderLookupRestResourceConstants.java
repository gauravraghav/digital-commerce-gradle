package com.digital.commerce.jaxrs.common.constants;

/**
 * @author TAIS
 *
 */
public class DigitalOrderLookupRestResourceConstants extends DigitalJaxRsResourceCommonConstants {

	public static final String DASHBOARD = "dashboard";
	
	public static final String HISTORY = "history";
	
	public static final String COUPON_FORM_HANDLER_PATH = "/atg/commerce/promotion/CouponFormHandler";

	public static final String ORDER_HISTORY_DROPLET = "/com/digital/commerce/services/order/DigitalOrderHistory";
	
	public static final String ORDER_LOOKUP_DROPLET = "/atg/commerce/order/OrderLookup";
	
	public static final String APPLIED_COUPONS_REQUEST = "appliedCoupons";

	public static final String USER_MESSAGE_RESOURCE_BUNDLE = "atg.commerce.order.UserMessages";

	public static final String UNABLE_TO_LOCATE_ORDER = "UnableToLocateOrder";
	
	public static final String INVALID_DATA_PASSED = "INVALID_DATA_PASSED";
	
	public static final String USER_ID = "userId";
	
	public static final String SOURCE_ORIGINATE = "sourceOriginate";
	
	public static final String NUM_OF_ORDERS = "numOrders";
	
	public static final String MONTHS = "months";
	
	public static final String START_INDEX = "startIndex";
	
	public static final String END_INDEX = "endIndex";
	
	public static final String CHAIN = "chain";
	
	public static final String STATUS = "status";
	
	public static final String SUBMITTED_DATE = "submittedDate";
	
	public static final String ORDER_SUMMARY = "orderSummary";
	
	public static final String WARNING_MESSAGES = "warningMessages";
	
	public static final String EXISTING_USER = "existingUser";
	
	public static final String ATG_PROFILE_ID = "ATGProfileId";
	
	public static final String USERID = "userID";
	
	public static final String LOYALTY_NUMBER = "loyaltyNumber";
	
	public static final String NEW_USER_ELIGIBILITY = "newUserEligibility";

}
