package com.digital.commerce.jaxrs.profile.restresources;

import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.jaxrs.auth.helper.DigitalJWTAuthRestResourceHelper;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalProfileRestResourceContants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.jaxrs.profile.helper.DigitalProfileRestResourceHelper;
import com.digital.commerce.services.profile.handler.DigitalProfileFormHandler;

import atg.core.util.StringUtils;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.actor.Actor;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class will provide RestResource for profile Services.
 *
 * @author TAIS
 *
 */
@RestResource(id = "com.digital.commerce.jaxrs.profile.restresources.DigitalProfileRestResource")
@Path("/profile")
@Api("Profile Service")
@Getter
@Setter
public class DigitalProfileRestResource extends DigitalGenericService {

	/**
	 * mProfileResourceHelper - DigitalProfileRestResourceHelper Component
	 */

	private DigitalProfileRestResourceHelper profileResourceHelper;

	/**
	 * mAuthHelper - DigitalJWTAuthRestResourceHelper Component
	 */
	private DigitalJWTAuthRestResourceHelper authHelper ;

	/**
	 * Service to loginV3 details.
	 *
	 * @see - /rest/model/atg/userprofiling/ProfileActor/loginV3
	 *
	 * @return Response
	 */

	@POST
	@Path("/loginV3")
	@Endpoint(id = "/profile/loginV3#POST", isSingular = true, filterId = "dsw-login-v3-response")
	public Response loginV3(JSONObject pLoginRequest) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method loginV3");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pLoginRequest);

		try {

			setRequestedTime();

			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.PROFILE_FORMHANDLER_COMPONENT_NAME,
					DigitalProfileRestResourceContants.LOGIN);
			executor.addInput(DigitalProfileRestResourceContants.LOGIN_VALUE, lRequestPayload.get(DigitalProfileRestResourceContants.LOGIN));
			executor.addInput(DigitalProfileRestResourceContants.PASSWORD_VALUE, lRequestPayload.get(DigitalProfileRestResourceContants.PASSWORD));
			executor.addInput(DigitalProfileRestResourceContants.FORCE_UPDATE_PASSWORD, true);

			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lResponseMap = getProfileResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			} else {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
						getProfileResourceHelper().populateLoginSuccessResponseV3(lRequestPayload));
			}
			setResponseHeaders();
		} catch (JSONException | ServletException e) {
			vlogError(e, "JSONException | ServletException");

		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method loginV3");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to loginV2 details.
	 *
	 * @see - /rest/model/atg/userprofiling/ProfileActor/loginV2
	 *
	 * @return Response
	 */

	@POST
	@Path("/loginV2")
	@Endpoint(id = "/profile/loginV2#POST", isSingular = true, filterId = "dsw-login-response")
	public Response loginV2(JSONObject pLoginRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method loginV2");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pLoginRequest);
		try {
			// login
			setRequestedTime();
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.PROFILE_FORMHANDLER_COMPONENT_NAME,
					DigitalProfileRestResourceContants.LOGIN);

			executor.addInput(DigitalProfileRestResourceContants.LOGIN_VALUE, lRequestPayload.get(DigitalProfileRestResourceContants.LOGIN));
			executor.addInput(DigitalProfileRestResourceContants.PASSWORD_VALUE, lRequestPayload.get(DigitalProfileRestResourceContants.PASSWORD));
			executor.addInput(DigitalProfileRestResourceContants.FORCE_UPDATE_PASSWORD, true);

			FormHandlerInvocationResult lResult = executor.execute();

			if (lResult.isFormError()) {
				// Error
				lResponseMap = getProfileResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			} else {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
						getProfileResourceHelper().populateloginV2SuccessResponse(lRequestPayload));
			}
		} catch (Exception e) {
			vlogError(e.toString(), " JSONException | ServletException | RepositoryException | PropertyNotFoundException");

		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method loginV2");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to loginV2 details.
	 *
	 * @see - /rest/model/atg/userprofiling/ProfileActor/login
	 *
	 * @return Response
	 */

	@POST
	@Path("/login")
	@Endpoint(id = "/profile/login#POST", isSingular = true, filterId = "dsw-login-response")
	public Response login(JSONObject pLoginRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method login");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pLoginRequest);

		try {
			// login
			setRequestedTime();
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.PROFILE_FORMHANDLER_COMPONENT_NAME,
					DigitalProfileRestResourceContants.LOGIN);

			executor.addInput(DigitalProfileRestResourceContants.LOGIN_VALUE, lRequestPayload.get(DigitalProfileRestResourceContants.LOGIN));
			executor.addInput(DigitalProfileRestResourceContants.PASSWORD_VALUE, lRequestPayload.get(DigitalProfileRestResourceContants.PASSWORD));
			boolean lForceUpdatePassword = lRequestPayload.get(DigitalProfileRestResourceContants.FORCE_UPDATE_PASSWORD) == null ? false
					: (boolean) lRequestPayload.get(DigitalProfileRestResourceContants.FORCE_UPDATE_PASSWORD);

			executor.addInput(DigitalProfileRestResourceContants.FORCE_UPDATE_PASSWORD, lForceUpdatePassword);

			FormHandlerInvocationResult lResult = executor.execute();

			if (lResult.isFormError()) {
				// Error
				lResponseMap = getProfileResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			} else {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
						getProfileResourceHelper().populateloginV2SuccessResponse(lRequestPayload));
			}
		} catch (Exception e) {
			vlogError(e.toString(), " JSONException | ServletException | RepositoryException | PropertyNotFoundException ");

		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method login");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to getUserInfo details.
	 *
	 * @see - Actor Chain -/atg/userprofiling/ProfileActor/getUserInfo
	 *
	 * @return Response
	 */

	@GET
	@Path("/getUserInfo")
	@Endpoint(id = "/profile/getUserInfo#@GET", isSingular = true, filterId = "dsw-getUserInfo-response")
	public Response getUserInfo(@QueryParam(DigitalProfileRestResourceContants.SKIP_CERTS) Boolean pSkipCerts ,@QueryParam(DigitalProfileRestResourceContants.SKIP_ORDERS) Boolean pSkipOrders,@QueryParam(DigitalProfileRestResourceContants.SKIP_FEV_STORE) Boolean pSkipFavStore) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method getUserInfo");
		}
		Map<String, Object> lResponseMap = new HashMap<>();

		Map<String,Object> lRequestMap = new HashMap<>();

		lRequestMap.put(DigitalProfileRestResourceContants.SKIP_CERTS, pSkipCerts);
		lRequestMap.put(DigitalProfileRestResourceContants.SKIP_ORDERS, pSkipOrders);
		lRequestMap.put(DigitalProfileRestResourceContants.SKIP_FEV_STORE, pSkipFavStore);

		setRequestedTime();
		try {
			lResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE, getProfileResourceHelper().getUserInfoResponse(lRequestMap));
		} catch (Exception e) {
			vlogError(e, " RepositoryException | PropertyNotFoundException");
		}
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method getUserInfo");
		}

		return Response.ok(lResponseMap).build();
	}

	/**
	 * Service to create profile details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/create
	 *
	 * @return Response
	 */

	@SuppressWarnings("unchecked") // Ok To Do
	@POST
	@Path("/create")
	@Endpoint(id = "/profile/create#POST", isSingular = true, filterId = "dsw-create-response")
	public Response create(JSONObject pLoginRequest) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method create");
		}
		setRequestedTime();

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pLoginRequest);
		Map<String, Object> lHomeAddressData = (Map<String, Object>) lRequestPayload.get(DigitalProfileRestResourceContants.HOME_ADDRESS);
		lHomeAddressData = (Map<String, Object>) lHomeAddressData.get(DigitalProfileRestResourceContants.ATG_REST_VALUES);

		try {
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.PROFILE_FORMHANDLER_COMPONENT_NAME,
					DigitalProfileRestResourceContants.CREATE);
			getProfileResourceHelper().inputsForCommonCreateChain(executor, lRequestPayload);
			FormHandlerInvocationResult lResult = executor.execute();
			DigitalProfileFormHandler lProfieFormHandler = (DigitalProfileFormHandler) lResult.getFormHandler();
			if (lResult.isFormError()) {
				// Error
				if (lProfieFormHandler.getSuggestedAddressMap() != null) {
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.SUGGESTED_ADDRESSMAP, lProfieFormHandler.getSuggestedAddressMap());
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.DECISION, DigitalJaxRsResourceCommonConstants.SUGGESTED);
				}
				lResponseMap = getProfileResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			} else {
				String lSource = (String) lRequestPayload.get(DigitalProfileRestResourceContants.SOURCE);
				if (StringUtils.isEmpty(lSource)) {
					String lSuccessMessage = lProfieFormHandler.getSuccessMessage();
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.STATUS_CODE, DigitalJaxRsResourceCommonConstants.SUCCESS);
					lResponseMap.put(DigitalProfileRestResourceContants.CREATE_SUCCESS, lSuccessMessage);

					Boolean lSkipSavings = (Boolean) lRequestPayload.get(DigitalProfileRestResourceContants.SKIP_SAVING);

					if (lSkipSavings == false) {
						Map<String, Object> lAvailableSaving = getProfileResourceHelper().getCouponHelper().populateAvailableSavings(null,
								null);
						if (lAvailableSaving != null) {
							if (lAvailableSaving.containsKey(DigitalJaxRsResourceCommonConstants.ERROR_SMALL)) {
								lResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
										lAvailableSaving.get(DigitalJaxRsResourceCommonConstants.ERROR_SMALL));
							}
							if (lAvailableSaving.containsKey(DigitalJaxRsResourceCommonConstants.SUCCESS)) {
								Map<String, Object> lSortedPromotions = (Map<String, Object>) lAvailableSaving
										.get(DigitalJaxRsResourceCommonConstants.SUCCESS);
								Map<String, Object> lAllSortedPromotions = new HashMap<>();
								for (String sortedPromotionKey : lSortedPromotions.keySet()) {
									lAllSortedPromotions
									.putAll((Map<? extends String, ? extends Object>) lSortedPromotions.get(sortedPromotionKey));
								}

								lResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lAllSortedPromotions);
							}
						}
					}

				} else {
					String lOnlineProfileId = lProfieFormHandler.getRepositoryId();
					String lTransactionId = lProfieFormHandler.getTransactionId();
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.STATUS_CODE, DigitalJaxRsResourceCommonConstants.SUCCESS);
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.TRANSACTION_ID, lOnlineProfileId);
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.ONLINE_PROFILE_ID, lTransactionId);
				}
			}

		} catch (Exception e) {
			vlogError(e, "ServletException | RepositoryException |JSONException ");
		}
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method create");
		}

		return Response.ok(lResponseMap).build();
	}

	/**
	 * Service to createV2 profile details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/createV2
	 *
	 * @return Response
	 */

	@SuppressWarnings("unchecked") // ok
	@POST
	@Path("/createV2")
	@Endpoint(id = "/profile/createV2#POST", isSingular = true, filterId = "dsw-createV2-response")
	public Response createV2(JSONObject pLoginRequest) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method createV2");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pLoginRequest);
		
		setRequestedTime();

		try {
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.PROFILE_FORMHANDLER_COMPONENT_NAME,
					DigitalProfileRestResourceContants.CREATE);
			getProfileResourceHelper().inputsForCommonCreateChain(executor, lRequestPayload);
			FormHandlerInvocationResult lResult = executor.execute();

			lResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
					getProfileResourceHelper().populateCreateV2Response(pLoginRequest, lResult));
		} catch (ServletException e) {
			vlogError(e, "ServletException");
		}
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method createV2");
		}

		return Response.ok(lResponseMap).build();
	}

	/**
	 * Service to user info V2 details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/getUserInfoV2
	 *
	 * @return Response
	 */

	@GET
	@Path("/getUserInfoV2")
	@Endpoint(id = "/profile/getUserInfoV2#GET", isSingular = true, filterId = "dsw-getUserInfoV2-response")
	public Response getUserInfoV2(@QueryParam(DigitalProfileRestResourceContants.SKIP_CERTS) Boolean pSkipCerts,
			@QueryParam(DigitalProfileRestResourceContants.SKIP_FEV_STORE) Boolean pSkipFavStore,
			@QueryParam(DigitalProfileRestResourceContants.SKIP_ORDERS) Boolean pSkipOrders,
			@QueryParam(DigitalProfileRestResourceContants.SYNC_REWARD) Boolean pSyncReward,
			@QueryParam(DigitalProfileRestResourceContants.SKIP_REWARD_DEATIL) Boolean pSkipRewardDetails) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getUserInfoV2");
		}
		setRequestedTime();
		Map<String, Object> lResponseMap = new HashMap<String, Object>();

		Map<String, Object> lUserInfoV2 = getProfileResourceHelper().populateGetUserInfoV2Response(pSkipCerts, pSkipFavStore, pSkipOrders,
				pSyncReward, pSkipRewardDetails);
		if (lUserInfoV2 != null) {
			if (lUserInfoV2.containsKey(DigitalJaxRsResourceCommonConstants.ERROR_SMALL)) {
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lUserInfoV2.get(DigitalJaxRsResourceCommonConstants.ERROR_SMALL));
			}
			if (lUserInfoV2.containsKey(DigitalJaxRsResourceCommonConstants.SUCCESS)) {
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lUserInfoV2.get(DigitalJaxRsResourceCommonConstants.SUCCESS));
			}
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getUserInfoV2");
		}
		setResponseHeaders();
		return Response.ok(lResponseMap).build();
	}

	/**
	 * Service to logout details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/logout
	 *
	 * @return Response
	 */

	@DELETE
	@Path("/logout")
	@Endpoint(id = "/profile/logout#DELETE", isSingular = true, filterId = "dsw-logout-response")
	public Response logout() {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method logout");
		}
		setRequestedTime();
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();

		try {
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.PROFILE_FORMHANDLER_COMPONENT_NAME,
					DigitalProfileRestResourceContants.LOGOUT);
			FormHandlerInvocationResult lResult = executor.execute();

			if (lResult.isFormError()) {
				lResponseMap = getProfileResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			}
		} catch (JSONException | ServletException e) {
			vlogError(e.toString(), "JSONException | ServletException ");

		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method logout ");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to summary details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/summary
	 *
	 * @return Response
	 */

	@GET
	@Path("/summary")
	@Endpoint(id = "/profile/summary#GET", isSingular = true, filterId = "dsw-summary-response")
	public Response summary() {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method summary");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();

		lFinalResponse.put(DigitalProfileRestResourceContants.PROFILE_RESPONSE, lProfile.getDataSource());

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method summary");
		}
		setResponseHeaders();
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to make default credit card details.
	 *
	 * @see - Actor Chain -
	 *      atg/userprofiling/ProfileActor/makeItAsDefaultCrditCard
	 *
	 * @return Response
	 */

	@PUT
	@Path("/makeItAsDefaultCreditCard")
	@Endpoint(id = "/profile/makeItAsDefaultCreditCard#PUT", isSingular = true, filterId = "dsw-makeItAsDefaultCrditCard-response")
	public Response makeItAsDefaultCreditCard(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method makeItAsDefaultCrditCard");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);
		if (isLoggingDebug()) {
			vlogDebug("Request payload is {0}", lRequestPayload);
		}
		String lCardId = (String) lRequestPayload.get(DigitalProfileRestResourceContants.CARD_ID);
		String lCardName = (String) lRequestPayload.get(DigitalProfileRestResourceContants.CARD_NAME);
		try {
			String lDefaultCrditCardResult = getProfileResourceHelper().makeItAsDefaultCreditCardResponse(lCardId, lCardName);
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lDefaultCrditCardResult);
		} catch (RepositoryException e) {
			vlogError(e, " RepositoryException ");
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method makeItAsDefaultCrditCard");
		}
		setResponseHeaders();
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to update profile details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/update
	 *
	 * @return Response
	 */

	@PUT
	@Path("/update")
	@Endpoint(id = "/profile/update#PUT", isSingular = true, filterId = "common-response-schema")
	public Response update(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method update");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);
		if (isLoggingDebug()) {
			vlogDebug("Request payload is {0}", lRequestPayload);
		}
		FormHandlerExecutor executor;
		try {
			executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.PROFILE_FORMHANDLER_COMPONENT_NAME,
					DigitalProfileRestResourceContants.UPDATE);
			getProfileResourceHelper().inputsForUpdateChain(executor, lRequestPayload);
			FormHandlerInvocationResult lResult = executor.execute();
			DigitalProfileFormHandler lProfieFormHandler = (DigitalProfileFormHandler) lResult.getFormHandler();
			if (lResult.isFormError()) {
				// Error
				if (lProfieFormHandler.getSuggestedAddressMap() != null) {
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.SUGGESTED_ADDRESSMAP, lProfieFormHandler.getSuggestedAddressMap());
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.DECISION, DigitalJaxRsResourceCommonConstants.SUGGESTED);
				} else {
					lResponseMap = getProfileResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
					lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
				}

			} else {
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.STATUS_CODE, DigitalJaxRsResourceCommonConstants.SUCCESS);
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.UPDATED_SUCCESS, lProfieFormHandler.getSuccessMessage());
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			}

		} catch (ServletException e) {
			vlogError(e, " ServletException ");
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method update");
		}
		setResponseHeaders();
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to update certs denomination details.
	 *
	 * @see - Actor Chain -
	 *      atg/userprofiling/ProfileActor/updateCertsDenomination
	 *
	 * @return Response
	 */

	@PUT
	@Path("/updateCertsDenomination")
	@Endpoint(id = "/profile/updateCertsDenomination#PUT", isSingular = true, filterId = "common-response-schema")
	public Response updateCertsDenomination(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method updateCertsDenomination");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);

		if (isLoggingDebug()) {
			vlogDebug("Request payload is {0}", lRequestPayload);
		}
		try {
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.PROFILE_FORMHANDLER_COMPONENT_NAME,
					DigitalProfileRestResourceContants.UPDATE_CERT_DENOMINATIONS);

			executor.addInput(DigitalProfileRestResourceContants.CERT_DENOMINATIONS,
					lRequestPayload.get(DigitalProfileRestResourceContants.CERT_DENOMINATIONS));

			executor.addInput(DigitalProfileRestResourceContants.PAPER_LESS_CART,
					lRequestPayload.get(DigitalProfileRestResourceContants.PAPER_LESS_CART));
			FormHandlerInvocationResult lResult = executor.execute();
			DigitalProfileFormHandler lProfieFormHandler = (DigitalProfileFormHandler) lResult.getFormHandler();

			if (lResult.isFormError()) {
				lResponseMap = getProfileResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			} else {
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.STATUS_CODE, DigitalJaxRsResourceCommonConstants.SUCCESS);
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.UPDATED_SUCCESS, lProfieFormHandler.getSuccessMessage());
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			}
		} catch (ServletException e) {
			vlogError(e, " ServletException ");
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method updateCertsDenomination");
		}
		setResponseHeaders();
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to get user credit card details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/getUserCreditCards
	 *
	 * @return Response
	 */

	@GET
	@Path("/getUserCreditCards")
	@Endpoint(id = "/profile/getUserCreditCards#GET", isSingular = true, filterId = "dsw-creditCards-schema")
	public Response getUserCreditCards(@QueryParam(DigitalProfileRestResourceContants.EXCLUDE_EXPIRED_CARDS) Boolean pExcludeExpiredCards) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getUserCreditCards");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();

		try {
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
					getProfileResourceHelper().getUserCreditCardsInfo(pExcludeExpiredCards));
		} catch (JSONException | ServletException e) {
			vlogError(e, " JSONException | ServletException ");
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getUserCreditCards");
		}
		setResponseHeaders();
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to get user favorite stores details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/getUserFavoriteStores
	 *
	 * @return Response
	 */

	@GET
	@Path("/getUserFavoriteStores")
	@Endpoint(id = "/profile/getUserFavoriteStores#GET", isSingular = true, filterId = "common-response-schema")
	public Response getUserFavoriteStores(@QueryParam(DigitalProfileRestResourceContants.LATITUDE) Double pLatitude,
			@QueryParam(DigitalProfileRestResourceContants.LONGNITUDE) Double pLongitude) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getUserFavoriteStores");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		try {
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
					getProfileResourceHelper().getUserFavoriteStoresInfo(pLatitude, pLongitude));
		} catch (RepositoryException e) {
			vlogError(e, " RepositoryException ");
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getUserFavoriteStores");
		}
		setResponseHeaders();
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to remove address details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/removeAddress
	 *
	 * @return Response
	 */

	@DELETE
	@Path("/removeAddress")
	@Endpoint(id = "/profile/removeAddress#DELETE", isSingular = true, filterId = "common-response-schema")
	public Response removeAddress(@QueryParam(DigitalProfileRestResourceContants.ADDRESS_ID) String pAddressId) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method removeAddress");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		try {
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, getProfileResourceHelper().removeAddressInfo(pAddressId));
		} catch (RepositoryException | DigitalIntegrationException e) {
			vlogError(e, " RepositoryException | DigitalIntegrationException  ");
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method removeAddress");
		}
		setResponseHeaders();
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to change password.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/changePasswordV2
	 *
	 * @return Response
	 */
	@PUT
	@Path("/changePasswordV2")
	@Endpoint(id = "/profile/changePasswordV2#PUT", isSingular = true, filterId = "common-response-schema")
	public Response changePasswordV2(JSONObject pLoginRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method changePasswordV2");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pLoginRequest);
		try {
			setRequestedTime();
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.PROFILE_FORMHANDLER_COMPONENT_NAME,
					DigitalProfileRestResourceContants.CHANGE_PASSWORD);
			getProfileResourceHelper().changePasswordV2Info(lRequestPayload, executor);
			FormHandlerInvocationResult lResult = executor.execute();
			DigitalProfileFormHandler lProfieFormHandler = (DigitalProfileFormHandler) lResult.getFormHandler();

			if (lResult.isFormError()) {
				// Error
				lResponseMap = getProfileResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			} else {
				String lSuccessMesg = lProfieFormHandler.getSuccessMessage();
				if (DigitalStringUtil.isNotEmpty(lSuccessMesg)) {
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.CHANGE_PASSWORD_SUCCESS, lSuccessMesg);
					lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
				}
			}
		} catch (Exception e) {
			vlogError(e.toString(), " JSONException | ServletException | RepositoryException | PropertyNotFoundException ");

		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method changePasswordV2");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to remove payment details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/removePayment
	 *
	 * @return Response
	 */

	@DELETE
	@Path("/removePayment")
	@Endpoint(id = "/profile/removePayment#DELETE", isSingular = true, filterId = "common-response-schema")
	public Response removePayment(@QueryParam(DigitalProfileRestResourceContants.CARD_NAME) String pCardName) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method removePayment");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		try {
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, getProfileResourceHelper().removePaymentInfo(pCardName));
		} catch (RepositoryException e) {
			vlogError(e, " RepositoryException ");
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method removePayment");
		}
		setResponseHeaders();
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to update other details details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/updateOtherAddress
	 *
	 * @return Response
	 */

	@PUT
	@Path("/updateOtherAddress")
	@Endpoint(id = "/profile/updateOtherAddress#PUT", isSingular = true, filterId = "common-response-schema")
	public Response updateOtherAddress(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method updateOtherAddress");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);

		if (isLoggingDebug()) {
			vlogDebug("Request payload is {0}", lRequestPayload);
		}
		try {
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, getProfileResourceHelper().updateOtherAddressDetails(lRequestPayload));
		} catch (RepositoryException | IntrospectionException | InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			vlogError(e,
					" RepositoryException | IntrospectionException | InstantiationException | IllegalAccessException | ClassNotFoundException ");
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method updateOtherAddress");
		}
		setResponseHeaders();
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to update user Favorite store details.
	 *
	 * @see - Actor Chain -
	 *      atg/userprofiling/ProfileActor/updateUserFavoriteStore
	 *
	 * @return Response
	 */

	@PUT
	@Path("/updateUserFavoriteStore")
	@Endpoint(id = "/profile/updateUserFavoriteStore#PUT", isSingular = true, filterId = "common-response-schema")
	public Response updateUserFavoriteStore(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method updateUserFavoriteStore");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);

		if (isLoggingDebug()) {
			vlogDebug("Request payload is {0}", lRequestPayload);
		}
		try {
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
					getProfileResourceHelper().updateUserFavoriteStoreInfo(lRequestPayload));
		} catch (RepositoryException e) {
			vlogError(e, " RepositoryException ");
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method updateUserFavoriteStore");
		}
		setResponseHeaders();
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to resetPasswordV2 details.
	 *
	 * @see - Actor Chain -atg/userprofiling/ProfileActor/resetPasswordV2
	 *
	 * @return Response
	 */

	@POST
	@Path("/resetPasswordV2")
	@Endpoint(id = "/profile/resetPasswordV2#POST", isSingular = true, filterId = "dsw-login-response")
	public Response resetPasswordV2(JSONObject pLoginRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method resetPasswordV2");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pLoginRequest);
		try {
			setRequestedTime();
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.FORGOT_PASSWORD_HANDLER_COMPONENT_NAME,
					DigitalProfileRestResourceContants.FORGOT_PASSWORD);

			executor.addInput(DigitalProfileRestResourceContants.LOGIN_VALUE, lRequestPayload.get(DigitalProfileRestResourceContants.EMAIL));
			executor.addInput(DigitalProfileRestResourceContants.VERSION, lRequestPayload.get(DigitalProfileRestResourceContants.TWO));

			FormHandlerInvocationResult lResult = executor.execute();

			if (lResult.isFormError()) {
				// Error
				lResponseMap = getProfileResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			} else {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, DigitalJaxRsResourceCommonConstants.SUCCESS);
			}
		} catch (Exception e) {
			vlogError(e.toString(), " JSONException | ServletException | RepositoryException | PropertyNotFoundException");

		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method resetPasswordV2");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to ivrResetPassword details.
	 *
	 * @see - Actor Chain -atg/userprofiling/ProfileActor/ivrResetPassword
	 *
	 * @return Response
	 */

	@POST
	@Path("/ivrResetPassword")
	@Endpoint(id = "/profile/ivrResetPassword#POST", isSingular = true, filterId = "common-response-schema")
	public Response ivrResetPassword(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method ivrResetPassword");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);

		setRequestedTime();
		try {
			Map<String, Object> lTokenResponse = getAuthHelper().validateJWTTokenResponse();

			if (!lTokenResponse.containsKey(DigitalJaxRsResourceCommonConstants.STATUS) ) {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lTokenResponse);
			} else {
				FormHandlerExecutor executor;

				executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.FORGOT_PASSWORD_HANDLER_COMPONENT_NAME,
						DigitalProfileRestResourceContants.FORGOT_PASSWORD);

				executor.addInput(DigitalProfileRestResourceContants.LOGIN_VALUE, lRequestPayload.get(DigitalProfileRestResourceContants.EMAIL));
				executor.addInput(DigitalProfileRestResourceContants.VERSION, lRequestPayload.get(DigitalProfileRestResourceContants.TWO));

				FormHandlerInvocationResult lResult = executor.execute();

				if (lResult.isFormError()) {
					// Error
					lResponseMap = getProfileResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
					lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
				} else {
					lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, DigitalJaxRsResourceCommonConstants.SUCCESS);
				}

			}
		} catch (ServletException e) {
			vlogError(e.toString(), " ServletException ");
		}
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method ivrResetPassword");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to createStoreEnrollment details.
	 *
	 * @see - Actor Chain -atg/userprofiling/ProfileActor/createStoreEnrollment
	 *
	 * @return Response
	 */

	@POST
	@Path("/createStoreEnrollment")
	@Endpoint(id = "/profile/createStoreEnrollment#POST", isSingular = true, filterId = "common-response-schema")
	public Response createStoreEnrollment(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method createStoreEnrollment");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);

		setRequestedTime();
		try {
			Map<String, Object> lTokenResponse = getAuthHelper().validateJWTTokenResponse();

			if (lTokenResponse.containsKey(DigitalJaxRsResourceCommonConstants.STATUS)) {

				FormHandlerExecutor executor;

				executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.PROFILE_FORMHANDLER_COMPONENT_NAME,
						DigitalProfileRestResourceContants.CREATE);

				executor.addInput(DigitalProfileRestResourceContants.VALUE_FIRSTNAME, lRequestPayload.get(DigitalProfileRestResourceContants.FIRSTNAME));
				executor.addInput(DigitalProfileRestResourceContants.VALUE_LASTNAME, lRequestPayload.get(DigitalProfileRestResourceContants.LASTNAME));
				executor.addInput(DigitalProfileRestResourceContants.LOGIN_VALUE, lRequestPayload.get(DigitalProfileRestResourceContants.LOGIN));
				executor.addInput(DigitalProfileRestResourceContants.SOURCE, lRequestPayload.get(DigitalProfileRestResourceContants.SOURCE));
				executor.addInput(DigitalProfileRestResourceContants.LOYALTY_NUMBER, lRequestPayload.get(DigitalProfileRestResourceContants.LOYALTY_NUMBER));
				executor.addInput(DigitalProfileRestResourceContants.SEND_EMAIL_CREATE_ACCOUNT, lRequestPayload.get(DigitalProfileRestResourceContants.FALSE));

				FormHandlerInvocationResult lResult = executor.execute();
				DigitalProfileFormHandler lProfileFormHandler = (DigitalProfileFormHandler) lResult.getFormHandler();
				Map<String, String> lSuggestedAddressMap = lProfileFormHandler.getSuggestedAddressMap();
				if (lResult.isFormError()) {
					// Error
					if (lSuggestedAddressMap.containsKey(DigitalJaxRsResourceCommonConstants.SUGGESTED_ADDRESSMAP)) {
						lResponseMap.put(DigitalJaxRsResourceCommonConstants.SUGGESTED_ADDRESSMAP, lSuggestedAddressMap);
						lResponseMap.put(DigitalJaxRsResourceCommonConstants.DECISION, DigitalJaxRsResourceCommonConstants.SUGGESTED);
					}
					lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
				} else {
					lResponseMap.put(DigitalProfileRestResourceContants.ONLINE_PROFILE_ID, lProfileFormHandler.getRepositoryId());
					lResponseMap.put(DigitalProfileRestResourceContants.TRANSACTION_ID, lProfileFormHandler.getTransactionId());
					lResponseMap.put(DigitalProfileRestResourceContants.STATUS_CODE, DigitalJaxRsResourceCommonConstants.SUCCESS);
					lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
				}

			} else {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lTokenResponse);
			}
		} catch (ServletException e) {
			vlogError(e.toString(), " ServletException ");
		}
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method createStoreEnrollment");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to getLoginbyProfileID details.
	 *
	 * @see  Actor Chain - atg/userprofiling/ProfileActor/getLoginbyProfileID
	 *
	 * @return Response
	 */

	@POST
	@Path("/getLoginbyProfileID")
	@Endpoint(id = "/profile/getLoginbyProfileID#POST", isSingular = true, filterId = "common-response-schema")
	public Response getLoginbyProfileID(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method getLoginbyProfileID");
		}
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);

		setRequestedTime();
		try {
			String lProfileId = (String) lRequestPayload.get(DigitalProfileRestResourceContants.PROFILE_ID);
			Map<String, Object> lTokenResponse = getAuthHelper().validateJWTTokenResponse();

			if (lTokenResponse.containsKey(DigitalJaxRsResourceCommonConstants.STATUS)) {
				lFinalResponse.put(DigitalProfileRestResourceContants.LOGIN, getProfileResourceHelper().getLoginbyProfileIDInfo(lProfileId));

			} else {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lTokenResponse);
			}
		} catch (RepositoryException e) {
			vlogError(e.toString(), " ServletException | RepositoryException ");
		}
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getLoginbyProfileID");
		}
		return Response.ok(lFinalResponse).build();
	}
	/**
	 * Service to get user details.
	 *
	 * @see - Actor Chain - atg/userprofiling/ProfileActor/getUserDetails
	 *
	 * @return Response
	 */

	@GET
	@Path("/getUserDetails")
	@Endpoint(id = "/profile/getUserDetails#GET", isSingular = true, filterId = "dsw-UserDetails-schema")
	public Response getUserDetails(@QueryParam(DigitalProfileRestResourceContants.EXCLUDE_EXPIRED_CARDS) Boolean pExcludeExpiredCards,
			@QueryParam(DigitalProfileRestResourceContants.CHECKOUT) Boolean pCheckout,@QueryParam(DigitalProfileRestResourceContants.LATITUDE) Double pLatitude,
			@QueryParam(DigitalProfileRestResourceContants.LONGNITUDE) Double pLongitude) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getUserDetails");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();

		Boolean lCheckout = pCheckout == null ? false : pCheckout;
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		try {
			Map<String, Object> lUserDetailsMap = getProfileResourceHelper().getUserDetailsInfo(lProfile,lCheckout,pExcludeExpiredCards,pLatitude,pLongitude);
			if (isLoggingDebug()) {
				logDebug("lUserDetailsMap" + lUserDetailsMap);
			}
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,lUserDetailsMap);
		} catch (RepositoryException | JSONException | ServletException e) {
			vlogError(e.toString(), " ServletException | RepositoryException | JSONException ");
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getUserDetails");
		}
		setResponseHeaders();

		return Response.ok(lFinalResponse).build();
	}

	/**
	 * This service will create new credit card.
	 * 
	 * @see - /rest/model/atg/commerce/order/purchase/CreateCreditCardActor/newCreditCard
	 * @param pRequest
	 * @return Response
	 */
	@POST
	@Path("/newCreditCard")
	@Endpoint(id = "/profile/newCreditCard#POST", isSingular = true, filterId = "dsw-new-creditCards-schema")
	public Response newCreditCard(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method newCreditCard");
		}
		Map<String, Object> lResponseMap = new HashMap<>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequest);

		setRequestedTime();
		try{
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalProfileRestResourceContants.CREATE_CREDIT_CRD_FORM_HANDLER,
					DigitalProfileRestResourceContants.HANDLE_ADD_NEW_CREDIT_CRD);
			getProfileResourceHelper().addInputForNewCreditCard(executor, lRequestMap);
			
			FormHandlerInvocationResult lResult = executor.execute();
			
			if (lResult.isFormError()) {
				lResponseMap = getProfileResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
			}else{
				Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
				Map<String,RepositoryItem> usersCreditCardMap = getProfileResourceHelper().getProfileTools().getActiveCreditCardsDetails(lProfile);
				RepositoryItem lDefaultCreditCard = getProfileResourceHelper().getProfileTools().getDefaultCreditCard(lProfile);
				if(lDefaultCreditCard != null){
					lResponseMap.put(DigitalProfileRestResourceContants.DEFAULT_CREDIT_CRD_ID, lDefaultCreditCard.getRepositoryId());
				}
				if(lDefaultCreditCard != null){
					lResponseMap.put( DigitalProfileRestResourceContants.CREDIT_CARDS, usersCreditCardMap);
				}
			}
			
		}catch (ServletException e) {
			vlogError(e, "CommerceException");
		}
		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method newCreditCard");
		}
		return Response.ok(lFinalResponse).build();
	}
	
}
