/**
 * 
 */
package com.digital.commerce.jaxrs.common.constants;

/**
 * Class stating constants used in DigitalProductCatalogRestResource
 * 
 * @author TAIS
 *
 */
public class DigitalCatalogConstants {

	public static final String DSW_PRODUCT_LOOKUP_DROPLET = "/com/digital/commerce/services/catalog/DigitalProductLookupDroplet";

	public static final String DSW_INVENTORY_LOOKUP_DROPLET = "/com/digital/commerce/services/inventory/DigitalPDPInventoryLookup";

	public static final Boolean TRUE = true;

	public static final Boolean FALSE = false;

	public static final String ERROR = "error";

	public static final String FILTER_BY_SITE = "filterBySite";

	public static final String FILTER_BY_CATALOG = "filterByCatalog";

	public static final String LOCATION_ID = "locationId";

	public static final String SKU_ID = "skuId";

	public static final String FILTER_OUT_OF_STOCK = "filterOutOfStock";

	public static final String POSTAL_CODE = "postalCode";

	public static final String CITY = "city";

	public static final String STATE = "state";

	public static final String LATITUDE = "latitude";

	public static final String LONGITUDE = "longitude";

	public static final String LOCATE_ITEMS_INVENTORY = "locateItemsInventory";

	public static final String STORE_INVENTORY = "SKUAndProductInventoryOfStore";

	public static final String INVENTORY_INFO = "inventoryInfo";

	public static final String INVENTORY_DROPLET_PARAM = "inventoryDropletParam";

	public static final String PRODUCT_STORE_INVENTORY = "productStoreInventory";

	public static final String SERVICES_RESOURCE_BUNDLE = "com.digital.commerce.services.ServicesResource";

	public static final String USER_RESOURCE_BUNDLE = "atg.commerce.catalog.custom.UserResources";

	public static final String EMPTY_PARAMETER = "PARAMETER_EMPTY";

	public static final String EMPTY_PRODUCTID = "PRODUCT_ID_EMPTY";

	public static final String PRODUCT_LOOKUP_DROPLET = "/atg/commerce/catalog/ProductLookup";

	public static final String INVALID_PRODUCTID = "INVALID_PRODUCT_ID";

	public static final String PRODUCT_NOT_IN_CURRENT_SITE = "PRODUCT_NOT_IN_CURRENT_SITE";

	public static final String PRODUCT_NOT_IN_CURRENT_CATALOG = "PRODUCT_NOT_IN_CURRENT_CATALOG";

	public static final String EMPTY = "empty";

	public static final String WRONG_SITE = "wrongSite";

	public static final String WRONG_CATALOG = "wrongCatalog";

	public static final String NO_CATALOG = "noCatalog";

	public static final String PRODUCT_BROWSED_DROPLET = "/atg/commerce/catalog/ProductBrowsed";

	public static final String EVENT_OBJECT = "eventobject";

	public static final String CURRENCY_CODE_DROPLET = "/atg/commerce/pricing/CurrencyCodeDroplet";

	public static final String CURRENCY_CODE = "currencyCode";

	public static final String PRODUCT_IN_FAVORITES = "isProductInFavorites";

	public static final String GIFTLIST_LOOKUP_DROPLET = "/atg/commerce/gifts/GiftlistLookupDroplet";

	public static final String GIFTLIST_SITE_FILTER_DROPLET = "/atg/commerce/collections/filter/droplet/GiftlistSiteFilterDroplet";

	public static final String COLLECTION = "collection";

	public static final String GIFTLIST_ITEMS = "giftlistItems";

	public static final String FILTERED_COLLECTION = "filteredCollection";

	public static final String NAVIGATION_URL_DROPLET = "/com/digital/commerce/services/utils/DigitalPDPNavigationUrlLookup";

	public static final String DIMENSION_NAME = "dimensionName";

	public static final String BRAND = "brand";

	public static final String REFINEMENT_NAME = "refinementName";

	public static final String NAV_URL = "navStringUrl";

	public static final String DSW_BRAND = "dswBrand";

	public static final String PRICE_IN_CART = "priceInCart";

	public static final String NON_MEMBER_MIN_PRICE = "nonMemberMinPrice";

	public static final String NON_MEMBER_MAX_PRICE = "nonMemberMaxPrice";

	public static final String NON_MEMBER_MSRP = "nonMemberMSRP";

	public static final String DEFAULT_SKU = "defaultSKU";

	public static final String NON_MEMBER_PRICE = "nonMemberPrice";

	public static final String PRICE_VALUE = "0.0";

	public static final String CHILD_SKUS = "childSKUs";

	public static final String ORIGINAL_PRICE = "originalPrice";

	public static final String LIST_PRICE = "listPrice";

	public static final String SALE_PRICE = "salePrice";

	public static final String HIGHEST_SALE_PRICE = "highestSalePrice";

	public static final String LOWEST_SALE_PRICE = "lowestSalePrice";

	public static final String PRICE_RANGE_DROPLET = "/atg/commerce/pricing/PriceRangeDroplet";

	public static final String LOWEST_PRICE = "lowestPrice";

	public static final String HIGHEST_PRICE = "highestPrice";

	public static final String PRICE_DROPLET = "/atg/commerce/pricing/priceLists/PriceDroplet";

	public static final String PRICE_LISTPRICE = "price.listPrice";

	public static final String PRICE_LIST = "priceList";

	public static final String SALE_PRICE_LIST = "salePriceList";

	public static final String SKU_LOOKUP = "/atg/commerce/catalog/SKULookup";

	public static final String ELEMENT_LISTPRICE = "element.listPrice";

	public static final String ELEMENT_SALEPRICE = "element.salePrice";

	public static final String PRODUCT_DETAIL_PROPERTIES[] = { "longDescription", "displayName", "description", "occasion", "toeShape",
			"displayCompareAtPrice", "isActive", "defaultColorCode", "isClearance", "showSize", "dswBrand", "showWidth", "productitemtype",
			"spinColorCode", "hasAnimatedImage", "productTypeWeb", "productStockLevel", "newProductText", "productGender", "style",
			"repositoryId", "isKidsProduct", "nonreturnable", "onlineOnly", "type", "widths", "isGiftWrappable",
			"isSignatureRequiredProduct", "productTitle", "hazmat", "isReturnableToStoreProduct", "isExcludeFromWishList",
			"productTypeInUse", "isLuxuryProduct", "inStock", "sizes", "alwaysShowSizeAndWidth", "productType", "materials", "siteIds",
			"construction", "dateAvailable", "reviewCount", "depId", "availableColors", "isWhiteGloveEligibleProduct", "businessUnit",
			"rating", "version", "season", "trueCountryOfOrigin", "lastModified", "taxCategory", "countryOfOrigin", "frameNumber",
			"vendorStyleNumber", "creationDate", "daysAvailable", "discountable", "subclassId", "isTaxExempt", "keywords", "toprated",
			"feature", "bestseller", "detailList", "nonMemberMSRP", "nonMemberMinPrice", "nonMemberMaxPrice", "bullets", "priceInCart",
			"ancestorCategories", "parentCategories", "defaultSKU", "childSKUs" };//

	public static final String PRODUCT_PROPERTIES[] = { "dswBrand", "repositoryId", "displayName", "description", "longDescription",
			"productTitle", "frameNumber", "nonMemberMinPrice", "nonMemberMaxPrice", "nonMemberMSRP", "newProductText", "priceInCart",
			"defaultColorCode", "availableColors", "inStock", "sizes", "showSize", "alwaysShowSizeAndWidth", "widths", "showWidth",
			"detailList", "isClearance", "productStockLevel", "daysAvailable", "creationDate", "bullets", "productTypeWeb", "toeShape",
			"heelHeight", "spinColorCode", "isActive", "hasAnimatedImage", "displayCompareAtPrice", "rating", "parentCategories",
			"ancestorCategories", "defaultSKU" };

	public static final String[] CATEGORY_PROPERTIES = { "displayName", "id", "description", "type", "defaultParentCategory" };

	public static final String[] SKU_PROPERTIES = { "id", "isClearanceItem", "nonMemberPrice", "color", "dimension", "materials",
			"originalPrice", "msrp", "size", "skuStockLevel", "upc", "isExclusiveLicense" };

	public static final String CATEGORY = "category";

}