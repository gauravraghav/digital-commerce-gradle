package com.digital.commerce.jaxrs.catalog.helper;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.json.JSONException;

import com.digital.commerce.jaxrs.catalog.vo.StoreInventoryParameterBean;
import com.digital.commerce.jaxrs.common.constants.DigitalCatalogConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalProfileRestResourceContants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.services.pricing.DigitalPricingTools;
import com.digital.commerce.services.profile.DigitalProfileTools;

import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.StringList;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.jaxrs.DropletInvoker;
import atg.service.jaxrs.OParam;
import atg.service.jaxrs.Output;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

/**
 * Helper class created for DigitalProductCatalogRestResourceHelper
 *
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalProductCatalogRestResourceHelper extends GenericService {

    /**
     * serviceResourceBundle - ServicesResource component holding static keys
     */
    final ResourceBundle serviceResourceBundle = LayeredResourceBundle.getBundle(DigitalCatalogConstants.SERVICES_RESOURCE_BUNDLE,
	    ServletUtil.getUserLocale());

    /**
     * userResourceBundle - UserResources component holding static keys
     */
    final ResourceBundle userResourceBundle = LayeredResourceBundle.getBundle(DigitalCatalogConstants.USER_RESOURCE_BUNDLE,
	    ServletUtil.getUserLocale());

    /**
     * profileTools - DigitalProfileTools component
     */
    private DigitalProfileTools profileTools;

    /**
     * productInFavorites - boolean property
     *
     * used in digitalOutputProductSummary to set value of the key 'productInFavorites' for each childSKU for a product
     *
     */
    private boolean productInFavorites = false;

    /**
     * pricingTools - DigitalPricingTools component
     */
    private DigitalPricingTools pricingTools;

    /**
     * Method to fetch the product response from DigitalProductLookupDroplet and return the product properties to DigitalProductCatalogRestResource
     *
     * @param pProductId
     *            This holds the productId of the product to be fetched
     * @return Map<String, Object>
     */
    public Map<String, Object> getDSWGetProductV2Response(String pProductId) {

	if (isLoggingDebug()) {
	    logDebug("Entering into method DigitalProductCatalogRestResourceHelper.getDSWGetProductV2Response : Product ID received : "
		    + pProductId);
	}

	Map<String, Object> response = new HashMap<String, Object>();

	DropletInvoker dropletInvoker = new DropletInvoker(DigitalCatalogConstants.DSW_PRODUCT_LOOKUP_DROPLET);

	dropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.ID, pProductId);
	dropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.REPOSITORY_KEY, ServletUtil.getUserLocale());
	dropletInvoker.addInput(DigitalCatalogConstants.FILTER_BY_CATALOG, DigitalCatalogConstants.FALSE);
	dropletInvoker.addInput(DigitalCatalogConstants.FILTER_BY_SITE, DigitalCatalogConstants.FALSE);

	OParam oParamOutput = dropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
	Output result = oParamOutput.addOutput(DigitalJaxRsResourceCommonConstants.PRODUCT, DigitalJaxRsResourceCommonConstants.ELEMENT);

	OParam oParamError = dropletInvoker.addOParam(DigitalCatalogConstants.ERROR);
	Output error = oParamError.addOutput(DigitalCatalogConstants.ERROR, DigitalJaxRsResourceCommonConstants.ELEMENT);

	dropletInvoker.invoke();

	if (result.getObject() != null) {
	    response.put(DigitalJaxRsResourceCommonConstants.PRODUCT, result.getObject());
	} else {
	    response.put(DigitalCatalogConstants.ERROR, error.getObject());
	}

	if (isLoggingDebug()) {
	    logDebug("Exiting method DigitalProductCatalogRestResourceHelper.getDSWGetProductV2Response : Response returned : " + response);
	}

	return response;

    }

    /**
     * Method to fetch the store inventory response from DigitalPDPInventoryLookup droplet and return the inventory details to
     * DigitalProductCatalogRestResource
     *
     * @param paramBean
     *            This holds the query parameters fetched from StoreInventoryParameterBean
     * @return Map<String, Object>
     */
    public Map<String, Object> getDSWStoreInventoryResponse(StoreInventoryParameterBean paramBean) {

	if (isLoggingDebug()) {
	    logDebug("Entering into method DigitalProductCatalogRestResourceHelper.getDSWStoreInventoryResponse : Parameters received : "
		    + paramBean.toString());
	}

	Map<String, Object> response = new HashMap<String, Object>();

	DropletInvoker dropletInvoker = new DropletInvoker(DigitalCatalogConstants.DSW_INVENTORY_LOOKUP_DROPLET);

	dropletInvoker.addInput(DigitalCatalogConstants.LOCATION_ID, paramBean.getLocationId());
	dropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.PRODUCT_ID, paramBean.getProductId());
	dropletInvoker.addInput(DigitalCatalogConstants.SKU_ID, paramBean.getSkuId());
	dropletInvoker.addInput(DigitalCatalogConstants.FILTER_OUT_OF_STOCK, paramBean.getFilterOutOfStock());
	dropletInvoker.addInput(DigitalCatalogConstants.POSTAL_CODE, paramBean.getPostalCode());
	dropletInvoker.addInput(DigitalCatalogConstants.CITY, paramBean.getCity());
	dropletInvoker.addInput(DigitalCatalogConstants.STATE, paramBean.getState());
	dropletInvoker.addInput(DigitalCatalogConstants.LATITUDE, paramBean.getLatitude());
	dropletInvoker.addInput(DigitalCatalogConstants.LONGITUDE, paramBean.getLongitude());
	dropletInvoker.addInput(DigitalCatalogConstants.LOCATE_ITEMS_INVENTORY, paramBean.getLocateItemsInventory());

	OParam oParamOutput = dropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
	Output storeInventory = oParamOutput.addOutput(DigitalCatalogConstants.STORE_INVENTORY, DigitalCatalogConstants.STORE_INVENTORY);
	Output inventoryInfo = oParamOutput.addOutput(DigitalCatalogConstants.INVENTORY_INFO, DigitalCatalogConstants.INVENTORY_INFO);
	Output error = oParamOutput.addOutput(DigitalCatalogConstants.ERROR, DigitalCatalogConstants.ERROR);

	dropletInvoker.invoke();

	if (storeInventory.getObject() == null && inventoryInfo.getObject() == null && error.getObject() == null) {
	    response.put("", "");
	} else {
	    if (error.getObject() != null) {
		response.put(DigitalCatalogConstants.ERROR, error.getObject());
	    } else if (inventoryInfo.getObject() != null) {
		response.put(DigitalCatalogConstants.INVENTORY_DROPLET_PARAM, inventoryInfo.getObject());
	    } else {
		response.put(DigitalCatalogConstants.PRODUCT_STORE_INVENTORY, storeInventory.getObject());
	    }
	}

	if (isLoggingDebug()) {
	    logDebug("Exiting method DigitalProductCatalogRestResourceHelper.getDSWStoreInventoryResponse : Response returned : " + response);
	}

	return response;

    }

    /**
     * Method to fetch the product details response from ProductLookup droplet and return the product details to
     * DigitalProductCatalogRestResource
     *
     * @param pProductId
     *            Holds the productId of the product to be fetched
     * @param pFilterBySite
     *            Holds the value of filterBySite query parameter
     * @param pFilterByCatalog
     *            Holds the value of filterByCatalog query parameter
     * @return Map<String, Object>
     */
    public Map<String, Object> getDSWGetProductDetailsResponse(String pProductId, String pFilterBySite, String pFilterByCatalog) {

	if (isLoggingDebug()) {
	    logDebug("Entering into method DigitalProductCatalogRestResourceHelper.getDSWGetProductDetailsResponse : Product ID received : "
		    + pProductId);
	}

	Map<String, Object> response = new HashMap<String, Object>();

	if (pProductId == null) {
	    response.put(DigitalCatalogConstants.ERROR, serviceResourceBundle.getString(DigitalCatalogConstants.EMPTY_PARAMETER));
	} else {
	    if (StringUtils.isEmpty(pProductId)) {
		response.put(DigitalCatalogConstants.ERROR, serviceResourceBundle.getString(DigitalCatalogConstants.EMPTY_PRODUCTID));
	    } else {
		DropletInvoker dropletInvoker = new DropletInvoker(DigitalCatalogConstants.PRODUCT_LOOKUP_DROPLET);

		dropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.REPOSITORY_KEY, ServletUtil.getUserLocale());
		dropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.ID, pProductId);
		dropletInvoker.addInput(DigitalCatalogConstants.FILTER_BY_SITE, pFilterBySite);
		dropletInvoker.addInput(DigitalCatalogConstants.FILTER_BY_CATALOG, pFilterByCatalog);

		OParam oParamOutput = dropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
		Output dropletOutput = oParamOutput.addOutput(DigitalJaxRsResourceCommonConstants.OUTPUT,
			DigitalJaxRsResourceCommonConstants.ELEMENT);

		OParam oParamNoCatalog = dropletInvoker.addOParam(DigitalCatalogConstants.NO_CATALOG);
		Output noCatalog = oParamNoCatalog.addOutput(DigitalCatalogConstants.NO_CATALOG, DigitalJaxRsResourceCommonConstants.ELEMENT);

		dropletInvoker.invoke();

		if (dropletInvoker.getOParam(DigitalCatalogConstants.EMPTY) != null) {
		    response.put(DigitalCatalogConstants.ERROR,
			    MessageFormat.format(serviceResourceBundle.getString(DigitalCatalogConstants.INVALID_PRODUCTID), pProductId));
		}

		if (dropletInvoker.getOParam(DigitalCatalogConstants.WRONG_SITE) != null) {
		    response.put(DigitalCatalogConstants.ERROR, MessageFormat
			    .format(serviceResourceBundle.getString(DigitalCatalogConstants.PRODUCT_NOT_IN_CURRENT_SITE), pProductId));
		}

		if (dropletInvoker.getOParam(DigitalCatalogConstants.WRONG_CATALOG) != null) {
		    response.put(DigitalCatalogConstants.ERROR, MessageFormat
			    .format(serviceResourceBundle.getString(DigitalCatalogConstants.PRODUCT_NOT_IN_CURRENT_CATALOG), pProductId));
		}

		if (noCatalog.getObject() != null) {
		    Map<String, Object> productMap = new HashMap<String, Object>();
		    Map<String, StringList> filter = new HashMap<String, StringList>();
		    RepositoryItem output = (RepositoryItem) noCatalog.getObject();
		    try {
			StringList filterArguments = new StringList();
			filterArguments.setStrings(DigitalCatalogConstants.PRODUCT_DETAIL_PROPERTIES);
			filter.put(output.getItemDescriptor().getItemDescriptorName(), filterArguments);

			StringList categoryFilters = new StringList();
			categoryFilters.setStrings(DigitalCatalogConstants.CATEGORY_PROPERTIES);
			filter.put(DigitalCatalogConstants.CATEGORY, categoryFilters);

			StringList skuFilters = new StringList();
			skuFilters.setStrings(DigitalCatalogConstants.SKU_PROPERTIES);
			filter.put(DigitalJaxRsResourceCommonConstants.SKU, skuFilters);

			productMap = DigitalJaxRsUtility.buildRepositoryMap((RepositoryItem) noCatalog.getObject(), filter);
		    } catch (JSONException e) {
			logError("JSON Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductDetailsResponse :: " + e);
		    } catch (RepositoryException e) {
			logError("Repository Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductDetailsResponse :: "
				+ e);
		    } catch (Exception e) {
			logError("Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductDetailsResponse :: " + e);
		    }

		    response.put(DigitalJaxRsResourceCommonConstants.PRODUCT,
			    dswOutputProduct(productMap, pFilterBySite, noCatalog.getObject()));
		}

		if (dropletOutput != null) {
		    Map<String, Object> productMap = new HashMap<String, Object>();
		    Map<String, StringList> filter = new HashMap<String, StringList>();
		    RepositoryItem output = (RepositoryItem) dropletOutput.getObject();
		    try {
			StringList filterArguments = new StringList();
			filterArguments.setStrings(DigitalCatalogConstants.PRODUCT_DETAIL_PROPERTIES);
			filter.put(output.getItemDescriptor().getItemDescriptorName(), filterArguments);

			StringList categoryFilters = new StringList();
			categoryFilters.setStrings(DigitalCatalogConstants.CATEGORY_PROPERTIES);
			filter.put(DigitalCatalogConstants.CATEGORY, categoryFilters);

			StringList skuFilters = new StringList();
			skuFilters.setStrings(DigitalCatalogConstants.SKU_PROPERTIES);
			filter.put(DigitalJaxRsResourceCommonConstants.SKU, skuFilters);

			productMap = DigitalJaxRsUtility.buildRepositoryMap((RepositoryItem) dropletOutput.getObject(),
				filter);
		    } catch (JSONException e) {
			logError("JSON Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductDetailsResponse :: " + e);
		    } catch (RepositoryException e) {
			logError("Repository Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductDetailsResponse :: "
				+ e);
		    } catch (Exception e) {
			logError("Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductDetailsResponse :: " + e);
		    }

		    response.put(DigitalJaxRsResourceCommonConstants.PRODUCT,
			    dswOutputProduct(productMap, pFilterBySite, dropletOutput.getObject()));
		}

	    }

	}

	if (isLoggingDebug()) {
	    logDebug(
		    "Exiting method DigitalProductCatalogRestResourceHelper.getDSWGetProductDetailsResponse : Response returned : " + response);
	}

	return response;

    }

    /**
     * Method to fetch the product details and return the product to getDSWGetProductDetailsResponse method
     *
     * @param pProductMap
     *            Holds the product properties
     * @param pFilterBySite
     *            Holds the value of filterBySite query parameter
     * @param pProduct
     *            Holds the product fetched from lookup
     * @return Map<String, Object>
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> dswOutputProduct(Map<String, Object> pProductMap, String pFilterBySite, Object pProduct) {

	if (isLoggingDebug()) {
	    logDebug("Entering into method DigitalProductCatalogRestResourceHelper.dswOutputProduct : Product received : "
		    + pProductMap.toString());
	}

	Map<String, Object> prodDetails = new HashMap<String, Object>();
	Map<String, Object> product = new HashMap<String, Object>();

	if (pProduct != null) {

	    RepositoryItem prod = (RepositoryItem) pProduct;

	    DropletInvoker dropletInvoker = new DropletInvoker(DigitalCatalogConstants.PRODUCT_BROWSED_DROPLET);
	    dropletInvoker.addInput(DigitalCatalogConstants.EVENT_OBJECT, pProduct);
	    dropletInvoker.invoke();

	    prodDetails = dswOutputProductSummary(pProductMap, pProduct);

	    product.putAll((Map<String, Object>) prodDetails.get(DigitalJaxRsResourceCommonConstants.PRODUCT));

	    if (product.get(DigitalCatalogConstants.PRICE_IN_CART) != null && (boolean) product.get(DigitalCatalogConstants.PRICE_IN_CART)) {

		if (isLoggingDebug()) {
		    logDebug("DigitalProductCatalogRestResourceHelper.dswOutputProduct :: product.priceInCart :: true");
		}

		product.put(DigitalCatalogConstants.NON_MEMBER_MIN_PRICE, DigitalCatalogConstants.PRICE_VALUE);
		product.put(DigitalCatalogConstants.NON_MEMBER_MAX_PRICE, DigitalCatalogConstants.PRICE_VALUE);
		product.put(DigitalCatalogConstants.NON_MEMBER_MSRP, DigitalCatalogConstants.PRICE_VALUE);

		Map<String, Object> defaultSKU = new HashMap<String, Object>();

		defaultSKU.put(DigitalCatalogConstants.NON_MEMBER_MIN_PRICE, DigitalCatalogConstants.PRICE_VALUE);
		defaultSKU.put(DigitalCatalogConstants.NON_MEMBER_MAX_PRICE, DigitalCatalogConstants.PRICE_VALUE);
		defaultSKU.put(DigitalCatalogConstants.NON_MEMBER_PRICE, DigitalCatalogConstants.PRICE_VALUE);

		product.put(DigitalCatalogConstants.DEFAULT_SKU, defaultSKU);

		ArrayList<Map<String, Object>> childSKUs = (ArrayList<Map<String, Object>>) product.get(DigitalCatalogConstants.CHILD_SKUS);

		if (childSKUs != null) {
		    childSKUs.forEach(childSKU -> {

			childSKU.put(DigitalCatalogConstants.NON_MEMBER_PRICE, DigitalCatalogConstants.PRICE_VALUE);
			childSKU.put(DigitalCatalogConstants.ORIGINAL_PRICE, DigitalCatalogConstants.PRICE_VALUE);
			childSKU.put(DigitalCatalogConstants.LIST_PRICE, DigitalCatalogConstants.PRICE_VALUE);
			childSKU.put(DigitalCatalogConstants.SALE_PRICE, DigitalCatalogConstants.PRICE_VALUE);
			childSKU.put(DigitalCatalogConstants.HIGHEST_SALE_PRICE, DigitalCatalogConstants.PRICE_VALUE);
			childSKU.put(DigitalCatalogConstants.LOWEST_SALE_PRICE, DigitalCatalogConstants.PRICE_VALUE);
		    });
		}
	    } else {

		if (isLoggingDebug()) {
		    logDebug("DigitalProductCatalogRestResourceHelper.dswOutputProduct :: product.priceInCart :: false");
		}

		DropletInvoker priceRangeDropletInvoker = new DropletInvoker(DigitalCatalogConstants.PRICE_RANGE_DROPLET);
		priceRangeDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.PRODUCT_ID, prod.getRepositoryId());

		OParam oParamOutput = priceRangeDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
		Output highestPrice = oParamOutput.addOutput(DigitalCatalogConstants.HIGHEST_PRICE, DigitalCatalogConstants.HIGHEST_PRICE);
		Output lowestPrice = oParamOutput.addOutput(DigitalCatalogConstants.LOWEST_PRICE, DigitalCatalogConstants.LOWEST_PRICE);

		priceRangeDropletInvoker.invoke();

		if (priceRangeDropletInvoker.getOParam(DigitalJaxRsResourceCommonConstants.OUTPUT) != null) {
		    product.put(DigitalCatalogConstants.LOWEST_SALE_PRICE, highestPrice.getObject());
		    product.put(DigitalCatalogConstants.HIGHEST_SALE_PRICE, lowestPrice.getObject());
		}

		ArrayList<Map<String, Object>> childSKUs = (ArrayList<Map<String, Object>>) product.get(DigitalCatalogConstants.CHILD_SKUS);

		if (childSKUs != null) {
		    if (isLoggingInfo()) {
			logInfo("DigitalProductCatalogRestResourceHelper.dswOutputProduct :: processing childSKUs : start");
		    }
		    childSKUs.forEach(childSKU -> {
			if (getPricingTools().isUsingPriceLists()) {

			    if (isLoggingDebug()) {
				logDebug(
					"DigitalProductCatalogRestResourceHelper.dswOutputProduct :: pricingTools.isUsingPriceLists() :: true");
			    }

			    Profile profile = (Profile) ServletUtil.getCurrentUserProfile();

			    DropletInvoker priceDropletInvoker = new DropletInvoker(DigitalCatalogConstants.PRICE_DROPLET);
			    priceDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.PRODUCT_ID, prod.getRepositoryId());
			    priceDropletInvoker.addInput(DigitalCatalogConstants.SKU_ID, childSKU.get(DigitalJaxRsResourceCommonConstants.ID));

			    OParam oPriceDropletOutputParam = priceRangeDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
			    Output listPrice = oPriceDropletOutputParam.addOutput(DigitalCatalogConstants.LIST_PRICE,
				    DigitalCatalogConstants.PRICE_LISTPRICE);

			    priceDropletInvoker.invoke();

			    if (listPrice.getObject() != null) {
				childSKU.put(DigitalCatalogConstants.LIST_PRICE, listPrice.getObject());
			    }

			    priceDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.PRODUCT_ID, prod.getRepositoryId());
			    priceDropletInvoker.addInput(DigitalCatalogConstants.SKU_ID, childSKU.get(DigitalJaxRsResourceCommonConstants.ID));
			    priceDropletInvoker.addInput(DigitalCatalogConstants.PRICE_LIST,
				    profile.getPropertyValue(DigitalCatalogConstants.SALE_PRICE_LIST));

			    OParam oOutputParam = priceRangeDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
			    Output salePrice = oOutputParam.addOutput(DigitalCatalogConstants.SALE_PRICE, DigitalCatalogConstants.PRICE_LISTPRICE);

			    priceDropletInvoker.invoke();

			    if (salePrice.getObject() != null) {
				childSKU.put(DigitalCatalogConstants.SALE_PRICE, salePrice.getObject());
			    }
			} else {

			    if (isLoggingDebug()) {
				logDebug(
					"DigitalProductCatalogRestResourceHelper.dswOutputProduct :: pricingTools.isUsingPriceLists() :: false");
			    }

			    DropletInvoker skuLookupDropletInvoker = new DropletInvoker(DigitalCatalogConstants.SKU_LOOKUP);
			    skuLookupDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.REPOSITORY_KEY, ServletUtil.getUserLocale());
			    skuLookupDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.ID,
				    childSKU.get(DigitalJaxRsResourceCommonConstants.ID));
			    skuLookupDropletInvoker.addInput(DigitalCatalogConstants.FILTER_BY_SITE, pFilterBySite);

			    OParam oOutputParam = skuLookupDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
			    Output listPrice = oOutputParam.addOutput(DigitalCatalogConstants.LIST_PRICE,
				    DigitalCatalogConstants.ELEMENT_LISTPRICE);
			    Output salePrice = oOutputParam.addOutput(DigitalCatalogConstants.SALE_PRICE,
				    DigitalCatalogConstants.ELEMENT_SALEPRICE);

			    skuLookupDropletInvoker.invoke();

			    if (listPrice.getObject() != null) {
				childSKU.put(DigitalCatalogConstants.LIST_PRICE, listPrice.getObject());
			    }

			    if (salePrice.getObject() != null) {
				childSKU.put(DigitalCatalogConstants.SALE_PRICE, salePrice.getObject());
			    }
			}
		    });
		    if (isLoggingInfo()) {
			logInfo("DigitalProductCatalogRestResourceHelper.dswOutputProduct :: processing childSKUs : end");
		    }
		}
	    }
	}

	if (isLoggingDebug()) {
	    logDebug("Exiting method DigitalProductCatalogRestResourceHelper.dswOutputProduct : Product returned : " + product.toString());
	}

	return product;

    }

    /**
     * Method to fetch the product details and return the product properties to dswOutputProduct method
     *
     * @param pProductMap
     *            Holds the map of product properties
     * @param pProduct
     *            Holds the value of product
     * @return Map<String, Object>
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> dswOutputProductSummary(Map<String, Object> pProductMap, Object pProduct) {

	if (isLoggingDebug()) {
	    logDebug("Entering into method DigitalProductCatalogRestResourceHelper.dswOutputProductSummary : Product received : "
		    + pProductMap.toString());
	}

	Map<String, Object> prodSummary = new HashMap<String, Object>();
	Map<String, Object> product = new HashMap<String, Object>();
	product.putAll(pProductMap);

	boolean isAnanymousUser;

	if (pProduct != null) {

	    RepositoryItem prod = (RepositoryItem) pProduct;

	    DropletInvoker currencyDropletInvoker = new DropletInvoker(DigitalCatalogConstants.CURRENCY_CODE_DROPLET);
	    currencyDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.LOCALE, ServletUtil.getUserLocale());

	    OParam oParamOutput = currencyDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
	    Output result = oParamOutput.addOutput(DigitalCatalogConstants.CURRENCY_CODE, DigitalCatalogConstants.CURRENCY_CODE);

	    currencyDropletInvoker.invoke();

	    if (result.getObject() != null) {
		product.put(DigitalCatalogConstants.CURRENCY_CODE, result.getObject());
	    }

	    isAnanymousUser = getProfileTools().isDSWAnanymousUser(ServletUtil.getCurrentUserProfile());

	    if (isAnanymousUser) {
		product.put(DigitalCatalogConstants.PRODUCT_IN_FAVORITES, DigitalCatalogConstants.FALSE);
	    } else {
		if (isLoggingDebug()) {
		    logDebug("DigitalProductCatalogRestResourceHelper.dswOutputProductSummary :: AnanymousUser : false");
		}
		Profile profile = (Profile) ServletUtil.getCurrentUserProfile();
		RepositoryItem giftlist = (RepositoryItem) profile.getPropertyValue(DigitalProfileRestResourceContants.WISHLIST);

		DropletInvoker giftlistDropletInvoker = new DropletInvoker(DigitalCatalogConstants.GIFTLIST_LOOKUP_DROPLET);
		giftlistDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.ID, giftlist.getRepositoryId());
		giftlistDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);

		giftlistDropletInvoker.invoke();

		if (giftlistDropletInvoker.getOParam(DigitalJaxRsResourceCommonConstants.OUTPUT) != null) {
		    DropletInvoker giftlistSiteFilterDropletInvoker = new DropletInvoker(DigitalCatalogConstants.GIFTLIST_SITE_FILTER_DROPLET);
		    giftlistSiteFilterDropletInvoker.addInput(DigitalCatalogConstants.COLLECTION,
			    giftlist.getPropertyValue(DigitalCatalogConstants.GIFTLIST_ITEMS));
		    OParam output = giftlistSiteFilterDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
		    Output response = output.addOutput(DigitalJaxRsResourceCommonConstants.OUTPUT, DigitalCatalogConstants.FILTERED_COLLECTION);

		    giftlistSiteFilterDropletInvoker.invoke();

		    if (giftlistSiteFilterDropletInvoker.getOParam(DigitalJaxRsResourceCommonConstants.OUTPUT) != null) {
			if (response != null && response.getObject() != null) {
			    Collection<RepositoryItem> items = (Collection<RepositoryItem>) response.getObject();
			    if (isLoggingDebug()) {
				logDebug(
					"DigitalProductCatalogRestResourceHelper.dswOutputProductSummary :: iterating over giftlist items to check if product is present");
			    }
			    if (items != null) {
				items.forEach(item -> {
				    if (item.getPropertyValue(DigitalJaxRsResourceCommonConstants.CATALOG_REF_ID) == null
					    && item.getPropertyValue(DigitalJaxRsResourceCommonConstants.PRODUCT_ID) != null
					    && item.getPropertyValue(DigitalJaxRsResourceCommonConstants.PRODUCT_ID).toString()
					    .equalsIgnoreCase(prod.getRepositoryId())) {
					productInFavorites = true;
				    }
				});
			    }
			}
		    }
		}
		product.put(DigitalCatalogConstants.PRODUCT_IN_FAVORITES, productInFavorites);
	    }

	    DropletInvoker navigationURLDropletInvoker = new DropletInvoker(DigitalCatalogConstants.NAVIGATION_URL_DROPLET);
	    navigationURLDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.PRODUCT_ID, prod.getRepositoryId());
	    navigationURLDropletInvoker.addInput(DigitalCatalogConstants.DIMENSION_NAME, DigitalCatalogConstants.BRAND);

	    navigationURLDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);

	    navigationURLDropletInvoker.invoke();

	    if (navigationURLDropletInvoker.getOParam(DigitalJaxRsResourceCommonConstants.OUTPUT) != null) {
		Map<String, Object> dswBrand = new HashMap<String, Object>();

		dswBrand.put(DigitalCatalogConstants.REFINEMENT_NAME, navigationURLDropletInvoker
			.getOParam(DigitalJaxRsResourceCommonConstants.OUTPUT).getOutput(DigitalCatalogConstants.REFINEMENT_NAME));
		dswBrand.put(DigitalCatalogConstants.NAV_URL, navigationURLDropletInvoker.getOParam(DigitalJaxRsResourceCommonConstants.OUTPUT)
			.getOutput(DigitalCatalogConstants.NAV_URL));

		product.put(DigitalCatalogConstants.DSW_BRAND, dswBrand);
	    }

	    prodSummary.put(DigitalJaxRsResourceCommonConstants.PRODUCT, product);

	}

	if (isLoggingDebug()) {
	    logDebug("Exiting method DigitalProductCatalogRestResourceHelper.dswOutputProduct : Product returned : " + prodSummary.toString());
	}

	return prodSummary;

    }

    /**
     * Method to fetch the navigation url response from DigitalPDPNavigationUrlLookup droplet and return the navigation url to
     * DigitalProductCatalogRestResource
     *
     * @param pProductId
     *            Holds the productId of product browsed
     * @param pDimensionName
     *            Holds the value of dimension
     * @return Map<String, Object>
     */
    public Map<String, Object> getNavigationURLResponse(String pProductId, String pDimensionName) {

	if (isLoggingDebug()) {
	    logDebug("Entering into method DigitalProductCatalogRestResourceHelper.getNavigationURLResponse : Product ID received : "
		    + pProductId + " and Dimension Name received : " + pDimensionName);
	}

	Map<String, Object> response = new HashMap<String, Object>();

	DropletInvoker navURLDropletInvoker = new DropletInvoker(DigitalCatalogConstants.NAVIGATION_URL_DROPLET);
	navURLDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.PRODUCT_ID, pProductId);
	navURLDropletInvoker.addInput(DigitalCatalogConstants.DIMENSION_NAME, pDimensionName);

	OParam oParamOutput = navURLDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
	Output refinementName = oParamOutput.addOutput(DigitalCatalogConstants.REFINEMENT_NAME, DigitalCatalogConstants.REFINEMENT_NAME);
	Output navStringUrl = oParamOutput.addOutput(DigitalCatalogConstants.NAV_URL, DigitalCatalogConstants.NAV_URL);

	navURLDropletInvoker.invoke();

	if (navURLDropletInvoker.getOParam(DigitalJaxRsResourceCommonConstants.OUTPUT) != null) {
	    response.put(DigitalCatalogConstants.REFINEMENT_NAME, refinementName.getObject());
	    response.put(DigitalCatalogConstants.NAV_URL, navStringUrl.getObject());
	}

	if (isLoggingDebug()) {
	    logDebug("Exiting method DigitalProductCatalogRestResourceHelper.getNavigationURLResponse : Response returned : " + response);
	}

	return response;

    }

    /**
     * Method to fetch the product details and return the product to DigitalProductCatalogRestResource
     *
     * @param pProductId
     *            Holds the productId of the product to be fetched
     * @param pFilterBySite
     *            Holds the value of filterBySite query parameter
     * @param pFilterByCatalog
     *            Holds the value of filterByCatalog query parameter
     * @return Map<String, Object>
     */
    public Map<String, Object> getDSWGetProductResponse(String pProductId, String pFilterBySite, String pFilterByCatalog) {

	if (isLoggingDebug()) {
	    logDebug("Entering into method DigitalProductCatalogRestResourceHelper.getDSWGetProductResponse : Product ID received : "
		    + pProductId);
	}

	Map<String, Object> response = new HashMap<String, Object>();

	if (pProductId == null) {
	    response.put(DigitalCatalogConstants.ERROR, serviceResourceBundle.getString(DigitalCatalogConstants.EMPTY_PARAMETER));
	} else {
	    if (StringUtils.isEmpty(pProductId)) {
		response.put(DigitalCatalogConstants.ERROR, serviceResourceBundle.getString(DigitalCatalogConstants.EMPTY_PRODUCTID));
	    } else {
		DropletInvoker dropletInvoker = new DropletInvoker(DigitalCatalogConstants.PRODUCT_LOOKUP_DROPLET);

		dropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.REPOSITORY_KEY, ServletUtil.getUserLocale());
		dropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.ID, pProductId);
		dropletInvoker.addInput(DigitalCatalogConstants.FILTER_BY_SITE, pFilterBySite);
		dropletInvoker.addInput(DigitalCatalogConstants.FILTER_BY_CATALOG, pFilterByCatalog);

		OParam oParamOutput = dropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
		Output dropletOutput = oParamOutput.addOutput(DigitalJaxRsResourceCommonConstants.OUTPUT,
			DigitalJaxRsResourceCommonConstants.ELEMENT);

		OParam oParamNoCatalog = dropletInvoker.addOParam(DigitalCatalogConstants.NO_CATALOG);
		Output noCatalog = oParamNoCatalog.addOutput(DigitalCatalogConstants.NO_CATALOG, DigitalJaxRsResourceCommonConstants.ELEMENT);

		dropletInvoker.invoke();

		if (dropletInvoker.getOParam(DigitalCatalogConstants.EMPTY) != null) {
		    response.put(DigitalCatalogConstants.ERROR,
			    MessageFormat.format(serviceResourceBundle.getString(DigitalCatalogConstants.INVALID_PRODUCTID), pProductId));
		}

		if (dropletInvoker.getOParam(DigitalCatalogConstants.WRONG_SITE) != null) {
		    response.put(DigitalCatalogConstants.ERROR, MessageFormat
			    .format(userResourceBundle.getString(DigitalCatalogConstants.PRODUCT_NOT_IN_CURRENT_SITE), pProductId));
		}

		if (dropletInvoker.getOParam(DigitalCatalogConstants.WRONG_CATALOG) != null) {
		    response.put(DigitalCatalogConstants.ERROR, MessageFormat
			    .format(userResourceBundle.getString(DigitalCatalogConstants.PRODUCT_NOT_IN_CURRENT_CATALOG), pProductId));
		}

		if (noCatalog.getObject() != null) {
		    Map<String, Object> productMap = new HashMap<String, Object>();
		    Map<String, StringList> filter = new HashMap<String, StringList>();
		    RepositoryItem output = (RepositoryItem) noCatalog.getObject();
		    try {
			StringList filterArguments = new StringList();
			filterArguments.setStrings(DigitalCatalogConstants.PRODUCT_DETAIL_PROPERTIES);
			filter.put(output.getItemDescriptor().getItemDescriptorName(), filterArguments);

			StringList categoryFilters = new StringList();
			categoryFilters.setStrings(DigitalCatalogConstants.CATEGORY_PROPERTIES);
			filter.put(DigitalCatalogConstants.CATEGORY, categoryFilters);

			StringList skuFilters = new StringList();
			skuFilters.setStrings(DigitalCatalogConstants.SKU_PROPERTIES);
			filter.put(DigitalJaxRsResourceCommonConstants.SKU, skuFilters);

			productMap = DigitalJaxRsUtility.buildRepositoryMap((RepositoryItem) noCatalog.getObject(), filter);
		    } catch (JSONException e) {
			logError("JSON Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductResponse :: " + e);
		    } catch (RepositoryException e) {
			logError("Repository Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductResponse :: " + e);
		    } catch (Exception e) {
			logError("Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductResponse :: " + e);
		    }

		    response.put(DigitalJaxRsResourceCommonConstants.PRODUCT,
			    dswOutputProduct(productMap, pFilterBySite, noCatalog.getObject()));
		}

		if (dropletOutput != null) {
		    Map<String, Object> productMap = new HashMap<String, Object>();
		    Map<String, StringList> filter = new HashMap<String, StringList>();
		    RepositoryItem output = (RepositoryItem) dropletOutput.getObject();
		    try {
			StringList filterArguments = new StringList();
			filterArguments.setStrings(DigitalCatalogConstants.PRODUCT_DETAIL_PROPERTIES);
			filter.put(output.getItemDescriptor().getItemDescriptorName(), filterArguments);

			StringList categoryFilters = new StringList();
			categoryFilters.setStrings(DigitalCatalogConstants.CATEGORY_PROPERTIES);
			filter.put(DigitalCatalogConstants.CATEGORY, categoryFilters);

			StringList skuFilters = new StringList();
			skuFilters.setStrings(DigitalCatalogConstants.SKU_PROPERTIES);
			filter.put(DigitalJaxRsResourceCommonConstants.SKU, skuFilters);

			productMap = DigitalJaxRsUtility.buildRepositoryMap((RepositoryItem) dropletOutput.getObject(),
				filter);
		    } catch (JSONException e) {
			logError("JSON Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductResponse :: " + e);
		    } catch (RepositoryException e) {
			logError("Repository Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductResponse :: " + e);
		    } catch (Exception e) {
			logError("Exception occured in DigitalProductCatalogRestResourceHelper.getDSWGetProductResponse :: " + e);
		    }

		    response.put(DigitalJaxRsResourceCommonConstants.PRODUCT,
			    dswOutputProduct(productMap, pFilterBySite, dropletOutput.getObject()));
		}

	    }

	}

	if (isLoggingDebug()) {
	    logDebug("Exiting method DigitalProductCatalogRestResourceHelper.getDSWGetProductResponse : Response returned : " + response);
	}

	return response;

    }
}