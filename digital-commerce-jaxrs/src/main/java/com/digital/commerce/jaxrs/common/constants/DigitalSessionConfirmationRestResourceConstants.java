package com.digital.commerce.jaxrs.common.constants;

/**
 * Constant file for session confirmation services
 *
 * @author TAIS
 *
 */
public class DigitalSessionConfirmationRestResourceConstants extends DigitalJaxRsResourceCommonConstants {

	public static final String SESSION_CONFIRMATION_DROPLET_V1 = "/com/digital/commerce/services/v1_0/session/DigitalSessionConfirmationDroplet";
	
	public static final String SESSION_CONFIRMATION_NUMBER = "sessionConfirmationNumber";
	
	public static final String SESSION_CONFIRMATION_DROPLET_V2 = "/com/digital/commerce/services/v2_0/session/DigitalSessionConfirmationDroplet";

	
}
