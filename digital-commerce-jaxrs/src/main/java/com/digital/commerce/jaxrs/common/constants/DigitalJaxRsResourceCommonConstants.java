package com.digital.commerce.jaxrs.common.constants;

/**
 * This class provide common constants for all services.
 *
 * @author TAIS
 *
 */
public class DigitalJaxRsResourceCommonConstants {
	
	public static final String SERVICES_RESOURCE_BUNDLE = "com.digital.commerce.services.ServicesResource";

	public static final String DSW_INVENTORY_LOOKUP_DROPLET = "/com/digital/commerce/services/inventory/DigitalPDPInventoryLookup";
	public static final String ANONYMOUS_USER = "anonymousUser";
	public static final String OUTPUT = "output";
	public static final String TOTAL_BAG_COUNT = "totalBagCount";
	public static final String MY_ORDERS = "myOrders";
	public static final String PROFILE_ID = "profileId";
	public static final String CURRENT_ORDER_ID = "currentOrderId";
	public static final String FIRST_NAME = "firstName";
	public static final String EMAIL = "email";
	public static final String LAST_NAME = "lastName";
	public static final String FORM_ERROR = "formError";
	public static final String FORM_EXCEPTION = "formExceptions";
	public static final String RESPONSE = "Response";
	public static final String ERROR = "Error";
	public static final String ERROR_SMALL = "error";

	public static final String ERROR_MSG = "errorMsg";
	public static final String USER_INFO_DROPLET = "/com/digital/commerce/services/profile/DigitalUserInfoDroplet";
	public static final String AVAILABLE_SAVINGS = "availableSavings";
	public static final String STATUS_CODE = "statusCode";
	public static final String SUCCESS = "success";
	public static final String CHECKOUT = "checkout";
	public static final String LATITUDE = "latitude";
	public static final String WISHLIST_L= "wishList";
	public static final String WISHLIST = "wishlist";
	public static final String TOTAL_ORDER_COUNT = "totalOrderCount";
	public static final String ERROR_CODE = "errorCode";
	public static final String LOCALIZED_MESSAGE = "localizedMessage";
	public static final String RESULT_SMALL = "result";
	public static final String REWARDS_CERTIFICATES = "rewardsCertificates";

	public static final String ID = "id";
	public static final String REPOSITORY_KEY = "repositoryKey";
	public static final String ELEMENT = "element";
	public static final String LOCALE = "locale";

	public static final String CATALOG_REF_ID = "catalogRefId";

	public static final String PRODUCT_ID = "productId";

	public static final String SKU = "sku";

	public static final String PRODUCT = "product";

	public static final String TRANSACTION_ID = "transactionId";
	public static final String ONLINE_PROFILE_ID = "transactionId";
	public static final String SUGGESTED_ADDRESSMAP = "suggestedAddressMap";
	public static final String DECISION = "decision";
	public static final String SUGGESTED = "suggested";
	public static final String MODAL = "model";

	public static final String QUANTITY = "quantity";

	public static final String LOCATION_ID = "locationId";

	public static final String SITE_ID = "siteId";

	public static final String ORDER_TOTAL = "orderTotal";

	public static final String CLOSE_QUALIFIER_DROPLET = "/atg/commerce/promotion/ClosenessQualifierDroplet";

	public static final String CLOSENESS_QUALIFIERS = "closenessQualifiers";

	public static final String ORDER = "order";

	public static final String ITEM = "item";

	public static final String INVOKE_ASSEMBLER_DROPLET = "/atg/endeca/assembler/droplet/InvokeAssembler";

	public static final String CONTENT_ITEM = "contentItem";

	public static final String CONTENT_COLLECTION = "contentCollection";

	public static final String INCLUDE_PATH = "includePath";

	public static final String UPDATED_SUCCESS = "updateSuccess";

	public static final String BIRTH_DAY = "birthDay";

	public static final String BIRTH_MONTH = "birthMonth";

	public static final String WEB_TYPE = "webType";

	public static final String GENDER = "gender";

	public static final String SIZE_GROUP = "sizeGroup";

	public static final String SIZES = "sizes";

	public static final String WIDTHS = "widths";

	public static final String GIFT_MESSAGE = "giftMessage";

	public static final String ARRAY = "array";

	public static final String COUNT = "count";

	public static final String DEFAULT_CREDIT_CARD = "defaultCreditCard";

	public static final String ACTIVE_CREDIT_CARD = "activeCreditCards";

	public static final String CREDIT_CARDS = "creditCards";

	public static final String CHANGE_PASSWORD_SUCCESS = "changePasswordsuccess";

	public static final String MEMBER_ID = "memberId";

	public static final String BARCODE = "barCode";

	public static final String START_DATE = "startDate";

	public static final String END_DATE = "endDate";

	public static final String REPRICE_ORDER_DROPLET_PATH = "/atg/commerce/order/purchase/RepriceOrderDroplet";

	public static final String PRICING_OP = "pricingOp";

	public static final String ORDER_TOTAL_PRICING_OP = "ORDER_TOTAL";

	public static final String CREATE_HARGOOD_SHIPPINGGROUP_FORMHANDLER = "/atg/commerce/order/purchase/CreateHardgoodShippingGroupFormHandler";

	public static final String NEW_HARDGOOD_SHIPPINGGROUP = "newHardgoodShippingGroup";

	public static final String STREET_ADDRESS = "streetAddress";

	public static final String RANK = "rank";

	public static final String PHONENUMBER= "phoneNumber";

	public static final String VARIFICATION="addressVerification";

	public static final String POBOX = "isPoBox";

	public static final String REGION = "region";

	public static final String ADDRESSTYPE = "addressType";

	public static final String POSTAL_CODE = "postalCode";

	public static final String COUNTRY = "country";

	public static final String STATE = "state";

	public static final String CITY = "city";

	public static final String ADDRESS1 = "address1";

	public static final String MIDDLENAME = "middleName";

	public static final String LASTNAME = "lastName";

	public static final String ADDRESS2 = "address2" ;

	public static final String FIRSTNAME = "firstName";

	public static final String STORE_ID = "storeId";

	public static final String SORT_BY = "sortBy";
	
	public static final String SOURCE = "source";
	
	public static final String HOW_MANY = "howMany";

	public static final String SUCCESS_L = "Success";

	public static final String BILLING_SERVICE_FORMHANDLER = "/com/digital/commerce/services/v1_0/order/purchase/DigitalBillingServiceHandler";

	public static final String BILLING_ADDRESS = "billingAddress";
	
	public static final String ORDER_ID = "orderId";
	
	public static final String REPRICE_ORDER_FLAG = "repriceOrderFlag";

	public static final String STATUS = "status";

	public static final String TOKEN = "token";

	public static final String FALSE = "false";

	public static final String PAGE_CONTENT_ITEM = "pageContentItem";

	public static final String PAGE_PATH = "pagePath";

	public static final String VERSION = "version";

	public static final String VERSION_2 = "2.0";

	public static final String CONTENT_CONTENTITEM = "contentContentItem";

	public static final String SHIPPING_ADDRESS = "shippingAddress";

	public static final String ADDRESS_TYPE = "addressType";

	public static final String PRIMARY_SHIPPING_ADDRESS = "primaryShippingAddress";

	public static final String OTHER_ADDRESSES = "otherAddresses";

	public static final String ALL_SECONDARY_ADDRESS = "allSecondaryAddresses";

	public static final String SECONDARY_ADDRESS = "secondaryAddress";

	public static final String INTL = "INTL";

	public static final String SECONDARY_ADDRESSESS = "secondaryAddresses";

	public static final String ADDRESS = "addresses";
	
	public static final String SORTED_LIST = "sortedlist";
	
	public static final String LONGITUDE = "longitude";
	
	public static final String SKU_ID = "skuId";
	
	public static final String SHOPPING_CART = "/atg/commerce/ShoppingCart";
	
	public static final String LAST_ORDER = "lastOrder";


}
