/**
 * 
 */
package com.digital.commerce.jaxrs.catalog.vo;

import javax.ws.rs.QueryParam;

/**
 * @author TAIS
 *
 */
public class StoreInventoryParameterBean {

	@QueryParam("locationId")
	private String locationId;

	@QueryParam("productId")
	private String productId;

	@QueryParam("skuId")
	private String skuId;

	@QueryParam("filterOutOfStock")
	private String filterOutOfStock;

	@QueryParam("postalCode")
	private String postalCode;

	@QueryParam("city")
	private String city;

	@QueryParam("state")
	private String state;

	@QueryParam("latitude")
	private String latitude;

	@QueryParam("longitude")
	private String longitude;

	@QueryParam("locateItemsInventory")
	private String locateItemsInventory;

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

	/**
	 * @param pLocationId
	 *            the locationId to set
	 */
	public void setLocationId(String pLocationId) {
		locationId = pLocationId;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param pProductId
	 *            the productId to set
	 */
	public void setProductId(String pProductId) {
		productId = pProductId;
	}

	/**
	 * @return the skuId
	 */
	public String getSkuId() {
		return skuId;
	}

	/**
	 * @param pSkuId
	 *            the skuId to set
	 */
	public void setSkuId(String pSkuId) {
		skuId = pSkuId;
	}

	/**
	 * @return the filterOutOfStock
	 */
	public String getFilterOutOfStock() {
		return filterOutOfStock;
	}

	/**
	 * @param pFilterOutOfStock
	 *            the filterOutOfStock to set
	 */
	public void setFilterOutOfStock(String pFilterOutOfStock) {
		filterOutOfStock = pFilterOutOfStock;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param pPostalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String pPostalCode) {
		postalCode = pPostalCode;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param pCity
	 *            the city to set
	 */
	public void setCity(String pCity) {
		city = pCity;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param pState
	 *            the state to set
	 */
	public void setState(String pState) {
		state = pState;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param pLatitude
	 *            the latitude to set
	 */
	public void setLatitude(String pLatitude) {
		latitude = pLatitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param pLongitude
	 *            the longitude to set
	 */
	public void setLongitude(String pLongitude) {
		longitude = pLongitude;
	}

	/**
	 * @return the locateItemsInventory
	 */
	public String getLocateItemsInventory() {
		return locateItemsInventory;
	}

	/**
	 * @param pLocateItemsInventory
	 *            the locateItemsInventory to set
	 */
	public void setLocateItemsInventory(String pLocateItemsInventory) {
		locateItemsInventory = pLocateItemsInventory;
	}

}