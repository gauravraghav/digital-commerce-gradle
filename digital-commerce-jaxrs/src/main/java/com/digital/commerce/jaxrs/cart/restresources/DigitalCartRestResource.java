package com.digital.commerce.jaxrs.cart.restresources;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.common.util.DigitalCollectionUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.jaxrs.cart.helper.DigitalCartRestResourceHelper;
import com.digital.commerce.jaxrs.common.constants.DigitalCartResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.jaxrs.gifts.helper.DigitalGiftListRestResourceHelper;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.purchase.DigitalCartModifierFormHandler;

import atg.commerce.order.Order;
import atg.commerce.order.purchase.CartModifierFormHandler;
import atg.droplet.DropletException;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.droplet.TagConverterManager;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * DigitalCartRestResource - Service class for Cart Services
 *
 * @author TAIS
 */
@RestResource(id = "com.digital.commerce.jaxrs.cart.restresources.DigitalCartRestResource")
@Path("/cart")
@Api("Cart Services")
@Getter
@Setter
public class DigitalCartRestResource extends DigitalGenericService {

	/**
	 * cartHelper - Helper class
	 */
	private DigitalCartRestResourceHelper cartHelper;

	/**
	 * giftListHelper - Helper class for Gift List
	 */
	private DigitalGiftListRestResourceHelper giftListHelper;

	/**
	 * Service to Add item to cart.
	 *
	 * @see -/atg/commerce/order/purchase/cartModifierActor/addItemToOrder
	 *
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/addItemToOrder")
	@Endpoint(id = "/cart/addItemToOrder#POST", isSingular = true, filterId = "dsw-cart-add-update-response")
	public Response addItemToOrder(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method addItemToOrder()");
		}
		setRequestedTime();
		Object lResponseMap = null;
		Map<String, Object> lFinalResponse = new HashMap<>();

		try {
			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);

			if (isLoggingDebug()) {
				vlogDebug("Request for add item to cart is {0}", lRequestMap);
			}

			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalCartResourceConstants.CART_MODIFER_FORM_HANDLER_PATH,
					DigitalCartResourceConstants.HANDLE_ADD_ITEM);

			getCartHelper().addInputForAddItem(executor, lRequestMap);

			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lResponseMap = getCartHelper().populateErrorResponse(lResult);
			} else {
				boolean lAddedFromGiftlist = false;
				FormHandlerInvocationResult lRemoveFromGiftListResult = null;
				String lGiftItemId = (String) lRequestMap.get(DigitalCartResourceConstants.GIFT_ITEM_ID);
				if (DigitalStringUtil.isNotBlank(lGiftItemId)) {
					lAddedFromGiftlist = true;
					lRemoveFromGiftListResult = getGiftListHelper().removeItemFromGiftList(lGiftItemId);
				}
				if (lAddedFromGiftlist && lRemoveFromGiftListResult.isFormError()) {
					// If any exception while remove item from wish list
					lResponseMap = getCartHelper().getCommonHelper()
							.createFormExceptionResponse(lRemoveFromGiftListResult.getFormExceptions());
				} else {
					CartModifierFormHandler lFH = (CartModifierFormHandler) lResult.getFormHandler();
					Order lOrder = lFH.getOrder();
					lResponseMap = getCartHelper().populateAddItemSuccessResponse(lOrder, lAddedFromGiftlist);
				}
			}

		} catch (ServletException e) {
			vlogError(e, "CommerceException");
		}

		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method addItemToOrder()");
		}

		return Response.ok(lFinalResponse).build();

	}

	/**
	 * Service for remove items from order.
	 *
	 * @see -/atg/commerce/order/purchase/cartModifierActor/removeItemFromOrder
	 *
	 * @param pRemovalCommerceIds
	 *            Commerce Item To Remove
	 * @return Response
	 */
	@DELETE
	@Path("/removeItemFromOrder")
	@Endpoint(id = "/cart/removeItemFromOrder#DELETE", isSingular = true, filterId = "common-response-schema")
	public Response removeItemFromOrder(@QueryParam("removalCommerceIds") String pRemovalCommerceIds) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method removeItemFromOrder()");
		}
		setRequestedTime();
		Map<String, Object> lResponseMap = null;
		Map<String, Object> lFinalResponse = new HashMap<>();

		try {

			if (isLoggingDebug()) {
				vlogDebug("Commerce item to remove {0}", pRemovalCommerceIds);
			}

			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalCartResourceConstants.CART_MODIFER_FORM_HANDLER_PATH,
					DigitalCartResourceConstants.REMOVE_ITEM_HANDLE);

			executor.addInput(DigitalCartResourceConstants.REMOVAL_COMMERCE_IDS, pRemovalCommerceIds,
					TagConverterManager.getTagConverterByName("array"), new Properties());

			executor.addInput(DigitalCartResourceConstants.MODIFY_ORDER_PRICING_OP, DigitalCartResourceConstants.ORDER_TOTAL_PRICING_OP);

			FormHandlerInvocationResult lResult = executor.execute();

			if (lResult.isFormError()) {
				lResponseMap = getCartHelper().populateErrorResponse(lResult);
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			}
		} catch (ServletException e) {
			vlogError(e, "CommerceException");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method removeItemFromOrder()");
		}

		return Response.ok(lFinalResponse).build();

	}

	/**
	 * Service For Updating Item In Cart
	 *
	 * @see -/atg/commerce/order/purchase/cartModifierActor/updateItemToOrder
	 *
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@PUT
	@Path("/updateItemToOrder")
	@Endpoint(id = "/cart/updateItemToOrder#PUT", isSingular = true, filterId = "dsw-cart-add-update-response")
	public Response updateItemToOrder(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method updateItemToOrder()");
		}
		setRequestedTime();
		Map<String, Object> lResponseMap = null;
		Map<String, Object> lFinalResponse = new HashMap<>();

		try {
			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);

			if (isLoggingDebug()) {
				vlogDebug("Request for update item to cart is {0}", lRequestMap);
			}

			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalCartResourceConstants.CART_MODIFER_FORM_HANDLER_PATH,
					DigitalCartResourceConstants.UPDATE_ITEM_HANDLE_METHOD);

			getCartHelper().addInputForUpdateItem(executor, lRequestMap);

			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lResponseMap = getCartHelper().populateErrorResponse(lResult);
			} else {
				CartModifierFormHandler lFH = (CartModifierFormHandler) lResult.getFormHandler();
				Order lOrder = lFH.getOrder();
				lResponseMap = getCartHelper().populateUpdateItemSuccessResponse(lOrder);
			}

		} catch (ServletException e) {
			vlogError(e, "CommerceException");
		}

		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method updateItemToOrder()");
		}
		return Response.ok(lFinalResponse).build();

	}

	/**
	 * Service for remove items from order.
	 *
	 * @see -/atg/commerce/order/purchase/cartModifierActor/removeItemFromOrder
	 *
	 * @param pRemovalCommerceIds
	 *            Commerce Item To Remove
	 * @return Response
	 */
	@GET
	@Path("/paypalLookup")
	@Endpoint(id = "/cart/paypalLookup#GET", isSingular = true, filterId = "common-response-schema")
	public Response paypalLookup(@QueryParam("guestCheckout") String pGuestCheckout,
			@QueryParam("paypalExpressCheckout") String pPaypalExpressCheckout) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method paypalLookup()");
		}
		setRequestedTime();
		Map<String, Object> lResponseMap = null;
		Map<String, Object> lFinalResponse = new HashMap<>();

		try {

			if (isLoggingDebug()) {
				vlogDebug("Guest Checkout {0}, Paypal Express Checkout {1}", pGuestCheckout, pPaypalExpressCheckout);
			}

			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalCartResourceConstants.CART_MODIFER_FORM_HANDLER_PATH,
					DigitalCartResourceConstants.PAYPAL_LOOKUP_HANDLE);

			executor.addInput(DigitalCartResourceConstants.PAYPAL_EXPRESS_CHECKOUT, pPaypalExpressCheckout);
			executor.addInput(DigitalCartResourceConstants.GUEST_CHECKOUT, pGuestCheckout);
			FormHandlerInvocationResult lResult = executor.execute();

			if (lResult.isFormError()) {
				lResponseMap = getCartHelper().populateErrorResponse(lResult);
			} else {
				lResponseMap = getCartHelper().populatePaypalLookupResponse(lResult);
			}
		} catch (ServletException e) {
			vlogError(e, "CommerceException");
		}

		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method paypalLookup()");
		}

		return Response.ok(lFinalResponse).build();

	}

	/**
	 * Checkout Service
	 *
	 * @see -/atg/commerce/order/purchase/cartModifierActor/checkout
	 *
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/checkout")
	@Endpoint(id = "/cart/checkout#POST", isSingular = true, filterId = "dsw-cart-checkout-schema")
	public Response checkout(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method checkout()");
		}
		setRequestedTime();
		Map<String, Object> lResponseMap = null;
		Map<String, Object> lFinalResponse = new HashMap<>();

		try {
			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
			FormHandlerExecutor executor = null;
			FormHandlerInvocationResult lResult = null;

			if (isLoggingDebug()) {
				vlogDebug("Request for checkout service is {0}", lRequestMap);
			}

			boolean lPaypalExpressCheckout = lRequestMap.get("paypalExpressCheckout") == null ? false
					: (boolean) lRequestMap.get("paypalExpressCheckout");

			boolean lGuestCheckout = lRequestMap.get("guestCheckout") == null ? false : (boolean) lRequestMap.get("guestCheckout");

			boolean lSkipOrderResponseObj = lRequestMap.get("skipOrderResponseObj") == null ? false
					: (boolean) lRequestMap.get("skipOrderResponseObj");

			if (lPaypalExpressCheckout) {
				executor = new FormHandlerExecutor(DigitalCartResourceConstants.CART_MODIFER_FORM_HANDLER_PATH,
						DigitalCartResourceConstants.PAYPAL_LOOKUP_HANDLE);

				executor.addInput(DigitalCartResourceConstants.PAYPAL_EXPRESS_CHECKOUT, lPaypalExpressCheckout);
				executor.addInput(DigitalCartResourceConstants.GUEST_CHECKOUT, lGuestCheckout);
				lResult = executor.execute();
				if (lResult.isFormError()) {
					lResponseMap = getCartHelper().populateErrorResponse(lResult);
				} else {
					lResponseMap = executePaypalCheckoutSuccess();
				}
			} else {
				lResponseMap = executeMoveToPurchaseInfo(lSkipOrderResponseObj);
			}

		} catch (ServletException e) {
			vlogError(e, "CommerceException");
		}

		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method checkout()");
		}

		return Response.ok(lFinalResponse).build();

	}

	/**
	 * Execute the paypal checkout success
	 *
	 * @see - /atg/commerce/order/purchase/CartModifierActor/paypalLookupCheckout-success
	 *
	 *
	 * @return
	 * @throws ServletException
	 */
	private Map<String, Object> executePaypalCheckoutSuccess() throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method executePaypalCheckoutSuccess()");
		}
		Map<String, Object> lResponseMap = null;

		FormHandlerExecutor executor = new FormHandlerExecutor(DigitalCartResourceConstants.CART_MODIFER_FORM_HANDLER_PATH,
				DigitalCartResourceConstants.MOVE_TO_PURCHASE_HANDLE);
		executor.addInput(DigitalCartResourceConstants.MODIFY_ORDER_PRICING_OP, DigitalCartResourceConstants.ORDER_TOTAL_PRICING_OP);
		FormHandlerInvocationResult lResult = executor.execute();

		if (lResult.isFormError()) {
			lResponseMap = getCartHelper().populateErrorResponse(lResult);
		} else {
			DigitalCartModifierFormHandler lFH = (DigitalCartModifierFormHandler) lResult.getFormHandler();
			DigitalOrderImpl lOrder = (DigitalOrderImpl) lFH.getOrder();
			if (DigitalCollectionUtil.isEmpty(lOrder.getInventoryProcessResult())) {
				lResponseMap = getCartHelper().populatePaypalLookupResponse(lResult);
			} else {
				lResponseMap = new HashMap<>();
				lResponseMap.put(DigitalCartResourceConstants.ERROR, lOrder.getInventoryProcessResult());
			}
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method executePaypalCheckoutSuccess()");
		}
		return lResponseMap;
	}

	/**
	 * Execution of MoveToPurchaseInfo chain for checkout service
	 *
	 * @param pSkipOrderResponseObj
	 *
	 * @see - /atg/commerce/order/purchase/CartModifierActor/moveToPurchaseInfo
	 *
	 * @return
	 * @throws DropletException
	 */
	private Map<String, Object> executeMoveToPurchaseInfo(boolean pSkipOrderResponseObj) throws DropletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method executeMoveToPurchaseInfo()");
		}
		Map<String, Object> lResponseMap = null;
		FormHandlerExecutor executor = new FormHandlerExecutor(DigitalCartResourceConstants.CART_MODIFER_FORM_HANDLER_PATH,
				DigitalCartResourceConstants.MOVE_TO_PURCHASE_HANDLE);
		FormHandlerInvocationResult lResult = executor.execute();
		if (lResult.isFormError()) {
			lResponseMap = getCartHelper().populateErrorResponse(lResult);
		} else {
			DigitalCartModifierFormHandler lFH = (DigitalCartModifierFormHandler) lResult.getFormHandler();
			DigitalOrderImpl lOrder = (DigitalOrderImpl) lFH.getOrder();
			if (DigitalCollectionUtil.isEmpty(lOrder.getInventoryProcessResult())) {
				lResponseMap = new HashMap<>();
				if (pSkipOrderResponseObj) {
					lResponseMap.put(DigitalCartResourceConstants.RESPONSE, DigitalCartResourceConstants.SUCCESS);
				} else {
					lResponseMap.put(DigitalCartResourceConstants.ORDER, lOrder);
				}
				lResponseMap.put(DigitalCartResourceConstants.WARNING_MESSAGES, lFH.getWarningMessages());
			} else {
				lResponseMap = new HashMap<>();
				lResponseMap.put(DigitalCartResourceConstants.ERROR, lOrder.getInventoryProcessResult());
			}
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method executeMoveToPurchaseInfo()");
		}
		return lResponseMap;
	}
}
