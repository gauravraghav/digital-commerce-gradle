package com.digital.commerce.jaxrs.purchase.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.jaxrs.common.constants.DigitalCommitOrderConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.jaxrs.purchase.helper.DigitalCommitOrderRestResourceHelper;
import com.digital.commerce.services.order.DigitalOrderImpl;

import atg.commerce.order.OrderHolder;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.annotation.Hidden;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;

/**
 * This class will provide RestResources for Commit Order Services.
 * 
 * @author TAIS
 *
 */
@RestResource(id = "com.digital.commerce.jaxrs.purchase.restresources.DigitalCommitOrderRestResource")
@Path("/purchase")
@Api("Commit Order Service")
public class DigitalCommitOrderRestResource extends DigitalGenericService{
	
	/**
	 * mCommitOrderHelper - Helper class.
	 */
	private DigitalCommitOrderRestResourceHelper mCommitOrderHelper;
	
	
	@Hidden
	@GET
	@Endpoint(id = "/purchase/#GET", isSingular = true, filterId = "common-response-schema")
	public Response getPurchase()  {
		return Response.ok(null).build();
	}

	
	/**
	 * Service for Commit Order
	 * 
	 * @see  /rest/model/atg/commerce/order/purchase/CommitOrderActor/commitOrder
	 * @param pRequestBody
	 * @return Response
	 */
	@POST
	@Path("/commitOrder")
	@Endpoint(id = "/purchase/commitOrder#POST", isSingular = true, filterId = "dsw-commit-order-response-schema")
	public Response commitOrder(JSONObject pRequestBody){
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method commitOrder()");
		}
		setRequestedTime();
		Map<String, Object> lResponseMap = new HashMap<>();
		Map<String, Object> lFinalResponse = new HashMap<>();
		
		Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
		if (isLoggingDebug()) {
			vlogDebug("Request for add item to cart is {0}", lRequestMap);
		}
		
		try{
			OrderHolder lshoppingCard = (OrderHolder)ComponentLookupUtil.lookupComponent(ComponentLookupUtil.SHOPPING_CART);
			DigitalOrderImpl lOrder = (DigitalOrderImpl) lshoppingCard.getCurrent();
			
			getCommitOrderHelper().getCommonHelper().invokeRepriceOrderDroplet(lOrder, DigitalCommitOrderConstants.ORDER_TOTAL_PRICING_OP);
			
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalCommitOrderConstants.COMMIT_ORDER_FORM_HANDLER_PATH,
					DigitalCommitOrderConstants.HANDLE_COMMIT_ORDER);
			
			getCommitOrderHelper().addInputForCommitOrder(executor, lRequestMap);
			
			FormHandlerInvocationResult lResult = executor.execute();
			
			
			if (lResult.isFormError()) {
				lResponseMap = getCommitOrderHelper().getCommonHelper().createGenericErrorResponse(lResult);
			}else{
				lResponseMap = getCommitOrderHelper().populateCommitOrderResponse(lResult);
				
			}
			
		} catch (ServletException e) {
			vlogError(e, "CommerceException");
		}
		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method commitOrder()");
		}
		return Response.ok(lFinalResponse).build();
		
	}
	
	public DigitalCommitOrderRestResourceHelper getCommitOrderHelper() {
		return mCommitOrderHelper;
	}

	public void setCommitOrderHelper(DigitalCommitOrderRestResourceHelper mCommitOrderHelper) {
		this.mCommitOrderHelper = mCommitOrderHelper;
	}

}
