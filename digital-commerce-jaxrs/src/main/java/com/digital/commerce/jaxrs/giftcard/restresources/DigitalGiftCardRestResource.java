package com.digital.commerce.jaxrs.giftcard.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalGiftCardRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.jaxrs.giftcard.helper.DigitalGiftCardRestResourceHelper;

import atg.commerce.CommerceException;
import atg.droplet.DropletException;
import atg.nucleus.annotation.Hidden;
import atg.service.jaxrs.RestException;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * DigitalGiftListRestResource - Service class for Gift Card Services
 *
 * @author TAIS
 */
@RestResource(id = "com.digital.commerce.jaxrs.giftcard.restresources.DigitalGiftCardRestResource")
@Path("/giftCard")
@Api("Gift Card Services")
@Getter
@Setter
public class DigitalGiftCardRestResource extends DigitalGenericService {

	/**
	 * giftCardHelper - DigitalGiftCardRestResourceHelper Component
	 */
	private DigitalGiftCardRestResourceHelper giftCardHelper;

	@Hidden
	@GET
	@Endpoint(id = "/giftCard/#GET", isSingular = true, filterId = "common-response-schema")
	public Response getGiftCard() throws RestException, CommerceException {
		return Response.ok(null).build();
	}

	/**
	 * Service to inquire for gift card balance.
	 *
	 * @see - Actor Chain
	 *      -com/digital/commerce/services/v1_0/order/purchase/GiftCardActor/balanceInquiry
	 *
	 * @return Response
	 */

	@POST
	@Path("/balanceInquiry")
	@Endpoint(id = "/giftcard/balanceInquiry#POST", isSingular = true, filterId = "common-response-schema")
	public Response balanceInquiry(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method balanceInquiry");
		}
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);

		setRequestedTime();

		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, getGiftCardHelper().balanceInquiryInfo(lRequestPayload));

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method balanceInquiry");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to add gift card .
	 *
	 * @see - Actor Chain
	 *      -com/digital/commerce/services/v1_0/order/purchase/GiftCardActor/addGiftCardItem
	 *
	 * @return Response
	 */

	@POST
	@Path("/addGiftCardItem")
	@Endpoint(id = "/giftcard/addGiftCardItem#POST", isSingular = true, filterId = "common-response-schema")
	public Response addGiftCardItem(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addGiftCardItem");
		}
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);

		setRequestedTime();

		try {
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, getGiftCardHelper().addGiftCardItemInfo(lRequestPayload));
		} catch (DropletException e) {
			vlogError(e.toString(), " DropletException | ServletException ");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addGiftCardItem");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to remove gift card.
	 *
	 * @see - Actor Chain
	 *      -com/digital/commerce/services/v1_0/order/purchase/GiftCardActor/removeGC
	 *
	 * @return Response
	 */

	@DELETE
	@Path("/removeGC")
	@Endpoint(id = "/giftcard/removeGC#DELETE", isSingular = true, filterId = "common-response-schema")
	public Response removeGC(@QueryParam(DigitalGiftCardRestResourceConstants.CARD_NUMBER) String pCardNumber) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method removeGC");
		}
		setRequestedTime();

		Map<String, Object> lFinalResponse = new HashMap<String, Object>();

		try {
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, getGiftCardHelper().removeGCInfo(pCardNumber));

		} catch (JSONException e) {
			vlogError(e.toString(), " JSONException ");

		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method removeGC ");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to add gift card .
	 *
	 * @see - Actor Chain
	 *      -com/digital/commerce/services/v1_0/order/purchase/GiftCardActor/applyGC
	 *
	 * @return Response
	 */

	@POST
	@Path("/applyGC")
	@Endpoint(id = "/giftcard/applyGC#POST", isSingular = true, filterId = "common-response-schema")
	public Response applyGC(JSONObject pRequest) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method applyGC");
		}
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);

		setRequestedTime();

		try {
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, getGiftCardHelper().applyGCInfo(lRequestPayload));
		} catch (JSONException e) {
			vlogError(e.toString(), " JSONException ");

		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method applyGC");
		}
		return Response.ok(lFinalResponse).build();
	}
}
