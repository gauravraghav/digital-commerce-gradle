package com.digital.commerce.jaxrs.paypal.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalPaypalPaymentRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalProfileRestResourceContants;
import com.digital.commerce.jaxrs.paypal.helper.DigitalPaypalPaymentRestResourceHelper;
import com.digital.commerce.services.order.payment.paypal.PaypalAuthenticationFormHandler;

import atg.commerce.order.OrderHolder;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.GenericService;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class provides PaypalPaymentActor chain ids implementations.
 *
 * @author TAIS
 *
 */

@RestResource(id = "com.digital.commerce.jaxrs.paypal.restresources.DigitalPaypalPaymentRestResource")
@Path("/paypalPayment")
@Api("Paypal Payment service")
@Getter
@Setter
public class DigitalPaypalPaymentRestResource extends GenericService {
	/**
	 * paypalHelper - DigitalPaypalPaymentRestResource Component
	 */
	private DigitalPaypalPaymentRestResourceHelper paypalHelper;
	
	/**
	 * Service to paypal authentication.
	 *
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/order/paypal/paypalPaymentActor/paypalAuthentication
	 *
	 * @return Response
	 */
	@GET
	@Path("/paypalAuthentication")
	@Endpoint(id = "/paypalPayment/paypalAuthentication#GET", isSingular = true, filterId = "common-response-schema")
	public Response paypalAuthentication(@QueryParam(DigitalPaypalPaymentRestResourceConstants.PAYPAL_EXPRESS_CHECKOUT)  Boolean pPaypalExpressCheckout,@QueryParam(DigitalPaypalPaymentRestResourceConstants.GUEST_CHECKOUT)  Boolean pGuestCheckout ) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method paypalAuthentication");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();

		FormHandlerExecutor executor;
		try {
			executor = new FormHandlerExecutor(DigitalPaypalPaymentRestResourceConstants.PAYPAL_PAYMENT_FORMHANDLER_COMPONENT_NAME,
					DigitalPaypalPaymentRestResourceConstants.PAYPAL_AUTHENTICATION);

			executor.addInput(DigitalPaypalPaymentRestResourceConstants.PAYPAL_EXPRESS_CHECKOUT, pPaypalExpressCheckout);
			executor.addInput(DigitalPaypalPaymentRestResourceConstants.GUEST_CHECKOUT,pGuestCheckout);

			FormHandlerInvocationResult lResult = executor.execute();
			PaypalAuthenticationFormHandler lPaypalHandler = (PaypalAuthenticationFormHandler) lResult.getFormHandler();
			if (lResult.isFormError()) {
				// Error
				lResponseMap = getPaypalHelper().getCommonHelper().createGenericErrorResponse(lResult);
			} else {
				Boolean isOrderRespRequiredFlag = lPaypalHandler.getOrderRespRequiredFlag();
				String responseStatus = lPaypalHandler.getResponseStatus();
				//TODO- dONE testing required.
				getPaypalHelper().getCommonHelper().invokeRepriceOrderDroplet(null, DigitalPaypalPaymentRestResourceConstants.ORDER_TOTAL_PRICING_OP);
				Boolean orderRespRequiredFlag =isOrderRespRequiredFlag == null || isOrderRespRequiredFlag == false ? false : true;
				if(!orderRespRequiredFlag){
					OrderHolder lShoppingCart =	ComponentLookupUtil.lookupComponent(DigitalProfileRestResourceContants.SHOPPING_CART, OrderHolder.class);
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.ORDER, lShoppingCart.getCurrent());
				}else{
					lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, responseStatus);
				}
			}
			lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);

		} catch (ServletException e) {
			vlogError(e.toString(), " ServletException ");
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method paypalAuthentication");
		}
		return Response.ok(lFinalResponse).build();
	}
}
