package com.digital.commerce.jaxrs.session.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.session.helper.DigitalSessionConfirmationRestResourceHelper;

import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class will provide RestResource for session confirmation services.
 *
 * @author TAIS
 *
 */
@RestResource(id = " com.digital.commerce.jaxrs.session.restresources.DigitalSessionConfirmationRestResource")
@Path("/session")
@Api("session confirmation Service")
@Getter
@Setter
public class DigitalSessionConfirmationRestResource extends DigitalGenericService {

	/**
	 * sessionResourceHelper - DigitalSessionConfirmationRestResourceHelper
	 * Component
	 */

	private DigitalSessionConfirmationRestResourceHelper sessionResourceHelper;

	/**
	 * Service to getSessionConfirmationNumberV1 details.
	 *
	 * @see - Actor Chain
	 *      -/com/digital/commerce/services/v1_0/session/DigitalSessionConfirmationActor/getSessionConfirmationNumber
	 *
	 * @return Response
	 */

	@GET
	@Path("/getSessionConfirmationNumberV1")
	@Endpoint(id = "/session/getSessionConfirmationNumberV1#@GET", isSingular = true, filterId = "common-response-schema")
	public Response getSessionConfirmationNumberV1() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method getSessionConfirmationNumberV1");
		}
		setRequestedTime();

		Map<String, Object> lFinalResponseMap = new HashMap<>();

		Map<String, Object> lResponseInfoMap;
		try {
			lResponseInfoMap = getSessionResourceHelper().getSessionConfirmationNumberV1Info();
			lFinalResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseInfoMap);
		} catch (ServletException e) {
			vlogError(e.toString(), " ServletException ");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method getSessionConfirmationNumberV1");
		}

		return Response.ok(lFinalResponseMap).build();
	}

	/**
	 * Service to getSessionConfirmationNumberV2 details.
	 *
	 * @see - Actor Chain
	 *      -/com/digital/commerce/services/v2_0/session/DigitalSessionConfirmationActor/getSessionConfirmationNumber
	 *
	 * @return Response
	 */

	@GET
	@Path("/getSessionConfirmationNumberV2")
	@Endpoint(id = "/session/getSessionConfirmationNumberV2#@GET", isSingular = true, filterId = "common-response-schema")
	public Response getSessionConfirmationNumberV2() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method getSessionConfirmationNumberV2");
		}
		Map<String, Object> lFinalResponseMap = new HashMap<>();

		setRequestedTime();

		Map<String, Object> lResponseInfoMap;
		try {
			lResponseInfoMap = getSessionResourceHelper().getSessionConfirmationNumberV2Info();
			lFinalResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseInfoMap);
		} catch (ServletException e) {
			vlogError(e.toString(), " ServletException ");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method getSessionConfirmationNumberV2");
		}

		return Response.ok(lFinalResponseMap).build();
	}

}
