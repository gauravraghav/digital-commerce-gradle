/**
 * 
 */
package com.digital.commerce.jaxrs.catalog.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.catalog.helper.DigitalProductCatalogRestResourceHelper;
import com.digital.commerce.jaxrs.catalog.vo.StoreInventoryParameterBean;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;

import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * ProductCatalog Rest Resource contains all ProductCatalog related services.
 * 
 * @author TAIS
 * 
 */
@RestResource(id = "com.digital.v1.catalog.DigitalProductCatalogRestResource")
@Path("/catalog")
@Api("Catalog Service")
@Getter
@Setter
public class DigitalProductCatalogRestResource extends DigitalGenericService {

	/**
	 * catalogHelper - Catalog Helper Component
	 */
	private DigitalProductCatalogRestResourceHelper catalogHelper;

	/**
	 * Service to return product
	 * 
	 * @see - Actor Chain - /atg/commerce/catalog/ProductCatalogActor/dswGetProductV2
	 * 
	 * @param productId
	 *            Holds the value of query parameter productId
	 * @return Response
	 */
	@GET
	@Path("/dswGetProductV2")
	@Endpoint(id = "/catalog/dswGetProductV2#GET", isSingular = true, filterId = "dsw-catalog-response")
	public Response getDSWGetProductV2(@QueryParam("productId") String productId) {

		if (isLoggingDebug()) {
			logDebug("Entering into method DigitalProductCatalogRestResource.getDSWGetProductV2");
		}

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> finalResponse = new HashMap<String, Object>();

		super.setRequestedTime();

		response = getCatalogHelper().getDSWGetProductV2Response(productId);
		finalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, response);

		super.setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exiting method DigitalProductCatalogRestResource.getDSWGetProductV2");
		}

		return Response.ok(finalResponse).build();

	}

	/**
	 * Service to return product
	 * 
	 * @see - Actor Chain - /atg/commerce/catalog/ProductCatalogActor/getProductsV2
	 * 
	 * @param productId
	 *            Holds the value of query parameter productId
	 * @return Response
	 */
	@GET
	@Path("/getProductsV2")
	@Endpoint(id = "/catalog/getProductsV2#GET", isSingular = true, filterId = "dsw-product-response")
	public Response getProductsV2(@QueryParam("productId") String productId) {

		if (isLoggingDebug()) {
			logDebug("Entering into method DigitalProductCatalogRestResource.getProductsV2");
		}

		Map<String, Object> finalResponse = new HashMap<String, Object>();

		finalResponse = getCatalogHelper().getDSWGetProductV2Response(productId);

		if (isLoggingDebug()) {
			logDebug("Exiting method DigitalProductCatalogRestResource.getProductsV2");
		}

		return Response.ok(finalResponse).build();

	}

	/**
	 * Service to return store inventory
	 * 
	 * @see - Actor Chain - /atg/commerce/catalog/ProductCatalogActor/getProductStoreInventory
	 * 
	 * @param paramBean
	 *            Holds the value of different query parameters in the form of bean
	 * @return Response
	 */
	@GET
	@Path("/getProductStoreInventory")
	@Endpoint(id = "/catalog/getProductStoreInventory#GET", isSingular = true, filterId = "dsw-store-inventory-response")
	public Response getProductStoreInventory(@BeanParam StoreInventoryParameterBean paramBean) {

		if (isLoggingDebug()) {
			logDebug("Entering into method DigitalProductCatalogRestResource.getProductStoreInventory");
		}

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> finalResponse = new HashMap<String, Object>();

		super.setRequestedTime();

		response = getCatalogHelper().getDSWStoreInventoryResponse(paramBean);
		finalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, response);

		super.setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exiting method DigitalProductCatalogRestResource.getProductStoreInventory");
		}

		return Response.ok(finalResponse).build();

	}

	/**
	 * Service to return the store inventory details
	 * 
	 * @see - Actor Chain - /atg/commerce/catalog/ProductCatalogActor/getProductStoreInventoryDetail
	 * 
	 * @param paramBean
	 *            Holds the value of different query parameters in the form of bean
	 * @return Response
	 */
	@GET
	@Path("/getProductStoreInventoryDetail")
	@Endpoint(id = "/catalog/getProductStoreInventoryDetail#GET", isSingular = true, filterId = "dsw-store-inventory-detail-response")
	public Response getProductStoreInventoryDetail(@BeanParam StoreInventoryParameterBean paramBean) {

		if (isLoggingDebug()) {
			logDebug("Entering into method DigitalProductCatalogRestResource.getProductStoreInventoryDetail");
		}

		Map<String, Object> finalResponse = new HashMap<String, Object>();

		finalResponse = getCatalogHelper().getDSWStoreInventoryResponse(paramBean);

		if (isLoggingDebug()) {
			logDebug("Exiting method DigitalProductCatalogRestResource.getProductStoreInventoryDetail");
		}

		return Response.ok(finalResponse).build();

	}

	/**
	 * Service to return product details
	 * 
	 * @see - Actor Chain - /atg/commerce/catalog/ProductCatalogActor/dswGetProductDetail
	 * 
	 * @param productId
	 *            Holds the value of query parameter productId
	 * @param filterBySite
	 *            Holds the value of query parameter filterBySite
	 * @param filterByCatalog
	 *            Holds the value of query parameter filterByCatalog
	 * @return Response
	 */
	@GET
	@Path("/dswGetProductDetail")
	@Endpoint(id = "/catalog/dswGetProductDetail#GET", isSingular = true, filterId = "dsw-product-detail-response")
	public Response getDSWGetProductDetail(@QueryParam("productId") String productId, @QueryParam("filterBySite") String filterBySite,
			@QueryParam("filterByCatalog") String filterByCatalog) {

		if (isLoggingDebug()) {
			logDebug("Entering into method DigitalProductCatalogRestResource.getDSWGetProductDetail");
		}

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> finalResponse = new HashMap<String, Object>();

		super.setRequestedTime();

		response = getCatalogHelper().getDSWGetProductDetailsResponse(productId, filterBySite, filterByCatalog);
		finalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, response);

		super.setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exiting method DigitalProductCatalogRestResource.getDSWGetProductDetail");
		}

		return Response.ok(finalResponse).build();

	}

	/**
	 * Service to return product details
	 * 
	 * @see - Actor Chain - /atg/commerce/catalog/ProductCatalogActor/getProductDetails
	 * 
	 * @param productId
	 *            Holds the value of query parameter productId
	 * @param filterBySite
	 *            Holds the value of query parameter filterBySite
	 * @param filterByCatalog
	 *            Holds the value of query parameter filterByCatalog
	 * @return Response
	 */
	@GET
	@Path("/getProductDetails")
	@Endpoint(id = "/catalog/getProductDetails#GET", isSingular = true, filterId = "dsw-product-detail")
	public Response getProductDetails(@QueryParam("productId") String productId, @QueryParam("filterBySite") String filterBySite,
			@QueryParam("filterByCatalog") String filterByCatalog) {

		if (isLoggingDebug()) {
			logDebug("Entering into method DigitalProductCatalogRestResource.getProductDetails");
		}

		Map<String, Object> finalResponse = new HashMap<String, Object>();

		finalResponse = getCatalogHelper().getDSWGetProductDetailsResponse(productId, filterBySite, filterByCatalog);

		if (isLoggingDebug()) {
			logDebug("Exiting method DigitalProductCatalogRestResource.getProductDetails");
		}

		return Response.ok(finalResponse).build();

	}

	/**
	 * Service to get product response
	 * 
	 * @see - Actor Chain - /atg/commerce/catalog/ProductCatalogActor/dswGetProduct
	 * 
	 * @param productId
	 *            Holds the value of query parameter productId
	 * @param filterBySite
	 *            Holds the value of query parameter filterBySite
	 * @param filterByCatalog
	 *            Holds the value of query parameter filterByCatalog
	 * @return Response
	 */
	@GET
	@Path("/dswGetProduct")
	@Endpoint(id = "/catalog/dswGetProduct#GET", isSingular = true, filterId = "dsw-get-product-response")
	public Response getDSWGetProduct(@QueryParam("productId") String productId, @QueryParam("filterBySite") String filterBySite,
			@QueryParam("filterByCatalog") String filterByCatalog) {

		if (isLoggingDebug()) {
			logDebug("Entering into method DigitalProductCatalogRestResource.getDSWGetProduct");
		}

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> finalResponse = new HashMap<String, Object>();

		super.setRequestedTime();

		response = getCatalogHelper().getDSWGetProductResponse(productId, filterBySite, filterByCatalog);
		finalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, response);

		super.setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exiting method DigitalProductCatalogRestResource.getDSWGetProduct");
		}

		return Response.ok(finalResponse).build();

	}

	/**
	 * Service to get product response
	 * 
	 * @see - Actor Chain - /atg/commerce/catalog/ProductCatalogActor/getProducts
	 * 
	 * @param productId
	 *            Holds the value of query parameter productId
	 * @param filterBySite
	 *            Holds the value of query parameter filterBySite
	 * @param filterByCatalog
	 *            Holds the value of query parameter filterByCatalog
	 * @return Response
	 */
	@GET
	@Path("/getProducts")
	@Endpoint(id = "/catalog/getProducts#GET", isSingular = true, filterId = "dsw-get-product")
	public Response getProducts(@QueryParam("productId") String productId, @QueryParam("filterBySite") String filterBySite,
			@QueryParam("filterByCatalog") String filterByCatalog) {

		if (isLoggingDebug()) {
			logDebug("Entering into method DigitalProductCatalogRestResource.getProducts");
		}

		Map<String, Object> finalResponse = new HashMap<String, Object>();

		finalResponse = getCatalogHelper().getDSWGetProductResponse(productId, filterBySite, filterByCatalog);

		if (isLoggingDebug()) {
			logDebug("Exiting method DigitalProductCatalogRestResource.getProducts");
		}

		return Response.ok(finalResponse).build();

	}

	/**
	 * Service to return navigation url
	 * 
	 * @see - Actor Chain - /atg/commerce/catalog/ProductCatalogActor/getNavigationUrl
	 * 
	 * @param productId
	 *            Holds the value of query parameter productId
	 * @param dimensionName
	 *            Holds the value of query parameter dimensionName
	 * @return Response
	 */
	@GET
	@Path("/getNavigationUrl")
	@Endpoint(id = "/catalog/getNavigationUrl#GET", isSingular = true, filterId = "navigation-url-response")
	public Response getNavigationUrl(@QueryParam("productId") String productId, @QueryParam("dimensionName") String dimensionName) {

		if (isLoggingDebug()) {
			logDebug("Entering into method DigitalProductCatalogRestResource.getNavigationUrl");
		}

		Map<String, Object> finalResponse = new HashMap<String, Object>();

		finalResponse = getCatalogHelper().getNavigationURLResponse(productId, dimensionName);

		if (isLoggingDebug()) {
			logDebug("Exiting method DigitalProductCatalogRestResource.getNavigationUrl");
		}

		return Response.ok(finalResponse).build();

	}
}