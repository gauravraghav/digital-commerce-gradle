package com.digital.commerce.jaxrs.shipping.helper;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import com.digital.commerce.jaxrs.common.constants.DigitalProfileRestResourceContants;
import com.digital.commerce.jaxrs.common.constants.DigitalShippingGroupRestResourceContants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.services.profile.DigitalProfileTools;

import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

/**
 * This class is used to provide response for login services
 *
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalHardgoodShippingGroupRestResourceHelper extends GenericService {


	/**
	 * mCommonHelper - DigitalCommonRestResourceHelper Component
	 */
	private DigitalCommonRestResourceHelper commonHelper;

	/**
	 * mProfileTools - DigitalProfileTools Component
	 */
	private DigitalProfileTools profileTools;

	/**
	 *  This method is used to provide input to executor for ShippingAddress .
	 *
	 * @param pExecutor
	 * @param pRequestPayload
	 * @throws ServletException
	 */
	public void inputsForCreateShippingAddressChain(FormHandlerExecutor pExecutor, Map<String, Object> pRequestPayload) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method inputsForCreateShippingAddressChain");
		}
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.GENERATE_NICK_NAME, DigitalShippingGroupRestResourceContants.TRUE);
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.PROFILE_PAGE,DigitalShippingGroupRestResourceContants.TRUE);
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.SAVE_AS_PRIMARY, pRequestPayload.get(DigitalShippingGroupRestResourceContants.SAVE_AS_PRIMARY));

		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_FIRSTNAME,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.FIRSTNAME));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_LASTNAME,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.LASTNAME));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_MIDDLENAME,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.MIDDLENAME));

		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_ADDRESS1,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.ADDRESS1));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_ADDRESS2,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.ADDRESS2));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_CITY,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.CITY));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_STATE,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.STATE));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_COUNTRY,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.COUNTRY));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_POSTAL_CODE,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.POSTAL_CODE));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_ADDRESSTYPE,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.ADDRESSTYPE));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_REGION,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.REGION));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_POBOX,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.POBOX));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_VARIFICATION,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.VARIFICATION));


		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_PHONENUMBER,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.PHONENUMBER));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.ADDRESS_RANK,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.RANK));

		pExecutor.addInput(DigitalShippingGroupRestResourceContants.CUSTOMER_DECISION, pRequestPayload.get(DigitalShippingGroupRestResourceContants.CUSTOMER_DECISION));

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method inputsForCreateShippingAddressChain");
		}

	}

	/**
	 * This method is used to provide success response for shipping address.
	 *
	 * @param pRequestPayload
	 * @param pResult
	 * @return
	 * @throws RepositoryException
	 */
	public Map<String, Object> populateShippingAddressResponse(Map<String, Object> pRequestPayload, FormHandlerInvocationResult pResult) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateShippingAddressResponse");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Boolean lCheckout = pRequestPayload.get(DigitalShippingGroupRestResourceContants.CHECKOUT) == null ? false
				: (boolean) pRequestPayload.get(DigitalShippingGroupRestResourceContants.CHECKOUT);
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		if(!lCheckout){
			lResponseMap.put(DigitalShippingGroupRestResourceContants.MAILLING_ADDRESS_EXIST,getProfileTools().isProfileMailingAddressExists(lProfile));
			lResponseMap.put(DigitalShippingGroupRestResourceContants.STREET_ADDRESS, lProfile.getPropertyValue(DigitalShippingGroupRestResourceContants.HOME_ADDRESS));
		}
		RepositoryItem	 lShippingAddress = (RepositoryItem) lProfile.getPropertyValue(DigitalShippingGroupRestResourceContants.SHIPPING_ADDRESS);
		if(lShippingAddress != null && lShippingAddress.getPropertyValue(DigitalShippingGroupRestResourceContants.ADDRESS_TYPE) != DigitalShippingGroupRestResourceContants.INTL){
			lResponseMap.put(DigitalShippingGroupRestResourceContants.PRIMARY_SHIPPING_ADDRESS, lShippingAddress);
		}
		Map<String , RepositoryItem>  lAllSecondaryAddresses =   (Map<String, RepositoryItem>) lProfile.getPropertyValue(DigitalShippingGroupRestResourceContants.ALL_SECONDARY_ADDRESS);
		Map<String , Object> lSecondaryMap = new HashMap<>();
		Map<String , Object> lSecondaryAddMap = new HashMap<>();
		for(Map.Entry<String , RepositoryItem> lSecondaryAddress: lAllSecondaryAddresses.entrySet())
		{
			if(!lSecondaryAddress.getValue().getPropertyValue(DigitalProfileRestResourceContants.ADDRESSTYPE).equals(DigitalProfileRestResourceContants.INTL)  ){
				lSecondaryAddMap.put(lSecondaryAddress.getKey(), lSecondaryAddress.getValue());
			}
		}

		lSecondaryMap.put(DigitalProfileRestResourceContants.ADDRESS,lSecondaryAddMap);
		lResponseMap.put(DigitalProfileRestResourceContants.OTHER_ADDRESSES, lSecondaryMap);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateShippingAddressResponse");
		}
		return lResponseMap;
	}

	/**
	 * This method is used to provide input to executor for ShippingAddress .
	 *
	 * @param pExecutor
	 * @param pRequestPayload
	 * @throws ServletException
	 */
	public void inputsForShippingGroupAddressChain(FormHandlerExecutor pExecutor, Map<String, Object> pRequestPayload) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method inputsForShippingGroupAddressChain");
		}
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.GENERATE_NICK_NAME, DigitalShippingGroupRestResourceContants.TRUE);
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.PROFILE_PAGE,DigitalShippingGroupRestResourceContants.TRUE);
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.SAVE_AS_PRIMARY, pRequestPayload.get(DigitalShippingGroupRestResourceContants.SAVE_AS_PRIMARY));

		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOR_SHIPPINGGROUP_NAME,pRequestPayload.get(DigitalShippingGroupRestResourceContants.HARDGOOR_SHIPPINGGROUP_NAME));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOR_SHIPPINGGROUP_TYPE, pRequestPayload.get(DigitalShippingGroupRestResourceContants.HARDGOOR_SHIPPINGGROUP_TYPE));

		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_FIRSTNAME,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.FIRSTNAME));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_MIDDLENAME,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.MIDDLENAME));

		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_ADDRESS1,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.ADDRESS1));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_ADDRESS2,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.ADDRESS2));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_CITY,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.CITY));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_STATE,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.STATE));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_COUNTRY,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.COUNTRY));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_POSTAL_CODE,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.POSTAL_CODE));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_ADDRESSTYPE,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.ADDRESSTYPE));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_REGION,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.REGION));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_POBOX,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.POBOX));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_VARIFICATION,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.VARIFICATION));

		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_PHONENUMBER,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.PHONENUMBER));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_RANK,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.RANK));

		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_SHIPTYPE,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.SHIPTYPE));
		pExecutor.addInput(DigitalShippingGroupRestResourceContants.HARDGOOD_SHIPPING_GROUP_ADDRESS_STOREID,
				pRequestPayload.get(DigitalShippingGroupRestResourceContants.STORE_ID));

		pExecutor.addInput(DigitalShippingGroupRestResourceContants.CUSTOMER_DECISION, pRequestPayload.get(DigitalShippingGroupRestResourceContants.CUSTOMER_DECISION));

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method inputsForShippingGroupAddressChain");
		}

	}
}
