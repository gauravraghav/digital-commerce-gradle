package com.digital.commerce.jaxrs.shipping.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalShippingGroupRestResourceContants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.jaxrs.shipping.helper.DigitalHardgoodShippingGroupRestResourceHelper;
import com.digital.commerce.services.order.purchase.DigitalCreateHardgoodShippingGroupFormHandler;

import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.annotation.Hidden;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class will provide RestResource for HardgoodShippingGroup Services.
 *
 * @author TAIS
 *
 */
@RestResource(id = "com.digital.commerce.jaxrs.shipping.restresources.DigitalHardgoodShippingGroupRestResource")
@Path("/hardgoodShippingGroup")
@Api("Hardgood ShippingGroup Service")
@Getter
@Setter
public class DigitalHardgoodShippingGroupRestResource extends DigitalGenericService {

	/**
	 * mHardgoodShippingGroupResourceHelper - DigitalHardgoodShippingGroupRestResourceHelper Component
	 */

	private DigitalHardgoodShippingGroupRestResourceHelper hardgoodShippingGroupResourceHelper;

	@Hidden
	@GET
	@Endpoint(id = "/hardgoodShippingGroup/#GET", isSingular = true, filterId = "common-response-schema")
	public Response getShippingGroups() {

		Map<String, Object> lResponse = new HashMap<String, Object>();

		return Response.ok(lResponse).build();
	}


	/**
	 * Service to create ShippingAddress.
	 *
	 * @see - Actor Chain - /atg/commerce/order/purchase/createHardgoodShippingGroupActor/createShippingAddress
	 *
	 * @return Response
	 */

	@POST
	@Path("/createShippingAddress")
	@Endpoint(id = "/shipping/createShippingAddress#POST", isSingular = true, filterId = "dsw-createShippingAddress-response-schema")
	public Response createShippingAddress(JSONObject pLoginRequest) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method createShippingAddress");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pLoginRequest);
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();

		setRequestedTime();

		try {
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalJaxRsResourceCommonConstants.CREATE_HARGOOD_SHIPPINGGROUP_FORMHANDLER,
					DigitalJaxRsResourceCommonConstants.NEW_HARDGOOD_SHIPPINGGROUP);
			getHardgoodShippingGroupResourceHelper().inputsForCreateShippingAddressChain(executor, lRequestPayload);
			FormHandlerInvocationResult lResult = executor.execute();
			DigitalCreateHardgoodShippingGroupFormHandler lShippingFormHandler = (DigitalCreateHardgoodShippingGroupFormHandler) lResult.getFormHandler();
			if (lResult.isFormError()) {
				lResponseMap = getHardgoodShippingGroupResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
				lResponseMap.put(DigitalShippingGroupRestResourceContants.SHIPPING_SERVICE_RESPONSE, lShippingFormHandler.getShippingAddressServiceResponse());
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			}else{
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
						getHardgoodShippingGroupResourceHelper().populateShippingAddressResponse(lRequestPayload, lResult));
			}
		} catch (ServletException e) {
			vlogError(e, "ServletException");
		}
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method createShippingAddress");
		}

		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service to create ShippingGroupAddress.
	 *
	 * @see - Actor Chain - /atg/commerce/order/purchase/createHardgoodShippingGroupActor/createShippingGroupAddress
	 *
	 * @return Response
	 */

	@POST
	@Path("/createShippingGroupAddress")
	@Endpoint(id = "/shipping/createShippingGroupAddress#POST", isSingular = true, filterId = "dsw-ShippingGroup-schema")
	public Response createShippingGroupAddress(JSONObject pLoginRequest) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method createShippingGroupAddress");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pLoginRequest);
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();

		setRequestedTime();

		try {
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalJaxRsResourceCommonConstants.CREATE_HARGOOD_SHIPPINGGROUP_FORMHANDLER,
					DigitalJaxRsResourceCommonConstants.NEW_HARDGOOD_SHIPPINGGROUP);
			getHardgoodShippingGroupResourceHelper().inputsForShippingGroupAddressChain(executor, lRequestPayload);
			FormHandlerInvocationResult lResult = executor.execute();
			DigitalCreateHardgoodShippingGroupFormHandler lShippingFormHandler = (DigitalCreateHardgoodShippingGroupFormHandler) lResult.getFormHandler();
			if (lResult.isFormError()) {
				lResponseMap = getHardgoodShippingGroupResourceHelper().getCommonHelper().createGenericErrorResponse(lResult);
				lResponseMap.put(DigitalShippingGroupRestResourceContants.SHIPPING_SERVICE_RESPONSE, lShippingFormHandler.getShippingAddressServiceResponse());
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			} else {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
						DigitalJaxRsResourceCommonConstants.SUCCESS_L);
			}
		} catch (ServletException e) {
			vlogError(e, "ServletException");
		}
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method createShippingGroupAddress");
		}

		return Response.ok(lFinalResponse).build();
	}
}
