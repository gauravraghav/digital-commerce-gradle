package com.digital.commerce.jaxrs.gifts.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.gifts.helper.DigitalGiftListRestResourceHelper;

import atg.core.util.StringUtils;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;

/**
 * DigitalGiftListLookupRestResource - Class for gift list look jax-rs services
 *
 * @author TAIS
 */
@RestResource(id = "com.digital.commerce.jaxrs.gifts.restresources.DigitalGiftListLookupRestResource")
@Path("/giftListLookup")
@Api("Gift List Lookup Service")
public class DigitalGiftListLookupRestResource extends DigitalGenericService {

	/**
	 * mGiftHelper - Helper class for gift resources
	 */
	public DigitalGiftListRestResourceHelper mGiftHelper;

	/**
	 * Service to provide AvailableSavings details.
	 *
	 * @see - /rest/model/atg/commerce/gifts/GiftlistLookupActor/dswViewWishlistV3
	 *
	 * @return Response
	 */

	@GET
	@Path("/dswViewWishlistV3")
	@Endpoint(id = "/giftListLookup/dswViewWishlistV3#GET", isSingular = true, filterId = "common-response-schema")
	public Response dswViewWishlistV3(@QueryParam("returnProdIdsOnly") Boolean returnProdIdsOnly, @QueryParam("No") Integer startIndex,
			@QueryParam("Nrpp") Integer endIndex, @QueryParam("giftlistId") String giftlistId) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method dswViewWishlistV3()");
		}
		setRequestedTime();

		Map<String, Object> lResponse = new HashMap<>();
		try {
			lResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
					mGiftHelper.populateWishListDetailsV3(returnProdIdsOnly, startIndex, endIndex, giftlistId));
		} catch (Exception e) {
			vlogError(e, "CommerceException");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method dswViewWishlistV3()");
		}

		return Response.ok(lResponse).build();

	}

	/**
	 * Service to provide AvailableSavings details.
	 *
	 * @see - /rest/model/atg/commerce/gifts/GiftlistLookupActor/dswViewSharedWishlistV3
	 *
	 * @return Response
	 * @throws Exception
	 */

	@GET
	@Path("/dswViewSharedWishlistV3")
	@Endpoint(id = "/giftListLookup/dswViewSharedWishlistV3#GET", isSingular = true, filterId = "common-response-schema")
	public Response dswViewSharedWishlistV3(@QueryParam("returnProdIdsOnly") Boolean returnProdIdsOnly,
			@QueryParam("No") Integer startIndex, @QueryParam("Nrpp") Integer endIndex, @QueryParam("giftlistId") String giftlistId) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method dswViewWishlistV3()");
		}
		setRequestedTime();

		Map<String, Object> lResponse = new HashMap<>();
		if (StringUtils.isNotBlank(giftlistId)) {
			try {
				lResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
						mGiftHelper.populateWishListDetailsV3(returnProdIdsOnly, startIndex, endIndex, giftlistId));
			} catch (Exception e) {
				vlogError(e, "CommerceException");
			}
		} else {
			lResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, "wishlist id can not be empty");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method dswViewWishlistV3()");
		}

		return Response.ok(lResponse).build();

	}

	/**
	 * @return the giftHelper
	 */
	public DigitalGiftListRestResourceHelper getGiftHelper() {
		return mGiftHelper;
	}

	/**
	 * @param pGiftHelper
	 *            the giftHelper to set
	 */
	public void setGiftHelper(DigitalGiftListRestResourceHelper pGiftHelper) {
		mGiftHelper = pGiftHelper;
	}

}
