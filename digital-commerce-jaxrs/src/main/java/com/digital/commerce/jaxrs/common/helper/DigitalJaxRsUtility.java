package com.digital.commerce.jaxrs.common.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang.ArrayUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.digital.commerce.common.util.ComponentLookupUtil;

import atg.adapter.gsa.ChangeAwareList;
import atg.adapter.gsa.ChangeAwareSet;
import atg.core.util.StringList;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;

/**
 *
 * Common utility class for Jax-rs services
 *
 * @author TAIS
 *
 */
public class DigitalJaxRsUtility {

	/**
	 * mCommonHelper - Common Helper
	 */
	private static DigitalCommonRestResourceHelper mCommonHelper = null;

	static {
		mCommonHelper = ComponentLookupUtil.lookupComponent("/com/digital/commerce/jaxrs/common/hepler/DigitalCommonRestResourceHelper",
				DigitalCommonRestResourceHelper.class);
	}

	/**
	 * Utility Method to convert JsonObject HashMap
	 *
	 * @param object
	 * @return new HashMap instance
	 * @throws JSONException
	 */
	public static Map<String, Object> toMap(JSONObject object) throws JSONException {

		Map<String, Object> map = new HashMap<String, Object>();

		if (object == null) {
			return map;
		}

		Iterator<String> keysItr = object.keys();
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);

			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	/**
	 * Helper method to convert json into map
	 *
	 * @see toMap
	 *
	 * @param JSONArray
	 *            array
	 * @return List
	 * @throws JSONException
	 */
	public static List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

	/**
	 * Utility method which will iterate all the keys present in map and check for repository item and if found it will convert item to map
	 * based on given filer if any.
	 *
	 * @param pInputMap
	 * @param pFilters
	 * @return Map of String and Object
	 * @throws Exception
	 * @throws RepositoryException
	 * @throws JSONException
	 * @throws BeanFilterException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" }) // As input values are object
	// so they can be of any type
	public static Map<String, Object> buildReposiotryItemToMap(Map<String, Object> pInputMap, Map<String, StringList> pFilters)
			throws JSONException, RepositoryException, BeanFilterException {
		Map<String, Object> lFinalMap = new HashMap<String, Object>();

		if (pInputMap == null) {
			return pInputMap;
		}
		Iterator<String> lKeySet = pInputMap.keySet().iterator();

		while (lKeySet.hasNext()) {
			String lKey = lKeySet.next();
			Object lValue = pInputMap.get(lKey);

			if (lValue instanceof RepositoryItem) {
				lValue = buildRepositoryMap((RepositoryItem) lValue, pFilters);
			} else if (lValue instanceof Map) {
				lValue = buildReposiotryItemToMap((Map<String, Object>) lValue, pFilters);
			} else if (lValue instanceof List) {
				List lListItems = (List) lValue;
				List<Object> lList = new ArrayList<>();
				for (Object lItem : lListItems) {
					if (lItem instanceof RepositoryItem) {
						lList.add(buildRepositoryMap((RepositoryItem) lItem, pFilters));
					} else if (lItem instanceof Map) {
						lList.add(buildReposiotryItemToMap((Map<String, Object>) lItem, pFilters));
					} else {
						lList.add(lItem);
					}
				}
				lValue = lList;
			} else if (lValue instanceof Set) {
				Set lSetItems = (Set) lValue;
				Set<Object> lProcessedSet = new HashSet<>();
				for (Object lItem : lSetItems) {
					if (lItem instanceof RepositoryItem) {
						lProcessedSet.add(buildRepositoryMap((RepositoryItem) lItem, pFilters));
					} else if (lItem instanceof Map) {
						lProcessedSet.add(buildReposiotryItemToMap((Map<String, Object>) lItem, pFilters));
					} else {
						lProcessedSet.add(lItem);
					}
				}
				lValue = lProcessedSet;
			}

			lFinalMap.put(lKey, lValue);
		}

		return lFinalMap;
	}

	/**
	 * This method gets a Repository item (any Repository item) and converts it to Map of Maps
	 *
	 * @param repItem
	 *            - Any Repository Item. It can be product or sku or material or anything
	 * @param filter
	 *            - Filter variable which lists out only required properties from each Item descriptor. Please note that this filter is for
	 *            each Item descriptor and not at Repository level.
	 * @param addAttributes
	 *            - Add any new attribute that are missing in the filter. Useful in WishList page where childSKU should not be sent for
	 *            products in wishlist (NOTE: This accepts only new attributes for Product item descriptor)
	 * @return - Returns Map of Maps with required properties
	 * @throws JSONException
	 * @throws RepositoryException
	 * @throws BeanFilterException
	 */
	public static Map<String, Object> buildRepositoryMap(RepositoryItem repItem, Map<String, StringList> filter, String... addAttributes)
			throws JSONException, RepositoryException, BeanFilterException {

		Map<String, Object> output = new HashMap<>();

		String itemDescriptor = repItem.getItemDescriptor().getItemDescriptorName();

		if (filter != null && filter.containsKey(itemDescriptor)) {
			// This item descriptor has a filter set. Let's loop through only
			// these attributes

			// Let's merge optional filter Array (addAttributes) with mandatory
			// filter array (filter) to form the final filter Array
			String[] filterArray = (String[]) ArrayUtils.addAll(filter.get(itemDescriptor).getStrings(), addAttributes);
			// Now, loop through merged filterArray
			for (String lFilterData : filterArray) {
				StringTokenizer lTokenizeData = new StringTokenizer(lFilterData, "-");
				String propName = null;
				String lOrginalPropName = lTokenizeData.nextToken();
				String lCustomizer = lTokenizeData.hasMoreTokens() ? lTokenizeData.nextToken() : null;
				String lNewPropName = lTokenizeData.hasMoreTokens() ? lTokenizeData.nextToken() : null;

				if (repItem.getItemDescriptor().hasProperty(lOrginalPropName)) {
					Object propValue = repItem.getPropertyValue(lOrginalPropName);

					propName = lNewPropName != null ? lNewPropName : lOrginalPropName;

					if (lCustomizer != null && !lCustomizer.equals("none")) {
						PropertyCustomizer lPropertyCustomizer = mCommonHelper.getPropertyCustomizers().get(lCustomizer);
						if (lPropertyCustomizer == null)
							throw new BeanFilterException("No filter found for specified key.");

						output.put(propName, lPropertyCustomizer.getPropertyValue(repItem, lOrginalPropName, null));
					} else if (propValue instanceof RepositoryItem) {
						output.put(propName, buildRepositoryMap((RepositoryItem) propValue, filter));
					} else if (propValue instanceof ChangeAwareList) {
						// ChangeAwareList is usually a List of Repository Items
						ChangeAwareList repList = (ChangeAwareList) propValue;
						List<Object> innerOutput = new ArrayList<>();
						for (int i = 0; i < repList.size(); i++) {
							Object repListitem = repList.get(i);
							if (repListitem instanceof RepositoryItem) {
								innerOutput.add(buildRepositoryMap((RepositoryItem) repListitem, filter));
							} else {
								innerOutput.add(repListitem);
							}
						}
						output.put(propName, innerOutput);
					} else if (propValue instanceof ChangeAwareSet) {
						// ChangeAwareList is usually a List of Repository Items
						ChangeAwareSet repList = (ChangeAwareSet) propValue;
						Set<Object> innerOutput = new HashSet<>();
						for (Object repListitem : repList) {
							if (repListitem instanceof RepositoryItem) {
								innerOutput.add(buildRepositoryMap((RepositoryItem) repListitem, filter));
							} else {
								innerOutput.add(repListitem);
							}
						}
						output.put(propName, innerOutput);
					} else if (propValue != null) {
						if (propValue instanceof String)
							output.put(propName, propValue.toString().trim());
						else
							output.put(propName, propValue);
					}
				}
			}
		}

		return output;
	}
	
	/**
	 * Check and get string array from input array list of objects.
	 *
	 * @param arrayListObject the list of string as objects
	 * @return the string[]
	 */
	public static String[] checkAndGetStringArray(Object arrayListObject) {
		String[] resultArray = null;
		if (arrayListObject != null && arrayListObject instanceof ArrayList) {
			List<Object> objectList = (List<Object>) arrayListObject;
			resultArray = objectList.stream().toArray(String[]::new);
		}
		return resultArray;
	}

}
