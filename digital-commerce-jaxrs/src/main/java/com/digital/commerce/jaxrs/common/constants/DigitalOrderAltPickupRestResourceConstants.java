package com.digital.commerce.jaxrs.common.constants;

/**
 * @author TAIS
 *
 */
public class DigitalOrderAltPickupRestResourceConstants extends DigitalJaxRsResourceCommonConstants {

	public static final String ADD_ALT_ORDER_PICKUP = "addAltPickUpPerson";
	
	public static final String ALT_PICKUP_FIRST_NAME = "altPickupFirstName";

	public static final String ALT_PICKUP_LAST_NAME = "altPickupLastName";

	public static final String ALT_PICKUP_EMAIL = "altPickupEmail";

	public static final String FNAME = "fname";

	public static final String LNAME = "lname";

	public static final String ALT_ORDER_PICKUP_FORMHANDLER_COMPONENT_NAME = "/com/digital/commerce/services/v1_0/order/purchase/DigitalAltOrderPickupFormHandler";

	public static final String REMOVE_ALT_ORDER_PICKUP = "removeAltPickUpPerson";


}
