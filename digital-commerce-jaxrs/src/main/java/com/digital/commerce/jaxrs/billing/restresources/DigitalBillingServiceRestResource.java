package com.digital.commerce.jaxrs.billing.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.services.order.billing.DigitalBillingAddressFormHandler;

import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.annotation.Hidden;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class will provide RestResource for Billing Services.
 *
 * @author TAIS
 *
 */
@RestResource(id = "com.digital.commerce.jaxrs.billing.restresources.DigitalBillingServiceRestResource")
@Path("/billingService")
@Api("Billing Service")
@Getter
@Setter
public class DigitalBillingServiceRestResource extends DigitalGenericService {

	/**
	 * mCommonHelper - DigitalCommonRestResourceHelper Component
	 */
	private DigitalCommonRestResourceHelper commonHelper;

	@Hidden
	@GET
	@Endpoint(id = "/billingService/#GET", isSingular = true, filterId = "common-response-schema")
	public Response getBillingService() {

		Map<String, Object> lResponse = new HashMap<String, Object>();

		return Response.ok(lResponse).build();
	}

	/**
	 * Service to create add or update billing address.
	 *
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/order/purchase/DigitalBillingServiceActor/addOrUpdateBillingAddress
	 *
	 * @return Response
	 */

	@POST
	@Path("/addOrUpdateBillingAddress")
	@Endpoint(id = "/shipping/addOrUpdateBillingAddress#POST", isSingular = true, filterId = "common-response-schema")
	public Response addOrUpdateBillingAddress(JSONObject pRequest) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method addOrUpdateBillingAddress");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();

		setRequestedTime();

		try {
			DynamoHttpServletRequest lServletRequest = ServletUtil.getCurrentRequest();
			for(Map.Entry<String, Object> lRequest:lRequestPayload.entrySet()){
				lServletRequest.setParameter(lRequest.getKey(), lRequest.getValue());
			}

			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalJaxRsResourceCommonConstants.BILLING_SERVICE_FORMHANDLER,
					DigitalJaxRsResourceCommonConstants.BILLING_ADDRESS);

			FormHandlerInvocationResult lResult = executor.execute();

			DigitalBillingAddressFormHandler lBillingServiceFormHandler = (DigitalBillingAddressFormHandler) lResult.getFormHandler();

			if (lResult.isFormError()) {
				lResponseMap = getCommonHelper().createGenericErrorResponse(lResult);
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			} else {
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.BILLING_ADDRESS, lBillingServiceFormHandler.getBillingServiceResponse().getAddress());
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE,lResponseMap);
			}
		} catch (ServletException e) {
			vlogError(e, "ServletException");
		}
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addOrUpdateBillingAddress");
		}

		return Response.ok(lFinalResponse).build();
	}
}
