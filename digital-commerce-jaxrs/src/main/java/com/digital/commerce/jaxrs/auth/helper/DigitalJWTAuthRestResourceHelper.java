package com.digital.commerce.jaxrs.auth.helper;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import com.digital.commerce.jaxrs.common.constants.DigitalJWTAuthRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;

import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.GenericService;
import lombok.Getter;
import lombok.Setter;

/**
 * This class provides DigitalJWTAuthActor's chain ids implementations.
 *
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalJWTAuthRestResourceHelper extends GenericService {

	/**
	 * commonHelper - DigitalCommonRestResourceHelper Component
	 */
	private DigitalCommonRestResourceHelper commonHelper;


	/**
	 * @param pResult
	 * @param pRequestPayload
	 * @return
	 */
	public Map<String, Object> validateJWTTokenResponse() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method validateJWTTokenResponse");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();

		FormHandlerExecutor executor;
		try {
			executor = new FormHandlerExecutor(DigitalJWTAuthRestResourceConstants.JWT_AUTH_HANDLER_COMPONENT_NAME,
					DigitalJWTAuthRestResourceConstants.VALIDATE_JWT_TOKEN);

			FormHandlerInvocationResult lResult = executor.execute();

			if (lResult.isFormError()) {
				// Error
				lResponseMap = getCommonHelper().createGenericErrorResponse(lResult);
			} else {
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.STATUS, DigitalJaxRsResourceCommonConstants.SUCCESS_L);
			}
		} catch (ServletException e) {
			vlogError(e.toString(), " JSONException | ServletException | RepositoryException");
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method validateJWTTokenResponse");
		}
		return lResponseMap;
	}
}
