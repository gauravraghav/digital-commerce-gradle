package com.digital.commerce.jaxrs.promotions.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.promotions.helper.DigitalCouponRestResourceHelper;

import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class provides CouponRestResource responses for coupon actor
 *
 * @author TAIS
 *
 */
@RestResource(id = "com.digital.commerce.jaxrs.promotions.restresources.DigitalCouponRestResource")
@Path("/coupon")
@Api("coupon Service")
@Getter
@Setter
public class DigitalCouponRestResource extends DigitalGenericService {

	/**
	 * couponHelper - DigitalCouponRestResourceHelper Component
	 */
	public DigitalCouponRestResourceHelper couponHelper;

	/**
	 * Service to provide AvailableSavings details.
	 *
	 * @see - Actor Chain -
	 *      /atg/commerce/promotion/CouponActor/getAvailableSavings
	 *
	 * @return Response
	 */
	@SuppressWarnings("unchecked") // OK
	@GET
	@Path("/getAvailableSavings")
	@Endpoint(id = "/coupon/getAvailableSavings#GET", isSingular = true, filterId = "dsw-availablesavings-schema")
	public Response getAvailableSavings(@QueryParam("loadOnlyGlobalPromo") Boolean pLoadOnlyGlobalPromo) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getAvailableSavings");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		setRequestedTime();

		Map<String, Object> lAvailableSavings;
		try {
			lAvailableSavings = getCouponHelper().populateAvailableSavings(pLoadOnlyGlobalPromo, null);

			if (lAvailableSavings != null) {
				if (lAvailableSavings.containsKey(DigitalJaxRsResourceCommonConstants.ERROR_SMALL)) {
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
							lAvailableSavings.get(DigitalJaxRsResourceCommonConstants.ERROR_SMALL));
				}
				if (lAvailableSavings.containsKey(DigitalJaxRsResourceCommonConstants.SUCCESS)) {
					Map<String, Object> lSortedPromotions = (Map<String, Object>) lAvailableSavings
							.get(DigitalJaxRsResourceCommonConstants.SUCCESS);
					lResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lSortedPromotions);
				}
			}
		} catch (Exception e) {
			vlogError(e, "Repository Exception and json Exception occured");

		}
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getAvailableSavings");
		}

		return Response.ok(lResponseMap).build();
	}

	/**
	 * Service to provide AvailableSavings details.
	 *
	 * @see - Actor Chain -
	 *      /atg/commerce/promotion/CouponActor/getAvailableSavingsV2
	 *
	 * @return Response
	 */
	@GET
	@Path("/getAvailableSavingsV2")
	@Endpoint(id = "/coupon/getAvailableSavingsV2#GET", isSingular = true, filterId = "dsw-availablesavingsV2-schema")
	public Response getAvailableSavingsV2() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getAvailableSavingsV2");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		setRequestedTime();

		Map<String, Object> lAvailableSavingsV2 = getCouponHelper().populateAvailableSavingsV2();

		if (lAvailableSavingsV2 != null) {
			if (lAvailableSavingsV2.containsKey(DigitalJaxRsResourceCommonConstants.ERROR_SMALL)) {
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE,
						lAvailableSavingsV2.get(DigitalJaxRsResourceCommonConstants.ERROR_SMALL));
			}
			if (lAvailableSavingsV2.containsKey(DigitalJaxRsResourceCommonConstants.SUCCESS)) {
				Map<String, Object> lSortedPromotions = (Map<String, Object>) lAvailableSavingsV2
						.get(DigitalJaxRsResourceCommonConstants.SUCCESS);
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lSortedPromotions);
			}
		}
		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getAvailableSavingsV2");
		}
		return Response.ok(lResponseMap).build();
	}

}
