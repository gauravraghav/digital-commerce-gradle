package com.digital.commerce.jaxrs.order.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalOrderLookupRestResourceConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.jaxrs.order.helper.DigitalOrderLookupRestResourceHelper;

import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * @author TAIS
 *
 */

@RestResource(id = "com.digital.commerce.jaxrs.order.restresources.DigitalOrderLookupRestResource")
@Path("/orderLookup")
@Api("OrderLookup Service")
@Getter
@Setter
public class DigitalOrderLookupRestResource extends DigitalGenericService {
	/**
	 * orderLookupHelper - DigitalOrderLookupRestResourceHelper Component
	 */
	private DigitalOrderLookupRestResourceHelper orderLookupHelper;

	/**
	 * Service to getOrderInfo.
	 * 
	 * @see - Actor Chain - /com/digital/commerce/order/DigitalOrderLookupRestResource/orderLookup/getOrderInfo
	 * 
	 * @return Response
	 */
	@GET
	@Path("/getOrderInfo")
	@Endpoint(id = "/orderLookup/getOrderInfo#GET", isSingular = true, filterId = "dsw-constants-response")
	public Response getOrderInfo() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getOrderInfo");
		}

		Map<String, Object> lResponse = new HashMap<String, Object>();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getOrderInfo");
		}
		return Response.ok(lResponse).build();
	}

	/**
	 * Service to orderDashboard details.
	 * 
	 * @see - Actor Chain - /com/digital/commerce/order/DigitalOrderLookupRestResource/orderLookup/orderDashboard
	 * 
	 * @return Response
	 */
	@GET
	@Path("/orderDashboard")
	@Endpoint(id = "/orderLookup/orderDashboard#GET", isSingular = true, filterId = "dsw-orderLookup-response")
	public Response orderDashboard() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method orderDashboard");
		}

		Map<String, Object> lFinalResponse = new HashMap<String, Object>();

		lFinalResponse = getOrderLookupHelper().populateOrderDashboard();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method orderDashboard");
		}
		return Response.ok(lFinalResponse).build();
	}
	
	/**
	 * Service for fetch list of old orders
	 * 
	 * @see - Actor Chain -
	 *      /atg/commerce/order/OrderLookupActor/orderHistory
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/orderHistoryDetails")
	@Endpoint(id = "/orderLookup/orderHistoryDetails#POST", isSingular = true, filterId = "dsw-order-history-summary-response")
	public Response orderHistoryDetails(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method orderHistoryDetails");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
		if (isLoggingDebug()) {
			logDebug("Request for orderHistoryDetails " + lRequestMap);
		}
		response = getOrderLookupHelper().populateOrderHistoryDetails(lRequestMap);
		lFinalResponse.put(DigitalOrderLookupRestResourceConstants.RESPONSE, response);
			
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addMissingPoints");
		}
		return Response.ok(lFinalResponse).build();
	}
	
	/**
	 * Service for fetch list of Order Items Details
	 * 
	 * @see - Actor Chain -
	 *      /rest/model/atg/commerce/order/OrderLookupActor/orderLookup
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@GET
	@Path("/orderLookupDetail")
	@Endpoint(id = "/orderLookup/orderLookupDetail#GET", isSingular = true, filterId = "dsw-order-lookup-detail-response")
	public Response orderLookupDetail(@QueryParam(DigitalOrderLookupRestResourceConstants.ORDER_ID) String pOrderId,
			@QueryParam(DigitalOrderLookupRestResourceConstants.EMAIL) String pEmail,
			@QueryParam(DigitalOrderLookupRestResourceConstants.REPRICE_ORDER_FLAG) String pRepriceOrderFlag) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method orderLookupDetail");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Map<String, Object> response = new HashMap<String, Object>();
		if (isLoggingDebug()) {
			logDebug("Request for orderLookupDetail pEmail :" + pEmail + "Order Id :" + pOrderId + "pRepriceOrderFlag" + pRepriceOrderFlag);
		}
		response = getOrderLookupHelper().populateOrderLookupDetail(pOrderId,pEmail,pRepriceOrderFlag);
		lFinalResponse.put(DigitalOrderLookupRestResourceConstants.RESPONSE, response);
			
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method orderLookupDetail");
		}
		return Response.ok(lFinalResponse).build();
	}
	
	/**
	 * Service for fetch list of old orders
	 * 
	 * @see - Actor Chain -
	 *      /rest/model/atg/commerce/order/OrderLookupActor/dswOrderLookup
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/dswOrderLookupDetails")
	@Endpoint(id = "/orderLookup/dswOrderLookupDetails#POST", isSingular = true, filterId = "dsw-dsworder-lookup-details-schema")
	public Response dswOrderLookupDetails(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method dswOrderLookupDetails");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
		if (isLoggingDebug()) {
			logDebug("Request for dswOrderLookupDetails " + lRequestMap);
		}
		response = getOrderLookupHelper().populateDSWOrderLookupDetails(lRequestMap);
		lFinalResponse.put(DigitalOrderLookupRestResourceConstants.RESPONSE, response);
			
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method dswOrderLookupDetails");
		}
		return Response.ok(lFinalResponse).build();
	}
}
