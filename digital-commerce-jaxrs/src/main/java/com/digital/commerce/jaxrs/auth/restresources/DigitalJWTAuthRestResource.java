package com.digital.commerce.jaxrs.auth.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.digital.commerce.jaxrs.auth.helper.DigitalJWTAuthRestResourceHelper;
import com.digital.commerce.jaxrs.common.constants.DigitalJWTAuthRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.services.v1_0.auth.jwt.JWTAuthFormHandler;

import atg.commerce.CommerceException;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.GenericService;
import atg.nucleus.annotation.Hidden;
import atg.service.jaxrs.RestException;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class provides DigitalJWTAuthActor's chain ids implementations.
 *
 * @author TAIS
 *
 */

@RestResource(id = "com.digital.commerce.jaxrs.auth.restresources.DigitalJWTAuthRestResource")
@Path("/auth")
@Api("auth Service")
@Getter
@Setter
public class DigitalJWTAuthRestResource extends GenericService {
	/**
	 * authHelper - DigitalJWTAuthRestResourceHelper Component
	 */
	private DigitalJWTAuthRestResourceHelper authHelper;
	
	@Hidden
	@GET
	@Endpoint(id = "/auth/#GET", isSingular = true, filterId = "common-response-schema")
	public Response getJWTToken() throws RestException, CommerceException {
		return Response.ok(null).build();
	}

	/**
	 * Service to generate JWTToken details.
	 *
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/auth/jwt/DigitalJWTAuthActor/generateJWTToken
	 *
	 * @return Response
	 */
	@POST
	@Path("/generateJWTToken")
	@Endpoint(id = "/auth/generateJWTToken#POST", isSingular = true, filterId = "common-response-schema")
	public Response generateJWTToken(JSONObject pRequest) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method generateJWTToken");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lFinalResponse = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pRequest);

		FormHandlerExecutor executor;
		try {
			executor = new FormHandlerExecutor(DigitalJWTAuthRestResourceConstants.JWT_AUTH_HANDLER_COMPONENT_NAME,
					DigitalJWTAuthRestResourceConstants.GENERATE_JWT_TOKEN);

			executor.addInput(DigitalJWTAuthRestResourceConstants.USER_NAME, lRequestPayload.get(DigitalJWTAuthRestResourceConstants.CLIENT_ID));
			executor.addInput(DigitalJWTAuthRestResourceConstants.PASSWORD, lRequestPayload.get(DigitalJWTAuthRestResourceConstants.CLIENT_SECRET));
			executor.addInput(DigitalJWTAuthRestResourceConstants.SOURCE, lRequestPayload.get(DigitalJWTAuthRestResourceConstants.AUDIENCE));

			FormHandlerInvocationResult lResult = executor.execute();
			JWTAuthFormHandler lJWTAuthFormHandler = (JWTAuthFormHandler) lResult.getFormHandler();
			if (lResult.isFormError()) {
				// Error
				lResponseMap = getAuthHelper().getCommonHelper().createGenericErrorResponse(lResult);
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			} else {
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.TOKEN, lJWTAuthFormHandler.getToken());
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
			}

		} catch (ServletException e) {
			vlogError(e.toString(), " ServletException ");
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method generateJWTToken");
		}
		return Response.ok(lFinalResponse).build();
	}
}
