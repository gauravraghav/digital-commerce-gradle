package com.digital.commerce.jaxrs.common.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;

import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.services.profile.DigitalProfileTools;

import atg.commerce.order.Order;
import atg.core.i18n.LayeredResourceBundle;
import atg.droplet.DropletException;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceMap;
import atg.service.filter.bean.PropertyCustomizer;
import atg.service.jaxrs.DropletInvoker;
import atg.service.jaxrs.OParam;
import atg.service.jaxrs.Output;
import atg.servlet.ServletUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalCommonRestResourceHelper extends GenericService {

	/**
	 * propertyCustomizers - Property Customizers
	 */
	private ServiceMap<PropertyCustomizer> propertyCustomizers;
	
	/**
     * profileTools - DigitalProfileTools component
     */
    private DigitalProfileTools profileTools;
    
    private DigitalBaseConstants dswConstants;
	
	/**
     * serviceResourceBundle - ServicesResource component holding static keys
     */
    public static final ResourceBundle SERVICE_RESOURCE_BUNDLE = LayeredResourceBundle.getBundle(DigitalJaxRsResourceCommonConstants.SERVICES_RESOURCE_BUNDLE,
	    ServletUtil.getUserLocale());

	/**
	 * Service to provide suppressed stackstrace.
	 *
	 * @return Map<String,String>
	 */
	public List<Map<String, String>> createFormExceptionResponse(Collection pFormExceptions) {
		@SuppressWarnings("unchecked")
		Vector<DropletException> lFormExceptions = (Vector<DropletException>) pFormExceptions;
		List<Map<String, String>> lFormExceptionMap = new ArrayList<>();
		for (DropletException exception : lFormExceptions) {
			Map<String, String> lExceptionMap = new HashMap<>();
			lExceptionMap.put(DigitalJaxRsResourceCommonConstants.ERROR_CODE, exception.getErrorCode());
			lExceptionMap.put(DigitalJaxRsResourceCommonConstants.LOCALIZED_MESSAGE, exception.getLocalizedMessage());
			lFormExceptionMap.add(lExceptionMap);
		}

		return lFormExceptionMap;
	}

	/**
	 * Common method to populate closeness qualifier based on type passed
	 *
	 * @param pOrder
	 *            Current Order
	 * @param pType
	 *            Type of qualifier
	 * @return
	 */
	public Object populateClosenessQualifiers(Order pOrder, String pType) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method populateClosenessQualifiers()");
		}
		DropletInvoker lClosenessQualifierDropletInvoker = new DropletInvoker(DigitalJaxRsResourceCommonConstants.CLOSE_QUALIFIER_DROPLET);
		lClosenessQualifierDropletInvoker.addInput("type", pType == null ? "all" : pType);
		lClosenessQualifierDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.ORDER, pOrder);
		OParam oParamOutput = lClosenessQualifierDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
		OParam oParamError = lClosenessQualifierDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.ERROR);

		Output lSuccessOutput = oParamOutput.addOutput(DigitalJaxRsResourceCommonConstants.CLOSENESS_QUALIFIERS,
				DigitalJaxRsResourceCommonConstants.CLOSENESS_QUALIFIERS);
		Output lErrorOutput = oParamError.addOutput(DigitalJaxRsResourceCommonConstants.ERROR_MSG, DigitalJaxRsResourceCommonConstants.ERROR_MSG);

		lClosenessQualifierDropletInvoker.invoke();

		if (lSuccessOutput.getObject() != null) {
			return lSuccessOutput.getObject();
		} else if (lErrorOutput.getObject() != null) {
			return lErrorOutput.getObject();
		} else {
			Map<String, String> lEmptyMap = new HashMap<>();
			lEmptyMap.put("empty", "[]");
			return lEmptyMap;
		}
	}
	/**
	 * This method is used to create generic error response.
	 *
	 * @param pResponseMap
	 * @param pResult
	 *
	 */
	public Map<String, Object> createGenericErrorResponse(FormHandlerInvocationResult pResult) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method errorResponse");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		List<Map<String, String>> lSupressedStacktrace = createFormExceptionResponse(pResult.getFormExceptions());

		lResponseMap.put(DigitalJaxRsResourceCommonConstants.FORM_ERROR, true);
		lResponseMap.put(DigitalJaxRsResourceCommonConstants.FORM_EXCEPTION, lSupressedStacktrace);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method errorResponse");
		}
		return lResponseMap;

	}
	
	/**
	 * Invoke reprice order droplet for reprice order.
	 *
	 * @param pOrder
	 *            the order
	 * @param mPricingOperation
	 *            the pricing operation name
	 */
	public void invokeRepriceOrderDroplet(Order pOrder, Object mPricingOperation) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method invokeRepriceOrderDroplet");
		}
		DropletInvoker lRepriceOrder = new DropletInvoker(DigitalJaxRsResourceCommonConstants.REPRICE_ORDER_DROPLET_PATH);
		lRepriceOrder.addInput(DigitalJaxRsResourceCommonConstants.PRICING_OP, mPricingOperation);
		lRepriceOrder.invoke();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method invokeRepriceOrderDroplet");
		}
	}
}
