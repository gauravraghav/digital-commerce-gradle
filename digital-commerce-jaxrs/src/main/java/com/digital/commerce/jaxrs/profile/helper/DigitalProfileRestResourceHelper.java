package com.digital.commerce.jaxrs.profile.helper;

import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.json.JSONException;
import org.json.JSONObject;

import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.integration.common.exception.DigitalIntegrationException;
import com.digital.commerce.jaxrs.common.constants.DigitalGiftListRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalProfileRestResourceContants;
import com.digital.commerce.jaxrs.common.constants.DigitalShippingGroupRestResourceContants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.jaxrs.gifts.helper.DigitalGiftListRestResourceHelper;
import com.digital.commerce.jaxrs.order.helper.DigitalOrderLookupRestResourceHelper;
import com.digital.commerce.jaxrs.promotions.helper.DigitalCouponRestResourceHelper;
import com.digital.commerce.jaxrs.rewards.helper.DigitalRewardsRestResourceHelper;
import com.digital.commerce.services.common.DigitalAddress;
import com.digital.commerce.services.gifts.DigitalGiftlistManager;
import com.digital.commerce.services.profile.DigitalProfileTools;
import com.digital.commerce.services.profile.handler.DigitalProfileFormHandler;
import com.digital.commerce.services.region.RegionProfile;
import com.digital.commerce.services.storelocator.DigitalStoreLocatorModel;

import atg.commerce.order.OrderHolder;
import atg.core.util.StringUtils;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.jaxrs.DropletInvoker;
import atg.service.jaxrs.OParam;
import atg.service.jaxrs.Output;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import atg.userprofiling.address.AddressTools;
import lombok.Getter;
import lombok.Setter;

/**
 * This class is used to provide response for login services
 *
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalProfileRestResourceHelper extends GenericService {

	/**
	 * profileTools - DigitalProfileTools Component
	 */
	private DigitalProfileTools profileTools;

	/**
	 * commonHelper - DigitalCommonRestResourceHelper Component
	 */
	private DigitalCommonRestResourceHelper commonHelper;

	/**
	 * mCoupanHelper - DigitalCouponRestResourceHelper Component
	 */
	private DigitalCouponRestResourceHelper couponHelper;

	/**
	 * orderLookupHelper - DigitalOrderLookupRestResourceHelper Component
	 */
	private DigitalOrderLookupRestResourceHelper orderLookupHelper;

	/**
	 * rewardsHelper - DigitalRewardsRestResourceHelper Component
	 */
	private DigitalRewardsRestResourceHelper rewardsHelper;

	/**
	 * giftlistManager - DigitalGiftlistManager Component
	 */
	private DigitalGiftlistManager giftlistManager;

	/**
	 * giftListHelper - DigitalGiftListRestResourceHelper Component
	 */
	private DigitalGiftListRestResourceHelper giftListHelper;

	/**
	 * Populate LoginV3 service Success Response
	 *
	 * @param pRequestPayload
	 * @return lResponseMap
	 */
	@SuppressWarnings("unchecked")
	public Object populateLoginSuccessResponseV3(Map<String, Object> pRequestPayload) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateLoginSuccessResponseV3");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		String lErrorMessage;

		Boolean lSkipOrdersCheckout = pRequestPayload.containsKey(DigitalProfileRestResourceContants.CHECKOUT) ? (boolean) pRequestPayload.get(DigitalProfileRestResourceContants.CHECKOUT):false;
		Boolean lSkipFavStore =pRequestPayload.containsKey(DigitalProfileRestResourceContants.SKIP_FEV_STORE) ? (boolean) pRequestPayload.get(DigitalProfileRestResourceContants.SKIP_FEV_STORE)
				: false;

		Boolean lSkipOrders = pRequestPayload.containsKey(DigitalProfileRestResourceContants.SKIP_ORDERS) ? (boolean) pRequestPayload.get(DigitalProfileRestResourceContants.SKIP_ORDERS):false;

		DropletInvoker dropletInvoker = new DropletInvoker(DigitalProfileRestResourceContants.USER_INFO_DROPLET);
		dropletInvoker.addInput(DigitalProfileRestResourceContants.SKIP_CERTS, true);
		dropletInvoker.addInput(DigitalProfileRestResourceContants.SKIP_ORDERS, lSkipOrders);
		dropletInvoker.addInput(DigitalProfileRestResourceContants.SKIP_FEV_STORE, lSkipFavStore);
		dropletInvoker.addInput(DigitalProfileRestResourceContants.SKIP_REWARD_DEATIL, false);
		dropletInvoker.addInput(DigitalProfileRestResourceContants.SYNC_REWARD, false);

		OParam oParamOutput = dropletInvoker.addOParam(DigitalProfileRestResourceContants.OUTPUT);
		Output lSuccessOutput = oParamOutput.addOutput(DigitalProfileRestResourceContants.DSW_USER_INFO,
				DigitalProfileRestResourceContants.DSW_USER_INFO);

		OParam oErrorParam = dropletInvoker.addOParam(DigitalProfileRestResourceContants.ERROR_SMALL);
		Output lErrorOutput = oErrorParam.addOutput(DigitalProfileRestResourceContants.ERROR_MSG, DigitalProfileRestResourceContants.ERROR_MSG);

		dropletInvoker.invoke();

		if (lSuccessOutput.getObject() != null) {
			lResponseMap = (Map<String, Object>) lSuccessOutput.getObject();
			Boolean lSkipSavings = pRequestPayload.containsKey(DigitalProfileRestResourceContants.SKIP_SAVING) ? (boolean) pRequestPayload.get(DigitalProfileRestResourceContants.SKIP_SAVING)
					: false;
			if (!lSkipSavings) {
				Map<String, Object> lAvailableSavings = getCouponHelper().populateAvailableSavingsV2();
				if (lAvailableSavings != null) {
					if (lAvailableSavings.containsKey(DigitalJaxRsResourceCommonConstants.ERROR_SMALL)) {
						lResponseMap.put(DigitalJaxRsResourceCommonConstants.AVAILABLE_SAVINGS,
								lAvailableSavings.get(DigitalJaxRsResourceCommonConstants.ERROR_SMALL));
					}
					if (lAvailableSavings.containsKey(DigitalJaxRsResourceCommonConstants.SUCCESS)) {
						lResponseMap.put(DigitalJaxRsResourceCommonConstants.AVAILABLE_SAVINGS,
								lAvailableSavings.get(DigitalJaxRsResourceCommonConstants.SUCCESS));
					}
				}
			}
			lResponseMap.put(DigitalProfileRestResourceContants.STATUS_CODE, DigitalProfileRestResourceContants.SUCCESS);
			return lResponseMap;
		} else {
			lErrorMessage = (String) lErrorOutput.getObject();
			if (isLoggingDebug()) {
				logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateLoginSuccessResponseV3");
			}
			return lErrorMessage;
		}
	}

	/**
	 * + Populate LoginV2 service Success Response
	 *
	 * @param pRequestPayload
	 * @return lResponseMap
	 * @throws Exception
	 */
	public Object populateloginV2SuccessResponse(Map<String, Object> pRequestPayload) throws Exception {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateloginV2SuccessResponse");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		// get-UserInfo
		lResponseMap.putAll(getUserInfo(pRequestPayload, lProfile,
				ComponentLookupUtil.lookupComponent(DigitalProfileRestResourceContants.SHOPPING_CART, OrderHolder.class)));
		String lLoyalityNumber = (String) lProfile.getPropertyValue(DigitalProfileRestResourceContants.LOYALTY_NUMBER);
		lResponseMap.put(DigitalProfileRestResourceContants.LOYALTY_NUMBER, lLoyalityNumber);
		if (StringUtils.isNotEmpty(lLoyalityNumber)) {
			lResponseMap.put(DigitalProfileRestResourceContants.REWARD_DETAILS, lProfile.getDataSource());
		}
		lResponseMap.put(DigitalProfileRestResourceContants.STATUS_CODE, DigitalProfileRestResourceContants.SUCCESS);

		Map<String, Object> lAvailableSavings = getCouponHelper().populateAvailableSavings(null, null);
		if (lAvailableSavings != null) {
			if (lAvailableSavings.containsKey(DigitalJaxRsResourceCommonConstants.ERROR_SMALL)) {
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.AVAILABLE_SAVINGS,
						lAvailableSavings.get(DigitalJaxRsResourceCommonConstants.ERROR_SMALL));
			}
			if (lAvailableSavings.containsKey(DigitalJaxRsResourceCommonConstants.SUCCESS)) {
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.AVAILABLE_SAVINGS,
						lAvailableSavings.get(DigitalJaxRsResourceCommonConstants.SUCCESS));
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateloginV2SuccessResponse");
		}
		return lResponseMap;
	}

	/**
	 * Service to getUserInfo details
	 *
	 * @param pRequestPayload
	 * @param pProfile
	 * @param pShoppingCart
	 * @throws Exception
	 *
	 */
	private Map<String, Object> getUserInfo(Map<String, Object> pRequestPayload, Profile pProfile, OrderHolder pShoppingCart)
			throws Exception {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getUserInfo");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Boolean lIstransient = getProfileTools().isDSWAnanymousUser(pProfile);
		lResponseMap.put(DigitalProfileRestResourceContants.ANONYMOUS_USER, lIstransient);

		DropletInvoker dropletInvoker = new DropletInvoker(DigitalProfileRestResourceContants.INSTORE_DEVICE_DROPLET);
		OParam oParamOutput = dropletInvoker.addOParam(DigitalProfileRestResourceContants.OUTPUT);
		Output lSuccessOutput = oParamOutput.addOutput(DigitalProfileRestResourceContants.IS_INSTORE_DEVICE,
				DigitalProfileRestResourceContants.IS_INSTORE_DEVICE);

		dropletInvoker.invoke();

		if (lSuccessOutput.getObject() != null) {
			lResponseMap.put(DigitalProfileRestResourceContants.IS_INSTORE_DEVICE, lSuccessOutput.getObject());
		}
		Integer lProfileSecurityStatus = (Integer) pProfile.getPropertyValue(DigitalProfileRestResourceContants.SECURITY_STATUS);
		lResponseMap.put(DigitalProfileRestResourceContants.DSW_VISA_MEMBER, getProfileTools().getCCFlag(pProfile));
		lResponseMap.put(DigitalProfileRestResourceContants.COOKIED_USER, lProfileSecurityStatus == 2);
		lResponseMap.put(DigitalProfileRestResourceContants.TOTAL_BAG_COUNT, pShoppingCart.getCurrent().getTotalCommerceItemCount());
		lResponseMap.put(DigitalProfileRestResourceContants.CURRENT_ORDER_ID, pShoppingCart.getCurrent().getId());
		lResponseMap.put(DigitalProfileRestResourceContants.PROFILE_ID, pProfile.getRepositoryId());
		lResponseMap.put(DigitalProfileRestResourceContants.LOGIN, pProfile.getProfileTools().getLogin(pProfile));

		Boolean lSkipFavStore = pRequestPayload.get(DigitalProfileRestResourceContants.SKIP_FEV_STORE) == null ? false
				: (boolean) pRequestPayload.get(DigitalProfileRestResourceContants.SKIP_FEV_STORE);
		if (!lSkipFavStore) {
			RegionProfile lregionProfile = ComponentLookupUtil.lookupComponent(DigitalProfileRestResourceContants.REGION_PROFILE,
					RegionProfile.class);
			Double lLatitude = (Double) pRequestPayload.get(DigitalProfileRestResourceContants.LATITUDE);
			Double lLongitude = (Double) pRequestPayload.get(DigitalProfileRestResourceContants.LONGNITUDE);
			DigitalStoreLocatorModel lFavoriteStoreDetails = null;
			lFavoriteStoreDetails = getProfileTools().getUserFavoriteStore(pProfile, lLatitude, lLongitude, lregionProfile);
			lResponseMap.put(DigitalProfileRestResourceContants.FAVORITE_STORE_DETAILS, lFavoriteStoreDetails);
		}
		lResponseMap.put(DigitalProfileRestResourceContants.USER_STATUS, getProfileTools().getCCFlag(pProfile));
		lResponseMap.putAll(getsecurityStatusInfo(pProfile, pRequestPayload));

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getUserInfo");
		}
		return lResponseMap;
	}

	/**
	 * Populate getsecurityStatusInfo service Success Response
	 *
	 * @param pProfile
	 * @param pRequestPayload
	 * @param pSkipOrder
	 * @throws Exception
	 */
	private Map<String, Object> getsecurityStatusInfo(Profile pProfile, Map<String, Object> pRequestPayload) throws Exception {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getsecurityStatusInfo");
		}
		Map<String, Object> lResponseMap = null;

		int lSecurityStatus = (int) pProfile.getPropertyValue(DigitalProfileRestResourceContants.SECURITY_STATUS);
		switch (lSecurityStatus) {
		case 0:
			lResponseMap = new HashMap<String, Object>();
			Map<String, Long> lWishlistCount = new HashMap<>();
			lWishlistCount.put(DigitalProfileRestResourceContants.WISHLIST_COUNT,0L);
			lResponseMap.put(DigitalProfileRestResourceContants.WISHLIST_L, lWishlistCount);
			break;
		case 2:
			lResponseMap = populateUserBasicDetails(pProfile, pRequestPayload);
			break;
		case 4:
			lResponseMap = populateUserBasicDetails(pProfile, pRequestPayload);
			lResponseMap.put(DigitalProfileRestResourceContants.LAST_NAME, pProfile.getPropertyValue(DigitalProfileRestResourceContants.LAST_NAME));
			break;
		case 5:
			lResponseMap = populateUserBasicDetails(pProfile, pRequestPayload);
			lResponseMap.put(DigitalProfileRestResourceContants.LAST_NAME, pProfile.getPropertyValue(DigitalProfileRestResourceContants.LAST_NAME));
			break;
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getsecurityStatusInfo");
		}
		return lResponseMap;

	}

	/**
	 * Populate User Basic Details
	 *
	 * @param pProfile
	 * @param pRequestPayload
	 * @param pSkipOrder
	 * @return lResponseMap
	 * @throws Exception
	 */
	private Map<String, Object> populateUserBasicDetails(Profile pProfile, Map<String, Object> pRequestPayload) throws Exception {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateUserBasicDetails");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();

		lResponseMap.put(DigitalProfileRestResourceContants.FIRST_NAME, pProfile.getPropertyValue(DigitalProfileRestResourceContants.FIRST_NAME));
		lResponseMap.put(DigitalProfileRestResourceContants.EMAIL, pProfile.getPropertyValue(DigitalProfileRestResourceContants.EMAIL));
		lResponseMap.put(DigitalProfileRestResourceContants.LOYALTY_TIER, getProfileTools().getLoyaltyTier(pProfile));
		lResponseMap.put(DigitalProfileRestResourceContants.LOYALTY_NUMBER, getProfileTools().getLoyaltyNumber(pProfile));

		if (StringUtils.isNotEmpty((String) pProfile.getPropertyValue(DigitalProfileRestResourceContants.LOYALTY_NUMBER))) {

			Boolean lSkipCerts = pRequestPayload.get(DigitalProfileRestResourceContants.SKIP_CERTS) == null ? false
					: (boolean) pRequestPayload.get(DigitalProfileRestResourceContants.SKIP_CERTS);

			if (!lSkipCerts) {
				lResponseMap.put(DigitalProfileRestResourceContants.REWARD_AMOUNT, getRewardsHelper().populateRewardsCerts());
			}
		}

		Boolean lSkipOrder = pRequestPayload.get(DigitalProfileRestResourceContants.CHECKOUT) == null ? false
				: (boolean) pRequestPayload.get(DigitalProfileRestResourceContants.CHECKOUT);
		if (!lSkipOrder) {
			Map<String, Object> lOrderDashbord = getOrderLookupHelper().populateOrderDashboard();
			if (lOrderDashbord.containsKey(DigitalProfileRestResourceContants.MY_ORDERS)) {
				lResponseMap.put(DigitalProfileRestResourceContants.MY_ORDERS, lOrderDashbord);
				lResponseMap.put(DigitalProfileRestResourceContants.TOTAL_ORDER_COUNT, lOrderDashbord);
			}
		}

		String lReturnWishlistIds = (String) pRequestPayload.get(DigitalProfileRestResourceContants.RETURN_WISHLIST_IDS);
		String lWishlistId = (String) pRequestPayload.get(DigitalProfileRestResourceContants.GIFT_LIST_ID);
		if (DigitalStringUtil.isNotBlank(lReturnWishlistIds)) {
			Map<String, Object> lwishlist = getGiftListHelper().populateWishListDetailsV3(true, null, null, lWishlistId);
			lResponseMap.put(DigitalProfileRestResourceContants.WISHLIST_L, lwishlist);
		} else {
			if (lWishlistId == null) {
				RepositoryItem lProfileWishlist = (RepositoryItem) pProfile.getPropertyValue(DigitalGiftListRestResourceConstants.WISHLIST);
				if (lProfileWishlist != null) {
					lWishlistId = lProfileWishlist.getRepositoryId();
				}
			}
			Map<String, Long> lWishlistCount = new HashMap<>();
			lWishlistCount.put(DigitalProfileRestResourceContants.WISHLIST_COUNT, getGiftlistManager().getWishlistCount(lWishlistId));
			lResponseMap.put(DigitalProfileRestResourceContants.WISHLIST_L, lWishlistCount);
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateUserBasicDetails");
		}
		return lResponseMap;

	}

	/**
	 * This method provide response corresponding to user info
	 *
	 * @param pLoginRequest
	 * @return lResponseMap
	 * @throws Exception
	 *
	 */
	public Map<String, Object> getUserInfoResponse( Map<String, Object> pRequestPayload) throws Exception {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method getUserInfoResponse");
		}

		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		OrderHolder lShoppingCart = ComponentLookupUtil.lookupComponent(DigitalProfileRestResourceContants.SHOPPING_CART,OrderHolder.class);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method getUserInfoResponse");
		}
		return getUserInfo(pRequestPayload, lProfile, lShoppingCart);
	}

	/**
	 * This method provide response corresponding to user info
	 *
	 * @param pLoginRequest
	 * @return lResponseMap
	 * @throws Exception
	 */
	public Map<String, Object> populateUserInfoResponse(JSONObject pLoginRequest) throws Exception {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateUserInfoResponse");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pLoginRequest);
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		OrderHolder lShoppingCart = (OrderHolder) Nucleus.getGlobalNucleus().resolveName(DigitalProfileRestResourceContants.SHOPPING_CART);
		lResponseMap.put(DigitalProfileRestResourceContants.SUCCESS, getUserInfo(lRequestPayload, lProfile, lShoppingCart));

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateUserInfoResponse");
		}
		return lResponseMap;
	}

	/**
	 * Populate Create chain -id Response
	 *
	 * @param pRequestPayload
	 * @return
	 * @throws ServletException
	 */
	public Object populateCreateResponse(Map<String, Object> pRequestPayload) throws ServletException {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method populateCreateResponse");
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method populateCreateResponse");
		}
		return null;
	}

	/**
	 * populate CreateV2 chain -id Response
	 *
	 * @param pLoginRequest
	 * @param pResult
	 * @return lResponseMap
	 * @throws ServletException
	 */
	public Map<String, Object> populateCreateV2Response(JSONObject pLoginRequest, FormHandlerInvocationResult pResult)
			throws ServletException {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method populateCreateV2Response");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Map<String, Object> lRequestPayload = DigitalJaxRsUtility.toMap(pLoginRequest);

		DigitalProfileFormHandler lProfieFormHandler = (DigitalProfileFormHandler) pResult.getFormHandler();
		if (pResult.isFormError()) {
			// Error
			List<Map<String, String>> lSupressedStacktrace = getCommonHelper().createFormExceptionResponse(pResult.getFormExceptions());

			if (lProfieFormHandler.getSuggestedAddressMap() != null) {
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.SUGGESTED_ADDRESSMAP, lProfieFormHandler.getSuggestedAddressMap());
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.DECISION, DigitalJaxRsResourceCommonConstants.SUGGESTED);
			}
			lResponseMap.put(DigitalJaxRsResourceCommonConstants.FORM_ERROR, true);
			lResponseMap.put(DigitalJaxRsResourceCommonConstants.FORM_EXCEPTION, lSupressedStacktrace);
		} else {
			String lSource = (String) lRequestPayload.get(DigitalProfileRestResourceContants.SOURCE);
			if (StringUtils.isEmpty(lSource)) {
				String lSuccessMessage = lProfieFormHandler.getSuccessMessage();
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.STATUS_CODE, DigitalJaxRsResourceCommonConstants.SUCCESS);
				lResponseMap.put(DigitalProfileRestResourceContants.CREATE_SUCCESS, lSuccessMessage);

				Boolean lSkipSavings = (Boolean) lRequestPayload.get(DigitalProfileRestResourceContants.SKIP_SAVING);

				if (lSkipSavings == false) {
					Map<String, Object> lAvailableSavingsV2 = getCouponHelper().populateAvailableSavingsV2();
					if (lAvailableSavingsV2 != null) {
						if (lAvailableSavingsV2.containsKey(DigitalJaxRsResourceCommonConstants.ERROR_SMALL)) {
							lResponseMap.put(DigitalJaxRsResourceCommonConstants.AVAILABLE_SAVINGS,
									lAvailableSavingsV2.get(DigitalJaxRsResourceCommonConstants.ERROR_SMALL));
						}
						if (lAvailableSavingsV2.containsKey(DigitalJaxRsResourceCommonConstants.SUCCESS)) {
							lResponseMap.put(DigitalJaxRsResourceCommonConstants.AVAILABLE_SAVINGS, lAvailableSavingsV2);
						}
					}
				}
			} else {
				String lOnlineProfileId = lProfieFormHandler.getRepositoryId();
				String lTransactionId = lProfieFormHandler.getTransactionId();
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.TRANSACTION_ID, lOnlineProfileId);
				lResponseMap.put(DigitalJaxRsResourceCommonConstants.ONLINE_PROFILE_ID, lTransactionId);
			}
		}

		return lResponseMap;
	}

	/**
	 * Service to provide get user info response.
	 *
	 * @param pSkipCerts
	 * @param pSkipFavStore
	 * @param pSkipOrders
	 * @param pSyncReward
	 * @param pSkipRewardDetails
	 * @return lResponseMap
	 */
	public Map<String, Object> populateGetUserInfoV2Response(Boolean pSkipCerts, Boolean pSkipFavStore, Boolean pSkipOrders,
			Boolean pSyncReward, Boolean pSkipRewardDetails) {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateGetUserInfoV2Response");
		}
		Map<String, Object> lResponseMap = new HashMap<>();

		Boolean lSkipCerts = (pSkipCerts == null || pSkipCerts == true ? true : false);
		Boolean lSkipOrders = (pSkipOrders == null || pSkipOrders == false ? false : true);
		Boolean lSkipFavStore = (pSkipFavStore == null || pSkipFavStore == false ? false : true);
		Boolean lSkipRewardDetails = (pSkipRewardDetails == null || pSkipRewardDetails == true ? true : false);
		Boolean lSyncReward = (pSyncReward == null || pSyncReward == true ? true : false);

		DropletInvoker dropletInvoker = new DropletInvoker(DigitalProfileRestResourceContants.USER_INFO_DROPLET);

		dropletInvoker.addInput(DigitalProfileRestResourceContants.SKIP_CERTS, lSkipCerts);
		dropletInvoker.addInput(DigitalProfileRestResourceContants.SKIP_ORDERS, lSkipOrders);
		dropletInvoker.addInput(DigitalProfileRestResourceContants.SKIP_FEV_STORE, lSkipFavStore);
		dropletInvoker.addInput(DigitalProfileRestResourceContants.SKIP_REWARD_DEATIL, lSkipRewardDetails);
		dropletInvoker.addInput(DigitalProfileRestResourceContants.SYNC_REWARD, lSyncReward);

		OParam oParamOutput = dropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
		Output lSuccessOutput = oParamOutput.addOutput(DigitalProfileRestResourceContants.DSW_USER_INFO,
				DigitalProfileRestResourceContants.DSW_USER_INFO);

		OParam oErrorParam = dropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.ERROR_SMALL);
		Output lErrorOutput = oErrorParam.addOutput(DigitalJaxRsResourceCommonConstants.ERROR_MSG, DigitalJaxRsResourceCommonConstants.ERROR_MSG);

		dropletInvoker.invoke();

		if (lErrorOutput.getObject() != null) {
			lResponseMap.put(DigitalJaxRsResourceCommonConstants.ERROR_SMALL, lErrorOutput.getObject());

		} else {
			lResponseMap.put(DigitalJaxRsResourceCommonConstants.SUCCESS, lSuccessOutput.getObject());
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateGetUserInfoV2Response");
		}

		return lResponseMap;
	}

	/**
	 * populate Logout Success Response
	 *
	 * @return lResponseMap
	 */
	public Map<String, Object> populateLogoutSuccessResponse() {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateLogoutSuccessResponse");
		}

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		lResponseMap.put(DigitalProfileRestResourceContants.STATUS_CODE, DigitalProfileRestResourceContants.SUCCESS);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateLogoutSuccessResponse");
		}
		return lResponseMap;
	}

	/**
	 * This method is use to update User favorite store.
	 *
	 * @param pPfavoriteStoreId
	 * @return lFavoriteStoreResult
	 * @throws RepositoryException
	 *
	 */
	public String updateUserFavoriteStore(String pfavoriteStoreId) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method updateUserFavoriteStore");
		}
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		String lFavoriteStoreResult = getProfileTools().updateFavoriteStore(lProfile, pfavoriteStoreId);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method updateUserFavoriteStore");
		}
		return lFavoriteStoreResult;

	}

	/**
	 * This method is use to make credit card as default.
	 *
	 * @param pCardId
	 * @param pCardName
	 * @return lDefaultCreditCard
	 * @throws RepositoryException
	 */
	public String makeItAsDefaultCreditCardResponse(String pCardId, String pCardName) throws RepositoryException {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method makeItAsDefaultCreditCardResponse");
		}
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		String lDefaultCreditCard = getProfileTools().makeItAsDefaultCrditCard(lProfile, pCardId, pCardName);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method makeItAsDefaultCreditCardResponse");
		}
		return lDefaultCreditCard;
	}

	/**
	 * This method is used to provide input to executor for update profile.
	 *
	 * @param pExecutor
	 * @param pRequestPayload
	 * @throws ServletException
	 */
	@SuppressWarnings("unchecked") // ok
	public void inputsForUpdateChain(FormHandlerExecutor pExecutor, Map<String, Object> pRequestPayload) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method inputsForUpdate");
		}
		Map<String, Object> lHomeAddressData = (Map<String, Object>) pRequestPayload.get(DigitalProfileRestResourceContants.HOME_ADDRESS);
		if(lHomeAddressData != null){
		lHomeAddressData = (Map<String, Object>) lHomeAddressData.get(DigitalProfileRestResourceContants.ATG_REST_VALUES);
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_ADDRESS1,
				lHomeAddressData.get(DigitalProfileRestResourceContants.ADDRESS1));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_ADDRESS2,
				lHomeAddressData.get(DigitalProfileRestResourceContants.ADDRESS2));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_ADDRESS3,
				lHomeAddressData.get(DigitalProfileRestResourceContants.ADDRESS3));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_CITY,
				lHomeAddressData.get(DigitalProfileRestResourceContants.CITY));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_STATE,
				lHomeAddressData.get(DigitalProfileRestResourceContants.STATE));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_COUNTRY,
				lHomeAddressData.get(DigitalProfileRestResourceContants.COUNTRY));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_JOBTITLE,
				lHomeAddressData.get(DigitalProfileRestResourceContants.JOBTITLE));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_POSTAL_CODE,
				lHomeAddressData.get(DigitalProfileRestResourceContants.POSTAL_CODE));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_FAXNUMBER,
				lHomeAddressData.get(DigitalProfileRestResourceContants.FAXNUMBER));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_FIRSTNAME,
				lHomeAddressData.get(DigitalProfileRestResourceContants.FIRSTNAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_LASTNAME,
				lHomeAddressData.get(DigitalProfileRestResourceContants.LASTNAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_MIDDLENAME,
				lHomeAddressData.get(DigitalProfileRestResourceContants.MIDDLENAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_PHONENUMBER,
				lHomeAddressData.get(DigitalProfileRestResourceContants.PHONENUMBER));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_PREFIX,
				lHomeAddressData.get(DigitalProfileRestResourceContants.PREFIX));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_SUFFIX,
				lHomeAddressData.get(DigitalProfileRestResourceContants.SUFFIX));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_COMPANYNAME,
				lHomeAddressData.get(DigitalProfileRestResourceContants.COMPANYNAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.HOMEADDRESS_HOMETYPE,
				lHomeAddressData.get(DigitalProfileRestResourceContants.ADDRESSTYPE));
		pExecutor.addInput(DigitalProfileRestResourceContants.HOMEADDRESS_REGION,
				lHomeAddressData.get(DigitalProfileRestResourceContants.REGION));
		pExecutor.addInput(DigitalProfileRestResourceContants.HOMEADDRESS_RANK,
				lHomeAddressData.get(DigitalProfileRestResourceContants.RANK));
		}
		Map<String, Object> lShippingAddressData = (Map<String, Object>) pRequestPayload
				.get(DigitalProfileRestResourceContants.SHIPPING_ADDRESS);
		if(lShippingAddressData != null){
		lShippingAddressData = (Map<String, Object>) lShippingAddressData.get(DigitalProfileRestResourceContants.ATG_REST_VALUES);
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_SHIPPING_ADDRESS_ADDRESSTYPE,
				lShippingAddressData.get(DigitalProfileRestResourceContants.ADDRESSTYPE));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_SHIPPING_ADDRESS_REGION,
				lShippingAddressData.get(DigitalProfileRestResourceContants.REGION));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_SHIPPING_ADDRESS_RANK,
				lShippingAddressData.get(DigitalProfileRestResourceContants.RANK));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_SHIPPING_ADDRESS_ADDRESS1,
				lShippingAddressData.get(DigitalProfileRestResourceContants.ADDRESS1));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_SHIPPING_ADDRESS_ADDRESS2,
				lShippingAddressData.get(DigitalProfileRestResourceContants.ADDRESS2));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_SHIPPING_ADDRESS_CITY,
				lShippingAddressData.get(DigitalProfileRestResourceContants.CITY));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_SHIPPING_ADDRESS_STATE,
				lShippingAddressData.get(DigitalProfileRestResourceContants.STATE));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_SHIPPING_ADDRESS_POSTALCODE,
				lShippingAddressData.get(DigitalProfileRestResourceContants.POSTAL_CODE));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_SHIPPING_ADDRESS_FIRST_NAME,
				lShippingAddressData.get(DigitalProfileRestResourceContants.FIRST_NAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_SHIPPING_ADDRESS_LAST_NAME,
				lShippingAddressData.get(DigitalProfileRestResourceContants.LAST_NAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_SHIPPING_ADDRESS_MIDDLE_NAME,
				lShippingAddressData.get(DigitalProfileRestResourceContants.MIDDLE_NAME));

		}
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_DAYTIME_PHONENUMBER,
				pRequestPayload.get(DigitalProfileRestResourceContants.DAYTIME_PHONENUMBER));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_AUTOLOGIN, pRequestPayload.get(DigitalProfileRestResourceContants.AUTOLOGIN));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_REALMID, pRequestPayload.get(DigitalProfileRestResourceContants.REALMID));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_CONFIRM_PASSWORD,
				pRequestPayload.get(DigitalProfileRestResourceContants.CONFIRM_PASSWORD));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_DOB, pRequestPayload.get(DigitalProfileRestResourceContants.DOB));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_FIRSTNAME,
				pRequestPayload.get(DigitalProfileRestResourceContants.FIRSTNAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_LASTNAME,
				pRequestPayload.get(DigitalProfileRestResourceContants.LASTNAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_EMAIL, pRequestPayload.get(DigitalProfileRestResourceContants.EMAIL));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_GENDER, pRequestPayload.get(DigitalProfileRestResourceContants.GENDER));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_LOGIN, pRequestPayload.get(DigitalProfileRestResourceContants.LOGIN));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_MEMBER, pRequestPayload.get(DigitalProfileRestResourceContants.MEMBER));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_MIDDLENAME,
				pRequestPayload.get(DigitalProfileRestResourceContants.MIDDLENAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_PASSWORD,
				pRequestPayload.get(DigitalProfileRestResourceContants.PASSWORD));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_ORDERID, pRequestPayload.get(DigitalProfileRestResourceContants.ORDERID));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_MOBILE, pRequestPayload.get(DigitalProfileRestResourceContants.MOBILE_PHONENUMBER));
		pExecutor.addInput(DigitalProfileRestResourceContants.DAYTIME_PHONENUMBER,
				pRequestPayload.get(DigitalProfileRestResourceContants.DAYTIME_PHONENUMBER));
		pExecutor.addInput(DigitalProfileRestResourceContants.BIRTH_MOUNTH, pRequestPayload.get(DigitalProfileRestResourceContants.BIRTH_MOUNTH));
		pExecutor.addInput(DigitalProfileRestResourceContants.BIRTHDAY, pRequestPayload.get(DigitalProfileRestResourceContants.BIRTHDAY));

		pExecutor.addInput(DigitalProfileRestResourceContants.BIRTH_YEAR, "2004");
		pExecutor.addInput(DigitalProfileRestResourceContants.PASSWORD_VALUE, pRequestPayload.get(DigitalProfileRestResourceContants.PASSWORD));

		pExecutor.addInput(DigitalProfileRestResourceContants.MAKE_LOGIN_AS_SAME_EMAIL,
				pRequestPayload.get(DigitalProfileRestResourceContants.MAKE_LOGIN_AS_SAME_EMAIL));
		pExecutor.addInput(DigitalProfileRestResourceContants.MAKE_LOGIN_AS_SAME_LOGIN,
				pRequestPayload.get(DigitalProfileRestResourceContants.MAKE_LOGIN_AS_SAME_LOGIN));
		if(pRequestPayload.get(DSWProfileRestResourceContants.ALERT_METHOD_EMAIL) != null){
			Boolean alertMethodEmail = (Boolean) pRequestPayload.get(DSWProfileRestResourceContants.ALERT_METHOD_EMAIL);
			pExecutor.addInput(DSWProfileRestResourceContants.ALERT_METHOD_EMAIL,Boolean.toString(alertMethodEmail));
		}
		pExecutor.addInput(DigitalProfileRestResourceContants.ALERT_METHOD_SMS,
				pRequestPayload.get(DigitalProfileRestResourceContants.ALERT_METHOD_SMS));
		pExecutor.addInput(DigitalProfileRestResourceContants.SUGGESTED_ADDRESS,
				pRequestPayload.get(DigitalProfileRestResourceContants.SUGGESTED_ADDRESS));
		
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method inputsForUpdate");
		}
	}

	/**
	 * This method is used to provide input to executor for createV2 profile.
	 *
	 * @param pExecutor
	 * @param pRequestPayload
	 * @throws ServletException
	 */
	@SuppressWarnings("unchecked") // ok
	public void inputsForCommonCreateChain(FormHandlerExecutor pExecutor, Map<String, Object> pRequestPayload)
			throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method inputsForCommonCreateChain");
		}
		Map<String, Object> lHomeAddressData = (Map<String, Object>) pRequestPayload
				.get(DigitalProfileRestResourceContants.HOME_ADDRESS);
		if (lHomeAddressData != null) {
			lHomeAddressData = (Map<String, Object>) lHomeAddressData
					.get(DigitalProfileRestResourceContants.ATG_REST_VALUES);
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_ADDRESS1,
					lHomeAddressData.get(DigitalProfileRestResourceContants.ADDRESS1));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_ADDRESS2,
					lHomeAddressData.get(DigitalProfileRestResourceContants.ADDRESS2));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_ADDRESS3,
					lHomeAddressData.get(DigitalProfileRestResourceContants.ADDRESS3));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_CITY,
					lHomeAddressData.get(DigitalProfileRestResourceContants.CITY));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_STATE,
					lHomeAddressData.get(DigitalProfileRestResourceContants.STATE));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_COUNTRY,
					lHomeAddressData.get(DigitalProfileRestResourceContants.COUNTRY));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_JOBTITLE,
					lHomeAddressData.get(DigitalProfileRestResourceContants.JOBTITLE));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_POSTAL_CODE,
					lHomeAddressData.get(DigitalProfileRestResourceContants.POSTAL_CODE));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_FAXNUMBER,
					lHomeAddressData.get(DigitalProfileRestResourceContants.FAXNUMBER));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_FIRSTNAME,
					lHomeAddressData.get(DigitalProfileRestResourceContants.FIRSTNAME));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_LASTNAME,
					lHomeAddressData.get(DigitalProfileRestResourceContants.LASTNAME));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_MIDDLENAME,
					lHomeAddressData.get(DigitalProfileRestResourceContants.MIDDLENAME));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_PHONENUMBER,
					lHomeAddressData.get(DigitalProfileRestResourceContants.PHONENUMBER));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_PREFIX,
					lHomeAddressData.get(DigitalProfileRestResourceContants.PREFIX));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_SUFFIX,
					lHomeAddressData.get(DigitalProfileRestResourceContants.SUFFIX));
			pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_COMPANYNAME,
					lHomeAddressData.get(DigitalProfileRestResourceContants.COMPANYNAME));
			pExecutor.addInput(DigitalProfileRestResourceContants.HOMEADDRESS_HOMETYPE,
					lHomeAddressData.get(DigitalProfileRestResourceContants.ADDRESS_TYPE));
			pExecutor.addInput(DigitalProfileRestResourceContants.HOMEADDRESS_REGION,
					lHomeAddressData.get(DigitalProfileRestResourceContants.REGION));
			pExecutor.addInput(DigitalProfileRestResourceContants.HOMEADDRESS_RANK,
					lHomeAddressData.get(DigitalProfileRestResourceContants.RANK));
		}
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_AUTOLOGIN,
				pRequestPayload.get(DigitalProfileRestResourceContants.AUTOLOGIN));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_REALMID,
				pRequestPayload.get(DigitalProfileRestResourceContants.REALMID));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_CONFIRM_PASSWORD,
				pRequestPayload.get(DigitalProfileRestResourceContants.CONFIRM_PASSWORD));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_DOB,
				pRequestPayload.get(DigitalProfileRestResourceContants.DOB));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_FIRSTNAME,
				pRequestPayload.get(DigitalProfileRestResourceContants.FIRSTNAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_LASTNAME,
				pRequestPayload.get(DigitalProfileRestResourceContants.LASTNAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_EMAIL,
				pRequestPayload.get(DigitalProfileRestResourceContants.EMAIL));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_GENDER,
				pRequestPayload.get(DigitalProfileRestResourceContants.GENDER));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_LOGIN,
				pRequestPayload.get(DigitalProfileRestResourceContants.LOGIN));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_MEMBER,
				pRequestPayload.get(DigitalProfileRestResourceContants.MEMBER));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_MIDDLENAME,
				pRequestPayload.get(DigitalProfileRestResourceContants.MIDDLENAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_PASSWORD,
				pRequestPayload.get(DigitalProfileRestResourceContants.PASSWORD));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_ORDERID,
				pRequestPayload.get(DigitalProfileRestResourceContants.ORDERID));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_HOMEADDRESS_DAYTIME_PHONENUMBER,
				pRequestPayload.get(DigitalProfileRestResourceContants.DAYTIME_PHONENUMBER));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_MOBILE_PHONENUMBER,
				pRequestPayload.get(DigitalProfileRestResourceContants.MOBILE_PHONENUMBER));

		pExecutor.addInput(DigitalProfileRestResourceContants.BIRTH_MOUNTH,
				pRequestPayload.get(DigitalProfileRestResourceContants.BIRTH_MOUNTH));
		pExecutor.addInput(DigitalProfileRestResourceContants.BIRTHDAY,
				pRequestPayload.get(DigitalProfileRestResourceContants.BIRTHDAY));

		pExecutor.addInput(DigitalProfileRestResourceContants.BIRTH_YEAR, DigitalProfileRestResourceContants.BIRTH_YEAR_VALUE);

		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_RECEIVEPROMO,
				pRequestPayload.get(DigitalProfileRestResourceContants.RECEIVEPROMO));
		pExecutor.addInput(DigitalProfileRestResourceContants.VALUE_PHONEPROMO,
				pRequestPayload.get(DigitalProfileRestResourceContants.PHONEPROMO));
		if(pRequestPayload.get(DSWProfileRestResourceContants.ALERT_METHOD_EMAIL) != null){
			Boolean alertMethodEmail = (Boolean) pRequestPayload.get(DSWProfileRestResourceContants.ALERT_METHOD_EMAIL);
			pExecutor.addInput(DSWProfileRestResourceContants.ALERT_METHOD_EMAIL,Boolean.toString(alertMethodEmail));
		}
		pExecutor.addInput(DigitalProfileRestResourceContants.ALERT_METHOD_SMS,
				pRequestPayload.get(DigitalProfileRestResourceContants.ALERT_METHOD_SMS));

		pExecutor.addInput(DigitalProfileRestResourceContants.SOURCE,
				pRequestPayload.get(DigitalProfileRestResourceContants.SOURCE));
		pExecutor.addInput(DigitalProfileRestResourceContants.LOYALTY_NUMBER,
				pRequestPayload.get(DigitalProfileRestResourceContants.LOYALTY_NUMBER));
		pExecutor.addInput(DigitalProfileRestResourceContants.SUGGESTED_ADDRESS,
				pRequestPayload.get(DigitalProfileRestResourceContants.SUGGESTED_ADDRESS));
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method inputsForCommonCreateChain");
		}
	}

	/**
	 * This method is used to provide Credit Cards Info.
	 *
	 * @param pExcludeExpiredCards
	 * @throws ServletException,JSONException
	 * @return lResponseMap
	 */
	public Map<String, Object> getUserCreditCardsInfo(Boolean pExcludeExpiredCards) throws ServletException, JSONException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getUserCreditCardsInfo");
		}
		Map<String, Object> lResponseMap = new HashMap<>();
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		if (pExcludeExpiredCards != null && pExcludeExpiredCards) {
			Map<String, RepositoryItem> lCreditCards = getProfileTools().geDefaultCreditCardsDetails(lProfile);
			lResponseMap.put(DigitalProfileRestResourceContants.PRIMARY_CREDIT_CARD,
					lCreditCards.get(DigitalJaxRsResourceCommonConstants.DEFAULT_CREDIT_CARD));
			Map<String, RepositoryItem> lActiveCreditCards = getProfileTools().getActiveCreditCardsDetails(lProfile);
			Map<String, Object> lCreditCardMap = new HashMap<>();
			lCreditCardMap.put(DigitalProfileRestResourceContants.ACTIVE_CREDIT_CARD, lActiveCreditCards);
			lResponseMap.put(DigitalProfileRestResourceContants.OTHER_CREDIT_CARD, lCreditCardMap);

		} else {
			lResponseMap.put(DigitalProfileRestResourceContants.PRIMARY_CREDIT_CARD,
					lProfile.getPropertyValue(DigitalJaxRsResourceCommonConstants.DEFAULT_CREDIT_CARD));
			Map<String, Object> lCreditCardMap = new HashMap<>();
			lCreditCardMap.put(DigitalProfileRestResourceContants.CREDIT_CARDS,
					lProfile.getPropertyValue(DigitalJaxRsResourceCommonConstants.CREDIT_CARDS));
			lResponseMap.put(DigitalProfileRestResourceContants.OTHER_CREDIT_CARD, lCreditCardMap);
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getUserCreditCardsInfo");
		}
		return lResponseMap;
	}

	/**
	 * This method will return user fav store.
	 *
	 * @param pLatitude
	 * @param pLongitude
	 * @return
	 * @throws RepositoryException
	 */
	public Object getUserFavoriteStoresInfo(Double pLatitude, Double pLongitude) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getUserFavoriteStoresInfo");
		}
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		RegionProfile lRegionProfile = (RegionProfile) ServletUtil.getCurrentRequest().getNucleus()
				.resolveName(DigitalProfileRestResourceContants.REGION_PROFILE);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getUserFavoriteStoresInfo");
		}
		return getProfileTools().getUserFavoriteStore(lProfile, pLatitude, pLongitude, lRegionProfile);
	}

	/**
	 * This method will remove address.
	 *
	 * @param pAddressId
	 * @return lResult
	 * @throws DigitalIntegrationException
	 * @throws RepositoryException
	 */
	public String removeAddressInfo(String pAddressId) throws RepositoryException, DigitalIntegrationException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method removeAddressInfo");
		}
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		String lResult = getProfileTools().removeProfileRepositoryAddressById(lProfile, pAddressId);
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method removeAddressInfo");
		}
		return lResult;
	}

	/**
	 * This method will change password.
	 *
	 * @param pRequestPayload
	 * @return
	 * @throws DigitalIntegrationException
	 * @throws RepositoryException
	 * @throws ServletException
	 */
	public void changePasswordV2Info(Map<String, Object> pRequestPayload, FormHandlerExecutor pExecutor)
			throws RepositoryException, DigitalIntegrationException, ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method changePasswordV2Info");
		}

		pExecutor.addInput(DigitalProfileRestResourceContants.LOGIN_HASH, pRequestPayload.get(DigitalProfileRestResourceContants.LOGIN_HASH));
		pExecutor.addInput(DigitalProfileRestResourceContants.PASSWORD_VALUE, pRequestPayload.get(DigitalProfileRestResourceContants.PASSWORD));
		pExecutor.addInput(DigitalProfileRestResourceContants.LOGIN_VALUE, pRequestPayload.get(DigitalProfileRestResourceContants.LOGIN));
		pExecutor.addInput(DigitalProfileRestResourceContants.OLD_PASSWORD_VALUE,
				pRequestPayload.get(DigitalProfileRestResourceContants.OLD_PASSWORD));
		pExecutor.addInput(DigitalProfileRestResourceContants.VERSION, DigitalProfileRestResourceContants.VERSION_VALUE);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method changePasswordV2Info");
		}
	}

	/**
	 * This method will remove payment info.
	 *
	 * @param pCardName
	 * @return lResult
	 * @throws RepositoryException
	 */
	public String removePaymentInfo(String pCardName) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method removeAddressInfo");
		}
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		String lResult = getProfileTools().removeCreditCard(lProfile, pCardName);
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method removeAddressInfo");
		}
		return lResult;
	}

	/**
	 * This method is used to provide other address details.
	 *
	 * @param pRequestPayload
	 * @return lResponseMap
	 * @throws IntrospectionException
	 * @throws RepositoryException
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@SuppressWarnings("unchecked") // OK
	public Map<Object, Object> updateOtherAddressDetails(Map<String, Object> pRequestPayload)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method removeAddressInfo");
		}

		Map<Object, Object> lResponseMap = new HashMap<>();

		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		Map<String, Object> lAddress = (Map<String, Object>) pRequestPayload.get(DigitalProfileRestResourceContants.OTHER_ADDRESS);
		String lAddressRepositryId = (String) pRequestPayload.get(DigitalProfileRestResourceContants.ID);
		String lMakePrimaryShippingAddress = (String) pRequestPayload.get(DigitalProfileRestResourceContants.MAKE_PRIMARY_SHIPPING_ADDRESS);
		String lSuggestedAddress = (String) pRequestPayload.get(DigitalProfileRestResourceContants.SUGGESTED_ADDRESS);
		lAddress.remove("atg-rest-class-type");
		DigitalAddress lDswAddress = (DigitalAddress) AddressTools.createAddressFromMap(lAddress, DigitalProfileRestResourceContants.DSWADDRESS_PATH);

		lResponseMap = getProfileTools().updateProfileRepositoryAddress(lProfile, lDswAddress, lAddressRepositryId,
				lMakePrimaryShippingAddress, lSuggestedAddress);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method removeAddressInfo");
		}
		return lResponseMap;
	}

	/**
	 * This method is used to provide user fav store details.
	 *
	 * @param pRequestPayload
	 * @return lResult
	 * @throws RepositoryException
	 */
	public String updateUserFavoriteStoreInfo(Map<String, Object> pRequestPayload) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method updateUserFavoriteStoreInfo");
		}

		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		String lFavoriteStoreId = (String) pRequestPayload.get(DigitalProfileRestResourceContants.FAVORITE_STORE_ID);

		String lResult = getProfileTools().updateFavoriteStore(lProfile, lFavoriteStoreId);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method updateUserFavoriteStoreInfo");
		}
		return lResult;
	}

	/**
	 * This method is use to get login by profileID.
	 *
	 * @param pPfavoriteStoreId
	 * @return lFavoriteStoreResult
	 * @throws RepositoryException
	 *
	 */
	public String getLoginbyProfileIDInfo(String pProfileID) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getLoginbyProfileIDInfo");
		}
		String lProfileIDResult = getProfileTools().findLoginByProfileID(pProfileID);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getLoginbyProfileIDInfo");
		}
		return lProfileIDResult;

	}

	/**
	 * This method is use to provide user address info.
	 *
	 * @param pProfile
	 * @param pCheckout
	 * @param pExcludeExpiredCards
	 * @param pLongitude
	 * @param pLatitude
	 * @return lResponseMap
	 * @throws ServletException
	 * @throws JSONException
	 * @throws RepositoryException
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getUserDetailsInfo(Profile pProfile, Boolean pCheckout, Boolean pExcludeExpiredCards, Double pLatitude, Double pLongitude) throws JSONException, ServletException, RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getUserDetailsInfo");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		Profile lProfile = (Profile) ServletUtil.getCurrentUserProfile();
		if(!pCheckout){
			getRewardsHelper().getRewardsComponent().reloadRewardsData();
			lResponseMap.put(DigitalProfileRestResourceContants.FAVORITE_STORES, getUserFavoriteStoresInfo(pLatitude, pLongitude));
		}

		lResponseMap.put(DigitalProfileRestResourceContants.CONTACT_INFO, lProfile.getDataSource());

		if(!pCheckout){
			lResponseMap.put(DigitalProfileRestResourceContants.MAILLING_ADDRESS_EXISTS,getProfileTools().isProfileMailingAddressExists(lProfile));
			lResponseMap.put(DigitalProfileRestResourceContants.STREET_ADDRESS, lProfile.getPropertyValue(DigitalProfileRestResourceContants.HOME_ADDRESS));
		}

		RepositoryItem	 lShippingAddress = (RepositoryItem) lProfile.getPropertyValue(DigitalProfileRestResourceContants.SHIPPING_ADDRESS);
		if(lShippingAddress != null && !DigitalProfileRestResourceContants.INTL.equals(lShippingAddress.getPropertyValue(DigitalShippingGroupRestResourceContants.ADDRESS_TYPE))){
			lResponseMap.put(DigitalProfileRestResourceContants.PRIMARY_SHIPPING_ADDRESS, lShippingAddress);
		}

		Map<String , RepositoryItem>  lAllSecondaryAddresses =   (Map<String, RepositoryItem>) lProfile.getPropertyValue(DigitalProfileRestResourceContants.ALL_SECONDARY_ADDRESS);
		Map<String , Object> lSecondaryMap = new HashMap<>();
		Map<String , Object> lSecondaryAddMap = new HashMap<>();
		for(Map.Entry<String , RepositoryItem> lSecondaryAddress: lAllSecondaryAddresses.entrySet())
		{
			if(!lSecondaryAddress.getValue().getPropertyValue(DigitalProfileRestResourceContants.ADDRESSTYPE).equals(DigitalProfileRestResourceContants.INTL)  ){
				lSecondaryAddMap.put(lSecondaryAddress.getKey(), lSecondaryAddress.getValue());
			}
		}

		lSecondaryMap.put(DigitalProfileRestResourceContants.ADDRESS,lSecondaryAddMap);
		lResponseMap.put(DigitalProfileRestResourceContants.OTHER_ADDRESSES, lSecondaryMap);
		lResponseMap.putAll(getUserCreditCardsInfo(pExcludeExpiredCards));

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getUserDetailsInfo");
		}
		return lResponseMap;
	}
	/**
	 * adding input fields of create credit card form handler.
	 * 
	 * @param pExecutor
	 * @param pRequestMap
	 * @throws ServletException
	 */
	public void addInputForNewCreditCard(FormHandlerExecutor pExecutor, Map<String, Object> pRequestMap) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method addInputForNewCreditCard()");
		}
		
		pExecutor.addInput(DigitalProfileRestResourceContants.BILLING_SAME_AS_SHIPPING_ADDRESS, pRequestMap.get(DigitalProfileRestResourceContants.BILLING_SAME_AS_SHIPPING_ADDRESS));
		pExecutor.addInput(DigitalProfileRestResourceContants.BILLING_SAME_AS_MAILING_ADDRESS, pRequestMap.get(DigitalProfileRestResourceContants.BILLING_SAME_AS_MAILING_ADDRESS));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_PAY_PAGE_REGISTRATION_ID, pRequestMap.get(DigitalProfileRestResourceContants.PAY_PAGE_REGISTRATION_ID));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_VANTIV_FIRST_SIX, pRequestMap.get(DigitalProfileRestResourceContants.VANTIV_FIRST_SIX));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_VANTIV_FIRST_FOUR, pRequestMap.get(DigitalProfileRestResourceContants.VANTIV_FIRST_FOUR));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_DEFAULT_CREDIT_CRD, pRequestMap.get(DigitalProfileRestResourceContants.IS_DEFAULT_CREDIT_CRD));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_CREDIT_CRD_TYPE, pRequestMap.get(DigitalProfileRestResourceContants.CREDIT_CRD_TYPE));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_NAME_ON_CRD, pRequestMap.get(DigitalProfileRestResourceContants.NAME_ON_CRD));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_CREDIT_CRD_NUMBER, pRequestMap.get(DigitalProfileRestResourceContants.CREDIT_CRD_NUMBER));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_EXPIRATION_MONTH, pRequestMap.get(DigitalProfileRestResourceContants.EXPIRATION_MONTH));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_EXPIRATION_YEAR, pRequestMap.get(DigitalProfileRestResourceContants.EXPIRATION_YEAR));
		pExecutor.addInput(DigitalProfileRestResourceContants.GENERATE_NICK_NAME, true);
		
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_BILL_FIRST_NAME, pRequestMap.get(DigitalProfileRestResourceContants.FIRST_NAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_BILL_MIDDLE_NAME, pRequestMap.get(DigitalProfileRestResourceContants.MIDDLE_NAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_BILL_LAST_NAME, pRequestMap.get(DigitalProfileRestResourceContants.LAST_NAME));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_BILL_ADDRESS_ONE, pRequestMap.get(DigitalProfileRestResourceContants.ADDRESS1));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_BILL_ADDRESS_TWO, pRequestMap.get(DigitalProfileRestResourceContants.ADDRESS2));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_BILL_CITY, pRequestMap.get(DigitalProfileRestResourceContants.CITY));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_BILL_STATE, pRequestMap.get(DigitalProfileRestResourceContants.STATE));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_BILL_COUNTRY, pRequestMap.get(DigitalProfileRestResourceContants.COUNTRY));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_BILL_POSTAL_CODE, pRequestMap.get(DigitalProfileRestResourceContants.POSTAL_CODE));
		pExecutor.addInput(DigitalProfileRestResourceContants.CRDT_CRD_BILL_PHONE_NUMBER, pRequestMap.get(DigitalProfileRestResourceContants.PHONE_NUMBER));
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method addInputForNewCreditCard()");
		}
	}
	
}
