package com.digital.commerce.jaxrs.common.constants;

/**
 * Constant file for Gift Card Services
 *
 * @author TAIS
 *
 */
public class DigitalGiftCardRestResourceConstants extends DigitalJaxRsResourceCommonConstants {

	public static final String CURRENCY_NAME = "currencyName";

	public static final String CARD_NUMBER = "cardNumber";

	public static final String Pin = "pin";

	public static final String GIFT_CARD_RESPONSE_DROPLET = "/com/digital/commerce/services/order/purchase/GiftCardResponseDroplet";

	public static final String ADD_ITEM_COUNT = "addItemCount";

	public static final String ADD_GIFTCARD_TO_ORDER = "addGiftCardItemToOrder";

	public static final String GIFT_CARD_FORMHANDLER = "/com/digital/commerce/services/order/purchase/GiftCardFormHandler";

	public static final String REPRICE_ORDER_DROPLET = "/atg/commerce/order/purchase/RepriceOrderDroplet";

	public static final String ORDER_SUBTOTAL_SHIPPING = "ORDER_SUBTOTAL_SHIPPING";

	public static final String GCSTATUS = "gcStatus";

	public static final String GENERIC_EXCEPTION = "genericExceptions";

	public static final String GIFTCARD_PG = "giftCardPaymentGroup";

	public static final String GIFTCARD_NUMBER = "giftCardNumber";

	public static final String PIN_NUMBER = "pinNumber";

	public static final String AMOUNT_REMAINING = "amountRemaining";

	public static final String GIFTCARD_COMPONENT = "/com/digital/commerce/services/v1_0/order/purchase/DigitalGCComponent";






}
