package com.digital.commerce.jaxrs.common.constants;

/**
 * @author TAIS
 *
 */
public class DigitalOrderCommonRestResourceConstants extends DigitalJaxRsResourceCommonConstants {

	public static final String ORDER_CONTACTINFO_FORMHANDLER_COMPONENT_NAME = "/com/digital/commerce/services/v1_0/order/contactinfo/DigitalOrderContactInfoHandler";

	public static final String ADD_UPDATE_CONTACTINFO = "addOrUpdateContactInfo";

	public static final String CONTACT_ADDRESS_PHONE_NUMBER = "contactAddress.phoneNumber";

	public static final String CONTACT_ADDRESS_EMAIL = "contactAddress.email";

	public static final String RECEIVE_EMAIL_OFFERS = "receiveEmailOffers";

	public static final String CONTACT_INFO = "contactInfo";

	


}
