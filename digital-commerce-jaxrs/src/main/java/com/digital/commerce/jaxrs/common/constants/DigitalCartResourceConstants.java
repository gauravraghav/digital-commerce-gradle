package com.digital.commerce.jaxrs.common.constants;

/**
 * Constant file for cart resource
 *
 * @author TAIS
 *
 */
public class DigitalCartResourceConstants extends DigitalJaxRsResourceCommonConstants {

    public static final String CART_MODIFER_FORM_HANDLER_PATH = "/atg/commerce/order/purchase/CartModifierFormHandler";

    public static final String HANDLE_ADD_ITEM = "addItemToOrder";

    public static final Object GIFT_ITEM_ID = "giftItemId";

    public static final String CATALOG_REF_IDS = "catalogRefIds";

    public static final String SHIP_TYPE = "shipType";

    public static final String STORE_ID = "storeId";

    public static final String STORE_LOCATION_ID = "storeLocationId";

    public static final String GITT_RECEIPT = "giftReceipt";

    public static final String GIFT_RECEIPT_MESSAGE = "giftReceiptMessage";

    public static final String GIFT_LIST_ITEM_ID = "giftlistItemId";

    public static final Object ADD_TO_WISH_LIST = "addToWishlist";

    public static final String GIFT_LIST_ID = "giftlistId";

    public static final String SUB_TOTAL = "subTotal";

    public static final String COMMERCE_ITEMS = "commerceItems";

    public static final String CONCURRENT_UPDATE = "concurrentUpdate";

    public static final String REMOVE_ITEM_HANDLE = "removeItemFromOrder";

    public static final String REMOVAL_COMMERCE_IDS = "removalCommerceIds";

    public static final String MODIFY_ORDER_PRICING_OP = "modifyOrderPricingOp";

    public static final String UPDATE_ITEM_HANDLE_METHOD = "updateItemToOrder";

    public static final String PAYPAL_LOOKUP_HANDLE = "paypalLookup";

    public static final String PAYPAL_EXPRESS_CHECKOUT = "paypalExpressCheckout";

    public static final String GUEST_CHECKOUT = "guestCheckout";

    public static final String ASC_URL = "acsUrl";

    public static final String PP_PAYLOAD = "ppPayload";

    public static final String CARDINAL_ORDER_ID = "cardinalOrderId";

    public static final String MOVE_TO_PURCHASE_HANDLE = "moveToPurchaseInfo";

    public static final String WARNING_MESSAGES = "warningMessages";

}