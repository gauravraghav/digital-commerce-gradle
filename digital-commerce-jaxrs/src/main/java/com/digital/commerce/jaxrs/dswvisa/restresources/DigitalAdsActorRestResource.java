package com.digital.commerce.jaxrs.dswvisa.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalAdsActorConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;

import atg.droplet.DropletException;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class will provide RestResources for adsPostData Services.
 * 
 * @author TAIS
 *
 */
@RestResource(id = "com.digital.commerce.jaxrs.dswvisa.restresources.DigitalAdsActorRestResource")
@Path("/dswvisa")
@Api("getAdsPostData Service")
@Getter
@Setter
public class DigitalAdsActorRestResource extends DigitalGenericService {

	/**
	 * commonHelper - Helper class.
	 */
	private DigitalCommonRestResourceHelper commonHelper;

	/**
	 * This Service for fetch getAdsPostData Results.
	 * 
	 * @see - Actor Chain
	 *      -/rest/model/com/digital/commerce/services/v1_0/dswvisa/AdsActor/getAdsPostData
	 * 
	 * @return Response
	 */
	@GET
	@Path("/getAdsPostData")
	@Endpoint(id = "/dswvisa/getAdsPostData#GET", isSingular = true, filterId = "dsw-adsPostData-response")
	public Response getAdsPostData() {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getAdsPostData()");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponseMap = new HashMap<>();
		Map<String, Object> lHelperMap = new HashMap<>();

		try {
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalAdsActorConstants.ADS_AUTH_FORM_HANDLER_PATH,
					DigitalAdsActorConstants.HANDLE_ADS_POSTDATA);

			FormHandlerInvocationResult lResult = executor.execute();

			if (lResult.isFormError()) {
				lHelperMap = getCommonHelper().createGenericErrorResponse(lResult);
			}

		} catch (DropletException e) {
			e.printStackTrace();
		}

		lFinalResponseMap.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lHelperMap);
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method getAdsPostData()");
		}
		return Response.ok(lFinalResponseMap).build();

	}

}
