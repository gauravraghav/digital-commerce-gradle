package com.digital.commerce.jaxrs.common.constants;

/**
 * CouponCommonConstants
 * 
 * @author TAIS
 *
 */
public class DigitalCouponRestResourceConstants {

	public static final String SKIP_DETAILS_REWARD = "skipRewardDetails";

	public static final String LOAD_ONLY_GLOBAL_PROMO = "loadOnlyGlobalPromo";

	public static final String SORTED_PROMOTIONS = "sortedPromotions";

	public static final String AVAILABLE_SAVING_DROPLET = "/com/digital/commerce/services/v1_0/promotions/DigitalAvailableSavingsDroplet";

	public static final String AVAILABLE_SAVING_DROPLET_V2 = "/com/digital/commerce/services/v2_0/promotions/DigitalAvailableSavingsDroplet";

	public static final String LOAD_OFFER_AND_CERTS = "loadOffersAndCerts";

}
