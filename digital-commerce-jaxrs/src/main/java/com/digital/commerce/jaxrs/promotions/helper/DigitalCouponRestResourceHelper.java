package com.digital.commerce.jaxrs.promotions.helper;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;

import com.digital.commerce.jaxrs.common.constants.DigitalCouponRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;

import atg.core.util.StringList;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.service.jaxrs.DropletInvoker;
import atg.service.jaxrs.OParam;
import atg.service.jaxrs.Output;
import lombok.Getter;

/**
 * @author TAIS
 *
 */
@Getter
public class DigitalCouponRestResourceHelper extends GenericService {

	private Map<String, StringList> availableSavingFilters;

	/**
	 * Provide AvailableSavings for coupon applied
	 *
	 * @param pLoadOnlyGlobalPromo
	 * @param pLoadOffersAndCerts
	 * @return lResult
	 * @throws Exception
	 * @throws RepositoryException
	 * @throws JSONException
	 */
	public Map<String, Object> populateAvailableSavings(Boolean pLoadOnlyGlobalPromo, Boolean pLoadOffersAndCerts)
			throws JSONException, RepositoryException, Exception {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getAvailableSavings");
		}

		boolean lLoadOnlyGlobalPromo = pLoadOffersAndCerts == null ? false : pLoadOffersAndCerts;
		boolean lLoadOffersAndCerts = pLoadOffersAndCerts == null ? false : pLoadOffersAndCerts;

		DropletInvoker dropletInvoker = new DropletInvoker(DigitalCouponRestResourceConstants.AVAILABLE_SAVING_DROPLET);
		dropletInvoker.addInput(DigitalCouponRestResourceConstants.LOAD_ONLY_GLOBAL_PROMO, lLoadOnlyGlobalPromo);
		dropletInvoker.addInput(DigitalCouponRestResourceConstants.LOAD_OFFER_AND_CERTS, lLoadOffersAndCerts);

		OParam oParamOutput = dropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
		Output lSuccessOutput = oParamOutput.addOutput(DigitalCouponRestResourceConstants.SORTED_PROMOTIONS,
				DigitalCouponRestResourceConstants.SORTED_PROMOTIONS);

		OParam oErrorParam = dropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.ERROR_SMALL);
		Output lErrorOutput = oErrorParam.addOutput(DigitalJaxRsResourceCommonConstants.ERROR_MSG, DigitalJaxRsResourceCommonConstants.ERROR_MSG);

		dropletInvoker.invoke();

		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		if (lErrorOutput.getObject() != null) {
			lResponseMap.put(DigitalJaxRsResourceCommonConstants.ERROR_SMALL, lErrorOutput.getObject());

		} else {
			Map lResult = DigitalJaxRsUtility.buildReposiotryItemToMap((Map<String, Object>) lSuccessOutput.getObject(),
					getAvailableSavingFilters());
			lResponseMap.put(DigitalJaxRsResourceCommonConstants.SUCCESS, lResult);
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getAvailableSavings");
		}

		return lResponseMap;
	}

	/**
	 * Provide AvailableSavings V2 for coupon applied
	 *
	 * @return lResult
	 */
	public Map<String, Object> populateAvailableSavingsV2() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method populateAvailableSavingsV2");
		}

		DropletInvoker dropletInvoker = new DropletInvoker(DigitalCouponRestResourceConstants.AVAILABLE_SAVING_DROPLET_V2);

		OParam oParamOutput = dropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
		Output lSuccessOutput = oParamOutput.addOutput(DigitalCouponRestResourceConstants.SORTED_PROMOTIONS,
				DigitalCouponRestResourceConstants.SORTED_PROMOTIONS);

		OParam oErrorParam = dropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.ERROR_SMALL);
		Output lErrorOutput = oErrorParam.addOutput(DigitalJaxRsResourceCommonConstants.ERROR_MSG, DigitalJaxRsResourceCommonConstants.ERROR_MSG);

		dropletInvoker.invoke();
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		if (lErrorOutput.getObject() != null) {
			lResponseMap.put(DigitalJaxRsResourceCommonConstants.ERROR_SMALL, lErrorOutput.getObject());

		} else {
			lResponseMap.put(DigitalJaxRsResourceCommonConstants.SUCCESS, lSuccessOutput.getObject());
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method populateAvailableSavingsV2");
		}

		return lResponseMap;
	}


	/**
	 * @param pAvailableSavingFilters
	 *            the availableSavingFilters to set
	 */
	public void setAvailableSavingFilters(Map<String, StringList> pAvailableSavingFilters) {
		if (pAvailableSavingFilters == null) {
			this.availableSavingFilters = null;
		} else {
			Map<String, StringList> mapNew = new HashMap<>();
			for (Map.Entry entryCur : pAvailableSavingFilters.entrySet()) {
				mapNew.put(entryCur.getKey().toString(), new StringList(entryCur.getValue().toString().split(",")));
			}
			this.availableSavingFilters = mapNew;
		}
	}

}
