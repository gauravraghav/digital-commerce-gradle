package com.digital.commerce.jaxrs.rewards.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalRewardResourceConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalJaxRsUtility;
import com.digital.commerce.jaxrs.rewards.helper.DigitalRewardsRestResourceHelper;

import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class will provide RestResource for Rewards Services.
 * 
 * @author TAIS
 *
 */
@RestResource(id = "com.digital.commerce.jaxrs.rewards.restresources.DigitalRewardsRestResource")
@Path("/rewards")
@Api("Rewards Service")
@Getter
@Setter
public class DigitalRewardsRestResource extends DigitalGenericService {

	/**
	 * rewardsHelper - Helper Component
	 */
	public DigitalRewardsRestResourceHelper rewardsHelper;

	/**
	 * Service to RewardsCerts details.
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/rewards/DigitalRewardsActor/getRewardsCerts
	 * 
	 * @return Response
	 */
	@GET
	@Path("/getRewardsCerts")
	@Endpoint(id = "/rewards/getRewardsCerts#GET", isSingular = true, filterId = "dsw-getRewardsCerts-response")
	public Response getRewardsCerts() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getRewardsCerts");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();

		lResponseMap = getRewardsHelper().populateRewardsCerts();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getRewardsCerts");
		}
		return Response.ok(lResponseMap).build();
	}

	/**
	 * Service for anonymous email subscription
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v2_0/rewards/DigitalRewardsActor/anonymousSubscriptionFooter
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/anonymousEmailSubscriptionV2")
	@Endpoint(id = "/rewards/anonymousEmailSubscriptionV2#POST", isSingular = true, filterId = "dsw-rewards-anonymous-email-subscription")
	public Response anonymousEmailSubscription(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method anonymousEmailSubscription");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		try {
			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
			if (isLoggingDebug()) {
				logDebug("Request for anonymousSubscription " + lRequestMap);
			}
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalRewardResourceConstants.REWARD_FORM_HANDLER_PATH,
					DigitalRewardResourceConstants.HANDLE_ANONYMOUS_SUBSCRIPTION);
			executor.addInput(DigitalRewardResourceConstants.EMAIL_KEY, lRequestMap.get(DigitalRewardResourceConstants.EMAIL));
			executor.addInput(DigitalRewardResourceConstants.EMAIL_SOURCE_KEY,
					lRequestMap.get(DigitalRewardResourceConstants.EMAIL_SOURCE));
			FormHandlerInvocationResult lResult = executor.execute();
			lFinalResponse = getRewardsHelper().populateAnonymousEmailSubscriptionResponse(lResult);
		} catch (ServletException e) {
			vlogError(e, "ServletException during anonymousSubscription");
		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method anonymousEmailSubscription");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service for add missing rewards to account
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v2_0/rewards/DigitalRewardsActor/addShopWithoutACardRequest
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/addMissingPointsV2")
	@Endpoint(id = "/rewards/addMissingPointsV2#POST", isSingular = true, filterId = "dsw-rewards-add-missing-points")
	public Response addMissingPoints(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addMissingPoints");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		try {
			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
			if (isLoggingDebug()) {
				logDebug("Request for addMissingPoints " + lRequestMap);
			}
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalRewardResourceConstants.REWARD_FORM_HANDLER_PATH,
					DigitalRewardResourceConstants.HANDLE_ADD_SHOP_WITHOUT_A_CARD_REQUEST);
			executor.addInput(DigitalRewardResourceConstants.MEMBER_ID_KEY,
					lRequestMap.get(DigitalRewardResourceConstants.MEMBER_ID));
			executor.addInput(DigitalRewardResourceConstants.BARCODE_KEY,
					lRequestMap.get(DigitalRewardResourceConstants.BARCODE));
			FormHandlerInvocationResult lResult = executor.execute();
			lFinalResponse = getRewardsHelper().populateAddMissingPointsResponse(lResult);
		} catch (ServletException e) {
			vlogError(e, "ServletException during addMissingPoints");
		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method addMissingPoints");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * 
	 * Service for Retrive Reward Details
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/rewards/DigitalRewardsActor/retrieveRewardDetails
	 * @param pFilters
	 * @param pSyncRewards
	 * @param pCombineBirthdayOffers
	 * @param pStartDate
	 * @param pEndDate
	 * @return
	 */
	@GET
	@Path("/retrieveRewardDetails")
	@Endpoint(id = "/rewards/retrieveRewardDetails#GET", isSingular = true, filterId = "common-response-schema")
	public Response retrieveRewardDetails(@QueryParam(DigitalRewardResourceConstants.FILTERS_KEY) String pFilters,
			@QueryParam(DigitalRewardResourceConstants.SYNC_REWARDS_KEY) String pSyncRewards,
			@QueryParam(DigitalRewardResourceConstants.COMBINE_BIRTHDAY_OFFERS_KEY) String pCombineBirthdayOffers,
			@QueryParam(DigitalRewardResourceConstants.START_DATE) String pStartDate,
			@QueryParam(DigitalRewardResourceConstants.END_DATE) String pEndDate) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method retrieveRewardDetails");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		try {
			if (isLoggingDebug()) {
				logDebug("Request for retrieveRewardDetails ");
			}
			pSyncRewards = (pSyncRewards == null || pSyncRewards != "true") ? "false" : "true";
			pCombineBirthdayOffers = (pCombineBirthdayOffers == null || pCombineBirthdayOffers != "true") ? "false"
					: "true";

			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalRewardResourceConstants.REWARD_FORM_HANDLER_PATH,
					DigitalRewardResourceConstants.HANDLE_RETRIEVE_REWARD_DETAILS);

			executor.addInput(DigitalRewardResourceConstants.FILTERS_KEY, pFilters);
			executor.addInput(DigitalRewardResourceConstants.SYNC_REWARDS_KEY, pSyncRewards);
			executor.addInput(DigitalRewardResourceConstants.COMBINE_BIRTHDAY_OFFERS_KEY, pCombineBirthdayOffers);
			executor.addInput(DigitalRewardResourceConstants.START_DATE, pStartDate);
			executor.addInput(DigitalRewardResourceConstants.END_DATE, pEndDate);

			FormHandlerInvocationResult lResult = executor.execute();
			lFinalResponse = getRewardsHelper().populateRewardDetailsResponse(lResult);

		} catch (ServletException e) {
			vlogError(e, "ServletException during retrieveRewardDetails");
		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method retrieveRewardDetails");
		}
		return Response.ok(lFinalResponse).build();
	}
	
	/**
	 * Service for Retrive Charites Information
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/rewards/DigitalRewardsActor/retrieveCharities 
	 * @return Response
	 */
	@GET
	@Path("/retrieveCharities")
	@Endpoint(id = "/rewards/retrieveCharities#GET", isSingular = true, filterId = "dsw-rewards-retrieve-charities")
	public Response retrieveCharities() {

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method retrieveCharities");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();
		try {
			if (isLoggingDebug()) {
				logDebug("Request for retrieveCharities ");
			}
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalRewardResourceConstants.REWARD_FORM_HANDLER_PATH,
					DigitalRewardResourceConstants.HANDLE_RETRIEVE_CHARITIES);
			FormHandlerInvocationResult lResult = executor.execute();
			lResponseMap = getRewardsHelper().populateRewardsCharitiesResponse(lResult);
			
		} catch (ServletException e) {
			vlogError(e, "ServletException during retrieveCharities");
		}
		setResponseHeaders();
		vlogDebug("Exit -> {0} Method retrieveCharities", getClass().getCanonicalName());
		return Response.ok(lResponseMap).build();
	}


	/**
	 * Service for anonymous email subscription
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/rewards/DigitalRewardsActor/getShopForFilters
	 * 
	 * @return Response
	 */
	@GET
	@Path("/getShopForFilters")
	@Endpoint(id = "/rewards/getShopForFilters#GET", isSingular = true, filterId = "common-response-schema")
	public Response getShopForFilters() {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getShopForFilters");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		try {
			lFinalResponse = getRewardsHelper().getShopForFiltersResponseMap();
			if (isLoggingDebug()) {
				logDebug("getShopForFilters lFinalResponse=" + lFinalResponse);
			}
		} catch (DigitalAppException e) {
			vlogError(e, "DigitalAppException during getShopForFilters");
		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getShopForFilters");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service for create Shop For Item
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/rewards/DigitalRewardsActor/createShopfor
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/createShopForItem")
	@Endpoint(id = "/rewards/createShopForItem#POST", isSingular = true, filterId = "common-response-schema")
	public Response createShopForItem(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method createShopForItem");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Object lResponseMap = null;
		try {
			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
			if (isLoggingDebug()) {
				logDebug("Request for createShopForItem " + lRequestMap);
			}
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalRewardResourceConstants.REWARD_FORM_HANDLER_PATH,
					DigitalRewardResourceConstants.HANDLE_CREATE_SHOP_FOR);
			getRewardsHelper().addInputForCreateShopForItem(executor, lRequestMap);
			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lResponseMap = getRewardsHelper().populateErrorResponse(lResult);
			} else {
				lResponseMap = getRewardsHelper().populateShopForItemSuccessResponse(lResult);
			}
		} catch (ServletException e) {
			vlogError(e, "ServletException during createShopForItem");
		}
		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method createShopForItem");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service for update Shop For Item
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/rewards/DigitalRewardsActor/updateShopfor
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/updateShopForItem")
	@Endpoint(id = "/rewards/updateShopForItem#POST", isSingular = true, filterId = "common-response-schema")
	public Response updateShopForItem(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method updateShopForItem");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Object lResponseMap = null;
		try {
			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
			if (isLoggingDebug()) {
				logDebug("Request for updateShopForItem " + lRequestMap);
			}
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalRewardResourceConstants.REWARD_FORM_HANDLER_PATH,
					DigitalRewardResourceConstants.HANDLE_UPDATE_SHOP_FOR);
			getRewardsHelper().addInputForUpdateShopForItem(executor, lRequestMap);
			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lResponseMap = getRewardsHelper().populateErrorResponse(lResult);
			} else {
				lResponseMap = getRewardsHelper().populateShopForItemSuccessResponse(lResult);
			}
		} catch (ServletException e) {
			vlogError("ServletException during updateShopForItem");
		}
		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method updateShopForItem");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service for delete Shop For Item
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/rewards/DigitalRewardsActor/deleteShopfor
	 * 
	 * @param shopForID
	 *            the shop for ID
	 * @param rewardsShopForID
	 *            the rewards shop for ID
	 * @return the response
	 */
	@DELETE
	@Path("/deleteShopForItem")
	@Endpoint(id = "/rewards/deleteShopForItem#DELETE", isSingular = true, filterId = "common-response-schema")
	public Response deleteShopForItem(@QueryParam(DigitalRewardResourceConstants.SHOP_FOR_ID) String shopForID,
			@QueryParam(DigitalRewardResourceConstants.SHOP_FOR_REWARDS_ID) String rewardsShopForID) {
		vlogDebug("Start -> {0} Method deleteShopForItem. shopforID {1}, rewardsShopForID {2}",
				getClass().getCanonicalName(), shopForID, rewardsShopForID);
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Object lResponseMap = null;
		try {
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalRewardResourceConstants.REWARD_FORM_HANDLER_PATH,
					DigitalRewardResourceConstants.HANDLE_DELETE_SHOP_FOR);
			executor.addInput(DigitalRewardResourceConstants.SHOP_FOR_ID, shopForID);
			executor.addInput(DigitalRewardResourceConstants.SHOP_FOR_REWARDS_ID, rewardsShopForID);
			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lResponseMap = getRewardsHelper().populateErrorResponse(lResult);
			} else {
				lResponseMap = getRewardsHelper().populateShopForItemSuccessResponse(lResult);
			}
		} catch (ServletException e) {
			vlogError(e, "ServletException during deleteShopForItem");
		}
		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method deleteShopForItem");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service for create Gift Item
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/rewards/DigitalRewardsActor/createGift
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/createGift")
	@Endpoint(id = "/rewards/createGift#POST", isSingular = true, filterId = "common-response-schema")
	public Response createGift(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method createGift");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Object lResponseMap = null;
		try {
			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
			if (isLoggingDebug()) {
				logDebug("Request for createGift " + lRequestMap);
			}
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalRewardResourceConstants.REWARD_FORM_HANDLER_PATH,
					DigitalRewardResourceConstants.HANDLE_CREATE_GIFT);
			getRewardsHelper().addInputForCreateGiftItem(executor, lRequestMap);
			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lResponseMap = getRewardsHelper().populateErrorResponse(lResult);
			} else {
				lResponseMap = getRewardsHelper().populateGiftItemSuccessResponse(lResult);
			}
		} catch (ServletException e) {
			vlogError(e, "ServletException during createGift");
		}
		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method createGift");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service for update Gift Item
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/rewards/DigitalRewardsActor/updateGift
	 * 
	 * @param pRequestBody
	 *            Request Payload
	 * @return Response
	 */
	@POST
	@Path("/updateGift")
	@Endpoint(id = "/rewards/updateGift#POST", isSingular = true, filterId = "common-response-schema")
	public Response updateGift(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method updateGift");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Object lResponseMap = null;
		try {
			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
			if (isLoggingDebug()) {
				logDebug("Request for updateGift " + lRequestMap);
			}
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalRewardResourceConstants.REWARD_FORM_HANDLER_PATH,
					DigitalRewardResourceConstants.HANDLE_UPDATE_GIFT);
			getRewardsHelper().addInputForUpdateGiftItem(executor, lRequestMap);
			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lResponseMap = getRewardsHelper().populateErrorResponse(lResult);
			} else {
				lResponseMap = getRewardsHelper().populateGiftItemSuccessResponse(lResult);
			}
		} catch (ServletException e) {
			vlogError(e, "ServletException during createGift");
		}
		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method updateGift");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service for issue certs
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/rewards/DigitalRewardsActor/issueCertificate
	 * 
	 * @param certDenomination
	 *            the denomination of cert to be issued
	 * @return the response
	 */
	@POST
	@Path("/issueCert")
	@Endpoint(id = "/rewards/issueCert#POST", isSingular = true, filterId = "common-response-schema")
	public Response issueCert(@QueryParam(DigitalRewardResourceConstants.CERT_DENOMINATION) String certDenomination) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method issueCert. certDenomination="
					+ certDenomination);
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Object lResponseMap = null;
		try {
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalRewardResourceConstants.REWARD_FORM_HANDLER_PATH,
					DigitalRewardResourceConstants.HANDLE_ISSUE_CERT);
			executor.addInput(DigitalRewardResourceConstants.CERT_DENOMINATION_KEY, certDenomination);
			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lResponseMap = getRewardsHelper().populateErrorResponse(lResult);
			} else {
				lResponseMap = getRewardsHelper().populateCertSuccessResponse();
			}
		} catch (ServletException e) {
			vlogError(e, "ServletException during issueCert");
		}
		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method issueCert");
		}
		return Response.ok(lFinalResponse).build();
	}

	/**
	 * Service for donate certs
	 * 
	 * @see - Actor Chain -
	 *      /com/digital/commerce/services/v1_0/rewards/DigitalRewardsActor/donateCertificates
	 * 
	 * @param pRequestBody
	 *            the request body
	 * @return the response
	 */
	@POST
	@Path("/donateCerts")
	@Endpoint(id = "/rewards/donateCerts#POST", isSingular = true, filterId = "common-response-schema")
	public Response donateCerts(JSONObject pRequestBody) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method donateCerts");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();
		Object lResponseMap = null;
		try {
			Map<String, Object> lRequestMap = DigitalJaxRsUtility.toMap(pRequestBody);
			if (isLoggingDebug()) {
				logDebug("Request for donateCerts " + lRequestMap);
			}
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalRewardResourceConstants.REWARD_FORM_HANDLER_PATH,
					DigitalRewardResourceConstants.HANDLE_DONATE_CERT);
			getRewardsHelper().addInputForDonateCerts(executor, lRequestMap);
			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lResponseMap = getRewardsHelper().populateErrorResponse(lResult);
			} else {
				getRewardsHelper().performRepriceOrder(lResult);
				lResponseMap = getRewardsHelper().populateCertSuccessResponse();
			}
		} catch (ServletException e) {
			vlogError(e, "ServletException during donateCerts");
		}
		lFinalResponse.put(DigitalJaxRsResourceCommonConstants.RESPONSE, lResponseMap);
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method donateCerts");
		}
		return Response.ok(lFinalResponse).build();
	}
}
