package com.digital.commerce.jaxrs.giftcard.helper;

import java.util.HashMap;
import java.util.Map;

import com.digital.commerce.jaxrs.common.constants.DigitalGiftCardRestResourceConstants;
import com.digital.commerce.jaxrs.common.constants.DigitalPaypalPaymentRestResourceConstants;
import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;
import com.digital.commerce.services.giftcard.domain.ApplyGiftCardResponse;
import com.digital.commerce.services.giftcard.domain.GiftCardBalanceInquiryResponse;
import com.digital.commerce.services.giftcard.domain.RemoveGiftCardResponse;
import com.digital.commerce.services.order.GiftCardDTO;
import com.digital.commerce.services.order.purchase.DigitalGCComponent;
import com.digital.commerce.services.order.purchase.GiftCardFormHandler;

import atg.droplet.DropletException;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.nucleus.GenericService;
import atg.service.jaxrs.DropletInvoker;
import atg.service.jaxrs.OParam;
import atg.service.jaxrs.Output;
import atg.servlet.ServletUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * Helper class for Gift Card Resource.
 *
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalGiftCardRestResourceHelper extends GenericService {
	
	/**
	 * commonHelper - DigitalCommonRestResourceHelper Component
	 */
	private DigitalCommonRestResourceHelper commonHelper;
	/**
	 * This method provides balance Inquiry Info for gift card.
	 *
	 * @param pRequestPayload
	 */
	public Object balanceInquiryInfo(Map<String, Object> pRequestPayload) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method balanceInquiryInfo");
		}
		String lCurrencyName = (String) pRequestPayload.get(DigitalGiftCardRestResourceConstants.CURRENCY_NAME);
		String lCardNumber = (String) pRequestPayload.get(DigitalGiftCardRestResourceConstants.CARD_NUMBER);
		String lPin = (String) pRequestPayload.get(DigitalGiftCardRestResourceConstants.Pin);

		DigitalGCComponent lGiftCardComponent = (DigitalGCComponent) ServletUtil.getCurrentRequest().resolveName(DigitalGiftCardRestResourceConstants.GIFTCARD_COMPONENT);
		GiftCardBalanceInquiryResponse lResponse = lGiftCardComponent.balanceInquiry(lCurrencyName, lCardNumber, lPin);

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method balanceInquiryInfo");
		}

		return lResponse;
	}

	/**
	 * This method add gift card item Info.
	 *
	 * @param pRequestPayload
	 * @return
	 * @throws DropletException
	 */
	public Object addGiftCardItemInfo(Map<String, Object> pRequestPayload) throws DropletException {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method addGiftCardItemInfo");
		}

		String lErrorMessage;

		DropletInvoker dropletInvoker = new DropletInvoker(DigitalGiftCardRestResourceConstants.GIFT_CARD_RESPONSE_DROPLET);

		OParam oParamOutput = dropletInvoker.addOParam(DigitalGiftCardRestResourceConstants.OUTPUT);
		Output lSuccessOutput = oParamOutput.addOutput(DigitalGiftCardRestResourceConstants.RESPONSE,
				DigitalGiftCardRestResourceConstants.RESPONSE);
		Output lSuccessOutputCount = oParamOutput.addOutput(DigitalGiftCardRestResourceConstants.ADD_ITEM_COUNT,
				DigitalGiftCardRestResourceConstants.ADD_ITEM_COUNT);

		OParam oErrorParam = dropletInvoker.addOParam(DigitalGiftCardRestResourceConstants.ERROR_SMALL);
		Output lErrorOutput = oErrorParam.addOutput(DigitalGiftCardRestResourceConstants.ERROR_MSG, DigitalGiftCardRestResourceConstants.ERROR_MSG);

		dropletInvoker.invoke();

		if (lSuccessOutput.getObject() != null && lSuccessOutputCount.getObject() != null) {
			GiftCardDTO[] lGiftCards = (GiftCardDTO[]) lSuccessOutput.getObject();
			FormHandlerExecutor executor = new FormHandlerExecutor(DigitalGiftCardRestResourceConstants.GIFT_CARD_FORMHANDLER,
					DigitalGiftCardRestResourceConstants.ADD_GIFTCARD_TO_ORDER);
			GiftCardFormHandler lFormHandler = (GiftCardFormHandler) executor.getFormHandler();
			lFormHandler.setGiftCardDTOList(lGiftCards);
			lFormHandler.setAddItemCount((int) lSuccessOutputCount.getObject());

			FormHandlerInvocationResult lResult = executor.execute();
			if (isLoggingDebug()) {
				logDebug("FormHandlerInvocationResult lResult ->" + lResult.toString() + lFormHandler.toString());
			}
			//TODO
			getCommonHelper().invokeRepriceOrderDroplet(null, DigitalGiftCardRestResourceConstants.ORDER_SUBTOTAL_SHIPPING);
			if (isLoggingDebug()) {
				logDebug("Exit ->" + getClass().getCanonicalName() + " Method addGiftCardItemInfo:lSuccessOutput.getObject() ");
			}

			return lSuccessOutput.getObject();
		} else {
			lErrorMessage = (String) lErrorOutput.getObject();
			if (isLoggingDebug()) {
				logDebug("Exit ->" + getClass().getCanonicalName() + " Method addGiftCardItemInfo:lErrorMessage");
			}
			return lErrorMessage;
		}

	}

	/**
	 * Remove gift card.
	 *
	 * @param pCardNumber
	 */
	public Object removeGCInfo(String pCardNumber) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method removeGCInfo");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();

		DigitalGCComponent lGiftCardComponent = (DigitalGCComponent) ServletUtil.getCurrentRequest().resolveName(DigitalGiftCardRestResourceConstants.GIFTCARD_COMPONENT);
		RemoveGiftCardResponse lResponse = lGiftCardComponent.removeGiftCard(pCardNumber);
		if (lResponse.getGenericExceptions() != null) {
			lResponseMap.put(DigitalGiftCardRestResourceConstants.FORM_ERROR, lResponse.getFormError());
			lResponseMap.put(DigitalGiftCardRestResourceConstants.GENERIC_EXCEPTION, lResponse.getGenericExceptions());
		} else {
			lResponseMap.put(DigitalGiftCardRestResourceConstants.GCSTATUS, lResponse.getGcStatus());
		}
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method removeGCInfo");
		}
		return lResponseMap;

	}

	/**
	 * Apply gift card
	 *
	 * @param pRequestPayload
	 * @return
	 */
	public Object applyGCInfo(Map<String, Object> pRequestPayload) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method applyGCInfo");
		}
		Map<String, Object> lResponseMap = new HashMap<String, Object>();

		String lCurrencyName = (String) pRequestPayload.get(DigitalGiftCardRestResourceConstants.CURRENCY_NAME);
		String lCardNumber = (String) pRequestPayload.get(DigitalGiftCardRestResourceConstants.CARD_NUMBER);
		String lPin = (String) pRequestPayload.get(DigitalGiftCardRestResourceConstants.Pin);

		DigitalGCComponent  lGiftCardComponent = (DigitalGCComponent) ServletUtil.getCurrentRequest().resolveName(DigitalGiftCardRestResourceConstants.GIFTCARD_COMPONENT);

		ApplyGiftCardResponse lResponse = lGiftCardComponent.applyGiftCard(lCurrencyName, lCardNumber, lPin);

		if(lResponse.getGenericExceptions() != null)
		{
			lResponseMap.put(DigitalGiftCardRestResourceConstants.FORM_ERROR, lResponse.getFormError());
			lResponseMap.put(DigitalGiftCardRestResourceConstants.GENERIC_EXCEPTION, lResponse.getGenericExceptions());
		}else
		{
			lResponseMap.put(DigitalGiftCardRestResourceConstants.GCSTATUS, lResponse.getGcStatus());
			lResponseMap.put(DigitalGiftCardRestResourceConstants.GIFTCARD_PG, lResponse.getGiftCardPaymentGroup());
			lResponseMap.put(DigitalGiftCardRestResourceConstants.GIFTCARD_NUMBER, lResponse.getGiftCardNumber());
			lResponseMap.put(DigitalGiftCardRestResourceConstants.PIN_NUMBER, lResponse.getPinNumber());
			lResponseMap.put(DigitalGiftCardRestResourceConstants.AMOUNT_REMAINING, lResponse.getAmountRemaining());
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method applyGCInfo");
		}
		return lResponseMap;
	}

}
