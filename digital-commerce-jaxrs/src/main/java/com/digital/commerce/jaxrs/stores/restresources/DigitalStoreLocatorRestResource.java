package com.digital.commerce.jaxrs.stores.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalStoreLocatorRestResourceConstants;
import com.digital.commerce.jaxrs.stores.helper.DigitalStoreLocatorRestResourceHelper;
import com.digital.commerce.services.storelocator.DigitalStoreLocatorModel;

import atg.core.util.StringUtils;
import atg.droplet.FormHandlerExecutor;
import atg.droplet.FormHandlerExecutor.FormHandlerInvocationResult;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;

/**
 * This class will provide RestResources for Store Location Services.
 * 
 * @author TAIS
 *
 */
@RestResource(id = "com.digital.commerce.jaxrs.stores.restresources.DigitalStoreLocatorRestResource")
@Path("/stores")
@Api("Stores Service")
@Getter
@Setter
public class DigitalStoreLocatorRestResource extends DigitalGenericService{
	
	private DigitalStoreLocatorRestResourceHelper storeLocatorHelper;
	
	/**
	 * This Service for fetch StoreLocator Results.
	 * @see - Actor Chain -/atg/commerce/locations/StoreLocatorActor/locateStores
	 * 
	 * @param pCountryCode
	 * @param pState
	 * @param pCity
	 * @param pPostalCode
	 * @param pDistance
	 * @param pLatitude
	 * @param pLongitude
	 * @param pItemId
	 * @param pFilterOutOfStock
	 * @param pMaxResultsPerPage
	 * @return Response
	 */
	@GET
	@Path("/locateStores")
	@Endpoint(id = "/stores/locateStores#GET", isSingular = true, filterId = "dsw-locateStores-response")
	public Response locateStoresDetail(@QueryParam(DigitalStoreLocatorRestResourceConstants.COUNTRY_CODE) String pCountryCode,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.STATE) String pState,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.CITY) String pCity,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.POSTAL_CODE) String pPostalCode,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.GEO_DISTANCE) String pDistance,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.GEO_LATITUDE) String pLatitude,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.GEO_LONGITUDE) String pLongitude,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.SKU_ID) String pSkuId,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.FILTER_OUT_OF_STOCK) String pFilterOutOfStock,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.MAX_RESULTS_PER_PAGE) String pMaxResultsPerPage) {
		
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method locateStores");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponseMap = new HashMap<>();
		Map<String, Object> lHelperMap = new HashMap<>();
		try {
			FormHandlerExecutor executor = new FormHandlerExecutor(
					DigitalStoreLocatorRestResourceConstants.STORE_LOCATOR_FORM_HANDLER_PATH,
					DigitalStoreLocatorRestResourceConstants.HANDLE_LOCATE_ITEMS);
			
			executor.addInput(DigitalStoreLocatorRestResourceConstants.COUNTRY_CODE,pCountryCode);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.STATE,pState);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.CITY,pCity);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.POSTAL_CODE,pPostalCode);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.GEO_DISTANCE,pDistance);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.GEO_LATITUDE,pLatitude);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.GEO_LONGITUDE,pLongitude);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.ITEM_ID,pSkuId);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.FILTER_OUT_OF_STOCK,pFilterOutOfStock);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.MAX_RESULTS_PER_PAGE,pMaxResultsPerPage);
			
			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lHelperMap = getStoreLocatorHelper().getCommonHelper().createGenericErrorResponse(lResult);
			} else {
				lHelperMap = getStoreLocatorHelper().populateStoreLocatorResponse(lResult);
				//calling Sorting logic
				sortStoreLocatorData(lHelperMap);
			}
			
			
			lFinalResponseMap.put(DigitalStoreLocatorRestResourceConstants.RESPONSE, lHelperMap);
			
		}catch (ServletException e) {
			vlogError(e, "ServletException during locateStores");
		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method locateStores");
		}
		return Response.ok(lFinalResponseMap).build();
	
	}
	
	
	/**
	 * This Service for fetch StoreLocator Results.
	 * @see - Actor Chain -/atg/commerce/locations/StoreLocatorActor/locateItems
	 * 
	 * @param pCountryCode
	 * @param pState
	 * @param pCity
	 * @param pPostalCode
	 * @param pDistance
	 * @param pLatitude
	 * @param pLongitude
	 * @param pItemId
	 * @param pFilterOutOfStock
	 * @param pMaxResultsPerPage
	 * @return Response
	 */
	@GET
	@Path("/locateItems")
	@Endpoint(id = "/stores/locateItems#GET", isSingular = true, filterId = "dsw-locateItems-response")
	public Response locateItemsDetail(@QueryParam(DigitalStoreLocatorRestResourceConstants.COUNTRY_CODE) String pCountryCode,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.STATE) String pState,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.CITY) String pCity,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.POSTAL_CODE) String pPostalCode,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.GEO_DISTANCE) String pDistance,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.GEO_LATITUDE) String pLatitude,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.GEO_LONGITUDE) String pLongitude,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.SKU_ID) String pSkuId,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.FILTER_OUT_OF_STOCK) String pFilterOutOfStock,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.LOCATE_ITEMS_INVENTORY) String pLocateItemsInventory,
			@QueryParam(DigitalStoreLocatorRestResourceConstants.MAX_RESULTS_PER_PAGE) String pMaxResultsPerPage) {
		
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method locateItems");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponseMap = new HashMap<>();
		Map<String, Object> lHelperMap = new HashMap<>();
		try {
			FormHandlerExecutor executor = new FormHandlerExecutor(
					DigitalStoreLocatorRestResourceConstants.STORE_LOCATOR_FORM_HANDLER_PATH,
					DigitalStoreLocatorRestResourceConstants.HANDLE_LOCATE_ITEMS);
			
			executor.addInput(DigitalStoreLocatorRestResourceConstants.COUNTRY_CODE,pCountryCode);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.STATE,pState);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.CITY,pCity);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.POSTAL_CODE,pPostalCode);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.GEO_DISTANCE,pDistance);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.GEO_LATITUDE,pLatitude);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.GEO_LONGITUDE,pLongitude);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.ITEM_ID,pSkuId);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.LOCATE_ITEMS_INVENTORY,pLocateItemsInventory);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.FILTER_OUT_OF_STOCK,pFilterOutOfStock);
			executor.addInput(DigitalStoreLocatorRestResourceConstants.MAX_RESULTS_PER_PAGE,pMaxResultsPerPage);
			
			FormHandlerInvocationResult lResult = executor.execute();
			if (lResult.isFormError()) {
				lHelperMap = getStoreLocatorHelper().getCommonHelper().createGenericErrorResponse(lResult);
			} else {
				lHelperMap = getStoreLocatorHelper().populateStoreLocatorResponse(lResult);
				
				if (StringUtils.isNotEmpty(pSkuId) && StringUtils.isNotEmpty(pFilterOutOfStock)
						&& lHelperMap != null&& lHelperMap.containsKey(DigitalStoreLocatorRestResourceConstants.STORE_LOCATOR_RESULTS_MAP)) {
					
					Map<String, DigitalStoreLocatorModel> lStoreLocatorResultsMap = (Map<String, DigitalStoreLocatorModel>)lHelperMap.get(DigitalStoreLocatorRestResourceConstants.STORE_LOCATOR_RESULTS_MAP);
					
					getStoreLocatorHelper().storeLocatorInventoryValidation(lStoreLocatorResultsMap, pSkuId, pFilterOutOfStock);
				}
				//calling Sorting logic
				sortStoreLocatorData(lHelperMap);
			}
			
			lFinalResponseMap.put(DigitalStoreLocatorRestResourceConstants.RESPONSE, lHelperMap);
			
		}catch (ServletException e) {
			vlogError(e, "ServletException during locateStores");
		}
		setResponseHeaders();
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method locateItems");
		}
		return Response.ok(lFinalResponseMap).build();
	
	}
	
	//TODO 
	/*
	 * need to resolve sorting issue.
	 * once we sorted map with distance attribute, the map is getting modified through payload schema.
	 * 
	 */
	
	/**
	 * Sort the StoreLocator Results Map and add to Final Restuls Map.
	 * @param pResutlsMap
	 */
	private void sortStoreLocatorData(Map<String,Object> pResutlsMap){
		
		if(pResutlsMap != null && pResutlsMap.containsKey(DigitalStoreLocatorRestResourceConstants.STORE_LOCATOR_RESULTS_MAP)){
			
			Map<String, DigitalStoreLocatorModel> lStoreLocatorModelMap = (Map<String, DigitalStoreLocatorModel>) pResutlsMap
					.get(DigitalStoreLocatorRestResourceConstants.STORE_LOCATOR_RESULTS_MAP);
			
			Map lSortedModelMap = getStoreLocatorHelper().sortMapData(lStoreLocatorModelMap, DigitalStoreLocatorRestResourceConstants.DISTANCE);
			if(lSortedModelMap != null && !lSortedModelMap.isEmpty()){
				pResutlsMap.remove(DigitalStoreLocatorRestResourceConstants.STORE_LOCATOR_RESULTS_MAP);
				pResutlsMap.putAll(lSortedModelMap);
			}
			if(isLoggingDebug()){
				vlogDebug("print sorted map :{0}", lSortedModelMap);
			}
			
		}
	}
}
