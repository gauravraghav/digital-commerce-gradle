package com.digital.commerce.jaxrs.session.helper;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import com.digital.commerce.jaxrs.common.constants.DigitalSessionConfirmationRestResourceConstants;

import atg.nucleus.GenericService;
import atg.service.jaxrs.DropletInvoker;
import atg.service.jaxrs.OParam;
import atg.service.jaxrs.Output;

/**
 * This class is used to provide response for DigitalSessionConfirmation actor.
 *
 * @author TAIS
 *
 */
public class DigitalSessionConfirmationRestResourceHelper extends GenericService {

	/**
	 * This method provide details for session confirmation number V1
	 * @return lResponseMap
	 * 
	 */
	public Map<String, Object> getSessionConfirmationNumberV1Info() throws ServletException{
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method getSessionConfirmationNumberV1Info");
		}
		Map<String, Object> lResponseMap = new HashMap<>();
		
		DropletInvoker dropletInvoker = new DropletInvoker(DigitalSessionConfirmationRestResourceConstants.SESSION_CONFIRMATION_DROPLET_V1);
		OParam oParamOutput = dropletInvoker.addOParam(DigitalSessionConfirmationRestResourceConstants.OUTPUT);
		Output lSuccessOutput = oParamOutput.addOutput(DigitalSessionConfirmationRestResourceConstants.SESSION_CONFIRMATION_NUMBER,
				DigitalSessionConfirmationRestResourceConstants.SESSION_CONFIRMATION_NUMBER);
		// TODO ErrorHandler 405
		dropletInvoker.invoke();
		if (lSuccessOutput.getObject() != null) {
			lResponseMap.put(DigitalSessionConfirmationRestResourceConstants.SESSION_CONFIRMATION_NUMBER, lSuccessOutput.getObject());
		}
		
		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method getSessionConfirmationNumberV1Info");
		}
		return lResponseMap;

	}

	/**
	 * This method provide details for session confirmation number V2
	 * @return lResponseMap
	 * 
	 */
	public Map<String, Object> getSessionConfirmationNumberV2Info() throws ServletException{

		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + "Method getSessionConfirmationNumberV2Info");
		}
		Map<String, Object> lResponseMap = new HashMap<>();

		DropletInvoker dropletInvoker = new DropletInvoker(DigitalSessionConfirmationRestResourceConstants.SESSION_CONFIRMATION_DROPLET_V2);
		OParam oParamOutput = dropletInvoker.addOParam(DigitalSessionConfirmationRestResourceConstants.OUTPUT);
		Output lSuccessOutput = oParamOutput.addOutput(DigitalSessionConfirmationRestResourceConstants.SESSION_CONFIRMATION_NUMBER,
				DigitalSessionConfirmationRestResourceConstants.SESSION_CONFIRMATION_NUMBER);

		dropletInvoker.invoke();
		// TODO ErrorHandler 405
		if (lSuccessOutput.getObject() != null) {
			lResponseMap.put(DigitalSessionConfirmationRestResourceConstants.SESSION_CONFIRMATION_NUMBER, lSuccessOutput.getObject());
		}

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + "Method getSessionConfirmationNumberV2Info");
		}
		return lResponseMap;
	}
	

}
