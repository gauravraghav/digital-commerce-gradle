package com.digital.commerce.jaxrs.paypal.helper;

import com.digital.commerce.jaxrs.common.helper.DigitalCommonRestResourceHelper;

import atg.nucleus.GenericService;
import lombok.Getter;
import lombok.Setter;

/**
 * This class provides paypalPaymentActor's chain ids implementations.
 *
 * @author TAIS
 *
 */
@Getter
@Setter
public class DigitalPaypalPaymentRestResourceHelper extends GenericService {

	/**
	 * commonHelper - DigitalCommonRestResourceHelper Component
	 */
	private DigitalCommonRestResourceHelper commonHelper;

}
