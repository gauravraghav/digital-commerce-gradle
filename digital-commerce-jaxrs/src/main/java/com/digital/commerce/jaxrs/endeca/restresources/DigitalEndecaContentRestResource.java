package com.digital.commerce.jaxrs.endeca.restresources;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.digital.commerce.common.nucleus.DigitalGenericService;
import com.digital.commerce.jaxrs.common.constants.DigitalJaxRsResourceCommonConstants;

import atg.service.jaxrs.DropletInvoker;
import atg.service.jaxrs.OParam;
import atg.service.jaxrs.Output;
import atg.service.jaxrs.RestException;
import atg.service.jaxrs.annotation.Endpoint;
import atg.service.jaxrs.annotation.RestResource;
import atg.servlet.ServletUtil;
import io.swagger.annotations.Api;

/**
 * DigitalEndecaContentRestResource - Service class for Endeca Content Services
 *
 * @author TAIS
 */
@RestResource(id = "com.digital.commerce.jaxrs.endeca.restresources.DigitalEndecaContentRestResource")
@Path("/content")
@Api("Endeca Content")
public class DigitalEndecaContentRestResource extends DigitalGenericService {

	/**
	 * Get endeca zone content service
	 *
	 * @see -/com/digital/commerce/endeca/controller/v1_0/content/ContentActor/getZoneContent
	 *
	 * @return Response
	 */
	@GET
	@Path("/getZoneContent")
	@Endpoint(id = "/content/getZoneContent#GET", isSingular = true, filterId = "dsw-endeca-zone-content-response")
	public Response getZoneContent(@QueryParam(DigitalJaxRsResourceCommonConstants.CONTENT_COLLECTION) String pContentCollection) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getZoneContent()");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();

		try {
			DropletInvoker lDropletInvoker = new DropletInvoker(DigitalJaxRsResourceCommonConstants.INVOKE_ASSEMBLER_DROPLET);
			lDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.CONTENT_COLLECTION, pContentCollection);
			OParam oParamOutput = lDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
			Output lSuccessOutput = oParamOutput.addOutput(DigitalJaxRsResourceCommonConstants.CONTENT_ITEM,
					DigitalJaxRsResourceCommonConstants.CONTENT_ITEM);

			lDropletInvoker.invoke();

			if (lSuccessOutput.getObject() != null) {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.CONTENT_CONTENTITEM, lSuccessOutput.getObject());
			} else {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.ERROR_SMALL, "Error Occurred.");
			}

		} catch (RestException e) {
			vlogError(e, "RestException");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getZoneContent()");
		}

		return Response.status(ServletUtil.getCurrentResponse().getStatus()).entity(lFinalResponse).build();

	}

	/**
	 * Get endeca page content service
	 *
	 * @see -/com/digital/commerce/endeca/controller/v2_0/content/ContentActor/getPageContent
	 *
	 *      Note - This is V2 service for page content
	 *
	 * @return Response
	 */
	@GET
	@Path("/getPageContentV2")
	@Endpoint(id = "/content/getPageContentV2#GET", isSingular = true, filterId = "dsw-endeca-page-content-response")
	public Response getPageContentV2(@QueryParam(DigitalJaxRsResourceCommonConstants.PAGE_PATH) String pPagePath) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getPageContentV2()");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();

		try {
			DropletInvoker lDropletInvoker = new DropletInvoker(DigitalJaxRsResourceCommonConstants.INVOKE_ASSEMBLER_DROPLET);
			lDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.INCLUDE_PATH, pPagePath);
			lDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.VERSION, DigitalJaxRsResourceCommonConstants.VERSION_2);
			OParam oParamOutput = lDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
			Output lSuccessOutput = oParamOutput.addOutput(DigitalJaxRsResourceCommonConstants.CONTENT_ITEM,
					DigitalJaxRsResourceCommonConstants.CONTENT_ITEM);

			lDropletInvoker.invoke();

			if (lSuccessOutput.getObject() != null) {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.PAGE_CONTENT_ITEM, lSuccessOutput.getObject());
			} else {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.ERROR_SMALL, "Error Occurred.");
			}

		} catch (RestException e) {
			vlogError(e, "RestException");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getPageContentV2()");
		}

		return Response.status(ServletUtil.getCurrentResponse().getStatus()).entity(lFinalResponse).build();

	}
	/**
	 * Get endeca page content service
	 *
	 * @see -/com/digital/commerce/endeca/controller/v1_0/content/ContentActor/getPageContent
	 *
	 *      Note - This is V1 service for page content
	 *
	 * @return Response
	 */
	@GET
	@Path("/getPageContent")
	@Endpoint(id = "/content/getPageContent#GET", isSingular = true, filterId = "dsw-endeca-page-content-response")
	public Response getPageContent(@QueryParam(DigitalJaxRsResourceCommonConstants.PAGE_PATH) String pPagePath) {
		if (isLoggingDebug()) {
			logDebug("Start ->" + getClass().getCanonicalName() + " Method getPageContent()");
		}
		setRequestedTime();
		Map<String, Object> lFinalResponse = new HashMap<>();

		try {
			DropletInvoker lDropletInvoker = new DropletInvoker(DigitalJaxRsResourceCommonConstants.INVOKE_ASSEMBLER_DROPLET);
			lDropletInvoker.addInput(DigitalJaxRsResourceCommonConstants.INCLUDE_PATH, pPagePath);
			OParam oParamOutput = lDropletInvoker.addOParam(DigitalJaxRsResourceCommonConstants.OUTPUT);
			Output lSuccessOutput = oParamOutput.addOutput(DigitalJaxRsResourceCommonConstants.CONTENT_ITEM,
					DigitalJaxRsResourceCommonConstants.CONTENT_ITEM);

			lDropletInvoker.invoke();

			if (lSuccessOutput.getObject() != null) {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.PAGE_CONTENT_ITEM, lSuccessOutput.getObject());
			} else {
				lFinalResponse.put(DigitalJaxRsResourceCommonConstants.ERROR_SMALL, "Error Occurred.");
			}

		} catch (RestException e) {
			vlogError(e, "RestException");
		}

		setResponseHeaders();

		if (isLoggingDebug()) {
			logDebug("Exit ->" + getClass().getCanonicalName() + " Method getPageContent()");
		}

		return Response.status(ServletUtil.getCurrentResponse().getStatus()).entity(lFinalResponse).build();

	}

}
