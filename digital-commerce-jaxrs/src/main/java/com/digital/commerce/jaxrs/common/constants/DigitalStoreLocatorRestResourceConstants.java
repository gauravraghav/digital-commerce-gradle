package com.digital.commerce.jaxrs.common.constants;

public class DigitalStoreLocatorRestResourceConstants extends DigitalJaxRsResourceCommonConstants {
	
	public static final String STORE_LOCATOR_FORM_HANDLER_PATH = "/atg/commerce/locations/StoreLocatorFormHandler";
	
	public static final String HANDLE_LOCATE_ITEMS = "locateItems";
	
	public static final String COUNTRY_CODE = "countryCode";
	
	public static final String DISTANCE = "distance";
	
	public static final String FILTER_OUT_OF_STOCK = "filterOutOfStock";
	
	public static final String MAX_RESULTS_PER_PAGE = "maxResultsPerPage";
	
	public static final String LOCATE_ITEMS_INVENTORY = "locateItemsInventory";
	
	public static final String INVENTORY_DROPLET_PARAM = "inventoryDropletParam";
	
	public static final String INVENTORY_INFO = "inventoryInfo";
	
	public static final String RESULTS_ARE_FROM_SEARC = "resultsAreFromSearch";
	
	public static final String TOTAL_COUNT = "totalCount";
	
	public static final String STORE_LEVEL_INVENTORY = "storeLevelInventory";
	
	public static final String STORE_LOCATOR_RESULTS_MAP = "StoreLocatorResultsMap";
	
	public static final String ITEM_ID = "itemId";

	public static final String GEO_DISTANCE = "geoDistance";

	public static final String GEO_LATITUDE = "geoLatitude";

	public static final String GEO_LONGITUDE = "geoLongitude";
	
	
}
