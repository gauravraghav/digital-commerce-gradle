package com.digital.commerce.jaxrs.common.constants;

public class DigitalLoggingActorConstants extends DigitalJaxRsResourceCommonConstants {

	public static final String CLIENT_LOGGER_PATH = "/com/digital/commerce/services/client/ClientLogger";

	public static final String LOGMESSAGE_METHOD = "logMessage";

}
