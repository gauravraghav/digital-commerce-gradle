package com.digital.commerce.services.order;

import java.io.Serializable;

public interface DigitalCommerceItemWrapper extends Serializable {

	public String getStoreId();
	
}
