package com.digital.commerce.common.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** This class contains static helper methods for dealing with cookies.
 * 
 * @author thanna */
public class DigitalCookieUtil {
	/** This method persists a URL parameter to a cookie. If the parameter exists on the URL, it will update the cookie
	 * and return it. If it doesn't exist on the URL, it will check to see if it has been stored as a cooke. If it
	 * has, it will return the cookie value.
	 * 
	 * @param request
	 * @param response
	 * @param paramName The name of the parameter you wish to fetch. This will also be used as the cookie name.
	 * @param maxAge The length of time the cookie should live. See the javax.servlet.http.Cookie documentation for details.
	 * @return */
	public static String getCookiedParameter( HttpServletRequest request, HttpServletResponse response, String paramName, int maxAge ) {
		String paramValue = request.getParameter( paramName );

		if( paramValue != null ) {
			Cookie cookie = new Cookie( paramName, paramValue );  //NOSONAR
			cookie.setMaxAge( maxAge );
			response.addCookie( cookie );
		} else {
			Cookie[] cookies = request.getCookies();
			if( cookies != null ) {
				for( int i = 0; i < cookies.length; i++ ) {
					if( paramName.equalsIgnoreCase( cookies[i].getName() ) ) {
						paramValue = cookies[i].getValue();
					}
				}
			}
		}
		return paramValue;
	}
}
