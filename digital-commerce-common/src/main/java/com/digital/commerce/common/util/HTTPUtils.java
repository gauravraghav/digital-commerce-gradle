package com.digital.commerce.common.util;

import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HTTPUtils {
	private HTTPUtils() {} /* static utility class, do not instantiate */

	/** Sets the cache control and expires headers
	 * 
	 * @param cacheSeconds Max seconds to cache for
	 * @param response HTTPServletResponse */
	public static void setCacheControlHeaders( int cacheSeconds, HttpServletResponse response, Date expirationDate ) {
		response.setDateHeader( "Expires", expirationDate.getTime() );
		long correctedCacheSeconds = ( expirationDate.getTime() - new Date().getTime() ) / 1000;
		if( cacheSeconds > 0 ) {
			response.setHeader( "Cache-Control", "max-age=" + correctedCacheSeconds );
		} else {
			response.setHeader( "Cache-Control", "max-age=0, no-cache, no-store" );
			response.setHeader( "Pragma", "no-cache" );
		}
	}

	/** Creates a session cookie and adds it to the response. Sets path to '/'
	 *
	 * @param name Cookie name
	 * @param value Cookie value
	 * @param response Respone object */
	public static void addSessionCookie( String name, String value, HttpServletResponse response, boolean secure, boolean isHttpOnly) {
		Cookie cookie = new Cookie( name, value );
		cookie.setPath( "/" );
		cookie.setHttpOnly(isHttpOnly);
		cookie.setSecure(secure);
		response.addCookie( cookie );
	}

	/** Finds a cookie with the given name and returns its value. Returns null if not found
	 * 
	 * @param cookieName
	 * @param request
	 * @return */
	public static String getCookieValue( String cookieName, HttpServletRequest request ) {
		Cookie[] cookies = request.getCookies();

		if( cookies != null ) {
			for( Cookie cookie : cookies ) {
				if( cookie.getName().equals( cookieName ) ) { return cookie.getValue(); }
			}
		}

		return null;
	}


	/**
	 * @param name
	 * @param value
	 * @param path
	 * @param expirySeconds
	 * @param isSecure
	 * @param domain
	 * @return
	 */
	public static Cookie createCookie( String name, String value, String path, int expirySeconds, boolean isSecure, String domain) {
		Cookie cookie = new Cookie( name, value );
		cookie.setPath(path);
		cookie.setMaxAge(expirySeconds);
		cookie.setSecure(isSecure);
		cookie.setDomain(domain);
		return cookie;
	}
	/**
	 * @param name
	 * @param value
	 * @param path
	 * @param expirySeconds
	 * @param domain
	 * @return
	 */
	public static Cookie createCookie(String name, String value,
			boolean isSecure, String path) {
		Cookie cookie = new Cookie(name, value);
		cookie.setSecure(isSecure);
		cookie.setPath(path);
		return cookie;
	}

	/**
	 * @param name
	 * @param value
	 * @param path
	 * @param expirySeconds
	 * @param isSecure
	 * @param domain
	 * @param httpOnly
	 * @return
	 */
	public static void createCookie(String name, String value, String path,
			int expirySeconds, boolean isSecure, String domain, boolean httpOnly,  HttpServletResponse response) {
		Cookie cookie = new Cookie(name, value);
		cookie.setPath(path);
		cookie.setMaxAge(expirySeconds);
		cookie.setSecure(isSecure);
		cookie.setDomain(domain);
		cookie.setHttpOnly(httpOnly);
		response.addCookie(cookie);
	}

	/**
	 * @param name
	 * @param value
	 * @param path
	 * @param isSecure
	 * @param isHttpOnly
	 * @return
	 */
	public static Cookie createCookie(String name, String value,
									  boolean isSecure, String path, boolean isHttpOnly) {
		Cookie cookie = new Cookie(name, value);
		cookie.setSecure(isSecure);
		cookie.setPath(path);
		cookie.setHttpOnly(isHttpOnly);
		return cookie;
	}
}
