package com.digital.commerce.common.listener;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.reflect.MethodUtils;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;

import atg.nucleus.ServiceException;
import atg.nucleus.logging.DebugLogEvent;
import atg.nucleus.logging.ErrorLogEvent;
import atg.nucleus.logging.ExternalLogSystemLogListener;
import atg.nucleus.logging.LogEvent;
import atg.nucleus.logging.TraceLogEvent;
import atg.nucleus.logging.WarningLogEvent;
import atg.nucleus.logging.commons.CommonsLoggingLogListener;
import lombok.Getter;
import lombok.Setter;

/** A customer listener to use instead of the {@link CommonsLoggingLogListener}. This streamlines the logging
 * functionality and fixes the issue where ATG still logs even if a category level is disabled (e.g., if debug is disabled and
 * you call logDebug("My message") it will still try to log "My message").
 * 
 * It will also expire entries in the cache after configurable time.
 * 
 * @author <a href="mailto:knaas@dswinc.com">knaas</a> */
@Getter
@Setter
public class Log4JLogListener extends ExternalLogSystemLogListener {
	private Map<String, String>		categories;
	private String					originatorFilterPattern	= "^(/atg/dynamo/servlet/pipeline/RequestScopeManager/RequestScope-[^/]*[/])|(/atg/dynamo/servlet/sessiontracking/GenericSessionManager/[^/]*/[^/]*/)";
	private int						maximumSize				= 50000;
	private int						duration				= 1;
	private TimeUnit				timeunit				= TimeUnit.DAYS;
	
	static final String				SESSION_ID_TXT_PREFIX	= " JSESSIONID:";
	static final String				COMMA_DELIMITER			= ", ";
	static final String 			UNIQUE_ID_KEY 			= "UNIQUE_ID";
	static final String 			APACHE_UNIQUE_ID_PREFIX = "APACHE_UNIQUE_ID:";
	static final String 			CLIENT_SOURCE_IP_KEY	= "Client-Source-IP";
	static final String 			CLIENT_SOURCE_IP_PREFIX	= "Client-Source-IP:";
	static final String 			SOURCE_IP_KEY			= "SOURCE_IP";
	static final String 			SOURCE_IP_KEY_PREFIX	= "SOURCE_IP:";
	static final String 			PROFILE_ID				= "DYN_USER_ID";
	static final String 			PROFILE_ID_PREFIX		= "PROFILE_ID:";
	
	@Override
	public void doStartService() throws ServiceException {
		super.doStartService();
	}

	/** Logs the event to the specified category. This handles trace logging differently than ATG in that
	 * it actually attempts to log to the TRACE priority if it is supported. If not it logs to the DEBUG priority. */
	public void logEvent( LogEvent logEvent ) {
		
		final DigitalLogger logger = getLogger( logEvent );
		
		if( logEvent instanceof ErrorLogEvent ) {
			if( null!=logger && logger.isErrorEnabled() ) {
					logger.error( logEvent.getMessage(), logEvent.getThrowable() );
			}
		} else if( logEvent instanceof WarningLogEvent ) {
			if( null!=logger && logger.isWarnEnabled() ) {
					logger.warn(logEvent.getMessage(), logEvent.getThrowable());
			}
		} else if( logEvent instanceof DebugLogEvent ) {
			if( null!=logger && logger.isDebugEnabled() ) {
					logger.debug( logEvent.getMessage(), logEvent.getThrowable() );
			}
		} else if( logEvent instanceof TraceLogEvent ) {

			Method traceMethod = null;
			boolean enabled = true;
			Method isEnabledMethod=null;
			if(null!=logger){
				 isEnabledMethod = MethodUtils.getMatchingAccessibleMethod( logger.getClass(), "isTraceEnabled", null );
			}
			if( isEnabledMethod != null ) {
				try {
					enabled = Boolean.TRUE.equals( isEnabledMethod.invoke( logger ) );
					if( enabled ) {
						traceMethod = MethodUtils.getMatchingAccessibleMethod( logger.getClass(), "trace", new Class[] { Object.class, Throwable.class } );
					}
				} catch( Exception e ) {
					enabled = logger.isDebugEnabled();
					if( enabled ) {
							logger.debug( "Unable to detect if trace is enabled", e );
					}
				}
			} else {
				if(null!=logger) {
				enabled = logger.isDebugEnabled();
				}
			}
			if( enabled ) {
				if( traceMethod != null ) {
					try {
							traceMethod.invoke( logger, logEvent.getMessage(), logEvent.getThrowable() );
					} catch( Exception e ) {
						enabled = logger.isDebugEnabled();
						if( enabled ) {
								logger.debug( "Unable to invoke trace logging", e );
								logger.debug( logEvent.getMessage(), logEvent.getThrowable() );
						}
					}
				} else {
					if(null!=logger) {
						logger.debug( logEvent.getMessage(), logEvent.getThrowable() );
					}
				}
			}
		} else {
			if( null!=logger && logger.isInfoEnabled() ) {
					logger.info( logEvent.getMessage(), logEvent.getThrowable() );
			}
		}
	}

	/** Returns a {@link Logger} for the given event. The general idea is to convert the originator to a
	 * java package.class. For originators that are without a package (e.g., *License.properties) we
	 * support a custom category mapping. If the originator is completely empty we use the current class.
	 * We cache the {@link Logger} so that it can be looked up quickly later.
	 * 
	 * @param logEvent
	 * @return */
	private DigitalLogger getLogger( LogEvent logEvent ) {
		final String originator = cleanOriginator( logEvent.getOriginator() );
		DigitalLogger logger = null;
		try {
			return DigitalLogger.getLogger( originator ); 
		} catch( Exception e ) {
			System.err.println( "Unable to allocate a logger " + originator + " because of:" + e );
		}
		return logger;
	}

	private String cleanOriginator( String originator ) {
		originator = DigitalStringUtil.trimToEmpty( originator ).replaceAll( DigitalStringUtil.trimToEmpty( originatorFilterPattern ), "" );
		if( DigitalStringUtil.isNotBlank( originator ) ) {
			if( originator.charAt( 0 ) == '/' ) {
				originator = originator.substring( 1 );
			}
			originator = originator.replace( '/', '.' );
			if( categories.containsKey( originator ) ) {
				originator = categories.get( originator );
			}
		} else {
			originator = Log4JLogListener.class.getName();
		}
		return originator;
	}
}
