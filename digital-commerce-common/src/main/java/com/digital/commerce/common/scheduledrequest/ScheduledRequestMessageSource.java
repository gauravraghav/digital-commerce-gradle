package com.digital.commerce.common.scheduledrequest;

import com.digital.commerce.common.userprofiling.DigitalMessageSource;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduledRequestMessageSource extends DigitalMessageSource {
	private String	outputPortName;
	private String	scheduledRequestMessageJMSType;

	public void fireScheduledRequestMessage( ScheduledRequest pScheduledRequest ) {
		if( isLoggingDebug() ) logDebug( "\n Sending message " + pScheduledRequest + "\n" );
		fireObjectMessage( pScheduledRequest, getOutputPortName(), getScheduledRequestMessageJMSType() );
	}
}
