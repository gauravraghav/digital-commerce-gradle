package com.digital.commerce.common.util;


import com.digital.commerce.common.exception.DataAccessException;

import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryItem;

public interface IRepositoryHandler {
	public Repository getRepository();

	/** Adds an item based on the itemdescriptor and executes the callback to allow any properties to be set. This
	 * allows the repository specific activity to be hidden from the caller.
	 * 
	 * @param itemDescriptor
	 * @param callback
	 * @return
	 * @throws DataAccessException */
	public MutableRepositoryItem add( String itemDescriptor, SaveCallback<MutableRepositoryItem> callback ) throws DataAccessException;

	/** Saves an item based on the id and itemdescriptor and executes the callback to allow any properties to be set. This
	 * allows the repository specific activity to be hidden from the caller.
	 * 
	 * @param id
	 * @param itemDescriptor
	 * @param callback
	 * @return
	 * @throws DataAccessException */
	public MutableRepositoryItem save( String id, String itemDescriptor, SaveCallback<MutableRepositoryItem> callback ) throws DataAccessException;

	/** Removes the item with the given id and item descriptor.
	 * 
	 * @param id
	 * @param itemDescriptor
	 * @throws DataAccessException */
	public void remove( String id, String itemDescriptor ) throws DataAccessException;

	public RepositoryItem findById( String id, String itemDescriptor ) throws DataAccessException;

	public RepositoryItem[] findByRQL( String rql, String itemDescriptor ) throws DataAccessException;

	public RepositoryItem findOneByRQL( String rql, String itemDescriptor ) throws DataAccessException;

	public RepositoryItem[] findByRQL( String rql, String itemDescriptor, Object... params ) throws DataAccessException;

	public RepositoryItem findOneByRQL( String rql, String itemDescriptor, Object... params ) throws DataAccessException;

	public RepositoryItem[] findBySQL( String sql, String itemDescriptor ) throws DataAccessException;

	public RepositoryItem findOneBySQL( String sql, String itemDescriptor ) throws DataAccessException;

	public RepositoryItem[] findBySQL( String sql, String itemDescriptor, int maxResults ) throws DataAccessException;

	public RepositoryItem[] findBySQL( String sql, String itemDescriptor, int startFrom, int maxResults ) throws DataAccessException;

	// public void executeSQL( String sql ) throws DataAccessException;
}
