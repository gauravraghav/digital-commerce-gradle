package com.digital.commerce.common.nucleus;

import java.util.concurrent.TimeUnit;

import com.digital.commerce.common.logger.LoggingUtil;

import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

/**
 * DigitalGenericService - Class written to provide common methods in all classes
 * whichever extends GenericService
 *
 * @author TAIS
 *
 */
public class DigitalGenericService extends GenericService {

    /**
     * Method to be called before any service call to put time in request object
     */
    public void setRequestedTime() {

	if (isLoggingDebug()) {
	    logDebug("Entering into method DigitalGenericService.setRequestedTime");
	}

	DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();

	long reqTime = System.nanoTime();

	if (isLoggingDebug()) {
	    logDebug("Setting requested time to the header-->" + reqTime);
	}
	request.setAttribute("dateValue", reqTime);

	if (isLoggingDebug()) {
	    logDebug("Exiting from method DigitalGenericService.setRequestedTime");
	}
    }

    /**
     * Method to be called before after service call to log time if debug
     * enabled
     */
    public void setResponseHeaders() {

	if (isLoggingDebug()) {
	    logDebug("Entering into method DigitalGenericService.setResponseHeaders");
	}

	DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();

	Object dateValue = request.getAttribute("dateValue");
	long reqInTime = (dateValue != null ? (Long) dateValue : 0);

	if (reqInTime == 0) {
	    if (isLoggingInfo()) {
		logInfo("Service " + request.getRequestURI() + " requested time cannot be traced");
	    }
	} else {
	    if (isLoggingDebug()) {
		final String requestInfo = LoggingUtil.getRequestInfo(request);
		StringBuilder logEntry = new StringBuilder();

		logEntry.append("[").append(requestInfo).append("EXECUTION_TIME: ")
		.append(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - reqInTime)).append(" ms ");

		logDebug(logEntry.toString());
	    }
	}

	if (isLoggingDebug()) {
	    logDebug("Exiting from method DigitalGenericService.setResponseHeaders");
	}
    }

}
