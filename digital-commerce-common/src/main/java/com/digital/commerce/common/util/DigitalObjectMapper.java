package com.digital.commerce.common.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

/**
 * Created by PR412029
 */
public class DigitalObjectMapper extends com.fasterxml.jackson.databind.ObjectMapper {

    public DigitalObjectMapper() {
        super();
        this.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        // this.setDateFormat(new ISO8601DateFormat());
        this.setDefaultPropertyInclusion(
            JsonInclude.Value.construct(JsonInclude.Include.ALWAYS, Include.NON_NULL));
        this.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    }

}
