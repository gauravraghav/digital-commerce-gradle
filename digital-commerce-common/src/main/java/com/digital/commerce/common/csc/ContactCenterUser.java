package com.digital.commerce.common.csc;

import atg.nucleus.logging.ApplicationLoggingImpl;
import lombok.Getter;
import lombok.Setter;

/**
 * @author psinha
 * @author rc402866 - added groups property
 * 
 */
@Getter
@Setter
public class ContactCenterUser extends ApplicationLoggingImpl {

	private String firstName;
	private String lastName;
	private String userId;
	private String role;
	private String remoteAddress;
	private String[] groups;

}
