package com.digital.commerce.common.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ResponseWrapper {
	private List<ResponseError> genericExceptions = new ArrayList<>();
	private boolean isRequestSuccess = true;
	private Boolean formError = false;

}
