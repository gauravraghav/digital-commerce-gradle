package com.digital.commerce.common.rewards.calculators;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.OrderImpl;
import atg.commerce.pricing.Constants;
import atg.commerce.pricing.DiscountCalculatorService;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.OrderPricingCalculator;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.definition.MatchingObject;
import atg.repository.RepositoryItem;

/** @author wibrahim */
public class DigitalOrderRewardPointsCalculator extends DiscountCalculatorService implements OrderPricingCalculator {

	// -------------------------------------
	/** Adds non-monetary rewards to this order if they apply The order's price
	 * *IS NOT* adjusted
	 * 
	 * @param pPriceQuote
	 *            OrderPriceInfo representing the current price quote for the
	 *            order
	 * @param pOrder
	 *            The order to price
	 * @param pPricingModel
	 *            A RepositoryItems representing a PricingModel
	 * @param pProfile
	 *            The user's profile
	 * @param pExtraParameters
	 *            A Map of extra parameters to be used in the pricing, may be
	 *            null */

	@SuppressWarnings("deprecation")
	public void priceOrder( OrderPriceInfo pPriceQuote, Order pOrder, RepositoryItem pPricingModel, Locale pLocale, RepositoryItem pProfile, @SuppressWarnings("rawtypes") Map pExtraParameters ) throws PricingException {

		if( isLoggingDebug() ) {
			logDebug( "Finding rewards promotions for Order : " + pOrder.getId() );
		}

		// pull items out of order
		@SuppressWarnings("unchecked")
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();

		// discover the items' current prices
		List<ItemPriceInfo> priceQuotes = new ArrayList<>(commerceItems.size());
		Iterator<CommerceItem> itemIterator = commerceItems.iterator();
		while( itemIterator.hasNext() ) {
			CommerceItem item = itemIterator.next();
			priceQuotes.add( item.getPriceInfo() );
		}

		MatchingObject ret = null;
		if( getQualifierService() != null ) {
			if( isLoggingDebug() ) {
				logDebug( "Finding if order qualifies for promo " + pPricingModel.getRepositoryId() );
			}

			ret = getQualifierService().findQualifyingOrder( priceQuotes, commerceItems, pPricingModel, pProfile, pLocale, pOrder, pPriceQuote, null, null, pExtraParameters );

			// if there's no qualifying order, simply return
			if( ret == null ) {

				if( isLoggingDebug() ) {
					logDebug( "order : " + pOrder.getId() + " didn't qualify." );
				}
				return;
			}

		} else {
			throw new PricingException( Constants.QUALIFIER_SERVICE_NOT_SET );
		}

		// the qualifier was met. Add the reward to the order

		if( isLoggingDebug() ) {
			logDebug( "Adding rewards promo to the order : " + pOrder.getId() );
		}
		OrderImpl order = (OrderImpl)pOrder;
		@SuppressWarnings("unchecked")
		List<String> rewards = (List<String>)order.getPropertyValue( "orderRewards" );
		if( rewards == null ) {
			rewards = new LinkedList<>();
		}
		rewards.add( pPricingModel.getRepositoryId() );
		order.setPropertyValue( "orderRewards", rewards );
	} // end priceOrder

}
