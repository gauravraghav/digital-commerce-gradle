package com.digital.commerce.common.services.pricing;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import atg.commerce.pricing.priceLists.Constants;
import atg.commerce.pricing.priceLists.PriceListException;
import atg.commerce.pricing.priceLists.PriceListManager;
import atg.multisite.Site;
import atg.nucleus.ServiceException;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.QueryOptions;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.SortDirective;
import atg.repository.SortDirectives;
import atg.userprofiling.Profile;

import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalPriceListManager extends PriceListManager {

	private static final String PRICE_CACHE_NAME = "/digital/edt/pricing/PriceCache";
	
	private static double NEGATIVE_PRICE_CACHE_HIT = -101;
	
	private static final Object EMPTY_OBJECT = new Object();

	private String	startDatePropertyName	= "startDate";
	private String	endDatePropertyName	= "endDate";
	private String	basePriceListDisplayPropertyName ;
	private String	salePriceListDisplayPropertyName ;
	private String 	displayPropertyName;
	private String 	parentProductsPropertyName;
	private String 	originalPricePropertyName;
	private String 	listPricePropertyName;

	/** According to the value of <code>ignoreProductFirst</code> calls
	 * the various getPrice methods until a price is found or all the
	 * methods have been tried. If <code>useBasePriceList</code> is
	 * true then the basePriceList is searched if a price is not found
	 * in the current priceList.
	 * 
	 * @param pPriceList The priceList to look in
	 * @param pProductId The id of the product we are interested in. Can be null
	 * @param pSkuId The id of the sku we are interested in. Can be null. **/
	public RepositoryItem getPriceForReprice( RepositoryItem pPriceList, String pProductId, String pSkuId, String pParentSkuId, Date orderDate ) throws PriceListException {
		final String METHOD_NAME="getPriceForReprice";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		
			RepositoryItem price = null;
	
			if( ( pProductId == null ) && ( pSkuId == null ) ) { return null; }
	
			if( pPriceList != null ) {
				if( isIgnoreProductFirst() ) {
					if( isLoggingDebug() ) logDebug( "Ignore product first, look for sku." );
					price = lookForPriceForReprice( pPriceList, null, pSkuId, pParentSkuId, orderDate );
				} else {
					if( isLoggingDebug() ) logDebug( "Ignore product is false, use product and sku." );
					price = lookForPriceForReprice( pPriceList, pProductId, pSkuId, pParentSkuId, orderDate );
				}
				if( price == null ) {
					if( isIgnoreProductFirst() ) {
						if( isLoggingDebug() ) logDebug( "Ignore product first, we looked for sku, now use product and sku" );
	
						price = lookForPriceForReprice( pPriceList, pProductId, pSkuId, pParentSkuId, orderDate );
					} else {
						if( isLoggingDebug() ) logDebug( "Ignore product is false, we used product and sku, now use just the sku" );
	
						price = lookForPriceForReprice( pPriceList, null, pSkuId, pParentSkuId, orderDate );
					}
					if( price == null ) {
						if( isLoggingDebug() ) logDebug( "Use just the product." );
						price = lookForPriceForReprice( pPriceList, pProductId, null, null, orderDate );
					}
					if( price == null && isUseSkuOnly() ) {
						if( isLoggingDebug() ) logDebug( "Everything else failed, use just the sku." );
	
						price = lookForPriceForReprice( pPriceList, null, pSkuId, null, orderDate );
					}
				}
			}
	
			if( price == null ) return getPrice( pPriceList, pProductId, pSkuId, isUseCache() );
	
			return price;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/** Build the query that searches the priceList for the price, OVERRIDING THIS METHOD TO SUPPORT START DATE AND END DATE ON A PRICELIST.
	 * If
	 * pProduct, pSku, or pParentSku is null, it is not ignored. The corresponding
	 * property in the price must also be null to be returned by the
	 * query. The query is then executed and the result is returned.
	 * An error is logged if the query returns more than one price. In
	 * that case, the first price in the result set is returned.
	 * 
	 * @param pPriceList The pricelist to search in
	 * @param pProductId The product value to use. May be null
	 * @param pSkuId The sku value to use. May be null
	 * @param pParentSkuId The parent sku value to use. May be null
	 * @return A price repository item
	 * @exception PriceListException **/
	protected RepositoryItem lookForPrice( RepositoryItem pPriceList, String pProductId, String pSkuId, String pParentSkuId ) throws PriceListException {
		final String METHOD_NAME="lookForPrice";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			final Calendar cal = Calendar.getInstance();
			cal.set( Calendar.HOUR_OF_DAY, 0 );
			cal.set( Calendar.MINUTE, 10 );
			cal.set( Calendar.SECOND, 0 );
			cal.set( Calendar.MILLISECOND, 0 );

			Timestamp dateSearch = new Timestamp(cal.getTimeInMillis());
			
			final String key = getKey(
					pPriceList == null ? null : pPriceList.getRepositoryId(),
					pProductId, pSkuId, pParentSkuId, dateSearch.toString());
			
			Object item = null;
			final PriceCache requestPriceCache = ComponentLookupUtil.lookupComponent(PRICE_CACHE_NAME, PriceCache.class);
			if (requestPriceCache != null) {
				item = requestPriceCache.getItem(key);
				if (item != null) {
					// Since we also want to use cache for null price objects
					// we check if stored price is a repository item
					if (item instanceof RepositoryItem) {
						return (RepositoryItem) item;
					}
					// Looks like key has no price object 
					return null;
					
				}
			}

			Repository priceListRep = getPriceListRepository();
			RepositoryView priceView = priceListRep.getView( getPriceViewName() );
			QueryBuilder qb = priceView.getQueryBuilder();

			Query priceListMatch;
			Query productMatch;
			Query skuMatch;
			Query parentSkuMatch;

			QueryExpression priceListProperty = qb.createPropertyQueryExpression( getPriceListPropertyName() );
			QueryExpression priceList = qb.createConstantQueryExpression( pPriceList );
			priceListMatch = qb.createComparisonQuery( priceList, priceListProperty, QueryBuilder.EQUALS );

			QueryExpression productProperty = qb.createPropertyQueryExpression( getProductIdPropertyName() );
			if( pProductId == null ) {
				productMatch = qb.createIsNullQuery( productProperty );
			} else {
				QueryExpression product = qb.createConstantQueryExpression( pProductId );
				productMatch = qb.createComparisonQuery( product, productProperty, QueryBuilder.EQUALS );
			}

			QueryExpression skuProperty = qb.createPropertyQueryExpression( getSkuIdPropertyName() );
			if( pSkuId == null ) {
				skuMatch = qb.createIsNullQuery( skuProperty );
			} else {
				QueryExpression sku = qb.createConstantQueryExpression( pSkuId );
				skuMatch = qb.createComparisonQuery( sku, skuProperty, QueryBuilder.EQUALS );
			}

			QueryExpression parentSkuProperty = qb.createPropertyQueryExpression( getParentSkuIdPropertyName() );
			if( pParentSkuId == null ) {
				parentSkuMatch = qb.createIsNullQuery( parentSkuProperty );
			} else {
				QueryExpression parentSku = qb.createConstantQueryExpression( pParentSkuId );
				parentSkuMatch = qb.createComparisonQuery( parentSku, parentSkuProperty, QueryBuilder.EQUALS );
			}

			Query priceStartDateMatch = null;
			QueryExpression startDateProperty = qb.createPropertyQueryExpression( getStartDatePropertyName() );
			if( dateSearch != null ) {
				QueryExpression orderDate = qb.createConstantQueryExpression( dateSearch );
				priceStartDateMatch = qb.createComparisonQuery( orderDate, startDateProperty, QueryBuilder.GREATER_THAN_OR_EQUALS );
			}

			Query priceEndDateMatch = null;
			QueryExpression endDateProperty1 = qb.createPropertyQueryExpression( getEndDatePropertyName() );
			if( dateSearch != null ) {
				QueryExpression orderDate = qb.createConstantQueryExpression( dateSearch );
				priceEndDateMatch = qb.createComparisonQuery( orderDate, endDateProperty1, QueryBuilder.LESS_THAN_OR_EQUALS );
			}

			Query priceEndDateMatchNull = null;
			QueryExpression endDateProperty2 = qb.createPropertyQueryExpression( getEndDatePropertyName() );
			if( dateSearch != null ) {
				priceEndDateMatchNull = qb.createIsNullQuery( endDateProperty2 );
			}

			Query[] endDateQueries = new Query[2];
			endDateQueries[0] = priceEndDateMatch;
			endDateQueries[1] = priceEndDateMatchNull;

			Query endDateQuery = qb.createOrQuery( endDateQueries );

			// sort the results by descending start date.
			SortDirectives sortDirectives = new SortDirectives();
			sortDirectives.addDirective( new SortDirective( getStartDatePropertyName(), SortDirective.DIR_DESCENDING ) );
			Query[] queries = new Query[6];
			queries[0] = priceListMatch;
			queries[1] = productMatch;
			queries[2] = skuMatch;
			queries[3] = parentSkuMatch;
			queries[4] = priceStartDateMatch;
			queries[5] = endDateQuery;

			Query findPrice = qb.createAndQuery( queries );
			if( isLoggingDebug() ) logDebug( "Executing query:" + priceView + ", query= " + findPrice );

			RepositoryItem[] prices = priceView.executeQuery( findPrice, new QueryOptions( 0, 5, sortDirectives, null ) );

			// Cache the results from the query if non-null
			// else, cache an empty Object
			if (requestPriceCache != null) {
				requestPriceCache.putItem(key, 
						prices == null || prices.length == 0 ? EMPTY_OBJECT : prices[0]);
			}
			if( prices == null ) {
				if( isLoggingDebug() ) logDebug( "Query returned nothing." );
				return null;
				// get the default price.
				// return super.lookForPrice(pPriceList, pProductId, pSkuId, pParentSkuId);
			}

			int length = prices.length;
			if( length == 0 ) {
				if( isLoggingDebug() ) logDebug( "NO PRICES RETURNED ------------------------>" );
				return null;
			}
			if( length > 1 ) {
				if( isLoggingError() ) {
					logError( Constants.QUERY_RETURNED_TOO_MANY_PRICES );
				}
			}
			if( isLoggingDebug() ) logDebug( "price returned: " + prices[0] );
			return prices[0];
		} catch( RepositoryException exc ) {
			throw new PriceListException( exc );
		} catch( Exception e ) {
			logError( e );
			throw new PriceListException( e );
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}// end finally
	}

	/** Build the query that searches the priceList for the price, THIS METHOD IS USED ONLY BY THE REPRICING REQUEST. If
	 * pProduct, pSku, or pParentSku is null, it is not ignored. The corresponding
	 * property in the price must also be null to be returned by the
	 * query. The query is then executed and the result is returned.
	 * An error is logged if the query returns more than one price. In
	 * that case, the first price in the result set is returned.
	 * 
	 * @param pPriceList The pricelist to search in
	 * @param pProductId The product value to use. May be null
	 * @param pSkuId The sku value to use. May be null
	 * @param pParentSkuId The parent sku value to use. May be null
	 * @return A price repository item
	 * @exception PriceListException **/
	protected RepositoryItem lookForPriceForReprice( RepositoryItem pPriceList, String pProductId, String pSkuId, String pParentSkuId, Date orderDateSearch ) throws PriceListException {
		final String METHOD_NAME="lookForPriceForReprice";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			Timestamp dateSearch = new Timestamp( orderDateSearch.getTime() );
			Repository priceListRep = getPriceListRepository();
			RepositoryView priceView = priceListRep.getView( getPriceViewName() );
			Query priceListMatch;
			Query productMatch;
			Query skuMatch;
			Query parentSkuMatch;

			QueryBuilder qb = priceView.getQueryBuilder();

			QueryExpression priceListProperty = qb.createPropertyQueryExpression( getPriceListPropertyName() );
			QueryExpression priceList = qb.createConstantQueryExpression( pPriceList );
			priceListMatch = qb.createComparisonQuery( priceList, priceListProperty, QueryBuilder.EQUALS );

			QueryExpression productProperty = qb.createPropertyQueryExpression( getProductIdPropertyName() );
			if( pProductId == null ) {
				productMatch = qb.createIsNullQuery( productProperty );
			} else {
				QueryExpression product = qb.createConstantQueryExpression( pProductId );
				productMatch = qb.createComparisonQuery( product, productProperty, QueryBuilder.EQUALS );
			}

			QueryExpression skuProperty = qb.createPropertyQueryExpression( getSkuIdPropertyName() );
			if( pSkuId == null ) {
				skuMatch = qb.createIsNullQuery( skuProperty );
			} else {
				QueryExpression sku = qb.createConstantQueryExpression( pSkuId );
				skuMatch = qb.createComparisonQuery( sku, skuProperty, QueryBuilder.EQUALS );
			}

			QueryExpression parentSkuProperty = qb.createPropertyQueryExpression( getParentSkuIdPropertyName() );
			if( pParentSkuId == null ) {
				parentSkuMatch = qb.createIsNullQuery( parentSkuProperty );
			} else {
				QueryExpression parentSku = qb.createConstantQueryExpression( pParentSkuId );
				parentSkuMatch = qb.createComparisonQuery( parentSku, parentSkuProperty, QueryBuilder.EQUALS );
			}

			Query priceStartDateMatch = null;
			QueryExpression startDateProperty = qb.createPropertyQueryExpression( getStartDatePropertyName() );
			if( dateSearch != null ) {
				QueryExpression orderDate = qb.createConstantQueryExpression( dateSearch );
				priceStartDateMatch = qb.createComparisonQuery( orderDate, startDateProperty, QueryBuilder.GREATER_THAN_OR_EQUALS );
			}

			Query priceEndDateMatch = null;
			QueryExpression endDateProperty1 = qb.createPropertyQueryExpression( getEndDatePropertyName() );
			if( dateSearch != null ) {
				QueryExpression orderDate = qb.createConstantQueryExpression( dateSearch );
				priceEndDateMatch = qb.createComparisonQuery( orderDate, endDateProperty1, QueryBuilder.LESS_THAN_OR_EQUALS );
			}

			Query priceEndDateMatchNull = null;
			QueryExpression endDateProperty2 = qb.createPropertyQueryExpression( getEndDatePropertyName() );
			if( dateSearch != null ) {
				priceEndDateMatchNull = qb.createIsNullQuery( endDateProperty2 );
			}

			Query[] endDateQueries = new Query[2];
			endDateQueries[0] = priceEndDateMatch;
			endDateQueries[1] = priceEndDateMatchNull;

			Query endDateQuery = qb.createOrQuery( endDateQueries );

			// sort the results by descending start date.
			SortDirectives sortDirectives = new SortDirectives();
			sortDirectives.addDirective( new SortDirective( getStartDatePropertyName(), SortDirective.DIR_DESCENDING ) );
			Query[] queries = new Query[6];
			queries[0] = priceListMatch;
			queries[1] = productMatch;
			queries[2] = skuMatch;
			queries[3] = parentSkuMatch;
			queries[4] = priceStartDateMatch;
			queries[5] = endDateQuery;

			Query findPrice = qb.createAndQuery( queries );
			if( isLoggingDebug() ) logDebug( "Executing query:" + priceView + ", query= " + findPrice );

			RepositoryItem[] prices = priceView.executeQuery( findPrice, new QueryOptions( 0, 5, sortDirectives, null ) );

			if( prices == null ) {
				if( isLoggingDebug() ) logDebug( "Query returned nothing." );
				return null;
				// get the default price.
				// return lookForPrice(pPriceList, pProductId, pSkuId, pParentSkuId);
			}

			int length = prices.length;
			if( length == 0 ) { return null; }
			if( length > 1 ) {
				if( isLoggingError() ) {
					logError( Constants.QUERY_RETURNED_TOO_MANY_PRICES );
				}
			}
			return prices[0];
		} catch( RepositoryException exc ) {
			throw new PriceListException( exc );
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}// end finally
	}

	/** the following code is copied from fooPriceListManager, which is useful for
	 * the pricing display purpose. */

	private String	mSalePriceListId;

	/** <p>
	 * The id of the sale price list
	 * 
	 * @param pSalePriceListId */
	public void setSalePriceListId( String pSalePriceListId ) {
		mSalePriceListId = pSalePriceListId;
	}

	/** <p>
	 * The id of the sale price list
	 * 
	 * @param pSalePriceListId */
	public String getSalePriceListId() {
		return mSalePriceListId;
	}

	private RepositoryItem	mSalePriceList;

	/** <p>
	 * The sale price list to use for eCommerce
	 * 
	 * @param pSalePriceList */
	protected void setSalePriceList( RepositoryItem pSalePriceList ) {
		mSalePriceList = pSalePriceList;
	}

	/** <p>
	 * The sale price list to use for eCommerce
	 * 
	 * @return */
	public RepositoryItem getSalePriceList() {
		final String METHOD_NAME="getSalePriceList";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			RepositoryItem salePriceList = mSalePriceList;
			Site site = MultiSiteUtil.getSite();
			if(site != null){
				try {
					salePriceList = getPriceListForSite(getSalePriceListPropertyName());
				} catch (RepositoryException e) {
					logError("Due to error getting site specific SalePriceList, returning default salepriceList ", e);
					
				}
			}
			return salePriceList;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

/*	MK:REDESIGN commented out for multi-site to use OOTB properties
 * 
 * // the profile property name of the regular price list
	private String	mPriceListPropertyName;

	public void setPriceListPropertyName( String pPriceListPropertyName ) {
		mPriceListPropertyName = pPriceListPropertyName;
	}

	public String getPriceListPropertyName() {
		return mPriceListPropertyName;
	}

	// the profile property name of the sale price list
	private String	mSalePriceListPropertyName;

	public void setSalePriceListPropertyName( String pSalePriceListPropertyName ) {
		mSalePriceListPropertyName = pSalePriceListPropertyName;
	}

	public String getSalePriceListPropertyName() {
		return mSalePriceListPropertyName;
	}*/

	/** <p>
	 * Convenience method to get the sale price list. Gets the sale price list from the <code>salePriceListPropertyName</code> off of the
	 * profile. If this is null, then return the sale price list defined by the PriceListManager.
	 * 
	 * @return RepositoryItem */
	protected RepositoryItem getSalePriceList( Profile pProfile ) {
		final String METHOD_NAME="getSalePriceList";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
					
			RepositoryItem salePriceList = null;
			try{
				
				// MK:RESDEIGN Multi site changes 
				// Get current site
			    Site site = MultiSiteUtil.getSite();
				
			    if( pProfile != null ) {
					//salePriceList = (RepositoryItem)pProfile.getPropertyValue( getSalePriceListPropertyName() );
			    	salePriceList = determinePriceList(pProfile, site, getSalePriceListPropertyName());
				}
				
			}catch (Exception ex){
				
			}
	
			if( salePriceList == null ) {
				salePriceList = getSalePriceList();
			}
			return salePriceList;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/** <p>
	 * Convenience method to get the price list. Gets the price list from the <code>priceListPropertyName</code> off of the profile. If this
	 * is null, then return the default price list defined by the PriceListManager.
	 * 
	 * @return RepositoryItem */
	protected RepositoryItem getListPriceList( Profile pProfile ) {
		final String METHOD_NAME="getListPriceList";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
					
			RepositoryItem priceList = null;
	
			// MK:RESDEIGN Multi site changes 
			// Get current site
		    Site site = MultiSiteUtil.getSite();
		    
			try {
				// MK:RESDEIGN Multi site changes commenting below line and calling multi-site specific method
				//priceList = getPriceList( pProfile, getPriceListPropertyName() );
				priceList = determinePriceList(pProfile, site, getPriceListPropertyName());
			} catch( RepositoryException pe ) {
				if( isLoggingError() ) {
					logError( "Error getting List Price List", pe );
				}
			}
			return priceList;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/** <p>
	 * See getListPrice(String pProductId, String pSkuId, Profile pProfile)
	 * 
	 * @see #getListPrice(String pProductId, String pSkuId, Profile pProfile)
	 * @param pProduct - the id of the product
	 * @param pSkuId
	 * @param pProfile
	 * @return
	 * @throws PriceListException */
	public Double getListPrice( RepositoryItem pProduct, RepositoryItem pSku, Profile pProfile ) throws PriceListException {
		final String METHOD_NAME="getListPrice";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		
			String productId = null;
			String skuId = null;
	
			if( pProduct != null ) {
				productId = pProduct.getRepositoryId();
			}
	
			if( pSku != null ) {
				skuId = pSku.getRepositoryId();
			}
	
			return getListPrice( productId, skuId, pProfile );
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/** <p>
	 * See getSalePrice(String pProductId, String pSkuId, Profile pProfile)
	 * 
	 * @see #getSalePrice(String pProductId, String pSkuId, Profile pProfile)
	 * @param pProduct - the id of the product
	 * @param pSkuId
	 * @param pProfile
	 * @return
	 * @throws PriceListException */
	public Double getSalePrice( RepositoryItem pProduct, RepositoryItem pSku, Profile pProfile ) throws PriceListException {
		final String METHOD_NAME="getSalePrice";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		
			String productId = null;
			String skuId = null;
	
			if( pProduct != null ) {
				productId = pProduct.getRepositoryId();
			}
	
			if( pSku != null ) {
				skuId = pSku.getRepositoryId();
			}
			return getSalePrice( productId, skuId, pProfile );
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/** <p>
	 * Nice convenience method to cut through all the complexity and just give us the Double price for a product and its sku. Which is all
	 * we ever want and seems to be something we do repeatedly.
	 * <p>
	 * pProduct or pSku may be null. One must be passed as a non-null value. If both are passed, pProduct must be a parent of pSku.
	 * <p>
	 * This method does not perform any dynamic pricing of the items using discount rules or user promotions. It only looks up the prices in
	 * the user's price list. If the user does not have a price list or the profile is null then this method will utilize
	 * <code>defaultPriceList</code> instead.
	 * 
	 * @param pProductId - The product id
	 * @param pSkuId - The id of the sku to price, this must be a child of productId.
	 * @param pProfile - The profile involved in the price lookup
	 * @return Double representing the sale price.
	 * 
	 * @throws PriceListException if no list price list is found or if no
	 *             price repository item with a valid price is found. */
	public Double getListPrice( String pProductId, String pSkuId, Profile pProfile ) throws PriceListException {
		final String METHOD_NAME="getListPrice";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			// get the list price list for the profile
			RepositoryItem priceList = getListPriceList( pProfile );
			if( priceList == null ) {
				priceList = getDefaultPriceList();
				pProfile.setPropertyValue( getPriceListPropertyName(), priceList );
				logDebug( "No pricelist returned setting to default." );
			}
			if( priceList == null ) { 
				throw new PriceListException( "No list price list found" ); 
			}
			
			final String key = getKey(
					priceList == null ? null : priceList.getRepositoryId(), 
					pProductId, pSkuId);
			
			Double cachedPrice = null;
			final PriceCache requestPriceCache = ComponentLookupUtil.lookupComponent(PRICE_CACHE_NAME, PriceCache.class);
			if (requestPriceCache != null) {
				cachedPrice = requestPriceCache.getPrice(key);
				if (cachedPrice != null) {
					// We also need to cache null returns from the price list
					// Check if stored value is similar to NEGATIVE_PRICE_CACHE_HIT
					// and return null in that case
					if (Double.compare(cachedPrice.doubleValue(), NEGATIVE_PRICE_CACHE_HIT) == 0) {
						throw new PriceListException("");
					} else {
						return cachedPrice;
					}
				}
			}
	
			// find the price repository item in the price list
			RepositoryItem priceItem = getPrice( priceList, pProductId, pSkuId );
			if( priceItem == null ) {
				// If cache exists, cache this as a NEGATIVE_PRICE_CACHE_HIT
				// to save further DB lookups
				if (requestPriceCache != null) {
					requestPriceCache.putPrice(key, NEGATIVE_PRICE_CACHE_HIT);
				}
				String msg = "No price item found for product=" + pProductId + " sku=" + pSkuId + " in price list " + priceList;
				throw new PriceListException( msg );
			}
	
			// get its list price value
			Double price = (Double)priceItem.getPropertyValue( getListPricePropertyName() );
			if( price == null ) {
				// If cache exists, cache this as a NEGATIVE_PRICE_CACHE_HIT
				// to save further DB lookups
				if (requestPriceCache != null) {
					requestPriceCache.putPrice(key, NEGATIVE_PRICE_CACHE_HIT);
				}
				String msg = "Price repository item " + priceItem + " has an invalid value for property" + getListPricePropertyName();
				throw new PriceListException( msg );
			}
			if (requestPriceCache != null) {
				requestPriceCache.putPrice(key, price);
			}
			return price;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/** <p>
	 * Nice convenience method to cut through all the complexity and just give us the Double price for a product and its sku. Which is all
	 * we ever want and seems to be something we do repeatedly.
	 * <p>
	 * pProduct or pSku may be null. One must be passed as a non-null value. If both are passed, pProduct must be a parent of pSku.
	 * <p>
	 * This method is very similar to #getListPrice but lacks the error checking as certain situations may arise where an item has no sale
	 * price or the system has no sale price list.
	 * <p>
	 * This method does not perform any dynamic pricing of the items using discount rules or user promotions. It only looks up the prices in
	 * the user's sale price list. If the user does not have a sale price list or the profile is null then this method will utilize
	 * <code>salePriceList</code> instead.
	 * 
	 * @param pProductId - The product id
	 * @param pSkuId - The id of the sku to price, this must be a child of productId.
	 * @param pProfile - The profile involved in the price lookup
	 * @return Double representing the sale price or null if not found.
	 * 
	 * @throws PriceListException if a price repository item is found with
	 *             a null price value. */
	public Double getSalePrice( String pProductId, String pSkuId, Profile pProfile ) throws PriceListException {
		final String METHOD_NAME="getSalePrice";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			Double price = null;
	
			// get the sale price list for the profile
			RepositoryItem priceList = getSalePriceList( pProfile );
			price = getPriceFromPriceList( priceList, pProductId, pSkuId );
	
			return price;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/** <p>
	 * Nice method to give us what we really want from most price lists. This does not take into account any pricing scheme beyond the
	 * simple list price scheme.
	 * 
	 * @param pPriceList
	 * @param pProductId
	 * @param pSkuId
	 * @return
	 * @throws PriceListException */
	public Double getPriceFromPriceList( RepositoryItem pPriceList, String pProductId, String pSkuId ) throws PriceListException {
		final String METHOD_NAME="getPriceFromPriceList";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);

			if( isLoggingDebug() ) {
				logDebug( "getPriceFromPriceList(): price list " + pPriceList + " productId " + pProductId + " skuId " + pSkuId );
			}
			
			final String key = getKey(
					pPriceList == null ? null : pPriceList.getRepositoryId(), 
					pProductId, pSkuId);
			
			Double cachedPrice = null;
			final PriceCache requestPriceCache = ComponentLookupUtil.lookupComponent(PRICE_CACHE_NAME, PriceCache.class);
			if (requestPriceCache != null) {
				cachedPrice = requestPriceCache.getPrice(key);
				// We also need to cache null returns from the price list
				// Check if stored value is similar to NEGATIVE_PRICE_CACHE_HIT
				// and return null in that case
				if (cachedPrice != null) {
					if (Double.compare(cachedPrice, NEGATIVE_PRICE_CACHE_HIT) == 0) {
						return null;
					}
					return cachedPrice;
				}
			}
	
			Double price = null;
	
			if( pPriceList != null ) {
	
				// find the price repository item in the price list
				RepositoryItem priceItem = getPrice( pPriceList, pProductId, pSkuId );
	
				if( priceItem != null ) {
					// get its price value
					price = (Double)priceItem.getPropertyValue( getListPricePropertyName() );
				}
	
				if( priceItem == null || price == null ) {
					if( isLoggingDebug() ) {
						logDebug( "Price not found" );
					}
					// If cache exists, cache this as a NEGATIVE_PRICE_CACHE_HIT
					// to save further DB lookups
					if (requestPriceCache != null) {
						requestPriceCache.putPrice(key, NEGATIVE_PRICE_CACHE_HIT);
					}
				} else {
					if (requestPriceCache != null) {
						requestPriceCache.putPrice(key, price);
					}
				}
			} // if a sale price list was found.
	
			return price;
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}

	/** <p>
	 * Upon startup fetch the price list ids and set the corresponding price list accessors. This sets the sale price list, default price
	 * list (list prices), and the additional shipping price list. It will cough up error messages if any of the ids do not resolve.
	 * 
	 * @throws ServiceException */
	public void doStartService() throws ServiceException {
		final String METHOD_NAME="doStartService";
		try{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
					
			super.doStartService();
	
			Repository repository = getPriceListRepository();
	
			try {
	
				if( null == getDefaultPriceListId() || null == getDefaultPriceList() ) {
	
					if( isLoggingError() ) {
						logError( "Starting the system without a default price list because" + " the id " + getDefaultPriceListId() + " is invalid" );
					}
				}
	
				if( null == getSalePriceListId() || null == repository.getItem( getSalePriceListId(), getPriceListItemType() ) ) {
	
					if( isLoggingError() ) {
						logError( "Starting the system without a sale price list because" + " the id " + getSalePriceListId() + " is invalid" );
					}
				} else {
					setSalePriceList( repository.getItem( getSalePriceListId(), getPriceListItemType() ) );
				}
	
			} catch( RepositoryException e ) {
				throw new ServiceException( e );
			}
		}finally{
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
		}
	}
	
	
	/**
	 * Build and return a key that will be used to store the object in a request
	 * based cache
	 * 
	 * @param priceListId
	 *            The price list identifier
	 * @param productId
	 *            The unique identifier for the product
	 * @param skuId
	 *            The unique identifier of the sku
	 * @return A composite key that identifies the unique price object
	 */
	private final String getKey(final String priceListId, final String productId, final String skuId) {
		if (isLoggingDebug()) {
			logDebug("getKey ENTRY [" + priceListId + ", " + productId + ", " + skuId + "]");
		}
		final StringBuilder keyBuffer = new StringBuilder();
		if (priceListId != null) {
			keyBuffer.append(priceListId).append(":");
		}
		if (productId != null) {
			keyBuffer.append(productId).append(":");
		}
		keyBuffer.append(skuId);
		if (isLoggingDebug()) {
			logDebug("getKey RETURN [" + keyBuffer.toString() + "]");
		}
		return keyBuffer.toString();
	}

	/**
	 * 
	 * @param priceListId
	 * @param productId
	 * @param skuId
	 * @param parentSkuId
	 * @param orderDate
	 * @return
	 */
	private final String getKey(final String priceListId, final String productId, final String skuId,
			final String parentSkuId, final String orderDate) {
		if (isLoggingDebug()) {
			logDebug("getKey ENTRY [" + priceListId + ", " + productId + ", " + skuId + ", " + parentSkuId + ", "
					+ orderDate + "]");
		}
		final StringBuilder keyBuffer = new StringBuilder();
		keyBuffer.append(getKey(priceListId, productId, skuId));
		if (parentSkuId != null) {
			keyBuffer.append(":").append(parentSkuId);
		}
		keyBuffer.append(":").append(orderDate);
		if (isLoggingDebug()) {
			logDebug("getKey RETURN [" + keyBuffer.toString() + "]");
		}
		return keyBuffer.toString();
	}

}// PriceListManager
