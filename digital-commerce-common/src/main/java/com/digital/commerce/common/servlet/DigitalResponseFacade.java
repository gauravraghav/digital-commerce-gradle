/** This class is meant to override the core Catalina class implementing HttpServletResponse and wrapping the response object itself.
 * 
 * This class in turn wraps the Catalina wrapper in order to ensure that JSESSIONID is not appended to any encoded URL's.
 * If this is found in the path info for the string to be encoded, the session ID is stripped out.
 * This should happen throughout any response in which cookies are not yet set.
 * In practice there will be exactly one such response. */

package com.digital.commerce.common.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;

import org.apache.catalina.connector.RequestFacade;
import org.apache.catalina.connector.Response;
import org.apache.catalina.connector.ResponseFacade;

public class DigitalResponseFacade extends ResponseFacade {

	private static final String	JSESSIONID_PATH	= ";jsessionid=";

	// wrap the previous wrapper
	private ResponseFacade		responseFacade;
	private RequestFacade		requestFacade;

	// our preferred constructor for this wrapper
	public DigitalResponseFacade( ResponseFacade responseFacade, RequestFacade requestFacade ) {
		super( null );
		this.responseFacade = responseFacade;
		this.requestFacade = requestFacade;
	}

	// the "old" constructor for backwards compatibility
	public DigitalResponseFacade( Response response ) {
		super( null );
		this.responseFacade = new ResponseFacade( response );
		this.requestFacade = new RequestFacade( response.getRequest() );
	}

	public void addCookie( Cookie cookie ) {
		responseFacade.addCookie( cookie );
	}

	public void addDateHeader( String name, long date ) {
		responseFacade.addDateHeader( name, date );
	}

	public void addHeader( String name, String value ) {
		responseFacade.addHeader( name, value );
	}

	public void addIntHeader( String name, int value ) {
		responseFacade.addIntHeader( name, value );
	}

	public void clear() {
		responseFacade.clear();
	}

	public boolean containsHeader( String name ) {
		return responseFacade.containsHeader( name );
	}

	public String encodeRedirectUrl( String url ) {
		if( url.contains( "_requestid" ) && !url.contains( "uniqueid" ) ) {
			java.util.Calendar now = java.util.Calendar.getInstance();
			url = url + "&uniqueid=" + now.getTimeInMillis();
		}
		return stripJSessionID( responseFacade.encodeRedirectUrl( url ) );
	}

	public String encodeRedirectURL( String url ) {
		if( url.contains( "_requestid" ) && !url.contains( "uniqueid" ) ) {
			java.util.Calendar now = java.util.Calendar.getInstance();
			url = url + "&uniqueid=" + now.getTimeInMillis();
		}
		return stripJSessionID( responseFacade.encodeRedirectURL( url ) );
	}

	public String encodeUrl( String url ) {
		if( url.contains( "_requestid" ) && !url.contains( "uniqueid" ) ) {
			java.util.Calendar now = java.util.Calendar.getInstance();
			url = url + "&uniqueid=" + now.getTimeInMillis();
		}
		return stripJSessionID( responseFacade.encodeUrl( url ) );
	}

	public String encodeURL( String url ) {
		if( url.contains( "_requestid" ) && !url.contains( "uniqueid" ) ) {
			java.util.Calendar now = java.util.Calendar.getInstance();
			url = url + "&uniqueid=" + now.getTimeInMillis();
		}
		return stripJSessionID( responseFacade.encodeURL( url ) );
	}

	public boolean equals( Object obj ) {
		return responseFacade.equals( obj );
	}

	public void finish() {
		responseFacade.finish();
	}

	public void flushBuffer() throws IOException {
		responseFacade.flushBuffer();
	}

	public int getBufferSize() {
		return responseFacade.getBufferSize();
	}

	public String getCharacterEncoding() {
		return responseFacade.getCharacterEncoding();
	}

	public String getContentType() {
		return responseFacade.getContentType();
	}

	public Locale getLocale() {
		return responseFacade.getLocale();
	}

	public ServletOutputStream getOutputStream() throws IOException {
		return responseFacade.getOutputStream();
	}

	public PrintWriter getWriter() throws IOException {
		return responseFacade.getWriter();
	}

	public int hashCode() {
		return responseFacade.hashCode();
	}

	public boolean isCommitted() {
		return responseFacade.isCommitted();
	}

	public boolean isFinished() {
		return responseFacade.isFinished();
	}

	public void reset() {
		responseFacade.reset();
	}

	public void resetBuffer() {
		responseFacade.resetBuffer();
	}

	public void sendError( int sc, String msg ) throws IOException {
		responseFacade.sendError( sc, msg );
	}

	public void sendError( int sc ) throws IOException {
		responseFacade.sendError( sc );
	}

	public void sendRedirect( String location ) throws IOException {
		responseFacade.sendRedirect( location );
	}

	public void setBufferSize( int size ) {
		responseFacade.setBufferSize( size );
	}

	public void setCharacterEncoding( String arg0 ) {
		responseFacade.setCharacterEncoding( arg0 );
	}

	public void setContentLength( int len ) {
		responseFacade.setContentLength( len );
	}

	public void setContentType( String type ) {
		responseFacade.setContentType( type );
	}

	public void setDateHeader( String name, long date ) {
		responseFacade.setDateHeader( name, date );
	}

	public void setHeader( String name, String value ) {
		responseFacade.setHeader( name, value );
	}

	public void setIntHeader( String name, int value ) {
		responseFacade.setIntHeader( name, value );
	}

	public void setLocale( Locale loc ) {
		responseFacade.setLocale( loc );
	}

	public void setStatus( int sc, String sm ) {
		responseFacade.setStatus( sc, sm );
	}

	public void setStatus( int sc ) {
		responseFacade.setStatus( sc );
	}

	/** Strip the JSESSIONID from the URL
	 * 
	 * @author pknoll */
	private String stripJSessionID( String encodedURL ) {
		int jSessionIDBegin = encodedURL.indexOf( JSESSIONID_PATH );
		if( jSessionIDBegin < 0 ) { return encodedURL; }

		int jSessionIdEnd = jSessionIDBegin + JSESSIONID_PATH.length() + requestFacade.getSession().getId().length();

		StringBuilder strippedURL = new StringBuilder();
		strippedURL.append( encodedURL.substring( 0, jSessionIDBegin ) );
		strippedURL.append( encodedURL.substring( jSessionIdEnd, encodedURL.length() ) );
		return strippedURL.toString();
	}

}
