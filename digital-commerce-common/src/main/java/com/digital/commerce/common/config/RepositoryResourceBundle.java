package com.digital.commerce.common.config;

import java.util.Enumeration;
import java.util.ResourceBundle;

/** @author wibrahim */
public class RepositoryResourceBundle extends ResourceBundle {
	private static MessageLocator	messageLocator;

	public MessageLocator getMessageLocator() {
		return messageLocator;
	}

	public void setMessageLocator( MessageLocator locator ) {
		messageLocator = locator;
	}

	public Enumeration<String> getKeys() {
		return getMessageLocator().getMessageKeyToStringMap();
	}

	/* (non-Javadoc)
	 * @see java.util.ResourceBundle#handleGetObject(java.lang.String) */
	protected Object handleGetObject( String key ) {
		return getMessageLocator().getMessageString( key );
	}

}
