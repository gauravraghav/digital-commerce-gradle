package com.digital.commerce.common.config;

import atg.commerce.pricing.priceLists.PriceListException;
import atg.repository.RepositoryItem;
import lombok.Getter;
import lombok.Setter;

/** @author wibrahim */
@Getter
@Setter
public class DigitalSKUPriceListComparator extends DigitalComparator {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object) */
	private static String	LIST_PRICE	= "listPrice";

	public int compare( Object sku1, Object sku2 ) {
		try {
			RepositoryItem price1 = getPriceListManager().getPrice( getPriceList(), null, (RepositoryItem)sku1 );
			RepositoryItem price2 = getPriceListManager().getPrice( getPriceList(), null, (RepositoryItem)sku2 );
			Double listPrice1 = (Double)price1.getPropertyValue( LIST_PRICE );
			Double listPrice2 = (Double)price2.getPropertyValue( LIST_PRICE );
			if( isAscending() ) {

				if( listPrice1 != null ) {
					return listPrice1.compareTo( listPrice2 );
				} else {
					return -1;
				}
			} else {
				if( listPrice2 != null ) {
					return listPrice2.compareTo( listPrice1 );
				} else {
					return 1;
				}
			}

		} catch( PriceListException e ) {
			return 0;
		}
	}
}
