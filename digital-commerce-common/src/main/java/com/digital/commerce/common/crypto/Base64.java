package com.digital.commerce.common.crypto;

/** This helper class contains the MIME/64 encoding and decoding
 * methods when dealing with password authentication. These methods
 * can be used by any agent to encode or decode whatever they wish. */
public class Base64 {

	// characters for values 0..63
	private static char[]	alphabet	= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".toCharArray();

	// lookup table for converting base64 characters to value in range 0..63
	private static byte[]	codes		= new byte[256];

	static {
		for( int i = 0; i < 256; i++ ) {
			codes[i] = -1;
		}
		for( int i = 'A'; i <= 'Z'; i++ ) {
			codes[i] = (byte)( i - 'A' );
		}
		for( int i = 'a'; i <= 'z'; i++ ) {
			codes[i] = (byte)( 26 + i - 'a' );
		}
		for( int i = '0'; i <= '9'; i++ ) {
			codes[i] = (byte)( 52 + i - '0' );
		}
		codes['+'] = 62;
		codes['/'] = 63;
	}

	/** This method encodes the given string
	 * according to the base64 specification.
	 * 
	 * @param pTarget the string to encode
	 * @return the base64 encoded value */
	public static String encode( String pTarget ) {
		return encode( pTarget.getBytes() );
	}

	/** encode the given byte array
	 * 
	 * @param pData a value of type 'byte[]'
	 * @return a value of type 'String' */
	public static String encode( byte[] pData ) {
		char[] out = new char[( ( pData.length + 2 ) / 3 ) * 4];

		//
		// 3 bytes encode to 4 chars. Output is always an even
		// multiple of 4 characters.
		//
		for( int i = 0, index = 0; i < pData.length; i += 3, index += 4 ) {
			boolean quad = false;
			boolean trip = false;

			int val = ( 0xFF & (int)pData[i] );
			val <<= 8;
			if( ( i + 1 ) < pData.length ) {
				val |= ( 0xFF & (int)pData[i + 1] );
				trip = true;
			}
			val <<= 8;
			if( ( i + 2 ) < pData.length ) {
				val |= ( 0xFF & (int)pData[i + 2] );
				quad = true;
			}
			out[index + 3] = alphabet[( quad ? ( val & 0x3F ) : 64 )];
			val >>= 6;
			out[index + 2] = alphabet[( trip ? ( val & 0x3F ) : 64 )];
			val >>= 6;
			out[index + 1] = alphabet[val & 0x3F];
			val >>= 6;
			out[index + 0] = alphabet[val & 0x3F];
		}
		return String.valueOf( out );
	}

	/** This method decodes the given string
	 * according to the base64 specification.
	 * 
	 * @param pTarget the string to decode
	 * @return the clear text decoded string */
	public static byte[] decode( String pTarget ) {
		// String out = new String(decode(pTarget.getBytes()));
		// return out;
		return decode( pTarget.getBytes() );
	}

	/** This method decodes the given string
	 * according to the base64 specification.
	 * 
	 * @param pData a value of type 'byte[]'
	 * @return a value of type 'byte[]' */
	public static byte[] decode( byte[] pData ) {
		// char[] data = new char[pTarget.length()];
		// pTarget.getChars(0, pTarget.length(), data, 0);
		int tempLen = pData.length;
		for( int ix = 0; ix < pData.length; ix++ ) {
			if( ( pData[ix] > 255 ) || codes[pData[ix]] < 0 ) {
				--tempLen;  // ignore non-valid chars and padding
			}
		}
		// calculate required length:
		// -- 3 bytes for every 4 valid base64 chars
		// -- plus 2 bytes if there are 3 extra base64 chars,
		// or plus 1 byte if there are 2 extra.

		int len = ( tempLen / 4 ) * 3;
		if( ( tempLen % 4 ) == 3 ) {
			len += 2;
		}
		if( ( tempLen % 4 ) == 2 ) {
			len += 1;
		}

		byte[] out = new byte[len];

		int shift = 0;   // # of excess bits stored in accum
		int accum = 0;   // excess bits
		int index = 0;

		// we now go through the entire array (NOT using the 'tempLen' value)
		for( int ix = 0; ix < pData.length; ix++ ) {
			int value = ( pData[ix] > 255 ) ? -1 : codes[pData[ix]];

			if( value >= 0 ) {         // skip over non-code
				accum <<= 6;            // bits shift up by 6 each time thru
				shift += 6;             // loop, with new bits being put in
				accum |= value;         // at the bottom.
				if( shift >= 8 ) {       // whenever there are 8 or more shifted in,
					shift -= 8;           // write them out (from the top, leaving any
					out[index++] =        // excess at the bottom for next iteration.
					(byte)( ( accum >> shift ) & 0xff );
				}
			}
		}
		return out;
	}

}
