package com.digital.commerce.common.services;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.RequestHeaderAttributesConstant;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@Getter
@Setter
public class DigitalBaseConstants {

	private static final int	DEFAULT_MIN_RECOMMENDED_PRODUCTS			= 6;
	private static final int	DEFAULT_MAX_RECOMMENDED_PRODUCTS			= 15;
	private static final int	DEFAULT_BASIC_REWARDS_POINTS_PER_DOLLAR		= 10;
	private static final int	DEFAULT_PREMIER_REWARDS_POINTS_PER_DOLLAR	= 15;
	private static final int	DEFAULT_ORDER_HISTORY_NUMBER_OF_ORDERS		= 100;
	private static final int	DEFAULT_REWARD_CERT_EXPIRING_SOON_DAYS 		= 100;

	public static final String	USER_TIER_DEFAULT = "CLUB";
	public static final String	USER_TIER_CLUB = "CLUB";
	public static final String	USER_TIER_GOLD= "GOLD";
	public static final String	USER_TIER_ELITE = "ELITE";
	public static final String  REWARD_CERT_TYPE="CERT";
	public static final String  REWARD_REWARD_TYPE="REW";


	public static final String	ANDROID_APP_TYPE						= "ANDROID_APP_TYPE";
	public static final String	IOS_APP_TYPE							= "IOS_APP_TYPE";
	public static final String	FULLSITE_APP_TYPE						= "FULLSITE_APP_TYPE";

	public static final String	ENCODING									= "UTF-8";

	// START : REDESIGN CONSTANTS

	public static final String	UNSUPPORTED_FEATURE							= "*********** DIGITAL_UNSUPPORTED : Currently Unsupported feature ************* ";

	// END : REDESIGN CONSTANTS
	private List<String>		xssCheckParams								= new ArrayList<>();

	private boolean				previewEnabled								= false;
	private boolean				lightningBoltEnabled						= true;
	private String				numSearchResultsPerPage;
	private boolean				useNaturalSequencingForCollection;
	private int					numProductUsesNaturalSequencing;
	private int					numCouponAllowed;
	private int					numRewardCertificatesAllowed;
	private boolean				promoPageABTest;
	private boolean				promoPageShoppingBag;
	private boolean				paTool;
	private long				stockLevelThreshold;
	private int					rewardCertificateLength;
	private int					rewardCertificateMarkdownLength;
	private boolean				stockLocatorEnabled;
	private String				gwpDepId;
	private String				gwpClassId;
	private String				gwpSubClassId;
	private String				searchAutoCompleteEnvironment;
	private int					maxRecommendedProducts						= DEFAULT_MIN_RECOMMENDED_PRODUCTS;
	private int					minRecommendedProducts						= DEFAULT_MAX_RECOMMENDED_PRODUCTS;
	private int					basicRewardsPointsPerDollar					= DEFAULT_BASIC_REWARDS_POINTS_PER_DOLLAR;
	private int					premierRewardsPointsPerDollar				= DEFAULT_PREMIER_REWARDS_POINTS_PER_DOLLAR;
	private boolean				enforceCVVOptional;

	private boolean				mobileCachingInventoryAware;
	private boolean				desktopCachingInventoryAware;
	private int					shortCacheSeconds;
	private int					mediumCacheSeconds;
	private int					longCacheSeconds;
	private int					shortCacheInventoryThreshold;
	private int					mediumCacheInventoryThreshold;

	private String				mobileGoogleAnalyticsAccount;

	private boolean				displayHeaderPhoneNumber;

	private String[]			splitSizeSeparators;

	private Properties			addAdditionalSizestoTrail;

	private String				kidsShoesRootCategory;
	private String				sandalsRootCategory;
	private String				bootsRootCategory;
	private String				seasonalRootCategory;
	private String				athleticRootCategory;

	private String				siteCatalystReportSuite;
	private String				siteCatalystMobileReportSuite;
	private String				siteCatalystDomain;
	private String				siteCatalystDomainSecure;
	private boolean				siteCatalystCallFromDataLayer;

	private boolean				useATGRecommendations;

	private Properties			mobilePageSizes;

	private boolean				internationalOverlayEnabled;

	private int					orderHistoryNumberOfOrders					= DEFAULT_ORDER_HISTORY_NUMBER_OF_ORDERS;

	private boolean				showInvisibleItems;

	private boolean				sellInvisibleItems;

	private boolean				googleTagsGoogplaCookieEnabled;

	private boolean				invalidateOrderBeforeReprice				= false;

	private double				purchaseLimit;

	private int					cartLineLimit;

	private int					skuQtyLimit;

	private boolean				chargeSendEnable;

	private String				seasonalShopName;

	private String				pageTitleSuffix;

	private String				categoryMetaDescriptionPrefix;

	private String				categoryPageTitleAdditionalText;

	private String				adsSecretKey;
	private String				adsApplyUrl;
	private String				adsManageUrl;

	private boolean				useNewCustomerReviews;
	private boolean				useNewCustomerReviewVideos;

	public static final String	SHIP_TYPE_BOPIS								= "BOPIS";
	public static final String	SHIP_TYPE_BOSTS								= "BOSTS";
	public static final String	SHIP_TYPE_SHIP								= "HOME";
	public static final String	CHANNEL_FLOW								= "channelFlow";
	public static final int		BOPIS_QUANTITY_LIMIT						= 1;

	public static final String	CONTACT_CENTER_ROLE							= "Contact-Center";

	private List<String>		productsWithSizeOrdered;

	private String				vantivPayPageApi;
	private String				vantivPayPageUrl;
	private String				vantivPayPageId;
	private String				vantivPayPageMerchantTxnId;
	private String				vantivPayPageReportGroup;
	private int					vantivPayPageTimeoutAttempts;
	private int					vantivPayPageTimeoutMS;
	private String				replaceChars;
	private String				specialChars								= "ÈÉÊËèéêëÌÍÎÏÐìíîïÀÁÂÃÄÅÆàáâãäåæÇçÒÓÔÕÖØðòóôõöÙÚÛÜùúûüÝýÿÑñ";
	private int					fraudRuleDescMaxLength;
	private boolean 			bloomreachEnabled;
	private boolean 			bloomreachThemePageEnabled;
	private int temporaryPasswordResetExpiryHours;
	private int temporaryPasswordCharLength;
	private String temporaryPasswordNumericChars;
	private boolean hideOrders;
	private boolean desktopLazyLoad = false;
	private boolean mobileLazyLoad = false;
	private String androidTitle;
	private String androidDesc;
	private String androidIcon;
	private String androidPrice;
	private String androidPriceSuffix;
	private String androidButtonText;
	private String androidButtonUrl;
	private boolean androidEnabled;


	private String				dswInStoreDeviceUserAgentPatterns;
	private String				dswInStoreDeviceIPAddresses;

	private boolean				overrideThreatMetrixId;
	private String				threatMetrixId;
	private String				fiveDigitThreatMetrixCode;
	private String				sessionInitializationThreatMetrixScriptUrl;
	private String				threatMetrixOrganizationId;

	// Constants used to calculate CC type based on pattern matching
	private String				dswVisaFirstSix;
	private String				dinnersCardPatternOne;
	private String				discoverCardPattern;
	private String				dinnersCardPatternTwo;
	private String				jcbCardPattern;
	private String				cardTypeUndefined;
	private String				masterCardPattern;
	private String				amexCardPattern;
	private String				visaCardPattern;
	private String				chinaUnionPayCardPattern;
	private String				defaultExpirationCentury;
	private String				stateCodeLength;

	// Credit card types constants
	private String				creditCardTypeVI;
	private String				creditCardTypeAX;
	private String				creditCardTypeDSWVisa;
	private String				creditCardTypeMaster;
	private String				creditCardTypeVisa;
	private String				creditCardTypeAmex;
	private String				creditCardTypeDiscover;
	private String				creditCardTypeDiners;
	private String				creditCardTypeJcb;
	private String				creditCardTypeCup;
	private String				creditCardTypeMc;
	private String				creditCardTypeDI;

	private boolean				backOrderEnabled;
	private boolean				preOrderEnabled;
	private boolean				stockThresholdEnabled;

	private boolean				cleanUpPromotionsPostLogin;
	private boolean				cleanUpPromotionsPostLogout;
	private boolean				cleanUpPromotionsPostCheckout;
	private boolean				cleanUpCSRPromotionsPostLogin;
	// Optin/out related constants
	private String				rewardsEmailSource;
	private String				emailUpdateNo;
	private String				emailUpdateYes;
	private String				fashionFrequencyEmailFlag;
	// key for contact infor error
	private String				contactInfoNotPresent;
	private String				previewSiteUrl;

	private String				testEmailAddressPattern;
	private boolean				evaluateTestOrders;
	private boolean				byPassFraudTestOrders;

	private String				regionPrefix;
	private String				sitePrefix;
	private String				siteGroupPrefix;
	private String				prefixDelimiter;

	private String				aggregateInventoryKey;

	private Map<String, String>	cardTypesMap						= new HashMap<>();

	private Map<String, String>	originOfOrderMap						= new HashMap<>();

	private Map<String, String>	yantraNotificationStatusMap			= new HashMap<>();

	private Map<String, String>	orderLineItemStatusMap				= new HashMap<>();

	private Map<String, String>	carrierCodeMap						= new HashMap<>();

	private Map<String,String>  ribbonOrder 						= new HashMap<>();
	
	private Map<String, String>	channelCodeMap						= new HashMap<>();

	private Map<String, Boolean> liveChatLocations					= new HashMap<>();

	public static final String	MISSINGGIFTCARDNUMBER				= "missingGiftCardNumber";
	public static final String	MISSINGGIFTCARDPIN					= "missingGiftCardPin";
	public static final String	MISSINGCVVNUMBER					= "invalidCVV";
	public static final String	MISSINGNAMEONCARD					= "invalideCardName";
	public static final String	INVALID_CVV_NUMERIC_REQUIRED		= "invalidCVVNumericRequired";
	public static final String	INVALID_CVV_LENGTH					= "invalidCVVLength";
	public static final String	INVALID_CVV_LENGTH_AMEX				= "invalidCVVLengthAmex";

	public static final int		GIFT_CARDNUMBER_LENGTH				= 19;
	public static final int		GIFT_PINNUMBER_LENGTH				= 4;
	public static final int		GIFT_CARD_APPROVAL_RESPONSE_CODE	= 1;
	public static final int		GIFT_CARD_INSUFFICIENT_FUNDS_RESPONSE_CODE	= 5;
	public static final int		MAX_PHONE_US_LENGTH					= 10;
	public static final String	SUCCESS_AUTHRETURN_CODE				= "APPROVED";

	public static final String	SVS_BUSINESS_ERROR_CODE				= "SVSBE";
	public static final String	SVS_COMMERCE_ERROR_CODE				= "SVSCOME";
	public static final String	GIFTCARD_UNAVAILABLE				= "unavailable";

	public static final String	SHIPPING_METHODS					= "shippingMethods";
	public static final String	STATIC_SHIPPING_RATES				= "staticShippingRates";
	public static final String	DEFAULT_SHIPPING_RATES				= "GRN=7.95,2ND=14.95,NDA=24.95,SGRN=0.0,S2ND=14.95,SNDA=24.95,SFND=0.0,ISPU=0.0,STS=0.0,SF2ND=0.0";
	public static final int		MAX_CREDIT_CARDNUMBER_LENGTH		= 16;
	public static final int		MAX_CREDIT_CARDNUMBER_LENGTH_AMEX	= 15;

	// Added for Credit Card Mandate Project. - Start
	public static final int[]	MAX_CREDIT_CARDNUMBER_LENGTH_DINERS	= { 14, 16 };  //NOSONAR

	private int					vantivIFrameTimeout;
	private String				vantivPayFrameAPI;

	private String				responsysUrl;
	private String				responsysRI;
	private String				responsysEI;
	private String				entryPageURL;
	private String				entryPageURLMobile;
	private String				returnId;
	private String				returnIdMobile;
	private String				clientId;
	private String				userEmail;
	private String				cardType;
	private String				passwordAlphanumericChars;
	private String				passwordNumericChars;
	private String				passwordAlphabeticChars;
	private String				passwordSpecialAlphabeticChars;
	private String				passwordUpperAlphabeticChars;
	private String				passwordLowerAlphabeticChars;
	private int					strongPasswordScore					= 2;

	private String[]			insoleRole;
	private String[]			dashboardRole;
	private String				inContactURL;
	private String				ulStartTag;
	private String				ulEndTag;
	private String				listItemEndTag;
	private String				listItemTag;
	private boolean				inContactEnabled;
	private String				googleMapsApIURL;
	private boolean				googleMapsApIEnabled;
	private String				bvAPIURL;
	private boolean				bvAPIEnabled;
	private String				defaultCsrid;
	private String				defaultFraudScore;
	private String				defaultCVVNumber;
	private String				countryCode;
	private String				countryCodeForUS;
	private boolean				adsEnabled;
	private boolean				arrowEyeEnabled;
	private boolean				xmPreviewEnabled;
	private int					orderHistoryRangeInMonths;
	private boolean				narwarEnabled;
	private String				narwarUrl;
	private boolean				tealiumEnabled;
	private String				tealiumAccount;
	private String				tealiumProfile;
	private String				tealiumEnvironment;
	private boolean				tealiumSupressFirstView;
	private String				bvCurationsUrl;
	private double				compareAtPriceConstTwo;
	private double				compareAtPriceConstFive;
	private double				compareAtPriceConstHundred;
	private double				compareAtPriceConstTen;
	private boolean				bccPreviewEnabled;
	private String				bccPreviewHost;
	private boolean				appdEnabled;
	private String				appdUrl;
	private String				imagesUrl;
	private boolean				recommendationsEnabled;
	private String				recommendationsMboxUrl;
	private boolean				trueFitEnabled;
	private String				trueFitUrl;
	private String				trueFitEnvironment;
	private int					productImageCount;
	private int					maxLinesInStackTrace;
	private List<String>		allowedLoggingMethods;
	private String				exclusionWords;
	private int					maxSearchResults;
	private boolean				showLocationPopup;
	private boolean				previewEnv;
	private String				customerServicePhoneNumber;
	private String				customerServiceEmail;
	private List<String>	customerServiceContactHours;
	private String 				inContactMobileURL;
	private String 				clearanceNvalue;
	private String 				storePagesUrl;
	private boolean 			avsCodeFraudCheckEnabled;
	private Integer				wishlistRecordsPerPage;
	private boolean				wishlistIsEnabled;
	private  boolean rewardsCacheEnabled;
	private  boolean productSkuStockCacheEnabled;
	private String[] productSkuGiftCardIdList;
	private Integer 	rewardCertExpiringSoonDays = DEFAULT_REWARD_CERT_EXPIRING_SOON_DAYS;

	private String				afterPayPortalUrl;

	private String 				andriodUserAgentValue = null;
	private String 				iosUserAgentValue = null;
	private int sessionTimeOutInSeconds = 900;
	private Map<String,String>  rewardDenominations;
	private boolean				placeOrderBeanFilterEnabled;
	private boolean				orderLookupBeanFilterEnabled;

	private List<String> 		rewardsBirthdayClubDiscountCode;
	private List<String> 		rewardsBirthdayGoldDiscountCode;
	private List<String>		rewardsBirthdayEliteDiscountCode;
	private Long 				defaultCertDenomination;


	private List<String>		rewards2XPointsOneDiscountCode;
	private List<String>		rewards2XPointsTwoDiscountCode;
	private List<String>		rewards3XPointsDiscountCode;
	private String doublePointsOfferDisplayName;
	private String triplePointsOfferDisplayName;
	private boolean expediatedShippingEnabled;
	private long accountLockTime;



	private long 				rewardsGoldTierQualifierDollar;
	private long 				rewardsEliteTierQualifierDollar;
	private int 				birthDayGiftBlackOutDays;
	private int geoLocationTimeout;
	private int geoLocationMaximumAge;
	private String iosVersion;
	private String androidVersion;
	private String braintreeMerchantId;
	private String braintreeEnvironment;
	private String braintreeTokenizationKey;
	private String braintreeJSUrl;
	private boolean braintreeApplePayEnabled;
	private String braintreeApplePayJSUrl;
	private String braintreeGooglepayMerchantId;
	private boolean braintreeGooglePayEnabled;
	private String braintreeGooglPayJSUrl;
	private String braintreeGooglePayAPIUrl;
	private boolean mPulseEnabeld;
	private String mPulseId;
	private String mPulseAPIKey;
	private String mPulseJSBaseURL;
	private boolean shoevatorEnabled;
	private Map<String, String>  shoevatorStores;
	private boolean votingEnabled;
	private Map<String, String> votingCategories;
	private String passwordAlphaNumaricSpecialChars;
	private String	passwordSpecialCharacters;
	private String	siteURL;
	private ServiceAvailabilityAggregator	serviceAvailabilityAggregator;
	private List<String> orderStatusAutomaticEventTypes;


	public static int getDefaultMinRecommendedProducts() {
		return DEFAULT_MIN_RECOMMENDED_PRODUCTS;
	}

	public Map<String, Object> getAllConstants() {
		Map<String, Object> allConstants = new HashMap<>();
		Map<String, Object> constants = new HashMap<>();

		// Customer Service contact details
		Map<String, Object> custServiceSettings = new HashMap<>();
		custServiceSettings.put( "phoneNumber", this.getCustomerServicePhoneNumber() );
		custServiceSettings.put( "email", this.getCustomerServiceEmail() );
		if(null!=this.getCustomerServiceContactHours() && !this.getCustomerServiceContactHours().isEmpty()){
			custServiceSettings.put( "contactHours", this.getCustomerServiceContactHours().toArray());
		}

		constants.put( "customerServiceSettings", custServiceSettings );

		Map<String, Object> bccPreviewSettings = new HashMap<>();
		bccPreviewSettings.put( "isEnabled", this.isBccPreviewEnabled() );
		if( isBccPreviewEnabled() ) {
			bccPreviewSettings.put( "host", this.getBccPreviewHost() );
		} else {
			bccPreviewSettings.put( "host", "" );
		}
		constants.put( "BccPreviewSettings", bccPreviewSettings );

		Map<String, Object> pdpCacheSettings = new HashMap<>();
		pdpCacheSettings.put( "ShortCacheSeconds", this.getShortCacheSeconds() );
		pdpCacheSettings.put( "ShortCacheInventoryThreshold", this.getShortCacheInventoryThreshold() );
		pdpCacheSettings.put( "MediumCacheSeconds", this.getMediumCacheSeconds() );
		pdpCacheSettings.put( "MediumCacheInventoryThreshold", this.getMediumCacheInventoryThreshold() );
		pdpCacheSettings.put( "longCacheSeconds", this.getLongCacheSeconds() );
		constants.put( "PDPCacheSettings", pdpCacheSettings );

		Map<String, Object> adsSettings = new HashMap<>();
		adsSettings.put( "isEnabled", this.isAdsEnabled() );
		adsSettings.put( "applyUrl", this.getAdsApplyUrl() );
		adsSettings.put( "manageUrl", this.getAdsManageUrl() );
		constants.put( "AdsSettings", adsSettings );

		Map<String, Object> inContactSettings = new HashMap<>();
		inContactSettings.put( "isEnabled", this.isInContactEnabled() );
		inContactSettings.put( "liveChatUrl", this.getInContactURL() );
		inContactSettings.put( "liveChatMobileUrl", this.getInContactMobileURL());
		Map<String, Object> liveChatLocations = new HashMap<>();
		for (Map.Entry<String, Boolean> entry : getLiveChatLocations().entrySet()) {
			String value = String.valueOf(entry.getValue());
			liveChatLocations.put(entry.getKey(), value.equalsIgnoreCase("true"));
		}
		inContactSettings.put("liveChatLocations", liveChatLocations);
		constants.put( "InContactSettings", inContactSettings );

		Map<String, Object> nvSettings = new HashMap<>();
		nvSettings.put( "isEnabled", this.isNarwarEnabled() );
		nvSettings.put( "url", this.getNarwarUrl() );
		constants.put( "NarwarSettings", nvSettings );

		Map<String, Object> tealiumSettings = new HashMap<>();
		tealiumSettings.put( "isEnabled", this.isTealiumEnabled() );
		tealiumSettings.put( "account", this.getTealiumAccount() );
		tealiumSettings.put( "profile", this.getTealiumProfile() );
		tealiumSettings.put( "environment", this.getTealiumEnvironment() );
		tealiumSettings.put( "suppressFirstView", this.isTealiumSupressFirstView() );
		constants.put( "TealiumSettings", tealiumSettings );

		Map<String, Object> afterPaySettings = new HashMap<>();
		afterPaySettings.put("portalUrl", this.getAfterPayPortalUrl());
		constants.put("AfterPaySettings",afterPaySettings);

		Map<String, Object> vantivPPSettings = new HashMap<>();
		vantivPPSettings.put( "isEnabled", serviceAvailabilityAggregator.isAuthorizationServiceEnabled() );
		vantivPPSettings.put( "payPageApi", this.getVantivPayPageApi() );
		vantivPPSettings.put( "payPageUrl", this.getVantivPayPageUrl() );
		vantivPPSettings.put( "payPageId", this.getVantivPayPageId() );
		vantivPPSettings.put( "payPageMerchantTxnId", this.getVantivPayPageMerchantTxnId() );
		vantivPPSettings.put( "payPageReportGroup", this.getVantivPayPageReportGroup() );
		vantivPPSettings.put( "payPageTimeoutAttempts", this.getVantivPayPageTimeoutAttempts() );
		vantivPPSettings.put( "payPageTimeoutMS", this.getVantivPayPageTimeoutMS() );
		vantivPPSettings.put( "iFrameTimeout", this.getVantivIFrameTimeout() );
		vantivPPSettings.put( "payFrameAPI", this.getVantivPayFrameAPI() );
		constants.put( "VantivPPSettings", vantivPPSettings );

		Map<String, Object> tmSettings = new HashMap<>();
		tmSettings.put( "SessionInitializationThreatMetrixScriptUrl", this.getSessionInitializationThreatMetrixScriptUrl() );
		tmSettings.put( "ThreatMetrixId", this.getThreatMetrixId() );
		tmSettings.put( "ThreatMetrixOrganizationId", this.getThreatMetrixOrganizationId() );
		tmSettings.put( "FiveDigitThreatMetrixCode", this.getFiveDigitThreatMetrixCode() );
		tmSettings.put( "isOverrideThreatMetrixId", this.isOverrideThreatMetrixId() );
		constants.put( "ThreatMetrixSettings", tmSettings );

		Map<String, Object> aeSettings = new HashMap<>();
		aeSettings.put( "isEnabled", this.isArrowEyeEnabled() );
		aeSettings.put( "entryPageURL", this.getEntryPageURL() );
		aeSettings.put( "entryPageURLMobile", this.getEntryPageURLMobile() );
		aeSettings.put( "returnId", this.getReturnId() );
		aeSettings.put( "returnIdMobile", this.getReturnIdMobile() );
		aeSettings.put( "clientId", this.getClientId() );
		aeSettings.put( "userEmail", this.getUserEmail() );
		aeSettings.put( "cardType", this.getCardType() );
		constants.put( "ArrowEyeSettings", aeSettings );

		Map<String, Object> bvSettings = new HashMap<>();
		bvSettings.put( "isEnabled", this.isBvAPIEnabled() );
		bvSettings.put( "url", this.getBvAPIURL() );
		bvSettings.put( "curationsUrl", this.getBvCurationsUrl() );
		constants.put( "BVAPISettings", bvSettings );

		Map<String, Object> googleMapsSettings = new HashMap<>();
		googleMapsSettings.put( "isEnabled", this.isGoogleMapsApIEnabled() );
		googleMapsSettings.put( "url", this.getGoogleMapsApIURL() );
		constants.put( "GoogleMapsSettings", googleMapsSettings );

		Map<String, Object> appdSettings = new HashMap<>();
		appdSettings.put( "isEnabled", this.isAppdEnabled() );
		appdSettings.put( "url", this.getAppdUrl() );
		constants.put( "AppDSettings", appdSettings );

		Map<String, Object> scene7Settings = new HashMap<>();
		scene7Settings.put( "imagesUrl", this.getImagesUrl() );
		scene7Settings.put( "productImageCount", this.getProductImageCount() );
		constants.put( "Scene7Settings", scene7Settings );

		Map<String, Object> yextSettings = new HashMap<>();
		yextSettings.put( "storePagesUrl", this.getStorePagesUrl() );
		constants.put( "YextSettings", yextSettings );

		Map<String, Object> abodeRecommendationsSettings = new HashMap<>();
		abodeRecommendationsSettings.put( "isEnabled", this.recommendationsEnabled );
		abodeRecommendationsSettings.put( "mboxUrl", this.recommendationsMboxUrl );
		constants.put( "AdobeRecommendationsSettings", abodeRecommendationsSettings );

		Map<String, Object> tfSettings = new HashMap<>();
		tfSettings.put( "isEnabled", this.trueFitEnabled );
		tfSettings.put( "url", this.trueFitUrl );
		tfSettings.put( "Environment", this.trueFitEnvironment );
		constants.put( "TrueFitSettings", tfSettings );
		
		//Start : DIG-1041 - Lazy load toggles in prerender Homepage
		Map<String, Object> preRenderSettings = new HashMap<>();
		preRenderSettings.put( "desktopLazyLoad", this.isDesktopLazyLoad() );
		preRenderSettings.put( "mobileLazyLoad", this.isMobileLazyLoad() );
		constants.put( "preRenderSettings", preRenderSettings );
		//End : DIG-1041 - Lazy load toggles in prerender Homepage

		//Start : DIG-1262 - Mobile Site App banner
		Map<String, Object> smartBannerSettings = new HashMap<>();
		Map<String, Object> androidSmartBannerSettings = new HashMap<>();
		Map<String, Object> iosSmartBannerSettings = new HashMap<>();
		androidSmartBannerSettings.put("title", this.getAndroidTitle());
		androidSmartBannerSettings.put("desc", this.getAndroidDesc());
		androidSmartBannerSettings.put("price", this.getAndroidPrice());
		androidSmartBannerSettings.put("priceSuffix", this.getAndroidPriceSuffix());
		androidSmartBannerSettings.put("icon", this.getAndroidIcon());
		androidSmartBannerSettings.put("buttonText", this.getAndroidButtonText());
		androidSmartBannerSettings.put("buttonUrl", this.getAndroidButtonUrl());
		androidSmartBannerSettings.put("isEnabled", this.isAndroidEnabled());
		smartBannerSettings.put( "android", androidSmartBannerSettings);
		smartBannerSettings.put( "ios", iosSmartBannerSettings );
		constants.put( "smartBannerSettings", smartBannerSettings );
		//End : DIG-1262 - Mobile Site App banner

		Map<String, Object> serviceAvailabilityMap = new HashMap<>();

		Map<String, Object> uspsMap = new HashMap<>();
		uspsMap.put( "isEnabled", serviceAvailabilityAggregator.isAddressValidationServiceEnabled() );
		uspsMap.put( "methodsAvailability", serviceAvailabilityAggregator.getAddressValidationServiceMethodsEnabled() );
		serviceAvailabilityMap.put( "USPS", uspsMap );

		Map<String, Object> svsMap = new HashMap<>();
		svsMap.put( "isEnabled", serviceAvailabilityAggregator.isStoredValueServiceEnabled() );
		svsMap.put( "methodsAvailability", serviceAvailabilityAggregator.getStoredValueServiceMethodsEnabled() );
		serviceAvailabilityMap.put( "SVS", svsMap );

		Map<String, Object> vantivMap = new HashMap<>();
		vantivMap.put( "isEnabled", serviceAvailabilityAggregator.isAuthorizationServiceEnabled() );
		vantivMap.put( "methodsAvailability", serviceAvailabilityAggregator.getAuthorizationServiceMethodsEnabled() );
		serviceAvailabilityMap.put( "VANTIV", vantivMap );

		Map<String, Object> vertexMap = new HashMap<>();
		vertexMap.put( "isEnabled", serviceAvailabilityAggregator.isTaxServiceEnabled() );
		vertexMap.put( "methodsAvailability", serviceAvailabilityAggregator.getTaxServiceMethodsEnabled() );
		serviceAvailabilityMap.put( "VERTEX", vertexMap );

		Map<String, Object> payPalMap = new HashMap<>();
		payPalMap.put( "isEnabled", serviceAvailabilityAggregator.isPaymentServiceEnabled() );
		payPalMap.put( "methodsAvailability", serviceAvailabilityAggregator.getPaymentServiceMethodsEnabled() );
		serviceAvailabilityMap.put( "PAYPAL", payPalMap );

		Map<String, Object> afterPayMap = new HashMap<>();
		afterPayMap.put( "isEnabled", serviceAvailabilityAggregator.isAfterPayServiceEnabled() );
		afterPayMap.put( "methodsAvailability", serviceAvailabilityAggregator.getAfterPayServiceMethodsEnabled() );
		serviceAvailabilityMap.put( "AFTERPAY", afterPayMap );

		Map<String, Object> rewardsMap = new HashMap<>();
		rewardsMap.put( "isEnabled", serviceAvailabilityAggregator.isRewardServiceEnabled() );
		rewardsMap.put( "methodsAvailability", serviceAvailabilityAggregator.getRewardServiceMethodsEnabled() );
		serviceAvailabilityMap.put( "REWARDS", rewardsMap );

		Map<String, Object> snMap = new HashMap<>();
		snMap.put( "isEnabled", serviceAvailabilityAggregator.isInventoryServiceEnabled() );
		snMap.put( "methodsAvailability", serviceAvailabilityAggregator.getInventoryServiceMethodsEnabled() );
		serviceAvailabilityMap.put( "STORENET", snMap );

		Map<String, Object> omsMap = new HashMap<>();
		omsMap.put( "isEnabled", serviceAvailabilityAggregator.isYantraInventoryServiceEnabled() );
		omsMap.put( "methodsAvailability", serviceAvailabilityAggregator.getYantraInventoryServiceMethodsEnabled() );

		serviceAvailabilityMap.put( "YANTRA", omsMap );

		constants.put( "ServicesAvailability", serviceAvailabilityMap );

		Map<String, Object> rewardsSettings = new HashMap<>();
		rewardsSettings.put("certExpiringSoonDays", this.getRewardCertExpiringSoonDays() );
		if (null !=  ribbonOrder) {
			Iterator<String> keySet = ribbonOrder.keySet().iterator();
			Set<Map<String,Integer>> order = new HashSet<>();
			while (keySet.hasNext())  {
				String key = keySet.next();
				Map<String, Integer> tmpOrder = new HashMap<>();
				tmpOrder.put(key,new Integer(ribbonOrder.get(key)));
				order.add(tmpOrder);
			}
			rewardsSettings.put("ribbonOrder", order );
		}


		if(this.getRewardDenominations() != null) {
			Iterator<String> keySet = rewardDenominations.keySet().iterator();
			Set<Map<String,Integer>> denoms = new HashSet<>();
			while (keySet.hasNext())  {
				String key = keySet.next();
				Map<String, Integer> denomPoints = new HashMap<>();
				denomPoints.put("value", new Integer(key));
				denomPoints.put("points", new Integer(rewardDenominations.get(key)));
				denoms.add(denomPoints);
			}
			rewardsSettings.put("denominations", denoms);
		}
		rewardsSettings.put("birthDayGiftBlackOutDays", birthDayGiftBlackOutDays);
		constants.put("rewardsSettings", rewardsSettings);


		Map<String, Object> bloomReachSettings = new HashMap<>();
		bloomReachSettings.put( "isEnabled", this.isBloomreachEnabled() );
		bloomReachSettings.put( "isThemepagesEnabled", this.isBloomreachThemePageEnabled() );
		constants.put( "BloomReachSettings", bloomReachSettings );

		Map<String, Object> wishListSettings = new HashMap<>();
		wishListSettings.put("isEnabled", this.isWishlistIsEnabled());
		wishListSettings.put("itemsMaxLimit", MultiSiteUtil.getWishlistItemLimit());
		wishListSettings.put("shareEmailsMaxLimit", MultiSiteUtil.getEmailSharedLimits());
		wishListSettings.put("recordsPerPage", this.getWishlistRecordsPerPage());

		constants.put( "wishListSettings", wishListSettings );

		Map<String, Object> miscSettings = new HashMap<>();
		miscSettings.put("basicShippingThreshold", MultiSiteUtil.getBasicShippingThreshold());
		miscSettings.put("clearanceNvalue", this.getClearanceNvalue());
		miscSettings.put("creditCardsMaxLimit", MultiSiteUtil.getCreditCardMaxLimit());
		if(null != MultiSiteUtil.getExcludeWords()) {
			miscSettings.put("exclusionWords", MultiSiteUtil.getExcludeWords().toString().replaceAll("[\\[()\\]]",""));
		}
		miscSettings.put("isPaTool", this.isPaTool());
		miscSettings.put("orderHistoryRangeInMonths", this.getOrderHistoryRangeInMonths());
		miscSettings.put("sessionTimeOutInSeconds", this.getSessionTimeOutInSeconds());
		miscSettings.put("shippingAddressesMaxLimit", MultiSiteUtil.getShippingAddressMaxLimit());
		miscSettings.put("showLocationPopup", this.isShowLocationPopup());
		miscSettings.put("geoLocationTimeout", this.getGeoLocationTimeout());
		miscSettings.put("xmPreviewEnabled", this.isXmPreviewEnabled());

		constants.put( "miscSettings", miscSettings );


		Map<String, Object> geoLocationSettings = new HashMap<>();
		geoLocationSettings.put("showPopup", this.isShowLocationPopup());
		geoLocationSettings.put("timeout", this.getGeoLocationTimeout());
		geoLocationSettings.put("maximumAge", this.getGeoLocationMaximumAge());
		constants.put( "geoLocationSettings", geoLocationSettings );

		Map<String, Object> mobileAppSettings = new HashMap<>();
		mobileAppSettings.put("iosVersion", this.getIosVersion());
		mobileAppSettings.put("androidVersion", this.getAndroidVersion());
		constants.put( "mobileAppSettings", mobileAppSettings );

		Map<String, Object> braintreeSettings = new HashMap<>();
		braintreeSettings.put("merchantId", this.getBraintreeMerchantId());
		braintreeSettings.put("environment", this.getBraintreeEnvironment());
		braintreeSettings.put("tokenizationKey", this.getBraintreeTokenizationKey());
		braintreeSettings.put("jsUrl", this.getBraintreeJSUrl());

		Map<String, Object> googlePaySettings = new HashMap<>();
		googlePaySettings.put("isEnabled", this.isBraintreeGooglePayEnabled());
		googlePaySettings.put("merchantId", this.getBraintreeGooglepayMerchantId());
		googlePaySettings.put("jsUrl", this.getBraintreeGooglPayJSUrl());
		googlePaySettings.put("apiUrl", this.getBraintreeGooglePayAPIUrl());
		braintreeSettings.put("googlePaySettings", googlePaySettings);

		Map<String, Object> applePaySettings = new HashMap<>();
		applePaySettings.put("isEnabled", this.isBraintreeApplePayEnabled());
		applePaySettings.put("jsUrl", this.getBraintreeApplePayJSUrl());
		braintreeSettings.put("applePaySettings", applePaySettings);

		constants.put( "braintreeSettings", braintreeSettings );

		Map<String, Object> mPulseSettings = new HashMap<>();
		mPulseSettings.put("isEnabled", this.isMPulseEnabeld());
		mPulseSettings.put("id", this.getMPulseId());
		mPulseSettings.put("jsBaseUrl", this.getMPulseJSBaseURL());
		mPulseSettings.put("apiKey", this.getMPulseAPIKey());
		constants.put("mPulseSettings", mPulseSettings);
		/* This entire map handling is bad as we need to figure of handling map of maps
		* as property eventually*/
		Map<String, Object> ShoevatorSettings = new HashMap<>();
		ShoevatorSettings.put("isEnabled", this.isShoevatorEnabled());
		List<HashMap<String, Object>> storeDetailsList = new ArrayList<>();
		for (Map.Entry<String, String> storeEntry : shoevatorStores.entrySet()) {
			String[] storeDetailEntries = storeEntry.getValue().split(",");
			HashMap<String, Object> storeDetailValue = new HashMap<>();

			for (String storeDetailEntry : storeDetailEntries) {
				String[] storeMapValues = storeDetailEntry.split("=");
				String key = storeMapValues[0];
				String value = storeMapValues[1];
				switch (key) {
					case "isEnabled":
						storeDetailValue.put(key, Boolean.valueOf(value));
						break;
					case "radius":
						storeDetailValue.put(key, Long.valueOf(value));
						break;
					case "latitude":
						storeDetailValue.put(key, Double.valueOf(value));
						break;
					case "longitude":
						storeDetailValue.put(key, Double.valueOf(value));
						break;
					default:
						storeDetailValue.put(key, value);
						break;
				}
			}
			storeDetailsList.add(storeDetailValue);
		}
		if(!storeDetailsList.isEmpty()) {
			ShoevatorSettings.put("stores",storeDetailsList);
			allConstants.put("ShoevatorSettings",ShoevatorSettings);
			constants.put("ShoevatorSettings",ShoevatorSettings);
		}
		
		
		Map<String, Object> expeditedShippingSettings = new HashMap<>();
		expeditedShippingSettings.put( "isEnabled", this.isExpediatedShippingEnabled());
		constants.put( "ExpeditedShippingSettings", expeditedShippingSettings );
		
		
		Map<String, Object> votingCatMap = new HashMap<>();
		for (Map.Entry<String, String> votingCategory: votingCategories.entrySet()) {
			String key = votingCategory.getKey();

			String[] values = votingCategory.getValue().split(",");
			votingCatMap.put(key, Arrays.asList(values));
		}
		if(!votingCatMap.isEmpty()) {
			votingCatMap.put("isEnabled",this.votingEnabled);
			mobileAppSettings.put("voting",votingCatMap);
		}


		allConstants.put( "dswEnvironmentSettings", constants );

		return allConstants;
	}

	public enum TemplateConstants {
		JSP_FRAG("JSPFrag"), ADV_BANNER("AdvertisementBanner"), MARKETING_SLOTS("MarketingSlots"), FEATURED_PRODUCTS("FeaturedProducts"), RECOMMENDATIONS("Recommendations"), HTML("HTMLContent");

		private final String	value;

		private TemplateConstants( String value ) {
			this.value = value;
		}

		/** The value of the name
		 *
		 * @return */
		public String getValue() {
			return value;
		}
	}

	public boolean isAndriodAppRequest(){
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		String ugHeader = null;
		if(request != null){
			ugHeader = request.getHeader(RequestHeaderAttributesConstant.USER_AGENT.getValue());
		}

		if(DigitalStringUtil.isNotBlank(ugHeader) && ugHeader.contains(this.getAndriodUserAgentValue())){
			return true;
		}
		return false;
	}

	public boolean isIosAppRequest(){
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		String ugHeader = null;
		if(request != null){
			ugHeader = request.getHeader(RequestHeaderAttributesConstant.USER_AGENT.getValue());
		}

		if(DigitalStringUtil.isNotBlank(ugHeader) && ugHeader.contains(this.getIosUserAgentValue())){
			return true;
		}
		return false;
	}



	public Integer getRewardCertExpiringSoonDays() {
		return MultiSiteUtil.getRewardsCertExpiringSoonDays();
	}

	public Map<String, String> getRibbonOrder() {
		Map<String, String> siteRibbonOrder = MultiSiteUtil.getRewardsRibbonOrder();
		if(siteRibbonOrder != null && !siteRibbonOrder.isEmpty()){
			return siteRibbonOrder;
		}else {
			return ribbonOrder;
		}
	}

	public List<String> getRewardsBirthdayClubDiscountCode() {
		List<String> siteRewardsBirthdayClubDiscountCode = MultiSiteUtil.getRewardsBirthdayClubDiscountCode();
		if(siteRewardsBirthdayClubDiscountCode != null && !siteRewardsBirthdayClubDiscountCode.isEmpty()){
			return siteRewardsBirthdayClubDiscountCode;
		}else {
			return rewardsBirthdayClubDiscountCode;
		}
	}

	public List<String> getRewardsBirthdayGoldDiscountCode() {
		List<String> siteRewardsBirthdayGoldDiscountCode = MultiSiteUtil.getRewardsBirthdayGoldDiscountCode();
		if(siteRewardsBirthdayGoldDiscountCode != null && !siteRewardsBirthdayGoldDiscountCode.isEmpty()){
			return siteRewardsBirthdayGoldDiscountCode;
		}else {
			return rewardsBirthdayGoldDiscountCode;
		}
	}

	public List<String> getRewardsBirthdayEliteDiscountCode() {
		List<String> siteRewardsBirthdayEliteDiscountCode = MultiSiteUtil.getRewardsBirthdayEliteDiscountCode();
		if(siteRewardsBirthdayEliteDiscountCode != null && !siteRewardsBirthdayEliteDiscountCode.isEmpty()){
			return siteRewardsBirthdayEliteDiscountCode;
		}else {
			return rewardsBirthdayEliteDiscountCode;
		}
	}


	public List<String> getRewards2XPointsOneDiscountCode() {
		List<String> siteRewards2XPointsOneDiscountCode = MultiSiteUtil.getRewards2XPointsOneDiscountCode();
		if (siteRewards2XPointsOneDiscountCode != null && !siteRewards2XPointsOneDiscountCode.isEmpty()) {
			return siteRewards2XPointsOneDiscountCode;
		} else {
			return rewards2XPointsOneDiscountCode;
		}
	}


	public List<String> getRewards2XPointsTwoDiscountCode() {
		List<String> siteRewards2XPointsTwoDiscountCode = MultiSiteUtil.getRewards2XPointsTwoDiscountCode();
		if(siteRewards2XPointsTwoDiscountCode != null && !siteRewards2XPointsTwoDiscountCode.isEmpty()){
			return siteRewards2XPointsTwoDiscountCode;
		}else {
			return rewards2XPointsTwoDiscountCode;
		}
	}

	public List<String> getRewards3XPointsDiscountCode() {
		List<String> siteRewards3XPointsDiscountCode = MultiSiteUtil.getRewards3XPointsDiscountCode();
		if(siteRewards3XPointsDiscountCode != null && !siteRewards3XPointsDiscountCode.isEmpty()){
			return siteRewards3XPointsDiscountCode;
		}else {
			return rewards3XPointsDiscountCode;
		}
	}

	public long getRewardsGoldTierQualifierDollar() {
		long siteRewardsGoldTierQualifierDollar = MultiSiteUtil.getRewardsGoldTierQualifierDollar();
		if(siteRewardsGoldTierQualifierDollar > 0){
			return siteRewardsGoldTierQualifierDollar;
		}else {
			return rewardsGoldTierQualifierDollar;
		}
	}


	public long getRewardsEliteTierQualifierDollar() {
		long siteRewardsEliteTierQualifierDollar = MultiSiteUtil.getRewardsEliteTierQualifierDollar();
		if(siteRewardsEliteTierQualifierDollar > 0){
			return siteRewardsEliteTierQualifierDollar;
		}else{
			return rewardsEliteTierQualifierDollar;
		}
	}

}