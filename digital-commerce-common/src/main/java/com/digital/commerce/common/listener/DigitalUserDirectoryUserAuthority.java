package com.digital.commerce.common.listener;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import atg.security.User;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.userdirectory.UserDirectoryLoginUserAuthority;

public class DigitalUserDirectoryUserAuthority extends UserDirectoryLoginUserAuthority {

	public boolean login( User user, String arg1, String arg2, Object arg3 ) {
		boolean ret = super.login( user, arg1, arg2, arg3 );
		if( !ret ) {
			logInfo( "====================================================================" );
			logInfo( "Attempt to Login to BCC failed" );
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss:SSS" );
			logInfo( "AT " + sdf.format( cal.getTime() ) );
			logInfo( "For User Id = " + arg1 );
			DynamoHttpServletRequest request = (DynamoHttpServletRequest)ServletUtil.getCurrentRequest();
			if( request != null ) {
				logInfo( "From Source IP = " + request.getRemoteAddr() );
			}
			logInfo( "====================================================================" );

		}

		return ret;
	}
}
