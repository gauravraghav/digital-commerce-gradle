package com.digital.commerce.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.google.common.base.Objects;

public class DecodeUtils {
	private static final String		DEFAULT_ENCODING								= "ISO-8859-1";

	private static final Pattern	DECODE_URL_INVALID_SEQUENCE_DETECTION_PATTERN	= Pattern.compile( "[%](?!\\d{2})" );
	private static final Pattern	DECODE_URL_PATTERN								= Pattern.compile( "[%]\\d{2}" );

	/** Does unescape XML to convert special characters in the escaped input
	 * string like apostrophe etc. Encodes with the format given, before
	 * returning.
	 * 
	 * @param str
	 * @return */
	public static String encodeXml( String str ) {
		return encodeXml( str, DEFAULT_ENCODING );
	}

	/** Does unescape XML to convert special characters in the escaped input
	 * string like apostrophe etc. Encodes with the format given, before
	 * returning.
	 * 
	 * @param str
	 * @param encoding
	 * @return */
	public static String encodeXml( String value, String encoding ) {
		String retVal = "";
		if( !DigitalStringUtil.isEmpty( value ) ) {
			retVal = encode( StringEscapeUtils.unescapeXml( value ), encoding );
		}
		return retVal;
	}

	/** @param value
	 *            - The value to be encoded
	 * @return The encoded value */
	public static String encode( String value ) {
		return encode( value, DEFAULT_ENCODING );
	}

	/** @param value
	 *            - The value to be encoded
	 * @param encoding
	 *            - The encoding
	 * @return The encoded value */
	public static String encode( String value, String encoding ) {
		String retVal = "";
		if( !DigitalStringUtil.isEmpty( value ) ) {
			try {
				retVal = URLEncoder.encode( value, Objects.firstNonNull( encoding, DEFAULT_ENCODING ) );
			} catch( UnsupportedEncodingException e ) {
				throw new IllegalArgumentException( e );
			}
		}
		return retVal;
	}

	/** Note that this is almost like {@link URLDecoder#decode(String, String)}.
	 * The sole difference is that this works one "%dd" match at a time to
	 * ensure that invalid sequences such as "%" do not cause an exception. They
	 * are simply left as is. The "dd" in the "%dd" is treated as a hex value.
	 * Note that it picks the fastest approach possible based on the input.
	 * 
	 * @param value
	 *            - The value to be decoded
	 * @return The decoded value with hex s */
	public static String decode( String value ) {
		return decode( value, DEFAULT_ENCODING );
	}

	/** Note that this is almost like {@link URLDecoder#decode(String, String)}.
	 * The sole difference is that this works one "%dd" match at a time to
	 * ensure that invalid sequences such as "%" do not cause an exception. They
	 * are simply left as is. The "dd" in the "%dd" is treated as a hex value.
	 * Note that it picks the fastest approach possible based on the input.
	 * 
	 * @param value
	 *            - The value to be decoded
	 * @param encoding
	 *            - The encoding (may be null)
	 * @return The decoded url with hex s */
	public static String decode( String value, String encoding ) {
		String retVal = "";
		encoding = Objects.firstNonNull( encoding, DEFAULT_ENCODING );
		if( DigitalStringUtil.isNotBlank( value ) ) {
			if( DECODE_URL_INVALID_SEQUENCE_DETECTION_PATTERN.matcher( value ).find() ) {
				final Matcher matcher = DECODE_URL_PATTERN.matcher( value );
				final StringBuffer sb = new StringBuffer( value.length() );
				while( matcher.find() ) {
					try {
						matcher.appendReplacement( sb, URLDecoder.decode( matcher.group(), encoding ) );
					} catch( UnsupportedEncodingException e ) {
						throw new IllegalArgumentException( e );
					}
				}
				matcher.appendTail( sb );
				retVal = sb.toString();
			} else {
				try {
					retVal = URLDecoder.decode( value, encoding );
				} catch( UnsupportedEncodingException e ) {
					throw new IllegalArgumentException( e );
				}
			}
		}
		return retVal;
	}
}
