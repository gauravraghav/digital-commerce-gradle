/** @author Sunil , Jagadish
 * 
 *         This component performs the task of editing the Nucleus components property value.
 *         It gets the component name, component property and it's new value from the caller. */
package com.digital.commerce.common.scheduledrequest;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.beans.PropertyNotFoundException;
import atg.nucleus.GenericService;
import atg.service.scheduler.Schedulable;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduledRequestService extends GenericService implements Schedulable {

	private ScheduledRequestManager scheduledRequestManager;


	public void performScheduledTask(Scheduler scheduler, ScheduledJob job) {
		ScheduledRequestScheduledJob scheduledRequestScheduledJob = (ScheduledRequestScheduledJob) job;
		if (isLoggingInfo()) {
			logInfo("\n Started executing job " + scheduledRequestScheduledJob + "\n");
		}
		try {
			if (DigitalStringUtil.isNotBlank(scheduledRequestScheduledJob.getScheduledRequest().getComponentProperty())) {
				scheduledRequestManager.editComponentProperty(scheduledRequestScheduledJob.getScheduledRequest());
			}

			if (DigitalStringUtil.isNotBlank(scheduledRequestScheduledJob.getScheduledRequest().getComponentMethod())) {
				scheduledRequestManager.invokeComponentMethod(scheduledRequestScheduledJob.getScheduledRequest());
			}
			
		} catch (PropertyNotFoundException e) {

			if (isLoggingError()) {
				logError("\n  Error in performScheduledTask " + scheduledRequestScheduledJob + "\n");
				logError(e.toString());
			}
		}
		if (isLoggingDebug()) {
			logDebug("\n  Finished executing job " + scheduledRequestScheduledJob + "\n");
		}
	}
}
