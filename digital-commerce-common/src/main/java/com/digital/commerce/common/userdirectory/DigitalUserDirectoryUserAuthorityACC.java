package com.digital.commerce.common.userdirectory;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.security.User;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.userdirectory.UserDirectoryLoginUserAuthority;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalUserDirectoryUserAuthorityACC extends UserDirectoryLoginUserAuthority {
	
	private static final String	USER_FAILED_ACCESS_ATTEMPS = "failedAccessAttempts";
	private static final String	USER_ACCOUNT_LOCKED_TIME = "accountLockedTime";
	private Repository adminSqlRepository;
	private long accountLockTime;
	private int maxAccessAttempts;
	private String accountLockedMsg;


	public boolean login(User user, String arg1, String arg2, Object arg3) {
		
		boolean ret = super.login(user, arg1, arg2, arg3);
		
		if (arg1 != null && !arg1.equals("")) {
			DynamoHttpServletRequest request = (DynamoHttpServletRequest) ServletUtil.getCurrentRequest();
			if (request != null) {
				String cPath = request.getContextPath();
				if (cPath != null && cPath.equalsIgnoreCase("/dyn/admin")) {
					//Update the failed login attempts and lock the account if exceeds the limit
					validateAccountLoginAttempts(arg1, ret);
				}
			}
		}
		
		
		if (!ret) {
			if (arg1 != null && !arg1.equals("")) {
				DynamoHttpServletRequest request = (DynamoHttpServletRequest) ServletUtil
						.getCurrentRequest();
				logInfo("====================================================================");
				if (request != null) {
					String cPath = request.getContextPath();
					logInfo("Attempt to Login to " + cPath + " failed");
					logInfo("From Source IP = " + request.getRemoteAddr());
				} else {
					logInfo("Attempt to Login to ACC failed");
				}
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss:SSS");
				logInfo("AT " + sdf.format(cal.getTime()));
				logInfo("For User Id = " + arg1);
				logInfo("====================================================================");

			}
		}
		return ret;
	}
	
	private void validateAccountLoginAttempts(String accountName,
 boolean ret) {
		try {
			MutableRepositoryItem userAccount = (MutableRepositoryItem) getAdminSqlRepository()
					.getItem(accountName, "account");
			if (null != userAccount) {
				if (!ret) {
					updateFailedAttemptsAndLockAccount(userAccount,ret);
				} else {
					userAccount.setPropertyValue(USER_ACCOUNT_LOCKED_TIME, null);
					userAccount.setPropertyValue(USER_FAILED_ACCESS_ATTEMPS, 0);
				}

			} else {
				if (isLoggingInfo())
					logInfo("################# NO USERS EXITS WITH USERNAME PASSED ######### :::"+accountName);
			}

		} catch (RepositoryException e) {
			if (isLoggingError())
				logError(e.getMessage());
		}
	}

	private void updateFailedAttemptsAndLockAccount(
			MutableRepositoryItem profile, boolean ret) {
		// TODO Auto-generated method stub

		// update failed access attempts
		Object failedAccessAttempts = profile.getPropertyValue(USER_FAILED_ACCESS_ATTEMPS);
		if (!ret) {
			if (failedAccessAttempts == null) {
				profile.setPropertyValue(USER_FAILED_ACCESS_ATTEMPS, 1);
			} else {
				int accessAttempts = (Integer) failedAccessAttempts;
				profile.setPropertyValue(USER_FAILED_ACCESS_ATTEMPS,
						accessAttempts + 1);
			}
		}

		// check if the failed access attempts exceeds max attempts
		if (failedAccessAttempts != null) {
			if ((Integer) failedAccessAttempts > getMaxAccessAttempts()) {
				profile.setPropertyValue(USER_ACCOUNT_LOCKED_TIME,new Timestamp(System.currentTimeMillis()));
				if (isLoggingInfo()) {
					logInfo("################# ACCOUNT LOCKED ######### :::"+getAccountLockedMsg());
				}
				profile.setPropertyValue(USER_FAILED_ACCESS_ATTEMPS, 0);
			}
		}

		// check if the profile has got accountLockedTime set
			Object userAccountLockedTimeObj = profile.getPropertyValue(USER_ACCOUNT_LOCKED_TIME);
			if (userAccountLockedTimeObj != null) {
				long accountLockTime = getAccountLockTime();
				long accountLockedTime = ((Date) userAccountLockedTimeObj).getTime();
				long currentTime = System.currentTimeMillis();
				long diff = currentTime - accountLockedTime;
				long diffMinutes = diff / (60 * 1000);
				if (diffMinutes < accountLockTime) {
					profile.setPropertyValue(USER_FAILED_ACCESS_ATTEMPS, 0);
				} else {
					profile.setPropertyValue(USER_ACCOUNT_LOCKED_TIME, null);
				}
			} 
	}
}
