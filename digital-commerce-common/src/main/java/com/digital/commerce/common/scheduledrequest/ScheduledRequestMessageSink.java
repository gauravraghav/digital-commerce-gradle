package com.digital.commerce.common.scheduledrequest;

import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;
import atg.service.ServerName;
import lombok.Getter;
import lombok.Setter;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.transaction.TransactionManager;

@Getter
@Setter
public class ScheduledRequestMessageSink extends GenericService implements MessageSink {
	boolean							enabled;
	private String					scheduledRequestMessageJMSType;
	private TransactionManager		transactionManager;
	private ScheduledRequestManager	scheduledRequestManager;
	private ServerName				serverNameComponent;

	public synchronized void receiveMessage( String pPortName, Message pMessage ) throws JMSException {
		if( !isEnabled() ) { return; }
		String type = pMessage.getJMSType();
		if( isLoggingDebug() ) {
			logDebug( "\n received pMessage.getJMSType() = " + type + "\n" );
		}
		if( type.equals( getScheduledRequestMessageJMSType() ) ) {
			String serverName = getServerNameComponent().getServerName();
			if( isLoggingDebug() ) {
				logDebug( "\n current ServerName = " + serverName + "\n" );
			}
			ObjectMessage oMessage = (ObjectMessage)pMessage;
			ScheduledRequest mScheduledRequest = (ScheduledRequest)oMessage.getObject();
			if( isLoggingDebug() ) {
				logDebug( "\n ScheduledRequestMessage = " + mScheduledRequest + "\n" );
			}
			if( isLoggingDebug() ) {
				logDebug( "\n  target ServerName(s) sent in the request = " + mScheduledRequest.getServerNames() + "\n" );
			}
			if( mScheduledRequest.getServerNames().isEmpty() || mScheduledRequest.getServerNames().contains( serverName ) ) {
				getScheduledRequestManager().addJobToScheduler( mScheduledRequest );
			}
		}
	}
}
