package com.digital.commerce.common.exception;

import com.digital.commerce.common.exception.DigitalAppException;

public class RepricingServicesException extends DigitalAppException {

	/* */

	public RepricingServicesException( String message, Exception e ) {
		super( message, e );
	}

	public RepricingServicesException( String message ) {
		super( message );
	}

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

}
