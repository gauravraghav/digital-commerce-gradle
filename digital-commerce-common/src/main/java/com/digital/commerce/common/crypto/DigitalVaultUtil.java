package com.digital.commerce.common.crypto;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultUtil;

import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.logger.DigitalLogger;

/**
 * Class containing methods to return the plain text for the JBoss Vaulted
 * Strings. To be used for all passwords in the application. The passwords are
 * vaulted and stored in JBoss specific vault files and folders and the vault is
 * initialized during JBoss start-up
 * 
 * @author HB391569
 */
public class DigitalVaultUtil {
	private static transient DigitalLogger LOGGER = DigitalLogger
			.getLogger(DigitalVaultUtil.class);

	public static String getDecryptedString(String encryptedString)
			throws DigitalAppException {
		String decryptedString = null;
		try {
			decryptedString = SecurityVaultUtil
					.getValueAsString(encryptedString);
		} catch (SecurityVaultException e) {
			LOGGER.error("Error while encrypting the String", e);
			throw new DigitalAppException(e.getMessage());
		}
		return decryptedString;
	}
}
