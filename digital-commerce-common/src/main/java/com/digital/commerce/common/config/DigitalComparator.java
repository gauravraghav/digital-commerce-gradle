package com.digital.commerce.common.config;

import java.util.Comparator;

import atg.commerce.pricing.priceLists.PriceListManager;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;
import lombok.Getter;
import lombok.Setter;

/** @author wibrahim */
@SuppressWarnings({"rawtypes"})
@Getter
@Setter
public abstract class DigitalComparator implements Comparator, DigitalPricingConfiguration {
	private static final String	ASC			= "asc";
	private static final String	DESC		= "desc";
	private boolean				ascending	= true;
	private RepositoryItem		category;
	private RepositoryItem		priceList;
	private PriceListManager	priceListManager;
	private Profile				profile;

	public String getDirection() {
		if( ascending )
			return ASC;
		else
			return DESC;
	}

	public void setDirection( String dir ) {
		ascending = ASC.equals( dir );
	}

}
