package com.digital.commerce.common.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Address {

	private String		firstName;
	private String		lastName;
	private String		middleName;
	private String		address1;
	private String		address2;
	private String		city;
	private String		state;
	private String		zip;
	private String		countryCode;
	private String		phoneNumber;
	private String		email;
}
