package com.digital.commerce.common.util;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.DigitalBaseConstants;


public final class DigitalCommonUtil {
	static Map<String, String> iso2CountryMap;
	static{
		String[] countries = Locale.getISOCountries();
		iso2CountryMap = new HashMap<>(countries.length);
		for (String country : countries) {
		    Locale locale = new Locale(country);
		    iso2CountryMap.put(locale.getISO3Country(), country);
		}		
	}
	
	/*
	 * Returns ISO 3166 country code.
	 * Handles Most ISO 3 to ISO 2 country codes.
	 */
	public static String getCountryCode(String iso3CountryCode) {
		String countryCode2 = null;
		if(iso3CountryCode!=null && iso3CountryCode.length() > 2){
			countryCode2 =iso2CountryMap.get(iso3CountryCode);
			if(null == countryCode2){
				iso3CountryCode.substring( 0, 2 );
			}
		}
		if(null == countryCode2){
			countryCode2=MultiSiteUtil.getDefaultCountry();
		}
	    return countryCode2;
	}
	
	public static double formatAmount(double pAmount){
		DecimalFormat df = new DecimalFormat("#.##");      
		double fAmount = Double.valueOf(df.format(pAmount));
		return fAmount;
	}
	
	public static boolean equalsZero(double x){
		return (Math.abs(x) < (2 * Double.MIN_VALUE));
	}
	
	public static String getSessionAttributeValue( DynamoHttpServletRequest pRequest, SessionAttributesConstant attribute ){
		String attributeValue=null;
		DynamoHttpServletRequest request = pRequest;
		if(null == request){
			request = ServletUtil.getCurrentRequest();
		}
		
		if(null != request){
			attributeValue = (String) request.getSession(false).getAttribute(attribute.getValue());
		}
		return attributeValue;
	}
	
	public static String getSessionAttributeValue(SessionAttributesConstant attribute ){
		return getSessionAttributeValue(null, attribute);
	}
	
	public static void setSessionAttributeValue( DynamoHttpServletRequest pRequest, SessionAttributesConstant attribute, String value ){
		DynamoHttpServletRequest request = pRequest;
		if(null == request){
			request = ServletUtil.getCurrentRequest();
		}
		
		if(null != request){
			request.getSession(false).setAttribute(attribute.getValue(), value);
		}
	}
	
	public static void setSessionAttributeValue(SessionAttributesConstant attribute, String value ){
		setSessionAttributeValue(null, attribute, value);
	}
	
	public static String getRemoteIpAddress(){
		String ipAddress= null;
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		if(null != request){
	    	ipAddress = request.getHeader(RequestHeaderAttributesConstant.HTTP_FORWARD_HEADER.getValue());
	    	if (ipAddress == null) {
	    	   ipAddress = ServletUtil.getCurrentRequest().getRemoteAddr();
	    	}
	    	ipAddress = ipAddress.split(",")[0];
		}
		
		return ipAddress;
	}

	/*Rounding Tax values to Decimal Format */
	public static double round( double value ) {
		double roundedDoubleValue = Math.round( value * 100 );
		return roundedDoubleValue / 100;
	}

    /**
     * This method was created because DigitalCommonUtil.round was rounding 64.475 to 64.47 instead of 64.48
     * @param value
     * @return
     */
    public static double roundHalfUp(double value) {
    	String strValue = new Double(value).toString();
        return (new BigDecimal( strValue ).setScale( 2, RoundingMode.HALF_UP )).doubleValue();
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
	    Map<String, Object> retMap = new HashMap<>();

	    if(json != JSONObject.NULL) {
	        retMap = toMap(json);
	    }
	    return retMap;
	}

	/* convert a JSONObject to a map*/
	public static Map<String, Object> toMap(JSONObject object) throws JSONException {
	    Map<String, Object> map = new HashMap<>();

	    Iterator<String> keysItr = object.keys();
	    while(keysItr.hasNext()) {
	        String key = keysItr.next();
	        Object value = object.get(key);

	        if(value instanceof JSONArray) {
	            value = toList((JSONArray) value);
	        }

	        else if(value instanceof JSONObject) {
	            value = toMap((JSONObject) value);
	        }
	        map.put(key, value);
	    }
	    return map;
	}

	public static List<Object> toList(JSONArray array) throws JSONException {
	    List<Object> list = new ArrayList<>();
	    for(int i = 0; i < array.length(); i++) {
	        Object value = array.get(i);
	        if(value instanceof JSONArray) {
	            value = toList((JSONArray) value);
	        }

	        else if(value instanceof JSONObject) {
	            value = toMap((JSONObject) value);
	        }
	        list.add(value);
	    }
	    return list;
	}
	
	/* Convert a (query) string like "param1=value1&param2=value2" into a map*/ 
	public static Map<String,String> stringToMap(String queryParamStr, String paramSeperator , String valueSeperator) {
		String[] parameters = queryParamStr.split(paramSeperator);
		Map<String,String> paramMap= new HashMap<>();
		for(String param : parameters ){
			String[] splitParamVal = param.split(valueSeperator);
			paramMap.put(splitParamVal[0],splitParamVal[1] );
		}
		return paramMap;
	}

	/** Returns the key to use for the {@link MessageLocator} for text
	 * that can represent the name.
	 * 
	 * @param shippingMethod
	 * @return */
	public static String getNameMessageLocatorKey( String shippingMethod ) {
		String shippingMethodKey;
		if( "GRN".equalsIgnoreCase( shippingMethod ) ) {
			shippingMethodKey = "checkout.shipping.GRN.label";
		} else if( "2ND".equalsIgnoreCase( shippingMethod ) ) {
			shippingMethodKey = "checkout.shipping.2ND.label";
		} else if( "NDA".equalsIgnoreCase( shippingMethod ) ) {
			shippingMethodKey = "checkout.shipping.NDA.label";
		} else {
			shippingMethodKey = "shipping";
		}
		return shippingMethodKey;
	}
	
	
	public static String getName(String firstName, String middleName, String lastName){
		String name = null;
		String fn = DigitalStringUtil.trim(firstName);
		String mn = DigitalStringUtil.trim(middleName);
		String ln = DigitalStringUtil.trim(lastName);
		
		if(DigitalStringUtil.isNotBlank(fn)){
			name = fn;
			if(DigitalStringUtil.isNotBlank(mn)){
				name += " " + mn;
			}
			if(DigitalStringUtil.isNotBlank(ln)){
				name += " " + ln;
			}
		}
		
		return name;
	}	
	
	/** Returns URLDecoded string
	 * 
	 * @param param
	 * @return
	 * @throws DigitalAppException */
	public static String getDecodedString( String param ) throws DigitalAppException {
		String decodedStr = "";
		try {
			decodedStr = URLDecoder.decode( param, "UTF-8" );
			return decodedStr;
		} catch( UnsupportedEncodingException e ) {
			throw new DigitalAppException( e );
		}
	}
	
	public static String getATGMappedOrderLineStatus(String statusCode) {
		DigitalBaseConstants dswConstants = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.DSW_BASE_CONSTANTS,
				DigitalBaseConstants.class);
		Map<String, String> orderLineStatusMap = dswConstants.getOrderLineItemStatusMap();
		return orderLineStatusMap.get(statusCode);

	}
	
	public static String getCarrierCode(String carrierCode){
		if(DigitalStringUtil.isBlank(carrierCode)){
			return carrierCode;
		}
		String carrierServiceCode = carrierCode.trim();
		DigitalBaseConstants dswConstants = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.DSW_BASE_CONSTANTS,
				DigitalBaseConstants.class);
		Map<String,String> carrierCodeMap = dswConstants.getCarrierCodeMap();
		
		String mappedCode = carrierCodeMap.get(carrierServiceCode);
		
		if(DigitalStringUtil.isNotBlank(mappedCode)){
			carrierServiceCode = mappedCode.trim();
		}
		
		return carrierServiceCode;		
	}
}
