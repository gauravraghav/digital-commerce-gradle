package com.digital.commerce.common.region;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import atg.commerce.locations.Coordinate;
import atg.commerce.units.UnitOfMeasureTools;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.QueryOptions;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.SortDirectives;
import atg.repository.rql.RqlStatement;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("rawtypes")
@Getter
@Setter
public class RegionManager {

	private Repository repository;

	private double radius;
	private String regionItemType;
	private String postalMasterItemType;
	private UnitOfMeasureTools unitOfMeasureTools;


	public Map getAllRegions() throws RepositoryException {
		RepositoryView view = getRepository().getView(this.regionItemType);
		RqlStatement statement = RqlStatement.parseRqlStatement("ALL");
		RepositoryItem[] items = statement.executeQuery(view, null);
		
		if(items == null)return null;
			
		HashMap<String, String> result = new HashMap<>();
		//int i = 0;
		for (RepositoryItem item : items) {
			String regionName = (String)item.getPropertyValue("regionName");
			result.put(regionName, regionName);
		}

		return result;

	}


	public String getRegionByState(String state, String country){
		try {
			RepositoryView view = getRepository().getView(this.regionItemType);
			RqlStatement statement = RqlStatement
					.parseRqlStatement("state INCLUDES \"" + state + "\"");
			RepositoryItem[] items = statement.executeQuery(view, null);

			if (items != null && items.length > 0)
				return (String) items[0].getPropertyValue("regionName");
		} catch (RepositoryException e) {
			return null;
		}
		return null;
	}
	
	public String getStateByCoordinate(double latitude, double longitude) {
		String state = null;

		try {
			RepositoryView view = getRepository().getView(this.postalMasterItemType);
			double distance = getUnitOfMeasureTools().convertValue(this.radius, "mi_us");
			Query query = getCoordinateRadiusQuery(new Coordinate(latitude, longitude), distance, view);
            QueryOptions queryOptions = new QueryOptions(0,1, new SortDirectives(), null);
			RepositoryItem[] items = view.executeQuery(query, queryOptions);
            //items.length doesn't make n number of calls. It is the executeQuery method that loops through the results set,
            // so it is good practise to set the queryOption with number of records to be returned.
			if (items != null && items.length > 0) {
				state = (String) items[0].getPropertyValue("state");
			}
		} catch (RepositoryException e) {
			return null;
		}
		return state;
	}

	public String getRegionByOther() {
		return null;
	}

	private Query getCoordinateRadiusQuery(Coordinate pCoordinate,
			double pRadius, RepositoryView pView) throws RepositoryException {
		Query query = null;
		if ((pRadius >= 0.0D) && (pCoordinate != null)) {
			QueryBuilder builder = pView.getQueryBuilder();

			double minLat = addMetersToLatitude(pCoordinate, -pRadius);
			double maxLat = addMetersToLatitude(pCoordinate, pRadius);
			double minLong = addMetersToLongitude(pCoordinate, -pRadius);
			double maxLong = addMetersToLongitude(pCoordinate, pRadius);

			QueryExpression lat = builder
					.createPropertyQueryExpression("latitude");
			QueryExpression log = builder
					.createPropertyQueryExpression("longitude");

			ArrayList<Query> queries = new ArrayList<>();

			Query latMinQuery = builder.createComparisonQuery(lat, builder
					.createConstantQueryExpression(Double.valueOf(minLat)),
					QueryBuilder.GREATER_THAN_OR_EQUALS);

			Query latMaxQuery = builder.createComparisonQuery(lat, builder
					.createConstantQueryExpression(Double.valueOf(maxLat)),
					QueryBuilder.LESS_THAN_OR_EQUALS);

			Query logMinQuery = builder.createComparisonQuery(log, builder
					.createConstantQueryExpression(Double.valueOf(minLong)),
					QueryBuilder.GREATER_THAN_OR_EQUALS);

			Query logMaxQuery = builder.createComparisonQuery(log, builder
					.createConstantQueryExpression(Double.valueOf(maxLong)),
					QueryBuilder.LESS_THAN_OR_EQUALS);

			queries.add(latMaxQuery);
			queries.add(latMinQuery);
			if (maxLong < minLong) {
				queries.add(builder.createOrQuery(new Query[] { logMinQuery,
						logMaxQuery }));
			} else {
				queries.add(logMinQuery);
				queries.add(logMaxQuery);
			}

			query = builder.createAndQuery((Query[]) queries
					.toArray(new Query[queries.size()]));
		}
		return query;
	}

	private Coordinate addDistanceToPoint(Coordinate pCoordinate,
			double pDistance, double pBearing) {
		double lat = Math.toRadians(pCoordinate.getLatitude());
		double lon = Math.toRadians(pCoordinate.getLongitude());
		double distance = Math.abs(pDistance) / 6371000.0D;
		double bearing = Math.toRadians(pBearing);
		double lat2 = Math.asin(Math.sin(lat) * Math.cos(distance)
				+ Math.cos(lat) * Math.sin(distance) * Math.cos(bearing));

		double lon2 = lon
				+ Math.atan2(
						Math.sin(bearing) * Math.sin(distance) * Math.cos(lat),
						Math.cos(distance) - Math.sin(lat) * Math.sin(lat2));

		double latDeg = Math.toDegrees(lat2);
		double lonDeg = Math.toDegrees(lon2);
		if (lonDeg > 360.0D) {
			lonDeg -= Math.floor(lonDeg / 360.0D) * 360.0D;
		} else if (lonDeg < -360.0D) {
			lonDeg += Math.floor(Math.abs(lonDeg / 360.0D)) * 360.0D;
		}
		if (lonDeg > 180.0D) {
			lonDeg -= 360.0D;
		} else if (lonDeg < -180.0D) {
			lonDeg += 360.0D;
		}
		if (latDeg > 90.0D) {
			latDeg = 90.0D;
		}
		if (latDeg < -90.0D) {
			latDeg = -90.0D;
		}
		return new Coordinate(latDeg, lonDeg);
	}

	public double addMetersToLatitude(Coordinate pCoordinate, double pMeters) {
		double direction = 0.0D;
		if (pMeters < 0.0D) {
			direction = 180.0D;
		}
		return addDistanceToPoint(pCoordinate, pMeters, direction)
				.getLatitude();
	}

	private double addMetersToLongitude(Coordinate pCoordinate, double pMeters) {
		double direction = 90.0D;
		if (pMeters < 0.0D) {
			direction = 270.0D;
		}
		return addDistanceToPoint(pCoordinate, pMeters, direction)
				.getLongitude();
	}

}
