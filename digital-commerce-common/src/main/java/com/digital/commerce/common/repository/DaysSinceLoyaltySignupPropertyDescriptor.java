package com.digital.commerce.common.repository;

import java.sql.Date;

import org.apache.commons.lang.time.DateUtils;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.adapter.gsa.GSAItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

/**  */
public class DaysSinceLoyaltySignupPropertyDescriptor extends RepositoryPropertyDescriptor {
	protected static final DigitalLogger	logger	= DigitalLogger.getLogger( DaysSinceLoyaltySignupPropertyDescriptor.class );
	private static final long		serialVersionUID	= 1L;

	protected static final String	TYPE_NAME			= "DaysSinceLoyaltySignupPropertyDescriptor";

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, DaysSinceLoyaltySignupPropertyDescriptor.class );
	}

	@Override
	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		Object retVal = getDefaultValue();
		if( pItem != null ) {
			GSAItem gsaItem = (GSAItem)pItem;
			Date loyaltySignupDate = (Date)gsaItem.getPropertyValue( "loyaltySignupDate" );
			if( loyaltySignupDate != null ) {
				final long currentTimeInMillis = System.currentTimeMillis();
				final long loyaltySignupInMillis = loyaltySignupDate.getTime();
				final long differenceInMillis = currentTimeInMillis - loyaltySignupInMillis;
				long differenceInDays = differenceInMillis / DateUtils.MILLIS_PER_DAY;
				retVal = new Long( differenceInDays );
			}
		}
		return retVal;
	}

}
