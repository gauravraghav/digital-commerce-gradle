package com.digital.commerce.common.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalAppException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorCode;

	public DigitalAppException(Throwable ex) {
		super(ex);
	}

	public DigitalAppException(String message) {
		super(message);
	}

	public DigitalAppException(String message, Throwable ex) {
		super(message, ex);
	}

	public DigitalAppException(String errorCode, String message, Throwable ex) {
		super(message, ex);
		this.errorCode = errorCode;
	}

}
