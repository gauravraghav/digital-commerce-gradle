package com.digital.commerce.common.taglib;

import java.lang.reflect.Array;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.servlet.SessionListener;
import com.digital.commerce.common.util.DecodeUtils;
import com.digital.commerce.common.util.DigitalCreditCardUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.common.util.EscapeUtils;
import com.google.common.base.Functions;
import com.google.common.base.Objects;
import com.google.common.base.Predicates;
import com.google.common.collect.AbstractIterator;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import atg.core.exception.ItemNotFoundException;
import atg.core.util.BeanUtils;
import atg.repository.RepositoryItem;
@SuppressWarnings({"unchecked","rawtypes"})
public class ELFunctions {
	private final static String XSS_REGEX = "[<>\"'&%]";
	private static final Logger	logger	= LogManager.getLogger( SessionListener.class );

	/**
	 * Returns a collection in reverse order. Note that iteration on the
	 * Collection better be in a consistent manner. E.g., for a {@link List} and
	 * {@link LinkedHashSet}, and {@link SortedSet} order is guaranteed. However
	 * for a {@link HashMap} order is not guaranteed.
	 * 
	 * @param collection
	 * @return
	 */
	public static Collection subCollection(final Collection collection, int beginIndex, int endIndex) {
		final Collection<Object> sub;
		if (collection instanceof Set) {
			sub = Sets.newHashSet();
		} else {
			sub = Lists.newArrayList();
		}
		if (collection != null) {
			beginIndex = Math.max(beginIndex, 0);
			endIndex = Math.min(endIndex, collection.size());
			if (beginIndex < endIndex) {
				int i = 0;
				for (Iterator<?> iterator = collection.iterator(); iterator.hasNext(); i++) {
					final Object item = iterator.next();
					if (i >= beginIndex && i < endIndex) {
						sub.add(item);
					}
				}
			}
		}

		return sub;
	}

	/**
	 * Returns a collection in reverse order. Note that iteration on the
	 * Collection better be in a consistent manner. E.g., for a {@link List} and
	 * {@link LinkedHashSet}, and {@link SortedSet} order is guaranteed. However
	 * for a {@link HashSet} order is not guaranteed.
	 * 
	 * @param collection
	 * @return
	 */
	public static List reverse(final Collection collection) {
		List<Object> retVal = null;
		if (collection != null) {
			retVal = Lists.newArrayList(collection);
			Collections.reverse(retVal);
		}
		return retVal;
	}

	/**
	 * Merges an object into another collection. The originals are not changed.
	 * The object to merge could be a {@link Iterable}, {@link Iterator}, or
	 * just an {@link Object}. If it is just an {@link Object}, it is simply
	 * added to the collection.
	 * 
	 * @param collection
	 *            may be null
	 * @param object
	 *            may be null
	 * @return
	 */
	public static Collection mergedCollection(final Collection collection, final Object object) {
		final Collection<Object> merged;
		if (collection instanceof Set) {
			merged = Sets.newLinkedHashSet();
		} else {
			merged = Lists.newArrayList();
		}
		if (collection != null) {
			merged.addAll(collection);
		}
		if (object instanceof Iterator) {
			merged.addAll(Lists.newArrayList((Iterator<?>) object));
		} else if (object instanceof Iterable) {
			merged.addAll(Lists.newArrayList((Iterable<?>) object));
		} else if (object != null) {
			if (object.getClass().isArray()) {
				final int length = Array.getLength(object);
				for (int i = 0; i < length; i++) {
					merged.add(Array.get(object, i));
				}
			} else {
				merged.add(object);
			}
		}
		return merged;
	}

	/**
	 * Joins an object into a string where each entry is separated by the
	 * separator. The originals are not changed. The object to merge could be a
	 * {@link Iterable}, {@link Iterator}, or just an {@link Object}. If it is
	 * just an {@link Object}, it is simply added to the collection.
	 * 
	 * @param object
	 *            may be null
	 * @param separator
	 *            may be null
	 * @return
	 */
	public static String join(final Object object, final String separator) {
		String retVal = null;
		if (object instanceof Iterable) {
			retVal = StringUtils.join(((Iterable<?>) object).iterator(), separator);
		} else if (object instanceof Iterator) {
			retVal = StringUtils.join((Iterator<?>) object, separator);
		} else if (object != null) {
			if (object.getClass().isArray()) {
				final List<Object> o = Lists.newArrayList();
				for (int i = 0; i < Array.getLength(object); i++) {
					o.add(Array.get(object, i));
				}
				retVal = StringUtils.join(o, separator);
			} else {
				retVal = object.toString();
			}
		}
		return retVal;
	}

	/**
	 * Joins an object into a string where each entry is separated by the
	 * separator. The originals are not changed. The object to merge could be a
	 * {@link Iterable}, {@link Iterator}, or just an {@link Object}. If it is
	 * just an {@link Object}, it is simply added to the collection.
	 * 
	 * @see EscapeUtils.escapeXml(String)
	 * 
	 * @param object
	 *            may be null
	 * @param separator
	 *            may be null
	 * @return
	 */
	public static String joinXml(final Object object, final String separator) {
		final Iterator<?> iter = iterator(object);
		return StringUtils.join(new AbstractIterator<Object>() {
			@Override
			public Object computeNext() {
				if (iter.hasNext()) {
					return EscapeUtils.escapeXml(ObjectUtils.toString(iter.next()));
				}
				return endOfData();
			}
		}, separator);
	}

	/**
	 * Joins an object into a string where each entry is separated by the
	 * separator. The originals are not changed. The object to merge could be a
	 * {@link Iterable}, {@link Iterator}, or just an {@link Object}. If it is
	 * just an {@link Object}, it is simply added to the collection.
	 * 
	 * @see ELFunctions#escapeJson(String)
	 * 
	 * @param object
	 *            may be null
	 * @param separator
	 *            may be null
	 * @return
	 */
	public static String joinJson(final Object object, final String separator) {
		final Iterator<?> iter = iterator(object);
		return StringUtils.join(new AbstractIterator<Object>() {
			@Override
			public Object computeNext() {
				if (iter.hasNext()) {
					return escapeJson(ObjectUtils.toString(iter.next()));
				}
				return endOfData();
			}
		}, separator);
	}

	/**
	 * Returns an object iterator for the given object. If it is an @link
	 * Iterable it calls @link {@link Iterable#iterator()}, arrays are converted
	 * to an iterator, all other objects are singleton iterators.
	 * 
	 * @param object
	 * @return
	 */
	protected static Iterator<?> iterator(final Object object) {
		final Iterator<?> iter;
		if (object instanceof Iterable) {
			iter = ((Iterable<?>) object).iterator();
		} else if (object instanceof Iterator) {
			iter = (Iterator<?>) object;
		} else if (object instanceof Enumeration) {
			iter = Iterators.forEnumeration((Enumeration<?>) object);
		} else if (object != null) {
			if (object.getClass().isArray()) {
				final int length = Array.getLength(object);
				final List<Object> o = Lists.newArrayListWithCapacity(length);
				for (int i = 0; i < length; i++) {
					o.add(Array.get(object, i));
				}
				iter = o.iterator();
			} else {
				iter = Iterators.singletonIterator(object);
			}
		} else {
			iter = Iterators.emptyIterator();
		}
		return iter;
	}

	/**
	 * Merges two maps into one. The originals are not changed.
	 * 
	 * @param map1
	 *            may be null
	 * @param map2
	 *            may be null
	 * @return
	 */
	public static Map mergedMap(final Map map1, final Map map2) {
		final Map merged = Maps.newLinkedHashMap();
		if (map1 != null) {
			merged.putAll(map1);
		}
		if (map2 != null) {
			merged.putAll(map2);
		}
		return merged;
	}

	/**
	 * Detects if an object contains an object. If the object was an
	 * {@link Iterable}, {@link Iterator}, or is an array it will search for the
	 * element based on {@link Object#equals(Object)}. If the object was a
	 * {@link CharSequence}, it will search for an occurrence of the
	 * {@link String}.
	 * 
	 * @param object
	 *            may be null
	 * @param element
	 *            may be null
	 * @return
	 */
	public static boolean contains(final Object object, final Object element) {
		boolean retVal = false;
		if (object instanceof Iterable) {
			retVal = Iterables.contains((Iterable<?>) object, element);
		} else if (object instanceof Iterator) {
			retVal = Iterators.contains((Iterator<?>) object, element);
		} else if (object instanceof CharSequence) {
			retVal = StringUtils.contains(object.toString(), Objects.firstNonNull(element, "").toString());
		} else if (object != null && object.getClass().isArray()) {
			final int length = Array.getLength(object);
			for (int i = 0; !retVal && i < length; i++) {
				final Object item = Array.get(object, i);
				retVal = (item == null && element == null) || (item != null && item.equals(element));
			}
		}

		return retVal;
	}

	/**
	 * Tests to see if the input string matches the regular expression
	 * 
	 * @param string
	 * @param regex
	 * @return
	 */
	public static boolean matches(Object object, String regex) {
		Iterator<?> it = iterator(object);
		return Iterators.any(Iterators.transform(it, Functions.toStringFunction()), Predicates.containsPattern(regex));
	}

	/**
	 * Replaces each substring in string that matches the regular expression.
	 * 
	 * @param string
	 *            string to replace text in
	 * @param regex
	 *            the regular expression to use
	 * @param replacement
	 *            the replacement text (may be a regular expression)
	 * @return
	 */
	public static String replaceAll(String string, String regex, String replacement) {
		return string != null ? string.replaceAll(regex, replacement) : null;
	}

	/**
	 * Gets a property value from a repository item. the objects that are
	 * evaluated can be {@link RepositoryItem}s, {@link Map}s, {@link Iterable}
	 * s, {@link Iterator}s, and Arrays. since JSP 1.1 does not support custom
	 * EL resolvers, our hands our tied a bit!
	 * 
	 * @param object
	 * @param dotExpression
	 * @return
	 */
	public static Object getPropertyValue(Object object, String dotExpression) {
		Object retVal = null;
		if (object == null) {
			retVal = StringUtils.EMPTY;
		} else if (DigitalStringUtil.isNotBlank(dotExpression)) {
			String property = dotExpression;
			String leftover = null;
			int dot = dotExpression.indexOf('.');
			if (dot != -1) {
				property = dotExpression.substring(0, dot);
				if (dotExpression.length() > dot) {
					leftover = dotExpression.substring(dot + 1);
				}
			}
			boolean isPropertyDigits = NumberUtils.isDigits(property);
			if (object instanceof RepositoryItem) {
				retVal = ((RepositoryItem) object).getPropertyValue(property);
			} else if (object instanceof Map) {
				retVal = ((Map<?, ?>) object).get(property);
			} else if (object instanceof Iterable && isPropertyDigits) {
				final int index = Integer.valueOf(property);
				if (index > -1 && index < Iterables.size((Iterable<?>) object)) {
					retVal = Iterables.get((Iterable<?>) object, index);
				}
			} else if (object instanceof Iterator && isPropertyDigits) {
				final int index = Integer.valueOf(property);
				if (index > -1 && index < Iterators.size((Iterator<?>) object)) {
					retVal = Iterators.get((Iterator<?>) object, index);
				}
			} else if (object instanceof Array && isPropertyDigits) {
				final int index = Integer.valueOf(property);
				final int length = Array.getLength(object);
				if (index > -1 && index < length) {
					retVal = Array.get(object, index);
				}
			} else if (object != null) {
				try {
					retVal = BeanUtils.getPropertyValue(object, property);
				} catch (ItemNotFoundException e) {
					// possible error here
					retVal = null;
				}
			}
			if (DigitalStringUtil.isNotBlank(leftover)) {
				retVal = getPropertyValue(retVal, leftover);
			}
		}
		return retVal;
	}

	/**
	 * Masks a credit card according to Digital styles.
	 * 
	 * @param creditCardNumber
	 * @return
	 */
	public static String getMaskedCreditCardNumber(String creditCardNumber) {
		return DigitalCreditCardUtil.getMaskedCreditCardNumber(creditCardNumber);
	}

	/**
	 * 
	 * @param creditCardNumber
	 * @return
	 */
	public static String getCreditCardNumberLast4(String creditCardNumber) {
		return DigitalCreditCardUtil.getCreditCardNumberLast4(creditCardNumber);
	}

	/**
	 * 
	 * @param giftCardNumber
	 * @return
	 */
	public static String getGiftCardNumberLast4(String giftCardNumber) {
		if (giftCardNumber == null) {
			return "";
		}
		if (giftCardNumber.length() < 4) {
			return "";
		}
		return giftCardNumber.substring(giftCardNumber.length() - 4);
	}

	/**
	 * 
	 * @param expirationMonth
	 * @param expirationYear
	 * @return
	 */
	public static boolean isCreditCardExpired(String expirationMonth, String expirationYear) {
		return DigitalCreditCardUtil.isCreditCardExpired(expirationMonth, expirationYear);
	}

	/**
	 * 
	 * @param giftCardNumber
	 * @return
	 */
	public static String getMaskedGiftCardNumber(String giftCardNumber) {
		String retVal = StringUtils.EMPTY;
		giftCardNumber = StringUtils.trimToEmpty(giftCardNumber);
		if (DigitalStringUtil.isNotBlank(giftCardNumber)) {
			retVal = new StringBuilder(DigitalBaseConstants.GIFT_CARDNUMBER_LENGTH)
					.append(StringUtils.repeat("X", DigitalBaseConstants.GIFT_CARDNUMBER_LENGTH - 4))
					.append(giftCardNumber.substring(DigitalBaseConstants.GIFT_CARDNUMBER_LENGTH - 4)).toString();
		}
		return retVal;
	}

	/**
	 * @param phoneNumber
	 * @return
	 */
	public static String getFormattedPhoneNumber(String phoneNumber) {
		phoneNumber = StringUtils.replace(phoneNumber, "-", "");
		phoneNumber = StringUtils.trimToEmpty(phoneNumber);
		if (StringUtils.length(phoneNumber) == DigitalBaseConstants.MAX_PHONE_US_LENGTH + 1
				&& phoneNumber.charAt(0) == '1') {
			phoneNumber = phoneNumber.substring(1);
		}
		if (StringUtils.length(phoneNumber) == DigitalBaseConstants.MAX_PHONE_US_LENGTH) {
			StringBuilder buffer = new StringBuilder();
			return buffer.append(phoneNumber.substring(0, 3)).append(".").append(phoneNumber.substring(3, 6))
					.append(".").append(phoneNumber.substring(6, DigitalBaseConstants.MAX_PHONE_US_LENGTH)).toString();
		}
		return phoneNumber;
	}

	/**
	 * @param parameter
	 *            - The parameter to be cleaned
	 * @return - The cleaned parameter
	 */
	public static String sanitize(final String value) {
		String retVal = "";
		if (!DigitalStringUtil.isEmpty(value)) {
			try {
				retVal = value.replaceAll(XSS_REGEX, "");
			} catch (PatternSyntaxException e) {
				throw new IllegalArgumentException(e);
			}
		}
		return retVal;
	}

	/**
	 * @param url
	 *            - The url to be encoded
	 * @param encoding
	 *            - The encoding
	 * @return The encoded url
	 */
	public static String encodeURL(String url, String encoding) {
		return DecodeUtils.encode(url, encoding);
	}

	/**
	 * Note that this is almost like {@link URLDecoder#decode(String, String)}.
	 * The sole difference is that this works one "%dd" match at a time to
	 * ensure that invalid sequences such as "%" do not cause an exception. They
	 * are simply left as is. The "dd" in the "%dd" is treated as a hex value.
	 * Note that it picks the fastest approach possible based on the input.
	 * 
	 * @param url
	 *            - The url to be decoded
	 * @param encoding
	 *            - The encoding (may be null)
	 * @return The decoded url with hex s
	 */
	public static String decodeURL(String url, String encoding) {
		return DecodeUtils.decode(url, encoding);
	}	

	/**
	 * Converts a string to uppercase without damaging escape sequences such as
	 * &amp; or &quot;
	 * 
	 * @param s
	 * @return The same string with display content converted to upper case
	 */
	public static String toUpperCaseXML(String s) {
		return StringEscapeUtils.escapeXml(StringEscapeUtils.unescapeXml(s).toUpperCase());
	}

	/**
	 * Escapes the string for Json. Note that this assumes single and double
	 * quotes will be escaped.
	 * 
	 * @param s
	 * @return
	 */
	public static String escapeJson(String s) {
		return StringEscapeUtils.escapeJavaScript(s);
	}

	/**
	 * Unescapes the html string for Json.
	 * 
	 * @see StringEscapeUtils#unescapeHtml(String)
	 * @param s
	 * @return
	 */
	public static String unescapeHtml(String s) {
		return StringEscapeUtils.unescapeHtml(s);
	}

	public static Date getCurrentDate() {
		return new Date();
	}

	public static int countMatches(String str, String sub) {
		return StringUtils.countMatches(str, sub);
	}

	public static boolean instanceOf(Object o, String className) {
		try {
			return Class.forName(className).isInstance(o);
		} catch (ClassNotFoundException e) {
			return false;
		}
	}

	/**
	 * Does unescape XML to convert special characters in the escaped input
	 * string like apostrophe etc. Encodes with the format given, before
	 * returning.
	 * 
	 * @param str
	 * @param encoding
	 * @return
	 */

	public static String encodeString(String str, String encoding) {
		return DecodeUtils.encodeXml(str, encoding);
	}

	public static String capitalize(String str) {
		return WordUtils.capitalize(str);
	}

	public static String capitalizeFully(String str) {
		return WordUtils.capitalizeFully(str);
	}

	public static long currentTimeMillis() {
		return System.currentTimeMillis();
	}

	public static String getValueFromMap(Map mapEntered, Object key) {
		if (mapEntered != null && key != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("None of the arguments are null");
			}
			return (String) mapEntered.get(key.toString());
		} else {
			if (mapEntered == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("The map passed in is null");
				}
			}
			if (key == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("The key passed in is null");
				}
			}
			if (logger.isDebugEnabled()) {
				logger.debug("The key is " + key);
			}
			return null;
		}
	}

	public static String getFormattedDoubleValue(Double value) {
		return new DecimalFormat("#0.00").format(value).toString();
	}
	
	public static String getSEOProductDisplayName(String productDisplayName) {	
		if(DigitalStringUtil.isBlank(productDisplayName)){
			return productDisplayName;
		}
		
		return productDisplayName.toLowerCase().trim().replaceAll("-", " ").replaceAll("\\s+", "-").replaceAll("&", "and")
				.replaceAll("[^a-z0-9\\-\\.]+", "");
	}
}
