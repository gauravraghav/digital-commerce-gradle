package com.digital.commerce.common.exception;

public class DataAccessException extends RuntimeException {
	private static final long	serialVersionUID	= 1L;

	public DataAccessException( Throwable ex ) {
		super( ex );
	}

	public DataAccessException( String message ) {
		super( message );
	}

	public DataAccessException( String message, Throwable ex ) {
		super( message, ex );
	}
}
