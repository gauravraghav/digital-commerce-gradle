package com.digital.commerce.common.communication;

import atg.userprofiling.email.TemplateEmailInfoImpl;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalTemplateEmailInfo extends TemplateEmailInfoImpl {
	
	private static final long serialVersionUID = 1L;
	private Long deliveryDelay = 0L;

}
