package com.digital.commerce.common.multisite;

import static com.digital.commerce.common.services.DigitalBaseConstants.DEFAULT_SHIPPING_RATES;
import static com.digital.commerce.common.services.DigitalBaseConstants.SHIPPING_METHODS;
import static com.digital.commerce.common.services.DigitalBaseConstants.STATIC_SHIPPING_RATES;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.multisite.DefaultSiteContextRuleFilter;
import atg.multisite.Site;
import atg.multisite.SiteContext;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.RequestLocale;
import atg.servlet.ServletUtil;

public class MultiSiteUtil {
	private static final String DEFAULT_COUNTRY_ISO2 = "US";
	private static final int DEFAULT_NEW_ON_CLEARANCE_DAYS = 30;
	private static final DigitalLogger logger = DigitalLogger.getLogger(MultiSiteUtil.class);
	private MultiSiteUtil() {

	}
	/**
	 *
	 * @return Site
	 */
	public static Site getSite() {
		// Get current site
		Site site = SiteContextManager.getCurrentSite();
		if (logger.isDebugEnabled() && site == null) {
			logger.debug("SiteId is null");
		}
		return site;
	}

	/**
	 *
	 * @return String - country code
	 */
	public static String getDefaultCountry() {
		Site site = getSite();
		String country = DEFAULT_COUNTRY_ISO2;
		if (null != site) {
			Object countryObj = site.getPropertyValue(SitePropertiesManager.getDefaultCountryPropertyName());
			country = (null != countryObj)
					? (String) countryObj : DEFAULT_COUNTRY_ISO2;
			if (DigitalStringUtil.isBlank(country)) {
				country = DEFAULT_COUNTRY_ISO2;
			}
		}
		return country;
	}

	/**
	 *
	 * @return double - dollar threshold value to allow FREE shipping for BASIC
	 *         and Anonymous customers
	 */
	public static double getBasicShippingThreshold() {
		double basicShippingThreshold = 35;
		Site site = getSite();

		if (null != site) {
			Object bsThreshold = site.getPropertyValue(SitePropertiesManager.getBasicShippingThresholdPropertyName());
			basicShippingThreshold = (null != bsThreshold)
					? (Double) bsThreshold
					: basicShippingThreshold;
			if (basicShippingThreshold < 0) {
				basicShippingThreshold = 35;
			}
		}
		return basicShippingThreshold;
	}

	/**
	 *
	 * @return double - dollar maximum threshold allowed for any given order
	 */
	public static double getCartPurchaseLimit() {
		int orderPurchaseLimit = 55000;
		Site site = getSite();

		if (null != site) {
			Object opL = site.getPropertyValue(SitePropertiesManager.getOrderPurchaseLimitPropertyName());
			orderPurchaseLimit = (null != opL)
					? (Integer) opL
					: orderPurchaseLimit;
			if (orderPurchaseLimit <= 0) {
				orderPurchaseLimit = 55000;
			}
		}

		return orderPurchaseLimit;
	}

	/**
	 *
	 * @return int - maximum number of commerce line allowed for any given order
	 */
	public static int getCartLineLimit() {
		int cartLineLimit = 30;
		Site site = getSite();
		if (null != site) {
			Object clL = site.getPropertyValue(SitePropertiesManager.getOrderCartLineLimitPropertyName());
			cartLineLimit = (null != clL)
					? (Integer) clL
					: cartLineLimit;
			if (cartLineLimit <= 0) {
				cartLineLimit = 30;
			}
		}
		return cartLineLimit;
	}

	/**
	 *
	 * @return int - maximum number of wishlist items allowed per profile
	 */
	public static int getWishlistItemLimit() {
		int wishlistItemLimit = 30;
		Site site = getSite();
		if (null != site) {
			Object wlIL = site.getPropertyValue(SitePropertiesManager.getWishlistItemLimitPropertyName());
			wishlistItemLimit = (null != wlIL)
					? (Integer)wlIL
					: wishlistItemLimit;
			if (wishlistItemLimit == 0) {
				wishlistItemLimit = 30;
			}
		}

		return wishlistItemLimit;
	}

	/**
	 *
	 * @return int - maximum number of commerce items allowed for any given
	 *         order
	 */
	public static int getCartSkuLimit() {
		int cartSKULimit = 30;
		Site site = getSite();

		if (null != site) {
			Object cSL = site.getPropertyValue(SitePropertiesManager.getOrderSkuQtyLimitPropertyName());
			cartSKULimit = (null != cSL)
					? (Integer) cSL
					: cartSKULimit;
			if (cartSKULimit <= 0) {
				cartSKULimit = 30;
			}
		}

		return cartSKULimit;
	}

	/**
	 * Check if the total amount in the cart is not greater than the allowed
	 * total amount when adding a GC to the cart.
	 *
	 * @param order
	 * @param limit
	 * @param amount
	 * @return true/false
	 */
	public static boolean isCartPurchaseLimitExceeded(Order order, double limit, double amount) {

		if (limit != -1 && order.getPriceInfo() != null) {
			if (order.getPriceInfo().getAmount() + amount > limit) {
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 * Checks if the total no of line items in the cart is not greater than the
	 * allowed total no of line item limit when a GC is added to the cart.
	 *
	 * @param order
	 * @param qty
	 * @return true/false
	 */
	public static boolean isGCCartLineLimitExceeded(Order order, int qty) {

		if (qty != -1) {
			if (order.getCommerceItemCount() >= qty) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the total No of skus per line item is not greater than the
	 * allowed limit when a GC is added to the cart.
	 *
	 * @param order
	 * @param qty
	 * @param cardqty
	 * @return true/false
	 */
	public static boolean isCartSkuLimitExceeded(Order order, int qty, long cardqty) {

		@SuppressWarnings("unchecked")
		List<CommerceItem> commerceItems = order.getCommerceItems();
		if (qty != -1) {
			for (CommerceItem item : commerceItems) {
				if (item.getQuantity() + cardqty > qty) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Checks if the total No of skus per line item is not greater than the
	 * allowed limit when a GC is added to the cart.
	 *
	 * @param cardqty
	 * @return true/false
	 */
	public static boolean isItemSkuLimitExceeded(int qtyLimit, long cardqty) {

		if (cardqty >= qtyLimit) {
			return true;
		}

		return false;
	}

	/**
	 *
	 * @param order
	 * @param limit
	 * @return true or false
	 */
	public static boolean isCartPurchaseLimitExceeded(Order order, double limit) {

		if (limit != -1 && order.getPriceInfo() != null) {
			if (order.getPriceInfo().getAmount() > limit) {
				return true;
			}
		}

		return false;
	}

	/**
	 *
	 * @param order
	 * @param qty
	 * @return true or false
	 */
	public static boolean isCartLineLimitExceeded(Order order, int qty) {

		if (qty != -1) {
			if (order.getCommerceItemCount() >= qty) {
				return true;
			}
		}

		return false;
	}

	/**
	 *
	 * @param order
	 * @param qty
	 * @return true or false
	 */
	public static boolean isCartLineLimitExceededForCheckout(Order order, int qty) {

		if (qty != -1) {
			if (order.getCommerceItemCount() > qty) {
				return true;
			}
		}

		return false;
	}

	/**
	 *
	 * @param order
	 * @param qty
	 * @return true or false
	 */
	public static boolean isCartSkuLimitExceeded(Order order, int qty) {

		@SuppressWarnings("unchecked")
		List<CommerceItem> commerceItems = order.getCommerceItems();

		if (qty != -1) {
			for (CommerceItem item : commerceItems) {
				if (item.getQuantity() > qty) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 *
	 * @return int
	 */
	public static int getNewOnClearanceDays() {
		int newOnClearanceDays = 30;
		Site site = getSite();
		if (null != site) {
			Object nOClD = site.getPropertyValue(SitePropertiesManager.getNewOnClearanceDaysPropertyName());
			newOnClearanceDays = (null != nOClD)
					? (Integer) nOClD
					: newOnClearanceDays;
			if (newOnClearanceDays == 0) {
				newOnClearanceDays = DEFAULT_NEW_ON_CLEARANCE_DAYS;
			}
		}
		return newOnClearanceDays;
	}

	/**
	 *
	 * @param siteId
	 * @return Site
	 */
	public static Site getSite(String siteId) {
		Site site = null;
		if (DigitalStringUtil.isBlank(siteId)) {
			DefaultSiteContextRuleFilter defaultSiteContextRuleFilter = ComponentLookupUtil
					.lookupComponent(ComponentLookupUtil.DEFAULT_SITE_RULE_FILTER, DefaultSiteContextRuleFilter.class);
			if (defaultSiteContextRuleFilter != null) {
				siteId = defaultSiteContextRuleFilter.getDefaultSiteId();
			}
		}
		if (DigitalStringUtil.isNotBlank(siteId)) {
			SiteContextManager siteContextManager = ComponentLookupUtil
					.lookupComponent(ComponentLookupUtil.SITE_CONTEXT, SiteContextManager.class);
			if (siteContextManager != null) {
				try {
					site = siteContextManager.getSite(siteId);
				} catch (SiteContextException e) {
					logger.error("Error Getting the Site Object for siteId :: " + siteId, e);
				}
			}
		}

		return site;
	}

	/**
	 *
	 * @param siteId
	 * @return true or false
	 */
	public static boolean setPushSiteContext(String siteId) {
		boolean pushed = false;
		if (DigitalStringUtil.isBlank(siteId)) {
			DefaultSiteContextRuleFilter defaultSiteContextRuleFilter = ComponentLookupUtil
					.lookupComponent(ComponentLookupUtil.DEFAULT_SITE_RULE_FILTER, DefaultSiteContextRuleFilter.class);
			if (defaultSiteContextRuleFilter != null) {
				siteId = defaultSiteContextRuleFilter.getDefaultSiteId();
			}
		}
		if (DigitalStringUtil.isNotBlank(siteId)) {
			SiteContextManager siteContextManager = ComponentLookupUtil
					.lookupComponent(ComponentLookupUtil.SITE_CONTEXT, SiteContextManager.class);
			if (siteContextManager != null) {
				SiteContext siteContext = null;
				try {
					siteContext = siteContextManager.getSiteContext(siteId);
					pushed = siteContextManager.pushSiteContext(siteContext);
					if (pushed) {
						logger.debug("Set the pushSite to siteId :: " + siteId);
					}
				} catch (SiteContextException e) {
					logger.error("Error Getting the Site Object for siteId :: " + siteId, e);
				}
			}
		}

		return pushed;
	}

	/**
	 *
	 * @param localeStr
	 */
	public static void setLocale(String localeStr) {
		if (DigitalStringUtil.isBlank(localeStr)) {
			return;
		}
		try {
			final DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			RequestLocale requestLocale = request.getRequestLocale();
			if (requestLocale != null) {
				Locale locale = requestLocale.getLocale();
				if (logger.isDebugEnabled()) {
					logger.debug("Current locale before resetting :: " + locale.getDisplayName());
				}
				requestLocale.setLocale(new Locale(localeStr));
				if (logger.isDebugEnabled()) {
					logger.debug("Resetting locale to :: " + localeStr);
					logger.debug("Current locale After resetting :: " + locale.getDisplayName());
				}
			}
		} catch (Exception ex) {
			logger.error("Error seting locale :: " + localeStr);
		}
	}

	/**
	 *
	 * @param pSiteId
	 * @return Map
	 */
	public static Map<String, Object> getShippingRates(String pSiteId) {
		String siteId = pSiteId;
		Map<String, Object> hsh = new HashMap<>();
		Properties shippingRates = new Properties();
		List<String> shippingMethods = new ArrayList<>();
		Site site = getSite();
		if (site == null) {
			if (DigitalStringUtil.isBlank(siteId)) {
				DefaultSiteContextRuleFilter defaultSiteContextRuleFilter = ComponentLookupUtil.lookupComponent(
						ComponentLookupUtil.DEFAULT_SITE_RULE_FILTER, DefaultSiteContextRuleFilter.class);
				if (defaultSiteContextRuleFilter != null) {
					siteId = defaultSiteContextRuleFilter.getDefaultSiteId();
				}
			}
			if (DigitalStringUtil.isNotBlank(siteId)) {
				SiteContextManager siteContextManager = ComponentLookupUtil
						.lookupComponent(ComponentLookupUtil.SITE_CONTEXT, SiteContextManager.class);
				if (siteContextManager != null) {
					try {
						site = siteContextManager.getSite(siteId);
					} catch (SiteContextException e) {
						logger.error("Error Getting the Site Object for siteId :: " + siteId, e);
					}
				}
			}
		}
		if (null != site) {
			Object sRS = site.getPropertyValue(SitePropertiesManager.getOrderShippingRatesPropertyName());
			String shippingRatesString = DigitalStringUtil
					.isEmpty((String) sRS)
					? DEFAULT_SHIPPING_RATES
					: (String) sRS;
			if (!DigitalStringUtil.isEmpty(shippingRatesString)) {
				StringTokenizer st = new StringTokenizer(shippingRatesString, ",");
				while (st.hasMoreElements()) {
					String object = (String) st.nextElement();
					String[] tokens = object.split("=");
					int tokenCount = tokens.length;
					if (tokenCount >= 2) {
						for (int j = 0; j < tokenCount; j = j + 2) {
							shippingRates.setProperty(tokens[j], tokens[j + 1]);
							shippingMethods.add(tokens[j]);
						}
					}
				}
			}
		}
		hsh.put(STATIC_SHIPPING_RATES, shippingRates);
		hsh.put(SHIPPING_METHODS, shippingMethods);

		return hsh;
	}

	/**
	 *
	 * @return String
	 */
	public static String getWebsiteLocationId() {
		String websiteLocationId = "FULLSITE";
		Site site = getSite();
		if (null != site) {
			Object wSLI  = site.getPropertyValue(SitePropertiesManager.getWebsiteLocationIdPropertyName());
			websiteLocationId = (null != wSLI)
					? (String)wSLI
					: websiteLocationId;
			if (DigitalStringUtil.isBlank(websiteLocationId)) {
				websiteLocationId = "FULLSITE";
			}
		}

		return websiteLocationId;
	}

	/**
	 *
	 * @param site
	 * @return String
	 */
	public static String getWebsiteLocationId(RepositoryItem site) {
		String websiteLocationId = "FULLSITE";
		if (null != site) {
			Object wsLI = site.getPropertyValue(SitePropertiesManager.getWebsiteLocationIdPropertyName());
			websiteLocationId = (null != wsLI)
					? (String) wsLI
					: websiteLocationId;
			if (DigitalStringUtil.isBlank(websiteLocationId)) {
				websiteLocationId = "FULLSITE";
			}
		}

		return websiteLocationId;
	}

	/**
	 *
	 * @return Map
	 */
	public static Map<String, String> getGWPConfiguration() {
		Map<String, String> gwpconfig = new HashMap<>();
		Site site = getSite();
		if (null != site) {
			Object gwp = site.getPropertyValue(SitePropertiesManager.getGwpConfigurationPropertyName());
			String gwpConfigurationString = (null != gwp)
					? (String) gwp
					: "";
			if (!DigitalStringUtil.isEmpty(gwpConfigurationString)) {
				StringTokenizer st = new StringTokenizer(gwpConfigurationString, ",");
				while (st.hasMoreElements()) {
					String object = (String) st.nextElement();
					String[] tokens = object.split("=");
					int tokenCount = tokens.length;
					if (tokenCount >= 2) {
						for (int j = 0; j < tokenCount; j = j + 2) {
							gwpconfig.put(tokens[j], tokens[j + 1]);
						}
					}
				}
			}
		}
		return gwpconfig;
	}

	/**
	 *
	 * @return int - maximum number of password retries allowed
	 */
	public static int getMaxForgotPasswordRetry() {
		int maxForgotPasswordRetry = 5;
		Site site = getSite();
		if (null != site) {
			Object mFPR = site
					.getPropertyValue(SitePropertiesManager.getMaxForgotPasswordRetryPropertyName());
			maxForgotPasswordRetry = (null != mFPR)
					? (Integer) mFPR
					: maxForgotPasswordRetry;
		}
		return maxForgotPasswordRetry;
	}

	/**
	 *
	 * @return List of derogatory words not allowed
	 */
	public static List<String> getExcludeWords() {
		String excludeWords = "(tits),(piss),(cocksucker),(bitch),(damn),(fuck),(motherfucker),(shit),(wtf),(cunt),(twat),(sucks),(whore),(whores),(bitches)";
		Site site = getSite();
		if (null != site) {
			Object exW = site.getPropertyValue(SitePropertiesManager.getExcludeWordsPropertyName());
			excludeWords = (null != exW) ? (String) exW : excludeWords;
		}
		{
			return (Lists.newArrayList(Splitter.on(',').omitEmptyStrings().trimResults().split(excludeWords)));
		}
	}

	/**
	 *
	 * @return int - maximum number of emails that can be used in shared
	 *         wishlist
	 */
	public static int getEmailSharedLimits() {
		int emailSharedLimits = 10;
		Site site = getSite();
		if (null != site) {
			Object eSL = site.getPropertyValue(SitePropertiesManager.getEmailSharedLimitsPropertyName());
			emailSharedLimits = (eSL != null
					? (Integer) eSL
					: emailSharedLimits);
		}
		return emailSharedLimits;
	}

	/**
	 *
	 * @return int - maximum message length allowed to be sent in shared
	 *         wishlist email
	 */
	public static int getMessageMaxLength() {
		int messageMaxLength = 1000;
		Site site = getSite();
		if (null != site) {
			Object mML = site.getPropertyValue(SitePropertiesManager.getMessageMaxLengthPropertyName());
			messageMaxLength = (mML != null)
					? (Integer)mML
					: messageMaxLength;
		}
		return messageMaxLength;
	}

	/**
	 *
	 * @return int
	 */
	public static int getRewardsThresholdAmount() {
		int rewardPointsThresholdAmount = 0;
		Site site = getSite();
		if (null != site) {
			Object rPTA =  site.getPropertyValue(
					SitePropertiesManager.getRewardPointsThresholdAmountPropertyName());

			rewardPointsThresholdAmount = rPTA != null
					? (Integer) rPTA
					: rewardPointsThresholdAmount;
		}
		return rewardPointsThresholdAmount;
	}

	/**
	 *
	 * @return double
	 */
	public static double getRewardsCertometerThreshold() {
		double rewardCertometerThresholdValue = 45;
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewardsCertometerThresholdPropertyName());

			rewardCertometerThresholdValue = value != null? ((Double)value).doubleValue()
					: rewardCertometerThresholdValue;
		}
		return rewardCertometerThresholdValue;
	}

	/**
	 *
	 * @return double
	 */
	public static double getRewardsTierometerCTGThreshold() {
		double rewardTierometerCTGThresholdValue = 30;
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewardsTierometerCTGThresholdPropertyName());

			rewardTierometerCTGThresholdValue = value != null? ((Double)value).doubleValue()
					: rewardTierometerCTGThresholdValue;
		}
		return rewardTierometerCTGThresholdValue;
	}

	/**
	 *
	 * @return double
	 */
	public static double getRewardsTierometerGTEThreshold() {
		double rewardTierometerGTEThresholdValue = 30;
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewardsTierometerGTEThresholdPropertyName());

			rewardTierometerGTEThresholdValue = value != null? ((Double)value).doubleValue()
					: rewardTierometerGTEThresholdValue;
		}
		return rewardTierometerGTEThresholdValue;
	}

	/**
	 *
	 * @return true of false
	 */
	public static boolean isSendEmailOnCreateAccount() {
		boolean sendEmailOnCreateAccount = false;
		Site site = getSite();
		if (null != site) {
			Object sMCA  = site
					.getPropertyValue(SitePropertiesManager.getEmailCreateAccountPropertyName());
			if (sMCA != null) {
				sendEmailOnCreateAccount = (Boolean) sMCA;
			}
		}
		return sendEmailOnCreateAccount;
	}

	/**
	 *
	 * @return String
	 */
	public static String getDefaultLocale() {
		RequestLocale requestLocale = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.REQUEST_LOCALE,
				RequestLocale.class);
		String localeName = "en_US";
		if (requestLocale != null) {
			requestLocale.getDefaultRequestLocaleName();
		}
		return localeName;
	}

	/**
	 *
	 * @return String
	 */
	public static String getRepricingClubProfileId() {
		String repriceClubProfileId = "2149867544";
		Site site = getSite();
		if (null != site) {
			Object rBP = site
					.getPropertyValue(SitePropertiesManager.getRepriceClubProfileIdPropertyName());
			if (rBP != null) {
				repriceClubProfileId = (String) rBP;
			}
		}
		return repriceClubProfileId;
	}

	/**
	 *
	 * @return String
	 */
	public static String getRepricingGoldProfileId() {
		String repriceGoldProfileId = "2149867553";
		Site site = getSite();
		if (null != site) {
			Object rBP = site
					.getPropertyValue(SitePropertiesManager.getRepriceGoldProfileIdPropertyName());
			if (rBP != null) {
				repriceGoldProfileId = (String) rBP;
			}
		}
		return repriceGoldProfileId;
	}

	/**
	 *
	 * @return String
	 */
	public static String getRepricingEliteProfileId() {
		String repriceEliteProfileId = "2149867428";
		Site site = getSite();
		if (null != site) {
			Object rBP = site
					.getPropertyValue(SitePropertiesManager.getRepriceEliteProfileIdPropertyName());
			if (rBP != null) {
				repriceEliteProfileId = (String) rBP;
			}
		}
		return repriceEliteProfileId;
	}

	/**
	 *
	 * @return String
	 */
	public static String getRepricingBasicProfileId() {
		String repriceBasicProfileId = "800394103";
		Site site = getSite();
		if (null != site) {
			Object rBP = site
					.getPropertyValue(SitePropertiesManager.getRepriceBasicProfileIdPropertyName());
			if (rBP != null) {
				repriceBasicProfileId = (String) rBP;
			}
		}
		return repriceBasicProfileId;
	}

	/**
	 *
	 * @return String
	 */
	public static String getRepricingPrimProfileId() {
		String repricePrimProfileId = "800394105";
		Site site = getSite();
		if (null != site) {
			Object rPPI  = site
					.getPropertyValue(SitePropertiesManager.getRepricePrimProfileIdPropertyName());
			if (rPPI != null) {
				repricePrimProfileId = (String)rPPI ;
			}
		}
		return repricePrimProfileId;
	}

	/**
	 *
	 * @return String
	 */
	public static String getRepricingAnonyProfileId() {
		String repriceAnonyProfileId = "800250909";
		Site site = getSite();
		if (null != site) {
			Object rAPp = site
					.getPropertyValue(SitePropertiesManager.getRepriceAnonyProfileIdPropertyName());
			if (rAPp != null) {
				repriceAnonyProfileId = (String) rAPp;
			}
		}
		return repriceAnonyProfileId;
	}

	/**
	 *
	 * @return String
	 */
	public static String getSiteURL() {
		String siteURL = "https://www.dsw.com/en/us/";
		Site site = getSite();
		if (null != site) {
			Object sURL = site.getPropertyValue(SitePropertiesManager.getSiteURLPropertyName());
			if (sURL != null) {
				siteURL = (String) sURL;
			}
		}
		return siteURL;
	}

	/**
	 *
	 * @return String
	 */
	public static String getShareWLURI() {
		String shareWLURI = "shared-favorites/";
		Site site = getSite();
		if (null != site) {
			Object sWLURI  = site.getPropertyValue(SitePropertiesManager.getShareWLURIPropertyName());
			if (sWLURI != null) {
				shareWLURI = (String) sWLURI;
			}
		}
		return shareWLURI;
	}

	/**
	 *
	 * @return String
	 */
	public static String getForgotPasswordURI() {
		String forgotPasswordURI = "sign-in?forgotPass=1&passwordReset=1";  //NOSONAR
		Site site = getSite();
		if (null != site) {
			Object fPURI = site
					.getPropertyValue(SitePropertiesManager.getForgotPasswordURIPropertyName());
			if (fPURI != null) {
				forgotPasswordURI = (String)fPURI ;
			}
		}
		return forgotPasswordURI;
	}

	public static String getCreateAccountURI() {
		String createAccountURI = "landing?type=ca&email=";  //NOSONAR
		Site site = getSite();
		if (null != site) {
			Object fPURI = site
					.getPropertyValue(SitePropertiesManager.getCreateAccountURIPropertyName());
			if (fPURI != null) {
				createAccountURI = (String)fPURI ;
			}
		}
		return createAccountURI;
	}

	/**
	 *
	 * @return String
	 */
	public static String getStoreLocatorURI() {
		String storeLocatorURI = "?modal=fs";
		Site site = getSite();
		if (null != site) {
			Object sLURI = site
					.getPropertyValue(SitePropertiesManager.getStoreLocatorURIPropertyName());
			if (sLURI != null) {
				storeLocatorURI = (String) sLURI;
			}
		}
		return storeLocatorURI;
	}

	/**
	 *
	 * @return String
	 */
	public static String getSignInURI() {
		String signInURI = "sign-in";
		Site site = getSite();
		if (null != site) {
			Object sIURL =  site.getPropertyValue(SitePropertiesManager.getSignInURIPropertyName());
			if (sIURL != null) {
				signInURI = (String) sIURL;
			}
		}
		return signInURI;
	}

	/**
	 *
	 * @return String
	 */
	public static String getEmailSignUpURI() {
		String emailSignUpURI = "#signup";
		Site site = getSite();
		if (null != site) {
			Object emaSignURL  = site.getPropertyValue(SitePropertiesManager.getEmailSignUpURIPropertyName());
			if (emaSignURL != null) {
				emailSignUpURI = (String)emaSignURL ;
			}
		}
		return emailSignUpURI;
	}

	/**
	 *
	 * @return int - maximum number of credit cards allowed to be saved on a
	 *         profile
	 */
	public static int getCreditCardMaxLimit() {
		int creditCardMaxLimit = 30;
		Site site = getSite();
		if (null != site) {
			Object cCMaL =  site
					.getPropertyValue(SitePropertiesManager.getCreditCardMaxLimitPropertyName());
			if (cCMaL != null) {
				creditCardMaxLimit = (Integer) cCMaL;
			}
		}
		return creditCardMaxLimit;
	}

	/**
	 *
	 * @return int - maximum number of shipping address allowed to be saved on a
	 *         profile
	 */
	public static int getShippingAddressMaxLimit() {
		int shippingAddressMaxLimit = 30;
		Site site = getSite();
		if (null != site) {
			Object sAML = site
					.getPropertyValue(SitePropertiesManager.getShippingAddressMaxLimitPropertyName());
			if (sAML != null) {
				shippingAddressMaxLimit = (Integer)sAML ;
			}
		}
		return shippingAddressMaxLimit;
	}

	/**
	 *
	 * @return int
	 */
	public static int getRewardsCertExpiringSoonDays() {
		int rewardsCertExpiringSoonDays = 30;
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewardsCertExpiringSoonDaysPropertyName());

			rewardsCertExpiringSoonDays = value != null? (Integer)value : rewardsCertExpiringSoonDays;
		}
		return rewardsCertExpiringSoonDays;
	}

	/**
	 *
	 * @return Map<String, String>
	 */
	public static Map<String, String> getRewardsRibbonOrder() {
		Map<String, String> rewardsRibbonOrder = new HashMap<>();
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewardsRibbonOrderPropertyName());

			String rewardsRibbonOrderStr = value != null? (String)value : null;
			if(DigitalStringUtil.isNotBlank(rewardsRibbonOrderStr)){
				String[] ribbons = DigitalStringUtil.split(rewardsRibbonOrderStr,",");
				for(String ribbon: ribbons){
					String[] ribbonPair = DigitalStringUtil.split(ribbon,"=");
					if(ribbonPair != null && ribbonPair.length == 2) {
						rewardsRibbonOrder.put(ribbonPair[0], ribbonPair[1]);
					}
				}
			}

		}
		return rewardsRibbonOrder;
	}

	/**
	 *
	 * @return int
	 */
	public static int getRewardsGoldTierQualifierDollar() {
		int rewardsGoldTierQualifierDollar = 200;
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewardsGoldTierQualifierDollarPropertyName());

			rewardsGoldTierQualifierDollar = value != null? (Integer)value : rewardsGoldTierQualifierDollar;
		}
		return rewardsGoldTierQualifierDollar;
	}

	/**
	 *
	 * @return int
	 */
	public static int getRewardsEliteTierQualifierDollar() {
		int rewardsEliteTierQualifierDollar = 500;
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewardsGoldTierQualifierDollarPropertyName());

			rewardsEliteTierQualifierDollar = value != null? (Integer)value : rewardsEliteTierQualifierDollar;
		}
		return rewardsEliteTierQualifierDollar;
	}

	public static List<String> getRewards2XPointsOneDiscountCode() {
		List<String> rewards2XPointsOneDiscountCode = new ArrayList<>();
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewards2XPointsOneDiscountCodePropertyName());

			String rewards2XPointsOneDiscountCodeStr = value != null? (String)value : null;
			if(DigitalStringUtil.isNotBlank(rewards2XPointsOneDiscountCodeStr)){
				String[]  doublePointsCodes = DigitalStringUtil.split(rewards2XPointsOneDiscountCodeStr, ",");
				rewards2XPointsOneDiscountCode = Arrays.asList(doublePointsCodes);
			}
		}
		return rewards2XPointsOneDiscountCode;
	}

	public static List<String> getRewards2XPointsTwoDiscountCode() {
		List<String> rewards2XPointsTwoDiscountCode = new ArrayList<>();
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewards2XPointsTwoDiscountCodePropertyName());

			String rewards2XPointsDiscountCodeStr = value != null? (String)value : null;
			if(DigitalStringUtil.isNotBlank(rewards2XPointsDiscountCodeStr)){
				String[]  doublePointsCodes = DigitalStringUtil.split(rewards2XPointsDiscountCodeStr, ",");
				rewards2XPointsTwoDiscountCode = Arrays.asList(doublePointsCodes);
			}
		}
		return rewards2XPointsTwoDiscountCode;
	}

	public static List<String> getRewards3XPointsDiscountCode() {
		List<String> rewards3XPointsDiscountCode = new ArrayList<>();
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewards3XPointsDiscountCodePropertyName());

			String rewards3XPointsDiscountCodeStr = value != null? (String)value : null;
			if(DigitalStringUtil.isNotBlank(rewards3XPointsDiscountCodeStr)){
				String[]  pointsCodes = DigitalStringUtil.split(rewards3XPointsDiscountCodeStr, ",");
				rewards3XPointsDiscountCode = Arrays.asList(pointsCodes);
			}
		}
		return rewards3XPointsDiscountCode;
	}

	public static List<String> getRewardsBirthdayClubDiscountCode() {
		List<String> rewardsBirthdayClubDiscountCode = new ArrayList<>();
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewardsBirthdayClubDiscountCodePropertyName());

			String siteRewardsBirthdayClubDiscountCode = value != null? (String)value : null;
			if(DigitalStringUtil.isNotBlank(siteRewardsBirthdayClubDiscountCode)){
				String[]  codes = DigitalStringUtil.split(siteRewardsBirthdayClubDiscountCode, ",");
				rewardsBirthdayClubDiscountCode = Arrays.asList(codes);
			}

		}
		return rewardsBirthdayClubDiscountCode;
	}

	public static List<String> getRewardsBirthdayGoldDiscountCode() {
		List<String> rewardsBirthdayGoldDiscountCode = new ArrayList<>();
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewardsBirthdayGoldDiscountCodePropertyName());

			String siteRewardsBirthdayGoldDiscountCode = value != null? (String)value : null;
			if(DigitalStringUtil.isNotBlank(siteRewardsBirthdayGoldDiscountCode)){
				String[]  codes = DigitalStringUtil.split(siteRewardsBirthdayGoldDiscountCode, ",");
				rewardsBirthdayGoldDiscountCode = Arrays.asList(codes);
			}

		}
		return rewardsBirthdayGoldDiscountCode;
	}

	public static List<String> getRewardsBirthdayEliteDiscountCode() {
		List<String> rewardsBirthdayEliteDiscountCode = new ArrayList<>();
		Site site = getSite();
		if (null != site) {
			Object value =  site.getPropertyValue(
					SitePropertiesManager.getRewardsBirthdayEliteDiscountCodePropertyName());

			String siteRewardsBirthdayEliteDiscountCode = value != null? (String)value : null;
			if(DigitalStringUtil.isNotBlank(siteRewardsBirthdayEliteDiscountCode)){
				String[]  codes = DigitalStringUtil.split(siteRewardsBirthdayEliteDiscountCode, ",");
				rewardsBirthdayEliteDiscountCode = Arrays.asList(codes);
			}

		}
		return rewardsBirthdayEliteDiscountCode;
	}
}
