package com.digital.commerce.common.config;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

@Getter
@Setter
public class DigitalPropertyDataSource {
	
	private HashMap<String,String> data = new HashMap<>();

}
