package com.digital.commerce.common.services.inventory;

import java.util.List;

import com.digital.commerce.common.services.DigitalBaseConstants;

import atg.commerce.inventory.InventoryManager;
import atg.commerce.inventory.RepositoryInventoryManager;
import atg.commerce.order.CommerceItem;

public interface InventoryHelper {

	int STOCK_LEVEL_INFINITE = -1;

	/** Returns the value to use for representing stock level infinite.
	 * 
	 * @return */
	int getStockLevelInfinite();

	/** Returns whether or not the sku is available to be ordered. It is either infinite availability, in stock, or on backorder.
	 * 
	 * @param sku
	 * @return */
	boolean isAvailable(InventoryManager inventoryManager, String sku);

	/** Returns whether or not the sku is available to be ordered in the given quantity.
	 * It is either infinite availability, in stock, or on backorder.
	 * 
	 * @param sku
	 * @param quantity
	 * @return */
	boolean isAnyAvailable(String sku, long quantity);

	/** Returns whether or not the sku is available to be ordered. It is either infinite availability, in stock, or on backorder.
	 * 
	 * @param sku
	 * @return */
	boolean isAnyAvailable(List<String> skus);

	/** Returns whether or not the sku is available to be ordered in the given quantity.
	 * It is either infinite availability, in stock, or on backorder.
	 * 
	 * @param sku
	 * @param quantity
	 * @return */
	boolean isAvailable(String sku, long quantity);

	/** Returns whether or not the sku is available to be ordered. It is either infinite availability, in stock, or on backorder.
	 * 
	 * @param sku
	 * @return */
	boolean isAvailable(String sku);

	/** Returns whether or not the commerceItem is available to be ordered in the given quantity.
	 * It is either infinite availability, in stock, or on backorder.
	 * 
	 * @param commerceItem
	 * @param quantity
	 * @return */
	boolean isAvailable(CommerceItem commerceItem, long quantity);

	/** Returns whether or not the commerceItem is available to be ordered. It is either infinite availability, in stock, or on backorder.
	 * 
	 * @param commerceItem
	 * @return */
	boolean isAvailable(CommerceItem commerceItem);

	/** Returns whether or not the sku is available to be ordered in the given quantity.
	 * It is either infinite availability, in stock, or on backorder.
	 * 
	 * @param sku
	 * @param quantity
	 * @return */
	boolean isAvailable(InventoryManager inventoryManager, CommerceItem commerceItem, long quantity);

	/** Returns whether or not the sku is available to be ordered in the given quantity.
	 * It is either infinite availability, in stock, or on backorder.
	 * 
	 * @param sku
	 * @param quantity
	 * @return */
	boolean isAvailable(InventoryManager inventoryManager, String skuId, long quantity);

	/**
	 * Overloaded method to return different status messages (instead of a boolean). 
	 * @param sku
	 * @param quantity
	 * @return Inventory Status 
	 * */
	int isAvailableWithCheckforExists(String skuId);

	/** Returns true if the skuId is unaccountable (e.g., gift cards).
	 * 
	 * @param skuId
	 * @return */
	boolean isUnaccountable(String skuId);

	/** Returns true if the commerceItem refers to a sku that is unaccountable (e.g., gift cards).
	 * 
	 * @param commerceItem
	 * @return */
	boolean isUnaccountable(CommerceItem commerceItem);

	/** Returns the max inventory that can be requested looking at current stock levels.
	 * 
	 * @param skuId
	 * @param quantity
	 * @return the max inventory that can be requested looking at current stock levels. */
	long getMaxRequestableInventory(String skuId, long quantity);

	/** Returns the max inventory that can be requested looking at current stock levels.
	 * 
	 * @param skuId
	 * @param quantity
	 * @return the max inventory that can be requested looking at current stock levels. */
	long getMaxRequestableInventory(InventoryManager inventoryManager, String skuId, long quantity);

	/** Returns one of the inventory status codes {@link InventoryManager#AVAILABILITY_STATUS_IN_STOCK},
	 * {@link InventoryManager#AVAILABILITY_STATUS_OUT_OF_STOCK}, {@link InventoryManager#AVAILABILITY_STATUS_BACKORDERABLE}.
	 * Note that an inventory less than {@link STOCK_LEVEL_INFINITE} is an infinite inventory per
	 * {@link RepositoryInventoryManager#decrementSKULevel(atg.repository.MutableRepositoryItem, long, String, String)}.
	 * 
	 * @param inventoryManager
	 * @param skuId
	 * @return */
	int getInventoryStatus(String skuId);
	
	/** Returns one of the inventory status codes {@link InventoryManager#AVAILABILITY_STATUS_IN_STOCK},
	 * {@link InventoryManager#AVAILABILITY_STATUS_OUT_OF_STOCK}, {@link InventoryManager#AVAILABILITY_STATUS_BACKORDERABLE}.
	 * Note that an inventory less than {@link STOCK_LEVEL_INFINITE} is an infinite inventory per
	 * {@link RepositoryInventoryManager#decrementSKULevel(atg.repository.MutableRepositoryItem, long, String, String)}.
	 * 
	 * @param inventoryManager
	 * @param skuId
	 * @param pLocationId
	 * @return */
	int getInventoryStatus( String skuId, String pLocationId );
	
	/** Returns the max inventory that can be requested looking at current stock levels.
	 * 
	 * @param skuId
	 * @param quantity
	 * @param pLocationId
	 * @return the max inventory that can be requested looking at current stock levels. */
	int getInventoryStatus( RepositoryInventoryManager inventoryManager, String skuId, long quantity, String pLocationId );

	/** Returns one of the inventory status codes {@link InventoryManager#AVAILABILITY_STATUS_IN_STOCK},
	 * {@link InventoryManager#AVAILABILITY_STATUS_OUT_OF_STOCK}, {@link InventoryManager#AVAILABILITY_STATUS_BACKORDERABLE}.
	 * Note that an inventory less than {@link STOCK_LEVEL_INFINITE} is an infinite inventory per
	 * {@link RepositoryInventoryManager#decrementSKULevel(atg.repository.MutableRepositoryItem, long, String, String)}.
	 * 
	 * @param inventoryManager
	 * @param skuId
	 * @param quantity
	 * @return */
	int getInventoryStatus(String skuId, long quantity);

	/** Returns one of the inventory status codes {@link InventoryManager#AVAILABILITY_STATUS_IN_STOCK},
	 * {@link InventoryManager#AVAILABILITY_STATUS_OUT_OF_STOCK}, {@link InventoryManager#AVAILABILITY_STATUS_BACKORDERABLE}.
	 * Note that an inventory less than {@link STOCK_LEVEL_INFINITE} is an infinite inventory per
	 * {@link RepositoryInventoryManager#decrementSKULevel(atg.repository.MutableRepositoryItem, long, String, String)}.
	 * 
	 * @param skuId
	 * @param quantity
	 * @return */
	int getInventoryStatus(InventoryManager inventoryManager, String skuId, long quantity);

	int guessQtyReserveAtStoreHub(String skuId, int quantity);

	int guessQtyReserveAtDropship(String skuId, int quantity);

	boolean isInventoryRecordExists(String skuId);

	InventoryManager getInventoryManager();

	DigitalBaseConstants getDigitalConstants();

}