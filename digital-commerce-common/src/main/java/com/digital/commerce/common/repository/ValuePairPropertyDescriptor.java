/* <ATGCOPYRIGHT>
 * Copyright (C) 1997-2004 Art Technology Group, Inc.
 * All Rights Reserved. No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from Art Technology Group. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 * Art Technology Group (ATG) MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. ATG SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 * Dynamo is a trademark of Art Technology Group, Inc.
 * </ATGCOPYRIGHT> */

package com.digital.commerce.common.repository;

import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

/** This is a simple read-only property descriptor which concatinates the
 * 'code' and 'value' properties resulting in the form key=value, where = is the SEPERATOR
 * Should either of the constituant properties be null it will be replaced by an empty string.
 * 
 * 
*/
@SuppressWarnings({"rawtypes"})
public class ValuePairPropertyDescriptor extends RepositoryPropertyDescriptor {

	private static final long	serialVersionUID	= -8363538404027289647L;

	// -------------------------------------
	/** Attribute names */
	static final String			SEPARATOR_PROPERTY	= "separator";
	static final String			KEY_PROPERTY		= "key";
	static final String			VALUE_PROPERTY		= "value";
	static final String			TYPE_NAME			= "ValuePair";
	/** Values of these attributes for this particular property */
	String						mSeparatorProperty	= null;
	String						mKeyProperty		= null;
	String						mValueProperty		= null;

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, ValuePairPropertyDescriptor.class );
	}

	// -------------------------------------
	/** Constructs a new ValuePairPropertyDescriptor **/
	public ValuePairPropertyDescriptor() {
		super();
	}

	// -------------------------------------
	/** Constructs a new ValuePairPropertyDescriptor for a particular property. **/
	public ValuePairPropertyDescriptor( String pPropertyName ) {
		super( pPropertyName );
	}

	// -------------------------------------
	/** Constructs a new RepositoryPropertyDescriptor with the given
	 * property name, property type, and short description. **/
	public ValuePairPropertyDescriptor( String pPropertyName, Class pPropertyType, String pShortDescription ) {
		super( pPropertyName, pPropertyType, pShortDescription );
	}

	// -------------------------------------
	/** Returns property Queryable **/
	public boolean isQueryable() {
		return false;
	}

	// -------------------------------------
	/** Returns property Writable. */
	public boolean isWritable() {
		return false;
	}

	// -------------------------------------
	/** This method is called to retrieve a read-only value for this property.
	 * 
	 * Once a repository has computed the value it would like to return for
	 * this property, this property descriptor gets a chance to modify it
	 * based on how the property is defined. For example, if null is to
	 * be returned, we return the default value. */
	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		String key = "";

		String val = "";

		if( mKeyProperty != null )
			key = (String)pItem.getPropertyValue( mKeyProperty );
		else
			throw new IllegalArgumentException( "No 'key' attribute set for property type: 'valuePair'" );
		if( mValueProperty != null )
			val = (String)pItem.getPropertyValue( mValueProperty );
		else
			throw new IllegalArgumentException( "No 'value' attribute set for property type: 'valuePair'" );
		if( mSeparatorProperty == null ) mSeparatorProperty = "=";
		return key + mSeparatorProperty + val;

	}

	/** Catch the attribute values that we care about and store them in
	 * member variables. */
	public void setValue( String pAttributeName, Object pValue ) {
		super.setValue( pAttributeName, pValue );

		if( pValue == null || pAttributeName == null ) return;
		if( pAttributeName.equalsIgnoreCase( SEPARATOR_PROPERTY ) ) mSeparatorProperty = pValue.toString();
		if( pAttributeName.equalsIgnoreCase( KEY_PROPERTY ) ) mKeyProperty = pValue.toString();
		if( pAttributeName.equalsIgnoreCase( VALUE_PROPERTY ) ) mValueProperty = pValue.toString();
	}

	/** Returns the name this type uses in the XML file. */
	public String getTypeName() {
		return TYPE_NAME;
	}

	public Class getPropertyType() {
		return String.class;
	}

	/** Perform type checking. */
	public void setPropertyType( Class pClass ) {
		if( pClass != String.class ) throw new IllegalArgumentException( "valuePair properties must be String" );
		super.setPropertyType( pClass );
	}

	public void setComponentPropertyType( Class pClass ) {
		if( pClass != null ) throw new IllegalArgumentException( "valuePair properties must be scalars" );
	}

	public void setPropertyItemDescriptor( RepositoryItemDescriptor pDesc ) {
		if( pDesc != null ) throw new IllegalArgumentException( "valuePair properties must be String" );
	}

	public void setComponentItemDescriptor( RepositoryItemDescriptor pDesc ) {
		if( pDesc != null ) throw new IllegalArgumentException( "valuePair properties must be scalars" );
	}

}
