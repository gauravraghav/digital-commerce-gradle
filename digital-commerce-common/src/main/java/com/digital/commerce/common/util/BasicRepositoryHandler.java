package com.digital.commerce.common.util;

import com.digital.commerce.common.exception.DataAccessException;

import atg.adapter.gsa.query.Builder;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasicRepositoryHandler implements IRepositoryHandler {
	private Repository	repository;

	@Override
	public MutableRepositoryItem add( String itemDescriptor, SaveCallback<MutableRepositoryItem> callback ) throws DataAccessException {
		MutableRepository mutableRepository = getMutableRepositoryChecked();
		try {
			MutableRepositoryItem item = mutableRepository.createItem( itemDescriptor );
			callback.save( item );
			mutableRepository.addItem( item );
			return item;
		} catch( RepositoryException e ) {
			throw new DataAccessException( String.format( "Unable to add item of type {}", itemDescriptor ), e );
		}
	}

	@Override
	public MutableRepositoryItem save( String id, String itemDescriptor, SaveCallback<MutableRepositoryItem> callback ) throws DataAccessException {
		MutableRepository mutableRepository = getMutableRepositoryChecked();
		try {
			MutableRepositoryItem item = mutableRepository.getItemForUpdate( id, itemDescriptor );
			callback.save( item );
			mutableRepository.updateItem( item );
			return item;
		} catch( RepositoryException e ) {
			throw new DataAccessException( String.format( "Unable to save item %s of type %s", id, itemDescriptor ), e );
		}
	}

	@Override
	public void remove( String id, String itemDescriptor ) throws DataAccessException {
		MutableRepository mutableRepository = getMutableRepositoryChecked();
		try {
			mutableRepository.removeItem( id, itemDescriptor );
		} catch( RepositoryException e ) {
			throw new DataAccessException( String.format( "Unable to remove item of type {}", itemDescriptor ), e );
		}
	}

	@Override
	public RepositoryItem[] findByRQL( String rql, String itemDescriptor ) throws DataAccessException {
		return findByRQL( rql, itemDescriptor, (Object)null );
	}

	@Override
	public RepositoryItem findOneByRQL( String rql, String itemDescriptor ) throws DataAccessException {
		return findOneByRQL( rql, itemDescriptor, (Object)null );
	}

	@Override
	public RepositoryItem findOneByRQL( String rql, String itemDescriptor, Object... params ) throws DataAccessException {
		RepositoryItem[] retVal = findByRQL( rql, itemDescriptor, params );
		return ( retVal != null && retVal.length > 0 ) ? retVal[0] : null;
	}

	@Override
	public RepositoryItem[] findByRQL( String rql, String itemDescriptor, Object... params ) throws DataAccessException {
		try {
			RepositoryView view = getViewChecked( itemDescriptor );
			RqlStatement statement = RqlStatement.parseRqlStatement( rql );
			return statement.executeQuery( view, params );
		} catch( Exception ex ) {
			throw new DataAccessException( ex );
		}
	}

	private RepositoryView getViewChecked( String itemDescriptor ) throws RepositoryException {
		RepositoryView view = repository.getView( itemDescriptor );
		if( view == null ) { throw new DataAccessException( String.format( "%s is not a valid item descriptor", itemDescriptor ) ); }
		return view;
	}

	private MutableRepository getMutableRepositoryChecked() {
		if( !( repository instanceof MutableRepository ) ) { throw new DataAccessException( "Repository is not mutable for %s" ); }
		return (MutableRepository)repository;
	}

	@Override
	public RepositoryItem findById( String id, String itemDescriptor ) throws DataAccessException {
		try {
			return repository.getItem( id, itemDescriptor );
		} catch( Exception ex ) {
			throw new DataAccessException( ex );
		}
	}

	@Override
	public RepositoryItem[] findBySQL( String sql, String itemDescriptor ) throws DataAccessException {
		try {
			RepositoryView view = getViewChecked( itemDescriptor );
			Builder builder = (Builder)view.getQueryBuilder();
			return view.executeQuery( builder.createSqlPassthroughQuery( sql, null ) );
		} catch( Exception ex ) {
			throw new DataAccessException( ex );
		}
	}

	@Override
	public RepositoryItem findOneBySQL( String rql, String itemDescriptor ) throws DataAccessException {
		RepositoryItem[] retVal = findBySQL( rql, itemDescriptor );
		return ( retVal != null && retVal.length > 0 ) ? retVal[0] : null;
	}

	@Override
	public RepositoryItem[] findBySQL( String sql, String itemDescriptor, int maxResults ) throws DataAccessException {
		return findBySQL( sql, itemDescriptor, 0, maxResults );
	}

	@Override
	public RepositoryItem[] findBySQL( String sql, String itemDescriptor, int startFrom, int maxResults ) throws DataAccessException {
		RepositoryItem[] retVal = null;
		RepositoryItem[] results = findBySQL( sql, itemDescriptor );
		if( results != null ) {
			retVal = new RepositoryItem[results.length - startFrom > maxResults ? maxResults : results.length - startFrom];
			System.arraycopy( results, startFrom, retVal, 0, retVal.length );
		}
		return retVal;
	}

	/*
	@Override
	public void executeSQL( String sql ) throws DataAccessException {
		if( !( repository instanceof GSARepository ) ) { throw new DataAccessException( String.format( "Attempt to execute SQL on a non-GSA repository %s", repository.getRepositoryName() ) ); }
		GSARepository repo = (GSARepository)repository;
		DataSource datasource = repo.getDataSource();

		Connection connection = null;
		Statement statement = null;
		try {
			connection = datasource.getConnection();
			statement = connection.createStatement();
			statement.execute( sql );
		} catch( Exception ex ) {
			throw new DataAccessException( "Failed to execute SQL: \"" + sql + "\"", ex );
		} finally {
			JdbcUtils.closeStatement( statement );
			JdbcUtils.closeConnection( connection );
		}
	}
*/

}
