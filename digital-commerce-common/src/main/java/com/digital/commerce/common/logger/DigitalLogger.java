package com.digital.commerce.common.logger;

import javax.servlet.http.Cookie;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

/**
 * This class is a Logging wrapper to add context to every log message like JSESSIONID, APACHE ID, PROFILE_ID, CLIENT_SOURCE_IP etc
 * @author MK402314
 *
 */
public class DigitalLogger{
	
	private Logger logger;
	
	private static final String				SESSION_ID_TXT_PREFIX	= " JSESSIONID:";
	private static final String				COMMA_DELIMITER			= ", ";
	private static final String 			UNIQUE_ID_KEY 			= "UNIQUE_ID";
	private static final String 			APACHE_UNIQUE_ID_PREFIX = "APACHE_UNIQUE_ID:";
	private static final String 			CLIENT_SOURCE_IP_KEY	= "Client-Source-IP";
	private static final String 			CLIENT_SOURCE_IP_PREFIX	= "Client-Source-IP:";
	private static final String 			SOURCE_IP_KEY			= "SOURCE_IP";
	private static final String 			SOURCE_IP_KEY_PREFIX	= "SOURCE_IP:";
	private static final String 			PROFILE_ID				= "DYN_USER_ID";
	private static final String 			PROFILE_ID_PREFIX		= "PROFILE_ID:";
	private static final String 			JSESSION_ID 			= "JSESSIONID";
	private static final String NEW_LINE_SEPARATOR = System.getProperty("line.separator");

	public static DigitalLogger getLogger(@SuppressWarnings("rawtypes") Class clazz){
		return DigitalLogger.getLogger(clazz.getName());
	}
	
	public static DigitalLogger getLogger(String name){
		Logger logger = LogManager.getLogger(name);
		return new DigitalLogger(logger);
	}
	
	/**
	 * @param pLogger
	 */
	public DigitalLogger(Logger pLogger) {
		logger = pLogger;
	}

	public void debug(Object message){
		if(logger.isDebugEnabled()){
			StringBuilder sb = this.getLogContext();
			if(null != sb){
				logger.debug(sb.append(message).toString());
			}else{
				logger.debug(message);
			}
		}
	}

	public void debug(Object message, Throwable t){
		if(logger.isDebugEnabled()){
			StringBuilder sb = this.getLogContext();
			String shortStacKtrace = getShortStackTrace(t);
			if(null != sb){
				logger.debug(sb.append(message).toString() + shortStacKtrace);
			}else{
				logger.debug(message + shortStacKtrace);
			}
		}
	}
	
	public void info(Object message){
		if(logger.isInfoEnabled()){
			StringBuilder sb = this.getLogContext();
			if(null != sb){
				logger.info(sb.append(message).toString());
			}else{
				logger.info(message);
			}
		}
	}
	
	public void info(Object message, Throwable t){
		if(logger.isInfoEnabled()){
			StringBuilder sb = this.getLogContext();
			String shortStacKtrace = getShortStackTrace(t);
			if(null != sb){
				logger.info(sb.append(message).toString() + shortStacKtrace);
			}else{
				logger.info(message + shortStacKtrace);
			}
		}
	}
	
	public void warn(Object message){
		if(logger.isWarnEnabled()){
			StringBuilder sb = this.getLogContext();
			if(null != sb){
				logger.warn(sb.append(message).toString());
			}else{
				logger.warn(message);
			}
		}
	}
	
	public void warn(Object message, Throwable t){
		if(logger.isWarnEnabled()){
			StringBuilder sb = this.getLogContext();
			String shortStacKtrace = getShortStackTrace(t);
			if(null != sb){
				logger.warn(sb.append(message).toString() + shortStacKtrace);
			}else{
				logger.warn(message + shortStacKtrace);
			}
		}
	}
	
	public void error(Object message){
		if(logger.isErrorEnabled()){
			StringBuilder sb = this.getLogContext();
			if(null != sb){
				logger.error(sb.append(message).toString());
			}else{
				logger.error(message);
			}
		}
	}
	
	public void error(Object message, Throwable t){
		if(logger.isErrorEnabled()){
			StringBuilder sb = this.getLogContext();
			String shortStacKtrace = getShortStackTrace(t);
			if(null != sb){
				logger.error(sb.append(message).toString() + shortStacKtrace);
			}else{
				logger.error(message + shortStacKtrace);
			}
		}
	}
	
	public void fatal(Object message){
		if(logger.isErrorEnabled()){
			StringBuilder sb = this.getLogContext();
			if(null != sb){
				logger.fatal(sb.append(message).toString());
			}else{
				logger.fatal(message);
			}
		}
	}
	
	public void fatal(Object message, Throwable t){
		if(logger.isErrorEnabled()){
			StringBuilder sb = this.getLogContext();
			String shortStacKtrace = getShortStackTrace(t);
			if(null != sb){
				logger.fatal(sb.append(message).toString() + shortStacKtrace);
			}else{
				logger.fatal(message + shortStacKtrace);
			}
		}
	}
	
	private StringBuilder getLogContext(){
		StringBuilder sb = null;
		try{
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			if(request != null){
				String sessionId =  null;
				String apacheUniqueId;
				String clientSourceIP;
				String sourceIP;
				String profileId;
				
				if(null != ServletUtil.getCurrentUserProfile()) {
					profileId=ServletUtil.getCurrentUserProfile().getRepositoryId();
				} else {
					profileId=request.getCookieParameter(PROFILE_ID);
				}
				// get JSESSIONID from cookie
		        final Cookie[] cookies = request.getCookies();
		        if(null != cookies) {
		            for (final Cookie c : cookies) {
		                if (c.getName().equalsIgnoreCase(JSESSION_ID)) {
		                	sessionId = c.getValue(); 
		                    break;
		                }
		            }
		        }
				apacheUniqueId=request.getHeader(UNIQUE_ID_KEY);
				clientSourceIP=request.getHeader(CLIENT_SOURCE_IP_KEY);
				sourceIP=request.getHeader(SOURCE_IP_KEY);
				
				
				if(DigitalStringUtil.isNotBlank(sessionId) || DigitalStringUtil.isNotBlank(profileId) || DigitalStringUtil.isNotBlank(apacheUniqueId) || DigitalStringUtil.isNotBlank(sourceIP) || DigitalStringUtil.isNotBlank(clientSourceIP)){
					sb = new StringBuilder();
					sb.append(SESSION_ID_TXT_PREFIX).append(sessionId).append(COMMA_DELIMITER);
					sb.append(PROFILE_ID_PREFIX).append(profileId).append(COMMA_DELIMITER);
					sb.append(APACHE_UNIQUE_ID_PREFIX).append(apacheUniqueId).append(COMMA_DELIMITER);
					sb.append(SOURCE_IP_KEY_PREFIX).append(sourceIP).append(COMMA_DELIMITER);
					sb.append(CLIENT_SOURCE_IP_PREFIX).append(clientSourceIP).append(COMMA_DELIMITER);
				}
			}
		}catch(Exception ex){
			logger.error("DSW_EXCEPTION : Error get the request context information for logging ", ex);
		}
		return sb;
	}
    
	/**
	 * @param stackTrace
	 * @param t
	 * @param maxStackTraceEntryCount
	 * @return
	 */
	private StringBuilder getShortStackTrace(StringBuilder stackTrace,
			Throwable t, int maxStackTraceEntryCount) {
		if (t == null) {
		    return null;
		}

		// If the new StringBuffer was null, instantiate it.
		if (stackTrace == null) {
			stackTrace = new StringBuilder(100);
		}

		// Get the stack trace array and begin to construct it.
		StackTraceElement[] ste = t.getStackTrace();
		stackTrace.append(t.getClass().getName());
		if (t.getMessage() != null) {
			stackTrace.append(": ");
			stackTrace.append(t.getMessage());
		}

		int linesToPrint = Math.min(ste.length, maxStackTraceEntryCount);

		// Add only as many lines of the stack trace as were asked for.
		for (int i = 0; i < linesToPrint; i++) {
			stackTrace.append(NEW_LINE_SEPARATOR).append("\tat ");
			stackTrace.append(ste[i].toString());
		}

		// If there are remaining lines, add the "... # more" line.
		if (linesToPrint < ste.length) {
			stackTrace.append(NEW_LINE_SEPARATOR).append("\t.... ");
			stackTrace.append(ste.length - linesToPrint);
			stackTrace.append(" more");
		}

		// Add any chained excpetions as well.
		if (t.getCause() != null) {
			stackTrace.append(NEW_LINE_SEPARATOR).append("Caused By: ");
			getShortStackTrace(stackTrace, t.getCause(),
					maxStackTraceEntryCount);
		}

		return stackTrace;
	}

	/**
	 * Method to return the max number of lines to be logged for an exception. For
	 * now Hard coded as 5, if we need to drive this from environment specific file
	 * we need to comment the lookup below.
	 * 
	 * @return
	 */
	private int getMaxLinesInStackTrace() {
		int maxLinesInStackTrace = 5;
		String maxLineSysValue = System.getProperty("com.dsw.commerce.maxLinesInStackTrace");
		if (DigitalStringUtil.isNotBlank(maxLineSysValue)) {
			try {
				maxLinesInStackTrace = Integer.parseInt(maxLineSysValue);
			} catch (Exception ex) {
				// do nothing
			}

		}
		return maxLinesInStackTrace;
	}

	/**
	 * Method to return the short stack trace given a Throwble object
	 * Uses getMaxLinesInStackTrace() method internally.
	 * @param t
	 * @return
	 */
	private String getShortStackTrace(Throwable t) {
		String shortStacKtrace = "";
		try {
			if(null != t) {
                StringBuilder stb = this.getShortStackTrace(null, t, getMaxLinesInStackTrace());
				shortStacKtrace =  ((stb == null) ? shortStacKtrace : stb.toString());
			}
		} catch(Exception ex) {
			logger.error("Exception while getting getShortStackTrace");
		}
		return shortStacKtrace;
	}
	
	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}

	public boolean isEnabledFor(Level level) {
		return logger.isEnabled(level);
	}

	public boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}
	
	public boolean isWarnEnabled() {
		return logger.isWarnEnabled();
	}
	
	public boolean isErrorEnabled() {
		return logger.isErrorEnabled();
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

}

