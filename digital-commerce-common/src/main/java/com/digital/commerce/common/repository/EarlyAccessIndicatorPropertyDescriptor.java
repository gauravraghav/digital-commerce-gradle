package com.digital.commerce.common.repository;

import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;


import java.util.Calendar;
import java.util.Date;

public class EarlyAccessIndicatorPropertyDescriptor extends
		RepositoryPropertyDescriptor {
	/**
     *
     */
	private static final long serialVersionUID = -6259343473515967032L;

	protected static final String TYPE_NAME = "EarlyAccessIndicatorPropertyDescriptor";

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass(TYPE_NAME,
				EarlyAccessIndicatorPropertyDescriptor.class);
	}

	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		// if value already cached return from the cache
		if (pValue != null && pValue instanceof Boolean) {
			return pValue;
		}

		Date earlyAccessStartDate = (Date) pItem
				.getPropertyValue("earlyAccessStartDate");
		Date earlyAccessEndDate = (Date) pItem
				.getPropertyValue("earlyAccessEndDate");
		Date currentDate = Calendar.getInstance().getTime();

		if (earlyAccessStartDate != null && earlyAccessEndDate != null) {
			// Return true if the current date falls between the
			// early access start and end dates
			if (currentDate.equals(earlyAccessStartDate)
					|| currentDate.equals(earlyAccessEndDate)
					|| (currentDate.after(earlyAccessStartDate) && currentDate
							.before(earlyAccessEndDate))) {
				return Boolean.TRUE; 
			}
		}

		return Boolean.FALSE;
	}

}
