package com.digital.commerce.common.communication;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class DigitalCommunicationConfiguration {

	private Map<String, String> emailTemplateMap = new HashMap<>();

}
