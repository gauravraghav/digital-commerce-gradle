package com.digital.commerce.common.util;

import atg.nucleus.Nucleus;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

/** @author mk402314 */
public final class ComponentLookupUtil {

	public static final String	PRICING_MODEL_COMP			= "/atg/commerce/pricing/UserPricingModels";
	public static final String	SHOPPING_CART				= "/atg/commerce/ShoppingCart";
	public static final String	PROMOTION_TOOLS				= "/atg/commerce/promotion/PromotionTools";
	public static final String	ADMIN_USER_AUTHORITY		= "/atg/dynamo/security/AdminUserAuthority";
	public static final String	PRICE_LIST_MANAGER			= "/atg/commerce/pricing/priceLists/PriceListManager";
	public static final String	INVENTORY_TOOLS				= "/com/digital/commerce/services/inventory/InventoryTools";
	public static final String	PROFILE						= "/atg/userprofiling/Profile";
	public static final String	STORE_INVENTORY_MANAGER		= "/com/digital/commerce/locations/DigitalStoreLocatorInventoryManager";
	public static final String	GEO_LOCATION_UTIL			= "/com/digital/commerce/locations/DigitalGeoLocationUtil";
	public static final String	INVENTORY_MANAGER			= "/atg/commerce/inventory/InventoryManager";
	public static final String	ORDER_SUMMARY				= "/com/digital/commerce/services/order/DigitalOrderSummary";
	public static final String	SHIPPING_GROUP_MANAGER		= "/com/digital/commerce/services/order/DigitalOrderSummary";
	public static final String	PRICE_TOOLS					= "/atg/commerce/pricing/PricingTools";
	public static final String	PURCHASE_PROCESS			= "/atg/commerce/order/purchase/PurchaseProcessHelper";
	public static final String	ORDER_MANAGER				= "/atg/commerce/order/OrderManager";
	public static final String	PROMOTIONS					= "/atg/commerce/pricing/Promotions";
	public static final String	CATALOG_TOOLS				= "/atg/commerce/catalog/CatalogTools";
	public static final String	MESSAGE_LOCATOR				= "/com/digital/commerce/services/MessageLocator";
	public static final String	TRANSACTION_MANAGER			= "/atg/dynamo/transaction/TransactionManager";
	public static final String	ORDER_SERVICES				= "/atg/commerce/order/OrderServices";
	public static final String	REWARD_CERTIFICATE_MANAGER	= "/com/digital/commerce/services/profile/rewards/DigitalRewardsManager";
	public static final String	REPRICING_CONSTANTS			= "/digital/edt/pricing/RepricingConstants";
	public static final String	SITE_CONTEXT				= "/atg/multisite/SiteContextManager";
	public static final String	DEFAULT_SITE_RULE_FILTER	= "/atg/multisite/DefaultSiteRuleFilter";
	public static final String	REQUEST_LOCALE				= "/atg/dynamo/servlet/RequestLocale";
	public static final String	BAZAAR_VOICE_REPOSITORY		= "/com/digital/commerce/common/reviews/repository/BazaarVoiceRepository/";
	public static final String	INSOLE_AGENT_BEAN			= "/com/digital/commerce/common/csc/ContactCenterUser";
	public static final String 	DSW_BASE_CONSTANTS			= "/com/digital/commerce/common/service/DigitalConstants";
	public static final String 	DSW_ORDER_STATUS_TOOLS		= "/com/digital/commerce/services/order/status/DigitalOrderStatusTools";
	public static final String	PROFILE_TOOLS				= "/atg/userprofiling/ProfileTools";
	public static final String  TRANSACTION_LOCK_FACTORY	= "/atg/commerce/util/TransactionLockFactory";
	public static final String  REST_CACHE_CONTROLLER		= "/com/digital/commerce/services/common/DigitalRestCacheController";
	public static final String  REWARDS_CACHE				= "/com/digital/commerce/integration/external/reward/util/BtsRewardsCache";
	public static final String  GIFTLIST_MANAGER			= "/atg/commerce/gifts/GiftlistManager";
	public static final String  WEB_OFFER_FILTER			= "/com/digital/commerce/services/v1_0/coupons/collections/filter/DigitalWebOfferFilter";
	public static final String 	ORDER_TOOLS					="/atg/commerce/order/OrderTools";
	public static final String 	COUPON_MESSAGE				="/com/digital/commerce/services/pricing/DigitalCouponMessage";
	public static final String DSW_PROMOTION_PROFILE_VALIDATOR="/com/digital/commerce/services/v1_0/promotions/collections/validator/DigitalPromotionProfileValidator";
	public static final String GWP_MARKER_MANAGER			="/atg/commerce/promotion/GWPMarkerManager";
	public static final String DSW_COMMIT_ORDER_FORM_HANDLER ="/atg/commerce/order/purchase/CommitOrderFormHandler";
	@SuppressWarnings("unchecked")
	public static final <T> T lookupComponent( String componentName, Class<T> expectedClass ) {
		return (T)doLookUp( componentName );
	}

	public static final Object lookupComponent( String componentName) {
		return doLookUp( componentName );
	}
	
	protected static Object doLookUp( String componentName ) {
		final DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		if( request == null ) {
			Nucleus nucleus = Nucleus.getGlobalNucleus();
			return nucleus.resolveName( componentName );
		} else {
			return request.resolveName( componentName );
		}
	}
}
