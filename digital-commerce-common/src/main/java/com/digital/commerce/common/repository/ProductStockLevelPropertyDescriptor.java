package com.digital.commerce.common.repository;

import static com.digital.commerce.common.util.ComponentLookupUtil.DSW_BASE_CONSTANTS;

import java.util.Collection;
import java.util.Iterator;

import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.ComponentLookupUtil;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

@SuppressWarnings({"rawtypes"})
public class ProductStockLevelPropertyDescriptor extends RepositoryPropertyDescriptor

{

	private static final long		serialVersionUID	= -8795816015069305986L;

	protected static final String	TYPE_NAME			= "ProductStockLevelPropertyDescriptor";

	protected static final String	COLLECTION			= "collection";

	protected static final String	PROPERTY			= "property";
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, ProductStockLevelPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		// pValue will get the data from Cache. We don't want stock level to come from Cache
		DigitalBaseConstants dswBaseConstants = (DigitalBaseConstants) ComponentLookupUtil.lookupComponent(DSW_BASE_CONSTANTS);
		if(null!=dswBaseConstants && dswBaseConstants.isProductSkuStockCacheEnabled()){
			if( pValue != null && pValue instanceof Long ) { return pValue; }
		}
		Long productStockLevel = Long.valueOf( 0 );
		long stockLevel = 0;
		String property = (String)getValue( COLLECTION );
		String subProperty = (String)getValue( PROPERTY );
		Object list = pItem.getPropertyValue( property );
		if( list instanceof Collection ) {
			Collection propList = (Collection)list;
			if( subProperty == null || subProperty.trim().length() == 0 ) {
				return Long.valueOf( 0 );
			} else {

				Iterator i = propList.iterator();
				while( i.hasNext() ) {
					RepositoryItem prop = (RepositoryItem)i.next();
					Long subPropVal = (Long)prop.getPropertyValue( subProperty );
					stockLevel = stockLevel + subPropVal.longValue();
				}

				productStockLevel = Long.valueOf( stockLevel );
				pItem.setPropertyValueInCache( this, productStockLevel );
				return productStockLevel;
			}
		} else {
			throw new IllegalArgumentException( "collection must be an instance of java.util.Collection" );
		}
	}

	public String getTypeName() {
		return TYPE_NAME;
	}

}
