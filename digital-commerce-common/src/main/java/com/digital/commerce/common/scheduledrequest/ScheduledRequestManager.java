package com.digital.commerce.common.scheduledrequest;

import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.reflect.MethodUtils;

import com.digital.commerce.common.exception.MethodNotFoundException;
import com.digital.commerce.common.util.IRepositoryHandler;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.ServerName;
import atg.service.scheduler.Scheduler;

@SuppressWarnings("unchecked")
@Getter
@Setter
public class ScheduledRequestManager extends GenericService implements ScheduledRequestConstants {

	private MutableRepository				scheduledRequestRepository;
	private ScheduledRequestMessageSource	scheduledRequestMessageSource;
	private Scheduler						scheduler;
	private ScheduledRequestService	scheduledRequestService;
	private ServerName						serverNameComponent;
	private IRepositoryHandler				scheduledRequestRepositoryHandler;

	public void persistScheduledRequest( ScheduledRequest scheduledRequest ) throws RepositoryException {
		MutableRepositoryItem scheduledRequestItem = getScheduledRequestRepository().createItem( SCHEDULED_REQUEST );
		scheduledRequestItem.setPropertyValue( COMPONENT_NAME, scheduledRequest.getComponentName() );
		scheduledRequestItem.setPropertyValue( COMPONENT_PROPERTY, scheduledRequest.getComponentProperty() );
		scheduledRequestItem.setPropertyValue( COMPONENT_METHOD, scheduledRequest.getComponentMethod() );
		scheduledRequestItem.setPropertyValue( SCHEDULE, scheduledRequest.getSchedule() );
		scheduledRequestItem.setPropertyValue( NEW_VALUE, scheduledRequest.getNewValue() );
		scheduledRequestItem.setPropertyValue( SERVER_NAMES, scheduledRequest.getServerNames() );
		getScheduledRequestRepository().addItem( scheduledRequestItem );

		scheduledRequest.setRepositoryId( scheduledRequestItem.getRepositoryId() );
		scheduledRequest.setCreationDate( (Timestamp)scheduledRequestItem.getPropertyValue( CREATION_DATE ) );
	}

	public void broadcastMessage( ScheduledRequest scheduledRequest ) {
		getScheduledRequestMessageSource().fireScheduledRequestMessage( scheduledRequest );
	}

	public void createAndBroadcast( ScheduledRequest scheduledRequest ) throws RepositoryException {
		persistScheduledRequest( scheduledRequest );
		broadcastMessage( scheduledRequest );
	}

	public void addJobToScheduler( ScheduledRequest scheduledRequest ) {
		ScheduledRequestScheduledJob job = new ScheduledRequestScheduledJob( getScheduledRequestService(), scheduledRequest );
		getScheduler().addScheduledJob( job );
	}

	public void addJobsToScheduler( Collection<ScheduledRequest> scheduledRequests ) {
		Iterator<ScheduledRequest> mIterator = scheduledRequests.iterator();
		while( mIterator.hasNext() ) {
			addJobToScheduler( mIterator.next() );
		}

    }

	public void editComponentProperty( ScheduledRequest scheduledRequest ) throws PropertyNotFoundException {
		String pComponentName = scheduledRequest.getComponentName();
		String pComponentProperty = scheduledRequest.getComponentProperty();
		String pNewValue = scheduledRequest.getNewValue();
		Object bean = Nucleus.getGlobalNucleus().resolveName( pComponentName );
		
		if( isLoggingDebug() ) {
			logDebug( String.format("Changing property '%s' on component '%s'", pComponentProperty, pComponentName));
		}
		
		DynamicBeans.setPropertyValueFromString( bean, pComponentProperty, pNewValue );		
	}
	
	public void invokeComponentMethod( ScheduledRequest scheduledRequest ) throws MethodNotFoundException {
		String pComponentName = scheduledRequest.getComponentName();
		String pComponentMethod = scheduledRequest.getComponentMethod();
		
		Object bean = Nucleus.getGlobalNucleus().resolveName( pComponentName );
		try {
			if( isLoggingDebug() ) {
				logDebug( String.format("Invoking method '%s' on component '%s'", pComponentMethod, pComponentName));
			}
			
			Object result = MethodUtils.invokeMethod(bean, pComponentMethod, null);
			
			if( isLoggingDebug() ) {
				logDebug( String.format("Invocation result of %s.%s: %s", pComponentName, pComponentMethod, result));
			}
		} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			logWarning("Error thrown while inovking method: " + pComponentMethod);
			throw new MethodNotFoundException();
		}
	}

	public void editComponentsProperties( Collection<ScheduledRequest> scheduledRequests ) throws PropertyNotFoundException {
		Iterator<ScheduledRequest> mIterator = scheduledRequests.iterator();
		while( mIterator.hasNext() ) {
			editComponentProperty( mIterator.next() );
		}
    }

	
	public void prepareOnStartup() {
		String serverName = getServerNameComponent().getServerName();
		List<ScheduledRequest> leadingScheduledRequests = new ArrayList<>();
		StringBuilder query = new StringBuilder();

		query.append( "select * from dsw_scheduled_request a, dsw_scheduled_request_server b where a.request_id = b.request_id and b.server_name = '" ).append( serverName ).append( "' and a.schedule > cast(sysdate as timestamp)" );

		if( isLoggingDebug() ) {
			logDebug( "Query to get leadingScheduledRequests = [" + query + "]" );
		}

		RepositoryItem[] results = getScheduledRequestRepositoryHandler().findBySQL( query.toString(), SCHEDULED_REQUEST );

		if( results != null && results.length > 0 ) {
			int i = 0;
			while( i < results.length ) {
				RepositoryItem scheduledRequestItem = results[i];

				ScheduledRequest mScheduledRequest = new ScheduledRequest();
				mScheduledRequest.setComponentName( (String)scheduledRequestItem.getPropertyValue( COMPONENT_NAME ) );
				mScheduledRequest.setComponentProperty( (String)scheduledRequestItem.getPropertyValue( COMPONENT_PROPERTY ) );
				mScheduledRequest.setComponentMethod( (String)scheduledRequestItem.getPropertyValue( COMPONENT_METHOD ) );
				mScheduledRequest.setNewValue( (String)scheduledRequestItem.getPropertyValue( NEW_VALUE ) );
				mScheduledRequest.setRepositoryId( (String)scheduledRequestItem.getRepositoryId() );
				Object schedule = scheduledRequestItem.getPropertyValue( SCHEDULE ) ;
				mScheduledRequest.setSchedule( (Date)schedule);
				mScheduledRequest.setStringifiedSchedule( ( (Date)schedule).toString() );
				mScheduledRequest.setCreationDate( (Timestamp)scheduledRequestItem.getPropertyValue( CREATION_DATE ) );
				mScheduledRequest.setServerNames( (Set<String>)scheduledRequestItem.getPropertyValue( SERVER_NAMES ) );

				leadingScheduledRequests.add( mScheduledRequest );
				i++;
			}
		}

		if( isLoggingInfo() ) {
			logInfo( "Found " + leadingScheduledRequests.size() + " leadingScheduledRequests for ServerName " + serverName );
		}

		if( isLoggingDebug() ) {
			logDebug( "leadingScheduledRequests = " + leadingScheduledRequests );
		}
		addJobsToScheduler( leadingScheduledRequests );
	}
}
