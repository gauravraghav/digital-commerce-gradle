package com.digital.commerce.common.repository;

import atg.commerce.inventory.InventoryException;
import atg.commerce.inventory.RepositoryInventoryManager;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import static com.digital.commerce.common.util.ComponentLookupUtil.DSW_BASE_CONSTANTS;
import static com.digital.commerce.common.util.ComponentLookupUtil.INVENTORY_MANAGER;

import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalStringUtil;

/**
 * @author psinha
 */
public class SkuStockLevelPropertyDescriptor extends RepositoryPropertyDescriptor {

    private static final long serialVersionUID = -6258342473415966032L;

    private static final String TYPE_NAME = "SkuStockLevelPropertyDescriptor";
    private static final String INFINITE_STOCK_LEVEL = "99999";
    private static final String ID = "id";

    static {
        RepositoryPropertyDescriptor.registerPropertyDescriptorClass(TYPE_NAME, SkuStockLevelPropertyDescriptor.class);
    }

    /**
     * @param pItem
     * @param pValue
     * @return Object - stock level inventory value
     */
    public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
        String skuId = null;

        Long skuStockLevel = 0L;

        DigitalBaseConstants dswBaseConstants = (DigitalBaseConstants) ComponentLookupUtil.lookupComponent(DSW_BASE_CONSTANTS);
        // pValue will get the data from Cache. Check if stock level has to come from Cache
        if (null != dswBaseConstants) {
            if (dswBaseConstants.isProductSkuStockCacheEnabled()) {
                if (pValue != null && pValue instanceof Long) {
                    return pValue;
                }
            }

            skuId = (String) pItem.getPropertyValue(ID);

            if (DigitalStringUtil.isEmpty(skuId)) {
                return skuStockLevel;
            }

            String[] productSkuGiftCardIdList = dswBaseConstants.getProductSkuGiftCardIdList();

            if (null != productSkuGiftCardIdList && productSkuGiftCardIdList.length > 0) {
                for (String productSkuGiftCardId : productSkuGiftCardIdList) {
                    if (productSkuGiftCardId.equalsIgnoreCase(skuId)) {
                        pItem.setPropertyValueInCache(this, INFINITE_STOCK_LEVEL);
                        return INFINITE_STOCK_LEVEL;
                    }
                }
            }
        }

        if (DigitalStringUtil.isEmpty(skuId)) {
            skuId = (String) pItem.getPropertyValue(ID);
        }

        RepositoryInventoryManager im =
                ComponentLookupUtil.lookupComponent(INVENTORY_MANAGER, RepositoryInventoryManager.class);

        if (im != null) {
            try {
                skuStockLevel = im.queryStockLevel(skuId, MultiSiteUtil.getWebsiteLocationId());
                setStockLevelProperty(pItem, skuStockLevel);
            } catch (InventoryException e) {
                // Swallow exception for giftCards and sku not having inventory records.
                // If no inventory record exists then add 0 to the Inventory cache for this SKU
				setStockLevelProperty( pItem, 0L );
            }
        }
        return skuStockLevel;
    }

    /**
     * @param item
     * @param skuStockLevel
     */
    private void setStockLevelProperty(RepositoryItemImpl item, Long skuStockLevel) {
        item.setPropertyValueInCache(this, skuStockLevel);
    }
}
