package com.digital.commerce.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EscapeUtils {
	private static final Logger					logger				= LogManager.getLogger( EscapeUtils.class );

	private static final Map<String, String>	ESCAPE_SEQUENCES	= new HashMap<>();

	private static final String[]				ESCAPE_SEARCH_LIST;
	private static final String[]				ESCAPE_REPLACEMENT_LIST;

	static {
		ESCAPE_SEQUENCES.put( "&trade;", "&#8482;" );
		ESCAPE_SEQUENCES.put( "&nbsp;", "&#160;" );
		ESCAPE_SEQUENCES.put( "&iexcl;", "&#161;" );
		ESCAPE_SEQUENCES.put( "&cent;", "&#162;" );
		ESCAPE_SEQUENCES.put( "&pound;", "&#163;" );
		ESCAPE_SEQUENCES.put( "&curren;", "&#164;" );
		ESCAPE_SEQUENCES.put( "&yen;", "&#165;" );
		ESCAPE_SEQUENCES.put( "&brvbar;", "&#166;" );
		ESCAPE_SEQUENCES.put( "&sect;", "&#167;" );
		ESCAPE_SEQUENCES.put( "&uml;", "&#168;" );
		ESCAPE_SEQUENCES.put( "&copy;", "&#169;" );
		ESCAPE_SEQUENCES.put( "&ordf;", "&#170;" );
		ESCAPE_SEQUENCES.put( "&not;", "&#172;" );
		ESCAPE_SEQUENCES.put( "&shy;", "&#173;" );
		ESCAPE_SEQUENCES.put( "&reg;", "&#174;" );
		ESCAPE_SEQUENCES.put( "&macr;", "&#175;" );
		ESCAPE_SEQUENCES.put( "&deg;", "&#176;" );
		ESCAPE_SEQUENCES.put( "&plusmn;", "&#177;" );
		ESCAPE_SEQUENCES.put( "&sup2;", "&#178;" );
		ESCAPE_SEQUENCES.put( "&sup3;", "&#179;" );
		ESCAPE_SEQUENCES.put( "&acute;", "&#180;" );
		ESCAPE_SEQUENCES.put( "&micro;", "&#181;" );
		ESCAPE_SEQUENCES.put( "&para;", "&#182;" );
		ESCAPE_SEQUENCES.put( "&middot;", "&#183;" );
		ESCAPE_SEQUENCES.put( "&cedil;", "&#184;" );
		ESCAPE_SEQUENCES.put( "&sup1;", "&#185;" );
		ESCAPE_SEQUENCES.put( "&ordm;", "&#186;" );
		ESCAPE_SEQUENCES.put( "&raquo;", "&#187;" );
		ESCAPE_SEQUENCES.put( "&frac14;", "&#188;" );
		ESCAPE_SEQUENCES.put( "&frac12;", "&#189;" );
		ESCAPE_SEQUENCES.put( "&frac34;", "&#190;" );
		ESCAPE_SEQUENCES.put( "&iquest;", "&#191;" );
		ESCAPE_SEQUENCES.put( "&Agrave;", "&#192;" );
		ESCAPE_SEQUENCES.put( "&Aacute;", "&#193;" );
		ESCAPE_SEQUENCES.put( "&Acirc;", "&#194;" );
		ESCAPE_SEQUENCES.put( "&Atilde;", "&#195;" );
		ESCAPE_SEQUENCES.put( "&Auml;", "&#196;" );
		ESCAPE_SEQUENCES.put( "&Aring;", "&#197;" );
		ESCAPE_SEQUENCES.put( "&AElig;", "&#198;" );
		ESCAPE_SEQUENCES.put( "&Ccedil;", "&#199;" );
		ESCAPE_SEQUENCES.put( "&Egrave;", "&#200;" );
		ESCAPE_SEQUENCES.put( "&Eacute;", "&#201;" );
		ESCAPE_SEQUENCES.put( "&Ecirc;", "&#202;" );
		ESCAPE_SEQUENCES.put( "&Euml;", "&#203;" );
		ESCAPE_SEQUENCES.put( "&Igrave;", "&#204;" );
		ESCAPE_SEQUENCES.put( "&Iacute;", "&#205;" );
		ESCAPE_SEQUENCES.put( "&Icirc;", "&#206;" );
		ESCAPE_SEQUENCES.put( "&Iuml;", "&#207;" );
		ESCAPE_SEQUENCES.put( "&ETH;", "&#208;" );
		ESCAPE_SEQUENCES.put( "&Ntilde;", "&#209;" );
		ESCAPE_SEQUENCES.put( "&Ograve;", "&#210;" );
		ESCAPE_SEQUENCES.put( "&Oacute;", "&#211;" );
		ESCAPE_SEQUENCES.put( "&Ocirc;", "&#212;" );
		ESCAPE_SEQUENCES.put( "&Otilde;", "&#213;" );
		ESCAPE_SEQUENCES.put( "&Ouml;", "&#214;" );
		ESCAPE_SEQUENCES.put( "&times;", "&#215;" );
		ESCAPE_SEQUENCES.put( "&Oslash;", "&#216;" );
		ESCAPE_SEQUENCES.put( "&Ugrave;", "&#217;" );
		ESCAPE_SEQUENCES.put( "&Uacute;", "&#218;" );
		ESCAPE_SEQUENCES.put( "&Ucirc;", "&#219;" );
		ESCAPE_SEQUENCES.put( "&Uuml;", "&#220;" );
		ESCAPE_SEQUENCES.put( "&Yacute;", "&#221;" );
		ESCAPE_SEQUENCES.put( "&THORN;", "&#222;" );
		ESCAPE_SEQUENCES.put( "&szlig;", "&#223;" );
		ESCAPE_SEQUENCES.put( "&agrave;", "&#224;" );
		ESCAPE_SEQUENCES.put( "&aacute;", "&#225;" );
		ESCAPE_SEQUENCES.put( "&acirc;", "&#226;" );
		ESCAPE_SEQUENCES.put( "&atilde;", "&#227;" );
		ESCAPE_SEQUENCES.put( "&auml;", "&#228;" );
		ESCAPE_SEQUENCES.put( "&aring;", "&#229;" );
		ESCAPE_SEQUENCES.put( "&aelig;", "&#230;" );
		ESCAPE_SEQUENCES.put( "&ccedil;", "&#231;" );
		ESCAPE_SEQUENCES.put( "&egrave;", "&#232;" );
		ESCAPE_SEQUENCES.put( "&eacute;", "&#233;" );
		ESCAPE_SEQUENCES.put( "&ecirc;", "&#234;" );
		ESCAPE_SEQUENCES.put( "&euml;", "&#235;" );
		ESCAPE_SEQUENCES.put( "&igrave;", "&#236;" );
		ESCAPE_SEQUENCES.put( "&iacute;", "&#237;" );
		ESCAPE_SEQUENCES.put( "&icirc;", "&#238;" );
		ESCAPE_SEQUENCES.put( "&iuml;", "&#239;" );
		ESCAPE_SEQUENCES.put( "&eth;", "&#240;" );
		ESCAPE_SEQUENCES.put( "&ntilde;", "&#241;" );
		ESCAPE_SEQUENCES.put( "&ograve;", "&#242;" );
		ESCAPE_SEQUENCES.put( "&oacute;", "&#243;" );
		ESCAPE_SEQUENCES.put( "&ocirc;", "&#244;" );
		ESCAPE_SEQUENCES.put( "&otilde;", "&#245;" );
		ESCAPE_SEQUENCES.put( "&ouml;", "&#246;" );
		ESCAPE_SEQUENCES.put( "&divide;", "&#247;" );
		ESCAPE_SEQUENCES.put( "&oslash;", "&#248;" );
		ESCAPE_SEQUENCES.put( "&ugrave;", "&#249;" );
		ESCAPE_SEQUENCES.put( "&uacute;", "&#250;" );
		ESCAPE_SEQUENCES.put( "&ucirc;", "&#251;" );
		ESCAPE_SEQUENCES.put( "&uuml;", "&#252;" );
		ESCAPE_SEQUENCES.put( "&yacute;", "&#253;" );
		ESCAPE_SEQUENCES.put( "&thorn;", "&#254;" );
		ESCAPE_SEARCH_LIST = ESCAPE_SEQUENCES.keySet().toArray( new String[0] );
		ESCAPE_REPLACEMENT_LIST = ESCAPE_SEQUENCES.values().toArray( new String[0] );
	}

	/** Escapes all double and single quotes to xml and converts to all numeric entities
	 * 
	 * @param html
	 * @return */
	public static String escapeXml( String xml ) {
		return StringUtils.replaceEach( StringEscapeUtils.escapeXml( xml ), ESCAPE_SEARCH_LIST, ESCAPE_REPLACEMENT_LIST );
	}

	/** Escapes all numeric entities
	 * 
	 * @param html
	 * @return */
	public static String escapeHtml( String html ) {
		return StringUtils.replaceEach( html, ESCAPE_SEARCH_LIST, ESCAPE_REPLACEMENT_LIST );
	}

	public static String encode( String s ) {
		return encode( s, true );
	}

	public static String encode( String s, boolean isXMLEscaped ) {
		return encode( s, isXMLEscaped, "UTF-8" );
	}

	/** At some point this could be optimized but here is the basic flow for this.
	 * <ol>
	 * <li>Trim it</li>
	 * <li>Lowercase it</li>
	 * <li>Replace all "&amp;amp;", "&amp;#38;", "&" with the word "and"</li>
	 * <li>Strip all single apostrophes</li>
	 * <li>Replace all entities (e.g., &quot) with space</li>
	 * <li>Remove all characters that are not digits/alpha/whitespace</li>
	 * <li>Collapse whitespace (e.g., two spaces become one space)</li>
	 * <li>encode it</li>
	 * <li>
	 * </ol>
	 * 
	 * @param s
	 * @param isXMLEscaped TODO: what is this for?
	 * @param encoding
	 * @return */
	public static String encode( String s, boolean isXMLEscaped, String encoding ) {
		String retVal = StringUtils.trimToEmpty( s ).toLowerCase();

		if( !isXMLEscaped ) {
			retVal = StringEscapeUtils.escapeXml( retVal );
		}
		retVal = retVal.replaceAll( "(&amp;)|(&#38;)|([\\s]&[\\s])", " and " ).replaceAll( "[']|(&#39;)|(&apos;)", "" ).replaceAll( "&[^;]*;", " " ).replaceAll( "[^a-zA-Z0-9\\s]", " " ).replaceAll( "\\s{2,}", " " ).trim();
		try {
			retVal = URLEncoder.encode( retVal, encoding );
		} catch( UnsupportedEncodingException ex ) {
			logger.debug( "Unable to URL Encode Product Description", ex );
		}
		retVal = retVal.replaceAll( "%20", "+" );
		return retVal;
	}

}
