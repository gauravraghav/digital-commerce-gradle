package com.digital.commerce.common.util;

import org.apache.commons.lang.StringUtils;

/** Util class to hold some common string utilites
 * 
 * @author djboyd
 * @author <a href="mailto:knaas@dswinc.com">knaas</a> */
public class DigitalStringUtil extends StringUtils{

	/** Performs a substring in such a way that exceptions are avoided. If str is null, then null is returned.
	 * 
	 * @param str
	 * @param start
	 * @return */
	public static String substring( String str, int start ) {
		if( str == null ) { return null; }
		return str.substring( start );
	}

	/** Performs a substring in such a way that exceptions are avoided. If str is null, then null is returned.
	 * 
	 * @param str
	 * @param start
	 * @param end
	 * @return */
	public static String substring( String str, int start, int end ) {
		if( str == null ) { return null; }
		return str.substring( start, end );
	}

	/** Returns true if the trimmed forms of str1 and str2 are equal.
	 * 
	 * @param str1
	 * @param str2
	 * @return */
	public static boolean trimEqualsIgnoreCase( String str1, String str2 ) {
		return str1 == null ? str2 == null : DigitalStringUtil.trimToEmpty( str1 ).equalsIgnoreCase( DigitalStringUtil.trimToEmpty( str2 ) );
	}

	public static String addURLParam( String URL, String param, String value ) {
		if(URL.contains("?")) {
			return URL + "&" + param + "=" + value;
		} else {
			return URL + "?" + param + "=" + value;
		}
	}

	/** strip out the non-digits from the String value, used for clean up phoneNumber */
	public static String stripNonDigit( String s ) {
		if( s == null ) { return null; }
		return s.replaceAll( "[^\\d]", "" );
	}
	
	public static boolean isEmpty(String str) {
        return StringUtils.isEmpty(str);
    }
	
	public static boolean isNotEmpty(String str) {
        return StringUtils.isNotEmpty(str);
    }
	
	public static boolean endsWith(String str, String suffix) {
		 return StringUtils.endsWith(str, suffix);
    }
 
    
    public static String trim(String str) {
        return StringUtils.trim(str);
    }
   
    public static boolean equals(String str1, String str2) {
        return StringUtils.equals(str1, str2);
    }
    
    
    public static boolean isNotBlank(String str) {
        return StringUtils.isNotBlank(str);
    }
   
    public static boolean isBlank(String str) {
    	return StringUtils.isBlank(str);
    }
    
    public static boolean equalsIgnoreCase(String str1, String str2) {
        return StringUtils.equalsIgnoreCase(str1, str2);
    }
    
    public static String upperCase(String str) {        
        return StringUtils.upperCase(str);
    }
    
    public static String lowercase(String str) {        
        return StringUtils.lowerCase(str);
    }
    public static String left(String str, int length) {        
		return StringUtils.left(str, length);
	}
    
    public static String stripStart(String str){
    	return StringUtils.stripStart(str,"0");
    }

	public static boolean contains(String str, String[] charsetArray) {
		if (isEmpty(str)) {
			return false;
		}
		for (String charset: charsetArray){			
			if (str.contains(charset)) return true;
		}
		return false;
	}
	
	public static String numToWord(Integer i) {

		final String[] units = { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten",
				"Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
		final String[] tens = { "", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
		if (i < 20)
			return units[i];
		if (i < 100)
			return tens[i / 10] + ((i % 10 > 0) ? " " + numToWord(i % 10) : "");
		if (i < 1000)
			return units[i / 100] + " Hundred" + ((i % 100 > 0) ? " and " + numToWord(i % 100) : "");
		if (i < 1000000)
			return numToWord(i / 1000) + " Thousand " + ((i % 1000 > 0) ? " " + numToWord(i % 1000) : "");
		return numToWord(i / 1000000) + " Million " + ((i % 1000000 > 0) ? " " + numToWord(i % 1000000) : "");
	}
	
	public static String replaceChars(String str,String searchChars, String replaceChars){
		
		return StringUtils.replaceChars(str, searchChars, replaceChars);
	}
	

}
