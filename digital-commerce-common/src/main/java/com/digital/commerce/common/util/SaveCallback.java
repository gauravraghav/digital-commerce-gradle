package com.digital.commerce.common.util;

import atg.repository.MutableRepositoryItem;

public interface SaveCallback<T extends MutableRepositoryItem> {

	/** Saves the item
	 * 
	 * @param item
	 * @return */
	public void save( MutableRepositoryItem item );

}
