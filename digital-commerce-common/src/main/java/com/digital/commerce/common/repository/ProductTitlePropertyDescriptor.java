package com.digital.commerce.common.repository;


import atg.adapter.gsa.GSAPropertyDescriptor;
import atg.repository.RepositoryItemImpl;

/** @author jmadich
 *         This class is used to provide the value for the displayName and description properties
 *         on a product by leveraging the value of required the productTitle property. */
public class ProductTitlePropertyDescriptor extends GSAPropertyDescriptor {

	/**
	 * 
	 */
	private static final long		serialVersionUID	= -5746125569487756413L;

	protected static final String	TYPE_NAME			= "ProductTitlePropertyDescriptor";

	protected static final String	PRODUCT_NAME		= "displayName";

	static {
		GSAPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, ProductTitlePropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		String productName = (String)pItem.getPropertyValue( PRODUCT_NAME );
		if( productName != null && !productName.isEmpty() ) { return productName; }
		return null;
	}

	public String getTypeName() {
		return TYPE_NAME;
	}

}
