package com.digital.commerce.common.util;

import lombok.Getter;

@Getter
public enum SessionAttributesConstant {
	PAYPAL_XPRESS_CHECKOUT("paypalXpressCheckout"),
	PAYPAL_EMAIL("paypalemail"),
	PAYPAL_ACS_URL("AcsURL"),
	PAYPAL_PAYLOAD("PpPayload"),
	PAYPAL_CARDINAL_ORDER_ID("cardinalOrderId"),
	DSW_SESSION_ID("DswSessionId"),
	SELECTED_PAYMENT("selectedpayment"),
	PAYPAL_TRANSACTION_ID("ppTransactionId");
	
	private final String value;
		
	private SessionAttributesConstant( String value ) {
		this.value = value;
	}
}
