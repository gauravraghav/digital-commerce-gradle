package com.digital.commerce.common.scheduledrequest;

public interface ScheduledRequestConstants {
	public static final String	COMPONENT_NAME				= "componentName";
	public static final String	COMPONENT_PROPERTY			= "componentProperty";
	public static final String	COMPONENT_METHOD			= "componentMethod";
	public static final String	NEW_VALUE					= "newValue";
	public static final String	SCHEDULE					= "schedule";
	public static final String	CREATION_DATE				= "creationDate";
	public static final String	SERVER_NAMES				= "serverNames";
	public static final String	SCHEDULED_REQUEST			= "scheduledRequest";
	public static final String	SCHEDULED_REQUEST_JMS_TYPE	= "com.digital.scheduledrequest.ScheduledRequest";
}
