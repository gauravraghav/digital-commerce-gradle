package com.digital.commerce.common.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** Can be used much like @Resource to inject Nucleus managed components into Spring components.
 * 
 * @author knaas */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface NucleusRef {

	/** The Nucleus location of the component (e.g., /atg/userprofiling/Profile)
	 * 
	 * @return the string */
	String value();

	/** Required.
	 * 
	 * @return true, if successful */
	boolean required() default true;
}
