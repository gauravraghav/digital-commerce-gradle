package com.digital.commerce.common.transaction;

import javax.transaction.SystemException;

import atg.commerce.util.NoLockNameException;
import atg.commerce.util.TransactionLockFactory;
import atg.commerce.util.TransactionLockService;
import atg.dtm.TransactionDemarcationException;
import atg.dtm.UserTransactionDemarcation;
import atg.repository.RepositoryItem;
import atg.service.lockmanager.DeadlockException;
import atg.service.lockmanager.LockManagerException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.ComponentLookupUtil;

/**
 * Basic helper class that allows controller and service classes to initiate and
 * end transactions properly. This <b>must</b> be called by all methods that get
 * invoked via http, or where you require access to any property of a repository
 * item
 * 
 * @author NS389061
 *
 */
public class TransactionUtils {

	private static final DigitalLogger LOGGER = DigitalLogger.getLogger(TransactionUtils.class);
	
	static final String LOCK_ATTR = "atg.CommerceProfileToolsLock";
	
	/**
	 * Starts a new transaction and returns the associated object for closing
	 * the transaction once work is completed
	 * 
	 * @param componentName
	 *            The name of the calling component
	 * @param methodName
	 *            The name of the calling method
	 * @return The user transaction object if transaction is started, null
	 *         otherwise
	 */
	public static final UserTransactionDemarcation startNewTransaction(final String componentName,
			final String methodName) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(componentName + " :: " + methodName + " :: startNewTransaction");
		}
		try {
			final UserTransactionDemarcation td = new UserTransactionDemarcation();
			td.begin();
			return td;
		} catch (TransactionDemarcationException e) {
			LOGGER.error("Unable to start transaction for " + componentName + " :: " + methodName);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.error("Error : " + e.getMessage(), e);
			} else {
				LOGGER.error("Error : " + e.getMessage());
			}
		} catch (Throwable t) {
			LOGGER.fatal("This should not occur outside of unit tests: " + t.getMessage(), t);
		}
		return null;
	}

	/**
	 * Ends the given transaction if it was initiated successfully
	 * 
	 * @param td
	 *            The user transaction object
	 * @param componentName
	 *            The name of the calling component
	 * @param methodName
	 *            The name of the calling method
	 */
	public static final void endTransaction(final UserTransactionDemarcation td, final String componentName,
			final String methodName) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(componentName + " :: " + methodName + " :: endTransaction");
		}
		if (td == null) {
			LOGGER.warn(componentName + " :: " + methodName + " :: Transaction object is null");
		} else {
			try {
				td.end();
			} catch (TransactionDemarcationException e) {
				LOGGER.error("Unable to end transaction for " + componentName + " :: " + methodName);
				if (LOGGER.isDebugEnabled()) {
					LOGGER.error("Error : " + e.getMessage(), e);
				} else {
					LOGGER.error("Error : " + e.getMessage());
				}
			} catch (Throwable t) {
				LOGGER.fatal("This should not occur outside of unit tests: " + t.getMessage(), t);
			}
		}
	}
	
	public static final void rollBackTransaction(final UserTransactionDemarcation td, final String componentName,
			final String methodName){
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(componentName + " :: " + methodName + " :: rollBackTransaction");
		}
		if (td == null) {
			LOGGER.warn(componentName + " :: " + methodName + " :: Transaction object is null");
		} else {
			if(td.getUserTransaction() == null){
				LOGGER.warn(componentName + " :: " + methodName + " :: UserTransaction object is null");
			}else if (!isTransactionMarkedAsRollBack(td)){
				try {
					
					td.getUserTransaction().setRollbackOnly();
				} catch (IllegalStateException | SystemException e) {
					LOGGER.error("Unable to rollBack transaction for " + componentName + " :: " + methodName);
					if (LOGGER.isDebugEnabled()) {
						LOGGER.error("Error : " + e.getMessage(), e);
					} else {
						LOGGER.error("Error : " + e.getMessage());
					}
				} catch (Throwable t) {
					LOGGER.fatal("This should not occur outside of unit tests: " + t.getMessage(), t);
				}
			}
		}
	}

   public static final boolean isTransactionMarkedAsRollBack(UserTransactionDemarcation td)
   {
     try
     {
       if (td != null && td.getUserTransaction() != null) {
         int transactionStatus = td.getUserTransaction().getStatus();
         if (transactionStatus == javax.transaction.Status.STATUS_MARKED_ROLLBACK ||
        		 transactionStatus == javax.transaction.Status.STATUS_ROLLEDBACK || 
        				 transactionStatus == javax.transaction.Status.STATUS_ROLLING_BACK) {
        	 return true;
         }
       }
     } catch (Throwable exc) {
         LOGGER.error(exc);
     }
     return false;
   }
	
	
    static TransactionLockFactory mTransactionLockFactory = null;
	
	public static TransactionLockFactory getTransactionLockFactory() {
		if(mTransactionLockFactory == null){
			mTransactionLockFactory = ComponentLookupUtil.lookupComponent(ComponentLookupUtil.TRANSACTION_LOCK_FACTORY, TransactionLockFactory.class);
		}
		return mTransactionLockFactory;
	}

	private static TransactionLockService getTransactionLockService(String profileId) {
		TransactionLockService mTransactionLockService = null;
		TransactionLockFactory factory = getTransactionLockFactory();

		if (factory != null) {
			mTransactionLockService = factory.getServiceInstance(profileId);
		} else {
			LOGGER.warn("missingTransactionLockFactory");
		}

		return mTransactionLockService;
	}
	
	public static void acquireTransactionLock(String componentName, String methodName) {
		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(componentName + " :: " + methodName + " :: acquireTransactionLock");
			}
			RepositoryItem profileItem = ServletUtil.getCurrentUserProfile();
			
			if (profileItem == null) {
				LOGGER.error(":: Can't Acquire Lock as ProfileItem is null for :: " + componentName + " :: " + methodName);
				return;
			}
			String profileId = profileItem.getRepositoryId();
			TransactionLockService service = getTransactionLockService(profileId);
			if (service != null) {
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				request.setAttribute(LOCK_ATTR, profileId);
				Long startTime = new Long(System.currentTimeMillis());
				long threadId = Thread.currentThread().getId();
				request.setAttribute("START_TIME_" + threadId, startTime);
				service.acquireTransactionLock(profileId);
			}else{
				LOGGER.error(":: TransactionLockService is null, check if client local manager is configured. :: " + componentName + " :: " + methodName);				
			}
		} catch (NoLockNameException exc) {
			LOGGER.error("Unable to acquireTransactionLock for " + componentName + " :: " + methodName + " due to NoLockNameException. ");
			LOGGER.error(exc);

		} catch (DeadlockException de) {
			LOGGER.error("Unable to acquireTransactionLock for " + componentName + " :: " + methodName + " due to DeadlockException. ");
			LOGGER.error(de);
		} catch (Throwable t) {
			LOGGER.error("Unable to acquireTransactionLock for " + componentName + " :: " + methodName);
			LOGGER.error(t);
		}
	}

	public static void releaseTransactionLock(String componentName, String methodName) {
		DynamoHttpServletRequest request = null;
		String profileId = null;
		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(componentName + " :: " + methodName + " :: releaseTransactionLock");
			}

			RepositoryItem profileItem = ServletUtil.getCurrentUserProfile();
			
			if (profileItem == null) {
				LOGGER.error(":: Can't Acquire Lock as ProfileItem is null for :: " + componentName + " :: " + methodName);
				return;
			}
			profileId = profileItem.getRepositoryId();
			
			TransactionLockService service = getTransactionLockService(profileId);
			if (service == null) {
				LOGGER.error(":: TransactionLockService is null, check if client local manager is configured. :: " + componentName + " :: " + methodName);				
				return;
			}
			
			request = ServletUtil.getCurrentRequest();
			String lockName = null;
			if(request.getAttribute(LOCK_ATTR) != null ){
				lockName = (String) request.getAttribute(LOCK_ATTR);
			}
			if (lockName == null) {
				LOGGER.info(":: Transaction Lock name 'atg.CommerceProfileToolsLock' is null. No need to release the lock. :: " + componentName + " :: " + methodName);
				return;
			}
			request.removeAttribute(LOCK_ATTR);
			
			if(service.getClientLockManager() != null){
				if(service.getClientLockManager().hasWriteLock(lockName, Thread.currentThread())){
					service.releaseTransactionLock(lockName);
				}else{
					LOGGER.debug(":: No Write lock on the profile for :: " + componentName + " :: " + methodName);				
				}
			}else{
				LOGGER.error(":: ClientLockManager is null, check if it is configured. :: " + componentName + " :: " + methodName);				
			}

		} catch (LockManagerException exc) {
			LOGGER.error("Unable to releaseTransactionLock for " + componentName + " :: " + methodName + " due to LockManagerException. ");
			LOGGER.error(exc);
		} catch (Throwable t) {
			LOGGER.error("Unable to releaseTransactionLock for " + componentName + " :: " + methodName);
			LOGGER.error(t);
			
		}finally{
			if(request != null){
				long threadId = Thread.currentThread().getId();
				if(request.getAttribute("START_TIME_" + threadId) != null){
					Long startTime = (Long)request.getAttribute("START_TIME_" + threadId);
					Long endTime = new Long(System.currentTimeMillis());
					if(startTime != null){
						LOGGER.info(":: PROFILE_LOCK_TIME for profile - " + profileId + " in :: " + componentName + " :: " + methodName + " == " + (endTime - startTime) + "ms");					
					}
				}
			}
		}
	}
}
