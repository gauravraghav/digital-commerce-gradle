package com.digital.commerce.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import com.digital.commerce.common.logger.DigitalLogger;

public class DigitalDateUtil extends DateUtils{
	public static final String	OFFER_DATE_FORMAT	= "yyyy-MM-dd'T'HH:mm:ss";
	private static final String	OPS_EMAIL_TRACKING_DATE_FORMAT	= "yyyyMMdd";
	public static final String	YANTRA_ORDER_STATUS_DATE_TIMEZONE_FORMAT	= "yyyy-MM-dd'T'HHmmssZ";
	
	public static Date calculteDOB(String birthDay, String birthMonth, String birthYear){
			Calendar c = Calendar.getInstance();
			if(DigitalStringUtil.isNotBlank(birthMonth) && DigitalStringUtil.isNotEmpty(birthMonth)){
				c.set(new Integer(birthYear), (new Integer(birthMonth)-1), new Integer(birthDay),0,0);
			}
			return c.getTime();
	}
	
	public static boolean validateDOB(String birthDay, String birthMonth, String birthYear) {
        final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
		if(birthMonth == null || birthDay == null) {
			return false;
		}
		StringBuilder date = new StringBuilder(10);
		if(birthDay.length() < 2) {
			birthDay = "0"+birthDay;
		}
		if(birthMonth.length() < 2) {
			birthMonth = "0"+birthMonth;
		}
		//make the year 2000 to support leap day
		date.append(birthMonth).append("/").append(birthDay).append("/").append(birthYear);
		SimpleDateFormat df = DATE_FORMAT;
		df.setLenient(false);
		try {
			df.parse(date.toString());
		} catch (java.text.ParseException e) {
			StringBuilder errorMsg = new StringBuilder("Invalid date:");
			errorMsg.append(" birthDay-").append(birthDay);
			errorMsg.append(" birthMonth-").append(birthMonth);
			errorMsg.append(" birthYear-").append(birthYear);
			DigitalLogger.getLogger(DigitalDateUtil.class).error(errorMsg.toString(),e);
			return false;
		}
		return true;
	}

	/**
	 * @return
	 */
	public static String getOperationEmailsTrackingDate() {
		String formatedDate = null;
		try {
			Date today = Calendar.getInstance().getTime();
			SimpleDateFormat formatter = new SimpleDateFormat(
					OPS_EMAIL_TRACKING_DATE_FORMAT);
			formatedDate = formatter.format(today);
		} catch (Exception ex) {
			// do nothing
		}
		return formatedDate;
	}
	
	/**
	 * @return
	 */
	public static Date parseDate(String date, String format)
	{
		if(DigitalStringUtil.isBlank(date)){
			return null;
		}
		try{
		    SimpleDateFormat formatter = new SimpleDateFormat(format);
		    return formatter.parse(date);
		}catch(Exception ex){
	    	return null;
	    }
	}
	
	public static String parseDate(String date, String sourceFormat, String targetFormat)
	{
		if(DigitalStringUtil.isBlank(date)){
			return null;
		}
		try{
		    SimpleDateFormat formatter = new SimpleDateFormat(sourceFormat);
		    Date sourceDate =  formatter.parse(date);
		    return (new SimpleDateFormat( targetFormat).format(sourceDate));
		}catch(Exception ex){
	    	return null;
	    }
	}
	
	/**
	 * @return
	 */
	public static Date parseYantraDateTime(String yantraDate, String format)
	{
		if(DigitalStringUtil.isBlank(yantraDate)){
			return null;
		}
		try{
			String date = yantraDate.replace(":", "");
		    SimpleDateFormat formatter = new SimpleDateFormat(format);
		    return formatter.parse(date);
		}catch(Exception ex){
	    	return null;
	    }
	}

	public static int getMonth(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int month = cal.get(Calendar.MONTH);

		return month;
	}

	public static long getDayOfMonth(Date date){
		return getFragmentInDays(date, Calendar.MONTH);
	}

	public static int getYear(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);

		return year;
	}

	public static int getCurrentYear(){
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);

		return year;
	}
	
	public static void main(String[] arg){
		String updateDate = "2016-08-25T15:03:51-04:00";
		Date date = parseYantraDateTime(updateDate, DigitalDateUtil.YANTRA_ORDER_STATUS_DATE_TIMEZONE_FORMAT);
		System.out.print(date);
	}
}
