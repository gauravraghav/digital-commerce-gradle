package com.digital.commerce.common.multisite;

/**
 * Properties manager for SiteRepository.
 * 
 */
public class SitePropertiesManager {
	private SitePropertiesManager() {
		
	}
	// -------------------------------------
	// Properties
	// -------------------------------------
	
	// -------------------------------------
	// Property: catalogIdPropertyName
	// -------------------------------------
	private static final String catalogIdPropertyName = "catalogId";
	
	/**
	 * @return the mCatalogIdPropertyName
	 */
	public static String getCatalogIdPropertyName() {
		return catalogIdPropertyName;
	}
	
	// -------------------------------------
	// Property: listPriceListIdPropertyName
	// -------------------------------------
	private static final String listPriceListIdPropertyName = "listPriceListId";
	
	/**
	 * @return the listPriceListIdPropertyName
	 */
	public static String getListPriceListIdPropertyName() {
		return listPriceListIdPropertyName;
	}
	
	
	// -------------------------------------
	// Property: salePriceListIdPropertyName
	// -------------------------------------
	private static final String salePriceListIdPropertyName = "salePriceListId";
	
	/**
	 * @return the salePriceListIdPropertyName
	 */
	public static String getSalePriceListIdPropertyName() {
		return salePriceListIdPropertyName;
	}
	
	
	// -------------------------------------
	// Property: resourceBundlePropertyName
	// -------------------------------------
	private static final String resourceBundlePropertyName = "resourceBundle";
	
	/**
	 * @return the resourceBundlePropertyName
	 */
	public static String getResourceBundlePropertyName() {
		return resourceBundlePropertyName;
	}
	
	// -------------------------------------
	// Property: newProductThresholdDaysPropertyName
	// -------------------------------------
	private static final String newProductThresholdDaysPropertyName = "newProductThresholdDays";
	
	/**
	 * @return the newProductThresholdDaysPropertyName
	 */
	public static String getNewProductThresholdDaysPropertyName() {
		return newProductThresholdDaysPropertyName;
	}
	
	
	// -------------------------------------
	// property: DefaultCountryPropertyName
	// -------------------------------------
	private static final String defaultCountryPropertyName = "defaultCountry";
	
	/**
	 * @return the String
	 */
	public static String getDefaultCountryPropertyName() {
		return defaultCountryPropertyName;
	}
	
	// -------------------------------------
	// property: BasicShippingThreshold
	// -------------------------------------
	private static final String basicShippingThresholdPropertyName = "orderBasicShippingThreshold";
	
	/**
	 * @return the String
	 */
	public static String getBasicShippingThresholdPropertyName() {
		return basicShippingThresholdPropertyName;
	}
	
	
	// -------------------------------------
	// Properties
	// -------------------------------------
	
	// -------------------------------------
	// property: DefaultLanguagePropertyName
	// -------------------------------------
	private static final String defaultLanguagePropertyName = "defaultLanguage";
	
	/**
	 * @return the String
	 */
	public static String getDefaultLanguagePropertyName() {
		return defaultLanguagePropertyName;
	}
	
	// -------------------------------------
	// property: LanguagesPropertyName
	// -------------------------------------
	private static final String languagesPropertyName = "languages";
	
	/**
	 * @return the String
	 */
	public static String getLanguagesPropertyName() {
		return languagesPropertyName;
	}
	
	// -------------------------------------
	// property: PurchaseLimitPropertyName
	// -------------------------------------
	private static final String orderPurchaseLimitPropertyName = "orderPurchaseLimit";
	
	/**
	 * @return the String
	 */
	public static String getOrderPurchaseLimitPropertyName() {
		return orderPurchaseLimitPropertyName;
	}
	
	
	// -------------------------------------
	// property: OrderCartLineLimitPropertyName
	// -------------------------------------
	private static final String orderCartLineLimitPropertyName = "orderCartLineLimit";
	
	public static String getOrderCartLineLimitPropertyName() {
		return orderCartLineLimitPropertyName;
	}
	
	// -------------------------------------
	// property: OrderSkuQtyLimitPropertyName
	// -------------------------------------
	private static final String orderSkuQtyLimitPropertyName = "orderSkuQtyLimit";

	public static String getOrderSkuQtyLimitPropertyName() {
		return orderSkuQtyLimitPropertyName;
	}
	
	// -------------------------------------
	// property: OrderCartLineLimitPropertyName
	// -------------------------------------
	private static final String wishlistItemLimitPropertyName = "wishlistItemLimit";

	public static String getWishlistItemLimitPropertyName() {
		return wishlistItemLimitPropertyName;
	}
	
	// -------------------------------------
	// property: NewOnClearanceDays
	// -------------------------------------
	private static final String newOnClearanceDaysPropertyName = "newOnClearanceDays";

	public static String getNewOnClearanceDaysPropertyName() {
		return newOnClearanceDaysPropertyName;
	}
	
	// -------------------------------------
	// property: OrderShippingRatesPropertyName
	// -------------------------------------
	private static final String orderShippingRatesPropertyName = "orderShippingRates";
	
	/**
	 * @return the String
	 */
	public static String getOrderShippingRatesPropertyName() {
		return orderShippingRatesPropertyName;
	}
	
	// -------------------------------------
	// property: WebsiteLocationIdPropertyName
	// -------------------------------------
	private static final String websiteLocationIdPropertyName = "websiteLocationId";
	
	/**
	 * @return the String
	 */
	public static String getWebsiteLocationIdPropertyName() {
		return websiteLocationIdPropertyName;
	}
	
	// -------------------------------------
	// property: gwpConfigurationPropertyName
	// -------------------------------------
	private static final String gwpConfigurationPropertyName = "gwpConfiguration";
	
	/**
	 * @return the String
	 */
	public static String getGwpConfigurationPropertyName() {
		return gwpConfigurationPropertyName;
	}
	
    // -------------------------------------
	// property: MaxForgotPasswordRetryPropertyName
	// -------------------------------------
	private static final String maxForgotPasswordRetryPropertyName = "maxForgotPasswordRetry"; //NOSONAR

	public static String getMaxForgotPasswordRetryPropertyName() {
		return maxForgotPasswordRetryPropertyName;
	}
	
	// -------------------------------------
	// property: MaxForgotPasswordRetryPropertyName
	// -------------------------------------
	private static final String excludeWordsPropertyName = "excludeWords";

	public static String getExcludeWordsPropertyName() {
		return excludeWordsPropertyName;
	}

	// -------------------------------------
	// property: EmailSharedLimitsPropertyName
	// -------------------------------------
	private static final String emailSharedLimitsPropertyName = "emailSharedLimits";

	public static String getEmailSharedLimitsPropertyName() {
		return emailSharedLimitsPropertyName;
	}

	// -------------------------------------
	// property: MessageMaxLengthPropertyName
	// -------------------------------------
	private static final String messageMaxLengthPropertyName = "messageMaxLength";

	public static String getMessageMaxLengthPropertyName() {
		return messageMaxLengthPropertyName;
	}
	
	// -------------------------------------
	// property: RewardPointsThresholdAmountPropertyName
	// -------------------------------------
	private static final String rewardPointsThresholdAmount = "rewardPointsThresholdAmount";

	public static String getRewardPointsThresholdAmountPropertyName() {
		return rewardPointsThresholdAmount;
	}

	// -------------------------------------
	// property: RewardsCertometerThresholdPropertyName
	// -------------------------------------
	private static final String rewardsCertometerThreshold = "certometerThreshold";

	public static String getRewardsCertometerThresholdPropertyName() {
		return rewardsCertometerThreshold;
	}

	// -------------------------------------
	// property: RewardsTierometerCTGThresholdPropertyName
	// -------------------------------------
	private static final String rewardsTierometerCTGThreshold = "tierometerCTGThreshold";

	public static String getRewardsTierometerCTGThresholdPropertyName() {
		return rewardsTierometerCTGThreshold;
	}

	// -------------------------------------
	// property: RewardsTierometerGTEThresholdPropertyName
	// -------------------------------------
	private static final String rewardsTierometerGTEThreshold = "tierometerGTEThreshold";

	public static String getRewardsTierometerGTEThresholdPropertyName() {
		return rewardsTierometerGTEThreshold;
	}
	
    // -------------------------------------
	// property: MaxForgotPasswordRetryPropertyName
	// -------------------------------------
	private static final String emailCreateAccountPropertyName = "emailCreateAccount";

	public static String getEmailCreateAccountPropertyName() {
		return emailCreateAccountPropertyName;
	}

    // -------------------------------------
	// property: RepriceAnonyProfileIdPropertyName
	// -------------------------------------
	private static final String repriceAnonyProfileIdPropertyName = "repriceAnonyProfileId";

	public static String getRepriceAnonyProfileIdPropertyName() {
		return repriceAnonyProfileIdPropertyName;
	}

	// -------------------------------------
	// property: RepriceClubProfileIdPropertyName
	// -------------------------------------
	private static final String repriceClubProfileIdPropertyName = "repriceClubProfileId";

	public static String getRepriceClubProfileIdPropertyName() {
		return repriceClubProfileIdPropertyName;
	}

	// -------------------------------------
	// property: RepriceGoldProfileIdPropertyName
	// -------------------------------------
	private static final String repriceGoldProfileIdPropertyName = "repriceGoldProfileId";

	public static String getRepriceGoldProfileIdPropertyName() {
		return repriceGoldProfileIdPropertyName;
	}

	// -------------------------------------
	// property: RepriceEliteProfileIdPropertyName
	// -------------------------------------
	private static final String repriceEliteProfileIdPropertyName = "repriceEliteProfileId";

	public static String getRepriceEliteProfileIdPropertyName() {
		return repriceEliteProfileIdPropertyName;
	}

    // -------------------------------------
	// property: RepriceBasicProfileIdPropertyName
	// -------------------------------------
	private static final String repriceBasicProfileIdPropertyName = "repriceBasicProfileId";

	public static String getRepriceBasicProfileIdPropertyName() {
		return repriceBasicProfileIdPropertyName;
	}
	
    // -------------------------------------
	// property: RepricePrimProfileIdPropertyName
	// -------------------------------------
	private static final String repricePrimProfileIdPropertyName = "repricePrimProfileId";

	public static String getRepricePrimProfileIdPropertyName() {
		return repricePrimProfileIdPropertyName;
	}
	
    // -------------------------------------
	// property: SiteURLPropertyName
	// -------------------------------------
	private static final String siteURLPropertyName = "siteURL";

	public static String getSiteURLPropertyName() {
		return siteURLPropertyName;
	}
	
    // -------------------------------------
	// property: SiteURLPropertyName
	// -------------------------------------
	private static final String shareWLURIPropertyName = "shareWLURI";

	public static String getShareWLURIPropertyName() {
		return shareWLURIPropertyName;
	}	
	
    // -------------------------------------
	// property: SiteURLPropertyName
	// -------------------------------------
	private static final String forgotPasswordURIPropertyName = "forgotPasswordURI"; //NOSONAR

	public static String getForgotPasswordURIPropertyName() {
		return forgotPasswordURIPropertyName;
	}

	// -------------------------------------
	// property: CreateAccountURLPropertyName
	// -------------------------------------
	private static final String createAccountURIPropertyName = "createAccountURI"; //NOSONAR

	public static String getCreateAccountURIPropertyName() {
		return createAccountURIPropertyName;
	}

    // -------------------------------------
	// property: SiteURLPropertyName
	// -------------------------------------
	private static final String storeLocatorURIPropertyName = "storeLocatorURI";

	public static String getStoreLocatorURIPropertyName() {
		return storeLocatorURIPropertyName;
	}	
	
    // -------------------------------------
	// property: SignInURIPropertyName
	// -------------------------------------
	private static final String signInURIPropertyName = "signInURI";

	public static String getSignInURIPropertyName() {
		return signInURIPropertyName;
	}	
	
    // -------------------------------------
	// property: EmailSignUpURIPropertyName
	// -------------------------------------
	private static final String emailSignUpURIPropertyName = "emailSignUpURI";

	public static String getEmailSignUpURIPropertyName() {
		return emailSignUpURIPropertyName;
	}
	
	// -------------------------------------
	// property: CreditCardMaxLimitPropertyName
	// -------------------------------------
	private static final String creditCardMaxLimitPropertyName = "creditCardMaxLimit";

	public static String getCreditCardMaxLimitPropertyName() {
		return creditCardMaxLimitPropertyName;
	}
	
	// -------------------------------------
	// property: ShippingAddressMaxLimitPropertyName
	// -------------------------------------
	private static final String shippingAddressMaxLimitPropertyName = "shippingAddressMaxLimit";

	public static String getShippingAddressMaxLimitPropertyName() {
		return shippingAddressMaxLimitPropertyName;
	}

	private static final String rewardsCertExpiringSoonDaysPropertyName = "rewardsCertExpiringSoonDays";
	private static final String rewardsRibbonOrderPropertyName = "rewardsRibbonOrder";
	private static final String rewardsGoldTierQualifierDollarPropertyName = "rewardsGoldTierQualifierDollar";
	private static final String rewardsEliteTierQualifierDollarPropertyName = "rewardsEliteTierQualifierDollar";
	private static final String rewards2XPointsOneDiscountCodePropertyName = "rewards2XPointsOneDiscountCode";
	private static final String rewards2XPointsTwoDiscountCodePropertyName = "rewards2XPointsTwoDiscountCode";
	private static final String rewards3XPointsDiscountCodePropertyName = "rewards3XPointsDiscountCode";
	private static final String rewardsBirthdayClubDiscountCodePropertyName = "rewardsBirthdayClubDiscountCode";
	private static final String rewardsBirthdayGoldDiscountCodePropertyName = "rewardsBirthdayGoldDiscountCode";
	private static final String rewardsBirthdayEliteDiscountCodePropertyName = "rewardsBirthdayEliteDiscountCode";

	public static String getRewardsCertExpiringSoonDaysPropertyName() {
		return rewardsCertExpiringSoonDaysPropertyName;
	}

	public static String getRewardsRibbonOrderPropertyName() {
		return rewardsRibbonOrderPropertyName;
	}

	public static String getRewardsGoldTierQualifierDollarPropertyName() {
		return rewardsGoldTierQualifierDollarPropertyName;
	}

	public static String getRewardsEliteTierQualifierDollarPropertyName() {
		return rewardsEliteTierQualifierDollarPropertyName;
	}

	public static String getRewards2XPointsOneDiscountCodePropertyName() {
		return rewards2XPointsOneDiscountCodePropertyName;
	}

	public static String getRewards2XPointsTwoDiscountCodePropertyName() {
		return rewards2XPointsTwoDiscountCodePropertyName;
	}

	public static String getRewards3XPointsDiscountCodePropertyName() {
		return rewards3XPointsDiscountCodePropertyName;
	}

	public static String getRewardsBirthdayClubDiscountCodePropertyName() {
		return rewardsBirthdayClubDiscountCodePropertyName;
	}

	public static String getRewardsBirthdayGoldDiscountCodePropertyName() {
		return rewardsBirthdayGoldDiscountCodePropertyName;
	}

	public static String getRewardsBirthdayEliteDiscountCodePropertyName() {
		return rewardsBirthdayEliteDiscountCodePropertyName;
	}
}
