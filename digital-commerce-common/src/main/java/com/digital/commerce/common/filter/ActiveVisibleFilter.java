package com.digital.commerce.common.filter;

import java.util.Collection;
import java.util.Iterator;

import atg.commerce.catalog.CatalogServices;
import atg.commerce.catalog.CatalogTools;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.collections.filter.CachedCollectionFilter;
import atg.service.collections.filter.FilterException;
import lombok.Getter;
import lombok.Setter;

/** This filter will filter products in the collection based on their inventory
 * availability.
 * <p>
 * The availability state for each sku is checked against the InventoryManager. If any of a product's skus are considered available, the
 * product is considered available and is added to the filtered results.
 * <p>
 * The inventory states that mean a sku is available are configurable using the <tt>inclludeInventoryStates</tt> property. See the
 * InventoryManager for valid states.
 * <p>
 * The InventoryFilter cache should be disabled if your Inventory Manager is already caching inventory status information.
 * 
 * @see atg.commerce.inventory.InventoryManager
 * @author Jim Fecteau
 * @version $Id: //product/DCS/main/Java/atg/commerce/collections/filter/InventoryFilter.java#6 $$Change: 414597 $
 * @updated $DateTime: 2007/03/29 18:54:00 $$Author: aking $ */

@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class ActiveVisibleFilter extends CachedCollectionFilter

{

	protected CatalogTools catalogTools;
	protected CatalogServices	catalogServices;

	/** @see atg.service.collections.filter.CachedCollectionFilter#shouldApplyFilter
	 * @return true by default */
	public boolean shouldApplyFilter( Collection pUnfilteredCollection, String pKey, RepositoryItem pProfile ) {
		return true;
	}

	/** This filter does not produce a context key.
	 * 
	 * @return null. This implementation of the InventoryFilter does not use a context
	 *         key. */
	public Object generateContextKey( Collection pUnfilteredCollection, String pCollectionIdentifierKey, RepositoryItem pProfile ) {
		return null;
	}

	/** Generates a filtered collection based on Inventory availabilty.
	 * <p>
	 * A product is included in the filtered collection if it does not contain any skus, or any of its skus is considered available.
	 * <p>
	 * The availabilty state of the sku must be one of the preconfigured include states.
	 * <p>
	 * If the new filtered collection is the same size as the unfiltered collection, the filtered collection is disgarded and the unfiltered
	 * collection is returned.
	 * 
	 * @param pUnfilteredCollection the unfiltered collection
	 * @param pCollectionIdentifierKey the key provided by the caller.
	 * @return the filtered collection */
	protected Collection generateFilteredCollection( Collection pUnfilteredCollection, String pCollectionIdentifierKey, RepositoryItem pProfile ) throws FilterException {

		Collection returnCollection = generateNewCollectionObject( pUnfilteredCollection );

		try {
			CatalogTools catalogTools = getCatalogTools();

			Iterator unfilterator = pUnfilteredCollection.iterator();
			while( unfilterator.hasNext() ) {
				Object nextObject = unfilterator.next();
				if( !catalogTools.isProduct( nextObject ) ) {
					// item in not a product
					// add it to the return collection and continue.
					returnCollection.add( nextObject );
					continue;
				}

				RepositoryItem nextItem = (RepositoryItem)nextObject;
				if( isLoggingDebug() ) logDebug( "generateFilteredCollection: nextItem is " + nextItem );

				Boolean isActive = (Boolean)nextItem.getPropertyValue( "isActive" );
				Boolean isVisible = (Boolean)nextItem.getPropertyValue( "isVisible" );
				String inStock = (String)nextItem.getPropertyValue( "inStock" );
				if( inStock == null ) inStock = "N";
				if( Boolean.TRUE.equals( isActive ) && Boolean.TRUE.equals( isVisible ) && inStock.equalsIgnoreCase( "Y" ) ) {
					returnCollection.add( nextObject );
				}
			}// end while products

			// if we didn't filter anything out, just return
			// the orig. collection. this will optimize the cleanup of the new collection
			// object.
			if( returnCollection.size() == pUnfilteredCollection.size() )
				return pUnfilteredCollection;
			else
				return returnCollection;
		} catch( RepositoryException  | ClassCastException exc ) {
			throw new FilterException( exc );
		}
	}

}
