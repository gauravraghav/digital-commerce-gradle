package com.digital.commerce.common.admin.security;

import java.util.Map;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.core.util.Base64;
import atg.repository.RepositoryItem;
import atg.security.PBKDF2PasswordHasher;
import atg.security.PasswordHasher;
import atg.servlet.ServletUtil;
@SuppressWarnings("rawtypes") 
public class DigitalPreviousNPasswordManagerAdminImpl extends
		atg.security.PreviousNPasswordManagerAdminImpl {

	public boolean previousPasswordsContains(String pClearTextPassword,
			Map pParam) {
		boolean contains = false;

		if (ServletUtil.getCurrentRequest() == null) {
			return contains;
		}
		String auth = ServletUtil.getCurrentRequest()
				.getHeader("Authorization");
		if (auth == null)
			return contains;
		int ix = auth.indexOf(' ');
		if (ix >= 0) {
			String authscheme = auth.substring(0, ix);
			if ("Basic".equalsIgnoreCase(authscheme)) {
				String authval = auth.substring(ix + 1);
				String plain = Base64.decodeToString(authval);
				int ix2 = plain.indexOf(':');
				if (ix2 >= 0) {
					String username = plain.substring(0, ix2);
					if (username == null) {
						username = "";
					}
					RepositoryItem ri = getAdminAccountManager()
							.getAccountItem(
									username,
									getAdminAccountManager()
											.getLoginDescriptorName());
					if (ri == null) {
						if (isLoggingDebug()) {
							logDebug("User "
									+ username
									+ " not found.  Exiting previousPasswordsContains check.");
						}
						return false;
					}
					String[] prevPasswords = (String[]) ri
							.getPropertyValue(getAdminAccountManager()
									.getPreviousNPasswordArrayPropertyName());
					PasswordHasher hasher = (PBKDF2PasswordHasher) (getAdminAccountManager()
							.getPasswordHasher());
					String encryptedPassword = null;
					if (hasher instanceof PBKDF2PasswordHasher) {
						PBKDF2PasswordHasher pwdhasher = (PBKDF2PasswordHasher) hasher;
						encryptedPassword = pwdhasher.encryptPassword(username,
								pClearTextPassword);
					}
					int i = 0;
					if (null!=encryptedPassword && !DigitalStringUtil.isEmpty(encryptedPassword))
						while ((i < prevPasswords.length)
								&& (i < getPreviousN())) {
							if (encryptedPassword.equals(prevPasswords[i])) {
								contains = true;
							}
							i++;
						}
				}
			}
		}
		return contains;
	}
}
