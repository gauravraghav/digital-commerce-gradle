package com.digital.commerce.common.scheduledrequest;

import atg.nucleus.GenericService;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduledRequestStartupService extends GenericService {

	private ScheduledRequestManager	scheduledRequestManager;

	public void doStartService() {
		if( isLoggingDebug() ) {
			logDebug( "Start reading persisted scheduled requests" );
		}

		getScheduledRequestManager().prepareOnStartup();

		if( isLoggingInfo() ) {
			logInfo( "Finished reading persisted scheduled requests" );
		}
	}
}
