package com.digital.commerce.common.userprofiling;

import java.util.List;
import java.util.Map;

import javax.mail.Message;

import atg.repository.RepositoryItem;
import atg.service.email.examiner.EmailExaminer;
import atg.targeting.Targeter;
import atg.userprofiling.dms.DPSMessageSource;
import atg.userprofiling.email.TemplateEmailEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalMessageSource extends DPSMessageSource {
	private boolean adminProfilePropertyUpdateMessageEnabled = true;
	private boolean adminProfileUpdateMessageEnabled = true;
	private boolean adminRegisterMessageEnabled = true;
	private boolean clickThroughMessageEnabled = true;
	private boolean endSessionMessageEnabled = true;
	private boolean inboundEmailMessageEnabled = true;
	private boolean loginMessageEnabled = true;
	private boolean logoutMessageEnabled = true;
	private boolean outboundEmailMessageEnabled = true;
	private boolean pageVisitMessageEnabled = true;
	private boolean profilePropertyUpdateMessageEnabled = true;
	private boolean profileUpdateMessageEnabled = true;
	private boolean referrerMessageEnabled = true;
	private boolean registerMessageEnabled = true;
	private boolean startSessionMessageEnabled = true;
	private boolean viewItemMessageEnabled = true;

	@Override
	public void fireViewItemMessage(String pRepositoryName, String pItemType, String pRepositoryId, String pFolder,
			RepositoryItem pItem, Targeter pTargeter, RepositoryItem pProfile) {
		if (viewItemMessageEnabled) {
			super.fireViewItemMessage(pRepositoryName, pItemType, pRepositoryId, pFolder, pItem, pTargeter, pProfile);
		}
	}

	@Override
	public void fireViewItemMessage(String pRepositoryName, String pItemType, String pRepositoryId, String pFolder,
			RepositoryItem pItem, Targeter pTargeter, RepositoryItem pProfile, String pPage) {
		if (viewItemMessageEnabled) {
			super.fireViewItemMessage(pRepositoryName, pItemType, pRepositoryId, pFolder, pItem, pTargeter, pProfile,
					pPage);
		}
	}

	@Override
	public void fireViewItemMessage(String pRepositoryName, String pItemType, String pRepositoryId, String pFolder,
			RepositoryItem pItem, Targeter pTargeter, RepositoryItem pProfile, String pSessionId,
			String pParentSessionId) {
		if (viewItemMessageEnabled) {
			super.fireViewItemMessage(pRepositoryName, pItemType, pRepositoryId, pFolder, pItem, pTargeter, pProfile,
					pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireViewItemMessage(String pRepositoryName, String pItemType, String pRepositoryId, String pFolder,
			RepositoryItem pItem, Targeter pTargeter, RepositoryItem pProfile, String pPage, String pSessionId,
			String pParentSessionId) {
		if (viewItemMessageEnabled) {
			super.fireViewItemMessage(pRepositoryName, pItemType, pRepositoryId, pFolder, pItem, pTargeter, pProfile,
					pPage, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void firePageVisitMessage(String pPath, RepositoryItem pProfile) {
		if (pageVisitMessageEnabled) {
			super.firePageVisitMessage(pPath, pProfile);
		}
	}

	@Override
	public void firePageVisitMessage(String pPath, String pScenarioPathInfo, RepositoryItem pProfile) {
		if (pageVisitMessageEnabled) {
			super.firePageVisitMessage(pPath, pScenarioPathInfo, pProfile);
		}
	}

	@Override
	public void firePageVisitMessage(String pPath, String pScenarioPathInfo, RepositoryItem pProfile, String pSessionId,
			String pParentSessionId) {
		if (pageVisitMessageEnabled) {
			super.firePageVisitMessage(pPath, pScenarioPathInfo, pProfile, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireClickThroughMessage(String pSourceURL, String pDestinationURL, String pSourceScenarioPathInfo,
			String pDestinationScenarioPathInfo, String[] pSourceNames, RepositoryItem pProfile) {
		if (clickThroughMessageEnabled) {
			super.fireClickThroughMessage(pSourceURL, pDestinationURL, pSourceScenarioPathInfo,
					pDestinationScenarioPathInfo, pSourceNames, pProfile);
		}
	}

	@Override
	public void fireClickThroughMessage(String pSourceURL, String pDestinationURL, String[] pSourceNames,
			String pProfileId) {
		if (clickThroughMessageEnabled) {
			super.fireClickThroughMessage(pSourceURL, pDestinationURL, pSourceNames, pProfileId);
		}
	}

	@Override
	public void fireClickThroughMessage(String pSourceURL, String pDestinationURL, String pSourceScenarioPathInfo,
			String pDestinationScenarioPathInfo, String[] pSourceNames, String pProfileId) {
		if (clickThroughMessageEnabled) {
			super.fireClickThroughMessage(pSourceURL, pDestinationURL, pSourceScenarioPathInfo,
					pDestinationScenarioPathInfo, pSourceNames, pProfileId);
		}
	}

	@Override
	public void fireClickThroughMessage(String pSourceURL, String pDestinationURL, String pSourceScenarioPathInfo,
			String pDestinationScenarioPathInfo, String[] pSourceNames, String pProfileId, String pSessionId,
			String pParentSessionId) {
		if (clickThroughMessageEnabled) {
			super.fireClickThroughMessage(pSourceURL, pDestinationURL, pSourceScenarioPathInfo,
					pDestinationScenarioPathInfo, pSourceNames, pProfileId, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireClickThroughMessage(String pSourceURL, String pDestinationURL, String pSourceScenarioPathInfo,
			String pDestinationScenarioPathInfo, String[] pSourceNames, RepositoryItem pProfile, String pSessionId,
			String pParentSessionId) {
		if (clickThroughMessageEnabled) {
			super.fireClickThroughMessage(pSourceURL, pDestinationURL, pSourceScenarioPathInfo,
					pDestinationScenarioPathInfo, pSourceNames, pProfile, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireInboundEmailMessage(Message pMessage, boolean pBounced) {
		if (inboundEmailMessageEnabled) {
			super.fireInboundEmailMessage(pMessage, pBounced);
		}
	}

	@Override
	public void fireInboundEmailMessage(Message pMessage, boolean pBounced, EmailExaminer pExaminer) {
		if (inboundEmailMessageEnabled) {
			super.fireInboundEmailMessage(pMessage, pBounced, pExaminer);
		}
	}

	@Override
	public void fireOutboundEmailMessage(TemplateEmailEvent pEvent) {
		if (outboundEmailMessageEnabled) {
			super.fireOutboundEmailMessage(pEvent);
		}
	}

	@Override
	public void fireStartSessionMessage(String pProfileId) {
		if (startSessionMessageEnabled) {
			super.fireStartSessionMessage(pProfileId);
		}
	}

	@Override
	public void fireStartSessionMessage(String pProfileId, String pSessionId) {
		if (startSessionMessageEnabled) {
			super.fireStartSessionMessage(pProfileId, pSessionId);
		}
	}

	@Override
	public void fireStartSessionMessage(String pProfileId, String pSessionId, String pScenarioPathInfo) {
		if (startSessionMessageEnabled) {
			super.fireStartSessionMessage(pProfileId, pSessionId, pScenarioPathInfo);
		}
	}

	@Override
	public void fireStartSessionMessage(String pProfileId, String pSessionId, String pParentSessionId,
			String scenarioPathInfo) {
		if (startSessionMessageEnabled) {
			super.fireStartSessionMessage(pProfileId, pSessionId, pParentSessionId, scenarioPathInfo);
		}
	}

	@Override
	public void fireEndSessionMessage(RepositoryItem pProfile, String pSessionId) {
		if (endSessionMessageEnabled) {
			super.fireEndSessionMessage(pProfile, pSessionId);
		}
	}

	@Override
	public void fireEndSessionMessage(RepositoryItem pProfile, String pSessionId, String pParentSessionId) {
		if (endSessionMessageEnabled) {
			super.fireEndSessionMessage(pProfile, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireLoginMessage(String pProfileId) {
		if (loginMessageEnabled) {
			super.fireLoginMessage(pProfileId);
		}
	}

	@Override
	public void fireLoginMessage(String pProfileId, String pSessionId, String pParentSessionId) {
		if (loginMessageEnabled) {
			super.fireLoginMessage(pProfileId, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireLoginMessage(RepositoryItem pProfile, String pSessionId, String pParentSessionId) {
		if (loginMessageEnabled) {
			super.fireLoginMessage(pProfile, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireLogoutMessage(String pProfileId) {
		if (logoutMessageEnabled) {
			super.fireLogoutMessage(pProfileId);
		}
	}

	@Override
	public void fireLogoutMessage(String pProfileId, String pSessionId, String pParentSessionId) {
		if (logoutMessageEnabled) {
			super.fireLogoutMessage(pProfileId, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireLogoutMessage(RepositoryItem pProfile, String pSessionId, String pParentSessionId) {
		if (logoutMessageEnabled) {
			super.fireLogoutMessage(pProfile, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireRegisterMessage(String pProfileId) {
		if (registerMessageEnabled) {
			super.fireRegisterMessage(pProfileId);
		}
	}

	@Override
	public void fireRegisterMessage(String pProfileId, String pSessionId, String pParentSessionId) {
		if (registerMessageEnabled) {
			super.fireRegisterMessage(pProfileId, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireRegisterMessage(RepositoryItem pProfile, String pSessionId, String pParentSessionId) {
		if (registerMessageEnabled) {
			super.fireRegisterMessage(pProfile, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireAdminRegisterMessage(String pAdminProfileId, String pProfileId) {
		if (adminRegisterMessageEnabled) {
			super.fireAdminRegisterMessage(pAdminProfileId, pProfileId);
		}
	}

	@Override
	public void fireAdminRegisterMessage(String pAdminProfileId, RepositoryItem pProfile) {
		if (adminRegisterMessageEnabled) {
			super.fireAdminRegisterMessage(pAdminProfileId, pProfile);
		}
	}

	@Override
	public void fireProfilePropertyUpdateMessage(String pPropertyPath, Object pOldValue, Object pNewValue,
			int pChangeSign, double pChangeAmount, double pChangePercentage, Object[] pElementsAdded,
			Object[] pElementsRemoved, String pProfileId) {
		if (profilePropertyUpdateMessageEnabled) {
			super.fireProfilePropertyUpdateMessage(pPropertyPath, pOldValue, pNewValue, pChangeSign, pChangeAmount,
					pChangePercentage, pElementsAdded, pElementsRemoved, pProfileId);
		}
	}

	@Override
	public void fireProfilePropertyUpdateMessage(String pPropertyPath, Object pOldValue, Object pNewValue,
			int pChangeSign, double pChangeAmount, double pChangePercentage, Object[] pElementsAdded,
			Object[] pElementsRemoved, RepositoryItem pProfile) {
		if (profilePropertyUpdateMessageEnabled) {
			super.fireProfilePropertyUpdateMessage(pPropertyPath, pOldValue, pNewValue, pChangeSign, pChangeAmount,
					pChangePercentage, pElementsAdded, pElementsRemoved, pProfile);
		}
	}

	@Override
	public void fireAdminProfilePropertyUpdateMessage(String pPropertyPath, Object pOldValue, Object pNewValue,
			int pChangeSign, double pChangeAmount, double pChangePercentage, Object[] pElementsAdded,
			Object[] pElementsRemoved, String pAdminProfileId, String pProfileId) {
		if (adminProfilePropertyUpdateMessageEnabled) {
			super.fireAdminProfilePropertyUpdateMessage(pPropertyPath, pOldValue, pNewValue, pChangeSign, pChangeAmount,
					pChangePercentage, pElementsAdded, pElementsRemoved, pAdminProfileId, pProfileId);
		}
	}

	@Override
	public void fireAdminProfilePropertyUpdateMessage(String pPropertyPath, Object pOldValue, Object pNewValue,
			int pChangeSign, double pChangeAmount, double pChangePercentage, Object[] pElementsAdded,
			Object[] pElementsRemoved, String pAdminProfileId, RepositoryItem pProfile) {
		if (adminProfilePropertyUpdateMessageEnabled) {
			super.fireAdminProfilePropertyUpdateMessage(pPropertyPath, pOldValue, pNewValue, pChangeSign, pChangeAmount,
					pChangePercentage, pElementsAdded, pElementsRemoved, pAdminProfileId, pProfile);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void fireProfileUpdateMessage(List pChangedProperties, Map pOldValues, Map pNewValues,
			RepositoryItem pProfile) {
		if (profileUpdateMessageEnabled) {
			super.fireProfileUpdateMessage(pChangedProperties, pOldValues, pNewValues, pProfile);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void fireProfileUpdateMessage(List pChangedProperties, Map pOldValues, Map pNewValues, String pProfileId) {
		if (profileUpdateMessageEnabled) {
			super.fireProfileUpdateMessage(pChangedProperties, pOldValues, pNewValues, pProfileId);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void fireAdminProfileUpdateMessage(List pChangedProperties, Map pOldValues, Map pNewValues,
			String pAdminProfileId, String pProfileId) {
		if (adminProfileUpdateMessageEnabled) {
			super.fireAdminProfileUpdateMessage(pChangedProperties, pOldValues, pNewValues, pAdminProfileId,
					pProfileId);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void fireAdminProfileUpdateMessage(List pChangedProperties, Map pOldValues, Map pNewValues,
			String pAdminProfileId, RepositoryItem pProfile) {
		if (adminProfileUpdateMessageEnabled) {
			super.fireAdminProfileUpdateMessage(pChangedProperties, pOldValues, pNewValues, pAdminProfileId, pProfile);
		}
	}

	@Override
	public void fireReferrerMessage(String pReferrerURL, String pProfileId) {
		if (referrerMessageEnabled) {
			super.fireReferrerMessage(pReferrerURL, pProfileId);
		}
	}

	@Override
	public void fireReferrerMessage(String pReferrerURL, String pProfileId, String pSessionId,
			String pParentSessionId) {
		if (referrerMessageEnabled) {
			super.fireReferrerMessage(pReferrerURL, pProfileId, pSessionId, pParentSessionId);
		}
	}

	@Override
	public void fireReferrerMessage(String pReferrerURL, RepositoryItem pProfile, String pSessionId,
			String pParentSessionId) {
		if (referrerMessageEnabled) {
			super.fireReferrerMessage(pReferrerURL, pProfile, pSessionId, pParentSessionId);
		}
	}
}
