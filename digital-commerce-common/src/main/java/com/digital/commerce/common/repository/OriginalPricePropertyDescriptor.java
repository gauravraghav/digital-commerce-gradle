package com.digital.commerce.common.repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.services.pricing.DigitalPriceListManager;
import com.digital.commerce.common.util.ComponentLookupUtil;

import atg.commerce.pricing.priceLists.PriceListException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;


@SuppressWarnings({"rawtypes"})
public class OriginalPricePropertyDescriptor extends RepositoryPropertyDescriptor {
	
	private static final long		serialVersionUID		= -6258342473415966033L;
	protected static final String	TYPE_NAME				= "OriginalPricePropertyDescriptor";
	private static final DigitalLogger		logger					= DigitalLogger.getLogger( OriginalPricePropertyDescriptor.class );

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, OriginalPricePropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {

		// Declare all required variable
		Double orginalPriceDb = null;
		RepositoryItem priceItemRepo = null;
		RepositoryItem basePriceList = null;
		RepositoryItem salePriceList = null;
		RepositoryItem tempPriceList = null;
		RepositoryItem product = null;
		double baseOriginalPrice = Double.MAX_VALUE;
		double baseListPrice = Double.MAX_VALUE;
		double saleListPrice = Double.MAX_VALUE;


		// Get Base and Sale Price List assigned to the current user profile
		DigitalPriceListManager priceListManager =  (DigitalPriceListManager)ComponentLookupUtil.lookupComponent(ComponentLookupUtil.PRICE_LIST_MANAGER, DigitalPriceListManager.class);

		Collection plists = null;
		if( priceListManager != null ) {
			try {
					plists = priceListManager.getPriceLists();
					Iterator i = plists.iterator();
					while( i.hasNext() ) {
						tempPriceList = (RepositoryItem)i.next();
						String displayName = (String)tempPriceList.getPropertyValue(priceListManager.getDisplayPropertyName());
						if( basePriceList == null && displayName.equalsIgnoreCase(priceListManager.getBasePriceListDisplayPropertyName()) ) {
							basePriceList = tempPriceList;
						}
						if( salePriceList == null && displayName.equalsIgnoreCase(priceListManager.getSalePriceListDisplayPropertyName())) {
							salePriceList = tempPriceList;
						}
					}
				}catch( PriceListException e ) {
					if(logger.isDebugEnabled()){
						logger.error( "No Price Lists found" );
					}
				}
		}

		// Get Parent Product
		Set parentProducts = (Set)pItem.getPropertyValue(priceListManager.getParentProductsPropertyName());
		if( parentProducts != null && parentProducts.size() > 0 ) product = (RepositoryItem)parentProducts.toArray()[0];
		
		if( product != null && pItem != null ) {
			try {
				// get the original and list price from basePriceList 
				if( basePriceList != null ) {
					priceItemRepo = priceListManager.getPrice(basePriceList, null, (String)pItem.getPropertyValue( "id" ) );
					if( priceItemRepo != null ) {
						Object tempOriginalPrice = priceItemRepo.getPropertyValue(priceListManager.getOriginalPricePropertyName());
						if(tempOriginalPrice !=null){
							baseOriginalPrice = (double)tempOriginalPrice;
						}
						Object tempListPrice = priceItemRepo.getPropertyValue(priceListManager.getListPricePropertyName());
						if(tempListPrice !=null){
							baseListPrice = (double)tempListPrice;
						}
						
					}
				}
				// get the sale price list from salePrile List for the passed in product and sku
				if(salePriceList != null ) {
					priceItemRepo = priceListManager.getPrice( salePriceList, (String)product.getPropertyValue( "id" ), (String)pItem.getPropertyValue( "id" ) );
					if( priceItemRepo != null ) {
						Object temp = priceItemRepo.getPropertyValue(priceListManager.getListPricePropertyName());
						if(temp !=null){
							saleListPrice = (double)temp;
						}
					}
				}

				//If we find  both saleListPrice & baseListPrice then consider baseListPrice as the original price
				if( saleListPrice != Double.MAX_VALUE && baseListPrice != Double.MAX_VALUE) {
					orginalPriceDb = Double.valueOf( baseListPrice );
				}else if(baseOriginalPrice != Double.MAX_VALUE){
					orginalPriceDb = Double.valueOf( baseOriginalPrice );
				}else{
					//Making null
					orginalPriceDb = null;	
				}
/*					if(logger.isDebugEnabled()){
						logger.debug("Original Price for "+(String)pItem.getPropertyValue( "id" ) +":  "+orginalPriceDb);
					
					}
*/					setPriceProperty( pItem, orginalPriceDb );
				
			} catch( Exception e ) {
				if(logger.isDebugEnabled()){
					logger.error( "Price list exception caught: ", e );
				}
			}
		}
		return orginalPriceDb;
	}

	private void setPriceProperty( RepositoryItemImpl item, Double price ) {
		item.setPropertyValueInCache( this, price );
	}	

}
