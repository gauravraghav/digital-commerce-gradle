package com.digital.commerce.common.util;

import org.apache.commons.lang.math.NumberUtils;

public class DigitalNumberUtil extends NumberUtils {

  /**
   * @param str
   * @return true or false
   */
  public static boolean isNumber(final String str) {
    return NumberUtils.isNumber(str);
  }
}
