package com.digital.commerce.common.services.repricing;

import java.rmi.RemoteException;

import com.digital.commerce.common.exception.RepricingServicesException;

public interface IRepricingServices {

	public String repriceOrder( String pOldOrder ) throws RemoteException, RepricingServicesException;

}