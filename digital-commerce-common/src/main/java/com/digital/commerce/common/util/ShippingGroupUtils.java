package com.digital.commerce.common.util;

import java.util.List;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupRelationship;

public class ShippingGroupUtils {
	
	private static final DigitalLogger LOGGER = DigitalLogger.getLogger(ShippingGroupUtils.class);

	/**
	 * Helper method to find the BOPIS store Id for a line item
	 * 
	 * @param lineItem
	 * @return The bopis store id if found, null otherwise
	 */
	@SuppressWarnings("unchecked")
	public static final String getBopisStoreId(final CommerceItem lineItem) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getBopisStoreId: " + lineItem);
		}
		if (lineItem != null) {
			final List<ShippingGroupRelationship> shippingGroupRels = lineItem.getShippingGroupRelationships();
			for (final ShippingGroupRelationship shipGrpRel : shippingGroupRels) {
				final ShippingGroup shipGroup = shipGrpRel.getShippingGroup();
				if (shipGroup instanceof HardgoodShippingGroup) {
					HardgoodShippingGroup hardGoodShipGroup = (HardgoodShippingGroup) shipGroup;
					final String shipType = (String) hardGoodShipGroup.getPropertyValue("shipType");
					if (shipType != null && shipType.equalsIgnoreCase("bopis")) {
						final String storeId = (String) hardGoodShipGroup.getPropertyValue("storeId");
						if (storeId != null) {
							if (LOGGER.isDebugEnabled()) {
								LOGGER.debug("Found Store Id: " + storeId + " for line item: " + lineItem);
							}
							return storeId;
						}
					}
				}
			}
		}
		return null;
	}

}
