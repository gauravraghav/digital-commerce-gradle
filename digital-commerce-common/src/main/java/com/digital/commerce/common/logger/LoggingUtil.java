package com.digital.commerce.common.logger;

import java.net.URLDecoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.digital.commerce.common.services.DigitalBaseConstants;

public class LoggingUtil {
	private static final String FORWARD_PATH_KEY = "javax.servlet.forward.request_uri";
	private static final String JSESSION_ID = "JSESSIONID";
	private static final String COMMA_DELIMITER = ", ";
	private static final String REQUEST_URI = "REQUEST_URI";
	private static final String QUERY_STRING = "QUERY_STRING";
	private static final String UNIQUE_ID_KEY = "UNIQUE_ID";
	private static final String SOURCE_IP_KEY = "SOURCE_IP";
	private static final String CLIENT_SOURCE_IP_KEY = "Client-Source-IP";

	public static String getRequestInfo(final HttpServletRequest request) {
		if (request == null) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		// original request URI
		sb.append(REQUEST_URI).append(":").append(request.getRequestURI())
				.append(COMMA_DELIMITER);

		sb.append("METHOD").append(":").append(request.getMethod())
				.append(COMMA_DELIMITER);

		// forwarded path
		String forwardPath = (null == request.getAttribute(FORWARD_PATH_KEY)) ?  "" : (String)request.getAttribute(FORWARD_PATH_KEY);
		sb.append("FORWARDED_PATH").append(":")
				.append(forwardPath).append(COMMA_DELIMITER);
		// query string
		final String queryString = request.getQueryString();
		String decQS = "";
		if (queryString != null) {
			decQS = queryString;
			try {
				decQS = URLDecoder.decode(queryString, DigitalBaseConstants.ENCODING);
			} catch (Exception ex) {
				// do nothing as its not a big deal
			}
		}
		sb.append(QUERY_STRING).append(":").append(decQS).append(COMMA_DELIMITER);
		
		// get JSESSIONID cookie
		final Cookie[] cookies = request.getCookies();
		String jsessionId = "";
		if (null != cookies) {
			for (final Cookie c : cookies) {
				if (c.getName().equalsIgnoreCase(JSESSION_ID)) {
					jsessionId = c.getValue();
					break;
				}
			}
		}
		sb.append(JSESSION_ID).append(":").append(jsessionId).append(COMMA_DELIMITER);
		// get uniqueid from Apache
		String apacheId = (null == request.getHeader(UNIQUE_ID_KEY)) ?  "" : request.getHeader(UNIQUE_ID_KEY);
		sb.append("APACHE_UNIQUE_ID").append(":")
				.append(apacheId)
				.append(COMMA_DELIMITER);

		// get source-ip from Apache. Concern from Security Audit
		String sourceIp = (null == request.getHeader(SOURCE_IP_KEY)) ?  "" : request.getHeader(SOURCE_IP_KEY);
		sb.append(SOURCE_IP_KEY).append(":")
				.append(sourceIp)
				.append(COMMA_DELIMITER);

		// get source-ip from Apache. Concern from Security Audit
		String clientSourceIp = (null == request.getHeader(CLIENT_SOURCE_IP_KEY)) ?  "" : request.getHeader(CLIENT_SOURCE_IP_KEY);
		sb.append(CLIENT_SOURCE_IP_KEY).append(":")
				.append(clientSourceIp)
				.append(COMMA_DELIMITER);
		return sb.toString();
	}
}
