package  com.digital.commerce.common.repository;


import java.util.Enumeration;

import atg.repository.RepositoryItem;

@SuppressWarnings({"rawtypes"})
public class RepositoryItemEnumeration implements Enumeration {
	private int					curElement	= 0;

	private RepositoryItem[]	items;

	public RepositoryItemEnumeration( RepositoryItem[] array ) {
		items = array;
	}

	/* (non-Javadoc)
	 * @see java.util.Enumeration#hasMoreElements() */
	public boolean hasMoreElements() {
		return items.length > curElement;
	}

	/* (non-Javadoc)
	 * @see java.util.Enumeration#nextElement() */
	public Object nextElement() {
		return items[curElement++].getRepositoryId();

	}

}
