package com.digital.commerce.common.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Principal;
import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Level;

import com.digital.commerce.common.logger.DigitalLogger;
import com.google.common.collect.Iterables;

import atg.droplet.DropletException;
import atg.nucleus.Nucleus;
import atg.service.dynamo.Configuration;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

/** Utility for dealing with errors during a servlet request.
 * 
 * @author <a href="mailto:knaas@dswinc.com">knaas</a> */
@SuppressWarnings({"rawtypes","unchecked"})
public class ServletErrorUtils {
	private static final DigitalLogger	logger			= DigitalLogger.getLogger( ServletErrorUtils.class );

	private static final String	PARAM_ERROR_ID	= "eid";

	/** Returns the server info for the request. If the global {@link Nucleus} is
	 * available it looks up the {@link Configuration} object to determine the
	 * server. However, if it is not available, it will fall back on the request
	 * url.
	 * 
	 * @param request
	 * @return */
	public static String getServerId( ServletRequest request ) {
		String server = null;
		try {
			Nucleus nucleus = getGlobalNucleus();
			if( nucleus != null ) {
				final Object configuration = nucleus.resolveName( "/atg/dynamo/Configuration" );
				if( configuration instanceof Configuration ) {
					server = DigitalStringUtil.trimToEmpty( ( (Configuration)configuration ).getThisHostname() ) + ":" + ( (Configuration)configuration ).getAdminPort();
				}
			}
			if( server == null ) {
				if( request instanceof HttpServletRequest ) {
					URL url = null;
					try {
						url = new URL( ( (HttpServletRequest)request ).getRequestURL().toString() );
					} catch( MalformedURLException ex ) {

					}
					if( url != null ) {
						server = DigitalStringUtil.trimToEmpty( url.getHost() ) + ( url.getPort() != -1 ? ":" + String.valueOf( url.getPort() ) : "" );
					}
				}
			}
		} catch( Throwable t ) {
			logger.error( "server is unavailable ", t );
		}
		return DigitalStringUtil.trimToEmpty( server );
	}

	/** Returns the global nucleus if it is available.
	 * 
	 * @return */
	private static Nucleus getGlobalNucleus() {
		Nucleus nucleus = null;
		try {
			nucleus = Nucleus.getGlobalNucleus();
		} catch( Throwable t ) {

		}
		return nucleus;
	}

	/** Returns the original uri that the client used to request the page.
	 * Because we may be in the middle of processing an error, the request url could actually
	 * be the error page url.
	 * 
	 * @param request
	 * @return never null! */
	public static String getClientRequestURI( ServletRequest request ) {
		Object requestUri = request.getAttribute( "javax.servlet.error.request_uri" );
		String retVal = requestUri != null ? requestUri.toString() : null;
		if( retVal == null && request instanceof HttpServletRequest ) {
			requestUri = ( (HttpServletRequest)request ).getRequestURL();
			retVal = requestUri != null ? requestUri.toString() : null;
		}
		return retVal == null ? "" : retVal;
	}

	/** Returns the error id for the request.
	 * 
	 * @param request
	 * @return */
	public static String getErrorId( ServletRequest request ) {
		String errorId = null;
		if( request.getAttribute( "javax.servlet.error.exception" ) != null ) {
			errorId = UUID.randomUUID().toString();
		} else {
			errorId = DigitalStringUtil.trimToEmpty( request.getParameter( PARAM_ERROR_ID ) );
		}
		return errorId;
	}

	/** Returns if the request contains an errorid
	 * 
	 * @param request
	 * @return */
	private static boolean containsErrorId( ServletRequest request ) {
		return DigitalStringUtil.isNotBlank( request.getParameter( PARAM_ERROR_ID ) );
	}

	/** Returns true if the request accepts html. This allows us to determine
	 * whether or not to actually write an error to the response or simply
	 * swallow it up.
	 * If the original request resulted in an error and we are now evaluating an error
	 * page we attempt to find the original request.
	 * 
	 * @deprecated Use {@link HttpServletUtil#isHtmlAccepted(HttpServletRequest)} instead */
	public static boolean isHtmlAcceptable( HttpServletRequest request ) {
		return HttpServletUtil.isHtmlAccepted( request );
	}

	/** Logs the error information contained in the request. If the {@link Nucleus} is available, it uses it to write to an error. If the
	 * {@link Nucleus} is unavailable, it writes to {@link System#err} Note that
	 * it only logs if the cause is not null. Thus if the error page is being
	 * directly accessed, it does not get logged.
	 * <p/>
	 * To get a good exception we look at the request attributes in the following locations:
	 * <ol>
	 * <li>DropletExceptions</li>
	 * <li>javax.servlet.error.exception</li>
	 * <li>javax.servlet.jsp.jspException</li>
	 * </ol>
	 * This ensures that we get a nice printable stack trace. When a {@link DropletException} is the cause, ATG unwraps it in such a way
	 * that the original stack trace is invisible. This is why we look at the non standard location first.
	 * 
	 * 
	 * @param request */
	public static void log( ServletRequest request, ServletResponse response, String id ) {

		Object cause = request.getAttribute( "DropletExceptions" );
		if( cause != null ) {
			if( cause instanceof Iterable && Iterables.size( (Iterable)cause ) > 0 ) {
				cause = Iterables.get( (Iterable)cause, 0 );
			} else {
				cause = null;
			}
		}
		if( cause == null ) {
			cause = request.getAttribute( "javax.servlet.error.exception" );
		}
		if( cause == null ) {
			cause = request.getAttribute( "javax.servlet.jsp.jspException" );
		}
		final Object causeType = request.getAttribute( "javax.servlet.error.exception_type" );
		final Object message = request.getAttribute( "javax.servlet.error.message" );

		Object requestUri = request.getAttribute( "javax.servlet.error.request_uri" );
		Object statusCode = request.getAttribute( "javax.servlet.error.status_code" );
		String userName = null;
		String sessionId = null;
		boolean ajaxRequest = false;

		if( request instanceof HttpServletRequest ) {
			final HttpServletRequest httpRequest = (HttpServletRequest)request;
			ajaxRequest = "XMLHttpRequest".equalsIgnoreCase( httpRequest.getHeader( "X-Requested-With" ) );
			final Principal principal = httpRequest.getUserPrincipal();
			if( principal != null ) {
				userName = principal.getName();
			}
			if( userName == null ) {
				userName = httpRequest.getRemoteUser();
			}
			final HttpSession session = httpRequest.getSession( false );
			if( session != null ) {
				sessionId = session.getId();
			}
			if( requestUri == null ) {
				requestUri = httpRequest.getRequestURL();
			}
			String queryString = httpRequest.getQueryString();
			if( queryString != null ) {
				requestUri = requestUri + "?" + queryString;
			}
		}
		try {

			try {
				final DynamoHttpServletRequest dynamoRequest = ServletUtil.getDynamoRequest( request );

				if( dynamoRequest != null ) {
					if( statusCode == null ) {
						final DynamoHttpServletResponse dynamoResponse = ServletUtil.getDynamoResponse( dynamoRequest, response );
						if( dynamoResponse != null ) {
							statusCode = dynamoResponse.getStatus();
						}
					}

					final Object profile = dynamoRequest.resolveName( "/atg/userprofiling/Profile" );
					if( profile instanceof Profile ) {
						userName = ( (Profile)profile ).getItemDisplayName();
					}
				}
			} catch( Throwable t ) {
				logger.error( "Nucleus is unavailable " + t.getMessage(), t );
			}

			Level priority = Level.ERROR;
			if( cause instanceof IOException && ajaxRequest ) {
				priority = Level.DEBUG;
			}
			if( containsErrorId( request ) ) {
				priority = Level.DEBUG;
			}
			if( logger.isEnabledFor( priority ) ) {
				final String line = "errorId:" + ( id != null ? id : "" ) + ", requestUri:" + ( requestUri != null ? requestUri : "" ) + ", statusCode:" + ( statusCode != null ? statusCode : "" ) + ", session:"
						+ ( sessionId != null ? sessionId : "" ) + ", user:" + ( userName != null ? userName : "" ) + ", ajaxRequest:" + ajaxRequest + ", committed:" + ( response.isCommitted() ? "yes" : "no" ) + ", message:"
						+ ( message != null ? message : "" ) + ", causeType:" + ( causeType != null ? causeType : "" );
				if( cause instanceof Throwable ) {
					logger.error( line, (Throwable)cause );
				} else {
					logger.error( line + ", cause:" + ( cause != null ? cause : "" ) );
				}
			}
		} catch( Throwable t ) {
			logger.error( "Unable to log error ", t );
		}
	}
}
