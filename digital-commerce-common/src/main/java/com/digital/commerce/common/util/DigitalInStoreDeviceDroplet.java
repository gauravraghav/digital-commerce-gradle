package com.digital.commerce.common.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;

import com.digital.commerce.common.services.DigitalBaseConstants;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalInStoreDeviceDroplet extends DynamoServlet {
	private static final String CDN_CLIENT_SOURCE_IP_HEADER = "Client-Source-IP";
	private static final String LOAD_BALANCER_SOURCE_IP_HEADER = "SOURCE_IP";
	private static final String XFORWARDED_FOR_HEADER = "X-Forwarded-For";
	private static final String ISDSWINSTOREDEVICE = "isDSWInStoreDevice";
	static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	private DigitalBaseConstants constants;

	public void setConstants(DigitalBaseConstants constants) {
		this.constants = constants;
		if (this.constants != null) {
			this.ipAddresses = this.constants.getDswInStoreDeviceIPAddresses()
					.split(",");
		}
	}

	private String[] ipAddresses;

	/**
	 * Overrides the default service method and returns back a boolean named
	 * isDigitalInStoreDevice in the request. IP Addresses are configured through
	 * 
	 * @link{DigitalConstants .
	 * 
	 *                    Because there are several tiers to our application,
	 *                    Source IP is a little muddy. The code below attempts
	 *                    to get the Source IP first from the header provided by
	 *                    the CDN. If it is unavailable it falls back to the
	 *                    load balancer which should work for origin requests.
	 *                    Finally it will fall back to pulling the IP address
	 *                    directly from the request which should cover
	 *                    development and local environments.
	 * 
	 *                    <pre>
	 * {@code
	 *      <c:set var="disableCheckout" value="false" scope="request"/>
	 *      <dsp:droplet name="DigitalInStoreDeviceDroplet">
	 *          <dsp:oparam name="isDigitalInStoreDevice">
	 *              <dsp:getvalueof var="disableCheckout" param="isDSWInStoreDevice" scope="request" />
	 *          </dsp:oparam>
	 *      </dsp:droplet>
	 * }
	 * </pre>
	 * 
	 * @param req
	 * @param res
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest req,
			DynamoHttpServletResponse res) throws ServletException, IOException {
		try {
			Boolean doesIPAddressMatch = false;
			List<String> requestIPAddress = new ArrayList<>();
			String clientIp = req.getHeader(CDN_CLIENT_SOURCE_IP_HEADER);
			if (isLoggingDebug()) {
				logDebug("Client-Source-IP: " + clientIp);
			}
			if (null != clientIp) {
				requestIPAddress.add(clientIp);
				if (isLoggingDebug()) {
					logDebug("Client-Source-IP found, hence check will be done against this");
				}
			}

			if (requestIPAddress.isEmpty()) {
				clientIp = req.getHeader(LOAD_BALANCER_SOURCE_IP_HEADER);
				if (isLoggingDebug()) {
					logDebug("SOURCE_IP: " + clientIp);
				}
				if (null != clientIp) {
					String[] sourceIpList = clientIp.split(",");
					if (isLoggingDebug()) {
						logDebug("SOURCE_IP found, hence check will be done against this");
					}
					requestIPAddress.addAll(Arrays.asList(sourceIpList));
				}
			}
			if (requestIPAddress.isEmpty()) {
				clientIp = req.getHeader(XFORWARDED_FOR_HEADER);
				if (isLoggingDebug()) {
					logDebug("X-Forwarded-For: " + clientIp);
				}
				if (null != clientIp) {
					String[] xHeaders = clientIp.split(",");
					if (isLoggingDebug()) {
						logDebug("X-Forwarded-For found, hence check will be done against this");
					}
					requestIPAddress.addAll(Arrays.asList(xHeaders));
				}
			}

			// loop through each ip address
			if (ipAddresses != null && (ipAddresses.length > 0)
					&& !requestIPAddress.isEmpty()) {
				for (String ipAddress : ipAddresses) {
					if (requestIPAddress.contains(ipAddress)) {
						doesIPAddressMatch = true;
						break;
					}
				}
			}

			req.setParameter(ISDSWINSTOREDEVICE, doesIPAddressMatch);
			req.serviceLocalParameter(OUTPUT, req, res); 
		} catch (Exception ex) {
			logWarning(
					"Error while determing the device as DSW Store device, doing nothing",
					ex);
		}
	}
}
