package com.digital.commerce.common.validator;

import java.util.Date;

import com.digital.commerce.common.services.DigitalBaseConstants;

import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryItem;
import atg.service.collections.validator.CollectionObjectValidator;
import atg.service.util.CurrentDate;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalDisplayDateValidator extends ApplicationLoggingImpl implements CollectionObjectValidator {

	private CurrentDate currentDate;
	private String startDatePropertyName;
	private String endDatePropertyName;
	private DigitalBaseConstants dswConstants;

	public boolean validateObject(Object pObject) {
		if (!getDswConstants().isPreviewEnv()) {

			if (!(pObject instanceof RepositoryItem)) {
				return false;
			}

			RepositoryItem item = (RepositoryItem) pObject;
			Date current = getCurrentDate().getTimeAsTimestamp();

			Date start = (Date) item.getPropertyValue(getStartDatePropertyName());

			if ((start != null) && (start.after(current))) {
				vlogDebug("Item {0} has start date after today and has failed validation",
						new Object[] { item.getRepositoryId() });
				return false;
			}

			Date end = (Date) item.getPropertyValue(getEndDatePropertyName());

			if (start == null || end == null) {
				return false;
			}
			if ((end != null) && (end.before(current))) {
				vlogDebug("Item {0} has end date before today and has failed validation",
						new Object[] { item.getRepositoryId() });
				return false;
			}
		}
		return true;
	}

}