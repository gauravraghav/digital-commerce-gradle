package com.digital.commerce.common.listener;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.dtm.StaticTransactionManagerWrapper;
import atg.dtm.TransactionManagerEvent;
import atg.dtm.TransactionManagerListener;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalTransactionListener implements TransactionManagerListener {

    private static transient DigitalLogger logger = DigitalLogger.getLogger(DigitalTransactionListener.class);
    private boolean enabled;
    private String[] threadNames;

    /**
     * @param paramTransactionManagerEvent
     */
    @Override
    public void transactionBegin(TransactionManagerEvent paramTransactionManagerEvent) {
        try {
            if (enabled && logger.isDebugEnabled()) {
                String threadName = Thread.currentThread().getName();
                if (isAuditTransaction(threadName)) {
                    StaticTransactionManagerWrapper tx = (StaticTransactionManagerWrapper) paramTransactionManagerEvent
                            .getSource();
                    logger.debug("transactionBegin: " + tx.getTransaction().toString());
                }
            }
        } catch (Exception ex) {

        }
    }

    /**
     * @param paramTransactionManagerEvent
     */
    @Override
    public void transactionCommit(TransactionManagerEvent paramTransactionManagerEvent) {
        try {
            if (enabled && logger.isDebugEnabled()) {
                String threadName = Thread.currentThread().getName();
                if (isAuditTransaction(threadName)) {
                    StaticTransactionManagerWrapper tx = (StaticTransactionManagerWrapper) paramTransactionManagerEvent
                            .getSource();
                    logger.debug("transactionCommit: " + tx.getTransaction().toString());
                }
            }
        } catch (Exception ex) {

        }
    }

    /**
     * @param paramTransactionManagerEvent
     */
    @Override
    public void transactionRollback(TransactionManagerEvent paramTransactionManagerEvent) {
        try {
            if (enabled && logger.isDebugEnabled()) {
                String threadName = Thread.currentThread().getName();
                if (isAuditTransaction(threadName)) {
                    StaticTransactionManagerWrapper tx = (StaticTransactionManagerWrapper) paramTransactionManagerEvent
                            .getSource();
                    logger.debug("transactionRollback: " + tx.getTransaction().toString());
                }
            }
        } catch (Exception ex) {

        }
    }

    /**
     * @param paramTransactionManagerEvent
     */
    @Override
    public void transactionSuspend(TransactionManagerEvent paramTransactionManagerEvent) {
        try {
            if (enabled && logger.isDebugEnabled()) {
                String threadName = Thread.currentThread().getName();
                if (isAuditTransaction(threadName)) {
                    StaticTransactionManagerWrapper tx = (StaticTransactionManagerWrapper) paramTransactionManagerEvent
                            .getSource();
                    logger.debug("transactionSuspend: " + tx.getTransaction().toString());
                }
            }
        } catch (Exception ex) {

        }
    }

    /**
     * @param paramTransactionManagerEvent
     */
    @Override
    public void transactionResume(TransactionManagerEvent paramTransactionManagerEvent) {
        try {
            if (enabled && logger.isDebugEnabled()) {
                String threadName = Thread.currentThread().getName();
                if (isAuditTransaction(threadName)) {
                    StaticTransactionManagerWrapper tx = (StaticTransactionManagerWrapper) paramTransactionManagerEvent
                            .getSource();
                    logger.debug("transactionResume: " + tx.getTransaction().toString());
                }
            }
        } catch (Exception ex) {

        }
    }

    /**
     * @param threadName
     * @return true or false
     */
    private boolean isAuditTransaction(String threadName) {
        String[] allowedThreadNames = getThreadNames();
        if(null != allowedThreadNames && allowedThreadNames.length > 0){
            for(String allowedThreadName : allowedThreadNames){
                if (threadName.contains(allowedThreadName)) {
                    logger.debug("Enabled logging for threadName: " + threadName + ", allowedThreadName:" + allowedThreadName);
                    return true;
                }
            }
        }
        return false;
    }
}
