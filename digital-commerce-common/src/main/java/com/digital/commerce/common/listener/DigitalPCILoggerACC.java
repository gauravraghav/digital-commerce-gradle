package com.digital.commerce.common.listener;

import static com.digital.commerce.common.util.ComponentLookupUtil.ADMIN_USER_AUTHORITY;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.digital.commerce.common.util.ComponentLookupUtil;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.repository.ItemDescriptorImpl;
import atg.repository.PropertiesChangedEvent;
import atg.repository.PropertiesChangedListener;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.security.ThreadSecurityManager;
import atg.security.User;
import atg.userdirectory.UserDirectoryLoginUserAuthority;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalPCILoggerACC extends GenericService implements PropertiesChangedListener {

	private Repository	repository;
	private String		itemType;

	public void doStartService() throws ServiceException {
		try {
			Repository rep = (Repository)getRepository();
			if( rep != null ) {
				ItemDescriptorImpl idesc = (ItemDescriptorImpl)rep.getItemDescriptor( itemType );
				if( idesc != null ) {
					idesc.addPropertiesChangedListener( this );
				}
			}

		} catch( RepositoryException rex ) {
			if( isLoggingError() ) {
				logError( rex );
			}
		}
	}

	public void propertiesChanged( PropertiesChangedEvent event ) {

		UserDirectoryLoginUserAuthority myUserAuthority = ComponentLookupUtil.lookupComponent(ADMIN_USER_AUTHORITY, UserDirectoryLoginUserAuthority.class);
		if( myUserAuthority != null ) {
			User user = ThreadSecurityManager.currentUser();
			if( user != null ) {
				atg.security.AliasedPersona ap = (atg.security.AliasedPersona)user.getPrimaryPersona( myUserAuthority );
				if( ap != null ) {
					logInfo( "***********************************************" );
					logInfo( "A property Changed Via ACC" );
					logInfo( "By User = " + ap.getName() );
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss:SSS" );
					logInfo( "AT " + sdf.format( cal.getTime() ) );
					logInfo( "Item Descriptor: " + event.getItemDescriptorName() );
					logInfo( "Repository Id: " + event.getRepositoryId() );
					logInfo( "Type: " + event.getType() );
					logInfo( "Class: " + event.getClass() );
					logInfo( "Item: " + event.getItem() );
					logInfo( "Properties: " + event.getProperties() );
					logInfo( "Source: " + event.getSource() );
					logInfo( "***********************************************" );
				}
			}
		}
	}
}
