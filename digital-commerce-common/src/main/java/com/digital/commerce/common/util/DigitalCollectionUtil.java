package com.digital.commerce.common.util;

import java.util.Collection;

public class DigitalCollectionUtil {
	
	@SuppressWarnings("rawtypes")
	public static boolean isEmpty(Collection coll){
		return (coll == null) || (coll.isEmpty());
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean isNotEmpty(Collection coll){
	    	return !isEmpty(coll);
	}
}
