package com.digital.commerce.common.ldap;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import atg.userprofiling.ProfileTools;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.csc.ContactCenterUser;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.logger.DigitalLogger;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalLdapTools {
	private static transient DigitalLogger logger = DigitalLogger
			.getLogger(DigitalLdapTools.class);
	private static final String LDAP_GROUP = "memberOf";
	private static final String LDAP_FIRST_NAME = "givenName";
	private static final String LDAP_LAST_NAME = "sn";
	private static final String LDAP_USER_ID = "CN";
	private static final String ERROR_MSG_PREFIX = "INSOLE_LOGIN_";

	private String ldapServerURL;
	private String authenticationType;
	private String baseDN;
	private String userGroup;
	private ProfileTools profileTools;
	private int timeOut;
	private MessageLocator messageLocator;


	public boolean doUserAuthetication(String userName, String userPassword,
			ContactCenterUser contactCenterUser, List<String> groups) throws DigitalAppException {
		if(contactCenterUser != null) {
			return doInsoleLDAPAuthetication(userName, userPassword, contactCenterUser);
		} else {
			return doDashboardLDAPAuthentiction(userName, userPassword, groups);
		}
	}

	private Boolean doInsoleLDAPAuthetication(String userName, String userPassword,
			ContactCenterUser contactCenterUser) throws DigitalAppException {

		if (logger.isDebugEnabled()) {
			logger.debug("In LDAPAuthentication Logic");
		}

		boolean retVal;

		Hashtable<String, String> env = new Hashtable<>();

		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, getLdapServerURL() + "/" + getBaseDN());

		// Needed for the Bind (User Authorized to Query the LDAP server)
		env.put(Context.SECURITY_AUTHENTICATION, getAuthenticationType());
		env.put(Context.SECURITY_PRINCIPAL, "CN=" + userName + ","
				+ getBaseDN());
		env.put(Context.SECURITY_CREDENTIALS, userPassword);

		DirContext ctx;
		try {
			ctx = new InitialDirContext(env);
		} catch (NamingException e) {
			logger.error("Exception while loooking up context", e);
			throw new DigitalAppException(e.getMessage());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("ctx lookup is successful ");
		}

		NamingEnumeration<SearchResult> results = null;

		try {
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE); // Search
																	// Entire
																	// Subtree
			controls.setCountLimit(1); // Sets the maximum number of entries to
										// be returned as a result of the search
			controls.setTimeLimit(this.timeOut); // Sets the time limit of
													// these SearchControls in
													// milliseconds
			String searchString = "(&(objectCategory=user)(sAMAccountName="
					+ userName + ")" + getUserGroup() + ")";
			results = ctx.search("", searchString, controls);

			if (results.hasMore()) {
				if (logger.isDebugEnabled()) {
					logger.debug("Found user with provided credentials");
				}
				SearchResult result = (SearchResult) results.next();
				Attributes attrs = result.getAttributes();

				Attribute firstNameAttr = attrs.get(LDAP_FIRST_NAME);
				if(null != firstNameAttr && null != firstNameAttr.get()) {
					contactCenterUser.setFirstName((String) firstNameAttr.get());
				}
				Attribute lastNameAttr = attrs.get(LDAP_LAST_NAME);
				if(null !=lastNameAttr &&  null != lastNameAttr.get()) {
					contactCenterUser.setLastName((String) lastNameAttr.get());
				}
				Attribute loginAttr = attrs.get(LDAP_USER_ID);
				if(null !=loginAttr && null != loginAttr.get()) {
					contactCenterUser.setUserId((String) loginAttr.get());
				}

				contactCenterUser.setRole("user");
				Attribute memberOfAttr = attrs.get(LDAP_GROUP);
				if(memberOfAttr != null) {
				List<String> groups = new ArrayList<>();
				int i= 0;
				while(memberOfAttr.size() > i){
					if(((String)memberOfAttr.get(i)).toLowerCase().contains("Insole".toLowerCase())){
						groups.add((String)memberOfAttr.get(i));
					}
					i++;
				}
				String[] groupsAttr  = groups.toArray(new String[groups.size()]);
				contactCenterUser.setGroups(groupsAttr);
				}
				retVal = true;
			} else {

				if (logger.isDebugEnabled()) {
					logger.debug("Found 0 user with provided credentials");
				}

				String errorMsg = messageLocator.getMessageString(
						ERROR_MSG_PREFIX + "525", userName);
				logger.error("No user foung while looking up agent in LDAP "
						+ errorMsg);
				throw new DigitalAppException(errorMsg);
			}

		} catch (NamingException e) { // Invalid Login
			logger.error(
					"AuthenticationException while looking up agent in LDAP ",
					e);
			throw new DigitalAppException(e.getMessage());
		} finally {
			if (results != null) {
				try {
					results.close();
				} catch (Exception e) {
					logger.error(
							"Exception while closing the results set from LDAP ",
							e);
				}
			}

			if (ctx != null) {
				try {
					ctx.close();
				} catch (Exception e) {
					logger.error(
							"Exception while closing the results context from LDAP ",
							e);
				}
			}
		}
		return retVal;
	}
	
	private Boolean doDashboardLDAPAuthentiction(String userName, String userPassword,
			List<String> groups) throws DigitalAppException {

		if (logger.isDebugEnabled()) {
			logger.debug("In Dashboard LDAPAuthentication Logic");
		}

		boolean retVal;

		Hashtable<String, String> env = new Hashtable<>();
		int i= 0;
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, getLdapServerURL() + "/" + getBaseDN());

		// Needed for the Bind (User Authorized to Query the LDAP server)
		env.put(Context.SECURITY_AUTHENTICATION, getAuthenticationType());
		env.put(Context.SECURITY_PRINCIPAL, "CN=" + userName + ","
				+ getBaseDN());
		env.put(Context.SECURITY_CREDENTIALS, userPassword);

		DirContext ctx;
		try {
			ctx = new InitialDirContext(env);
		} catch (NamingException e) {
			logger.error("Exception while loooking up context", e);
			throw new DigitalAppException(e.getMessage());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("ctx lookup is successful ");
		}

		NamingEnumeration<SearchResult> results = null;

		try {
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE); // Search
																	// Entire
																	// Subtree
			controls.setCountLimit(1); // Sets the maximum number of entries to
										// be returned as a result of the search
			controls.setTimeLimit(this.timeOut); // Sets the time limit of
													// these SearchControls in
													// milliseconds
			String searchString = "(&(objectCategory=user)(sAMAccountName="
					+ userName + ")" + getUserGroup() + ")";
			results = ctx.search("", searchString, controls);

			if (results.hasMore()) {
				if (logger.isDebugEnabled()) {
					logger.debug("Found user with provided credentials");
				}
				SearchResult result = (SearchResult) results.next();
				Attributes attrs = result.getAttributes();

				Attribute memberOfAttr = attrs.get(LDAP_GROUP);
				if(memberOfAttr != null) {
				while(memberOfAttr.size() > i){
					if(groups.contains(memberOfAttr.toString())){
						groups.add((String)memberOfAttr.get(i));
					}
					i++;
				}
				}
				retVal = true;
			} else {

				if (logger.isDebugEnabled()) {
					logger.debug("Found 0 user with provided credentials");
				}

				String errorMsg = messageLocator.getMessageString(
						ERROR_MSG_PREFIX + "525", userName);
				logger.error("No user foung while looking up agent in LDAP "
						+ errorMsg);
				throw new DigitalAppException(errorMsg);
			}

		} catch (NamingException e) { // Invalid Login
			logger.error(
					"AuthenticationException while looking up agent in LDAP ",
					e);
			throw new DigitalAppException(e.getMessage());
		} finally {
			if (results != null) {
				try {
					results.close();
				} catch (Exception e) {
					logger.error(
							"Exception while closing the results set from LDAP ",
							e);
				}
			}

			if (ctx != null) {
				try {
					ctx.close();
				} catch (Exception e) {
					logger.error(
							"Exception while closing the results context from LDAP ",
							e);
				}
			}
		}
		if(retVal && i ==0) {
			throw new DigitalAppException("DASHBOARD_USER_UNAUTHORIZED", getMessageLocator().getMessageString("dashboard.user.unauthorized"), null);
		} 
		if(retVal && i>= 0) {
			return true;
		} else {
			return false;
		}
	}

}