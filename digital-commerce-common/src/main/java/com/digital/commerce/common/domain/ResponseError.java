package com.digital.commerce.common.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseError {
	private String errorCode;
	private String localizedMessage;

	public ResponseError(String errorCode, String localizedMessage) {
		super();
		this.errorCode = errorCode;
		this.localizedMessage = localizedMessage;
	}
}
