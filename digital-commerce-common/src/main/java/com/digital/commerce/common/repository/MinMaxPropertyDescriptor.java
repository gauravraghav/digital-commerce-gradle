package com.digital.commerce.common.repository;

import java.util.Collection;
import java.util.Iterator;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

@SuppressWarnings({"rawtypes"})
public class MinMaxPropertyDescriptor extends RepositoryPropertyDescriptor {

	private static final long		serialVersionUID	= -5016104077806848335L;

	protected static final String	TYPE_NAME			= "minMax";

	protected static final String	COLLECTION			= "collection";

	protected static final String	PROPERTY			= "property";

	protected static final String	OPERATION			= "operation";

	protected static final String	MIN					= "min";

	protected static final String	MAX					= "max";
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, MinMaxPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		String colName = (String)getValue( COLLECTION );
		Object col = pItem.getPropertyValue( colName );
		if( col instanceof Collection ) {
			Collection list = (Collection)col;
			Iterator i = list.iterator();
			double compVal = 0;
			String operation = (String)getValue( OPERATION );
			String property = (String)getValue( PROPERTY );
			boolean max = operation.equals( MAX );
			if( max ) {
				compVal = Double.MIN_VALUE;
			} else {
				compVal = Double.MAX_VALUE;
			}
			RepositoryItem returnItem = null;
			while( i.hasNext() ) {
				RepositoryItem curItem = (RepositoryItem)i.next();
				Number curVal = (Number)curItem.getPropertyValue( property );
				Boolean isActive = (Boolean)curItem.getPropertyValue( "isActive" );

				if( Boolean.TRUE.equals( isActive ) ) {
					if( max && curVal.doubleValue() > compVal ) {
						compVal = curVal.doubleValue();
						returnItem = curItem;
					} else if( !max && curVal.doubleValue() < compVal ) {
						compVal = curVal.doubleValue();
						returnItem = curItem;
					}
				}
			}
			return returnItem;
		} else {
			throw new IllegalArgumentException( "collection must be an instance of java.util.Collection" );
		}
	}

	public String getTypeName() {
		return TYPE_NAME;
	}

}
