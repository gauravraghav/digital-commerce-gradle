package com.digital.commerce.common.config;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.core.i18n.LayeredResourceBundle;
import atg.nucleus.GenericService;
import atg.service.dynamo.LangLicense;
import atg.servlet.ServletUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * Multi-locale aware MessageLocator
 * 
 * @author Manju krishnappa
 */
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class MessageLocator extends GenericService implements Serializable {

	private static final long serialVersionUID = 1L;

	private static transient DigitalLogger LOGGER = DigitalLogger.getLogger(MessageLocator.class);

	protected String resourceBundleName;

	protected String defaultMessage;

	private ResourceBundle messageBundle;

	private Properties messages = new Properties();	

	public void doStartService() {
		this.messageBundle = getResourceBundle();

		// MessageLocator previously exposed it's messages as a Properties
		// object which other classes now rely on,
		// so we're converting the ResourceBundle to that type.
		if (this.messageBundle != null) {
			Enumeration<String> keys = this.messageBundle.getKeys();
			while (keys.hasMoreElements()) {
				String key = keys.nextElement();
				messages.put(key, this.messageBundle.getString(key));
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public Properties getMsgKeyToStringMap() {
		return this.messages;
	}


	/**
	 * 
	 * @param pKey
	 * @return
	 */
	protected String getString(String pKey) {
		String message = getDefaultMessage();
		ResourceBundle resourceBundle = getResourceBundle();
		if (null != resourceBundle) {
			message = resourceBundle.getString(pKey);
			if (null == message) {
				LOGGER.error("Unknown key passed to message locator :: " + pKey);
				message = defaultMessage;
			}
		}

		return message;
	}

	/**
	 * 
	 * @param message
	 * @return
	 */
	protected MessageFormat getFormat(String message) {
		return new MessageFormat(message);
	}

	/**
	 * 
	 * @param pKey
	 * @return
	 */
	public String getMessageString(String pKey) {
		return getString(pKey);
	}

	/**
	 * 
	 * @param pKey
	 * @param pParam
	 * @return
	 */
	public String getMessageString(String pKey, Object pParam) {
		String message = getString(pKey);
		if (message.equalsIgnoreCase(getDefaultMessage())) {
			return message;
		} else {
			Object[] params = { new StringBuffer((String) pParam) };
			return getFormat(message).format(params);
		}
	}

	/**
	 * 
	 * @param pKey
	 * @param params
	 * @return
	 */
	public String getMessageString(String pKey, Object... params) {
		String message = getString(pKey);
		if (message.equalsIgnoreCase(getDefaultMessage())) {
			return message;
		} else if (params != null && params.length > 0) {
			return getFormat(message).format(params);
		} else {
			return message;
		}
	}

	/**
	 * 
	 * @return
	 */
	protected Locale getDefaultLocale() {
		return Locale.getDefault();
	}

	/**
	 * @return The resource bundle to be used in this class.
	 */
	public ResourceBundle getResourceBundle() {
		if (this.messageBundle != null) {
			return this.messageBundle;
		}

		// Get the locale from the logged in user. If the user's locale can't be
		// found,
		// get the browser locale. If either of these can't be found, use the
		// server locale.
		Locale locale = ServletUtil.getUserLocale();

		if (null == getResourceBundleName()) {
			LOGGER.error("ResourceBundleName is not configured ");
			return null;
		}
		if (locale != null) {
			// Get the resource bundle associated with the current locale.
			return LayeredResourceBundle.getBundle(getResourceBundleName(), locale);
		}

		// Couldn't retrieve the locale from user's profile, browser or server
		// so just get the default locale.
		return LayeredResourceBundle.getBundle(getResourceBundleName(), LangLicense.getLicensedDefault());
	}

	/**
	 * 
	 * @return
	 */
	public Enumeration<String> getMessageKeyToStringMap() {
		return getResourceBundle().getKeys();
	}

	/**
	 * 
	 * @param errorCodeMap
	 * @param errorCode
	 * @return
	 */
	public String getErrorMessageFromErrorCodeMap(String errorCodeMap, String errorCode) {
		String messageMap = null;
		String message = "default";
		Map hsh = new HashMap();
		ResourceBundle resourceBundle = getResourceBundle();
		if (null != resourceBundle) {
			messageMap = resourceBundle.getString(errorCodeMap);
			StringTokenizer st = new StringTokenizer(messageMap, ",");
			while (st.hasMoreElements()) {
				String object = (String) st.nextElement();
				String[] tokens = object.split("=");
				int tokenCount = tokens.length;
				if (tokenCount >= 2) {
					for (int j = 0; j < tokenCount; j = j + 2) {
						hsh.put(tokens[j], tokens[j + 1]);
					}
				}
			}
			if (null == messageMap) {
				LOGGER.error("Unknown Error code passed to message locator :: " + errorCode);
			}
		}
		message = (String) hsh.get(errorCode);
		return message;
	}

}
