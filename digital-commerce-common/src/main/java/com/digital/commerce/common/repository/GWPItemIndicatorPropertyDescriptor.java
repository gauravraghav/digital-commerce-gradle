package com.digital.commerce.common.repository;

import java.util.HashMap;

import com.digital.commerce.common.multisite.MultiSiteUtil;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

public class GWPItemIndicatorPropertyDescriptor extends
		RepositoryPropertyDescriptor {
	/**
     *
     */
	private static final long serialVersionUID = -6259343473515967032L;

	private static final String TYPE_NAME = "GWPItemIndicatorPropertyDescriptor";
		
	private static final String	PROD_DEPT_PROPERTY			= "depId";
	private static final String	PROD_CLASS_PROPERTY			= "classId";
	private static final String	PROD_SUBCLASS_PROPERTY		= "subclassId";

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass(TYPE_NAME,
				GWPItemIndicatorPropertyDescriptor.class);
	}

	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		// if value already cached return from the cache
		if (pValue != null && pValue instanceof Boolean) {
			return pValue;
		}		
		return  isGWPItem(pItem);
	}
	
	
	//Determines whether the product is an GWP item
	private boolean isGWPItem(RepositoryItem productRef){
		if (productRef!=null)
		{				
			String productDepartment = (String)productRef.getPropertyValue(PROD_DEPT_PROPERTY);			
			String productClass =(String)productRef.getPropertyValue(PROD_CLASS_PROPERTY);
			String productSubClass = (String)productRef.getPropertyValue(PROD_SUBCLASS_PROPERTY);
			HashMap <String,String> gwpconfig = (HashMap<String,String>) MultiSiteUtil.getGWPConfiguration();
			if(productDepartment != null && productDepartment.equals(gwpconfig.get("gwpDepId")) 
					&& (productClass !=null && productClass.equals(gwpconfig.get("gwpClassId")))
					&& (productSubClass!=null && productSubClass.equals(gwpconfig.get("gwpSubClassId")) )) {
				return true;	
		}				
	 }
		return false;
	}
}
