package com.digital.commerce.common.repository;

import java.util.List;

import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

/** @author Prabu Ramaraj */
public class FindPriceinClearancePropertyDescriptor extends RepositoryPropertyDescriptor

{

	private static final long		serialVersionUID	= -1795816015039204986L;

	protected static final String	TYPE_NAME			= "FindPriceinClearancePropertyDescriptor";

	protected static final String	CHILDSKU_PROPERTY			= "collection";

	protected static final String	NONMEMBERPRICE_PROPERTY			= "priceProperty";
	
	protected static final String	SKUCLEARANCE_PROPERTY			= "skuclearanceProperty";
	
	protected static final String	PRODUCTCLEARANCE_PROPERTY			= "productclearanceProperty";
	
	protected static final String	SKUSTOCKLEVEL_PROPERTY			= "stockproperty";
	
	protected static final String	MINORMAXPRICE_PROPERTY			= "pricecompareProperty";
	
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, FindPriceinClearancePropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {
		
		//If available, let's take this value from cache
		if( pValue != null && pValue instanceof Long ) { return pValue; }
		
		Boolean isPriceinClearance = false;		
		String childSkusProperty = (String)getValue( CHILDSKU_PROPERTY );
		String priceProperty = (String)getValue( NONMEMBERPRICE_PROPERTY );
		String skuClearanceProperty = (String)getValue( SKUCLEARANCE_PROPERTY );
		String productClearanceProperty = (String)getValue( PRODUCTCLEARANCE_PROPERTY ); 
		String stockProperty = (String)getValue( SKUSTOCKLEVEL_PROPERTY ); 
		String minorMaxPriceProperty = (String)getValue( MINORMAXPRICE_PROPERTY ); 
		
		//Process this only if the Product is in Clearance. Otherwise, none of the SKUs will be in clearance
		Object productClearanceObj = pItem.getPropertyValue(productClearanceProperty);
		if (productClearanceObj != null && (Boolean)productClearanceObj) {
			Object childSkusObj = pItem.getPropertyValue( childSkusProperty );
			if( childSkusObj != null && childSkusObj instanceof List<?>) {
				@SuppressWarnings("unchecked")
				List<RepositoryItem> childSKUItems = (List<RepositoryItem>)childSkusObj;
				for( RepositoryItem skuItem : childSKUItems ) {
					
					//Set isPriceinClearance = true only if the SKU is on clearance, has inventory, Min/Max Price == nonMemberPrice
					Object skuClearanceObj = skuItem.getPropertyValue(skuClearanceProperty);
					Object stockObj = skuItem.getPropertyValue(stockProperty);
					Object minorMaxPriceObj = pItem.getPropertyValue(minorMaxPriceProperty);
					Object priceObj = skuItem.getPropertyValue(priceProperty);
					if (skuClearanceObj != null && (Boolean)skuClearanceObj &&
							stockObj !=null && (long)stockObj > 0 &&
									minorMaxPriceObj !=null && priceObj !=null && 
						(double)minorMaxPriceObj == (double)priceObj) {
						//Yes, this Min/Max Price belongs to a SKU which is on Clearance
						isPriceinClearance = true;
						break;
					}
				}
			}
		}

		return isPriceinClearance;
		
	}

	public String getTypeName() {
		return TYPE_NAME;
	}

}
