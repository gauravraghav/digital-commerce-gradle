package com.digital.commerce.common.util;

import com.google.common.base.Predicate;

public abstract interface DigitalPredicate<T> extends Predicate<T>{

}
