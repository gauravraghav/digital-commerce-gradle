package com.digital.commerce.common.util;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.service.perfmonitor.PerformanceMonitor;

public class DigitalPerformanceMonitorUtil {
	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalPerformanceMonitorUtil.class);

	public static void startPerformanceMonitorOperation(String componentName, String serviceMethod) {
		try{
/*			if(logger.isDebugEnabled()){
				logger.debug("::::::::: START METHOD : "+ componentName+"."+serviceMethod + "  :::::::::");
			}
*/			PerformanceMonitor.startOperation(componentName, serviceMethod);
		}catch(Exception e){
			logger.warn(String.format("%s-%s PerformanceStartMonitor Exception", componentName, serviceMethod), e);
		}
	}

	public static void endPerformanceMonitorOperation(String componentName, String serviceMethod) {
		
		try {
			PerformanceMonitor.endOperation(componentName, serviceMethod);
			
		} catch (Exception e) {
			logger.warn(String.format("%s-%s PerfStackMismatchException", componentName, serviceMethod), e);
		}/*finally{
			if(logger.isDebugEnabled()){
				logger.debug("::::::::: END METHOD : "+ componentName+"."+serviceMethod + ":::::::::");
			}
		}*/
	}
}
