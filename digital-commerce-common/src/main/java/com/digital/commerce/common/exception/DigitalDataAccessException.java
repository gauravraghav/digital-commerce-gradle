package com.digital.commerce.common.exception;

public class DigitalDataAccessException extends DigitalRuntimeException {
	private static final long	serialVersionUID	= 1L;

	public DigitalDataAccessException( Throwable ex ) {
		super( ex );
	}

	public DigitalDataAccessException( String message ) {
		super( message );
	}

	public DigitalDataAccessException( String message, Throwable ex ) {
		super( message, ex );
	}
}
