package com.digital.commerce.common.config;

import lombok.Getter;
import lombok.Setter;

import java.util.Properties;


@Getter
@Setter
public class CCAuthFraudMessageLocator{

	private Properties	reasonCodeMap;

}
