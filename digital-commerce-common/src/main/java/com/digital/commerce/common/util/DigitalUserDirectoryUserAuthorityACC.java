package com.digital.commerce.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import atg.security.User;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.userdirectory.UserDirectoryLoginUserAuthority;

public class DigitalUserDirectoryUserAuthorityACC extends UserDirectoryLoginUserAuthority {

	public boolean login( User user, String arg1, String arg2, Object arg3 ) {
		boolean ret = super.login( user, arg1, arg2, arg3 );
		if( !ret ) {
			if( DigitalStringUtil.isNotBlank(arg1) ) {
				DynamoHttpServletRequest request = (DynamoHttpServletRequest)ServletUtil.getCurrentRequest();
				logInfo( "====================================================================" );
				if( request != null ) {
					String cPath = request.getContextPath();
					logInfo( "Attempt to Login to " + cPath + " failed" );
					logInfo( "From Source IP = " + request.getRemoteAddr() );
				} else {
					logInfo( "Attempt to Login to ACC failed" );
				}
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss:SSS" );
				logInfo( "AT " + sdf.format( cal.getTime() ) );
				logInfo( "For User Id = " + arg1 );
				logInfo( "====================================================================" );

			}
		}
		return ret;
	}
}
