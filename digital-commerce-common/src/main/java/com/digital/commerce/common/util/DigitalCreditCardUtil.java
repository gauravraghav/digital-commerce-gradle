package com.digital.commerce.common.util;

import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.digital.commerce.common.services.DigitalBaseConstants;

/**
 * Encapsulates some generic utility methods to deal with credit cards.
 * 
 * @author kbajaj
 */
public class DigitalCreditCardUtil {

	/**
	 * 
	 * @param expirationMonth
	 * @param expirationYear
	 * @return
	 */
	public static boolean isCreditCardExpired(String expirationMonth, String expirationYear) {
		boolean retVal = false;
		final Calendar now = java.util.Calendar.getInstance();
		final int currentYear = now.get(Calendar.YEAR);
		final int currentMonth = now.get(Calendar.MONTH) + 1;
		final int cardExpirationMonth = NumberUtils.toInt(expirationMonth, -1);
		final int cardExpirationYear = NumberUtils.toInt(expirationYear, -1);
		if (cardExpirationYear > 0 && cardExpirationMonth > 0) {
			if (cardExpirationYear < currentYear
					|| (cardExpirationYear == currentYear && cardExpirationMonth < currentMonth)) {
				retVal = true;
			}
		}
		return retVal;
	}

	/**
	 * Masks a credit card according to Digital styles.
	 * 
	 * @param creditCardNumber
	 * @return
	 */
	public static String getMaskedCreditCardNumber(String creditCardNumber) {
		creditCardNumber = StringUtils.trimToEmpty(creditCardNumber);

		// Added for Credit Card Mandate Project. - Start
		if (StringUtils.length(creditCardNumber) == (DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH_DINERS[0])) {
			return new StringBuilder(DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH_DINERS[0])
					.append(StringUtils.repeat("X", DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH_DINERS[0] - 4))
					.append(creditCardNumber.substring(DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH_DINERS[0] - 4))
					.toString();
		}
		// Added for Credit Card Mandate Project. - End

		else if (StringUtils.length(creditCardNumber) == DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH_AMEX) {
			return new StringBuilder(DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH_AMEX)
					.append(StringUtils.repeat("X", DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH_AMEX - 4))
					.append(creditCardNumber.substring(DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH_AMEX - 4))
					.toString();
		} else if (StringUtils.length(creditCardNumber) == DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH) {
			return new StringBuilder(DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH)
					.append(StringUtils.repeat("X", DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH - 4))
					.append(creditCardNumber.substring(DigitalBaseConstants.MAX_CREDIT_CARDNUMBER_LENGTH - 4)).toString();
		}
		return "";
	}

	/**
	 * Masks a credit card according to Digital styles.
	 * 
	 * @param creditCardNumber
	 * @return
	 */
	public static String getCreditCardNumberLast4(String creditCardNumber) {
		creditCardNumber = StringUtils.trimToEmpty(creditCardNumber);
		int stringLength = creditCardNumber.length() <= 4 ? creditCardNumber.length() : 4;
		return creditCardNumber.substring(creditCardNumber.length() - stringLength);
	}
}
