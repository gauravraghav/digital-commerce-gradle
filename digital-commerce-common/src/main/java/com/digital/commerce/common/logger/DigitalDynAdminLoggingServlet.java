package com.digital.commerce.common.logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;

import atg.core.util.Base64;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.PipelineableServletImpl;

/**
 * @author HB391569 This is written to log all the access and/or any edit done
 *         through dyn access to address the GAC controls defined by IT-Security
 *         for DOTCOM
 */
public class DigitalDynAdminLoggingServlet extends PipelineableServletImpl {

	private static final String FORWARD_PATH_KEY = "javax.servlet.forward.request_uri";
	private static final String SEPERATOR = System
			.getProperty("line.separator");
	private static final String HTTP_POST_METHOD = "POST";
	private static final String LOGGED_IN_DYN_USER = "LOGGED_IN_DYN_USER";
	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss:SSS");
	private Pattern pattern = Pattern
			.compile("([^\\s]+(\\.(?i)(jpg|png|gif|bmp|css|js|gif))$)");
	private Matcher matcher;

	/**
	 * To enable/disable the Dyn Admin access logging
	 */
	private boolean enabled;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws IOException,
			ServletException {

		/*
		 * Only if this servlet is enabled then log the data needed and ignore
		 * any exceptions during the logging
		 */
		if (enabled) {
			try {
				boolean isPost = HTTP_POST_METHOD.equals(pRequest.getMethod());

				StringBuilder sb = new StringBuilder();
				if (isPost) {
					sb.append(SEPERATOR)
							.append("*********** Attempt made via Dyn/admin to change/modify data ********************************");
				} else {
					sb.append("*********** Attempt made via Dyn/admin to read/view data ************************************");
				}
				sb.append(getRequestInfo(pRequest))
						.append("***************************************************************************************************")
						.append(SEPERATOR);

				if (isLoggingInfo()) {
					logInfo(sb.toString());
				}
			} catch (Exception ex) {
				// not a big deal, just log and move on
				logError("Error while logging Dyn Access data" + ex);
			}
		}
		/* Move on in the pipeline all the times */
		passRequest(pRequest, pResponse);
	}

	/**
	 * Retrieve various data points from request
	 * 
	 * @param request
	 * @return
	 */
	private final String getRequestInfo(final DynamoHttpServletRequest request) {
		if (request == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(SEPERATOR).append("ACCESSED_FROM").append(": ")
				.append(request.getRemoteAddr()).append(SEPERATOR);

		/* get the IP of the request origin */
		sb.append("ACCESSED_AT").append(": ")
				.append(sdf.format(Calendar.getInstance().getTime()))
				.append(SEPERATOR);

		/* get user name of the dyn admin logged in */
		final String loggedInUser = this.getLoggedInUser(request);
		if (null != loggedInUser) {
			sb.append("ACCESSED_BY").append(": ").append(loggedInUser)
					.append(SEPERATOR);
		}

		/* get the page URL being accessed */
		final String requestURI = request.getRequestURI();
		if (null != requestURI && !isStaticContent(requestURI)) {
			sb.append("REQUEST_URI").append(": ").append(requestURI)
					.append(SEPERATOR);

		}

		/* get the forwarded page path (if any) */
		final Object forwardedPath = request.getAttribute(FORWARD_PATH_KEY);
		if (null != forwardedPath) {
			sb.append("FORWARDED_PATH").append(": ")
					.append((String) forwardedPath).append(SEPERATOR);
		}
		/* get the referrer page URL */
		final String referrer = request.getHeader("Referer");
		if (null != referrer) {
			sb.append("REFERRER").append(": ").append(referrer)
					.append(SEPERATOR);
		}

		/* get all the request parameters */
		final Map<?, ?> requestParameters = request.getParameterMap();
		if (requestParameters != null && requestParameters.size() > 0) {
			sb.append("REQUEST_DATA").append(SEPERATOR);
			for (Object param : requestParameters.keySet()) {
				String[] valMap = (String[]) requestParameters.get(param);
				sb.append(param).append(": ").append(valMap[0])
						.append(SEPERATOR);
			}
		}
		return sb.toString();
	}

	/**
	 * Check whether the resource being accessed is a static content
	 * 
	 * @param fileName
	 * @return
	 */
	private boolean isStaticContent(final String requestURI) {
		matcher = pattern.matcher(requestURI);
		return matcher.matches();
	}

	/**
	 * Retrieve the logged in user from Basic auth header
	 * 
	 * @param authorization
	 * @return
	 */
    private String getLoggedInUser(final DynamoHttpServletRequest request) {
        String user = null;
        try {
             final String authorization = request.getHeader("Authorization");
             if (authorization != null && authorization.startsWith("Basic")) {
                  // Authorization: Basic base64credentials
                  String base64Credentials = authorization.substring(
                            "Basic".length()).trim();
                  String credentials = new String(
                            Base64.decodeToString(base64Credentials));
                  // credentials = username:password
                  final String[] values = credentials.split(":", 2);
                  if (values.length > 0) {
                       user = values[0];
                       request.getSession().setAttribute(LOGGED_IN_DYN_USER, user);
                  }
             } else if (null != request.getSession().getAttribute(LOGGED_IN_DYN_USER)) {
            	 user = (String) request.getSession().getAttribute(LOGGED_IN_DYN_USER);
             }
        } catch (Exception ex) {
             // not a big deal move on
             logError("Error while retrieving the looged dyn admin user info "
                       + ex.getMessage());
        }
        return user;
   }}