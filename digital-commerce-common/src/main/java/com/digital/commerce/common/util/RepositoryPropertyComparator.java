package com.digital.commerce.common.util;

import java.beans.IntrospectionException;
import java.util.Comparator;
import java.util.regex.Pattern;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.beans.DynamicBeans;
import atg.repository.RepositoryItem;
@SuppressWarnings("rawtypes")
public abstract class RepositoryPropertyComparator implements Comparator {
	private static final DigitalLogger	logger	= DigitalLogger.getLogger( RepositoryPropertyComparator.class );

	protected final int compare( Object arg0, Object arg1, String pPropertyName ) {
		if( arg0 == arg1 ) { return 0; }
		if( arg1 == null ) { return 1; }
		if( arg0 == null ) { return -1; }
		if( arg0.equals( arg1 ) ) { return 0; }
		RepositoryItem item1 = (RepositoryItem)arg0;
		RepositoryItem item2 = (RepositoryItem)arg1;
		try {
			if( DynamicBeans.getBeanInfo( item1 ).hasProperty( pPropertyName ) && DynamicBeans.getBeanInfo( item2 ).hasProperty( pPropertyName ) ) {
				String name1 = (String)item1.getPropertyValue( pPropertyName );
				String name2 = (String)item2.getPropertyValue( pPropertyName );
				if( isNumeric( name1 ) && isNumeric( name2 ) ) { return new Double( name1 ).compareTo( new Double( name2 ) ); }
				if( name1 != null ) {
					return name1.compareTo( name2 );
				} else if( name2 != null ) { return -1; }
			}
		} catch( NumberFormatException  | IntrospectionException e ) {
			// Won't happen
			logger.error( "Exception: NumberFormatException", e );
		}
		return 0;

	}

	public abstract int compare( Object o1, Object o2 );

	private static final String	NUMERIC_PATTERN_CHARS	= "[0-9\\.]+";

	private static final Pattern	NUMERIC_PATTERN			= Pattern.compile( NUMERIC_PATTERN_CHARS );

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object) */
	public boolean isNumeric( String str ) {
		if( str == null ) return false;
		return NUMERIC_PATTERN.matcher( str ).matches();
	}

}
