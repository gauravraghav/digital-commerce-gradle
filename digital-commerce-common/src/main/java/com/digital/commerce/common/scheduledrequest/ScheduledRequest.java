package com.digital.commerce.common.scheduledrequest;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import java.sql.Timestamp;

@Getter
@Setter
@ToString
public class ScheduledRequest implements Serializable {

	private static final long serialVersionUID = 5486598257657814290L;
	private String componentName;
	private String componentProperty;
	private String componentMethod;
	private String stringifiedSchedule;
	private Date schedule;
	private Set<String> serverNames = new HashSet<>();
	private String newValue;
	private String repositoryId;
	private Timestamp creationDate;
}
