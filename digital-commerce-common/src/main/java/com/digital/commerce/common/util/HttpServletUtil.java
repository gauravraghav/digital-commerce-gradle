package com.digital.commerce.common.util;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.ObjectUtils;

import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.RequestLocale;
import atg.servlet.ServletUtil;

public class HttpServletUtil
{
	private static final String[]	DYNAMIC_FILE_EXTENSIONS	= { "jsp", "html", "htm", "xml" };
	private static final String[]	STATIC_FILE_EXTENSIONS	= { "jpg", "gif", "png", "tif", "css", "js", "ico", "pdf" };
    private static final List<String> htmlContentResponseTypes = Lists.newArrayList();
    private static final List<String> nonResourceContentTypes = Lists.newArrayList();
    
    static 
    {
        htmlContentResponseTypes.add("text/html");
        htmlContentResponseTypes.add("application/xhtml+xmll");
        htmlContentResponseTypes.add("*/*");
        nonResourceContentTypes.add("text/html");
        nonResourceContentTypes.add("application/xhtml+xml");
        nonResourceContentTypes.add("application/json");
        nonResourceContentTypes.add("application/vnd.apple.pkpass");
        nonResourceContentTypes.add("text/html");
        nonResourceContentTypes.add("*/*");
    }
    
    /**
     * Returns true if the request accepts html. This allows us to determine
	 * whether or not to actually write an error to the response or simply
	 * swallow it up.
	 * If the original request resulted in an error and we are now evaluating an error
     * page we attempt to find the original request.
     */
    public static boolean isHtmlAccepted(HttpServletRequest request)
    {
		final String acceptHeader = request.getHeader( "accept" );
		String extension = DigitalStringUtil.substringAfterLast( request.getRequestURI(), "." );
		String originalRequestUri = ObjectUtils.toString( request.getAttribute( "javax.servlet.error.request_uri" ), "" );
        if (DigitalStringUtil.isNotBlank(originalRequestUri) && ArrayUtils.contains(DYNAMIC_FILE_EXTENSIONS, extension)) 
        {
			extension = DigitalStringUtil.substringAfterLast( originalRequestUri, "." );
		}
    	return !ArrayUtils.contains(STATIC_FILE_EXTENSIONS, extension) && (acceptHeader == null ||
				Iterables.tryFind(htmlContentResponseTypes, new ContainsPredicate(acceptHeader)).isPresent() ||
				ArrayUtils.contains(DYNAMIC_FILE_EXTENSIONS, extension) );
	}

    /**
     * Returns true if the request accepts what we determine is secondary content (e.g., images, css files, etc.). 
	 * This allows us to determine whether or not to actually write an error to the response or simply swallow it up.
	 * If the original request resulted in an error and we are now evaluating an error
     * page we attempt to find the original request.
     */
    public static boolean isSecondaryRequest(HttpServletRequest request)
    {
		final String acceptHeader = request.getHeader( "accept" );
		String extension = DigitalStringUtil.substringAfterLast( request.getRequestURI(), "." );
		String originalRequestUri = ObjectUtils.toString( request.getAttribute( "javax.servlet.error.request_uri" ), "" );
        if (DigitalStringUtil.isNotBlank(originalRequestUri) && ArrayUtils.contains(DYNAMIC_FILE_EXTENSIONS, extension)) 
        {
			extension = DigitalStringUtil.substringAfterLast( originalRequestUri, "." );
		}
    	return ArrayUtils.contains(STATIC_FILE_EXTENSIONS, extension) || (acceptHeader != null &&
				Iterables.all(nonResourceContentTypes, Predicates.not(new ContainsPredicate(acceptHeader)))
				&& !ArrayUtils.contains(DYNAMIC_FILE_EXTENSIONS, extension) );
    }

    private static class ContainsPredicate implements DigitalPredicate<String> {
        private final String value;
        
        ContainsPredicate(String value) {
            this.value = value;
        }
        
        @Override
        public boolean apply(String input)
        {
            return DigitalStringUtil.isNotBlank(input) && DigitalStringUtil.isNotBlank(value) && value.contains(input);
        }
    }
    
    /**
     * Check if the request is from a mobile device
     * 
     * @return true or false
     */
    public static boolean isMobileOrder(){
		if (null != ServletUtil.getCurrentRequest()) {
			String deviceName = ServletUtil.getCurrentRequest().getHeader(
					RequestHeaderAttributesConstant.AKAMAI_DEVICE_KEY.getValue());
			if(DigitalStringUtil.isNotBlank(deviceName)){
				switch(deviceName){
					case "androidapp":
						return true;
					case "iosapp":
						return true;
					case "mobile":
						return true;
					default:
						return false;
				}
			}
		}
		return false;
	}
    
    public static String getLocaleInfo(){
		final String LOCALE_SELECTION_PARAMETER = "locale";
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		String localeSelection = request.getParameter(LOCALE_SELECTION_PARAMETER);
		
		return localeSelection;
    }

    public static Locale getUserLocale(DynamoHttpServletRequest pRequest)
    	     throws ServletException, IOException {
    			Locale locale = null;
    			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(
    					"HTTPServletUtil", "getUserLocale");
    	     try
    	     {
    	       Object requestLocale; 
    	       
    	         requestLocale = pRequest.getRequestLocale();
    	        if (requestLocale != null) {
    	          return ((RequestLocale)requestLocale).getLocale();
    	         }
    	     }
    	     finally {
    	    	 DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(
    						"HTTPServletUtil", "getUserLocale");
    	     }
    	     return locale;
     }
	public static String getRequestId() {
        DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
        return (null != request ? request.getHeader("UNIQUE_ID") : null);
    }
}