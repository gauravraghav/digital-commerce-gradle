package com.digital.commerce.common.ldap;

import atg.adapter.ldap.LDAPJNDIEnvironment;

import com.digital.commerce.common.crypto.DigitalVaultUtil;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.logger.DigitalLogger;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalLDAPJNDIEnvironment extends LDAPJNDIEnvironment {

	/**
	 * 
	 */
	private static final DigitalLogger logger = DigitalLogger
			.getLogger(DigitalLDAPJNDIEnvironment.class);
	private static final long serialVersionUID = 1L;

	private String connectionPassword;

	@SuppressWarnings("unchecked")
	public void setSecurityCredentials(String pSecurityCredentials) {
		String decryptSecurityCredentials = this
				.getDecryptedString(getConnectionPassword());

		if (decryptSecurityCredentials == null) {
			remove("java.naming.security.credentials");
		} else {
			put("java.naming.security.credentials", decryptSecurityCredentials);
		}
	}

	private String getDecryptedString(String encryptedString) {
		String decryptedString = null;
		try {
			decryptedString = DigitalVaultUtil.getDecryptedString(encryptedString);

		} catch (DigitalAppException e) {
			logger.error(String.format("Caught %1$s during decryption of %2$s",
					e.getClass().getSimpleName(), encryptedString));
		}
		return decryptedString;
	}

}