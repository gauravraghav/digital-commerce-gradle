package com.digital.commerce.common.repository;

import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;
import atg.repository.RepositoryView;
import atg.servlet.ServletUtil;

/** @author psinha */
public class SkuStockLevelFCPropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
     *
     */
	private static final long		serialVersionUID	= -6258342474415966032L;

	protected static final String	TYPE_NAME			= "SkuStockLevelFCPropertyDescriptor";

	/** thread safe and don't serialize it. */

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, SkuStockLevelFCPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {

		// if value already cached return from the cache
		if( pValue != null && pValue instanceof Long ) { return pValue; }

		Long fcStockLevel = Long.valueOf( 0 );
		String skuId = (String)pItem.getPropertyValue( "id" );
		RepositoryItem inventoryItem = getInventoryItem( skuId );
		if(inventoryItem != null) {
			Object fcStockLevelObj = inventoryItem.getPropertyValue("fcStockLevel");
			if (fcStockLevelObj != null) {
				fcStockLevel = (Long) fcStockLevelObj;
			}
		}

		setStockLevelProperty( pItem, fcStockLevel );

		return fcStockLevel;
	}

	private void setStockLevelProperty( RepositoryItemImpl item, Long fcStockLevel ) {
		item.setPropertyValueInCache( this, fcStockLevel );
	}

	/** try and reterive a inventory item from the inventory repository by sku id
	 * 
	 * @param pSkuId
	 *            string value of the sku
	 * @return inventory repository item */
	private RepositoryItem getInventoryItem( String pSkuId ) {
		RepositoryItem item = null;

		try {

			Repository inventoryRepository = (Repository)ServletUtil.getCurrentRequest().resolveName( "/atg/commerce/inventory/InventoryRepository" );
			RepositoryView view = inventoryRepository.getView( "inventory" );
			if( null != view ) {
				QueryBuilder builder = view.getQueryBuilder();

				QueryExpression prop = builder.createPropertyQueryExpression( "catalogRefId" );
				QueryExpression constant = builder.createConstantQueryExpression( pSkuId );
				Query query = builder.createComparisonQuery( prop, constant, QueryBuilder.EQUALS );

				RepositoryItem[] items = view.executeQuery( query );
				// TEST IF THERE ARE NO INVENTORY ITEMS FOR THIS SKU
				if( items != null ) {
					if( items.length >= 1 ) {
						item = (RepositoryItem)items[0];
					}
				}
			}
		} catch( RepositoryException e ) {
			// Swallow exception for giftCards and sku not having inventory records.
		}
		return item;
	}

}
