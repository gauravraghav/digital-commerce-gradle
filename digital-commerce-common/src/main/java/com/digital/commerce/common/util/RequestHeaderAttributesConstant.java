package com.digital.commerce.common.util;

import lombok.Getter;

@Getter
public enum RequestHeaderAttributesConstant {
	HTTP_FORWARD_HEADER("X-FORWARDED-FOR"),
	USER_AGENT("User-Agent"),
	AKAMAI_DEVICE_KEY("DSW_CLIENT_DEVICE_TYPE");
	
	private final String value;
	
	private RequestHeaderAttributesConstant( String value ) {
		this.value = value;
	}
}
