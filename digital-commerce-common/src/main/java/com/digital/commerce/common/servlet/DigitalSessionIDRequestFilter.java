package com.digital.commerce.common.servlet;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalStringUtil;

import atg.servlet.ServletUtil;



public class DigitalSessionIDRequestFilter implements Filter {
	
	private static final Logger LOGGER = LogManager.getLogger(DigitalSessionIDRequestFilter.class);
	
	private static final String JSESSION_ID = "JSESSIONID";
	private static final String COMMA_DELIMITER = ", ";
	private static final String REQUEST_URI = "REQUEST_URI";
	private static final String QUERY_STRING = "QUERY_STRING";
    private static final String UNIQUE_ID_KEY = "UNIQUE_ID";
	private static final String DYN_USER_ID = "DYN_USER_ID";
	private static final String SOURCE_IP_KEY = "SOURCE_IP";
	private static final String CLIENT_SOURCE_IP_KEY = "Client-Source-IP";
	private static final String AKAMAI_EDGESCAPE_KEY = "REQ_SRC";
	private static final String AKAMAI_DEVICE_KEY = "DSW_CLIENT_DEVICE_TYPE";

	public void destroy() {
		// Do nothing
	}
	
	public void init( FilterConfig arg0 ) throws ServletException {
		// Do nothing
	}

    /*
     * Function to return any of the request info for debugging purposes
    */
    private String getRequestInfo(final HttpServletRequest request, HttpServletResponse response) {
        if (request == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        // original request URI
        sb.append(REQUEST_URI).append(":").append(request.getRequestURI()).append(COMMA_DELIMITER);
        
        // hTTP method
       	sb.append("METHOD").append(":").append(request.getMethod()).append(COMMA_DELIMITER);

       	int statusCode = response.getStatus();
				if(statusCode == 500) {
					statusCode = 450;
				}
        // forwarded path
        sb.append("HTTP_STATUS").append(":").append(statusCode).append(COMMA_DELIMITER);

        // query string
        final String queryString = request.getQueryString();
        String decQS = "";
        if(queryString != null) {
            decQS = queryString;
            try {
                decQS = URLDecoder.decode(queryString,DigitalBaseConstants.ENCODING);
            }catch(Exception ex) {
				// Do nothing
            }
        }
        sb.append(QUERY_STRING).append(":").append(decQS).append(COMMA_DELIMITER);

        String profileId = "";

        if(null != ServletUtil.getCurrentUserProfile()) {
			profileId=ServletUtil.getCurrentUserProfile().getRepositoryId();
		} 

        // get JSESSIONID cookie
        final Cookie[] cookies = request.getCookies();
        Map<String, String> cookieMap = new HashMap<>();
        if(null != cookies) {
            for (final Cookie c : cookies) {
                if (c.getName().equalsIgnoreCase(JSESSION_ID)) {
                	cookieMap.put(JSESSION_ID, c.getValue());
                }
                if(DigitalStringUtil.isBlank(profileId) && c.getName().equalsIgnoreCase(DYN_USER_ID)) {
                   	cookieMap.put(DYN_USER_ID, c.getValue());
                }
            }
        }

        sb.append(JSESSION_ID).append(":").append(cookieMap.get(JSESSION_ID)).append(COMMA_DELIMITER);
        // get uniqueid from Apache
		String apacheId = (null == request.getHeader(UNIQUE_ID_KEY)) ?  "" : request.getHeader(UNIQUE_ID_KEY);
        sb.append("APACHE_UNIQUE_ID").append(":").append(apacheId).append(COMMA_DELIMITER);

        // get source-ip from Apache. Concern from Security Audit
		String sourceIp = (null == request.getHeader(SOURCE_IP_KEY)) ?  "" : request.getHeader(SOURCE_IP_KEY);
        sb.append(SOURCE_IP_KEY).append(":").append(sourceIp).append(COMMA_DELIMITER);

        // get source-ip from Apache. Concern from Security Audit
		String clientSrcIp = (null == request.getHeader(CLIENT_SOURCE_IP_KEY)) ?  "" : request.getHeader(CLIENT_SOURCE_IP_KEY);
        sb.append(CLIENT_SOURCE_IP_KEY).append(":").append(clientSrcIp).append(COMMA_DELIMITER);

        // get source-ip from Apache. Concern from Security Audit
		String dynUserId = (null == cookieMap.get(DYN_USER_ID)) ?  "" : cookieMap.get(DYN_USER_ID);
        sb.append(DYN_USER_ID).append(":").append(dynUserId).append(COMMA_DELIMITER);

        // get akamai edgescape header, refer to Apache logs for all details
        String reqSrc = (null == request.getHeader("Akamai-Bot")) ?  "" : request.getHeader("Akamai-Bot");
        sb.append(AKAMAI_EDGESCAPE_KEY).append(":").append(reqSrc).append(COMMA_DELIMITER);        

        // get akamai device characteristics, refer to Apache logs for all details
		String clientDevice = (null == request.getHeader(AKAMAI_DEVICE_KEY)) ?  "" : request.getHeader(AKAMAI_DEVICE_KEY);
        sb.append(AKAMAI_DEVICE_KEY).append(":").append(clientDevice).append(COMMA_DELIMITER);

        return sb.toString();
	}
	
    @Override
	public void doFilter( ServletRequest servletRequest, final ServletResponse servletResponse, FilterChain chain ) throws IOException, ServletException {
		long startTime = 0;
		long endTime = 0;
		HttpServletRequest request = null;
		HttpServletResponse response = null;
		try {
			request = (HttpServletRequest) servletRequest;
			response = (HttpServletResponse) servletResponse;
			startTime = System.currentTimeMillis();
			HttpServletResponseWrapper wrappedResponse = new DigitalWrappedResponse(
					request, response);
			chain.doFilter(servletRequest, wrappedResponse);
		} finally {
			if (LOGGER.isInfoEnabled()) {
				final String requestInfo = this.getRequestInfo(request, response);
				endTime = System.currentTimeMillis();
				final long totalTime = endTime - startTime;
                StringBuilder sb = new StringBuilder();
                        sb.append("[")
                        .append(requestInfo)
                         .append("EXECUTION_TIME: ")
                        .append(totalTime).append(" ms]");
				LOGGER.info(sb.toString());
			}
		}
	}

}

class DigitalWrappedResponse extends HttpServletResponseWrapper {
	private static final String JSESSIONID_PATH = ";jsessionid=";
	HttpServletResponse response;
	HttpServletRequest  request;

	public DigitalWrappedResponse( HttpServletRequest request, HttpServletResponse response ) {
		super( response );
		this.request = request;
		this.response = response;
	}

	/** each of the four methods below proxies for its respective method on response
	 *	 nearly a line for line extraction of what was in the old facade
	 */
	@SuppressWarnings("deprecation")
	@Override
	public String encodeRedirectUrl( String url ) {
		url = applyTimestamp( url );
		url = response.encodeRedirectUrl( url );
		return stripJSessionID( url );
	}
	@Override
	public String encodeRedirectURL( String url ) {
		url = applyTimestamp( url );
		url = response.encodeRedirectURL( url );
		return stripJSessionID( url );
	}
	@SuppressWarnings("deprecation")
	@Override
	public String encodeUrl( String url ) {
		url = applyTimestamp( url );
		url = response.encodeUrl( url );
		return stripJSessionID( url );
	}
	@Override
	public String encodeURL( String url ) {
		url = applyTimestamp( url );
		url = response.encodeURL( url );
		return stripJSessionID( url );
	}

	/** this applies a timestamp for a uniqueid on checkout pages (or any page with uniqueid param)
	 *	 line for line extraction of what was in the old facade
	*/
	public String applyTimestamp( String url ) {
		if( url.contains( "_requestid" ) && !url.contains( "uniqueid" ) ) {
			java.util.Calendar now = java.util.Calendar.getInstance();
			url = url + "&uniqueid=" + now.getTimeInMillis();
		}
		return url;
	}

	/** this function strips off ";jessionid=<whatever>" off of URLs that may have this (usually on session start)
	 *	 line for line extraction of what was in the old facade
	 */
	public String stripJSessionID( String encodedURL ) {
		// find the section of the URL that has the JSessionID in it
		int jSessionIDBegin = encodedURL.indexOf( JSESSIONID_PATH );
		if( jSessionIDBegin < 0 ) {
			return encodedURL;
		}
		int jSessionIdEnd = jSessionIDBegin + JSESSIONID_PATH.length() + this.request.getSession().getId().length();

		// slice it out of the URL
		StringBuilder strippedURL = new StringBuilder();
		strippedURL.append( encodedURL.substring( 0, jSessionIDBegin ) );
		strippedURL.append( encodedURL.substring( jSessionIdEnd, encodedURL.length() ) );
		return strippedURL.toString();
	}
}
