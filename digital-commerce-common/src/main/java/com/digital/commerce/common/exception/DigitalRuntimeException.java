package com.digital.commerce.common.exception;

public class DigitalRuntimeException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DigitalRuntimeException( Throwable ex ) {
		super( ex );
	}

	public DigitalRuntimeException( String message ) {
		super( message );
	}

	public DigitalRuntimeException( String message, Throwable ex ) {
		super( message, ex );
	}
}
