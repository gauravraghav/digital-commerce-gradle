package com.digital.commerce.common.util;

import lombok.Getter;

@Getter
public enum RequestParametersConstant {
	PUSH_SITE("pushSite"),
	LOCALE("locale");
	
	private final String value;
	
	private RequestParametersConstant( String value ) {
		this.value = value;
	}
}
