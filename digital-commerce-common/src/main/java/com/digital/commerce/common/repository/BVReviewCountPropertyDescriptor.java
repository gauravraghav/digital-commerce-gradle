package com.digital.commerce.common.repository;

import com.digital.commerce.common.util.ComponentLookupUtil;

import atg.adapter.gsa.GSAPropertyDescriptor;
import atg.adapter.gsa.GSARepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;

/** @author Prabu
 *         This class is used in customCatalog.xml. It returns the Bazaar Voice Review Count for the given Product ID. */
public class BVReviewCountPropertyDescriptor extends GSAPropertyDescriptor {

	/**
	 * 
	 */
	private static final long		serialVersionUID		= -5746125569487756413L;

	protected static final String	TYPE_NAME				= "BVReviewCountPropertyDescriptor";

	protected static final String	RATING_REPO_ITEM		= "rating";

	protected static final String	REVIEW_COUNT_PROPERTY	= "reviewCount";

	static {
		GSAPropertyDescriptor.registerPropertyDescriptorClass( TYPE_NAME, BVReviewCountPropertyDescriptor.class );
	}

	public Object getPropertyValue( RepositoryItemImpl pItem, Object pValue ) {

		RepositoryItem ratingItem = null;

		// Get the Bazaar Voice Repository from Nucleus
		GSARepository bvRepository = (GSARepository)ComponentLookupUtil.lookupComponent( ComponentLookupUtil.BAZAAR_VOICE_REPOSITORY, GSARepository.class );
		Object ratingID = null;
		try {
			ratingID = pItem.getPropertyValue( "id" );
			// Get the "rating" item inside the repository
			ratingItem = bvRepository.getItem( (String)ratingID, RATING_REPO_ITEM );

		} catch( RepositoryException e ) {
			if( isLoggingError() ) {
				logError( "Exception reading BV rating for product " + ratingID );
				logError( e );
			}
		}

		// If "rating" item is available then attempt to get the "reviewCount" property from it and return
		if( ratingItem != null ) { return ratingItem.getPropertyValue( REVIEW_COUNT_PROPERTY ); }

		// else, return null to show as "No value specified" in BCC
		return null;
	}

	public String getTypeName() {
		return TYPE_NAME;
	}

}
