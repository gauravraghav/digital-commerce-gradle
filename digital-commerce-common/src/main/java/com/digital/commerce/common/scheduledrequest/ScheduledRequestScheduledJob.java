package com.digital.commerce.common.scheduledrequest;

import atg.service.scheduler.AbsoluteSchedule;
import atg.service.scheduler.Schedulable;
import atg.service.scheduler.ScheduledJob;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ScheduledRequestScheduledJob extends ScheduledJob {
	private ScheduledRequest	scheduledRequest;

	public ScheduledRequestScheduledJob( Schedulable schedulable, ScheduledRequest scheduledRequest ) {
		// using SCHEDULER_THREAD = 1;
		super( "ScheduledRequestScheduledJob", "Job to schedule nucleus components property changes", "/com/digital/scheduledrequest/ScheduledRequestManager", new AbsoluteSchedule( scheduledRequest.getSchedule().getTime() ), schedulable, false );
		this.scheduledRequest = scheduledRequest;
	}
}
