package com.digital.commerce.common.services;

import java.util.Map;

public interface ServiceAvailabilityAggregator {

	boolean isAddressValidationServiceEnabled();

	boolean isStoredValueServiceEnabled();

	boolean isAuthorizationServiceEnabled();

	boolean isTaxServiceEnabled();

	boolean isPaymentServiceEnabled();

	boolean isRewardServiceEnabled();

	boolean isOrderManagementServiceEnabled();

	boolean isEmailServiceEnabled();

	boolean isInventoryServiceEnabled();

	boolean isYantraInventoryServiceEnabled();

	boolean isAfterPayServiceEnabled();

	public Map<String, Boolean> getAddressValidationServiceMethodsEnabled();
	public Map<String, Boolean> getStoredValueServiceMethodsEnabled();
	public Map<String, Boolean> getAuthorizationServiceMethodsEnabled();
	public Map<String, Boolean> getTaxServiceMethodsEnabled();
	public Map<String, Boolean> getPaymentServiceMethodsEnabled();
	public Map<String, Boolean> getRewardServiceMethodsEnabled();
	public Map<String, Boolean> getInventoryServiceMethodsEnabled();
	public Map<String, Boolean> getYantraInventoryServiceMethodsEnabled();
	public Map<String, Boolean> getAfterPayServiceMethodsEnabled();

}