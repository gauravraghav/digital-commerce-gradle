package com.digital.commerce.common.util;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.digital.commerce.common.crypto.Base64;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.services.DigitalBaseConstants;

import atg.apache.lang.RandomStringUtils;
import lombok.Getter;
import lombok.Setter;

/**
 * @author HB391569
 *
 */
@Getter
@Setter
public class DigitalAESEncryptionUtil {
	private static transient DigitalLogger logger = DigitalLogger.getLogger(DigitalAESEncryptionUtil.class);
	private static final String ALGORITHM = "AES";
	private DigitalBaseConstants dswConstants;

	/**
	 * @param plainText
	 * @return
	 * @throws DigitalAppException
	 */
	public String encrypt(String plainText) throws DigitalAppException {
		String retval = null;
		String salt = null;
		try {
			salt = this.getSalt();
			if (logger.isDebugEnabled()) {
				logger.debug("salt:  " + salt);
			}
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			SecretKeySpec k = new SecretKeySpec(salt.getBytes(), ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, k);
			byte[] encryptedTextBytes = cipher.doFinal(plainText.getBytes("UTF-8"));
			String encrStr = Base64.encode(encryptedTextBytes);
			if (logger.isDebugEnabled()) {
				logger.debug("Encrypted string:  " + encrStr);
			}
			retval = URLEncoder.encode(Base64.encode(salt.getBytes()) + "::" + encrStr,"UTF-8");
		} catch (Exception ex) {
			logger.error("Error while AES  encrypting the string ", ex);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Final encrypted: " + retval);
		}
		return retval;
	}

	/**
	 * @param encryptedText
	 * @return
	 * @throws DigitalAppException
	 */
	public String decrypt(String encryptedText) throws DigitalAppException {
		String decrStr = null;
		try {
			String tmp = URLDecoder.decode(encryptedText,"UTF-8");
			String[] tokens = tmp.split("::");
			if (logger.isDebugEnabled()) {
				logger.debug("Tokens for decryption : " + Arrays.toString(tokens));
			}
			if (null == tokens || tokens.length < 2) {
				return decrStr;
			}
			String salt = new String(Base64.decode(tokens[0]));
			if (logger.isDebugEnabled()) {
				logger.debug("Salt for decryption : " + salt);
			}

			// Decrypt salt with static key
			SecretKeySpec k = new SecretKeySpec(salt.getBytes(), ALGORITHM);
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, k);
			byte[] decryptedTextBytes = cipher.doFinal(Base64.decode(tokens[1]));
			if (null != decryptedTextBytes && decryptedTextBytes.length > 0) {
				decrStr = new String(decryptedTextBytes);
			}
		} catch (Exception e) {
			logger.error("Error while AES decrypting the string ", e);
		}
		return decrStr;
	}

	/**
	 * @return
	 */
	private String getSalt() {
		StringBuilder sb = new StringBuilder();
		sb.append(RandomStringUtils.random(32, true, true));
		return sb.toString();
	}
}
