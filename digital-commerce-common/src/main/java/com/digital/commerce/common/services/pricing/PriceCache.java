package com.digital.commerce.common.services.pricing;

import java.util.HashMap;
import java.util.Map;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;

/**
 * A class that will store query results and item prices in maps. This is a
 * per-request caching mechanism only since most pages inefficiently make the
 * same price calls several times.
 * 
 *
 */
public class PriceCache extends GenericService {

	private final Map<String, Double> priceCache = new HashMap<>();
	private final Map<String, Object> itemCache = new HashMap<>();
	private final Map<String, Integer> accessCount = new HashMap<>();

	private int cacheMisses = 0;
	private int cacheHits = 0;

	private static final String ITEM_KEY_PREFIX = "item:";
	private static final String PRICE_KEY_PREFIX = "price:";

	@Override
	public void doStartService() throws ServiceException {
		super.doStartService();
		if (isLoggingDebug()) {
			logDebug("Starting PriceCache Service");
		}
	}

	@Override
	public void doStopService() throws ServiceException {
		super.doStopService();
		if (isLoggingDebug()) {
			logDebug("Stopped PriceCache Service");
			logDebug("------ PriceCache Stats ------");
			logDebug("Total Requests: [ " + (cacheHits + cacheMisses) 
					+ " ] Hits: [ " + cacheHits 
					+ " ] Misses: [ " + cacheMisses + " ]");
			for (String key : accessCount.keySet()) {
				logDebug('\t' + "Key: [ " + key + " ] Hit Count: [ " + accessCount.get(key) + " ]");
			}
		}
		priceCache.clear();
		itemCache.clear();
		accessCount.clear();
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public final Double getPrice(final String key) {
		if (priceCache.containsKey(key)) {
			if (isLoggingDebug()) {
				if (accessCount.containsKey(PRICE_KEY_PREFIX + key)) {
					int hitCount = accessCount.get(PRICE_KEY_PREFIX + key) + 1;
					accessCount.put(PRICE_KEY_PREFIX + key, hitCount);
				} else {
					accessCount.put(PRICE_KEY_PREFIX + key, 1);
				}
				cacheHits++;
			}
			return priceCache.get(key);
		} else {
			if (isLoggingDebug()) {
				cacheMisses++;
			}
			return null;
		}
	}

	/**
	 * 
	 * @param key
	 * @param price
	 */
	public final void putPrice(final String key, final Double price) {
		priceCache.put(key, price);
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public final Object getItem(final String key) {
		if (itemCache.containsKey(key)) {
			if (isLoggingDebug()) {
				if (accessCount.containsKey(ITEM_KEY_PREFIX + key)) {
					int hitCount = accessCount.get(ITEM_KEY_PREFIX + key) + 1;
					accessCount.put(ITEM_KEY_PREFIX + key, hitCount);
				} else {
					accessCount.put(ITEM_KEY_PREFIX + key, 1);
				}
				cacheHits++;
			}
			return itemCache.get(key);
		} else {
			if (isLoggingDebug()) {
				cacheMisses++;
			}
			return null;
		}
	}

	/**
	 * 
	 * @param key
	 * @param item
	 */
	public final void putItem(final String key, final Object item) {
		itemCache.put(key, item);
	}

}
