package com.digital.commerce.common.version;


import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.LinkedHashMap;

@Getter
@Setter
public class DigitalVersionController {

	String buildNumber="@bamboo.buildNumber@";
	String buildTimeStamp="@bamboo.buildTimeStamp@";
	String revisionNumber= "@bamboo.repository.revision.number@";
	String projectVersion="@project.version@";
	String version="@project.version@.@bamboo.buildNumber@.@bamboo.repository.revision.number@";
	String buildKey="@bamboo.buildKey@";
	String branchName="@bamboo.branchName@";
	
	String commonModuleVersion="@bamboo.commonModVersion@";
	
	protected HashMap<String, HashMap<String, String>> versionDetailsMap;
	
	public void getVersionDetails(){
		versionDetailsMap = new LinkedHashMap<>();
		HashMap<String, String> versionMap = new LinkedHashMap<>();
		versionMap.put("buildNumber", getBuildNumber());
		versionMap.put("buildTimeStamp", getBuildTimeStamp());
		versionMap.put("revisionNumber", getRevisionNumber());
		versionMap.put("projectVersion", getProjectVersion());
		versionMap.put("version", getVersion());
		versionMap.put("buildKey", getBuildKey());
		versionMap.put("branchName", getBranchName());
		
		versionMap.put("commonModuleVersion", this.getCommonModuleVersion());
		versionDetailsMap.put("dswBuildVersion",versionMap);
	}
}
