package com.digital.commerce.common.crypto;

/** <P>
 * Exception for crypto utilities */

public class CryptoException extends Exception

{
	private static final long	serialVersionUID	= -8866148980826855441L;

	/** <P>
	 * constructor
	 * 
	 * @param message type String */

	public CryptoException( String message ) {
		super( message );
	}
}
