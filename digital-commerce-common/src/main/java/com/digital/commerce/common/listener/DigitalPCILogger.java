package com.digital.commerce.common.listener;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.repository.ItemDescriptorImpl;
import atg.repository.PropertiesChangedEvent;
import atg.repository.PropertiesChangedListener;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.security.ThreadSecurityManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.userdirectory.UserDirectoryLoginUserAuthority;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalPCILogger extends GenericService implements PropertiesChangedListener {

	private Repository	repository;
	private String[]	itemType;
	private String[] 	inclusionList;
	private String[] 	exclusionList;
	
	public void doStartService() throws ServiceException {
		try {
			Repository rep = (Repository)getRepository();
			if( rep != null ) {
				for (String itemTypeDesc : itemType) {
					ItemDescriptorImpl idesc = (ItemDescriptorImpl)rep.getItemDescriptor( itemTypeDesc );
					if( idesc != null ) {
						idesc.addPropertiesChangedListener( this );
					}
				}
			}

		} catch( RepositoryException rex ) {
			if( isLoggingError() ) {
				logError( rex );
			}
		}
	}

	public void propertiesChanged( PropertiesChangedEvent event ) {

		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		if( request != null ) {
			String cPath = request.getContextPath();
			String queryParam = request.getQueryParameter("activity");
			logInfo("cPath-->"+cPath+" queryParam-->"+queryParam);
			
			/*if( cPath.contains( "/AssetManager" ) || !(cPath.contains("dyn/admin"))) {*/
			
			if(checkInclusionList(cPath) || !checkExclusionList(cPath) ){
				logInfo( "***********************************************" );
				logInfo( "A property Changed" );
				logInfo( "Item Descriptor: " + event.getItemDescriptorName() );
				logInfo( "Repository Id: " + event.getRepositoryId() );
				logInfo( "Type: " + event.getType() );
				logInfo( "Class: " + event.getClass() );
				logInfo( "Item: " + event.getItem() );
				logInfo( "Properties: " + event.getProperties() );
				logInfo( "Source: " + event.getSource() );
				printRequestAttributes( request, "Accessed" );
				logInfo( "***********************************************" );
			}
		}
	}

	boolean checkExclusionList(String item){
		String[] exclustionList = this.getExclusionList();
		boolean ret = false;
		if(exclustionList != null){
			for(String exlustion : exclustionList){
				if(item.contains(exlustion)){
					ret = true;
					break;
				}
			}
		}else{
			logInfo( "*********** Exclustion List not configured for DigitalPCILogger **********");
		}
		
		return ret;
	}
	
	boolean checkInclusionList(String item){
		String[] inclustionList = this.getInclusionList();
		boolean ret = false;
		if(inclustionList != null){
			for(String inclustion : inclustionList){
				if(item.contains(inclustion)){
					ret = true;
					break;
				}
			}
		}else{
			logInfo( "*********** Inclustion List not configured for DigitalPCILogger **********");
		}
		
		return ret;		
	}
	
	/** Prints request attributes passed (used for arroweye integration) can be
	 * turned off by setting showRequestAttributes to false
	 * 
	 * @param req */
	private void printRequestAttributes( DynamoHttpServletRequest req, String operation ) {
		if( req != null ) {
			String cPath = req.getContextPath();
			logInfo( "Contex Path = " + cPath );
			logInfo( "Source IP = " + req.getRemoteAddr() );
			logInfo( "Remote User = " + req.getRemoteUser() );
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss:SSS" );
			logInfo( "AT " + sdf.format( cal.getTime() ) );
			UserDirectoryLoginUserAuthority myUserAuthority = (UserDirectoryLoginUserAuthority)req.resolveName( "/atg/dynamo/security/AdminUserAuthority" );
			if( myUserAuthority != null ) {
				atg.security.AliasedPersona ap = (atg.security.AliasedPersona)ThreadSecurityManager.currentUser().getPrimaryPersona( myUserAuthority );
				if( ap != null ) {
					logInfo( "User = " + ap.getName() );
				}
			}
		}
	}
}
