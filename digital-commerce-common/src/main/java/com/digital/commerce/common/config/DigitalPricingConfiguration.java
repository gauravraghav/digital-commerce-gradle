package com.digital.commerce.common.config;

import atg.commerce.pricing.priceLists.PriceListManager;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

/** @author wibrahim */
public interface DigitalPricingConfiguration {
	public RepositoryItem getCategory();

	public void setCategory( RepositoryItem cat );

	public RepositoryItem getPriceList();

	/* (non-Javadoc)
	 * @see com.digital.edt.common.DigitalPricingConfiguration#getPriceListManager() */
	public PriceListManager getPriceListManager();

	/* (non-Javadoc)
	 * @see com.digital.edt.common.DigitalPricingConfiguration#getProfile() */
	public Profile getProfile();

	/* (non-Javadoc)
	 * @see com.digital.edt.common.DigitalPricingConfiguration#setPriceList(atg.repository.RepositoryItem) */
	public void setPriceList( RepositoryItem priceList );

	public void setPriceListManager( PriceListManager priceListManager );

	/* (non-Javadoc)
	 * @see com.digital.edt.common.DigitalPricingConfiguration#setProfile(atg.userprofiling.Profile) */
	public void setProfile( Profile profile );
}
