package com.digital.commerce.common.servlet;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This is an attempt at immediately removing items from the
 * /atg/userprofiling/ProfileAdapterRepository as soon as a session is
 * destroyed.
 * 
 * @author knaas
 */
public class SessionListener implements HttpSessionListener {

	private static final Logger	logger	= LogManager.getLogger( SessionListener.class );

	@Override
	public void sessionCreated( HttpSessionEvent event ) {
			final HttpSession session = event.getSession();
			String sessionId = null;
			String sessionCreationTime = null;
			if(null != session) {
				sessionId = session.getId();
				sessionCreationTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss:SSS").format(new Date (session.getCreationTime()));
				// this flag is used in DigitalOrderCleanUpServlet to clean-up any previous session payment group (PayPal & GiftCard) attached with the order
				session.setAttribute("CLEANUP_PAYMENT", true);
			}
			logger.info( String.format( "New session %s is created at %s ", sessionId, sessionCreationTime));
	}

	@Override
	public void sessionDestroyed( HttpSessionEvent event ) {
		String sessionId = null;
		long startTime = 0;
		try {
			startTime = System.currentTimeMillis();
			final HttpSession session = event.getSession();
			String sessionCreationTime = null; 
			String sessionLastAccessedTime = null;			
			if(null != session) {
				sessionId = session.getId();
				sessionCreationTime = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss:SSS").format(new Date (session.getCreationTime()));
				sessionLastAccessedTime = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss:SSS").format(new Date (session.getLastAccessedTime()));
				logger.info( String.format( "Existing session %s is destroyed that was created at %s, last accessed at %s with maximum inactivity interval %d seconds",sessionId,sessionCreationTime,sessionLastAccessedTime, session.getMaxInactiveInterval() ));
			}
			
		} catch( Throwable t ) {
			if( logger.isDebugEnabled() ) {
				logger.debug( "Unable to remove item from cache", t );
			}
		} finally {
			long endTime = System.currentTimeMillis();
			logger.info("Sessionid " + sessionId + " took " + (endTime-startTime) + " milliseconds to destroy & completed at " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss:SSS").format(new Date()));
		}
	}
}
