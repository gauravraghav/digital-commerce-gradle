package com.digital.commerce.csc.common.util;

import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.ProfileServices;

import com.digital.commerce.common.csc.ContactCenterUser;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.services.utils.DigitalServiceConstants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class DigitalInsoleUserTools {
	
	private String contactCenterUserPath;
	
	private DigitalServiceConstants	dswConstants;
	
	private ProfileServices profileServices;
	
	public Boolean verifyInsoleLoginRules(DynamoHttpServletRequest request) {
		ContactCenterUser contactCenterUser = (ContactCenterUser) request
				.resolveName(contactCenterUserPath);
		boolean cscUser = false;
		if (contactCenterUser != null
				&& contactCenterUser.getGroups() != null) {
			String[] cscGroups = contactCenterUser.getGroups();
			for (String group : cscGroups) {
				if (DigitalStringUtil.isNotBlank(group)) {
					for (String insoleRole : getDswConstants().getInsoleRole())
						if (group.contains(insoleRole)) {
							cscUser = true;
							break;
						}
				}
			}
		}
		return cscUser;
	}


	/**
	 * @param login
	 * @return
	 * @throws DigitalAppException
	 */
	public String getUserId(String login) throws DigitalAppException {
		try {
			return profileServices.getProfileId(login);
		} catch (Exception ex) {
			throw new DigitalAppException(ex.getMessage());
		}
	}	
}
