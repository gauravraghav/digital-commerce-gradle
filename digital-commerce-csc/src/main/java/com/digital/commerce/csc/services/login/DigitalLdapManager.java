package com.digital.commerce.csc.services.login;

import com.digital.commerce.common.csc.ContactCenterUser;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.ldap.DigitalLdapTools;
import lombok.Getter;
import lombok.Setter;

/**
 */
@Getter
@Setter
public class DigitalLdapManager {

	private DigitalLdapTools ldapTools;

	public boolean checkAgentAuthentication(String pLogin, String pPassword,
			ContactCenterUser contantCenterUser) throws DigitalAppException {
		return getLdapTools().doUserAuthetication(pLogin, pPassword,
				contantCenterUser, null);
	}

}
