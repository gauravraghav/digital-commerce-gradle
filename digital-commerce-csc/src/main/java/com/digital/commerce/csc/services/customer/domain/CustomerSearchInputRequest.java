package com.digital.commerce.csc.services.customer.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CustomerSearchInputRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String firstName;
	private String lastName;
	private String billingEmail;
	private String phoneNumber;
	private String loyaltyNumber;
	private String orderNumber;
	private String login;

}
