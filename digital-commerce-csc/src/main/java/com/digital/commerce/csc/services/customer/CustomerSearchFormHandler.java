package com.digital.commerce.csc.services.customer;

import java.io.IOException;

import javax.servlet.ServletException;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.csc.ContactCenterUser;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.csc.common.util.DigitalInsoleUserTools;
import com.digital.commerce.csc.services.customer.domain.CustomerOrdersSearchResultVO;
import com.digital.commerce.csc.services.customer.domain.CustomerSearchInputRequest;
import com.digital.commerce.csc.services.customer.domain.CustomerSearchResultVO;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.dtm.UserTransactionDemarcation;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerSearchFormHandler extends GenericFormHandler {
	
	private CustomerSearchInputRequest customerSearchInputRequest = new CustomerSearchInputRequest();
	private static final String CLASSNAME = CustomerSearchFormHandler.class.getName();
	
	private String profileId;
	
	private String orderId;
	
	private String email;
	
	private DigitalSearchingTools searchingTools;

	private MessageLocator messageLocator;
	
	private String state;

	private CustomerSearchResultVO customerSearchResult;

	private CustomerOrdersSearchResultVO customerOrdersSearchResult;
	
	private RepeatingRequestMonitor			repeatingRequestMonitor;

	private DigitalInsoleUserTools insoleUserTools;
	
	/**
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleSearchCustomers(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String METHOD_NAME = "CustomerSearchFormHandler.handleSearchCustomers";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			if (!getInsoleUserTools().verifyInsoleLoginRules(pRequest)) {
				String msg = getMessageLocator().getMessageString("insole.user.login.required");
				addFormException(new DropletException(msg, "INSOLE_USER_LOGIN_REQUIRED"));
				return false;
			}
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
				if (customerSearchInputRequest != null) {
					if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getLogin())) {
						customerSearchInputRequest.setLogin(customerSearchInputRequest.getLogin().toLowerCase());
					}
					if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getBillingEmail())) {
						customerSearchInputRequest
								.setBillingEmail(customerSearchInputRequest.getBillingEmail().toLowerCase());
					}
					if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getOrderNumber())) {
						customerSearchInputRequest
								.setOrderNumber(customerSearchInputRequest.getOrderNumber().toLowerCase());
					}
					if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getLoyaltyNumber())) {
						customerSearchInputRequest
								.setLoyaltyNumber(customerSearchInputRequest.getLoyaltyNumber().toLowerCase());
					}
					customerSearchResult = getSearchingTools().searchOrdersOnCriteria(customerSearchInputRequest);
					return true;
				} else {
					addFormException(new DropletException(
							messageLocator.getMessageString("insole.customer.search.criteria.missing"),
							"INSOLE_CUSTOMER_SEARCH_CRITERIA_MISSING"));
					return false;
				}
			} catch (DigitalAppException e) {
				if (isLoggingError()) {
					logError("Error customer search", e);
				}
				addFormException(new DropletException(getMessageLocator().getMessageString(e.getLocalizedMessage()),
						"CUSTOMER_SEARCH_ERROR"));
			} finally {
				if(rrm != null){
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			}
		}
		return false;
	}
	
	/**
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleCustomerOrders(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String METHOD_NAME = "CustomerSearchFormHandler.handleCustomerOrders";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			if (!getInsoleUserTools().verifyInsoleLoginRules(pRequest)) {
				String msg = getMessageLocator().getMessageString("insole.user.login.required");
				addFormException(new DropletException(msg, "INSOLE_USER_LOGIN_REQUIRED"));
				return false;
			}
			UserTransactionDemarcation td = null;
			try {
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
				if (getProfileId() != null) {
					customerOrdersSearchResult = getSearchingTools().searchCustomerOrders(getProfileId());
				} else {
					addFormException(new DropletException(
							messageLocator.getMessageString("insole.customer.search.orders.missing.profileid"),
							"INSOLE_CUSTOMER_SEARCH_ORDERS_MISSING_PROFILEID"));
					return false;
				}
				return true;
			} catch (DigitalAppException e) {
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				if (isLoggingError()) {
					logError("Error customer orders search", e);
				}
				addFormException(new DropletException(
						getMessageLocator().getMessageString(e.getLocalizedMessage(), getProfileId()),
						"CUSTOMER_ORDERS_SEARCH_ERROR"));
			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				if(rrm != null){
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			}
		}
		return false;
	}
	
	/**
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleCustomerIncompleteOrders(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		final String METHOD_NAME = "CustomerSearchFormHandler.handleCustomerIncompleteOrders";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			if (!getInsoleUserTools().verifyInsoleLoginRules(pRequest)) {
				String msg = getMessageLocator().getMessageString("insole.user.login.required");
				addFormException(new DropletException(msg, "INSOLE_USER_LOGIN_REQUIRED"));
				return false;
			}
			UserTransactionDemarcation td = null;
			try {
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
				if (getEmail() != null) {
					customerOrdersSearchResult = getSearchingTools().searchCustomerIncompleteOrders(getEmail());
				} else {
					addFormException(new DropletException(
							messageLocator.getMessageString("insole.customer.search.orders.missing.email"),
							"INSOLE_CUSTOMER_SEARCH_ORDERS_MISSING_EMAIL"));
					return false;
				}
				return true;
			} catch (DigitalAppException e) {
				if (isLoggingError()) {
					logError("Error customer incomplete orders search", e);
				}
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				addFormException(
						new DropletException(getMessageLocator().getMessageString(e.getLocalizedMessage(), getEmail()),
								"CUSTOMER_INCOMPLETE_ORDERS_SEARCH_ERROR"));
			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				if(rrm != null){
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			}
		}
		return false;
	}
	
	/**
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleResetBadOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String METHOD_NAME = "CustomerSearchFormHandler.handleResetBadOrder";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			if (!getInsoleUserTools().verifyInsoleLoginRules(pRequest)) {
				String msg = getMessageLocator().getMessageString("insole.user.login.required");
				addFormException(new DropletException(msg, "INSOLE_USER_LOGIN_REQUIRED"));
				return false;
			}
			UserTransactionDemarcation td = null;
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				if (getOrderId() != null) {
					state = getSearchingTools().resetBadOrder(getOrderId());
					if (isLoggingInfo()) {
						try {
							ContactCenterUser contactCenterUser = ComponentLookupUtil
									.lookupComponent(ComponentLookupUtil.INSOLE_AGENT_BEAN, ContactCenterUser.class);
							StringBuilder message = new StringBuilder();
							message.append("Insole agent ");
							if (null != contactCenterUser) {
								message.append(contactCenterUser.getUserId());
							}
							message.append(" attempted to fix the customer order " + orderId);
							logInfo(message.toString());
						} catch (Exception e1) {
							logError("Error logging InSole Application Data");
						}
					}
				} else {
					addFormException(new DropletException(
							messageLocator.getMessageString("insole.customer.reset.order.missing.orderId"),
							"INSOLE_RESET_ORDER_MISSING_ORDERID"));
					return false;
				}
				return true;
			} catch (DigitalAppException e) {
				if (isLoggingError()) {
					logError("Error Reset Bad order", e);
				}
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				addFormException(new DropletException(
						getMessageLocator().getMessageString(e.getLocalizedMessage(), getOrderId()),
						"RESET_BAD_ORDER_ERROR"));
				return false;
			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				if(rrm != null){
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
		return false;
	}
	
	/**
	 * @param pRequest
	 * @param pResponse
	 * @return true or false
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleClearPromotions(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String METHOD_NAME = "CustomerSearchFormHandler.handleClearPromotions";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(METHOD_NAME)) {
			if (!getInsoleUserTools().verifyInsoleLoginRules(pRequest)) {
				String msg = getMessageLocator().getMessageString("insole.user.login.required");
				addFormException(new DropletException(msg, "INSOLE_USER_LOGIN_REQUIRED"));
				return false;
			}
			UserTransactionDemarcation td = null;
			try {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getName(), METHOD_NAME);
				td = TransactionUtils.startNewTransaction(CLASSNAME, METHOD_NAME);
				if (getProfileId() != null) {
					if (isLoggingInfo()) {
						try {
							ContactCenterUser contactCenterUser = ComponentLookupUtil
									.lookupComponent(ComponentLookupUtil.INSOLE_AGENT_BEAN, ContactCenterUser.class);
							StringBuilder message = new StringBuilder();
							message.append("Insole agent ");
							if (null != contactCenterUser) {
								message.append(contactCenterUser.getUserId());
							}
							message.append(" attempted to fix promotions for customer with id " + profileId);
							logInfo(message.toString());
						} catch (Exception e1) {
							logError("Error logging InSole Application Data");
						}
					}
					state = getSearchingTools().clearPromotions(getProfileId());
				} else {
					addFormException(new DropletException(
							messageLocator.getMessageString("insole.customer.reset.promotions.missing.profileId"),
							"INSOLE_RESET_PROMOTIONS_MISSING_PROFILEID"));
					return false;
				}
				return true;
			} catch (DigitalAppException e) {
				if (isLoggingError()) {
					logError("Error customer search", e);
				}
				TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
				addFormException(new DropletException(
						getMessageLocator().getMessageString(e.getLocalizedMessage(), getProfileId()),
						"RESET_PROMOTIONS_ERROR"));
				return false;

			} finally {
				TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);
				if(rrm != null){
					rrm.removeRequestEntry(METHOD_NAME);
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getName(), METHOD_NAME);
			}
		}
		return false;
	}

}
