package com.digital.commerce.csc.services.customer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.csc.services.customer.domain.CustomerOrders;
import com.digital.commerce.csc.services.customer.domain.CustomerOrdersSearchResultVO;
import com.digital.commerce.csc.services.customer.domain.CustomerProfile;
import com.digital.commerce.csc.services.customer.domain.CustomerSearchInputRequest;
import com.digital.commerce.csc.services.customer.domain.CustomerSearchResultVO;
import com.digital.commerce.services.order.DigitalGiftCard;
import com.digital.commerce.services.order.DigitalOrderImpl;
import com.digital.commerce.services.order.DigitalShippingGroupManager;
import com.digital.commerce.services.order.payment.paypal.PaypalPayment;

import atg.commerce.CommerceException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.core.util.ContactInfo;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.QueryOptions;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.repository.SortDirective;
import atg.repository.SortDirectives;
import atg.repository.rql.RqlStatement;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"rawtypes"})
@Setter
@Getter
public class DigitalSearchingTools extends ApplicationLoggingImpl{

	private Repository profileRepository;
	
	private Repository orderRepository;
	
	private MessageLocator messageLocator;
	
	private CustomerSearchResultVO customerSearchResult;

	private CustomerOrdersSearchResultVO customerOrdersSearchResult;
	
	private OrderManager orderManager;
	
	private DigitalShippingGroupManager dswShippingGroupManager;
	
	private DigitalBaseConstants dswConstants;

	public DigitalSearchingTools() {
		super(DigitalSearchingTools.class.getName());
	}

	/**
	 * @param customerSearchInputRequest
	 * @return
	 * @throws DigitalAppException
	 */
	public CustomerSearchResultVO searchOrdersOnCriteria(CustomerSearchInputRequest customerSearchInputRequest)
			throws DigitalAppException {

		if (customerSearchInputRequest != null && DigitalStringUtil.isBlank(customerSearchInputRequest.getOrderNumber())
				&& DigitalStringUtil.isBlank(customerSearchInputRequest.getLoyaltyNumber())
				&& DigitalStringUtil.isBlank(customerSearchInputRequest.getBillingEmail())
				&& DigitalStringUtil.isBlank(customerSearchInputRequest.getLogin())
				&& DigitalStringUtil.isBlank(customerSearchInputRequest.getPhoneNumber())
				&& DigitalStringUtil.isBlank(customerSearchInputRequest.getFirstName())
				&& DigitalStringUtil.isBlank(customerSearchInputRequest.getLastName())) {
			throw new DigitalAppException("insole.customer.search.criteria.missing");
		}
		
		try {
			Repository mutRepp = getOrderRepository();
			RepositoryItemDescriptor ordersDesc = mutRepp.getItemDescriptor("order");
			RepositoryView orderRepView = ordersDesc.getRepositoryView();
			QueryBuilder qBuild = orderRepView.getQueryBuilder();
			QueryExpression qeName = null;
			QueryExpression qeNameVal = null;
			Query qName = null;
			Query andQuery = null;
			RepositoryItem[] orderItems = null;

			Repository mutProfileRepp = getProfileRepository();
			RepositoryItemDescriptor usersDesc = mutProfileRepp.getItemDescriptor("user");
			RepositoryView userRepView = usersDesc.getRepositoryView();
			QueryBuilder qUserBuild = userRepView.getQueryBuilder();
			QueryExpression pQeName = null;
			QueryExpression pQeNameVal = null;
			Query pQName = null;
			Query pAndQuery = null;
			RepositoryItem[] userItems = null;
			boolean searchFlag = true;

			List<String> profileIdList = new ArrayList<>();

			// Below search always searches using only one search criteria even
			// though multiple search inputs are passed from UI.
			if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getOrderNumber())) {
				try{
					DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_ORDERNUM");
					qeName = qBuild.createPropertyQueryExpression("id");
					qeNameVal = qBuild.createConstantQueryExpression(customerSearchInputRequest.getOrderNumber());
					qName = qBuild.createPatternMatchQuery(qeName, qeNameVal, QueryBuilder.EQUALS, false);
					Query[] pieces = { qName };
					andQuery = qBuild.createAndQuery(pieces);
	
					orderItems = orderRepView.executeQuery(andQuery);
	
					if (orderItems != null && orderItems.length == 1) {
						for (RepositoryItem orderItem : orderItems){
							profileIdList.add((String) orderItem.getPropertyValue("profileId"));
						}
					}
					if (profileIdList.size() > 0) {
						pQeName = qUserBuild.createPropertyQueryExpression("id");
						pQeNameVal = qUserBuild.createConstantQueryExpression(profileIdList);
						pQName = qUserBuild.createIncludesAnyQuery(pQeName, pQeNameVal);
						userItems = userRepView.executeQuery(pQName);
						if(userItems != null && userItems.length > 0) {
							searchFlag = false;
						}
					}
				}
				finally{
					DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_ORDERNUM");
				}
			}

			if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getLoyaltyNumber()) && searchFlag) {
				try{
					DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_LOYALTYNUM");
					pQeName = qUserBuild.createPropertyQueryExpression("loyaltyNumber");
					pQeNameVal = qUserBuild.createConstantQueryExpression(customerSearchInputRequest.getLoyaltyNumber());
					pQName = qUserBuild.createPatternMatchQuery(pQeName, pQeNameVal, QueryBuilder.EQUALS, false);
					Query[] pieces = { pQName };
					pAndQuery = qUserBuild.createAndQuery(pieces);
					userItems = userRepView.executeQuery(pAndQuery);
					if(userItems != null && userItems.length > 0) {
						searchFlag = false;
					}
				}
				finally{
					DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_LOYALTYNUM");
				}
			}
			if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getBillingEmail()) && searchFlag) {
				try{
					DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_EMAIL");
					pQeName = qUserBuild.createPropertyQueryExpression("billingAddress.email");
					pQeNameVal = qUserBuild.createConstantQueryExpression(customerSearchInputRequest.getBillingEmail());
					pQName = qUserBuild.createPatternMatchQuery(pQeName, pQeNameVal, QueryBuilder.EQUALS, false);
					if (pAndQuery != null) {
						Query[] pieces = { pAndQuery, pQName };
						pAndQuery = qUserBuild.createAndQuery(pieces);
					} else {
						Query[] pieces = { pQName };
						pAndQuery = qUserBuild.createAndQuery(pieces);
					}
					userItems = userRepView.executeQuery(pAndQuery);
					if(userItems != null && userItems.length > 0) {
						searchFlag = false;
					}
				}
				finally{
					DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_EMAIL");
				}
			}
			if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getLogin()) && searchFlag) {
				try{
					DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_LOGIN");
					pQeName = qUserBuild.createPropertyQueryExpression("login");
					pQeNameVal = qUserBuild.createConstantQueryExpression(customerSearchInputRequest.getLogin());
					pQName = qUserBuild.createPatternMatchQuery(pQeName, pQeNameVal, QueryBuilder.EQUALS, false);
					if (pAndQuery != null) {
						Query[] pieces = { pAndQuery, pQName };
						pAndQuery = qUserBuild.createAndQuery(pieces);
					} else {
						Query[] pieces = { pQName };
						pAndQuery = qUserBuild.createAndQuery(pieces);
					}
					userItems = userRepView.executeQuery(pAndQuery);
					if(userItems != null && userItems.length > 0) {
						searchFlag = false;
					}
				}
				finally{
					DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_LOGIN");
				}
			}
			if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getPhoneNumber()) && searchFlag) {
				DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_PHONENUM");
				pQeName = qUserBuild.createPropertyQueryExpression("mobilePhoneNumber");
				String phone  = customerSearchInputRequest.getPhoneNumber();
				phone = phone.replace("-", "");
				phone = phone.replace(".", "");
				if(isLoggingDebug()) {
					logDebug("cleaned-up phone number: " + phone);
				}
				pQeNameVal = qUserBuild.createConstantQueryExpression(phone);
				pQName = qUserBuild.createPatternMatchQuery(pQeName, pQeNameVal, QueryBuilder.EQUALS, false);

				QueryExpression pQeNameHomePhone = qUserBuild.createPropertyQueryExpression("homeAddress.phoneNumber");
				Query pQNameHomePhone = qUserBuild.createPatternMatchQuery(pQeNameHomePhone, pQeNameVal,
						QueryBuilder.EQUALS, false);

				if (pAndQuery != null) {
					Query[] pieces = { pQName, pQNameHomePhone };
					Query q = qUserBuild.createOrQuery(pieces);
					Query[] piecesAnd = { pAndQuery, q };
					pAndQuery = qUserBuild.createAndQuery(piecesAnd);
				} else {
					Query[] pieces = { pQName, pQNameHomePhone };
					pAndQuery = qUserBuild.createOrQuery(pieces);
				}
				userItems = userRepView.executeQuery(pAndQuery);
				if(userItems != null && userItems.length > 0) {
					searchFlag = false;
				}
				DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_PHONENUM");
			}
			if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getFirstName())
					&& DigitalStringUtil.isNotBlank(customerSearchInputRequest.getLastName()) && searchFlag) {
				try{
					DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_FIRSTNAME_LASTNAME");
					// using Rql to take advantage of EQUALS IGNORECASE
					Object[] params = new Object[] { customerSearchInputRequest.getFirstName(),
							customerSearchInputRequest.getLastName(), getDswConstants().getMaxSearchResults() };
					String rql = "firstName EQUALS IGNORECASE ?0 AND lastName EQUALS IGNORECASE ?1 RANGE +?2";
					userItems = findByRql(userRepView, params, rql);
					if(userItems != null && userItems.length > 0) {
						searchFlag = false;
					}
				}
				finally{
					DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_FIRSTNAME_LASTNAME");
				}
			} else if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getFirstName()) && searchFlag) {
				try{
					DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_FIRSTNAME");
					Object[] params = new Object[] { customerSearchInputRequest.getFirstName(), getDswConstants().getMaxSearchResults() };
					String rql = "firstName EQUALS IGNORECASE ?0 RANGE +?1";
					userItems = findByRql(userRepView, params, rql);
					if(userItems != null && userItems.length > 0) {
						searchFlag = false;
					}
					}
				finally{
					DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_FIRSTNAME");
				}
			} else if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getLastName()) && searchFlag) {
				try{
					DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_LASTNAME");
					Object[] params = new Object[] { customerSearchInputRequest.getLastName(), getDswConstants().getMaxSearchResults() };
					String rql = "lastName EQUALS IGNORECASE ?0 RANGE +?1";
					userItems = findByRql(userRepView, params, rql);
					if(userItems != null && userItems.length > 0) {
						searchFlag = false;
					}
				}
				finally{
					DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_LASTNAME");
				}
			}
			List<CustomerProfile> customerProfileList = new ArrayList<>();
			if (userItems != null && userItems.length > 0 ) {
				if(userItems.length >= 1000){
					throw new DigitalAppException("insole.customer.search.maxresult");
				}
				//qeName = qBuild.createPropertyQueryExpression("profileId");
				orderItems = null;
				try{
					DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_PROFILEID");
					for (RepositoryItem userItem : userItems) {
						CustomerProfile customerProfile = new CustomerProfile();
						customerProfile.setFirstName((String) userItem.getPropertyValue("firstName"));
						customerProfile.setLastName((String) userItem.getPropertyValue("lastName"));
						customerProfile.setLogin((String) userItem.getPropertyValue("login"));
						customerProfile.setProfileId((String) userItem.getPropertyValue("id"));
						/*
						 * get email,phonenumber and postalcode from billing address
						 * of the order for order search for all others get from the
						 * profile.
						 */
						if (DigitalStringUtil.isNotBlank(customerSearchInputRequest.getOrderNumber())) {
							ContactInfo address = getOrderNumberSearchDetails(customerSearchInputRequest);
							if (address != null) {
								customerProfile.setBillingEmail(address.getEmail());
								customerProfile.setPostalCode(address.getPostalCode());
								customerProfile.setPhoneNumber(address.getPhoneNumber());
							}
						} else {
	
							RepositoryItem billingAddress = (RepositoryItem) userItem
									.getPropertyValue("billingAddress");
							if (billingAddress != null) {
								customerProfile.setBillingEmail((String) billingAddress.getPropertyValue("email"));
								customerProfile.setPostalCode((String) billingAddress.getPropertyValue("postalCode"));
							}							
							RepositoryItem homeAddress = (RepositoryItem) userItem.getPropertyValue("homeAddress");
							if (homeAddress != null && homeAddress.getPropertyValue("phoneNumber") != null
									&& DigitalStringUtil.isNotBlank((String) homeAddress.getPropertyValue("phoneNumber"))) {
								customerProfile.setPhoneNumber((String) homeAddress.getPropertyValue("phoneNumber"));
							} else {
								customerProfile.setPhoneNumber((String) userItem.getPropertyValue("mobilePhoneNumber"));
							}
						}						
						
						customerProfile.setHasOrder(true);
						customerProfileList.add(customerProfile);
					}
					}
					finally{
						DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), "SERACHBY_PROFILEID");
					}
					customerSearchResult.setCustomer(customerProfileList);
			}
			 else {
				throw new DigitalAppException("insole.customer.search.noresult");
			}

		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException while searching Customer based on criteria ", e);
			}
			throw new DigitalAppException("insole.customer.search.repository.error");
		} catch (CommerceException e) {
			logError("RepositoryException while searching Customer based on criteria ", e);
		}
		return customerSearchResult;
	}

	/**
	 * @param customerSearchInputRequest
	 * @throws CommerceException
	 */
	@SuppressWarnings("unchecked")
	private ContactInfo getOrderNumberSearchDetails(
			CustomerSearchInputRequest customerSearchInputRequest)
			throws CommerceException {
		DigitalOrderImpl order = (DigitalOrderImpl)getOrderManager().loadOrder(customerSearchInputRequest.getOrderNumber());
		List<PaymentGroup> listPaymentGroups = order.getPaymentGroups();
		ContactInfo contactInfo = null;
		if(listPaymentGroups!=null){
			for (PaymentGroup paymentGroup : listPaymentGroups){
				if (paymentGroup instanceof CreditCard) {
					//get the payment group if its exists
					CreditCard creditCard = (CreditCard) paymentGroup;
					contactInfo = (ContactInfo)creditCard.getBillingAddress();							
				}else if( paymentGroup instanceof PaypalPayment ) {
					PaypalPayment paypalPayment = (PaypalPayment)paymentGroup;
					contactInfo = (ContactInfo)paypalPayment.getBillingAddress();					
				}else if( paymentGroup instanceof DigitalGiftCard ) {
					List<HardgoodShippingGroup> hgShippingGroups = getDswShippingGroupManager().getHardgoodShippingGroups(order);
					if(hgShippingGroups!=null){
						for (HardgoodShippingGroup hgShippingGroup : hgShippingGroups){
							contactInfo = (ContactInfo)hgShippingGroup.getShippingAddress();
						}
					}
				}
			}
		}
		return contactInfo;
	}

	/**
	 * @param userRepView
	 * @param params
	 * @return
	 * @throws RepositoryException
	 */
	private RepositoryItem[] findByRql(RepositoryView userRepView,
			Object[] params,String rql) throws RepositoryException {
		RepositoryItem[] userItems;
		RqlStatement statement = RqlStatement.parseRqlStatement( rql );
		userItems = statement.executeQuery( userRepView, params );
		return userItems;
	}
	
	/**
	 * @param profileId
	 * @return
	 * @throws DigitalAppException
	 */
	public CustomerOrdersSearchResultVO searchCustomerOrders(String profileId) throws DigitalAppException {
		
		if(DigitalStringUtil.isBlank(profileId)) {
					throw new DigitalAppException("insole.customer.search.profileid.missing");
				}
		try {
			Repository mutRepp = getOrderRepository();
			RepositoryItemDescriptor ordersDesc = mutRepp
					.getItemDescriptor("order");
			RepositoryView orderRepView = ordersDesc.getRepositoryView();
			QueryBuilder qBuild = orderRepView.getQueryBuilder();
			QueryExpression qeName = null;
			QueryExpression qeNameVal = null;
			Query qName = null;
			Query andQuery = null;
			RepositoryItem[] orderItems = null;
			
			Repository mutProfileRepp = getProfileRepository();
			RepositoryItemDescriptor usersDesc = mutProfileRepp
						.getItemDescriptor("user");
			RepositoryView userRepView = usersDesc.getRepositoryView();
			QueryBuilder qUserBuild = userRepView.getQueryBuilder();
			QueryExpression pQeName  = null;
			QueryExpression pQeNameVal = null;
			Query pQName = null;
			Query pAndQuery = null;
			RepositoryItem[] userItems = null;
			
			/*StringBuilder query = new StringBuilder();
			query.append( "select  from dps_user user,dps_user_address address where user.id=dps_user_address.id " );*/
			
			pQeName = qUserBuild
					.createPropertyQueryExpression("id");
			pQeNameVal = qUserBuild.createConstantQueryExpression(profileId);
			pQName = qUserBuild.createComparisonQuery(pQeName, pQeNameVal,
					QueryBuilder.EQUALS);
			Query[] pieces = { pQName };
			pAndQuery = qUserBuild.createAndQuery(pieces);
			userItems = userRepView.executeQuery(pAndQuery);
			
			if(userItems !=null && userItems.length>0){
				pieces = null;
				qeName = qBuild
						.createPropertyQueryExpression("profileId");
				qeNameVal = qBuild
						.createConstantQueryExpression(profileId);
				qName = qBuild.createComparisonQuery(qeName, qeNameVal,
						QueryBuilder.EQUALS);
				Query[] oPieces = { qName };
				andQuery = qBuild.createAndQuery(oPieces);
				orderItems = orderRepView.executeQuery(andQuery);
			}
				
			if (orderItems != null && orderItems.length > 0) {
				List<CustomerOrders> customerOrdersList = new ArrayList<>();
				for(RepositoryItem orderItem : orderItems) {
					CustomerOrders customerOrders = new CustomerOrders();
					customerOrders.setFirstName((String) userItems[0]
						.getPropertyValue("firstName"));
					customerOrders.setLastName((String) userItems[0]
						.getPropertyValue("lastName"));
					customerOrders.setLogin((String) userItems[0]
						.getPropertyValue("login"));
					customerOrders.setProfileId((String) userItems[0]
							.getPropertyValue("id"));
					RepositoryItem billingAddress = (RepositoryItem)userItems[0].getPropertyValue("billingAddress");
					if(billingAddress != null){
						customerOrders.setBillingEmail((String)billingAddress.getPropertyValue("email"));
						customerOrders.setPhoneNumber((String)billingAddress.getPropertyValue("phoneNumber"));
						customerOrders.setPostalCode((String)billingAddress.getPropertyValue("postalCode"));
					}
					customerOrders.setOrderNumber((String) orderItem
							.getPropertyValue("id"));
					
				customerOrdersList.add(customerOrders);
				}
				customerOrdersSearchResult.setCustomerOrders(customerOrdersList);
			} else {
				throw new DigitalAppException("insole.customer.order.search.noresult");
			}
			
		}
		catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException while searching orders for Customer Profile ",
						e);
			}
			throw new DigitalAppException("insole.customer.order.search.repository.error");
		}
		return customerOrdersSearchResult;
	}
	
	/**
	 * @param email
	 * @return
	 * @throws DigitalAppException
	 */
	public CustomerOrdersSearchResultVO searchCustomerIncompleteOrders(String email) throws DigitalAppException {
		
		if(DigitalStringUtil.isBlank(email)) {
					throw new DigitalAppException("insole.customer.search.email.missing");
				}
		try {
			Repository mutRepp = getOrderRepository();
			RepositoryItemDescriptor ordersDesc = mutRepp
					.getItemDescriptor("order");
			RepositoryView orderRepView = ordersDesc.getRepositoryView();
			QueryBuilder qBuild = orderRepView.getQueryBuilder();
			QueryExpression qeName = null;
			QueryExpression qeNameVal = null;
			Query qName = null;
			Query andQuery = null;
			RepositoryItem[] orderItems = null;
			
			Repository mutProfileRepp = getProfileRepository();
			RepositoryItemDescriptor usersDesc = mutProfileRepp
						.getItemDescriptor("user");
			RepositoryView userRepView = usersDesc.getRepositoryView();
			QueryBuilder qUserBuild = userRepView.getQueryBuilder();
			QueryExpression pQeName  = null;
			QueryExpression pQeNameVal = null;
			Query pQName = null;
			Query pAndQuery = null;
			RepositoryItem[] userItems = null;
			
			/*StringBuilder query = new StringBuilder();
			query.append( "select  from dps_user user,dps_user_address address where user.id=dps_user_address.id " );*/
			
			pQeName = qUserBuild
					.createPropertyQueryExpression("login");
			pQeNameVal = qUserBuild.createConstantQueryExpression(email);
			pQName = qUserBuild.createPatternMatchQuery(pQeName, pQeNameVal,
					QueryBuilder.EQUALS, false);
			Query[] pieces = { pQName };
			pAndQuery = qUserBuild.createAndQuery(pieces);
			userItems = userRepView.executeQuery(pAndQuery);
			
			if(userItems !=null && userItems.length>0){
				qeName = qBuild
						.createPropertyQueryExpression("profileId");
				qeNameVal = qBuild
						.createConstantQueryExpression((String)userItems[0].getPropertyValue("id"));
				qName = qBuild.createComparisonQuery(qeName, qeNameVal,
						QueryBuilder.EQUALS);
				
				qeName = qBuild
						.createPropertyQueryExpression("state");
				qeNameVal = qBuild
						.createConstantQueryExpression("INCOMPLETE");
				Query qState = qBuild.createComparisonQuery(qeName, qeNameVal,
						QueryBuilder.EQUALS);
				Query[] oPieces = { qName, qState };
				
				andQuery = qBuild.createAndQuery(oPieces);
				
				SortDirectives sortDirectives = new SortDirectives();
				  sortDirectives.addDirective(new SortDirective("id",
				SortDirective.DIR_DESCENDING));
				 String [] precachedPropertyNames = {"id"};
				  
				orderItems = orderRepView.executeQuery(andQuery, new QueryOptions(0, -1, sortDirectives,
						precachedPropertyNames));
			} else {
				throw new DigitalAppException("insole.customer.incompleteorder.customer.noresult");
			}
				
			if (orderItems != null && orderItems.length > 0) {
				List<CustomerOrders> customerIncompleteOrdersList = new ArrayList<>();
				for(RepositoryItem orderItem : orderItems) {
					CustomerOrders customerIncompleteOrders = new CustomerOrders();
					customerIncompleteOrders.setFirstName((String) userItems[0]
						.getPropertyValue("firstName"));
					customerIncompleteOrders.setLastName((String) userItems[0]
						.getPropertyValue("lastName"));
					customerIncompleteOrders.setProfileId((String) userItems[0]
							.getPropertyValue("id"));
					RepositoryItem billingAddress = (RepositoryItem)userItems[0].getPropertyValue("billingAddress");
					if(billingAddress != null){
						customerIncompleteOrders.setBillingEmail((String)billingAddress.getPropertyValue("email"));
						customerIncompleteOrders.setPhoneNumber((String)billingAddress.getPropertyValue("phoneNumber"));
						customerIncompleteOrders.setPostalCode((String)billingAddress.getPropertyValue("postalCode"));
					}
					if(null != userItems[0]
							.getPropertyValue("activePromotions") && null != userItems[0]
									.getPropertyValue("grantedCoupons")) {
						customerIncompleteOrders.setPromotionsExist(true);
					}
					customerIncompleteOrders.setOrderNumber((String) orderItem
							.getPropertyValue("id"));
					customerIncompleteOrders.setOrderStatus((String) orderItem
							.getPropertyValue("state"));
					
					
					
					customerIncompleteOrdersList.add(customerIncompleteOrders);
				}
				customerOrdersSearchResult.setCustomerOrders(customerIncompleteOrdersList);
			} else {
				throw new DigitalAppException("insole.customer.incompleteorder.search.noresult");
			}
			
		}
		catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException while searching incomplete orders for Customer Profile ",
						e);
			}
			throw new DigitalAppException("insole.customer.incompleteorder.search.repository.error");
		}
		return customerOrdersSearchResult;
	}
	
	/**
	 * @param orderId
	 * @return
	 * @throws DigitalAppException
	 */
	public String resetBadOrder(String orderId) throws DigitalAppException{
		MutableRepositoryItem badOrder;
		String state = "FAILED";
		try	{
			if( isLoggingDebug() ) {
				logDebug( "Handle Order Begin" );
				logDebug( "Order # = " +  orderId);
			}
			badOrder = (MutableRepositoryItem)((MutableRepository)getOrderRepository()).getItemForUpdate( orderId, "order" );
			String currentState = (String)badOrder.getPropertyValue( "state");
			if( badOrder != null && DigitalStringUtil.isNotBlank(currentState) && "INCOMPLETE".equalsIgnoreCase(currentState)) {
				badOrder.setPropertyValue( "state", "FAILED" );
				// get profile id to cleanup active promotions and granted coupons
				String profileId = (String)badOrder.getPropertyValue( "profileId" );
				if( profileId != null ) {
					try {
						clearPromotions(profileId );
					} catch (DigitalAppException de) {
						if( isLoggingDebug() ) {
							logDebug( "Nothing to clear in promotions for incomplete order" );
						}
					}
				}
				((MutableRepository)getOrderRepository()).updateItem( badOrder );
				state = "Success";
			} else if(badOrder != null && DigitalStringUtil.isNotBlank(currentState) && !"INCOMPLETE".equalsIgnoreCase(currentState)) {
				throw new DigitalAppException("insole.customer.badorder.reset.state.not.incomplete");
			}

			if( isLoggingDebug() ) {
				logDebug( "Handle Order End" );
			}
		}
		catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException while searching orders for Customer Profile ",
						e);
			}
			throw new DigitalAppException("insole.customer.badorder.reset.repository.error");
		}
		return state;
	}
	
	
	/**
	 * @param profileId
	 * @return
	 * @throws DigitalAppException
	 */
	public String clearPromotions( String profileId) throws DigitalAppException{
		String state = "FAILED";
		int clearPromo = 0;
		int clearCoupon = 0;
		MutableRepositoryItem userProfile;
		try	{
			if( isLoggingDebug() ) {
				logDebug( "Handle ProfileId Begin" );
				logDebug( "Profile # = " + profileId );
			}
			userProfile = (MutableRepositoryItem)((MutableRepository)getProfileRepository()).getItemForUpdate( profileId, "user" );	

			if( userProfile != null ) {
				// Cleanup Active promotions
				List promotions = (List)userProfile.getPropertyValue( "activePromotions" );
				if( promotions != null && promotions.size() > 0 ) {
					promotions.clear();
					clearPromo++;
					userProfile.setPropertyValue( "activePromotions", promotions );
				}
				// Cleanup Granted Coupons
				Set coupons = (Set)userProfile.getPropertyValue( "grantedCoupons" );
				if( coupons != null && coupons.size() > 0 ) {
					coupons.clear();
					clearCoupon++;
					userProfile.setPropertyValue( "grantedCoupons", coupons );
				}
				if( clearPromo > 0 || clearCoupon > 0 ) {
					((MutableRepository)getProfileRepository()).updateItem( userProfile );
					state = "Success";
				} else {
					throw new DigitalAppException("insole.customer.clearpromotions.clear.nothing");
				}
			} 

			if( isLoggingDebug() ) {
				logDebug( "Handle Profile End" );
			}
		}
		catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException while searching orders for Customer Profile ",
						e);
			}
			throw new DigitalAppException("insole.customer.clearpromotions.repository.error");
		}
		return state;
	}

}