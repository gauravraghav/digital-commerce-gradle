package com.digital.commerce.csc.common.pipeline;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.rest.servlet.RestPipelineServlet;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.digital.commerce.csc.common.util.DigitalInsoleUtil;
import lombok.Getter;
import lombok.Setter;


/**
 * @author HB391569 This is written to log all the access done via InSole for
 *         compliance
 */
@Getter
@Setter
public class DigitalInSoleRestPipeplineServlet extends RestPipelineServlet {

	private static final String APPD_USER_AGENT = "AppDynamics";

	/**
	 * To enable/disable the InSole access logging
	 */
	private boolean enabled;

	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws IOException,
			ServletException {

		/*
		 * Only if this servlet is enabled then log the data needed and ignore
		 * any exceptions during the logging
		 */
		if (enabled && !this.isMonitorUser(pRequest)) {
			try {
				StringBuilder sb = new StringBuilder();
				sb.append(DigitalInsoleUtil.getRequestInfo(pRequest));
				if (isLoggingInfo()) {
					logInfo(sb.toString());
				}
			} catch (Exception ex) {
				// not a big deal, just log and move on
				logError("Error while logging InSole Access data" + ex);
			}
		}
		/* Move on in the pipeline all the times */
		passRequest(pRequest, pResponse);
	}



	/**
	 * @param request
	 * @return
	 */
	private boolean isMonitorUser(final DynamoHttpServletRequest request) {
		boolean monitorUser = false;
		try {
			monitorUser = (APPD_USER_AGENT.equalsIgnoreCase(request
					.getHeader("User-Agent"))) ? true : false;
		} catch (Exception ex) {
			// not a big deal move on
			logError("Error while retrieving the looged dyn admin user info "
					+ ex.getMessage());
		}
		return monitorUser;
	}

}