package com.digital.commerce.csc.services.profile.handler;

import java.io.IOException;

import javax.servlet.ServletException;

import com.digital.commerce.common.csc.ContactCenterUser;
import com.digital.commerce.common.util.ComponentLookupUtil;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.csc.common.util.DigitalInsoleUserTools;
import com.digital.commerce.services.profile.DigitalProfileTools;

import atg.droplet.DropletException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import lombok.Getter;
import lombok.Setter;


/**
 * 
 * @author ad402865
 * 
 */
@Getter
@Setter
public class DigitalProfileFormHandler extends
		com.digital.commerce.services.profile.handler.DigitalProfileFormHandler {

	private DigitalInsoleUserTools insoleUserTools;

	 protected void preLoginUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	            throws ServletException, IOException
 {
		final String METHOD_NAME = "preLoginUser";
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
			String login = (String) this.getValue().get("login");
			String profileId = insoleUserTools.getUserId(login);
			RepositoryItem existingUserProfile = ServletUtil.getCurrentUserProfile();
			String existingUserLogin = null;
			if (null != existingUserProfile) {
				existingUserLogin = (String) existingUserProfile.getPropertyValue("login");
			}
			if (!DigitalStringUtil.isBlank(profileId)&& ((DigitalProfileTools) getProfileTools()) != null && ((DigitalProfileTools) getProfileTools()).isLoggedIn(((DigitalProfileTools) getProfileTools()).getProfileItem(profileId))) {
				String msg = getMessageLocator().getMessageString("insoleCustomerAlreadyLoggedIn", existingUserLogin);
				addFormException(new DropletException(msg,"insoleCustomerAlreadyLoggedIn"));
			}
		} catch (Exception ex) {
			String msg = getMessageLocator().getMessageString("insoleCustomerLoginGenericError");
			addFormException(new DropletException(msg,"insoleCustomerLoginGenericError"));
			logError("Exception occured when trying a impersonate the user ",ex);
			throw new ServletException(ex.getMessage());
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME);
		}

	}
	/**
	 * Extending ProfileFormHandler from Services module for Insole Role check for Impersonate Login by Agent
	 */
	/* (non-Javadoc)
	 * @see com.digital.commerce.services.profile.handler.DigitalProfileFormHandler#handleLogin(atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	public boolean handleLogin(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		final String METHOD_NAME = "handleLogin";
		boolean ret = false;  
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME);
			if(!getInsoleUserTools().verifyInsoleLoginRules(pRequest)){
				String msg = getMessageLocator().getMessageString( "insole.user.login.required" );
				addFormException( new DropletException( msg, "INSOLE_USER_LOGIN_REQUIRED" ) );
				return false;
			}
			String login = (String)this.getValue().get("login");
			if (isLoggingInfo()) {
				try {
					ContactCenterUser contactCenterUser = ComponentLookupUtil
							.lookupComponent(
									ComponentLookupUtil.INSOLE_AGENT_BEAN,
									ContactCenterUser.class);
					StringBuilder message = new StringBuilder();
					message.append("Insole agent ");
					if (null != contactCenterUser) {
						message.append(contactCenterUser.getUserId());
					}
					message.append(" attempted impersonation as customer "
							+ login);
					logInfo(message.toString());
				} catch (Exception e1) {
					logError("Error logging InSole Application Data");
				}
			}
			String profileId = insoleUserTools.getUserId(login);
			if(DigitalStringUtil.isBlank(profileId)) {
				String msg = getMessageLocator().getMessageString( "insoleCustomerLoginDoesNotExist" );
				addFormException( new DropletException( msg, "insoleCustomerLoginDoesNotExist" ) );
				return false;
			}
			ret = super.handleLogin(pRequest, pResponse);
		} catch (ServletException se) {
			String msg = getMessageLocator().getMessageString( "insoleCustomerLoginGenericError" );
			addFormException( new DropletException( msg, "insoleCustomerLoginGenericError" ) );
			logError("ServletException occured when trying a impersonate the user ", se);
			throw se;
		}
		catch (IOException io) {
			String msg = getMessageLocator().getMessageString( "insoleCustomerLoginGenericError" );
			addFormException( new DropletException( msg, "insoleCustomerLoginGenericError" ) );
			logError("IOException occured when trying a impersonate the user ", io);
			throw io;
		} catch (Exception ex) {
			String msg = getMessageLocator().getMessageString( "insoleCustomerLoginGenericError" );
			addFormException( new DropletException( msg, "insoleCustomerLoginGenericError" ) );
			logError("Exception occured when trying a impersonate the user ", ex);
			throw new ServletException(ex.getMessage()); 
		}
		finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME);
		}
		return ret;
	}
}
