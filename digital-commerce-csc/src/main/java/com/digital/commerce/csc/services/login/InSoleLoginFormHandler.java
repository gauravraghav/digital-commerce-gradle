package com.digital.commerce.csc.services.login;

import java.io.IOException;

import javax.servlet.ServletException;

import com.digital.commerce.common.config.MessageLocator;
import com.digital.commerce.common.csc.ContactCenterUser;
import com.digital.commerce.common.exception.DigitalAppException;
import com.digital.commerce.common.transaction.TransactionUtils;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.csc.services.customer.CustomerSearchFormHandler;

import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.dtm.UserTransactionDemarcation;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import lombok.Getter;
import lombok.Setter;

/**
 */
/**
 * @author MS398446
 * 
 */
@Getter
@Setter
public class InSoleLoginFormHandler extends GenericFormHandler {

	private String loginId;
	private String logPassword;
	private DigitalLdapManager loginManager;
	private ContactCenterUser contantCenterUser;
	private MessageLocator messageLocator;
	private static final String ERROR_MSG_PREFIX = "INSOLE_LOGIN_";
	private static final String CLASSNAME = CustomerSearchFormHandler.class.getName();


	public boolean handleLogin(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		boolean loggedIn = false;
		final String METHOD_NAME = "handleLogin";
		if (isLoggingDebug()) {
			logDebug("In AgentLoginFormHandler handleLogin. Attempting to login: "
					+ loginId);
		}
		UserTransactionDemarcation td = null;
		try {
			if (DigitalStringUtil.isBlank(loginId)
					|| DigitalStringUtil.isBlank(logPassword)) {
				addFormException(new DropletException(
						messageLocator
								.getMessageString("INSOLE_LOGIN_PASSWORD_EMPTY"),
						"INSOLE_LOGIN_PASSWORD_EMPTY"));
				return false;
			}
			td = TransactionUtils.startNewTransaction(CLASSNAME,METHOD_NAME);
			String remoteAddr = pRequest.getRemoteAddr();
			String x;
			if ((x = pRequest.getHeader("X-Forwarded-For")) != null) {
				remoteAddr = x;
				int idx = remoteAddr.indexOf(',');
				if (idx > -1) {
					remoteAddr = remoteAddr.substring(0, idx);
				}
			}
			getContantCenterUser().setRemoteAddress(remoteAddr);
			getLoginManager().checkAgentAuthentication(loginId, logPassword,
					getContantCenterUser());
			loggedIn = true;
		} catch (DigitalAppException e) {
			if (isLoggingError()) {
				logError("Error looking up customer", e);
			}
			TransactionUtils.rollBackTransaction(td, CLASSNAME, METHOD_NAME);
			addFormException(this.findLdapErrorMessage(e, loginId));
		} finally {
			TransactionUtils.endTransaction(td, CLASSNAME, METHOD_NAME);			
			if (loggedIn) {
				logInfo("Insole agent " + loginId + " successfully logged-in");
			} else {
				logInfo("Insole agent " + loginId + " failed to login");
			}
		}
		return true;
	}

	private DropletException findLdapErrorMessage(DigitalAppException e,
			String userName) {
		String errorMessage = "Error looking up Agent Profile";
		if (e.getMessage() != null) {
			String[] messageSet = e.getMessage().split(",");
			if (messageSet != null && messageSet.length > 2) {
				String[] dataErrorCode = messageSet[messageSet.length - 2]
						.split(" ");
				if (dataErrorCode != null && dataErrorCode.length > 1) {
					String dataError = dataErrorCode[dataErrorCode.length - 1];

					if (isLoggingDebug()) {
						logDebug("in findLdapErrorMessage. Looking up message for error code "
								+ dataError);
					}
					errorMessage = messageLocator.getMessageString(
							ERROR_MSG_PREFIX + dataError, userName);
					return new DropletException(errorMessage, ERROR_MSG_PREFIX
							+ dataError);
				}
			}
		}
		return new DropletException (errorMessage);
	}

}
