package com.digital.commerce.csc.services.security;

import atg.security.ProfileIdentityManager;

/**
 * @author HB391569 Custom identity provider to be used only for InSole. Always
 *         returns true for checkAuthenticationByPassword so that we can
 *         simulate password less login for impersonating the customer login
 */
public class DigitalInSoleProfileIdentityManager extends ProfileIdentityManager {
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * atg.security.BasicIdentityManager#checkAuthenticationByPassword(java.
	 * lang.String, java.lang.String) Always return true for impersonation
	 */
	@Override
	public boolean checkAuthenticationByPassword(String pLogin, String pPassword)
			throws SecurityException {
		return true;
	}
}
