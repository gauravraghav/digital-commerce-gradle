package com.digital.commerce.csc.services.customer.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CustomerProfile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String login;
	private String profileId;
	private String billingEmail;
	private String phoneNumber;
	private String postalCode;
	private Boolean hasOrder;

}
