package com.digital.commerce.csc.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import atg.servlet.DynamoHttpServletRequest;

import com.digital.commerce.common.csc.ContactCenterUser;

public final class DigitalInsoleUtil {

	private static final String SEPERATOR = ",";
	private static final Pattern STATIC_CONTENT_PATTERN = Pattern
			.compile("([^\\s]+(\\.(?i)(jpg|png|gif|bmp|css|js|gif))$)");

	/**
	 * Retrieve various data points from request
	 * 
	 * @param request
	 * @return
	 */
	public static final String getRequestInfo(
			final DynamoHttpServletRequest request) {
		if (request == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		/* get the IP of the request origin */
		sb.append("InSoleAccessEvent: ");
		String remoteAddr = request.getRemoteAddr();
		String x;
		if ((x = request.getHeader("SOURCE_IP")) != null) {
			remoteAddr = x;
			int idx = remoteAddr.indexOf(',');
			if (idx > -1) {
				remoteAddr = remoteAddr.substring(0, idx);
			}
		}
		/* get user name of the Insole Agent logged in */
		Object agent = request
				.resolveName("/com/digital/commerce/common/csc/ContactCenterUser");
		if (null != agent) {
			ContactCenterUser inSoleAgent = (ContactCenterUser) request
					.resolveName("/com/digital/commerce/common/csc/ContactCenterUser");
			sb.append("ACCESSED_BY").append(": ")
					.append(inSoleAgent.getUserId()).append(SEPERATOR);
			sb.append("ACCESSED_FROM").append(": ").append(remoteAddr)
					.append(SEPERATOR);
		} else {
			sb.append("ACCESSED_BY").append(": ")
					.append(request.getParameter("loginId")).append(SEPERATOR);
		}

		/* get the page URL being accessed */
		final String requestURIWithQS = request.getRequestURIWithQueryString();
		if (null != requestURIWithQS && !isStaticContent(requestURIWithQS)) {
			sb.append("REQUEST_URI").append(": ").append(requestURIWithQS).append(SEPERATOR);
		}
		sb.append("REQUEST_METHOD").append(": ").append(request.getMethod());
		return sb.toString();
	}

	/**
	 * Check whether the resource being accessed is a static content
	 * 
	 * @param fileName
	 * @return
	 */
	public static boolean isStaticContent(final String requestURI) {
		final Matcher matcher = STATIC_CONTENT_PATTERN.matcher(requestURI);
		return matcher.matches();
	}

}
