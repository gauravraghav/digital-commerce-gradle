package com.digital.commerce.csc.services.customer.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class CustomerOrders implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String login;
	private String profileId;
	private String billingEmail;
	private String phoneNumber;
	private String postalCode;
	private String orderNumber;
	private String orderStatus;
	private Boolean promotionsExist;

}
