package com.digital.commerce.csc.services.customer.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CustomerOrdersSearchResultVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	List<CustomerOrders> customerOrders = new ArrayList<>();

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

}
