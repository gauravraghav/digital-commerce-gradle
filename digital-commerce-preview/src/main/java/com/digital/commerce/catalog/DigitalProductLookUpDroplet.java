package com.digital.commerce.catalog;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.servlet.ItemLookupDroplet;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.digital.commerce.common.multisite.MultiSiteUtil;
import com.digital.commerce.services.catalog.DigitalCatalogTools;
import com.digital.commerce.services.utils.DigitalServiceConstants;

@SuppressWarnings({ "unchecked" })
@Getter
@Setter
public class DigitalProductLookUpDroplet extends ItemLookupDroplet

{
    private static final String EMPTY_SPACE = "";
    private static final ParameterName ELEMENT_NAME = ParameterName
            .getParameterName("elementName");
    private static final ParameterName ID_PARAM = ParameterName.getParameterName("id");
    private static final ParameterName PUSHSITE_PARAM = ParameterName.getParameterName("pushSite");
    private static final ParameterName ITEM_DESCRIPTOR_PARAM = ParameterName
            .getParameterName("itemDescriptor");
    private static final String ELEMENT = "element";
    private static final String OUTPUT_OPARAM = "output";

    private DigitalCatalogTools dswCatalogTools;

    private DigitalServiceConstants dswConstants;

    /**
     * @param item
     * @return
     * @throws JSONException
     */
    private JSONObject constructToeShape(RepositoryItem item)
            throws JSONException {
        JSONObject basicInfo = new JSONObject();
        basicInfo.put("displayName", item.getPropertyValue("displayName"));
        return basicInfo;
    }

    /**
     * @param item
     * @return
     * @throws JSONException
     */
    private JSONObject constructHeelHeight(RepositoryItem item)
            throws JSONException {
        JSONObject basicInfo = new JSONObject();
        basicInfo.put("displayName", item.getPropertyValue("displayName"));
        return basicInfo;
    }

    /**
     * @param item
     * @return
     * @throws JSONException
     */
    private JSONObject constructDimension(RepositoryItem item)
            throws JSONException {
        JSONObject dimensionObj = new JSONObject();
        dimensionObj.put("displayName", item.getPropertyValue("displayName"));
        dimensionObj.put("dimensionCode",
                item.getPropertyValue("dimensionCode"));
        dimensionObj.put("dimensionSeqCode",
                item.getPropertyValue("dimensionSeqCode"));
        return dimensionObj;
    }

    /**
     * @param item
     * @return
     * @throws JSONException
     */
    private JSONObject constructMaterial(RepositoryItem item)
            throws JSONException {
        JSONObject materialObj = new JSONObject();
        materialObj.put("displayName", item.getPropertyValue("displayName"));
        return materialObj;
    }

    /**
     * @param item
     * @return
     * @throws JSONException
     */
    private JSONObject constructSize(RepositoryItem item) throws JSONException {
        JSONObject sizeObj = new JSONObject();
        sizeObj.put("displayName", item.getPropertyValue("displayName"));
        sizeObj.put("sizeCode", item.getPropertyValue("sizeCode"));
        return sizeObj;
    }

    /**
     * @param item
     * @return
     * @throws JSONException
     */
    private JSONObject constructColor(RepositoryItem item) throws JSONException {
        JSONObject colorObj = new JSONObject();
        colorObj.put("displayName", item.getPropertyValue("displayName"));
        colorObj.put("colorCode", item.getPropertyValue("colorCode"));
        return colorObj;
    }

    /**
     * @param childSKUItem
     * @return
     * @throws JSONException
     */
    private JSONObject constructChildSKU(RepositoryItem childSKUItem)
            throws JSONException {
        JSONObject childSKUObj = new JSONObject();

        long startTime = System.currentTimeMillis();

        childSKUObj.put("isClearanceItem",
                childSKUItem.getPropertyValue("isClearanceItem"));
        childSKUObj.put("upc", childSKUItem.getPropertyValue("upc"));
        childSKUObj.put("size", constructSize((RepositoryItem) childSKUItem
                .getPropertyValue("size")));
        childSKUObj.put("id", childSKUItem.getPropertyValue("id"));
        childSKUObj.put("dimension",
                constructDimension((RepositoryItem) childSKUItem
                        .getPropertyValue("dimension")));
        List<RepositoryItem> materialItems = (List<RepositoryItem>) childSKUItem
                .getPropertyValue("materials");
        JSONArray materialObjs = new JSONArray();
        for (RepositoryItem materialItem : materialItems) {
            JSONObject materialObj = constructMaterial((RepositoryItem) materialItem);
            materialObjs.add(materialObj);
        }
        childSKUObj.put("materials", materialObjs);
        childSKUObj.put("msrp", childSKUItem.getPropertyValue("msrp"));
        childSKUObj.put("color", constructColor((RepositoryItem) childSKUItem
                .getPropertyValue("color")));
        childSKUObj.put("skuStockLevel",
                childSKUItem.getPropertyValue("skuStockLevel"));
        childSKUObj.put("nonMemberPrice",
                childSKUItem.getPropertyValue("nonMemberPrice"));
        childSKUObj.put("originalPrice",
                childSKUItem.getPropertyValue("originalPrice"));


        logInfo("Time taken for constructChildSKU: " + (System.currentTimeMillis()-startTime) + " ms" );

        return childSKUObj;
    }

    /**
     * @param item
     * @return
     * @throws JSONException
     */
    private JSONObject constructCategory(RepositoryItem item)
            throws JSONException {
        JSONObject categoryObj = new JSONObject();
        categoryObj.put("id", item.getPropertyValue("id"));
        categoryObj.put("displayName", item.getPropertyValue("displayName"));
        return categoryObj;
    }

    /**
     * @param dswBrandItem
     * @return
     * @throws JSONException
     */
    private JSONObject constructBrand(RepositoryItem dswBrandItem)
            throws JSONException {

        JSONObject dswBrandObj = new JSONObject();
        Object brandName = dswBrandItem.getPropertyValue("displayNameDefault");
        dswBrandObj.put("displayNameDefault", brandName);
        if (null != brandName) {
            String navStringURL = dswCatalogTools
                    .getURLforEndecaDimensionValue("brand",
                            brandName.toString());
            if (navStringURL != null)
                dswBrandObj.put("navStringURL", navStringURL);
        }
        return dswBrandObj;
    }

    /*
     * (non-Javadoc)
     *
     * @see atg.repository.servlet.ItemLookupDroplet#service(atg.servlet.
     * DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
     */
    @SuppressWarnings("ConstantConditions")
    @Override
    public void service(DynamoHttpServletRequest pRequest,
                        DynamoHttpServletResponse pResponse) throws ServletException,
            IOException {
        Transaction tr = null;
        long startTime = 0L;
        String id = null;
        String pushSite = null;
        double minPrice = 0.00;
        double maxPrice = 0.00;

        try {
            startTime = System.currentTimeMillis();
            tr = ensureTransaction();
            id = pRequest.getParameter(ID_PARAM);
            pushSite = pRequest.getParameter(PUSHSITE_PARAM);
            RepositoryItem item = null;
            JSONObject prodJson = null;
            JSONObject respJson = null;
            JSONObject prodObj = null;
            try {
                if (pushSite!=null){
                    MultiSiteUtil.setPushSiteContext(pRequest.getParameter("pushSite"));
                }
                item = findItem(id, getItemDescriptor(pRequest),
                        getRepository(pRequest));
                prodJson = new JSONObject();
                prodObj = new JSONObject();
                respJson = new JSONObject();
                if (item != null) {
                    long localStartTime = System.currentTimeMillis();
                    if (null != item.getPropertyValue("sizes")) {
                        Set<RepositoryItem> sizeItems = (Set<RepositoryItem>) item
                                .getPropertyValue("sizes");
                        JSONArray sizeObjs = new JSONArray();
                        for (RepositoryItem sizeItem : sizeItems) {
                            JSONObject sizeObj = constructSize((RepositoryItem) sizeItem);
                            sizeObjs.add(sizeObj);
                        }
                        prodObj.put("sizes", sizeObjs);
                    }

                    logInfo("Time taken After Sizes: " + id + " " + (System.currentTimeMillis()-localStartTime) + " ms" );

                    localStartTime = System.currentTimeMillis();

                    prodObj.put("hasAnimatedImage",
                            item.getPropertyValue("hasAnimatedImage"));
                    prodObj.put("bvRating", item.getPropertyValue("bvRating"));
                    prodObj.put("isClearance",
                            item.getPropertyValue("isClearance"));
                    prodObj.put("isGWPItem",
                            item.getPropertyValue("isGWPItem"));
                    prodObj.put("defaultColorCode",
                            item.getPropertyValue("defaultColorCode"));
                    prodObj.put("alwaysShowSizeAndWidth",
                            item.getPropertyValue("alwaysShowSizeAndWidth"));
                    prodObj.put("productGender",
                            item.getPropertyValue("productGender"));

                    if (null != item.getPropertyValue("widths")) {

                        Set<RepositoryItem> widthItems = (Set<RepositoryItem>) item
                                .getPropertyValue("widths");
                        JSONArray widthObjs = new JSONArray();
                        for (RepositoryItem widthItem : widthItems) {
                            JSONObject widthObj = constructDimension((RepositoryItem) widthItem);
                            widthObjs.add(widthObj);
                        }
                        prodObj.put("widths", widthObjs);
                    }

                    logInfo("Time taken After widths: " + id + " " + (System.currentTimeMillis()-localStartTime) + " ms" );

                    localStartTime = System.currentTimeMillis();

                    if (null != item.getPropertyValue("parentCategories")) {
                        Set<RepositoryItem> parentCategoriesItems = (Set<RepositoryItem>) item
                                .getPropertyValue("parentCategories");
                        JSONArray parentCategoriesObjs = new JSONArray();
                        for (RepositoryItem parentCategoriesItem : parentCategoriesItems) {
                            JSONObject parentCategoriesObj = constructCategory((RepositoryItem) parentCategoriesItem);
                            parentCategoriesObjs.add(parentCategoriesObj);
                        }
                        prodObj.put("parentCategories", parentCategoriesObjs);
                        // This is a BAD HACK due to UI not hanlding null checks correctly
                        prodObj.put("ancestorCategories", parentCategoriesObjs);
                    }

                    logInfo("Time taken After categories calculation: " + id + " " + (System.currentTimeMillis()-localStartTime) + " ms" );
                    localStartTime = System.currentTimeMillis();

                    prodObj.put("spinColorCode",
                            item.getPropertyValue("spinColorCode"));
                    prodObj.put("description",
                            item.getPropertyValue("description"));

                    if (item.getItemDescriptor().hasProperty("toeShape") && null != item
                            .getPropertyValue("toeShape")) {
                        prodObj.put("toeShape",
                                constructToeShape((RepositoryItem) item
                                        .getPropertyValue("toeShape")));
                    }

                    logInfo("Time taken After Toe Shape: " + id + " " + (System.currentTimeMillis()-localStartTime) + " ms" );
                    localStartTime = System.currentTimeMillis();


                    prodObj.put("longDescription",
                            item.getPropertyValue("longDescription"));
                    if (null != item.getPropertyValue("bullets")) {
                        prodObj.put("bullets", getProductBullets(item
                                .getPropertyValue("bullets")));
                    }
                    prodObj.put("showWidth", item.getPropertyValue("showWidth"));
                    boolean priceInCart = (null == item
                            .getPropertyValue("priceInCart")) ? false
                            : (Boolean) item.getPropertyValue("priceInCart");
                    prodObj.put("priceInCart", priceInCart);

                    localStartTime = System.currentTimeMillis();

                    if (null != item.getPropertyValue("defaultSKU")) {
                        JSONObject defaultSKUObj = constructChildSKU((RepositoryItem) item
                                .getPropertyValue("defaultSKU"));
                        prodObj.put("defaultSKU", defaultSKUObj);
                        // Get the MSRP from the default SKU to match the front end
                        prodObj.put("nonMemberMSRP", defaultSKUObj.get("msrp"));
                    }

                    logInfo("Time taken After default SKU: " + id + " " + (System.currentTimeMillis()-localStartTime) + " ms" );
                    localStartTime = System.currentTimeMillis();

                    if (null != item.getPropertyValue("childSKUs")) {
                        List<RepositoryItem> childSKUItems = (List<RepositoryItem>) item
                                .getPropertyValue("childSKUs");
                        JSONArray childSKUObjs = new JSONArray();
                        for (RepositoryItem childSKUItem : childSKUItems) {
                            JSONObject childSKUObj = constructChildSKU(childSKUItem);
                            childSKUObjs.add(childSKUObj);
                        }
                        prodObj.put("childSKUs", childSKUObjs);

                        long priceStartTime = System.currentTimeMillis();
                        if (!priceInCart) {
                            // Set Min/Max Price by looping through all child SKUs with inventory
                            int counter = 0;
                            for (RepositoryItem childSKUItem : childSKUItems) {
                                if (null != childSKUItem.getPropertyValue("skuStockLevel")
                                        && (Long) childSKUItem.getPropertyValue("skuStockLevel") > 0) {
                                    if (null != childSKUItem.getPropertyValue("nonMemberPrice")) {
                                        counter++;
                                        // Set the minPrice if this is the first price found
                                        if (counter == 1) {
                                            minPrice = (Double) childSKUItem.getPropertyValue("nonMemberPrice");
                                        } else if ((Double) childSKUItem.getPropertyValue("nonMemberPrice") < minPrice) {
                                            minPrice = (Double) childSKUItem.getPropertyValue("nonMemberPrice");
                                        }
                                        if ((Double) childSKUItem.getPropertyValue("nonMemberPrice") > maxPrice) {
                                            maxPrice = (Double) childSKUItem.getPropertyValue("nonMemberPrice");
                                        }
                                    }
                                }
                            }
                        }
                        prodObj.put("nonMemberMinPrice", minPrice);
                        prodObj.put("nonMemberMaxPrice", maxPrice);
                        logInfo("Time taken setting min/max price: " + id + " " + (System.currentTimeMillis()-priceStartTime) + " ms" );
                    }

                    logInfo("Time taken buildingSKUS: " + id + " " + (System.currentTimeMillis()-localStartTime) + " ms" );
                    localStartTime = System.currentTimeMillis();

                    prodObj.put("id", item.getPropertyValue("id"));
                    prodObj.put("showSize", item.getPropertyValue("showSize"));
                    prodObj.put("isActive", item.getPropertyValue("isActive"));

                    if (item.getItemDescriptor().hasProperty("heelHeight") && null != item
                            .getPropertyValue("heelHeight")) {
                        RepositoryItem heelHeightItem = (RepositoryItem) item
                                .getPropertyValue("heelHeight");
                        prodObj.put("heelHeight",
                                constructHeelHeight(heelHeightItem));
                    }
                    if(null != item
                            .getPropertyValue("dswBrand")) {
                        prodObj.put("dswBrand",
                                constructBrand((RepositoryItem) item
                                        .getPropertyValue("dswBrand")));
                    }

                    logInfo("Time taken After Brand&heelHeight: " + id + " " + (System.currentTimeMillis()-localStartTime) + " ms" );


                    prodObj.put("displayName",
                            item.getPropertyValue("displayName"));
                    prodObj.put("productTypeWeb",
                            item.getPropertyValue("productTypeWeb"));
                    prodObj.put("isProductInFavorites", false);
                    prodObj.put("newProductText",
                            item.getPropertyValue("newProductText"));
                    prodObj.put("hasAnimatedImage",
                            item.getPropertyValue("hasAnimatedImage"));

                    prodJson.put("product", prodObj);
                    respJson.put("Response", prodJson);

                    logInfo("Time taken After building json: " + id + " " + (System.currentTimeMillis()-startTime) + " ms" );


                    if (null != item.getPropertyValue("displayName")) {
                        String displayName = (String) item
                                .getPropertyValue("displayName");
                        displayName = displayName.trim();
                        displayName = displayName.toLowerCase();
                        displayName = displayName.replaceAll(" ", "_");
                        displayName = displayName.replaceAll("'", "_");
                        pRequest.setParameter("productDisplayName", displayName);
                    }

                    logInfo("Time taken After building url: " + id + " " + (System.currentTimeMillis()-startTime) + " ms" );


                    pRequest.setParameter("productId",
                            item.getPropertyValue("id"));
                    pRequest.setParameter("defaultColorCode",
                            item.getPropertyValue("defaultColorCode"));
                } else {
                    if (isLoggingDebug()) {
                        logDebug("Looked up Product is not found");
                    }
                }
            } catch (RepositoryException exc) {
                if (isLoggingError()) {
                    logError(exc);
                }
                pRequest.setParameter("error", exc);
            } catch (JSONException e) {
                logError(e);
                pRequest.setParameter("error", e);
            }
            String elementName = pRequest.getParameter(ELEMENT_NAME);
            if (elementName == null) {
                elementName = ELEMENT;
            }
            if (isLoggingDebug()) {
                logDebug("Response JSON=" + respJson);
            }
            pRequest.setParameter(elementName, respJson);

            pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
        } finally {
            logInfo("Time taken for service method for productId: " + id + " " + (System.currentTimeMillis()-startTime) + " ms" );
            if (tr != null) {
                commitTransaction(tr);
            }

        }
    }


    /**
     * @param pId
     * @param pItemDescriptorName
     * @param pRepository
     * @return
     * @throws RepositoryException
     */
    protected RepositoryItem findItem(String pId, String pItemDescriptorName,
                                      Repository pRepository) throws RepositoryException {
        RepositoryItem item = null;
        long startTime = 0L;
        try {
            if ((pRepository != null) && (pId != null)) {
                if (isLoggingInfo()) {
                    logInfo("Find item: id=" + pId + "; type="
                            + pItemDescriptorName);
                }
                startTime = System.currentTimeMillis();
                if (pItemDescriptorName != null) {
                    item = pRepository.getItem(pId, pItemDescriptorName);
                } else {
                    item = pRepository.getItem(pId,
                            pRepository.getDefaultViewName());
                }
                if ((item == null) && (isGetDefaultItem())
                        && (pRepository != getRepository())
                        && (getRepository() != null)) {
                    if (isLoggingInfo()) {
                        logInfo("Item not found, look in default repository. id="
                                + pId + "; type=" + pItemDescriptorName);
                    }
                    if (pItemDescriptorName != null) {
                        item = getRepository().getItem(pId, pItemDescriptorName);
                    } else {
                        String defaultViewName = getRepository()
                                .getDefaultViewName();
                        item = getRepository().getItem(pId, defaultViewName);
                    }
                }
            }
        } finally {
            logInfo("Time taken for findItem for productId: " + pId + " " + (System.currentTimeMillis()-startTime) + " ms" );
        }
        return item;
    }

    /**
     * @param pRequest
     * @return
     */
    private String getItemDescriptor(DynamoHttpServletRequest pRequest) {
        String itemDescriptorName = getItemDescriptor();
        if (isUseParams()) {
            String obj = pRequest.getParameter(ITEM_DESCRIPTOR_PARAM);
            itemDescriptorName = obj;
            if (isLoggingInfo()) {
                logInfo("Use item descriptor parameter "
                        + itemDescriptorName);
            }
        }
        return itemDescriptorName;
    }

    /**
     * @param pRequest
     * @return
     */
    protected Repository getRepository(DynamoHttpServletRequest pRequest) {
        Repository repository = null;
        if (isUseParams()) {
            Object obj = getRepository();
            if (obj != null) {
                repository = (Repository) obj;
                if (isLoggingInfo()) {
                    logInfo("Using parameter repository "
                            + repository.getRepositoryName());
                }
            }
        }
        return repository;
    }

    /**
     * @param bulletsString
     * @return
     */
    private JSONArray getProductBullets(Object bulletsString) {
        JSONArray bullets = null;
        long startTime = 0L;
        if (null != bulletsString && bulletsString instanceof String) {
            startTime = System.currentTimeMillis();

            String[] list = bulletsString.toString().split(
                    getDswConstants().getListItemEndTag());
            bullets = new JSONArray();
            for (String lists : list) {
                String bullList = lists.replace(getDswConstants()
                        .getListItemTag(), EMPTY_SPACE);
                // get rid of the <ul> tags if any in the string
                bullList = bullList.replace(getDswConstants().getUlStartTag(),
                        EMPTY_SPACE);
                // get rid of the </ul> tags if any in the string
                bullList = bullList.replace(getDswConstants().getUlEndTag(),
                        EMPTY_SPACE);
                // finally get rid of any html special characters if there are
                // any in the string
                bullList = bullList
                        .replaceAll("\\<(/?[^\\>]+)\\>", EMPTY_SPACE);
                // Trim empty spaces before we add it to the collection.
                bullets.add(bullList.replace(
                        getDswConstants().getListItemEndTag(), EMPTY_SPACE)
                        .trim());
            }

        }
        logInfo("Time taken for getProductBullets: " + (System.currentTimeMillis()-startTime) + " ms" );
        return bullets;
    }
}
