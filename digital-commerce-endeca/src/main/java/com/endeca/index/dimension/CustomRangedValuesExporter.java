package com.endeca.index.dimension;

import java.util.Locale;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.endeca.index.EndecaContext;
import atg.endeca.index.dimension.RangedValuesExporter;
import atg.endeca.index.record.Record;
import atg.endeca.index.record.Records;
import atg.repository.search.indexing.IndexingException;

/**
 */
public class CustomRangedValuesExporter extends RangedValuesExporter {

    /**
     * <b>createExportRecords</b> - Used for creating export records.
     * 
     * @param EndecaContext
     *            pContext
     * @param Set
     *            <Locale> pLocales
     * @param double pMin
     * @param double pMax
     */
    @Override
    protected Records createExportRecords(EndecaContext pContext,
            Set<Locale> pLocales, double pMin, double pMax)
            throws IndexingException {
        if (isLoggingDebug()) {
            logDebug("Inside -> " + getClass().getCanonicalName()
                    + " -> METHOD -> createExportRecords()");
        }
        int max = 0;
        final Records lExportRecords = super.createExportRecords(pContext,
                pLocales, pMin, max);
        for (Record lRecord : lExportRecords.getRecords()) {
            final String lLowerBoundStr = lRecord
                    .getOnlyValueForPropertyName("dimval.range.lower_bound");
            if (StringUtils.isNotBlank(lLowerBoundStr)) {
                final Double lLowerBound = Double.valueOf(lLowerBoundStr);
                if (!lLowerBound.equals(getMaximumFacetsValue())) {
                    lRecord.setPropertyValue(
                            "dimval.range.upper_bound_inclusive", "true");
                }
                lRecord.setPropertyValue("dimval.range.lower_bound_inclusive",
                        "true");
            }
        }

        if (isLoggingDebug()) {
            logDebug("Exit -> " + getClass().getCanonicalName()
                    + " -> METHOD -> createExportRecords()");
        }
        return lExportRecords;
    }
}