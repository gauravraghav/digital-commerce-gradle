/**
 * 
 */
package com.digital.endeca.index.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.sql.DataSource;

import atg.endeca.index.admin.SimpleIndexingAdmin;
import atg.repository.search.indexing.IndexingException;
import lombok.Getter;
import lombok.Setter;

/**
 * @author oracle
 *
 */
@Getter
@Setter
public class DigitalSimpleIndexingAdmin extends SimpleIndexingAdmin {
	
	private DataSource inventoryDataSource;
	private String	endRunSQL;
	
	/**
	 * This method is used to update the base line finish time in partial update
	 * table.
	 */
	@Override
	public boolean indexBaseline() throws IndexingException {
		if (isLoggingInfo()) {
			logInfo("START DigitalSimpleIndexingAdmin:::indexBaseline() method");
		}
		long startTime = System.currentTimeMillis();
		boolean baseLineStatus = super.indexBaseline();
		long endTime = System.currentTimeMillis();
		if (baseLineStatus) {
			try {
				if (isLoggingInfo()) {
					logInfo("Update the base line finish time... ");
				}
				setEndRun();
			} catch (SQLException e) {
				if (isLoggingError()) {
					logError("Error while updating the finish time. " + e);
				}
			}
		}
		if (isLoggingInfo()) {
			logInfo("Total time for partial indexing : " + (endTime - startTime));
		}		
		if (isLoggingInfo()) {
			logInfo("END DigitalSimpleIndexingAdmin:::indexBaseline() method");
		}
		return baseLineStatus;
	}
	
	/**
	 * <Description> Update the table when the partial indexing end.
	 * 
	 * @throws SQLException
	 */
	private void setEndRun() throws SQLException {
		if( isLoggingInfo() ) {
			logInfo( "Inside setEndRun. running: ");
		}
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = this.getInventoryDataSource().getConnection();
			statement = connection.prepareStatement( getEndRunSQL() );
			statement.setTimestamp( 1, new Timestamp( System.currentTimeMillis() ) );
			statement.executeUpdate();
			if( isLoggingInfo() ) {
				logInfo( "Completed setEndRun" );
			}
		} catch( SQLException sqlex ) {
			if( isLoggingError() ) logError( "Error setting run end time." );
			throw sqlex;
		} finally {
			if( statement != null ) {
				try {
					statement.close();
				} catch( Exception ex ) {
					if (isLoggingError()) {
						logError( "Error while doing JDBC  statement close in setEndRun", ex );
					}
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					if (isLoggingError()) {
						logError( "Error while doing connection close in setEndRun", ex );
					}
				}
			}
		}
	}
}
