/**
 * 
 */
package com.digital.commerce.endeca.recordstore;

/**
 * @author oracle
 *
 */
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.endeca.itl.record.PropertyValue;
import com.endeca.itl.record.Record;
import com.endeca.itl.recordstore.RecordStore;
import com.endeca.itl.recordstore.RecordStoreConfiguration;
import com.endeca.itl.recordstore.RecordStoreException;
import com.endeca.itl.recordstore.RecordStoreLocator;
import com.endeca.itl.recordstore.RecordStoreWriter;
import com.endeca.itl.recordstore.TransactionId;
import com.endeca.itl.recordstore.TransactionType;

import atg.endeca.index.EndecaIndexingOutputConfig;
import atg.endeca.index.Indexable;
import atg.endeca.index.admin.IndexingTask;
import atg.repository.Query;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.search.indexing.BulkLoaderResults;
import atg.repository.search.indexing.IndexingException;
import atg.repository.search.indexing.LoaderResults;
import lombok.Getter;
import lombok.Setter;

/**
 * TypeAheadKeywordsExporter reads typeAheadKeyword property from the product catalog and adds the property to a record which is 
 * written to recordstore
 * @author TAISTECH
 */
@Getter
@Setter
public class TypeAheadKeywordsExporter extends EndecaIndexingOutputConfig implements Indexable, IndexingTask.StatusReporter{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private int casPort;
	private String casHost;
	private String endecaTypeAheadId;
	private String recordStoreName;
	private Repository siteRepository;
	private String seperator;
	private int	completedRecords = 0;
	private String typeAheadKeywordPropertyName;
	private String typeAheadDimensionName;
	private String productIdPropertyName;
	private String ancestorIdPropertyName;
	private String productActivePropertyName;
	private String typeAheadViewName;
	
	public TypeAheadKeywordsExporter() throws RemoteException {
		super();
	}
	
	public void write(){
		processTypeAheadKeyword();
	}

	public void processTypeAheadKeyword(){
		TransactionId tId = null;
		RecordStoreWriter writer = null;
		RecordStore recordStore = null;
		Collection<Record> records = new LinkedList<Record>();
		Collection<PropertyValue> tempTypeAheadList = new ArrayList<>();
		try {
			RecordStoreLocator locator = RecordStoreLocator.create(getCasHost(), getCasPort(), getRecordStoreName());
			recordStore = locator.getService();
			RecordStoreConfiguration config = new RecordStoreConfiguration();
			config.setIdPropertyName(getEndecaTypeAheadId());
			config.setChangePropertyNames(new ArrayList<String>());
			recordStore.setConfiguration(config);
			logInfo(".....Starting a new transaction ...");
			tId = recordStore.startTransaction(TransactionType.READ_WRITE);
			writer = RecordStoreWriter.createWriter(recordStore, tId, 100);
			writer.deleteAll();
			completedRecords = 0;
			Map<String, TypeAheadVO> keywordMap = getTypeAheadKeyword();
			if (keywordMap != null && !keywordMap.isEmpty()) {
				for (String typeAheadKey : keywordMap.keySet()) {
					completedRecords++;
					Record record = new Record();
					record.addPropertyValue( new PropertyValue(getEndecaTypeAheadId(), keywordMap.get(typeAheadKey).getRecordId() ));
					record.addPropertyValue( new PropertyValue(getAncestorIdPropertyName(), keywordMap.get(typeAheadKey).getAllAncestorsId() ));
					record.addPropertyValue( new PropertyValue(getProductIdPropertyName(), keywordMap.get(typeAheadKey).getProductId() ));
					record.addPropertyValue( new PropertyValue(getProductActivePropertyName(), keywordMap.get(typeAheadKey).getProductActive() ));
					record.addPropertyValue( new PropertyValue("product.siteId", keywordMap.get(typeAheadKey).getSiteId()));
					for (String keyword : keywordMap.get(typeAheadKey).getTypeAheadKeywords()) {
						logInfo("typeahead keyword :: " + keyword);
						tempTypeAheadList.add(new PropertyValue(getTypeAheadDimensionName(), keyword));
					}
					record.addPropertyValues(tempTypeAheadList);
					records.add(record);
					logInfo("Writing records ..." + record);
				}
				writer.write(records);
				writer.close();
				logInfo("Committing transaction ...");
				recordStore.commitTransaction(tId);
			}
		} catch (RecordStoreException exception) {
			if( isLoggingError() ) {
				logError("Excpetion in closing writer for typeahead", exception);
			}
			if (tId != null) {
				try {
					recordStore.rollbackTransaction(tId);
				} catch (RecordStoreException anotherException) {
					if( isLoggingError() ) {
						logError("Failed to roll back transaction.", anotherException);
					}
				}
			}
		} catch( Exception ex ) {
			if( isLoggingError() ) {
				logError( "General Exception while writing to CAS in .....", ex );
			}
		} if( null != writer ) {
			try {
				writer.close();
			} catch (Exception e) {
				if( isLoggingError() ) {
					logError("Excpetion in closing writer for typeahead", e);
				}
			} 
		}
	}

	private Map<String, TypeAheadVO> getTypeAheadKeyword() throws Exception{
		Map<String, TypeAheadVO> keywordMap = new HashMap<>();
		RepositoryView view = null;
		Query query = null;
		RepositoryItem[] siteItems = null;
		try {
			view = getSiteRepository().getView("siteConfiguration");
			query = view.getQueryBuilder().createUnconstrainedQuery();
			siteItems = view.executeQuery(query);
			if (siteItems != null && siteItems.length > 0) {
				for (RepositoryItem lRepositoryItem : siteItems) {
					TypeAheadVO lTypeAheadVO = new TypeAheadVO();
					RepositoryItem typeAheadItem = (RepositoryItem) lRepositoryItem.getPropertyValue("siteSpecTypeAhead");
					if (typeAheadItem != null) {
						lTypeAheadVO.setSiteId(lRepositoryItem.getRepositoryId());
						lTypeAheadVO.setProductId((String) typeAheadItem.getPropertyValue("productId"));
						lTypeAheadVO.setRecordId((String) typeAheadItem.getPropertyValue("recordId"));
						lTypeAheadVO.setProductActive((String) typeAheadItem.getPropertyValue("productActive"));
						lTypeAheadVO.setAllAncestorsId((String) typeAheadItem.getPropertyValue("allAncestorsId"));
						lTypeAheadVO.setTypeAheadKeywords((List) typeAheadItem.getPropertyValue("typeAheadKeywords"));
						keywordMap.put(lRepositoryItem.getRepositoryId(), lTypeAheadVO);
					}
				}
			} else {
				logInfo("No item found to write");
			}
			
			logInfo("typeAhead repository item size :::: " + keywordMap.size());
		} catch (Exception e) {
			if (isLoggingError()) {
				logError("Error while fetching the type ahead keyword", e);
			}
			throw e;
		}
		return keywordMap;
	}
	@Override
	public BulkLoaderResults performBaselineUpdate(IndexingTask pTask)
			throws IndexingException {
		write();
		return new MinimalBulkLoaderResults( true );
		//return super.performBaselineUpdate(pTask);
	}
	
	@Override
	public void postIndexingCleanup( IndexingTask indexingTask, boolean b, LoaderResults loaderResults ) throws IndexingException {
		return;
	}
	
	public static class MinimalBulkLoaderResults extends BulkLoaderResults {

		/**
		 * 
		 */
		private static final long	serialVersionUID	= 1L;

		@Override
		public boolean isSuccessful() {
			return mSuccessful;
		}

		@Override
		public int getGeneration() {
			throw new UnsupportedOperationException();
		}

		boolean	mSuccessful;

		public MinimalBulkLoaderResults( boolean pSuccessful ) {
			mSuccessful = pSuccessful;
		}
	}
	
	@Override
	public int getSuccessCount() {
		return completedRecords;
	}

	@Override
	public int getFailureCount() {
		return 0;
	}
	
	@Override
	public boolean isNeededForIncremental() {
		return false;
	}

	@Override
	public boolean mayNeedCleanup() {
		return false;
	}
	
	@Override
	public boolean isSupportsStatusCounts() {
		return true;
	}

	@Override
	public Set<String> getIndexingOutputConfigPaths() {
		return new HashSet<>();
	}

	@Override
	public boolean isForceToBaseline() {
		return false;
	}
}
