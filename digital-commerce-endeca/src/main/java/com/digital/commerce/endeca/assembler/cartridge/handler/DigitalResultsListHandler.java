package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import atg.endeca.assembler.navigation.filter.FilterUtils;
import atg.endeca.assembler.navigation.filter.PropertyConstraint;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.cartridge.support.NavigationStateSupport;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.digital.commerce.endeca.assembler.util.DigitalEndecaUtils;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.event.request.RequestEventFactory;
import com.endeca.infront.cartridge.ResultsList;
import com.endeca.infront.cartridge.ResultsListConfig;
import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.FilterState;
import com.endeca.infront.navigation.model.MatchMode;
import com.endeca.infront.navigation.model.SearchFilter;
import com.endeca.infront.navigation.model.SubRecordsPerAggregateRecord;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.infront.navigation.request.RecordsMdexQuery;
import com.endeca.navigation.ENEQueryResults;

import atg.core.util.StringList;
import atg.endeca.assembler.cartridge.handler.ResultsListHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"unchecked"})
@Getter
@Setter
public class DigitalResultsListHandler extends ResultsListHandler

{
	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalResultsListHandler.class);
	private MdexRequest mdexRequest;
	private int eneQueryTimeout;
	private DimensionValueCacheTools	dimensionValueCacheTools;
	private DigitalBaseConstants digitalBaseConstants;
	
	private StringList					faceoutLogicHandleforFilters;
	private StringList					faceoutLogicHandleforKeywords;




	public enum InventoryClearanceCombo {
		BOTH_TRUE,ONLY_INVENTORY,ONLY_CLEARANCE,BOTH_FALSE,UNDERTERMINED
	}

	@Override
	public void preprocess(ResultsListConfig cartridgeConfig) throws CartridgeHandlerException {
		NavigationState navState = getNavigationState();
		if (DigitalEndecaConstants.TRUE.equalsIgnoreCase(navState.getParameter(DigitalEndecaConstants.GETPRODUCTSFROMIDS))) {
			SearchFilter searchFilter = new SearchFilter();
			searchFilter.setKey(DigitalEndecaConstants.PRODUCTID);
			searchFilter.setTerms((navState.getFilterState().getSearchFilters().get(0)).getTerms());
			searchFilter.setMatchMode(MatchMode.ALLANY);
			List<SearchFilter> searchFilterList = new ArrayList<>();
			searchFilterList.add(searchFilter);
			navState.getFilterState().setSearchFilters(searchFilterList);
			RecordsMdexQuery recordsQuery = new RecordsMdexQuery();
			recordsQuery.setOffset(cartridgeConfig.getOffset());
			recordsQuery.setRecordsPerPage(cartridgeConfig.getRecordsPerPage());
			recordsQuery.setSortOption(cartridgeConfig.getSortOption());
			recordsQuery.setWhyMatchEnabled(cartridgeConfig.isWhyMatchEnabled());
			recordsQuery.setWhyRankEnabled(cartridgeConfig.isWhyRankEnabled());
			recordsQuery.setSubRecordsPerAggregateRecord(cartridgeConfig.getSubRecordsPerAggregateRecord());
			mdexRequest = createMdexRequest(navState.getFilterState(), recordsQuery);
		} else {
			//If Version 2 then send only 1 SKU from the Child SKU list
	    	DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
	    	String version = request.getParameter(DigitalEndecaConstants.VERSION);
	    	if(DigitalStringUtil.isNotBlank(version) &&  new Double(version).doubleValue() > 1.0) {
	    		cartridgeConfig.setSubRecordsPerAggregateRecord(SubRecordsPerAggregateRecord.ONE);
	    	}
	    	
	    	//Remove Early access products only if the current Category page is a non-Earlyaccess Category page. Also, skip this filter for Shoephoria
			if (! NavigationStateSupport.containsNavigationFilterfromDimValCache(navState, Arrays.asList(DigitalEndecaConstants.SKUEARLYACCESS), getDimensionValueCacheTools()) && !getDigitalBaseConstants().isPaTool()) {
				java.util.List<java.lang.String> recordFilters = navState.getFilterState().getRecordFilters();
				recordFilters.add( FilterUtils.not(new PropertyConstraint("sku.isEarlyAccess", "1")));
				navState.getFilterState().setRecordFilters(recordFilters);
			}

			super.preprocess(cartridgeConfig);
		}
	}

	@Override
	public ResultsList process(ResultsListConfig cartridgeConfig) throws CartridgeHandlerException {
		ResultsList resultsList = new ResultsList(cartridgeConfig);
		NavigationState navState = getNavigationState();

		ExecutorService executor = Executors.newCachedThreadPool();
		Callable<ENEQueryResults> task = new Callable<ENEQueryResults>() {
			@Override
			public ENEQueryResults call() throws CartridgeHandlerException {
				return executeMdexRequest(mdexRequest);
			}
		};
		Future<ENEQueryResults> future = null;
		try {
			if (DigitalEndecaConstants.TRUE
					.equalsIgnoreCase(navState.getParameter(DigitalEndecaConstants.GETPRODUCTSFROMIDS))) {
				future = executor.submit(task);
				ENEQueryResults results = future.get(eneQueryTimeout, TimeUnit.MILLISECONDS);
				NavigationState navigationState = getNavigationState();
				navigationState.inform(results);
				resultsList.setRecords(createRecords(results, navigationState, cartridgeConfig));
				if (!resultsList.getRecords().isEmpty()) {
					resultsList.setFirstRecNum(cartridgeConfig.getOffset() + 1L);
					resultsList.setLastRecNum(cartridgeConfig.getOffset() + resultsList.getRecords().size());
				}
				resultsList.setTotalNumRecs(getTotalNumRecs(results));
				resultsList.setRecsPerPage(cartridgeConfig.getRecordsPerPage());

				if (RequestEventFactory.isLoggingEnabled()) {
					dispatchNavigationEventInformation(cartridgeConfig, resultsList);
				}
			} else {
				resultsList = super.process(cartridgeConfig);
				
				//Handle faceout logic for V1 vs V2
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		    	String version = request.getParameter(DigitalEndecaConstants.VERSION);    	
		    	//If Version is 1.0 or BLANK then use old logic
				if(null!=resultsList.get(DigitalEndecaConstants.RECORDS) && (DigitalStringUtil.isBlank(version) || (DigitalStringUtil.isNotBlank(version) && new Double(version).longValue() < 2.0)) ){					
					DigitalEndecaUtils.addSelectedColorCode((ArrayList<Record>) resultsList.get(DigitalEndecaConstants.RECORDS), isClearanceRequest());
				}
				//If version = 2.0 
				else if(DigitalStringUtil.isNotBlank(version) && new Double(version).longValue() > 1.0 && this.faceoutLogicHandleforFilters != null && this.faceoutLogicHandleforKeywords != null) {
					DigitalEndecaUtils.addSelectedColorCodeV2((ArrayList<Record>) resultsList.get(DigitalEndecaConstants.RECORDS), getNavigationState(), Arrays.asList(this.faceoutLogicHandleforFilters.stringAt( 0 ).split( "," )), Arrays.asList(this.faceoutLogicHandleforKeywords.stringAt( 0 ).split( "," )));
				}
			}

			// Add selected Featured Sort option
			if (cartridgeConfig.getSortOption() != null && DigitalStringUtil.isNotBlank(cartridgeConfig.getSortOption().getLabel()))  {
				resultsList.put(DigitalEndecaConstants.FEATURED_SORT, cartridgeConfig.getSortOption().getLabel());
			}
		} catch (InterruptedException | ExecutionException | TimeoutException ex) {
			// ExecutionException will be the wrapped ENE exception
			logger.error("Exception while reading the dimension value from MDEX response", ex);
		}
		return resultsList;
	}

	private boolean isClearanceRequest() {
		FilterState filterState = getNavigationState().getFilterState();
		if (null != filterState.getSearchFilters() && !filterState.getSearchFilters().isEmpty()
				&& DigitalStringUtil.containsIgnoreCase(filterState.getSearchFilters().get(0).getTerms(), DigitalEndecaConstants.CLEARANCE)) {
			return true;
		}
		List<String> navFilters = getNavigationState().getFilterState().getNavigationFilters();
		for (String navFilter : navFilters) {
			if (DigitalStringUtil.equals(getNavigationState().getDimLocation(navFilter).getDimValue().getDimensionName(),
					DigitalEndecaConstants.IS_CLEARANCE)) {
				return true;
			}
		}
		return false;
	}
}
