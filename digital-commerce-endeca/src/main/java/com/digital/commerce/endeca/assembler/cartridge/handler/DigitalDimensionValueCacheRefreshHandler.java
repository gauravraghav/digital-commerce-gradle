package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.params.HttpMethodParams;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.digital.commerce.endeca.assembler.util.DigitalEndecaUtils;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.DimensionSearchResults;
import com.endeca.infront.cartridge.DimensionSearchResultsConfig;
import com.endeca.infront.cartridge.DimensionSearchResultsHandler;
import com.endeca.infront.cartridge.model.DimensionSearchGroup;
import com.endeca.infront.cartridge.model.DimensionSearchValue;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.NavigationStateBuilder;
import com.endeca.infront.navigation.model.SearchFilter;
import com.endeca.infront.navigation.url.UrlNavigationStateBuilder;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.UrlFormatter;
import com.endeca.soleng.urlformatter.UrlState;
import com.endeca.soleng.urlformatter.basic.BasicUrlFormatter;
import com.endeca.soleng.urlformatter.seo.SeoUrlFormatter;
import com.endeca.soleng.urlformatter.seo.UrlParamEncoder;

import atg.commerce.endeca.cache.DimensionValueCache;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.net.URLUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.endeca.assembler.configuration.AssemblerApplicationConfiguration;
import atg.servlet.ServletUtil;

@Getter
@Setter
public class DigitalDimensionValueCacheRefreshHandler extends DimensionSearchResultsHandler {

	private String dimensionName;
	private String repositoryIdProperty;
	private DimensionValueCacheTools dimensionValueCacheTools;
	private NavigationStateBuilder navigationStateBuilder;
	private Map<String, String>					applicationKeyToSites;
	private AssemblerApplicationConfiguration	AssemblerApplicationConfiguration;
	private int						siteHttpsServerPort;
	private String					pagePath;
	private boolean	httpsEnabled;
	private String	siteHttpServerName;
	private int	siteHttpServerPort;
	private String	serviceUrl;
	private String	soTimeout;
	private int		bufferSize;
	private String	connectionTimeout;
	private int		retries	= 0;

	public DigitalDimensionValueCacheRefreshHandler() {
		this.dimensionValueCacheTools = null;
	}

	private static final DigitalLogger	logger	= DigitalLogger.getLogger( DigitalTopNavigationListHandler.class );

	@Override
	public DimensionSearchResults process( DimensionSearchResultsConfig pConfig ) throws CartridgeHandlerException {

		final String METHOD_NAME = "process";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );

		try {
			// super.process() does a Dimension Search to get all Dimension values for the Dimensions defined in
			// DimensionValueCacheRefreshConfig.properties
			DimensionSearchResults allDimensions = super.process( pConfig );

			List<DimensionSearchGroup> dimensions = allDimensions.getDimensionSearchGroups();

			// Usually product.category is cached here out of the box. If no
			// dimensions cached then raise a warning.
			if( dimensions.isEmpty() ) {
				logger.warn( "No Dimensions configured to be Cached" );
			}

			// create a brand new Cache. This cache will swap the already existing stale cache
			DimensionValueCache newCache = getDimensionValueCacheTools().createEmptyCache();

			// loop through all dimensions we got from Dimension Search
			for( DimensionSearchGroup dimension : dimensions ) {
				// Get all Dimension Values from the current Dimension
				List<DimensionSearchValue> items = dimension.getDimensionSearchValues();
				// process and add each Dimension value to the new Cache
				for( DimensionSearchValue item : items ) {

					// get Dimension Value ID (This is the Key for the Cache object)
					String dimvalId = parseDimvalId( item.getNavigationState() );
					if( ( dimvalId == null ) || ( dimvalId.isEmpty() ) ) {
						// Key is required.
						AssemblerTools.getApplicationLogging().vlogError( "Cannot find the dimension value id for item {0}, this item will not be cached" );
					}

					// Derive the URL
					StringBuilder sb = new StringBuilder( item.getContentPath() );
					sb.append( item.getNavigationState() );

					// Add Dimension Value label to the Cache Properties. Used in Text Breadcrumbs Cartridge
					List<String> cacheProperties = Arrays.asList( item.getLabel() );

					/* Cache format
					 * Actual: RepositoryID, DimValID, URL, Ancestors
					 * Now: <<SiteID>>-DimValID, Dimension Name-Dimension Value Name, URL, CacheProperties (to store additional info)
					 * This for loop won't add <<SiteID>> to the Key. This is because these dimension Values are common across Sites
					 * Decided to use "Dimension Name-Dimension Value Name" in the place of Dimension Value ID to help PDP and WishList page
					 * to load faster. I am looking up Dimension URL by Dimension Name in those pages */
					newCache.put( dimvalId, dimension.getDimensionName() + "-" + item.getLabel(), sb.toString(), cacheProperties );
				}
			}

			// Swap the current cache with the new Cache only if we got entries in the new cache.
			if( newCache.getNumCacheEntries() > 0 ) {
				getDimensionValueCacheTools().swapCache( newCache );
			} else {
				AssemblerTools.getApplicationLogging().vlogWarning( "A cache refresh occurred which returned 0 entries, cache will not be updated", new Object[0] );
			}

			/* Include Top Nav and Footer Dimension Values to the Cache.
			 * This is achieved by making a call to Home page with both Header and Footer.
			 * Otherwise, we need two calls to getZoneContent (header and footer) */
			String queryString = null;
			queryString = DigitalEndecaConstants.BLANK;
			Map<String, String> paramMap = new HashMap<>();
			// skipHeaderFooterContent is set to FALSE to get both header and footer (default is also False. But, added this for
			// understanding)
			paramMap.put( DigitalEndecaConstants.SKIPHEADERFOOTER, DigitalEndecaConstants.STRING_FALSE );

			// This application can have multiple Sites in it. Let's get all the Sites applicable for this application first
			String pushSite = applicationKeyToSites.get( getAssemblerApplicationConfiguration().getCurrentApplicationKey() );

			// Each application have unique Locale. We will create multiple applications for multiple locales.
			paramMap.put( DigitalEndecaConstants.LOCALE, getAssemblerApplicationConfiguration().getCurrentApplicationLocale().toString() );

			// This is for Multi-site support. Each application can have multiple site for the same locale
			// This functionality is still not tested. Tested only for en_US. May need to dynamically update the Page path
			String[] allSites = pushSite.split( "\\|" );
			for( String thisSite : allSites ) { // for now, we have only one pushSite (DSW)
				paramMap.put( DigitalEndecaConstants.PUSHSITE, thisSite );
				// generate the Service URL for the getPageContent call
				String pagePathUrl = DigitalEndecaUtils.generatePagePathUrl( isHttpsEnabled(), getSiteHttpServerName(), getSiteHttpServerPort(), getServiceUrl(), queryString, getPagePath(), paramMap );

				HttpClient client = new HttpClient();

				if( retries > 0 ) {
					client.getParams().setParameter( HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler( retries, true ) );
				} else {
					client.getParams().setParameter( HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler( retries, false ) );
				}

				HttpConnectionManager connectionManager = client.getHttpConnectionManager();
				if( connectionManager != null ) {
					HttpConnectionManagerParams managerParams = connectionManager.getParams();
					if( managerParams != null ) {
						managerParams.setParameter( HttpConnectionParams.CONNECTION_TIMEOUT, Integer.parseInt( getConnectionTimeout() ) * 1000 );
						managerParams.setParameter( HttpConnectionParams.SO_TIMEOUT, Integer.parseInt( getSoTimeout() ) * 1000 );
					}
				}

				GetMethod method = new GetMethod( pagePathUrl );
				DigitalEndecaUtils.setMethodHeaders( method );
				int statusCode;
				try {
					if( logger.isDebugEnabled() ) {
						logger.debug( "Calling Home page with header and footer to add Top Nav and Footer navigation states  " + pagePathUrl );
					}
					// GET request to the getPageContent
					// Refer to DigitalLinkToCacheHandler to understand how Top Nav and Footer links are added to Cache
					statusCode = client.executeMethod( method );

					if( statusCode != HttpStatus.SC_OK ) {
						logger.error( "SEVERE ERROR: Problem while making GET request (status code != 200) to Home page in DigitalDimensionValueCacheRefreshHandler. DimensionValueCache won't have Top Nav and Footer navigation states" );
						throw new CartridgeHandlerException( "Problem while making GET request to Home page in DigitalDimensionValueCacheRefreshHandler. DimensionValueCache won't have Top Nav and Footer navigation states" );
					}
					if( logger.isDebugEnabled() ) {
						logger.debug( "Success: Call to Home page Successful to add Top Nav and Footer navigation states. Status Code: " + statusCode );
					}
				} catch( HttpException httpException ) {
					logger.error( "Http Exception  ::::::\n   ", httpException );
				} catch( IOException ioException ) {
					logger.error( "IO Exception  ::::::\n   ", ioException );
				} finally {
					// Release connection (This was not added in many other Cartridge handlers)
					method.releaseConnection();
				}
			}
			return allDimensions;
		} catch( Exception e ) {
			logger.error( "DigitalDimensionValueCacheRefreshHandler Exception", e );
			return null;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}

	}

	// out of the box method
	protected String parseDimvalId( String pNavState ) {
		String dimvalId = null;
		String queryString = pNavState;

		if( getNavigationStateBuilder() instanceof UrlNavigationStateBuilder ) {
			UrlNavigationStateBuilder navigationStateBuilder = (UrlNavigationStateBuilder)getNavigationStateBuilder();

			String dimvalParamName = navigationStateBuilder.getNavigationFiltersParam();

			UrlFormatter urlFormatter = navigationStateBuilder.getUrlFormatter();

			if( urlFormatter instanceof SeoUrlFormatter ) {
				SeoUrlFormatter seoUrlFormatter = (SeoUrlFormatter)urlFormatter;

				String[] navStatePieces = pNavState.split( new StringBuilder().append( "/" ).append( dimvalParamName ).append( seoUrlFormatter.getPathKeyValueSeparator() ).toString() );

				if( navStatePieces.length == 2 ) {
					dimvalId = navStatePieces[1];
				}

				if( !( DigitalStringUtil.isEmpty( dimvalId ) ) ) {
					UrlParamEncoder[] encoders = seoUrlFormatter.mUrlParamEncoders();

					if( encoders != null ) {
						for( UrlParamEncoder encoder : encoders ) {
							if( !( encoder.getParamKey().equals( dimvalParamName ) ) )
								continue;
							try {
								UrlState urlState = seoUrlFormatter.parseRequest( ServletUtil.getCurrentRequest() );

								dimvalId = encoder.decodeValue( urlState, dimvalId );
							} catch( UrlFormatException ufe ) {
								AssemblerTools.getApplicationLogging().vlogError( ufe, "There was a problem decoding dimvalId: ", new Object[] { dimvalId } );
							}
						}

					}

				}

			} else if( urlFormatter instanceof BasicUrlFormatter ) {
				if( queryString.startsWith( "?" ) ) {
					queryString = queryString.substring( 1 );
				}

				Map<String, String[]> table = URLUtils.parseQueryStringToMap( queryString );

				if( table.containsKey( dimvalParamName ) ) {
					dimvalId = ( (String[])table.get( dimvalParamName ) )[0];
				}
			}
		}
		return dimvalId;
	}

	@Override
	protected SearchFilter getDimensionSearchFilter( NavigationState navigationState ) {
		SearchFilter filter = new SearchFilter();
		filter.setTerms( "*" );
		return filter;
	}

	protected DimensionSearchGroup getDimension( DimensionSearchResults pDimensionSearchResults ) {
		List<DimensionSearchGroup> dimensions = pDimensionSearchResults.getDimensionSearchGroups();
		for( DimensionSearchGroup dimension : dimensions ) {
			if( dimension.getDimensionName().equals( getDimensionName() ) )
				return dimension;
		}
		return null;
	}

}
