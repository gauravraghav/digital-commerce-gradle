package com.digital.commerce.endeca.index.filters;

import atg.nucleus.GenericService;
import atg.repository.search.indexing.PropertyValuesFilter;
import atg.repository.search.indexing.ValueAndSecurity;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

@Getter
@Setter
public class MaterialsFilter extends GenericService implements
		PropertyValuesFilter {
	private List<String> excludedList;

	@Override
	public boolean applyAfterFormatting() {
		return false;
	}

	@Override
	public ValueAndSecurity[] filter(String s,
			ValueAndSecurity[] valueAndSecurities) {
		if (valueAndSecurities == null || valueAndSecurities.length == 0)
			return valueAndSecurities;
		List<ValueAndSecurity> results = new LinkedList<>();
		for (ValueAndSecurity value : valueAndSecurities) {
			if (value.getValue() == null)
				continue;
			String oldValue = value.getValue().toString().trim();
			if (!(excludedList.contains(oldValue))) {
				results.add(value);
			} else {
				if (isLoggingDebug()) {
					logDebug("MaterialsFilter skipped [" + oldValue + "].");
				}
			}

		}
		return (ValueAndSecurity[]) results
				.toArray(new ValueAndSecurity[results.size()]);
	}
}
