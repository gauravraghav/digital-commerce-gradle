package com.digital.commerce.endeca.index.accessor;

import atg.commerce.pricing.priceLists.PriceListException;
import atg.commerce.pricing.priceLists.PriceListManager;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiscountPropertyAccessor extends PropertyAccessorImpl {
	private static final String	LIST_PRICE		= "listPrice";

	private static final String	ORIGINAL_PRICE	= "originalPrice";

	private Repository			repository;

	private PriceListManager	priceListManager;
	private AccessorHelper		helper;

	protected Object getTextOrMetaPropertyValue( Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType ) {

		if( isLoggingDebug() ) {
			logDebug( "pContext =" + pContext );
			logDebug( "pItem =" + pItem );
			logDebug( "pPropertyName =" + pPropertyName );
			logDebug( "pType =" + pType );
		}

		Object discount = null;
		try {
			RepositoryItem priceList = (RepositoryItem)helper.getActivePriceList( pContext, pItem );

			// priceList may be NULL (rarely). Skip this logic if that happens
			if( priceList != null ) {
				RepositoryItem priceItem = priceListManager.getPrice( priceList, null, pItem );

				// 5-24-2016 : Adding the check if original price is null here because today we don't have original price setup for all the
				// skus
				Object originalPriceObj = priceItem.getPropertyValue( ORIGINAL_PRICE );
				if( ( null != priceItem ) && ( null != originalPriceObj) ) {
					double originalPrice = (double)originalPriceObj;
					double listPrice = (double)priceItem.getPropertyValue( LIST_PRICE );
					double discountAmount = originalPrice - listPrice;
					discount = (double)discountAmount / originalPrice * 100;
				}
			}

		} catch( PriceListException e ) {
			logError( "Exception in calculating discount for the sku", e );
		}

		return discount;
	}
}
