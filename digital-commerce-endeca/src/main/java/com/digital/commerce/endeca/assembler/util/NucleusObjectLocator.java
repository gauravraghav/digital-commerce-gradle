package com.digital.commerce.endeca.assembler.util;

import atg.servlet.ServletUtil;

import com.endeca.infront.assembler.util.ObjectLocator;

public class NucleusObjectLocator implements ObjectLocator {
	public Object locate(String pResource) {
		if (null != ServletUtil.getCurrentRequest()) {
			return ServletUtil.getCurrentRequest().resolveName(pResource);
		}
		return null;
	}
}