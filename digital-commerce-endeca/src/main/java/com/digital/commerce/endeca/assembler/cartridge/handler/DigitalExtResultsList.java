package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.endeca.assembler.navigation.filter.FilterUtils;
import atg.endeca.assembler.navigation.filter.PropertyConstraint;
import com.digital.commerce.common.services.DigitalBaseConstants;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.cartridge.support.NavigationStateSupport;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.digital.commerce.endeca.assembler.util.DigitalEndecaUtils;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.ResultsList;
import com.endeca.infront.cartridge.ResultsListConfig;
import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.content.support.XmlContentItem;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.FilterState;
import com.endeca.infront.navigation.model.SubRecordsPerAggregateRecord;

import atg.core.util.StringList;
import atg.endeca.assembler.cartridge.handler.ResultsListHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalExtResultsList<E> extends ResultsListHandler

{

    private static final String METHOD_NAME = "process";
    private StringList					faceoutLogicHandleforFilters;
    private StringList					faceoutLogicHandleforKeywords;
	private DimensionValueCacheTools	dimensionValueCacheTools;
	private DigitalBaseConstants digitalBaseConstants;


    /**
	 * 
	 */
    @Override
    public void preprocess(ResultsListConfig cartridgeConfig)
	    throws CartridgeHandlerException {
		NavigationState navState = getNavigationState();
    	//If Version 2 then send only 1 SKU from the Child SKU list
    	DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();    	
    	String version = request.getParameter(DigitalEndecaConstants.VERSION);
    	if(DigitalStringUtil.isNotBlank(version) &&  new Double(version).doubleValue() > 1.0) {
    		cartridgeConfig.setSubRecordsPerAggregateRecord(SubRecordsPerAggregateRecord.ONE);
    	}
    	
    	//Remove Early access products only if the current Category page is a non-Earlyaccess Category page. Also, skip this filter for Shoephoria
		if (! NavigationStateSupport.containsNavigationFilterfromDimValCache(navState, Arrays.asList(DigitalEndecaConstants.SKUEARLYACCESS), getDimensionValueCacheTools()) && !getDigitalBaseConstants().isPaTool()) {
			java.util.List<java.lang.String> recordFilters = navState.getFilterState().getRecordFilters();
			recordFilters.add( FilterUtils.not(new PropertyConstraint("sku.isEarlyAccess", "1")));
			navState.getFilterState().setRecordFilters(recordFilters);
		}
    	super.preprocess(cartridgeConfig);
    }

    /**
	 * 
	 */
    @SuppressWarnings("unchecked")
	@Override
    public ResultsList process(ResultsListConfig cartridgeConfig)
	    throws CartridgeHandlerException {
	ResultsList resultsList = null;
	int specialTilesCount = 0;
	try {
	    DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
		    .getClass().getName(), METHOD_NAME);
	    resultsList = super.process(cartridgeConfig);
	    
	    //Handle faceout logic for V1 vs V2
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
    	String version = request.getParameter(DigitalEndecaConstants.VERSION);    	
    	//If Version is 1.0 or BLANK then use old logic
		if(null!=resultsList.get(DigitalEndecaConstants.RECORDS) && (DigitalStringUtil.isBlank(version) || (DigitalStringUtil.isNotBlank(version) && new Double(version).longValue() < 2.0)) ){					
			DigitalEndecaUtils.addSelectedColorCode((ArrayList<Record>) resultsList.get(DigitalEndecaConstants.RECORDS), isClearanceRequest());
		}
		//If version = 2.0 
		else if(DigitalStringUtil.isNotBlank(version) && new Double(version).longValue() > 1.0 && this.faceoutLogicHandleforFilters != null && this.faceoutLogicHandleforKeywords != null) {
			DigitalEndecaUtils.addSelectedColorCodeV2((ArrayList<Record>) resultsList.get(DigitalEndecaConstants.RECORDS), getNavigationState(), Arrays.asList(this.faceoutLogicHandleforFilters.stringAt( 0 ).split( "," )), Arrays.asList(this.faceoutLogicHandleforKeywords.stringAt( 0 ).split( "," )));
		}
		
		
	    int firstRecordNum = (int) resultsList.getFirstRecNum();
	    int lastRecordNum = (int) resultsList.getLastRecNum();
	    List<Record> records = resultsList.getRecords();
	    List<XmlContentItem> genericContentInfo = (List<XmlContentItem>) cartridgeConfig
		    .get(DigitalEndecaConstants.GENERIC_CONTENT_INFO);
	    int position = 0;
	    if (genericContentInfo != null && !genericContentInfo.isEmpty()) {
		for (XmlContentItem genericItem : genericContentInfo) {
		    position = Integer.parseInt((String) genericItem
			    .get(DigitalEndecaConstants.GENERIC_CONTENT_POSITION));
		    if (position > firstRecordNum
			    && position < cartridgeConfig.getRecordsPerPage()
			    && position < lastRecordNum) {
			position = position - firstRecordNum;
			Record record = generateRecordFromGenericContent(genericItem);
			if (records != null && !records.isEmpty()) {
				records.add(position, record);
				if (lastRecordNum-firstRecordNum+1 == cartridgeConfig.getRecordsPerPage()) {
					records.remove(records.size()-1);
				}
				specialTilesCount++;
			}
		    }
		}
	    }
	}

	finally {
	    DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
		    .getClass().getName(), METHOD_NAME);

	}
	resultsList.put(DigitalEndecaConstants.SPECIAL_TILES_COUNT,
		specialTilesCount);

	// Add selected Featured Sort option
	if (cartridgeConfig.getSortOption() != null && DigitalStringUtil.isNotBlank(cartridgeConfig.getSortOption().getLabel()))  {
		resultsList.put(DigitalEndecaConstants.FEATURED_SORT, cartridgeConfig.getSortOption().getLabel());
	}

	return resultsList;
    }

    /**
     * 
     * @return
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Record generateRecordFromGenericContent(
	    XmlContentItem genericContentInfo) {

	Iterator it = genericContentInfo.keySet().iterator();
	Record record = new Record();
	Map myMap = new HashMap();
	while (it.hasNext()) {
	    String key = (String) it.next();
	    myMap.put(key, genericContentInfo.get(key));
	    record.setAttributes(myMap);
	}

	return record;
    }
    
    
    private boolean isClearanceRequest() {
		FilterState filterState = getNavigationState().getFilterState();
		if (null != filterState.getSearchFilters() && !filterState.getSearchFilters().isEmpty()
				&& DigitalStringUtil.containsIgnoreCase(filterState.getSearchFilters().get(0).getTerms(), DigitalEndecaConstants.CLEARANCE)) {
			return true;
		}
		List<String> navFilters = getNavigationState().getFilterState().getNavigationFilters();
		for (String navFilter : navFilters) {
			if (DigitalStringUtil.equals(getNavigationState().getDimLocation(navFilter).getDimValue().getDimensionName(),
					DigitalEndecaConstants.IS_CLEARANCE)) {
				return true;
			}
		}
		return false;
	}
}
