package com.digital.commerce.endeca.index.filters;

import java.util.LinkedList;
import java.util.List;

import com.digital.commerce.common.logger.DigitalLogger;

import atg.repository.search.indexing.PropertyValuesFilter;
import atg.repository.search.indexing.ValueAndSecurity;
import lombok.Getter;
import lombok.Setter;

/**
 * This class implements a filter based on the db-data-config.xml that was used by Solr to extract data for search.
 * It implements the following two filters
 * regexp_replace(dsw_heelheight.DISPLAY_NAME, ' *[(][^)]*[)]', '') != 'Not Applicable'"
 * regexp_replace(DISPLAY_NAME, ' *[(][^)]*[)]', '')
 */
@Getter
@Setter
public class HeelHeightFilter implements PropertyValuesFilter {


	private static final DigitalLogger logger = DigitalLogger.getLogger( HeelHeightFilter.class );
	
    @Override
    public boolean applyAfterFormatting() {
        return false;
    }
    
    private List<String> excludedList;

    @Override
    public ValueAndSecurity[] filter(String s, ValueAndSecurity[] valueAndSecurities) {
        if(valueAndSecurities==null || valueAndSecurities.length==0)return valueAndSecurities;

        List<ValueAndSecurity> results = new LinkedList<>();

        for(ValueAndSecurity value : valueAndSecurities){
            /**
             * Filter out empty values
             */
            if(value.getValue() == null)continue;
            /**
             * Filter value to essentially remove any string in braces "(*)"
             */
            String newValue = value.getValue().toString().replaceAll("[(][^)]*[)]","");
            /**
             * Filter out values that are flagged as Not applicable
             */
            if(excludedList.contains(newValue.toLowerCase())){
            	
            	if( logger.isDebugEnabled() ) {
					logger.debug( "HeelHeightFilter skipped [" + newValue + "]." );
				}
            	continue;
            }            
            results.add(new ValueAndSecurity(newValue,value.getSecurityConstraints(),value.getScheme()));
        }
        return (ValueAndSecurity[])results.toArray(new ValueAndSecurity[results.size()]);
    }
}
