package com.digital.commerce.endeca.inventory;

import atg.commerce.catalog.CatalogTools;
import atg.endeca.index.Indexable;
import atg.endeca.index.admin.IndexingTask;
import atg.repository.search.indexing.BulkLoaderResults;
import atg.repository.search.indexing.IncrementalLoaderResults;
import atg.repository.search.indexing.IndexingException;
import atg.repository.search.indexing.LoaderResults;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.endeca.itl.record.PropertyValue;
import com.endeca.itl.record.Record;
import com.endeca.itl.recordstore.RecordStoreWriter;
import com.endeca.itl.recordstore.RecordStoreLocator;
import com.endeca.itl.recordstore.RecordStore;
import com.endeca.itl.recordstore.TransactionId;
import com.endeca.itl.recordstore.TransactionType;
import com.endeca.itl.recordstore.ConcurrentWriteException;
import com.endeca.itl.recordstore.RecordStoreException;
import lombok.Getter;
import lombok.Setter;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/** Created by mbatra on 7/23/2016. */
@Getter
@Setter
public class InventoryExporter extends atg.service.scheduler.SingletonSchedulableService implements Indexable, IndexingTask.StatusReporter {

	private DataSource		inventoryDataSource;

	private String			lastRunSQL;
	private String			startRunSQL;
	private String			endRunSQL;
	private String			changedInventorySQL;
	private String			fullInventorySQL;
	private String			cleanStateTableSQL;
	private int				sendBatchSize;
	private String			lastRunColumnName;
	private String			productFullInventorySql;
	private String			productRecordStoreName;

	private String			productIdColumnName;
	private String			productInventoryColumnName;
	private String 			productClearanceInventoryColumnName;

	private String			endecaRecordProductInventoryLevelName;
	private String			endecaRecordProductClearanceInventoryLevelName;

	private String			skuIdColumnName;
	private String			inventoryColumnName;
	private String			locationIdColumnName;

	private String			endecaRecordSKUIDName;
	private String			endecaRecordInventoryLevelName;

	private String			storeInventoryColumnName;
	private String			fullInventoryColumnName;
	private String			endecaRecordProductRepositoryId;
	private String			endecaRecordInventoryRepositoryId;
	private String			endecaRecordInventoryLocationId;

	private boolean			running					= false;
	private boolean			cancelled				= false;
	private boolean			runningBaseLine			= false;
	private int				completedRecords		= 0;
	private int				completedProductRecords	= 0;

	private boolean			enabled;

	protected CatalogTools catalogTools;

	private String	casHost;
	private int		casPort;
	private String	recordStoreName;

	private void cleanStateTable() throws SQLException {
		if( isLoggingInfo() ) logInfo( "Cleaning state table, we do this for baselines, so all inventory records are processed." );
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = this.getInventoryDataSource().getConnection();
			statement = connection.prepareStatement( getCleanStateTableSQL() );
			statement.execute();

		} catch( SQLException sqlex ) {
			if( isLoggingError() ) logError( "Error cleaning state table" );
			throw sqlex;
		} finally {
			if( statement != null ) {
				try {
					statement.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing JDBC  statement close in cleanStateTable", ex );
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing connection close in cleanStateTable", ex );
				}
			}
		}
	}

	private void setStartRun() throws SQLException {
		if( isLoggingInfo() ) {
			logInfo( "Inside setStartRun. running: " + running + " runningBaseLine: " + runningBaseLine + " cancelled: " + cancelled );
		}
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = this.getInventoryDataSource().getConnection();
			statement = connection.prepareStatement( getStartRunSQL() );
			statement.setTimestamp( 1, new Timestamp( System.currentTimeMillis() ) );
			statement.executeUpdate();
			if( isLoggingInfo() ) {
				logInfo( "Completed setStartRun" );
			}
		} catch( SQLException sqlex ) {
			if( isLoggingError() ) logError( "Error setting run start time." );
			throw sqlex;
		} finally {
			if( statement != null ) {
				try {
					statement.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing JDBC  statement close in setStartRun", ex );
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing connection close in setStartRun", ex );
				}
			}
		}
	}

	private void setEndRun() throws SQLException {
		if( isLoggingInfo() ) {
			logInfo( "Inside setEndRun. running: " + running + " runningBaseLine: " + runningBaseLine + " cancelled: " + cancelled );
		}
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = this.getInventoryDataSource().getConnection();
			statement = connection.prepareStatement( getEndRunSQL() );
			statement.setTimestamp( 1, new Timestamp( System.currentTimeMillis() ) );
			statement.executeUpdate();
			if( isLoggingInfo() ) {
				logInfo( "Completed setEndRun" );
			}
		} catch( SQLException sqlex ) {
			if( isLoggingError() ) logError( "Error setting run end time." );
			throw sqlex;
		} finally {
			if( statement != null ) {
				try {
					statement.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing JDBC  statement close in setEndRun", ex );
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing connection close in setEndRun", ex );
				}
			}
		}
	}

	private Timestamp getLastRun() throws SQLException {
		if( isLoggingInfo() ) {
			logInfo( "Inside getLastRun. running: " + running + " runningBaseLine: " + runningBaseLine + " cancelled: " + cancelled );
		}
		Connection connection = null;
		PreparedStatement lastRunStatement = null;
		ResultSet results = null;
		Timestamp lastRun = null;
		try {
			connection = this.getInventoryDataSource().getConnection();
			lastRunStatement = connection.prepareStatement( getLastRunSQL() );
			results = lastRunStatement.executeQuery();

			if( results.next() ) {
				lastRun = results.getTimestamp( this.getLastRunColumnName() );
			}
			if( isLoggingInfo() ) {
				if( lastRun == null ) {
					logInfo( "No last run time found." );
				} else {
					logInfo( "Last successful run was at - " + lastRun.toString() );
				}
			}

		} catch( SQLException sqlex ) {
			if( isLoggingError() ) logError( "Error fetching last run time." );
			throw sqlex;
		} finally {
			if( results != null ) {
				try {
					results.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing resultlist close in getLastRun", ex );
				}
			}
			if( lastRunStatement != null ) {
				try {
					lastRunStatement.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing JDBC  statement close in getLastRun", ex );
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing connection close in getLastRun", ex );
				}
			}
		}
		return lastRun;

	}

	private void processInventory( Timestamp lastRun ) throws Exception {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;
		if( isLoggingInfo() ) {
			logInfo( "Starting sku inventory extraction.running: " + running + " runningBaseLine: " + runningBaseLine + " cancelled: " + cancelled );
		}
		try {
			connection = this.getInventoryDataSource().getConnection();
			if( lastRun != null ) {
				statement = connection.prepareStatement( getChangedInventorySQL() );
				statement.setTimestamp( 1, lastRun, Calendar.getInstance() );
				statement.setTimestamp( 2, lastRun, Calendar.getInstance() );
				if( isLoggingDebug() ) logDebug( statement.toString() );
				if( isLoggingInfo() ) {
					logInfo("Extracting sku inventory that has changed from last run." );
					logInfo( "STARTING the execution of Inventory SQL query ChangedInventorySQLL at : " + new Timestamp( System.currentTimeMillis() ) );
				}
			} else {
				statement = connection.prepareStatement( getFullInventorySQL() );
				if( isLoggingInfo() ) {
					logInfo( "No previous run found, extracting  all sku inventory." );
					logInfo( "STARTING the execution of Inventory SQL query fullInventorySQL at : " + new Timestamp( System.currentTimeMillis() ) );
				}
			}
			
			results = statement.executeQuery();
			if( isLoggingInfo() ) {
				logInfo( "FINISHED the execution of Inventory SQL query at : " + new Timestamp( System.currentTimeMillis() ) );
				logInfo( "Extraction complete, submitting sku inventory to CAS." );
			}
			ClassLoader clRestore = Thread.currentThread().getContextClassLoader();
			RecordStoreWriter writer = null;
			try {
				if( isLoggingInfo() ) {
					logInfo( "Inside processInventory. Classloader: " + this.getClass().getClassLoader() );
				}
				Thread.currentThread().setContextClassLoader( this.getClass().getClassLoader() );
				RecordStoreLocator locator = RecordStoreLocator.create( getCasHost(), getCasPort(), getRecordStoreName() );
				RecordStore recordStore = locator.getService();
				TransactionId transactionId = recordStore.startTransaction( TransactionType.READ_WRITE );
				writer = RecordStoreWriter.createWriter( recordStore, transactionId, getSendBatchSize() );
				if( runningBaseLine ) {
					writer.deleteAll();
				}
				completedRecords = 0;
				String tempSkuId = "";
				Collection<PropertyValue> tempLocationList = new ArrayList<>();
				String fullSiteStockLevel = null;
				String tempFullSiteStock = null;
				String locationId = "";
				String skuId = "";
				if( isLoggingInfo() ) {
					logInfo( "STARTING writing the SKU inventory records to Endeca CAS at : " + new Timestamp( System.currentTimeMillis() ) );
				}
				while( results.next() ) {
					if( cancelled ) {
						if( isLoggingDebug() ) logDebug( "Current inventory extraction has been canceled. Terminating" );
						break;
					}
					skuId = results.getString( getSkuIdColumnName() );
					fullSiteStockLevel = ( ( results.getString( getFullInventoryColumnName() ) ) == null ) ? "0" : results.getString( getFullInventoryColumnName() );
					locationId = results.getString( getLocationIdColumnName() );

					int storeStockLevel = ( ( results.getString( getStoreInventoryColumnName() ) ) == null ) ? 0 : Integer.parseInt( results.getString( getStoreInventoryColumnName() ) );

					if( DigitalStringUtil.isEmpty( tempSkuId ) ) {
						tempSkuId = skuId;
						tempFullSiteStock = fullSiteStockLevel;
					}

					else if( !DigitalStringUtil.equals( tempSkuId, skuId ) ) {
						if( isLoggingDebug() ) {
							logDebug( "Starting writing the record to CAS for sku Id : " + getSkuIdColumnName() + new Timestamp( System.currentTimeMillis() ) );
						}
						Record record = new Record();
						if( isLoggingDebug() ) {
							logDebug( "Creating record with skuId {" + skuId + "} , fullSiteStockLevel {" + fullSiteStockLevel + "} and storeLocationId {" + tempLocationList + "}" );
						}
						record.addPropertyValue( new PropertyValue( getEndecaRecordInventoryRepositoryId(), tempSkuId ) );
						record.addPropertyValue( new PropertyValue( getEndecaRecordInventoryLevelName(), tempFullSiteStock ) );
						if( tempLocationList.size() > 0 ) {
							record.addPropertyValues( tempLocationList );
							tempLocationList = new ArrayList<>();
						}
						writer.write( record );
						if( isLoggingDebug() ) {
							logDebug( "Finished writing the record to CAS for sku Id : " + getSkuIdColumnName() + new Timestamp( System.currentTimeMillis() ) );
						}
						completedRecords++;
						tempSkuId = skuId;
						tempFullSiteStock = fullSiteStockLevel;
					}
					if( isLoggingDebug() ) {
						logDebug( "Plan to create the record with skuId {" + skuId + "} , fullSiteStockLevel {" + fullSiteStockLevel + "}" );
					}

					if( !DigitalStringUtil.equals( locationId, "no_store_data" ) && ( storeStockLevel > 0 ) ) {
						tempLocationList.add( new PropertyValue( getEndecaRecordInventoryLocationId(), locationId ) );
					}
				}
				if( !DigitalStringUtil.isEmpty( tempSkuId ) ) {
					Record record = new Record();
					if( isLoggingDebug() ) {
						logDebug( "Creating record with skuId {" + skuId + "} , fullSiteStockLevel {" + fullSiteStockLevel + "} and storeLocationId {" + tempLocationList + "}" );
					}
					record.addPropertyValue( new PropertyValue( getEndecaRecordInventoryRepositoryId(), tempSkuId ) );
					record.addPropertyValue( new PropertyValue( getEndecaRecordInventoryLevelName(), tempFullSiteStock ) );
					if( tempLocationList.size() > 0 ) {
						record.addPropertyValues( tempLocationList );
						tempLocationList = new ArrayList<>();
					}
					writer.write( record );
					completedRecords++;
				}
				// Flush all records to CAS before committing the transaction.
				// We are using batch size as 5000.
				writer.flush();

				recordStore.commitTransaction( transactionId );
				if( isLoggingInfo() ) {
					logInfo( "FINISHED writing the SKU inventory records to Endeca CAS at : " + new Timestamp( System.currentTimeMillis() ) );
				}
			} finally {
				if( null != writer ) {
					writer.close();
				}
				Thread.currentThread().setContextClassLoader( clRestore );
			}
		} catch( SQLException sqlex ) {
			if( isLoggingError() ) {
				logError( "SQLException fetching inventory from processInventory" );
				logError( "::::::::: Last SKU in the resultSet    :  " + results.getString( getSkuIdColumnName() ) );
			}
			throw sqlex;
		} catch( ConcurrentWriteException cwex ) {
			if( isLoggingError() ) {
				logError( "ConcurrentWriteException while writing to CAS from processInventory" );
				logError( "::::::::: Last SKU in the resultSet    :  " + results.getString( getSkuIdColumnName() ) );
			}
			throw cwex;
		} catch( RecordStoreException rsex ) {
			if( isLoggingError() ) {
				logError( "RecordStoreException while wiring to CAS from processInventory" );
				logError( "::::::::: Last SKU in the resultSet    :  " + results.getString( getSkuIdColumnName() ) );
			}
			throw rsex;
		} catch( Exception ex ) {
			if( isLoggingError() ) {
				logError( "General Exception while writing to CAS from processInventory", ex );
				logError( "::::::::: Last SKU in the resultSet    :  " + results.getString( getSkuIdColumnName() ) );
			}
			throw ex;
		} finally {
			if( results != null ) {
				try {
					results.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing resultset from processInventory", ex );
				}
			}
			if( statement != null ) {
				try {
					statement.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing statement from processInventory", ex );
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing connection from processInventory", ex );
				}
			}
		}
	}

	public void processProductInventory() throws Exception {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;
		if( isLoggingInfo() ) {
			logInfo( "Starting product inventory extraction." + running + " runningBaseLine: " + runningBaseLine + " cancelled: " + cancelled );
		}
		try {
			connection = this.getInventoryDataSource().getConnection();
			statement = connection.prepareStatement( getProductFullInventorySql() );
			if( isLoggingInfo() ) {
				logInfo( "STARTING the execution of Inventory SQL query productFullInventorySQL at : " + new Timestamp( System.currentTimeMillis() ) );
			}
			results = statement.executeQuery();
			if( isLoggingInfo() ) {
				logInfo( "FINISHED the execution of Inventory SQL query productFullInventorySQL at : " + new Timestamp( System.currentTimeMillis() ) );
				logInfo( "Extraction complete, submitting product inventory to CAS." );
			}
			ClassLoader clRestore = Thread.currentThread().getContextClassLoader();
			RecordStoreWriter writer = null;
			try {
				if( isLoggingInfo() ) {
					logInfo( "Inside processProductInventory. Classloader: " + this.getClass().getClassLoader() );
				}
				Thread.currentThread().setContextClassLoader( this.getClass().getClassLoader() );
				RecordStoreLocator locator = RecordStoreLocator.create( getCasHost(), getCasPort(), getProductRecordStoreName() );
				RecordStore recordStore = locator.getService();
				TransactionId transactionId = recordStore.startTransaction( TransactionType.READ_WRITE );
				writer = RecordStoreWriter.createWriter( recordStore, transactionId, getSendBatchSize() );
				writer.deleteAll(); // This will only be called via baseline, so
									// just delete the existing generations.
				if( isLoggingInfo() ) {
					logInfo( "STARTING writing the Product inventory records to Endeca CAS at : " + new Timestamp( System.currentTimeMillis() ) );
				}
				while( results.next() ) {
					if( cancelled ) {
						if( isLoggingDebug() ) logDebug( "Current product inventory extraction has been canceled. Terminating" );
						break;
					}
					if( isLoggingDebug() ) {
						logDebug( "::::::::: Writing record for product id    :  " + results.getString( getProductIdColumnName() ) );
					}
					Record record = new Record();
					String productId = results.getString( getProductIdColumnName() );
					String stockLevel = results.getString( getProductInventoryColumnName() );
					String clearanceStockLevel = results.getString( getProductClearanceInventoryColumnName());
					if( isLoggingTrace() ) logTrace( "Creating record with productId {" + productId + "} and stockLevel {" + stockLevel + "}" );
					record.addPropertyValue( new PropertyValue( getEndecaRecordProductRepositoryId(), productId ) );
					record.addPropertyValue( new PropertyValue( getEndecaRecordProductInventoryLevelName(), stockLevel ) );
					record.addPropertyValue(new PropertyValue( getEndecaRecordProductClearanceInventoryLevelName(), clearanceStockLevel));
					if( isLoggingTrace() ) logTrace( record.toString() );
					writer.write( record );
					completedProductRecords++;
				}

				writer.close();
				recordStore.commitTransaction( transactionId );
				if( isLoggingInfo() ) {
					logInfo( "Finished submitting product inventory to CAS" );
				}
				if( isLoggingInfo() ) {
					logInfo( "FINISHED writing the Product inventory records to Endeca CAS at : " + new Timestamp( System.currentTimeMillis() ) );
				}

			} finally {
				if( null != writer ) {
					writer.close();
				}
				Thread.currentThread().setContextClassLoader( clRestore );
				if( isLoggingInfo() ) {
					logInfo( "Product inventoryRecords processed : " + completedProductRecords );
				}
			}

		} catch( SQLException sqlex ) {
			if( isLoggingError() ) {
				logError( "SQLException fetching product inventory in processProductInventory" );
				logError( "::::::::: Last product in the resultSet    :  " + results.getString( getProductIdColumnName() ) );
			}
			throw sqlex;
		} catch( ConcurrentWriteException cwex ) {
			if( isLoggingError() ) {
				logError( "ConcurrentWriteException writing to CAS for the Product Inventory Record Store in processProductInventory" );
				logError( "::::::::: Last product in the resultSet    :  " + results.getString( getProductIdColumnName() ) );
			}
			throw cwex;
		} catch( RecordStoreException rsex ) {
			if( isLoggingError() ) {
				logError( "RecordStoreException writing to the product inventory record store in processProductInventory" );
				logError( "::::::::: Last product in the resultSet    :  " + results.getString( getProductIdColumnName() ) );
			}
			throw rsex;
		} catch( Exception ex ) {
			if( isLoggingError() ) {
				logError( "General Exception while writing to CAS in processProductInventory", ex );
				logError( "::::::::: Last product in the resultSet    :  " + results.getString( getProductIdColumnName() ) );
			}
			throw ex;
		} finally {
			if( results != null ) {
				try {
					results.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing resultset from processProductInventory", ex );
				}
			}
			if( statement != null ) {
				try {
					statement.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing statement from processProductInventory", ex );
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing connection from processProductInventory", ex );
				}
			}
			cancelled = false;
		}
	}

	@Override
	public void doScheduledTask( Scheduler scheduler, ScheduledJob scheduledJob ) {
		if( isEnabled() ) {
			if( isLoggingInfo() ) logInfo( "Scheduler Enabled, running inventory extraction." );
			run();
		} else {
			if( isLoggingInfo() ) logInfo( "Scheduler Disabled, not running inventory extraction" );
		}
	}

	public void run() {
		if( isLoggingInfo() ) {
			logInfo( "Inside run. running: " + running + " runningBaseLine: " + runningBaseLine + " cancelled: " + cancelled );
		}
		if( running ) {
			if( isLoggingDebug() ) logInfo( "Inventory update already running." );
			return;
		}
		try {
			running = true;
			Timestamp lastRun = getLastRun();
			if( isLoggingInfo() ) {
				logInfo( "Inside run. lastRun: " + lastRun );
			}
			setStartRun();
			processInventory( lastRun );
			setEndRun();

			if( runningBaseLine ) {
				if( isLoggingInfo() ) {
					logInfo( "Inside run. runningBaseLine: " + runningBaseLine + " Hence trying to push product inventory as well to Endeca" );
				}
				processProductInventory();
			}

		} catch( Exception ex ) {
			logInfo( "Exception from run method", ex );
		}
		cancelled = false;
		running = false;
	}

	@Override
	public BulkLoaderResults performBaselineUpdate( IndexingTask indexingTask ) throws IndexingException {
		if( isLoggingInfo() ) {
			logInfo( "Inside run. running: " + running + " runningBaseLine: " + runningBaseLine + " cancelled: " + cancelled );
		}
		if( running ) {
			if( runningBaseLine ) {
				if( isLoggingInfo() ) logInfo( "Already running a baseline extraction... this shouldnt happen, so just returning success" );
				return new MinimalBulkLoaderResults( true );
			} else {
				if( isLoggingInfo() ) logInfo( "We are probably running a scheduled incremental extraction, so cancelling that and starting a base line extraction" );
				cancel( indexingTask, false );
			}
		}
		try {
			cleanStateTable();
		} catch( SQLException sqlex ) {
			if( isLoggingError() ) logError( "Error cleaning state table", sqlex );
			throw new IndexingException( "Error cleaning state table", sqlex );
		}
		runningBaseLine = true;
		indexingTask.addActiveStatusReporter( this );
		run();
		runningBaseLine = false;
		return new MinimalBulkLoaderResults( true );

	}

	@Override
	public LoaderResults performPartialUpdate( IndexingTask indexingTask ) throws IndexingException {
		if( isLoggingInfo() ) logInfo( "partialUpdate called... this isnt supported, so just returing success." );
		return new MinimalIncrementalLoaderResults( true );
	}

	@Override
	public boolean isNeededForIncremental() {
		return false;
	}

	@Override
	public boolean mayNeedCleanup() {
		return false;
	}

	@Override
	public boolean cancel( IndexingTask indexingTask, boolean b ) throws IndexingException {
		if( running ) {
			if( isLoggingInfo() ) logInfo( "Attempting to cancel current inventory extraction." );
			this.cancelled = true;
			while( this.cancelled ) {
				if( isLoggingInfo() ) logInfo( "Waiting for inventory extraction process to cancel..." );
				try {
					Thread.sleep( 1000 );
				} catch( InterruptedException iex ) {
					logInfo( "InterruptedException while cancelling the InventoryExport", iex );
				}
			}
			if( isLoggingInfo() ) logInfo( "Inventory extraction has been cancelled." );

		} else {
			if( isLoggingInfo() ) logInfo( "There is no running inventory extraction, noting to cancel" );
		}
		return true;
	}

	@Override
	public boolean isSupportsStatusCounts() {
		return true;
	}

	@Override
	public Set<String> getIndexingOutputConfigPaths() {
		return new HashSet<>();
	}

	@Override
	public boolean isForceToBaseline() {
		return false;
	}

	@Override
	public void postIndexingCleanup( IndexingTask indexingTask, boolean b, LoaderResults loaderResults ) throws IndexingException {
		return;
	}

	@Override
	public int getSuccessCount() {
		return completedRecords;
	}

	@Override
	public int getFailureCount() {
		return 0;
	}

	public static class MinimalIncrementalLoaderResults extends IncrementalLoaderResults {

		/**
		 * 
		 */
		private static final long	serialVersionUID	= 1L;

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public Set getGenerations() {
			throw new UnsupportedOperationException();
		}

		public MinimalIncrementalLoaderResults( boolean pSuccessful ) {
			super( pSuccessful, null );
		}
	}

	public static class MinimalBulkLoaderResults extends BulkLoaderResults {

		/**
		 * 
		 */
		private static final long	serialVersionUID	= 1L;

		public int getGeneration() {
			throw new UnsupportedOperationException();
		}

		boolean successful;

		public boolean isSuccessful() {
			return successful;
		}

		public MinimalBulkLoaderResults( boolean pSuccessful ) {
			successful = pSuccessful;
		}
	}
}
