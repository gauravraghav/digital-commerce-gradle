package com.digital.commerce.endeca.index.filters;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.PropertyValuesFilter;
import atg.repository.search.indexing.ValueAndSecurity;
import atg.service.perfmonitor.PerformanceMonitor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SizeFilter extends GenericService implements PropertyValuesFilter {

	private String sizeFilterDelimiter;
	private Repository catalogRepository;
	private String sizeItemType;
	private static final String PERFORM_MONITOR_NAME = "SizeFilter";
	private static final String PERF_NAME = "filter";

	@Override
	public boolean applyAfterFormatting() {
		return false;
	}

	@Override
	public ValueAndSecurity[] filter(String s, ValueAndSecurity[] valueAndSecurities) {
		PerformanceMonitor.startOperation(PERFORM_MONITOR_NAME, PERF_NAME);
		if (valueAndSecurities == null || valueAndSecurities.length == 0) {
			return valueAndSecurities;
		}
		if (isLoggingDebug()) {
			logDebug("ValueAndSecurities has {" + valueAndSecurities.length + "} entries.");
		}
		List<ValueAndSecurity> results = new LinkedList<>();

		for (ValueAndSecurity value : valueAndSecurities) {
			/**
			 * Filter out empty values
			 */
			if (value.getValue() == null){
				continue;
			}

			String sizeRepositoryId = value.getValue().toString();

			if (isLoggingDebug()) {
				logDebug("Processing size {" + sizeRepositoryId + "}");
			}
			Set<String> sizeFilterNames = getSizeFilterNames(sizeRepositoryId);
			if (!sizeFilterNames.isEmpty()) {
				for (String sizeFilterName : sizeFilterNames) {
					results.add(new ValueAndSecurity(sizeFilterName, value.getSecurityConstraints(), value.getScheme()));
					if (isLoggingDebug())
						logDebug("Adding {" + sizeFilterName + "}");
				}
			}
		}
		PerformanceMonitor.endOperation(PERFORM_MONITOR_NAME, PERF_NAME);
		return (ValueAndSecurity[]) results.toArray(new ValueAndSecurity[results.size()]);
	}

	private Set<String> getSizeFilterNames(String sizeRepositoryId) {
		RepositoryItem sizeItem = getSize(sizeRepositoryId);
		final Set<String> sizeFilters = new HashSet<>();
		if (sizeItem == null)
			return sizeFilters;
		String delimiterSeperatedSizeFilter = (String) sizeItem.getPropertyValue("sizeFilters");
		final List<String> sizeFiltersList = splitSizeFilters(delimiterSeperatedSizeFilter);
		for (String sizeFilter : sizeFiltersList) {
			sizeFilters.add(sizeFilter);
		}
		return sizeFilters;
	}

	private List<String> splitSizeFilters(String delimiterSeperatedSizeFilter) {
		final List<String> retVal = new ArrayList<>();
		if (DigitalStringUtil.isNotBlank(delimiterSeperatedSizeFilter)) {
			retVal.add(delimiterSeperatedSizeFilter);
			final String[] values = DigitalStringUtil.split(delimiterSeperatedSizeFilter, getSizeFilterDelimiter());
			if (values != null && values.length > 0) {
				retVal.clear();
				for (final String value : values) {
					retVal.add(value);
				}
			}
		}
		return retVal;
	}

	private RepositoryItem getSize(String id) {
		try {
			return this.getCatalogRepository().getItem(id, getSizeItemType());
		} catch (RepositoryException req) {
			if (isLoggingWarning())
				logWarning("Couldn't get size item with id {" + id + "}");
			if (isLoggingDebug())
				logDebug(req);
		}
		return null;
	}
}
