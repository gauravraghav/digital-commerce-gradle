package com.digital.commerce.endeca.assembler.cartridge.support;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.servlet.ServletUtil;
import com.ctc.wstx.util.StringUtil;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.RefinementMenuConfig;
import com.endeca.infront.cartridge.model.RefinementBreadcrumb;
import com.endeca.infront.cartridge.support.BreadcrumbBuilder;
import com.endeca.infront.navigation.MdexRequestBroker;
import com.endeca.infront.navigation.NavigationException;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.FilterState;
import com.endeca.infront.navigation.model.SearchFilter;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.infront.navigation.request.RefinementMdexQuery;
import com.endeca.infront.navigation.request.RefinementMdexQuery.RefinementSortType;
import com.endeca.infront.navigation.url.ActionPathProvider;
import com.endeca.infront.site.model.SiteState;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.DimensionList;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.UrlParam;
import com.endeca.soleng.urlformatter.UrlState;
import com.endeca.soleng.urlformatter.basic.BasicUrlFormatter;

import atg.commerce.endeca.cache.DimensionValueCache;
import atg.commerce.endeca.cache.DimensionValueCacheObject;

public class NavigationStateSupport {

	private static HashMap<String, ENEQueryResults>	cachedResults		= new HashMap<>();
	private static final DigitalLogger					logger				= DigitalLogger.getLogger( NavigationStateSupport.class );
	private static boolean							cleanupInitialized	= false;

	public static ENEQueryResults getResultsForNavigationState( RefinementMenuConfig currentFacetConfiguration, NavigationState currentNavigationState, MdexRequestBroker mdexRequestFactory, long timeout, int eneQueryTimeout )
			throws CartridgeHandlerException, NavigationException {
		if( currentNavigationState == null ) { return null; }
		ExecutorService executor = Executors.newCachedThreadPool();
		RefinementMdexQuery refinement = createRefinementForCall( currentFacetConfiguration );
		final MdexRequest mdexRequest = mdexRequestFactory.createMdexRequest( currentNavigationState.getFilterState(), refinement );
		StringBuilder cacheKey = new StringBuilder( "" );
		FilterState filterState = currentNavigationState.getFilterState();

		for( String filter : filterState.getNavigationFilters() )
			cacheKey.append( filter ).append( "-" );

		for( String recFilter : filterState.getRecordFilters() )
			cacheKey.append( recFilter ).append( "-" );

		for( SearchFilter searchFilter : filterState.getSearchFilters() )
			cacheKey.append( searchFilter.getTerms() ).append( "-" );

		cacheKey.append( refinement.getDimensionId() );

		if( !cleanupInitialized ) {
			initializeCleanup( timeout );
		}

		ENEQueryResults results;
		if( !cachedResults.containsKey( cacheKey.toString() ) ) {

			Callable<ENEQueryResults> task = new Callable<ENEQueryResults>() {
				@Override
				public ENEQueryResults call() throws ENEQueryException, NavigationException {
					return mdexRequest.execute();
				}
			};
			Future<ENEQueryResults> future;
			try {
				future = executor.submit( task );
				results = future.get( eneQueryTimeout, TimeUnit.MILLISECONDS );
				currentNavigationState.inform( results );
				addCacheEntry( cacheKey.toString(), results );

				if( results.getNavigation() == null ) {
					results = null;
				}
				return results;
			} catch( InterruptedException | ExecutionException | TimeoutException e ) {
				// ExecutionException will be the wrapped ENE exception
				logger.error( "NavigationStateSupport process exception", e );
			}
		} else {
			logger.debug( "Cached results found! while building out navigation for key=" + cacheKey.toString() );
			results = cachedResults.get( cacheKey.toString() );
			currentNavigationState.inform( results );
			return results;
		}
		return null;
	}

	/** This method is a synchronized method to add an entry to the cache of ENEQueryResults. This prevents multiple threads from
	 * stepping on each other.
	 * 
	 * @param cacheKey
	 * @param results */
	private synchronized static void addCacheEntry( String cacheKey, ENEQueryResults results ) {
		cachedResults.put( cacheKey, results );
	}

	public static Dimension getDimensionByNavigationState( RefinementMenuConfig cartridgeConfig, NavigationState navState, MdexRequestBroker mdexReq, long timeout, int eneQueryTimeout ) throws CartridgeHandlerException, NavigationException {
		if( navState == null ) { return null; }
		ENEQueryResults results = getResultsForNavigationState( cartridgeConfig, navState, mdexReq, timeout, eneQueryTimeout );
		if(null!=results){
			return getDimensionByNavigationState( cartridgeConfig, results );
		}else{
			return null;
		}
		
	}

	public static Dimension getDimensionByNavigationState( RefinementMenuConfig cartridgeConfig, ENEQueryResults results ) {
		if( null!=results && results.getNavigation() == null ) {
			return null;
			}
		Dimension dimension = null;
		if(null != results) {
			dimension = results.getNavigation().getRefinementDimensions().getDimension( Long.parseLong( cartridgeConfig.getDimensionId() ) );
		}

		if( dimension == null ) {
			return null;
		}

		return dimension;
	}

	public static NavigationState getNavigationStateForCall( NavigationState navStateOrig, String dimId, boolean searchPage, String searchPageFilterAddId ) throws CartridgeHandlerException, NavigationException {
		if( navStateOrig == null ) { return null; }
		NavigationState navigationStateTemp = navStateOrig.augment( navStateOrig.getFilterState().clone() );

		if( navStateOrig.getFilterState() != null ) {
			if( navStateOrig.getFilterState().getNavigationFilters() != null ) navigationStateTemp.getFilterState().getNavigationFilters().clear();
			if( navStateOrig.getFilterState().getRangeFilters() != null ) navigationStateTemp.getFilterState().getRangeFilters().clear();
			if( navStateOrig.getFilterState().getRecordFilters() != null ) navigationStateTemp.getFilterState().getRecordFilters().clear();

			if( searchPage ) {
				if( navStateOrig.getFilterState().getSearchFilters().isEmpty() && searchPageFilterAddId != null ) {
					if( searchPageFilterAddId.contains( " " ) ) {
						navigationStateTemp.getFilterState().getNavigationFilters().addAll( Arrays.asList( searchPageFilterAddId.split( " " ) ) );
					} else
						navigationStateTemp.getFilterState().getNavigationFilters().add( searchPageFilterAddId );
				}
			} else {
				if( navStateOrig.getFilterState().getSearchFilters() != null ) navigationStateTemp.getFilterState().getSearchFilters().clear();
			}
		}

		return getNavigationStateForCall( navigationStateTemp, dimId );

	}

	public static NavigationState getNavigationStateForCall( NavigationState navStateOrig, String dimId ) throws CartridgeHandlerException, NavigationException {
		if( navStateOrig == null ) { return null; }
		NavigationState navigationStateTemp = navStateOrig.augment( navStateOrig.getFilterState().clone() );
		if( !dimId.equals( DigitalEndecaConstants.ZERO ) ) navigationStateTemp.getFilterState().getNavigationFilters().add( dimId );

		return navigationStateTemp;

	}

	// get the navigation state of the link in question
	private static List<String> getNavigationStateFilters( UrlParam urlParam ) {
		List<String> navigationFilters = new ArrayList<>();
		if( urlParam != null && urlParam.getValue() != null ) {
			navigationFilters.addAll( Arrays.asList( urlParam.getValue().split( " " ) ) );
		}
		return navigationFilters;
	}

	public static List<String> getNavigationState( String queryState ) {

		BasicUrlFormatter urlFormatterTemp = new BasicUrlFormatter();
		UrlState urlState = null;
		try {
			urlState = urlFormatterTemp.parseRequest( queryState, "UTF-8" );
		} catch( UrlFormatException e ) {
			logger.error( "DigitalTopNavigationListHandler Size exception", e );
		}
		List<String> navStateLinkFilter=null;
		if(null!=urlState)
		{
		UrlParam navigationParam = urlState.getUrlParam( "N" );		
		navStateLinkFilter = getNavigationStateFilters( navigationParam );
		}
		return navStateLinkFilter;
		
	}

	private static Timer	cacheUpdateTimer	= new Timer( true );

	public static void initializeCleanup( long timeout ) {

		cacheUpdateTimer.scheduleAtFixedRate( new TimerTask() {

			@Override
			public void run() {
				logger.debug( "clearing NavigationStateSupport cache" );
				cachedResults.clear();

			}
		}, 0, timeout );
		cleanupInitialized = true;
	}

	public static RefinementMdexQuery createRefinementForCall( RefinementMenuConfig cartridgeConfig ) throws CartridgeHandlerException {
		RefinementMdexQuery refinement = new RefinementMdexQuery();
		refinement.setDimensionId( cartridgeConfig.getDimensionId() );
		updateSortConfig( cartridgeConfig, refinement );
		refinement.setNumRefinements( cartridgeConfig.getLimit() );
		refinement.setBoostedDvals( cartridgeConfig.getBoostRefinements() );
		refinement.setBuriedDvals( cartridgeConfig.getBuryRefinements() );
		refinement.setWhyPrecedenceRuleFiredEnabled( cartridgeConfig.isWhyPrecedenceRuleFired() );
		return refinement;
	}

	public static void updateSortConfig( RefinementMenuConfig cartridgeConfig, RefinementMdexQuery dimConfig ) throws CartridgeHandlerException {
		RefinementSortType sort = RefinementSortType.DISABLED;
		if( cartridgeConfig.getSort() != null ) {
			String configuredSort = cartridgeConfig.getSort().toUpperCase();
			if( DigitalEndecaConstants.DYNRANK.equalsIgnoreCase( configuredSort ) ) {
				sort = RefinementSortType.DYNAMIC;
			} else if( DigitalEndecaConstants.STATIC.equalsIgnoreCase( configuredSort ) ) {
				sort = RefinementSortType.STATIC;
			} else if( !DigitalEndecaConstants.DEFAULT.equalsIgnoreCase( configuredSort ) ) { throw new CartridgeHandlerException( "sort configuration: " + configuredSort + " is invalid." ); }
		}
		dimConfig.setSortType( sort );
	}

	public static List<RefinementBreadcrumb> createBreadcrumbsForDimension( long dimensionId, DimensionList descDimensionsBC, ActionPathProvider provider, NavigationState navState, SiteState siteState) {
		List<RefinementBreadcrumb> refinementcrumbs = new ArrayList<>();
		Dimension bcDim;
		for( Object dimObject : descDimensionsBC ) {
			bcDim = (Dimension)dimObject;
			if( bcDim.getId() == dimensionId ) {
				DimVal desc = bcDim.getDescriptor();
				RefinementBreadcrumb crumb = BreadcrumbBuilder.createRefinementBreadcrumb( provider, navState,siteState, bcDim );
				crumb.getProperties().put( DigitalEndecaConstants.DIM_ID_PROPERTY_NAME, Long.toString( desc.getId() ) );
				refinementcrumbs.add( crumb );
			}
		}
		return refinementcrumbs;
	}

	public static String removeCategoryFromBreadcrumbs( DimensionList descDimensionsBC ) {
		Object[] descDimensionsArray = descDimensionsBC.toArray();
		Dimension bcDim;
		for( Object dimObject : descDimensionsArray ) {
			bcDim = (Dimension)dimObject;
			if( bcDim.getName().equals( DigitalEndecaConstants.PRODUCTCATEGORY ) ) { return Long.toString( bcDim.getDescriptor().getId() ); }
		}
		return null;
	}
	
	public static String removeAlreadySelectedFilters(NavigationState navState, String appliedFilters) {
		
		Map<String, String> allFilters = new LinkedHashMap<>();
		String navFiltersAsString = ""; //This string will have final list of Nav IDs after removing least priority dimensions
		List<String> navRequestFilters = navState.getFilterState().getNavigationFilters();
		
		//If no filters applied by the user then just send the + seperated String of Dimension Value IDs that are already coming from Base URL (Top nav or Footer)
		if (DigitalStringUtil.isBlank(appliedFilters)) {
			// convert navReqFilter to a "+" delimited string
			for( int i = 0; i < navRequestFilters.size(); i++ ) {
				navFiltersAsString = navFiltersAsString + navRequestFilters.get( i ) + DigitalEndecaConstants.PLUS;
			}
			// remove the + at the end of the string if length > 0
			if( !navFiltersAsString.isEmpty() ) navFiltersAsString = navFiltersAsString.substring( 0, navFiltersAsString.length() - 1 );
			return navFiltersAsString;
		}
		
		// convert navRequestFilters to a Map of <Dimension Name, Dimension Value ID>
		for( int i = 0; i < navRequestFilters.size(); i++ ) {
			if (navState.getDimLocation(navRequestFilters.get(i)) == null)
				logger.error( "Dimension Value ID does not exist in Nav state. Clear all/Filters won't work properly");
			String key = navState.getDimLocation(navRequestFilters.get(i)).getDimValue().getDimensionName();
			if (allFilters.containsKey(key)) {
				allFilters.put(key, allFilters.get(key) + "+" + navRequestFilters.get(i));
			} else {
				allFilters.put(key, navRequestFilters.get(i));
			}			
		}
		
		//Let's remove the user applied filters from the allFilters list. Remember, allFilters has the base filters too
		List<String> appliedFiltersList = Arrays.asList(appliedFilters.split(","));		
		for (int i=0; i<appliedFiltersList.size(); i++){
			if(DigitalEndecaConstants.TRUE.equalsIgnoreCase(navState.getParameter(DigitalEndecaConstants.IS_SHOP_BY_SIZE)) && ("gender".equalsIgnoreCase(appliedFiltersList.get(i)))){
				continue;
			}
			allFilters.remove(appliedFiltersList.get(i));
		}	
		
		// convert allFilters to a "+" delimited string. Ideally this should be a base page.
		for (Map.Entry<String, String> entry : allFilters.entrySet()) {
			navFiltersAsString = navFiltersAsString + entry.getValue() + DigitalEndecaConstants.PLUS;
		}
		
		// remove the + at the end of the string if length > 0
		if( !navFiltersAsString.isEmpty() ) navFiltersAsString = navFiltersAsString.substring( 0, navFiltersAsString.length() - 1 );
		return navFiltersAsString;
		
				
	}
	
	/**
	 * 
	 * @param DimensionNames - List of Dimension Names. Can contain 1 Dimension name also
	 * @return Will return TRUE, if any of the given Dimension name is used for Filtering in the given Navigation State
	 */
	public static Boolean containsNavigationFilter(NavigationState navState, List<String> DimensionNames) {
		
		//Get all Filters that user is using to Filter on
		List<String> navFilters = navState.getFilterState().getNavigationFilters();
		
		//Loop through all Filters that user is using
		for (String navFilter : navFilters) {
			//If that filter belongs to a Dimension that we are looking for then return true
			for (String dimName : DimensionNames) {
				if (DigitalStringUtil.equals(navState.getDimLocation(navFilter).getDimValue().getDimensionName(),
						dimName)) {
					return true;
				}
			}
			
		}
		return false; //If nothing matches then return false
	}

	/**
	 * This method uses the DimValCache to find the Dimension Name.
	 * Note: Use this method for only those Dimensions that are in DimValCache (Base dimensions)
	 *@param navState - Current Navigation state
	 * @param DimensionNames - List of Dimension Names. Can contain 1 Dimension name also
	 * @return Will return TRUE, if any of the given Dimension name is used for Filtering in the given Navigation State
	 */
	public static Boolean containsNavigationFilterfromDimValCache(NavigationState navState, List<String> DimensionNames, DimensionValueCacheTools dimValCacheTools) {

		//Get all Filters that user is using to Filter on
		List<String> navFilters = navState.getFilterState().getNavigationFilters();
		DimensionValueCache dimValCache = dimValCacheTools.getCache();

		if( dimValCache.getNumCacheEntries() > 0 ) {
			//Loop through all Filters that user is using
			for (String navFilter : navFilters) {
				//If that filter belongs to a Dimension that we are looking for then return true
				for (String dimName : DimensionNames) {
					List<DimensionValueCacheObject> dimValCacheObject = dimValCache.get(navFilter);
					if (dimValCacheObject != null && DigitalStringUtil.contains(dimValCacheObject.get(0).getDimvalId(), dimName)) {
						 return true;
					}
				}
			}
		}
		return false; //If nothing matches then return false
	}

	/**
	 * This method will return TRUE if user is filtering on Size, Color or Width (passed in DimensionNames) OR if the User is on Search page
	 * @param navState
	 * @param DimensionNames
	 * @return Boolean
	 */
	public static Boolean shouldUseFaceoutfromChildSKU(NavigationState navState, List<String> DimensionNames, List<String> Keywords) {
		try {
    		FilterState filterState = navState.getFilterState();
    		
        	//If user is searching by a Keyword (search page) then can't use V2. If we use V2 then faceout color and Color swatches won't work properly as they are at product level
    		//TEMPORARY LOGIC: This logic to check olive or blue or teal is temporary for a demo. I will fix this by adding a property file settings once approved.
        	if (null != filterState.getSearchFilters() && filterState.getSearchFilters().size() > 0
    				&& DigitalStringUtil.isNotBlank(filterState.getSearchFilters().get(0).getTerms()) ) {
        		String searchKeyword = filterState.getSearchFilters().get(0).getTerms().toLowerCase().trim();
        		if (DigitalStringUtil.isNotBlank(ServletUtil.getCurrentRequest().getParameter(DigitalEndecaConstants.CORRECTED_SEARCH_KEYWORD))) {
        			searchKeyword = ServletUtil.getCurrentRequest().getParameter(DigitalEndecaConstants.CORRECTED_SEARCH_KEYWORD).toLowerCase().trim();
				}
        		for(String keyword: Keywords) {        			
        		    if(searchKeyword.matches(".*?\\b" + keyword.toLowerCase().trim() + "\\b.*?"))
        		       return true;
        		}
        		
    		}
        	
        	//This is a Category page. Check if User is filtering on Size, Width or Color (DimensionNames). If yes, then return True
        	return containsNavigationFilter(navState, DimensionNames);
        	
		}
    	catch(Exception ex) {
    		logger.error( "Exception thrown while trying to find out if we can need to use FaceoutColor from SKu or not - shouldUseFaceoutfromChildSKU()", ex );
    		return false; //This is default in case exception happens. Let's safely use V1 and give full functionality to end user
    	}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
