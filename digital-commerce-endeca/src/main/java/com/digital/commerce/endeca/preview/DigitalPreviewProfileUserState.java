package com.digital.commerce.endeca.preview;

import java.util.HashSet;
import java.util.Set;

import com.digital.commerce.common.util.DigitalStringUtil;

import atg.servlet.ServletUtil;
import atg.userprofiling.endeca.PreviewProfileUserState;

public class DigitalPreviewProfileUserState extends PreviewProfileUserState {
	
	public Set<String> getProfileGroups(){
		String endecaUserSegment = ServletUtil.getCurrentRequest().getParameter("Endeca_user_segments");
		/*Only If the request is coming from Preview endeca XM, it will have the Endeca_user_segments 
		 * parameter We will not execute ATG profile user segments in that case which is done in 
		 * getProfileGroups method of parent class*/
		if(DigitalStringUtil.isEmpty(endecaUserSegment)){
			return super.getProfileGroups();
		}
		else
		{
			return new HashSet<>(); //returning new not null because in the calling method in 
									//MdexContentRequestBroker is not making anull check
		}
	}

}
