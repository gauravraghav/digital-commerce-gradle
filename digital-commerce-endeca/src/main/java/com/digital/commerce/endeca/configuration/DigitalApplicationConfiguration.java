package com.digital.commerce.endeca.configuration;

import java.util.Map;

import atg.endeca.configuration.ApplicationConfiguration;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("rawtypes")
@Getter
@Setter
public class DigitalApplicationConfiguration extends ApplicationConfiguration {

	private Map<String, String>	applicationKeyToSites;

}
