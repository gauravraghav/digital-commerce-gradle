package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.content.support.ConfigNodeContentItem;
import com.endeca.infront.content.support.XmlContentItem;
import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.ENEQuery;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.PropertyMap;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"unchecked"})
@Getter
@Setter
public class DigitalSearchBoxHandler extends NavigationCartridgeHandler<ConfigNodeContentItem, ConfigNodeContentItem> {
	private static DigitalLogger logger = DigitalLogger.getLogger(DigitalSearchBoxHandler.class);
	private String currentMdexHostName;
	private int currentMdexPort;
	private String recordId;
	private static ENEConnection eneConnection;
	private static final String METHOD_NAME = "process";
	private final ENEQuery eneQuery = new ENEQuery();
	private int eneQueryTimeout;

	@Override
	public void preprocess(ConfigNodeContentItem pCartridgeConfig) throws CartridgeHandlerException {
		eneConnection = new HttpENEConnection(getCurrentMdexHostName(), String.valueOf(getCurrentMdexPort()));
		String queryString = getRecordId();
		eneQuery.setERecSpec(queryString);
	}

	@Override
	public ConfigNodeContentItem process(ConfigNodeContentItem pCartridgeConfig) throws CartridgeHandlerException {
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
		ExecutorService executor = Executors.newCachedThreadPool();
		Callable<ENEQueryResults> task = new Callable<ENEQueryResults>() {
			@Override
			public ENEQueryResults call() throws ENEQueryException {
				return eneConnection.query(eneQuery);
			}
		};

		Future<ENEQueryResults> future = null;

		try {
			future = executor.submit(task);
			ENEQueryResults eneQueryResults = future.get(eneQueryTimeout, TimeUnit.MILLISECONDS);
			ERec rec = eneQueryResults.getERec();
			PropertyMap propsMap = rec.getProperties();
			Collection<String> topSearchKeywords = propsMap.getValues(DigitalEndecaConstants.TOPSEARCH_ENDECA_PROPERTY);
			pCartridgeConfig.put(DigitalEndecaConstants.TRENDINGFIELD, topSearchKeywords);
		} catch (InterruptedException | ExecutionException | TimeoutException ex) {
			// ExecutionException will be the wrapped ENE exception
			logger.error("Exception while reading the dimension value from MDEX response", ex);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
		}
		return pCartridgeConfig;

	}

	@Override
	protected ConfigNodeContentItem wrapConfig(ContentItem paramContentItem) {
		return (ConfigNodeContentItem) paramContentItem;
	}

}
