package com.digital.commerce.endeca.index.accessor;

import atg.commerce.pricing.priceLists.PriceListException;
import atg.commerce.pricing.priceLists.PriceListManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessorHelper extends GenericService {

	private PriceListManager priceListManager;

	Object getActivePriceList(Context pContext, RepositoryItem skuItem) throws PriceListException {
		String[] allowablePriceLists = (String[]) pContext.getAttribute("atg.priceListVariantProdcer.priceListIds");

		if (allowablePriceLists == null) {
			if (isLoggingDebug()) {
				logDebug("Error class : DiscountPropertyAccessor.\n Error Method : getTextOrMetaPropertyValue."
						+ " \n Error : Could not locate the priceLists associated. Hence not able to find price and hence discount");
			}
			return null;
		}

		Object activePrice = null;
		for (String priceListId : allowablePriceLists) {
			RepositoryItem priceList = priceListManager.getPriceList(priceListId);
			if (priceList != null) {
				RepositoryItem price = priceListManager.getPrice(priceList, null, skuItem);
				if (price != null) {
					Object p = price.getPropertyValue("listPrice");
					if (p != null) {
						return priceList;
					}
				}
			}
		}
		return activePrice;
	}
}
