package com.digital.commerce.endeca.index.accessor;

import java.util.Set;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.cartridge.handler.DigitalTopNavigationListHandler;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
@SuppressWarnings({"rawtypes"})
public class PricePropertyAccessor extends PropertyAccessorImpl {

	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalTopNavigationListHandler.class);

	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
			PropertyTypeEnum pType) {
		try {
			String itemDescriptor = pItem.getItemDescriptor().getItemDescriptorName();
			if (itemDescriptor.equals(DigitalEndecaConstants.SKU)) {
				Set parentProductsItem = null;
				RepositoryItem currentParentItem = null;
				Object parentProducts = pItem.getPropertyValue(DigitalEndecaConstants.PARENT_PRODUCTS);
				if (null != parentProducts) {
					parentProductsItem = (Set) parentProducts;
					if (null != parentProductsItem) {
						for (Object parentProductObject : parentProductsItem) {
							currentParentItem = (RepositoryItem) parentProductObject;
							Object isActiveObj = currentParentItem.getPropertyValue("isActive");
							if ((null != isActiveObj)
									&& (Boolean) isActiveObj) {
								Object priceInCartObj = currentParentItem.getPropertyValue(DigitalEndecaConstants.PRICE_IN_CART);
								if (null != priceInCartObj && DigitalStringUtil.equals(priceInCartObj.toString(), DigitalEndecaConstants.STRING_TRUE)) {
									return DigitalEndecaConstants.ZERO;
								}
							}
						}
					} 
				}
				return pItem.getPropertyValue(pPropertyName);
			} else {
				Object priceInCartObj = pItem.getPropertyValue(DigitalEndecaConstants.PRICE_IN_CART);
				if (null != priceInCartObj
						&& (Boolean) priceInCartObj) {
					return DigitalEndecaConstants.ZERO;
				} else {
					return pItem.getPropertyValue(pPropertyName);
				}
			}
		}

		catch (RepositoryException repositoryException) {
			logger.error("Exception  caught while updating the price property for indexing ::::::\n   ",
					repositoryException);
			return null;

		}
	}
}