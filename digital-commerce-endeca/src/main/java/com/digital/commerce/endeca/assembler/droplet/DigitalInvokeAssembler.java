package com.digital.commerce.endeca.assembler.droplet;

import atg.endeca.assembler.droplet.InvokeAssembler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import lombok.Getter;
import lombok.Setter;

import javax.servlet.ServletException;
import java.io.IOException;

@Getter
@Setter
public class DigitalInvokeAssembler extends InvokeAssembler {
    private 	final DigitalLogger logger						= DigitalLogger.getLogger( getClass().getName() );
    private int errorResponseCode;

    /* Override out of the box InvokeAssembler.service */
    /* Purpose of this override is to set Response status to 503 when Endeca is busy */
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        final String METHOD_NAME = "service";

        try {
            DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
            super.service(pRequest, pResponse);
            if (pRequest.getParameter(this.CONTENT_ITEM_PARAM) == null) {
                // If there is no "contentItem" then something wrong for sure. It can be timeout, Content not exists etc.,
                pResponse.setStatus(this.getErrorResponseCode());
            }
        } catch (Exception e) {
            logger.error(e);
            pResponse.setStatus(this.getErrorResponseCode());
        } finally {
            DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
        }

    }
}
