package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.endeca.assembler.cartridge.support.CartridgeHandlerUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.cartridge.model.LinkBuilder;
import com.endeca.infront.content.support.ConfigNodeContentItem;

import atg.commerce.endeca.cache.DimensionValueCacheTools;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalLinkToCacheHandler extends NavigationCartridgeHandler<ConfigNodeContentItem, ConfigNodeContentItem> {

	private DimensionValueCacheTools	dimensionValueCacheTools;
	private static final DigitalLogger		logger	= DigitalLogger.getLogger( DigitalLinkToCacheHandler.class );

	@Override
	public ConfigNodeContentItem process( ConfigNodeContentItem pCartridgeConfig ) throws CartridgeHandlerException {
		final String METHOD_NAME = "process";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		try {
			Set<String> propSet = pCartridgeConfig.keySet();
			Iterator<String> it = propSet.iterator();
			while( it.hasNext() ) {
				String sectionNAme = it.next();
				// Process only if we have "linktext" section
				if( DigitalEndecaConstants.LINKTEXT.equalsIgnoreCase( sectionNAme ) ) {
					LinkBuilder linkBuilder = (LinkBuilder)pCartridgeConfig.get( sectionNAme );
					String queryString = linkBuilder.getQueryString();

					// Why this condition?
					// We need to Cache only those Link builder that has Navigation States selected from Record browser
					// No need to Cache Link builders in which business copied & pasted the URL
					if( queryString != null ) {
						linkBuilder.setQueryString( CartridgeHandlerUtil.generateSEONvalue( queryString ) );
						pCartridgeConfig.put( DigitalEndecaConstants.LINKTEXT, linkBuilder );
						if( getDimensionValueCacheTools().getCache() != null ) {
							String dimValID = queryString.split( DigitalEndecaConstants.EQUALTO )[1];

							/* Cache format
							 * Actual: RepositoryID, DimValID, URL, Ancestors
							 * Now: <<SiteID>>-DimValID, DimValID, URL, CacheProperties (to store additional info)
							 * Always any links from Top Nav and Footer are Site specific. So, make sure to add <SiteID> to the Key */
							if( getDimensionValueCacheTools().getCache().get( getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE ) + "-" + dimValID ) == null ) {

								// Add the display name and "Hide Refinement" to the Cache Properties. Only those cache objects with
								// "Hide Refinement" will have that dimension disabled in collection page
								String h1TagText = "";
								String titleTag = "";
								String metaDescription = "";
																
								titleTag = pCartridgeConfig.get(DigitalEndecaConstants.TITLE_TAG_PROPERTY).toString();
								metaDescription = pCartridgeConfig.get(DigitalEndecaConstants.META_PROPERTY).toString();
								h1TagText = pCartridgeConfig.get(DigitalEndecaConstants.H1TAGTEXT).toString();

								List<String> cacheProperties = Arrays.asList( h1TagText, DigitalEndecaConstants.HIDEINREFINEMENT, titleTag, metaDescription );

								getDimensionValueCacheTools().getCache().put( getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE ) + "-" + dimValID, dimValID, null, cacheProperties );
							}
						}
					}
				}
			}
		} catch( Exception e ) {
			logger.error( "DigitalLinkToCacheHandler exception", e );
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}

		return pCartridgeConfig;
	}

	@Override
	protected ConfigNodeContentItem wrapConfig( ContentItem paramContentItem ) {
		return (ConfigNodeContentItem)paramContentItem;
	}

}
