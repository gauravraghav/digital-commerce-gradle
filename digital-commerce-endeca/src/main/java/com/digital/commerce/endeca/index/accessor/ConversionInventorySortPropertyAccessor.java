package com.digital.commerce.endeca.index.accessor;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;

public class ConversionInventorySortPropertyAccessor extends PropertyAccessorImpl {

    private static final DigitalLogger logger = DigitalLogger.getLogger(ConversionInventorySortPropertyAccessor.class);


    protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
                                                PropertyTypeEnum pType) {

        try {

            Object productUnitsSoldLastWeekObj = pItem.getPropertyValue(DigitalEndecaConstants.PRODUCT_UNITS_SOLD_WEEK);
            Object productConversionObj = pItem.getPropertyValue(DigitalEndecaConstants.PRODUCT_CONVERSION_RATIO);
            Object productStockLevelObj = pItem.getPropertyValue(DigitalEndecaConstants.PRODUCT_STOCK_LEVEL);

            if (null != productUnitsSoldLastWeekObj && null != productStockLevelObj && null != productConversionObj) {
                int productUnitsSoldLastWeek = Integer.parseInt(productUnitsSoldLastWeekObj.toString());
                float productConversion = Float.parseFloat(productConversionObj.toString());
                long productStockLevel    = Long.parseLong(productStockLevelObj.toString());

                //RULE: If current inventory is LESS THAN last 7 days Units sold then set demand as ZERO
                if (productStockLevel < productUnitsSoldLastWeek) {
                    return 0;
                } else {
                    return productConversion;
                }
            }

            return 0; // If any data doesn't exist then return 0

        } catch (Exception e) {
            logger.error("Exception occurred while calculating product.demandConversionInventorySort", e);
            return 0;
        }
    }
}
