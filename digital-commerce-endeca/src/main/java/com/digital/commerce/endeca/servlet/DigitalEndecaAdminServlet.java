package com.digital.commerce.endeca.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.endeca.infront.assembler.servlet.admin.AbstractAdminServlet;
import com.endeca.infront.assembler.servlet.admin.AdministrationService;

import atg.nucleus.Nucleus;
import atg.servlet.ServletUtil;

public class DigitalEndecaAdminServlet extends AbstractAdminServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8337536469468096317L;
	private String adminServicePath = null; //NOSONAR

	public void init(ServletConfig pConfig) throws ServletException {
		super.init(pConfig);

		this.adminServicePath = pConfig.getInitParameter("adminServicePath");
		if (DigitalStringUtil.isEmpty(this.adminServicePath)) {
			throw new ServletException(
					"Required parameter not found: adminServicePath.  Please check your servlet configuration.");
		}
	}

	protected AdministrationService getAdministrationService() {
		if (ServletUtil.getCurrentRequest() != null)
			return (AdministrationService) ServletUtil.getCurrentRequest()
					.resolveName(this.adminServicePath, true);
		else
			return (AdministrationService) Nucleus.getGlobalNucleus()
					.resolveName(this.adminServicePath);

	}

}
