package com.digital.commerce.endeca.index.accessor;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;

public class DemandSortPropertyAccessor extends PropertyAccessorImpl {

    private static final DigitalLogger logger = DigitalLogger.getLogger(DemandSortPropertyAccessor.class);

    protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
                                                PropertyTypeEnum pType) {

        try {

            Object productUnitsSoldLastWeekObj = pItem.getPropertyValue(DigitalEndecaConstants.PRODUCT_UNITS_SOLD_WEEK);
            Object productDemandObj = pItem.getPropertyValue(DigitalEndecaConstants.PRODUCT_DEMAND);
            Object productStockLevelObj = pItem.getPropertyValue(DigitalEndecaConstants.PRODUCT_STOCK_LEVEL);

            if (null != productUnitsSoldLastWeekObj && null != productDemandObj && null != productStockLevelObj) {
                int productUnitsSoldLastWeek = Integer.parseInt(productUnitsSoldLastWeekObj.toString());
                int productDemand         = Integer.parseInt(productDemandObj.toString());
                long productStockLevel    = Long.parseLong(productStockLevelObj.toString());

                //RULE: If current inventory is LESS THAN last 7 days Units sold then set demand as ZERO
                if (productStockLevel < productUnitsSoldLastWeek) {
                    productDemand = 0;
                }
                return  productDemand;
            }

            return 0; // If any data doesn't exist then return 0

        } catch (Exception e) {
            logger.error("Exception occurred while calculating product.demandSort", e);
            return 0;
        }
    }
}
