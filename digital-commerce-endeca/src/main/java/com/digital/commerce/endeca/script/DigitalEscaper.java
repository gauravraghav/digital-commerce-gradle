/**
 * 
 */
package com.digital.commerce.endeca.script;

/**
 * @author TAIS
 *
 */
public class DigitalEscaper {

    public static final char[] sHexDigits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public DigitalEscaper() {
    }

    public String toSafeString(String pStr) {
        int len = pStr.length();
        StringBuilder buf = new StringBuilder(len);
        appendSafeString(buf, pStr);
        return buf.toString();
    }

    public StringBuilder appendSafeString(StringBuilder pBuilder, String pStr) {
        StringBuilder buf = pBuilder;
        char[] var5 = pStr.toCharArray();
        int var6 = var5.length;

        for(int var7 = 0; var7 < var6; ++var7) {
            char c = var5[var7];
            if (c == '_') {
                buf.append('_');
                buf.append('_');
            } else if (!shouldEscapeChar(c)) {
                buf.append(c);
            } else {
                buf.append('_');
                buf.append(sHexDigits[c >> 12 & 15]);
                buf.append(sHexDigits[c >> 8 & 15]);
                buf.append(sHexDigits[c >> 4 & 15]);
                buf.append(sHexDigits[c & 15]);
            }
        }

        return buf;
    }

    protected boolean shouldEscapeChar(char pChar) {
        return (pChar < 'A' || pChar > 'Z') && (pChar < 'a' || pChar > 'z') && (pChar < '0' || pChar > '9') && pChar != '-' && pChar != '.';
    }
}
