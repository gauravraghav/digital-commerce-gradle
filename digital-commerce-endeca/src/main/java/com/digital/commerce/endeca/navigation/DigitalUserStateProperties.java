package com.digital.commerce.endeca.navigation;

import atg.endeca.assembler.navigation.UserStateProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalUserStateProperties extends UserStateProperties{
	private String regionPrefix = null;
}