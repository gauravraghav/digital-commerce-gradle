/**
 * 
 */
package com.digital.commerce.endeca.recordstore;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author oracle
 *
 */
@Getter
@Setter
public class TypeAheadVO {

	private String productId;
	
	private String recordId;
	
	private String productActive;
	
	private String allAncestorsId;
	
	private String siteId;
	
	private List<String> typeAheadKeywords;
}
