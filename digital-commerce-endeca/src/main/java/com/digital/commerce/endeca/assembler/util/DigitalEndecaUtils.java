package com.digital.commerce.endeca.assembler.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.methods.GetMethod;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.cartridge.handler.DigitalResultsListHandler.InventoryClearanceCombo;
import com.digital.commerce.endeca.assembler.cartridge.handler.DigitalTopNavigationListHandler;
import com.digital.commerce.endeca.assembler.cartridge.support.CartridgeHandlerUtil;
import com.digital.commerce.endeca.assembler.cartridge.support.NavigationStateSupport;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.cartridge.model.Attribute;
import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValList;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.DimensionList;
import com.endeca.navigation.Navigation;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalEndecaUtils {

	private static final DigitalLogger logger = DigitalLogger.getLogger( DigitalTopNavigationListHandler.class );

	/** @param queryString */
	public static String generateContentPathUrl( boolean httpsEnabled, String siteHttpServerName, int siteHttpsServerPort, String serviceUrl, String queryString, String contentPath, Map<String, String> parameters ) {

		final String METHOD_NAME = "generateContentPathUrl";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( DigitalEndecaUtils.class.getName(), METHOD_NAME );

		try {
			StringBuilder contentPathUrl = new StringBuilder();
			if( queryString != null ) {

				if( httpsEnabled ) {
					contentPathUrl.append( DigitalEndecaConstants.HTTPSPROTOCOL );
				} else {
					contentPathUrl.append( DigitalEndecaConstants.HTTPPROTOCOL );
				}

				contentPathUrl.append( siteHttpServerName );
				contentPathUrl.append( DigitalEndecaConstants.CLUMNSEPARATOR );

				contentPathUrl.append( siteHttpsServerPort );

				contentPathUrl.append( serviceUrl );
				if( null != CartridgeHandlerUtil.generateSEONvalue( queryString ) ) {
					contentPathUrl.append( CartridgeHandlerUtil.generateSEONvalue( queryString ) );
				}
				contentPathUrl.append( DigitalEndecaConstants.CONTENTPATH );
				contentPathUrl.append( contentPath );
				Iterator it = parameters.entrySet().iterator();
				while( it.hasNext() ) {
					Map.Entry<String, String> pair = (Map.Entry<String, String>)it.next();
					contentPathUrl.append( DigitalEndecaConstants.AMPERSAND + pair.getKey() + DigitalEndecaConstants.EQUALTO );
					contentPathUrl.append( pair.getValue() );
				}
				if( logger.isDebugEnabled() ) {
					logger.debug( "Final content path:  " + contentPathUrl.toString() );
				}

			}
			return contentPathUrl.toString();
		} catch( Exception e ) {
			logger.error( "generateContentPathUrl exception", e );
			return null;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( DigitalEndecaUtils.class.getName(), METHOD_NAME );
		}

	}

	/** Generates and returns Page path to query endeca
	 * 
	 * @param httpsEnabled
	 * @param siteHttpServerName
	 * @param siteHttpsServerPort
	 * @param serviceUrl
	 * @param queryString
	 * @param contentPath
	 * @param parameters
	 * @return */
	public static String generatePagePathUrl( boolean httpsEnabled, String siteHttpServerName, int siteHttpsServerPort, String serviceUrl, String queryString, String pagePath, Map<String, String> parameters ) {
		final String METHOD_NAME = "generatePagePathUrl";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( DigitalEndecaUtils.class.getName(), METHOD_NAME );

		try {
			StringBuilder pagePathUrl = new StringBuilder();
			if( queryString != null ) {

				if( httpsEnabled ) {
					pagePathUrl.append( DigitalEndecaConstants.HTTPSPROTOCOL );
				} else {
					pagePathUrl.append( DigitalEndecaConstants.HTTPPROTOCOL );
				}

				pagePathUrl.append( siteHttpServerName );
				pagePathUrl.append( DigitalEndecaConstants.CLUMNSEPARATOR );

				pagePathUrl.append( siteHttpsServerPort );

				pagePathUrl.append( serviceUrl );
				if( null != CartridgeHandlerUtil.generateSEONvalue( queryString ) ) {
					pagePathUrl.append( CartridgeHandlerUtil.generateSEONvalue( queryString ) );
				}
				pagePathUrl.append( DigitalEndecaConstants.PAGEPATH );
				pagePathUrl.append( pagePath );
				Iterator it = parameters.entrySet().iterator();
				while( it.hasNext() ) {
					Map.Entry<String, String> pair = (Map.Entry<String, String>)it.next();
					pagePathUrl.append( DigitalEndecaConstants.AMPERSAND + pair.getKey() + DigitalEndecaConstants.EQUALTO );
					pagePathUrl.append( pair.getValue() );
				}
				if( logger.isDebugEnabled() ) {
					logger.debug( "Final content path:  " + pagePathUrl.toString() );
				}

			}
			return pagePathUrl.toString();
		} catch( Exception e ) {
			logger.error( "generatePagePathUrl exception", e );
			return null;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( DigitalEndecaUtils.class.getName(), METHOD_NAME );
		}

	}
	
	
	
	/**
	 * Adds a new Property at Product level called "product.selectedColorCode" based on if user is filtering on Clearance or NOT
	 * If user filters on Clearance then use product.faceoutClearance else use product.faceoutNonClearance for "product.selectedColorCode"
	 * @param navState
	 */
	public static void addSelectedColorCodeV2(ArrayList<Record> prodRecords, NavigationState navState, List<String> DimensionNames, List<String> Keywords) {
		
		//First let's check if User is filtering on any of the "DimensionNames" that is passed to this method (Usually Color, Width, Size). These are SKU level Filters.
		Boolean userFilteringonSKUs = NavigationStateSupport.shouldUseFaceoutfromChildSKU(navState, DimensionNames, Keywords);
		
		//Second let's check if User is filtering on Clearance page.
		Boolean userIsOnClearancePage = NavigationStateSupport.containsNavigationFilter(navState, Arrays.asList(DigitalEndecaConstants.IS_CLEARANCE));
		
		//Now, loop through each product record and implement our logic
		if( null != prodRecords && prodRecords.size() > 0 ) {
			//If user is filtering on a SKU level Dimension then take the Color code from the First Child SKU and use that for SelectedColorCode.
			if (userFilteringonSKUs) {
				for( Record prodRecord : prodRecords ) {
					
					ArrayList<Record> skuRecords = (ArrayList<Record>)prodRecord.getRecords();
					
					//We should have one and only one record for Child SKUs
					if (skuRecords.size() == 1) {
						Record skuRecord = skuRecords.get(0);
						Map<String, Attribute> skuMap = new HashMap<>();
						skuMap = skuRecord.getAttributes();
						Attribute currentSkuColorCode = skuMap.get( DigitalEndecaConstants.COLOR_CODE );
						if (currentSkuColorCode == null) break; //I can't do anything if Color code is NULL for a SKU. This should not happen though. 
						prodRecord.getAttributes().put(DigitalEndecaConstants.PRODUCT_SELECTED_COLOR_CODE, currentSkuColorCode);
						//Now, we don't need these Faceout attributes we are getting from Endeca
						prodRecord.getAttributes().remove(DigitalEndecaConstants.FACEOUT_NONCLEARANCE_PROPERTY);
						prodRecord.getAttributes().remove(DigitalEndecaConstants.FACEOUT_CLEARANCE_PROPERTY);
					}
				}
			}
			//If user is NOT filtering on SKU level Dimension then apply our default logic for V2
			else {
				for( Record prodRecord : prodRecords ) {
					//If user is on Clearance page then set the "product.SelectedColorCode" to "product.faceOutClearance" property
					if (userIsOnClearancePage && prodRecord.getAttributes().get(DigitalEndecaConstants.FACEOUT_CLEARANCE_PROPERTY) != null)
						prodRecord.getAttributes().put(DigitalEndecaConstants.PRODUCT_SELECTED_COLOR_CODE, prodRecord.getAttributes().get(DigitalEndecaConstants.FACEOUT_CLEARANCE_PROPERTY));
					else //Else, set "product.selectedColorCode" to "product.faceOutNonClearance" property
						prodRecord.getAttributes().put(DigitalEndecaConstants.PRODUCT_SELECTED_COLOR_CODE, prodRecord.getAttributes().get(DigitalEndecaConstants.FACEOUT_NONCLEARANCE_PROPERTY));
					//Now, we don't need these Faceout attributes we are getting from Endeca
					prodRecord.getAttributes().remove(DigitalEndecaConstants.FACEOUT_NONCLEARANCE_PROPERTY);
					prodRecord.getAttributes().remove(DigitalEndecaConstants.FACEOUT_CLEARANCE_PROPERTY);
				}
			}			
		}
	}

	/** @param prodRecords
	 * @param isClearanceRequest
	 *            This updates the defualtColorcode as per Jira card KTLO1-1 and KTLO1-2 */
	public static void addSelectedColorCode( ArrayList<Record> prodRecords, boolean isClearanceRequest ) {
		
		if( null != prodRecords && prodRecords.size() > 0 ) {
			for( Record prodRecord : prodRecords ) {
				ArrayList<Record> skuRecords = (ArrayList<Record>)prodRecord.getRecords();
				float currentSkuInventory = 0;
				//DefaultColorCode is set to 001 as Default value in Custom Catalog. Not doing NULL check.
				Attribute defaultColorCode = prodRecord.getAttributes().get( DigitalEndecaConstants.PRODUCT_DEFAULT_COLOR_CODE );
				Attribute selectedColorCode = defaultColorCode;
				String skuOnClearance = DigitalEndecaConstants.ZERO;
				Map<Attribute, Float> colorInvMap = new HashMap<>();
				Map<Attribute, Float> nonClearanceMap = new HashMap<>();
				boolean gotoNextProduct = false;
				boolean nonClearanceMapExist = false;
				boolean checkInMap = false;
				for( Record skuRecord : skuRecords ) {
					Map<String, Attribute> skuMap = new HashMap<>();
					skuMap = skuRecord.getAttributes();
					currentSkuInventory = ( null == skuMap.get( DigitalEndecaConstants.SKU_INVENTORY ) ) ? 0 : Float.parseFloat( skuMap.get( DigitalEndecaConstants.SKU_INVENTORY ).toString() );
					Attribute currentSkuColorCode = skuMap.get( DigitalEndecaConstants.COLOR_CODE );
					if (currentSkuColorCode == null) continue; //Let's continue with next SKU if this SKU don't have a Color Code set. 
					skuOnClearance = ( null == skuMap.get( DigitalEndecaConstants.SKU_IS_CLEARANCE_ITEM ) ) ? DigitalEndecaConstants.ZERO : skuMap.get( DigitalEndecaConstants.SKU_IS_CLEARANCE_ITEM ).toString();

					InventoryClearanceCombo invClearanceCombo = inventoryClearanceLogicGate( skuOnClearance, currentSkuInventory );
					// if the request is coming from clearance page - need to consider clearance options.
					if( isClearanceRequest ) {
						float existingInventory = colorInvMap.get( currentSkuColorCode ) == null ? 0 : colorInvMap.get( currentSkuColorCode );
						float existingNonClearanceInventory = nonClearanceMap.get( currentSkuColorCode ) == null ? 0 : nonClearanceMap.get( currentSkuColorCode );
						// if the current sku color is same as default color
						if( DigitalStringUtil.equals( defaultColorCode.toString(), currentSkuColorCode.toString() ) ) {
							switch( invClearanceCombo ) {
							case BOTH_TRUE:
								// default color has inventory and is on clearance
								selectedColorCode = currentSkuColorCode;
								gotoNextProduct = true;
								checkInMap = false;
								break;
							case ONLY_INVENTORY:
								if( colorInvMap.containsKey( currentSkuColorCode ) ) {
									colorInvMap.put( currentSkuColorCode, existingInventory + currentSkuInventory );
									checkInMap = true;
								} else {
									nonClearanceMap.put( currentSkuColorCode, existingNonClearanceInventory + currentSkuInventory );
									nonClearanceMapExist = true;
								}
							default:
								continue;
							}
						} else {
							switch( invClearanceCombo ) {
							case BOTH_TRUE:
								colorInvMap.put( currentSkuColorCode, existingInventory + currentSkuInventory );
								checkInMap = true;
							case ONLY_INVENTORY:
								// sku has inventory but not on clearance
								if( colorInvMap.containsKey( currentSkuColorCode ) ) {
									colorInvMap.put( currentSkuColorCode, existingInventory + currentSkuInventory );
									checkInMap = true;
								} else {
									nonClearanceMap.put( currentSkuColorCode, existingNonClearanceInventory + currentSkuInventory );
									nonClearanceMapExist = true;
								}
							default:
								continue;
							}

						}
						// if default color code has inventory and is on clearance break out as we found the color selection
						if( gotoNextProduct ) break;
					} else {
						if( DigitalStringUtil.equals( defaultColorCode.toString(), currentSkuColorCode.toString() ) && currentSkuInventory > 0 ) {
							selectedColorCode = currentSkuColorCode;
							checkInMap = false;
							break;
						} else {
							float existingInventory = colorInvMap.get( currentSkuColorCode ) == null ? 0 : colorInvMap.get( currentSkuColorCode );
							colorInvMap.put( currentSkuColorCode, existingInventory + currentSkuInventory );
							checkInMap = true;
						}
					}

				}
				if( checkInMap ) {
					if( nonClearanceMapExist ) {
						for( Attribute nonClearanceAttr : nonClearanceMap.keySet() ) {
							if( colorInvMap.containsKey( nonClearanceAttr ) ) {
								colorInvMap.put( nonClearanceAttr, colorInvMap.get( nonClearanceAttr ) + nonClearanceMap.get( nonClearanceAttr ) );
							}
						}
					}
					selectedColorCode = getMaxInvColor( colorInvMap );
				}
				if( null != selectedColorCode && selectedColorCode.size() > 0 ) {
					prodRecord.getAttributes().put( DigitalEndecaConstants.PRODUCT_SELECTED_COLOR_CODE, selectedColorCode );
				}
			}
		}
		
	}
	
	//Get the Dimension Value Name by passing the Dimension ValueID
	//Also works with Dimension ID instead of Dimension Value ID
	public static String getDimensionValueNamefromID(Navigation nav, String dimValID) {
		//As of now, loop through only the Descriptor Dimensions to get the Dimension Name
		//This can be extended to look into all Refinement dimensions in case Descriptor Dimension fails to get the name
		String dimValName = null;
		DimensionList dimList = nav.getDescriptorDimensions();
		for (Object dim : dimList) {
			if (dim instanceof Dimension) {
				
				DimValList dimaValist = ((Dimension) dim).getCompletePath();
				for( int j = 0; j < dimaValist.size(); j++ ) {
					DimVal dimVal = (DimVal)dimaValist.get( j );						
					if(Long.toString(dimVal.getId()).equals(dimValID) ) {
						dimValName = dimVal.getName();
						return dimValName;
					}
				}
			}
		}
		return null;
	}

	private static Attribute getMaxInvColor( Map<Attribute, Float> colorInvMap ) {
		Map.Entry<Attribute, Float> maxEntry = null;
		for( Map.Entry<Attribute, Float> entry : colorInvMap.entrySet() ) {
			if( maxEntry == null || entry.getValue().compareTo( maxEntry.getValue() ) > 0 ) {
				maxEntry = entry;
			}
		}
		if (null!=maxEntry)
		{
			return maxEntry.getKey();
		}else
		{
			return null;
		}
		
	}

	// Added this logic here instead of if else in the method itself to accommodate future changes.
	private static InventoryClearanceCombo inventoryClearanceLogicGate( String skuOnClearance, float currentSkuInventory ) {
		if( DigitalStringUtil.equals( skuOnClearance, "1" ) && currentSkuInventory > 0 )
			return InventoryClearanceCombo.BOTH_TRUE;
		else if( !DigitalStringUtil.equals( skuOnClearance, "1" ) && currentSkuInventory > 0 )
			return InventoryClearanceCombo.ONLY_INVENTORY;
		else if( DigitalStringUtil.equals( skuOnClearance, "1" ) && !( currentSkuInventory > 0 ) )
			return InventoryClearanceCombo.ONLY_CLEARANCE;
		else if( !DigitalStringUtil.equals( skuOnClearance, "1" ) && !( currentSkuInventory > 0 ) ) return InventoryClearanceCombo.BOTH_FALSE;
		return InventoryClearanceCombo.UNDERTERMINED;
	}

	public static void setMethodHeaders( GetMethod method ) {
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		method.setRequestHeader( DigitalEndecaConstants.DSW_CLIENT_DEVICE_TYPE, request.getHeader( DigitalEndecaConstants.DSW_CLIENT_DEVICE_TYPE ) );
		method.setRequestHeader( DigitalEndecaConstants.APACHE_UNIQUE_ID, request.getHeader( DigitalEndecaConstants.APACHE_UNIQUE_ID ) );
		method.setRequestHeader( DigitalEndecaConstants.SOURCE_IP, request.getHeader( DigitalEndecaConstants.SOURCE_IP ) );
		method.setRequestHeader( DigitalEndecaConstants.CLIENT_SOURCE_IP, request.getHeader( DigitalEndecaConstants.CLIENT_SOURCE_IP ) );
		method.setRequestHeader( DigitalEndecaConstants.JSESSIONID, request.getCookieParameter( DigitalEndecaConstants.JSESSIONID ) );
	}
}
