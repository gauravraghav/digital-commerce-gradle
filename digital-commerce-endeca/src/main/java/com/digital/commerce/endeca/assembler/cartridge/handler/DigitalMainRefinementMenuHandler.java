package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.cartridge.support.NavigationStateSupport;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.RefinementMenu;
import com.endeca.infront.cartridge.RefinementMenuConfig;
import com.endeca.infront.cartridge.RefinementMenuHandler;
import com.endeca.infront.cartridge.model.Refinement;
import com.endeca.infront.cartridge.model.RefinementBreadcrumb;
import com.endeca.infront.cartridge.support.RefinementBuilder;
import com.endeca.infront.navigation.NavigationException;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.infront.navigation.request.RefinementMdexQuery;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValList;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.DimensionList;
import com.endeca.navigation.ENEQueryResults;

import atg.commerce.endeca.cache.DimensionValueCache;
import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalMainRefinementMenuHandler extends RefinementMenuHandler {

	private MdexRequest mdexRequest;

	private static final DigitalLogger		logger	= DigitalLogger.getLogger( DigitalMainRefinementMenuHandler.class );
	private DimensionValueCacheTools	dimensionValueCacheTools;
	private int							eneQueryTimeout;


	private String				timeout;

	private static final String	METHOD_NAME	= "process";


	/** Creates a query to the MDEX for the refinements of the Dimension
	 * specified by {@link RefinementMenuConfig#getDimensionId()}. If the
	 * dimension id is not specified, this CartridgeHandler will return null for
	 * its ContentItem. */
	@Override
	public void preprocess( RefinementMenuConfig cartridgeConfig ) throws CartridgeHandlerException {
		RefinementMdexQuery refinement = new RefinementMdexQuery();
		refinement.setDimensionId( cartridgeConfig.getDimensionId() );
		NavigationStateSupport.updateSortConfig( cartridgeConfig, refinement );
		refinement.setNumRefinements( Integer.valueOf( cartridgeConfig.getLimit() ) );
		refinement.setBoostedDvals( cartridgeConfig.getBoostRefinements() );
		refinement.setBuriedDvals( cartridgeConfig.getBuryRefinements() );
		refinement.setWhyPrecedenceRuleFiredEnabled( cartridgeConfig.isWhyPrecedenceRuleFired() );
		refinement.setBoostedDvals( cartridgeConfig.getBoostRefinements() );
		refinement.setBuriedDvals( cartridgeConfig.getBuryRefinements() );
		mdexRequest = createMdexRequest( getNavigationState().getFilterState(), refinement );
	}

	@Override
	public RefinementMenu process( RefinementMenuConfig cartridgeConfig ) throws CartridgeHandlerException {
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );

		ExecutorService executor = Executors.newCachedThreadPool();
		Callable<ENEQueryResults> task = new Callable<ENEQueryResults>() {
			@Override
			public ENEQueryResults call() throws CartridgeHandlerException {
				return executeMdexRequest(mdexRequest);
			}
		};

		Future<ENEQueryResults> future = null;
		try {
			future = executor.submit( task );
			ENEQueryResults results = future.get( eneQueryTimeout, TimeUnit.MILLISECONDS );

			long timeout = Long.parseLong( getTimeout() );
			getNavigationState().inform( results );
			if( results.getNavigation() == null ) {
				logger.warn( "No navigation object in eneResults" );
				return null;
			}

			// -------Hide filters based on business navigation state selection
			// in Top Nav and Footer ---STARTS HERE--------------
			// Do not fire the "Hide Filters" logic for Search Results page
			if( !DigitalStringUtil.equals( getNavigationState().getParameter( DigitalEndecaConstants.CARTRIDGE_PROP_PAGE_PATH ), DigitalEndecaConstants.BROWSE_PAGE_PATH ) ) {
				DimensionValueCache dimValCache = getDimensionValueCacheTools().getCache();
				getNavigationState().getFilterState().getNavigationFilters();
				List<String> navFilters = new ArrayList<>();
				String navFiltersAsString = "";				

				if( dimValCache.getNumCacheEntries() > 0 ) {					
					
					//Let's remove the user selected Filters from the Navigation Filters. Ideally this should exactly match with an entry in DimvalCache.
					//If in case no match found in DimvalCache then start removing from the end until you reach a base page
					navFiltersAsString = NavigationStateSupport.removeAlreadySelectedFilters(getNavigationState(), getNavigationState().getParameter("filter"));
					
					
					/* convert navReqFilter to a "+" delimited string
					for( int i = 0; i < navReqFilter.size(); i++ ) {
						navFiltersAsString = navFiltersAsString + navReqFilter.get( i ) + DigitalEndecaConstants.PLUS;
					}
					// remove the + at the end of the string if length > 0 /*
					if( !navFiltersAsString.isEmpty() ) navFiltersAsString = navFiltersAsString.substring( 0, navFiltersAsString.length() - 1 ); */

					// Attempt to get DimensionValueCacheObject from
					// DimensionValueCache using key = dimValID and key =
					// <<Site>>-dimValID
					// key = dimValID will satisfy for common dimension values
					// across sites
					// key = <<Site>>-dimValID will satisfy for this Site
					// specific dimension values (usually from Top nav and
					// Footer)
					// We will get DimensionValueCacheObject only if it exists
					// in the Cache. Otherwise NULL will be returned
					// If we got a match in Cache with URL=HIDEINREFINEMENT then
					// set this to navFilters.
					// navFilters will be used in the for loop below to remove
					// the current Refinement
					List<DimensionValueCacheObject> dimValCacheObject = dimValCache.get( navFiltersAsString );
					// use above dimValCacheObject if not null. Otherwise,
					// attempt to get dimValCacheObject using <<Site>>-dimValID
					// as key
					dimValCacheObject = dimValCacheObject != null ? dimValCacheObject : dimValCache.get( getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE ) + "-" + navFiltersAsString );

					// if Still dimValCacheObject is null then get into this
					// loop to remove the last filter and try again
					while( dimValCacheObject == null ) {
						// If no PLUS exists then this is the final Navigation
						// Filter. So, break the loop now
						if( !navFiltersAsString.contains( DigitalEndecaConstants.PLUS ) ) break;
						// Remove the last Navigation Filter (after the last
						// PLUS) and try again. This is required to reach the
						// base
						// navigation
						// states. Remove one filter at a time from the bottom
						// until you reach a base navigation state
						navFiltersAsString = navFiltersAsString.substring( 0, navFiltersAsString.lastIndexOf( DigitalEndecaConstants.PLUS ) );

						dimValCacheObject = dimValCache.get( navFiltersAsString );
						// use above dimValCacheObject if not null. Otherwise,
						// attempt to get dimValCacheObject using
						// <<Site>>-dimValID as
						// key
						dimValCacheObject = dimValCacheObject != null ? dimValCacheObject : dimValCache.get( getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE ) + "-" + navFiltersAsString );
					}

					if( dimValCacheObject != null ) { // Did we get an exact
														// match in Cache for
														// the Current
														// Navigation State?
						// Did that Cache object has "Hide Refinement" in the
						// URL? We are setting this in
						// DSWLinktoCacheHandler.java :)
						// This condition will satisfy only for Top Navigation
						// and Footer states.
						// Sizes or Brands or any other Dimensions set from
						// DimensionValueCacheRefreshConfig.properties won't
						// satisfy
						// There is no way we will get 2 Cache object for given
						// key in our case. So, let's take first one
						List<String> cacheProperties = dimValCacheObject.get( 0 ).getAncestorRepositoryIds();

						if( cacheProperties.contains( DigitalEndecaConstants.HIDEINREFINEMENT ) ) {

							// So, we got something exactly matched to the Cache
							// and it also came from Top Nav/Footer. Let's set
							// the
							// navFilters
							if( navFiltersAsString.contains( DigitalEndecaConstants.PLUS ) ) {
								String[] navId = navFiltersAsString.split( DigitalEndecaConstants.NVALUESEPARATOR );
								for( int k = 0; k < navId.length; k++ ) {
									navFilters.add( navId[k] );
								}
							} else {
								navFilters.add( navFiltersAsString );
							}

						}
					}
				} else {
					// getDimensionValueCacheTools().getCache() will
					// automatically fill the Cache if empty.
					// If still, Cache count is 0 then something is wrong. Raise
					// an exception.
					logger.error( "DimensionValueCache is empty even after force refreshing the cache. May be MDEX engine is down. Hide Filters and Clear all filters won't function properly" );
					throw new CartridgeHandlerException( "DimensionValueCache is empty. Hide Filters and Clear all filters won't function properly" );
				}

				// Now, check if current Refinement is part of the navFilters.
				// If yes, then return null (meaning, delete this refinement)
				DimensionList dimensionList = results.getNavigation().getDescriptorDimensions();
				for( int k = 0; k < navFilters.size(); k++ ) {
					long id = Long.parseLong( (String)navFilters.get( k ) );
					for( int i = 0; i < dimensionList.size(); i++ ) {
						Dimension dim = (Dimension)dimensionList.get( i );
						DimValList dimaValist = dim.getCompletePath();
						for( int j = 0; j < dimaValist.size(); j++ ) {
							DimVal dimVal = (DimVal)dimaValist.get( j );
							if( dimVal.getId() == id ) {
								long dimId = dimVal.getDimensionId();
								if( dimId == Long.parseLong( cartridgeConfig.getDimensionId() ) ) { return null; }
							}
							if(DigitalEndecaConstants.TRUE.equalsIgnoreCase(getNavigationState().getParameter(DigitalEndecaConstants.IS_SHOP_BY_SIZE)) && "gender".equalsIgnoreCase((String)cartridgeConfig.get("dimensionName"))){
								return null;
							}
						}
					}
				}
			}
			// -------Hide filters based on business navigation state selection
			// in Top Nav and Footer ---ENDS HERE--------------

			/* -------Logic to include Dimensions with ALL Dead Ends ---STARTS
			 * HERE-------------------------- Assume user filtered by Gender and
			 * Size. Assume "Trends" dimension has Dimension values that are all
			 * leading to DEAD END. In this case, Endeca won't send this
			 * Dimension as part of the results. But, this logic will add this
			 * "Trends" dimension and all of its dimension values with
			 * "allDeadEnd" flag set to "yes" */

			Dimension dimension = results.getNavigation().getRefinementDimensions().getDimension( Long.parseLong( cartridgeConfig.getDimensionId() ) );
			RefinementMenu outputModel = new RefinementMenu( cartridgeConfig );
			if( dimension == null ) {
				dimension = results.getNavigation().getDescriptorDimensions().getDimension( Long.parseLong( cartridgeConfig.getDimensionId() ) );
				if( dimension == null ) { return null; // This is not a Refinement dimension or Description Dimension. No need to process
														// this
				}
			}
			/*** PLAT-2451 - Comment out dead end logic Starts here -------------- **/
			/* if( dimension == null ) {
			 * dimension = results.getNavigation().getDescriptorDimensions().getDimension( Long.parseLong( cartridgeConfig.getDimensionId()
			 * ) );
			 * if( dimension == null ) {
			 * NavigationState navStateTmp = NavigationStateSupport.getNavigationStateForCall( getNavigationState(),
			 * DigitalEndecaConstants.ZERO, false, null );
			 * dimension = NavigationStateSupport.getDimensionByNavigationState( cartridgeConfig, navStateTmp, getMdexRequestBroker(),
			 * timeout );
			 * if( dimension != null ) {
			 * List<Refinement> deadEndRefinements = setDeadEndRefinements( dimension, cartridgeConfig );
			 * outputModel.setRefinements( deadEndRefinements );
			 * outputModel.put( DigitalEndecaConstants.ALL_DEAD_END, DigitalEndecaConstants.YES );
			 * return outputModel;
			 * } else {
			 * logger.warn( "Dimension is not present in Endeca application" );
			 * return null;
			 * }
			 * } else if( !dimension.getRoot().isMultiSelectOr() ) {
			 * logger.warn( "No dimesion presents" );
			 * return null;
			 * }
			 * } */
			/** PLAT-2451 - Comment out dead end logic Ends here -------------- ***/
			// -------Logic to include Dimensions with ALL Dead Ends ---ENDS HERE----------------------

			/*--------Logic to include selected dimension values with checked=true AND
			 * 		  Logic to include Dead End dimension values --------------------- STARTS HERE-------------
			 * 
			 */
			// Get dimensions that are applied as Filter
			DimensionList descDimensionsBC = results.getNavigation().getDescriptorDimensions();
			// Derive refinementcrumbs with selected Dimension values for this
			// Dimension.
			// This may be empty if no selected Dimension values available for
			// this Refinement or Dimension
			List<RefinementBreadcrumb> refinementcrumbs = NavigationStateSupport.createBreadcrumbsForDimension( dimension.getId(), descDimensionsBC, getActionPathProvider(), getNavigationState(), this.getSiteState());
			RefinementBuilder.populateRefinementMenuProperties( outputModel, cartridgeConfig, dimension, DigitalEndecaConstants.NAV_REFINEMENT_MENU_CONFIG, DigitalEndecaConstants.SHOW_MORE, getNavigationState(),this.getSiteState(), getActionPathProvider() );

			DimValList list = dimension.getRefinements();
			Map<String, Refinement> refinementActions = new HashMap<>();
			List<Refinement> refinements = new ArrayList<>(list.size());

			// loop through available dimension values for this Dimension and
			// build the refinements output.
			// This "refinements" will be used if the next "IF" condition
			// satisfies
			for( int j = 0; j < list.size() && j < cartridgeConfig.getLimit(); j++ ) {
				Refinement ref = RefinementBuilder.createRefinement( getActionPathProvider(), list.getDimValue( j ), getNavigationState() ,this.getSiteState() );
				ref.getProperties().put( DigitalEndecaConstants.DIM_ID_PROPERTY_NAME, Long.toString( ( (DimVal)list.getDimValue( j ) ).getId() ) );
				refinements.add( ref );
				refinementActions.put( Long.toString( ( (DimVal)list.getDimValue( j ) ).getId() ), ref );
			}
			// This IF condition satisfies if there are no filters applied to
			// this query OR
			// If this dimension is an AND dimension or If this dimension has
			// "NONE" selected for multi-select option.
			// Basically, this condition will satisfy for product.category
			if( descDimensionsBC == null || ( descDimensionsBC != null && descDimensionsBC.isEmpty() ) || !dimension.getRoot().isMultiSelectOr() ) {
				outputModel.setRefinements( refinements );
			} else if( !refinementcrumbs.isEmpty() ) { // PLAT-2451: Updated this ELSE-IF to fire only if there are selected refinements.
				// Most or All of the Dimensions in DSW Endeca app will come to
				// this ELSE (except product.category)
				// Dangerous: "getMultiSelectRefinements" is making call to
				// ENDECA to get all Dimension values for this dimension. This
				// is
				// done to handle DEAD END and to ensure selected refinements
				// are added in correct order to the Dimension
				// For example, if gender: Men is selected then Endeca will
				// remove the "Men" from Gender and add it to refinementcrumbs.
				// We
				// will add that "Men" back to Gender in
				// "getMultiSelectRefinements" + we will also ensure that "Men"
				// is added to the correct order to the Gender dimension (if Men
				// is second in Gender dimension then Men will be added as
				// second entry). There is really no easy way to achieve this in
				// Endeca. But, restrict this Endeca call to only those
				// selected Refinements
				List<Refinement> refinementsMulti = getMultiSelectRefinements( descDimensionsBC, cartridgeConfig, refinementcrumbs, refinementActions, timeout, getEneQueryTimeout() );
				NavigationState tmp = getNavigationState().clearFilterState();
				tmp = tmp.updateFilterState( getNavigationState().getFilterState().clone() );
				if( refinementsMulti == null ) {
					outputModel.put( DigitalEndecaConstants.ALL_DEAD_END, DigitalEndecaConstants.YES );
				} else if( !refinementsMulti.isEmpty() && cartridgeConfig.getLimit() > refinementsMulti.size() ) {
					outputModel.setMoreLink( null );
				}
				outputModel.setRefinements( refinementsMulti );
			} else { // If this is multi-or or multi-and dimension and user didn't select any of this dimension value for filtering then
						// this condition will satisfy
				outputModel.setRefinements( refinements );
			}
			return outputModel;

		} catch( NumberFormatException | NavigationException | InterruptedException | ExecutionException | TimeoutException ex ) {
			logger.error( "Exception thrown in DigitalMainRefinementMenuHandler", ex );
			throw new CartridgeHandlerException( ex );
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}
	}

	private List<Refinement> getMultiSelectRefinements( DimensionList descDimensionsBC, RefinementMenuConfig cartridgeConfig, List<RefinementBreadcrumb> refinementcrumbs, Map<String, Refinement> originalActions, long timeout, int eneQueryTimeout )
			throws CartridgeHandlerException, NavigationException {
		final String METHOD_NAME = "getMultiSelectRefinements";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		try {
			List<Refinement> refinements = new ArrayList<>();
			Map<String, RefinementBreadcrumb> idsContain = new HashMap<>();
			Object[] refinementcrumbsArray = refinementcrumbs.toArray();
			RefinementBreadcrumb refBread;
			if( refinementcrumbs != null && !refinementcrumbs.isEmpty() ) {
				for( Object refBreadObject : refinementcrumbsArray ) {
					refBread = (RefinementBreadcrumb)refBreadObject;
					if( refBread.getProperties().containsKey( DigitalEndecaConstants.DIM_ID_PROPERTY_NAME ) ) idsContain.put( refBread.getProperties().get( DigitalEndecaConstants.DIM_ID_PROPERTY_NAME ), refBread );
				}
			}

			String keepId = NavigationStateSupport.removeCategoryFromBreadcrumbs( descDimensionsBC );

			NavigationState navigationStateTemp = NavigationStateSupport.getNavigationStateForCall( getNavigationState(), DigitalEndecaConstants.ZERO, true, keepId );
			Dimension dimension = NavigationStateSupport.getDimensionByNavigationState( cartridgeConfig, navigationStateTemp, getMdexRequestBroker(), timeout, eneQueryTimeout );
			if( dimension == null ) {
				logger.warn( "No dimesion presents" );
				return null;
			}
			DimValList list = dimension.getRefinements();
			for( int j = 0, i = 0; j < list.size() && i < cartridgeConfig.getLimit(); j++ ) {
				Refinement ref = RefinementBuilder.createRefinement( getActionPathProvider(), list.getDimValue( j ), navigationStateTemp ,this.getSiteState() );
				ref.getProperties().put( DigitalEndecaConstants.DIM_ID_PROPERTY_NAME, Long.toString( ( (DimVal)list.getDimValue( j ) ).getId() ) );
				if( idsContain != null && idsContain.containsKey( Long.toString( ( (DimVal)list.getDimValue( j ) ).getId() ) ) ) {
					ref.getProperties().put( DigitalEndecaConstants.CHECKED, DigitalEndecaConstants.YES );
					RefinementBreadcrumb refBreadTmp = idsContain.get( Long.toString( ( (DimVal)list.getDimValue( j ) ).getId() ) );
					ref.setNavigationState( refBreadTmp.getRemoveAction().getNavigationState() );
					ref.setCount( refBreadTmp.getCount() );
				} else if( originalActions.containsKey( Long.toString( ( (DimVal)list.getDimValue( j ) ).getId() ) ) ) {
					Refinement refTmp = originalActions.get( Long.toString( ( (DimVal)list.getDimValue( j ) ).getId() ) );
					ref.setNavigationState( refTmp.getNavigationState() );
					ref.setCount( refTmp.getCount() );
				} else {
					ref.getProperties().put( DigitalEndecaConstants.DEAD_END, DigitalEndecaConstants.YES );
				}
				refinements.add( ref );
				i++;
			}
			return refinements;
		} catch( Exception e ) {
			logger.error( "DigitalMainRefinementMenuHandler exception", e );
			return null;
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}

	}

}
