package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.RefinementMenu;
import com.endeca.infront.cartridge.RefinementMenuConfig;
import com.endeca.infront.cartridge.RefinementMenuHandler;
import com.endeca.infront.cartridge.model.Refinement;
import com.endeca.infront.navigation.model.DvalSpec;

public class DigitalBrandsListHandler extends RefinementMenuHandler {
	
	private static final String METHOD_NAME = "process";

	@Override
	public RefinementMenu process(RefinementMenuConfig pCartridgeConfig)
			throws CartridgeHandlerException {
		
		RefinementMenu refinementMenu = null;
		try {
			
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME);
			refinementMenu = super.process(pCartridgeConfig);
			if (refinementMenu != null
					&& refinementMenu.getRefinements() != null) {
				List<Refinement> refinements = refinementMenu.getRefinements();
				@SuppressWarnings("unchecked")
				List<DvalSpec> starredRefinements = (List<DvalSpec>) pCartridgeConfig
						.get(DigitalEndecaConstants.STARRED_BRAND);
				Map<String, String> localMap = new HashMap<>();
				for (DvalSpec dvalSpec : starredRefinements) {
					localMap.put(dvalSpec.getName(), dvalSpec.getName());
				}
				if (starredRefinements != null && !starredRefinements.isEmpty()) {
					for (Refinement refinement : refinements) {
						if (localMap.get(refinement.getLabel()) != null) {
							refinement
									.getProperties()
									.put(DigitalEndecaConstants.REFINEMENT_STARRED_PROPERTY,
											DigitalEndecaConstants.TRUE);
						}
					}
				}
			}
		}

		finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this
					.getClass().getName(), METHOD_NAME);
		}

		return refinementMenu;
	}
}
