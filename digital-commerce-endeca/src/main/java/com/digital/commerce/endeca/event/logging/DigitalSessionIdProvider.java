package com.digital.commerce.endeca.event.logging;

import atg.servlet.ServletUtil;

import com.digital.commerce.common.logger.DigitalLogger;
import com.endeca.infront.assembler.event.request.SessionIdProvider;

/**
 * Class to return the current sessionId for the Endeca logserver requests
 * 
 * @author HB391569
 * @see SessionIdProvider
 * 
 */
public class DigitalSessionIdProvider implements SessionIdProvider {
	private static transient DigitalLogger LOG = DigitalLogger
			.getLogger(DigitalSessionIdProvider.class);

	@Override
	public String getSessionId() {
		try {
			if (null != ServletUtil.getCurrentRequest()) {
				return ServletUtil.getCurrentRequest().getSession().getId();
			}

		} catch (Exception ex) {
			LOG.error("Exception while getting the sessionId for the request ",
					ex);
		}
		return null;
	}

}
