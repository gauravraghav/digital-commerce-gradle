/**
 * 
 */
package com.digital.commerce.endeca.index.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import com.digital.commerce.endeca.inventory.vo.InventoryVO;

import atg.core.util.StringUtils;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.GenerativePropertyAccessor;
import atg.repository.search.indexing.IndexingOutputConfig;
import atg.repository.search.indexing.specifier.OutputProperty;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalProductInventoryAccessor extends GenerativePropertyAccessor {
	
	private static final String INVENTORY_LOW = "low";
	private static final String INVENTORY_HIGH = "high";
	private static final String INVENTORY_MEDIUM = "medium";
	private String endecaRecordProductRepositoryId;
	private String endecaRecordProductInventoryLevelName;

	private DataSource inventoryDataSource;

	private String productFullInventorySql;
	private String	productIdColumnName;
	private String	productInventoryColumnName;
	private String 	productClearanceInventoryColumnName;
	private String endecaRecordProductClearanceInventoryLevelName;
	private String endecaRecordProductInventoryTitle;
	private String daysAvailablePropertyName;

	@Override
	protected Map<String, Object> getPropertyNamesAndValues(Context pContext, RepositoryItem pProductRepositoryItem,
			String pPropertyName, PropertyTypeEnum pType, boolean pArg4) {
		Map<String, InventoryVO> fullProductInventoryMap = null;
		Map<String, Object> productInventoryMap = new HashMap<String, Object>();
		if(pProductRepositoryItem != null) {
			fullProductInventoryMap = processProductInventory(pContext);
			if (null != fullProductInventoryMap && fullProductInventoryMap.get(pProductRepositoryItem.getRepositoryId()) != null) {
				Integer daysAvailable = (Integer) pProductRepositoryItem.getPropertyValue(getDaysAvailablePropertyName());
				String stockLevel = fullProductInventoryMap.get(pProductRepositoryItem.getRepositoryId()).getStockLevel();
				productInventoryMap.put(getEndecaRecordProductRepositoryId(), fullProductInventoryMap.get(pProductRepositoryItem.getRepositoryId()).getProductId());
				productInventoryMap.put(getEndecaRecordProductInventoryLevelName(), fullProductInventoryMap.get(pProductRepositoryItem.getRepositoryId()).getStockLevel());
				productInventoryMap.put(getEndecaRecordProductClearanceInventoryLevelName(), fullProductInventoryMap.get(pProductRepositoryItem.getRepositoryId()).getStockLevelClearance());
				if (!StringUtils.isBlank(stockLevel) && Integer.valueOf(stockLevel) < 100) {
					productInventoryMap.put(getEndecaRecordProductInventoryTitle(), INVENTORY_LOW);
				} else if (daysAvailable > 300) {
					productInventoryMap.put(getEndecaRecordProductInventoryTitle(), INVENTORY_HIGH);
				} else {
					productInventoryMap.put(getEndecaRecordProductInventoryTitle(), INVENTORY_MEDIUM);
				}
			}
		}
		return productInventoryMap;
	}

	@Override
	public boolean ownsDynamicPropertyName(String paramString,
			IndexingOutputConfig paramIndexingOutputConfig,
			OutputProperty paramOutputProperty) {
		// TODO Auto-generated method stub
		return false;
	}

	public Map<String, InventoryVO> processProductInventory(Context pContext) {

		InventoryVO lInventoryVO;
		Map<String, InventoryVO> fullInventoryMap = new HashMap<String, InventoryVO>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;
		if( isLoggingInfo() ) {
			logInfo( "Starting product inventory extraction.");
		}
		try {
			synchronized (this) {
				Object contextObject = pContext.getGlobalAttribute("productInventory");
				if (contextObject == null) {
					connection = this.getInventoryDataSource().getConnection();
					statement = connection.prepareStatement( getProductFullInventorySql() );
					if( isLoggingInfo() ) {
						logInfo( "STARTING the execution of Inventory SQL query productFullInventorySQL at : " + new Timestamp( System.currentTimeMillis() ) );
					}
					results = statement.executeQuery();
					if( isLoggingInfo() ) {
						logInfo( "FINISHED the execution of Inventory SQL query productFullInventorySQL at : " + new Timestamp( System.currentTimeMillis() ) );
					}
					if( isLoggingInfo() ) {
						logInfo( "Inside processProductInventory. Classloader: " + this.getClass().getClassLoader() );
					}
					while( results.next() ) {
						if( isLoggingDebug() ) {
							logDebug( "::::::::: Fetching record for product id    :  " + results.getString(getProductIdColumnName()));
						}
						lInventoryVO = new InventoryVO();
						String productId = results.getString( getProductIdColumnName() );
						String stockLevel = results.getString( getProductInventoryColumnName() );
						String clearanceStockLevel = results.getString( getProductClearanceInventoryColumnName());
						lInventoryVO.setProductId(productId);
						lInventoryVO.setStockLevel(stockLevel);
						lInventoryVO.setStockLevelClearance(clearanceStockLevel);
						fullInventoryMap.put(productId, lInventoryVO);
					}
					pContext.setGlobalAttribute("productInventory", fullInventoryMap);
					if (isLoggingInfo()) {
						logInfo("Product Inventory Size MAP: " + fullInventoryMap.size());
					}
				}
			}
		} catch( SQLException sqlex ) {
			if( isLoggingError() ) {
				logError( "SQLException fetching product inventory in processProductInventory", sqlex );
			}
		} catch( Exception ex ) {
			if( isLoggingError() ) {
				logError( "General Exception in processProductInventory", ex );
			}
		} finally {
			if( results != null ) {
				try {
					results.close();
				} catch( Exception ex ) {
					if (isLoggingError()) {
						logError( "Exception while closing resultset from processProductInventory", ex );
					}
				}
			}
			if( statement != null ) {
				try {
					statement.close();
				} catch( Exception ex ) {
					if (isLoggingError()) {
						logError( "Exception while closing statement from processProductInventory", ex );
					}
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					if (isLoggingError()) {
						logError( "Exception while closing connection from processProductInventory", ex );
					}
				}
			}
		}
		return (Map<String, InventoryVO>)pContext.getGlobalAttribute("productInventory");
	}
}