package com.digital.commerce.endeca.index.accessor;

import atg.beans.DynamicPropertyDescriptor;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.MultiTranslationPropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;


public class LanguagePropertyAccessor
  extends MultiTranslationPropertyAccessorImpl
{
  protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType)
  {
    if (isLoggingDebug()) {
      pContext.getIndexingOutputConfig().logDebug("LanguagePropertyAccessor.getTextOrMetaPropertyValue(" + pItem + " , " + pPropertyName + ")");
    }
    DynamicPropertyDescriptor propDesc = null;
    try
    {
      propDesc = pItem.getItemDescriptor().getPropertyDescriptor(pPropertyName);
      
      MultiTranslationPropertyAccessorImpl.IndexingLanguageTranslation translationDescriptor = getCachedContextTranslationDescriptor(pContext, propDesc);
      if (translationDescriptor != null)
      {
        Object objResult = translationDescriptor.derivePropertyValue(pItem);
        if (isLoggingDebug()) {
          pContext.getIndexingOutputConfig().logDebug("The IndexingTranslationDescriptor returned " + objResult + " for " + pPropertyName + " of " + pItem + " using locale of " + pContext.getCurrentDocumentLocale());
        }
        return objResult;
      }
      if (isLoggingDebug()) {
        pContext.getIndexingOutputConfig().logDebug("No derived property descriptor for " + pPropertyName + " of " + pItem);
      }
    }
    catch (RepositoryException e)
    {
      pContext.getIndexingOutputConfig().logError(e);
    }
    return super.getTextOrMetaPropertyValue(pContext, pItem, pPropertyName, pType);
  }
  
  protected Object getDefaultPropertyValue(RepositoryItem pItem, String pPropertyName)
  {
    return pItem.getPropertyValue(pPropertyName + "Default");
  }
}