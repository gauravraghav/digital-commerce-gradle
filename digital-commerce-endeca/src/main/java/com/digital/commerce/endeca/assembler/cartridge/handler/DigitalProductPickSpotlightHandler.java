package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.RecordSpotlight;
import com.endeca.infront.cartridge.RecordSpotlightConfig;
import com.endeca.infront.cartridge.RecordSpotlightHandler;
import com.endeca.infront.cartridge.model.Attribute;
import com.endeca.infront.cartridge.model.Record;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalProductPickSpotlightHandler extends RecordSpotlightHandler{
	private static final String METHOD_NAME = "process";

	private String defaultSiteFilter;

	@Override
	public RecordSpotlight process(RecordSpotlightConfig cartridgeConfig) throws CartridgeHandlerException{
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), METHOD_NAME);
		 RecordSpotlight spotLight=super.process(cartridgeConfig);
		 if(null!=spotLight){
			 List<Record> aRecList=spotLight.getRecords();
			  List<Record> tempArecList= new ArrayList<>();
			  Iterator<Record> it = aRecList.iterator();
			  while (it.hasNext()) {
				  Record parentRec=it.next();
				  String recFilter=null;
				  Map<String,Attribute> aRecAttributes=parentRec.getAttributes();
				  Attribute<String> atr= new Attribute<>();
				  StringBuilder prodFilter=new StringBuilder();
				  List<Record> eRecList=parentRec.getRecords();
				  Iterator<Record> recIt = eRecList.iterator();
				  while (recIt.hasNext()) {
					  Record childRec=recIt.next();
					  Map<String,Attribute> recAttributes=childRec.getAttributes();
					  StringBuilder filter=new StringBuilder();
					  Set<String> attributeSet=recAttributes.keySet();
					  Iterator<String> iter = attributeSet.iterator();
					  while (iter.hasNext()) {
						    String key=iter.next();
						    String value=(recAttributes.get(key)).toString();
						    if(DigitalEndecaConstants.PRODUCT_WEB_TYPE.equalsIgnoreCase(key)){
						    	filter.append(key+DigitalEndecaConstants.COLON+value+DigitalEndecaConstants.COMMA);
						    }
						    if(DigitalEndecaConstants.GENDER.equalsIgnoreCase(key)){
						    	filter.append(key+DigitalEndecaConstants.COLON+value+DigitalEndecaConstants.COMMA);
						    }
						    if(DigitalEndecaConstants.PRODUCT_PRICE_BUCKET.equalsIgnoreCase(key)){
						    	filter.append(key+DigitalEndecaConstants.COLON+value+DigitalEndecaConstants.COMMA);
						    }						    
					  }
					  if(filter.length()>0){
						  recFilter=filter.toString();
						  (filter.deleteCharAt(filter.length()-1)).toString();
					  }
				  }	
				  if(recFilter!=null){
					  prodFilter.append(DigitalEndecaConstants.NR_AND_PARANTHESIS);
					  prodFilter.append(recFilter);
					  prodFilter.append(getDefaultSiteFilter());
					  prodFilter.append(DigitalEndecaConstants.RIGHTPARANTHESIS);
					  atr.add(prodFilter.toString());
					  aRecAttributes.put(DigitalEndecaConstants.FILTER, atr);
				  }
				  parentRec.setAttributes(aRecAttributes);
				  tempArecList.add(parentRec);
			  }			  
			  spotLight.setRecords(tempArecList); 
		 }		  
		  return spotLight;
	  }

}
