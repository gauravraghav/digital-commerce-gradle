package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.servlet.ServletUtil;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.cartridge.support.NavigationStateSupport;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.digital.commerce.endeca.assembler.util.DigitalEndecaUtils;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.Breadcrumbs;
import com.endeca.infront.cartridge.BreadcrumbsConfig;
import com.endeca.infront.cartridge.BreadcrumbsHandler;
import com.endeca.infront.cartridge.model.NavigationAction;
import com.endeca.infront.cartridge.model.RefinementBreadcrumb;
import com.endeca.infront.cartridge.model.SearchBreadcrumb;
import com.endeca.infront.cartridge.support.BreadcrumbBuilder;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.request.BreadcrumbsMdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.ENEQueryResults;

import atg.commerce.endeca.cache.DimensionValueCache;
import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalBreadcrumbsHandler extends BreadcrumbsHandler {

	private static final String	METHOD_NAME	= "process";
	private DimensionValueCacheTools	dimensionValueCacheTools;
	private static final DigitalLogger		logger	= DigitalLogger.getLogger( DigitalBreadcrumbsHandler.class );
	private MdexRequest mMdexRequest;
	

	@Override
	public void preprocess( BreadcrumbsConfig cartridgeConfig ) throws CartridgeHandlerException {
		this.mMdexRequest = createMdexRequest(getNavigationState()
				.getFilterState(), new BreadcrumbsMdexQuery());
	}

	@Override
	public Breadcrumbs process( BreadcrumbsConfig cartridgeConfig ) throws CartridgeHandlerException {
		
		Breadcrumbs breadcrumbs = new Breadcrumbs(cartridgeConfig);
		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
			ENEQueryResults results = executeMdexRequest(this.mMdexRequest);
			NavigationState navigationState = getNavigationState();
			navigationState.inform(results);
			
			BreadcrumbBuilder.createAllBreadcrumbs(breadcrumbs, results.getNavigation(), navigationState, this.getSiteState(),getActionPathProvider());

			// If Browse page then check if the search keyword got auto-corrected. If yes, then add that corrected word to request
			// This corrected word will be used by NavigationStateSupport.shouldUseFaceoutfromChildSKU
			// Skip all other logic below which are for Category pages only
			if( DigitalStringUtil.equals( getNavigationState().getParameter( DigitalEndecaConstants.CARTRIDGE_PROP_PAGE_PATH ), DigitalEndecaConstants.BROWSE_PAGE_PATH ) ) {
				if (breadcrumbs.getSearchCrumbs().size() > 0 ) {
					SearchBreadcrumb searchBreadCrumb = breadcrumbs.getSearchCrumbs().get(0); // We are using only one keyword for search
					if (DigitalStringUtil.isNotBlank(searchBreadCrumb.getCorrectedTerms())) {
						ServletUtil.getCurrentRequest().setParameter(DigitalEndecaConstants.CORRECTED_SEARCH_KEYWORD, searchBreadCrumb.getCorrectedTerms());
					}
				}
				return breadcrumbs;
			}

			DimensionValueCache dimValCache = getDimensionValueCacheTools().getCache();
			getNavigationState().getFilterState().getNavigationFilters();
			NavigationState navState = getNavigationState().clearFilterState();
			List<String> navFilters = new ArrayList<>();
			String navFiltersAsString = "";
			String basePageH1Tag = "";
			String baseTitleTag = "";
			String baseMetaDescription = "";

			if( dimValCache.getNumCacheEntries() > 0 ) {
				
				//Let's remove the user selected Filters from the Navigation Filters. Ideally this should exactly match with an entry in DimvalCache.
				//If in case no match found in DimvalCache then start removing from the end until you reach a base page
				navFiltersAsString = NavigationStateSupport.removeAlreadySelectedFilters(getNavigationState(), getNavigationState().getParameter("filter"));

				// Start with Current Filter state. If perfect match doesn't exist in the Cache then remove the last filter state and try to
				// find exact match with Cache. If perfect match was found then "while condition" won't satisfy and navFiltersAsString has the
				// Clear all navigation state
				//First, try to get from Dimvalcache using <<Site>>-dimValID as key. We are doing this because we have product.price used as base page in Top Navigation
				//and also we are adding all product.price to DimvalCache. Becasue of this we are having two entires for "Under $100" (as an example) one with DSW-102772
				//and another one with "102772". The one with "DSW-102772" is used for H1 Tag. If we search for "102772" first instead of "DSW-102772" then we will get the 
				//DimValCache object for "102772" which doesn't have the "Hide Refinement" property. So, we won't be populating the H1 Tag label in this case.
				List<DimensionValueCacheObject> dimValCacheObject = dimValCache.get( getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE ) + "-" + navFiltersAsString ) ;
				// use above dimValCacheObject if not null. Otherwise, attempt to get dimValCacheObject using 
				dimValCacheObject = dimValCacheObject != null ? dimValCacheObject : dimValCache.get( navFiltersAsString );

				while( dimValCacheObject == null ) {
					// If no PLUS exists then this is the final Navigation Filter. So, break the loop now
					if( !navFiltersAsString.contains( DigitalEndecaConstants.PLUS ) ) {
						// If I blank this out then UI will go to 404 page. Instead, I am defaulting Clear all filter to first navigation state
						break;
					}
					// Remove the last Navigation Filter (after the last PLUS)
					navFiltersAsString = navFiltersAsString.substring( 0, navFiltersAsString.lastIndexOf( DigitalEndecaConstants.PLUS ) );

					dimValCacheObject = dimValCache.get( navFiltersAsString );
					// use above dimValCacheObject if not null. Otherwise, attempt to get dimValCacheObject using <<Site>>-dimValID as key
					dimValCacheObject = dimValCacheObject != null ? dimValCacheObject : dimValCache.get( getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE ) + "-" + navFiltersAsString );
				}

				// If more than 1 navigation filters exists then split and create the list. Else, just set the navFilters
				if( navFiltersAsString.contains( DigitalEndecaConstants.PLUS ) ) {
					String[] navId = navFiltersAsString.split( DigitalEndecaConstants.NVALUESEPARATOR );
					for( int k = 0; k < navId.length; k++ ) {
						navFilters.add( navId[k] );
					}
				} else if( !navFiltersAsString.isEmpty() ) {
					navFilters.add( navFiltersAsString );
				}
				
				if (dimValCacheObject != null) { //If we have a successful Dim Val Cache then get the Base Dimension Name
					//There is no way we will have 2 dim val cache objects. So, don't worry, take the first one.
					List<String> cacheProperties = dimValCacheObject.get( 0 ).getAncestorRepositoryIds();
					
					//Get the Top Navigation or Footer Page Name
					if (!cacheProperties.isEmpty() && cacheProperties.contains( DigitalEndecaConstants.HIDEINREFINEMENT ))
						basePageH1Tag = cacheProperties.get(0); //First cache property is always the Name
					
					//Get the Title Tag and Meta Description defined in LinkTextHeader cartridge
					if (cacheProperties.size() > 3) {
						baseTitleTag = cacheProperties.get(2);
						baseMetaDescription = cacheProperties.get(3);
					}
					
					//If basePageH1Tag is still blank then see if it's a Brand page then take the Name of the Brand Name. 
					//For all other Base pages, we are not going to give the H1 tag name as Business is going to set those in XM as textbreadcrumb override.
					if (DigitalStringUtil.isBlank(basePageH1Tag) && DigitalStringUtil.contains(dimValCacheObject.get(0).getDimvalId(), DigitalEndecaConstants.FIND_BRAND) ) { 
						basePageH1Tag = cacheProperties.get(0);
						baseTitleTag = DigitalStringUtil.isBlank(baseTitleTag) ? basePageH1Tag : baseTitleTag;
					}
				}
				
			} else {
				// getDimensionValueCacheTools().getCache() will automatically fill the Cache if empty.
				// If still, Cache count is 0 then something is wrong. Raise an exception.
				logger.error( "DimensionValueCache is empty even after force refreshing the cache. May be MDEX engine is down. Hide Filters and Clear all filters won't function properly" );
				throw new CartridgeHandlerException( "DimensionValueCache is empty. Hide Filters and Clear all filters won't function properly" );
			}

			navState.getFilterState().setNavigationFilters( navFilters );
			NavigationAction removeAllAction = new NavigationAction( navState.toString() );			
						
			//Set H1Tag, Title Tag and Meta Description from LinkTextHeader Cartridge to removeAllAction.label
			removeAllAction.setLabel(basePageH1Tag + "~" + baseTitleTag + "~" + baseMetaDescription);
			
			breadcrumbs.setRemoveAllAction( removeAllAction );
			
			//Add isBase flag to let UI know these filters are base Filters for the page. Base filters are the filters needed for the base page (From top nav, footer, brand pages)
			List<RefinementBreadcrumb> allBreadcrumbs = breadcrumbs.getRefinementCrumbs();
			for (String baseNavID : navFilters) {
				//Get the Baes Dimension Value Name from the Base Dimension Value ID
				String dimValName = DigitalEndecaUtils.getDimensionValueNamefromID(results.getNavigation(), baseNavID);			

				//Loop through each Breadcrumb and ensure "isBase" property is added for Base Dimension Value Names
				for (RefinementBreadcrumb thisBreadCrumb : allBreadcrumbs) {
					if (thisBreadCrumb.getLabel().equals(dimValName)) {
						Map<String, String> properties = new HashMap<>();
						properties.put("isBase", "1");
						thisBreadCrumb.setProperties(properties);
						break;
					}						
				}
				
				
			}
			
			//Never show "Shoe" in the breadcrumbs irrespective of whatever logic satisfies above.
			for (RefinementBreadcrumb thisBreadCrumb : allBreadcrumbs) {
				if (DigitalStringUtil.isNotEmpty(thisBreadCrumb.getLabel())  && (thisBreadCrumb.getLabel().equalsIgnoreCase(DigitalEndecaConstants.PRODUCT_WEBTYPE_SHOE) || thisBreadCrumb.getLabel().equalsIgnoreCase(DigitalEndecaConstants.ISCLEARANCE_YES))) {
					Map<String, String> properties = new HashMap<>();
					properties.put("isBase", "1");
					thisBreadCrumb.setProperties(properties);
					break;
				}						
			}
			
		}
		catch( Exception e ) {
			logger.error( "DigitalTopNavigationListHandler Size exception", e );
		}
		finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}
		return breadcrumbs;		
		
	}

}
