package com.digital.commerce.endeca.index.accessor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



import com.digital.commerce.common.util.DigitalStringUtil;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

/**
 * Convert the given "Set" Property to a comma separated string
 * @author PR412029
 *
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class ColorSwatchDisplayNamePropertyAccessor extends PropertyAccessorImpl {

    protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
                                                PropertyTypeEnum pType) {

        Set<String> allColorCodes = new HashSet<>();

        Object allColorCodesObj = pItem.getPropertyValue(pPropertyName);
        if (null != pItem && null != allColorCodesObj) {
            allColorCodes = (Set) allColorCodesObj;
            return DigitalStringUtil.join(allColorCodes, "|");
        }
        return null;
    }
}
