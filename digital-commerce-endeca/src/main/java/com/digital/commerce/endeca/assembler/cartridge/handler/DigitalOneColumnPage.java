package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.json.JSONObject;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalCommonUtil;
import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.digital.commerce.endeca.assembler.util.DigitalEndecaUtils;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.cartridge.ResultsList;
import com.endeca.infront.content.support.ConfigNodeContentItem;
import com.endeca.infront.navigation.NavigationState;

import atg.json.JSONException;
import atg.rest.input.JSONInputCustomizer;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import lombok.Getter;
import lombok.Setter;
@SuppressWarnings({"rawtypes","unchecked"})
@Getter
@Setter
public class DigitalOneColumnPage extends NavigationCartridgeHandler<ConfigNodeContentItem, ContentItem>{
	
	private static DigitalLogger logger = DigitalLogger
			.getLogger(DigitalOneColumnPage.class);

	private int siteHttpsServerPort;
	private String browsePagePath;
	private boolean httpsEnabled;
	private String siteHttpServerName;
	private int siteHttpServerPort;
	private String contentPath;
	private String serviceUrl;
	private String soTimeout;
	private int bufferSize;
	private int retries = 0;
	private String	connectionTimeout;
	private NavigationState navigationState;

	@Override
	public void preprocess(ConfigNodeContentItem pContentItemX) throws CartridgeHandlerException{
		if(getNavigationState()!=null  && null != getNavigationState().getParameter(DigitalEndecaConstants.SKIPHEADERFOOTER)){
			if(DigitalEndecaConstants.TRUE.equalsIgnoreCase(getNavigationState().getParameter(DigitalEndecaConstants.SKIPHEADERFOOTER))){
				pContentItemX.remove(DigitalEndecaConstants.HEADERCONTENT);
				pContentItemX.remove(DigitalEndecaConstants.FOOTERCONTENT);
			}
		}
		super.preprocess(pContentItemX);
	}

	@Override
	 public ContentItem process(ConfigNodeContentItem pContentItem) throws CartridgeHandlerException{
			
			final DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();

			/*
			 * This 'if' check makes sure that zero results check is done only in
			 * following cases : 
			 *   1. user has searched for something to make sure this is not done for the category pages, AND
			 *   2. Already null search calculation is not done, AND
			 *   3. Nrs parameter with storeLocation is not in the request (this means store refinement done on search results - "for need it today") 
			 */
				if ((DigitalStringUtil.equals(getNavigationState().getParameter(DigitalEndecaConstants.CARTRIDGE_PROP_PAGE_PATH),DigitalEndecaConstants.BROWSE_PAGE_PATH) && 
						(null == request.getParameter(DigitalEndecaConstants.REQUEST_FOR_NSRP) || !DigitalStringUtil.equals(request.getParameter(DigitalEndecaConstants.REQUEST_FOR_NSRP),DigitalEndecaConstants.STRING_TRUE)) 
						&& (null == request.getParameter(DigitalEndecaConstants.NRS) || !request.getParameter(DigitalEndecaConstants.NRS).contains(DigitalEndecaConstants.STORE_LOCATIONS)))) {
				ArrayList<ConfigNodeContentItem> contentList = (ArrayList<ConfigNodeContentItem>) pContentItem.get(DigitalEndecaConstants.CARTRIDGE_PROP_MAIN_CONTENT);
				Iterator<ConfigNodeContentItem> iterator = contentList.iterator();
				while (iterator.hasNext()) {
					Object contentItem = iterator.next();
					if (contentItem != null && (contentItem instanceof ConfigNodeContentItem)) {
						ConfigNodeContentItem tempItem = (ConfigNodeContentItem) contentItem;
						if(null != tempItem && null != tempItem.get(DigitalEndecaConstants.NAME) && DigitalStringUtil.equals(tempItem.get(DigitalEndecaConstants.NAME).toString(),DigitalEndecaConstants.SEARCH_ADJUSTMENT)){
							if(null!=tempItem.get(DigitalEndecaConstants.CARTRIDGE_PROP_CONTENTS)){
								Object searchAdjustmentContents= ((ArrayList)tempItem.get(DigitalEndecaConstants.CARTRIDGE_PROP_CONTENTS)).get(0);
								if (null!= (Map)searchAdjustmentContents && null != ((Map)searchAdjustmentContents).get(DigitalEndecaConstants.SUGGESTED_SEARCHES)){
									break;
								}
							}
						}
						if(null != tempItem.get(DigitalEndecaConstants.CARTRIDGE_PROP_CONTENTS) && !((ArrayList)tempItem.get(DigitalEndecaConstants.CARTRIDGE_PROP_CONTENTS)).isEmpty()){
							Object contentsObj = ((ArrayList<ConfigNodeContentItem>) tempItem.get(DigitalEndecaConstants.CARTRIDGE_PROP_CONTENTS)).get(0);
							// we need to check if the resultsList is getting zero results to make a call to NSRP
							if (contentsObj instanceof ResultsList) {
								ResultsList contents = (ResultsList) contentsObj;
								if ((Long) contents.get(DigitalEndecaConstants.CARTRIDGE_PROP_TOTAL_NUM_RECS) == DigitalEndecaConstants.ZERO_RESULTS_COUNT) { 
									String queryString = null;
								 queryString = DigitalEndecaConstants.BLANK;
								 String queryParamStr = getNavigationState().getCanonicalLink().substring(1);
								 Map<String,String> paramMap = DigitalCommonUtil.stringToMap(queryParamStr,DigitalEndecaConstants.AMPERSAND,DigitalEndecaConstants.EQUALTO);
								 paramMap.remove(DigitalEndecaConstants.NTT);
								 paramMap.remove(DigitalEndecaConstants.CARTRIDGE_PROP_CONTENTS_PATH);
								 paramMap.remove(DigitalEndecaConstants.CARTRIDGE_PROP_PAGE_PATH);
								 paramMap.remove(DigitalEndecaConstants.QUERY_TIER);
								 paramMap.put(DigitalEndecaConstants.REQUEST_FOR_NSRP,DigitalEndecaConstants.STRING_TRUE);
								 String contentPathUrl =  DigitalEndecaUtils.generateContentPathUrl(isHttpsEnabled(), getSiteHttpServerName(), getSiteHttpServerPort(), getServiceUrl(), queryString, getContentPath(), paramMap);
								 
								 HttpClient client = new HttpClient();
								if(retries > 0) {
									client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(retries, true));
								} else {
									client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(retries, false));
								}
								HttpConnectionManager connectionManager = client.getHttpConnectionManager();
								if( connectionManager != null ) {
									HttpConnectionManagerParams managerParams = connectionManager.getParams();
									if( managerParams != null ) {
										managerParams.setParameter( HttpConnectionParams.CONNECTION_TIMEOUT, Integer.parseInt( getConnectionTimeout() ) * 1000 );
										managerParams.setParameter( HttpConnectionParams.SO_TIMEOUT, Integer.parseInt( getSoTimeout() ) * 1000 );
									}
								}

								 GetMethod method = new GetMethod(contentPathUrl);
								 DigitalEndecaUtils.setMethodHeaders( method );
								 
								 try {
									 client.executeMethod(method);
								} catch (HttpException httpException) {
									logger.error("Http Exception  ::::::\n   ", httpException);
								} catch (IOException ioException) {
									logger.error("IO Exception  ::::::\n   ", ioException);
								}
								 String response = getResponseBody(method);
								 JSONObject finalJson = JSONInputCustomizer.isValidJSONString(response);
								
								 Map<String, Object> responseMap = null;
								try {
									responseMap = DigitalCommonUtil.jsonToMap(finalJson);
								} catch (org.json.JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								 pContentItem.clear();
								 pContentItem.putAll(responseMap);
								 return pContentItem;
								 }
							}
						}
					}
				}
			}
			return pContentItem;
	}	 

	private String getResponseBody(GetMethod method) {
			if (method != null && method.hasBeenUsed()) {
				StringWriter stringOut = new StringWriter();
				try (BufferedWriter dumpOut = new BufferedWriter(stringOut,
						bufferSize);
						BufferedReader in = new BufferedReader(
								new InputStreamReader(
										method.getResponseBodyAsStream()))) {
					String line = "";
					while ((line = in.readLine()) != null) {
						dumpOut.write(line);
						dumpOut.newLine();
					}
				} catch (IOException e) {
					logger.error("DigitalOneColumnPage IO exception", e);
				}

				return stringOut.toString();
			}
			return null;
		}

		@Override
		protected ConfigNodeContentItem wrapConfig(ContentItem paramContentItem) {
			return (ConfigNodeContentItem) paramContentItem;
		}
}
