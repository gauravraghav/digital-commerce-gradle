package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.DimensionSearchResults;
import com.endeca.infront.cartridge.DimensionSearchResultsConfig;
import com.endeca.infront.cartridge.DimensionSearchResultsHandler;
import com.endeca.infront.cartridge.support.DimensionSearchResultsBuilder;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.SearchFilter;
import com.endeca.infront.navigation.request.DimensionSearchMdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.ENEQueryResults;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalDimensionSearchResultsHandler extends DimensionSearchResultsHandler {
	private static final DigitalLogger logger = DigitalLogger.getLogger(DigitalDimensionSearchResultsHandler.class);

	private MdexRequest mMdexRequest;
	private int eneQueryTimeout;

	@Override
	public void preprocess(DimensionSearchResultsConfig cartridgeConfig) throws CartridgeHandlerException {
		NavigationState navigationState = getNavigationState();
		SearchFilter searchFilter = getDimensionSearchFilter(navigationState);
		if (cartridgeConfig.isEnabled() && searchFilter != null) {
			NavigationState dimNavState = createDimensionSearchNavigationState(navigationState);
			NavigationState dimNavStateFinal = dimNavState.augment(dimNavState.getFilterState().clone());
			dimNavStateFinal.getFilterState().setRecordFilters(null);
			mMdexRequest = createMdexRequest(dimNavStateFinal.getFilterState(),
					buildMdexQuery(cartridgeConfig, searchFilter));
		}
	}

	@Override
	public DimensionSearchResults process(DimensionSearchResultsConfig cartridgeConfig)
			throws CartridgeHandlerException {
		if (this.mMdexRequest != null) {

			ExecutorService executor = Executors.newCachedThreadPool();
			Callable<ENEQueryResults> task = new Callable<ENEQueryResults>() {
				@Override
				public ENEQueryResults call() throws CartridgeHandlerException {
					return executeMdexRequest(mMdexRequest);
				}
			};

			Future<ENEQueryResults> future = null;
			try {
				future = executor.submit(task);
				ENEQueryResults results = future.get(eneQueryTimeout, TimeUnit.MILLISECONDS);

				NavigationState navigationState = getNavigationState();
				navigationState.inform(results);

				DimensionSearchResults dimensionSearchResults = new DimensionSearchResults(cartridgeConfig);
				DimensionSearchResultsBuilder.build(getActionPathProvider(), dimensionSearchResults, navigationState,
						this.getSiteState(),results.getDimensionSearch(), cartridgeConfig.getDimensionList(),
						cartridgeConfig.getMaxResults(), cartridgeConfig.isShowCountsEnabled());

				return dimensionSearchResults;
			} catch (InterruptedException | ExecutionException | TimeoutException ex) {
				// ExecutionException will be the wrapped ENE exception
				logger.error("Exception while reading the dimension value from MDEX response", ex);
				return null;
			}
		}
		return null;
	}

	private static DimensionSearchMdexQuery buildMdexQuery(DimensionSearchResultsConfig itemConfig, SearchFilter search)
			{
		DimensionSearchMdexQuery query = new DimensionSearchMdexQuery();
		if (itemConfig.getDimensionList() != null && !itemConfig.getDimensionList().isEmpty()) {
			query.setDimensionValues(itemConfig.getDimensionList());
		}
		query.setShowCountsEnabled(itemConfig.isShowCountsEnabled());
		if (itemConfig.getRelRankStrategy() == null
				|| DigitalEndecaConstants.BLANK.equals(itemConfig.getRelRankStrategy())) {
			query.setRelRankStrategy(null);
		} else {
			query.setRelRankStrategy(itemConfig.getRelRankStrategy());
		}
		if (search != null) {
			query.setTerms(search.getTerms());
		}
		query.setMaxDvalsPerDimension(itemConfig.getMaxResultsPerDimension());
		return query;
	}

}
