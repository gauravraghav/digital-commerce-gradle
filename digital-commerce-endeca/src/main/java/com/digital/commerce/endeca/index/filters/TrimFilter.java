package com.digital.commerce.endeca.index.filters;

import atg.nucleus.GenericService;
import atg.repository.search.indexing.PropertyValuesFilter;
import atg.repository.search.indexing.ValueAndSecurity;

import java.util.LinkedList;
import java.util.List;

public class TrimFilter  extends GenericService implements PropertyValuesFilter {

    @Override
    public boolean applyAfterFormatting() {
        return false;
    }

    @Override
    public ValueAndSecurity[] filter(String s, ValueAndSecurity[] valueAndSecurities) {
        if (valueAndSecurities == null || valueAndSecurities.length == 0) return valueAndSecurities;
        List<ValueAndSecurity> results = new LinkedList<>();

        for (ValueAndSecurity value : valueAndSecurities) {
            /**
             * Filter out empty values
             */
            if (value.getValue() == null) continue;
            /**
             * Just take the value, convert to string and apply the trim method.
             */
            String newValue = value.getValue().toString().trim();
            if(isLoggingDebug()){
                if(!newValue.equals(value.getValue().toString())){
                    logDebug("Trim Transformer changed ["+value.getValue().toString()+"] into ["+newValue+"]");
                }
            }
            results.add(new ValueAndSecurity(newValue,value.getSecurityConstraints(),value.getScheme()));

        }
        return (ValueAndSecurity[])results.toArray(new ValueAndSecurity[results.size()]);
    }
}
