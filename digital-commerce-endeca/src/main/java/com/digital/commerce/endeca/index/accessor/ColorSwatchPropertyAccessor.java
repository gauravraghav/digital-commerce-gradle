package com.digital.commerce.endeca.index.accessor;

import java.util.HashSet;
import java.util.Set;



import com.digital.commerce.common.util.DigitalStringUtil;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

/**
 * Convert the given "Set" Property to a comma separated string
 * @author PR412029
 *
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class ColorSwatchPropertyAccessor extends PropertyAccessorImpl {

	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
			PropertyTypeEnum pType) {

		Set<String> allColorCodes = new HashSet<>();
		Set<String> outputColorCodes = new HashSet<>();

		Object allColorCodesObj = pItem.getPropertyValue(pPropertyName);
		if (null != pItem && null != allColorCodesObj) {
			allColorCodes = (Set) allColorCodesObj;

			for (String color : allColorCodes) {
				if (color.contains("~"))
					outputColorCodes.add(color.split("~")[0]);
			}

			return DigitalStringUtil.join(outputColorCodes, ",");
		}
		return null;
	}
}
