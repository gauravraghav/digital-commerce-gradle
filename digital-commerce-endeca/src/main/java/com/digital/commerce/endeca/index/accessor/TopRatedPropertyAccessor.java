package com.digital.commerce.endeca.index.accessor;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopRatedPropertyAccessor extends PropertyAccessorImpl {
	private static final String RATING = "rating";
	private static final String REVIEWCOUNT = "reviewCount";
	private static final String ISTOPRATED = "yes";
	private static final String ISNOTTOPRATED = "no";
	private static final Double ZERODOUBLEVALUE = 0.0;
	private static final int ZEROINTEGERVALUE = 0;

	private Double minAvgRating;
	private int minRreviewCount;

	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
			PropertyTypeEnum pType) {
		Object topRatedProperty = TopRatedPropertyAccessor.ISNOTTOPRATED;
		Double avgRating = TopRatedPropertyAccessor.ZERODOUBLEVALUE;
		int reviewCount = TopRatedPropertyAccessor.ZEROINTEGERVALUE;
		Object ratingObj = pItem.getPropertyValue(TopRatedPropertyAccessor.RATING);
		Object reviewCountObj = pItem.getPropertyValue(TopRatedPropertyAccessor.REVIEWCOUNT);
		if (null != pItem && null != ratingObj
				&& null != reviewCountObj) {
			avgRating = (Double) ratingObj;
			reviewCount = (Integer) reviewCountObj;
			if (avgRating >= getMinAvgRating() && reviewCount >= getMinRreviewCount())
				topRatedProperty = TopRatedPropertyAccessor.ISTOPRATED;
		}
		return topRatedProperty;
	}
}
