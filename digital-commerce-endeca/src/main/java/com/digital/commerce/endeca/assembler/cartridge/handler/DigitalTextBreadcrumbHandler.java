package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.content.support.ConfigNodeContentItem;

import atg.commerce.endeca.cache.DimensionValueCache;
import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalTextBreadcrumbHandler extends NavigationCartridgeHandler<ConfigNodeContentItem, ConfigNodeContentItem> {

	private DimensionValueCacheTools	dimensionValueCacheTools;
	private static final DigitalLogger		logger	= DigitalLogger.getLogger( DigitalTopNavigationListHandler.class );

	@Override
	public ConfigNodeContentItem process( ConfigNodeContentItem pCartridgeConfig ) throws CartridgeHandlerException {
		
		
		/*
		 * This is No longer used
		 */
		
		
		
		
		final String METHOD_NAME = "process";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		try {
			Set<String> propSet = pCartridgeConfig.keySet();
			Iterator<String> it = propSet.iterator();
			while( it.hasNext() ) {
				String sectionNAme = it.next();
				if( DigitalEndecaConstants.TEXTBREADCRUMB.equalsIgnoreCase( sectionNAme ) ) {
					String breadCrumbName = (String)pCartridgeConfig.get( sectionNAme );
					
					if((breadCrumbName == null || DigitalEndecaConstants.BLANK.equalsIgnoreCase( breadCrumbName )) ) {
						List<String> navReqFilter = getNavigationState().getFilterState().getNavigationFilters();
						String textBreadCumbName = DigitalEndecaConstants.BLANK;
						DimensionValueCache dimValCache = getDimensionValueCacheTools().getCache();
						String navFiltersAsString = "";

						if( dimValCache.getNumCacheEntries() > 0 ) {
							// convert navReqFilter to a "+" delimited string
							for( int i = 0; i < navReqFilter.size(); i++ ) {
								navFiltersAsString = navFiltersAsString + navReqFilter.get( i ) + DigitalEndecaConstants.PLUS;
							}
							// remove the + at the end of the string if length > 0
							if( !navFiltersAsString.isEmpty() )
								navFiltersAsString = navFiltersAsString.substring( 0, navFiltersAsString.length() - 1 );

							// Start with Current Filter state. If perfect match doesn't exist in the Cache then remove the last filter
							// state
							// and try to
							// find exact match with Cache. If perfect match was found then "while condition" won't satisfy and
							// navFiltersAsString has the
							// Clear all navigation state
							List<DimensionValueCacheObject> dimValCacheObject = dimValCache.get( getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE ) + "-" + navFiltersAsString );
							// use above dimValCacheObject if not null. Otherwise, attempt to get dimValCacheObject using <<Site>>-dimValID
							// as
							// key
							dimValCacheObject = dimValCacheObject != null ? dimValCacheObject : dimValCache.get( navFiltersAsString );

							while( dimValCacheObject == null ) {
								// If no PLUS exists then this is the final Navigation Filter. So, break the loop now
								if( !navFiltersAsString.contains( DigitalEndecaConstants.PLUS ) ) {
									break;
								}
								// Remove the last Navigation Filter (after the last PLUS)
								navFiltersAsString = navFiltersAsString.substring( 0, navFiltersAsString.lastIndexOf( DigitalEndecaConstants.PLUS ) );

								dimValCacheObject = dimValCache.get( navFiltersAsString );
								// use above dimValCacheObject if not null. Otherwise, attempt to get dimValCacheObject using
								// <<Site>>-dimValID
								// as key
								dimValCacheObject = dimValCacheObject != null ? dimValCacheObject : dimValCache.get( getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE ) + "-" + navFiltersAsString );
							}

							if( dimValCacheObject != null ) {
								List<String> cacheProperties = dimValCacheObject.get( 0 ).getAncestorRepositoryIds();
								textBreadCumbName = cacheProperties.get( 0 );
							}
						}
						pCartridgeConfig.put( sectionNAme, textBreadCumbName );
					}
				}
			}
		} catch( Exception e ) {
			logger.error( "DigitalTextBreadcrumbHandler exception", e );
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}
		return pCartridgeConfig;

	}

	@Override
	protected ConfigNodeContentItem wrapConfig( ContentItem paramContentItem ) {
		return (ConfigNodeContentItem)paramContentItem;
	}

}
