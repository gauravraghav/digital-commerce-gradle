package com.digital.commerce.endeca.index.accessor;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * This class is used to calculate the product.is_new based on daysAvailable
 * property.
 * 
 * @author oracle
 *
 */
@Getter
@Setter
public class NewProductPropertyAccessor extends PropertyAccessorImpl {
	private static final String	CONST_NO	= "no";
	private static final String	CONST_YES	= "yes";
	
	private String daysAvailablePropertyName;
	private int daysAvailablePropertyValue;
	
	@Override
	protected Object getTextOrMetaPropertyValue( Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType ) {
		
		String isProductNew = null;
		int daysAvailable = 0;
		if (pItem != null) {
			daysAvailable = (int) pItem.getPropertyValue(getDaysAvailablePropertyName());
			if (daysAvailable <= getDaysAvailablePropertyValue()) {
				isProductNew = CONST_YES;
			} else {
				isProductNew = CONST_NO;
			}
		}
		return isProductNew;
	}
}
