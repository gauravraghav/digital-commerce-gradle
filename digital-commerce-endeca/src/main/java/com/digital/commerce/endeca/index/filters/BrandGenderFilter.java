package com.digital.commerce.endeca.index.filters;

import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.PropertyValuesFilter;
import atg.repository.search.indexing.ValueAndSecurity;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class BrandGenderFilter   extends GenericService implements PropertyValuesFilter {

    private Repository catalogRepository;
    private String brandItemType;
    private Map<String,String> genderMappings;

    @Override
    public boolean applyAfterFormatting() {
        return false;
    }

    public ValueAndSecurity[] filter(String s, ValueAndSecurity[] valueAndSecurities) {


        if(valueAndSecurities==null || valueAndSecurities.length ==0)return valueAndSecurities;
        if(isLoggingDebug())logDebug("ValueAndSecurities has {"+valueAndSecurities.length+"} entries.");
        List<ValueAndSecurity> results = new LinkedList<>();
        for(ValueAndSecurity value : valueAndSecurities){
            /**
             * Filter out empty values
             */
            if(value.getValue() == null)continue;

            String brandRepositoryId = value.getValue().toString();

            if(isLoggingDebug())logDebug("Processing brand {"+brandRepositoryId+"}");

            RepositoryItem brandItem = getBrand(brandRepositoryId);
            if(brandItem==null)continue;
            String brandName = (String)brandItem.getPropertyValue("displayName");
            String brandGender = (String)brandItem.getPropertyValue("brandGender");
            String mappedBrandGender = getGenderMappings().get(brandGender);
            String genderToUse = mappedBrandGender!=null?mappedBrandGender:brandGender;
            if(isLoggingDebug())logDebug("brandName {"+brandName+"},genderToUse {"+genderToUse+"}");
            String newValue = brandName+" for "+genderToUse;
            results.add(new ValueAndSecurity(newValue,value.getSecurityConstraints(),value.getScheme()));
        }

        return (ValueAndSecurity[])results.toArray(new ValueAndSecurity[results.size()]);
    }

    RepositoryItem getBrand(String id){
        try {
            return this.getCatalogRepository().getItem(id, getBrandItemType());
        }catch(RepositoryException req){
            if(isLoggingWarning())logWarning("Couldn't get brand item with id {"+id+"}");
            if(isLoggingDebug())logDebug(req);
        }
        return null;
    }
}
