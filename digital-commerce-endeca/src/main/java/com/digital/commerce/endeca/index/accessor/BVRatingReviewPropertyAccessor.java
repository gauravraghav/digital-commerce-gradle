package com.digital.commerce.endeca.index.accessor;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BVRatingReviewPropertyAccessor extends PropertyAccessorImpl {
	private static final Object UNDEFINED = -1;
	private Repository repository;

	protected Object getTextOrMetaPropertyValue(Context pContext,
			RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType) {
		
		
		Object propertyVal = BVRatingReviewPropertyAccessor.UNDEFINED;
		
		if (isLoggingDebug()) {
			logDebug("pContext =" + pContext);
			logDebug("pItem =" + pItem);
			logDebug("pPropertyName =" + pPropertyName);
			logDebug("pType =" + pType);
		}
		
		RepositoryItem ratingItem = null;

		try {
			ratingItem = getRepository().getItem(pItem.getRepositoryId(), "rating");
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("Exception reading BV rating for product " + pItem);
				logError(e);
			}
		}
		
		if (ratingItem != null) {
			propertyVal = ratingItem.getPropertyValue(pPropertyName);
		}
		return propertyVal;
	}
}
