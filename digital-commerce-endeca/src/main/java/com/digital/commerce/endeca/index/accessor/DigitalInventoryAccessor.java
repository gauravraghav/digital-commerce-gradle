/**
 * 
 */
package com.digital.commerce.endeca.index.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.inventory.vo.InventoryVO;
import com.endeca.itl.record.PropertyValue;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.GenerativePropertyAccessor;
import atg.repository.search.indexing.IndexingOutputConfig;
import atg.repository.search.indexing.specifier.OutputProperty;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalInventoryAccessor extends GenerativePropertyAccessor {
	
	private String endecaRecordInventoryRepositoryId;
	private String endecaRecordInventoryLocationId;
	private String endecaRecordInventoryLevelName;
	
	private String endecaRecordProductRepositoryId;
	private String endecaRecordProductInventoryLevelName;

	private DataSource inventoryDataSource;


	private String fullInventorySQL;
	private String skuIdColumnName;
	private String fullInventoryColumnName;
	private String locationIdColumnName;
	private String storeInventoryColumnName;
	private String endecaRecordHasInventoryLevelName;
	
	@Override
	protected Map<String, Object> getPropertyNamesAndValues(Context pContext, RepositoryItem pSkuRepositoryItem,
			String pPropertyName, PropertyTypeEnum pParamPropertyTypeEnum, boolean pParamBoolean) {
		
		Map<String, InventoryVO> fullInventoryMap = null;
		Map<String, Object> inventoryMap = new HashMap<String, Object>();
		
		if (pSkuRepositoryItem != null) {
			fullInventoryMap = processInventory(pContext);
			if (null != fullInventoryMap && fullInventoryMap.get(pSkuRepositoryItem.getRepositoryId()) != null) {
				String fullsiteStockLevel = fullInventoryMap.get(pSkuRepositoryItem.getRepositoryId()).getFullsiteStockLevel();
				inventoryMap.put(getEndecaRecordInventoryLevelName(), fullInventoryMap.get(pSkuRepositoryItem.getRepositoryId()).getFullsiteStockLevel());
				if( !DigitalStringUtil.equals(fullInventoryMap.get(pSkuRepositoryItem.getRepositoryId()).getLocationId(), "no_store_data" ) && ( fullInventoryMap.get(pSkuRepositoryItem.getRepositoryId()).getStoreStockLevel() > 0 ) ) {
					inventoryMap.put(getEndecaRecordInventoryLocationId(), fullInventoryMap.get(pSkuRepositoryItem.getRepositoryId()).getLocationId());
				}
				if (Integer.valueOf(fullsiteStockLevel) >= 1) {
					inventoryMap.put(getEndecaRecordHasInventoryLevelName(), "1");
				} else {
					inventoryMap.put(getEndecaRecordHasInventoryLevelName(), "0");
				}
			}			
		}
		return inventoryMap;
	}

	@Override
	public boolean ownsDynamicPropertyName(String paramString,
			IndexingOutputConfig paramIndexingOutputConfig,
			OutputProperty paramOutputProperty) {
		return false;
	}

	private Map<String, InventoryVO> processInventory(Context pContext) {

		InventoryVO lInventoryVO;
		Map<String, InventoryVO> fullInventoryMap = new HashMap<String, InventoryVO>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;
		try {
			if (isLoggingInfo()) {
				logInfo("STARTED the execution of processInventory SQL query at : "
						+ new Timestamp(System.currentTimeMillis()));
			}
			synchronized (this) {
				Object contextObject = pContext.getGlobalAttribute("DSWSKUInventory");
				if (contextObject == null) {
					connection = this.getInventoryDataSource().getConnection();
					statement = connection.prepareStatement(getFullInventorySQL());
					results = statement.executeQuery();
					if (isLoggingInfo()) {
						logInfo("FINISHED the execution of Inventory SQL query at : "
								+ new Timestamp(System.currentTimeMillis()));
					}
					String tempSkuId = "";
					Collection<PropertyValue> tempLocationList = new ArrayList<>();
					String fullSiteStockLevel = null;
					String tempFullSiteStock = null;
					String locationId = "";
					String skuId = "";
					while (results.next()) {
						if (isLoggingDebug()) {
							logDebug("::::::::: Fetching record for sku id    :  " + results.getString(getSkuIdColumnName()));
						}
						lInventoryVO = new InventoryVO();
						skuId = results.getString(getSkuIdColumnName());
						fullSiteStockLevel = ((results.getString(getFullInventoryColumnName())) == null) ? "0"
								: results.getString(getFullInventoryColumnName());
						locationId = results.getString(getLocationIdColumnName());
						int storeStockLevel = ((results.getString(getStoreInventoryColumnName())) == null) ? 0
								: Integer.parseInt(results.getString(getStoreInventoryColumnName()));
						lInventoryVO.setSkuId(skuId);
						lInventoryVO.setLocationId(locationId);
						lInventoryVO.setStoreStockLevel(storeStockLevel);
						lInventoryVO.setFullsiteStockLevel(fullSiteStockLevel);
						fullInventoryMap.put(skuId, lInventoryVO);
					}
					pContext.setGlobalAttribute("DSWSKUInventory", fullInventoryMap);
					if (isLoggingInfo()) {
						logInfo("SKU Inventory Size MAP: " + fullInventoryMap.size());
					}
				}
			}
		} catch (SQLException sqlex) {
			if (isLoggingError()) {
				logError("SQLException fetching inventory from processInventory", sqlex);
			}
		} catch (Exception ex) {
			if (isLoggingError()) {
				logError("General Exception while writing to CAS from processInventory", ex);
			}
		} finally {
			if (results != null) {
				try {
					results.close();
				} catch (Exception ex) {
					if (isLoggingError()) {
						logError("Exception while closing resultset from processInventory", ex);
					}
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (Exception ex) {
					if (isLoggingError()) {
						logError("Exception while closing statement from processInventory", ex);
					}
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception ex) {
					if (isLoggingError()) {
						logError("Exception while closing connection from processInventory", ex);
					}
				}
			}
		}
		return (Map<String, InventoryVO>)pContext.getGlobalAttribute("DSWSKUInventory");
	}
}
