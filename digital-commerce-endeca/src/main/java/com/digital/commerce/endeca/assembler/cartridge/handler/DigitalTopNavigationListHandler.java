package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.endeca.assembler.cartridge.support.CartridgeHandlerUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.digital.commerce.endeca.assembler.util.DigitalEndecaUtils;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.cartridge.model.LinkBuilder;
import com.endeca.infront.content.support.ConfigNodeContentItem;

import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.rest.input.JSONInputCustomizer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DigitalTopNavigationListHandler extends NavigationCartridgeHandler<ConfigNodeContentItem, ConfigNodeContentItem> {
	private static final String			METHOD_NAME	= "process";

	private static transient DigitalLogger		logger		= DigitalLogger.getLogger( DigitalTopNavigationListHandler.class );

	private int							siteHttpsServerPort;
	private boolean						previewEnabled;

	private DimensionValueCacheTools	dimensionValueCacheTools;
	private int retries = 0;
	private boolean	httpsEnabled;
	private String	siteHttpServerName;
	private int	siteHttpServerPort;
	private String	contentPath;
	private String	serviceUrl;
	private String	connectionTimeout;
	private String	soTimeout;
	private int		bufferSize;

	private String getResponseBody( GetMethod method ) {
		if( method != null && method.hasBeenUsed() ) {
			StringWriter stringOut = new StringWriter();
			try( BufferedWriter dumpOut = new BufferedWriter( stringOut, bufferSize );BufferedReader in = new BufferedReader( new InputStreamReader( method.getResponseBodyAsStream() ) )) {
				String line = "";
				while( ( line = in.readLine() ) != null ) {
					dumpOut.write( line );
					dumpOut.newLine();
				}
			} catch( IOException e ) {
				logger.error( "DigitalTopNavigationListHandler IO exception", e );
			}

			return stringOut.toString();
		}
		return null;
	}

	/** @param pCartridgeConfig */
	@Override
	public ConfigNodeContentItem process( ConfigNodeContentItem pCartridgeConfig ) throws CartridgeHandlerException {

		try {
			DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
			boolean showSize = true;
			showSize = (boolean)pCartridgeConfig.get( DigitalEndecaConstants.SHOWSIZE );
			if( showSize ) {
				String queryString = null;
				LinkBuilder linkBuilder = (LinkBuilder)pCartridgeConfig.get( DigitalEndecaConstants.LINKTEST );
				queryString = linkBuilder.getQueryString();
				String contentPathUrl = generateContentPathUrl( queryString );
				HttpClient client = new HttpClient();
				if(retries > 0) {
					client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(retries, true));
				} else {
					client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(retries, false));
				}

				HttpConnectionManager connectionManager = client.getHttpConnectionManager();
				if( connectionManager != null ) {
					HttpConnectionManagerParams managerParams = connectionManager.getParams();
					if( managerParams != null ) {
						managerParams.setParameter( HttpConnectionParams.CONNECTION_TIMEOUT, Integer.parseInt( getConnectionTimeout() ) * 1000 );
						managerParams.setParameter( HttpConnectionParams.SO_TIMEOUT, Integer.parseInt( getSoTimeout() ) * 1000 );
					}
				}
				GetMethod method = new GetMethod( contentPathUrl );
				DigitalEndecaUtils.setMethodHeaders( method );
				try {
					int statusCode = client.executeMethod( method );
					if( statusCode == DigitalEndecaConstants.STATUSCODE ) {
						String response = getResponseBody( method );
						List<Map<String, String>> sizeList = new ArrayList<>();
						Map<String, Object> contentMap = new HashMap<>();
						JSONObject finalJson = JSONInputCustomizer.isValidJSONString( response );
						if( response != null && finalJson.has( DigitalEndecaConstants.CONTENTITEM ) ) {
							JSONObject contentContentItem = (JSONObject)finalJson.get( DigitalEndecaConstants.CONTENTITEM );
							if( contentContentItem != null && contentContentItem.length() > 0 && contentContentItem.has( DigitalEndecaConstants.CONTENTS ) ) {
								JSONArray contents = (org.json.JSONArray)contentContentItem.get( DigitalEndecaConstants.CONTENTS );
								if( contents != null && contents.length() > 0 ) {
									JSONObject refinementContent = (JSONObject)contents.get( 0 );
									if( refinementContent.has( DigitalEndecaConstants.REFINEMENTS ) ) {
										JSONArray dimValList = (JSONArray)refinementContent.get( DigitalEndecaConstants.DIMVALIST );
										JSONArray refinementList = (JSONArray)refinementContent.get( DigitalEndecaConstants.REFINEMENTS );
										if( refinementContent.has( DigitalEndecaConstants.DIMVALIST ) && dimValList.length() > 0 ) {
											String contentPath = this.getContentPath();
											String locale = getNavigationState().getParameter( DigitalEndecaConstants.LOCALE );
											String pushSite = getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE );
											String securityFilter = DigitalEndecaConstants.SECURITYFILTER;
											String NavQueryString = DigitalEndecaConstants.SITEFILTERPARAM + securityFilter + DigitalEndecaConstants.LOCALEPARAM + locale + DigitalEndecaConstants.CONTENTPATHPARAM + contentPath + DigitalEndecaConstants.PUSHSITEPARAM
													+ pushSite;
											for( int i = 0; i < dimValList.length(); i++ ) {
												Map<String, String> sizeMap = new HashMap<>();
												JSONObject dimVal = dimValList.getJSONObject( i );
												String sizeStr = (String)dimVal.get( DigitalEndecaConstants.NAME );
												String iD = (String)dimVal.get( DigitalEndecaConstants.ID );
												String navStr = CartridgeHandlerUtil.generateSEONvalue( queryString + DigitalEndecaConstants.PLUS + iD ) + NavQueryString;
												// add this Size to Cache now
												if( null!=queryString && getDimensionValueCacheTools().getCache() != null ) {
													String dimValID = queryString.split( DigitalEndecaConstants.EQUALTO )[1] + DigitalEndecaConstants.PLUS + iD;

													/* Cache format
													 * Actual: RepositoryID, DimValID, URL, Ancestors
													 * Now: <<SiteID>>-DimValID, DimValID, URL, CacheProperties (to store additional info)
													 * Note: <SiteID> should not be added to the Key here. It should be added only for Top
													 * Nav and Footer links */
													if( getDimensionValueCacheTools().getCache().get( getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE ) + "-" + dimValID ) == null ) {

														// Add the display name only. "Hide refinement" should not be added here
														List<String> cacheProperties = Arrays.asList( sizeStr );

														getDimensionValueCacheTools().getCache().put( getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE ) + "-" + dimValID, dimValID, null, cacheProperties );
													}

												}
												sizeMap.put( DigitalEndecaConstants.LABEL, sizeStr );
												sizeMap.put( DigitalEndecaConstants.NAVIGATIONSTATE, navStr );
												sizeList.add( sizeMap );
											}
										} else {
											// This else condition should never be required. Business should always select Dimension
											// Values in DimValList Editor. We are not caching this.
											for( int i = 0; i < refinementList.length(); i++ ) {
												Map<String, String> sizeMap = new HashMap<>();
												JSONObject refinement = refinementList.getJSONObject( i );
												String sizeStr = (String)refinement.get( DigitalEndecaConstants.LABEL );
												String navStr = (String)refinement.get( DigitalEndecaConstants.NAVIGATIONSTATE );
												sizeMap.put( DigitalEndecaConstants.LABEL, sizeStr );
												sizeMap.put( DigitalEndecaConstants.NAVIGATIONSTATE, navStr );
												sizeList.add( sizeMap );
											}
										}
									}
								}
								contentMap.put( DigitalEndecaConstants.CONTENTS, sizeList );
								if( this.previewEnabled ) {
									if( contentContentItem.has( DigitalEndecaConstants.PREVIEWMODULEURL ) ) {
										String previewModuleUrl = (String)contentContentItem.get( DigitalEndecaConstants.PREVIEWMODULEURL );
										contentMap.put( DigitalEndecaConstants.PREVIEWMODULEURL, previewModuleUrl );
									}
									if( contentContentItem.has( DigitalEndecaConstants.AUDITCONTENTS ) ) {
										String auditContents = (String)contentContentItem.get( DigitalEndecaConstants.AUDITCONTENTS );
										contentMap.put( DigitalEndecaConstants.AUDITCONTENTS, auditContents );
									}
								}
								pCartridgeConfig.put( DigitalEndecaConstants.SIZE, contentMap );
							} else {
								pCartridgeConfig.put( DigitalEndecaConstants.SIZE, DigitalEndecaConstants.BLANK );
								pCartridgeConfig.put( DigitalEndecaConstants.RESPONSE, "Empty Contents" );
							}
						} else {
							pCartridgeConfig.put( DigitalEndecaConstants.SIZE, DigitalEndecaConstants.BLANK );
							pCartridgeConfig.put( DigitalEndecaConstants.RESPONSE, "Response is null" );
						}
					} else {
						pCartridgeConfig.put( DigitalEndecaConstants.SIZE, DigitalEndecaConstants.BLANK );
						pCartridgeConfig.put( DigitalEndecaConstants.RESPONSE, "Getting size is having issue like connectivity issue" );
					}
				}

				catch( ConnectTimeoutException ce ) {

					logger.error( "DigitalTopNavigationListHandler ConnectTimeOut exception", ce );
					pCartridgeConfig.put( DigitalEndecaConstants.SIZE, DigitalEndecaConstants.BLANK );

				} catch( SocketTimeoutException se ) {

					logger.error( "DigitalTopNavigationListHandler SocketTimeOut exception", se );
					pCartridgeConfig.put( DigitalEndecaConstants.SIZE, DigitalEndecaConstants.BLANK );

				} catch( Exception e ) {
					logger.error( "DigitalTopNavigationListHandler Size exception", e );
				}
			}
		}

		finally {

			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation( this.getClass().getName(), METHOD_NAME );
		}

		return pCartridgeConfig;

	}

	/** @param queryString */
	private String generateContentPathUrl( String queryString ) {
		StringBuilder contentPathUrl = new StringBuilder();
		if( queryString != null ) {

			if( isHttpsEnabled() ) {
				contentPathUrl.append( DigitalEndecaConstants.HTTPSPROTOCOL );
			} else {
				contentPathUrl.append( DigitalEndecaConstants.HTTPPROTOCOL );
			}

			contentPathUrl.append( getSiteHttpServerName() );
			contentPathUrl.append( DigitalEndecaConstants.CLUMNSEPARATOR );

			if( isHttpsEnabled() ) {
				contentPathUrl.append( getSiteHttpsServerPort() );
			} else {
				contentPathUrl.append( getSiteHttpServerPort() );
			}

			contentPathUrl.append( this.getServiceUrl() );
			contentPathUrl.append( CartridgeHandlerUtil.generateSEONvalue( queryString ) );
			contentPathUrl.append( DigitalEndecaConstants.CONTENTPATH );
			contentPathUrl.append( this.getContentPath() );
			contentPathUrl.append( DigitalEndecaConstants.PUSHSITEPARAM + getNavigationState().getParameter( DigitalEndecaConstants.PUSHSITE ) );
			contentPathUrl.append( DigitalEndecaConstants.LOCALEPARAM + getNavigationState().getParameter( DigitalEndecaConstants.LOCALE ) );
		}
		if( logger.isDebugEnabled() ) {
			logger.debug( "Final content path:  " + contentPathUrl.toString() );
		}
		return contentPathUrl.toString();
	}

	protected ConfigNodeContentItem wrapConfig( ContentItem paramContentItem ) {
		return (ConfigNodeContentItem)paramContentItem;
	}
}
