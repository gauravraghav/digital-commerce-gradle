package com.digital.commerce.endeca.index.filters;

import atg.nucleus.GenericService;
import atg.repository.search.indexing.PropertyValuesFilter;
import atg.repository.search.indexing.ValueAndSecurity;

import java.util.LinkedList;
import java.util.List;

public class GenderFilter extends GenericService implements PropertyValuesFilter {


    @Override
    public boolean applyAfterFormatting() {
        return false;
    }

    @Override
    public ValueAndSecurity[] filter(String s, ValueAndSecurity[] valueAndSecurities) {
        if (valueAndSecurities == null || valueAndSecurities.length == 0) return valueAndSecurities;
        List<ValueAndSecurity> results = new LinkedList<>();

        for (ValueAndSecurity value : valueAndSecurities) {
            /**
             * Filter out empty values
             */
            if (value.getValue() == null) continue;
            /**
             * Just take the value, convert to string and apply the trim method.
             */

            String oldValue = value.getValue().toString().trim();
            boolean transformed = false;
            if( "201".equals( oldValue ) || "203".equals( oldValue ) ) {
                results.add(new ValueAndSecurity("Women",value.getSecurityConstraints(),value.getScheme()));
                transformed=true;
            }
            if( "202".equals( oldValue ) || "203".equals( oldValue ) ) {
                results.add(new ValueAndSecurity("Men",value.getSecurityConstraints(),value.getScheme()));
                transformed=true;

            }
            if( "204".equals( oldValue ) || "205".equals( oldValue ) ) {
                results.add(new ValueAndSecurity("Girls",value.getSecurityConstraints(),value.getScheme()));
                transformed=true;

            }
            if( "204".equals( oldValue ) || "206".equals( oldValue ) ) {
                results.add(new ValueAndSecurity("Boys",value.getSecurityConstraints(),value.getScheme()));
                transformed=true;
            }

            if(!transformed){
                results.add(value);
            }else{
                if(isLoggingDebug())logDebug("GenderFilter transformed ["+oldValue+"] into a gender string.");
            }

        }
        return (ValueAndSecurity[])results.toArray(new ValueAndSecurity[results.size()]);
    }
}
