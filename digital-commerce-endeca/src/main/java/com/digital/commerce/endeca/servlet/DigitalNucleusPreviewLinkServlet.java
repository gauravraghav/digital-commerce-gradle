package com.digital.commerce.endeca.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.cartridge.model.LinkBuilder;
import com.endeca.infront.cartridge.model.NavigationAction;
import com.endeca.infront.content.ContentException;
import com.endeca.infront.content.UserState;
import com.endeca.infront.content.source.ContentSource;
import com.endeca.infront.navigation.NavigationStateBuilder;
import com.endeca.infront.navigation.url.ActionPathProvider;
import com.endeca.infront.serialization.JsonSerializer;
import com.endeca.infront.site.RequestParamParser;
import com.endeca.infront.site.SiteUtils;
import com.endeca.infront.site.model.SiteDefinition;
import com.endeca.infront.site.model.SiteState;

import atg.endeca.servlet.NucleusPreviewLinkServlet;
@SuppressWarnings({"rawtypes","unchecked"})
public class DigitalNucleusPreviewLinkServlet extends NucleusPreviewLinkServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6347557714988017934L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String jsonp = request.getParameter("__jsonp");
		if (jsonp == null) {
			throw new ServletException("Expect a parameter of name '__jsonp' specifying the json callback function.");  //NOSONAR
		}
		String contentPath = request.getParameter("__contentUri");
		if (contentPath == null) {
			throw new ServletException(  //NOSONAR
					"Expect a parameter of name '__contentUri' specifying the content path (for example, '/pages/browse').");  //NOSONAR
		}

		NavigationStateBuilder navigationStateBuilder = getNavigationStateBuilder();
		ContentSource contentSource = getContentSource();

		String linkParameter = (request.getParameter("__link") == null) ? "" : request.getParameter("__link");

		// This code is added for previewing the static pages content
		String innerContentUri = (request.getParameter("__innercontentUri") == null) ? ""
				: request.getParameter("__innercontentUri");

		Map output = new HashMap();
		String staticPath = null;
		try {
			UserState userState = new UserState();
			String workspaceId = request.getParameter("workspaceId");
			if (workspaceId == null) {
				workspaceId = "";
			}

			userState.setWorkspaceId(workspaceId);
			
			SiteState siteState = null;
			String siteId = request.getParameter(RequestParamParser.SITEID_PARAM_NAME);
			if (siteId == null) {
				siteId = SiteUtils.getSiteId(contentPath);
			}

			SiteDefinition siteDef;
			if (siteId == null) {
				siteDef = this.getSiteManager().getDefaultSite();
				siteId = siteDef.getId();
			} else {
				siteDef = this.getSiteManager().getSite(siteId);
			}

			siteState = new SiteState(siteId, siteDef);
			LinkBuilder link = new LinkBuilder(contentPath, LinkBuilder.LinkType.RELATIVE_NAV, linkParameter);
			//NavigationAction action = (NavigationAction) link.createAction(navigationStateBuilder, contentSource, null,null);
			NavigationAction action = (NavigationAction) link.createAction(navigationStateBuilder, contentSource,
					(ActionPathProvider) null, siteState, userState);
			
			String initialUrl = getUrlForAction(action,userState);
			String[] staticUrlTypes = { DigitalEndecaConstants.STATIC_CONTENT_PATH, DigitalEndecaConstants.STATIC_PAGES_PATH, DigitalEndecaConstants.STATIC_PROMOTIONS_PATH };
			String actionUrl = initialUrl
					.replaceFirst(DigitalEndecaConstants.DSW_HOME_PATH, DigitalEndecaConstants.EN_US_WITH_SLASH)
					.replaceFirst(DigitalEndecaConstants.DSW_PATH, DigitalEndecaConstants.EN_US_NO_SLASH);
			if (!DigitalStringUtil.contains(innerContentUri, staticUrlTypes)) {
				if (actionUrl.contains(DigitalEndecaConstants.NTT_WITH_AMPERSAND)) {
					String firstHalf = actionUrl.split(DigitalEndecaConstants.NTT_WITH_AMPERSAND)[0];
					String secondHalf = actionUrl.split(DigitalEndecaConstants.NTT_WITH_AMPERSAND)[1];
					actionUrl = firstHalf.replaceFirst(DigitalEndecaConstants.ESCAPED_QUESTIONMARK,
							DigitalEndecaConstants.FORWARD_SLASH + secondHalf + DigitalEndecaConstants.FORWARD_SLASH
									+ DigitalEndecaConstants.ESCAPED_QUESTIONMARK);
				}

			} else {
				if (innerContentUri.contains(DigitalEndecaConstants.STATIC_CONTENT_PATH)) {
					staticPath = DigitalEndecaConstants.CONTENT + DigitalEndecaConstants.FORWARD_SLASH + innerContentUri.split(DigitalEndecaConstants.STATIC_CONTENT_PATH)[1].split(DigitalEndecaConstants.FORWARD_SLASH)[0].replaceAll(DigitalEndecaConstants.UNDERSCORE, DigitalEndecaConstants.HYPHEN);
				} else if (innerContentUri.contains(DigitalEndecaConstants.STATIC_PAGES_PATH)) {
					staticPath = DigitalEndecaConstants.PAGE + DigitalEndecaConstants.FORWARD_SLASH+innerContentUri.split(DigitalEndecaConstants.STATIC_PAGES_PATH)[1].replaceAll(DigitalEndecaConstants.UNDERSCORE, DigitalEndecaConstants.HYPHEN);
				} else if (innerContentUri.contains(DigitalEndecaConstants.STATIC_PROMOTIONS_PATH)) {
					staticPath = DigitalEndecaConstants.PROMOTIONS + DigitalEndecaConstants.FORWARD_SLASH+ innerContentUri.split(DigitalEndecaConstants.STATIC_PROMOTIONS_PATH)[1].split(DigitalEndecaConstants.FORWARD_SLASH)[0].replaceAll(DigitalEndecaConstants.UNDERSCORE, DigitalEndecaConstants.HYPHEN);
				}
				actionUrl = DigitalEndecaConstants.EN_US + staticPath;
			}
			output.put("action", actionUrl);
		} catch (ContentException e) {
			throw new ServletException("Cannot create link", e);  //NOSONAR
		}

		try {
			response.setHeader("content-type", "application/javascript");
			response.getWriter().write(jsonp);
			response.getWriter().write(40);
			new JsonSerializer(response.getWriter()).write(output);
			response.getWriter().write(");");
		} catch (IOException e) {
			throw new ServletException("Error writing response", e);  //NOSONAR
		}
	}
}
