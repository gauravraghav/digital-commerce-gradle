package com.digital.commerce.endeca.index.accessor;

import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BestSellerPropertyAccessor extends PropertyAccessorImpl {

	private int bestSellerThreshold;

	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
			PropertyTypeEnum pType) {
		String isBestSeller = DigitalEndecaConstants.NO; // Using string to keep the value inline with existing values (yes/no instead of true/false)
		Object unitsSoldObj = pItem.getPropertyValue(DigitalEndecaConstants.UNITS_SOLD);
		if (null != pItem && null != unitsSoldObj) {
			long unitsSold = Long.parseLong(unitsSoldObj.toString());
			if (unitsSold >= this.bestSellerThreshold)
				isBestSeller = DigitalEndecaConstants.YES;
		}
		return isBestSeller;
	}
}
