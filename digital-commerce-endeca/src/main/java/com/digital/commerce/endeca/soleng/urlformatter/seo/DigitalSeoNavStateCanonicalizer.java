/**
 * 
 */
package com.digital.commerce.endeca.soleng.urlformatter.seo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.endeca.assembler.cartridge.handler.DigitalMainRefinementMenuHandler;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimLocationList;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.MutableDimLocationList;
import com.endeca.soleng.urlformatter.NavStateUrlParam;
import com.endeca.soleng.urlformatter.seo.SeoNavStateCanonicalizer;

import atg.core.util.StringList;
import lombok.Getter;
import lombok.Setter;

/**
 * @author pr412029
 *
 */
@SuppressWarnings({"unchecked"})
@Getter
@Setter
public class DigitalSeoNavStateCanonicalizer extends SeoNavStateCanonicalizer {
	
	private StringList					dimensionSortOrder;
	private StringList					skipSortWithinDimensions;
	private static final DigitalLogger		logger	= DigitalLogger.getLogger( DigitalMainRefinementMenuHandler.class );

	/**
	 * 
	 */
	public DigitalSeoNavStateCanonicalizer() {
		// TODO Auto-generated constructor stub
	}
	
	private String getDimNamefromDimVal(DimLocation pDimLocation)
	{
		DimVal dimVal = pDimLocation.getDimValue();
		return dimVal.getDimensionName();
	}	
		
	public void canonicalize(NavStateUrlParam pNavState) {
		
		//First, Create a Map with Dimension name and Position as per the Business requirement
		final Map<String, Integer> requiredDimensionOrder = new HashMap<>();
		List<String> requiredDimensionOrderList = new ArrayList<>();
		requiredDimensionOrderList = Arrays.asList(getDimensionSortOrder().stringAt(0).split(","));
		for (int i=0; i<requiredDimensionOrderList.size(); i++) {
			requiredDimensionOrder.put(requiredDimensionOrderList.get(i), i);			
		}
		
		//Now, Create a Map for Dimensions that needs to skipped for Second level of Sorting
		final List<String> skipSortWithinDimensionsList = Arrays.asList(getSkipSortWithinDimensions().stringAt(0).split(","));		
		
		
		//Next, let's create a List <String> based on current incoming Dimension order
		DimLocationList dimLocationList = pNavState.getDimLocationList();
		if (!(dimLocationList instanceof MutableDimLocationList)) {
			dimLocationList = new MutableDimLocationList(dimLocationList);
		}
		final List<String> currentDimensionOrder = new ArrayList<>();
		for (Object thisDimLoc : dimLocationList) {
			if (thisDimLoc instanceof DimLocation)
				currentDimensionOrder.add(getDimNamefromDimVal((DimLocation)thisDimLoc));			
		}
		
		//Now, let's sort this currentDimensionOrder to match the requiredDimensionOrder per Business requirement
		Collections.sort(dimLocationList, new Comparator<Object>() {			
			public int compare(Object left, Object right) {
				if (left instanceof DimLocation && right instanceof DimLocation) {
					DimLocation dimlocLeft = (DimLocation)left;
					DimLocation dimlocRight = (DimLocation)right;
					Integer leftPosition = requiredDimensionOrder.get(getDimNamefromDimVal(dimlocLeft));
				    Integer rightPosition = requiredDimensionOrder.get(getDimNamefromDimVal(dimlocRight));
				    if (leftPosition == null || rightPosition == null) {				    	
				    	logger.error( "Bad Dimension Name encountered while sorting in DigitalSeoNavStateCanonicalizer. Canonical URL won't work properly" );		
				    	return 0;
				    }
				    return leftPosition.compareTo(rightPosition);
				} else {
					return 0;
				}
		    }
		});
		
		//Now, let's sort the Dimension values by Dimension Value ID to ensure Filters selected within a single dimension is also ordered properly
		Collections.sort(dimLocationList, new Comparator<Object>() {			
			public int compare(Object left, Object right) {
				if (left instanceof DimLocation && right instanceof DimLocation) {
					DimLocation dimlocLeft = (DimLocation)left;
					DimLocation dimlocRight = (DimLocation)right;
					
					//Do not Sort with in Dimension for the Dimensions listed in "skipSortWithinDimensions" property file attribute
					if (skipSortWithinDimensionsList.contains(getDimNamefromDimVal(dimlocLeft)))
						return 0;					
					
					if (getDimNamefromDimVal(dimlocLeft).equals(getDimNamefromDimVal(dimlocRight))) {
						String leftDimValName = dimlocLeft.getDimValue().getName();
					    String rightDimValName = dimlocRight.getDimValue().getName();				  
					    return leftDimValName.toUpperCase().compareTo(rightDimValName.toUpperCase());
					} else {
						return 0;
					}
					
				} else {
					return 0;
				}
		    }
		});
		
		
		
		pNavState.setDimLocationList(dimLocationList);
		
	}
	
	

}
