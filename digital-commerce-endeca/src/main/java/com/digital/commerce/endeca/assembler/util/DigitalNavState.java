package com.digital.commerce.endeca.assembler.util;

import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;

@Getter
@Setter
public class DigitalNavState implements Comparable<DigitalNavState> {

	private String label;
	private String navState;
	
	@Override
	public int compareTo(DigitalNavState navState) {
		return this.getLabel().compareTo(navState.label);
	}
	
	private static Comparator<DigitalNavState> NavLabelComparator = new Comparator<DigitalNavState>() {

		public int compare(DigitalNavState navState1, DigitalNavState navState2) {

			String navLabel1 = navState1.getLabel().toUpperCase();
			String navLabel2 = navState2.getLabel().toUpperCase();

			//ascending order
			return navLabel1.compareTo(navLabel2);

			//descending order
			//return navLabel2.compareTo(navLabel1);
		}
	};
}
