/**
 * 
 */
package com.digital.commerce.endeca.inventory.vo;

import atg.repository.Repository;
import lombok.Getter;
import lombok.Setter;

/**
 * @author oracle
 *
 */
@Getter
@Setter
public class InventoryVO {

	private String productId;
	
	private String stockLevel;
	
	private String stockLevelClearance;
	
	private String skuId;
	
	private String fullsiteStockLevel;
	
	private String locationId;
	
	private int storeStockLevel;
	
	private String skuHasInventory;
}
