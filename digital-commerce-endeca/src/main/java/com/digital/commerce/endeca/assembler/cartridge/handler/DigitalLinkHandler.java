package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.Iterator;
import java.util.Set;

import com.digital.commerce.endeca.assembler.cartridge.support.CartridgeHandlerUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.cartridge.model.LinkBuilder;
import com.endeca.infront.content.support.ConfigNodeContentItem;

public class DigitalLinkHandler extends NavigationCartridgeHandler<ConfigNodeContentItem, ConfigNodeContentItem> {


	@Override
	public ConfigNodeContentItem process(ConfigNodeContentItem pCartridgeConfig) throws CartridgeHandlerException {
		Set<String> propSet= pCartridgeConfig.keySet();
		Iterator<String> it= propSet.iterator();
		while(it.hasNext()){
			String sectionNAme= it.next();
			if(DigitalEndecaConstants.LINKTEXT.equalsIgnoreCase(sectionNAme)){
				LinkBuilder linkBuilder = (LinkBuilder) pCartridgeConfig.get(DigitalEndecaConstants.LINKTEXT);
				if(linkBuilder.getQueryString()!=null){
					linkBuilder.setQueryString(CartridgeHandlerUtil.generateSEONvalue(linkBuilder.getQueryString()));
					pCartridgeConfig.put(DigitalEndecaConstants.LINKTEXT, linkBuilder);
				}
			}
			if(DigitalEndecaConstants.LINKTEXT1.equalsIgnoreCase(sectionNAme)){
				LinkBuilder linkBuilder = (LinkBuilder) pCartridgeConfig.get(DigitalEndecaConstants.LINKTEXT1);
				if(linkBuilder.getQueryString()!=null){
					linkBuilder.setQueryString(CartridgeHandlerUtil.generateSEONvalue(linkBuilder.getQueryString()));
					pCartridgeConfig.put(DigitalEndecaConstants.LINKTEXT1, linkBuilder);
				}
			}
			if(DigitalEndecaConstants.LINKTEXT2.equalsIgnoreCase(sectionNAme)){
				LinkBuilder linkBuilder = (LinkBuilder) pCartridgeConfig.get(DigitalEndecaConstants.LINKTEXT2);
				if(linkBuilder.getQueryString()!=null){
					linkBuilder.setQueryString(CartridgeHandlerUtil.generateSEONvalue(linkBuilder.getQueryString()));
					pCartridgeConfig.put(DigitalEndecaConstants.LINKTEXT2, linkBuilder);
				}
			}
			if(DigitalEndecaConstants.IMAGECTA.equalsIgnoreCase(sectionNAme)){
				LinkBuilder linkBuilder = (LinkBuilder) pCartridgeConfig.get(DigitalEndecaConstants.IMAGECTA);
				if(linkBuilder.getQueryString()!=null){
					linkBuilder.setQueryString(CartridgeHandlerUtil.generateSEONvalue(linkBuilder.getQueryString()));
					pCartridgeConfig.put(DigitalEndecaConstants.IMAGECTA, linkBuilder);
				}
			}
			if(DigitalEndecaConstants.TERMSANDCONDITIONS.equalsIgnoreCase(sectionNAme)){
				LinkBuilder linkBuilder = (LinkBuilder) pCartridgeConfig.get(DigitalEndecaConstants.TERMSANDCONDITIONS);
				if(linkBuilder.getQueryString()!=null){
					linkBuilder.setQueryString(CartridgeHandlerUtil.generateSEONvalue(linkBuilder.getQueryString()));
					pCartridgeConfig.put(DigitalEndecaConstants.TERMSANDCONDITIONS, linkBuilder);
				}
			}
			if(DigitalEndecaConstants.VIEWDETAILS.equalsIgnoreCase(sectionNAme)){
				LinkBuilder linkBuilder = (LinkBuilder) pCartridgeConfig.get(DigitalEndecaConstants.VIEWDETAILS);
				if(linkBuilder.getQueryString()!=null){
					linkBuilder.setQueryString(CartridgeHandlerUtil.generateSEONvalue(linkBuilder.getQueryString()));
					pCartridgeConfig.put(DigitalEndecaConstants.VIEWDETAILS, linkBuilder);
				}
			}
		}
		
		return pCartridgeConfig;
	}	
	
	@Override
	protected ConfigNodeContentItem wrapConfig(ContentItem paramContentItem) {
		return (ConfigNodeContentItem) paramContentItem;
	}
	
}
