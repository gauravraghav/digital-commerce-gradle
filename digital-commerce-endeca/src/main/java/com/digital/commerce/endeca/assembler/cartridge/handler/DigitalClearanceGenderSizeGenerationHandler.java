package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.json.JSONArray;
import org.json.JSONObject;

import atg.endeca.assembler.configuration.AssemblerApplicationConfiguration;
import atg.rest.input.JSONInputCustomizer;

import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.common.util.DigitalPerformanceMonitorUtil;
import com.digital.commerce.endeca.assembler.cartridge.support.CartridgeHandlerUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.digital.commerce.endeca.assembler.util.DigitalEndecaUtils;
import com.digital.commerce.endeca.assembler.util.DigitalNavState;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.cartridge.model.LinkBuilder;
import com.endeca.infront.content.support.ConfigNodeContentItem;
import com.endeca.infront.content.support.XmlContentItem;
import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.HttpENEConnection;

/**
 * 
 * @author mayank batra
 * 
 */
@Getter
@Setter
public class DigitalClearanceGenderSizeGenerationHandler
		extends NavigationCartridgeHandler<ConfigNodeContentItem, ConfigNodeContentItem> {

	private static DigitalLogger logger = DigitalLogger.getLogger(DigitalClearanceGenderSizeGenerationHandler.class);
	private List<String> queryStrings = null;
	private ENEConnection eneConnection = null;
	private String currentMdexHostName;
	private boolean httpsEnabled;
	private int siteHttpServerPort;
	private String serviceUrl;
	private String siteHttpServerName;
	private Map<String, String> applicationKeyToSites;
	private AssemblerApplicationConfiguration AssemblerApplicationConfiguration;
	private int bufferSize;
	private int retries = 0;
	private String	connectionTimeout;
	private String	soTimeout;
	private int currentMdexPort;

	public ENEConnection getEneConnection() {
		if (eneConnection == null) {
			eneConnection = new HttpENEConnection(currentMdexHostName, currentMdexPort);
		}
		return eneConnection;
	}

	/**
	 * 
	 * @param pCartridgeConfig
	 */
	@Override
	public void preprocess(ConfigNodeContentItem pCartridgeConfig) throws CartridgeHandlerException {
		if (null != pCartridgeConfig.get("clearanceGenderList")) {
			// clearanceGenderList exists in XM
			@SuppressWarnings("unchecked")
			List<ConfigNodeContentItem> clearanceGenderList = (List<ConfigNodeContentItem>) pCartridgeConfig
					.get("clearanceGenderList");
			queryStrings = new LinkedList<>();
			for (ConfigNodeContentItem clearanceGenderLink : clearanceGenderList) {
				LinkBuilder linkbuilder = (LinkBuilder) clearanceGenderLink.get("linkText");
				String queryString = linkbuilder.getQueryString();
				queryStrings.add(queryString);
			}
		}
		super.preprocess(pCartridgeConfig);
	}

	/**
	 * 
	 * @param pCartridgeConfig
	 */
	@Override
	public ConfigNodeContentItem process(ConfigNodeContentItem pCartridgeConfig) throws CartridgeHandlerException {
		String methodName = "process";
		DigitalPerformanceMonitorUtil.startPerformanceMonitorOperation(this.getClass().getName(), methodName);
		try {
			// get the path to the clearance sizes defined by the business
			String cartridgePath = (String) pCartridgeConfig.get("clearanceCatridge");
			String genderLabel = "";
			List<DigitalNavState> sizeNavPojoList = new LinkedList<>();
			Map<String, String> paramMap = new HashMap<>();
			Map<String, List<DigitalNavState>> genderSizeMap = new HashMap<>();

			// get current site
			String pushSite = applicationKeyToSites
					.get(getAssemblerApplicationConfiguration().getCurrentApplicationKey());
			// get current locale
			paramMap.put(DigitalEndecaConstants.LOCALE,
					getAssemblerApplicationConfiguration().getCurrentApplicationLocale().toString());
			paramMap.put("pushSite", pushSite);

			HttpClient client = new HttpClient();

			if(retries > 0) {
				client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(retries, true));
			} else {
				client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(retries, false));
			}
			HttpConnectionManager connectionManager = client.getHttpConnectionManager();
			if( connectionManager != null ) {
				HttpConnectionManagerParams managerParams = connectionManager.getParams();
				if( managerParams != null ) {
					managerParams.setParameter( HttpConnectionParams.CONNECTION_TIMEOUT, Integer.parseInt( getConnectionTimeout() ) * 1000 );
					managerParams.setParameter( HttpConnectionParams.SO_TIMEOUT, Integer.parseInt( getSoTimeout() ) * 1000 );
				}
			}
			for (String queryString : this.getQueryStrings()) {
				String pagePathUrl = DigitalEndecaUtils.generateContentPathUrl(httpsEnabled, siteHttpServerName,
						siteHttpServerPort, serviceUrl, queryString, cartridgePath, paramMap);
				GetMethod method = new GetMethod(pagePathUrl);
				DigitalEndecaUtils.setMethodHeaders( method );
				int statusCode;

				statusCode = client.executeMethod(method);
				if (statusCode != HttpStatus.SC_OK) {
					logger.error(
							"SEVERE ERROR: Problem while making GET request (status code != 200) to Home page in DSWDimensionValueCacheRefreshHandler. DimensionValueCache won't have Top Nav and Footer navigation states");
					throw new CartridgeHandlerException(
							"Problem while making GET request to Home page in DigitalDimensionValueCacheRefreshHandler. DimensionValueCache won't have Top Nav and Footer navigation states");
				}
				String response = getResponseBody(method);
				JSONObject finalJson = JSONInputCustomizer.isValidJSONString(response);

				if (response != null && finalJson.has(DigitalEndecaConstants.CONTENTITEM)) {
					JSONObject contentContentItem = (JSONObject) finalJson.get(DigitalEndecaConstants.CONTENTITEM);
					if (contentContentItem != null && contentContentItem.length() > 0
							&& contentContentItem.has(DigitalEndecaConstants.CONTENTS)) {
						JSONArray contents = (org.json.JSONArray) contentContentItem.get(DigitalEndecaConstants.CONTENTS);
						if (contents != null && contents.length() > 0) {
							JSONObject refinementContent = (JSONObject) contents.get(0);
							if (refinementContent.has(DigitalEndecaConstants.REFINEMENTS)) {
								genderLabel = (String) refinementContent.get("shopBySize");
								JSONArray dimValList = (JSONArray) refinementContent.get(DigitalEndecaConstants.DIMVALIST);
								JSONArray refinementList = (JSONArray) refinementContent
										.get(DigitalEndecaConstants.REFINEMENTS);
								if (refinementContent.has(DigitalEndecaConstants.DIMVALIST) && dimValList.length() > 0) {
									for (int i = 0; i < dimValList.length(); i++) {
										DigitalNavState navPojo = new DigitalNavState();
										JSONObject dimVal = dimValList.getJSONObject(i);
										String sizeStr = (String) dimVal.get(DigitalEndecaConstants.NAME);
										String iD = (String) dimVal.get(DigitalEndecaConstants.ID);
										String navStr = CartridgeHandlerUtil
												.generateSEONvalue(queryString + DigitalEndecaConstants.PLUS + iD);
										navPojo.setLabel(sizeStr);
										navPojo.setNavState(navStr);
										sizeNavPojoList.add(navPojo);
									}
								} else {
									for (int i = 0; i < refinementList.length(); i++) {
										DigitalNavState navPojo = new DigitalNavState();
										JSONObject refinement = refinementList.getJSONObject(i);
										String sizeStr = (String) refinement.get(DigitalEndecaConstants.LABEL);
										String navStr = (String) refinement.get(DigitalEndecaConstants.NAVIGATIONSTATE);
										navPojo.setLabel(sizeStr);
										navPojo.setNavState(navStr);
										sizeNavPojoList.add(navPojo);
									}
								}
							}
						}

					} else {
						pCartridgeConfig.put(DigitalEndecaConstants.SIZE, DigitalEndecaConstants.BLANK);
						pCartridgeConfig.put(DigitalEndecaConstants.RESPONSE, "Empty Contents");
					}
				}
				genderSizeMap.put(genderLabel, sizeNavPojoList);
				sizeNavPojoList = new LinkedList<>();
			}
			pCartridgeConfig.put(DigitalEndecaConstants.CLEARANCE_GENDER_SIZE_GENDERPARAM, genderSizeMap);
		} catch (Exception e) {
			logger.error("DigitalClearanceGenderSizeGenerationHandler process exception", e);
		} finally {
			DigitalPerformanceMonitorUtil.endPerformanceMonitorOperation(this.getClass().getName(), methodName);
		}
		return pCartridgeConfig;
	}

	private String getResponseBody(GetMethod method) {
		if (method != null && method.hasBeenUsed()) {
			StringWriter stringOut = new StringWriter();
			try (BufferedWriter dumpOut = new BufferedWriter(stringOut, bufferSize);
					BufferedReader in = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()))) {
				String line = "";
				while ((line = in.readLine()) != null) {
					dumpOut.write(line);
					dumpOut.newLine();
				}
			} catch (IOException e) {
				logger.error("DigitalTopNavigationListHandler IO exception", e);
			}

			return stringOut.toString();
		}
		return null;
	}

	@Override
	protected ConfigNodeContentItem wrapConfig(ContentItem arg0) {
		return (ConfigNodeContentItem) arg0;
	}

}
