package com.digital.commerce.endeca.index.accessor;

import java.util.ArrayList;
import java.util.List;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * This class is used to calculate the splitProductColors based on color
 * property.
 * 
 * @author oracle
 *
 */
@Getter
@Setter
public class DigitalSplitColorAccessor extends PropertyAccessorImpl {
	
	private String colorPropertyName;
	
	private String displayNamePropertyName;
	
	@Override
	protected Object getTextOrMetaPropertyValue( Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType ) {
		List<String> splitColorList = new ArrayList<>();
		RepositoryItem colorItem = null;
		String colorDisplayName = null;
		if (pItem != null) {
			colorItem = (RepositoryItem) pItem.getPropertyValue(getColorPropertyName());
			if (colorItem != null) {
				colorDisplayName = (String) colorItem.getPropertyValue(getDisplayNamePropertyName());
				generateSplitColor(splitColorList, colorDisplayName);
			}
		}
		return splitColorList;
	}
	
	private List<String> generateSplitColor(List<String> splitColorList, String colorDisplayName) {
		String[] commaSplitColor = null;
		String[] splitColor = null;
		if (!StringUtils.isBlank(colorDisplayName)) {
			colorDisplayName = colorDisplayName.replace(" ", ",");
			commaSplitColor = colorDisplayName.split(",");
			if (commaSplitColor != null && commaSplitColor.length > 0 && commaSplitColor[0].contains("/")) {
				splitColor = commaSplitColor[0].split("/");
				for (String lColor : splitColor) {
					splitColorList.add(lColor);
				}
			} else {
				splitColorList.add(commaSplitColor[0]);
			}
		}
		return splitColorList;
	}
}
