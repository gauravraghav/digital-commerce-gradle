package com.digital.commerce.endeca.index.accessor;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import com.digital.commerce.common.logger.DigitalLogger;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;

public class DemandNewArrivalSortPropertyAccessor extends PropertyAccessorImpl {

    private static final DigitalLogger logger = DigitalLogger.getLogger(DemandNewArrivalSortPropertyAccessor.class);
    private int newArrivalcutoff;

    public int getNewArrivalcutoff() {
        return newArrivalcutoff;
    }

    public void setNewArrivalcutoff(int newArrivalcutoff) {
        this.newArrivalcutoff = newArrivalcutoff;
    }

    protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
                                                PropertyTypeEnum pType) {

        try {

            Object productDemandObj = pItem.getPropertyValue(DigitalEndecaConstants.PRODUCT_DEMAND);
            Object productDaysAvailableObj = pItem.getPropertyValue(DigitalEndecaConstants.PRODUCT_DAYSAVAILABLE);

            if (null != productDemandObj && null != productDaysAvailableObj) {
                int productDemand  = Integer.parseInt(productDemandObj.toString());
                int productDaysAvailable = Integer.parseInt(productDaysAvailableObj.toString());

                // RULE: If Product is New then sort it using Demand $, otherwise bury it.
                if (productDaysAvailable < getNewArrivalcutoff()) {
                    return productDemand;
                } else {
                    return 0;
                }
            }

            return 0; // If any data doesn't exist then return 0

        } catch (Exception e) {
            logger.error("Exception occurred while calculating product.demandNewArrivalSort", e);
            return 0;
        }
    }
}
