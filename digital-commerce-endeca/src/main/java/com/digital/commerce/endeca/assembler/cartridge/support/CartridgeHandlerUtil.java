package com.digital.commerce.endeca.assembler.cartridge.support;

import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;

public class CartridgeHandlerUtil {
	
	public static String generateSEONvalue(String queryStr) {
		String finalSeoString=null;
		StringBuilder seoString = new StringBuilder();
		if (queryStr != null) {			
			String[] navStates=queryStr.split(DigitalEndecaConstants.AMPERSAND);
			String searchQuery=null;
			String queryString=null;
			for (String navState : navStates) {
				if(navState.contains(DigitalEndecaConstants.NTT)){
					searchQuery=navState;
				}
				if(navState.contains(DigitalEndecaConstants.N)){
					queryString=navState;
				}
			}
			if (queryString != null) {	
				queryString = queryString.substring(queryString.indexOf(DigitalEndecaConstants.EQUALTO) + 1,
						queryString.length());
				String[] dimValIds = queryString.split(DigitalEndecaConstants.NVALUESEPARATOR);
				String seoVar = null;
				int count = 0;
				for (String dimValId : dimValIds) {				
					seoVar = Long.toString(Long.parseLong(dimValId), 36);
					if (count < dimValIds.length - 1) {
						seoString.append(seoVar).append(
								DigitalEndecaConstants.DIMVALSEPARATOR);
					}
	
					else if (count == dimValIds.length - 1) {
						seoString.append(seoVar);
					}
	
					count++;
				}
			}
			if(queryString!=null && searchQuery!=null){
				finalSeoString=DigitalEndecaConstants.SEODELIMITER+DigitalEndecaConstants.SEOPREFIX+seoString.toString()+DigitalEndecaConstants.QUESTIONMARK+searchQuery;
			}else if(queryString!=null){
				finalSeoString=DigitalEndecaConstants.SEODELIMITER+DigitalEndecaConstants.SEOPREFIX+seoString.toString();
			}else if(searchQuery!=null){
				finalSeoString=DigitalEndecaConstants.QUESTIONMARK+searchQuery;
			}
			
		}
		return finalSeoString;
	}

}
