package com.digital.commerce.endeca.assembler.cartridge.handler;

import java.util.List;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.assembler.cartridge.support.CartridgeHandlerUtil;
import com.digital.commerce.endeca.assembler.constants.DigitalEndecaConstants;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.cartridge.model.LinkBuilder;
import com.endeca.infront.content.support.ConfigNodeContentItem;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.request.BreadcrumbsMdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.ENEQueryResults;

public class DigitalPercentSavingLinkHandler extends NavigationCartridgeHandler<ConfigNodeContentItem, ConfigNodeContentItem> {
	
	private MdexRequest mMdexRequest;

	@Override
	public void preprocess(ConfigNodeContentItem pCartridgeConfig) throws CartridgeHandlerException {
		super.preprocess(pCartridgeConfig);
		this.mMdexRequest = createMdexRequest(getNavigationState().getFilterState(), new BreadcrumbsMdexQuery());
	}

	@Override
	public ConfigNodeContentItem process(ConfigNodeContentItem pCartridgeConfig) throws CartridgeHandlerException {
		ENEQueryResults results = executeMdexRequest(this.mMdexRequest);
		NavigationState navigationState = getNavigationState();
		navigationState.inform(results);
		LinkBuilder linkBuilder = (LinkBuilder) pCartridgeConfig.get(DigitalEndecaConstants.LINKTEXT);
		if (linkBuilder.getQueryString() != null) {
			StringBuilder navParams = new StringBuilder(DigitalEndecaConstants.NEQUALS);

			List<String> incomingNavFilters = getNavigationState().getFilterState().getNavigationFilters();
			boolean dontAppendDiscount = false;
			for (String incomingNavFilter : incomingNavFilters) {
				if ((null != getNavigationState().getDimLocation(incomingNavFilter))){
					if ((!DigitalStringUtil.equals(
							getNavigationState().getDimLocation(incomingNavFilter).getDimValue().getDimensionName(),
							DigitalEndecaConstants.PRODUCT_DISCOUNT_BUCKET)) && (!DigitalStringUtil.equals(
									getNavigationState().getDimLocation(incomingNavFilter).getDimValue().getDimensionName(),
									DigitalEndecaConstants.PRODUCT_SAVINGS_BUCKET))) {
						navParams.append(incomingNavFilter + DigitalEndecaConstants.PLUS);
					} else if(!dontAppendDiscount) { //This condition is to handle multi-select savings Bucket
						navParams.append(DigitalStringUtil.substringAfter(linkBuilder.getQueryString(), DigitalEndecaConstants.NEQUALS)); // This
																											// code
																											// is
																											// added
																											// at
																											// this
																											// particular
																											// location
																											// to
																											// maintain
																											// the
																											// filter
																											// location
																											// of
																											// the
																											// discountBucket
						dontAppendDiscount = true;
						navParams.append(DigitalEndecaConstants.PLUS);
					}
				}
			}
			if(!dontAppendDiscount){
				navParams.append(DigitalStringUtil.substringAfter(linkBuilder.getQueryString(), DigitalEndecaConstants.NEQUALS));
			}
			else{
				navParams.deleteCharAt(navParams.length()-1);
			}
			linkBuilder.setQueryString(CartridgeHandlerUtil.generateSEONvalue(navParams.toString()));
			pCartridgeConfig.put(DigitalEndecaConstants.LINKTEXT, linkBuilder);
		}
		return pCartridgeConfig;
	}

	@Override
	protected ConfigNodeContentItem wrapConfig(ContentItem paramContentItem) {
		return (ConfigNodeContentItem) paramContentItem;
	}

}