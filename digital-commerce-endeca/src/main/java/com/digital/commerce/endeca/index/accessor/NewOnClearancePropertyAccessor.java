package com.digital.commerce.endeca.index.accessor;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import atg.commerce.pricing.priceLists.PriceListException;
import atg.commerce.pricing.priceLists.PriceListManager;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.digital.commerce.common.multisite.MultiSiteUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewOnClearancePropertyAccessor extends PropertyAccessorImpl {
	private static final Object	UNDEFINED		= false;
	private static final String	CLEARANCE_DATE	= "clearanceDate";
	private PriceListManager	priceListManager;
	private AccessorHelper		helper;

	protected Object getTextOrMetaPropertyValue( Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType ) {

		if( isLoggingDebug() ) {
			logDebug( "pContext =" + pContext );
			logDebug( "pItem =" + pItem );
			logDebug( "pPropertyName =" + pPropertyName );
			logDebug( "pType =" + pType );
		}

		Object newOnClearance = NewOnClearancePropertyAccessor.UNDEFINED;

		try {
			// get price list
			RepositoryItem priceList = (RepositoryItem)helper.getActivePriceList( pContext, pItem );

			// priceList may be NULL (rarely). Skip this logic if that happens
			if( priceList != null ) {
				// get price for the item
				RepositoryItem priceItem = priceListManager.getPrice( priceList, null, pItem );

				// business logic to check to see if Clearance Date is within 30
				// days and return appropriate value
				Object clearanceDateObj = priceItem.getPropertyValue(CLEARANCE_DATE); 
				if( priceItem != null && clearanceDateObj != null ) {
					// Number of days to check to set newOnClearance flag
					Timestamp clearanceDateTimestamp = (Timestamp)clearanceDateObj;
					Date clearanceDate = new Date( clearanceDateTimestamp.getTime() );

					Calendar calNewOnClearance = Calendar.getInstance();
					calNewOnClearance.add( Calendar.DATE, -MultiSiteUtil.getNewOnClearanceDays() );
					Date newOnClearanceDate = calNewOnClearance.getTime();

					int nbrDays = clearanceDate.compareTo( newOnClearanceDate );

					// if clearance date is within the number of days set
					// newOnClearance to true
					newOnClearance = nbrDays >= 0 ? true : false;
					if( isLoggingDebug() ) {
						logDebug( "NewOnClearance =" + newOnClearance + ", nbrDays = " + nbrDays + ", clearanceDate = " + clearanceDate + ", newOnClearanceDate = " + calNewOnClearance.getTime() );
					}
				}
			}

		} catch( PriceListException e ) {
			logError( "Exception reading price for SKU: " + pItem, e );
		}

		return newOnClearance;
	}
}
