/**
 * 
 */
package com.digital.commerce.endeca.recordstore;

/**
 * @author oracle
 *
 */
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import javax.sql.DataSource;

import com.digital.commerce.common.util.DigitalStringUtil;
import com.digital.commerce.endeca.inventory.vo.InventoryVO;
import com.endeca.itl.record.PropertyValue;
import com.endeca.itl.record.Record;
import com.endeca.itl.recordstore.RecordStore;
import com.endeca.itl.recordstore.RecordStoreConfiguration;
import com.endeca.itl.recordstore.RecordStoreException;
import com.endeca.itl.recordstore.RecordStoreLocator;
import com.endeca.itl.recordstore.RecordStoreWriter;
import com.endeca.itl.recordstore.TransactionId;
import com.endeca.itl.recordstore.TransactionType;

import atg.core.util.StringUtils;
import atg.endeca.index.EndecaIndexingOutputConfig;
import atg.endeca.index.Indexable;
import atg.endeca.index.admin.IndexingTask;
import atg.nucleus.GenericService;
import atg.repository.Query;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.search.indexing.BulkLoaderResults;
import atg.repository.search.indexing.IndexingException;
import atg.repository.search.indexing.LoaderResults;
import atg.repository.search.indexing.VariantProducer;

/**
 * 
 * @author oracle
 *
 */
public class PartialUpdateExporter extends EndecaIndexingOutputConfig implements Indexable, IndexingTask.StatusReporter {
	
	public static final String CATALOG_VAR_PRODUCER = "CustomCatalogVariantProducer";
	public static final String PRICE_LIST_VAR_PRODUCER = "PriceListPairVariantProducer";
	public static final String LOCALE_VAR_PRODUCER = "LocaleVariantProducer";
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private Repository mRepository;
	
	private Repository siteRepository;
	
	private Repository inventoryRepository;
	
	private Repository productCatalogRepository;
	
	private String	changedInventorySQL;
	
	private String	fullInventorySQL;
	
	private int	sendBatchSize;
	
	private String	lastRunColumnName;
	
	private String	lastRunSQL;
	private String	startRunSQL;
	private String	endRunSQL;
	
	private String cleanStateTableSQL;
	
	private int casPort;
	private String casHost;
	private String endecaTypeAheadId;
	private String recordStoreName;
	private int	completedRecords = 0;
	
	private String productFullInventorySql;
	
	private DataSource inventoryDataSource;
	
	private String changeInventorySQL;
	
	private VariantProducer[] mVariantProducers;
	
	Map<String, InventoryVO> processPrdMap = new ConcurrentHashMap<String, InventoryVO>();
	Integer threadCount = 5;

	
	
	public Integer getThreadCount() {
		return threadCount;
	}

	public void setThreadCount(Integer threadCount) {
		this.threadCount = threadCount;
	}

	public PartialUpdateExporter() throws RemoteException {
		super();
	}
	RecordStoreWriter writer = null;
	RecordStore recordStore = null;
	TransactionId tId = null;
	/**
	 * <Description> This method is used to fetch the partial data and write the
	 * data to the Endeca record.
	 */
	public void processPartialData() {
		
		boolean rollback = true;
		Collection<Record> records = new LinkedList<Record>();
		try {
			RecordStoreLocator locator = RecordStoreLocator.create(getCasHost(), getCasPort(), getRecordStoreName());
			recordStore = locator.getService();
			RecordStoreConfiguration config = new RecordStoreConfiguration();
			config.setIdPropertyName(getEndecaTypeAheadId());
			config.setChangePropertyNames(new ArrayList<String>());
			recordStore.setConfiguration(config);
			logInfo(".....Starting a new transaction ...");
			tId = recordStore.startTransaction(TransactionType.READ_WRITE);
			writer = RecordStoreWriter.createWriter(recordStore, tId, getSendBatchSize());
			completedRecords = 0;
			Timestamp lastRun = getLastRun();
			if( isLoggingInfo() ) {
				logInfo( "Inside run. lastRun: " + lastRun );
			}
			setStartRun();
			
			
			Map<String, InventoryVO> fullInventoryMap = processPartialInventory( lastRun );
		
            CompletableFuture<List<Record>> future =
				        CompletableFuture.supplyAsync(() -> {
							try {
								return writeSKUData(fullInventoryMap);
								
							} catch (Exception e1) {
								if( isLoggingError() ) {
									logError( "Error in processing Record for records:" + fullInventoryMap.keySet(), e1 );
								}
							}
							return null;
						}).whenComplete((result, ex) -> {
							if(isLoggingInfo()){
								logInfo("Proccessed record:");
							}
		                    if (null != ex) {
		                        logError("Error in processing Record for records:" + fullInventoryMap.keySet(), ex);
		                    }
		                });
		                
					try {
						
						records = future.get();
					} catch (InterruptedException e) {
						if( isLoggingError() ) {
							logError( "Error in processing Record for records:" + fullInventoryMap.keySet(), e );
						}
					} catch (ExecutionException e) {
						if( isLoggingError() ) {
							logError( "Error in processing Record for records:" + fullInventoryMap.keySet(), e );
						}
					}
	            
	        
			setEndRun();
			
			logInfo("Total records proccessed: " + records.size());
			
			records.forEach((record) -> {System.out.println("Proccessed record: ");record.getAllPropertyNames().stream().
			forEach((v) -> {System.out.print(" Prop: "+ v +"="+record.getPropertySingleValue(v));});});
			
			writer.write(records);
			writer.close();
			recordStore.commitTransaction(tId);
			rollback = false;
		} catch (RecordStoreException ex) {			
			if( isLoggingError() ) {
				logError( "General Exception while writing to CAS in .....", ex );
			}
		} catch( Exception ex ) {
			if( isLoggingError() ) {
				logError( "General Exception while writing to CAS in .....", ex );
			}
		} finally {
			//clear global map
			processPrdMap.clear();
			if (rollback && tId != null) {
				try {
					recordStore.rollbackTransaction(tId);
				} catch (RecordStoreException e) {
					if( isLoggingError() ) {
						logError( "Failed to roll back transaction.", e);
					}
				}
			}
			if( rollback && null != writer ) {
			try {
				writer.close();
			} catch (Exception e) {
				logError("Excpetion in closing writer for Partial index", e);
			} 
		}
		}
	}
	
	/**
	 * <Description> Update the table when the partial indexing end.
	 * 
	 * @throws SQLException
	 */
	private void setEndRun() throws SQLException {
		if( isLoggingInfo() ) {
			logInfo( "Inside setEndRun. running: ");
		}
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = this.getInventoryDataSource().getConnection();
			statement = connection.prepareStatement( getEndRunSQL() );
			statement.setTimestamp( 1, new Timestamp( System.currentTimeMillis() ) );
			statement.executeUpdate();
			if( isLoggingInfo() ) {
				logInfo( "Completed setEndRun" );
			}
		} catch( SQLException sqlex ) {
			if( isLoggingError() ) logError( "Error setting run end time." );
			throw sqlex;
		} finally {
			if( statement != null ) {
				try {
					statement.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing JDBC  statement close in setEndRun", ex );
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing connection close in setEndRun", ex );
				}
			}
		}
	}
	
	/**
	 * <Description> Update the table when the partial indexing start.
	 * 
	 * @throws SQLException
	 */
	private void setStartRun() throws SQLException {
		if( isLoggingInfo() ) {
			logInfo( "Inside setStartRun. running: ");
		}
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = this.getInventoryDataSource().getConnection();
			statement = connection.prepareStatement( getStartRunSQL() );
			statement.setTimestamp( 1, new Timestamp( System.currentTimeMillis() ) );
			statement.executeUpdate();
			if( isLoggingInfo() ) {
				logInfo( "Completed setStartRun" );
			}
		} catch( SQLException sqlex ) {
			if( isLoggingError() ) logError( "Error setting run start time." );
			throw sqlex;
		} finally {
			if( statement != null ) {
				try {
					statement.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing JDBC  statement close in setStartRun", ex );
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing connection close in setStartRun", ex );
				}
			}
		}
	}
	
	/**
	 * <Description> get the max time from this table to run the partial indexing.
	 * 
	 * @throws SQLException
	 */	
	private Timestamp getLastRun() throws SQLException {
		if( isLoggingInfo() ) {
			logInfo( "Inside getLastRun. running: ");
		}
		Connection connection = null;
		PreparedStatement lastRunStatement = null;
		ResultSet results = null;
		Timestamp lastRun = null;
		try {
			connection = this.getInventoryDataSource().getConnection();
			lastRunStatement = connection.prepareStatement( getLastRunSQL() );
			results = lastRunStatement.executeQuery();

			if( results.next() ) {
				lastRun = results.getTimestamp( this.getLastRunColumnName() );
			}
			if( isLoggingInfo() ) {
				if( lastRun == null ) {
					logInfo( "No last run time found." );
				} else {
					logInfo( "Last successful run was at - " + lastRun.toString() );
				}
			}

		} catch( SQLException sqlex ) {
			if( isLoggingError() ) logError( "Error fetching last run time." );
			throw sqlex;
		} finally {
			if( results != null ) {
				try {
					results.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing resultlist close in getLastRun", ex );
				}
			}
			if( lastRunStatement != null ) {
				try {
					lastRunStatement.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing JDBC  statement close in getLastRun", ex );
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					logInfo( "Error while doing connection close in getLastRun", ex );
				}
			}
		}
		return lastRun;

	}
	
	/**
	 * <Description> Write the SKU inventory data to the endeca record.
	 * @param records
	 * @param productItem
	 * @param processPrdList
	 * @param record
	 * @throws RepositoryException
	 */
	private Record writeProductData( RepositoryItem productItem, Record record) throws RepositoryException {
		InventoryVO productVO = null;
		if (processPrdMap.containsKey(productItem.getRepositoryId())) {
			productVO = processPrdMap.get(productItem.getRepositoryId());
		} else {
			productVO = processPartialProductInventory(productItem.getRepositoryId());
			if(null != productVO){
				processPrdMap.put(productItem.getRepositoryId(), productVO);
			} else {
				return record;
			}
		}
			String stockLevel = productVO.getStockLevel();
			Integer daysAvailable = (Integer) productItem.getPropertyValue("daysAvailable");
			record.addPropertyValue( new PropertyValue("product.inventory", productVO.getStockLevel()));
			record.addPropertyValue( new PropertyValue("product.repositoryId", productVO.getProductId() ));
			record.addPropertyValue( new PropertyValue("product.clearanceInventory", productVO.getStockLevelClearance() ));
			if (!StringUtils.isBlank(stockLevel) && Integer.valueOf(stockLevel) < 100) {
				record.addPropertyValue( new PropertyValue("product.inventoryTitle", "low"));
			} else if (daysAvailable > 300) {
				record.addPropertyValue( new PropertyValue("product.inventoryTitle", "high"));
			} else {
				record.addPropertyValue( new PropertyValue("product.inventoryTitle", "medium"));
			}
			if (!processPrdMap.containsKey(productItem.getRepositoryId())) {			
				processPrdMap.put(productItem.getRepositoryId(), productVO);
			}
		return record;
	}
	
	/**
	 * <Description> Write the Product inventory data to the endeca record.
	 * @param records
	 * @param fullInventoryMap
	 * @throws RepositoryException
	 */
	private List<Record> writeSKUData(Map<String, InventoryVO> fullInventoryMap) throws Exception {
		
		List<Record> recordList = new ArrayList<Record>();
		
		fullInventoryMap.entrySet().stream().parallel().forEach(e -> {
			RepositoryItem skuItem = null;
			RepositoryItem productItem = null;
			InventoryVO inventoryVo = null;
			String skuId = null;
			try {
				skuId = e.getKey();
				inventoryVo = e.getValue();

				skuItem = getProductCatalogRepository().getItem(skuId, "sku");
				if (skuItem != null) {
					@SuppressWarnings("unchecked")
					Set<RepositoryItem> parentProducts = (Set<RepositoryItem>) skuItem
							.getPropertyValue("parentProducts");
					if (parentProducts != null && !parentProducts.isEmpty()) {
						productItem = new ArrayList<RepositoryItem>(parentProducts).get(0);
						
						String recordId = generateRecordId(skuId, productItem.getRepositoryId());
						if (isLoggingDebug()) {
							logDebug("Endeca Record Id : " + recordId);
						}
						if (isLoggingInfo()) {
							logInfo("Creating endeca record for record ID: " + recordId);
						}
						if(recordId != null) {
							String fullsiteStockLevel = inventoryVo.getFullsiteStockLevel();
							Record record = new Record();
							record.addPropertyValue(new PropertyValue("record.id", recordId));
							record.addPropertyValue(new PropertyValue("sku.inventory", inventoryVo.getFullsiteStockLevel()));
							record.addPropertyValue(new PropertyValue("sku.repositoryId", inventoryVo.getSkuId()));
							if (!DigitalStringUtil.equals(inventoryVo.getLocationId(), "no_store_data")
									&& (inventoryVo.getStoreStockLevel() > 0)) {
								record.addPropertyValue(new PropertyValue("sku.storeLocationId", inventoryVo.getLocationId()));
							}
							if (Integer.valueOf(fullsiteStockLevel) >= 1) {
								record.addPropertyValue(new PropertyValue("sku.hasInventory", "1"));
							} else {
								record.addPropertyValue(new PropertyValue("sku.hasInventory", "0"));
							}
							record = writeProductData(productItem, record);
							recordList.add(record);
						}
					} else {
						if(isLoggingWarning()) {
							logWarning("ParentProducts is null for SKU:" + skuId);
						}
					}
				}
				
				
			} catch (Exception ex) {
				if (isLoggingError()) {
					logError("Error in generating Record Id for SKU:" + skuId + "and product ID : "+ productItem + " -", ex);;
				}
			}
		});
		return recordList;
	}	
	
	/**
	 * 
	 * @param lastRun
	 * @return
	 * @throws Exception
	 */
	private Map<String, InventoryVO> processPartialInventory( Timestamp lastRun ) throws Exception {
		InventoryVO lInventoryVO;
		Map<String, InventoryVO> fullInventoryMap = new HashMap<String, InventoryVO>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;
		if( isLoggingInfo() ) {
			logInfo( "Starting processPartialInventory method ");
		}
		try {
			connection = this.getInventoryDataSource().getConnection();
			if( lastRun != null ) {
				statement = connection.prepareStatement( getChangedInventorySQL() );
				statement.setTimestamp( 1, lastRun, Calendar.getInstance() );
				statement.setTimestamp( 2, lastRun, Calendar.getInstance() );
				if( isLoggingDebug() ) logDebug( statement.toString() );
				if( isLoggingInfo() ) {
					logInfo("Extracting sku inventory that has changed from last run." );
					logInfo( "STARTING the execution of Inventory SQL query ChangedInventorySQLL at : " + new Timestamp( System.currentTimeMillis() ) );
				}
			} else {
				statement = connection.prepareStatement( getFullInventorySQL() );
				if( isLoggingInfo() ) {
					logInfo( "No previous run found, extracting  all sku inventory." );
					logInfo( "STARTING the execution of Inventory SQL query fullInventorySQL at : " + new Timestamp( System.currentTimeMillis() ) );
				}
			}

			results = statement.executeQuery();
			if( isLoggingInfo() ) {
				logInfo( "FINISHED the execution of Inventory SQL query at : " + new Timestamp( System.currentTimeMillis() ) );
			}
			completedRecords = 0;
			while( results.next() ) {
				lInventoryVO = new InventoryVO();
				String skuId = results.getString("catalog_ref_id");
				String fullSiteStockLevel = ((results.getString("fullsite_stock_level")) == null) ? "0"
						: results.getString("fullsite_stock_level");
				String locationId = results.getString("location_id");
				int storeStockLevel = ((results.getString("store_stock_level")) == null) ? 0
						: Integer.parseInt(results.getString("store_stock_level"));
				lInventoryVO.setSkuId(skuId);
				lInventoryVO.setLocationId(locationId);
				lInventoryVO.setStoreStockLevel(storeStockLevel);
				lInventoryVO.setFullsiteStockLevel(fullSiteStockLevel);
				fullInventoryMap.put(skuId, lInventoryVO);
			}
		} catch( SQLException sqlex ) {
			if( isLoggingError() ) {
				logError( "SQLException fetching inventory from processInventory" );
			}
			throw sqlex;
		} catch( Exception ex ) {
			if( isLoggingError() ) {
				logError( "General Exception while writing to CAS from processInventory", ex );
			}
			throw ex;
		} finally {
			if( results != null ) {
				try {
					results.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing resultset from processInventory", ex );
				}
			}
			if( statement != null ) {
				try {
					statement.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing statement from processInventory", ex );
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing connection from processInventory", ex );
				}
			}
		}
		return fullInventoryMap;
	}
	
	/**
	 * <Description> This method return the inventory information for the
	 * particular productId.
	 * 
	 * @param productId
	 * @return
	 */
	public InventoryVO processPartialProductInventory(String productId) {

		InventoryVO lInventoryVO = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;
		if( isLoggingInfo() ) {
			logInfo( "Starting product inventory extraction.");
		}
		try {
			connection = this.getInventoryDataSource().getConnection();
			statement = connection.prepareStatement( getProductFullInventorySql() );
			statement.setString(1, productId);
			if( isLoggingInfo() ) {
				logInfo( "STARTING the execution of Inventory SQL query productFullInventorySQL at : " + new Timestamp( System.currentTimeMillis() ) );
			}
			results = statement.executeQuery();
			if( isLoggingInfo() ) {
				logInfo( "FINISHED the execution of Inventory SQL query productFullInventorySQL at : " + new Timestamp( System.currentTimeMillis() ) );
			}
			if( isLoggingInfo() ) {
				logInfo( "Inside processProductInventory. Classloader: " + this.getClass().getClassLoader() );
			}
			if( results.next() ) {
				lInventoryVO = new InventoryVO();
				String prodId = results.getString("product_id");
				String stockLevel = results.getString("stock_level");
				String clearanceStockLevel = results.getString("stock_level_clearance");
				lInventoryVO.setProductId(prodId);
				lInventoryVO.setStockLevel(stockLevel);
				lInventoryVO.setStockLevelClearance(clearanceStockLevel);
			}
		} catch( SQLException sqlex ) {
			if( isLoggingError() ) {
				logError( "SQLException fetching product inventory in processProductInventory" );
			}
		} catch( Exception ex ) {
			if( isLoggingError() ) {
				logError( "General Exception in processProductInventory", ex );
			}
		} finally {
			if( results != null ) {
				try {
					results.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing resultset from processProductInventory", ex );
				}
			}
			if( statement != null ) {
				try {
					statement.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing statement from processProductInventory", ex );
				}
			}
			if( connection != null ) {
				try {
					connection.close();
				} catch( Exception ex ) {
					logInfo( "Exception while closing connection from processProductInventory", ex );
				}
			}
		}
		return lInventoryVO;
	}
	
	/**
	 * <Description> generate the record id to update the record in endeca
	 * 
	 * @param skuId
	 *            - SkuRepositoryId
	 * @param productId
	 *            - ProductRepositoryId
	 * @return - record id
	 */
	private String generateRecordId(String skuId, String productId) {
		StringBuilder recordId = new StringBuilder();
		RepositoryView view = null;
		Query query = null;
		RepositoryItem[] siteItems = null;
		String defaultLanguage = null;
		String defaultCountry = null;
		RepositoryItem defaultCatalog = null;
		RepositoryItem defaultListPriceList = null;
		RepositoryItem defaultSalePriceList = null;
		List<String> variantProducerNames = getVariantProducerName();
		try {
			view = getSiteRepository().getView("siteConfiguration");
			query = view.getQueryBuilder().createUnconstrainedQuery();
			siteItems = view.executeQuery(query);
			if (siteItems != null && siteItems.length > 0) {
				RepositoryItem lRepositoryItem = siteItems[0];
				defaultLanguage = (String) lRepositoryItem.getPropertyValue("defaultLanguage");
				defaultCountry = (String) lRepositoryItem.getPropertyValue("defaultCountry");
				defaultCatalog = (RepositoryItem) lRepositoryItem.getPropertyValue("defaultCatalog");
				defaultListPriceList = (RepositoryItem) lRepositoryItem.getPropertyValue("defaultListPriceList");
				defaultSalePriceList = (RepositoryItem) lRepositoryItem.getPropertyValue("defaultSalePriceList");
			}
			recordId.append("sku-").append(skuId).append("..").append(productId);
			if (variantProducerNames.contains(CATALOG_VAR_PRODUCER) && defaultCatalog != null) {
				recordId.append(".").append(defaultCatalog.getRepositoryId());
			}
			if (variantProducerNames.contains(LOCALE_VAR_PRODUCER) && !StringUtils.isBlank(defaultLanguage)) {
				recordId.append(".").append(defaultLanguage);
			}
			if (variantProducerNames.contains(LOCALE_VAR_PRODUCER) && !StringUtils.isBlank(defaultCountry)) {
				recordId.append("__").append(defaultCountry);
			}
			if (variantProducerNames.contains(PRICE_LIST_VAR_PRODUCER) && defaultSalePriceList != null) {
				recordId.append(".").append(defaultSalePriceList.getRepositoryId());
			}
			if (variantProducerNames.contains(PRICE_LIST_VAR_PRODUCER) && defaultListPriceList != null) {
				recordId.append("__").append(defaultListPriceList.getRepositoryId());
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("Error while generating the record id" + e);
			}

		}
		return recordId.toString();
	}
	
	/**
	 * <Description> This method fetch the OOTB variant producer name.
	 * 
	 * @return list of variant producer name.
	 */
	private List<String> getVariantProducerName() {
		List<String> variantProducerName = new ArrayList<>();
		VariantProducer[] producer = getVariantProducers();
		if (producer != null) {
			for (VariantProducer lVariantProducer : producer) {
				if (lVariantProducer instanceof GenericService) {
					variantProducerName.add(((GenericService)lVariantProducer).getName());
				}
			}
		}
		return variantProducerName;
	}
	
	@Override
	public LoaderResults performPartialUpdate(IndexingTask pTask) throws IndexingException {
		processPartialData();
		return new MinimalBulkLoaderResults( true );
	}
	
	/*@Override
	public BulkLoaderResults performBaselineUpdate(IndexingTask pTask)
			throws IndexingException {
		write();
		return new MinimalBulkLoaderResults( true );
		//return super.performBaselineUpdate(pTask);
	}*/
	
	public int getCasPort() {
		return casPort;
	}

	public void setCasPort(int casPort) {
		this.casPort = casPort;
	}

	public String getCasHost() {
		return casHost;
	}

	public void setCasHost(String casHost) {
		this.casHost = casHost;
	}

	public String getEndecaTypeAheadId() {
		return endecaTypeAheadId;
	}

	public void setEndecaTypeAheadId(String endecaTypeAheadId) {
		this.endecaTypeAheadId = endecaTypeAheadId;
	}

	public String getRecordStoreName() {
		return recordStoreName;
	}

	public void setRecordStoreName(String recordStoreName) {
		this.recordStoreName = recordStoreName;
	}

	@Override
	public void postIndexingCleanup( IndexingTask indexingTask, boolean b, LoaderResults loaderResults ) throws IndexingException {
		return;
	}
	
	public static class MinimalBulkLoaderResults extends BulkLoaderResults {

		/**
		 * 
		 */
		private static final long	serialVersionUID	= 1L;

		@Override
		public boolean isSuccessful() {
			return mSuccessful;
		}

		@Override
		public int getGeneration() {
			throw new UnsupportedOperationException();
		}

		boolean	mSuccessful;

		public MinimalBulkLoaderResults( boolean pSuccessful ) {
			mSuccessful = pSuccessful;
		}
	}
	
	@Override
	public int getSuccessCount() {
		return completedRecords;
	}

	@Override
	public int getFailureCount() {
		return 0;
	}
	
	@Override
	public boolean isNeededForIncremental() {
		return false;
	}

	@Override
	public boolean mayNeedCleanup() {
		return false;
	}
	
	@Override
	public boolean isSupportsStatusCounts() {
		return true;
	}

	@Override
	public Set<String> getIndexingOutputConfigPaths() {
		return new HashSet<>();
	}

	@Override
	public boolean isForceToBaseline() {
		return false;
	}

	/**
	 * @return the repository
	 */
	@Override
	public Repository getRepository() {
		return mRepository;
	}

	/**
	 * @param pRepository the repository to set
	 */
	@Override
	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}

	/**
	 * @return the inventoryRepository
	 */
	public Repository getInventoryRepository() {
		return inventoryRepository;
	}

	/**
	 * @param pInventoryRepository the inventoryRepository to set
	 */
	public void setInventoryRepository(Repository pInventoryRepository) {
		inventoryRepository = pInventoryRepository;
	}

	/**
	 * @return the productFullInventorySql
	 */
	public String getProductFullInventorySql() {
		return productFullInventorySql;
	}

	/**
	 * @param pProductFullInventorySql the productFullInventorySql to set
	 */
	public void setProductFullInventorySql(String pProductFullInventorySql) {
		productFullInventorySql = pProductFullInventorySql;
	}

	/**
	 * @return the inventoryDataSource
	 */
	public DataSource getInventoryDataSource() {
		return inventoryDataSource;
	}

	/**
	 * @param pInventoryDataSource the inventoryDataSource to set
	 */
	public void setInventoryDataSource(DataSource pInventoryDataSource) {
		inventoryDataSource = pInventoryDataSource;
	}

	/**
	 * @return the changeInventorySQL
	 */
	public String getChangeInventorySQL() {
		return changeInventorySQL;
	}

	/**
	 * @param pChangeInventorySQL the changeInventorySQL to set
	 */
	public void setChangeInventorySQL(String pChangeInventorySQL) {
		changeInventorySQL = pChangeInventorySQL;
	}

	/**
	 * @return the productCatalogRepository
	 */
	public Repository getProductCatalogRepository() {
		return productCatalogRepository;
	}

	/**
	 * @param pProductCatalogRepository the productCatalogRepository to set
	 */
	public void setProductCatalogRepository(Repository pProductCatalogRepository) {
		productCatalogRepository = pProductCatalogRepository;
	}

	/**
	 * @return the changedInventorySQL
	 */
	public String getChangedInventorySQL() {
		return changedInventorySQL;
	}

	/**
	 * @param pChangedInventorySQL the changedInventorySQL to set
	 */
	public void setChangedInventorySQL(String pChangedInventorySQL) {
		changedInventorySQL = pChangedInventorySQL;
	}

	/**
	 * @return the fullInventorySQL
	 */
	public String getFullInventorySQL() {
		return fullInventorySQL;
	}

	/**
	 * @param pFullInventorySQL the fullInventorySQL to set
	 */
	public void setFullInventorySQL(String pFullInventorySQL) {
		fullInventorySQL = pFullInventorySQL;
	}

	/**
	 * @return the sendBatchSize
	 */
	public int getSendBatchSize() {
		return sendBatchSize;
	}

	/**
	 * @param pSendBatchSize the sendBatchSize to set
	 */
	public void setSendBatchSize(int pSendBatchSize) {
		sendBatchSize = pSendBatchSize;
	}

	/**
	 * @return the lastRunColumnName
	 */
	public String getLastRunColumnName() {
		return lastRunColumnName;
	}

	/**
	 * @param pLastRunColumnName the lastRunColumnName to set
	 */
	public void setLastRunColumnName(String pLastRunColumnName) {
		lastRunColumnName = pLastRunColumnName;
	}

	/**
	 * @return the lastRunSQL
	 */
	public String getLastRunSQL() {
		return lastRunSQL;
	}

	/**
	 * @param pLastRunSQL the lastRunSQL to set
	 */
	public void setLastRunSQL(String pLastRunSQL) {
		lastRunSQL = pLastRunSQL;
	}

	/**
	 * @return the startRunSQL
	 */
	public String getStartRunSQL() {
		return startRunSQL;
	}

	/**
	 * @param pStartRunSQL the startRunSQL to set
	 */
	public void setStartRunSQL(String pStartRunSQL) {
		startRunSQL = pStartRunSQL;
	}

	/**
	 * @return the endRunSQL
	 */
	public String getEndRunSQL() {
		return endRunSQL;
	}

	/**
	 * @param pEndRunSQL the endRunSQL to set
	 */
	public void setEndRunSQL(String pEndRunSQL) {
		endRunSQL = pEndRunSQL;
	}

	/**
	 * @return the siteRepository
	 */
	public Repository getSiteRepository() {
		return siteRepository;
	}

	/**
	 * @param pSiteRepository the siteRepository to set
	 */
	public void setSiteRepository(Repository pSiteRepository) {
		siteRepository = pSiteRepository;
	}

	/**
	 * @return the cleanStateTableSQL
	 */
	public String getCleanStateTableSQL() {
		return cleanStateTableSQL;
	}

	/**
	 * @param pCleanStateTableSQL the cleanStateTableSQL to set
	 */
	public void setCleanStateTableSQL(String pCleanStateTableSQL) {
		cleanStateTableSQL = pCleanStateTableSQL;
	}

	/**
	 * @return the variantProducers
	 */
	@Override
	public VariantProducer[] getVariantProducers() {
		return mVariantProducers;
	}

	/**
	 * @param pVariantProducers the variantProducers to set
	 */
	@Override
	public void setVariantProducers(VariantProducer[] pVariantProducers) {
		mVariantProducers = pVariantProducers;
	}
}
